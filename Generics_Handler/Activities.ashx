<%@ WebHandler Language="VB" Class="Activities" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports MaintenanceRpt
Imports clsSubSonicCommonFunctions
Imports System.IO

Public Class Activities : Implements IHttpHandler, IRequiresSessionState 
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response

        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strid As String = request("MUser")
       
        Dim totalRecords As Integer
        Dim MaintenanceRpt As Collection(Of MaintenanceRpt) = Activities(strid, numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
        Dim output As String = BuildJQGridResults(MaintenanceRpt, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output)
    End Sub
 
   
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Private Function BuildJQGridResults(ByVal MaintenanceRpt As Collection(Of MaintenanceRpt), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String

        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Activities As MaintenanceRpt In MaintenanceRpt
            Dim row As New JQGridRow()
            row.cell = New String(8) {}
           
            row.cell(0) = Activities.strassignedto.ToString()
            row.cell(1) = Activities.strreqdate.ToString()
            row.cell(2) = Activities.strassignedtime.ToString()
            row.cell(3) = Activities.strcomments.ToString()
            row.cell(4) = Activities.strclosed.ToString()
            row.cell(5) = Activities.strtotal.ToString()
            row.cell(6) = Activities.strdelayedby.ToString()
            row.cell(7) = Activities.strstatus.ToString()
            row.cell(8) = Activities.stralert.ToString()
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords / numberOfRows
        result.records = totalRecords
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function
    
    Private Function Activities(ByVal strid As String, ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of MaintenanceRpt)
        Dim users As New Collection(Of MaintenanceRpt)()
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strid
        
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = pageIndex
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(2).Value = sortColumnName
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(3).Value = sortOrderBy
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = Convert.ToInt32(numberOfRows)
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = 0
        param(5).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_MAINTENANCE_ACTIVITIES", param)
        
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim user As MaintenanceRpt
            user = New MaintenanceRpt()
            'user.strrequisitionid = "<a href=../../EFM/EFM_WEBfiles/frmUpdateUserMaintenance.aspx?id=" + Convert.ToString(ds.Tables(0).Rows(i).Item("REQUESITION_ID")) + ">" + Convert.ToString(ds.Tables(0).Rows(i).Item("REQUESITION_ID")) + "</a>"
      
            user.strassignedto = Convert.ToString(ds.Tables(0).Rows(i).Item("ASSIGNED_TO"))
            user.strreqdate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("REQUESTED_DATE"))
            user.strassignedtime = Convert.ToString(ds.Tables(0).Rows(i).Item("ASSIGNED_TIME"))
            user.strcomments = Convert.ToString(ds.Tables(0).Rows(i).Item("COMMENTS"))
            user.strclosed = Convert.ToString(ds.Tables(0).Rows(i).Item("CLOSED_TIME"))
            user.strtotal = Convert.ToString(ds.Tables(0).Rows(i).Item("TOTAL_TIME"))
            user.strdelayedby = Convert.ToString(ds.Tables(0).Rows(i).Item("DELAYED_TIME"))
            user.strstatus = Convert.ToString(ds.Tables(0).Rows(i).Item("REQUEST_STATUS"))
            If user.strstatus = "Pending" Then
                '    HttpContext.Current.Response.Write("../../images/icon_alert.gif")
                user.stralert = "<Img Src='../../images/icon_alert.gif' title='Pending' />"
            ElseIf user.strstatus = "InProgress" Then
                user.stralert = "<Img Src='../../images/error_small.png' title='InProgress' />"
            ElseIf user.strstatus = "Closed" Then
                user.stralert = "<Img Src='../../images/alert_icon.gif' title='Closed' />"
            End If
            
            users.Add(user)
            totalRecords = CInt(ds.Tables(1).Rows(0).Item("TotalRecords"))
        Next
         
        Return users
    End Function

End Class