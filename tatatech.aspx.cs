﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OneLogin.SamlTatatech;

public partial class tatatech : System.Web.UI.Page
{
    public static bool CreateLogFile(string message)
    {
        try
        {
            // string location = @"C://IRPC//myfile1.txt";
            string filename = "NewLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            string location = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") + filename); // ''(System.Environment.CurrentDirectory + ("\log.txt"))
            if (!File.Exists(location))
            {
                FileStream fs;
                fs = new FileStream(location, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                fs.Close();
            }

            Console.WriteLine(message);
            // Release the File that is created
            StreamWriter sw = new StreamWriter(location, true);
            sw.Write((message + Environment.NewLine));
            sw.Close();
            sw = null;
            return true;
        }
        catch (Exception ex)
        {
            EventLog.WriteEntry("MIDocShare", ("Error in CreateLogFile" + ex.Message.ToString()), EventLogEntryType.Error, 6000);
            return false;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateLogFile("Saml Start At " + DateTime.Now);
        CreateLogFile("Saml Response" + Request.Form["SAMLResponse"]);

        AccountSettings accountSettings = new AccountSettings();
        if (Request.Form["SAMLResponse"] == "" || Request.Form["SAMLResponse"] == null)
        {
            AuthRequest req = new AuthRequest(new AppSettings2(), accountSettings);
            //string st = accountSettings.idp_sso_target_url;//+ "?SAMLRequest=" + Server.UrlEncode(req.GetRequest(AuthRequest.AuthRequestFormat.Base64));
            string st = accountSettings.TataTechidp_sso_target_url + "?SAMLRequest=" + Server.UrlEncode(req.GetRequest(AuthRequest.AuthRequestFormat.Base64));
            Response.Redirect(st);
        }
        else
        {
            CreateLogFile("Saml Start At " + DateTime.Now);
            Response samlResponse = new Response(accountSettings);
            samlResponse.LoadXmlFromBase64(Request.Form["SAMLResponse"]);

            CreateLogFile("Saml Response" + Request.Form["SAMLResponse"]);

            string email_id = "";
            string emp_id = "";
            string filename = "";
            string filepath = "";
            string str1 = "~/LogTatatech.aspx?company=";
            string redirect_url = "";

            if (samlResponse.IsValid())
            {
                //CreateLogFile("Start In IsValid");
                emp_id = samlResponse.GetNameID();
                email_id = samlResponse.GetNameID();
                CreateLogFile("emp_id:- " + emp_id + " email_id : - " + email_id);
                redirect_url = str1 + "Tatatech" + "&empid=" + emp_id + "&email=" + email_id;
                CreateLogFile("Redirect URL: " + redirect_url);
                //Server.TransferRequest(redirect_url);
                Response.Redirect(redirect_url);
                //CreateLogFile("emp_id:- " + emp_id + " email_id : - " + email_id); 
            }
            else
            {

                Response.Write("114 Failed");
                //CreateLogFile("Error in Isvalid");
            }
        }
    }

}