﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap.css" rel="stylesheet" />
    
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .row {
            margin-top: 40px;
            padding: 0 10px;
        }

        .clickable {
            cursor: pointer;
        }

        .panel-heading span {
            margin-top: -20px;
            font-size: 15px;
        }
    </style>
</head>

<body data-ng-controller="SystemPreferencesController">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>System Preferences</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frm" class="form-horizontal well">
                        <%--<div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <div class="box">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <label><b>Module</b> <span style="color: red;">*</span></label>
                                    <select id="Select4" data-ng-model="Currentmodule.MOD_NAME" class="form-control" name="MOD_NAME" data-live-search="true">
                                        <option value="ALL" selected>ALL</option>
                                        <option data-ng-repeat="mod in modules" value="{{mod.MOD_NAME}}">{{mod.MOD_NAME}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary' />
                                    <a class="btn btn-primary" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Space Management</h3>
                                        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="box box-primary">
                                            <div class="box-body">
                                                <div class="col-md-12">
                                                    <label class="col-md-10"><b>Level 2 Approval No. Of Seats:</b></label>
                                                    <div class="col-md-3">
                                                        <input type="text" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-12">
                                                    <label class="col-md-10"><b>Free Seat Overload Count: </b></label>
                                                    <div class="col-md-3">
                                                        <input type="text" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-12">
                                                    <label class="col-md-10"><b>Utilization Threshold: </b></label>
                                                    <div class="col-md-3">
                                                        <input type="text" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-md-12">
                                                    <label class="col-md-10"><b>Space Requisition Workflow: </b></label>
                                                    <div class="alert alert-info">
                                                        <div class="form-inline">
                                                            <label class="radio">
                                                                <input value="1" type="radio">Department</label>
                                                            <label class="radio">
                                                                <input value="2" type="radio">Employee</label>
                                                            <label class="radio">
                                                                <input value="3" type="radio">Request Count per Employee</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Helpdesk Management</h3>
                                        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="box box-primary">
                                            <div class="box-body">
                                                <div class="col-md-12">
                                                    <label class="col-md-10"><b>Define levels of SLA/Escalations for the role assigned</b></label>
                                                    <div class="box-body">
                                                        <div class="col-md-12">
                                                            <div class="well" style="max-height: 300px; overflow: auto;">
                                                                <ul class="list-group checked-list-box">
                                                                    <li class="list-group-item">Status</li>
                                                                    <li class="list-group-item">In Progress</li>
                                                                    <li class="list-group-item">Requisition</li>
                                                                    <li class="list-group-item">Closed</li>
                                                                    <li class="list-group-item">Roles</li>
                                                                    <li class="list-group-item">R1</li>
                                                                    <li class="list-group-item">R2</li>
                                                                    <li class="list-group-item">R3</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        var app = angular.module('QuickFMS', []);
        app.directive('validSubmit', ['$parse', function ($parse) {
            return {
                require: 'form',
                link: function (scope, element, iAttrs, form) {
                    form.$submitted = false;
                    var fn = $parse(iAttrs.validSubmit);
                    element.on('submit', function (event) {
                        scope.$apply(function () {
                            form.$submitted = true;
                            if (form.$valid) {
                                fn(scope, { $event: event });
                                form.$submitted = false;
                            }

                        });
                    });
                }
            };
        }
        ])
    </script>
    <script src="../Js/SystemPreferences.js" defer></script>
</body>
</html>
