﻿app.service("CategoryRoleMappingService", function ($http, $q, $filter, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CategoryRoleMapping/GetCustomizedRole', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGridbind = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CategoryRoleMapping/GetGridRoleData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});
app.controller('CategoryRoleMappingController', function ($scope, $q, $http, CategoryRoleMappingService, UtilityService, $timeout, $filter) {
    $scope.AstTypelist = [];
    $scope.categorylist = [];
    $scope.SubCatlist = [];
    $scope.Brandlist = [];
    $scope.Modellist = [];
    $scope.Company = [];
    $scope.ActionStatus = 0;
    $scope.nameArr = [];
    $scope.compArr = [];
    $scope.columnDefs = [
        { headerName: "Asset Type", field: "ASSET_TYPE_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "Asset Category", field: "VT_TYPE", cellClass: 'grid-align', width: 150 },
        { headerName: "Sub Category", field: "AST_SUBCAT_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "Role", field: "AUM_ROLE_NAME", cellClass: 'grid-align', width: 350, },
        { headerName: "Company", field: "comp_name", cellClass: 'grid-align', width: 300, },
        { headerName: "Edit", template: '<a data-ng-click ="EditDetails(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true, suppressSorting: true, width: 60, }

    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        $scope.messageVisible = false;
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        onReady: function () {
            $scope.semdetailsOptions.api.sizeColumnsToFit()
        },

    };
    $scope.loadgrid = function () {
        CategoryRoleMappingService.GetGridbind().then(function (data) {
            $scope.gridata = data.data;
            progress(0, 'Loading...', true);
            if ($scope.gridata == null) {
                $scope.IsDisabled = false
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.GridVisiblity = true;
                $scope.GridVisiblity2 = true;
                var dum = $scope.gridata[0].AUM_BASED_ON;
                if (dum == "role") {
                    $scope.Customized.rol = true;
                    $scope.checkchange(1);
                } else {
                    $scope.Customized.compn = true;
                    $scope.checkchange(2);
                }
                $scope.IsDisabled = true
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
            progress(0, '', false);
        })
    }

    $scope.Pageload = function () {

        UtilityService.GetAssetTypes(1).then(function (response) {
            if (response.data != null) {
                $scope.AstTypelist = response.data;
                $scope.AstTypelistsource = response.data;
            }
        });

        UtilityService.GetCategories(1).then(function (response) {
            //console.log(response.data);
            if (response.data != null) {
                $scope.categorylist = response.data;
                $scope.categorylistsource = response.data;
                //angular.forEach($scope.categorylist, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });

        UtilityService.GetSubCategories(1).then(function (response) {
            if (response.data != null) {
                $scope.SubCatlist = response.data;
                $scope.SubCatlistsource = response.data;
                //angular.forEach($scope.SubCatlist, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });

        UtilityService.GetBrands(1).then(function (response) {
            if (response.data != null) {
                $scope.Brandlist = response.data;
                $scope.Brandlistsource = response.data;
                //angular.forEach($scope.Brandlist, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });

        UtilityService.GetModels(1).then(function (response) {
            if (response.data != null) {
                $scope.Modellist = response.data;
                $scope.Modellistsource = response.data;
                //angular.forEach($scope.Modellist, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
            }
        });
        UtilityService.GetRoles(0).then(function (response) {
            if (response.data != null) {
                $scope.Role = response.data;
            }
        });

    }, function (error) {
        console.log(error);
    }
    $scope.Pageload();
    $scope.loadgrid();
    $scope.AstTypeChanged = function () {
        $scope.categorylist = [];
        _.forEach($scope.categorylistsource, function (n, key) {
            _.forEach($scope.Customized.selectedtype, function (n2, key2) {
                if (n.CAT_STATUS === n2.TYPE_ID) {
                    $scope.categorylist.push(n);
                }
            });
        });
    }
    $scope.CatChanged = function () {
        $scope.SubCatlist = [];
        _.forEach($scope.SubCatlistsource, function (n, key) {
            _.forEach($scope.Customized.selectedcat, function (n2, key2) {
                if (n.CAT_CODE === n2.CAT_CODE) {
                    $scope.SubCatlist.push(n);
                }
            });
        });

    }
    $scope.SubCatChanged = function () {
        $scope.Brandlist = [];
        angular.forEach($scope.Brandlistsource, function (n, key) {
            angular.forEach($scope.Customized.selectedsubcat, function (n2, key2) {
                if (n.BRND_SUBCODE === n2.AST_SUBCAT_CODE && n.CAT_CODE === n2.CAT_CODE) {
                    $scope.Brandlist.push(n);
                }
            });
        });
        angular.forEach($scope.Brandlist, function (value, key) {
            value.ticked = true;
        })
        $scope.Customized.selectedBrands = $scope.Brandlist;
        $scope.BrandChanged();
    }
    $scope.BrandChanged = function () {
        $scope.Modellist = [];
        //console.log($scope.Customized.selectedBrands);
        angular.forEach($scope.Modellistsource, function (n, key) {
            angular.forEach($scope.Customized.selectedBrands, function (n2, key2) {
                if (n.BRND_CODE == n2.BRND_CODE && n.BRND_SUBCODE === n2.AST_SUBCAT_CODE && n.CAT_CODE === n2.CAT_CODE) {
                    $scope.Modellist.push(n);
                }
            });
        });
        angular.forEach($scope.Modellist, function (value, key) {
            value.ticked = true;
        })
    }

    $scope.catSelectAll = function () {
        $scope.Customized.selectedcat = $scope.categorylist;
        $scope.CatChanged();
    }
    $scope.subcatSelectAll = function () {
        $scope.Customized.selectedsubcat = $scope.SubCatlist;
        $scope.SubCatChanged();
    }
    $scope.BrandSelectAll = function () {
        $scope.Customized.selectedBrands = $scope.Brandlist;
        $scope.BrandChanged();
    }
    $scope.checkchange = function (a) {
        if (a == 1) {
            $scope.company1 = false;
            $scope.role1 = true;
        }
        else {
            $scope.role1 = false;
            $scope.company1 = true;
        }
    }
    $scope.COMPAll = function () {
        $scope.Customized.Company = $scope.Company;
        //console.log($scope.Customized.Company);
    }
    $scope.ROLEALL = function () {
        $scope.Customized.Role = $scope.Role;

    }
    $scope.CMPChanged = function () {
        angular.forEach($scope.Company, function (n) {
            if (n.ticked == true) {
                $scope.Customized.Company = $scope.Customized.CNP_NAME;
            }
        });
        //console.log($scope.Customized.Company);
    }
    $scope.ROLChanged = function () {
        angular.forEach($scope.Role, function (n1) {
            if (n1.ticked == true) {
                $scope.Customized.Role = $scope.Customized.ROL_DESCRIPTION;
            }
        });
    }
    $scope.SubmitData = function (sta) {
        progress(0, 'Loading...', true);
        //if (sta == 1) {
        var params = {
            AssetTypes: $scope.Customized.selectedtype,
            Categorylst: $scope.Customized.selectedcat,
            Subcatlst: $scope.Customized.selectedsubcat,
            Modellst: $scope.Customized.selectedModels,
            Brandlst: $scope.Customized.selectedBrands,
            CNP_NAME: $scope.Customized.Company,
            rolelsts: $scope.Customized.Role,
            ROLBASE: $scope.Customized.rol,
            COMPBASE: $scope.Customized.compn,
            Status: sta,
        };
        //} 
        //console.log(params);
        CategoryRoleMappingService.GetGriddata(params).then(function (data) {
            $scope.loadgrid();
            $scope.messageText = "Asset Category Role Mapping successful !";
            $scope.messageVisible = true;
        })
        progress(0, 'Loading...', false);
    },
        function (error) {
            console.log(error);
        }

    $scope.EditDetails = function (data) {
        // console.log(data);
        $scope.ActionStatus = 1;
        progress(0, 'Loading...', true);
        angular.forEach($scope.categorylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.SubCatlist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Brandlist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Modellist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Role, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Company, function (value, key) {
            value.ticked = false;
        });
        var cat = _.find($scope.categorylist, { CAT_CODE: data.VT_CODE });
        if (cat != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cat.ticked = true;
                    $scope.Customized.selectedcat.push(cat);
                });
            }, 100)
        }
        var subcat = _.find($scope.SubCatlist, { AST_SUBCAT_CODE: data.AST_SUBCAT_CODE });
        if (subcat != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    subcat.ticked = true;
                    $scope.Customized.selectedsubcat.push(subcat);
                    $scope.SubCatChanged();
                });
            }, 100)
        }
        var basedon = data.AUM_BASED_ON;
        if (basedon == "role") {
            $scope.Customized.rol = true;
            $scope.checkchange(1);
            var rolename = data.AUM_ROLE_NAME;
            $scope.nameArr = rolename.split(',');
            angular.forEach($scope.Role, function (n1, key) {
                angular.forEach($scope.nameArr, function (n, key) {
                    if (n1.ROL_DESCRIPTION == n) {
                        n1.ticked = true;
                        $scope.Customized.Role = $scope.Role;
                    }
                });
            });

        }
        else {
            $scope.Customized.compn = true;
            $scope.checkchange(2);
            var Compname = data.comp_name;
            $scope.compArr = Compname.split(',');
            angular.forEach($scope.Company, function (n1, key) {
                angular.forEach($scope.compArr, function (n, key) {
                    if (n1.CNP_NAME == n) {
                        n1.ticked = true;
                        $scope.Customized.Company = $scope.Company;
                    }
                });
            });
            // console.log($scope.Customized.Company);
        }
        progress(0, 'Loading...', false);
    }

});