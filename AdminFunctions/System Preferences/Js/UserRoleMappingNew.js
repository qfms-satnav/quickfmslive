﻿app.service("UserRoleMappingNewService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    /**************************************************************************************************************/
    this.GetUserRoleList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMappingNew/GetUserRoleDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    /**************************************************************************************************************/
    this.DeleteSeat = function (Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMappingNew/DeleteSeat', Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    /**************************************************************************************************************/
    this.InactiveEmployee = function (Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMappingNew/InactiveSpaces', Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    /**************************************************************************************************************/
    this.GetUserEmpDetails = function (Id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMappingNew/GetUserEmpDetails/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    /**************************************************************************************************************/
    this.SaveEmployeeDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMappingNew/SaveEmpDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    /**************************************************************************************************************/
    this.Getmodules = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMappingNew/Getmodules')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    /**************************************************************************************************************/
    this.accessbymodule = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMappingNew/GetAccessByModule', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    /**************************************************************************************************************/
    this.SaveRMDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMappingNew/SaveRoleModDetls', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    /**************************************************************************************************************/
    this.GetAllMapData = function (Id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMappingNew/GetAllMapData/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    /**************************************************************************************************************/
    this.SaveMappingDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMappingNew/SaveMappingDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('UserRoleMappingNewController', ['$scope', '$q', 'UserRoleMappingNewService', 'UtilityService', '$timeout', '$filter',
    '$http', '$ngConfirm', function ($scope, $q, UserRoleMappingNewService, UtilityService, $timeout, $filter, $http, $ngConfirm) {
        /**************************************************************************************************************/
        $scope.Viewstatus = 0;
        $scope.tabStatus = 1;
        $scope.EmployeeDetailsObj = {
            AUR_ID: "", AUR_KNOWN_AS: "", AUR_EMAIL: "", AUR_RES_NUMBER: "", AUR_DOB: "", AUR_DOJ: "", USR_LOGIN_PASSWORD: "",
            AUR_REPORTING_TO_NAME: "", AUR_EXTENSION: "", designationObj: {}, verticalObj: {}, costcenterObj: {}, locationObj: {},
        };
        $scope.RoleDetailsObj = { ModuleObj: {}, MapAccess: 'N', AUR_ID: "", AUR_KNOWN_AS: "" }
        $scope.MapDetailsObj = { CountryObj: {} }
        $scope.passwordText = 'password';
        $scope.BsmDet = [];
        $scope.designationddl = [];
        $scope.verticalddl = [];
        $scope.costcenterddl = [];
        $scope.usrStatus = [];
        $scope.locationddl = [];
        $scope.EmployeeType = [];
        $scope.modules = [];
        $scope.acceslevel = [];
        $scope.licationCode = '';
        $scope.VerticalCode = '';
        $scope.costcenterCode = '';
        $scope.RolValidationArr = true;
        $scope.mapLocation = [];
        $scope.mapFloorArr = [];
        $scope.mappPerantArr = [];
        $scope.mapChildArr = [];
        $scope.mapVerArr = [];
        $scope.mapCostArr = [];

        /**************************************************************************************************************/

        $scope.accessbymodule = function () {
            $scope.dataobject = {
                AurId: $scope.EmployeeDetailsObj.AUR_ID, ModuleId: $scope.RoleDetailsObj.ModuleObj[0].MOD_CODE
            };
            UserRoleMappingNewService.accessbymodule($scope.dataobject).then(function (rol) {
                progress(0, 'Loading...', false);
                var dta = rol.data;
                _.forEach(dta, function (value, key) {
                    value.isChecked = false;
                    _.forEach($scope.EmployeeDetailsObj.URL_ROL_ID.split(','), function (value11, key11) {
                        if (value.ROL_ID == value11.trim()) {
                            value.isChecked = true;
                        }
                    });
                });
                $scope.acceslevel = angular.copy(dta);
            });
        }

        $scope.UploadFile = function () {
            if ($('#FileUpl', $('#FileUsrUpl')).val()) {
                var filetype = $('#FileUpl', $('#FileUsrUpl')).val().substring($('#FileUpl', $('#FileUsrUpl')).val().lastIndexOf(".") + 1);
                if (filetype == "xlsx" || filetype == "xls") {
                    var formData = new FormData();
                    var UplFile = $('#FileUpl')[0];
                    formData.append("UplFile", UplFile.files[0]);
                    formData.append("CurrObj", "");
                    $.ajax({
                        url: UtilityService.path + '/api/UserRoleMapping/UploadTemplate',
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            var respdata = JSON.parse(data);
                            if (respdata.data != null)
                                $scope.LoadUploadedData(respdata.data);
                            else
                                showNotification('error', 8, 'bottom-right', respdata.Message);
                        }
                    });
                }
                else
                    showNotification('error', 8, 'bottom-right', 'Please Upload only Excel (.xlsx) File(s)');
            }
            else
                showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');
        };
        /**************************************************************************************************************/
        var columnDefs = [
            { headerName: "User ID", field: "AUR_ID", width: 120, cellClass: "grid-align" },
            { headerName: "User Name", field: "AUR_KNOWN_AS", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data)">{{data.AUR_KNOWN_AS}}</a>' },
            { headerName: "Location", field: "AUR_LOCATION", cellClass: "grid-align" },
            { headerName: "Role Name", field: "ROL_DESCRIPTION", cellClass: "grid-align" },
            { headerName: "Map Level Access", field: "MAP_ACCESS", width: 120, cellClass: "grid-align" },
            { headerName: "Email", field: "AUR_EMAIL", cellClass: "grid-align" },
            { headerName: "Designation", field: "AUR_DESGN_ID", cellClass: "grid-align" },
            { headerName: "Employee type", field: "AUR_GRADE", width: 120, cellClass: "grid-align" },
            { headerName: "Vertical", field: "AUR_VERTICAL_NAME", cellClass: "grid-align" },
            { headerName: "Costcenter", field: "AUR_COSTCENTER_NAME", cellClass: "grid-align" },
            { headerName: '<i class="fa fa-times" aria-hidden="true"></i>', width: 50, suppressMenu: true, cellclass: "grid-align", template: '<a ng-click="Delete(data)"><i class="fa fa-times" aria-hidden="true"></i></a>' }
        ];
        /**************************************************************************************************************/

        var columnDefs1 = [
            { headerName: "Country Name", field: "HST_CNY_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "City Name", field: "HST_CTY_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "Location", field: "HST_LCM_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "Tower", field: "HST_TWR_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "Floor", field: "HST_FLR_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "Vertical Name", field: "HST_VERT_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "Cost Center Name", field: "HST_CST_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "User Role", field: "HST_USR_ROLE_NAME", cellClass: "grid-align", width: 100 },
            { headerName: "User Id", field: "HST_USR_ID", cellClass: "grid-align", width: 100 },
            { headerName: "Remarks", field: "HST_REM", cellClass: "grid-align", width: 450 }
        ];

        $scope.gridOptions1 = {
            columnDefs: columnDefs1,
            rowData: null,
            enableSorting: true,
            enableFilter: true,
            cellClass: 'grid-align',
            angularCompileRows: true,
            enableScrollbars: false,
            rowSelection: 'single',
            onReady: function () {
                $scope.gridOptions1.api.sizeColumnsToFit()
            }
        };

        $scope.gridOptions = {
            columnDefs: columnDefs,
            debug: true,
            rowData: null,
            enableSorting: true,
            enableFilter: true,
            cellClass: 'grid-align',
            angularCompileRows: true,
            enableScrollbars: false,
            enableColResize:true,
            rowSelection: 'single',
            rowModelType: 'virtual',
            paginationOverflowSize: 2,
            maxConcurrentDatasourceRequests: 2,
            paginationInitialRowCount: 1,
            maxPagesInCache: 2,
            //onReady: function () {
            //    $scope.gridOptions.api.sizeColumnsToFit()
            //}
        };
        /**************************************************************************************************************/
        $scope.onRowSelectedFunc = function (data) {
            $scope.Viewstatus = 1;
            $scope.EmployeeDetails = true;
            $scope.RoleAssignment = true;
            $scope.Mapping = true;
            $scope.tabStatus = 1;
            $scope.usrStatus = [{ value: 0, Name: "In-Active" }, { value: 1, Name: "Active" }];
            UtilityService.getBussHeirarchy().then(function (response2) {
                if (response2.data != null) {
                    progress(0, 'Loading...', true);
                    $scope.BsmDet = response2.data;
                    getDatainApi(data);
                }
            });
        };
        /**************************************************************************************************************/
        $scope.LoadUploadedData = function (data) {
            $scope.$apply(function () {
                $scope.Viewstatus = 2;
                $scope.gridata1 = data.Table;
                $scope.gridOptions1.api.setRowData($scope.gridata1);

            });
        }

        function getDatainApi(data) {
            var Id = data.AUR_ID.split("/").join("__");
            UtilityService.getVerticals(1).then(function (Vedata) {
                if (Vedata.data != null) {
                    $scope.verticalddl = Vedata.data;
                    UtilityService.getCostCenters(1).then(function (Csdata) {
                        if (Csdata.data != null) {
                            $scope.costcenterddl = Csdata.data;
                            UtilityService.getLocations(1).then(function (Lcmdata) {
                                if (Lcmdata.data != null) {
                                    $scope.locationddl = Lcmdata.data;
                                    UtilityService.getEmployeeType(1).then(function (empdata) {
                                        if (empdata.data != null) {
                                            $scope.EmployeeType = empdata.data;
                                            UtilityService.getDesignation().then(function (response1) {
                                                if (response1.data != null) {
                                                    $scope.designationddl = response1.data;
                                                    UserRoleMappingNewService.GetUserEmpDetails(Id).then(function (response) {
                                                        progress(0, 'Loading...', false);
                                                        if (response.data != null) {
                                                            $scope.selectedEmpDB = {
                                                                selected: {
                                                                    data: {
                                                                        AUR_ID: response.data[0].AUR_REPORTING_TO,
                                                                        NAME: response.data[0].AUR_REPORTING_TO_NAME,
                                                                        ticked: false,
                                                                        VERTICAL: response.data[0].AUR_VERT_CODE,
                                                                        COSTCENTER: response.data[0].AUR_PRJ_CODE,
                                                                        AUR_EMAIL: response.data[0].AUR_EMAIL,
                                                                        AUR_RES_NUMBER: response.data[0].AUR_RES_NUMBER,
                                                                        AUR_PASSWORD: response.data[0].USR_LOGIN_PASSWORD,
                                                                        selectedEmp: response.data[0].AUR_REPORTING_TO,
                                                                        AUR_EXTENSION: response.data[0].AUR_EXTENSION,
                                                                        AUR_DESGN_ID: response.data[0].AUR_DESGN_ID,
                                                                        LOCATION: response.data[0].AUR_LOCATION
                                                                    },
                                                                    title: ""
                                                                }
                                                            }
                                                            $scope.EmployeeDetailsObj = response.data[0];
                                                            $scope.reselectedEmp($scope.selectedEmpDB);
                                                            $scope.EmployeeDetailsObj.AUR_ID = response.data[0].AUR_ID;
                                                            $scope.EmployeeDetailsObj.AUR_KNOWN_AS = response.data[0].AUR_KNOWN_AS;
                                                            $scope.EmployeeDetailsObj.AUR_EMAIL = response.data[0].AUR_EMAIL;
                                                            $scope.EmployeeDetailsObj.AUR_RES_NUMBER = response.data[0].AUR_RES_NUMBER;
                                                            $scope.EmployeeDetailsObj.AUR_DOB = $filter('date')(response.data[0].AUR_DOB, $scope.reqdate);
                                                            $scope.EmployeeDetailsObj.AUR_DOJ = $filter('date')(response.data[0].AUR_DOJ, $scope.reqdate);
                                                            $scope.EmployeeDetailsObj.USR_LOGIN_PASSWORD = response.data[0].USR_LOGIN_PASSWORD;
                                                            $scope.EmployeeDetailsObj.AUR_REPORTING_TO_NAME = response.data[0].AUR_REPORTING_TO_NAME;
                                                            $scope.EmployeeDetailsObj.AUR_EXTENSION = response.data[0].AUR_EXTENSION;
                                                            $scope.EmployeeDetailsObj.DSG_CODE = response.data[0].AUR_DESGN_ID;
                                                            $scope.EmployeeDetailsObj.AUR_EMP_REMARKS = response.data[0].AUR_EMP_REMARKS;
                                                            $scope.EmployeeDetailsObj.AUR_STA_ID = response.data[0].AUR_STA_ID.toString();
                                                            $scope.EmployeeDetailsObj.AUR_COUNTRY = response.data[0].AUR_COUNTRY;
                                                            $scope.EmployeeDetailsObj.AUR_PASSWORD = response.data[0].USR_LOGIN_PASSWORD;
                                                            $scope.licationCode = angular.copy(response.data[0].AUR_LOCATION);
                                                            $scope.VerticalCode = angular.copy(response.data[0].AUR_VERT_CODE);
                                                            $scope.costcenterCode = angular.copy(response.data[0].AUR_PRJ_CODE);
                                                            $scope.RoleDetailsObj.AUR_ID = response.data[0].AUR_ID;
                                                            $scope.MapDetailsObj.AUR_ID = response.data[0].AUR_ID;
                                                            $scope.RoleDetailsObj.AUR_KNOWN_AS = response.data[0].AUR_KNOWN_AS;
                                                            _.forEach($scope.designationddl, function (value, key) {
                                                                if (value.DSG_CODE == response.data[0].AUR_DESGN_ID) {
                                                                    value.ticked = true;
                                                                }
                                                            });
                                                            _.forEach($scope.verticalddl, function (value1, key1) {
                                                                if (value1.VER_CODE == response.data[0].AUR_VERT_CODE) {
                                                                    value1.ticked = true;
                                                                }
                                                            });
                                                            _.forEach($scope.costcenterddl, function (value2, key2) {
                                                                if (value2.Cost_Center_Code == response.data[0].AUR_PRJ_CODE) {
                                                                    value2.ticked = true;
                                                                }
                                                            });
                                                            _.forEach($scope.usrStatus, function (value3, key3) {
                                                                if (value3.value == response.data[0].AUR_STA_ID) {
                                                                    value3.ticked = true;
                                                                }
                                                            });
                                                            _.forEach($scope.locationddl, function (value4, key4) {
                                                                if (value4.LCM_CODE == response.data[0].AUR_LOCATION) {
                                                                    value4.ticked = true;
                                                                }
                                                            });
                                                            _.forEach($scope.EmployeeType, function (value5, key5) {
                                                                if (value5.AUR_GRADE == response.data[0].AUR_GRADE) {
                                                                    value5.ticked = true;
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            showNotification('error', 8, 'bottom-right', data.Message);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        };
        /**************************************************************************************************************/
        $scope.passwordFn = function (typ) {
            if (typ == 'S')
                $scope.passwordText = 'text';
            else
                $scope.passwordText = 'password';
        };
        /**************************************************************************************************************/
        $scope.back = function () {
            $scope.Viewstatus = 0;
        };
        /**************************************************************************************************************/
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };
        /**************************************************************************************************************/
        $scope.reselectedEmp = function (obj) {
            if (obj) {
                $scope.selectedEmployee = obj.selected.data;
                $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
                $scope.EmployeeDetailsObj.AUR_REPORTING_TO = $scope.selectedEmployee.AUR_ID;
            } else { }
        };
        /**************************************************************************************************************/
        $scope.selectedEmp = function (selected) {
            if (selected) {
                $scope.selectedEmployee = selected.originalObject;
                $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
                $scope.EmployeeDetailsObj.AUR_REPORTING_TO = $scope.selectedEmployee.AUR_ID;
                $scope.EmployeeDetailsObj.VERTICAL = $scope.selectedEmployee.Vertical;
                $scope.EmployeeDetailsObj.COSTCENTER = $scope.selectedEmployee.Costcenter;
            }
        };
        /**************************************************************************************************************/
        $scope.tabStatusFn = function (typ) {
            if (typ == 'E') {
                $scope.tabStatus = 1;
                _.forEach($scope.locationddl, function (value, key) {
                    value.ticked = false;
                    if (value.LCM_CODE == $scope.licationCode) {
                        value.ticked = true;
                    }
                });
                _.forEach($scope.verticalddl, function (value1, key1) {
                    value1.ticked = false;
                    if (value1.VER_CODE == $scope.VerticalCode) {
                        value1.ticked = true;
                    }
                });
                _.forEach($scope.costcenterddl, function (value2, key2) {
                    value2.ticked = false;
                    if (value2.Cost_Center_Code == $scope.costcenterCode) {
                        value2.ticked = true;
                    }
                });
            }
            else if (typ == 'R') {
                progress(0, 'Loading...', true);
                $scope.tabStatus = 2;
                $scope.GblRolechk = false;
                $scope.RoleDetailsObj.MapAccess = $scope.EmployeeDetailsObj.URL_MAP_ACCESS;
                UserRoleMappingNewService.Getmodules().then(function (data) {
                    $scope.modules = data.data;
                    $scope.modules.push({ MOD_CODE: 'ALL', MOD_NAME: 'ALL' });
                    _.forEach($scope.modules, function (value, key) {
                        if (value.MOD_CODE == 'ALL') {
                            value.ticked = true;
                        }
                    });
                    $scope.dataobject = {
                        AurId: $scope.EmployeeDetailsObj.AUR_ID, ModuleId: 'ALL'/* $scope.modules[0].MOD_CODE*/
                    };
                    UserRoleMappingNewService.accessbymodule($scope.dataobject).then(function (rol) {
                        progress(0, 'Loading...', false);
                        var dta = rol.data;
                        _.forEach(dta, function (value, key) {
                            value.isChecked = false;
                            _.forEach($scope.EmployeeDetailsObj.URL_ROL_ID.split(','), function (value11, key11) {
                                if (value.ROL_ID == value11.trim()) {
                                    value.isChecked = true;
                                    if (value.ROL_ID == 1) {
                                        $scope.GblRolechk = true;
                                    }
                                }
                            });
                        });
                        $scope.acceslevel = angular.copy(dta);
                    });
                }, function (error) {
                    progress(0, '', false);
                    console.log(error);
                });
            } else if (typ == 'M') {
                progress(0, 'Loading...', true);
                var Ids = $scope.EmployeeDetailsObj.AUR_ID.split("/").join("__");
                UserRoleMappingNewService.GetAllMapData(Ids).then(function (response) {
                    if (response.Message == "OK") {
                        $scope.mapLocation = response.data;
                        $scope.mapFloorArr = response.data1;
                        $scope.mappPerantArr = response.data2;
                        $scope.mapChildArr = response.data3;
                        $scope.mapVerArr = response.data4;
                        $scope.mapCostArr = response.data5;
                        $scope.tabStatus = 3;
                        _.forEach($scope.locationddl, function (value1, key1) {
                            value1.ticked = false;
                        });
                        _.forEach($scope.verticalddl, function (value1, key1) {
                            value1.ticked = false;
                        });
                        _.forEach($scope.costcenterddl, function (value1, key1) {
                            value1.ticked = false;
                        });
                        _.forEach($scope.mapLocation, function (value, key) {
                            _.forEach($scope.locationddl, function (value1, key1) {
                                if (value.LCM_CODE == value1.LCM_CODE) {
                                    value1.ticked = true;
                                }
                            });
                        });
                        _.forEach($scope.mapVerArr, function (value, key) {
                            _.forEach($scope.verticalddl, function (value1, key1) {
                                if (value.UVM_VERT_CODE == value1.VER_CODE) {
                                    value1.ticked = true;
                                }
                            });
                        });
                        _.forEach($scope.mapCostArr, function (value, key) {
                            _.forEach($scope.costcenterddl, function (value1, key1) {
                                if (value.UCM_COST_CENTER_CODE == value1.Cost_Center_Code) {
                                    value1.ticked = true;
                                }
                            });
                        });
                        UtilityService.getCountires(1).then(function (Condata) {
                            if (Condata.data != null) {
                                $scope.Countrylst = Condata.data;
                                _.forEach($scope.mapLocation, function (value, key) {
                                    _.forEach($scope.Countrylst, function (value1, key1) {
                                        if (value.LCM_CNY_ID == value1.CNY_CODE) {
                                            value1.ticked = true;
                                        }
                                    });
                                });
                                UtilityService.getCities(1).then(function (Cnydata) {
                                    if (Cnydata.data != null) {
                                        $scope.Citylst = Cnydata.data;
                                        _.forEach($scope.mapLocation, function (value, key) {
                                            _.forEach($scope.Citylst, function (value1, key1) {
                                                if (value.LCM_CTY_ID == value1.CTY_CODE) {
                                                    value1.ticked = true;
                                                }
                                            });
                                        });
                                        UtilityService.getLocations(1).then(function (Cnydata) {
                                            if (Cnydata.data != null)
                                            {
                                                $scope.Locationlst = Cnydata.data;
                                                _.forEach($scope.mapLocation, function (value, key) {
                                                    _.forEach($scope.Citylst, function (value1, key1) {
                                                        if (value.LCM_CODE == value1.LCM_CODE) {
                                                            value1.ticked = true;
                                                        }
                                                    });
                                                });
                                                UtilityService.getTowers(1).then(function (towrdata) {
                                                    if (towrdata.data != null) {
                                                        $scope.Towerlist = towrdata.data;
                                                     _.forEach($scope.mapFloorArr, function (value, key) {
                                                    _.forEach($scope.Towerlist, function (value1, key1) {
                                                        if (value.TWR_CODE == value1.TWR_CODE) {
                                                            value1.ticked = true;
                                                        }
                                                    });
                                                });
                                                UtilityService.getFloors(1).then(function (Flrdata) {
                                                    if (Condata.data != null) {
                                                        $scope.Floorlist = Flrdata.data;
                                                        _.forEach($scope.mapFloorArr, function (value, key) {
                                                            _.forEach($scope.Floorlist, function (value1, key1) {
                                                                if (value.FLR_CODE == value1.FLR_CODE) {
                                                                    value1.ticked = true;
                                                                }
                                                            });
                                                        });
                                                        UtilityService.getParentEntity(1).then(function (parent) {
                                                            if (Condata.data != null) {
                                                                $scope.Parentlist = parent.data;
                                                                _.forEach($scope.mappPerantArr, function (value, key) {
                                                                    _.forEach($scope.Parentlist, function (value1, key1) {
                                                                        if (value.UPE_PE_CODE == value1.PE_CODE) {
                                                                            value1.ticked = true;
                                                                        }
                                                                    });
                                                                });
                                                                UtilityService.getChildEntity(1).then(function (chddata) {
                                                                    if (Condata.data != null) {
                                                                        $scope.Childlist = chddata.data;
                                                                        progress(0, 'Loading...', false);
                                                                        _.forEach($scope.mapChildArr, function (value, key) {
                                                                            _.forEach($scope.Childlist, function (value1, key1) {
                                                                                if (value.UCHE_CHE_CODE == value1.CHE_CODE) {
                                                                                    value1.ticked = true;
                                                                                }
                                                                            });
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });

                    }
                }, function (error) {
                    progress(0, '', false);
                    console.log(error);
                });
            }
        };
        /**************************************************************************************************************/
        $scope.CountryChanged = function () {
            UtilityService.getCitiesbyCny($scope.UserRole.selectedCountries, 1).then(function (response) {
                if (response.data != null) {
                    $scope.Citylst = response.data
                } else { $scope.Citylst = [] }
            }, function (error) {
                console.log(error);
            });
        };
        /**************************************************************************************************************/
        $scope.GenReport = function (Allocdata, Type) {
            progress(0, 'Loading...', true);
            Allocdata.Type = Type;
            if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
                if (Allocdata.Type == "pdf") {
                    progress(0, 'Loading...', true);
                    $scope.GenerateFilterPdf();
                    progress(0, '', false);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.GenerateFilterExcel();
                    progress(0, '', false);
                }

            }
            else {
                $scope.DocTypeVisible = 0
                progress(0, 'Loading...', true);
                $http({
                    url: UtilityService.path + '/api/UserRoleMappingNew/GetEmployeeData',
                    method: 'POST',
                    data: Allocdata,
                    responseType: 'arraybuffer'
                }).then(function (data, status, headers, config) {
                    var file = new Blob([data.data], {
                        type: 'application/' + Type
                    });
                    var fileURL = URL.createObjectURL(file);
                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'EmployeeData.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    progress(0, '', false);
                }), function (error, data, status, headers, config) {
                    progress(0, '', false);
                };
            }
        };
        /**************************************************************************************************************/
        $scope.GlobalValidation = false;
        /**************************************************************************************************************/
        $scope.Rolechk = function () {
            var ChkLength = $filter('filter')($scope.gridata, { ROL_DESCRIPTION: "Global Admin" }).length;
            if (ChkLength >= 5) {
                $scope.GlobalValidation = false;
            }
            else {
                $scope.GlobalValidation = true;
            }
        };


        /**************************************************************************************************************/
        $scope.SaveEmpDetails = function () {
            var fromdate = moment($scope.EmployeeDetailsObj.AUR_DOB);
            var todate = moment($scope.EmployeeDetailsObj.AUR_DOJ);
            if (fromdate > todate) {
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            }
           
            if ($scope.EmployeeDetailsObj.locationObj[0] == undefined) {
                showNotification('error', 8, 'bottom-right', 'Please Select location');
            }

            else {
                if ($scope.EmployeeDetailsObj.AUR_ID.length > 0 & $scope.EmployeeDetailsObj.locationObj != undefined) {
                    progress(0, 'Loading...', true);
                    $scope.EmployeeDetailsObj.VERTICAL = $scope.EmployeeDetailsObj.verticalObj[0].VER_CODE;
                    $scope.EmployeeDetailsObj.COSTCENTER = $scope.EmployeeDetailsObj.costcenterObj[0].Cost_Center_Code;
                    $scope.EmployeeDetailsObj.NEW_AUR_ID = $scope.EmployeeDetailsObj.AUR_ID;
                    $scope.EmployeeDetailsObj.DSG_CODE = $scope.EmployeeDetailsObj.designationObj == undefined ? '' : $scope.EmployeeDetailsObj.designationObj[0].DSG_CODE;
                    $scope.EmployeeDetailsObj.LOCATION = $scope.EmployeeDetailsObj.locationObj[0].LCM_CODE;
                    $scope.EmployeeDetailsObj.AUR_GRADE = $scope.EmployeeDetailsObj.EmployeeTypeObj == undefined ? '' : $scope.EmployeeDetailsObj.EmployeeTypeObj[0].AUR_GRADE;
                    UserRoleMappingNewService.SaveEmployeeDetails($scope.EmployeeDetailsObj).then(function (data) {
                        progress(0, 'Loading...', false);
                        if (data.data == undefined) {
                            showNotification('error', 8, 'bottom-right', data.Message);
                        }
                        else {
                            showNotification('success', 8, 'bottom-right', 'Employee Details Saved Successfully');
                            var data = { AUR_ID: $scope.EmployeeDetailsObj.AUR_ID };
                            //getDatainApi(data);
                            $scope.tabStatusFn("R");
                            $scope.gridOptions.api.refreshView();
                        }
                    });
                }
                else {
                    showNotification('success', 8, 'bottom-right', 'Please select to auto fill the data.');
                }
            }
        };
        /**************************************************************************************************************/
        $scope.SaveRMDetls = function () {
            progress(0, 'Loading...', true);
            $scope.Rolechk();
            $scope.selectedRoles = [];
            $scope.selectedRoles = $filter('filter')($scope.acceslevel, { isChecked: true });
            var Rolechk = false;
            var Rolechks = [];
            Rolechks = $scope.selectedRoles.find(val => val.ROL_ID === 1);
            Rolechk = Rolechks == undefined ? false : true;
            if ($scope.selectedRoles.length > 0) {
                if ((Rolechk == true & $scope.GlobalValidation == true) || (Rolechk == false) || ($scope.GblRolechk == true)) {
                    $scope.dataobject = { UserRoleMapping: { AUR_ID: $scope.RoleDetailsObj.AUR_ID }, UserRoleVM: $scope.selectedRoles, MapLevelAccess: $scope.RoleDetailsObj.MapAccess };
                    UserRoleMappingNewService.SaveRMDetails($scope.dataobject).then(function (response) {
                        progress(0, 'Loading...', false);
                        if (response.data != undefined) {
                            showNotification('success', 8, 'bottom-right', 'Role Assigned Successfully');
                            $scope.RoleDetailsObj.MapAccess = $scope.EmployeeDetailsObj.URL_MAP_ACCESS;
                            $scope.tabStatusFn("M");
                        } else {
                            progress(0, 'Loading...', false);
                            $scope.RolValidation = false;
                            showNotification('error', 8, 'bottom-right', 'Please Select Atleast One Role to Assign');
                        }
                    }, function (error) {
                        progress(0, '', false);
                    });
                }
                else {
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', 'Global Admin Role Exceeds more than 5 employees');
                    $scope.gridOptions.api.refreshView();
                }
            } else {
                progress(0, 'Loading...', false);
                $scope.RolValidation = false;
                showNotification('error', 8, 'bottom-right', 'Please Select Atleast One Role to Assign');
            }
        };
        /**************************************************************************************************************/
        $scope.SaveMappingDetls = function () {
            progress(0, 'Loading...', true);
            $scope.dataobject = { UserRoleMapping: { AUR_ID: $scope.MapDetailsObj.AUR_ID }, USERMAPPINGDETAILS: $scope.MapDetailsObj, MapLevelAccess: 'Y' };
            UserRoleMappingNewService.SaveMappingDetails($scope.dataobject).then(function (response) {
                if (response.data != null) {
                    showNotification('success', 8, 'bottom-right', 'Mapped Successfully');
                    //$scope.tabStatusFn("M");
                    $scope.Viewstatus = 0;
                }
            }, function (error) {
                progress(0, '', false);
            });
            progress(0, 'Loading...', false);
        };
        /**************************************************************************************************************/
        $("#filtertxt").change(function () {
            onFilterChanged($(this).val());
        }).keydown(function () {
            onFilterChanged($(this).val());
        }).keyup(function () {
            onFilterChanged($(this).val());
        }).bind('paste', function () {
            onFilterChanged($(this).val());
        });
        /**************************************************************************************************************/
        function onFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
        };
        /**************************************************************************************************************/
        $scope.LoadData = function () {
            progress(0, 'Loading...', true);
            $scope.dataobject = { Pagesize: $scope.pageSize, PageNumber: $scope.pageNumber };
            UserRoleMappingNewService.GetUserRoleList().then(function (data) {
                if (data.data != null) {
                    $scope.gridata = data.data;
                    $scope.name = data.data[0].EMP_COUNT;
                    $scope.gridOptions.api.setRowData($filter('filter')($scope.gridata, { EMP_COUNT: 0 }));
                    progress(0, '', false);
                }
                else {
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, '', false);
                    showNotification('error', 8, data.Message);
                }
            }, function (error) {
                progress(0, '', false);
            });
        };
        /**************************************************************************************************************/
        $scope.VerChange = function (data) {
            $scope.Data = { VER_CODE: data };
            _.forEach($scope.costcenterddl, function (value2, key2) {
                value2.ticked = false;
            });
            $scope.costcenterddl = [];
            UtilityService.getCostcenterByVerticalcode($scope.Data, 1).then(function (response) {
                setTimeout(function () { $('#COSTCENTER').selectpicker('refresh'); }, 500);
                $scope.costcenterddl = response.data;
            }, function (error) {
                console.log(error);
            });
        };
        /**************************************************************************************************************/
        $scope.CountryChanged = function () {
            $scope.Citylst = [];
            UtilityService.getCitiesbyCny($scope.MapDetailsObj.CountryObj, 1).then(function (response) {
                if (response.data != null) {
                    $scope.Citylst = response.data
                } else { $scope.Citylst = [] }
            }, function (error) {
                console.log(error);
            });
        };

        $scope.CnyChangeAll = function () {
            $scope.MapDetailsObj.CountryObj = $scope.Countrylst;
            $scope.CountryChanged();
        }
        /**************************************************************************************************************/
        $scope.CityChanged = function () {
            $scope.MapDetailsObj.CountryObj = [];
            $scope.locationddl = [];
            UtilityService.getLocationsByCity($scope.MapDetailsObj.CitylstObj, 1).then(function (data) {
                if (data.data == null) { $scope.Locationlst = [] } else {
                    $scope.locationddl = data.data;
                }
            }, function (error) {
                console.log(error);
            });
            angular.forEach($scope.Countrylst, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Citylst, function (value, key) {
                var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
                if (cny != undefined && value.ticked == true && cny.ticked == false) {
                    cny.ticked = true;
                    $scope.MapDetailsObj.CountryObj.push(cny);
                }
            });
        };
        /**************************************************************************************************************/
        $scope.CtyChangeAll = function () {
            $scope.MapDetailsObj.CitylstObj = $scope.Citylst;
            $scope.CityChanged();
        }

        $scope.LocChange = function () {
            $scope.MapDetailsObj.CountryObj = [];
            $scope.MapDetailsObj.CitylstObj = [];
            $scope.Towerlist = [];
            UtilityService.getTowerByLocation($scope.MapDetailsObj.selectedLocations, 1).then(function (data) {
                if (data.data != null) { $scope.Towerlist = data.data } else { $scope.Towerlist = [] }
            }, function (error) {
                console.log(error);
            });
            angular.forEach($scope.Countrylst, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Citylst, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.locationddl, function (value, key) {
                var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
                if (cny != undefined && value.ticked == true && cny.ticked == false) {
                    cny.ticked = true;
                    $scope.MapDetailsObj.CountryObj.push(cny);
                }
            });

            angular.forEach($scope.locationddl, function (value, key) {
                var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
                if (cty != undefined && value.ticked == true && cty.ticked == false) {
                    cty.ticked = true;
                    $scope.MapDetailsObj.CitylstObj.push(cty);
                }
            });
        };

        $scope.LCMChangeAll = function () {
            $scope.MapDetailsObj.selectedLocations = $scope.Locationlst;
            $scope.LocChange();
        }

        /**************************************************************************************************************/
        $scope.TwrChange = function () {
            $scope.MapDetailsObj.CountryObj = [];
            $scope.MapDetailsObj.CitylstObj = [];
            $scope.MapDetailsObj.selectedLocations = [];
            $scope.Floorlist = [];
            UtilityService.getFloorByTower($scope.MapDetailsObj.selectedTowers, 1).then(function (data) {
                if (data.data != null) { $scope.Floorlist = data.data } else { $scope.Floorlist = [] }
            }, function (error) {
                console.log(error);
            });
            angular.forEach($scope.Countrylst, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Citylst, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.locationddl, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Towerlist, function (value, key) {
                var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
                if (cny != undefined && value.ticked == true && cny.ticked == false) {
                    cny.ticked = true;
                    $scope.MapDetailsObj.CountryObj.push(cny);
                }
            });

            angular.forEach($scope.Towerlist, function (value, key) {
                var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
                if (cty != undefined && value.ticked == true && cty.ticked == false) {
                    cty.ticked = true;
                    $scope.MapDetailsObj.CitylstObj.push(cty);
                }
            });

            angular.forEach($scope.Towerlist, function (value, key) {
                var lcm = _.find($scope.locationddl, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
                if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                    lcm.ticked = true;
                    $scope.MapDetailsObj.selectedLocations.push(lcm);
                }
            });
        };

        $scope.TwrChangeAll = function () {
            $scope.MapDetailsObj.selectedTowers = $scope.Towerlist;
            $scope.TwrChange();
        }


        $scope.FloorChange = function () {
            $scope.MapDetailsObj.CountryObj = [];
            $scope.MapDetailsObj.CitylstObj = [];
            $scope.MapDetailsObj.selectedLocations = [];
            $scope.MapDetailsObj.selectedTowers = [];

            angular.forEach($scope.Countrylst, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Citylst, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.locationddl, function (value, key) {
                value.ticked = false;
            });

            angular.forEach($scope.Towerlist, function (value, key) {
                value.ticked = false;
            });

            angular.forEach($scope.Floorlist, function (value, key) {
                var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
                if (cny != undefined && value.ticked == true && cny.ticked == false) {
                    cny.ticked = true;
                    $scope.MapDetailsObj.CountryObj.push(cny);
                }
            });

            angular.forEach($scope.Floorlist, function (value, key) {
                var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
                if (cty != undefined && value.ticked == true && cty.ticked == false) {
                    cty.ticked = true;
                    $scope.MapDetailsObj.CitylstObj.push(cty);
                }
            });

            angular.forEach($scope.Floorlist, function (value, key) {
                var lcm = _.find($scope.locationddl, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
                if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                    lcm.ticked = true;
                    $scope.MapDetailsObj.selectedLocations.push(lcm);
                }
            });

            angular.forEach($scope.Floorlist, function (value, key) {
                var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
                if (twr != undefined && value.ticked == true && twr.ticked == false) {
                    twr.ticked = true;
                    $scope.MapDetailsObj.selectedTowers.push(twr);
                }
            });
        }

        $scope.FloorChangeAll = function () {
            $scope.MapDetailsObj.selectedFloors = $scope.Floorlist;
            $scope.FloorChange();
        }
        /**************************************************************************************************************/
        $scope.ChildEntityChange = function () {
            $scope.verticalddl = [];
            UtilityService.getverticalbychild($scope.MapDetailsObj.selectedChildEntity, 1).then(function (response) {
                if (response.data != null) {
                    $scope.verticalddl = response.data
                }
                else {
                    $scope.verticalddl = []
                }
            }, function (error) {
                console.log(error);
            });
        };
        /**************************************************************************************************************/
        $scope.VerticalChange = function () {
            $scope.costcenterddl = [];
            UtilityService.getCostcenterByVertical($scope.MapDetailsObj.selectedVerticals, 1).then(function (data) {
                if (data.data != null) { $scope.costcenterddl = data.data } else { $scope.costcenterddl = [] }
            }, function (error) {
                console.log(error);
            });
        };
        /**************************************************************************************************************/
        $scope.refreshEmpDetails = function () {
            $scope.LoadData();
        }
        /**************************************************************************************************************/
        setTimeout(function () {
            $scope.LoadData();
        }, 100);

        /**************************************************************************************************************/
        $scope.Delete = function (data) {
            var encoding = encodeURIComponent(data.AUR_ID);
            //var Id = encoding.replace(/%2F/i, "_");
            var Id = encoding.split(/%2F/i).join("__");
            var obj = { AurId: Id };
            progress(0, 'Loading...', true);
            $ngConfirm({
                icon: 'fa fa-warning',
                columnClass: 'col-md-6 col-md-offset-3',
                title: 'Confirm!',
                content: 'Are you sure want to Inactive / Delete  - ' + '(' + data.AUR_ID + ') ' + data.AUR_KNOWN_AS,
                closeIcon: true,
                closeIconClass: 'fa fa-close',
                type: 'red',
                autoClose: 'cancel|10000',
                buttons: {
                    Confirm: {
                        text: 'Delete',
                        btnClass: 'btn-red',
                        action: function (button) {
                            debugger;
                            UserRoleMappingNewService.DeleteSeat(obj).then(function (response) {
                                if (response.data != null) {
                                    $ngConfirm({
                                        icon: 'fa fa-success',
                                        title: 'Status',
                                        content: 'Employee Deleted Successfully',
                                        closeIcon: true,
                                        closeIconClass: 'fa fa-close'
                                    });
                                }
                                setTimeout(function () {
                                    $scope.LoadData();
                                }, 800);
                            });
                        }
                    },
                    Inactive: {
                        text: 'Inactive',
                        btnClass: 'btn-blue',
                        action: function (button) {
                            debugger;
                            UserRoleMappingNewService.InactiveEmployee(obj).then(function (response) {
                                if (response.data != null) {
                                    $ngConfirm({
                                        icon: 'fa fa-success',
                                        title: 'Status',
                                        content: 'Selected Employee Is Inactive',
                                        closeIcon: true,
                                        closeIconClass: 'fa fa-close'
                                    });
                                }
                                setTimeout(function () {
                                    $scope.LoadData();
                                }, 800);
                            });
                        }
                    },
                    cancel: function () {
                        $ngConfirm({
                            title: 'Status!',
                            content: 'Cancelled',
                        });
                    }
                }
            });
            progress(0, 'Loading...', false);
        }
        /**************************************************************************************************************/

    }]);




