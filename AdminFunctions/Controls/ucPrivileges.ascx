<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucPrivileges.ascx.vb" Inherits="Modules_ucPrivileges" %>
<div class="row">
    <div class="col-md-12">       
            <asp:GridView ID="gvPrivileges" runat="server" AutoGenerateColumns="false" EmptyDataText="No Item(s) found." HeaderStyle-HorizontalAlign="left"
                CellPadding="3" CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:BoundField DataField="ROL_MODULE" HeaderText="Module" ItemStyle-Width="10%" />
                    <asp:HyperLinkField DataNavigateUrlFields="ROL_ID" DataNavigateUrlFormatString="~/AdminFunctions/AddPrivileges.aspx?_id={0}" DataTextField="ROL_ACRONYM" HeaderText="ROLE ACRONYM" ItemStyle-Width="20%" />
                    <asp:BoundField DataField="ROL_DESCRIPTION" HeaderText="ROLE DESCRIPTION" ItemStyle-Width="30%" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>       
    </div>
</div>

