﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LoginHistory.aspx.vb" Inherits="LoginHistory" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'dd/mm/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Login History Report</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                    <div class="row">
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12">Select Employee<span style="color: red;">*</span></label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="none"
                                    ErrorMessage="Please Select Employee" ValidationGroup="Val1" ControlToValidate="ddlEmp"></asp:RequiredFieldValidator>
                                <div class="col-md-12">
                                    <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>From Date<span class="text-danger">*</span></label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                    ErrorMessage="Please Select From Date" ValidationGroup="Val1" ControlToValidate="txtFdate"></asp:RequiredFieldValidator>
                                <div class="input-group date" id='fromdate'>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </div>
                                    <asp:TextBox ID="txtFdate" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>To Date<span class="text-danger">*</span></label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none"
                                    ErrorMessage="Please Select To Date" ValidationGroup="Val1" ControlToValidate="txtTdate"></asp:RequiredFieldValidator>
                                <div class="input-group date" id='todate'>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                    </div>
                                    <asp:TextBox ID="txtTdate" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6" style="padding-top: 18px;">
                            <div class="form-group">
                                <asp:Button ID="btnsubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="35%"></rsweb:ReportViewer>
                        </div>

                    </div>

                </form>
            </div>

        </div>
    </div>
    <%-- </div>
        </div>--%>
    <%--</div>--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
