﻿Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Net.Mail
Imports System.Web.Services
Imports System.Web
Imports System.Collections.Generic
Imports FreeTextBoxControls.Support
Imports FreeTextBoxControls.Licensing
Imports FreeTextBoxControls
Imports System.Windows.Documents
'Imports System.Object
'Imports System.Windows.Documents.TextRange
'Imports System.Windows.Documents.TextSelection


Imports clsSubSonicCommonFunctions
Partial Class AdminFunctions_AdminPanel_Template_Editors
    Inherits System.Web.UI.Page
    Dim objData As SqlDataReader
    Dim obj As New clsWorkSpace
    Dim clsObj As New clsMasters
    Dim dtProject As DataTable
    Dim strRedirect As String = String.Empty
    Dim strQuery As String = String.Empty
    Dim MailDesc As String = ""
    Dim Content As String = ""
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            LoadMailDescriptions()
            txtAreaContent.ViewStateMode = UI.ViewStateMode.Enabled
        End If
    End Sub
    Public Sub LoadMailDescriptions()
        ObjSubSonic.Binddropdown(ddlMailDescription, "USP_GET_MAIL_DESCRIPTIONS", "Mail_desc", "Mail_id")
        ddlMailDescription.Items(0).Text = "--Select--"
    End Sub

    Public Sub Binddropdown(ByRef cboCombo As DropDownList, ByVal SP_Name As String, ByVal TxtField As String, ByVal ValField As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        Dim dummy As String = sp.Command.CommandSql
        Dim ds As New DataSet
        ds = sp.GetDataSet
        cboCombo.DataSource = ds
        cboCombo.DataTextField = TxtField
        cboCombo.DataValueField = ValField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")

    End Sub
    'Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
    '    BindTextArea()
    'End Sub
    'Private Sub BindTextArea()
    '    If ddlMailDescription.SelectedValue = "--Select--" Then
    '        MailDesc = ""
    '    Else
    '        MailDesc = ddlMailDescription.SelectedValue
    '    End If
    '    Dim sp(0) As SqlParameter
    '    sp(0) = New SqlParameter("@MAIL_ID", SqlDbType.NVarChar, 200)
    '    sp(0).Value = ddlMailDescription.SelectedValue
    '    txtAreaContent.InnerText = ObjSubSonic.GetSubSonicExecuteScalar("GET_MAIL_BODY", sp)

    'End Sub
    Protected Sub ddlMailDescription_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMailDescription.SelectedIndexChanged

        If ddlMailDescription.SelectedValue = "--Select--" Then
            MailDesc = ""
        Else
            MailDesc = ddlMailDescription.SelectedValue
        End If
        Dim sp(0) As SqlParameter
        sp(0) = New SqlParameter("@MAIL_ID", SqlDbType.NVarChar, 200)
        sp(0).Value = ddlMailDescription.SelectedValue
        txtAreaContent.Text = ObjSubSonic.GetSubSonicExecuteScalar("GET_MAIL_BODY", sp)

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        'Dim flowDocSelection As New TextRange(flowDoc.ContentStart, flowDoc.ContentEnd)

        'Dim rtfText As String

        ' Dim hhh As New TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd)

        'using (MemoryStream ms = new MemoryStream())
        '{
        '    tr.Save(ms, DataFormats.Rtf);
        '    rtfText = Encoding.ASCII.GetString(ms.ToArray());
        '}
        Dim s As String = txtAreaContent.Text
        Dim sp(1) As SqlParameter
        sp(0) = New SqlParameter("@MAIL_BODY", SqlDbType.NVarChar, -1)
        sp(0).Value = txtAreaContent.Text
        sp(1) = New SqlParameter("@MAIL_ID", SqlDbType.NVarChar, 200)
        sp(1).Value = ddlMailDescription.SelectedValue
        ObjSubSonic.GetSubSonicExecute("UPDATE_MAIL_BODY", sp)
        lblMsg.Text = "*Template Updated Successfully"
        ddlMailDescription.SelectedIndex = 0
        txtAreaContent.Text = " "
    End Sub


End Class

