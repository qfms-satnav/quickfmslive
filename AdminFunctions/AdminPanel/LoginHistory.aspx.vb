﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class LoginHistory
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim objMasters As clsMasters
    Dim fdate As DateTime
    Dim tdate As DateTime
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            ObjSubsonic.Binddropdown(ddlEmp, "USP_GET_USER_LOGIN_HISTORY", "NAME", "AUR_ID")
            BindActiveUser()
            txtFdate.Text = getoffsetdatetime(DateTime.Now).AddMonths(-1).ToString("dd/MM/yyyy")
            txtTdate.Text = getoffsetdatetime(DateTime.Now).ToString("dd/MM/yyyy")
            BindGrid()
            End If
    End Sub


    Public Sub BindActiveUser()
        Dim li3 As ListItem = Nothing
        li3 = ddlEmp.Items.FindByValue(Session("UID"))
        If Not li3 Is Nothing Then
            li3.Selected = True
        End If
    End Sub

    Private Sub BindGrid()

        Dim ds As New DataSet
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOGIN_HISTORY_REPORT")
        'Dim ds As DataSet = sp.GetDataSet()

        Dim fdt As DateTime = DateTime.ParseExact(txtFdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
        Dim tdt As DateTime = DateTime.ParseExact(txtTdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
        fdate = CDate(fdt.ToShortDateString)
        tdate = CDate(tdt.ToShortDateString)
        If fdate > tdate Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('From Date should be less than to date');", True)
        Else
            Dim param As SqlParameter() = New SqlParameter(2) {}

            param(0) = New SqlParameter("@FROM_DATE", SqlDbType.NVarChar)
            param(0).Value = fdt
            param(1) = New SqlParameter("@TO_DATE", SqlDbType.NVarChar)
            param(1).Value = tdt
            param(2) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar)
            param(2).Value = ddlEmp.SelectedValue
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_LOGIN_HISTORY_REPORT", param)
        End If
       

            Dim rds As New ReportDataSource()
            rds.Name = "LoginHistoryDS"
        rds.Value = ds.Tables(0)

        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/AdminFunctions/LoginHistoryReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
       
        'This refers to the dataset name in the RDLC file


    End Sub
    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        Dim fdt As DateTime = DateTime.ParseExact(txtFdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
        Dim tdt As DateTime = DateTime.ParseExact(txtTdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
        fdate = CDate(fdt.ToShortDateString)
        tdate = CDate(tdt.ToShortDateString)
        If fdate > tdate Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('From Date should be less than to date');", True)
        Else
            BindGrid()
        End If

    End Sub
End Class
