﻿app.service("CompanyService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    this.getModules = function () {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/Company/GetModules ')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

    //For Grid
    this.GetCompanyList = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Company/GetCompanyList')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //to save
    this.Save = function (Company) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Company/Save', Company)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GRPSave = function (GRPCompany) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Company/GRPSave', GRPCompany)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //to Modify
    this.Modify = function (Company) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Company/Modify', Company)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GRPModify = function (GRPCompany) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Company/GRPModify', GRPCompany)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GRPEdit = function (GRPCompany) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Company/GRPEdit', GRPCompany)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

}]);

app.controller('CompanyController', ['$scope', '$q', 'CompanyService', '$timeout','UtilityService', function ($scope, $q, CompanyService, $timeout, UtilityService) {

    $scope.Company = {};
    $scope.ActionStatus = 0;

    $scope.GRPCNPDetails = [];
    $scope.GRPCompany = {};
    $scope.GRPActionStatus = 0;

    $scope.GetModules = function () {
        CompanyService.getModules().then(function (response) {
            if (response.data != null) {
                $scope.MODlst = response.data;
                //console.log($scope.Modules);
            }
        });
    }

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        CompanyService.GetCompanyList().then(function (data) {
            if (data != null) {
                $scope.CNPDetails = data.CNPDetails;
                $scope.GRPCNPDetails = data.GRPCNPDetails;
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Data not found');
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.SaveOrModify = function () {
        progress(0, 'Loading...', true);
        if ($scope.ActionStatus == 0) {
            $scope.Save();
        }
        else {
            $scope.Modify();
        }
        progress(0, '', false);
    }

    $scope.Save = function () {
        progress(0, 'Loading...', true);
        CompanyService.Save($scope.Company).then(function (response) {
            if (response.data != null) {
                $scope.Company.CNP_ID = response.data;
                angular.copy($scope.Company, $scope.CNPDetails)
                //$scope.CNPDetails.push($scope.Company);
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    //Mofify
    $scope.Modify = function () {
        progress(0, 'Loading...', true);
        CompanyService.Modify($scope.Company).then(function (response) {
            if (response.data != null) {
                $scope.CNPDetails = [];
                $scope.CNPDetails.push($scope.Company);
                //console.log($scope.Company);
                //angular.copy($scope.Company, $scope.CNPDetails)
                $scope.ActionStatus = 0;
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.Edit = function (data) {
        progress(0, 'Loading...', true);
        $scope.ActionStatus = 1;
        $scope.Company = {};
        angular.copy(data, $scope.Company)
        progress(0, '', false);
    }

    $scope.GRPSaveOrModify = function () {
        progress(0, 'Loading...', true);
        if ($scope.GRPActionStatus == 0) {
            $scope.GRPSave();
        }
        else {
            $scope.GRPModify();
        }
        progress(0, '', false);
    }

    $scope.GRPSave = function () {
        progress(0, 'Loading...', true);
        $scope.GRPCompany.CNP_PARENT_ID = $scope.CNPDetails[0].CNP_ID;
        //console.log($scope.GRPCompany);
        CompanyService.GRPSave($scope.GRPCompany).then(function (response) {
            if (response.data != null) {
                $scope.GRPCompany.CNP_ID = response.data.CNP_ID;
                $scope.GRPCompany.CNP_MODULE = response.data.CNP_MODULE;
                $scope.GRPCNPDetails.push($scope.GRPCompany);
                $scope.GRPClear();
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    //Mofify
    $scope.GRPModify = function () {
        progress(0, 'Loading...', true);
        CompanyService.GRPModify($scope.GRPCompany).then(function (response) {
            if (response.data != null) {
                //console.log($scope.GRPCompany);
                $scope.GRPCompany.CNP_MODULE = response.data.CNP_MODULE;
                angular.copy($scope.GRPCompany, $scope.GRPTempobj)
                //$scope.GRPCNPDetails.push($scope.GRPCompany);
                $scope.GRPActionStatus = 0;
                $scope.GRPClear();
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.GRPEdit = function (data) {
        progress(0, 'Loading...', true);
        CompanyService.GRPEdit($scope.GRPCompany).then(function (response) {
            if (response.data != null) {
                angular.forEach($scope.MODlst, function (value, key) {
                    value.ticked = false;
                });
                //$scope.MODlst = response.data;
                angular.forEach($scope.MODlst, function (value, key) {
                    $scope.SelectedObj = _.find(response.data, { MOD_CODE: value.MOD_CODE })
                    //console.log($scope.SelectedObj);
                    if ($scope.SelectedObj != undefined) {
                        value.ticked = true;
                    }
                });
                $scope.GRPActionStatus = 1;
                progress(0, '', false);
            }
        }, function (response) {
            progress(0, '', false);
        });
        $scope.GRPActionStatus = 1;
        $scope.GRPTempobj = data;
        angular.copy(data, $scope.GRPCompany)
        progress(0, '', false);
    }

    //clear 
    $scope.Clear = function () {
        $scope.Company = {};
        $scope.ActionStatus = 0;
        $scope.frmCompany.$submitted = false;
    }

    //clear 
    $scope.GRPClear = function () {
        $scope.GRPCompany = {};
        $scope.GRPCompany.MODlst = [];
        $scope.GRPActionStatus = 0;
        angular.forEach($scope.MODlst, function (value, key) {
            value.ticked = false;
        });
        $scope.frmGRPCompany.$submitted = false;
    }

    setTimeout(function () {
        $scope.LoadData();
    }
    , 200);

    $scope.GetModules();
}]);