<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="AddPrivileges.aspx.vb" Inherits="AdminFunctions_AddPrivileges" Title="Add Privileges" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>

       

        function check(objP, status) {
            var obj = document.getElementById(objP);
            var collection = obj.getElementsByTagName('input');
            for (var i = 0; i < collection.elements.length; i++) {
                var e = collection.elements[i];
                if (e.type.toUpperCase() == 'CHECKBOX') {
                    if (status == '0')
                        e.checked = true;
                    else
                        e.checked = false;
                }
            }
        }

        function CheckParent(Id) {
            var divID = document.getElementsByTagName(Id);
            var SleepTestComplete, i;
            SleepTestComplete = "";
            var elm = document.getElementById(Id).length;
            for (i = 0; i < elm; i++) {
                if (divID[i].checked)
                    SleepTestComplete = divID[i].value;
                alert(SleepTestComplete);
            }
        }

      
      

    </script>

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Add Privileges </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <h4>
                                                <label class="col-md-12 control-label" style="text-align: center;">
                                                    Roles Selection for
                                                    <asp:Label ID="lblRoleName" runat="server"></asp:Label>
                                                </label>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-left">

                                    <asp:DataList ID="gvPrivileges" runat="server" CellPadding="3" ShowHeader="false"
                                        RepeatLayout="Table" Width="100%">
                                        <ItemTemplate>
                                            <table style="width: 99%; padding: 3px;">
                                                <tr>
                                                    <td style="width: 5%;" nowrap="nowrap">
                                                        <asp:CheckBox ID="chkParent" runat="server" Text='<%#Eval("cls_description") %>' />
                                                        <asp:Label ID="lblId" runat="server" Text='<%#Eval("Cls_Id") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblAssigned" runat="server" Text='<%#Eval("isAssigned") %>' Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="text-align: left; vertical-align: top;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%"></td>
                                                    <td style="text-align: left; vertical-align: top;">
                                                        <asp:Panel ID="pnlSub" runat="server" Width="100%">
                                                            <asp:DataList ID="gvSubPrivileges" runat="server" CellPadding="3" ShowHeader="false"
                                                                RepeatLayout="Table" Width="100%">
                                                                <ItemTemplate>
                                                                    <table style="width: 99%; padding: 3px;">
                                                                        <tr>
                                                                            <td style="width: 5%;" nowrap="nowrap">
                                                                                <asp:CheckBox ID="chkChild" runat="server" Text='<%#Eval("cls_description") %>' />
                                                                                <asp:Label ID="lblSubId" runat="server" Text='<%#Eval("Cls_Id") %>' Visible="false"></asp:Label>
                                                                                <asp:Label ID="lblSubAssigned" runat="server" Text='<%#Eval("isAssigned") %>' Visible="false"></asp:Label>
                                                                            </td>
                                                                            <td style="text-align: left; vertical-align: top;"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%"></td>
                                                                            <td style="text-align: left; vertical-align: top;">
                                                                                <asp:Panel ID="pnlInner" runat="server" Width="100%">
                                                                                    <asp:DataList ID="gvInnerPrivileges" runat="server" HeaderStyle-HorizontalAlign="Left"
                                                                                        CellPadding="3" ShowHeader="false" Width="100%" RepeatLayout="Table">
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkInnerChild" runat="server" Text='<%#Eval("cls_description") %>' />
                                                                                            <asp:Label ID="lblInnerId" runat="server" Text='<%#Eval("Cls_Id") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label ID="lblInnerAssigned" runat="server" Text='<%#Eval("isAssigned") %>' Visible="false"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" />
                                    <asp:Button ID="btnBack" runat="Server" Text="Back" CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

