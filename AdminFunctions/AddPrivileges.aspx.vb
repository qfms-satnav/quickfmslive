Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic


Partial Class AdminFunctions_AddPrivileges
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Try

                Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ROLE_COUNT")
                sp3.Command.AddParameter("@id", Request.QueryString("_id"), DbType.Int32)
                Dim ds3 As New DataSet()
                ds3 = sp3.GetDataSet()
                If ds3.Tables(0).Rows.Count > 0 Then
                    lblRoleName.Text = ds3.Tables(0).Rows(0).Item("ROL_ACRONYM")
                End If
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"PRIVILEGES_GetBYParentIdAndRoleId_SP")
            Dim parid As Integer = 0
            Dim rolid As Integer
            rolid = Convert.ToInt32(Request.QueryString("_id"))
            sp.Command.AddParameter("@ParentId", parid, DbType.Int32)
            sp.Command.AddParameter("@RolId", rolid, DbType.Int32)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvPrivileges.DataSource = ds
            gvPrivileges.DataBind()

            For Each gRow As DataListItem In gvPrivileges.Items
                Dim chkParent As CheckBox = CType(gRow.FindControl("chkParent"), CheckBox)
                Dim lblId As Label = CType(gRow.FindControl("lblId"), Label)
                Dim lblAssigned As Label = CType(gRow.FindControl("lblAssigned"), Label)
                Dim gvSubPrivileges As DataList = CType(gRow.FindControl("gvSubPrivileges"), DataList)
                Dim pnlSub As Panel = CType(gRow.FindControl("pnlSub"), Panel)
                'chkParent.Attributes.Add("onclick", "JavaScript:check('" & pnlSub.ClientID & "','" & lblAssigned.Text & "');")
                chkParent.Attributes.Add("onclick", "return CheckParent('" & pnlSub.ClientID & "');")
                If lblAssigned.Text = "0" Then
                    chkParent.Checked = False
                Else
                    chkParent.Checked = True
                End If

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"PRIVILEGES_GetBYParentIdAndRoleId_SP")
                sp1.Command.AddParameter("@ParentId", Convert.ToInt32(lblId.Text), DbType.Int32)
                sp1.Command.AddParameter("@RolId", Convert.ToInt32(Request.QueryString("_id")), DbType.Int32)
                Dim ds1 As New DataSet
                ds1 = sp1.GetDataSet
                gvSubPrivileges.DataSource = ds1
                gvSubPrivileges.DataBind()

                For Each gSubRow As DataListItem In gvSubPrivileges.Items
                    Dim chkChild As CheckBox = CType(gSubRow.FindControl("chkChild"), CheckBox)
                    Dim lblSubId As Label = CType(gSubRow.FindControl("lblSubId"), Label)
                    Dim lblSubAssigned As Label = CType(gSubRow.FindControl("lblSubAssigned"), Label)
                    Dim gvInnerPrivileges As DataList = CType(gSubRow.FindControl("gvInnerPrivileges"), DataList)
                    Dim pnlInner As Panel = CType(gSubRow.FindControl("pnlInner"), Panel)
                    chkChild.Attributes.Add("onclick", "JavaScript:check('" & pnlInner.ClientID & "','" & lblSubAssigned.Text & "');")
                    If lblSubAssigned.Text = "0" Then
                        chkChild.Checked = False
                    Else
                        chkChild.Checked = True
                    End If

                    If Not gvInnerPrivileges Is Nothing Then

                        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"PRIVILEGES_GetBYParentIdAndRoleId_SP")
                        sp2.Command.AddParameter("@ParentId", Convert.ToInt32(lblSubId.Text), DbType.Int32)
                        sp2.Command.AddParameter("@RolId", Convert.ToInt32(Request.QueryString("_id")), DbType.Int32)
                        Dim ds2 As New DataSet
                        ds2 = sp2.GetDataSet

                        gvInnerPrivileges.DataSource = ds2
                        gvInnerPrivileges.DataBind()

                        For Each gInnerRow As DataListItem In gvInnerPrivileges.Items
                            Dim chkInnerChild As CheckBox = CType(gInnerRow.FindControl("chkInnerChild"), CheckBox)
                            Dim lblInnerId As Label = CType(gInnerRow.FindControl("lblInnerId"), Label)
                            Dim lblInnerAssigned As Label = CType(gInnerRow.FindControl("lblInnerAssigned"), Label)
                            If lblInnerAssigned.Text = "0" Then
                                chkInnerChild.Checked = False
                            Else
                                chkInnerChild.Checked = True
                            End If
                        Next
                    End If

                Next

            Next
        End If
    End Sub
    
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_DelPrivileges_SP")
        sp.Command.AddParameter("@prvrolid", Request.QueryString("_id"), DbType.Int32)
        sp.Execute()

        For Each gRow As DataListItem In gvPrivileges.Items
            Dim chkParent As CheckBox = CType(gRow.FindControl("chkParent"), CheckBox)
            Dim lblId As Label = CType(gRow.FindControl("lblId"), Label)
            Dim lblAssigned As Label = CType(gRow.FindControl("lblAssigned"), Label)
            Dim gvSubPrivileges As DataList = CType(gRow.FindControl("gvSubPrivileges"), DataList)

            If chkParent.Checked = True Then
                Insert(Request.QueryString("_id"), lblId.Text)
            End If

            For Each gSubRow As DataListItem In gvSubPrivileges.Items
                Dim chkChild As CheckBox = CType(gSubRow.FindControl("chkChild"), CheckBox)
                Dim lblSubId As Label = CType(gSubRow.FindControl("lblSubId"), Label)
                Dim lblSubAssigned As Label = CType(gSubRow.FindControl("lblSubAssigned"), Label)
                Dim gvInnerPrivileges As DataList = CType(gSubRow.FindControl("gvInnerPrivileges"), DataList)
                If chkChild.Checked = True Then
                    Insert(Request.QueryString("_id"), lblSubId.Text)
                End If

                If Not gvInnerPrivileges Is Nothing Then
                    For Each gInnerRow As DataListItem In gvInnerPrivileges.Items
                        Dim chkInnerChild As CheckBox = CType(gInnerRow.FindControl("chkInnerChild"), CheckBox)
                        Dim lblInnerId As Label = CType(gInnerRow.FindControl("lblInnerId"), Label)
                        Dim lblInnerAssigned As Label = CType(gInnerRow.FindControl("lblInnerAssigned"), Label)

                        If chkInnerChild.Checked = True Then
                            Insert(Request.QueryString("_id"), lblInnerId.Text)
                        End If
                    Next
                End If
            Next
            Next
            '    Response.Redirect("Privileges.aspx")
    End Sub
    Public Sub Insert(ByVal PrvRolId As Integer, ByVal PrvClsId As Integer)

        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_PRVGLS")
            sp.Command.AddParameter("@PrvRolId", PrvRolId, DbType.Int32)
            sp.Command.AddParameter("@PrvClsId", PrvClsId, DbType.Int32)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("Privileges.aspx")
    End Sub
    Dim strUserName As String = String.Empty
    Protected ReadOnly Property UserName() As String

        Get
            If strUserName.Length = 0 Then

                If Not System.Web.HttpContext.Current Is Nothing Then
                    strUserName = System.Web.HttpContext.Current.User.Identity.Name
                Else
                    strUserName = System.Threading.Thread.CurrentPrincipal.Identity.Name
                End If
                Return strUserName
            End If
            Return strUserName
        End Get
    End Property
End Class
