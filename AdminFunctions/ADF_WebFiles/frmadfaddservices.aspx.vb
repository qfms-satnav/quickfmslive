Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfaddservices
    Inherits System.Web.UI.Page
#Region "General Variables"

    Dim drService As SqlDataReader
    Dim dsService As New DataSet()
    Dim strTfld, strVfld, str, strSql As String
    Dim strDesc, strPid, strLevel, strPsyid As String
    Dim strTitle, strSdesc, strSpage, strStpage, strSmenu, strSinstance As String
    Dim iSpsyid, iSclsid As Integer
#End Region
    Public Sub loadcombos(ByVal str As String, ByVal cmbsrc As DropDownList)
        Try
            Select Case str
                Case "process"
                    strSql = "SELECT PSY_ID,PSY_TITLE from  " & Session("TENANT") & "." & "PARTICIPATING_SYSTEM WHERE PSY_ID " _
                    & " in (SELECT POW_PSY_ID FROM " & Session("TENANT") & "." & "PROCESS_OWNER WHERE POW_ACTIVE='N')"
                    '& " in (SELECT POW_PSY_ID FROM PROCESS_OWNER WHERE POW_USR_ID='" & Session("uid") & "')"
                    'strSql = "NP_INLINE_GET_PROCESS"
                    strTfld = "PSY_TITLE"
                    strVfld = "PSY_ID"
                Case "pprocess"
                    strSql = "SELECT CLS_ID,CLS_DESCRIPTION FROM  " & Session("TENANT") & "." & "AMT_MENUSNEW_VW WHERE SER_ID IS NULL " _
                    & "AND CLS_PSY_ID IN (SELECT POW_PSY_ID FROM " & Session("TENANT") & "." & "PROCESS_OWNER  WHERE POW_ACTIVE='N')"
                    '& "AND CLS_PSY_ID IN (SELECT POW_PSY_ID FROM PROCESS_OWNER WHERE POW_USR_ID='" & Session("uid") & "')"
                    'strSql = "NP_INLINE_GET_PROCESS_CLASSIFICATION"
                    strTfld = "CLS_DESCRIPTION"
                    strVfld = "CLS_ID"
            End Select
            dsService = SqlHelper.ExecuteDataset(CommandType.Text, strSql)
            cmbsrc.Items.Clear()
            cmbsrc.DataSource = dsService
            cmbsrc.DataTextField = strTfld
            cmbsrc.DataValueField = strVfld
            cmbsrc.DataBind()
            cmbsrc.Items.Insert(0, "--Select--")
            If str = "pprocess" Then
                cmbsrc.Items.Insert(1, "Top Most")
            End If
            dsService.Clear()
        Catch exp As Exception
            Response.Write(exp.GetBaseException)
        End Try
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            loadcombos("process", cboProcess)
            loadcombos("pprocess", cboParent)
        End If
    End Sub
    Public Sub insertclassification()
        'max auto gen of cls id
        str = "select max(cls_id) from " & Session("TENANT") & "." & "classification"
        'str = "NP_INLINE_MAX_CLS_ID_CLASSIFICATION"
        Dim iClsID As Integer = CType(SqlHelper.ExecuteScalar(CommandType.Text, str), Integer) + 1
        strDesc = Replace(Trim(txtSerTitle.Text), "'", "''")
        If cboParent.SelectedItem.Value = "Top Most" Then
            cboParent.SelectedItem.Value = 0
        End If
        strPid = cboParent.SelectedItem.Value
        If strPid = 0 Then
            strLevel = 1
        Else
            strSql = "SELECT CLS_LEVEL FROM  " & Session("TENANT") & "." & "CLASSIFICATION Where CLS_ID=" & strPid
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "NP_INLINE_CLS_LEVEL_CLASSIFICATION")
            'sp.Command.AddParameter("@STRPID", strPid, DbType.String)
            'sp.ExecuteScalar()
            drService = SqlHelper.ExecuteReader(CommandType.Text, strSql)
            'drService = sp.GetReader()
            While drService.Read
                strLevel = drService("CLS_LEVEL") + 1
            End While
            drService.Close()
        End If
        strPsyid = cboProcess.SelectedItem.Value
        strSql = "INSERT INTO  " & Session("TENANT") & "." & "CLASSIFICATION (CLS_DESCRIPTION,CLS_PARENT_ID,CLS_LEVEL " & _
        ",CLS_PSY_ID,CLS_ACTIVE,CLS_UPDATED_BY,CLS_UPDATED_ON,CLS_TIP) VALUES ('" & strDesc & "','" _
        & strPid & "'," & strLevel & "," & strPsyid & ",'Y','" & Session("UID") & "','" _
        & getoffsetdatetime(DateTime.Now) & "','None')"
        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "NP_INLINE_INSERT_CLASSIFICATION")
        'sp1.Command.AddParameter("@STRDESC", strDesc, DbType.String)
        'sp1.Command.AddParameter("@STRPID", strPid, DbType.String)
        'sp1.Command.AddParameter("@STRLEVEL", strLevel, DbType.String)
        'sp1.Command.AddParameter("@STRPSYID", strPsyid, DbType.String)
        'sp1.Command.AddParameter("@UID", Session("Uid"), DbType.String)
        'sp1.ExecuteScalar()
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
        If strPid = 0 Then
        Else
            insertservice(iClsID)
        End If
    End Sub
    Public Sub insertservice(ByVal chkclsid As Integer)
        strTitle = Replace(Trim(txtSerTitle.Text), "'", "''")
        strSdesc = Replace(Trim(txtDesc.Text), "'", "''")
        strSpage = Replace(Trim(txtSerPage.Text), "'", "''")
        strStpage = Replace(Trim(txtTrackPage.Text), "'", "''")
        strSmenu = "N"
        If chkReqId.Checked = True Then
            strSinstance = 1
        Else
            strSinstance = 0
        End If
        iSpsyid = cboProcess.SelectedItem.Value
        iSclsid = chkclsid
        strSql = "INSERT INTO  " & Session("TENANT") & "." & "SERVICE (SER_TITLE,SER_DESCRIPTION,SER_PSY_ID,SER_PAGE_PATH" _
        & ",SER_trACKING_PAGE,SER_CLS_ID,SER_MENU_ITEM,SER_INSTANCE_OPTION) values ('" _
        & strTitle & "','" & strSdesc & "'," & iSpsyid & ",'" & strSpage & "','" & strStpage & "'," _
        & iSclsid & ",'" & strSmenu & "','" & strSinstance & "')"
        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "NP_INLINE_INSERT_SERVICE")
        'sp1.Command.AddParameter("@STRTITLE", strTitle, DbType.String)
        'sp1.Command.AddParameter("@STRSDESC", strSdesc, DbType.String)
        'sp1.Command.AddParameter("@ISPSYID", iSpsyid, DbType.String)
        'sp1.Command.AddParameter("@STRSPAGE", strSpage, DbType.String)
        'sp1.Command.AddParameter("@STRSTPAGE", strStpage, DbType.String)
        'sp1.Command.AddParameter("@ISCLSID", iSclsid, DbType.String)
        'sp1.Command.AddParameter("@STRSMENU", strSmenu, DbType.String)
        'sp1.Command.AddParameter("@STRSINSTANCE", strSinstance, DbType.String)
        'sp1.ExecuteScalar()
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSql) '
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        insertclassification()
        lblMessage.Visible = True
        If chkParent.Checked = True Then

        End If
    End Sub

    Private Sub btnReset_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.ServerClick
        cboProcess.SelectedItem.Value = ""
        cboParent.SelectedItem.Value = ""
        txtSerTitle.Text = ""
        chkParent.Checked = ""
        txtTrackPage.Text = ""
        txtDesc.Text = ""
        chkReqId.Checked = ""
    End Sub
End Class


