﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--CodeFile="frmUserRoleMappingNew.aspx.cs" Inherits="AdminFunctions_ADF_WebFiles_frmUserRoleMappingNew"--%>
<!DOCTYPE html>

<html data-ng-app="QuickFMS" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/Messenger/messenger.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/UserRoleMapping.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />

    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({
                format: 'M dd, yyyy',
                autoclose: true,
                orientation: 'bottom'
            });
        };
    </script>

</head>
<body data-ng-controller="UserRoleMappingNewController" class="amantra">
    <%--  <form id="form1">--%>

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">
                    <label>User & Role Mapping </label>
                </h3>
            </div>
            <div class="card" data-ng-show="Viewstatus==0">
                <div style="margin: 0px 28px 2% 3px; border: -51px; background-color: #eceded;">
                    <br />
                    <form role="form" id="FileUsrUpl" name="FileUsrUpl" data-valid-submit="UploadFile()" novalidate>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <a style="margin-left: 25px;" href="../../Masters/Mas_Webfiles/User_Role_Mapping_Template.xlsx">Click here to download template</a><%--font-size: large;--%>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12" style="width: 9%; margin-right: -3%;">
                                <%--style="width: 9%; margin-right: -3%;"--%>
                                <div class="form-group "> <%--form-inline--%>
                                    <b>Upload File </b>
                                    <input type="file" name="UPLFILE" id="FileUpl" required="" />
                                </div>
                            </div>
                            <%-- <div class="col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    
                                </div>

                            </div>--%>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <%--style="margin-left: -35%;"--%>
                                <div class="form-group ">
                                    <input type="submit" id="btnUpload" class="btn btn-primary custom-button-color" value="Upload" /><%--style="margin-left: 110%;"--%>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <%-- style="margin-left: 35%;"--%>
                                <div class="form-group pull-right">
                                    <a id="HyperLink11" href="../../Masters/Mas_Webfiles/frmNewUser.aspx" onmouseover="Tip('Add New User')" onmouseout="UnTip()"><i class="fa fa-user fa-2x"></i></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <input id="filtertxt" class="form-control" placeholder="Filter by any..." type="text" style="width: 50%" />
                    </div>
                    <div class="col-sm-4">
                        <div>
                            <label style="display: flex"><b>Total Employee Count:&nbsp;</b><%--<a href="#3">--%><b><span ng-bind="name"></span></b><%--</a> --%></label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="pull-right form-inline">
                            <input style="margin-right: 25px;" type="submit" class="btn btn-primary nextBtn pull-right" value="Refresh Table" data-ng-click="refreshEmpDetails()">
                            <a data-ng-click="GenReport(EmployeeDetailsObj,'xlsx')"><i id="I2" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right" style="padding-right: 45px"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row"></div>
                <div data-ag-grid="gridOptions" style="height: 630px; width: 98%; margin: 5px 1px 35px 0px;" class="ag-blue"></div>
            </div>
            <div style="height: 530px" data-ng-show="Viewstatus==2">
                <input id="Text1" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                <div data-ag-grid="gridOptions1" style="height: 100%; width: 98%;" class="ag-blue"></div>
            </div>
            <div data-ng-show="Viewstatus==1" class="container-fluid">
                <div class="card">
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <span style="color: red;">&nbsp; &nbsp;  <span style="color: red;">**</span> Select to auto fill the data
                        </div>
                    </div>
                    <div class="container" style="padding-top: 10px;">
                        <div class="stepwizard">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step" data-ng-show="EmployeeDetails">
                                    <a href="#step-1" id="stp1" type="button" ng-click="tabStatusFn('E')"
                                        ng-class="{'btn btn-primary btn-circle': tabStatus==1, 'btn btn-default btn-circle': tabStatus!=1}">Employee Details</a>
                                </div>
                                <div class="stepwizard-step" data-ng-show="RoleAssignment">
                                    <a href="#step-2" id="stp2" type="button" ng-click="tabStatusFn('R')"
                                        ng-class="{'btn btn-primary btn-circle': tabStatus==2, 'btn btn-default btn-circle': tabStatus!=2}">Role Assignment</a>
                                </div>
                                <div class="stepwizard-step" data-ng-show="Mapping">
                                    <a href="#step-3" id="stp3" type="button" ng-click="tabStatusFn('M')"
                                        ng-class="{'btn btn-primary btn-circle': tabStatus==3, 'btn btn-default btn-circle': tabStatus!=3}">Mapping</a>
                                </div>
                            </div>
                        </div>
                        <div class="setup-content" data-ng-if="tabStatus==1">
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>User ID</b><span style="color: red;">*</span></label>
                                        <input id="UserId" type="text" class="form-control" data-ng-model="EmployeeDetailsObj.AUR_ID" name="AUR_ID" required />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Name</b> </label>
                                        <br />
                                        <input type="text" id="Username" class="form-control" data-ng-model="EmployeeDetailsObj.AUR_KNOWN_AS" name="AUR_KNOWN_AS" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Email</b></label>
                                        <input type="text" class="form-control" data-ng-model="EmployeeDetailsObj.AUR_EMAIL" name="AUR_EMAIL" ng-pattern="/^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z-.]+\.[a-zA-Z.]{2,5}$/" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Phone No</b> </label>
                                        <input type="text" class="form-control" data-ng-model="EmployeeDetailsObj.AUR_RES_NUMBER" maxlength="11" minlength="10" name="AUR_RES_NUMBER" ng-pattern="/^[0-9]+$/" />
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Date Of Birth</b></label>
                                        <div class="input-group date" id='frmdate'>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('frmdate')"></span>
                                            </span>
                                            <input class="form-control" id="txtreqfrmdate" data-ng-model="EmployeeDetailsObj.AUR_DOB" name="AUR_DOB" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Date Of Joining</b></label>
                                        <div class="input-group date" id='doj'>
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('doj')"></i>
                                            </div>
                                            <input class="form-control" id="txtdoj" data-ng-model="EmployeeDetailsObj.AUR_DOJ" name="AUR_DOJ" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="form-inline">
                                            <b>Password</b>&nbsp;
                                                    <a href="#1" data-ng-if="passwordText=='password'" data-ng-click="passwordFn('S')" style="font-size: 10px;"><u>Show</u></a>
                                            <a href="#2" data-ng-if="passwordText=='text'" data-ng-click="passwordFn('H')" style="font-size: 10px;"><u>Hide</u></a></label>
                                        <div>
                                            <input class="form-control" id="txtPassword" data-ng-model="EmployeeDetailsObj.USR_LOGIN_PASSWORD" name="USR_LOGIN_PASSWORD" type="{{passwordText}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Reporting To</label>
                                    </div>
                                    <div angucomplete-alt id="ex7" placeholder="Search Employee" pause="500" selected-object="selectedEmp"
                                        remote-url="../../../api/Utility/GetEmployeeNameAndID/" remote-url-request-formatter="remoteUrlRequestFn"
                                        remote-url-data-field="items" title-field="NAME" minlength="2" input-class="form-control"
                                        match-class="highlight" ng-model="EmployeeDetailsObj.AUR_REPORTING_TO_NAME" required>
                                    </div>
                                    <input type="text" id="txtrptTo" data-ng-model="EmployeeDetailsObj.AUR_REPORTING_TO_NAME" style="display: none" name="AUR_REPORTING_TO_NAME" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Extension No</b></label>
                                        <input type="text" class="form-control" data-ng-model="EmployeeDetailsObj.AUR_EXTENSION" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Designation</b></label><br />
                                        <div isteven-multi-select data-input-model="designationddl" data-output-model="EmployeeDetailsObj.designationObj" data-button-label="icon DSG_NAME" data-item-label="icon DSG_NAME maker"
                                            data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>{{BsmDet.Parent}}</b></label><br />
                                        <div isteven-multi-select data-input-model="verticalddl" data-output-model="EmployeeDetailsObj.verticalObj" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                            data-on-item-click="VerChange(EmployeeDetailsObj.verticalObj[0].VER_CODE)" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>{{BsmDet.Child}}</b></label><br />
                                        <div isteven-multi-select data-input-model="costcenterddl" data-output-model="EmployeeDetailsObj.costcenterObj" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                            data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Status</b></label><br />
                                        <div isteven-multi-select data-input-model="usrStatus" data-output-model="EmployeeDetailsObj.StatusObj" data-button-label="icon Name" data-item-label="icon Name maker"
                                            data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Location</b></label><br />
                                        <div isteven-multi-select data-input-model="locationddl" data-output-model="EmployeeDetailsObj.locationObj" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <label class="control-label"><b>Employee Grade</b></label>
                                    <div isteven-multi-select data-input-model="EmployeeType" data-output-model="EmployeeDetailsObj.EmployeeTypeObj" data-button-label="icon AUR_GRADE" data-item-label="icon AUR_GRADE maker"
                                        data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Remarks</b> </label>
                                        <br />
                                        <textarea style="height: 10%;" rows="1" cols="10" id="UserRemarks" class="form-control" data-ng-model="EmployeeDetailsObj.AUR_EMP_REMARKS"></textarea>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group btn-toolbar pull-right">
                                        <input type="button" style="margin-left: 10px;" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                        <input type="submit" class="btn btn-primary nextBtn pull-right" value="Save & Continue" data-ng-click="SaveEmpDetails()">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="setup-content" data-ng-if="tabStatus==2">
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>
                                            <b>Module</b> <span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="modules" data-output-model="RoleDetailsObj.ModuleObj" data-button-label="icon MOD_NAME" data-item-label="icon MOD_NAME maker"
                                            data-on-select-none="EmployeeType()" data-on-item-click="accessbymodule()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>User ID</b></label>
                                        <input id="UserId1" type="text" class="form-control" data-ng-model="RoleDetailsObj.AUR_ID" name="AUR_ID" disabled="true" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Name</b> </label>
                                        <br />
                                        <input type="text" id="Username1" class="form-control" data-ng-model="RoleDetailsObj.AUR_KNOWN_AS" name="AUR_KNOWN_AS" disabled="true" />
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Access Level</b> <span style="color: red;">*</span></label><br />
                                        <div data-ng-repeat="obj in acceslevel">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <label>
                                                    <input type="checkbox" data-ng-model="obj.isChecked" ng-value="{{obj.isChecked}}" ng-change="Rolechk()" />
                                                    {{obj.ROL_DESCRIPTION}}                                                                                 
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Map Level Access</b></label><br />
                                        <input type="radio" name="userdetails" value="Y" ng-model="RoleDetailsObj.MapAccess" />&nbsp View & Edit &nbsp&nbsp&nbsp&nbsp
                                                        <input type="radio" name="userdetails" value="N" ng-model="RoleDetailsObj.MapAccess" />&nbsp Only View
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <span class="error" data-ng-show="RolValidation == false" style="color: red">Please Select Atleast One Role to Assign</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group btn-toolbar pull-right">
                                        <input type="button" style="margin-left: 10px;" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                        <input type="submit" class="btn btn-primary nextBtn pull-right" value="Save & Continue" data-ng-click="SaveRMDetls()">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="setup-content" data-ng-if="tabStatus==3">
                            <br />
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>Country</b> <span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="Countrylst" data-output-model="MapDetailsObj.CountryObj" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                            data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>City</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="Citylst" data-output-model="MapDetailsObj.CitylstObj" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                            data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>Location</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="locationddl" data-output-model="MapDetailsObj.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>Tower</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="Towerlist" data-output-model="MapDetailsObj.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                            data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>Floor</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="Floorlist" data-output-model="MapDetailsObj.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                            data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>Parent Entity</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="Parentlist" data-output-model="MapDetailsObj.selectedParentEntity" data-button-label="icon PE_NAME" data-item-label="icon PE_NAME maker"
                                            data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>Child Entity</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="Childlist" data-output-model="MapDetailsObj.selectedChildEntity" data-button-label="icon CHE_NAME" data-item-label="icon CHE_NAME maker"
                                            data-on-item-click="ChildEntityChange()" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>{{BsmDet.Parent}}</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="verticalddl" data-output-model="MapDetailsObj.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                            data-on-item-click="VerticalChange()" data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" style="width: 30%;">
                                    <div class="form-group">
                                        <label>
                                            <b>{{BsmDet.Child}}</b> <span style="color: red;">*</span><span style="color: red;">*</span>
                                        </label>
                                        <div isteven-multi-select data-input-model="costcenterddl" data-output-model="MapDetailsObj.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                            data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group btn-toolbar pull-right">
                                        <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                        <input type="submit" class="btn btn-primary nextBtn pull-right" value="Save & Continue" data-ng-click="SaveMappingDetls()">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- </form>--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js" defer></script>
    <script src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script src="../../BootStrapCSS/qtip/qtip.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt", "cp.ngConfirm"]);
        <%--var UserEdit = '<%=Session("USER_EDIT")%>'--%>
    </script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../System%20Preferences/Js/UserRoleMappingNew.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js" defer></script>
</body>
</html>
