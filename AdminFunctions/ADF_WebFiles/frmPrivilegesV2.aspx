﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmPrivilegesV2.aspx.cs" Inherits="AdminFunctions_ADF_WebFiles_frmPrivilegesV2" %>



<!DOCTYPE html>

<html ng-app="QuickFMS">
<head>
    <title></title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/menu/css/metismenu.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
</head>
   
<body data-ng-controller="PrivilegesController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Privileges</h3>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <h5>
                                    <label class="col-md-12 control-label" style="text-align: center;">
                                        Privileges Selection For 
                                                    <label id="lblRoleName"></label>
                                    </label>
                                </h5>

                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <input type="submit" id="btnBacktop" class="btn btn-primary custom-button-color" value="Back" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <permission-tree edit-data="permissionEditData"></permission-tree>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" id="btnUpdate" data-ng-click="SaveDetails()" class="btn btn-primary custom-button-color" value="Submit" />
                            <input type="submit" id="btnBack" class="btn btn-primary custom-button-color" value="Back" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="//angular-ui.github.io/ui-router/release/angular-ui-router.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../BootStrapCSS/jstree/jstree.min.js" defer></script>
    <script type="text/javascript" defer>
        $(function () {
            var name = GetParameterValues('Value');
            $('#lblRoleName').html(name);
        });
        $('#btnBack').click(function () {
            window.location.href = "../../AdminFunctions/Privileges.aspx";
        });
        $('#btnBacktop').click(function () {
            window.location.href = "../../AdminFunctions/Privileges.aspx";
        });
    </script>
    <script type="text/javascript" defer>
        // When the document is ready
        //    var app = angular.module('Example', ['ui.router'])


        var app = angular.module('QuickFMS', ["agGrid", 'ui.router'])

            .run(["$rootScope", "$state", function (n, i) {
                n.$state = i;
            }])
            .service('crudService', function ($http, $q, UtilityService) {
                var roleid;
                var deferred = $q.defer();
                this.GetGriddata = function (roleid) {
                    deferred = $q.defer();
                    return $http.get(UtilityService.path + '/api/Privileges/GetPrivilegesDataByRoleID/' + roleid)
                        .then(function (response) {
                            deferred.resolve(response.data);
                            return deferred.promise;
                        }, function (response) {
                            deferred.reject(response);
                            return deferred.promise;
                        });
                };

                //to save
                this.SavePrivilegeDetails = function (dataobject) {
                    deferred = $q.defer();
                    return $http.post(UtilityService.path + '/api/Privileges/save', dataobject)
                        .then(function (response) {
                            deferred.resolve(response.data);
                            return deferred.promise;
                        }, function (response) {
                            deferred.reject(response);
                            return deferred.promise;

                        });
                };

            })
            .controller('PrivilegesController', ['$scope', 'crudService', '$state', function ($scope, crudService, $state) {
                var roleid = GetParameterValues('_id');


                $scope.SaveDetails = function () {
                    $scope.existingPrivileges = [];
                    angular.forEach($scope.permissionEditData.grantedPermissionNames, function (value, key) {
                        if (value.id != undefined) {
                            $scope.existingPrivileges.push(value.id);
                            $scope.permissionEditData.grantedPermissionNames = $scope.existingPrivileges
                        }

                    });

                    $scope.dataobject = {
                        permissions: $scope.permissionEditData.grantedPermissionNames,
                        RoleID: roleid
                    };

                    crudService.SavePrivilegeDetails($scope.dataobject).then(function (dataobj) {
                        if (dataobj.data != null) {
                            showNotification('success', 8, 'bottom-right', dataobj.Status);
                        }
                        else {
                            showNotification('error', 8, 'bottom-right', dataobj.Status);
                        }
                    }, function (error) {
                    });


                }


                $scope.tree = [];
                $scope.LoadData = function () {
                    crudService.GetGriddata(roleid).then(function (pl) {
                        //console.log(pl);
                        $scope.permissionEditData = {
                            permissions: pl.permissions,
                            grantedPermissionNames: pl.grantedPermissionNames

                        };

                    }, function (error) {
                        console.log(error);
                    });
                }
                $scope.LoadData();
            }]);

        app.directive("permissionTree", [function () {
            return {
                restrict: "E",
                template: '<div class="permission-tree"><\/div>',
                scope: {
                    editData: "="
                },
                link: function (n, t) {
                    function e() {
                        u && i.jstree("destroy");
                        var map = _.map(n.editData.permissions, function (t) {
                            return {
                                id: t.CLS_ID,
                                parent: t.CLS_PARENT_ID != 0 ? t.CLS_PARENT_ID : "#",
                                text: t.name,
                                state: {
                                    opened: !1,
                                    selected: _.findIndex(n.editData.grantedPermissionNames, function (item) {
                                        return item.id == t.CLS_ID
                                    }) > -1
                                }
                            }
                        });

                        i.jstree({
                            core: {
                                data: map
                            },
                            types: {
                                "default": {
                                    //   icon: "fa fa-folder tree-item-icon-color icon-lg"
                                },
                                file: {
                                    //   icon: "fa fa-file tree-item-icon-color icon-lg"
                                }
                            },
                            checkbox: {
                                keep_selected_style: !1,
                                three_state: !1,
                                cascade: ""
                            },
                            plugins: ["checkbox", "types"]
                        });
                        u = !0;
                        i.on("changed.jstree", function (t, u) {
                            var s, e;
                            u.node && (s = r, s || (r = !0), u.node.state.selected ? (f(i.jstree("get_parent", u.node)), e = $.makeArray(i.jstree("get_children_dom", u.node)), i.jstree("select_node", e)) : (e = $.makeArray(i.jstree("get_children_dom", u.node)), i.jstree("deselect_node", e)), s || (n.$apply(function () {
                                n.editData.grantedPermissionNames = o()
                            }), r = !1))
                        })
                    }

                    function f(n) {
                        i.jstree("select_node", n, !0);
                        var t = i.jstree("get_parent", n);
                        t && f(t)
                    }

                    function o() {
                        for (var t = [], r = i.jstree("get_selected", !0), n = 0; n < r.length; n++)
                            t.push(r[n].original.id);
                        return t
                    }
                    var i = $(t).find(".permission-tree"),
                        u = !1,
                        r = !1;
                    n.$watch("editData", function () {
                        n.editData && e()
                    })
                }
            }
        }]);

    </script>


    <script src="../../SMViews/Utility.js" defer></script>
</body>
</html>
