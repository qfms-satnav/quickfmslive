Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.ClientScriptManager
Imports System.IO
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_frmPermitTenant
    Inherits System.Web.UI.Page
    Dim strT As String = ""
    Dim strB As String = ""
    Dim r As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

      
            If Not Page.IsPostBack Then
                strSQL = "select distinct aur_id,aur_known_as,aur_email,(select distinct property_name " & _
                " from  " & Session("TENANT") & "."  & "PN_PROPERTIES_ALL WHERE PROPERTY_CODE=AUR_BDG_ID)BUILDING FROM " & _
                "  " & Session("TENANT") & "."  & "AMANtrA_USER WHERE AUR_INACTIVE='Y' order by aur_known_As "
                BindGrid(strSQL, gvDetails)
                If gvDetails.Rows.Count > 0 Then
                    gvDetails.Visible = True
                    tr1.Visible = True
                    lblMsg.Visible = False
                Else
                    gvDetails.Visible = False
                    tr1.Visible = False
                    lblMsg.Visible = True
                    lblMsg.Text = "No Data Found!"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvDetails_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowCreated
        Try
            If e.Row.RowType = ListItemType.Header Then
                AddHandler CType(e.Row.FindControl("chkAll"), CheckBox).CheckedChanged, AddressOf grd_selectall
            End If
        Catch ex As Exception
            'lblError.Text = ex.Message
            'ErrorLog.LogError(ex)

        End Try
    End Sub
    Private Sub grd_selectall(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim i As Integer
            If chk.Checked = True Then
                For i = 0 To gvDetails.Rows.Count - 1
                    Dim chkselect As CheckBox = CType(gvDetails.Rows(i).FindControl("chkSelect"), CheckBox)
                    chkselect.Checked = True
                Next
                chk.Checked = True
            ElseIf chk.Checked = False Then
                For i = 0 To gvDetails.Rows.Count - 1
                    Dim chkselect As CheckBox = CType(gvDetails.Rows(i).FindControl("chkSelect"), CheckBox)
                    chkselect.Checked = False
                Next
                chk.Checked = False
            End If
        Catch ex As Exception
            'ErrorLog.LogError(ex)

        End Try
    End Sub
#Region " Display POPUP Messages "

    Private Sub PopUpMessage(ByVal strText As String)
        Dim strScript As String
        strScript = "<script language='javascript'>alert('" & strText & "')</script>"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "strScript", strScript)
        Return
    End Sub

#End Region

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Dim strTenant As String
        'strTenant = ""
        Dim cnt As Integer = 0
        Try
            For k As Integer = 0 To gvDetails.Rows.Count - 1
                Dim chkselect As CheckBox = CType(gvDetails.Rows(k).FindControl("chkSelect"), CheckBox)
                If chkselect.Checked = True Then
                    cnt = 1
                End If
            Next
            If cnt = 0 Then
                PopUpMessage("Please Select Tenants!")
                Exit Sub
            End If

            For k As Integer = 0 To gvDetails.Rows.Count - 1
                Dim chkselect As CheckBox = CType(gvDetails.Rows(k).FindControl("chkSelect"), CheckBox)
                Dim chkselectAC As CheckBox = CType(gvDetails.Rows(k).FindControl("chkSelectAC"), CheckBox)
                Dim chkselectCC As CheckBox = CType(gvDetails.Rows(k).FindControl("chkSelectCC"), CheckBox)
                Dim ddlHr1 As DropDownList = CType(gvDetails.Rows(k).FindControl("ddlHr"), DropDownList)
                Dim ddlMin1 As DropDownList = CType(gvDetails.Rows(k).FindControl("ddlMin"), DropDownList)
                Dim ddlHr2 As DropDownList = CType(gvDetails.Rows(k).FindControl("ddlHr1"), DropDownList)
                Dim ddlMin2 As DropDownList = CType(gvDetails.Rows(k).FindControl("ddlMin1"), DropDownList)
                Dim strFrom As String = ""
                Dim strTo As String = ""
                strFrom = ddlHr1.SelectedItem.Text.Trim & ":" & ddlMin1.SelectedItem.Text.Trim
                strTo = ddlHr2.SelectedItem.Text.Trim & ":" & ddlMin2.SelectedItem.Text.Trim
                If chkselect.Checked = True Then
                    'strTenant = gvDetails.Rows(k).cells(0).Text.Trim

                    strSQL = "update  " & Session("TENANT") & "."  & "AMANtrA_USER set aur_sta_id=1,aur_inactive='N',AUR_CATEGORY='TEN',AUR_OFFICE_FROM_HRS='" & strFrom & "',AUR_OFFICE_TO_HRS='" & strTo & "'"
                    strSQL1 = "UPDATE  " & Session("TENANT") & "."  & "[USER] set usr_active='Y',Usr_logged='N' where usr_id='" & gvDetails.Rows(k).cells(1).Text.Trim & "' "
                    SqlHelper.ExecuteNonQuery(Data.CommandType.Text, strSQL1)
                    strSQL1 = "INSERT INTO  " & Session("TENANT") & "."  & "User_Role (Url_USR_Id,Url_Rol_Id,Url_ScopeMap_Id) VALUES('" & gvDetails.Rows(k).cells(1).Text.Trim & "',12,21)"
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL1)
                    'strT = gvDetails.Rows(k).cells(1).Text.Trim & ","
                    'strB = gvDetails.Rows(k).cells(3).Text.Trim & ","



                    If chkselectAC.Checked Then
                        strSQL = strSQL & ",AUR_EXtrA_AC='Y'"
                    Else
                        strSQL = strSQL & ",AUR_EXtrA_AC='N'"
                    End If
                    If chkselectCC.Checked = True Then
                        strSQL = strSQL & ",AUR_CHILLER_CHARGE='Y'"
                    Else
                        strSQL = strSQL & ",AUR_CHILLER_CHARGE='N'"
                    End If
                    strSQL = strSQL & " where aur_id='" & gvDetails.Rows(k).cells(1).Text.Trim & "'"
                    r = SqlHelper.ExecuteNonQuery(Data.CommandType.Text, strSQL)
                End If
            Next
            'Session("strT") = strT
            'Session("strB") = strB

            If r > 0 Then
                Response.Redirect("../frmFinalPage.aspx?staid=PrivilegesGiven")
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class
