﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--  <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
        <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />--%>
</head>
<body data-ng-controller="CategoryRoleMappingController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<%--<div ba-panel ba-panel-title="Category Mapping" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <%--   <h3 class="panel-title">Category Role Mapping</h3>
            </div>
            <<div class="card">--%>

                <h3 class="panel-title">Category Role Mapping</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" name="Categorymap" novalidate>
                    <div class="clearfix">
                         <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="ddlasttype">Asset Type<span style="color: red;">*</span></label>
                                    <select id="ddlasttype" class="form-control selectpicker"  data-ng-model="Customized.selectedtype" data-ng-change="AstTypeChanged()" data-live-search="true" title="Select Asset Type">
                                        <option value="">--Select--</option>
                                        <option value="0">Capital Asset</option>
                                        <option value="1">Consumable Asset</option>
                                    </select>
                                </div>
                            </div>--%>
      <label id="lblMessage" ng-show="messageVisible" style="color: red !important; text-align: left; width: 100%; margin-left: 0;" ng-bind="messageText"></label>
                 <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': Categorymap.$submitted && Categorymap.ASSET_TYPE_NAME.$invalid}">
                                <label for="txtcode">Asset Type<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AstTypelist" data-output-model="Customized.selectedtype" data-button-label="icon ASSET_TYPE_NAME" data-item-label="icon ASSET_TYPE_NAME maker"
                                    data-on-item-click="AstTypeChanged()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedtype" name="ASSET_TYPE_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="Categorymap.$submitted && Categorymap.ASSET_TYPE_NAME.$invalid" style="color: red;">Please select Asset Type </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': Categorymap.$submitted && Categorymap.CAT_NAME.$invalid}">
                                <label for="txtcode">Asset Category<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="categorylist" data-output-model="Customized.selectedcat" data-button-label="icon CAT_NAME" data-item-label="icon CAT_NAME maker"
                                    data-on-item-click="CatChanged()" data-on-select-all="catSelectAll()" data-on-select-none="catSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedcat" name="CAT_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="Categorymap.$submitted && Categorymap.CAT_NAME.$invalid" style="color: red;">Please select Category </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': Categorymap.$submitted && Categorymap.AST_SUBCAT_NAME.$invalid}">
                                <label for="txtcode">Asset Subcategory<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="SubCatlist" data-output-model="Customized.selectedsubcat" data-button-label="icon AST_SUBCAT_NAME" data-item-label="icon AST_SUBCAT_NAME maker"
                                    data-on-item-click="SubCatChanged()" data-on-select-all="subcatSelectAll()" data-on-select-none="" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedsubcat" name="AST_SUBCAT_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="Categorymap.$submitted && Categorymap.AST_SUBCAT_NAME.$invalid" style="color: red;">Please select Subcategory </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Brand <span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Brandlist" data-output-model="Customized.selectedBrands" data-button-label="icon BRND_NAME" data-item-label="icon BRND_NAME maker"
                                    data-on-item-click="BrandChanged()" data-on-select-all="BrandSelectAll()" data-on-select-none="BrandSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedBrands" name="BRND_NAME" style="display: none" required="" />
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Model <span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Modellist" data-output-model="Customized.selectedModels" data-button-label="icon MD_NAME" data-item-label="icon MD_NAME maker"
                                    data-on-item-click="MdlChanged()" data-on-select-all="MdlChangeAll()" data-on-select-none="MdlSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedModels" name="MD_NAME" style="display: none" required="" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <%--<div class="form-group">
                            <label for="txtcode">Mapping Based on<span style="color: red;"></span></label>
                            <br />
                            <input type="radio" class="custom-control-input"
                                name="roleid" value="Role" data-ng-value="true"
                                data-ng-click="checkchange(1)" data-ng-model="Customized.rol" />Role
                                            <input type="radio" class="custom-control-input"
                                                name="roleid" value="Comp" data-ng-value="true" checked="checked"
                                                data-ng-click="checkchange(2)" data-ng-model="Customized.compn" />Company
                        </div>--%>
                            <div class="form-group">
                                <label for="txtcode">Mapping Based on<span style="color: red;"></span></label>
                                <br />
                                <input type="radio"  name="roleid" value="Role" data-ng-value="true" data-ng-disabled="IsDisabled"
                                    data-ng-click="checkchange(1)" data-ng-model="Customized.rol">Role
                                            <input type="radio" 
                                                name="roleid" value="Comp" data-ng-value="true" data-ng-disabled="IsDisabled"
                                                data-ng-click="checkchange(2)" data-ng-model="Customized.compn">Company
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="company1">
                            <div class="form-group" data-ng-class="{'has-error': Categorymap.$submitted && Categorymap.CNP_NAME.$invalid}">
                                <label class="control-label">Company<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="Customized.CNP_NAME" button-label="icon CNP_NAME"
                                    data-on-item-click="CMPChanged()" item-label="icon CNP_NAME" data-tick-property="ticked" data-on-select-all="COMPAll()" data-on-select-none="" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="Categorymap.$submitted && Categorymap.CNP_NAME.$invalid" style="color: red;">Please select Company </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="role1">
                            <div class="form-group" data-ng-class="{'has-error': Categorymap.$submitted && Categorymap.ROL_DESCRIPTION.$invalid}">
                                <label class="control-label">Role<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Role" data-output-model="Customized.ROL_DESCRIPTION" button-label="icon ROL_DESCRIPTION"
                                    item-label="icon ROL_DESCRIPTION" data-tick-property="ticked" data-on-select-all="ROLEALL()" data-on-select-none="" data-max-labels="1"
                                    data-on-item-click="ROLChanged()">
                                </div>
                                <input type="text" data-ng-model="Customized.ROL_DESCRIPTION" name="ROL_DESCRIPTION" style="display: none" required="" />
                                <span class="error" data-ng-show="Categorymap.$submitted && Categorymap.ROL_DESCRIPTION.$invalid" style="color: red;">Please select Role </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 text-left">

                            <input type="submit" value="Submit" id="btnSubmit" class="btn btn-primary custom-button-color" data-ng-click="SubmitData(1)" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" id="btnmodify" class="btn btn-primary custom-button-color" data-ng-click="SubmitData(2)" data-ng-show="ActionStatus==1" />
                        </div>
                    </div>
                    <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                        <%--<%--<div class="input-group" style="width: 20%">
                        <input type="text" class="form-control" id="filtertxt" name="srch-term" placeholder="Search" />--%>
                        <%--<div class="input-group-btn">
                            <button class="btn btn-primary custom-button-color" data-ng-click="SubmitData(1)" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                   </div>--%>
                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>

                </form>
            </div>
        </div>
          </div>
           <%--</div>
        </div>--%>
        <%--</div>--%>
        <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
        <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
        <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
        <script defer>
            var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
            var CompanySessionId = '<%= Session["COMPANYID"]%>';
        </script>

        <script src="../../SMViews/Utility.js" defer></script>
        <script src="../System%20Preferences/Js/CategoryRoleMapping.js"></script>
</body>

</html>
