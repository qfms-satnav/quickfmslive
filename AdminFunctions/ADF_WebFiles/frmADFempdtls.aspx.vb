Imports System.Text
Imports System.Security.Cryptography
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class frmADFempdtls
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

#Region "Variables"
    Dim strID As String
    Dim strTval As String = "fmg"
#End Region

    Private Sub page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If Request.QueryString("user") <> "" Then
                btnNext.Visible = False
                pnlChk.Visible = False

            ElseIf Request.QueryString("Eid") <> "" Then
                'btnBack.Visible = False
                btnNext.Visible = True
                pnlChk.Visible = True
            End If
        Catch ex As System.Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Deatisl", "Load", ex)
        End Try



        If Not Page.IsPostBack Then
            Try
                If Request.QueryString("user") <> "" Then
                    strID = Request.QueryString("user")
                Else
                    strID = Request.QueryString("eid")
                End If
                Dim Temp As String
                Dim ds As DataSet, i As Integer
           
                Dim sp As New SqlParameter("@dummy", SqlDbType.Int)
                sp.Value = 0

                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "AMT_GetRoleDtls_SP", sp)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim lsti As New ListItem
                    lsti.Value = ds.Tables(0).Rows(i).Item(0)
                    lsti.Text = ds.Tables(0).Rows(i).Item(1)
                    chkList.Items.Add(lsti)
                Next
                bindCheckBox()
                'strSQL = "SELECT ISNULL(AUR_KNOWN_AS,' ') Name,ISNULL(AUR_ID ,' ')," & _
                '         " ISNULL(AUR_EMAIL,' '),ISNULL(AUR_Desgn_ID,' ') ," & _
                '         " ISNULL(AUR_EXTENSION,' '),ISNULL(AUR_DIRECT_LINE,' '),ISNULL(AUR_RES_NUMBER,' ') FROM  " & Session("TENANT") & "."  & "AMT_AMANtrAUSER_VW WHERE AUR_ID LIKE '" & Trim(strID) & "'"

                'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

                Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
                sp1.Value = Trim(strID)
                ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getUserDetails", sp1)
                While ObjDR.Read
                    txtEname.Text = ObjDR.GetValue(0).ToString + " (" + ObjDR.GetValue(1).ToString + ")"

                    'txtUserId.Text = ObjDR.GetValue(1).ToString
                    txtEmail.Text = ObjDR.GetValue(2).ToString
                    Temp = ObjDR.GetValue(3).ToString

                End While
                ObjDR.Close()
                'LoadCity()
            Catch ex As System.Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
            End Try
        End If
    End Sub

    'Public Sub LoadCity()
    '    ObjSubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")

    'End Sub
    'Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
    '    ddlSelectLocation.Items.Clear()
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
    '    param(0).Value = ddlCity.SelectedItem.Value
    '    ObjSubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)


    'End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try

            'Dim str As String = ddlSelectLocation.Text
            If Request.QueryString("user") <> "" Then
                strID = Request.QueryString("user")
            Else
                strID = Request.QueryString("eid")
            End If
            Dim userid As SqlParameter = New SqlParameter("@userid", SqlDbType.NVarChar, 50)
            userid.Value = strID
            strSQL = "AMT_DelUserRole_SP"
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, strSQL, userid)
            Dim i As Integer

            For i = 0 To chkList.Items.Count - 1
                If chkList.Items(i).Selected = True Then
                    Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
                    sp1.Value = strID
                    Dim sp2 As New SqlParameter("@rol_id", SqlDbType.NVarChar, 10)
                    sp2.Value = chkList.Items(i).Value
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_InsertUserRole", sp1, sp2)
                End If
            Next
            bindCheckBox()
            lblmessage.Text = "Roles are updated to the User"
            lblmessage.Visible = True
        Catch ex As System.Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
        End Try

    End Sub

    Private Sub bindCheckBox()
        Try
            If Request.QueryString("user") <> "" Then
                strID = Request.QueryString("user")
            Else
                strID = Request.QueryString("eid")
            End If
            Dim ds As DataSet, i, j As Integer
            For i = 0 To chkList.Items.Count - 1
                'strSQL = "SELECT distinct URL_ROL_ID AS NAME FROM  " & Session("TENANT") & "."  & "USER_ROLE WHERE URL_USR_ID='" & strID & "'"
                'ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
                Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
                sp1.Value = strID
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getRoleNoforUser", sp1)
                Dim str As String = ""
                For j = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows(j).Item(0).ToString() = chkList.Items(i).Value.ToString() Then
                        chkList.Items(i).Selected = True
                    End If
                Next
            Next
        Catch ex As System.Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
        End Try

    End Sub
End Class
