<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfaddrole.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfaddrole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div>
        <table id="tb1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0" >
        <tr>
        <td>
         <asp:Label ID="lblHead" runat="server" CssClass="clsHead"  Width="95%">Masters
         <hr align="center" width="75%"/></asp:Label>
        </td>
        </tr>
        </table>
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                    align="center" border="0">
                    <tr>
                        <td>
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER">
                            <strong>&nbsp; Add Role&nbsp;</strong></td>
                        <td>
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                            &nbsp;</td>
                        <td align="center">
								<asp:panel id="pnlAddRole" runat="server" HorizontalAlign="Center" Width="95%">
									<br />
									<div align="left">
										<asp:Label id="lblMnd" runat="server" cssclass="clsNote">(*) Mandatory Fields.</asp:Label></div>
									<table id="tblAddRole" width="100%" align="center">
										<tr>
											<td class="clstblnew" width="50%" colspan="2">Add New Role</td>
										</tr>
										<tr>
											<td class="Label" width="30%">Role Acronym<font class="clsNote">*
													<asp:requiredfieldvalidator id="rfvRole" runat="server" ErrorMessage="Please Enter The Role !" ControlToValidate="txtroleAcro"
														Display="None"></asp:requiredfieldvalidator></font></td>
											<td width="70%">
												<asp:textbox id="txtroleAcro" runat="server" Width="100%" cssclass="clsTextField"></asp:textbox></td>
										</tr>
										<tr>
											<td class="Label" width="30%">Role Description</td>
											<td width="70%">
												<asp:textbox id="txtroleDesc" runat="server" Width="100%" cssclass="clsTextField"></asp:textbox></td>
										</tr>
										<tr>
											<td class="Label" width="30%">Scope<font class="clsNote">* <input id="txtHact" style="WIDTH: 23px; HEIGHT: 18px" type="hidden" size="1" name="txtHact"
														runat="server" />
													<asp:CompareValidator id="cfvScope" runat="server" ErrorMessage="Please Select The Scope !" ControlToValidate="cboScope"
														Display="None" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator></font></td>
											<td width="70%">
												<asp:DropDownList id="cboScope" runat="server" Width="100%" cssclass="clsComboBox"></asp:DropDownList></td>
										</tr>
									</table>
									<br />
									<table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1">
										<tr>
											<td align="center" width="33%">
												<asp:button id="btnSubmit" runat="server" cssclass="clsButton" Text="Submit"></asp:button>
												<asp:button id="btnUpdt" runat="server" cssclass="clsButton" Text="Update" Visible="False"></asp:button></td>
											<td align="center" width="33%"><input class="clsButton" id="btnReset" style="WIDTH: 100px; HEIGHT: 18px" type="reset"
													value="Reset" name="hreset" runat="server" /></td>
											<td align="center" width="33%"><input class="clsButton" id="btnBack" onclick="butBack_OnClick()" type="button" value="Back"
													name="hback" runat="server" /></td>
										</tr>
									</table>
									<br />
									<asp:Label id="lblErr" runat="server" cssclass="clsMessage"></asp:Label>
								
								</asp:panel></td>
						
                        
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
           
           
         
        </div>
 

</asp:Content>