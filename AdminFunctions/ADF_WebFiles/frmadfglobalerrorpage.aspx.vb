Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfglobalerrorpage
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Write("<br /><br /><br />")
        Response.Write("<Center>")
        Response.Write("<table width=80% border=2>")
        Response.Write("<tr><td>")
        Response.Write("<center>")
        Response.Write("<font face=Zurich BT size=large color=red>")
        Response.Write("<font face=Zurich BT size=4 color=red><br /> <U> ERROR </U> ")
        Response.Write("<b><br /><font face=Zurich BT size=medium color=brown>")
        Response.Write("<font face=Zurich BT size=3 color=brown><br />  This Page is Under Maintenance . Please Contact Your Administrator ")
        Response.Write("<br /><br /> Thank you for using the system. ")

        Send_Mail("", "Error Mail", Session("MSG"))


        Response.Write("<font face=arial BT size=2 color=brown><br /><br /><I>" & Request.QueryString("msg") & "</I><br /><br /></font></center></td></tr></table>")
        Response.Write("<Center>")
        Response.Write("<br /><br />")
        Response.Write("<center><input type=button value=Back onclick=""javascript:history.go(-1)""></center>")

    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal subject As String, ByVal msg As String)
        Dim obj As System.Web.Mail.SmtpMail
        Dim Mailmsg As New System.Web.Mail.MailMessage
        obj.SmtpServer = Session("serverip")
        Mailmsg.To = MailTo.Trim()
        'Mailmsg.From = "\" & "AmantraAdmin" & "\ <" & "amantraadmin@iciciprulife.com" & ">"
        Mailmsg.From = Session("uname") & "<" & Session("uemail") & ">"
        Mailmsg.Subject = subject.Trim()
        Mailmsg.Body = msg.Trim()
        Try
            'Call the send method to send the mail
            obj.Send(Mailmsg)
        Catch ex As Exception
            'Response.Write("Message:   " & ex.Message)
            Dim desc, pageURL, trace, time, mlto, subj As String
            pageURL = ex.GetBaseException.ToString
            trace = (ex.StackTrace.ToString)
            'desc = ex.InnerException.StackTrace.ToString
            mlto = Mailmsg.To.ToString
            subj = "Company Name- Satnav"
            time = getoffsetdatetime(DateTime.Now).ToString
            Dim StrQuery As String
            StrQuery = "INSERT INTO  " & Session("TENANT") & "."  & "AMT_MAIL(AMT_PAGE_URL," & _
                                     "AMT_DESC,AMT_MAIL_TO,AMT_MAIL_CC,AMT_MAIL_BCC,AMT_SUBJECT,AMT_MAIL_TIME) " & _
                                     "VALUES('" & pageURL & "'," & trace & "," & mlto & ",'ramasatyanarayana@iciciprulife.com','rajkumar@iciciprulife.com'," & subj & "," & time & ")"

            SqlHelper.ExecuteNonQuery(CommandType.Text, StrQuery)
        Finally
            Response.Write("Sorry...! Server May be down...!  Mail Will Be Sent When It Gets Activated.")
            Response.End()
        End Try
    End Sub
End Class

