<%@ Page Language="VB"  MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmPermitTenant.aspx.vb" Inherits="AdminFunctions_frmPermitTenant" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
      <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Admin Functions
      <hr align="center" width="75%"/></asp:Label>
    <br />
        <asp:Panel ID="PNLCONTAINER" runat="server" Height="96px" Width="95%">
            <br />
            <table id="table4" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="width: 63px">
                        <img alt="" id="IMG1" height="40" onclick="return IMG1_onclick()" src="../../images/table_left_top_corner.gif"
                            width="63" /></td>
                    <td background="../images/table_mid_top_bg.gif" class="tableheader" width="100%" align="left">
                        <strong><font face="Arial" size="2">Give Privileges To Tenant</font></strong></td>
                    <td align="right" valign="top" width="37">
                        <img alt="" height="40" src="../../images/table_right_top_corner.gif" width="37" /></td>
                </tr>
                <tr align="center" valign="top">
                    <td colspan="3">
                       
                      <asp:panel id="panel1" runat="server" Width="95%" >
								<p align="center"><br />
                                            <table  id="table1" cellspacing="1" cellpadding="1" width="100%"
										border="1" runat="server">		
        
        <tr align="left">
        <td colspan="4">
        <asp:GridView id="gvDetails" runat="server" Width="100%" Visible="False" AutoGenerateColumns="False">
										<Columns>
										<asp:TemplateField>
										<HeaderTemplate>
										<asp:CheckBox runat="server" ID="chkAll" AutoPostBack="true"  />
										</HeaderTemplate>
										<ItemStyle HorizontalAlign="Left" Width="4%"  />
										<ItemTemplate>									
										<asp:CheckBox runat="server" ID="chkSelect"  />
										</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="aur_id" headerText="Tenant ID">
												<headerStyle HorizontalAlign="Left" CssClass="label"></headerStyle>
												<ItemStyle HorizontalAlign="Left"  />
											</asp:BoundField>
											<asp:BoundField DataField="aur_known_as" headerText="Person Name">
												<headerStyle HorizontalAlign="Left" CssClass="label"></headerStyle>
												<ItemStyle HorizontalAlign="Left"  />
											</asp:BoundField>
											<asp:BoundField DataField="building" headerText="Building">
												<headerStyle HorizontalAlign="Left" CssClass="label"></headerStyle>
												<ItemStyle HorizontalAlign="Left"  />
											</asp:BoundField>
											<asp:BoundField DataField="aur_email" headerText="E-mail ID">
												<headerStyle HorizontalAlign="Left" CssClass="label"></headerStyle>
												<ItemStyle HorizontalAlign="Left"  />
											</asp:BoundField>
											
											<asp:TemplateField HeaderText="Extra AC Charge">
										<ItemStyle HorizontalAlign="Left"  />
										<ItemTemplate>									
										<asp:CheckBox runat="server" ID="chkSelectAC"  />
										</ItemTemplate>
										</asp:TemplateField>
										
										<asp:TemplateField HeaderText="Chiller Charge">
										<ItemStyle HorizontalAlign="Left"  />
										<ItemTemplate>									
										<asp:CheckBox runat="server" ID="chkSelectCC"  />
										</ItemTemplate>
										</asp:TemplateField>
										
										<asp:TemplateField HeaderText="Office Hrs From HH:MM">                                    
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlHr" runat="server" AutoPostBack="false" CssClass="clsComboBox">                                               
                                                <asp:ListItem Value="1">0</asp:ListItem>
                                                <asp:ListItem Value="2">1</asp:ListItem>
                                                <asp:ListItem Value="3">2</asp:ListItem>
                                                <asp:ListItem Value="4">3</asp:ListItem>
                                                <asp:ListItem Value="5">4</asp:ListItem>
                                                <asp:ListItem Value="6">5</asp:ListItem>
                                                <asp:ListItem Value="7">6</asp:ListItem>
                                                <asp:ListItem Value="8">7</asp:ListItem>
                                                <asp:ListItem Value="9">8</asp:ListItem>
                                                <asp:ListItem Value="10">9</asp:ListItem>
                                                <asp:ListItem Value="11">10</asp:ListItem>
                                                <asp:ListItem Value="12">11</asp:ListItem>
                                                <asp:ListItem Value="13">12</asp:ListItem>
                                                <asp:ListItem Value="14">13</asp:ListItem>
                                                <asp:ListItem Value="15">14</asp:ListItem>
                                                <asp:ListItem Value="16">15</asp:ListItem>
                                                <asp:ListItem Value="17">16</asp:ListItem>
                                                <asp:ListItem Value="18">17</asp:ListItem>
                                                <asp:ListItem Value="19">18</asp:ListItem>
                                                <asp:ListItem Value="20">19</asp:ListItem>
                                                <asp:ListItem Value="21">20</asp:ListItem>
                                                <asp:ListItem Value="22">21</asp:ListItem>
                                                <asp:ListItem Value="23">22</asp:ListItem>
                                                <asp:ListItem Value="24">23</asp:ListItem>
                                            </asp:DropDownList> 
                                              <asp:CompareValidator ID="cmpHr" runat="server" Operator="notequal" ValueToCompare="0" ControlToValidate="ddlHr" Display="None" ErrorMessage="Please Select Service Type!" Type="integer"></asp:CompareValidator>
                                            <asp:DropDownList ID="ddlMin" runat="server" AutoPostBack="false"
                                                CssClass="clsComboBox" >
                                                
                                                <asp:ListItem Value="1">00</asp:ListItem>
                                                <asp:ListItem Value="2">15</asp:ListItem>
                                                <asp:ListItem Value="3">30</asp:ListItem>
                                                <asp:ListItem Value="4">45</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                            <ItemStyle Width="16%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Office Hrs To HH:MM">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlHr1" runat="server" AutoPostBack="false" CssClass="clsComboBox">
                                                <asp:ListItem Value="1">0</asp:ListItem>
                                                <asp:ListItem Value="2">1</asp:ListItem>
                                                <asp:ListItem Value="3">2</asp:ListItem>
                                                <asp:ListItem Value="4">3</asp:ListItem>
                                                <asp:ListItem Value="5">4</asp:ListItem>
                                                <asp:ListItem Value="6">5</asp:ListItem>
                                                <asp:ListItem Value="7">6</asp:ListItem>
                                                <asp:ListItem Value="8">7</asp:ListItem>
                                                <asp:ListItem Value="9">8</asp:ListItem>
                                                <asp:ListItem Value="10">9</asp:ListItem>
                                                <asp:ListItem Value="11">10</asp:ListItem>
                                                <asp:ListItem Value="12">11</asp:ListItem>
                                                <asp:ListItem Value="13">12</asp:ListItem>
                                                <asp:ListItem Value="14">13</asp:ListItem>
                                                <asp:ListItem Value="15">14</asp:ListItem>
                                                <asp:ListItem Value="16">15</asp:ListItem>
                                                <asp:ListItem Value="17">16</asp:ListItem>
                                                <asp:ListItem Value="18">17</asp:ListItem>
                                                <asp:ListItem Value="19">18</asp:ListItem>
                                                <asp:ListItem Value="20">19</asp:ListItem>
                                                <asp:ListItem Value="21">20</asp:ListItem>
                                                <asp:ListItem Value="22">21</asp:ListItem>
                                                <asp:ListItem Value="23">22</asp:ListItem>
                                                <asp:ListItem Value="24">23</asp:ListItem>
                                            </asp:DropDownList>
                                             <asp:CompareValidator ID="cmpHr1" runat="server" Operator="notequal" ValueToCompare="0" ControlToValidate="ddlHr1" Display="None" ErrorMessage="Please Select Service Type!" Type="integer"></asp:CompareValidator>
                                            <asp:DropDownList ID="ddlMin1" runat="server" AutoPostBack="false"
                                                CssClass="clsComboBox" >
                                                <asp:ListItem Value="1">00</asp:ListItem>
                                                <asp:ListItem Value="2">15</asp:ListItem>
                                                <asp:ListItem Value="3">30</asp:ListItem>
                                                <asp:ListItem Value="4">45</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle Width="16%" />
                                    </asp:TemplateField>
                                    
											</Columns>
                                                </asp:GridView>
        </td>
           </tr>
        <tr id="tr1" runat="server">
        	<td align="center" colspan="4">
        	<asp:button id="btnSubmit" runat="server" Width="76px" Text="Submit" CssClass="button">
        	</asp:button>
        	</td>
        </tr> <tr>
        	<td align="center" colspan="4">
       
                                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                                    </td></tr> </table> 
                            </asp:Panel> 
                       
                       
                       
                       
                       
                     </td>
                </tr>
                <tr>
                    <td style="height: 18px">
                        <img alt="" height="16" src="../../images/table_left_bot_corner.gif" width="63" /></td>
                    <td height="18" style="height: 18px">
                        <img alt="" height="16" src="../../images/table_mid_bot_bg.gif" width="25" /></td>
                    <td style="height: 18px">
                        <img alt="" height="16" src="../../images/table_right_bot_corner.gif" width="37" /></td>
                </tr>
            </table>
        </asp:Panel>
      
    </div>
    </asp:Content>