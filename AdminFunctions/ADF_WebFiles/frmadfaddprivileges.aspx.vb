Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Exception
Imports System.Web

Partial Class AdminFunctions_ADF_WebFiles_frmadfaddprivileges
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim strRolename As String
    Dim ObjComm As New SqlCommand()
    Dim param As SqlParameter()
#End Region
    Private Sub page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            ' strSQL = "SELECT ROL_ACRONYM,rol_description FROM  " & Session("TENANT") & "."  & "ROLE WHERE ROL_ID=" & Request.QueryString("rid")
            Dim sp1 As New SqlParameter("@rol_id", SqlDbType.NVarChar, 10)
            sp1.Value = Request.QueryString("rid")
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getRoleDetails", sp1)
            While ObjDR.Read()
                strRolename = (ObjDR.GetValue(1) & "(") + ObjDR.GetValue(0) & ")"
            End While
            lblSrole.Text = strRolename
            ObjDR.Close()
        Catch ex As System.Exception
            ' lblError.Text = ex.Message
            'ErrorLog.LogError(ex)
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Privillages", "Load", ex)
        End Try

        Response.Clear()
        Response.Write("<form name=f1 method=post action='frmADFprivilegessubmit.aspx'>")
        'Response.Write("<script>window.loaction.reload();</script>"); 

        Response.Write("<center><table border=1 class='TABLE' align=center cellspacing=0 cellpadding=0 width=95% >")
        Try
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@Rolid", SqlDbType.Int, 4)
            param(0).Value = Request.QueryString("rid").ToString()
            Dim str As String = String.Empty
            str = "AMT_GETPRIVILEGE_SP_NEW"
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, str, param)
            While ObjDR.Read()
                Dim value As String = (((ObjDR("CLS_ID").ToString() & "_") + ObjDR("CLS_PARENT_ID").ToString() & "_") + ObjDR("p_parent").ToString() & "_") + ObjDR("P_P_parent").ToString() & "_"
                If ObjDR("CLS_LEVEL").ToString() = "1" Then
                    Response.Write("<tr><td bgcolor='#6699CC'><img src='../../Images/BACKGROUND.jpg' height='2' width='100%'></td></tr>")
                    Response.Write("<tr>")
                    Dim cls_level As String = ObjDR("CLS_LEVEL").ToString()
                    Dim cls_id As Integer = Integer.Parse(ObjDR("CLS_ID").ToString())
                End If
                If (ObjDR("CLS_LEVEL").ToString() = "1") OrElse (ObjDR("CLS_LEVEL").ToString() = "2") OrElse (ObjDR("CLS_LEVEL").ToString() = "3") Then
                    Dim mystr As String
                    mystr = "<td>" & Filler(ObjDR("CLS_LEVEL")) & "<input type='checkbox' id='" & ObjDR("CLS_ID") & "' name='chkPrv' onClick=javascript:CheckAll('" & ObjDR("CLS_ID").ToString() & "') value='" & value & "' "
                    Response.Write(mystr)
                End If

                If ObjDR("CLS_LEVEL").ToString() = "4" Then

                    Response.Write("<td>" & Filler(ObjDR("CLS_LEVEL")) & "<input type='checkbox' id='" & ObjDR("CLS_ID") & "' name='chkPrv' onClick=javascript:CheckParent('" & ObjDR("CLS_ID") & "') value='" + value & "' ")
                End If

                If ObjDR("rol_prv").ToString() = "Y" Then
                    Response.Write(" Checked ")
                End If
                Response.Write(">")
                If ObjDR("SER_ID") IsNot Nothing Then
                    Response.Write("<b>" & ObjDR("CLS_DESCRIpTION").ToString() & "</b>")
                Else
                    Response.Write(ObjDR("CLS_DESCRIpTION").ToString())
                End If
                Response.Write("</td></tr>")
            End While
            ObjDR.Close()
        Catch ex As System.Exception
            ' lblError.Text = "Invalid Data"; 
            ' Common.fn_ErrorLog(1, ex.Message, "General Exception", "Admin_frmadfaddprivileges : Page_Load", "frmadfaddprivileges.aspx.cs", Session["user"].ToString()); 
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Privillages", "Load", ex)
        End Try

        Response.Write("<table width=95% border=1 cellpadding=0 cellspacing=0 align=center><tr><td align=center>")
        Response.Write("<button TYpE=Submit class=button name=btnSubmit >Submit</button></td>")
        Response.Write("<td align=center><button TYpE=Reset class=button name=btnReset >Reset</button></td>")
        Response.Write("<td align=center><button TYpE=Reset class=button name=btnBack  onClick=javascript:window.history.back(-1) accessKey=B>Back</button>")
        Response.Write("<input type=hidden name=rid value=" & Request.QueryString("rid") & ">")
        Response.Write("</td></tr></table><br><br>")
        Response.Write("</center></form>")
    End Sub
    Function Filler(ByVal nTimes)
        'Filler = "<img src='../../Images/filler.gif' height='5' width='" & (CInt(nTimes) * 15) & "'>"
        Filler = "<img height='5' width='" & (CInt(nTimes) * 15) & "'>"
    End Function
End Class
