﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>
<script runat="server">


</script>

<html data-ng-app="QuickFMS" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                orientation: 'bottom'
            });
        };
    </script>
</head>
<body data-ng-controller="UserRoleMappingController" class="amantra">
    <div class="animsition">
        <div class="al-content" style="height: 600px;">
            <div class="widgets">
                <h3>User & Role Mapping</h3>
            </div>
            <div class="card">
                <div data-ng-show="ViewUploption">
                    <div class="clearfix">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <form role="form" id="FileUsrUpl" name="FileUsrUpl" data-valid-submit="UploadFile()" novalidate>
                                <div class="col-md-4 col-sm-12 col-xs-12 mt-4">
                                    <%--<label>Upload File </label>--%>
                                    <div>
                                        <input type="file" name="UPLFILE" id="FileUpl">
                                        <label style="padding: 5px 10px"></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 mt-4">
                                    <button type="submit" id="btnUpload" class="btn btn-primary">Upload</button>
                                    <%--<input type="file" name="UPLFILE" id="FileUpl">--%>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12 mt-4">
                            <a class="btn btn-primary" href="../../Masters/Mas_Webfiles/User_Role_Mapping_Template.xlsx" style="margin-right: 20px">Click here to download template</a>
                        
                            <a class="btn btn-primary pull-right" id="HyperLink11" href="../../Masters/Mas_Webfiles/frmNewUser.aspx"><i class="fa fa-user pr-2"></i>Add New User</a>
                        </div>
                    </div>
                </div>
                <br />
                <div data-ng-show="Viewstatus==0">
                    <div class="row align-items-center mb-3">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-transparent text-light"><i class="fa fa-search"></i></span>
                                </span>
                                <input type="text" id="filtertxt" class="form-control" placeholder="Filter by any..." />
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="d-flex align-items-center column-gap-5">
                                <label class="mb-0">Total Employee Count: </label>
                                <a href="#"><span ng-bind="name" class="font-weight-bold"></span></a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="pull-right">
                                <a data-ng-click="GenReport(CurrentProfileEmp,'xlsx')" data-ng-show="Viewstatus==0"><i id="I2" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-3x"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 400px; width: auto"></div>
                        </div>
                    </div>
                </div>
                <div data-ng-show="Viewstatus==1">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="form-group-name text-right">
                                <span class="text-danger">** Select to auto fill the data</span>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col-md-12">
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel d-flex align-items-center justify-content-around flex-wrap">
                                    <div class="stepwizard-step" data-ng-show="EmployeeDetails">
                                        <a href="#step-1" id="stp1" type="button" class="btn btn-primary">Employee Details</a>
                                    </div>
                                    <div class="stepwizard-step" data-ng-show="RoleAssignment">
                                        <a href="#step-2" id="stp2" type="button" ng-click='LoadRMdetails_Step2()' class="btn btn-primary">Role Assignment</a>
                                    </div>
                                    <div class="stepwizard-step" data-ng-show="Mapping">
                                        <a href="#step-3" id="stp3" type="button" ng-click="LoadMappingDtls_Step3()" class="btn btn-primary">Mapping</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <form role="form" id="EmpDetails" name="frmEmpDetails">
                                <input type="hidden" id="AUR_ID_NEW" class="form-control">
                                <div class="setup-content" id="step-1">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>User ID <span class="text-danger">*</span></label>
                                                <input id="UserId" type="text" class="form-control" data-ng-model="CurrentProfileEmp.NEW_AUR_ID" name="c" required />
                                                <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.NEW_AUR_ID.$invalid">Please enter Employee ID</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" id="Username" class="form-control" data-ng-model="CurrentProfileEmp.AUR_KNOWN_AS" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.AUR_EMAIL.$invalid}">
                                                    <input type="text" class="form-control" data-ng-model="CurrentProfileEmp.AUR_EMAIL" name="AUR_EMAIL" ng-pattern="/^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z-.]+\.[a-zA-Z.]{2,5}$/" />
                                                    <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.AUR_EMAIL.$invalid" style="color: red">Please enter a valid email address</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Phone No</label>
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.AUR_RES_NUMBER.$invalid}">
                                                    <input type="text" class="form-control" data-ng-model="CurrentProfileEmp.AUR_RES_NUMBER" maxlength="11" minlength="10" name="AUR_RES_NUMBER" ng-pattern="/^[0-9]+$/" />
                                                    <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.AUR_RES_NUMBER.$invalid" style="color: red">Please enter a 10-digit mobile number</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Date Of Birth</label>
                                                <div class="input-group date" id='frmdate'>
                                                    <%--<div class="input-group-text bg-transparent">
                                                        <%--<i class="fa fa-calendar-o" onclick="setup('frmdate')"></i>
                                                    </div>--%>
                                                    <input id="txtreqfrmdate" class="form-control" data-ng-model="CurrentProfileEmp.AUR_DOB" name="AUR_DOB" type="date" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Date Of Joining</label>
                                                <div class="input-group date" id='doj'>
                                                   <%-- <div class="input-group-text bg-transparent">
                                                        <i class="fa fa-calendar-o" onclick="setup('doj')"></i>
                                                    </div>--%>
                                                    <input class="form-control" id="txtdoj" data-ng-model="CurrentProfileEmp.AUR_DOJ" name="AUR_DOJ" type="date" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <select ng-show="false" data-ng-model="CurrentProfileEmp.AUR_COUNTRY" id="ddlcountry" class="form-control" name="AUR_COUNTRY" data-live-search="true">
                                                    <option value="">--Select--</option>
                                                    <option data-ng-repeat="cnt in Countrylst" value="{{cnt.CNY_CODE}}">{{cnt.CNY_NAME}}</option>
                                                </select>
                                                <input type="password" class="form-control" data-ng-model="CurrentProfileEmp.AUR_PASSWORD" name="AUR_PASSWORD" />
                                                <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.AUR_PASSWORD.$invalid">Please enter Password</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.AUR_REPORTING_TO.$invalid}">
                                                <div class="form-group">
                                                    <label>Reporting To</label>

                                                    <div angucomplete-alt
                                                        id="ex7"
                                                        placeholder="Search Employee"
                                                        pause="500"
                                                        selected-object="selectedEmp"
                                                        remote-url="../../../api/Utility/GetEmployeeNameAndID"
                                                        remote-url-request-formatter="remoteUrlRequestFn"
                                                        remote-url-data-field="items"
                                                        title-field="NAME"
                                                        minlength="2"
                                                        input-class="form-control"
                                                        match-class="highlight"
                                                        required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Extension No</label>
                                                <input type="text" class="form-control" data-ng-model="CurrentProfileEmp.AUR_EXTENSION" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.DSG_CODE.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.DSG_CODE" class="form-control" id="DSG"
                                                        name="DSG_CODE" cssclass="form-control selectpicker" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="dsg in designationddl" value="{{dsg.DSG_CODE}}">{{dsg.DSG_NAME}}</option>
                                                    </select>
                                                    <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.DSG.$invalid">Please Select Designation</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>{{BsmDet.Parent}}</label>
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.VERTICAL.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.VERTICAL" class="form-control" id="VERTICAL"
                                                        name="VER_CODE" cssclass="form-control selectpicker" data-ng-change="VerChange(CurrentProfileEmp.VERTICAL)" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="ver in verticalddl" value="{{ver.VER_CODE}}">{{ver.VER_NAME}}</option>
                                                    </select>
                                                    <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.VERTICAL.$invalid">Please Select Vertical</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>{{BsmDet.Child}}</label>
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.COSTCENTER.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.COSTCENTER" class="form-control" id="COSTCENTER"
                                                        name="Cost_Center_Code" cssclass="form-control selectpicker" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="cos in costcenterddl" value="{{cos.Cost_Center_Code}}">{{cos.Cost_Center_Name}}</option>
                                                    </select>
                                                    <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.COSTCENTER.$invalid" style="color: red">Please Select Cost center</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select data-ng-model="CurrentProfileEmp.AUR_STA_ID" class="form-control" id="ddlusrStatus" data-ng-change="Alert()"
                                                    name="AUR_STA_ID" data-live-search="true">
                                                    <option data-ng-repeat="sts in usrStatus" value="{{sts.value}}">{{sts.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Location</label>
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.Location.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.LOCATION" class="form-control" id="LOCATION"
                                                        name="LCM_CODE" cssclass="form-control selectpicker" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="lcm in locationddl" value="{{lcm.LCM_CODE}}">{{lcm.LCM_NAME}}</option>
                                                    </select>
                                                    <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.Location.$invalid" style="color: red">Please Select Location</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.EmployeeType.$invalid}">
                                                <label class="control-label">Employee Grade</label>

                                                <div isteven-multi-select data-input-model="EmployeeType" data-output-model="CurrentProfileEmp.EmployeeType" data-button-label="icon AUR_GRADE" data-item-label="icon AUR_GRADE maker"
                                                    data-on-select-none="EmployeeType()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                                <input type="text" data-ng-model="CurrentProfileEmp.EmployeeType" name="AUR_GRADE" style="display: none" required="" />
                                                <span class="text-danger" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.EmployeeType.$invalid" style="color: red">Please select the employee type</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <textarea rows="1" cols="10" id="UserRemarks" class="form-control" data-ng-model="CurrentProfileEmp.AUR_EMP_REMARKS"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-right">
                                                <button type="button" class="btn btn-primary" data-ng-click="back()">Back</button>
                                                <button type="button" class="btn btn-primary" ng-show="SAVE" data-ng-click="SaveEmpDetailsonly()">Save</button>
                                                <button type="submit" class="btn btn-primary nextBtn" ng-show="SAVE_Continue" data-ng-click="SaveEmpDetails()">Save & Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form role="form" id="frmMRdetails" name="frmMREmp" data-valid-submit="SaveRMDetls()" novalidate>
                                <div class="setup-content" id="step-2">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Module<span class="text-danger">*</span></label>
                                                <select id="module" data-ng-model="Currentmodule.MOD_NAME" data-ng-change="accessbymodule(Currentmodule.MOD_NAME)"
                                                    class="form-control" name="MOD_NAME" data-live-search="true">
                                                    <option value="ALL" selected>ALL</option>
                                                    <option data-ng-repeat="mod in modules" value="{{mod.MOD_NAME}}">{{mod.MOD_NAME}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12" data-ng-disabled="!RolStatus">
                                            <div class="form-group">
                                                <label>Access Level<span class="text-danger">*</span></label>
                                                <div class="user_maping_wrap" data-ng-class="{'has-error': frmMREmp.$submitted && frmMREmp.AUR_RES_NUMBER.$invalid}">
                                                    <div data-ng-repeat="obj in acceslevel">
                                                        <div class="col-md-3 col-sm-12 col-xs-3">
                                                            <label>
                                                                <input type="checkbox" data-ng-model="obj.isChecked" ng-value="{{obj.isChecked}}" ng-change="Rolechk()" />
                                                                {{obj.ROL_DESCRIPTION}}                                                                                 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--{{(acceslevel | filter: {isChecked :  true}).length}}--%>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10 col-sm-12 col-xs-12 m-4" data-ng-show="SpaceVisible == true">
                                            <div class="form-group">
                                                <label>Map level access</label>
                                                <div class="radio radio_button_wrap">
                                                    <div class="form-check form-check-inline margin_wrap">
                                                        <input class="form-check-input" type="radio" name="userdetails" id="userdetail-radio-1" value="Y">
                                                        <label class="form-check-label" for="userdetail-radio-1">View & Edit</label>
                                                    </div>
                                                    <br />
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="userdetails" id="userdetail-radio-2" value="N">
                                                        <label class="form-check-label" for="userdetail-radio-2">Only View</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <span class="text-danger" data-ng-show="RolValidation == false">Please select at least one role to assign.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-right">
                                                <button type="button" value="Back" class="btn btn-primary" data-ng-click="back()">Back</button>
                                                <button type="submit" id="btnsave" class="btn btn-primary nextBtn" data-ng-disabled="RolValidation == false">Save & Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form role="form" id="frmMappingDetails" name="frmMappingEmp" data-valid-submit="SaveMappingDetls()" novalidate>
                                <div class="setup-content" id="step-3">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.CNY_NAME.$invalid}">
                                                <label for="txtcode">Country <span class="text-danger">*</span></label>
                                                <div isteven-multi-select data-input-model="Countrylst" data-output-model="UserRole.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                                    data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.CNY_NAME.$invalid">Please Select Country </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.CTY_NAME.$invalid}">
                                                <label for="txtcode">City <span class="text-danger">**</span></label>
                                                <div isteven-multi-select data-input-model="Citylst" data-output-model="UserRole.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                                    data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.CTY_NAME.$invalid">Please Select City </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.LCM_NAME.$invalid}">
                                                <label for="txtcode">Location <span class="text-danger">**</span></label>
                                                <div isteven-multi-select data-input-model="Locationlst" data-output-model="UserRole.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                    data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.LCM_NAME.$invalid">Please Select location </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.TWR_NAME.$invalid}">
                                                <label for="txtcode">Tower <span class="text-danger">**</span></label>
                                                <div isteven-multi-select data-input-model="Towerlist" data-output-model="UserRole.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                                    data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.TWR_NAME.$invalid">Please Select Tower </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.FLR_NAME.$invalid}">
                                                <label for="txtcode">Floor <span class="text-danger">**</span></label>
                                                <div isteven-multi-select data-input-model="Floorlist" data-output-model="UserRole.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                                    data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.FLR_NAME.$invalid">Please Select Floor </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12" data-ng-show="Currentmodule.PCENTITY">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.PE_NAME.$invalid}">
                                                <label for="txtcode">Parent Entity<span class="text-danger">*</span></label>
                                                <div isteven-multi-select data-input-model="Parentlist" data-output-model="UserRole.selectedParentEntity" data-button-label="icon PE_NAME" data-item-label="icon PE_NAME maker"
                                                    data-on-item-click="ParentEntityChange()" data-on-select-all="ParentEntityChangeAll()" data-on-select-none="ParentEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedParentEntity" name="PE_NAME" style="display: none" ng-required="Currentmodule.PCENTITY" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.PE_NAME.$invalid">Please Select Parent Entity </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="showhide" data-ng-show="Currentmodule.VERT_COST">
                                        <div class="col-md-4 col-sm-12 col-xs-12" data-ng-show="Currentmodule.PCENTITY">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.CHE_NAME.$invalid}">
                                                <label for="txtcode">Child Entity  <span class="text-danger">**</span></label>
                                                <div isteven-multi-select data-input-model="Childlist" data-output-model="UserRole.selectedChildEntity" data-button-label="icon CHE_NAME" data-item-label="icon CHE_NAME maker"
                                                    data-on-item-click="ChildEntityChange()" data-on-select-all="ChildEntityChangeAll()" data-on-select-none="ChildEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedChildEntity" name="CHE_NAME" style="display: none" ng-required="Currentmodule.PCENTITY" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.CHE_NAME.$invalid">Please Select Child Entity </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.VER_NAME.$invalid}">
                                                <label for="txtcode">{{BsmDet.Parent}} <span class="text-danger">*</span></label>
                                                <div isteven-multi-select data-input-model="Verticallist" data-output-model="UserRole.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                                    data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedVerticals" name="VER_NAME" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.VER_NAME.$invalid">Please Select  {{BsmDet.Parent |lowercase}}  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.Cost_Center_Name.$invalid}">
                                                <label for="txtcode">{{BsmDet.Child}}  <span class="text-danger">**</span></label>
                                                <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="UserRole.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                                    data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCostcenter" name="Cost_Center_Name" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                <span class="text-danger" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.Cost_Center_Name.$invalid">Please Select {{BsmDet.Child |lowercase}} </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-right">
                                                <button type="button" class="btn btn-primary" data-ng-click="back()">Back</button>
                                                <button type="submit" class="btn btn-primary">Finish</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div data-ng-show="Viewstatus==2">
                    <div class="row mb-3">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-transparent text-light"><i class="fa fa-search"></i></span>
                                </span>
                                <input type="text" id="Text1" class="form-control border-left-0" placeholder="Filter by any..." />
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div data-ag-grid="gridOptions1" class="ag-blue" style="height: 400px; width: auto"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%-- <div data-ng-show="Viewstatus==1">
                <div class="panel-body">
                    <div class="container">
                        <form role="form" name="frmMappingEmp" data-valid-submit="SaveMappingDetls()" novalidate>
                            <div class="row setup-content">
                                <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                <input type="submit" class="btn btn-primary pull-right" value="Finish" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>--%>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/root.js"></script>
    <script defer src="//cdnjs.cloudflare.com/ajax/libs/angular-multi-select/4.0.0/isteven-multi-select.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt", "cp.ngConfirm"]);
        var UserEdit = '<%=Session("USER_EDIT")%>'
        FormWizard();
        //FORM WIZARD END JS

        function FormWizard() {
            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-white btn-outline-primary');
                    $item.removeClass('btn-white btn-outline-primary').addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function () {

                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        };
    </script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../System%20Preferences/Js/UserRoleMapping.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js" defer></script>
    <%--<script src="../System%20Preferences/Js/UserRoleMapping.min.js" defer></script>--%>
</body>
</html>


