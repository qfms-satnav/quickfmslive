Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_frmADFSearch1
    Inherits System.Web.UI.page
    Dim strRID As String
    Dim strUID As String
    Dim strEmpId As String
    Dim arrTEmployees As Array

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            gvEmp.PageIndex = 0
            databindrc()
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
        End Try

    End Sub


    Protected Sub page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                databindrc()
                ' ViewState("RefUrl") = Request.UrlReferrer.ToString()

            End If
            If Page.IsPostBack Then
                strRID = Session("rid")
                strUID = Session("uid")
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
        End Try
    End Sub

    Protected Sub lnkbtnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnsearch.Click
        Try
            Dim strUrl As String = "frmADFemployeesearch.aspx"
            Response.Redirect(strUrl, False)
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
        End Try


    End Sub
    Private Sub databindrc()
        Try
            lblMsg.Visible = True
            gvEmp.Visible = True
            gvEmp.Columns(4).Visible = True

            strEmpId = ""

            If Trim(txtEmployee.Text) <> "" And Trim(txtEmployee.Text) <> "'" Then
                arrTEmployees = Split(txtEmployee.Text, ",")
                Dim iEmp As Integer
                For iEmp = LBound(arrTEmployees) To UBound(arrTEmployees)
                    strEmpId = strEmpId & arrTEmployees(iEmp) & ";"
                Next
                If strEmpId.Length > 0 Then
                    strEmpId = strEmpId.Remove(strEmpId.Length - 1, 1)
                End If
            End If

            Dim Ds As New DataSet
            Dim arParms() As SqlParameter = New SqlParameter(0) {}
            arParms(0) = New SqlParameter("@aurid", SqlDbType.NVarChar, Len(strEmpId))
            arParms(0).Value = strEmpId

            If Trim(txtEmployee.Text) <> "" Then
                Ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "AMT_UserDtls_SP", arParms)
            Else
                Ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "AMT_GetUserDtls_SP")
            End If
            gvEmp.DataSource = Ds
            If Ds.Tables(0).Rows.Count = 0 Then
                lblEmp.Visible = True
                gvEmp.Visible = False
            Else
                gvEmp.Visible = True
                lblEmp.Visible = False
                gvEmp.DataBind()
                Ds.Dispose()
                Dim i, j As Integer
                gvEmp.Columns(4).Visible = True
                For i = 0 To gvEmp.Rows.Count - 1
                    gvEmp.Columns(4).Visible = True

                    'strSQL = "SELECT DISTINCT (SELECT ROL_DESCRIpTION FROM  " & Session("TENANT") & "."  & "ROLE WHERE ROL_ID=URL_ROL_ID) AS NAME FROM  " & Session("TENANT") & "."  & "USER_ROLE WHERE URL_USR_ID='" & gvEmp.Rows(i).cells(4).Text & "'"
                    'Ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
                    Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
                    sp1.Value = gvEmp.Rows(i).Cells(4).Text
                    Ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_GetRoleforUser", sp1)
                    Dim gvRoles As GridView = CType(gvEmp.Rows(i).FindControl("gvRoles"), GridView)
                    gvRoles.DataSource = Ds.Tables(0)
                    gvRoles.DataBind()
                    'Dim str As String = ""
                    'For j = 0 To Ds.Tables(0).Rows.Count - 1
                    '    str = str & Ds.Tables(0).Rows(j).Item(0) & "<br>"
                    'Next
                    'gvEmp.Rows(i).cells(2).Text = str
                Next
            End If
            gvEmp.Columns(4).Visible = False
        Catch ex As Exception
            gvEmp.Columns(4).Visible = False
        End Try
    End Sub
    <WebMethod()> _
    <System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GetEmployeeList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Dim cKey As String = ""
        If count = 0 Then
            count = 10
        End If
        Dim s As String = prefixText
        Dim dtEle As New DataTable()
        'strSQL = "select AUR_ID from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID like '" & prefixText & "%' order by AUR_ID"
        'dtEle = SqlHelper.ExecuteDatatable(CommandType.Text, strSQL)
        Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
        sp1.Value = prefixText
        dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_GetUsers", sp1)
        Dim dtcount As Integer = CInt(dtEle.Rows.Count)
        'count = dtcount
        If dtcount <> 0 Then
            Dim items As New List(Of String)(count)
            Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
            For i As Integer = 0 To total - 1
                Dim dr As DataRow = dtEle.Rows(i)
                items.Add(dr(0))
            Next
            Return items.ToArray()
        Else
            Return New String(-1) {}
        End If
    End Function

    Protected Sub gvEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmp.PageIndexChanging
        Try
            gvEmp.Columns(4).Visible = True
            gvEmp.PageIndex = e.NewPageIndex
            databindrc()
            gvEmp.Columns(4).Visible = False
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "User Search", "Load", ex)
        End Try


    End Sub
End Class