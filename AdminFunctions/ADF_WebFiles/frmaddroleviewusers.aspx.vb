﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports Npgsql
Partial Class AdminFunctions_ADF_WebFiles_frmaddroleviewusers
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        BindGrid()
        'If ds.Tables.Count > 0 Then
        '    Response.Write("<LINK href=""../../../Scripts/styWSP.cs"" rel=stylesheet>")
        '    Response.Write("<Title>A-Mantra</title>")
        '    Response.Write("<body leftmargin=10>")
        '    Response.Write("<table align=center cellSpacing=0 cellPadding=0 border=0 width=100%>")
        '    Response.Write("<br>")
        '    Response.Write("<br>")
        '    Response.Write("<br>")
        '    Response.Write("<tr align=center>")
        '    Response.Write("<td bgcolor=#4863A0><b><u>Employee Details</u></b></td>")
        '    Response.Write("</tr>")
        '    Response.Write("</table>")
        '    Response.Write("<br>")
        '    Response.Write("<table cellpadding=2 cellspacing=0 border=1 align=center width=300>")
        '    Response.Write("<tr><td valign><b>Employee Id : </b></td><td valign><b>Employee Name : </b></td></tr>")
        '    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

        '        Response.Write("<tr><td> " & ds.Tables(0).Rows(i).Item("AUR_ID").ToString & "<td> " & ds.Tables(0).Rows(i).Item("AUR_KNOWN_AS").ToString & "</td></tr>")

        '    Next
        '    Response.Write("</table>")
        '    Response.Write("<br>")
        '    Response.Write("<table align=center>")
        '    'Response.Write("<tr>")
        '    'Response.Write("<td style=""CURSOR: hand;""><center><img src=""~/Images/close.jpg"" onClick=""javascript:top.window.close();""></center></td>")
        '    'Response.Write("</tr>")
        '    Response.Write("</table>")
        'End If
    End Sub
    Private Sub bindgrid()
        Dim rol_id As String = Request.QueryString("rol_id")

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_USERSBY_ROLID")
        sp.Command.AddParameter("@ROL_ID", Convert.ToInt16(rol_id), DbType.Int16)
        'sp.Command.AddParameter("@S", 1, DbType.Int16)
        'sp.Command.AddParameter("@E", 10, DbType.Int16)
        Dim ds As New DataSet
        'ds = sp.GetDataSet()
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()
    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If txtEmpId.Text <> "" Then
            Dim rol_id As String = Request.QueryString("rol_id")
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_USERSBY_ROLIDEMPID")
            sp.Command.AddParameter("@ROL_ID", Convert.ToInt16(rol_id), DbType.Int16)
            sp.Command.AddParameter("@AUR_ID", txtEmpId.Text, DbType.String)
            'sp.Command.AddParameter("@E", 10, DbType.Int16)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            gvItem.DataSource = ds
            gvItem.DataBind()

        Else
            'Response.Write("Please enter Employee Id")
        End If
    End Sub
End Class
