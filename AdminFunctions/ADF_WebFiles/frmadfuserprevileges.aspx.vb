Imports System.Data
Imports System.Data.SqlClient
'Imports ADODB
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfuserprevileges
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim uid, strRoldIds
    Dim rsRoles As SqlDataReader
    Dim rsRoles1 As SqlDataReader
    Dim strSQLRoles, strSQLSelectRoleMap, strSQLInsertRoleMap
    Dim strUserid, strSQL As String
    Dim iCount    ' counter
    Dim iScope    ' counter
    Dim arrRole    ' array to hold role ids
    Dim arrScopeIds   ' to store the scope Ids
    'ObjCon.ConnectionString = conSql
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            'Response.Write(strSQLSelectRoleMap)

            uid = Session("uid")
            If Request.QueryString("UserId") > "" Then
                strUserid = Request.QueryString("UserId")
            Else
                strUserid = Request.form("UserId")
            End If

            ' self post
            If (Request.QueryString("UserId") <> "" And Request.QueryString("RoleId") <> "") Then
                Dim intRoleId, intScopeMapId
                intRoleId = Request.QueryString("RoleId")
                intScopeMapId = Request.QueryString("ScopeMapId")
                Call DeleteUserRole(strUserid, intRoleId, intScopeMapId)
            End If


            If Request.form("btnMapRoleSubmit") > "" Then
                arrRole = Split(Request.form("RoleIds"), ",")

                ' loop on the role present on the page
                For iCount = LBound(arrRole) To UBound(arrRole)

                    If UCase(Request.form(arrRole(iCount))) <> "NULL" Then
                        arrScopeIds = Split(Request.form(arrRole(iCount)), ",")

                        ' loop on the scope(s) for that role
                        ' also building # seperated SQL insert query

                        For iScope = LBound(arrScopeIds) To UBound(arrScopeIds)
                            If UCase(arrScopeIds(iScope)) <> "" Then
                                Dim flag
                                flag = 0

                                ' check for duplicate
                                strSQLSelectRoleMap = "SELECT *  FROM  " & Session("TENANT") & "."  & "User_Role" & _
                                                      " WHERE Url_USR_Id='" & strUserid & "' AND Url_Rol_Id = " & arrRole(iCount) & " AND Url_ScopeMap_Id = " & arrScopeIds(iScope)


                                strSQL = strSQLSelectRoleMap
                                rsRoles = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

                                If Not rsRoles.Read() Then
                                    flag = 1
                                End If

                                rsRoles.Close()


                                If (flag = 1) Then
                                    strSQLInsertRoleMap = "INSERT INTO  " & Session("TENANT") & "."  & "User_Role (Url_USR_Id,Url_Rol_Id,Url_ScopeMap_Id) VALUES ('" & strUserid & "'," & arrRole(iCount) & "," & arrScopeIds(iScope) & ")"
                                    strSQL = strSQLInsertRoleMap
                                    ' ObjComm.ExecuteNonQuery()
                                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)

                                End If
                            End If
                        Next
                    End If

                Next

                ' free the array space
                Erase arrScopeIds
            End If

            strSQLRoles = "SELECT Rol_Id,Rol_Description,Rol_Scp_Id FROM  " & Session("TENANT") & "."  & "Role WHERE ROL_ACTIVE = 'Y' AND ROL_SCP_ID IS NOT NULL ORDER BY ROL_DESCRIPTION"

            strSQL = strSQLRoles
            rsRoles = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

            Response.Write("<form method=post action=frmADFuserprevileges.aspx name=frmMapRoles><br /><p class=clsHead align=center>Admin Functions<hr align=center width=70%></p><table align=center border=1 style=""Z-INDEX: 103;  BORDER-COLLAPSE: collapse;"" cellspacing=0 cellpadding=0 border=1 width=95%% ><tr><td class=""clstblnew"" colSpan=2 width=50%>Assign Scope</td></tr>")

            Do While rsRoles.Read
                Response.Write("<tr><td class=clsLabel>" & rsRoles.GetValue(1) & "</td>")
                Call PopulateScopeData(rsRoles.GetValue(2), rsRoles.GetValue(0))
                Response.Write("</tr>" & vbCrLf)
            Loop
            Response.Write("<tr><td align=center><Button TYPE=Submit name=btnMapRoleSubmit class=clsButton>Assign Roles</Span></button>")
            Response.Write("</td><td align=center><Button TYPE=Back name=cmd_back onclick=history.back(-1) class=clsButton>Back</button>")
            Response.Write("</td></tr></table>")
            rsRoles.Close()


            'Response.Write("<table style=""Z-INDEX: 103;  BORDER-COLLAPSE: collapse;"" align=center border=1 cellspacing=0 cellpadding=0 width=95%>")
            'Response.Write("<tr><td align=center width=200><Button TYPE=Submit name=btnMapRoleSubmit class=clsButton>Assign Roles</Span></button>")
            'Response.Write("</td><td align=center width=400><Button TYPE=Back name=cmd_back onclick=history.back(-1) class=clsButton>Back</button>")
            'Response.Write("</td></tr></table>")
            Response.Write("<input type=hidden name=UserId value=" & strUserid & ">")
            Response.Write("<input type=hidden name=RoleIds value=" & Left(strRoldIds, Len(strRoldIds) - 1) & "></form>")

            Call DisplayUserRoleScope(strUserid)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Sub PopulateScopeData(ByVal intScopeId, ByVal intRoleId)
        Try
            Dim rsScope    ' recordset to hold scope data. 
            Dim rstable    ' recordset to hold list of tables data. 
            Dim strSQLScope ' SQL to get the scopes
            Dim strSQLtable   ' SQL to get the table name associated with the scope
            Dim strtableName  ' to hold the table name
            Dim strFieldNames  ' to hold the field names
            Dim strActiveFlagField  ' to store the active flag field name
            Dim strOrderByField  ' to store the order by field name
            Dim str
            Dim con

            con = Server.CreateObject("adodb.connection")
            str = Application("oldframeworkConstr")

            con.Open(str)

            rsScope = Server.CreateObject("ADODB.Recordset")
            rstable = Server.CreateObject("ADODB.Recordset")

            strSQLtable = "SELECT DISTINCT S.Stm_Dbtable_Name AS table_Name, S.Stm_Select_Field1, S.Stm_ActiveFlag_Field, S.Stm_OrderBy_Field" & _
                          " FROM  " & Session("TENANT") & "."  & "Role AS R INNER JOIN  " & Session("TENANT") & "."  & "SCOPE_table_MAP AS S ON R.Rol_Scp_Id = S.Stm_Scp_Id" & _
                          " WHERE R.Rol_Active = 'Y' AND 	S.Stm_Active = 'Y' AND  R.Rol_Scp_Id  = " & intScopeId


            rstable.Open(strSQLtable, con)


            If Not rstable.EOF Then
                strtableName = rstable("table_Name").value
                strFieldNames = rstable("Stm_Select_Field1").value
                strActiveFlagField = rstable("Stm_ActiveFlag_Field").value
                strOrderByField = rstable("Stm_OrderBy_Field").value

                strSQLScope = "SELECT " & strFieldNames & _
                              " FROM " & Session("TENANT") & "." & strtableName & _
                              " WHERE " & strActiveFlagField & " = 1 ORDER BY " & strOrderByField
                rsScope.Open(strSQLScope, con)

                strRoldIds = strRoldIds & intRoleId & ","


                Response.Write("<td><Select Name=" & intRoleId & " multiple size=1 width=100% class=clsComboBox>")
                Response.Write("<Option Value=Null>------------------- Map the Scope -------------------</option>")
                While Not rsScope.EOF
                    Response.Write("<Option Value=" & rsScope(0).value & ">" & rsScope(1).value & "</option>")
                    rsScope.MoveNext()
                End While
                Response.Write("</Select></td></tr>")
                rstable.Close()
                rsScope.Close()
            Else
                rstable.Close()
                Response.Write("table mapping not found in the database ......")
                Response.End()
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Sub DisplayUserRoleScope(ByVal strUserid)
        Try
            Dim con, str, rsScopeMap
            con = Server.CreateObject("adodb.connection")
            str = Application("oldframeworkConstr")
            con.Open(str)
            rsScopeMap = Server.CreateObject("ADODB.Recordset")

            'AMT_SCOPEMAP_SP Displays SCOPE_MAP_ID, SCOPE_MAP_DESCRIPTION, ROL_ID, ROL_DESCRIPTION
            rsScopeMap.Open("Execute AMT_SCOPEMAP_SP " & strUserid, con)


            If Not rsScopeMap.EOF Then

                Response.Write("<table border=1 style=""Z-INDEX: 103;  BORDER-COLLAPSE: collapse;"" ALIGN=center BORDER=""1"" cellspacing=""0"" cellpadding=""0"" width=""95%"">")
                Response.Write("<tr><td class=clsTblHead>Role</td><td class=clsTblHead>Scope</td><td class=clsTblHead>Option</td></tr>")

                While Not rsScopeMap.EOF
                    Response.Write("<Tr><Td>" & rsScopeMap("Rol_Description").value & "</Td><Td Class=""clsFields""> " & rsScopeMap("Scope_Map_Description").value & "</Td><Td Class=""clsFields""><a href=frmADFuserprevileges.aspx?UserId=" & strUserid & "&RoleId=" & rsScopeMap("Rol_Id").value & "&ScopeMapId=" & rsScopeMap("Scope_Map_Id").value & ">Delete</a></Td></Tr>")
                    rsScopeMap.MoveNext()
                End While

                Response.Write("</table><br /><br />")
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Sub DeleteUserRole(ByVal strUserid, ByVal intRoleId, ByVal intScopeMapId)
        Try
            Dim con, str, strDeleteUserScope
            con = Server.CreateObject("adodb.connection")
            str = Application("oldframeworkConstr")
            con.Open(str)
            strDeleteUserScope = "DELETE FROM  " & Session("TENANT") & "."  & "User_Role" & _
                                 " WHERE Url_USR_Id = '" & strUserid & "' AND Url_Rol_Id = " & intRoleId & " AND Url_ScopeMap_Id = " & intScopeMapId

            con.Execute(strDeleteUserScope)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
End Class

