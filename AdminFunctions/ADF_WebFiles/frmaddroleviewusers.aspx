﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmaddroleviewusers.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmaddroleviewusers" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-4 control-label">Employee ID</label>
                                        <asp:RequiredFieldValidator ID="rfvtodate" runat="server" ControlToValidate="txtEmpId" Display="None"
                                            ErrorMessage="Please Enter Employee Id"> </asp:RequiredFieldValidator>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <%-- <div class="col-sm-6">
                                <div class="col-sm-12 text-left">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" EmptyDataText="No Employee Found."
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Employee ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAurid" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCount" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
