Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfprocessownermain
    Inherits System.Web.UI.Page
#Region "General Variables"

    Dim drProcess As SqlDataReader
    Dim strSql As String
    Dim daProcess As New SqlDataAdapter()
    Dim dsProcess As New DataSet()
    Dim sqlAct As String
    Dim iPro As Integer
    Dim strActive As String
    Dim iUsrID As Integer
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            If Not IsPostBack Then
                strSql = "SELECT PSY_ID,PSY_TITLE " & _
                         " FROM  " & Session("TENANT") & "." & "PARTICIPATING_SYSTEM ORDER BY PSY_TITLE"
                'strSql = "NP_INLINE_PROCESS_SYSTEM"
                drProcess = SqlHelper.ExecuteReader(CommandType.Text, strSql)
                cboOwner.DataSource = drProcess
                cboOwner.DataTextField = "PSY_TITLE"
                cboOwner.DataValueField = "PSY_ID"
                cboOwner.DataBind()
                cboOwner.Items.Insert(0, "--Select--")
                cboOwner.Items.Insert(1, "All")
                lblMessage.Text = ""
                lblMessage.Text = Request.QueryString("strMsg")
                lblMessage.Visible = True
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Public Sub BindDataList()
        Try
            iUsrID = Trim(cboOwner.SelectedItem.Value)
            Dim Ds As New DataSet()
            If Trim(cboOwner.SelectedItem.Value) = "All" Then
                strSql = "SELECT POW_ID,PSY_ID,POW_PSY_ID,POW_USR_ID,POW_ACTIVE,PSY_TITLE From  " & Session("TENANT") & "." & "PROCESS_OWNER" _
                         & ", " & Session("TENANT") & "." & "PARTICIPATING_SYSTEM WHERE PSY_ID=POW_PSY_ID"
                'strSql = "NP_INLINE_GET_PROCESS_PARTICIPATING"
            Else
                strSql = "SELECT POW_ID,PSY_ID,POW_PSY_ID,POW_USR_ID,POW_ACTIVE,PSY_TITLE From  " & Session("TENANT") & "." & "PROCESS_OWNER" _
                         & ", " & Session("TENANT") & "." & "PARTICIPATING_SYSTEM WHERE PSY_ID=" & Trim(cboOwner.SelectedItem.Value) _
                         & " AND PSY_ID=POW_PSY_ID"
                'strSql = "NP_INLINE_GET_PROCESS_PARTICIPATING_WITH_PARAM"
                'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "NP_INLINE_GET_PROCESS_PARTICIPATING_WITH_PARAM")
                'sp1.Command.AddParameter("@UID", iUsrID, DbType.String)
                'strSql = sp1.ExecuteScalar()
            End If
            Ds = SqlHelper.ExecuteDataset(CommandType.Text, strSql)
            grdProcess.DataSource = Ds
            grdProcess.DataBind()
            If grdProcess.Rows.Count <= 0 Then
                lblMessage.Text = "No Data Found!"
                lblMessage.Visible = True
                grdProcess.Visible = False
            Else
                lblMessage.Text = ""
                grdProcess.Visible = True
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub cboOwner_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOwner.SelectedIndexChanged
        Try
            If cboOwner.SelectedItem.Value = "--Select--" Then
                lblMessage.Visible = False
                lblMessage.Text = ""
                grdProcess.Visible = False
            Else
                lblMessage.Visible = False
                lblMessage.Text = ""
                BindDataList()
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Protected Sub grdProcess_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProcess.PageIndexChanging
        Try
            grdProcess.PageIndex = e.NewPageIndex
            BindDataList()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub

    Protected Sub grdProcess_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdProcess.RowCommand
        Try
            Dim index As Integer = e.CommandArgument
            Dim lbtnStatus As LinkButton = grdProcess.Rows(index).Cells(1).Controls(0)
            Dim str As String
            str = lbtnStatus.Text
            If str = "Y" Or str = "N" Then
            Else
                Exit Sub
            End If

            iPro = grdProcess.Rows(index).Cells(0).Text
            Select Case Trim(lbtnStatus.Text)
                Case "Y"
                    lbtnStatus.Text = "N"

                    strActive = "UPDATE  " & Session("TENANT") & "."  & "PROCESS_OWNER " & _
                                " SET POW_ACTIVE='N' " & _
                                " WHERE(POW_ID = " & iPro & ")"
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strActive)
                    'BindDataList()
                Case "N"
                    lbtnStatus.Text = "Y"
                    strActive = "UPDATE  " & Session("TENANT") & "."  & "PROCESS_OWNER " & _
                                " SET POW_ACTIVE='Y' " & _
                                " WHERE(POW_ID = " & iPro & ")"
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strActive)
                    'BindDataList()
                Case Else
                    Exit Sub
            End Select
            BindDataList()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
End Class

