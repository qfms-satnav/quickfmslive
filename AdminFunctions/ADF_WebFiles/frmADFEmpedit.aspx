﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmADFEmpedit.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmADFEmpedit" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%--<script type="text/javascript" src="../../Scripts/Cal.js">--%>
    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>
                            <asp:Label ID="lblHead" runat="server"></asp:Label></legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="PNLCONTAINER" runat="server" class="row">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ValidationGroup="Val1" ForeColor="Red" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Gender<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rdbGender" Display="None"
                                                 ErrorMessage="Please Select Gender" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:RadioButtonList ID="rdbGender" runat="server" RepeatDirection="Horizontal" Width="50%">
                                                    <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                                    <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--   <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">--%>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Title<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="cvtitle" runat="server" ValueToCompare="0" Operator="NotEqual"
                                                ControlToValidate="ddltitle" Display="None" ErrorMessage="Please Select Title"
                                                ValidationGroup="Val1"></asp:CompareValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddltitle" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="1">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                                    <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                                    <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">First Name<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="Comptxtfname" runat="server" Operator="NotEqual" ControlToValidate="txtfname"
                                                Display="None" ErrorMessage="Please Enter only characters" ValidationGroup="Val1"></asp:CompareValidator>
                                            <asp:RegularExpressionValidator ID="RegExptxtfname" runat="server" ControlToValidate="txtfname"
                                                Display="None" ErrorMessage="Please Enter First Name only in Characters" ValidationExpression="^[a-zA-Z ]*"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvfname" runat="server" Width="48px" ControlToValidate="txtfname"
                                                Display="None" ErrorMessage="Please Enter First Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter first name in alphabets,space and 50 characters allowed')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtfname" runat="server" CssClass="form-control" MaxLength="50"
                                                        TabIndex="1"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Middle Name</label>
                                            <%--<asp:RegularExpressionValidator ID="Regexptxtmname" runat="server" ControlToValidate="txtmname"
                                                Display="None" ErrorMessage="Please Enter Middle Name only in Characters"
                                                ValidationExpression="^[a-zA-Z ]*" ValidationGroup="Val1"></asp:RegularExpressionValidator>--%>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter middle name in alphabets,space and 50 characters allowed')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtmname" runat="server" CssClass="form-control" MaxLength="50"
                                                        TabIndex="2"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Last Name<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="RegexpLname" runat="server" ControlToValidate="txtlname"
                                                Display="None" ErrorMessage="Please Enter Last Name only in Characters" ValidationExpression="^[a-zA-Z ]*"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:CompareValidator ID="Comptxtlname" runat="server" Operator="NotEqual" ControlToValidate="txtlname"
                                                Display="None" ErrorMessage="Please Enter lastname only characters" ValidationGroup="Val1"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="rfvLname" runat="server" Width="48px" ControlToValidate="txtlname"
                                                Display="None" ErrorMessage="Please Enter Last Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter last name in alphabets and 50 characters allowed')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtlname" runat="server" CssClass="form-control" MaxLength="50"
                                                        TabIndex="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Email Id<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="revemail" runat="server" ControlToValidate="txtmailid"
                                                Display="None" ErrorMessage="Please Enter Valid Email Id" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvmail" runat="server" Width="48px" ControlToValidate="txtmailid"
                                                Display="None" ErrorMessage="Please Enter Email" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter email id')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtmailid" runat="server" CssClass="form-control" TabIndex="4"
                                                        MaxLength="250"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">User ID<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="revaurid" runat="server" ControlToValidate="txtaurid"
                                                Display="None" ErrorMessage="
								Please Enter User Id only in Alphanumerics"
                                                ValidationExpression="^[a-zA-Z0-9 -_]*" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvaurid" runat="server" Width="48px" ControlToValidate="txtaurid"
                                                Display="None" ErrorMessage="Please Enter User Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter UserID in alphabets,numbers and _,-')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtaurid" runat="server" CssClass="form-control" MaxLength="20" Enabled="false"
                                                        TabIndex="5"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Country<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="CompareValidator3" runat="server" ValueToCompare="--Select--"
                                                Operator="NotEqual" ControlToValidate="ddlcountry" Display="None" ErrorMessage="Please Select Country"
                                                ValidationGroup="Val1"></asp:CompareValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlcountry" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="6" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Time Zone<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RFVtimezone" runat="server" ControlToValidate="ddlTimeZone"
                                                Display="None" ErrorMessage="Please Select Time Zone" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="7">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="Reqtxtcity" runat="server" ControlToValidate="ddlcity"
                                                Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlcity" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="8" AutoPostBack="True" ></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="reqLocation" runat="server" ControlToValidate="ddlLocation"
                                                Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvbdg" runat="server" ValueToCompare="--Select--" Operator="NotEqual"
                                                ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1"></asp:CompareValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="9">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Enter Password(Maximum 8 characters)<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="refpwd" runat="server" Width="48px" ControlToValidate="txtpwd"
                                                Display="None" ErrorMessage="Please Enter Password" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CmpValpwd" runat="server" ControlToValidate="txtpwd" Display="None"
                                                ErrorMessage="Passwords do not match" ControlToCompare="txtcpwd" ValidationGroup="Val1"></asp:CompareValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter password upto 8 characters')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtpwd" runat="server" CssClass="form-control" MaxLength="8"
                                                        TextMode="Password" TabIndex="10"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Confirm Password(Maximum 8 characters)<span style="color: red;">*</span></label>
                                            <asp:TextBox ID="txtp" runat="server" Visible="False" Width="0px"></asp:TextBox>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtcpwd" runat="server" CssClass="form-control" MaxLength="8"
                                                    TextMode="Password" TabIndex="11"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>

                               <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Grade</label>
                                            <%--<asp:RegularExpressionValidator ID="revGrade" runat="server" ControlToValidate="txtGrade"
                                                Display="None" ErrorMessage="
								Please Enter User Id only in Alphanumerics"
                                                ValidationExpression="^[a-zA-Z0-9 -_]*" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvGreade" runat="server" Width="48px" ControlToValidate="txtGrade"
                                                Display="None" ErrorMessage="Please Enter Grade" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter Grade in alphabets,numbers and _,-,')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtGrade" runat="server" Width="100%" CssClass="form-control" MaxLength="8"
                                                        TabIndex="12"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Department</label>
                                           <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" ValueToCompare="--Select--"
                                                Operator="NotEqual" ControlToValidate="ddldept" Display="None" ErrorMessage="Please Select Department"
                                                ValidationGroup="Val1"></asp:CompareValidator>--%>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddldept" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="13">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Designation</label>
                                          <%--  <asp:CompareValidator ID="CompareValidator2" runat="server" ValueToCompare="--Select--"
                                                Operator="NotEqual" ControlToValidate="ddldesn" Display="None" ErrorMessage="Please Select Designation"
                                                ValidationGroup="Val1"></asp:CompareValidator>--%>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddldesn" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="14">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Reporting Manager</label>
                                           <%-- <asp:CompareValidator ID="CompareValidator4" runat="server" ValueToCompare="--Select--"
                                                Operator="NotEqual" ControlToValidate="ddlrepmgr" Display="None" ErrorMessage="Please Select Reporting Manager"
                                                ValidationGroup="Val1"></asp:CompareValidator>--%>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlrepmgr" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="15">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblVertical" class="col-md-5 control-label" runat="server"><span style="color: red;">*</span></asp:Label>
                                           <%-- <asp:CompareValidator ID="CompareValidator6" runat="server" ValueToCompare="--Select--"
                                                Operator="NotEqual" ControlToValidate="ddlVertical" Display="None" ErrorMessage="Please Select "
                                                ValidationGroup="Val1"></asp:CompareValidator>--%>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlVertical" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="16" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblProcess" class="col-md-5 control-label" runat="server"><span style="color: red;">*</span></asp:Label>
                                           <%-- <asp:CompareValidator ID="CompareValidator5" runat="server" ValueToCompare="--Select--"
                                                Operator="NotEqual" ControlToValidate="ddlCostcenter" Display="None" ErrorMessage="Please Select "
                                                ValidationGroup="Val1"></asp:CompareValidator>--%>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlCostcenter" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    TabIndex="17">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                               <%-- <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Address</label>

                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter Address in 200 characters allowed')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="200"
                                                        TabIndex="18"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Mobile No<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtmob"
                                                Display="None" ErrorMessage="Please Enter Mobile Number in Digits" ValidationExpression="^[0-9-]*"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtmob"
                                                Display="None" ErrorMessage="Please Enter Mobile Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter mobile number in digits')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtmob" runat="server" CssClass="form-control" MaxLength="15"
                                                        TabIndex="19"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="Reqtxtcity0" runat="server" ControlToValidate="ddlStatus" Display="None" ErrorMessage="Please Select Status" 
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                                                        TabIndex="21">
                                                        <asp:ListItem Text="--Select--" Value="--Select--"></asp:ListItem>
                                                        <asp:ListItem Text="Active" Value="N"></asp:ListItem>
                                                        <asp:ListItem Text="In Active" Value="Y"></asp:ListItem>
                                                    </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                          <%--  <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Date of Joining<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvDOJ" runat="server" ControlToValidate="txtDOJ"
                                                Display="None" ErrorMessage="Please Enter Date of Joining" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Pick Date')" onmouseout="UnTip()">--%>
                                                    <%--<asp:TextBox ID="txtDOJ" runat="server" CssClass="form-control"
                                                        TabIndex="18"></asp:TextBox>--%>
                                                   <%-- <div class='input-group date' id='fromdate'>
                                                        <asp:TextBox ID="txtDOJ" runat="server" CssClass="form-control" MaxLength="10" TabIndex="20" Enabled ="false"> </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                               
                            </div>--%>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvempdetials" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped"
                                        EmptyDataText="Not Allocated to any space" AutoGenerateColumns="false" ShowHeaderWhenEmpty="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Employee ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("AUR_FIRST_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblAUR_ID" runat="server" Text='<%#Eval("AUR_ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblemail" runat="server" Text='<%#Eval("AUR_EMAIL") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Space ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblspaceID" runat="server" Text='<%#Eval("AUR_LAST_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="From Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSSA_FROM_DATE" runat="server" Text='<%#Eval("SSA_FROM_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSSA_TO_DT" runat="server" Text='<%#Eval("SSA_TO_DT") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="From Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFROM_TIME" runat="server" Text='<%#Eval("FROM_TIME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTO_TIME" runat="server" Text='<%#Eval("TO_TIME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSpaceRelease" CausesValidation="false" CommandArgument='<%#Eval("AUR_LAST_NAME") %>'
                                                        CommandName="SpaceRelease" runat="server">Space Release</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:TextBox ID="txttoday" runat="server" Width="0px" Visible="False"></asp:TextBox>
                                            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" TabIndex="21" ValidationGroup="Val1"></asp:Button>
                                            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" PostBackUrl="~/AdminFunctions/ADF_WebFiles/frmADFsearch.aspx" Text="Back"  TabIndex="22" CausesValidation="False"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
