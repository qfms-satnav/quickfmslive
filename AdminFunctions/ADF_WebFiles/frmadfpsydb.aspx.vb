Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfpsydb
    Inherits System.Web.UI.Page

#Region "Variable Declaration"
    Dim strSql1 As String
    Dim daPsy As New SqlDataAdapter()
    Dim dsPsy As New DataSet()
    Dim drPsy As SqlDataReader
    Dim iPro As Integer
    Dim strActive As String
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try


            If Not IsPostBack Then
                LoadGrid()
            End If
            lblProcess.Text = Request.QueryString("strMsg")
            lblProcess.Visible = True
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Private Sub LoadGrid()
        Try
            Dim iCount As Integer
            Dim Ds As New DataSet()
            strActive = "SELECT PSY_ID,PSY_TITLE,PSY_DESCRIPTION,PSY_ACTIVE" & _
                      " FROM  " & Session("TENANT") & "."  & "PARTICIPATING_SYSTEM" '& _
            '" WHERE PSY_ID = " & iPro & ""  
            Ds = SqlHelper.ExecuteDataset(CommandType.Text, strActive)
            grdProcess.DataSource = Ds
            grdProcess.DataBind()


            
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
   
  


    Protected Sub grdProcess_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProcess.PageIndexChanging
        Try
            grdProcess.PageIndex = e.NewPageIndex
            LoadGrid()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub

    Protected Sub grdProcess_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdProcess.RowCommand
        Try
            Dim index As Integer
            Dim lbtnStatus As LinkButton = grdProcess.Rows(index).Cells(1).Controls(0)

            Dim str As String
            str = lbtnStatus.Text
            If str = "Y" Or str = "N" Then
            Else
                Exit Sub
            End If

            iPro = grdProcess.Rows(index).Cells(0).Text

            Select Case Trim(lbtnStatus.Text)
                Case "Y"
                    lbtnStatus.Text = "N"

                    strActive = "UPDATE  " & Session("TENANT") & "."  & "PARTICIPATING_SYSTEM " & _
                                " SET PSY_ACTIVE='N' " & _
                                " WHERE(PSY_ID = " & iPro & ")"


                    SqlHelper.ExecuteDataset(CommandType.Text, strActive)
                    LoadGrid()

                Case "N"
                    lbtnStatus.Text = "Y"

                    strActive = "UPDATE  " & Session("TENANT") & "."  & "PARTICIPATING_SYSTEM " & _
                                " SET PSY_ACTIVE='Y' " & _
                                " WHERE(PSY_ID = " & iPro & ")"
                    SqlHelper.ExecuteDataset(CommandType.Text, strActive)
                    LoadGrid()
                Case Else
                    Exit Sub
            End Select
            LoadGrid()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
End Class

