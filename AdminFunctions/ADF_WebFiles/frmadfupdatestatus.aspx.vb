Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfupdatestatus
    Inherits System.Web.UI.Page


    
    Protected strSql As String
    Protected ObjDR As SqlDataReader

    Dim strIntID, strIntlID As String
    'Dim strName, strRem, strdesc As String
   
    Dim iStatus As Integer

    Public Sub cleardata()
        txtcode.Text = ""
        txtName.Text = ""
        txtrem.Text = ""
        txtdesc.Text = ""
        'cmbPKeys.SelectedIndex = 0
    End Sub
    Private Sub binddata()
        Try
            If LblPageStatus.Text = "Delete" Then
                strSql = "SELECT DISTINCT STA_ID,STA_TITLE from  " & Session("TENANT") & "."  & "Status ORDER BY STA_TITLE"
            Else
                strSql = "SELECT DISTINCT STA_ID,STA_TITLE from  " & Session("TENANT") & "."  & "Status WHERE STA_STA_ID=1 ORDER BY STA_TITLE"
            End If


            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSql)
            cmbPKeys.Items.Clear()
            cmbPKeys.DataSource = ObjDR
            cmbPKeys.DataValueField = "STA_ID"
            cmbPKeys.DataTextField = "STA_TITLE"
            cmbPKeys.DataBind()
            cmbPKeys.Items.Insert(0, "--Select--")
            ObjDR.Close()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        LoadGrid()
        If Not IsPostBack Then

            binddata()
            rbActions.Items(0).Selected = True
            cleardata()
            LblPageStatus.Text = "Add"
        
        End If

       

    End Sub

    Private Sub LoadGrid()
        Try
            Dim iCount As Integer
            Dim Ds As New DataSet
            strSql = "SELECT DISTINCT STA_ID,STA_TITLE,STA_STA_ID from  " & Session("TENANT") & "."  & "Status WHERE STA_STA_ID IN (1,2) ORDER BY STA_TITLE"
            Ds = SqlHelper.ExecuteDataset(CommandType.Text, strSql)

            grdStatus.DataSource = Ds
            grdStatus.DataBind()


            For iCount = 0 To grdStatus.Rows.Count - 1
                Dim lbtnGrd As LinkButton = CType(grdStatus.Rows(iCount).Cells(1).Controls(0), LinkButton)
                If Trim(Val(grdStatus.Rows(iCount).Cells(2).Text)) = 1 Then
                    lbtnGrd.Text = "Y"
                ElseIf Trim(Val(grdStatus.Rows(iCount).Cells(2).Text)) = 2 Then
                    lbtnGrd.Text = "N"
                End If
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
  
    


    Private Sub Insertdata()
        Try
            'chk for length of remarks  > 500
            If ((Len(Trim(txtrem.Text)) >= 500) Or (Len(Trim(txtdesc.Text)) >= 500)) Then
                LblFinalStatus.Text = "Please Enter Remarks/Description in not more than 500 Characters"
            Else
                'chk for duplicate entries
                strSql = "SELECT COUNT(*) from  " & Session("TENANT") & "."  & "Status WHERE STA_CODE='" & Replace(Trim(txtcode.Text), "'", "''") & "'"
                strSql = strSql
                If CType(SqlHelper.ExecuteScalar(CommandType.Text, strSql), Integer) > 0 Then
                    LblFinalStatus.Text = "This code already exists.Please modify the code!"
                Else

                    iStatus = 1
                    strSql = "INSERT INTO  " & Session("TENANT") & "."  & "STATUS " _
                    & "(STA_CODE,STA_TITLE,STA_STA_ID,STA_REM,STA_DESC,STA_UP_BY,STA_UP_DT)" & _
                    " VALUES('" & Replace(Trim(txtcode.Text), "'", "''") & "','" _
                    & Replace(Trim(txtName.Text), "'", "''") & "'," & iStatus & ",'" _
                    & Replace(Trim(txtrem.Text), "'", "''") & "','" _
                    & Replace(Trim(txtdesc.Text), "'", "''") & "','" & Session("uid") & "','" & FormatDateTime(Now, DateFormat.ShortDate) _
                    & "')"

                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)

                    LblFinalStatus.Text = "Record has been Inserted"
                    cleardata()
                End If
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub modifydata()
        Try
            'chk for length of remarks  > 500
            If ((Len(Trim(txtrem.Text)) >= 500) Or (Len(Trim(txtdesc.Text)) >= 500)) Then
                LblFinalStatus.Text = "Please Enter Remarks/Description in not more than 500 Characters!"
            Else
                iStatus = 1
                strSql = "UPDATE  " & Session("TENANT") & "."  & "STATUS SET" & _
                        " STA_TITLE='" & Replace(Trim(txtName.Text), "'", "''") & "'," & _
                        " STA_DESC='" & Replace(Trim(txtdesc.Text), "'", "''") & "'," & _
                        " STA_STA_ID= " & iStatus & "," & _
                        " STA_REM='" & Replace(Trim(txtrem.Text), "'", "''") & "'," & _
                        "STA_UP_BY='" & Session("uid") & "'," & _
                        "STA_UP_DT='" & FormatDateTime(Now, DateFormat.ShortDate) & "'" & _
                        " WHERE  STA_ID=" & cmbPKeys.SelectedItem.Value

                SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
                LblFinalStatus.Text = "Record has been Updated!"
                cleardata()
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub cmbPKeys_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPKeys.SelectedIndexChanged
        Try
            If cmbPKeys.SelectedItem.Value <> "--Select--" Then
                strSql = "SELECT DISTINCT STA_ID,STA_CODE,STA_TITLE,STA_DESC,STA_STA_ID,STA_REM " _
      & " from  " & Session("TENANT") & "."  & "Status WHERE STA_ID =" & cmbPKeys.SelectedItem.Value
                'Response.Write(strSql)
                LblFinalStatus.Text = ""

                ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSql)
                Do While ObjDR.Read
                    strIntID = ObjDR.GetValue(0) '.ToString
                    txtcode.Text = ObjDR.GetValue(1).ToString
                    txtName.Text = ObjDR.GetValue(2).ToString
                    txtdesc.Text = ObjDR.GetValue(3).ToString
                    iStatus = ObjDR.GetValue(4) '.ToString
                    txtrem.Text = ObjDR.GetValue(5).ToString
                Loop
                ObjDR.Close()
            Else
                cleardata()
                binddata()
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub rbActions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        Try
            Dim iRbCount As Integer
            For iRbCount = 0 To rbActions.Items.Count - 1
                If rbActions.Items(iRbCount).Selected = True Then
                    If rbActions.Items(iRbCount).Text = "Add" Then
                        LblPageStatus.Text = rbActions.Items(iRbCount).Value
                        lblchnl.Visible = False
                        cmbPKeys.Visible = False
                        txtcode.Enabled = True
                        cleardata()
                        binddata()
                        LblFinalStatus.Text = ""
                    ElseIf rbActions.Items(iRbCount).Text = "Modify" Then
                        LblPageStatus.Text = rbActions.Items(iRbCount).Value
                        cmbPKeys.Items.Clear()
                        lblchnl.Visible = True
                        cmbPKeys.Visible = True
                        txtcode.Enabled = False
                        cleardata()
                        binddata()
                        LblFinalStatus.Text = ""
                    End If
                End If
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("frmMasters.aspx")
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If LblPageStatus.Text = "Add" Then
                Insertdata()
                LoadGrid()
            ElseIf LblPageStatus.Text = "Modify" Then
                modifydata()
                LoadGrid()
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Protected Sub grdStatus_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdStatus.PageIndexChanging
        Try
            If grdStatus.Rows.Count <= 9 Then
                grdStatus.PageIndex = e.NewPageIndex
                binddata()
            End If
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

    Protected Sub grdStatus_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdStatus.RowCommand
        Try
            Dim index As Integer = e.CommandArgument
            Dim lbtnStatus As LinkButton = grdStatus.Rows(index).Cells(1).Controls(0)
            Select Case Trim(lbtnStatus.Text)
                Case "Y"

                    strSql = "UPDATE  " & Session("TENANT") & "."  & "STATUS SET STA_STA_ID=2 WHERE STA_ID=" & _
                             grdStatus.Rows(index).Cells(3).Text

                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
                    LoadGrid()
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)

                Case "N"
                    strSql = "UPDATE  " & Session("TENANT") & "."  & "STATUS SET STA_STA_ID=1 WHERE STA_ID=" & grdStatus.Rows(index).Cells(3).Text
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
                    LoadGrid()
                Case Else
            End Select
            binddata()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
End Class

