<%@ Page Language="VB"  MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmadfupdatestatus.aspx.vb" Inherits="AdminFunctions_ADF_WebFiles_frmadfupdatestatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="Table1" width="100%" align="center">
						<tr>
						<td align="center" width="100%">
<asp:label id="lblStatus" runat="server" Width="95%" Height="24px" cssclass="head">Admin Functions
<hr width="70%" align="center" /></asp:label>
</td>
</tr>
</table>
				
				<asp:panel id="pnlStatus" Width="95%" HorizontalAlign="Center" Runat="server">
					<div align="left">
						<asp:Label id="lblMnd" runat="server" cssclass="clsNote">(*) Mandatory Fields.</asp:Label></div>
					<table id="tblStatus" width="100%" align="center">
						<tr>
							<td class="clstblnew" width="50%" colspan="2">Status Details</td>
						</tr>
						<tr>
							<td align="center" colspan="2">
								<asp:radiobuttonlist id="rbActions" runat="server" cssclass="clsRadioButton" Height="16px" AutoPostBack="true"
									Repeatdirection="Horizontal">
									<asp:ListItem Value="Add">Add</asp:ListItem>
									<asp:ListItem Value="Modify">Modify</asp:ListItem>
								</asp:radiobuttonlist></td>
						</tr>
						<tr>
							<td class="label" style="WIDTH: 450px" width="450">
								<asp:label id="lblchnl" runat="server" cssclass="label" Visible="False">Status<font class="clsNote">
										*</font></asp:label>
								<asp:comparevalidator id="cmpPkeys" runat="server" ErrorMessage="Please Select The Status !" ControlToValidate="cmbPKeys"
									Display="None" Operator="NotEqual" ValueToCompare="--Select--"></asp:comparevalidator></td>
							<td width="70%">
								<asp:dropdownlist id="cmbPKeys" runat="server" cssclass="clsComboBox" Width="100%" AutoPostBack="true"
									Visible="False"></asp:dropdownlist></td>
						</tr>
						<tr>
							<td class="label" style="WIDTH: 450px" width="450">Code<font class="clsNote">*</font>
								<asp:requiredfieldvalidator id="rfvCode" runat="server" Width="48px" ErrorMessage="Please Enter The Code !"
									ControlToValidate="txtcode" Display="None"></asp:requiredfieldvalidator></td>
							<td>
								<asp:textbox id="txtcode" runat="server" cssclass="clsTextField" Width="100%" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td class="label" style="WIDTH: 450px" width="450">Name<font class="clsNote">*
									<asp:requiredfieldvalidator id="rfvName" runat="server" Width="48px" ErrorMessage="Please Enter The Name !"
										ControlToValidate="txtName" Display="None"></asp:requiredfieldvalidator></font></td>
							<td>
								<asp:textbox id="txtName" runat="server" cssclass="clsTextField" Width="100%" MaxLength="50"></asp:textbox></td>
						</tr>
						<tr>
							<td class="label" style="WIDTH: 450px" width="450">Description
							</td>
							<td>
								<asp:textbox id="txtdesc" runat="server" cssclass="clsTextField" Height="33px" Width="100%" MaxLength="500"
									TextMode="MultiLine"></asp:textbox></td>
						</tr>
						<tr>
							<td class="label" style="WIDTH: 450px" width="450">Remarks<font class="clsNote">*</font>
								<asp:requiredfieldvalidator id="rvRem" runat="server" Width="48px" ErrorMessage="Please Enter The Remarks !"
									ControlToValidate="txtrem" Display="None"></asp:requiredfieldvalidator></td>
							<td>
								<asp:textbox id="txtrem" runat="server" cssclass="clsTextField" Height="33px" Width="100%" MaxLength="500"
									TextMode="MultiLine"></asp:textbox></td>
						</tr>
						<tr>
							<td align="center" colspan="2">
								<asp:button id="Button2" runat="server" cssclass="clsButton" Text="Submit"></asp:button>
							</td>
						</tr>
					</table>
					<br />
					<asp:label id="LblFinalStatus" runat="server" cssclass="clsmessage" Width="367px"></asp:label>
					<br />
					<br />
					<asp:gridview id="grdStatus" runat="server" Width="100%" AutoGenerateColumns="False" PageSize="9"
						AllowPaging="true">
						<Columns>
							<asp:BoundField DataField="STA_TITLE" HeaderText="Status">
								<HeaderStyle Width="50%" cssclass="clsTblHead"></HeaderStyle>
							</asp:BoundField>
							<asp:ButtonField HeaderText="Active">
								<HeaderStyle Width="50%" cssclass="clsTblHead"></HeaderStyle>
							</asp:ButtonField>
							<asp:BoundField Visible="False" DataField="STA_STA_ID" HeaderText="STA_STA_ID"></asp:BoundField>
							<asp:BoundField Visible="False" DataField="STA_ID" HeaderText=" STA_ID"></asp:BoundField>
						</Columns>
					</asp:gridview>
					
					</asp:panel><p align="center"><asp:label id="LblPageStatus"  runat="Server"  Visible="False">Label</asp:label></p>
			<p align="center"><asp:validationsummary id="vsStatus" runat="server" ShowMessageBox="true" ShowSummary="False"></asp:validationsummary></p>
			<p>&nbsp;</p>
</asp:Content>		