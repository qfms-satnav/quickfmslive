Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfempsearchresults
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim arrEmployees As Array
    Dim strEmpid As String
    Dim iEmp As Integer
    Dim strSql As String
    Dim strUID As String
    Dim strEID As String
    Dim strDept As String
    Dim strFName As String
    Dim strMName As String
    Dim strLName As String
    Dim strExt As String
    Dim strEmail As String
    Dim objdata As SqlDataReader
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack() Then
            lblPageSize.Text = "10"
            lblCurrIndex.Text = "0"
            lbldisp.Visible = False
            DataBind1()
        End If
      
    End Sub



    Private Sub btnSub_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("MultiUsersPrevilege.aspx?UserId=" & txtHidPartId.Text)
    End Sub
  
    Private Sub DataBind1()

        strEID = Replace(Trim(Request.QueryString("eid")), "'", "''")


        strDept = Replace(Trim(Request.QueryString("ccname")), "'", "''")

        strFName = Replace(Trim(Request.QueryString("Fname")), "'", "''")

        strMName = Replace(Trim(Request.QueryString("Mname")), "'", "''")


        strLName = Replace(Trim(Request.QueryString("Lname")), "'", "''")


        strExt = Replace(Trim(Request.QueryString("Ext")), "'", "''")


        strEmail = Replace(Trim(Request.QueryString("Email")), "'", "''")

        strUID = Session("uid")
        strEmpid = ""
        If strEID <> "" Then
            arrEmployees = Split(strEID, ",")
            For iEmp = LBound(arrEmployees) To UBound(arrEmployees)
                strEmpid = strEmpid & "'" & arrEmployees(iEmp) & "',"
            Next
        End If
        Dim ObjData As SqlDataReader


        If Trim(txtEmpid.Text) <> "" Then
            strSql = "select AUR_FULL_NAME AUR_Name, AUR_ID,AUR_EXTENSION ,AUR_DEP_NAME as CCTM_NAME, AUR_EMAIL ,AUR_DEP_ID," & _
                     " isnull((select 'Checked' where AUR_Id in (" & Left(strEmpid, Len(strEmpid) - 1) & ")),'') as sta,DEP_NAME from " & _
                     "  " & Session("TENANT") & "."  & "AMT_AMANtrAUSER_VW, " & Session("TENANT") & "."  & "Department where AUR_id is not null AND aur_dep_id=dep_id "

        Else
            strSql = "select AUR_FULL_NAME AUR_Name, " & _
                    "  AUR_ID,AUR_EXTENSION ,AUR_DEP_ID as CCTM_NAME, AUR_EMAIL ,AUR_DEP_ID,isnull((select 'Checked' where AUR_Id in ('')),'') as sta,DEP_NAME from " & _
                    "  " & Session("TENANT") & "."  & "AMT_AMANtrAUSER_VW, " & Session("TENANT") & "."  & "Department where AUR_id is not null AND aur_dep_id=dep_id  "
            
        End If


        If Trim(strDept) <> 1 Then strSql = strSql & " and AUR_DEP_ID ='" & strDept & "'"
        If strFName <> "" Then strSql = strSql & " and AUR_first_name like '%" & strFName & "%' "
        If strMName <> "" Then strSql = strSql & " and AUR_middle_name like '%" & strMName & "%' "
        If strLName <> "" Then strSql = strSql & " and AUR_last_name like '%" & strLName & "%' "
        If strEID <> "" Then strSql = strSql & " and AUR_ID in (" & Left(strEmpid, Len(strEmpid) - 1) & ") "
        If strExt <> "" Then strSql = strSql & " and AUR_extension like '%" & strExt & "%' "
        If strExt <> "" Then strSql = strSql & " and AUR_email like '%" & strEmail & "%' "

        strSql = strSql & "  order by Aur_Name "

        
        Dim objDS As New DataSet

        If Not Page.IsPostBack() Then
            objDS = SqlHelper.ExecuteDataset(CommandType.Text, strSql)
            lblRecordCount.Text = CStr(objDS.tables(0).Rows.Count)
            objDS = Nothing
            objDS = New DataSet
        End If
      
        dlstSearch.DataSource = objDS.tables(0).DefaultView
        dlstSearch.DataBind()

        PrintStatus()
       

    End Sub

    Public Sub ShowFirst(ByVal s As Object, ByVal e As EventArgs)

        lblCurrIndex.Text = "0"
        DataBind1()
       

    End Sub

    Public Sub ShowPrevious(ByVal s As Object, ByVal e As EventArgs)

        lblCurrIndex.Text = CStr(CInt(lblCurrIndex.Text) - CInt(lblPageSize.Text))
        If CInt(lblCurrIndex.Text) < 0 Then
            lblCurrIndex.Text = "0"
        End If
        lbldisp.Visible = False
        lbldisp.Text = ""
        DataBind1()
       

    End Sub

    Public Sub ShowNext(ByVal s As Object, ByVal e As EventArgs)
          lblCurrIndex.Text = CStr(CInt(lblCurrIndex.Text) + CInt(lblPageSize.Text))
        If CInt(lblCurrIndex.Text) + 1 > CInt(lblRecordCount.Text) Then
            lblCurrIndex.Text = lblPageSize.Text
            lbldisp.Visible = False
            lbldisp.Text = ""
        End If

        DataBind1()



    End Sub

    Public Sub ShowLast(ByVal s As Object, ByVal e As EventArgs)

        Dim tmpInt As Integer

        tmpInt = CInt(lblRecordCount.Text) Mod CInt(lblPageSize.Text)
        If tmpInt >= 0 Then
            lblCurrIndex.Text = CStr(CInt(lblRecordCount.Text) - tmpInt)
        Else
            lblCurrIndex.Text = CStr(CInt(lblRecordCount.Text) - CInt(lblPageSize.Text))
        End If
        DataBind1()
       
    End Sub

    Private Sub PrintStatus()

        lblStatus.Text = "Total Records:<b>" & lblRecordCount.Text
        lblStatus.Text += "</b> - Showing Page:<b> "
        lblStatus.Text += CStr(CInt(CInt(lblCurrIndex.Text) / CInt(lblPageSize.Text) + 1))
        lblStatus.Text += "</b> of <b>"

        If (CInt(lblRecordCount.Text) Mod CInt(lblPageSize.Text)) > 0 Then
            lblStatus.Text += CStr(CInt(CInt(lblRecordCount.Text) / CInt(lblPageSize.Text) + 1))
        Else
            lblStatus.Text += CStr(CInt(lblRecordCount.Text) / CInt(lblPageSize.Text))
        End If
        lblStatus.Text += "</b>"
       

    End Sub

    Public Sub Resetme(ByVal s As Object, ByVal e As EventArgs)

        txtEmpid.Text = ""
        txtHidPartEmail.Text = ""
        txtHidPartId.Text = ""

        DataBind1()
        showparticipants()
       
    End Sub

    Public Sub AddEmp(ByVal s As Object, ByVal e As EventArgs)

        Dim i As Integer
        Dim Tchkemp As String
        Tchkemp = "'" & Replace(Request.Form("chkEmp"), ",", "','") & "',"

        txtEmpid.Text = txtEmpid.Text & Tchkemp

        DataBind1()
        showparticipants()
       

    End Sub

    Public Sub showparticipants()

        dgSearch.Visible = True
        If Len(txtEmpid.Text) = 0 Then
            strSql = "select * from " & Session("TENANT") & "." & "VW_Employee_Details where AUR_ID in ('')"

            lblHead.Visible = False
            btnSub.Visible = False

            lblNodata.Text = "No Selected View"
            lblNodata.Visible = True
        Else
            strSql = "select * from " & Session("TENANT") & "." & "VW_Employee_Details where AUR_ID in (" & Left(txtempid.Text, Len(txtempid.Text) - 1) & ")"

            lblHead.Visible = True
            lblHead.Text = "Seleced Users View "

            lblNodata.Visible = False
            btnSub.Visible = True

        End If


        strSQL = strSql
        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        dgSearch.DataSource = objdata
        dgSearch.DataBind()
        objdata.Close()

        If dgSearch.Rows.Count = 0 Then
            dgSearch.Visible = False
        Else
            dgSearch.Visible = True
            strSql = "select AUR_ID,AUR_EMAIL from " & Session("TENANT") & "." & "VW_Employee_Details where AUR_ID in (" & Left(txtempid.Text, Len(txtempid.Text) - 1) & ")"

            objdata = SqlHelper.ExecuteReader(CommandType.Text, strSql)
            While objdata.Read
                txtHidPartEmail.Text = txtHidPartEmail.Text & objdata("AUR_EMAIL").ToString & ","
                txtHidPartId.Text = txtHidPartId.Text & objdata("AUR_ID").ToString & ","
            End While
            objdata.Close()


        End If

      

    End Sub

   
   

   

    Protected Sub dgSearch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgSearch.PageIndexChanging

    End Sub

    Protected Sub dgSearch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgSearch.RowCommand
        Try
            Dim index As Integer = e.CommandArgument
            If dgSearch.Rows(index).RowType = ListItemType.Pager Or _
                    dgSearch.Rows(index).RowType = ListItemType.Header Then Exit Sub
            Dim btn As Button = CType(e.CommandSource, Button)

            If btn.Text = "Delete" Then
                strEID = dgSearch.Rows(index).Cells(0).Text
                strEID = "'" & strEID & "',"
                txtempid.Text = Replace(Trim(txtempid.Text), strEID, "")

                DataBind1()
                showparticipants()
            End If
        Catch ex As Exception
           
        End Try

    End Sub
End Class


