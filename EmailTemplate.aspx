﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmailTemplate.aspx.vb" Inherits="EmailTemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="http://devv4.a-mantra.com/BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="http://devv4.a-mantra.com/BootStrapCSS/amantra.min.css" rel="stylesheet" />
    <link href="http://devv4.a-mantra.com/BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    
    <style>

        .footeremail {
            -ms-filter: "progid:DXImageTransform.Microsoft.Gradient(GradientType=1,StartColorStr=#1D4A81,EndColorStr=#466788)";
            background: linear-gradient(to bottom,#466788 0,#1D4A81 100%);
            border-color: #365e86;
            color: #eee;
            width: 847px;
            height: 50px;
            padding: 11px 0 10px 0;
            text-align: center;
            border-top: 1px solid #1D4A81;
            position: relative;
            bottom: -35px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 850px; border: solid; border-color: #466788; border-width: 2px; margin-left: auto; margin-right: auto;">

            <nav class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <img src='http://devv4.a-mantra.com/BootStrapCSS/images/logo_Quick.gif'
                        class="ie8-pngcss" alt="" style="margin-top: 2px;" />
                     <%--height: 60px; width: 300px;--%>
                </div>
            </nav>
            <br />
            <br />
            <div style="padding: 8px; margin: 8px;">


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">Dear <b>Jaydeep K Gharat , </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-8 control-label">
                                   Today's total conference room booking details:
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="panel panel-primary" style="width: 800px; flex-align: center">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Booking details</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" style="width: 800px; flex-align: center">
                        <br />
                        <br />
                        <table class="table table-condensed table-bordered table-hover table-striped" style="width: 750px; flex-align: center">
                            <tr>
                                <th>City</th>
                                <th>Location</th>
                                <th>Tower</th>
                                <th>Floor</th>
                                <th>Conference Room</th>
                                <th>Conference Slot</th>
                                <th>Booked Hours</th>
                            </tr>
                            <tr>
                                <td>Hyderabad</td>
                                <td>Hyderabad - Madhapur</td>
                                <td>No Tower</td>
                                <td>First Floor</td>
                                <td>Presentation Room</td>
                                <td>1:00PM :  4:00PM</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>Hyderabad</td>
                                <td>Hyderabad - Hitech City</td>
                                <td>Satnav Towers</td>
                                <td>Ground Floor</td>
                                <td>Video Conference Room1</td>
                                <td>10:00PM : 11:00PM</td>
                                <td>1</td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    </div>
                </div>
                <br />

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-5 control-label">
                                    <p><b>Regards,</b></p>
                                    <p><b>a-mantra Team</b></p>
                                </label>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footeremail">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        Other Links : 
                <a href="http://www.satnavtech.com/" target="_blank" style="color: white">www.satnavtech.com</a>
                    </div>
                    <div class="col-md-3 pull-right">
                        &copy; 2015 a-mantra
                    </div>
                </div>
            </div>
            <br />
            <br />

        </div>
    </form>
    
</body>
</html>
