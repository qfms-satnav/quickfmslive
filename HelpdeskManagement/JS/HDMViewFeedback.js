﻿app.service("HDMViewFeedBackService", function ($http, $q, UtilityService) {
    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewFeedBack/GetGriddata')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    
    this.GetGriddataBySearch = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewFeedBack/GetGriddataBySearch', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});


app.controller('HDMViewFeedBackController', function ($scope, $q, $http, HDMViewFeedBackService, UtilityService, HDMFeedBackService, $timeout, $filter, $window) {
    $scope.HDMViewFeedback = {};
    $scope.Request_Type = [];
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.CompanyVisible = 0;
    $scope.columnDefs = [];
   
    var RequestId;
    $scope.ReqId = true;

   
   

    // fill company ddls

    var ROLID; var b;
    $scope.PageLoad = function () {

        UtilityService.GetLocationsall(1).then(function (response) {
            if (response.data != null) {
                $scope.Clinic = response.data;
                $scope.HDMViewFeedback.Clinic = $scope.Clinic;
            }
        });

        HDMFeedBackService.GetMainCat().then(function (response) {
            if (response.data != null) {
                $scope.Main_Category = response.data;
            }
        });
        HDMFeedBackService.Getsubcat().then(function (response) {
            if (response.data != null) {
                $scope.Sub_Category = response.data;
            }
        });
        HDMFeedBackService.Getchildcat().then(function (response) {
            if (response.data != null) {
                $scope.Child = response.data;
            }
        });
        HDMFeedBackService.GetStatusList().then(function (response) {
            if (response.data != null) {
                $scope.Status = response.data;
            }
        });
       
    }
    $scope.PageLoad();

    $scope.Redirect = function (data) {
        RequestId = data.REQUEST_ID;
        $window.location.href = 'HDMViewModifyFeedback.aspx?RequestId=' + RequestId + '&FDBK_SUB_CAT_CODE=' + data.FDBK_SUB_CAT_CODE + '&FDBK_ID=' + data.FDBK_ID + '';
    }

    // }

    var columnDefs = [
        { headerName: "Unique Id", field: "FDBK_ID", width: 130, cellClass: 'grid-align' },

        { headerName: "Requisition Id", field: "REQUEST_ID", width: 130, cellClass: 'grid-align', template: '<a ng-click="Redirect(data)">{{data.REQUEST_ID}}</a>' },
        { headerName: "Location", field: "LOCATION", width: 140, cellClass: 'grid-align', },
        { headerName: "Complaint Received Date", field: "CMPLT_REV_DATE", width: 150, cellClass: 'grid-align', },
        { headerName: "Client Name", field: "CLNT_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Main Status", field: "MAIN_STATUS", width: 150, cellClass: 'grid-align', },
        { headerName: "Main Category", field: "MAIN_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Sub Category", field: "SUB_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Child Category", field: "CHILD_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Mode Comments", field: "MODE_CMTS", width: 130, cellClass: 'grid-align', },
        { headerName: "Mode Name", field: "MODE_NAME", width: 130, cellClass: 'grid-align', },
        { headerName: "Clinic Manager", field: "CLNC_MGR", width: 150, cellClass: 'grid-align', },
        { headerName: "AOM", field: "AOM", width: 130, cellClass: 'grid-align', },
        { headerName: "OTHERS", field: "OTHERS", width: 130, cellClass: 'grid-align', },
        { headerName: "Sub Status", field: "SUB_STA_NAME", width: 100, cellClass: 'grid-align', },
        { headerName: "Status Comments", field: "SUB_STA_CMTS", width: 100, cellClass: 'grid-align', },
        { headerName: "Requested By", field: "REQUESTED_BY", width: 150, cellClass: 'grid-align', },
        //{ headerName: "No.Of Escalations", field: "ESC_COUNT", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Labour Cost", field: "LABOUR_COST", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Spare Cost", field: "SPARE_COST", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Additional Cost", field: "ADDITIONAL_COST", width: 120, cellClass: 'grid-align', },
        //{ headerName: "Closed Time", field: "CLOSED_TIME", width: 130, cellClass: 'grid-align', },
        //{ headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Response TAT", field: "RESPONSE_TAT", width: 140, cellClass: 'grid-align', },
        //{ headerName: "Delayed TAT", field: "DELAYED_TAT", width: 140, cellClass: 'grid-align', },
    ];


    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        //showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Main Category", field: "Main_Category",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    HDMViewFeedBackService.GetGriddata().then(function (response) {
        console.log(response.data[0]);
        $scope.gridata = response.data;
        if (response.data == null) {
            $scope.gridOptions.api.setRowData([]);
            progress(0, 'Loading...', false);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            $scope.gridOptions.api.setRowData(response.data);
            progress(0, 'Loading...', false);
        }
    });

   
    $scope.HDMViewFeedback.selstatus = "1";
    $scope.STATUSCHANGE = function () {
        switch ($scope.HDMViewFeedback.selstatus) {
            case '1':
                $scope.HDMViewFeedback.selstatus = "1";
                break;
            case '2':
                $scope.HDMViewFeedback.selstatus = "2";
                break;
            case '3':
                $scope.HDMViewFeedback.selstatus = "3";
                break
        }
    }
    $scope.HDMViewFeedback.selVal = "THISMONTH";
    $scope.HDMViewFeedback.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
    $scope.HDMViewFeedback.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
    $scope.rptDateRanges = function () {
        switch ($scope.HDMViewFeedback.selVal) {
            case 'TODAY':
                $scope.HDMViewFeedback.FromDate = moment().format('DD-MMM-YYYY');
                $scope.HDMViewFeedback.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.HDMViewFeedback.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.HDMViewFeedback.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break
            case '7':
                $scope.HDMViewFeedback.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.HDMViewFeedback.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.HDMViewFeedback.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.HDMViewFeedback.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.HDMViewFeedback.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.HDMViewFeedback.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.HDMViewFeedback.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.HDMViewFeedback.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    var clk = true;
    var ExportColumns;
    $scope.LoadData = function () {
        var unticked = _.filter($scope.Cols, function (item) {
            return item.ticked == false;
        });
        var ticked = _.filter($scope.Cols, function (item) {
            return item.ticked == true;
        });

        if ($scope.HDMViewFeedback.FromDate == undefined && $scope.HDMViewFeedback.ToDate == undefined) {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1)) + '/' + firstDay.getDate() + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1)) + '/' + lastDay.getDate() + '/' + lastDay.getFullYear();

            $scope.HDMViewFeedback.FromDate = firstDayWithSlashes;
            $scope.HDMViewFeedback.ToDate = lastDayWithSlashes;
        }
        //var cp = _.find($scope.Company, { ticked: true })
        var params = {
            RequestID: $scope.HDMViewFeedback.RequestID,
            LCMlst: $scope.HDMViewFeedback.Clinic,
            MAINlst: $scope.HDMViewFeedback.Main_Category,
            SUBlst: $scope.HDMViewFeedback.Sub_Category,
            CHILDlst: $scope.HDMViewFeedback.Child,
            STATUSlst: $scope.HDMViewFeedback.Status,
            HSTSTATUS: $scope.HDMViewFeedback.selstatus,
            //Request_Type: $scope.HDMViewSubRequisitionsForm.Request_Type,
            FromDate: $scope.HDMViewFeedback.FromDate,
            ToDate: $scope.HDMViewFeedback.ToDate,
            //CompanyId: cp.CNP_ID
        };

        console.log(params);
        
        HDMViewFeedBackService.GetGriddataBySearch(params).then(function (response) {
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData(response.data);
                progress(0, 'Loading...', false);
            }
        });

    }, function (error) {
        console.log(error);
    }

    $scope.CustmPageLoad = function () {
    }, function (error) {
        console.log(error);
    }
    
    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMViewFeedback.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.HDMViewFeedback.Sub_Category).then(function (response) {
            $scope.Child = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getmainbysub = function () {
        HDMFeedBackService.getmainbysub($scope.HDMViewFeedback.Sub_Category[0].SUBC_CODE).then(function (response) {
            angular.forEach($scope.Main_Category, function (Main_Category) {
                Main_Category.ticked = false;
            });
            var MainLst = response.data;
            angular.forEach(MainLst, function (value, key) {
                var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
                if (main != undefined && main.ticked == false) {
                    main.ticked = true;
                }
            });
        }, function (error) {
            console.log(error);
        });
    }

    
});
