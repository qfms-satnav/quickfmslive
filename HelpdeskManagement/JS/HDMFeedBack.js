﻿app.service("HDMFeedBackService", function ($http, $q, UtilityService) {
    this.GetMainCat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBack/GetMainCat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.Getsubcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBack/Getsubcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    this.Getchildcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBack/Getchildcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    this.GetStatusList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBack/GetStatusList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    this.GetSubStatusList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBack/GetSubStatusList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    this.GetClinicMangerByLoc = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMFeedBack/GetClinicMangerByLoc', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.GetAOMByLoc = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMFeedBack/GetAOMByLoc', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    this.GetOTHERSByLoc = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMFeedBack/GetOTHERSByLoc', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.GetFdBackMode = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBack/GetFdBackMode')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.SaveFDBckDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMFeedBack/SaveFDBckDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.getmainbysub = function (code) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBack/getmainbysub?code=' + code + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

});

app.controller('HDMFeedBackController', function ($scope, $q, $http, HDMFeedBackService, UtilityService, $timeout, $filter, $window) {
    $scope.HDMFeedBackRequest = {};
    $scope.HDMFeedBackRequest.Child = [];
    $scope.locSelectAll = function () {
        $scope.HDMFeedBackRequest.Clinic = $scope.Clinic;
    }
    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMFeedBackRequest.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.HDMFeedBackRequest.Sub_Category).then(function (response) {
            $scope.Child = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.mainSelectAll = function () {
        $scope.HDMFeedBackRequest.Main_Category = $scope.Main_Category;
        $scope.getsubbymain();
    }
    $scope.subSelectAll = function () {
        $scope.HDMFeedBackRequest.Sub_Category = $scope.Sub_Category;
        $scope.getchildbysub();
    }
    $scope.childSelectAll = function () {
        $scope.HDMFeedBackRequest.Child = $scope.Child;
        $scope.getchildbysub();
    }
    $scope.getCMByLoc = function () {
        var param =
        {
            LCM_CODE: $scope.HDMFeedBackRequest.Clinic[0].LCM_CODE,
        };

        HDMFeedBackService.GetClinicMangerByLoc(param).then(function (response) {
            $scope.Clinic_Manager = response.data;
        }, function (error) {
            console.log(error);
        });
        HDMFeedBackService.GetAOMByLoc(param).then(function (response) {
            $scope.AOM = response.data;
        }, function (error) {
            console.log(error);
        });

        HDMFeedBackService.GetOTHERSByLoc(param).then(function (response) {
            $scope.OTHERS = response.data;
        }, function (error) {
            console.log(error);
        });

    };


    $scope.SaveFDBckDetails = function () {
        var statuscom = "";
        var Others = "";       
        var Aom = "";
        var ClinicManager = "";
       
        var Child = "";
        //console.log($scope.HDMFeedBackRequest.STATUSCOM);
        //console.log($scope.HDMFeedBackRequest.STATUSCOM.length);
        if ($scope.HDMFeedBackRequest.STATUSCOM.length == "0") {
            statuscom = "";
        }
        else {
            statuscom = $scope.HDMFeedBackRequest.STATUSCOM[0].SER_APP_STA_REMARKS_ID;
        }
        if ($scope.HDMFeedBackRequest.OTHERS.length == "0") {
            Others = "";
        }
        else {
            var selectedOthers = [];
            angular.forEach($scope.HDMFeedBackRequest.OTHERS, function (value, i) {
                selectedOthers[i] = value.OTH_ID;
            });
            Others = selectedOthers.join(",");
            //Others = $scope.HDMFeedBackRequest.OTHERS[0].OTH_ID;
        }
        if ($scope.HDMFeedBackRequest.Child.length == "0") {
            Child = "";
        }
        else {
            Child = $scope.HDMFeedBackRequest.Child[0].CHC_TYPE_NAME;
        }
        if ($scope.HDMFeedBackRequest.AOM.length == "0") {
            Aom = "";
        }
        else {
            Aom = $scope.HDMFeedBackRequest.AOM[0].AOM_ID;
        }
        if ($scope.HDMFeedBackRequest.Clinic_Manager.length == "0") {
            ClinicManager = "";
        }
        else {
            ClinicManager = $scope.HDMFeedBackRequest.Clinic_Manager[0].CLINIC_MGR_ID;
        }

        var params = {
            REQ_ID: $scope.HDMFeedBackRequest.Reqid,
            AUR_ID: $scope.HDMFeedBackRequest.Aurid,
            COMPLAINT_DATE: $scope.HDMFeedBackRequest.ComplaintDate,
            LCM_CODE: $scope.HDMFeedBackRequest.Clinic[0].LCM_CODE,
            //CLINIC_MGR_ID: $scope.HDMFeedBackRequest.Clinic_Manager[0].CLINIC_MGR_ID,
            CLINIC_MGR_ID: ClinicManager,
            CLIENTNAME: $scope.HDMFeedBackRequest.Client_Name,
            CONTACT: $scope.HDMFeedBackRequest.Contact,
            MAIN_CAT: $scope.HDMFeedBackRequest.Sub_Category[0].MNC_CODE,
            SUB_CAT: $scope.HDMFeedBackRequest.Sub_Category[0].SUBC_CODE,
            CHILD_CAT: Child,//$scope.HDMFeedBackRequest.Child[0].CHC_TYPE_CODE,
            AOM_ID: Aom, //$scope.HDMFeedBackRequest.AOM[0].AOM_ID,
            OTHERS: Others,//$scope.HDMFeedBackRequest.OTHERS[0].OTH_ID,
            STATUS: $scope.HDMFeedBackRequest.Status[0].SER_APP_STA_REMARKS_ID,
            MODECOMMENTS: $scope.HDMFeedBackRequest.ModeComments,
            MODE: $scope.HDMFeedBackRequest.MODE[0].MODE_ID,
            STATUSCOMMENTS: $scope.HDMFeedBackRequest.StatusComments,
            STATUSCOM: statuscom,
            PostedFiles: rowData,
            Data: data
            //Approvals: $scope.HDMRaiseSubRequest.Approvals[0].SER_APP_STA_ID,
            //CompanyId: cp.CNP_ID
        };
        var paramsdta = {
            FDBK_LIST: [],
            PostedFiles: rowData,
            REQ_ID: ''
        }

        angular.forEach($scope.HDMFeedBackRequest.Sub_Category, function (values) {
            var params1 = {
                REQ_ID: $scope.HDMFeedBackRequest.Reqid,
                AUR_ID: $scope.HDMFeedBackRequest.Aurid,
                COMPLAINT_DATE: $scope.HDMFeedBackRequest.ComplaintDate,
                LCM_CODE: $scope.HDMFeedBackRequest.Clinic[0].LCM_CODE,
                //CLINIC_MGR_ID: $scope.HDMFeedBackRequest.Clinic_Manager[0].CLINIC_MGR_ID,
                CLINIC_MGR_ID: ClinicManager,
                CLIENTNAME: $scope.HDMFeedBackRequest.Client_Name,
                CONTACT: $scope.HDMFeedBackRequest.Contact,
                MAIN_CAT: $scope.HDMFeedBackRequest.Sub_Category[0].MNC_CODE,
                SUB_CAT: values.SUBC_CODE,
                CHILD_CAT: Child,
                AOM_ID: Aom,
                OTHERS: Others,
                STATUS: $scope.HDMFeedBackRequest.Status[0].SER_APP_STA_REMARKS_ID,
                MODECOMMENTS: $scope.HDMFeedBackRequest.ModeComments,
                MODE: $scope.HDMFeedBackRequest.MODE[0].MODE_ID,
                STATUSCOMMENTS: $scope.HDMFeedBackRequest.StatusComments,
                STATUSCOM: statuscom
            };
            paramsdta.FDBK_LIST.push(params1);
        });
        console.log(params);
        if ($scope.HDMFeedBackRequest.ComplaintDate != undefined && $scope.HDMFeedBackRequest.Clinic[0].LCM_CODE != undefined && //$scope.HDMFeedBackRequest.Clinic_Manager[0].CLINIC_MGR_ID != undefined &&
            $scope.HDMFeedBackRequest.Client_Name != undefined && $scope.HDMFeedBackRequest.Contact != undefined && $scope.HDMFeedBackRequest.Sub_Category[0].MNC_CODE != undefined &&
            $scope.HDMFeedBackRequest.Sub_Category[0].SUBC_CODE != undefined && $scope.HDMFeedBackRequest.Status[0].SER_APP_STA_REMARKS_ID != undefined && // $scope.HDMFeedBackRequest.AOM[0].AOM_ID != undefined
            $scope.HDMFeedBackRequest.ModeComments != undefined)    //&& $scope.HDMFeedBackRequest.STATUSCOM[0].SER_APP_STA_REMARKS_ID != undefined&& $scope.HDMFeedBackRequest.StatusComments != undefined
        {
            HDMFeedBackService.SaveFDBckDetails(paramsdta).then(function (response) {
                $.ajax({
                    type: "POST",
                    url: 'https://live.quickfms.com/api/HDMFeedBack/UploadFiles',    // CALL WEB API TO SAVE THE FILES.
                    //enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                    cache: false,
                    data: params.Data, 		        // DATA OR FILES IN THIS CONTEXT.
                    success: function (data, textStatus, xhr) {
                        showNotification('success', 8, 'bottom-right', response.Message);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }
                });
                $scope.Clear();
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.Clear = function () {

        angular.forEach($scope.Clinic, function (Clinic) {
            Clinic.ticked = false;
        });
        angular.forEach($scope.Main_Category, function (Main_Category) {
            Main_Category.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (Sub_Category) {
            Sub_Category.ticked = false;
        });
        angular.forEach($scope.Child, function (Child) {
            Child.ticked = false;
        });
        angular.forEach($scope.Status, function (Status) {
            Status.ticked = false;
        });
        angular.forEach($scope.STATUSCOM, function (STATUSCOM) {
            STATUSCOM.ticked = false;
        });
        angular.forEach($scope.MODE, function (MODE) {
            MODE.ticked = false;
        });
        angular.forEach($scope.Clinic_Manager, function (Clinic_Manager) {
            Clinic_Manager.ticked = false;
        });
        angular.forEach($scope.AOM, function (AOM) {
            AOM.ticked = false;
        });
        angular.element("input[type='file']").val(null);
        $scope.HDMFeedBackRequest = {};
        $scope.HDMFeedBack.$submitted = false;
        rowData = [];
    }

    $scope.PageLoad = function () {

        UtilityService.GetLocationsall(5).then(function (response) {
            if (response.data != null) {
                $scope.Clinic = response.data;
                $scope.HDMFeedBackRequest.Clinic = $scope.Clinic;
            }
        });

        HDMFeedBackService.GetMainCat().then(function (response) {
            if (response.data != null) {
                $scope.Main_Category = response.data;
            }
        });
        //HDMFeedBackService.Getsubcat().then(function (response) {
        //    if (response.data != null) {
        //        $scope.Sub_Category = response.data;
        //    }
        //});
        HDMFeedBackService.GetStatusList().then(function (response) {
            if (response.data != null) {
                $scope.Status = response.data;
            }
        });

        HDMFeedBackService.GetSubStatusList().then(function (response) {
            if (response.data != null) {
                $scope.STATUSCOM = response.data;
            }
        });
        HDMFeedBackService.GetFdBackMode().then(function (response) {
            if (response.data != null) {
                $scope.MODE = response.data;
            }
        });
    }
    $scope.PageLoad();

    $scope.getmainbysub = function () {
        HDMFeedBackService.getmainbysub($scope.HDMFeedBackRequest.Sub_Category[0].SUBC_CODE).then(function (response) {
            angular.forEach($scope.Main_Category, function (Main_Category) {
                Main_Category.ticked = false;
            });
            var MainLst = response.data;
            angular.forEach(MainLst, function (value, key) {
                var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
                if (main != undefined && main.ticked == false) {
                    main.ticked = true;
                }
            });
        }, function (error) {
            console.log(error);
        });
    }

});