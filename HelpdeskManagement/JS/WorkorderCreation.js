﻿app.service("WorkorderService", function ($http, $q, UtilityService) {

    this.HelpdeskRequists = function () {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Workorder/HelpdeskRequists')
            .then(function (response) {
                //console.log(response.data);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.maintenanceRequests = function () {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Workorder/maintenanceRequests')
            .then(function (response) {
                //console.log(response.data);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetResource = function (resoure) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Workorder/GetResource?Reqid=' + resoure + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetResourcemaint = function (resoure) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Workorder/GetMaintResource?Reqid=' + resoure + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGriddata = function (workorder) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Workorder/GetSubmitDetails', workorder)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetManitGriddata = function (workorder) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Workorder/GetMaintSubmitDetails', workorder)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getSparePartsData = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Workorder/getSparePartsData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetAssets = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Workorder/GetAssets')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('WorkorderController', function ($scope, $q, WorkorderService, UtilityService, $filter, $parse, $window) {

    $scope.Workorder = {};
    $scope.selectedRequestids = [];
    $scope.Assets = [];
    $scope.Spareparts = [];
    $scope.Resource = [];
    $scope.WO = [];
    $scope.selectedItems = [];
    $("#rdt_main").attr("checked", false);
    $("#rbt_hdm").prop('checked', true);
    $('#MainGrid').hide();
    $scope.Clear = function () {
        $scope.gridOptions.rowData = [];
        $scope.gridOptionsMain.rowData = [];

    };
    WorkorderService.GetAssets().then(function (response) {
        $scope.Assets = response.data;
        $scope.getSparePartsData();
    });
    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = true;
                        $scope.tempspace = value;
                        $scope.tempspace.STACHECK = UtilityService.Added;
                        $scope.selectedRequestids.push($scope.tempspace);
                        console.log($scope.selectedRequestids);
                        $scope.tempspace = {};
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = false;
                        _.remove($scope.selectedRequestids, _.find($scope.selectedRequestids, { SER_REQ_ID: value.SER_REQ_ID }));
                        value.STACHECK = UtilityService.Deleted;
                        $scope.selectedRequestids.push(value);
                    });
                });
            }
        });
        return eHeader;
    }
    var columnDefs = [
        { headerName: "Select All", field: "ticked", width: 90, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox'   ng-model='data.ticked' ng-change='bindgridRaiseWorkorder()'/>", headerCellRenderer: headerCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Request Id", field: "SER_REQ_ID", width: 180, cellClass: "grid-align", pinned: 'left', filter: 'set', suppressMenu: true },
        { headerName: "Requested Date", field: "SER_CAL_LOG_DT", cellClass: "grid-align" },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align" },
        { headerName: "Status", field: "SER_STATUS", cellClass: "grid-align" },
        { headerName: "Urgency", field: "UGC_NAME", cellClass: "grid-align" },
        { headerName: "Description", field: "SER_PROB_DESC", cellClass: "grid-align" },
        { headerName: "Impact", field: "IMP_NAME", cellClass: "grid-align" },
        { headerName: "Main Category", field: "MNC_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Sub Category", field: "SUBC_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 180, cellClass: "grid-align" },
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        enableColResize: true,
        //rowSelection: 'multiple',
    };

    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);
        WorkorderService.HelpdeskRequists().then(function (response) {
            if (response.data != null) {
                $scope.gridata = response.data;
                $scope.gridOptions.api.setRowData($scope.gridata);
                $('#MAINfrm').hide();
            }
            progress(0, 'Loading...', false);
        });
    };

    setTimeout(function () {
        $scope.LoadDetails();

    }, 500);
    function maintheaderCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptionsMain.rowData, function (value, key) {
                        value.ticked = true;
                        $scope.tempspace = value;
                        $scope.tempspace.STACHECK = UtilityService.Added;
                        $scope.selectedRequestids.push($scope.tempspace);
                        console.log($scope.selectedRequestids);
                        $scope.tempspace = {};
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptionsMain.rowData, function (value, key) {
                        value.ticked = false;
                        _.remove($scope.selectedRequestids, _.find($scope.selectedRequestids, { PVD_ID: value.PVD_ID }));
                        value.STACHECK = UtilityService.Deleted;
                        $scope.selectedRequestids.push(value);
                    });
                });
            }
        });
        return eHeader;
    }
    var columnDefsmain = [
        {
            headerName: "Select All", field: "ticked", width: 90, cellClass: 'grid-align', filter: 'set',
            cellRenderer: function (params) {
                var input = document.createElement('input');
                input.type = "checkbox";
                input.checked = params.value;
                input.addEventListener('click', function (event) {
                    input.checked != input.checked;
                    params.data.ticked = input.checked;
                    $('#MAINfrm').show();
                    var d = _.filter($scope.Maintgridata, function (o) { return o.ticked == true; });
                    // console.log(d);
                    $scope.gridMaintRaise.api.setRowData(d);
                    angular.forEach(d, function (value, key) {
                        
                        WorkorderService.GetResourcemaint(value.PVD_ASSET_CODE).then(function (response) {
                            //console.log(response.data);
                        $scope.MaintResource = response.data;
                            angular.forEach(d, function (value, key) {
                                if ($scope.gridMaintRaise.api.inMemoryRowController.rowsAfterFilter.length != 0) {
                                    $('#MAINfrm').show();
                                }
                                else {
                                    $('#MAINfrm').hide();
                                }
                            });
                        });
                    });
                });
                return input;
            },
            headerCellRenderer: maintheaderCellRendererFunc, suppressMenu: true, pinned: 'left'
        },
        { headerName: "PVD_ID", field: "PVD_ID", width: 180, cellClass: "grid-align", hide: true },
        { headerName: "Plan id", field: "PVD_PLAN_ID", width: 150, cellClass: "grid-align" },
        { headerName: "Project ", field: "TAG_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Asset Code", field: "PVD_ASSET_CODE", width: 150, cellClass: "grid-align" },
        { headerName: "Asset Location", field: "lcm_name", width: 150, cellClass: "grid-align" },
        { headerName: "Schedule Date", field: "PVD_PLANSCHD_DT", width: 150, cellClass: "grid-align" },
        { headerName: "Category", field: "VT_TYPE", width: 150, cellClass: "grid-align" },
        { headerName: "Sub Category", field: "AST_SUBCAT_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Brand", field: "manufacturer", width: 150, cellClass: "grid-align" },
    ];

    $scope.gridOptionsMain = {
        columnDefs: columnDefsmain,
        enableCellSelection: false,
        rowData: null,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true
    };

    $scope.LoadMaintenceDetails = function () {
        progress(0, 'Loading...', true);
        WorkorderService.maintenanceRequests().then(function (response) {
            if (response.data != null) {
                $scope.Maintgridata = response.data;
                $scope.gridOptionsMain.api.setRowData(response.data);
            }
            progress(0, 'Loading...', false);
        });
    };

    //setTimeout(function () {
    //    $scope.LoadMaintenceDetails();

    //}, 500);


    $('#rdt_main').change(function () {
        $scope.LoadMaintenceDetails();
        $('#HDMGrid').hide();
        $('#MainGrid').show();
        $('#HDMfrm').hide();
        $('#MAINfrm').hide();

    });
    $('#rbt_hdm').change(function () {
        $('#HDMGrid').show();
        $('#MainGrid').hide();
        $('#MAINfrm').hide();
    });



    $scope.AddWorkorder = [
        { headerName: "Select All", field: "ticked", width: 90, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox'   ng-model='data.ticked' ng-change='bindgridRaiseWorkorder()'/>", headerCellRenderer: headerCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Request Id", field: "SER_REQ_ID", cellClass: "grid-align" },
        { headerName: "Location", field: "CHC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Child Category", field: "LCM_NAME", cellClass: "grid-align" },
        {
            headerName: "Resource", field: "AST_REsource", cellClass: "grid-align", filter: 'set', suppressMenu: true, width: 200, 
            template: "<select ng-click='bindresource(data.SER_REQ_ID,data.AUR_KNOWN_AS)' ng-model='data.AUR_KNOWN_AS' style='height:31px;'  ><option value=''>--Select--</option><option ng-repeat='Ty in Resource' value='{{Ty.AUR_KNOWN_AS}}'>{{Ty.AUR_KNOWN_AS}} </option></select> "
        },
        {
            headerName: "Asset Name", field: "AAT_DESC", cellClass: "grid-align", filter: 'set', suppressMenu: true, width: 250, cellStyle: { "overflow": 'visible' },
            cellRenderer: customEditor
        },
        {
            headerName: "Spare Parts", field: "AAS_SPAREPART_NAME", cellClass: "grid-align", filter: 'set', width: 200, suppressMenu: true, cellStyle: { "overflow": 'visible' },
            cellRenderer: customEditor
            //template:
            //    "<select ng-model='data.AAS_SPAREPART_ID'><option value=''>--Select--</option><option ng-repeat='Ty in Spareparts' value='{{Ty.AAS_SPAREPART_ID}}'>{{Ty.AAS_SPAREPART_NAME}}</option> </select>"

        },

        { headerName: "Cost", field: "AST_COST", cellClass: "grid-align", filter: 'set', width: 200,suppressMenu: true, cellRenderer: customEditor },
        //{ headerName: "Delete", cellclass: "grid-align", suppressMenu: true, template: '<a ng-click="ReDelete(data.SER_REQ_ID)"><i class="fa fa-times" aria-hidden="true"></i></a>', width: 50 }
    ];
    var name, model;
    function customEditor(params) {

        var eCell = document.createElement('span');
        var index = params.rowIndex;

        if (params.column.colId === "AAT_DESC") {
            name = 'Assets' + index;
            model = $parse(name);
            $scope.AST = angular.copy($scope.Assets);
            model.assign($scope, $scope.AST);
            eCell.innerHTML = '<div isteven-multi-select data-input-model="' + name + '"  data-output-model="AST.Assets" data-button-label="AAT_DESC" data-item-label="AAT_DESC"  data-on-item-click="getSparepartsByAsset(0)" data-on-select-all="ASTChangeAll(' + index + ')" data-on-select-none="ASTChangeNone(' + index + ')"  data-tick-property="ticked" data-max-labels="1" ></div >';
            angular.forEach($scope.AST, function (val) {
                val.rowvalue = index;
            });
        }
        if (params.column.colId === "AAS_SPAREPART_NAME") {
            name = 'Spares' + index;
            model = $parse(name);
            $scope.Spare = [];
            model.assign($scope, $scope.Spare);
            eCell.innerHTML = '<div isteven-multi-select data-input-model="' + name + '"  data-output-model="AST.SpareParts" data-button-label="AAS_SPAREPART_NAME" data-item-label="AAS_SPAREPART_NAME"  data-on-item-click="getSparePartDetails(' + index + ')" data-on-select-all="ASTSpareAll(' + index + ')" data-on-select-none="ASTSpareNone(' + index + ')"  data-tick-property="ticked" data-max-labels="1" ></div >';
            angular.forEach($scope.AST, function (val) {
                val.rowvalue = index;
            });
        }
        if (params.column.colId === "AST_COST") {
            name = 'Cost' + index;
            model = $parse(name);
            $scope.cost = [];
            model.assign($scope, $scope.cost);
            eCell.innerHTML = '<input type="textbox" ng-model="' + name + ' | number : 2" step="0.01" style="height:31px">'
        }

        return eCell;

    }
    $scope.getSparePartsData = function () {
        WorkorderService.getSparePartsData().then(function (response) {
            if (response != null) {
                $scope.Spareparts = response;
            };
        });
    };
    var predicate = function () {
        var args = _.toArray(arguments);
        return function (sparepart) {
            var equalToSparePartAsset = _.partial(_.isEqual, sparepart.AAS_AAT_CODE);
            return args.some(equalToSparePartAsset);
        };
    };
    $scope.bindresource = function (ind, AUR_KNOWN_AS) {

        angular.forEach($scope.gridRaiseWorkorder.rowData, function (val5, key) {
            if (val5.req_id == ind) {
                $scope.gridRaiseWorkorder.rowData[key].resouce = AUR_KNOWN_AS;
            }
        });
        //console.log($scope.gridRaiseWorkorder.rowData);
    }
    var tt = [];
    $scope.getSparePartDetails = function (re) {
        var name, model, quanty = 0, cost = 0, total = 0;
        $scope.cost = 0;
        tt = $scope.gridRaiseWorkorder.rowData[re].AST;
        if ($scope.gridRaiseWorkorder.rowData[re].AST != undefined) {
            tt.SpareParts = $scope.AST.SpareParts;
            $scope.gridRaiseWorkorder.rowData[re].AST = tt;
        }
        else {
            $scope.gridRaiseWorkorder.rowData[re].AST = ($scope.AST);
        }

        angular.forEach($scope.AST.SpareParts, function (val3) {
            quanty = 0; cost = 0;
            quanty = val3.AAS_SPAREPART_COUNT + quanty;
            cost = val3.AAS_SPAREPART_COST + cost;
            total = (cost / quanty) + total;
        });


        $scope.gridRaiseWorkorder.rowData[re].total = total;
        if ($scope.AST.SpareParts.length != 0) {
            if ($scope.AST.SpareParts[0].rowvalue == undefined) {
                name = 'Cost' + re;
            }
            else {
                name = 'Cost' + $scope.AST.SpareParts[0].rowvalue;
            }
        }
        else {
            if ($scope.AST.SpareParts[0].rowvalue == undefined) {
                name = 'Cost' + re;
            }
            else {
                name = 'Cost' + $scope.AST.Assets[0].rowvalue;
            }
        }
        model = $parse(name);
        $scope.cost = angular.copy(total);
        model.assign($scope, $scope.cost);
        console.log($scope.gridRaiseWorkorder.rowData);
        //tt = $scope.AST;
        $scope.AST = [];
        // console.log($scope.gridRaiseWorkorder.rowData);
    };
    $scope.getSparepartsByAsset = function (stsid) {
        var name, model;
        $scope.Spare.Spares = [];
        angular.forEach($scope.AST.Assets, function (val1) {
            angular.forEach($scope.Spareparts, function (val2) {
                if (val1.AAT_CODE == val2.AAS_AAT_CODE) {
                    $scope.Spare.Spares.push({
                        "AAS_AAT_CODE": val2.AAS_AAT_CODE, "AAS_SPAREPART_ID": val2.AAS_SPAREPART_ID,
                        "AAS_SPAREPART_NAME": val2.AAS_SPAREPART_NAME, "AAS_SPAREPART_COST": val2.AAS_SPAREPART_COST,
                        "AAS_STA_ID": val2.AAS_STA_ID, "AAS_SPAREPART_COUNT": val2.AAS_SPAREPART_COUNT,
                        "AAT_ID": val2.AAT_ID, "rowvalue": val1.rowvalue
                    });
                }
            });
        });
        //if ($scope.AST.SpareParts.length != 0) {
        //    name = 'Spares' + $scope.AST.Assets[0].rowvalue;
        //}
        //else {
        //    name = 'Cost' + $scope.AST.rowvalue;
        //}
        //name = 'Spares' + $scope.AST.Assets[0].rowvalue;
        if (stsid == "1") {
            name = 'Spares' + SelectAllIndex;
        }
        else {
            name = 'Spares' + $scope.AST.Assets[0].rowvalue;
        }
        model = $parse(name);
        $scope.Spare = angular.copy($scope.Spare.Spares);
        model.assign($scope, $scope.Spare);
    };
    var SelectAllIndex = "";
    $scope.ASTChangeAll = function (id) {
        SelectAllIndex = id;
        $scope.AST.Assets = $scope.Assets;
        $scope.getSparepartsByAsset("1");
    }
    $scope.ASTChangeNone = function () {
        $scope.AST.Assets = [];
        //$scope.Spare.Spares = [];
        $scope.getSparepartsByAsset("1");
    }

    $scope.ASTSpareAll = function (re) {
        $scope.AST.SpareParts = $scope.Spare;
        $scope.getSparePartDetails(re);
    }
    $scope.ASTSpareNone = function (re) {
        $scope.AST.SpareParts = [];
        $scope.getSparePartDetails(re);
    }
    $scope.gridRaiseWorkorder = {
        columnDefs: $scope.AddWorkorder,
        onReady: function () {
            $scope.gridRaiseWorkorder.api.sizeColumnsToFit();
        },
        angularCompileRows: true,
        rowHeight: 35,
        rowData: null,
    };
    $scope.MaintWorkorder = [
        { headerName: "Select All", field: "ticked", width: 90, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox'   ng-model='data.ticked' />", headerCellRenderer: headerCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Asset Code", field: "PVD_ASSET_CODE", cellClass: "grid-align" },
        { headerName: "Plan Id", field: "PVD_PLAN_ID", cellClass: "grid-align" },
        { headerName: "Asset Name", field: "AAT_DESC", width: 100, cellClass: "grid-align" },
        { headerName: "Scheduled Date", field: "PVD_PLANSCHD_DT", cellClass: "grid-align" },
        {
            headerName: "Resource", field: "AST_REsource", cellClass: "grid-align", filter: 'set', width: 100, suppressMenu: true,
            template: "<select  ng-model='data.AVR_CODE' style='height:31px;'   ><option value=''>--Select--</option><option  ng-repeat='Ty in MaintResource' value='{{Ty.AVR_CODE}}'>{{Ty.AVR_NAME}}  </option></select>  "
        },
        {
            headerName: "Spare Parts", field: "AAS_SPAREPART_NAME", cellClass: "grid-align", filter: 'set', width: 130, suppressMenu: true, cellStyle: { "overflow": 'visible' },
            cellRenderer: customEditor1
            //template:
            //    "<select ng-model='data.AAS_SPAREPART_ID'><option value=''>--Select--</option><option ng-repeat='Ty in Spareparts' value='{{Ty.AAS_SPAREPART_ID}}'>{{Ty.AAS_SPAREPART_NAME}}</option> </select>"

        },
        { headerName: "Cost", field: "AST_COST", cellClass: "grid-align", filter: 'set', suppressMenu: true, width: 100, cellRenderer: customEditor1 },
       // { headerName: "Delete", cellclass: "grid-align", suppressMenu: true, template: '<a ng-click="MaintReDelete(data.PVD_ID)"><i class="fa fa-times" aria-hidden="true"></i></a>', width: 50 }
    ];
    function customEditor1(params) {

        var eCell = document.createElement('span');
        var index = params.rowIndex;
        if (params.column.colId === "AAS_SPAREPART_NAME") {
            var name, model;
            $scope.Spare = [];
            $scope.Spare.Spares1 = [];
            $scope.MaintSpareParts = [];
            angular.forEach($scope.Spareparts, function (val2) {
                if (params.data.PVD_ASSET_CODE == val2.AAS_AAT_CODE) {
                    $scope.Spare.Spares1.push({
                        "AAS_AAT_CODE": val2.AAS_AAT_CODE, "AAS_SPAREPART_ID": val2.AAS_SPAREPART_ID,
                        "AAS_SPAREPART_NAME": val2.AAS_SPAREPART_NAME, "AAS_SPAREPART_COST": val2.AAS_SPAREPART_COST,
                        "AAS_STA_ID": val2.AAS_STA_ID, "AAS_SPAREPART_COUNT": val2.AAS_SPAREPART_COUNT,
                        "AAT_ID": val2.AAT_ID
                    });
                }
                else if
                    (val2.AAC_CON_CATID == val2.AAT_AAG_CODE) {
                    $scope.Spare.Spares1.push({
                        "AAS_AAT_CODE": val2.AAS_AAT_CODE, "AAS_SPAREPART_ID": val2.AAS_SPAREPART_ID,
                        "AAS_SPAREPART_NAME": val2.AAS_SPAREPART_NAME, "AAS_SPAREPART_COST": val2.AAS_SPAREPART_COST,
                        "AAS_STA_ID": val2.AAS_STA_ID, "AAS_SPAREPART_COUNT": val2.AAS_SPAREPART_COUNT,
                        "AAT_ID": val2.AAT_ID
                    });
                }
              
            });
            name = 'Spares' + index;
            model = $parse(name);
            $scope.Spare = angular.copy($scope.Spare.Spares1);
            model.assign($scope, $scope.Spare);
            eCell.innerHTML = '<div isteven-multi-select data-input-model="' + name + '"  data-output-model="Spare.MaintSpareParts" data-button-label="AAS_SPAREPART_NAME" data-item-label="AAS_SPAREPART_NAME"  data-on-item-click="getMaintSpareCost(' + index + ')" data-on-select-all="MainSpareAll(' + index + ')" data-on-select-none="MainSpareNone(' + index + ')"  data-tick-property="ticked" data-max-labels="1" ></div >';
            angular.forEach($scope.AST, function (val) {
                val.rowvalue = index;
            });
        }
        if (params.column.colId === "AST_COST") {
            name = 'Cost' + index;
            model = $parse(name);
            $scope.cost = [];
            model.assign($scope, $scope.cost);
            eCell.innerHTML = '<input type="textbox" ng-model="' + name + ' | number : 2" step="0.01" style="height:31px">'
        }
        return eCell;
    }
    $scope.getMaintSpareCost = function (re) {
        var name, model, maintquanty = 0, maintcost = 0, mainttotal = 0;
        $scope.cost = 0;
        angular.forEach($scope.Spare.MaintSpareParts, function (val3) {
            maintquanty = 0; maintcost = 0;
                maintquanty = val3.AAS_SPAREPART_COUNT + maintquanty;
                maintcost = val3.AAS_SPAREPART_COST + maintcost;
                mainttotal = (maintcost / maintquanty) + mainttotal;
        });
        $scope.gridMaintRaise.rowData[re].MaintSpareParts = $scope.Spare.MaintSpareParts;
        $scope.gridMaintRaise.rowData[re].MaintTotal = mainttotal;
        name = 'Cost' + re;
        model = $parse(name);
        $scope.cost = angular.copy(mainttotal);
        model.assign($scope, $scope.cost);
       // console.log($scope.gridMaintRaise.rowData);
    };
    $scope.MainSpareAll = function (re) {
        $scope.Spare.MaintSpareParts = $scope.Spare;
        $scope.getMaintSpareCost(re);
    }
    $scope.MainSpareNone = function (re) {
        $scope.Spare.MaintSpareParts = [];
        $scope.getMaintSpareCost(re);
    }
    $scope.gridMaintRaise = {
        columnDefs: $scope.MaintWorkorder,
        onReady: function () {
            $scope.gridMaintRaise.api.sizeColumnsToFit();
        },
        angularCompileRows: true,
        rowHeight : 35,
        rowData: null,
    };
    $scope.bindgridRaiseWorkorder = function () {
       
        var d = _.filter($scope.gridata, function (o) { return o.ticked == true; });
        $scope.gridRaiseWorkorder.api.setRowData(d);
        angular.forEach(d, function (value, key) {
            WorkorderService.GetResource(value.SER_REQ_ID).then(function (response) {
                $scope.Resource = response.data;
                angular.forEach(d, function (value, key) {
                    if ($scope.gridRaiseWorkorder.api.inMemoryRowController.rowsAfterFilter.length != 0) {
                        $('#HDMfrm').show();
                    }
                    else {
                        $('#HDMfrm').hide();
                    }
                });
            });
        });
    };

    //$scope.bindgridMaintWorkorder = function () {       
    //    console.log($scope.Maintgridata);
    //    var d = _.filter($scope.Maintgridata, function (o) { return o.ticked == true; });
    //    $scope.gridMaintRaise.api.setRowData(d);
    //    angular.foreach(d, function (value, key) {
    //        WorkorderService.GetResource(value.SER_REQ_ID).then(function (response) {
    //            $scope.Resource = response.data;
    //            angular.forEach(d, function (value, key) {
    //                if ($scope.gridMaintRaise.api.inMemoryRowController.rowsAfterFilter.length != 0) {
    //                    $('#MAINfrm').show();
    //                }
    //                else {
    //                    $('#MAINfrm').hide();
    //                }
    //            });
    //        });
    //    });
    //};
    $scope.SavingData = function () {
        //console.log($scope.gridRaiseWorkorder.rowData);
        if ($scope.createworkorder.SRN_REQ_REM == undefined) {
            return;
        }
        if ($scope.frmCreateWorkorder.Terms_Conditions == undefined) {
            return;
        }
        $scope.workorder = [];
        $scope.Assetspare = [];
        angular.forEach($scope.gridRaiseWorkorder.rowData, function (val1, key) {
            var i = 0;
            angular.forEach($scope.gridRaiseWorkorder.rowData[key].AST.Assets, function (val2) {
                angular.forEach($scope.gridRaiseWorkorder.rowData[key].AST.SpareParts, function (val3) {
                    if (val2.AAT_CODE == val3.AAS_AAT_CODE) {
                        $scope.Assetspare.push({
                            "Reqid": $scope.gridRaiseWorkorder.rowData[key].SER_REQ_ID, "Asset_code": val2.AAT_CODE, "Asset_Name": val2.AAT_DESC, "SpareName": val3.AAS_SPAREPART_NAME,
                            "Sparecost": val3.AAS_SPAREPART_COST, "sparecount": val3.AAS_SPAREPART_COUNT, "indcost": (val3.AAS_SPAREPART_COST / val3.AAS_SPAREPART_COUNT),
                            "CHC_TYPE_NAME": val1.CHC_TYPE_NAME, "CHC_TYPE_CODE": val1.CHC_TYPE_CODE, "LCM_NAME": val1.LCM_NAME,
                            "Total": val1.total, "SUBC_CODE": val1.SUBC_CODE, "SUBC_NAME": val1.SUBC_NAME, "MNC_NAME": val1.MNC_NAME, "MNC_CODE": val1.MNC_CODE,
                            "LCM_CODE": val1.LCM_CODE, "Resource": val1.AUR_KNOWN_AS
                        });
                    }
                });
            });

            //angular.forEach($scope.gridRaiseWorkorder.rowData[key].AST.Assets, function (val2) {
            //    asset[i] = val2.AAT_DESC;
            //    assetcode[l] =val2.AAT_CODE;
            //    i++;
            //});
            //var assetname = asset.join(",");
        });
        //console.log($scope.Assetspare);
        var data = {
            workorder: $scope.Assetspare,
            Remarks: $scope.createworkorder.SRN_REQ_REM,
            termsandcdtns: $scope.frmCreateWorkorder.Terms_Conditions,
            workorderid: "",
        }
        progress(0, 'Loading...', true);
        WorkorderService.GetGriddata(data).then(function (response) {
            var data = {
                workorderid: response.data,
                Remarks: $scope.createworkorder.SRN_REQ_REM,
                termsandcdtns: $scope.frmCreateWorkorder.Terms_Conditions,
                id: 1,
            }
            //console.log(data1);
            //console.log(JSON.stringify(data1));
            $window.location.href = '/HelpdeskManagement/Views/WorkorderReport.aspx?rid=' + JSON.stringify(data);
            progress(0, 'Loading...', false);
        });
    }
    $scope.SavingMaintData = function () {
        //console.log($scope.gridMaintRaise.rowData);
        $scope.MaintAssetspare = [];
        angular.forEach($scope.gridMaintRaise.rowData, function (val1, key) {
            angular.forEach($scope.gridMaintRaise.rowData[key].MaintSpareParts, function (val3) {
            $scope.MaintAssetspare.push({
                    "PVD_ID": val1.PVD_ID, "PVD_PLAN_ID": val1.PVD_PLAN_ID, "PVD_ASSET_CODE": val1.PVD_ASSET_CODE, "PVD_PLANSCHD_DT": val1.PVD_PLANSCHD_DT,
                    "PVM_GRP_ID": val1.PVM_GRP_ID, "PVM_GRP_TYPE_ID": val1.PVM_GRP_TYPE_ID, "PVM_BRND_ID": val1.PVM_BRND_ID, "AAT_DESC": val1.AAT_DESC,
                "MaintTotal": val1.MaintTotal, "Resourceid": val1.AVR_CODE, "cost": (val3.AAS_SPAREPART_COST / val3.AAS_SPAREPART_COUNT),
                "Sparename": val3.AAS_SPAREPART_NAME,  "Spareid": val3.AAS_SPAREPART_ID,//"sparedetails": $scope.mainSpareparts
            });
            });
        });
        //console.log($scope.MaintAssetspare);
        var data = {
            maintworkorder: $scope.MaintAssetspare,
            Remarks: $scope.MAINworkorder.Remarks,
            termsandcdtns: $scope.MAINworkorder.TermsConditions,
            Maintworkorderid: "",
        }
        progress(0, 'Loading...', true);
        WorkorderService.GetManitGriddata(data).then(function (response) {

            var data1 = {
                workorderid: response.data,
                Remarks: $scope.MAINworkorder.Remarks,
                termsandcdtns: $scope.MAINworkorder.TermsConditions,
                id:2,
            }
            //console.log(data1);
            //console.log(JSON.stringify(data1));
            $window.location.href = '/HelpdeskManagement/Views/WorkorderReport.aspx?rid=' + JSON.stringify(data1);
            progress(0, 'Loading...', false);
        });
    }



    $scope.ReDelete = function (data) {
        var d = _.find($scope.gridOptions.rowData, function (o) { return o.SER_REQ_ID == data; });
        d.ticked = false;
        var c = _.find($scope.gridOptions.rowData, function (o) { return o.ticked == true; });

        $scope.gridRaiseWorkorder.api.setRowData([c]);
        if (c != undefined) {
            $('#HDMfrm').show();
        }
        else {
            $('#HDMfrm').hide();
        }
    };
    $scope.MaintReDelete = function (data) {
        var d = _.find($scope.gridOptionsMain.rowData, function (o) { return o.PVD_ID == data; });
        d.ticked = false;
        var c = _.find($scope.gridOptionsMain.rowData, function (o) { return o.ticked == true; });

        $scope.gridMaintRaise.api.setRowData([c]);
        if (c != undefined) {
            $('#MAINfrm').show();
        }
        else {
            $('#MAINfrm').hide();
        }
    };



});