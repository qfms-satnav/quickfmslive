﻿

app.service("FacilityService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();
    this.getFacility = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDFacility')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //SAVE
    this.saveFacility = function (facility) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Facility/Create', facility)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //UPDATE BY ID
    this.updateFacility = function (facility) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Facility/UpdateFacilityData', facility)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //Bind Grid
    this.GetFacilityBindGrid = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Facility/GetFacilityGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('FacilityController', ['$scope', '$q', 'FacilityService', '$http', 'UtilityService', '$timeout', function ($scope, $q, FacilityService, $http, UtilityService, $timeout) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.HDFacility = {};
    $scope.locationlist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.RegExpName = UtilityService.RegExpName;
    $scope.RegExpRemarks = UtilityService.RegExpRemarks;
    $scope.RegExpCode = UtilityService.RegExpCode;

    //to bind location
    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.locationlist = response.data;
            $scope.locationlist[0].ticked = true;
            angular.forEach($scope.locationlist[0], function (value, key) {
                value.ticked = true;
            });
        }
    });
    //to Save the data
    $scope.Save = function () {
        if ($scope.IsInEdit) {

            FacilityService.updateFacility($scope.HDFacility).then(function (facility) {
                var updatedobj = {};
                angular.copy($scope.HDFacility, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully uploaded";
                $scope.LoadData();
                $scope.IsInEdit = false;
                $scope.ClearData();
                setTimeout(function () {
                    $scope.$apply(function () {
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.HDFacility.FD_STA_ID = "1";
            FacilityService.saveFacility($scope.HDFacility).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully inserted";
                var savedobj = {};
                angular.copy($scope.HDFacility, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.HDFacility = {};
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;

                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    //for GridView
    var columnDefs = [
        { headerName: "Facility Code", field: "FD_CODE", width: 120, cellClass: 'grid-align' },
        { headerName: "Facility Name", field: "FD_Name", width: 160, cellClass: 'grid-align' },
        { headerName: "Location", field: "FD_LOCATION", width: 160, cellClass: 'grid-align' },
        { headerName: "Cost", field: "COST", width: 160, cellClass: 'grid-align' },
        { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.FD_STA_ID)}}", width: 100, cellClass: 'grid-align' },
        { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, onmouseover: "cursor: hand (a pointing hand)" }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        FacilityService.GetFacilityBindGrid().then(function (data) {
            $scope.gridata = data;
            // $scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    }
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $timeout(function () { $scope.LoadData() }, 1000);

    $scope.EditFunction = function (data) {

        $scope.HDFacility = {};
        angular.copy(data, $scope.HDFacility);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
    }
    $scope.ClearData = function () {
        $scope.HDFacility = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmFacility.$submitted = false;
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

}]);

