﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class HDM_HDM_Webfiles_frmHDMViewAssignRequisition
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim filePath As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        filePath = HttpContext.Current.Request.Url.Authority & "/UploadFiles/" & Session("TENANT")
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                'BindLocations()
                Dim UID As String = Session("uid")
                Session("reqid") = Request.QueryString("rid").ToString()
                'BindUsers(UID)
                BindRequisition()
                'showFeedback.Visible = True
                BindIncharges()
                lbls.Visible = False
                lblMessage.Visible = False

                ' Disabling the Claim and Estimated cost for Marks and spencers based on system preferences
                Dim spms As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                spms.Command.AddParameter("@TYPE ", 44, DbType.Int32)
                Dim dsms As New DataSet()
                Dim MSChanges = spms.ExecuteScalar()
                If ddlStatus.Text = "9" Then
                    ddlAssignTo.Enabled = False
                    btnApprove.Enabled = False
                Else
                    ddlAssignTo.Enabled = True
                    btnApprove.Enabled = True
                End If
                If MSChanges = 1 Then
                    CAID.Visible = False
                    FCID.Visible = False
                    IMID.Visible = False
                    URID.Visible = False
                    RCID.Visible = False
                Else
                    CAID.Visible = True
                    FCID.Visible = True
                    IMID.Visible = True
                    URID.Visible = True
                    RCID.Visible = True
                End If

            End If
        End If
    End Sub

    Private Sub BindRequisition()
        Try
            Dim ds As New DataSet
            Dim param As SqlParameter() = New SqlParameter(0) {}
            param(0) = New SqlParameter("@REQID", Session("reqid").ToString())
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQUISITION_BY_REQ_ID", param)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblReqId.Text = ds.Tables(0).Rows(0)("REQUEST_ID").ToString()
                    txtEmp.Text = ds.Tables(0).Rows(0).Item("REQUESTED_BY")
                    txtDepartment.Text = ds.Tables(0).Rows(0)("DEPARTMENT").ToString()
                    txtSpaceID.Text = ds.Tables(0).Rows(0)("SPACE_ID").ToString()
                    txtclaimamount.Text = ds.Tables(0).Rows(0)("SER_CLAIM_AMT").ToString()
                    BindLocations()
                    ddlLocation.ClearSelection()
                    If ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() IsNot Nothing Then
                        ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString()).Selected = True

                    End If
                    BindMainCategory()
                    ddlMainCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() IsNot Nothing Then
                        ddlMainCategory.Items.FindByValue(ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString()).Selected = True
                    End If
                    BindSubCategory()
                    ddlSubCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() IsNot Nothing Then
                        ddlSubCategory.Items.FindByValue(ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString()).Selected = True
                    End If
                    BindChildCategory()
                    ddlChildCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() IsNot Nothing Then
                        ddlChildCategory.Items.FindByValue(ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString()).Selected = True
                    End If
                    BindAssetLocations()
                    ddlAssetLoaction.ClearSelection()
                    If ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() IsNot Nothing Then
                        ddlAssetLoaction.Items.FindByValue(ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString()).Selected = True
                    End If
                    txtMobile.Text = ds.Tables(0).Rows(0)("MOBILE").ToString()
                    BindRepeatCalls()
                    ddlRepeatCalls.ClearSelection()
                    If ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() IsNot Nothing Then
                        ddlRepeatCalls.Items.FindByValue(ds.Tables(0).Rows(0)("REPEAT_CALL").ToString()).Selected = True
                    End If
                    BindImpact()
                    ddlImpact.ClearSelection()
                    If ds.Tables(0).Rows(0)("IMPACT").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("IMPACT").ToString() IsNot Nothing Then
                        ddlImpact.Items.FindByValue(ds.Tables(0).Rows(0)("IMPACT").ToString()).Selected = True
                    End If
                    BindUrgency()
                    ddlUrgency.ClearSelection()
                    If ds.Tables(0).Rows(0)("URGENCY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("URGENCY").ToString() IsNot Nothing Then
                        ddlUrgency.Items.FindByValue(ds.Tables(0).Rows(0)("URGENCY").ToString()).Selected = True
                    End If
                    BindStatus()
                    ddlStatus.ClearSelection()
                    If ds.Tables(0).Rows(0)("STATUS_TITLE").ToString <> "" AndAlso ds.Tables(0).Rows(0)("STATUS_TITLE").ToString() IsNot Nothing Then
                        ddlStatus.Items.FindByText(ds.Tables(0).Rows(0)("STATUS_TITLE").ToString()).Selected = True
                    End If
               
                    txtProbDesc.Text = ds.Tables(0).Rows(0)("REMARKS").ToString()
                    lblStatus.Text = ds.Tables(0).Rows(0)("STATUS ID").ToString()

                    lblLocation.Text = ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString
                    lblMain.Text = ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString
                    lblSub.Text = ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString
                    lblChild.Text = ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString

                    lblFacility.Text = IIf(Convert.ToString(ds.Tables(0).Rows(0)("FD_NAME")) = "", "NA", Convert.ToString(ds.Tables(0).Rows(0)("FD_NAME")))

                    Dim list As List(Of fileInfo)
                    list = GetFiles()
                    Dim jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(list)
                    jsonstringhdn.Value = jsonstring
                End If
            End If
            'HISTORY IN GRID
            Dim dsHistory As New DataSet
            Dim spHist As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_REQUEST_HISTORY_DETAILS")
            spHist.Command.AddParameter("@REQID", Session("reqid"), Data.DbType.String)
            dsHistory = spHist.GetDataSet()
            gvReqHistory.DataSource = dsHistory
            gvReqHistory.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Class fileInfo
        Public Property Name As String
        Public Property Path As String
        Public Property UplTimeName As String
    End Class

    Public Function GetFiles() As List(Of fileInfo)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_FILES")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, Data.DbType.String)
        Dim fileslist As List(Of fileInfo) = New List(Of fileInfo)()

        Dim dr As SqlDataReader
        dr = sp.GetReader()
        While (dr.Read())
            fileslist.Add(New fileInfo() With {.Name = dr.GetString(0), .Path = "/UploadFiles/" & Session("TENANT") & "/" & dr.GetString(1), .UplTimeName = dr.GetString(1)})
        End While
        dr.Close()
        Return fileslist
    End Function

    Private Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindMainCategory()
        ddlMainCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE")
    End Sub

    Public Sub BindSubCategory()
        ddlSubCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub

    Public Sub BindAssetLocations()
        ddlAssetLoaction.Items.Clear()
        ObjSubsonic.Binddropdown(ddlAssetLoaction, "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS", "HAL_LOC_NAME", "HAL_LOC_CODE")
    End Sub

    Public Sub BindRepeatCalls()
        ddlRepeatCalls.Items.Clear()
        ObjSubsonic.Binddropdown(ddlRepeatCalls, "HDM_GET_ALL_ACTIVE_REPEATCALLS", "RPT_NAME", "RPT_CODE")
    End Sub

    Public Sub BindImpact()
        ddlImpact.Items.Clear()
        ObjSubsonic.Binddropdown(ddlImpact, "HDM_GET_ALL_ACTIVE_IMPACT", "IMP_NAME", "IMP_CODE")
    End Sub

    Public Sub BindUrgency()
        ddlUrgency.Items.Clear()
        ObjSubsonic.Binddropdown(ddlUrgency, "HDM_GET_ALL_ACTIVE_URGENCY", "UGC_NAME", "UGC_CODE")
    End Sub
    Public Sub BindStatus()
        ddlStatus.Items.Clear()
        ObjSubsonic.Binddropdown(ddlStatus, "HDM_STATUS_BIND", "STA_TITLE", "STA_ID")
    End Sub
  
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_ASSIGN_REQUEST")
            sp.Command.AddParameter("@REQ_ID", lblReqId.Text, Data.DbType.String)
            sp.Command.AddParameter("@USR_ID", Session("UID"), Data.DbType.String)
            sp.Command.AddParameter("@ASSIGN_TO", ddlAssignTo.SelectedValue, Data.DbType.String)
            sp.Command.AddParameter("@UPDATE_REMARKS", txtRemarks.Text, Data.DbType.String)
            sp.Execute()
            lblMessage.Visible = True
            lblMessage.Text = "Assigned to incharge Successfully"
        Catch ex As Exception

        End Try
    End Sub

    Public Sub BindIncharges()
        Dim ds As New DataSet
        Dim param As SqlParameter() = New SqlParameter(4) {}
        param(0) = New SqlParameter("@SEM_LOC_CODE", lblLocation.Text)
        param(1) = New SqlParameter("@SEM_MNC_CODE", lblMain.Text)
        param(2) = New SqlParameter("@SEM_SUBC_CODE", lblSub.Text)
        param(3) = New SqlParameter("@SEM_CHC_CODE", lblChild.Text)
        param(4) = New SqlParameter("@COMPANYID", Session("COMPANYID"))
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_BIND_INCHARGES", param)
        ddlAssignTo.DataSource = ds
        ddlAssignTo.DataTextField = "ASSIGNED_TO"
        ddlAssignTo.DataValueField = "AUR_ID"
        ddlAssignTo.ClearSelection()
        ddlAssignTo.DataBind()
        ddlAssignTo.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmHDMAdminViewRequisitions.aspx")
    End Sub

End Class
