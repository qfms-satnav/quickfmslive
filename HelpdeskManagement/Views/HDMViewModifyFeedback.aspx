﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HDMViewModifyFeedback.aspx.cs" Inherits="HelpdeskManagement_Views_HDMViewModifyFeedback" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <link href="../../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script src="../../Scripts/aggrid/ag-grid-2.js" defer></script>
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
        var length;
        var PostedFiles;
        var data;
        var gridLengthInitial;
        var fd = [];
        var columnDefs = [
            { headerName: "Uploaded Files", field: "Name", width: 350 },
            {
                headerName: "Image/File Name", field: "", cellRenderer: ghimages, suppressMenu: true, width: 250
            },
            {
                headerName: "Download", field: "Path", width: 250, cellRenderer: function (params) {
                    return '<a download="' + params.data.Name + '" href="' + params.data.Path + '"><span class="glyphicon glyphicon-download"></span></a>';
                }
            },
            //{ headerName: "Remove", template: '<a href="#" onclick="Remove(this.parentNode.parentNode)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 150 }
        ];

        function createImageSpan(image) {
            var resultElement = document.createElement("span");
            for (var i = 0; i < 1; i++) {
                var imageElement = document.createElement("img");
                imageElement.src = "../.." + image;
                imageElement.height = 50;
                imageElement.width = 50;
                resultElement.appendChild(imageElement);
            }
            return resultElement;
        }

        function ghimages(params) {
            //var Extn = params.data.Path.substr((params.data.Path.lastIndexOf('.') + 1)).toLowerCase();
            //var ExtArray = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];
            //if ($.inArray(Extn, ExtArray) > -1) {
            //    return createImageSpan(params.data.Path);
            //}
            //else {
            //    return params.data.Name;
            //}
        }

        var rowData = [];

        var gridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            rowHeight: 70,          //image size in ag grid
            enableColResize: true,
            suppressHorizontalScroll: true,
            onGridReady: function sizeToFit() {
                gridOptions.api.sizeColumnsToFit();
            }
        };

        function resetselectedfiles() {
            for (i = 0; i < rowData.length; i++) {
                var selectedfiles = document.createElement("input");
                selectedfiles.setAttribute("type", "hidden");
                selectedfiles.setAttribute("name", "selectedfiles");
                selectedfiles.setAttribute("value", rowData[i].Name);
                document.getElementById("form1").appendChild(selectedfiles);
            }
        }

        function showselectedfiles(fu) {
            data = new FormData($('form')[0]);
            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
                fd.push({ Name: fu.files[i].name, size: fu.files[i].size, type: fu.files[i].type });
                length = fu.files.length;
            }
            gridOptions.api.setRowData(rowData);
        }

        function Remove(node) {
            var ndx = parseInt(node.getAttribute("row"));
            rowData.splice(ndx, 1);
            gridOptions.api.setRowData(rowData);
        }

    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMViewModifyFeedbackController" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="FeedBack" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">View & Modify Feedback</h3>
                        </div>

                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" name="HDMViewModifyFeedbackReq" novalidate>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.ComplaintDate.$invalid}">
                                            <label for="txtcode">Complaint Received Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='ComplaintReceiveddat'>
                                                <input type="text" class="form-control" data-ng-model="HDMViewModifyFeedback.ComplaintDate" id="ComplaintDate" name="ComplaintDate" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('ComplaintReceiveddat')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.ComplaintDate.$invalid" style="color: red;">Please Select Complaint Date</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.LCM_NAME.$invalid}">
                                            <label class="control-label">Clinic <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Clinic" data-output-model="HDMViewModifyFeedback.Clinic" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" id="DDL" data-on-item-click="getCMByLoc()"
                                                data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.Clinic[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span id="Error" class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.LCM_NAME.$invalid" style="color: red;">Please Select Clinic</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" <%--data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.CLINIC_MGR_NAME.$invalid}"--%>>
                                            <label class="control-label">Clinic Manager</label>
                                            <div isteven-multi-select data-input-model="Clinic_Manager" data-output-model="HDMViewModifyFeedback.Clinic_Manager" data-button-label="icon CLINIC_MGR_NAME"
                                                data-item-label="icon CLINIC_MGR_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <%--<input type="text" data-ng-model="HDMViewModifyFeedback.Clinic_Manager[0]" name="CLINIC_MGR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.CLINIC_MGR_NAME.$invalid" style="color: red;">Please Select Clinic Manager</span>--%>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.Client_Name.$invalid}">
                                            <label for="txtcode">Client Name <span style="color: red;">*</span></label>
                                            <div class="control-label" id='Client_Name'>
                                                <input type="text" class="form-control" data-ng-model="HDMViewModifyFeedback.Client_Name" id="txtClientname" name="Contact" required="" />
                                                <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.Client_Name.$invalid" style="color: red;">Please Enter Client Name</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Contact <span style="color: red;"></span></label>
                                            <div class="control-label" id='Contact'>
                                                <input type="text" class="form-control" data-ng-model="HDMViewModifyFeedback.Contact" id="txtContact" name="Contact" required="" />
                                                <%--<span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.Contact.$invalid" style="color: red;">Please Enter Contact</span>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.MNC_NAME.$invalid}">
                                            <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Main_Category" data-output-model="HDMViewModifyFeedback.Main_Category" data-button-label="icon MNC_NAME"
                                                data-item-label="icon MNC_NAME maker" data-on-item-click="getsubbymain()" data-tick-property="ticked" data-max-labels="1" is-disabled="true"  selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.Main_Category[0]" name="MNC_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.MNC_NAME.$invalid" style="color: red;">Please Select Main Category</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.SUBC_NAME.$invalid}">
                                            <label class="control-label">Sub Category <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Sub_Category" data-output-model="HDMViewModifyFeedback.Sub_Category" data-button-label="icon SUBC_NAME"
                                                data-item-label="icon SUBC_NAME maker" data-on-item-click="getchildbysub()" data-tick-property="ticked" data-max-labels="1" is-disabled="true" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.Sub_Category[0]" name="SUBC_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.SUBC_NAME.$invalid" style="color: red;">Please Select Subcategory</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.CHC_TYPE_NAME.$invalid}">
                                            <label class="control-label">Child Category </label>
                                            <div isteven-multi-select data-input-model="Child" data-output-model="HDMViewModifyFeedback.Child" data-button-label="icon CHC_TYPE_NAME" 
                                                data-item-label="icon CHC_TYPE_NAME maker" data-on-item-click="ChildChanged()"  data-on-select-none="ChildSelectNone()" data-tick-property="ticked" data-max-labels="1" is-disabled="true">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.Child[0]" name="CHC_TYPE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.CHC_TYPE_NAME.$invalid" style="color: red">Please Select Child Category </span>
                                        </div>
                                     </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" <%--data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.AOM_NAME.$invalid}"--%>>
                                            <label class="control-label">AOM </label>
                                            <div isteven-multi-select data-input-model="AOM" data-output-model="HDMViewModifyFeedback.AOM" data-button-label="icon AOM_NAME"
                                                data-item-label="icon AOM_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                          <%--  <input type="text" data-ng-model="HDMViewModifyFeedback.AOM[0]" name="AOM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.AOM_NAME.$invalid" style="color: red;">Please Select AOM</span>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                         <div class="col-md-3 col-sm-6 col-xs-12" style="display:none">
                                        <div class="form-group">
                                            <label class="control-label">Others </label>
                                            <div isteven-multi-select data-input-model="OTHERS" data-output-model="HDMViewModifyFeedback.OTHERS" data-button-label="icon OTH_NAME"
                                                data-item-label="icon OTH_NAME maker" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.OTHERS[0]" name="OTH_NAME" style="display: none" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.SER_APP_STA_REMARKS_NAME.$invalid}">
                                            <label class="control-label">Status <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Status" data-output-model="HDMViewModifyFeedback.Status" data-button-label="icon SER_APP_STA_REMARKS_NAME"
                                                data-item-label="icon SER_APP_STA_REMARKS_NAME maker" data-on-item-click="" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.Status[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.SER_APP_STA_REMARKS_NAME.$invalid" style="color: red;">Please Select Status</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix" style="margin-left: 200px; margin-top: 30px;">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.txtModeComments.$invalid}">
                                            <label for="txtcode">Comments <span style="color: red;">*</span></label>
                                            <div class="control-label" id='ModeComments'>
                                                <textarea name="txtModeComments" class="form-control" data-ng-model="HDMViewModifyFeedback.ModeComments" cols="40" rows="5" required=""></textarea>
                                                <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.txtModeComments.$invalid" style="color: red;">Please Mode Comments</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.MODE_NAME.$invalid}">
                                            <label class="control-label">MODE <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="MODE" data-output-model="HDMViewModifyFeedback.MODE" data-button-label="icon MODE_NAME"
                                                data-item-label="icon MODE_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.MODE[0]" name="MODE_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.MODE_NAME.$invalid" style="color: red;">Please Select Mode</span>
                                        </div>
                                        <label for="txtcode" class="custom-file">Upload Images/Document<a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                        <input multiple type="file" name="UPLFILE" class="form-control" data-ng-model="HDMViewModifyFeedback.UPLFILE[0]" id="UPLFILE" accept=".mp3" class="custom-file-input" onchange="showselectedfiles(this)">
                                        <span class="custom-file-control"></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="clearfix" style="margin-left: 200PX; margin-top: 30px;">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Status Comments </label>
                                            <div class="control-label" id='Description'>
                                                <textarea name="txtStatusComments" class="form-control" data-ng-model="HDMViewModifyFeedback.StatusComments" cols="40" rows="5" required=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Status </label>
                                            <div isteven-multi-select data-input-model="STATUSCOM" data-output-model="HDMViewModifyFeedback.STATUSCOM" data-button-label="icon SER_APP_STA_REMARKS_NAME"
                                                data-item-label="icon SER_APP_STA_REMARKS_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewModifyFeedback.STATUSCOM[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" />
                                            <%--<input type="text" data-ng-model="HDMViewModifyFeedback.STATUSCOM[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMViewModifyFeedbackReq.$submitted && HDMViewModifyFeedbackReq.SER_APP_STA_REMARKS_NAME.$invalid" style="color: red;">Please Select Status</span>--%>
                                        </div>
                                        <label for="txtcode" class="custom-file">Upload Images/Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                        <input multiple type="file" name="UPLFILE1" class="form-control" data-ng-model="HDMViewModifyFeedback.UPLFILE1[0]" id="UPLFILE1" accept=".mp3" class="custom-file-input" onchange="showselectedfiles(this)">
                                        <span class="custom-file-control"></span>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-6 col-sm-12 col-xs-12 text-right" style="margin-left: 500px; margin-top: 20px;">
                                        <div class="form-group">
                                            <input type="submit" value="Modify" data-ng-click="ModifyFdbkDetails()" class="btn btn-primary custom-button-color" />
                                            <input type="button" value="Back" data-ng-click="Redirect()" class="btn btn-primary custom-button-color" />
                                            <%--onclick="GetUpload()" SaveEmpDetails--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-12">
                                        <div id="myGrid" data-ng-model="HDMViewModifyFeedback.myGrid" style="height: 250px; width: 400px" class="ag-blue"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../JS/HDMViewModifyFeedback.js" defer></script>
    <script src="../JS/HDMFeedBack.js" defer></script>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>

    <script defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        //$(document).ready(function () {
        //    setDateVals();
        //});

        //function setDateVals() {
            //$('#Billdate').datepicker({
            //    format: 'dd-M-yyyy',
            //    autoclose: true,
            //    todayHighlight: true
            //});
            //$('#InvoiceDate').datepicker({
            //    format: 'dd-M-yyyy',
            //    autoclose: true,
            //    todayHighlight: true
            //});
            //$('#Billdate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY')));
            //$('#InvoiceDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY')));
        //}
    </script>
</body>
</html>
