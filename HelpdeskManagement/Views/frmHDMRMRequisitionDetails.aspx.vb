﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

Partial Class HDM_HDM_Webfiles_frmHDMRMRequisitionDetails
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim filePath As String
    Public Shared CostStatus As Boolean
    Public Shared CostStatus1 As Boolean
    Dim MaintenanceReq As String = ""
    Dim MaintenanceReq_Upl As String = ""
    Dim STATUS As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        filePath = HttpContext.Current.Request.Url.Authority & "/UploadFiles/" & Session("TENANT")
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                Session("reqid") = Request.QueryString("rid").ToString()
                BindRequisition()

                divAC.Visible = True
                ' Check Labour/Spare Costs visible status
                Dim spCS As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                spCS.Command.AddParameter("@TYPE ", 4, DbType.Int32)
                Dim ChkCostSts = spCS.ExecuteScalar()
                CostStatus = IIf(ChkCostSts = 1, True, False)

                txtAC.Text = "0"

                divLSC.Visible = False

                ' Disabling the Claim and Estimated cost for Marks and spencers based on system preferences
                Dim spms As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                spms.Command.AddParameter("@TYPE ", 44, DbType.Int32)
                Dim dsms As New DataSet()
                Dim MSChanges = spms.ExecuteScalar()
                If MSChanges = 1 Then
                    CAID.Visible = False
                    FCID.Visible = False
                    IMID.Visible = False
                    URID.Visible = False
                    RCID.Visible = False
                    ENFD.Visible = True
                Else
                    CAID.Visible = True
                    FCID.Visible = True
                    IMID.Visible = True
                    URID.Visible = True
                    RCID.Visible = True
                    ENFD.Visible = False
                End If

                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_REQ_BREACHED")
                sp.Command.AddParameter("@REQ_ID", Request.QueryString("rid").ToString(), DbType.String)
                'Dim breachedSts = sp.ExecuteScalar()
                Dim breachedSts As DataSet = sp.GetDataSet()

                If Convert.ToInt32(breachedSts.Tables(0)(0)("ESCPARAMS_STATUS")) = 1 Then
                    'Corrective Action and Preventive Action will be visibile only when the request is breached
                    divBreachedAct.Visible = IIf(breachedSts.Tables(0)(0)("ESC_COUNT") >= 1, True, False)
                Else
                    divBreachedAct.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub BindRequisition()
        Try
            Dim ds As New DataSet
            Dim param As SqlParameter() = New SqlParameter(0) {}
            param(0) = New SqlParameter("@REQID", Session("reqid").ToString())
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQUISITION_BY_REQ_ID", param)

            'HISTORY IN GRID
            Dim dsHistory As New DataSet
            Dim spHist As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_REQUEST_HISTORY_DETAILS")
            spHist.Command.AddParameter("@REQID", Session("reqid"), Data.DbType.String)
            dsHistory = spHist.GetDataSet()
            gvReqHistory.DataSource = dsHistory
            gvReqHistory.DataBind()
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblReqId.Text = ds.Tables(0).Rows(0)("REQUEST_ID").ToString()
                    txtEmp.Text = ds.Tables(0).Rows(0).Item("REQUESTED_BY")
                    txtDepartment.Text = ds.Tables(0).Rows(0)("DEPARTMENT").ToString()
                    txtSpaceID.Text = ds.Tables(0).Rows(0)("SPACE_ID").ToString()
                    txtclaimamt.Visible = True
                    txtclaimamt.Text = ds.Tables(0).Rows(0)("SER_CLAIM_AMT").ToString()
                    Dim list As List(Of fileInfo)
                    list = GetFiles()
                    Dim jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(list)
                    jsonstringhdn.Value = jsonstring
                    BindLocations()
                    ddlLocation.ClearSelection()
                    If ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() IsNot Nothing Then
                        ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString()).Selected = True
                    End If
                    BindMainCategory()
                    ddlMainCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() IsNot Nothing Then
                        ddlMainCategory.Items.FindByValue(ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString()).Selected = True
                    End If
                    BindSubCategory()
                    ddlSubCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() IsNot Nothing Then
                        ddlSubCategory.Items.FindByValue(ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString()).Selected = True
                    End If
                    BindChildCategory()
                    ddlChildCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() IsNot Nothing Then
                        ddlChildCategory.Items.FindByValue(ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString()).Selected = True
                    End If
                    BindAssetLocations()
                    ddlAssetLoaction.ClearSelection()
                    If ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() IsNot Nothing Then
                        ddlAssetLoaction.Items.FindByValue(ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString()).Selected = True
                    End If
                    txtMobile.Text = ds.Tables(0).Rows(0)("MOBILE").ToString()
                    BindRepeatCalls()
                    ddlRepeatCalls.ClearSelection()
                    If ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() IsNot Nothing Then
                        ddlRepeatCalls.Items.FindByValue(ds.Tables(0).Rows(0)("REPEAT_CALL").ToString()).Selected = True
                    End If
                    BindImpact()
                    ddlImpact.ClearSelection()
                    If ds.Tables(0).Rows(0)("IMPACT").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("IMPACT").ToString() IsNot Nothing Then
                        ddlImpact.Items.FindByValue(ds.Tables(0).Rows(0)("IMPACT").ToString()).Selected = True
                    End If
                    BindUrgency()
                    ddlUrgency.ClearSelection()
                    If ds.Tables(0).Rows(0)("URGENCY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("URGENCY").ToString() IsNot Nothing Then
                        ddlUrgency.Items.FindByValue(ds.Tables(0).Rows(0)("URGENCY").ToString()).Selected = True
                    End If
                    BindStatus()
                    ddlStatus.ClearSelection()
                    If ds.Tables(0).Rows(0)("STATUS_TITLE").ToString <> "" AndAlso ds.Tables(0).Rows(0)("STATUS_TITLE").ToString() IsNot Nothing Then
                        ddlStatus.Items.FindByText(ds.Tables(0).Rows(0)("STATUS_TITLE").ToString()).Selected = True
                    End If
                    lblFacility.Text = IIf(Convert.ToString(ds.Tables(0).Rows(0)("FD_NAME")) = "", "NA", Convert.ToString(ds.Tables(0).Rows(0)("FD_NAME")))
                    txtPB.Text = ds.Tables(0).Rows(0)("REMARKS").ToString()

                    'Dim list As List(Of fileInfo)
                    'list = GetFiles()
                    'Dim jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(list)
                    'jsonstringhdn.Value = jsonstring
                End If
            End If



        Catch ex As Exception

        End Try
    End Sub

    Class fileInfo
        Public Property Name As String
        Public Property Path As String
        Public Property UplTimeName As String
    End Class

    Public Function GetFiles() As List(Of fileInfo)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_FILES")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, Data.DbType.String)
        Dim fileslist As List(Of fileInfo) = New List(Of fileInfo)()

        Dim dr As SqlDataReader
        dr = sp.GetReader()
        While (dr.Read())
            fileslist.Add(New fileInfo() With {.Name = dr.GetString(0), .Path = "/UploadFiles/" & Session("TENANT") & "/" & dr.GetString(1), .UplTimeName = dr.GetString(1)})
        End While
        dr.Close()
        Return fileslist
    End Function

    Private Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindMainCategory()
        ddlMainCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE")
    End Sub

    Public Sub BindSubCategory()
        ddlSubCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub

    Public Sub BindAssetLocations()
        ddlAssetLoaction.Items.Clear()
        ObjSubsonic.Binddropdown(ddlAssetLoaction, "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS", "HAL_LOC_NAME", "HAL_LOC_CODE")
    End Sub

    Public Sub BindRepeatCalls()
        ddlRepeatCalls.Items.Clear()
        ObjSubsonic.Binddropdown(ddlRepeatCalls, "HDM_GET_ALL_ACTIVE_REPEATCALLS", "RPT_NAME", "RPT_CODE")
    End Sub

    Public Sub BindStatus()
        ddlStatus.Items.Clear()
        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        ddlStatus.DataSource = ds1
        ddlStatus.DataTextField = "STA_TITLE"
        ddlStatus.DataValueField = "STA_ID"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, "--Select--")
    End Sub

    Public Sub BindImpact()
        ddlImpact.Items.Clear()
        ObjSubsonic.Binddropdown(ddlImpact, "HDM_GET_ALL_ACTIVE_IMPACT", "IMP_NAME", "IMP_CODE")
    End Sub

    Public Sub BindUrgency()
        ddlUrgency.Items.Clear()
        ObjSubsonic.Binddropdown(ddlUrgency, "HDM_GET_ALL_ACTIVE_URGENCY", "UGC_NAME", "UGC_CODE")
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click

        Try
            MaintenanceReq = lblReqId.Text
            MaintenanceReq_Upl = MaintenanceReq.Replace("/", "_")
            Dim projectPath As String = Server.MapPath("~/")
            Dim folderName As String = Path.Combine(projectPath, "UploadFiles\" & Session("TENANT"))
            If Not Directory.Exists(folderName) Then
                System.IO.Directory.CreateDirectory(folderName)
            End If
            Dim RaiseDup As Int16
            Dim RepeatCall As String
            Dim Impact As String
            Dim Urgency As String
            Dim REQ_TYPE As Integer
            Dim CALL_LOG_BY As String
            Dim Facility As String

            'Dim lblmsg As String
            'lblmsg.te = ""



            If fu1.PostedFiles IsNot Nothing Then
                Dim selectedfiles = Request.Form("selectedfiles")
                If selectedfiles <> Nothing Then
                    Dim selectedfilesarray = selectedfiles.Split(",")
                    For fucount As Integer = 0 To selectedfilesarray.Length - 1
                        'Dim intsixe As Long = fu1.PostedFiles(fucount).ContentLength
                        For Each File In fu1.PostedFiles
                            If selectedfilesarray(fucount).Equals(File.FileName) Then
                                Dim intsize As Long = CInt(File.ContentLength)
                                Dim strFileName As String
                                Dim strFileExt As String
                                If intsize <= 20971520 Then
                                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("hhmmss")
                                    strFileName = System.IO.Path.GetFileName(File.FileName)
                                    strFileName = strFileName.Replace(" ", "_")
                                    strFileExt = System.IO.Path.GetExtension(File.FileName)
                                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Session("TENANT") & "\" & MaintenanceReq_Upl & "_" & Upload_Time & "_" & strFileName '& "." & strFileExt
                                    File.SaveAs(filePath)

                                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_INSERT_UPLOADED_FILES")
                                    sp2.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
                                    sp2.Command.AddParameter("@STATUS_ID", ddlStatus.SelectedValue, DbType.Int32)
                                    sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String)
                                    sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String)
                                    sp2.Command.AddParameter("@REMARKS", txtProbDesc.Text, DbType.String)
                                    sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
                                    sp2.ExecuteScalar()
                                End If
                            End If
                        Next
                    Next
                End If
            Else
                lblMessage.Text = "Select Uploadable file by clicking the Browse button"
            End If

            Dim spms As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            spms.Command.AddParameter("@TYPE ", 44, DbType.Int32)
            Dim dsms As New DataSet()
            Dim MSChanges1 = spms.ExecuteScalar()

            If MSChanges1 = 1 Then

                'Dim colsedtime As String = Request.Form("txtToDt")
                'Dim combinedDateTime As String = txtToDt.Text & " " & colsedtime
                Dim colsedtime As String = DateTime.Now.ToString("HH:mm:ss")
                Dim combinedDateTime As String = txtToDt.Text & " " & colsedtime

                Dim param As SqlParameter() = New SqlParameter(8) {}
                param(0) = New SqlParameter("@REQ_ID", lblReqId.Text)
                param(1) = New SqlParameter("@STA_ID", ddlStatus.SelectedValue)
                param(2) = New SqlParameter("@UPDATE_REMARKS", txtProbDesc.Text)
                param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                param(4) = New SqlParameter("@ADDN_COST", txtAC.Text)
                param(5) = New SqlParameter("@LBR_COST", txtLC.Text)
                param(6) = New SqlParameter("@SPR_COST", txtSC.Text)
                param(7) = New SqlParameter("@CLA_AMT", txtclaimamt.Text)
                param(8) = New SqlParameter("@Close_Date", combinedDateTime)
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQ_UPDATE", param)
                lblMessage.Visible = True
                lblMessage.Text = "Request " + lblReqId.Text + " Updated Successfully"

            Else

                Dim param As SqlParameter() = New SqlParameter(7) {}
                param(0) = New SqlParameter("@REQ_ID", lblReqId.Text)
                param(1) = New SqlParameter("@STA_ID", ddlStatus.SelectedValue)
                param(2) = New SqlParameter("@UPDATE_REMARKS", txtProbDesc.Text)
                param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                param(4) = New SqlParameter("@ADDN_COST", txtAC.Text)
                param(5) = New SqlParameter("@LBR_COST", txtLC.Text)
                param(6) = New SqlParameter("@SPR_COST", txtSC.Text)
                param(7) = New SqlParameter("@CLA_AMT", txtclaimamt.Text)
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQ_UPDATE", param)
                lblMessage.Visible = True
                lblMessage.Text = "Request " + lblReqId.Text + " Updated Successfully"
            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try

        BindRequisition()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmRMViewRequisitions.aspx")
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        If ddlStatus.SelectedValue = "9" Then
            divAC.Visible = True
            If CostStatus = True Then
                divLSC.Visible = True
                Dim spCS As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                spCS.Command.AddParameter("@TYPE ", 7, DbType.Int32)
                Dim ChkCostSts1 = spCS.ExecuteScalar()
                CostStatus1 = IIf(ChkCostSts1 = 1, True, False)
                If CostStatus1 = True Then
                    divAC.Visible = True
                    RequiredFieldValidator1.Visible = True
                    RequiredFieldValidator2.Visible = True
                    RequiredFieldValidator3.Visible = True
                    span1.Visible = True
                    span2.Visible = True
                    span3.Visible = True
                End If
                divLSC.Visible = True
                divAC.Visible = True
            Else
                divLSC.Visible = False
            End If
        Else
            divAC.Visible = False
            divLSC.Visible = False
        End If
    End Sub

End Class
