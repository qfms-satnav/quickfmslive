﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmRMViewRequisitions.aspx.cs" Inherits="HelpdeskManagement_Views_frmRMViewRequisitions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../Dashboard/CSS/ProfileStyle.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        body {
            text-align: left;
            vertical-align: top;
        }

        .Container {
            height: 600px;
            width: 100%;
            overflow-y: auto;
            background-color: white;
            border-radius: 5px;
            margin: 0;
            vertical-align: top;
        }

        .Content {
            /*width: 300px;*/
            /*color: gray;*/
            text-align: center;
        }

        .Flipped, .Flipped .Content {
            transform: rotateX(360deg);
        }

        #wrapper1, #wrapper2 {
            width: 100%;
            border: none 0px RED;
            overflow-x: scroll;
            overflow-y: hidden;
        }

        #wrapper1 {
            height: 20px;
        }

        #wrapper2 {
            /*  height: 500px;
            overflow-y:auto;*/
            /*height: 1000px;*/
            overflow-y: scroll;
        }

        #div1 {
            width: 1800px;
            height: 20px;
        }

        #div2 {
            width: 1000px;
            height: 100px;
            background-color: #88FF88;
            overflow: auto;
        }


        /*.child {
            transform: rotateX(180deg);
                  }*/





        /* Designing for scroll-bar */
        /*::-webkit-scrollbar {
            width: 5px;
        }*/

        /* Track */
        /*  ::-webkit-scrollbar-track {
            background: gainsboro;
            border-radius: 5px;
        }*/

        /* Handle */
        /*  ::-webkit-scrollbar-thumb {
            background: black;
            border-radius: 5px;
        }*/

        /* Handle on hover */
        /*  ::-webkit-scrollbar-thumb:hover {
                background: #555;
            }*/
    </style>
    <style type="text/css">
        .FixedHeader {
            position: absolute;
            font-weight: bold;
            z-index: 10;
            position: sticky;
            top: 0
        }

        .pagination {
            display: inline-block;
            /*padding-right: 1100px;*/
            /*margin-top: 50px*/
        }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
                border: 1px solid #ddd;
            }

                .pagination a.active {
                    background-color: #4CAF50;
                    color: white;
                    border: 1px solid #4CAF50;
                }

                .pagination a:hover:not(.active) {
                    background-color: #ddd;
                }

                .pagination a:first-child {
                    border-top-left-radius: 5px;
                    border-bottom-left-radius: 5px;
                }

                .pagination a:last-child {
                    border-top-right-radius: 5px;
                    border-bottom-right-radius: 5px;
                }
    </style>
    <style>
        #ddlAssignStatus1 {
            height: 600px;
            max-height: 600px;
            overflow-y: scroll;
        }

        #ddlAssignStatus {
            height: auto;
            max-height: 200px;
            overflow-y: scroll;
        }

        /* .custom-button-color {
            height: 26px;
        }
*/
        .hide {
            display: none;
        }
    </style>
    <script type="text/javascript" defer>

        function LoadScroll() {
            var wrapper1 = document.getElementById('wrapper1');
            var wrapper2 = document.getElementById('wrapper2');
            wrapper1.onscroll = function () {
                wrapper2.scrollLeft = wrapper1.scrollLeft;
            };
            wrapper2.onscroll = function () {
                wrapper1.scrollLeft = wrapper2.scrollLeft;
            };
        }
        setTimeout(LoadScroll, 1000);

        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvViewRequisitions.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    var Isvalid = false;
                    Isvalid = Page_ClientValidate("Val3");
                    if (Isvalid) {
                        return true;
                    }
                    else {
                        alert('Please enter Additional/Labour/Spare costs or else Zero.\nMandatory fields');
                        return false;
                    }
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }

        function validateCheckBoxesUnA() {
            var gridView = document.getElementById("<%=gvUnassigned.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Raise Request" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Update Request</h3>
            </div>
            <div class="card">
                <%-- <div class="card-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="accordion">
                                <div visible="true">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">My Requests</h4>
                                    </div>
                                    <br />
                                    <div style="padding-left: 25px">
                                        <div id="showDiv" runat="server">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtid">Enter Request Id </label>
                                                        <asp:TextBox ID="txtReqId" runat="server" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtloc">Location</label>
                                                        <asp:DropDownList ID="ddlLocaton1" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtcat">Main Category</label>
                                                        <asp:DropDownList ID="ddlManinCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtcat">Subcategory</label>
                                                        <asp:DropDownList ID="ddlSubCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtcat">Child Category</label>
                                                        <asp:DropDownList ID="ddlChildCategroy1" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtcat">Select Status</label>
                                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <%--  <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtid">Enter From Date</label>
                                                                            <div class='input-group date' id='fromdate'>
                                                                                <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>
                                                <div class="col-sm-3 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Enter From Date<span class="text-danger">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                                            ErrorMessage="Please Select From Date" ValidationGroup="Val1" ControlToValidate="txtfromDt"></asp:RequiredFieldValidator>
                                                        <div class="input-group date" id='fromdate'>
                                                            <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy"></asp:TextBox>

                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                            </span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-xs-6">
                                                    <div class="form-group">
                                                        <label>Enter To Date<span class="text-danger">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none"
                                                            ErrorMessage="Please Select To Date" ValidationGroup="Val1" ControlToValidate="txtToDt"></asp:RequiredFieldValidator>
                                                        <div class="input-group date" id='toDt'>
                                                            <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy"></asp:TextBox>
                                                            <%-- <div class="input-group-append">
                                            <div class="input-group-text bg-transparent">
                                                <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                            </div>
                                        </div>--%>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>











                                                <%--                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtloc">Enter To Date</label>
                                                                            <div class='input-group date' id='toDt'>
                                                                                <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                                                </span>
                                                                            </div>
                                                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="To date should be greater than From date" ControlToValidate="txtToDt" ControlToCompare="txtfromDt" Type="Date" Operator="GreaterThan" runat="server" />
                                                                        </div>
                                                                    </div>--%>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 ">
                                                    <div class="col-md-12 text-right">
                                                        <br />
                                                        <asp:Button ID="txtReqIdFilter" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                            CausesValidation="true" TabIndex="2" OnClick="txtReqIdFilter_Click" />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <%--  <div class="horizontal-scroll">
                                                            <div class="form-group col-md-12">--%>

                                    <h1 style="color: green;"></h1>
                                    <h2 style="color: green;"></h2>
                                    <div>
                                        <div id="wrapper1">
                                            <div id="div1">
                                            </div>
                                        </div>
                                        <div id="wrapper2">
                                            <div class="div2">
                                                <div class="Content">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="gvViewRequisitions" runat="server" AutoGenerateColumns="false" Style="height: 400px; overflow: scroll"
                                                                            HeaderStyle-CssClass="FixedHeader" AllowPaging="true" CssClass="table GridStyle" GridLines="None" OnPageIndexChanging="gvViewRequisitions_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                                                    <HeaderTemplate>
                                                                                        Select All
                                                                                                   <input id="chkSelect1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Ticket Id">
                                                                                    <ItemTemplate>
                                                                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HelpdeskManagement/Views/frmHDMRMRequisitionDetails.aspx?RID={0}")%>'>
                                                                                            <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                                        </asp:HyperLink>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Date Requested">
                                                                                    <HeaderStyle Width="300px" HorizontalAlign="Left" Wrap="False"></HeaderStyle>
                                                                                    <ItemStyle Width="300" HorizontalAlign="Left" Wrap="False" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requestor" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <%-- <asp:BoundField DataField="REQ_STATUS" HeaderText="Ticket Status" ItemStyle-HorizontalAlign="left" />--%>

                                                                                <asp:TemplateField HeaderText="Ticket status">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" Width="100px" Text='<%# Eval("REQ_STATUS") %>' Style='<%# "color:" + Eval("STA_COLOR") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Update Status">
                                                                                    <ItemTemplate>
                                                                                        <asp:DropDownList ID="ddlModifyStatus" AutoPostBack="true" OnDataBound="ddlModifyStatus_DataBound" CssClass="selectpicker" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" runat="server">
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="REMARKS" HeaderText="Latest Remarks" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Update Notes">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtUpdateRemarks" runat="server" TextMode="MultiLine" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>


                                                                                <asp:TemplateField HeaderText="Issue Location">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblLocation" Text='<%#Eval("LOCATION").ToString() %>' runat="server">
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Category" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Subcategory" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Specific Detail" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned Agent/Technician/Service InCharge" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CONTACT_NO" HeaderText="Contact Number" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="UGC_NAME" HeaderText="Priority Level" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="SER_APPR_AMOUNT" HeaderText="Approved Amount" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ESC_COUNT" HeaderText="Escalation Count" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <%--  <asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>--%>
                                                                                <asp:TemplateField HeaderText="Resolution Time">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTAT" Width="150" Text='<%# Eval("TOTAL_TIME") %>' runat="server">
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="CallLog Date" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCallLogDt" Text='<%#Eval("SER_CAL_LOG_DT").ToString() %>' runat="server">
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="IMP_NAME" HeaderText="Impact" ItemStyle-HorizontalAlign="left">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SER_CLAIM_AMT" HeaderText="Cost Estimate">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>

                                                                                <asp:TemplateField HeaderText="Additional/Approved Cost" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                                                                    <ItemTemplate>
                                                                                        <asp:RegularExpressionValidator ID="revAC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtADC"
                                                                                            ErrorMessage="Please Enter cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <asp:TextBox ID="txtADC" Visible="false" runat="server" />
                                                                                        <cc1:FilteredTextBoxExtender ID="txtADC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtADC"></cc1:FilteredTextBoxExtender>
                                                                                        <asp:Label ID="lblADC" runat="server" Text="0"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Labour Cost" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                                                                    <ItemTemplate>
                                                                                        <asp:RegularExpressionValidator ID="revTR" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtLBC"
                                                                                            ErrorMessage="Please Enter Labour cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <asp:TextBox ID="txtLBC" Visible="false" runat="server" />
                                                                                        <cc1:FilteredTextBoxExtender ID="txtLBC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtLBC"></cc1:FilteredTextBoxExtender>
                                                                                        <asp:Label ID="lblLBC" runat="server" Text="0"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Spare Cost" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                                                                    <ItemTemplate>
                                                                                        <asp:RegularExpressionValidator ID="rfvSC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtSPC"
                                                                                            ErrorMessage="Please Enter Spare cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <asp:TextBox ID="txtSPC" Visible="false" runat="server" />
                                                                                        <cc1:FilteredTextBoxExtender ID="txtSPC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtSPC"></cc1:FilteredTextBoxExtender>
                                                                                        <asp:Label ID="lblSPC" runat="server" Text="0"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                            <PagerStyle CssClass="pagination-ys" />

                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr class="pagination">
                                                                    <td>
                                                                        <asp:Repeater ID="repeaterPaging" runat="server">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="pagingLinkButton" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                                                                    Font-Underline="false" OnClick="linkButton_Click" CssClass='<%#Eval("IsActive") %>'></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="txtReqIdFilter" />
                                                            <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                                <div class="form-group">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Button ID="btnUpdate1" runat="server" Text="Update" CssClass="btn btn-primary custom-button-color"
                                                                OnClientClick="javascript:return validateCheckBoxesMyReq();" ValidationGroup="Val2" OnClick="btnUpdate1_Click" />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="txtReqIdFilter" EventName="Click" />
                                                            <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div id="accordion1">
                                        <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="padding-right: 5px;">
                                            <asp:Button ID="btnShowUnassignedRequests" runat="server" Text="Unassigned Requests" OnClick="btnShowUnassignedRequests_Click" CssClass="btn btn-primary" />
                                        </div>
                                        <div runat="server" id="divhd" visible="false">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Unassigned Requests</h3>
                                            </div>
                                            <br />
                                            <div style="padding-left: 25px">
                                                <div id="showDiv2" runat="server">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="txtid">Enter Request Id </label>
                                                                <asp:TextBox ID="textReqID2" runat="server" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="txtloc">Location</label>
                                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="txtcat">Main Category</label>
                                                                <asp:DropDownList ID="ddlManinCatMR1" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true" OnSelectedIndexChanged="ddlManinCatMR1_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="txtcat">Subcategory</label>
                                                                <asp:DropDownList ID="ddlSubCatUAReq" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true" OnSelectedIndexChanged="ddlSubCatUAReq_SelectedIndexChanged"></asp:DropDownList>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="txtcat">Child Category</label>
                                                                <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="txtUAid">Enter From Date</label>
                                                                <div class='input-group date' id='UAfromDt'>
                                                                    <asp:TextBox ID="txtUAfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('UAfromDt')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label for="txtUAtoId">Enter To Date</label>
                                                                <div class='input-group date' id='UAtoDt'>
                                                                    <asp:TextBox ID="txtUAtoDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('UAtoDt')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <br />
                                                                <asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                                    CausesValidation="true" TabIndex="2" OnClick="Button1_Click" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 ">
                                                            <div class="form-group">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row horizontal-scroll" style="overflow-x: scroll">
                                                <div class="col-md-12">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvUnassigned" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                                EmptyDataText="No Requisition Found." CssClass="table GridStyle" GridLines="None" OnPageIndexChanging="gvUnassigned_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Request Id" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--<asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />--%>
                                                                    <asp:TemplateField HeaderText="Requested Date">
                                                                        <ItemTemplate>
                                                                            <asp:Label runat="server" Width="100px" Text='<%# Eval("REQUESTED_DATE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:TemplateField HeaderText="Location">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLocation" Text='<%#Eval("LOCATION").ToString() %>' runat="server">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="REQ_STATUS" HeaderText="Current Status" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="SER_CLAIM_AMT" HeaderText="Amount" />
                                                                    <asp:BoundField DataField="SER_APPR_AMOUNT" HeaderText="Approved Amount" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                                                    <asp:TemplateField HeaderText="Update Status">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="ddlAssignStatus" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" runat="server">
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="REMARKS" HeaderText="Current Remarks" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:TemplateField HeaderText="Update Remarks">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtUpdate2Remarks" runat="server" TextMode="MultiLine" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ESC_COUNT" HeaderText="No. Of Escalations" ItemStyle-HorizontalAlign="left" />
                                                                    <%--<asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left" />--%>
                                                                    <asp:TemplateField HeaderText="Time Taken" Visible="true">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTAT" runat="server" Width="100px" Text='<%# Eval("TOTAL_TIME") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="CallLog Date" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCallLogDt" Text='<%#Eval("SER_CAL_LOG_DT").ToString() %>' runat="server">
                                                                            </asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="IMP_NAME" HeaderText="Impact" ItemStyle-HorizontalAlign="left" />
                                                                    <asp:BoundField DataField="UGC_NAME" HeaderText="Urgency" ItemStyle-HorizontalAlign="left" />

                                                                    <asp:TemplateField HeaderText="Additional Cost" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="revAC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtADC"
                                                                                ErrorMessage="Please Enter cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtADC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtADC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtADC">
                                                                            </cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblADC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Labour Cost" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="revTR" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtLBC"
                                                                                ErrorMessage="Please Enter Labour cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtLBC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtLBC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtLBC"></cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblLBC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Spare Cost" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide">
                                                                        <ItemTemplate>
                                                                            <asp:RegularExpressionValidator ID="rfvSC" ValidationGroup="Val2" runat="server" Display="none" ControlToValidate="txtSPC"
                                                                                ErrorMessage="Please Enter Spare cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                                            <asp:TextBox ID="txtSPC" Visible="false" runat="server" />
                                                                            <cc1:FilteredTextBoxExtender ID="txtSPC_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtSPC"></cc1:FilteredTextBoxExtender>
                                                                            <asp:Label ID="lblSPC" runat="server" Text="0"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            Select All
                                                                        <input id="chkSelect2" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkSelect1', this.checked)">
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect1" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                <PagerStyle CssClass="pagination-ys" />
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                                <div class="form-group">
                                                    <asp:Button ID="btnUpdate2" runat="server" Text="Update" CssClass="btn btn-primary custom-button-color" Visible="false" OnClientClick="javascript:return validateCheckBoxesUnA();" OnClick="btnUpdate2_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        $(document).ready(function () {

            var toDate = new Date();
            var twoDigitMonth = (parseInt(toDate.getMonth()) + parseInt(1)) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
            var twoDigitDate = toDate.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
            var currentDate = twoDigitMonth + "/" + twoDigitDate + "/" + toDate.getFullYear();
            debugger;
            $('#txtUAtodt').val(currentDate);
            $('#txtToDt').val(currentDate);

            var fromDate = new Date();
            fromDate.setDate(fromDate.getDate() - 30);
            twoDigitMonth = (parseInt(fromDate.getMonth()) + parseInt(1)) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
            twoDigitDate = fromDate.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
            currentDate = twoDigitMonth + "/" + twoDigitDate + "/" + fromDate.getFullYear();
            $('#txtfromDt').val(currentDate);
            $('#txtUAfromDt').val(currentDate);

            $('#<%=gvViewRequisitions.ClientID %>').Scrollable({
                ScrollHeight: 300,
                IsInUpdatePanel: true
            });
        });

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function refreshSelectpicker() {
            $("#<%=ddlLocaton1.ClientID%>").selectpicker();
            $("#<%=ddlManinCatMR.ClientID%>").selectpicker();
            $("#<%=ddlSubCatMR.ClientID%>").selectpicker();
            $("#<%=ddlChildCategroy1.ClientID%>").selectpicker();
            $("#<%=ddlStatus.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlManinCatMR1.ClientID%>").selectpicker();
            $("#<%=ddlSubCatUAReq.ClientID%>").selectpicker();
            $("#<%=ddlChildCategory.ClientID%>").selectpicker();

        }
        refreshSelectpicker();
    </script>
</body>
</html>
