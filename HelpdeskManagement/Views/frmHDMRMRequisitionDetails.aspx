﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmHDMRMRequisitionDetails.aspx.vb" Inherits="HDM_HDM_Webfiles_frmHDMRMRequisitionDetails" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        #user, #dept, #id {
            color: deepskyblue;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .right {
            text-align: right;
        }
         .modal-dialog {
            max-width: 800px !important;
        }
    </style>
    <script src="../../Scripts/aggrid/ag-grid-2.js" defer></script>
    <script defer>
        var gridLengthInitial;
        var columnDefs = [
            { headerName: "Uploaded Files", field: "Name", width: 250 },
            {
                headerName: "Image/File Name", field: "", suppressMenu: true, width: 250
            },
            {
                headerName: "Download", field: "Path", width: 250, cellRenderer: function (params) {
                    return '<a download="' + params.data.UplTimeName + '" href="' + params.data.Path + '"><span class="glyphicon glyphicon-download"></span></a>';
                }
            },
            { headerName: "Remove", template: '<a href="#" onclick="Remove(this.parentNode.parentNode)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 150 }
        ];
        //cellRenderer: ghimages
        function createImageSpan(image) {
            var resultElement = document.createElement("span");
            for (var i = 0; i < 1; i++) {
                var imageElement = document.createElement("img");
                imageElement.src = "../.." + image;
                imageElement.height = 50;
                imageElement.width = 50;
                resultElement.appendChild(imageElement);
            }
            return resultElement;
        }

        //function ghimages(params) {
        //    var Extn = params.data.Path.substr((params.data.Path.lastIndexOf('.') + 1)).toLowerCase();
        //    var ExtArray = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];
        //    if ($.inArray(Extn, ExtArray) > -1) {
        //        return createImageSpan(params.data.Path);
        //    }
        //    else {
        //        return params.data.Name;
        //    }
        //}
        var rowData = [];

        var gridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            rowHeight: 70,
            enableColResize: true,
            suppressHorizontalScroll: true,
            onGridReady: function sizeToFit() {
                gridOptions.api.sizeColumnsToFit();
            }
        };

        function resetselectedfiles() {
            for (i = 0; i < rowData.length; i++) {
                var selectedfiles = document.createElement("input");
                selectedfiles.setAttribute("type", "hidden");
                selectedfiles.setAttribute("name", "selectedfiles");
                selectedfiles.setAttribute("value", rowData[i].Name);
                document.getElementById("form1").appendChild(selectedfiles);
            }
        }

        //function showselectedfiles(fu) {
        //    if (gridLengthInitial < gridOptions.rowData.length) {
        //        for (j = gridLengthInitial; j <= gridOptions.rowData.length; j++) {
        //            rowData.splice(gridLengthInitial, 1);
        //            gridOptions.api.setRowData(rowData);
        //        }
        //    }

        //    for (i = 0; i < fu.files.length; i++) {
        //        rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
        //    }
        //    gridOptions.api.setRowData(rowData);
        //}

        function showselectedfiles(fu) {
            //clearing prev uploaded files from grid
            var gridLength = gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    rowData.splice(0, 1);
                    gridOptions.api.setRowData(rowData);
                }
            }

            //Will hide
            //rowData = [];
            console.log(fu.files);
            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
            }
            gridOptions.api.setRowData(rowData);
        }

        function Remove(node) {
            $("#fu1").val("");
            var ndx = parseInt(node.getAttribute("row"));
            rowData.splice(ndx, 1);
            gridOptions.api.setRowData(rowData);
        }

        function Remove(node) {
            console.log(jQuery("#lblStatus").val);
            if ($("#lblStatus").val != "9") {
                var ndx = parseInt(node.getAttribute("row"));
                if (ndx == 0)
                    rowData = [];
                else
                    rowData.splice(ndx, 1);
                gridOptions.api.setRowData(rowData);
            }
        }



        document.addEventListener("DOMContentLoaded", function () {
            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
            var jsonstringhdn = document.getElementById("jsonstringhdn");
            var jsonstring = jsonstringhdn.value;
            var jsonobj = JSON.parse(jsonstring);
            //console.log(jsonobj);
            for (i = 0; i < jsonobj.length; i++) {
                rowData.push(jsonobj[i]);
            }
            if (jsonobj.length != 0) {
                gridOptions.api.setRowData(rowData);
                gridLengthInitial = gridOptions.rowData.length;
            }
            else {
                eGridDiv.style.display = 'none';
            }
        });
        $(function () {
            var maybe = true;
            var text = $('#txtSpaceID').val();
            if (maybe) {
                $('#txtSpaceID').attr('title', text);
            }
        });

        //---------------Date Picker validation-----------------
        $(document).ready(function () {
            var lastUpdatedDateString = $('#<%= gvReqHistory.ClientID %> tr:last').find('td:eq(1)').text().trim();
            var lastUpdatedDate = new Date(lastUpdatedDateString);
            if (!isNaN(lastUpdatedDate.getTime())) {
                var formattedDate = (lastUpdatedDate.getMonth() + 1) + '/' + lastUpdatedDate.getDate() + '/' + lastUpdatedDate.getFullYear();
                $('.datepicker-icon').click(function () {
                    setup('toDt', formattedDate);
                });
            }
        });
        function setup(id, formattedDate) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                startDate: new Date(formattedDate)
            });
        };

    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Update Request</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i id="id" class="fa fa-tag"></i></div>
                                            <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i id="user" class="fa fa-user"></i></div>
                                            <asp:TextBox ID="txtEmp" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i id="dept" class="fa fa-bookmark"></i></div>
                                            <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <img src="../../images/Chair_Blue.gif" />
                                            </div>
                                            <asp:TextBox ID="txtSpaceID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Location<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                            ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                        </asp:CompareValidator>
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" Enabled="false" data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Main Category<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlMainCategory"
                                            ErrorMessage="Please Select Main Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                        </asp:CompareValidator>
                                        <asp:DropDownList ID="ddlMainCategory" runat="server" CssClass="form-control selectpicker" Enabled="false" data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Subcategory<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlSubCategory"
                                            ErrorMessage="Please Select Subcategoryy" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                        </asp:CompareValidator>
                                        <asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control selectpicker" Enabled="false" data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Child Category<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator4" runat="server" Display="None" ControlToValidate="ddlChildCategory"
                                            ErrorMessage="Please Select Child Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                        </asp:CompareValidator>
                                        <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="form-control selectpicker" Enabled="false" data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel8" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <label for="txtcode">Upload Images/Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                <%--<div class="btn btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>--%>
                                                <asp:FileUpload ID="fu1" runat="Server" AllowMultiple="True" Enabled="true" onChange="showselectedfiles(this)" ClientIDMode="Static" class="form-control" />

                                                <%--</div>--%>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>


                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Status</label>
                                            <asp:CompareValidator ID="cvStatus" runat="server" Display="None" ControlToValidate="ddlStatus"
                                                ErrorMessage="Please Select Status" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                            </asp:CompareValidator>
                                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-xs-6" id="ENFD" runat="server">
                                        <div class="form-group">
                                            <label>Closed Date<span class="text-danger">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="none"
                                                ErrorMessage="Please Select To Date" ValidationGroup="Val1" ControlToValidate="txtToDt"></asp:RequiredFieldValidator>
                                            <div class="input-group date" id="toDt">
                                                <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control datepicker-input" placeholder="mm/dd/yyyy"></asp:TextBox>
                                                <span class="input-group-addon datepicker-icon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Service Request/ Incident Description</label>
                                            <asp:TextBox ID="txtPB" runat="server" CssClass="form-control" Rows="3" TabIndex="15" Enabled="false" TextMode="MultiLine">                               
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Remarks</label>
                                            <asp:TextBox ID="txtProbDesc" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine"></asp:TextBox>
                                        </div>

                                    </div>

                                    <div id="divBreachedAct" visible="false" runat="server">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">Corrective Action<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvCA" runat="server" ControlToValidate="txtCorrAct" ValidationGroup="Val1"
                                                    ErrorMessage="Please Enter Corrective Action"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtCorrAct" runat="server" CssClass="form-control" Rows="3" ValidationGroup="Val1" TabIndex="15" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">Preventive Action<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvPA" runat="server" ControlToValidate="txtPrecAct" ValidationGroup="Val1"
                                                    ErrorMessage="Please Enter Preventive Action"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtPrecAct" runat="server" CssClass="form-control" Rows="3" ValidationGroup="Val1" TabIndex="15" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <br />
                                        <br />
                                        <br />
                                        <a href="#FMI" onclick="hidedetails()"><u><strong>Update More Information</strong></u></a>
                                    </div>
                                </div>

                                <br />

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12 text-left">
                                        <a id="lnkShowEscaltion" href="#" onclick="showPopWin()" enabled="false" runat="server">Click here to view SLA & Escalation</a>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 text-left">
                                        <a id="lnkShowEscaltion2" href="#" onclick="showPopWinAppr()" enabled="false" runat="server">Click here to view Approved Comments</a>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnApprove" Text="Submit" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                                                CausesValidation="true" TabIndex="17" OnClientClick="resetselectedfiles(); if ( Page_ClientValidate() ) { this.value='Submitting..'; this.disabled=true; }" UseSubmitBehavior="false" />

                                            <%--                                            <asp:Button ID="btnApprove" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                                CausesValidation="true" TabIndex="17" OnClientClick=" if ( Page_ClientValidate() ) { this.value='Submitting..'; this.disabled=true; }" UseSubmitBehavior="false"/>--%>


                                            <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="FMI" class="collapse" runat="server">
                                        <div class="row">

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txtcode">Asset Location</label>
                                                    <asp:DropDownList ID="ddlAssetLoaction" runat="server" CssClass="form-control selectpicker" Enabled="false" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>


                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txtcode">Mobile Number</label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMobile"
                                                        ErrorMessage="Please Enter Mobile Number In Digits Only" Display="None" ValidationExpression="^[0-9 ]+"
                                                        ValidationGroup="Val5">
                                                    </asp:RegularExpressionValidator>
                                                    <asp:TextBox ID="txtMobile" MaxLength="13" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12" id="RCID" runat="server">
                                                <div class="form-group">
                                                    <label for="txtcode">Repeat Calls</label>
                                                    <asp:DropDownList ID="ddlRepeatCalls" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12" id="FCID" runat="server">
                                                <div class="form-group">
                                                    <label for="txtcode">Facility</label><br />
                                                    <%--<asp:Label ID="lblFacility" runat="server"></asp:Label>--%>
                                                    <asp:TextBox ID="lblFacility" runat="server" Enabled="false" CssClass="form-control" TabIndex="15"></asp:TextBox>

                                                    <%--<asp:DropDownList ID="ddlfacility" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>--%>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12" id="IMID" runat="server">
                                                <div class="form-group">
                                                    <label for="txtcode">Impact</label>
                                                    <asp:DropDownList ID="ddlImpact" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12" id="URID" runat="server">
                                                <div class="form-group">
                                                    <label for="txtcode">Urgency</label>
                                                    <asp:DropDownList ID="ddlUrgency" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12" id="CAID" runat="server">
                                                <div class="form-group">
                                                    <label for="txtclaimamt">Claim Amount</label>
                                                    <asp:TextBox ID="txtclaimamt" runat="server" Enabled="false" CssClass="form-control" TabIndex="15"></asp:TextBox>
                                                    <%-- <asp:TextBox ID="txtclaimamt" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine" ></asp:TextBox>--%>
                                                </div>
                                            </div>
                                            
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div id="divAC" runat="server">
                                                    <div class="form-group">
                                                        <label for="txtcode">Final Cost<span id="span1" runat="server" style="color: red;" visible="false">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Val1" runat="server" Visible="false" Display="none" ControlToValidate="txtAC" ErrorMessage="Additional Cost required"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revAC" ValidationGroup="Val1" runat="server" Display="none" ControlToValidate="txtAC"
                                                            ErrorMessage="Please Enter Additional cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                        <asp:TextBox ID="txtAC" runat="server" CssClass="form-control" Width="100"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divLSC" runat="server">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtcode">Labour Cost<span id="span2" runat="server" style="color: red;" visible="false">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Val1" runat="server" Visible="false" Display="none" ControlToValidate="txtLC" ErrorMessage="Labour Cost required"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="Val1" runat="server" Display="none" ControlToValidate="txtLC"
                                                            ErrorMessage="Please Enter Additional cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                        <asp:TextBox ID="txtLC" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtcode">Spare Cost<span id="span3" runat="server" style="color: red;" visible="false">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Val1" runat="server" Visible="false" Display="none" ControlToValidate="txtSC" ErrorMessage="Spare Cost required"></asp:RequiredFieldValidator>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="Val1" runat="server" Display="none" ControlToValidate="txtSC"
                                                            ErrorMessage="Please Enter Additional cost in integers only" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                        <asp:TextBox ID="txtSC" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            <br />

                                    </div>
                                    <h4>Request History Details </h4>
                                    <div class="row">
                                        <div class="col-md-12" style="overflow-x: hidden;">
                                            <asp:GridView ID="gvReqHistory" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                EmptyDataText="No Requisition Found." CssClass="table GridStyle">
                                                <Columns>
                                                    <asp:BoundField DataField="CREATEDBY" HeaderText="Requsted By"></asp:BoundField>
                                                    <asp:BoundField DataField="UPDATEDDATE" HeaderText="Updated Date"></asp:BoundField>
                                                    <asp:BoundField DataField="ASSIGNTO" HeaderText="Assigned To"></asp:BoundField>
                                                    <asp:BoundField DataField="CONTACT_NO" HeaderText="Contact No"></asp:BoundField>
                                                    <asp:BoundField DataField="UPDATEDBY" HeaderText="Updated By"></asp:BoundField>
                                                    <asp:BoundField DataField="REMARKS" HeaderText="Remarks"></asp:BoundField>
                                                    <asp:BoundField DataField="STA_TITLE" HeaderText="Status"></asp:BoundField>
                                                    <%--  <asp:BoundField DataField="Count_Down" HeaderText="Time Left(HH:MM:SS)" ItemStyle-Width="10px" ItemStyle-ForeColor="#6666ff" ItemStyle-Font-Size="Medium">
                                                    <HeaderStyle Width="150px" HorizontalAlign="Left" Wrap="False"></HeaderStyle>
                                                    <ItemStyle Width="150" HorizontalAlign="Left" Wrap="False" />
                                                </asp:BoundField>--%>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                        </ContentTemplate>
                        <Triggers>
                            <%--<asp:PostBackTrigger ControlID="btnModify" />--%>
                            <asp:PostBackTrigger ControlID="btnApprove" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:HiddenField ID="jsonstringhdn" runat="server" />
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h4>Service History Attachments  </h4>
                            <div id="myGrid" style="height: 232px; width: 520px;" visible="false" class="ag-blue"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title">View SLA & Escalation Details</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="450px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal2" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title">Approved Comments</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modelcontainer2">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe2" width="100%" height="450px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function showPopWin() {
            $("#modalcontentframe").attr("src", "frmSLADetails.aspx?LOC_CODE=" + document.getElementById('ddlLocation').value + "&MAIN_CAT_CODE=" + encodeURIComponent(document.getElementById('ddlMainCategory').value)
                + "&SUB_CAT_CODE=" + encodeURIComponent(document.getElementById('ddlSubCategory').value) + "&CHILD_CAT_CODE=" + encodeURIComponent(document.getElementById('ddlChildCategory').value));
            $("#myModal").modal('show');
            return false;
        }

        function showPopWinAppr() {
            $("#modalcontentframe2").attr("src", "ApprovalComments.aspx");
            $("#myModal2").modal('show');
            return false;
        }
    </script>
    <script defer>
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                refreshSelectpicker();
            });

            var toDate = new Date();
            var twoDigitMonth = (parseInt(toDate.getMonth()) + parseInt(1)) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
            var twoDigitDate = toDate.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
            var currentDate = twoDigitMonth + "/" + twoDigitDate + "/" + toDate.getFullYear();
            $('#txtToDt').val(currentDate);

            function setup(id) {
                $('#' + id).datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true
                });
            };

            function refreshSelectpicker() {
                $("#<%=ddlLocation.ClientID%>").selectpicker();
                $("#<%=ddlSubCategory.ClientID%>").selectpicker();
                $("#<%=ddlChildCategory.ClientID%>").selectpicker();
                $("#<%=ddlMainCategory.ClientID%>").selectpicker();
                $("#<%=ddlAssetLoaction.ClientID%>").selectpicker();
                $("#<%=ddlStatus.ClientID%>").selectpicker();
                $("#<%=ddlRepeatCalls.ClientID%>").selectpicker();
                $("#<%=ddlImpact.ClientID%>").selectpicker();
                $("#<%=ddlUrgency.ClientID%>").selectpicker();
            }
        });
        function hidedetails() {
            $('#FMI').toggle('show');

        }
    </script>
</body>
</html>


