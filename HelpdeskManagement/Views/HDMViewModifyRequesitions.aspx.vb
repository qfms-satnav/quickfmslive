﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Imports System.IO

Partial Class HDM_HDM_Webfiles_HDMViewModifyRequesitions
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim filePath As String
    Dim feedbackVal = String.Empty
    Dim repeatCallSts As Int32
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        filePath = HttpContext.Current.Request.Url.Authority & "/UploadFiles/" & Session("TENANT")
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                'BindLocations()

                Dim UID As String = Session("uid")
                Session("reqid") = Request.QueryString("rid").ToString()

                Dim sp6 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                sp6.Command.AddParameter("@TYPE ", 6, DbType.Int32)
                ViewState("DSN_MAPPING_STA") = sp6.ExecuteScalar()

                'BindUsers(UID)
                btnApprove.Visible = False
                BindFeedback()
                BindRequisition()
                'btnModify.Visible = True

                ' Disabling the Claim and Estimated cost for Marks and spencers based on system preferences
                Dim spms As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                spms.Command.AddParameter("@TYPE ", 44, DbType.Int32)
                Dim dsms As New DataSet()
                Dim MSChanges = spms.ExecuteScalar()
                If MSChanges = 1 Then
                    CAID.Visible = False
                    FCID.Visible = False
                    IMID.Visible = False
                    URID.Visible = False
                    RCID.Visible = False
                Else
                    CAID.Visible = True
                    FCID.Visible = True
                    IMID.Visible = True
                    URID.Visible = True
                    RCID.Visible = True
                End If

                If lblStatus.Text = "9" Then
                    ' CHECK AUTO REPEAT CALL YES/NO                
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                    sp.Command.AddParameter("@TYPE ", 2, DbType.Int32)
                    repeatCallSts = sp.ExecuteScalar()
                    ''Session("repeatCallSts") = repeatCallSts
                    If repeatCallSts = 1 Then
                        btnReopen.Visible = True
                    Else
                        btnReopen.Visible = False
                    End If

                    txtRemarks.Visible = True
                    btnModify.Visible = False

                    If String.IsNullOrEmpty(feedbackVal) Then
                        showFeedback.Visible = True
                        btnApprove.Visible = True
                    Else
                        showFeedback.Visible = True
                        ddlFeedback.Enabled = False
                        ddlAck.Enabled = False
                    End If
                Else
                    showFeedback.Visible = False
                    txtRemarks.Visible = False
                    btnApprove.Visible = False
                End If
                If lblStatus.Text = "1" Or lblStatus.Text = "2" Or lblStatus.Text = "8" Or lblStatus.Text = "13" Then
                    btnModify.Visible = True
                    fu1.Enabled = True
                Else
                    btnModify.Visible = False
                    fu1.Enabled = True
                End If
                If lblStatus.Text = "45" Then
                    btnModify.Visible = True
                    ddlChildCategory.Enabled = True
                    txtProbDesc.Enabled = True
                End If
                If lblStatus.Text = "6" Or lblStatus.Text = "7" Then
                    txtProbDesc.Enabled = True
                    btnModify.Visible = False

                End If
            End If
        End If
    End Sub

    Private Sub BindRequisition()
        Try
            Dim ds As New DataSet
            Dim param As SqlParameter() = New SqlParameter(0) {}
            param(0) = New SqlParameter("@REQID", Session("reqid").ToString())
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_REQUISITION_BY_REQ_ID", param)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblReqId.Text = ds.Tables(0).Rows(0)("REQUEST_ID").ToString()
                    txtEmp.Text = ds.Tables(0).Rows(0).Item("REQUESTED_BY")
                    txtDepartment.Text = ds.Tables(0).Rows(0)("DEPARTMENT").ToString()
                    txtSpaceID.Text = ds.Tables(0).Rows(0)("SPACE_ID").ToString()
                    lblStatus.Text = ds.Tables(0).Rows(0)("STATUS ID").ToString()
                    ViewState("DSN_CODE") = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                    feedbackVal = ds.Tables(0).Rows(0)("FEEDBACK_CODE").ToString()
                    txtclaimamount.Visible = True
                    txtclaimamount.Text = ds.Tables(0).Rows(0).Item("SER_CLAIM_AMT")
                    ddlFeedback.ClearSelection()
                    If Not String.IsNullOrEmpty(feedbackVal) Then
                        ddlFeedback.Items.FindByValue(feedbackVal).Selected = True
                        ddlFeedback.Enabled = False
                    End If

                    ddlAck.ClearSelection()
                    If Not String.IsNullOrEmpty(ds.Tables(0).Rows(0)("FEEDBACK_ACKMT").ToString()) Then
                        ddlAck.Items.FindByValue(ds.Tables(0).Rows(0)("FEEDBACK_ACKMT").ToString()).Selected = True
                        ddlAck.Enabled = False
                    End If

                    BindStatus()
                    ddlStatus.ClearSelection()
                    If ds.Tables(0).Rows(0)("STATUS ID").ToString <> "" AndAlso ds.Tables(0).Rows(0)("STATUS ID").ToString() IsNot Nothing Then
                        ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0)("STATUS ID").ToString()).Selected = True
                    End If
                    If ddlStatus.SelectedValue = 13 Then
                        btnModify.Text = "Submit"
                        txtcode.Visible = False
                        ddlStatus.Visible = False
                    Else
                        btnModify.Text = "Modify"
                        txtcode.Visible = True
                        ddlStatus.Visible = True
                    End If

                    If lblStatus.Text = "1" Or lblStatus.Text = "2" Or lblStatus.Text = "8" Or lblStatus.Text = "13" Or lblStatus.Text = "45" And lblStatus.Text <> "" Then
                        btnModify.Visible = True
                        ddlLocation.Enabled = True
                        ddlMainCategory.Enabled = True
                        ddlSubCategory.Enabled = True
                        ddlChildCategory.Enabled = True
                        ddlAssetLoaction.Enabled = True
                        txtMobile.Enabled = True
                        ddlRepeatCalls.Enabled = True
                        ddlImpact.Enabled = True
                        ddlUrgency.Enabled = True
                        txtProbDesc.Enabled = True
                        fu1.Enabled = False
                    Else
                        ddlLocation.Enabled = False
                        ddlMainCategory.Enabled = False
                        ddlSubCategory.Enabled = False
                        ddlChildCategory.Enabled = False
                        ddlAssetLoaction.Enabled = False
                        txtMobile.Enabled = False
                        ddlRepeatCalls.Enabled = False
                        ddlImpact.Enabled = False
                        ddlUrgency.Enabled = False
                        txtProbDesc.Enabled = False
                        fu1.Enabled = False
                        btnModify.Visible = False
                    End If

                    If lblStatus.Text = "1" Or lblStatus.Text = "2" Or lblStatus.Text = "8" Or lblStatus.Text = "10" Then
                        btnCancel.Visible = True
                    Else
                        btnCancel.Visible = False
                    End If
                    BindLocations()
                    ddlLocation.ClearSelection()
                    If ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() IsNot Nothing Then
                        ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString()).Selected = True
                    End If
                    BindMainCategory()

                    ddlMainCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() IsNot Nothing Then
                        ddlMainCategory.Items.FindByValue(ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString()).Selected = True
                    End If
                    BindSubCategory()
                    ddlSubCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() IsNot Nothing Then
                        ddlSubCategory.Items.FindByValue(ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString()).Selected = True
                    End If
                    BindChildCategory()
                    ddlChildCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() IsNot Nothing Then
                        ddlChildCategory.Items.FindByValue(ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString()).Selected = True
                    End If
                    BindAssetLocations()
                    ddlAssetLoaction.ClearSelection()
                    If ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() IsNot Nothing Then
                        ddlAssetLoaction.Items.FindByValue(ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString()).Selected = True
                    End If
                    txtMobile.Text = ds.Tables(0).Rows(0)("MOBILE").ToString()
                    BindRepeatCalls()
                    ddlRepeatCalls.ClearSelection()
                    If ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() IsNot Nothing Then
                        ddlRepeatCalls.Items.FindByValue(ds.Tables(0).Rows(0)("REPEAT_CALL").ToString()).Selected = True
                    End If
                    BindImpact()
                    ddlImpact.ClearSelection()
                    If ds.Tables(0).Rows(0)("IMPACT").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("IMPACT").ToString() IsNot Nothing Then
                        ddlImpact.Items.FindByValue(ds.Tables(0).Rows(0)("IMPACT").ToString()).Selected = True
                    End If
                    BindUrgency()
                    ddlUrgency.ClearSelection()
                    If ds.Tables(0).Rows(0)("URGENCY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("URGENCY").ToString() IsNot Nothing Then
                        ddlUrgency.Items.FindByValue(ds.Tables(0).Rows(0)("URGENCY").ToString()).Selected = True
                    End If

                    txtProbDesc.Text = ds.Tables(0).Rows(0)("REMARKS").ToString()

                    BindFacilities(ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString())
                    ddlfacility.ClearSelection()
                    If ds.Tables(0).Rows(0)("FD_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("FD_CODE").ToString() IsNot Nothing Then
                        ddlfacility.Items.FindByValue(ds.Tables(0).Rows(0)("FD_CODE").ToString()).Selected = True
                    End If

                    Dim list As List(Of fileInfo)
                    list = GetFiles()
                    Session.Add("ExistingFileList", list)
                    Dim jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(list)
                    jsonstringhdn.Value = jsonstring
                End If
            End If

            'HISTORY IN GRID
            Dim dsHistory As New DataSet
            Dim spHist As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_REQUEST_HISTORY_DETAILS")
            spHist.Command.AddParameter("@REQID", Session("reqid"), Data.DbType.String)
            dsHistory = spHist.GetDataSet()
            gvReqHistory.DataSource = dsHistory
            gvReqHistory.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Class fileInfo
        Public Property Name As String
        Public Property Path As String
        Public Property UplTimeName As String
    End Class

    Public Function GetFiles() As List(Of fileInfo)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_FILES")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, Data.DbType.String)
        Dim fileslist As List(Of fileInfo) = New List(Of fileInfo)()

        Dim dr As SqlDataReader
        dr = sp.GetReader()
        While (dr.Read())
            fileslist.Add(New fileInfo() With {.Name = dr.GetString(0), .Path = "/UploadFiles/" & Session("TENANT") & "/" & dr.GetString(1), .UplTimeName = dr.GetString(1)})
        End While
        dr.Close()
        Return fileslist
    End Function

    Public Sub BindStatus()
        ddlStatus.Items.Clear()
        ObjSubsonic.Binddropdown(ddlStatus, "HDM_STATUS_BIND", "STA_TITLE", "STA_ID")
    End Sub

    Public Sub BindFacilities(ByVal loc As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 50)
        param(0).Value = loc
        param(1) = New SqlParameter("@CMP_ID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlfacility, "HDM_GET_FACILITY_DTLS", "FD_NAME", "FD_CODE", param)
    End Sub

    Private Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindMainCategory()
        'ddlMainCategory.Items.Clear()
        'ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE")
        ddlMainCategory.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(0).Value = ViewState("DSN_MAPPING_STA")
        param(1) = New SqlParameter("@DSN_CODE", DbType.String)
        param(1).Value = ViewState("DSN_CODE")
        ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE", param)
    End Sub

    Public Sub BindSubCategory()
        'ddlSubCategory.Items.Clear()
        'ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
        ddlSubCategory.Items.Clear()
        Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        'param(0).Value = ddlMainCategory.SelectedValue
        param(0) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(0).Value = ViewState("DSN_MAPPING_STA")
        param(1) = New SqlParameter("@DSN_CODE", DbType.String)
        param(1).Value = ViewState("DSN_CODE")
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE", param)
    End Sub

    Public Sub BindChildCategory()
        'ddlChildCategory.Items.Clear()
        'ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
        ddlChildCategory.Items.Clear()
        Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        'param(0).Value = ddlMainCategory.SelectedValue
        'param(1) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 50)
        'param(1).Value = ddlSubCategory.SelectedValue
        param(0) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(0).Value = ViewState("DSN_MAPPING_STA")
        param(1) = New SqlParameter("@DSN_CODE", DbType.String)
        param(1).Value = ViewState("DSN_CODE")
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
    End Sub

    Public Sub BindAssetLocations()
        ddlAssetLoaction.Items.Clear()
        ObjSubsonic.Binddropdown(ddlAssetLoaction, "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS", "HAL_LOC_NAME", "HAL_LOC_CODE")
    End Sub

    Public Sub BindRepeatCalls()
        ddlRepeatCalls.Items.Clear()
        ObjSubsonic.Binddropdown(ddlRepeatCalls, "HDM_GET_ALL_ACTIVE_REPEATCALLS", "RPT_NAME", "RPT_CODE")
    End Sub

    Public Sub BindImpact()
        ddlImpact.Items.Clear()
        ObjSubsonic.Binddropdown(ddlImpact, "HDM_GET_ALL_ACTIVE_IMPACT", "IMP_NAME", "IMP_CODE")
    End Sub

    Public Sub BindUrgency()
        ddlUrgency.Items.Clear()
        ObjSubsonic.Binddropdown(ddlUrgency, "HDM_GET_ALL_ACTIVE_URGENCY", "UGC_NAME", "UGC_CODE")
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            lblMessage.Visible = True
            If ddlFeedback.SelectedIndex > 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_INSERT_USER_FEEDBACK")
                sp.Command.AddParameter("@REQ_ID", lblReqId.Text, Data.DbType.String)
                sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
                sp.Command.AddParameter("@FBD_CODE", ddlFeedback.SelectedValue, DbType.String)
                sp.Command.AddParameter("@ACK", ddlAck.SelectedValue, DbType.Int32)
                sp.Command.AddParameter("@REMARKS", txtRemarks.Text)
                sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
                sp.Execute()
                lblMessage.Text = "Thank You For Your Feedback"
                ddlFeedback.Enabled = False
                btnApprove.Visible = False
                btnReopen.Visible = False
            Else
                lblMessage.Text = "Please Select Feedback"
                Exit Sub
            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlFeedback_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFeedback.SelectedIndexChanged
        'If Session("repeatCallSts") = 1 Then
        '    btnReopen.Visible = True
        'Else
        If (ddlFeedback.SelectedItem.Text = "Re-Open Ticket") Then

                btnReopen.Visible = True
                btnApprove.Visible = False
            Else

                lblMessage.Text = ""
            btnReopen.Visible = True
            btnApprove.Visible = True
            End If
        ''End If
    End Sub

    'Public Sub BindFeedback()
    '    Dim ds1 As New DataSet
    '    ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_FEEDBACK_USER")
    '    ddlFeedback.DataSource = ds1
    '    ddlFeedback.DataTextField = "FBD_NAME"
    '    ddlFeedback.DataValueField = "FBD_CODE"
    '    ddlFeedback.DataBind()
    '    ddlFeedback.Items.Insert(0, "--Select--")
    '    ddlFeedback.ClearSelection()
    'End Sub

    Public Sub BindFeedback()
        Dim ds1 As New DataSet
        ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_FEEDBACK_USER")

        Dim selectedValue As String = ddlFeedback.SelectedValue
        ddlFeedback.Items.Clear()

        For Each row As DataRow In ds1.Tables(0).Rows
            Dim feedbackCode As String = row("FBD_CODE").ToString()
            Dim feedbackName As String = row("FBD_NAME").ToString()
            Dim imagePath As String = row("FBD_IMAGE").ToString()

            Dim item As New ListItem(feedbackName, feedbackCode)

            'Dim imagePath As String = "/BootStrapCSS/assets/img/default.png"

            'Select Case feedbackCode
            '    Case "EXC"
            '        imagePath = "/BootStrapCSS/assets/img/EXC.png"
            '    Case "AVG"
            '        imagePath = "/BootStrapCSS/assets/img/AVG.png"
            '    Case "Bad"
            '        imagePath = "/BootStrapCSS/assets/img/Bad.png"
            '    Case "TFB"
            '        imagePath = "/BootStrapCSS/assets/img/TFB.png"
            'End Select

            item.Attributes.Add("data-image", imagePath)

            item.Attributes.Add("style", "background-image: url('" & imagePath & "'); padding-left: 30px; background-repeat: no-repeat; background-position: left center;background-size : contain")

            ddlFeedback.Items.Add(item)
        Next

        ddlFeedback.Items.Insert(0, New ListItem("--Select--", ""))
        'ddlFeedback.ClearSelection()
        ddlFeedback.SelectedValue = selectedValue
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CANCEL_VIEW_REQUISITON")
            sp.Command.AddParameter("@SER_ID", lblReqId.Text, DbType.String)
            sp.Command.AddParameter("@SER_STATUS", 3, DbType.String)
            sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
            sp.Command.AddParameter("@SER_PROB_DESC", txtProbDesc.Text, Data.DbType.String)
            sp.Execute()
            lblMessage.Visible = True
            lblMessage.Text = "Request Cancelled successfully."

        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmHDMViewRequisitions.aspx")
    End Sub

    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        Try
            Dim MaintenanceReq_Upl As String = ""
            MaintenanceReq_Upl = lblReqId.Text.Replace("/", "_")
            Dim Req_status As Integer
            If lblStatus.Text = "6" Or lblStatus.Text = "7" Then
                Req_status = 2

            Else
                If ViewState("DSN_MAPPING_STA") = 1 Then
                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_APPROVAL_LEVELS_BY_CATEGORIES")
                    sp1.Command.AddParameter("@MNC_CODE", ddlMainCategory.SelectedItem.Value, DbType.String)
                    sp1.Command.AddParameter("@SUB_CAT", ddlSubCategory.SelectedItem.Value, DbType.String)
                    sp1.Command.AddParameter("@CHILD_CAT", ddlChildCategory.SelectedItem.Value, DbType.String)
                    ViewState("APPR_LEVELS") = sp1.ExecuteScalar()

                    If ViewState("APPR_LEVELS") = 0 Then
                        Req_status = 2
                    Else
                        Req_status = 8
                    End If
                Else
                    Req_status = 2
                End If
            End If
            If lblStatus.Text = "45" Then
                Req_status = 45
            End If


            Dim list As List(Of fileInfo)
            If fu1.PostedFiles IsNot Nothing Then
                Dim selectedfiles = Request.Form("selectedfiles")

                If selectedfiles <> Nothing Then
                    Dim selectedfilesarray = selectedfiles.Split(",")
                    If fu1.PostedFiles.Count > 0 And fu1.PostedFiles(0).FileName <> Nothing Then
                        For fucount As Integer = 0 To selectedfilesarray.Length - 1
                            'Dim intsixe As Long = fu1.PostedFiles(fucount).ContentLength
                            For Each File In fu1.PostedFiles
                                If selectedfilesarray(fucount).Equals(File.FileName) Then
                                    Dim intsize As Long = CInt(File.ContentLength)
                                    Dim strFileName As String
                                    Dim strFileExt As String
                                    If intsize <= 20971520 Then
                                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("hhmmss")
                                        strFileName = System.IO.Path.GetFileName(File.FileName)
                                        strFileName = strFileName.Replace(" ", "_")
                                        strFileExt = System.IO.Path.GetExtension(File.FileName)
                                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Session("TENANT") & "\" & MaintenanceReq_Upl & "_" & Upload_Time & "_" & strFileName '& "." & strFileExt
                                        File.SaveAs(filePath)

                                        Dim filename As String = selectedfilesarray(fucount)
                                        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_MODIFY_UPLOADED_FILES")
                                        sp2.Command.AddParameter("@REQ_ID", lblReqId.Text, DbType.String)
                                        sp2.Command.AddParameter("@STATUS_ID", Req_status, DbType.Int32)
                                        sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String)
                                        sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String)
                                        sp2.Command.AddParameter("@REMARKS", txtProbDesc.Text, DbType.String)
                                        sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
                                        sp2.ExecuteScalar()
                                    End If
                                End If
                            Next
                        Next
                    End If

                    list = Session("ExistingFileList")
                    For Each sfile In list
                        Dim found = selectedfilesarray.Contains(sfile.Name)
                        If Not found Then
                            Dim filetoremove = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Session("TENANT") & "\" & sfile.Name
                            Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmmss")
                            Dim strFileName As String
                            strFileName = System.IO.Path.GetFileName(sfile.Name)
                            Dim Path As String = sfile.UplTimeName

                            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_DELETE_UPLOADED_FILES")
                            sp3.Command.AddParameter("@REQ_ID", lblReqId.Text, DbType.String)
                            sp3.Command.AddParameter("@FILE_NAME", strFileName, DbType.String)
                            sp3.Command.AddParameter("@UPLOADED_PATH", Path, DbType.String)
                            sp3.ExecuteScalar()
                            System.IO.File.Delete(filetoremove)
                        End If
                    Next
                Else
                    Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_DELETE_UPLOADED_FILES_REQ_ID")
                    sp4.Command.AddParameter("@REQ_ID", lblReqId.Text, DbType.String)
                    sp4.ExecuteScalar()
                End If
            Else
                lblMessage.Text = "Select Uploadable file by clicking the Browse button"
            End If
            list = GetFiles()
            Session.Add("ExistingFileList", list)
            Dim jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(list)
            jsonstringhdn.Value = jsonstring

            If lblStatus.Text = "6" Or lblStatus.Text = "7" Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_MODIFY_REQUISITION_BY_REQ_ID_ONHOLD_INPROGRESS")
                sp.Command.AddParameter("@REQID", lblReqId.Text, Data.DbType.String)
                sp.Command.AddParameter("@SER_PROB_DESC ", txtProbDesc.Text, DbType.String)
                sp.Execute()
            Else
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_MODIFY_REQUISITION_BY_REQ_ID")
                sp.Command.AddParameter("@REQID", lblReqId.Text, Data.DbType.String)
                sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
                sp.Command.AddParameter("@SER_LOC_CODE ", ddlLocation.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SER_MNC_CODE", ddlMainCategory.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SER_SUB_CAT_CODE ", ddlSubCategory.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SER_CHILD_CAT_CODE", ddlChildCategory.SelectedValue, DbType.String)
                sp.Command.AddParameter("@STA_ID ", Req_status, DbType.Int32)
                sp.Command.AddParameter("@SER_AST_LOC_CODE ", ddlAssetLoaction.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SER_MOBILE ", txtMobile.Text, DbType.String)
                sp.Command.AddParameter("@SER_CALL_TYPE ", ddlRepeatCalls.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SER_IMPACT ", ddlImpact.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SER_URGENCY ", ddlUrgency.SelectedValue, DbType.String)
                sp.Command.AddParameter("@SER_PROB_DESC ", txtProbDesc.Text, DbType.String)
                sp.Command.AddParameter("@SER_CLAIM_AMT ", txtclaimamount.Text, DbType.String)
                'sp.Command.AddParameter("@SER_DOC_LINK", FLstr, DbType.String)
                sp.Execute()
            End If

            lblMessage.Visible = True
            If btnModify.Text = "Modify" Then
                lblMessage.Text = "Service Requisition (" + lblReqId.Text + ") Modified successfully. "
            Else
                lblMessage.Text = "Service Requisition (" + lblReqId.Text + ") raised successfully..."
            End If

        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlChildCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChildCategory.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 And ddlMainCategory.SelectedIndex > 0 And ddlSubCategory.SelectedIndex > 0 And ddlChildCategory.SelectedIndex > 0 Then
            lnkShowEscaltion.Visible = True
        Else
            lnkShowEscaltion.Visible = False
        End If
        Dim dr As SqlDataReader = Child_getmain_dtls(ddlChildCategory.SelectedValue)
        If (dr.Read) Then

            ddlSubCategory.SelectedValue = dr("CHC_TYPE_SUBC_CODE")
            ddlMainCategory.SelectedValue = dr("CHC_TYPE_MNC_CODE")
        End If
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_SERVICE_INCHARGE")
        sp1.Command.AddParameter("@LOC", ddlLocation.SelectedItem.Value, DbType.String)
        'sp1.Command.AddParameter("@TWR", ddlTower.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@MAIN_CATEGORY", ddlMainCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SUB_CATEGORY", ddlSubCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@CHILD_CATEGORY", ddlChildCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim flag1 As Integer = sp1.ExecuteScalar()
        Dim Ser_Inch_errmsg As String = String.Empty
        If flag1 = 0 Then
            'Ser_Inch_errmsg = "Service Incharges are Not mapped to the selected service."
            lblMessage.Text = "Service Incharges are Not mapped to the selected service."
            btnModify.Visible = False
            lblMessage.Visible = True
            Exit Sub
        Else
            lblMessage.Visible = False
            btnModify.Visible = True
        End If
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_ESCALTIONS")
        sp2.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        sp2.Command.AddParameter("@MAIN_CATEGORY", ddlMainCategory.SelectedItem.Value, DbType.String)
        sp2.Command.AddParameter("@SUB_CATEGORY", ddlSubCategory.SelectedItem.Value, DbType.String)
        sp2.Command.AddParameter("@CHILD_CATEGORY", ddlChildCategory.SelectedItem.Value, DbType.String)
        sp2.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim flag2 As Integer = sp2.ExecuteScalar()
        If flag2 = 0 Then
            lblMessage.Text = Ser_Inch_errmsg + "<br/> SLA was not Defined to the selected service."
            lblMessage.Visible = True
            btnModify.Visible = False
            Exit Sub
        Else
            btnModify.Visible = True
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        ddlMainCategory.SelectedIndex = 0
        lblMessage.Visible = False
    End Sub

    Protected Sub ddlMainCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMainCategory.SelectedIndexChanged
        ddlSubCategory.SelectedIndex = 0
        lblMessage.Visible = False
        If ddlMainCategory.SelectedIndex <> 0 Then
            ddlSubCategory.Items.Clear()
            BindSubCategory_By_MainCategory(ddlMainCategory.SelectedItem.Value, ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
        Else
            ddlSubCategory.Items.Clear()
            ddlSubCategory.Items.Insert(0, "--Select--")
            ddlChildCategory.Items.Clear()
            ddlChildCategory.Items.Insert(0, "--Select--")
        End If
    End Sub

    Public Sub BindSubCategory_By_MainCategory(Main_Category As String, DSN_STA As Integer, DSN_CODE As String)
        'ddlSubCategory.Items.Clear()
        'Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        'param(0).Value = Main_Category
        'ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_SUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
        ddlSubCategory.Items.Clear()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = Main_Category
        param(1) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(1).Value = DSN_STA
        param(2) = New SqlParameter("@DSN_CODE", DbType.String)
        param(2).Value = DSN_CODE
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_SUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
    End Sub

    Protected Sub ddlSubCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubCategory.SelectedIndexChanged
        ddlChildCategory.SelectedIndex = 0
        lblMessage.Visible = False

        If ddlMainCategory.SelectedIndex <> 0 And ddlSubCategory.SelectedIndex <> 0 Then
            ddlChildCategory.Items.Clear()
            BindChildCategory_By_SubCategory(ddlMainCategory.SelectedItem.Value, ddlSubCategory.SelectedItem.Value, ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"), Session("Uid"))
        Else
            ddlChildCategory.Items.Clear()
            ddlChildCategory.Items.Insert(0, "--Select--")
        End If

    End Sub

    Public Sub BindChildCategory_By_SubCategory(Main_Category As String, Sub_Category As String, DSN_STA As Integer, DSN_CODE As String, EMPID As String)
        'ddlChildCategory.Items.Clear()
        'Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        'param(0).Value = Main_Category
        'param(1) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 50)
        'param(1).Value = Sub_Category
        'ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
        ddlChildCategory.Items.Clear()
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = Main_Category
        param(1) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = Sub_Category
        param(2) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(2).Value = DSN_STA
        param(3) = New SqlParameter("@DSN_CODE", DbType.String)
        param(3).Value = DSN_CODE
        param(4) = New SqlParameter("@EMP_ID", DbType.String)
        param(4).Value = EMPID
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
    End Sub

    Protected Sub btnReopen_Click(sender As Object, e As EventArgs) Handles btnReopen.Click
        If txtRemarks.Text = "" Then
            lblMessage.Text = "Please Enter Remarks"
        Else

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_MODIFY_REQ_REOPEN_TICKET")
            sp.Command.AddParameter("@REQID", Session("reqid"), DbType.String)
            sp.Command.AddParameter("@AURID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@SER_CALL_TYPE", "Y", DbType.String)   'REPEAT CALL
            sp.ExecuteScalar()
            Response.Redirect("frmHDMViewRequisitions.aspx?Reopened=1")
        End If
    End Sub
    Public Function Child_getmain_dtls(ByVal CHC_CODE As String) As SqlDataReader
        'strSQL = "select twr_cty_id,twr_loc_id,twr_cny_id from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code='" & tow_id & "' "
        'Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Dim sp1 As SqlParameter = New SqlParameter("@CHC_CODE", SqlDbType.NVarChar, 50)
        sp1.Value = CHC_CODE
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_MAIN_SUB_BY_CHILD_CATEGORY", sp1)
        Return ds
    End Function
End Class
