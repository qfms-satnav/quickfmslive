﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class HDM_HDM_Webfiles_frmReqHistoryDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Dim Req_id As String
            Req_id = Request.QueryString("Reqid")
        
            Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GET_REQUEST_HISTORY_DETAILS")
            sp.Command.AddParameter("@REQID", Req_id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()

            If ds.Tables(0).Columns.Count > 1 And ds.Tables(0).Rows.Count > 0 Then
                gvHist.DataSource = ds.Tables(0)
                gvHist.DataBind()
            Else
                gvHist.DataSource = Nothing
                gvHist.DataBind()
            End If
        End If
    End Sub
End Class
