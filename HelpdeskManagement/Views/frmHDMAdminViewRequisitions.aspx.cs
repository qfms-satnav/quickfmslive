﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class HelpdeskManagement_Views_frmHDMAdminViewRequisitions : System.Web.UI.Page
{
    private clsSubSonicCommonFunctions ObjSubsonic = new clsSubSonicCommonFunctions();
    private int TotalRows;
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
        if (Session["UID"].ToString() == "")
            Response.Redirect(Application["FMGLogout"].ToString());
        if (!IsPostBack)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string host = HttpContext.Current.Request.Url.Host;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            {
                Value = Session["UID"]
            };
            param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            {
                Value = path
            };
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
            {
                if (!sdr.HasRows)
                {
                    Response.Redirect(Application["FMGLogout"].ToString());
                }
            }
            txtfromDt.Text = DateTime.Now.AddDays(-29).ToString("MM/dd/yyyy");
            txtToDt.Text = DateTime.Now.ToString("MM/dd/yyyy");
            Session["StartTime"] = 9;
            Session["EndTime"] = 18;
            Session["StartMinutes"] = 0;
            Session["EndMinutes"] = 0;
            Session["HolidayList"] = new List<HolidayVM>();
            BindGrid();
            BindLocations();
            BindSubCategory();
            BindChildCategory();
            BindMainCategory();
            FillStatus();
            // lbls.Visible = False
            if (gvViewRequisitions.Rows.Count == 0)
            {
                showDiv.Visible = false;
                btnSubmit.Visible = false;
            }
            else
            {
                showDiv.Visible = true;
                btnSubmit.Visible = true;
            }
        }
    }
    private void GetTotalRecordsCount()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_VIEW_REQUISITIONS_TO_ASSIGN_COUNT");
        sp.Command.AddParameter("@AurId", Session["UID"], DbType.String);
        DataSet dsCount = sp.GetDataSet();
        if (dsCount.Tables.Count > 0)
            TotalRows = Convert.ToInt32(dsCount.Tables[0].Rows[0]["COUNT"].ToString());
    }

    protected void linkButton_Click(object sender, EventArgs e)
    {
        int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
        LinkButton lb = sender as LinkButton;
        BindGrid(pageIndex, 10);
        GetTotalRecordsCount();
        DatabindRepeater(pageIndex, lb.Text);
    }
    public class RepeaterListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string IsActive { get; set; }
        public RepeaterListItem(string name, string input, bool status)
        {
            Text = name;
            Value = input;
            if (!status)
                IsActive = "active";
        }
    }
    private void DatabindRepeater(int currentPage, string pageName = "")
    {
        double dblPageCount = Convert.ToDouble(Convert.ToDouble(TotalRows) / (double)Convert.ToDecimal(10));
        int pageCount = Convert.ToInt32(Math.Ceiling(dblPageCount));
        List<RepeaterListItem> pages = new List<RepeaterListItem>();
        int i = 0;
        if (pageCount > 0)
        {
            if (pageCount - currentPage <= 10 && pageName != "<<<")
            {
                int remainder = currentPage % 10;
                int startNo = (currentPage - remainder) + 1;
                if (remainder == 0)
                    startNo = currentPage - 10 + 1;
                pages.Add(new RepeaterListItem("<<<", (startNo - 1).ToString(), true));
                for (i = startNo; i <= pageCount - 1; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
            }
            else if (pageName == ">>>")
            {
                pages.Add(new RepeaterListItem("<<<", (currentPage - 1).ToString(), true));
                for (i = currentPage; i <= currentPage + 9; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
            else if (pageName == "<<<")
            {
                int remainder = currentPage % 10;
                int startNo = (currentPage - remainder) + 1;
                if (remainder == 0)
                    startNo = currentPage - 10 + 1;
                if (startNo > 10)
                    pages.Add(new RepeaterListItem("<<<", (startNo - 1).ToString(), true));
                for (i = startNo; i <= startNo + 10 - 1; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
            else if (currentPage == 1)
            {
                for (i = 1; i <= 10; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
            else
            {
                int remainder = currentPage % 10;
                int startNo = (currentPage - remainder) + 1;
                if (remainder == 0)
                    startNo = currentPage - 10 + 1;
                if (startNo > 10)
                    pages.Add(new RepeaterListItem("<<<", (startNo - 1).ToString(), true));
                for (i = startNo; i <= startNo + 10 - 1; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
        }
        repeaterPaging.DataSource = pages;
        repeaterPaging.DataBind();
    }
    private void FillStatus()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_HDMSTATUS_FILTERS");
        sp.Command.AddParameter("@TYPE", 3, DbType.String);
        ddlStatus.DataSource = sp.GetDataSet();
        ddlStatus.DataTextField = "STA_TITLE";
        ddlStatus.DataValueField = "STA_ID";
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, "--Select--");
    }


    private void BindGrid(int pageNumber = 0, int pageSize = 10)
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_VIEW_REQUISITIONS_TO_ASSIGN_NEW");
        sp.Command.AddParameter("@AurId", Session["UID"], DbType.String);
        sp.Command.AddParameter("@CompanyId", Session["COMPANYID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String);
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String);
        ds = sp.GetDataSet();
        Session["Reqs"] = ds;
        gvViewRequisitions.DataSource = ds;
        gvViewRequisitions.DataBind();
        if (ds.Tables.Count > 1 && ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 0)
        {
            var hoildayJson = JsonConvert.SerializeObject(ds.Tables[1]);
            Session["HolidayList"] = JsonConvert.DeserializeObject<List<HolidayVM>>(hoildayJson);
        }
        if (ds.Tables.Count > 1)
        {
            var startValue = ds.Tables[2].Rows[0]["SYSP_VAL1"].ToString().Split(':');
            var endValue = ds.Tables[2].Rows[0]["SYSP_VAL2"].ToString().Split(':');
            Session["StartTime"] = Convert.ToInt32(startValue[0]);
            Session["EndTime"] = Convert.ToInt32(endValue[0]);
            if (startValue.Length > 1)
            {
                Session["StartMinutes"] = Convert.ToInt32(startValue[1]);
            }
            if (endValue.Length > 1)
            {
                Session["EndMinutes"] = Convert.ToInt32(endValue[1]);
            }
        }
        CalculateTAT();
    }

    public void CalculateTAT()
    {
        SubSonic.StoredProcedure spSts = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_USER_STATUS_BYROLE");
        spSts.Command.AddParameter("@AUR_ID", Session["UID"], System.Data.DbType.String);
        var ds1 = spSts.GetDataSet();
        for (int i = 0; i <= gvViewRequisitions.Rows.Count - 1; i++)
        {
            string reqid = ((Label)gvViewRequisitions.Rows[i].FindControl("lblreqid")).Text;
            string status = ((Label)gvViewRequisitions.Rows[i].FindControl("lblStatus")).Text;
            DropDownList ddlAssign = (DropDownList)gvViewRequisitions.Rows[i].FindControl("ddlAssignTo");
            if (status == "Closed")
                ddlAssign.Enabled = false;
            else
                ddlAssign.Enabled = true;
            string loc = ((Label)gvViewRequisitions.Rows[i].FindControl("lblLocation")).Text;
            string mainCat = ((Label)gvViewRequisitions.Rows[i].FindControl("lblMain")).Text;
            string subCat = ((Label)gvViewRequisitions.Rows[i].FindControl("lblSub")).Text;
            string childCat = ((Label)gvViewRequisitions.Rows[i].FindControl("lblChild")).Text;
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@SEM_LOC_CODE", loc);
            param[1] = new SqlParameter("@SEM_MNC_CODE", mainCat);
            param[2] = new SqlParameter("@SEM_SUBC_CODE", subCat);
            param[3] = new SqlParameter("@SEM_CHC_CODE", childCat);
            param[4] = new SqlParameter("@COMPANYID", Session["COMPANYID"]);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_BIND_INCHARGES", param);

            // Dim ddlAssign As DropDownList = CType(e.Row.FindControl("ddlAssignTo"), DropDownList)
            ddlAssign.DataSource = ds;
            ddlAssign.DataTextField = "ASSIGNED_TO";
            ddlAssign.DataValueField = "AUR_ID";
            ddlAssign.ClearSelection();
            ddlAssign.DataBind();
            ddlAssign.Items.Insert(0, "--Select--");

            Label callogDate = (Label)gvViewRequisitions.Rows[i].FindControl("lblCallLogDt");
            DateTime createdDt = Convert.ToDateTime(callogDate.Text);
            double totalMinutes = 0;
            Label location = (Label)gvViewRequisitions.Rows[i].FindControl("lblLocation");
            if (createdDt.Date == DateTime.Today)
            {
                totalMinutes = CalculateTodayHours(createdDt,
                    Convert.ToInt32(Session["StartTime"].ToString() ?? "9"),
                    Convert.ToInt32(Session["EndTime"].ToString() ?? "18"),
                    Convert.ToInt32(Session["StartMinutes"].ToString() ?? "0"),
                    Convert.ToInt32(Session["EndMinutes"].ToString() ?? "0"), (List<HolidayVM>)Session["HolidayList"], location.Text);
            }
            else
            {
                totalMinutes = CalculateTotalMinutes(createdDt,
                    Convert.ToInt32(Session["StartTime"].ToString() ?? "9"),
                    Convert.ToInt32(Session["EndTime"].ToString() ?? "18"),
                    Convert.ToInt32(Session["StartMinutes"].ToString() ?? "0"),
                    Convert.ToInt32(Session["EndMinutes"].ToString() ?? "0"),
                   (List<HolidayVM>)Session["HolidayList"], location.Text);
            }
            Label timeTaken = (Label)gvViewRequisitions.Rows[i].FindControl("lblTAT");
            timeTaken.Text = ConvertSectoDay(Convert.ToInt32(totalMinutes));
        }
    }

    private string ConvertSectoDay(int n)
    {
        int days = (int)Math.Floor(n / (double)540);
        int hours = (int)Math.Floor((n % 540) / (double)60);
        int mins = n % 60;
        return (days > 0 ? (days + " " + "Days ") : "") + hours + " " + "Hr " + mins + " " + "Min ";
    }
    private double CalculateTodayHours(DateTime startDate, int startTime, int endTime, int startMinutes, int endMinutes, List<HolidayVM> holidayVMs, string locationCode)
    {
        double time = 0;

        if (startDate.DayOfWeek == DayOfWeek.Sunday || startDate.DayOfWeek == DayOfWeek.Saturday || Convert.ToDateTime(startDate).TimeOfDay >= (TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) || holidayVMs.Any(x => x.HOL_LOC_CODE == locationCode && x.HOL_DATE.Date == startDate.Date))
            return time;

        if (Convert.ToDateTime(startDate).TimeOfDay <= TimeSpan.FromHours(startTime))
            startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddHours(startTime).AddMinutes(startMinutes);

        if (DateTime.Now.TimeOfDay >= TimeSpan.FromHours(endTime))
            time = ((TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) - startDate.TimeOfDay).TotalMinutes;
        else
            time = (DateTime.Now.TimeOfDay - startDate.TimeOfDay).TotalMinutes;

        return time;
    }

    public double CalculateTotalMinutes(DateTime startDate, int startTime, int endTime, int startMinutes, int endMinutes, List<HolidayVM> holidayVMs, string locationCode)
    {
        double time = 0;
        while (startDate.Date != DateTime.Now.Date)
        {
            if (startDate.DayOfWeek == DayOfWeek.Sunday || startDate.DayOfWeek == DayOfWeek.Saturday || Convert.ToDateTime(startDate).TimeOfDay >= (TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) || holidayVMs.Any(x => x.HOL_LOC_CODE == locationCode && x.HOL_DATE.Date == startDate.Date))
            {
                startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddDays(1).AddHours(startTime).AddMinutes(startMinutes);
                continue;
            }

            if (Convert.ToDateTime(startDate).TimeOfDay <= (TimeSpan.FromHours(startTime) + TimeSpan.FromMinutes(startMinutes)))
                startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddHours(startTime).AddMinutes(startMinutes);

            time += ((TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) - startDate.TimeOfDay).TotalMinutes;
            startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddDays(1).AddHours(startTime).AddMinutes(startMinutes);
        }

        time += CalculateTodayHours(startDate, startTime, endTime, startMinutes, endMinutes, holidayVMs, locationCode);
        return time;
    }


    public void BindLocations()
    {
        ddlLocation.Items.Clear();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@USER_ID", SqlDbType.NVarChar, 50);
        param[0].Value = Session["Uid"].ToString().Trim();
        ObjSubsonic.Binddropdown(ref ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param);
    }

    public void BindSubCategory()
    {
        // ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ref ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE");
    }

    public void BindChildCategory()
    {
        ddlChildCategory.Items.Clear();
        ObjSubsonic.Binddropdown(ref ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE");
    }
    public void BindMainCategory()
    {
        // ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ref ddlManinCatMR, "HDM_GET_ACTIVE_MAIN_CATEGORY", "MNC_NAME", "MNC_CODE");
    }

    protected void gvViewRequisitions_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvViewRequisitions.PageIndex = e.NewPageIndex;
        gvViewRequisitions.DataSource = Session["Reqs"];
        gvViewRequisitions.DataBind();
        CalculateTAT();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_SEARCH_REQUISITIONS_VIEW_ASSIGN_NEW");
        sp.Command.AddParameter("@REQID", txtReqId.Text, DbType.String);
        sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, DbType.String);
        sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, DbType.String);
        sp.Command.AddParameter("@SUBCATEGORY", ddlSubCatMR.SelectedValue, DbType.String);
        sp.Command.AddParameter("@MAINCATEGORY", ddlManinCatMR.SelectedValue, DbType.String);
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String);
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String);
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedValue, DbType.String);
        sp.Command.AddParameter("@AURID", Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.String);
        var dataSet = sp.GetDataSet();
        gvViewRequisitions.DataSource = dataSet.Tables[0];
        gvViewRequisitions.DataBind();
        Session["Reqs"] = dataSet.Tables[0];
        CalculateTAT();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Label lblreqid;
        DropDownList ddlAssign;
        string reqids = string.Empty;
        Int32 Out;
        int grdCount = gvViewRequisitions.Rows.Count;
        int chkCnt = 0;
        foreach (GridViewRow row in gvViewRequisitions.Rows)
        {
            DropDownList ddlAsn;
            ddlAsn = (DropDownList)row.FindControl("ddlAssignTo");
            if (ddlAsn.SelectedValue == "--Select--")
                chkCnt = chkCnt + 1;
        }

        if (grdCount == chkCnt)
        {
            lblMessage.Text = "Please Re-Assign for Atleast One HD Request";
            return;
        }

        try
        {
            DataSet ds = new DataSet();
            foreach (GridViewRow row in gvViewRequisitions.Rows)
            {
                ddlAssign = (DropDownList)row.FindControl("ddlAssignTo");
                if (ddlAssign.SelectedValue != "--Select--")
                {
                    lblreqid = (Label)row.FindControl("lblreqid");
                    TextBox rems = (TextBox)row.FindControl("txtUpdateRemarks");
                    SqlParameter[] param = new SqlParameter[4];
                    param[0] = new SqlParameter("@REQ_ID", lblreqid.Text);
                    param[1] = new SqlParameter("@ASSIGN_TO", ddlAssign.SelectedValue);
                    param[2] = new SqlParameter("@UPDATE_REMARKS", rems.Text);
                    param[3] = new SqlParameter("@USR_ID", Session["UID"].ToString());
                    Out = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_ASSIGN_REQUEST", param);
                }
            }
            BindGrid();
            // txtUpdateRemarks.Text = ""
            lblMessage.Visible = true;
            lblMessage.Text = "Requests Assigned successfully";
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void ddlSubCatMR_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlChildCategory.Items.Clear();
        if (ddlSubCatMR.SelectedIndex > 0)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SUBC_CODE", SqlDbType.VarChar, 50);
            param[0].Value = ddlSubCatMR.SelectedValue;
            ObjSubsonic.Binddropdown(ref ddlChildCategory, "HDM_GET_CHILDBY_SUBCATG", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param);
        }
        else
            ObjSubsonic.Binddropdown(ref ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE");
    }
    protected void ddlManinCatMR_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSubCatMR.Items.Clear();
        if (ddlManinCatMR.SelectedIndex > 0)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@MNC_CODE", SqlDbType.VarChar, 50);
            param[0].Value = ddlManinCatMR.SelectedValue;
            ObjSubsonic.Binddropdown(ref ddlSubCatMR, "HDM_GET_ALLSUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param);
        }
        else
            ObjSubsonic.Binddropdown(ref ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE");
    }
}
