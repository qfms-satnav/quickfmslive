﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HelpdeskManagement_Views_WorkorderReport : System.Web.UI.Page
{
    public class Assetdata
    {
        //public List<Workorderdeatils> workorder { get; set; }
        public string Remarks { get; set; }
        public string termsandcdtns { get; set; }
        public string workorderid { get; set; }
        public int id { get; set; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Assetdata ad = new Assetdata();
            Assetdata Workrequest = new Assetdata();
            Workrequest = JsonConvert.DeserializeObject<Assetdata>(Request.QueryString["rid"]);
            ad.id = Workrequest.id;
            byte[] bytes;
            

            if (ad.id == 1)
            {
                LoadReport();
                ReportViewer1.Visible = true;
                ReportViewer2.Visible = false;
                bytes = ReportViewer1.LocalReport.Render("PDF");
            }
            else
            {
                MaintLoadReport();
                ReportViewer2.Visible = true;
                ReportViewer1.Visible = false;
                bytes = ReportViewer2.LocalReport.Render("PDF");
            }
            string path = Server.MapPath("~/Print_Files");
            Random rnd = new Random();
            int month = rnd.Next(1, 13); // creates a number between 1 and 12
            int dice = rnd.Next(1, 7); // creates a number between 1 and 6
            int card = rnd.Next(9); // creates a number between 0 and 51
            string file_name = "Installments" + "List" + month + dice + card + ".pdf"; //save the file in unique name            

            FileStream file = new FileStream(path + "/" + file_name, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            file.Write(bytes, 0, bytes.Length);
            file.Dispose();
            hyprlink.NavigateUrl = "~/Print.aspx?File=" + file_name;
        }
    }
    public void LoadReport()

    {
        DataSet ds = new DataSet();
        Assetdata ad = new Assetdata();
        List<Workorderdeatils> RILst = new List<Workorderdeatils>();
        Workorderdeatils RI = new Workorderdeatils();
        Assetdata Workrequest = new Assetdata();
        Workrequest = JsonConvert.DeserializeObject<Assetdata>(Request.QueryString["rid"]);
        ad.workorderid = Workrequest.workorderid.ToString();
        ad.Remarks = Workrequest.Remarks.ToString();
        ad.termsandcdtns = Workrequest.termsandcdtns.ToString();

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_WORKORDER_DETAILS");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@Reqid", ad.workorderid, DbType.String);
        ds = sp.GetDataSet();
        //DataTable dt2 = UtilityService.ConvertToDataTable(ad.workorder);
        //ds.Tables.Add(ds.Tables[0]);
        // ds.Tables.Add(dt2);
        string imagePath = "file:///" + Server.MapPath(BindLogo()); 
        ReportParameter rp1 = new ReportParameter("Parameter1", ad.workorderid);
        ReportParameter rp2 = new ReportParameter("Parameter2", ad.Remarks);
        ReportParameter rp3 = new ReportParameter("Parameter3", ad.termsandcdtns);
        ReportParameter rp4 = new ReportParameter("Imagepath", imagePath);
        //ReportParameter f = new ReportParameter();
        //f.Values.Add(ad.termsandcdtns);

        // DataTable dt1 = ad.Remarks.ToString();
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "Workorder";
        rds.Value = ds.Tables[0];

        ReportViewer1.Reset();

        ReportViewer1.LocalReport.DataSources.Add(rds);
        ReportViewer1.LocalReport.EnableExternalImages = true;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/WorkOrderReport.rdlc");
        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4 });
        ReportViewer1.SizeToReportContent = true;

        ReportViewer1.Visible = true;
        ReportViewer1.LocalReport.DisplayName = "Work Order";
        ReportViewer1.LocalReport.Refresh();
    }
    public string BindLogo()
    {
        // Dim imagePath As String
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "Update_Get_LogoImage");
        sp3.Command.AddParameter("@type", "2", DbType.String);
        sp3.Command.AddParameter("@AUR_ID", Session["uid"], DbType.String);
        sp3.Command.AddParameter("@Tenant", Session["TENANT"], DbType.String);
        DataSet ds3 = sp3.GetDataSet();
        if (ds3.Tables[0].Rows.Count > 0)
            return ds3.Tables[0].Rows[0]["ImagePath"].ToString();
        else
            return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png";
    }
    public void MaintLoadReport()

    {
        DataSet ds = new DataSet();
        Assetdata ad = new Assetdata();
        List<Workorderdeatils> RILst = new List<Workorderdeatils>();
        Workorderdeatils RI = new Workorderdeatils();
        Assetdata Workrequest = new Assetdata();
        Workrequest = JsonConvert.DeserializeObject<Assetdata>(Request.QueryString["rid"]);
        ad.workorderid = Workrequest.workorderid.ToString();
        ad.Remarks = Workrequest.Remarks.ToString();
        ad.termsandcdtns = Workrequest.termsandcdtns.ToString();

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_MAINTWORKORDER_DETAILS");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@Reqid", ad.workorderid, DbType.String);
        ds = sp.GetDataSet();
        //DataTable dt2 = UtilityService.ConvertToDataTable(ad.workorder);
        //ds.Tables.Add(ds.Tables[0]);
        // ds.Tables.Add(dt2);
        string imagePath = "file:///" + Server.MapPath(BindLogo());
        ReportParameter rp1 = new ReportParameter("Parameter1", ad.workorderid);
        ReportParameter rp2 = new ReportParameter("Parameter2", ad.Remarks);
        ReportParameter rp3 = new ReportParameter("Parameter3", ad.termsandcdtns);
        ReportParameter rp4 = new ReportParameter("Imagepath", imagePath);
        //ReportParameter f = new ReportParameter();
        //f.Values.Add(ad.termsandcdtns);

        // DataTable dt1 = ad.Remarks.ToString();
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "MaintWorkOrder";
        rds.Value = ds.Tables[0];

        ReportViewer2.Reset();

        ReportViewer2.LocalReport.DataSources.Add(rds);
        ReportViewer2.LocalReport.EnableExternalImages = true;
        ReportViewer2.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/MaintWorkOrder.rdlc");
        ReportViewer2.LocalReport.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4 });
        ReportViewer2.SizeToReportContent = true;

        ReportViewer2.Visible = true;
        ReportViewer2.LocalReport.DisplayName = "Work Order";
        ReportViewer2.LocalReport.Refresh();
    }
}