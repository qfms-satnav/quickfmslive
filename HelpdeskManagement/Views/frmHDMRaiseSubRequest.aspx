﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script src="../../Scripts/aggrid/ag-grid-2.js" defer></script>
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

        var columnDefs = [
            { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 250 },
            { headerName: "Image", field: "Name", cellClass: "grid-align", width: 250 },
            { headerName: "Remove", template: '<a href="#" onclick="Remove(this.parentNode.parentNode)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 80 }
        ];

        var rowData = [];
        var data;
        var gridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            onGridReady: function () {
                gridOptions.api.sizeColumnsToFit();
            }
        };
        var selectedfiles;
        function resetselectedfiles() {
            console.log('btnsbt');
            console.log(rowData);
            for (i = 0; i < rowData.length; i++) {
                selectedfiles = document.createElement("input");
                selectedfiles.setAttribute("type", "hidden");
                selectedfiles.setAttribute("name", "selectedfiles");
                selectedfiles.setAttribute("value", rowData[i].Name);
                document.getElementById("form1").appendChild(selectedfiles);
            }
        }
        var length;
        var PostedFiles;
        var fd = [];
        function showselectedfiles(fu) {
            data = new FormData($('form')[0]);
            var gridLength = gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    rowData.splice(0, 1);
                    gridOptions.api.setRowData(rowData);
                }
            }

            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
                //rowData.push({ FileName: fu.files[i].name, size: fu.files[i].size, ContentType: fu.files[i].type,ContentLength:fu.files[i].length});
                //file = $('#UPLFILE')[i].files[i];
                //fileName = file.name;
                fd.push({ Name: fu.files[i].name, size: fu.files[i].size, type: fu.files[i].type });
                length = fu.files.length;
                console.log(length);
            }
            console.log(rowData);
            gridOptions.api.setRowData(rowData);

        }

        function Remove(node) {
            $("#fu1").val("");
            var ndx = parseInt(node.getAttribute("row"));
            rowData.splice(ndx, 1);
            gridOptions.api.setRowData(rowData);
        }

        document.addEventListener("DOMContentLoaded", function () {
            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
        });

        function GetValueOnKeyPress() {
            var edValue = document.getElementById("txtEmpId");
            var s = edValue.value;
            var lblValue = document.getElementById("lblValue");
            lblValue.innerText = "The text box contains: " + s;
        }

        function GetUpload() {
            var data = new FormData($('form')[0]);
            console.log(data);
            $.ajax({
                type: "POST",
                url: 'https://live.quickfms.com/api/HDMRaiseSubRequest/UploadFiles',    // CALL WEB API TO SAVE THE FILES.
                //enctype: 'multipart/form-data',
                contentType: false,
                processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                cache: false,
                data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                success: function (data, textStatus, xhr) {
                    $('p').text(data);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus + ': ' + errorThrown);
                }
            });
        };

    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMRaiseSubRequestController" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Raise Sub Request</h3>
            </div>

            <div class="card">
                <form id="form1" name="HDMRaiseSubReq" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="HDMRaiseSubRequest.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" id="DDL" data-on-item-click="getClientPrefixId()"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMRaiseSubRequest.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span id="Error" class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.LCM_NAME.$invalid" style="color: red;">Please Select Location</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.MNC_NAME.$invalid}">
                                <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Main_Category" data-output-model="HDMRaiseSubRequest.Main_Category" data-button-label="icon MNC_NAME"
                                    data-item-label="icon MNC_NAME maker" data-on-item-click="getsubbymain()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMRaiseSubRequest.Main_Category[0]" name="MNC_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.MNC_NAME.$invalid" style="color: red;">Please Select Main Category</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.SUBC_NAME.$invalid}">
                                <label class="control-label">Subcategory  <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Sub_Category" data-output-model="HDMRaiseSubRequest.Sub_Category" data-button-label="icon SUBC_NAME"
                                    data-item-label="icon SUBC_NAME maker" data-on-item-click="getchildbysub()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMRaiseSubRequest.Sub_Category[0]" name="SUBC_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.SUBC_NAME.$invalid" style="color: red;">Please Select Subcategory </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.CHC_TYPE_NAME.$invalid}">
                                <label class="control-label">Child Category <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="child_Category" data-output-model="HDMRaiseSubRequest.child_Category" data-button-label="icon CHC_TYPE_NAME"
                                    data-item-label="icon CHC_TYPE_NAME maker" data-on-item-click="childchange()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMRaiseSubRequest.child_Category[0]" name="CHC_TYPE_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.CHC_TYPE_NAME.$invalid" style="color: red;">Please Select Child Category</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Asset Location</label>
                                            <div isteven-multi-select data-input-model="AssetLocation" data-output-model="HDMRaiseSubRequest.AssetLocation" data-button-label="icon HAL_LOC_NAME"
                                                data-item-label="icon HAL_LOC_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMRaiseSubRequest.AssetLocation[0]" name="HAL_LOC_NAME" style="display: none" required="" />
                                        </div>
                                    </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Mobile/Extension Number</label>
                                <div class="control-label" id='Mobile'>
                                    <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.Mobile" id="txtMobile" name="Mobile" maxlength="12" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.CustID.$invalid}">
                                <label for="txtcode">Customer ID <span style="color: red;">*</span></label>
                                <div class="control-label" id='CustID'>
                                    <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.CustID" id="txtCustID" name="CustID" required="" />
                                    <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.CustID.$invalid" style="color: red;">Please Enter Customer ID</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">

                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.CustName.$invalid}">
                                <label for="txtcode">Customer Name <span style="color: red;">*</span></label>
                                <div class="control-label" id='CustName'>
                                    <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.CustName" id="txtCustName" name="CustName" required="" />
                                    <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.CustName.$invalid" style="color: red;">Please Enter Customer Name</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.BilledBy.$invalid}">
                                <label for="txtcode">Billed By <span style="color: red;">*</span></label>
                                <div class="control-label" id='BilledBy'>
                                    <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.BilledBy" id="txtBilledBy" name="BilledBy" required="" />
                                    <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.BilledBy.$invalid" style="color: red;">Please Enter Billed By</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <%--  <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group"  data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.BillNo.$invalid}">
                                            <label for="txtcode">Bill No<span style="color: red;">*</span></label>
                                            <div class="control-label" id='BillNumber'>
                                                <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.BillNo" id="BillNo" name="BillNo" required="" />
                                                <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.BillNo.$invalid" style="color: red;">Please Enter Bill Number</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group"  data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.Billdate.$invalid}">
                                            <label for="txtcode">Bill Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='Billdat'>
                                                <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.Billdate" id="Billdate" name="Billdate" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('Billdat')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.Billdate.$invalid" style="color: red;">Please Select Bill Date</span>
                                        </div>
                                    </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.InvoiceNo.$invalid}">
                                <label for="txtcode">Invoice No <span style="color: red;">*</span></label>
                                <div class="control-label" id='InvoiceNo'>
                                    <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.InvoiceNo" id="txtInvoiceNo" name="InvoiceNo" required="" />
                                    <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.InvoiceNo.$invalid" style="color: red;">Please Enter InvoiceNo</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.InvoiceDate.$invalid}">
                                <label for="txtcode">Invoice Date <span style="color: red;">*</span></label>
                                <div class="input-group date" id='Invoicedat'>
                                    <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.InvoiceDate" id="InvoiceDate" name="InvoiceDate" required="" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('Invoicedat')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.InvoiceDate.$invalid" style="color: red;">Please Select Invoice Date</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <label for="txtcode" class="custom-file">Upload Images/Document<a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                            <input multiple type="file" name="UPLFILE" class="form-control" data-ng-model="HDMRaiseSubRequest.UPLFILE[0]" id="UPLFILE" accept=".xls,.xlsx" class="custom-file-input" onchange="showselectedfiles(this)">
                            <span class="custom-file-control"></span>
                        </div>
                        <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.SER_APP_STA_NAME.$invalid}">
                                            <label class="control-label">Approvals <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Approvals" data-output-model="HDMRaiseSubRequest.Approvals" data-button-label="icon SER_APP_STA_NAME"
                                                data-item-label="icon SER_APP_STA_NAME maker"  data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMRaiseSubRequest.Approvals[0]" name="SER_APP_STA_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.SER_APP_STA_NAME.$invalid" style="color: red;">Please Select Approvals</span>
                                        </div>
                                    </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.Amount.$invalid}">
                                <label for="txtcode">Amount <span style="color: red;">*</span></label>
                                <div class="control-label" id='Amount'>
                                    <input type="text" class="form-control" data-ng-model="HDMRaiseSubRequest.Amount" id="txtAmount" name="Amount" required="" />
                                    <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.InvoiceNo.$invalid" style="color: red;">Please Enter Amount</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMRaiseSubReq.$submitted && HDMRaiseSubReq.txtProbDesc.$invalid}">
                                <label for="txtcode">Description/ Service and location <span style="color: red;">*</span></label>
                                <div class="control-label" id='Description'>
                                    <textarea name="txtProbDesc" class="form-control" data-ng-model="HDMRaiseSubRequest.ProbDesc" cols="40" rows="5" required=""></textarea>
                                    <span class="error" data-ng-show="HDMRaiseSubReq.$submitted && HDMRaiseSubReq.txtProbDesc.$invalid" style="color: red;">Please Enter Description</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">

                        <div class="col-md-6 col-sm-12 col-xs-12 text-right" style="margin-left: 500px">
                            <br />
                            <div class="form-group">
                                <input type="submit" value="Submit Request" data-ng-click="SaveEmpDetails()" class="btn btn-primary custom-button-color" />
                                <input type="button" value="Clear" data-ng-click="Clear()" class="btn btn-primary custom-button-color" /><%--onclick="GetUpload()" SaveEmpDetails--%>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-6">
                            <a id="lnkShowEscaltion" href="#" onclick="showPopWin()" runat="server" hidden="">Click here to view SLA & Escalation</a>
                        </div>
                        <div class="col-md-12">
                            <div id="myGrid" data-ng-model="HDMRaiseSubRequest.myGrid" style="height: 250px; width: 400px" class="ag-blue"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../HDM/HDM_Webfiles/Reports/Js/HDMRaiseSubRequest.js" defer></script>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>

    <script defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
</body>
</html>


