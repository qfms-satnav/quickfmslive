﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HelpdeskManagement_Views_FeedBackResponseMail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string RequestId;
        string FeedBackCode, Tenant = "";

        RequestId = Request.QueryString["RequestId"];
        FeedBackCode = Request.QueryString["FeedBackCode"];
        Tenant = Request.QueryString["Tenant"];
        if (Tenant == "AdityaBirla")
        {
            aditya.Visible = true;
        }
        else
        {
            other.Visible = true;
        }
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("[" + Tenant + "].[dbo]" + "." + "HDM_SUBMIT_FDBK_BY_MAIL");
        sp.Command.AddParameter("@RequestId", RequestId, DbType.String);
        sp.Command.AddParameter("@FDBKCODE", FeedBackCode, DbType.String);

        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        if (ds.Tables[0].Rows[0]["Result"].ToString() == "1")
        {
            Success.Visible = true;
        }
        else if (ds.Tables[0].Rows[0]["Result"].ToString() == "0")
        {
            alreadySubmitted.Visible = true;
        }
        else
        {
            Fail.Visible = true;
        }
    }
}