﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class HelpdeskManagement_Views_frmRMViewRequisitions : System.Web.UI.Page
{
    private clsSubSonicCommonFunctions ObjSubsonic = new clsSubSonicCommonFunctions();

    public static bool CostStatus;
    private int totalRows = 0;
    private List<HolidayVM> HolidayList = new List<HolidayVM>();
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
        if (!IsPostBack)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string host = HttpContext.Current.Request.Url.Host;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            {
                Value = Session["UID"]
            };
            param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            {
                Value = path
            };
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
            {
                if (!sdr.HasRows)
                {
                    Response.Redirect(Application["FMGLogout"].ToString());
                }
            }
            repeaterPaging.Visible = false;
            // Check Labour/Spare Costs visible status
            SubSonic.StoredProcedure spCS = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_CHECK_SYS_PREFERENCES");
            spCS.Command.AddParameter("@TYPE ", 4, DbType.Int32);
            var ChkCostSts = spCS.ExecuteScalar();
            CostStatus = Convert.ToInt32(ChkCostSts.ToString()) == 1 ? true : false;
            txtfromDt.Text = DateTime.Now.AddDays(-29).ToString("MM/dd/yyyy");
            txtToDt.Text = DateTime.Now.ToString("MM/dd/yyyy");
            txtUAfromDt.Text = DateTime.Now.AddDays(-29).ToString("MM/dd/yyyy");
            txtUAtoDt.Text = DateTime.Now.ToString("MM/dd/yyyy");
            Session["StartMinutes"] = 0;
            Session["EndMinutes"] = 0;
            BindGrid();
            //BindUnassignedGrid();
            BindLocations();
            BindChildCategory();
            BindSubCategory();
            BindMainCategory();
            FillStatus();
            if (gvViewRequisitions.Rows.Count == 0)
            {
                //showDiv.Visible = false;
                btnUpdate1.Visible = false;
            }
            else
            {
                //showDiv.Visible = true;
                btnUpdate1.Visible = true;
            }
            //if (gvUnassigned.Rows.Count == 0)
            //{
            //    //showDiv2.Visible = false;
            //    btnUpdate2.Visible = false;
            //}
            //else
            //{
            //    //showDiv2.Visible = true;
            //    btnUpdate2.Visible = true;
            //}
            if (Request.QueryString["Updated"] != null && Convert.ToInt32(Request.QueryString["Updated"].ToString()) == 1)
                lblMessage.Text = "Request Updated Successfully";
            else
                lblMessage.Text = "";
        }
    }
    protected void btnShowUnassignedRequests_Click(object sender, EventArgs e)
    {
        BindUnassignedGrid();
        btnUpdate2.Visible = true;
        divhd.Visible = true;
        btnShowUnassignedRequests.Visible = false;
    }
    private void GetTotalRecordsCount()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_GET_PENDING_REQUESTS_COUNT_FOR_INCHARGE");
        sp.Command.AddParameter("@AurId", Session["UID"], DbType.String);
        DataSet dsCount = sp.GetDataSet();
        if (dsCount.Tables.Count > 0)
            totalRows = Convert.ToInt32(dsCount.Tables[0].Rows[0]["COUNT"].ToString());
    }
    protected void linkButton_Click(object sender, EventArgs e)
    {
        int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
        LinkButton lb = sender as LinkButton;
        BindGrid(pageIndex, 10);
        GetTotalRecordsCount();
        DatabindRepeater(pageIndex, lb.Text);
    }
    public class RepeaterListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string IsActive { get; set; }
        public RepeaterListItem(string name, string input, bool status)
        {
            Text = name;
            Value = input;
            if (!status)
                IsActive = "active";
        }
    }
    private void DatabindRepeater(int currentPage, string pageName = "")
    {
        double dblPageCount = Convert.ToDouble(Convert.ToDouble(totalRows) / (double)Convert.ToDecimal(10));
        int pageCount = System.Convert.ToInt32(Math.Ceiling(dblPageCount));
        List<RepeaterListItem> pages = new List<RepeaterListItem>();
        int i = 0;
        if (pageCount > 0)
        {
            if (pageCount - currentPage <= 10 && pageName != "<<<")
            {
                int remainder = currentPage % 10;
                int startNo = (currentPage - remainder) + 1;
                if (remainder == 0)
                    startNo = currentPage - 10 + 1;
                pages.Add(new RepeaterListItem("<<<", (startNo - 1).ToString(), true));
                for (i = startNo; i <= pageCount - 1; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
            }
            else if (pageName == ">>>")
            {
                pages.Add(new RepeaterListItem("<<<", (currentPage - 1).ToString(), true));
                for (i = currentPage; i <= currentPage + 9; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
            else if (pageName == "<<<")
            {
                int remainder = currentPage % 10;
                int startNo = (currentPage - remainder) + 1;
                if (remainder == 0)
                    startNo = currentPage - 10 + 1;
                if (startNo > 10)
                    pages.Add(new RepeaterListItem("<<<", (startNo - 1).ToString(), true));
                for (i = startNo; i <= startNo + 10 - 1; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
            else if (currentPage == 1)
            {
                for (i = 1; i <= 10; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
            else
            {
                int remainder = currentPage % 10;
                int startNo = (currentPage - remainder) + 1;
                if (remainder == 0)
                    startNo = currentPage - 10 + 1;
                if (startNo > 10)
                    pages.Add(new RepeaterListItem("<<<", (startNo - 1).ToString(), true));
                for (i = startNo; i <= startNo + 10 - 1; i++)
                    pages.Add(new RepeaterListItem(i.ToString(), i.ToString(), i != currentPage));
                pages.Add(new RepeaterListItem(">>>", (i).ToString(), true));
            }
        }
        repeaterPaging.DataSource = pages;
        repeaterPaging.DataBind();
    }

    private void FillStatus()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_HDMSTATUS_FILTERS");
        sp.Command.AddParameter("@TYPE", 2, DbType.String);
        ddlStatus.DataSource = sp.GetDataSet();
        ddlStatus.DataTextField = "STA_TITLE";
        ddlStatus.DataValueField = "STA_ID";
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, "--Select--");
    }

    private void BindGrid(int pageNumber = 0, int pageSize = 10)
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_GET_PENDING_REQUESTS_INCHARGE_NEW");
        sp.Command.AddParameter("@AurId", Session["UID"], DbType.String);
        sp.Command.AddParameter("@CompanyId", Session["COMPANYID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String);
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String);
        ds = sp.GetDataSet();
        Session["AssignedGrid"] = ds.Tables[0];
        gvViewRequisitions.DataSource = ds.Tables[0];
        gvViewRequisitions.DataBind();
        if (ds.Tables.Count > 1 && ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 0)
        {
            var hoildayJson = JsonConvert.SerializeObject(ds.Tables[1]);
            HolidayList = JsonConvert.DeserializeObject<List<HolidayVM>>(hoildayJson);
        }
        if (ds.Tables.Count > 1)
        {
            var startValue = ds.Tables[2].Rows[0]["SYSP_VAL1"].ToString().Split(':');
            var endValue = ds.Tables[2].Rows[0]["SYSP_VAL2"].ToString().Split(':');
            Session["StartTime"] = Convert.ToInt32(startValue[0]);
            Session["EndTime"] = Convert.ToInt32(endValue[0]);
            if (startValue.Length > 1)
            {
                Session["StartMinutes"] = Convert.ToInt32(startValue[1]);
            }
            if (endValue.Length > 1)
            {
                Session["EndMinutes"] = Convert.ToInt32(endValue[1]);
            }
        }

        CalculateTAT();
    }

    public void CalculateTAT()
    {
        SubSonic.StoredProcedure spSts = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_USER_STATUS_BYROLE");
        spSts.Command.AddParameter("@AUR_ID", Session["UID"], System.Data.DbType.String);
        var ds1 = spSts.GetDataSet();
        for (int i = 0; i <= gvViewRequisitions.Rows.Count - 1; i++)
        {
            DropDownList ddlstat = (DropDownList)gvViewRequisitions.Rows[i].FindControl("ddlModifyStatus");
            ddlstat.DataSource = ds1;
            ddlstat.DataTextField = "STA_TITLE";
            ddlstat.DataValueField = "STA_ID";
            ddlstat.DataBind();
            ddlstat.ClearSelection();
            for (int j = 0; j < ddlstat.Items.Count - 1; j++)
            {
                string color = ds1.Tables[0].Rows[j]["STA_COLOR"].ToString();
                ddlstat.Items[j + 1].Attributes["style"] = " ";
            }

            gvViewRequisitions.HeaderRow.Cells[18].Visible = CostStatus;
            gvViewRequisitions.HeaderRow.Cells[19].Visible = CostStatus;
            gvViewRequisitions.Rows[i].Cells[18].Visible = CostStatus;
            gvViewRequisitions.Rows[i].Cells[19].Visible = CostStatus;

            Label callogDate = (Label)gvViewRequisitions.Rows[i].FindControl("lblCallLogDt");
            DateTime createdDt = Convert.ToDateTime(callogDate.Text);
            double totalMinutes = 0;
            Label location = (Label)gvViewRequisitions.Rows[i].FindControl("lblLocation");
            if (createdDt.Date == DateTime.Today)
            {
                totalMinutes = CalculateTodayHours(createdDt,
                    Convert.ToInt32(Session["StartTime"].ToString() ?? "9"),
                    Convert.ToInt32(Session["EndTime"].ToString() ?? "18"),
                    Convert.ToInt32(Session["StartMinutes"].ToString() ?? "0"),
                    Convert.ToInt32(Session["EndMinutes"].ToString() ?? "0"), HolidayList, location.Text);
            }
            else
            {
                totalMinutes = CalculateTotalMinutes(createdDt,
                    Convert.ToInt32(Session["StartTime"].ToString() ?? "9"),
                    Convert.ToInt32(Session["EndTime"].ToString() ?? "18"),
                    Convert.ToInt32(Session["StartMinutes"].ToString() ?? "0"),
                    Convert.ToInt32(Session["EndMinutes"].ToString() ?? "0"),
                    HolidayList, location.Text);
            }
            Label timeTaken = (Label)gvViewRequisitions.Rows[i].FindControl("lblTAT");
            timeTaken.Text = ConvertSectoDay(Convert.ToInt32(totalMinutes));
        }
    }

    private string ConvertSectoDay(int n)
    {
        int days = (int)Math.Floor(n / (double)540);
        int hours = (int)Math.Floor((n % 540) / (double)60);
        int mins = n % 60;
        return (days > 0 ? (days + " " + "Days ") : "") + hours + " " + "Hr " + mins + " " + "Min ";
    }


    private double CalculateTodayHours(DateTime startDate, int startTime, int endTime, int startMinutes, int endMinutes, List<HolidayVM> holidayVMs, string locationCode)
    {
        double time = 0;

        if (startDate.DayOfWeek == DayOfWeek.Sunday || startDate.DayOfWeek == DayOfWeek.Saturday || Convert.ToDateTime(startDate).TimeOfDay >= (TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) || holidayVMs.Any(x => x.HOL_LOC_CODE == locationCode && x.HOL_DATE.Date == startDate.Date))
            return time;

        if (Convert.ToDateTime(startDate).TimeOfDay <= TimeSpan.FromHours(startTime))
            startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddHours(startTime).AddMinutes(startMinutes);

        if (DateTime.Now.TimeOfDay >= TimeSpan.FromHours(endTime))
            time = ((TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) - startDate.TimeOfDay).TotalMinutes;
        else
            time = (DateTime.Now.TimeOfDay - startDate.TimeOfDay).TotalMinutes;

        return time;
    }

    public double CalculateTotalMinutes(DateTime startDate, int startTime, int endTime, int startMinutes, int endMinutes, List<HolidayVM> holidayVMs, string locationCode)
    {
        double time = 0;

        while (startDate.Date != DateTime.Now.Date)
        {
            if (startDate.DayOfWeek == DayOfWeek.Sunday || startDate.DayOfWeek == DayOfWeek.Saturday || Convert.ToDateTime(startDate).TimeOfDay >= (TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) || holidayVMs.Any(x => x.HOL_LOC_CODE == locationCode && x.HOL_DATE.Date == startDate.Date))
            {
                startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddDays(1).AddHours(startTime).AddMinutes(startMinutes);
                continue;
            }

            if (Convert.ToDateTime(startDate).TimeOfDay <= (TimeSpan.FromHours(startTime) + TimeSpan.FromMinutes(startMinutes)))
                startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddHours(startTime).AddMinutes(startMinutes);

            time += ((TimeSpan.FromHours(endTime) + TimeSpan.FromMinutes(endMinutes)) - startDate.TimeOfDay).TotalMinutes;
            startDate = Convert.ToDateTime(startDate.ToString("MM-dd-yyyy")).AddDays(1).AddHours(startTime).AddMinutes(startMinutes);
        }

        time += CalculateTodayHours(startDate, startTime, endTime, startMinutes, endMinutes, holidayVMs, locationCode);
        return time;
    }

    private void BindUnassignedGrid()
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@AurId", SqlDbType.VarChar, 50);
        param[0].Value = Session["UID"];
        param[1] = new SqlParameter("@CompanyId", SqlDbType.VarChar, 200);
        param[1].Value = Session["COMPANYID"];
        param[2] = new SqlParameter("@FROMDATE", SqlDbType.VarChar, 50);
        param[2].Value = txtUAfromDt.Text;
        param[3] = new SqlParameter("@TODATE", SqlDbType.VarChar, 50);
        param[3].Value = txtUAtoDt.Text;
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_GET_PENDING_UNASSIGNED_REQUESTS_INCHARGE_NEW", param);
        gvUnassigned.DataSource = ds;
        gvUnassigned.DataBind();
        Session["UnAssignedGrid"] = ds;
        CalculateUnAssignedGridTAT();
    }

    private void CalculateUnAssignedGridTAT()
    {
        DataSet ds1 = new DataSet();
        SubSonic.StoredProcedure spSts = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_USER_STATUS_BYROLE");
        spSts.Command.AddParameter("@AUR_ID", Session["UID"], DbType.String);
        ds1 = spSts.GetDataSet();
        for (int i = 0; i <= gvUnassigned.Rows.Count - 1; i++)
        {
            DropDownList ddlAssignStatus = (DropDownList)gvUnassigned.Rows[i].FindControl("ddlAssignStatus");
            ddlAssignStatus.DataSource = ds1;
            ddlAssignStatus.DataTextField = "STA_TITLE";
            ddlAssignStatus.DataValueField = "STA_ID";
            ddlAssignStatus.DataBind();
            ddlAssignStatus.ClearSelection();

            gvUnassigned.HeaderRow.Cells[17].Visible = CostStatus;
            gvUnassigned.HeaderRow.Cells[18].Visible = CostStatus;
            gvUnassigned.Rows[i].Cells[17].Visible = CostStatus;
            gvUnassigned.Rows[i].Cells[18].Visible = CostStatus;

            Label callogDate = (Label)gvUnassigned.Rows[i].FindControl("lblCallLogDt");
            DateTime createdDt = Convert.ToDateTime(callogDate.Text);
            double totalMinutes = 0;
            Label location = (Label)gvUnassigned.Rows[i].FindControl("lblLocation");
            if (createdDt.Date == DateTime.Today)
            {
                totalMinutes = CalculateTodayHours(createdDt, Convert.ToInt32(Session["StartTime"].ToString() ?? "9"),
                    Convert.ToInt32(Session["EndTime"].ToString() ?? "18"),
                    Convert.ToInt32(Session["StartMinutes"].ToString() ?? "0"),
                    Convert.ToInt32(Session["EndMinutes"].ToString() ?? "0"),
                    HolidayList, location.Text);
            }
            else
            {
                totalMinutes = CalculateTotalMinutes(createdDt, Convert.ToInt32(Session["StartTime"].ToString() ?? "9"),
                    Convert.ToInt32(Session["EndTime"].ToString() ?? "18"),
                    Convert.ToInt32(Session["StartMinutes"].ToString() ?? "0"),
                    Convert.ToInt32(Session["EndMinutes"].ToString() ?? "0"),
                    HolidayList, location.Text);
            }
            Label timeTaken = (Label)gvUnassigned.Rows[i].FindControl("lblTAT");
            timeTaken.Text = ConvertSectoDay(Convert.ToInt32(totalMinutes));
        }
    }
    protected void gvViewRequisitions_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvViewRequisitions.PageIndex = e.NewPageIndex;
        gvViewRequisitions.DataSource = Session["AssignedGrid"];
        gvViewRequisitions.DataBind();
        CalculateTAT();
    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {
        DropDownList ddlStatus = new DropDownList();
        bool invalidStatusSelected = false;
        try
        {
            DataSet ds = new DataSet();
            foreach (GridViewRow row in gvViewRequisitions.Rows)
            {
                CheckBox chkselect = (CheckBox)row.FindControl("chkselect");
                Label lblreqid = (Label)row.FindControl("lblreqid");
                TextBox txtUpdateRemarks = (TextBox)row.FindControl("txtUpdateRemarks");
                ddlStatus = (DropDownList)row.FindControl("ddlModifyStatus");
                TextBox txtAC = (TextBox)row.FindControl("txtADC");
                TextBox txtLBR = (TextBox)row.FindControl("txtLBC");
                TextBox txtSPR = (TextBox)row.FindControl("txtSPC");
                var txtClaim = row.Cells[9].Text.Trim();

                string claim;
                if (txtClaim == null)
                    claim = "";
                else
                    claim = txtClaim;
                if (chkselect.Checked == true && ddlStatus.SelectedValue != "0")
                {
                    SqlParameter[] param = new SqlParameter[8];
                    param[0] = new SqlParameter("@REQ_ID", lblreqid.Text);
                    param[1] = new SqlParameter("@STA_ID", ddlStatus.SelectedValue);
                    param[2] = new SqlParameter("@UPDATE_REMARKS", txtUpdateRemarks.Text);
                    param[3] = new SqlParameter("@USR_ID", Session["UID"].ToString());
                    param[4] = new SqlParameter("@ADDN_COST", txtAC.Text);
                    param[5] = new SqlParameter("@LBR_COST", txtLBR.Text);
                    param[6] = new SqlParameter("@SPR_COST", txtSPR.Text);
                    param[7] = new SqlParameter("@CLA_AMT", claim);
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQ_UPDATE", param);
                }
                invalidStatusSelected = true;
            }
            if (gvViewRequisitions.Rows != null && gvViewRequisitions.Rows.Count > 0)
            {
                lblMessage.Text = "Request Updated Successfully";
                BindGrid();
                lblMessage.Visible = true;
            }
            if (invalidStatusSelected)
            {
                if (gvViewRequisitions.Rows != null && gvViewRequisitions.Rows.Count > 0)
                {
                    lblMessage.Text = "Request Updated Successfully";
                    BindGrid();
                    lblMessage.Visible = true;
                }
            }
            else
            {
                lblMessage.Text = "Please select status";
                lblMessage.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = "An error occurred while updating the request.";
            lblMessage.Visible = true;
        }
    }

    protected void btnUpdate2_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            foreach (GridViewRow row in gvUnassigned.Rows)
            {
                CheckBox chkSelect1 = (CheckBox)row.FindControl("chkSelect1");
                Label lblreqid = (Label)row.FindControl("lblreqid");
                TextBox txtUpdate2Remarks = (TextBox)row.FindControl("txtUpdate2Remarks");
                DropDownList ddlStatus2 = (DropDownList)row.FindControl("ddlAssignStatus");

                TextBox txtUpdateRemarks = (TextBox)row.FindControl("txtUpdateRemarks");
                TextBox txtAC = (TextBox)row.FindControl("txtADC");
                TextBox txtLBC = (TextBox)row.FindControl("txtLBC");
                TextBox txtSC = (TextBox)row.FindControl("txtSPC");

                if (chkSelect1.Checked == true)
                {
                    SqlParameter[] param = new SqlParameter[5];
                    param[0] = new SqlParameter("@REQ_ID", lblreqid.Text);
                    param[1] = new SqlParameter("@STA_ID", ddlStatus2.SelectedValue);
                    param[2] = new SqlParameter("@UPDATE_REMARKS", txtUpdate2Remarks.Text);
                    param[3] = new SqlParameter("@USR_ID", Session["UID"].ToString());
                    param[4] = new SqlParameter("@ADDN_COST", txtAC.Text);
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_UPDATE_UNASSIGNED_REQUESTS", param);
                }
            }
            if (gvUnassigned.Rows != null && gvUnassigned.Rows.Count > 0)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Request Updated Successfully";
                BindUnassignedGrid();
                BindGrid();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void BindLocations()
    {
        ddlLocation.Items.Clear();
        ddlLocaton1.Items.Clear();
        SubSonic.StoredProcedure spSts = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_GETACTIVELOCATION");
        spSts.Command.AddParameter("@USER_ID", Session["UID"], DbType.String);
        DataSet ds = spSts.GetDataSet();
        ddlLocation.DataSource = ds;
        ddlLocation.DataTextField = "LCM_NAME";
        ddlLocation.DataValueField = "LCM_CODE";
        ddlLocation.DataBind();
        ddlLocation.Items.Insert(0, "--Select--");
        ddlLocation.ClearSelection();

        ddlLocaton1.DataSource = ds;
        ddlLocaton1.DataTextField = "LCM_NAME";
        ddlLocaton1.DataValueField = "LCM_CODE";
        ddlLocaton1.DataBind();
        ddlLocaton1.Items.Insert(0, "--Select--");
        ddlLocaton1.ClearSelection();

        //ObjSubsonic.Binddropdown(ref ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param);
        //ObjSubsonic.Binddropdown(ref ddlLocaton1, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param);
    }

    public void BindChildCategory()
    {
        ddlChildCategory.Items.Clear();
        ddlChildCategroy1.Items.Clear();

        SubSonic.StoredProcedure spSts = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY");
        DataSet ds = spSts.GetDataSet();
        ddlChildCategory.DataSource = ds;
        ddlChildCategory.DataTextField = "CHC_TYPE_NAME";
        ddlChildCategory.DataValueField = "CHC_TYPE_CODE";
        ddlChildCategory.DataBind();
        ddlChildCategory.Items.Insert(0, "--Select--");
        ddlChildCategory.ClearSelection();

        ddlChildCategroy1.DataSource = ds;
        ddlChildCategroy1.DataTextField = "CHC_TYPE_NAME";
        ddlChildCategroy1.DataValueField = "CHC_TYPE_CODE";
        ddlChildCategroy1.DataBind();
        ddlChildCategroy1.Items.Insert(0, "--Select--");
        ddlChildCategroy1.ClearSelection();
        //// ddlChildCategory.Items.Clear()
        //ObjSubsonic.Binddropdown(ref ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE");
        //ObjSubsonic.Binddropdown(ref ddlChildCategroy1, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE");
    }

    public void BindSubCategory()
    {
        ddlSubCatMR.Items.Clear();
        ddlSubCatUAReq.Items.Clear();

        SubSonic.StoredProcedure spSts = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_GET_ALL_ACTIVE_SUB_CATEGORY");
        DataSet ds = spSts.GetDataSet();
        ddlSubCatMR.DataSource = ds;
        ddlSubCatMR.DataTextField = "SUBC_NAME";
        ddlSubCatMR.DataValueField = "SUBC_CODE";
        ddlSubCatMR.DataBind();
        ddlSubCatMR.Items.Insert(0, "--Select--");
        ddlSubCatMR.ClearSelection();

        ddlSubCatUAReq.DataSource = ds;
        ddlSubCatUAReq.DataTextField = "SUBC_NAME";
        ddlSubCatUAReq.DataValueField = "SUBC_CODE";
        ddlSubCatUAReq.DataBind();
        ddlSubCatUAReq.Items.Insert(0, "--Select--");
        ddlSubCatUAReq.ClearSelection();

        //// ddlChildCategory.Items.Clear()
        //ObjSubsonic.Binddropdown(ref ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE");
        //ObjSubsonic.Binddropdown(ref ddlSubCatUAReq, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE");
    }
    public void BindMainCategory()
    {
        ddlManinCatMR.Items.Clear();
        ddlManinCatMR1.Items.Clear();

        SubSonic.StoredProcedure spSts = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORY");
        DataSet ds = spSts.GetDataSet();
        ddlManinCatMR.DataSource = ds;
        ddlManinCatMR.DataTextField = "MNC_NAME";
        ddlManinCatMR.DataValueField = "MNC_CODE";
        ddlManinCatMR.DataBind();
        ddlManinCatMR.Items.Insert(0, "--Select--");
        ddlManinCatMR.ClearSelection();

        ddlManinCatMR1.DataSource = ds;
        ddlManinCatMR1.DataTextField = "MNC_NAME";
        ddlManinCatMR1.DataValueField = "MNC_CODE";
        ddlManinCatMR1.DataBind();
        ddlManinCatMR1.Items.Insert(0, "--Select--");
        ddlManinCatMR1.ClearSelection();

        //// ddlChildCategory.Items.Clear()
        //ObjSubsonic.Binddropdown(ref ddlManinCatMR, "HDM_GET_ACTIVE_MAIN_CATEGORY", "MNC_NAME", "MNC_CODE");
        //ObjSubsonic.Binddropdown(ref ddlManinCatMR1, "HDM_GET_ACTIVE_MAIN_CATEGORY", "MNC_NAME", "MNC_CODE");
    }

    protected void txtReqIdFilter_Click(object sender, EventArgs e)
    {
        DataSet DS;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_SEARCH_PENDING_REQUESTS_INCHARGE_NEW");
        sp.Command.AddParameter("@REQID", txtReqId.Text, DbType.String);
        sp.Command.AddParameter("@LOCATION", ddlLocaton1.SelectedValue, DbType.String);
        sp.Command.AddParameter("@CATEGORY", ddlChildCategroy1.SelectedValue, DbType.String);
        sp.Command.AddParameter("@SUBCATEGORY", ddlSubCatMR.SelectedValue, DbType.String);
        sp.Command.AddParameter("@MAINCATEGORY", ddlManinCatMR.SelectedValue, DbType.String);
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String);
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String);
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedValue, DbType.String);
        sp.Command.AddParameter("@AURID", Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.String);
        DS = sp.GetDataSet();
        gvViewRequisitions.DataSource = DS.Tables[0];
        gvViewRequisitions.DataBind();
        Session["AssignedGrid"] = DS.Tables[0];
        CalculateTAT();
        btnUpdate1.Visible = DS.Tables.Count >= 1 && DS.Tables[0].Rows != null && DS.Tables[0].Rows.Count > 0;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        DataSet DS;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "HDM_SEARCH_PENDING_UNASSIGNED_REQUESTS_INCHARGE_NEW");
        sp.Command.AddParameter("@REQID", textReqID2.Text, DbType.String);
        sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, DbType.String);
        sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, DbType.String);
        sp.Command.AddParameter("@SUBCATEGORY", ddlSubCatUAReq.SelectedValue, DbType.String);
        sp.Command.AddParameter("@MAINCATEGORY", ddlManinCatMR1.SelectedValue, DbType.String);
        sp.Command.AddParameter("@FROMDATE", txtUAfromDt.Text, DbType.String);
        sp.Command.AddParameter("@TODATE", txtUAtoDt.Text, DbType.String);
        sp.Command.AddParameter("@AURID", Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", Session["COMPANYID"], DbType.String);
        DS = sp.GetDataSet();
        gvUnassigned.DataSource = DS;
        gvUnassigned.DataBind();
        Session["UnAssignedGrid"] = DS;
        CalculateUnAssignedGridTAT();
    }

    protected void gvUnassigned_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUnassigned.PageIndex = e.NewPageIndex;
        gvUnassigned.DataSource = Session["UnAssignedGrid"];
        gvUnassigned.DataBind();
        CalculateUnAssignedGridTAT();
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl_status = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl_status.Parent.Parent;
            TextBox txtadncost = (TextBox)row.FindControl("txtADC");
            Label lbladncost = (Label)row.FindControl("lblADC");

            TextBox txtLbrcost;
            Label lblLbrcost;

            TextBox txtSprcost;
            Label lblSprcost;

            // If CostStatus = True Then
            txtLbrcost = (TextBox)row.FindControl("txtLBC");
            lblLbrcost = (Label)row.FindControl("lblLBC");

            txtSprcost = (TextBox)row.FindControl("txtSPC");
            lblSprcost = (Label)row.FindControl("lblSPC");

            if (ddl_status.SelectedValue == "9")
            {
                txtadncost.Visible = true;
                lbladncost.Visible = false;

                if (CostStatus == true)
                {
                    txtLbrcost.Visible = true;
                    lblLbrcost.Visible = false;

                    txtSprcost.Visible = true;
                    lblSprcost.Visible = false;
                }
                else
                {
                    txtLbrcost.Visible = false;
                    lblLbrcost.Visible = false; // True

                    txtSprcost.Visible = false;
                    lblSprcost.Visible = false; // True
                }
            }
            else
            {
                txtadncost.Visible = false;
                lbladncost.Visible = true;

                if (CostStatus == true)
                {
                    lblLbrcost.Visible = true;
                    txtLbrcost.Visible = false;

                    lblSprcost.Visible = true;
                    txtSprcost.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlSubCatMR_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlChildCategroy1.Items.Clear();
        if (ddlSubCatMR.SelectedIndex > 0)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SUBC_CODE", SqlDbType.VarChar, 50);
            param[0].Value = ddlSubCatMR.SelectedValue;
            ObjSubsonic.Binddropdown(ref ddlChildCategroy1, "HDM_GET_CHILDBY_SUBCATG", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param);
        }
        else
            ObjSubsonic.Binddropdown(ref ddlChildCategroy1, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE");
    }
    protected void ddlManinCatMR_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSubCatMR.Items.Clear();
        if (ddlManinCatMR.SelectedIndex > 0)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@MNC_CODE", SqlDbType.VarChar, 50);
            param[0].Value = ddlManinCatMR.SelectedValue;
            ObjSubsonic.Binddropdown(ref ddlSubCatMR, "HDM_GET_ALLSUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param);
        }
        else
            ObjSubsonic.Binddropdown(ref ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE");
    }
    protected void ddlManinCatMR1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSubCatUAReq.Items.Clear();
        if (ddlManinCatMR1.SelectedIndex > 0)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@MNC_CODE", SqlDbType.VarChar, 50);
            param[0].Value = ddlManinCatMR1.SelectedValue;
            ObjSubsonic.Binddropdown(ref ddlSubCatUAReq, "HDM_GET_ALLSUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param);
        }
        else
            ObjSubsonic.Binddropdown(ref ddlSubCatUAReq, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE");
    }

    protected void ddlSubCatUAReq_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlChildCategory.Items.Clear();
        if (ddlSubCatUAReq.SelectedIndex > 0)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SUBC_CODE", SqlDbType.VarChar, 50);
            param[0].Value = ddlSubCatUAReq.SelectedValue;
            ObjSubsonic.Binddropdown(ref ddlChildCategory, "HDM_GET_CHILDBY_SUBCATG", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param);
        }
        else
            ObjSubsonic.Binddropdown(ref ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE");
    }
    protected void ddlModifyStatus_DataBound(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        if (ddl != null)
        {
            ddl.Items.Insert(0, new ListItem("--Select--", "0"));
            ddl.SelectedIndex = 0;
        }
    }
}
