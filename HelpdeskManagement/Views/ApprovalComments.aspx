﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ApprovalComments.aspx.vb" Inherits="HelpdeskManagement_Views_ApprovalComments" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
     <style>
        .widgets{
            background-color:white;
        }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div class="card">
                    <form id="form1" runat="server">
                        <div class="row form-inline">
                            <div class="form-group col-md-12">
                                <asp:GridView ID="gvapprcmts" runat="server" EmptyDataText="No Remarks Found."
                                    CssClass="table GridStyle" GridLines="None">
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
