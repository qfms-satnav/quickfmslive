﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSLADetails.aspx.vb" Inherits="HDM_HDM_Webfiles_frmSLADetails" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .widgets{
            background-color:white;
        }
         .imgBox{
                text-align: center;
        }
        .imgBox .imgCSS{
            background-color: #eae1e1;
            max-height: 100px;
            max-width: 100px;
            padding: 5px;
            border-radius: 50px;
            margin-top: 10px;
        }
        .imgBox .role{
                font-size: 12px;
                margin-top: 5px;
                color: #777;
                background: #eee;
                border-radius: 10px;
        }
        .imgBox .user{
                font-size: 13px;
                letter-spacing: 0.5px;
                white-space: nowrap;
                width: 150px;
                overflow: hidden;
                text-overflow: ellipsis;
}
        .wrapper{
            display: flex;
            justify-content: center;
        }

          .arrow {
    display: inline-block;
    width: 20px; /* Adjust arrow width as needed */
    text-align: center;
  }
  
  .dotted-line {
    position: unset;
    top: 50%;
    left: 100px; /* Adjust based on image width */
    height: 1px;
    width: 94px; /* Adjust based on image width */
    border-top: 1px solid lightgrey;
    transform: translateY(-50%);
  }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                
                <div class="card">
                    <h4>SLA Details</h4>
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <div class="row form-inline">
                            <div class="form-group col-md-12">
                                <asp:GridView ID="gvSLA" runat="server" AutoGenerateColumns="true"
                                    EmptyDataText="SLA Not Defined. Please Define SLA." CssClass="table GridStyle" GridLines="None">
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <br />
                        <h4>Service Escalation Mapping Details</h4>
                        <div class="row form-inline">
                            <div class="form-group col-md-12">
                                <asp:GridView ID="gvSLAMapped" runat="server" AutoGenerateColumns="false"
                                    EmptyDataText="Service Escalation Not Defined. Please Define Service Escalation." CssClass="table GridStyle" GridLines="None" Visible="false">
                                    <Columns>
                                        <asp:BoundField DataField="ROL_DESC" HeaderText="Role" ItemStyle-HorizontalAlign="left" />
                                        <asp:BoundField DataField="USERS" HeaderText="Employee ID" ItemStyle-HorizontalAlign="left" />
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                       <%-- <h4>View</h4>--%>
                        <div class="row form-inline">
                            <div class="form-group col-md-12 wrapper">
                                <asp:Literal ID="litGridViewContent" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
