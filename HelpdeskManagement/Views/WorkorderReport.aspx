﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WorkOrderReport.aspx.cs" Inherits="HelpdeskManagement_Views_WorkorderReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='http://localhost:58331/fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script lang="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
     <div class="animsition">
          <div class="al-content">
               <div class="widgets">
                    <div ba-panel ba-panel-title="Country Master" ba-panel-class="with-scroll">
                        <div class="panel">
                            <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Work Order</h3>
                        </div>
                               <div class="panel-body" style="padding-right: 50px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                <asp:ValidationSummary ID="vs2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                                <asp:ValidationSummary ID="vsTDS" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val3" />
                                <div class="col-md-12 text-right" >
                                 
                                    <input action="action" class="btn btn-primary custom-button-color" onclick="window.history.go(-1); return false;" type="button" value="Back" />
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:HyperLink id="hyprlink" runat="server" Text="Print" Target="_blank" CssClass="btn btn-primary custom-button-color"/>
                                </div>
                                <div>
                                    <asp:Label runat="server" ID="lblRemarks" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="row table table table-condensed table-responsive">
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" ShowBackButton="true" AsyncRendering="false" ShowPrintButton="false"></rsweb:ReportViewer>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <rsweb:ReportViewer ID="ReportViewer2" runat="server" Width="100%" ShowBackButton="true" AsyncRendering="false" ShowPrintButton="false"></rsweb:ReportViewer>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
               </div>
          </div>
     </div>
      <script src="../../Scripts/moment.min.js"></script>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
    </html>