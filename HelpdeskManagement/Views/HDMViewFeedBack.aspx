﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HDMViewFeedBack.aspx.cs" Inherits="HelpdeskManagement_Views_HDMViewFeedBack" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .disabled {
            cursor: not-allowed;
            pointer-events: none;
            opacity: .5;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMViewFeedBackController" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">View Feedback</h3>
            </div>

            <div class="card">
                <form id="form1" name="HDMViewFeedbackForm">
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMViewFeedback.$submitted && HDMViewFeedback.CustID.$invalid}">
                                <label for="txtcode">Enter Request ID <span style="color: red;"></span></label>
                                <div class="control-label" id='RequestID'>
                                    <input type="text" class="form-control" data-ng-model="HDMViewFeedback.RequestID" id="txtRequestID" name="RequesID" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Clinic <span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Clinic" data-output-model="HDMViewFeedback.Clinic" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMViewFeedback.Clinic[0]" name="LCM_NAME" style="display: none" />
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Main Category <span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Main_Category" data-output-model="HDMViewFeedback.Main_Category" data-button-label="icon MNC_NAME"
                                    data-item-label="icon MNC_NAME maker" data-on-item-click="getsubbymain()" data-on-select-all="mainSelectAll()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMViewFeedback.Main_Category[0]" name="MNC_NAME" style="display: none" />

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Subcategory<span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Sub_Category" data-output-model="HDMViewFeedback.Sub_Category" data-button-label="icon SUBC_NAME"
                                    data-item-label="icon SUBC_NAME maker" data-on-select-all="subSelectAll()" data-tick-property="ticked" data-on-item-click="getchildbysub()" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMViewFeedback.Sub_Category[0]" name="SUBC_NAME" style="display: none" />

                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Child Category <span style="color: red;"></span></label>
                                            <div isteven-multi-select data-input-model="Child" data-output-model="HDMViewFeedback.Child" data-button-label="icon CHC_TYPE_NAME"
                                                data-item-label="icon CHC_TYPE_NAME maker" data-on-item-click="childchange()" data-on-select-all="childSelectAll()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMViewFeedback.Child[0]" name="CHC_TYPE_NAME" style="display: none" />

                                        </div>
                                    </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': HDMViewFeedback.$submitted && HDMViewFeedback.SER_APP_STA_REMARKS_NAME.$invalid}">
                                <label class="control-label">Status <span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Status" data-output-model="HDMViewFeedback.Status" data-button-label="icon SER_APP_STA_REMARKS_NAME"
                                    data-item-label="icon SER_APP_STA_REMARKS_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="HDMViewFeedback.Status[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="HDMViewFeedback.$submitted && HDMViewFeedback.SER_APP_STA_REMARKS_NAME.$invalid" style="color: red;">Please Select Status</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">History Status<span style="color: red;"></span></label>
                                <br />
                                <select id="ddlstatus" class="selectpicker" data-ng-model="HDMViewFeedback.selstatus" data-ng-change="STATUSCHANGE()">
                                    <option value="1">ACTIVE</option>
                                    <option value="2">IN-ACTIVE</option>
                                    <option value="3">ALL</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Select Range<span style="color: red;">*</span></label>
                                <br />
                                <select id="ddlRange" class="selectpicker" data-ng-model="HDMViewFeedback.selVal" data-ng-change="rptDateRanges()">
                                    <option value="">Select Range</option>
                                    <option value="TODAY">Today</option>
                                    <option value="YESTERDAY">Yesterday</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 30 Days</option>
                                    <option value="THISMONTH">This Month</option>
                                    <option value="LASTMONTH">Last Month</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="HDMViewFeedback.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="form1.$submitted && form1.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="HDMViewFeedback.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="form1.$submitted && form1.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-6 col-xs-12">
                            <br />
                            <div class="box-footer">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="LoadData()" />
                            </div>
                        </div>
                    </div>
                </form>
                <form id="form2">
                    <div data-ng-show="GridVisiblity">
                        <div class="clearfix">
                            <%--<div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                                <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                                <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                            </div>
                                        </div>--%>
                        </div>
                        <%-- <div class="row" style="padding-left: 30px">--%>
                        <div class="clearfix">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../JS/HDMViewFeedback.js" defer></script>
    <script src="../JS/HDMFeedBack.js" defer></script>

    <%--<script src="../Js/HDMcustomizedReport.js"></script>--%>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../../Scripts/JSONToCSVConvertor.js" defer></script>
    <script defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';

    </script>
</body>
</html>


