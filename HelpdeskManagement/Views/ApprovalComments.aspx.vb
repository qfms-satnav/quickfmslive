﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class HelpdeskManagement_Views_ApprovalComments
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param(0).Value = Session("reqid")
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("APPR_COMMENTS", param)
            If ds.Tables(0).Columns.Count > 1 And ds.Tables(0).Rows.Count > 0 Then
                gvapprcmts.DataSource = ds.Tables(0) 'TransposeData.GenerateTransposedTable(ds.Tables(0))
                gvapprcmts.DataBind()
            Else
                gvapprcmts.DataSource = Nothing 'TransposeData.GenerateTransposedTable(ds.Tables(0))
                gvapprcmts.DataBind()
            End If
        End If
    End Sub
End Class
