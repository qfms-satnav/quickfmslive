﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class HDM_HDM_Webfiles_frmSLADetails
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Dim LOC_CODE As String
            Dim MAIN_CAT_CODE As String
            Dim SUB_CAT_CODE As String
            Dim CHILD_CAT_CODE As String
            LOC_CODE = Request.QueryString("LOC_CODE")
            MAIN_CAT_CODE = Request.QueryString("MAIN_CAT_CODE")
            SUB_CAT_CODE = Request.QueryString("SUB_CAT_CODE")
            CHILD_CAT_CODE = Request.QueryString("CHILD_CAT_CODE")
            param = New SqlParameter(4) {}
            param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = LOC_CODE
            param(1) = New SqlParameter("@MAIN_CAT_CODE", SqlDbType.NVarChar, 200)
            param(1).Value = MAIN_CAT_CODE
            param(2) = New SqlParameter("@SUB_CAT_CODE", SqlDbType.NVarChar, 200)
            param(2).Value = SUB_CAT_CODE
            param(3) = New SqlParameter("@CHILD_CAT_CODE", SqlDbType.NVarChar, 200)
            param(3).Value = CHILD_CAT_CODE
            param(4) = New SqlParameter("@COMPANY_ID", SqlDbType.Int)
            param(4).Value = Session("COMPANYID")
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("HDM_GET_SLA_DETAILS_BY_LOCATION", param)
            If ds.Tables(0).Columns.Count > 1 And ds.Tables(0).Rows.Count > 0 Then
                gvSLA.DataSource = ds.Tables(0) 'TransposeData.GenerateTransposedTable(ds.Tables(0))
                gvSLA.DataBind()
            Else
                gvSLA.DataSource = Nothing 'TransposeData.GenerateTransposedTable(ds.Tables(0))
                gvSLA.DataBind()
            End If

            gvSLAMapped.DataSource = ds.Tables(1)
            gvSLAMapped.DataBind()
            litGridViewContent.Text = GenerateGridViewContent(ds)
        End If
    End Sub
    Private Function GenerateGridViewContent(DS As DataSet) As String
        Dim htmlContent As New StringBuilder()

        Dim table As DataTable = DS.Tables(1)
        Dim imgTable As DataTable = DS.Tables(2)
        For i As Integer = 0 To table.Rows.Count - 1
            ' Access the data for the current row
            Dim row As DataRow = table.Rows(i)
            Dim role As String = row("ROL_DESC").ToString()
            Dim user As String = row("USERS").ToString()

            Dim Usr As String() = row("USERS").ToString().Split(",")

            Dim imageUrl As String

            htmlContent.Append("<div class=""imgBox"">")
            For Each user In Usr
                Dim Img As String = (From row1 In imgTable.AsEnumerable()
                                     Where row1.Field(Of String)("AUR_KNOWN_AS") = user
                                     Select row1.Field(Of String)("AUR_IMAGEPATH")).FirstOrDefault()
                If Not IsDBNull(Img) AndAlso Not String.IsNullOrEmpty(Img) Then
                    imageUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/userprofiles/" + Img
                Else
                    ' Set default image URL here
                    imageUrl = "/BootStrapCSS/assets/img/default-user.png"
                End If
                htmlContent.Append("<img src=""" & imageUrl & """ alt=""Employee Image"" class=""imgCSS"" />")

                htmlContent.Append("<div class=""user"" title=""" & user & """>" & user & "</div>")
            Next
            htmlContent.Append("<div class=""role"">" & role & "</div>")

            'If Not IsDBNull(row("AUR_IMAGEPATH")) AndAlso Not String.IsNullOrEmpty(row("AUR_IMAGEPATH").ToString()) Then
            '    For Each img1 In img

            '    Next
            '    imageUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/userprofiles/" + row("AUR_IMAGEPATH").ToString()
            'Else
            '    ' Set default image URL here
            '    imageUrl = "/BootStrapCSS/assets/img/default-user.png"
            'End If

            htmlContent.Append("</div>")

            ' Add the dotted line only if it's not the last record
            If i < table.Rows.Count - 1 Then
                htmlContent.Append("<div class=""dotted-line""></div>")
            End If
        Next


        Return htmlContent.ToString()
    End Function
    Protected Sub gvSLAMapped_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        ' You can handle RowDataBound event if you need additional customization
    End Sub
End Class
