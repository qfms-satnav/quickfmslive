﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SkabWorkOrderReport.aspx.cs" Inherits="HelpdeskManagement_Views_SkabWorkOrderReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8">
    <%--<meta http-equiv="X-UA-Compatible" content="IE=edge">--%>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <%-- <script lang="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>--%>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Work Order</h3>
            </div>
            <div>
                <iframe id="PrintReport" name="PrintReport" style="width: 100%; display: none"
                    allowtransparency="true" href="" frameborder="0"></iframe>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                    <asp:ValidationSummary ID="vs2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                    <asp:ValidationSummary ID="vsTDS" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val3" />
                    <div class="col-md-12 text-right">
                        <input action="action" class="btn btn-primary custom-button-color" onclick="window.history.go(-1); return false;" type="button" value="Back" />
                        &nbsp;&nbsp;&nbsp;
                                    <asp:HyperLink ID="hyprlink" runat="server" Text="Print" Target="_blank" CssClass="btn btn-primary custom-button-color" />
                    </div>
                    <div>
                        <asp:Label runat="server" ID="lblRemarks" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" ShowBackButton="true" AsyncRendering="false"></rsweb:ReportViewer>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <rsweb:ReportViewer ID="ReportViewer2" runat="server" Width="100%" ShowBackButton="true" AsyncRendering="false"></rsweb:ReportViewer>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="../../Scripts/moment.min.js"></script>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>


    <script type="text/javascript">






        $(document).ready(function () {
            $('.ToolbarPrint').click(function () {
                //Remove the Print Page size selection Dialog window
                $(".msrs-printdialog-main").remove();
                //Remove the overlay screen
                $(".msrs-printdialog-overlay").remove();
                //Get the RDLC report internalviewer
                var internalViewer = $find('ReportViewer1')._getInternalViewer().ExportUrlBase + 'PDF';
                //Embed the PdfPreviewUrl content in iframe src
                $("#PrintReport").attr("src", internalViewer.PdfPreviewUrl);
            })
        });

          <%--function Print() {
              var report = document.getElementById("<%=ReportViewer1.ClientID %>");
              var div = report.getElementsByTagName("DIV");
              var reportContents;
              for (var i = 0; i < div.length; i++) {
                  if (div[i].id.indexOf("VisibleReportContent") != -1) {
                      reportContents = div[i].innerHTML;
                      break;
                  }
              }
              var frame1 = document.createElement('iframe');
              frame1.name = "frame1";
              //frame1.style.position = "absolute";
              //frame1.style.top = "-1000000px";
              document.body.appendChild(frame1);
              var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
              frameDoc.document.open();
              frameDoc.document.write('<html><head>');
              frameDoc.document.write('</head><body>');
              frameDoc.document.write(reportContents);
              frameDoc.document.write('</body></html>');
              frameDoc.document.close();
              setTimeout(function () {
                  window.frames["frame1"].focus();
                  window.frames["frame1"].print();
                  document.body.removeChild(frame1);
              }, 500);
          }--%>

          //function openInNewTab() {
          //    window.document.forms[0].target = '_blank';
          //    setTimeout(function () { window.document.forms[0].target = ''; }, 0);
          //}

    </script>
</body>
</html>
