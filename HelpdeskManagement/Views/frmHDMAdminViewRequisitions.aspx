﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmHDMAdminViewRequisitions.aspx.cs" Inherits="HelpdeskManagement_Views_frmHDMAdminViewRequisitions" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        body {
            text-align: left;
            vertical-align: top;
        }

        .Container {
            height: 600px;
            width: 100%;
            overflow-y: auto;
            background-color: white;
            border-radius: 5px;
            margin: 0;
            vertical-align: top;
        }

        .Content {
            width: 300px;
            color: gray;
            text-align: center;
        }

        .Flipped, .Flipped .Content {
            transform: rotateX(360deg);
        }

        #wrapper1, #wrapper2 {
            width: 100%;
            border: none 0px RED;
            overflow-x: scroll;
            overflow-y: hidden;
        }

        #wrapper1 {
            height: 20px;
        }

        #wrapper2 {
            /*height: 750px;*/
            overflow-y: scroll;
        }

        #div1 {
            /*width: 1900px;*/
            height: 20px;
        }

        #div2 {
            /*width: 1000px;*/
            height: 100px;
            background-color: #88FF88;
            overflow: auto;
        }
        /*.child {
            transform: rotateX(180deg);
                  }*/
        /* Designing for scroll-bar */
        ::-webkit-scrollbar {
            width: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            /*background: gainsboro;*/
            border-radius: 5px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            /*background: black;*/
            border-radius: 5px;
        }

            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
                background: #555;
            }
    </style>
    <style type="text/css">
        .FixedHeader {
            position: absolute;
            font-weight: bold;
            z-index: 10;
            position: sticky;
            top: 0
        }

        .pagination {
            display: inline-block;
            padding-right: 500px;
            margin: 0px;
        }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
                border: 1px solid #ddd;
            }

                .pagination a.active {
                    background-color: #4CAF50;
                    color: white;
                    border: 1px solid #4CAF50;
                }

                .pagination a:hover:not(.active) {
                    background-color: #ddd;
                }

                .pagination a:first-child {
                    border-top-left-radius: 5px;
                    border-bottom-left-radius: 5px;
                }

                .pagination a:last-child {
                    border-top-right-radius: 5px;
                    border-bottom-right-radius: 5px;
                }
    </style>
    <script type="text/javascript" defer>
        function LoadScroll() {
            var wrapper1 = document.getElementById('wrapper1');
            var wrapper2 = document.getElementById('wrapper2');
            wrapper1.onscroll = function () {
                wrapper2.scrollLeft = wrapper1.scrollLeft;
            };
            wrapper2.onscroll = function () {
                wrapper1.scrollLeft = wrapper2.scrollLeft;
            };
        }
        setTimeout(LoadScroll, 1000);

        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="View and Assign Requests" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Assign Request</h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="showDiv" runat="server">
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtid">Enter Request Id </label>
                                            <asp:TextBox ID="txtReqId" runat="server" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtloc">Location</label>
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Main Category</label>
                                            <asp:DropDownList ID="ddlManinCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true" OnSelectedIndexChanged="ddlManinCatMR_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Subcategory</label>
                                            <asp:DropDownList ID="ddlSubCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true" OnSelectedIndexChanged="ddlSubCatMR_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Child Category</label>
                                            <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>




                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Select Status</label>
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <%--  <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtid">Enter From Date</label>
                                                        <div class='input-group date' id='fromdate'>
                                                            <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>--%>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Enter From Date<span class="text-danger">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                                ErrorMessage="Please Select From Date" ValidationGroup="Val1" ControlToValidate="txtfromDt"></asp:RequiredFieldValidator>
                                            <div class="input-group date" id='fromdate'>
                                                <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy"></asp:TextBox>
                                                <%--<div class="input-group-append">
                                            <div class="input-group-text bg-transparent">
                                                <span class="fa fa-calendar" onclick="setup('fromdate') "></span>
                                            </div>
                                        </div>--%>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtloc">Enter To Date</label>
                                            <div class='input-group date' id='toDt'>
                                                <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                </span>
                                            </div>
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="To date should be greater than From date" ControlToValidate="txtToDt" ControlToCompare="txtfromDt" Type="Date" Operator="GreaterThan" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="col-md-12 text-right">
                                            <br />
                                            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                CausesValidation="true" TabIndex="2" OnClick="btnSubmit_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--     <div class="horizontal-scroll">
                                            <div class="form-group col-md-12">--%>
                            <h1 style="color: green;"></h1>
                            <h2 style="color: green;"></h2>
                            <div>
                                <div id="wrapper1">
                                    <div id="div1">
                                    </div>
                                </div>
                                <div id="wrapper2">
                                    <div class="div2">
                                        <div class="Content">
                                            <table>
                                                <tr>
                                                    <td>


                                                        <%--<asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                                            EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped" OnRowDataBound="OnRowDataBound">--%>
                                                        <asp:GridView ID="gvViewRequisitions" runat="server" AutoGenerateColumns="false" Style="height: 400px; overflow: scroll" AllowPaging="true"
                                                            HeaderStyle-CssClass="FixedHeader" CssClass="table GridStyle" GridLines="None" OnPageIndexChanging="gvViewRequisitions_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Ticket Id">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HelpdeskManagement/Views/frmHDMViewAssignRequisition.aspx?RID={0}")%>'>
                                                                            <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                        </asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Date Submitted">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" Width="100px" Text='<%# Eval("REQUESTED_DATE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />--%>
                                                                <asp:BoundField DataField="REQUESTED_BY" HeaderText="Submitter Name" ItemStyle-HorizontalAlign="left" />
                                                                <asp:TemplateField HeaderText="Reassign Ticket To">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlAssignTo" runat="server" CssClass="selectpicker" Width="70px" ClientIDMode="Static">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="LOCATION" HeaderText="Issue Location" ItemStyle-HorizontalAlign="left" />

                                                                <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Issue Main Category" ItemStyle-HorizontalAlign="left" />
                                                                <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category Detail" ItemStyle-HorizontalAlign="left" />
                                                                <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left" />
                                                                <asp:TemplateField HeaderText="Ticket Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStatus" runat="server" Width="100px" Text='<%# Eval("STATUS")%>' Style='<%# "color:" + Eval("STA_COLOR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:BoundField DataField="STATUS" HeaderText="Current Status" ItemStyle-HorizontalAlign="left" />--%>
                                                                <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Current Assignee" ItemStyle-HorizontalAlign="left" />
                                                                <asp:BoundField DataField="CONTACT_NO" HeaderText="Requester Contact" ItemStyle-HorizontalAlign="left" />
                                                                <asp:BoundField DataField="SER_CLAIM_AMT" HeaderText="Cost Estimate" ItemStyle-HorizontalAlign="left" />
                                                                
                                                                <asp:TemplateField HeaderText="Additional Notes">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtUpdateRemarks" runat="server" TextMode="MultiLine" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="ESC_COUNT" HeaderText="Escalation Count" ItemStyle-HorizontalAlign="left" />
                                                                <%--<asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left" />--%>
                                                                <asp:TemplateField HeaderText="Time Elapsed">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTAT" runat="server" Width="100px" Text='<%# Eval("TOTAL_TIME")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="CallLog Date" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCallLogDt" Text='<%#Eval("SER_CAL_LOG_DT").ToString() %>' runat="server">
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("LCM_CODE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Main" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblMain" runat="server" Text='<%# Eval("MNC_CODE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sub" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSub" runat="server" Text='<%# Eval("SUBC_CODE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Child" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblChild" runat="server" Text='<%# Eval("CHC_TYPE_CODE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                            <PagerStyle CssClass="pagination-ys" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr class="pagination">
                                                    <td>
                                                        <asp:Repeater ID="repeaterPaging" runat="server">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="pagingLinkButton" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                                                    Font-Underline="false" OnClick="linkButton_Click" CssClass='<%#Eval("IsActive") %>'></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                                <%-- <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none">
                                    <div style="height: 60px;">

                                        <div class="row">

                                            <div class="col-md-4">
                                                <label>Total Records </label>
                                                <asp:Label ID="totalnumberofrecords" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                Search by Any     
                                                <asp:TextBox ID="txtSearchVal" runat="server"></asp:TextBox>--%>
                            <%--<asp:Button ID="btnSearchItems" runat="server" Text="Submit" OnClick="btnSearchItems_Click" />--%>
                            <%-- </div>
                                          </div>
                                        <div class="clearfix">
                                            <div class="col-md-7">--%>
                            <%-- <asp:Button ID="Btn_Previous" CommandName="Previous"
                                                    runat="server" OnCommand="ChangePage"
                                                    Text="Previous" />
                                                Page --%>
                            <%-- <asp:Label runat="server" ID="lblCurrentPage"></asp:Label>
                                                of 
                                                <asp:Label runat="server" ID="lblTotalPages"></asp:Label>--%>

                            <%--<asp:Button ID="Btn_Next" runat="server" CommandName="Next"
                                                    OnCommand="ChangePage" Text="Next" />--%>
                            <%--</div>

                                        </div>
                                    </div>
                                </asp:Panel>--%>
                            <div class="row" style="padding-top: 18px;">
                                <%-- <div class="col-md-6 col-sm-12 col-xs-12"></div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 text-left">
                                        <div class="form-group">
                                            <label for="txtcode">Remarks</label>
                                            <asp:TextBox ID="txtUpdateRemarks" Visible="false" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div><br /><br /><br />--%>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" ClientIDMode="Static" CssClass="btn btn-primary custom-button-color" runat="server" Text="Bulk Assign" CausesValidation="true" TabIndex="17" OnClick="Button1_Click" />
                                    </div>
                                </div>
                            </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        $(document).ready(function () {
            var toDate = new Date();
            var twoDigitMonth = (parseInt(toDate.getMonth()) + parseInt(1)) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
            var twoDigitDate = toDate.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
            var currentDate = twoDigitMonth + "/" + twoDigitDate + "/" + toDate.getFullYear();
            $('#txtToDt').val(currentDate);

            var fromDate = new Date();
            fromDate.setDate(fromDate.getDate() - 30);
            twoDigitMonth = (parseInt(fromDate.getMonth()) + parseInt(1)) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
            twoDigitDate = fromDate.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
            currentDate = twoDigitMonth + "/" + twoDigitDate + "/" + fromDate.getFullYear();
            $('#txtfromDt').val(currentDate);


            $('#<%=gvViewRequisitions.ClientID %>').Scrollable({
                ScrollHeight: 300,
                IsInUpdatePanel: true
            });

        });

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function refreshSelectpicker() {
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlManinCatMR.ClientID%>").selectpicker();
            $("#<%=ddlSubCatMR.ClientID%>").selectpicker();
            $("#<%=ddlChildCategory.ClientID%>").selectpicker();
            $("#<%=ddlStatus.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
</body>
</html>
