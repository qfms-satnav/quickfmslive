<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="SetLeaseReminders.aspx.vb" Inherits="Masters_Configuration_SetLeaseReminders"
    Title="Set Lease Reminders" EnableEventValidation="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=leaseReminderChkbox.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            var count = 0;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    count++;
                    if (count > 3) {
                        count = 0;
                        isValid = false;
                        break;
                    }
                }
            }
            args.IsValid = isValid;
        }
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Set Lease Reminders 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ShowSummary="true" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please Select Location"
                                            ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Location" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Lease<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvBDG" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select Lease"
                                            Display="None" ControlToValidate="ddlLeases" InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLeases" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Lease">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div runat="server" id="chkboxRow" visible="false">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Set Lease Reminders for<span style="color: red;">*</span></label>
                                            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least 1 Reminder & atmost 3 Reminders."
                                                ClientValidationFunction="ValidateCheckBoxList" runat="server" Display="None" ValidationGroup="Val1" />
                                            <div class="col-md-7">
                                                <asp:CheckBoxList runat="server" ID="leaseReminderChkbox"
                                                    RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3"
                                                    CssClass="col-md-12 control-label">
                                                    <asp:ListItem Text="6 Months" Value="180"></asp:ListItem>
                                                    <asp:ListItem Text="3 Months" Value="90"></asp:ListItem>
                                                    <asp:ListItem Text="2 Months" Value="60"></asp:ListItem>
                                                    <asp:ListItem Text="1 Month" Value="30"></asp:ListItem>
                                                    <asp:ListItem Text="15 days" Value="15"></asp:ListItem>
                                                    <asp:ListItem Text="7 days" Value="7"></asp:ListItem>
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Status</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status">
                                                <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" CausesValidation="false"></asp:Button>
                                    <asp:Button ID="btnback" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvitems" TabIndex="9" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="True" EmptyDataText="No Lease Reminders Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lease Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLease" runat="server" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLeaseLoc" runat="server" Text='<%#Eval("LEASE_LOCATION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("LEASE_REMINDER_STATUS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSpaceRelease" CausesValidation="false" CommandArgument='<%#Eval("LEASE_NAME")%>'
                                                    CommandName="Edit" runat="server">Edit</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

