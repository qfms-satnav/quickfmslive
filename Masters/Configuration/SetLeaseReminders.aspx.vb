Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Masters_Configuration_SetLeaseReminders
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocations()
            BindGrid()
        End If
    End Sub

    Protected Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATIONS_BY_PROPERTY_ADMIN")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ALL_REMINDERS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"))
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindLeases(ddlLocation.SelectedItem.Value.ToString())
    End Sub

    Private Sub BindLeases(ByVal loc_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LEASES_BY_LOCATION")
        sp.Command.AddParameter("@LOC_ID", loc_code, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count <> 0 Then
            ddlLeases.DataSource = ds
            ddlLeases.DataTextField = "LEASE_NAME"
            ddlLeases.DataValueField = "LEASE_CODE"
            ddlLeases.DataBind()
            ddlLeases.Items.Insert(0, New ListItem("--Select--", "0"))
            chkboxRow.Visible = True
        Else
            ddlLeases.Items.Clear()
            ddlLeases.Items.Insert(0, New ListItem("--Select--", "0"))
            chkboxRow.Visible = False
        End If
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Cleardata()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If
        If btnSubmit.Text = "Submit" Then
            lblMsg.Text = ""
            AddReminder()
            BindGrid()
        ElseIf btnSubmit.Text = "Modify" Then
            lblMsg.Text = ""
            ModifyReminder()
            BindGrid()
        End If
        Cleardata()
    End Sub

    Private Sub AddReminder()
        Try
            Dim days As String = String.Empty
            For Each item As ListItem In Me.leaseReminderChkbox.Items
                If item.Selected Then
                    days += item.Value.ToString() + ","
                End If
            Next

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LEASE_REMINDER")
            sp.Command.AddParameter("@LEASE_NAME", ddlLeases.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@LEASE_LOCATION", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@DAYS", days, DbType.String)
            sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@STATUS_VAL", ddlStatus.SelectedItem.Value, DbType.String)
            Dim status As Integer
            status = sp.ExecuteScalar()
            If status = 0 Then
                lblMsg.Text = "Cannot Add, Reminder already exists for the selected Lease. Please use Edit to modify."
            Else
                lblMsg.Text = "Reminder Added Successfully"
                BindGrid()
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Private Sub ModifyReminder()
        Try
            Dim days As String = String.Empty
            For Each item As ListItem In Me.leaseReminderChkbox.Items
                If item.Selected Then
                    days += item.Value.ToString() + ","
                End If
            Next

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_LEASE_REMINDER")
            sp.Command.AddParameter("@LEASE_NAME", ddlLeases.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@LEASE_LOCATION", ddlLocation.SelectedItem.Value.ToString(), DbType.String)
            sp.Command.AddParameter("@DAYS", days, DbType.String)
            sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@STATUS_VAL", ddlStatus.SelectedItem.Value, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Text = "Reminder Modified Successfully"
            BindGrid()
        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Private Sub Cleardata()
        ddlLocation.SelectedIndex = 0
        ddlLeases.Items.Clear()
        leaseReminderChkbox.ClearSelection()
        chkboxRow.Visible = False
        btnSubmit.Text = "Submit"
    End Sub

    Protected Sub gvitems_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub

    Protected Sub gvitems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems.RowCommand
        lblMsg.Text = ""
        leaseReminderChkbox.ClearSelection()
        If e.CommandName = "Edit" Then
            ViewState("LEASE_NAME") = e.CommandArgument.ToString()
            btnSubmit.Text = "Modify"

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LEASE_INFO")
            sp.Command.AddParameter("@LEASE_NAME", ViewState("LEASE_NAME").ToString(), DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlLocation.SelectedValue = ds.Tables(0).Rows(0)(0).ToString()
                BindLeases(ddlLocation.SelectedItem.Value.ToString())
                ddlLeases.SelectedValue = ViewState("LEASE_NAME").ToString()
                ddlStatus.SelectedValue = ds.Tables(0).Rows(0)(1).ToString()

                For Each item As ListItem In Me.leaseReminderChkbox.Items
                    For Each row As DataRow In ds.Tables(1).Rows
                        If item.Value.ToString() = row.Item(0).ToString() Then
                            item.Selected = True
                        End If
                    Next
                Next

            End If
        End If
        BindGrid()
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/MaintenanceManagement/Masters/Mas_Webfiles/frmPropMasters.aspx")
    End Sub
End Class