Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Masters_Configuration_SetEmailContent
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        
        Try
            lblMsg.Text = ""
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@MAIL_BODY", SqlDbType.NVarChar, 4000)
            param(0).Value = HttpUtility.HtmlEncode(FreeTextBox1.Text)
            param(1) = New SqlParameter("@MAIL_DESC", SqlDbType.NVarChar, 200)
            param(1).Value = txtMailDescription.Text
            param(2) = New SqlParameter("@MAIL_SUBJECT", SqlDbType.NVarChar, 200)
            param(2).Value = txtSubject.Text
            ObjSubSonic.GetSubSonicExecute("MAIL_MASTER_INSERT_UPDATE", param)
            lblMsg.Text = "Mail Template Inserted Successfully."
        Catch ex As Exception

        End Try
    End Sub

   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub

    Private Sub BindGrid()
        ObjSubSonic.BindGridView(gvItems, "GETMAIL_MASTER")
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)


    End Sub
End Class
