﻿Imports System.Data
Imports System.Data.DataRowView

Partial Class Masters_Configuration_frmPropertyAuthority
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then
            BindCity()
            BindGrid()
        End If
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ALLCITY")
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROPERTY_AUTHORITY")
        Dim ds As DataSet
        ds = sp.GetDataSet()
        gvitems.DataSource = ds
        gvitems.DataBind()
        Session("dataset") = ds
    End Sub

    Protected Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ALL_LOCATIONS")
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        lblmsg.Text = ""

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If
        If btnSubmit.Text = "Add" Then
            lblMsg.Text = ""
            Dim validatecode As Integer
            validatecode = ChechAdminLoc()
            If validatecode = 0 Then
                AddAdmin()
                BindGrid()
                lblMsg.Text = "Added Successfully"
            Else
                lblMsg.Text = "Selected Admin Already Exists To This Location"
                Exit Sub
            End If
        ElseIf btnSubmit.Text = "Modify" Then
            ModifyAdmin()
            BindGrid()
            lblMsg.Text = "Modified Successfully"
        End If
            Cleardata()
    End Sub

    Private Function ChechAdminLoc()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "valdiateAdminLoc")
        sp.Command.AddParameter("@CITY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LOC_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@EMP_CODE", txtEmpCode.Text, DbType.String)
        Dim validatecode As Integer = sp.ExecuteScalar()
        Return validatecode
    End Function

    Private Sub AddAdmin()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_PROPERTY_ADMIN")
            sp.Command.AddParameter("@CITY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LOC_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@EMP_CODE", txtEmpCode.Text, DbType.String)
            sp.Command.AddParameter("@STATUS", ddlstatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Execute()
        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Private Sub ModifyAdmin()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_PROPERTY_ADMIN")
            sp.Command.AddParameter("@CITY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LOC_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@EMP_CODE", txtEmpCode.Text, DbType.String)
            sp.Command.AddParameter("@STATUS", ddlstatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@ID", ViewState("ID").ToString(), DbType.String)
            sp.Execute()
        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Private Sub Cleardata()
        btnSubmit.Text = "Add"
        ddlCity.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        txtEmpCode.Text = String.Empty
        ddlstatus.SelectedIndex = 0
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems.RowCommand
        lblmsg.Text = ""
        If e.CommandName = "Edit" Then
            ViewState("ID") = e.CommandArgument.ToString()
            btnSubmit.Text = "Modify"
            BindCity()
            BindLocations()

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROPERTY_ADMIN_DETAILS")
            sp.Command.AddParameter("@ID", e.CommandArgument.ToString(), DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCity.SelectedValue = ds.Tables(0).Rows(0)(0).ToString()
                ddlLocation.SelectedValue = ds.Tables(0).Rows(0)(1).ToString()
                txtEmpCode.Text = ds.Tables(0).Rows(0)(2).ToString()
                ddlstatus.SelectedValue = ds.Tables(0).Rows(0)(3).ToString()
            End If
        End If

    End Sub

    Protected Sub gvitems_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        lblmsg.Text = ""
        Cleardata()
    End Sub

    Protected Sub gvitems_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gvitems.Sorting
        Dim ds As DataSet = TryCast(Session("dataset"), DataSet)
        Dim dt = ds.Tables(0)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gvitems.DataSource = dt
            gvitems.DataBind()
        End If
    End Sub

    Private Function GetSortDirection(ByVal column As String) As String
        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"
        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then
                    sortDirection = "DESC"
                End If
            End If
        End If
        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column
        Return sortDirection
    End Function

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/MaintenanceManagement/Masters/Mas_Webfiles/frmPropMasters.aspx")
    End Sub
End Class