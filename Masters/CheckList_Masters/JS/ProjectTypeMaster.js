﻿
app.service("ProjectTypeService", function ($http, $q, UtilityService) {
    //Save
    this.Save = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectType/Create', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //Update
    this.Update = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectType/Update', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //Bind Grid
    this.BindGrid = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ProjectType/BindGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('ProjectTypeController', function ($scope, $q, ProjectTypeService, $http, $timeout, UtilityService) {

    $scope.ProjectType = {};
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;

    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'In-Active' }];

    var columnDefs = [
        { headerName: "Project Type Code", field: "Prj_Code", width: 150, cellClass: 'grid-align' },
        { headerName: "Project Type Name", field: "Prj_Name", width: 150, cellClass: 'grid-align' },
        { headerName: "Status", template: "{{ShowStatus(data.Prj_Sta_Id)}}", width: 150, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Edit", width: 150, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    ];


    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        suppressMovable: false,
        suppressHorizontalScroll: true,
        enableCellSelection: false,


    };

    //To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        ProjectTypeService.BindGrid().then(function (response) {
            $scope.gridata = response.data;

            $scope.gridOptions.api.setRowData($scope.gridata);
            progress(0, 'Loading...', false);

        }, function (error) {
            console.log(error);
        });
    }
    $timeout(function () {
        $scope.LoadData();
    }, 1000)

    $scope.Save = function () {

        if ($scope.IsInEdit) {
           
            ProjectTypeService.Update($scope.ProjectType).then(function (repeat) {

               $timeout(function () {
                    $scope.LoadData();
                }, 1000)
                
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.IsInEdit = false;
                $scope.ClearData();
                
            }, function (error) {
                console.log(error);
            })
        }
        else {

            ProjectTypeService.Save($scope.ProjectType).then(function (response) {

                $timeout(function () {
                    $scope.LoadData();
                }, 1000)
                showNotification('success', 8, 'bottom-right', $scope.Success);
                
            }, function (error) {
                
                showNotification('error', 8, 'bottom-right', $scope.error);
                
            });
        }
    }

    $scope.EditFunction = function (data) {
        $scope.ProjectType = {};
        angular.copy(data, $scope.ProjectType);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        
    }
    $scope.ClearData = function () {
        $scope.ProjectType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmProjectType.$submitted = false;
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 1 ? 0 :1].Name;
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

});

