﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>


    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/colorpicker.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

</head>
<body data-ng-controller="ProjectTypeController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <div ba-panel ba-panel-title="Project Type Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
                <div class="panel">
                    <div class="panel-heading" style="height: 41px;">
                        <h3 class="panel-title">Project Type Master</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 50px;">
                        <form role="form" id="form1" name="frmProjectType" data-valid-submit="Save()" novalidate>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Project Type Code<span style="color: red;">*</span></label>
                                        <div class="col-md-12">
                                            <div data-ng-class="{'has-error': frmProjectType.$submitted && frmProjectType.Prj_Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers')" onmouseout="UnTip()">
                                                <input id="Prj_Code" type="text" name="Prj_Code" data-ng-readonly="ActionStatus==1" maxlength="15" autofocus  data-ng-model="ProjectType.Prj_Code" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmProjectType.$submitted && frmProjectType.Prj_Code.$invalid" style="color: red">Please enter project type code </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Project Type Name<span style="color: red;">*</span></label>
                                        <div class="col-md-12">
                                            <div data-ng-class="{'has-error': frmProjectType.$submitted && frmProjectType.Prj_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers')" onmouseout="UnTip()">
                                                <input id="Prj_Name" type="text" name="Prj_Name" maxlength="50" data-ng-model="ProjectType.Prj_Name" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmProjectType.$submitted && frmProjectType.Prj_Name.$invalid" style="color: red">Please enter project type name </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              
                                <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Remarks</label>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <textarea id="Textarea1" style="height: 30%" runat="server" data-ng-model="ProjectType.Prj_REM" class="form-control" maxlength="500"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                  <div class="col-md-3 col-sm-12 col-xs-12" style="padding-left: 15px">
                                    <div class="form-group">
                                        <div data-ng-show="ActionStatus==1">
                                            <label class="col-md-12 control-label">Status<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <select id="Select1" name="Prj_Sta_Id" data-ng-model="ProjectType.Prj_Sta_Id" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="row text-right" style="padding-right: 15px; padding-top: 15px">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                        <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>

                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row" style="padding-left: 30px;">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" style="height: 250px; width: 80%;" class="ag-blue"></div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    

    <%--<script defer src="../../SMViews/Utility.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js"></script>
    <script src="../JS/ProjectTypeMaster.js"></script>
</body>
</html>
