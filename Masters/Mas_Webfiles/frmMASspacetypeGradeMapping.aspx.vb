﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager

Partial Class Masters_Mas_Webfiles_frmMASspacetypeGradeMapping
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If Session("uid") = "" Or Session("Parent") = "" Then
        '    Response.Redirect(AppSettings("logout"))
        'End If
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        'lblMsg.Text = String.Empty
        If Not Page.IsPostBack Then
            space_grade.Checked = True
            RoleGrade.Visible = False
            GradeGrid.Visible = False
            bindSpacetype()
            bindGrades()
            BindGrid()
            GradeGridData()
            BindAllocGrades()
        End If
    End Sub

    Protected Sub grades_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles space_grade.CheckedChanged, Role_grade.CheckedChanged
        Try
            If space_grade.Checked = True Then
                SpaceGrade.Visible = True
                SpaceGrid.Visible = True
                rfvcty.Enabled = True
                RoleGrade.Visible = False
                ddlGradeValidator.Enabled = False
                GradeGrid.Visible = False
                lblMsg.Visible = False
            Else
                ddlGradeValidator.Enabled = True
                SpaceGrade.Visible = False
                rfvcty.Enabled = False
                RoleGrade.Visible = True
                SpaceGrid.Visible = False
                GradeGrid.Visible = True
                lblMsg.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub


    Public Sub BindAllocGrades()
        ddlgrades.Items.Clear()
        ObjSubsonic.BindListBox(ddlgrades, "Get_User_Grades", "aur_grade", "aur_grade")
    End Sub

    Public Sub bindSpacetype()
        ddlSpaceType.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSpaceType, "GET_SPACE_LAYERS", "SPC_TYPE_NAME", "SPC_TYPE_CODE")
    End Sub

    Public Sub bindGrades()
        ddlfromGrade.Items.Clear()
        ObjSubsonic.BindListBox(ddlfromGrade, "AMANTRA_EMPLOYEE_GRADE_GETDETAILS", "grade_name", "grade_Code")
        'ObjSubsonic.Binddropdown(ddlToGrade, "AMANTRA_EMPLOYEE_GRADE_GETDETAILS", "grade_name", "grade_Code")
        'For i As Integer = 1 To 350
        '    ddlfromGrade.Items.Insert((i - 1).ToString(), i.ToString())
        '    ddlToGrade.Items.Insert((i - 1).ToString(), i.ToString())
        'Next

    End Sub

    Public Sub BindGrid()
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
        param(0).Value = ""
        param(1) = New SqlParameter("@ID", SqlDbType.Int)
        param(1).Value = 0
        param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
        param(2).Value = "Get"
        param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
        param(3).Value = ""
        param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
        param(4).Value = ""
        param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
        param(5).Value = ""
        param(6) = New SqlParameter("@COMPANY", SqlDbType.NVarChar, 200)
        param(6).Value = ""
        ObjSubsonic.BindGridView(gvItem, "INSERT_SPACE_TYPE_GRADE_MAPPING", param)
    End Sub


    Public Sub GradeGridData()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_GRADE_ALLOCATION_DATA")

        Dim ds As New DataSet
        ds = sp.GetDataSet
        GradesWise.DataSource = ds
        GradesWise.DataBind()

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If space_grade.Checked = True Then

            Dim strEroorMsg As String = String.Empty
            Dim selectedIndex As String = String.Empty

            For Each item As ListItem In ddlfromGrade.Items
                If item.Selected Then
                    If selectedIndex = "" Then
                        selectedIndex = item.Value
                    Else
                        selectedIndex = selectedIndex & ", " & item.Value
                    End If
                End If
            Next

            If Not selectedIndex = "" Then
                If btnSubmit.Text = "Submit" Then
                    strEroorMsg = "Error has been occured while inserting data"
                    btnSubmit.Text = "Submit"
                    If ddlSpaceType.SelectedItem.Value = "--Select--" Or ddlfromGrade.SelectedItem.Value = "--Select--" Then
                        ' PopUpMessage("Enter Mandatory fields", Me)
                        lblMsg.Text = "Enter Mandatory fields"
                        lblMsg.Visible = True
                    Else
                        insertdata(selectedIndex)
                        'obj.Bindlocation(ddlselectLoc)
                    End If
                ElseIf btnSubmit.Text = "Modify" Then
                    strEroorMsg = "Error has been occured while Updating data"
                    btnSubmit.Text = "Modify"
                    If ddlSpaceType.SelectedItem.Value = "--Select--" Or ddlfromGrade.SelectedItem.Value = "--Select--" Then
                        'PopUpMessage("Enter Mandatory fields", Me)
                        lblMsg.Text = "Enter Mandatory fields"
                        lblMsg.Visible = True
                    Else
                        Modifydata(selectedIndex)
                        'obj.Bindlocation(ddlselectLoc)
                        If lblMsg.Text = "" Then
                            btnSubmit.Text = "Submit"
                        End If
                    End If
                End If
                BindGrid()
            Else
                lblMsg.Text = "select at least one grade"
            End If

        Else

            Dim GradeValues = ddlgrades.GetSelectedIndices().ToList()
            Dim GradesArray = (From c In GradeValues Select ddlgrades.Items(c).Value).ToList()
            Dim FinalGrades As String = String.Join(",", GradesArray.ToArray())

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(1).Value = HttpContext.Current.Session("COMPANYID")
            param(2) = New SqlParameter("@GRADES", SqlDbType.NVarChar, 200)
            param(2).Value = FinalGrades
            param(3) = New SqlParameter("@COUNTVAL", SqlDbType.NVarChar, 200)
            param(3).Value = totalcount.Text

            Dim iStatus As String = ObjSubsonic.GetSubSonicExecute("GRADES_ALLOCATION_COUNT_WISE_MAPPING", param)
            If iStatus = "1" Then
                lblMsg.Text = "Grade Counts Successfully Submitted"
                lblMsg.Visible = True
                Cleardata()
            ElseIf iStatus = "0" Then
                lblMsg.Text = "Something went wrong"
                lblMsg.Visible = True
                Cleardata()
            Else
                lblMsg.Text = iStatus + "There are more allocations for grades. Please update and release."
                lblMsg.Visible = True
                Cleardata()
            End If

            GradeGridData()


        End If

    End Sub

    Private Sub insertdata(selectedIndex As String)

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
        param(0).Value = ddlSpaceType.SelectedValue
        param(1) = New SqlParameter("@ID", SqlDbType.Int)
        param(1).Value = 0
        param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
        param(2).Value = "INSERT"
        param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
        param(3).Value = selectedIndex
        param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
        param(4).Value = ddlstatus.SelectedValue
        param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
        param(5).Value = Session("UID")
        param(6) = New SqlParameter("@COMPANY", SqlDbType.Int)
        param(6).Value = HttpContext.Current.Session("COMPANYID")
        Dim iStatus As Integer = ObjSubsonic.GetSubSonicExecute("INSERT_SPACE_TYPE_GRADE_MAPPING", param)
        If iStatus = 0 Then
            lblMsg.Text = "Space Type Successfully Inserted"
            lblMsg.Visible = True
            Cleardata()
        ElseIf iStatus = 1 Then
            lblMsg.Text = "Space Type is in use; try another"
            lblMsg.Visible = True
            Cleardata()
        End If
        BindGrid()
    End Sub
    Private Sub Modifydata(selectedIndex As String)
        Dim param1(0) As SqlParameter
        param1(0) = New SqlParameter("@SPC_TYPE", SqlDbType.NVarChar, 50)
        param1(0).Value = ddlSpaceType.SelectedValue
        Dim stat As Integer = ObjSubsonic.GetSubSonicExecuteScalar("SMS_CHECK_SPCREQ_GRADECHANGE", param1)
        If stat = 0 Then

            Dim param(6) As SqlParameter
            param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
            param(0).Value = ddlSpaceType.SelectedValue
            param(1) = New SqlParameter("@ID", SqlDbType.Int)
            param(1).Value = ViewState("id")
            param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
            param(2).Value = "UPDATE"
            param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
            param(3).Value = selectedIndex
            param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
            param(4).Value = ddlstatus.SelectedValue
            param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
            param(5).Value = Session("UID")
            param(6) = New SqlParameter("@COMPANY", SqlDbType.Int)
            param(6).Value = HttpContext.Current.Session("COMPANYID")

            Dim iStatus As Integer = ObjSubsonic.GetSubSonicExecute("INSERT_SPACE_TYPE_GRADE_MAPPING", param)

            If iStatus = 0 Then
                lblMsg.Text = "Space Type successfully modified "
                lblMsg.Visible = True
                Cleardata()
            ElseIf iStatus = 1 Then
                lblMsg.Text = "Space Type is in use; try another to the Grade"
                lblMsg.Visible = True
                Cleardata()
            End If

            BindGrid()
        Else
            lblMsg.Text = "Approve the pending space requests"
        End If
    End Sub

    Public Sub Cleardata()
        ddlSpaceType.ClearSelection()
        ddlfromGrade.ClearSelection()
        ddlstatus.ClearSelection()
        ddlgrades.ClearSelection()
        totalcount.Text = ""
        btnSubmit.Text = "Submit"
    End Sub


    Protected Sub gvItem_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try

            If e.CommandName = "Edit" Then
                btnSubmit.Text = "Modify"
                ViewState("id") = e.CommandArgument.ToString()
                Dim dr As SqlDataReader
                bindSpacetype()
                bindGrades()
                Dim param(6) As SqlParameter
                param(0) = New SqlParameter("@SGM_LAYER", SqlDbType.NVarChar, 200)
                param(0).Value = ""
                param(1) = New SqlParameter("@ID", SqlDbType.Int)
                param(1).Value = ViewState("id")
                param(2) = New SqlParameter("@FLAG", SqlDbType.NVarChar, 200)
                param(2).Value = "TYPE"
                param(3) = New SqlParameter("@SGM_GRADE", SqlDbType.NVarChar, 200)
                param(3).Value = ""
                param(4) = New SqlParameter("@SGM_STA_ID", SqlDbType.NVarChar, 200)
                param(4).Value = ""
                param(5) = New SqlParameter("@SGM_UPT_BY", SqlDbType.NVarChar, 200)
                param(5).Value = ""
                param(6) = New SqlParameter("@COMPANY", SqlDbType.NVarChar, 200)
                param(6).Value = HttpContext.Current.Session("COMPANYID").ToString
                dr = ObjSubsonic.GetSubSonicDataReader("INSERT_SPACE_TYPE_GRADE_MAPPING", param)
                If (dr.HasRows) Then
                    If dr.Read() Then
                        ddlSpaceType.SelectedValue = dr("SGM_LAYER").ToString()
                        ddlstatus.SelectedValue = dr("SGM_STA_ID").ToString()
                    End If
                    dr.NextResult()
                    While (dr.Read())
                        For Each i As ListItem In ddlfromGrade.Items
                            If i.Value = dr("SGM_GRADE").ToString() Then
                                i.Selected = True
                            End If
                        Next
                    End While
                Else
                    lblMsg.Text = "No space type has been mapped."
                    lblMsg.Visible = True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvItem_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvItem.RowEditing

    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Cleardata()
    End Sub
End Class
