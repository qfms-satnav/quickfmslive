
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Collections.Generic
Imports System.Web.Services

Partial Class Masters_Mas_Webfiles_frmNewUser
    Inherits System.Web.UI.Page

    Dim obj As New clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lbl1.Visible = True
            lbl1.Text = String.Empty
            Dim s As String = Session("uid")
            txtmob.Text = ""
            'txtmob.Attributes.Add("autocomplete", "off")
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
                'txtDOJ.Attributes.Add("readonly", "readonly")
            End If
            EmpCount()
            If Not Page.IsPostBack Then
                lblHead.Visible = True
                lblHead.Text = "Add New User"
                lblProcess.Text = Session("Child")
                lblVertical.Text = Session("Parent")

                txttoday.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.ShortDate)

                obj.BindDepartment(ddldept)
                obj.binddesig(ddldesn)
                ''obj.binduser(ddlrepmgr, Session("uid"))
                obj.BindCountry(ddlcountry)
                ddlcountry.ClearSelection()
                If Session("usercountry") Is Nothing Or Session("usercountry") = "" Then
                    ddlcountry.SelectedValue = 0
                Else
                    ddlcountry.Items.FindByValue(Session("usercountry")).Selected = True
                    BindTimeZones()
                    ddlTimeZone.Items.FindByValue(Session("useroffset")).Selected = True
                    BindCity()
                End If

                ObjSubsonic.Binddropdown(ddlGrade, "GET_EMP_GRADE", "GRADE_NAME", "GRADE_CODE")
                BindCompany()
                If Session("COMPANYID") = 1 Then
                    ddlCompany.ClearSelection()
                    ddlCompany.Items.FindByValue(Session("COMPANYID")).Selected = True
                    ddlCompany.Enabled = True
                Else
                    ddlCompany.ClearSelection()
                    ddlCompany.Items.FindByValue(Session("COMPANYID")).Selected = True
                    ddlCompany.Enabled = False
                End If


                BindVertical(ddlVertical)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmNewUser", "Page_Load", exp)
        End Try
    End Sub
    Public Sub BindVertical(ByVal ddl As DropDownList)
        'BindCombo("USP_GET_VERTICAL", ddl, "VER_NAME", "VER_CODE")
        'ddl.SelectedIndex = 0
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "USP_GET_VERTICAL_BY_USER")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddl.DataSource = sp.GetDataSet()
        ddl.DataTextField = "VER_NAME"
        ddl.DataValueField = "VER_CODE"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select--")
    End Sub

    Private Sub EmpCount()
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "CHECK_TOTAL_EMP_COUNT")
        sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
        Dim EmpCount As Integer
        EmpCount = sp.ExecuteScalar()
        'EmpCount = 1
        If EmpCount = 1 Then
            lbl1.Text = "You Have Exceeded the Maximum Number of Employees."
            Button1.Enabled = False
            Exit Sub
        End If
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GTE_CITYLOC")
        sp.Command.AddParameter("@coun", ddlcountry.SelectedItem.Value, DbType.String)
        ddlcity.DataSource = sp.GetDataSet()
        ddlcity.DataTextField = "cty_name"
        ddlcity.DataValueField = "cty_code"
        ddlcity.DataBind()
        ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindCompany()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COMPANYS")
        ddlCompany.DataSource = sp.GetDataSet()
        ddlCompany.DataTextField = "CNP_NAME"
        ddlCompany.DataValueField = "CNP_ID"
        ddlCompany.DataBind()
        'ddlCompany.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindTimeZones()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TIME_ZONES_BYCTYID")
        sp.Command.AddParameter("@CNY_CODE", ddlcountry.SelectedItem.Value, DbType.String)
        ddlTimeZone.DataSource = sp.GetDataSet()
        ddlTimeZone.DataTextField = "TIME_ZONE_NAME"
        ddlTimeZone.DataValueField = "TIME_ZONE"
        ddlTimeZone.DataBind()
        ddlTimeZone.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    <WebMethod()> _
       <System.Web.Script.Services.ScriptMethod()> _
    Public Shared Function GetEmployeeList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 12
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETUSERS_NAME", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            'count = dtcount
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmNewUser", "GetEmployeeList", exp)
        End Try
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            ' If txtpwd.Text <> txtcpwd.Text Then
            'lbl1.Text = " Please enter correct password "
            'txtpwd.Text = String.Empty
            'txtcpwd.Text = String.Empty
            ' Exit Sub
            ' Else
            '  txtp.Text = txtpwd.Text.Trim

            'End If

            Dim iVal As Integer = obj.checkuser(txtaurid.Text.Trim(), Page)

            If iVal < 1 Then

                insertdata()
                lbl1.Text = "User Added Successfully"
                Cleardata()

            Else
                PopUpMessageNormal("USER ID :" & txtaurid.Text & " Already Exists. Please choose another ID !", Me.Page)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while inserting data", "frmNewUser", "Button1_Click", exp)
        End Try
    End Sub

    Public Sub insertdata()
        Dim strEmp As String = String.Empty
        'strEmp = ddlrepmgr.SelectedValue
        strEmp = Split(ddlrepmgr.Text, "/")(0)
        obj.insert_user(txtfname.Text.Trim.Replace("'", "''"),
                        txtmname.Text.Trim.Replace("'", "''"), txtlname.Text.Replace("'", "''"), txtaurid.Text.Trim("'", "''"), txtpwd.Text, ddltitle.SelectedItem.Text.Trim(), txtmailid.Text.Trim().Replace("'", "''"), strEmp, ddldg.SelectedItem.Value, txtAddress.Text.Trim().Replace("'", "''"), ddlcity.SelectedItem.Value, "", ddlGrade.SelectedValue.Trim().Replace("'", "''"), Me, Request.Form(txtmob.UniqueID).ToString().Trim().Replace("'", "''"), ddldesn.SelectedValue, ddldept.SelectedValue, ddlcountry.SelectedValue, ddlTimeZone.SelectedValue, ddlCostcenter.SelectedValue, ddlVertical.SelectedValue, ddlCompany.SelectedValue, RemarksDesc.Text.Trim.Replace("'", "''"))
        obj.Bindlocation(ddldg)
    End Sub

    Private Sub Cleardata()
        ddltitle.SelectedIndex = 0
        txtfname.Text = ""
        txtmname.Text = ""
        txtlname.Text = ""
        txtmailid.Text = ""
        txtaurid.Text = ""
        ddlcountry.SelectedIndex = 0
        ddlTimeZone.SelectedIndex = 0
        ddlcity.SelectedIndex = 0
        ddlGrade.ClearSelection()
        ddldg.SelectedIndex = 0
        ddldept.SelectedIndex = 0
        ddldesn.SelectedIndex = 0
        ddlrepmgr.Text = ""
        ddlCostcenter.SelectedIndex = 0
        txtAddress.Text = ""
        txtmob.Text = ""
        txtpwd.Text = ""
        RemarksDesc.Text = ""
        'txtcpwd.Text = ""
        'txtDOJ.Text = ""
        'lblMsg.Text = ""
    End Sub

    Protected Sub txtpwd_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtpwd.TextChanged

    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        'Response.Redirect("~/AdminFunctions/Adf_WEBFILES/frmADFsearch.aspx")
        Response.Redirect("~/AdminFunctions/Adf_WEBFILES/frmUserRoleMapping.aspx")
    End Sub

    Protected Sub ddlcountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcountry.SelectedIndexChanged
        If ddlcountry.SelectedIndex > 0 Then
            BindCity()
            BindTimeZones()
        Else
            ddlcity.Items.Clear()
            ' ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
            ' ddlcity.SelectedIndex = 0
        End If

    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlcountry.SelectedIndex > 0 And ddlcity.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETLOC")
            sp.Command.AddParameter("@city", ddlcity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@country", ddlcountry.SelectedItem.Value, DbType.String)
            ddldg.DataSource = sp.GetDataSet()
            ddldg.DataTextField = "LCM_NAME"
            ddldg.DataValueField = "LCM_CODE"
            ddldg.DataBind()
            ddldg.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        If (ddlVertical.SelectedIndex > 0) Then
            Dim sp1 As New SqlParameter("@vc_Vertical", SqlDbType.VarChar, 250)
            sp1.Value = ddlVertical.SelectedValue
            ddlCostcenter.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getvscostcenter", sp1)
            ddlCostcenter.DataTextField = "cost_center_name"
            ddlCostcenter.DataValueField = "cost_center_code"
            ddlCostcenter.DataBind()
            ddlCostcenter.Items.Insert(0, "--Select--")
        Else
            ddlCostcenter.SelectedIndex = 0
            ddlCostcenter.Items.Clear()
        End If
    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        ddltitle.SelectedIndex = 0
        txtfname.Text = ""
        txtmname.Text = ""
        txtlname.Text = ""
        txtmailid.Text = ""
        txtaurid.Text = ""
        ddlcountry.SelectedIndex = 0
        ddlTimeZone.SelectedIndex = 0
        ddlcity.SelectedIndex = 0
        ddldg.SelectedIndex = 0
        txtAddress.Text = ""
        txtmob.Text = ""
        ddlGrade.SelectedIndex = 0
        ddlCompany.SelectedIndex = 0
        ddldept.SelectedIndex = 0
        ddldesn.SelectedIndex = 0
        ddlrepmgr.Text = ""
        ddlVertical.SelectedIndex = 0
        ddlCostcenter.SelectedIndex = 0
        txtpwd.Text = ""
        'txtcpwd.Text = ""
    End Sub
End Class
