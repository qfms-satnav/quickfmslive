﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master"
    CodeFile="AddVehicle.aspx.vb" Inherits="Masters_Mas_Webfiles_AddVehicle" Title="Add Vehicle Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

    <script language="javascript" type="text/javascript" defer> 
     function maxLength(s,args)
     { 
     if(args.Value.length >= 500)
     args.IsValid=false;
     }
    function noway(go)
    {
        if(document.all)
        {
            if (event.button == 2)
            {
                alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                return false;
            }
        }
        if (document.layers) 
        {
            if (go.which == 3)
            {
                alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                return false;
            }
        }
    } 
    
    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">  Add Vehicle <hr align="center" width="60%" /></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp; Add Vehicle </strong>
                    </td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="divmessagebackground"
                            ForeColor="" ValidationGroup="Val2" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                            <tr id="Tr8" runat="server">
                                <td align="left" style="width: 25%; height: 26px;">
                                    Employee Id
                                    <%-- <asp:RequiredFieldValidator ID="rfvEmployeeId" runat="server" ControlToValidate="txtEmpId"
                                                        Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Employee Id"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revemployee" runat="server" ControlToValidate="txtEmpId"
                                                        ErrorMessage="Please enter valid Employee Id" Display="None" ValidationGroup="Val2"
                                                        ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>--%>
                                </td>
                                <td align="left" style="width: 25%; height: 26px;">
                                    <div onmouseover="Tip('Enter Numerics only')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtEmpId" runat="server" CssClass="clsTextField" TabIndex="5" Width="97%"
                                            MaxLength="10"></asp:TextBox>
                                </td>
                                <td align="center" style="height: 10px">
                                    &nbsp;
                                    <asp:Button ID="btnView" runat="server" CssClass="button" Text="Add Vehicle" Width="90px"
                                        ValidationGroup="Val2" />
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="pnlEmpName" runat="server" Width="100%" Visible="false">
                            <table id="table5" cellspacing="1" cellpadding="1" width="100%" border="1">
                                <tr id="Tr1" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Employee Name
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseover="Tip('Enter Employee Name')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtEmpName" runat="server" CssClass="clsTextField" TabIndex="5"
                                                Width="97%" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Cost Center
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseover="Tip('Enter Cost Center')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtCostCenter" runat="server" CssClass="clsTextField" TabIndex="5"
                                                Width="97%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="Tr3" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        New Grade
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseover="Tip('Enter New Grade ')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtNewGrade" runat="server" CssClass="clsTextField" TabIndex="5"
                                                Width="97%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlInfo" runat="server" Width="100%" Visible="false">
                            <table id="table4" cellspacing="1" cellpadding="1" width="100%" border="1">
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        PO / Capitalization Date
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPODate"
                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Date"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td35" align="left" style="width: 25%; height: 26px;" runat="server">
                                        <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtPODate" runat="server" CssClass="clsTextField" Width="97%" TabIndex="25"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtPODate"
                                                Format="dd-MMM-yyyy">
                                            </cc1:CalendarExtender>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Fuel Type
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                               ControlToValidate="txtFuelType" Display="None" 
                                               ErrorMessage="Please Enter Fuel Type" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Fuel Type')">
                                            <asp:TextBox ID="txtFuelType" runat="server" CssClass="clsTextField" MaxLength="10"
                                                TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Invoice Date
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtInvDate"
                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Date"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td1" align="left" style="width: 25%; height: 26px;" runat="server">
                                        <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtInvDate" runat="server" CssClass="clsTextField" Width="97%" TabIndex="25"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtInvDate"
                                                Format="dd-MMM-yyyy">
                                            </cc1:CalendarExtender>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Invoice No
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtInvNo"
                                                   Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Invoice No"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseover="Tip('Enter Invoice No')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtInvNo" runat="server" CssClass="clsTextField" TabIndex="5" Width="97%"
                                                MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="Tr2" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Asset/Model
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAssetModel"
                                                   Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Asset/Model"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseover="Tip('Enter Asset/Model')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtAssetModel" runat="server" CssClass="clsTextField" TabIndex="5"
                                                Width="97%" MaxLength="10"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Registration No
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtRegNo"
                                                   Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Registration No"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseover="Tip('Enter Registration No')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtRegNo" runat="server" CssClass="clsTextField" TabIndex="5" Width="97%"
                                                MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Engine No
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                            ControlToValidate="txtEngnNo" Display="None" 
                                                            ErrorMessage="Please Enter Engine No" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Engine No')">
                                            <asp:TextBox ID="txtEngnNo" runat="server" CssClass="clsTextField" MaxLength="10"
                                                TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Chassis No
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                               ControlToValidate="txtChasisNo" Display="None" 
                                               ErrorMessage="Please Enter Chasis No" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Chasis No')">
                                            <asp:TextBox ID="txtChasisNo" runat="server" CssClass="clsTextField" MaxLength="10"
                                                TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr6" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        CC
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                               ControlToValidate="txtCC" Display="None" ErrorMessage="Please Enter CC" 
                                               ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                           --%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Numerics only')">
                                            <asp:TextBox ID="txtCC" runat="server" CssClass="clsTextField" MaxLength="10" TabIndex="5"
                                                Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        New Limit
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                               ControlToValidate="txtNewLimit" Display="None" 
                                               ErrorMessage="Please Enter New Limit" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Numerics only')">
                                            <asp:TextBox ID="txtNewLimit" runat="server" CssClass="clsTextField" MaxLength="10"
                                                TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr9" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Total Vehicle Cost(Including Oct)
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                               ControlToValidate="txtTotVehicleCost" Display="None" 
                                               ErrorMessage="Please Enter Total Vehicle Cost(Including Oct)" 
                                               ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Numerics only')">
                                            <asp:TextBox ID="txtTotVehicleCost" runat="server" CssClass="clsTextField" MaxLength="10"
                                                TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Registration Date
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtRegDate"
                                            Display="None" ErrorMessage="Please Enter Registration Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td2" runat="server" align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Please click on the textbox to select Date')">
                                            <asp:TextBox ID="txtRegDate" runat="server" CssClass="clsTextField" TabIndex="25"
                                                Width="97%"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd-MMM-yyyy"
                                                TargetControlID="txtRegDate">
                                            </cc1:CalendarExtender>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Date of Replacement
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtDOFReplacement"
                                            Display="None" ErrorMessage="Please Enter Date of Replacement" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td3" runat="server" align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Please click on the textbox to select Date')">
                                            <asp:TextBox ID="txtDOFReplacement" runat="server" CssClass="clsTextField" TabIndex="25"
                                                Width="97%"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd-MMM-yyyy"
                                                TargetControlID="txtDOFReplacement">
                                            </cc1:CalendarExtender>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Filling Status
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" 
                                               ControlToValidate="txtFillingStatus" Display="None" 
                                               ErrorMessage="Please Enter Filling Status" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Filling Status')">
                                            <asp:TextBox ID="txtFillingStatus" runat="server" CssClass="clsTextField" TabIndex="5"
                                                Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr11" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Original Buyer
                                        <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                                               ControlToValidate="txtOrgBuyer" Display="None" 
                                               ErrorMessage="Please Enter Orginal Buyer" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Original Buyer')">
                                            <asp:TextBox ID="txtOrgBuyer" runat="server" CssClass="clsTextField" TabIndex="5"
                                                Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Year of Purchase
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                                               ControlToValidate="txtYOP" Display="None" 
                                               ErrorMessage="Please Enter Year of Purchase" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Numerics only')">
                                            <asp:TextBox ID="txtYOP" runat="server" CssClass="clsTextField" MaxLength="10" TabIndex="5"
                                                Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Date of Disposal
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtDOP"
                                            Display="None" ErrorMessage="Please Enter Date of Disposal" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td4" runat="server" align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Please click on the textbox to select Date')">
                                            <asp:TextBox ID="txtDOP" runat="server" CssClass="clsTextField" TabIndex="25" Width="97%"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd-MMM-yyyy"
                                                TargetControlID="txtDOP">
                                            </cc1:CalendarExtender>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Month of Retirement
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                                               ControlToValidate="txtMOR" Display="None" 
                                               ErrorMessage="Please Enter Month of Retirement" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Month of Retirement')">
                                            <asp:TextBox ID="txtMOR" runat="server" CssClass="clsTextField" TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr14" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Insurance Policy No
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" 
                                               ControlToValidate="txtInsPlcyNo" Display="None" 
                                               ErrorMessage="Please Enter Insurance Policy No" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Insurance Policy No')">
                                            <asp:TextBox ID="txtInsPlcyNo" runat="server" CssClass="clsTextField" TabIndex="5"
                                                Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Insurance From Date
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtInsFrmDate"
                                            Display="None" ErrorMessage="Please Enter Insurance From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td5" runat="server" align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Please click on the textbox to select Date')">
                                            <asp:TextBox ID="txtInsFrmDate" runat="server" CssClass="clsTextField" TabIndex="25"
                                                Width="97%"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd-MMM-yyyy"
                                                TargetControlID="txtInsFrmDate">
                                            </cc1:CalendarExtender>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Insurance To Date
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtInsToDate"
                                            Display="None" ErrorMessage="Please Enter Insurance To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td id="Td6" runat="server" align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Please click on the textbox to select Date')">
                                            <asp:TextBox ID="txtInsToDate" runat="server" CssClass="clsTextField" TabIndex="25"
                                                Width="97%"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender7" runat="server" Format="dd-MMM-yyyy"
                                                TargetControlID="txtInsToDate">
                                            </cc1:CalendarExtender>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Year Of Expiry
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" 
                                               ControlToValidate="txtYOE" Display="None" 
                                               ErrorMessage="Please Enter Year Of Expiry" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Month of Year Of Expiry ')">
                                            <asp:TextBox ID="txtYOE" runat="server" CssClass="clsTextField" TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="Tr16" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        RC Book Status
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" 
                                               ControlToValidate="txtRCBook" Display="None" 
                                               ErrorMessage="Please Enter RC Book Status" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter RC Book Status ')">
                                            &nbsp;<asp:DropDownList ID="ddlRCBookStatus" runat="server">
                                            </asp:DropDownList></div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Tax Invoice Status
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" 
                                               ControlToValidate="txtTaxInv" Display="None" 
                                               ErrorMessage="Please Enter Tax Invoice Status" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Tax Invoice Status ')">
                                            &nbsp;<asp:DropDownList ID="ddlTaxInvoice" runat="server">
                                            </asp:DropDownList></div>
                                    </td>
                                </tr>
                                <tr id="Tr18" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        File No
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter File No ')">
                                            <asp:TextBox ID="txtFileNo" runat="server" CssClass="clsTextField" TabIndex="5" Width="97%"></asp:TextBox>
                                        </div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Remarks ')">
                                            &nbsp;</div>
                                    </td>
                                </tr>
                                <tr id="Tr20" runat="server">
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Status
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" 
                                               ControlToValidate="txtStatus" Display="None" ErrorMessage="Please Enter Status" 
                                               ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Status ')">
                                            &nbsp;<asp:DropDownList ID="ddlVehStatus" runat="server">
                                            </asp:DropDownList></div>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Status of Insurance Policy
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" 
                                               ControlToValidate="txtStatusIns" Display="None" 
                                               ErrorMessage="Please Enter Status of Insurance Policy" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <div onmouseout="UnTip()" onmouseover="Tip('Enter Status of Insurance Policy ')">
                                            &nbsp;<asp:DropDownList ID="ddlInsPolicy" runat="server">
                                            </asp:DropDownList></div>
                                    </td>
                                </tr>
                                <tr id="Tr4" runat="server">
                                    <td align="left" style="width: 25%; height: 26px" valign="top">
                                        Remarks
                                    </td>
                                    <td align="left" colspan="3" style="height: 26px">
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" TabIndex="5"
                                                TextMode="MultiLine" Width="97%" Rows="6"></asp:TextBox></td>
                                </tr>
                            </table>
                            <table border="1" class="table" style="height: 22px" width="100%">
                                <tr>
                                    <td align="center" style="height: 10px">
                                        &nbsp;
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" ValidationGroup="Val1"
                                            Width="76px" />
                                        &nbsp;
                                        <asp:Button ID="btnback" runat="server" CssClass="button" Text="Back" Width="76px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                            &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px;" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
