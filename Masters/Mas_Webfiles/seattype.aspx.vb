Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_Mas_Webfiles_seattype
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If Not Page.IsPostBack Then
                objsubsonic.BindGridView(gvItem, "EFM_SEAT_MASTER")
                trCName.Visible = False
                rbActions.Items(0).Selected = True
                objsubsonic.Binddropdown(ddlseattype, "EFM_SEAT_MASTER", "seat_type", "seat_type_code")

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmSeatType", "Page_Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        Try
            Dim i As Integer
            For i = 0 To rbActions.Items.Count - 1
                If rbActions.Items(i).Selected = True Then
                    If rbActions.Items(i).Text = "Add" Then
                        trCName.Visible = False
                        txtseattypecode.ReadOnly = False
                        btnSubmit.Text = "Submit"
                        Cleardata()
                    ElseIf rbActions.Items(i).Text = "Modify" Then
                        trCName.Visible = True
                        txtseattypecode.ReadOnly = True
                        btnSubmit.Text = "Modify"
                        ddlseattype.Items.Clear()
                        objsubsonic.Binddropdown(ddlseattype, "EFM_SEAT_MASTER", "seat_type", "seat_type_code")

                    End If
                End If
            Next
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmSeatType", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub
    Private Sub Cleardata()
        txtseattypecode.Text = String.Empty
        txtseatname.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlseattype.SelectedIndex = -1
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.SelectedItem.Text = "Add" Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtseattypecode.Text = String.Empty Or txtseatname.Text = String.Empty Or txtRemarks.Text = String.Empty Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 1000 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 1000 characters", Me.Page)
                Else
                    Insertdata(1)
                End If
            ElseIf rbActions.SelectedItem.Text = "Modify" Then
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If ddlseattype.SelectedItem.Value = "--Select--" Or txtseattypecode.Text = String.Empty Or txtseatname.Text = String.Empty Or txtRemarks.Text = String.Empty Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 1000 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 1000 characters", Me.Page)
                Else
                    Insertdata(2)
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmSeatType", "btnSubmit_Click", exp)
        End Try
    End Sub
    Private Sub Insertdata(ByVal mode As Integer)
        Dim i As Integer
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@SEAT_TYPE_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = txtseattypecode.Text
        param(1) = New SqlParameter("@SEAT_TYPE_NAME", SqlDbType.NVarChar, 200)
        param(1).Value = txtseatname.Text
        param(2) = New SqlParameter("@REMARKS", SqlDbType.NVarChar, 1000)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@MODE", SqlDbType.Int)
        param(3).Value = mode
        i = objsubsonic.GetSubSonicExecuteScalar("INSERT_SEAT_TYPE_MASTER", param)
        If i = 1 Then
            lblMsg.Text = "Seat Type code already Exists "
            lblMsg.Visible = True
        ElseIf i = 2 Then

            lblMsg.Text = "Seat Type added Successfully "
            lblMsg.Visible = True
            Cleardata()
        Else
            lblMsg.Text = "Seat Type Modified Successfully "
            lblMsg.Visible = True
            Cleardata()
        End If
        objsubsonic.BindGridView(gvItem, "EFM_SEAT_MASTER")
        objsubsonic.Binddropdown(ddlseattype, "EFM_SEAT_MASTER", "seat_type", "seat_type_code")
    End Sub
   

    Protected Sub ddlseattype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlseattype.SelectedIndexChanged
        Try
            If ddlseattype.SelectedItem.Value <> "--Select--" Then
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@SEAT_TYPE_CODE", SqlDbType.NVarChar, 200)
                param(0).Value = ddlseattype.SelectedItem.Value
                Dim ds As DataSet = objsubsonic.GetSubSonicDataSet("GET_SEAT_TYPE_DATA", param)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtseattypecode.Text = ds.Tables(0).Rows(0).Item("SEAT_TYPE_CODE")
                    txtseatname.Text = ds.Tables(0).Rows(0).Item("SEAT_TYPE")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("SEAT_REMARKS")
                End If
            Else
                Cleardata()
                objsubsonic.BindGridView(gvItem, "EFM_SEAT_MASTER")
                objsubsonic.Binddropdown(ddlseattype, "EFM_SEAT_MASTER", "seat_type", "seat_type_code")
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmSeatType", "ddlseattype_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex()
        objsubsonic.BindGridView(gvItem, "EFM_SEAT_MASTER")
    End Sub
End Class
