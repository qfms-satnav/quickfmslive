Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Masters_MAS_WebFiles_frmMASDepartment
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If Session("uid") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'End If
            lblMsg.Text = ""

            If IsPostBack Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
            End If
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            'param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using
            'RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            'RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            'lblvertical.Text = Session("Parent") + " Name "
            'frvVer.ErrorMessage = "Please Select " + Session("Parent") + " Name "
            obj.Department_LoadGrid(gvItem)

            If Not Page.IsPostBack Then
                trCName.Visible = False
                rbActions.Checked = True
                obj.BindDepartment(ddlDepartment)
                'obj.BindVertical(ddlVertical)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDepartment", "Page_Load", exp)
        End Try
    End Sub

    Private Sub Cleardata()
        txtDeptcode.Text = String.Empty
        txtDeptname.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlDepartment.SelectedIndex = 0
        'ddlVertical.SelectedIndex = 0
    End Sub
    Dim intStatus As Integer
    Dim intInsertModifyStatus As Integer
    Private Sub insertdata()
        obj.getcode = txtDeptcode.Text
        obj.getname = txtDeptname.Text
        obj.getRemarks = txtRemarks.Text
        intInsertModifyStatus = 1
        intStatus = obj.InsertDepartment(Me, intInsertModifyStatus)

        If intStatus = 1 Then
            lblMsg.Text = "Department Code is in use; try another."
        ElseIf intStatus = 2 Then
            lblMsg.Text = "Department Successfully Inserted..."
        ElseIf intStatus = 3 Then
            lblMsg.Text = "Department Successfully Inserted..."
            Cleardata()
        End If
        obj.Department_LoadGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getname = txtDeptname.Text
        obj.getRemarks = txtRemarks.Text
        intInsertModifyStatus = 2
        obj.getcode = txtDeptcode.Text
        intStatus = obj.InsertDepartment(Me, intInsertModifyStatus)
        If intStatus = 4 Then
            lblMsg.Text = "Department Successfully modified"
            Cleardata()
        Else
            lblMsg.Text = "Modification failed"
        End If

        obj.Department_LoadGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trCName.Visible = False
                txtDeptcode.ReadOnly = False
                Cleardata()
                btnSubmit.Text = "Submit"
            Else
                Cleardata()
                trCName.Visible = True

                txtDeptcode.ReadOnly = True
                Cleardata()
                btnSubmit.Text = "Modify"
                obj.BindDepartment(ddlDepartment)
                'obj.BindVertical(ddlVertical)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDepartment", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASDepartment", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDepartment.SelectedIndexChanged
        Try
            If ddlDepartment.SelectedValue <> "--Select--" Then
                'obj.BindVertical(ddlVertical)
                obj.Department_SelectedIndex_Changed(ddlDepartment)


                txtDeptcode.Text = obj.getcode
                txtDeptname.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindDepartment(ddlDepartment)
                'obj.BindVertical(ddlVertical)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDepartment", "ddlDepartment_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Department_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDepartment", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub lnkStauts_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lnk As LinkButton = CType(sender, LinkButton)
        Dim intStatus As Integer = lnk.AccessKey
        Dim strDept As String = lnk.CommandArgument
        Try
            obj.Department_Rowcommand(intStatus, strDept)

            obj.BindDepartment(ddlDepartment)
            'obj.BindVertical(ddlVertical)
            obj.Department_LoadGrid(gvItem)
            Cleardata()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASDepartment", "lnkStauts_Click", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
End Class