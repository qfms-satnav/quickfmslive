﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Masters_Mas_Webfiles_frmMASShift
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        RegularExpressionValidator1.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        revPropertyType.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()

        If Not IsPostBack Then
            GETCOUNTRY()
            GetSeatType()
            btnSubmit.Text = "Add"
            fillgrid()
            trshift.Visible = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If btnSubmit.Text = "Add" Then
            Dim ValidateCode As Integer
            ValidateCode = ValidateBrand(txtBrand.Text)
            If ValidateCode = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Shift Code is in use; try another"
            ElseIf ValidateCode = 1 Then
                insertnewrecord()
                GETSHIFTS()
                'fillgrid()
            End If
        Else
            txtBrand.Enabled = False
            If ddlBrand.SelectedIndex <> 0 Then
                modifydata()
                cleardata()
            End If
            'GETSHIFTS()
        End If
        ' Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=1")
        ' insertnewrecord()
        'cleardata()

    End Sub

    Public Function ValidateBrand(ByVal brandcode As String)
        Dim ValidateCode As Integer
        'Dim PN_PROPERTYTYPE As String = brandcode
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_VALIDATE_SHIFT")
        sp1.Command.AddParameter("@SH_CODE", brandcode, DbType.String)
        sp1.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function

    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_INSERT_SHIFT")
            '@VT_CODE,@VT_TYPE,@VT_STATUS,@VT_CREATED_BY,@VT_CREATED_DT,@VT_REM
            sp1.Command.AddParameter("@SH_CODE", txtBrand.Text, DbType.String)
            sp1.Command.AddParameter("@SH_NAME", txtBrandName.Text, DbType.String)
            sp1.Command.AddParameter("@SH_FRM_HRS", starttimehr.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@SH_TO_HRS", endtimehr.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@SH_FRM_MINS", starttimemin.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@SH_TO_MINS", endtimemin.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@SH_STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@SH_CNY", ddlCountry.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SH_LOC", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SH_SEAT_TYPE", ddlShiftType.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "New Shift Successfully Added"
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_SHIFTS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvCat.DataSource = ds
        gvCat.DataBind()
        For i As Integer = 0 To gvCat.Rows.Count - 1
            Dim lblstatus As Label = CType(gvCat.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "Inactive"
            End If
        Next
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged

        If rbActions.Checked = True Then
            'ddlBrand.Visible = False
            'lblAssetBrand.Visible = False
            trshift.Visible = False
            cleardata()
            btnSubmit.Text = "Add"
            lblMsg.Visible = False
            txtBrand.Enabled = True
            fillgrid()
        Else
            'ddlBrand.Visible = True
            'lblAssetBrand.Visible = True
            trshift.Visible = True
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            txtBrand.Enabled = False
            ' GETSHIFTS()
            fillgrid()
        End If

    End Sub

    Private Sub cleardata()
        txtBrand.Text = ""
        txtBrandName.Text = ""
        txtBrand.Enabled = True
        ddlBrand.SelectedIndex = -1
        ddlStatus.SelectedIndex = 0
        starttimehr.SelectedIndex = 0
        endtimehr.SelectedIndex = 0
        starttimemin.SelectedIndex = 0
        endtimemin.SelectedIndex = 0

        ddlCountry.ClearSelection()
        ddlLocation.ClearSelection()
        ddlLocation.Items.Clear()
        ddlBrand.Items.Clear()
        ddlShiftType.SelectedIndex = 0
    End Sub

    Private Sub GETSHIFTS()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ALLSHIFTS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlBrand.DataSource = sp.GetDataSet()
        ddlBrand.DataTextField = "SH_CODE"
        ddlBrand.DataValueField = "SH_NAME"
        ddlBrand.DataBind()
        ddlBrand.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        If ddlBrand.SelectedIndex <> 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ALLSHIFTS_2")
            sp.Command.AddParameter("@SH_CODE", ddlBrand.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCN_CODE", ddlLocation.SelectedItem.Value, DbType.String)

            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then '
                txtBrand.Text = ds.Tables(0).Rows(0).Item("SH_CODE")
                txtBrandName.Text = ds.Tables(0).Rows(0).Item("SH_NAME")

                Dim FTime As String = ds.Tables(0).Rows(0).Item("SH_FRM_HRS")
                Dim ToTime As String = ds.Tables(0).Rows(0).Item("SH_TO_HRS")
                Dim FtimeArray As String() = FTime.Split(":")
                Dim TtimeArray As String() = ToTime.Split(":")

                starttimehr.SelectedValue = FtimeArray(0)
                starttimemin.SelectedValue = FtimeArray(1)

                endtimehr.SelectedValue = TtimeArray(0)
                endtimemin.SelectedValue = TtimeArray(1)

                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("SH_STATUS")).Selected = True
                ddlShiftType.ClearSelection()
                ddlShiftType.Items.FindByValue(ds.Tables(0).Rows(0).Item("SH_SEAT_TYPE").ToString()).Selected = True
            End If
        End If
    End Sub

    Private Sub modifydata()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_MODIFY_SHIFT")
        sp1.Command.AddParameter("@SH_CODE", ddlBrand.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SH_NAME", txtBrandName.Text, DbType.String)
        sp1.Command.AddParameter("@SH_FRM_HRS", starttimehr.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SH_TO_HRS", endtimehr.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SH_FRM_MINS", starttimemin.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SH_TO_MINS", endtimemin.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SH_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp1.Command.AddParameter("@SH_STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp1.Command.AddParameter("@SH_SEAT_TYPE", ddlShiftType.SelectedItem.Value, DbType.Int32)
        sp1.ExecuteScalar()
        fillgrid()
        lblMsg.Visible = True
        lblMsg.Text = "Shift successfully modified"
    End Sub

    'Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Response.Redirect("~/Masters/Mas_Webfiles/frmMASMasters.aspx")
    'End Sub

    Private Sub GETCOUNTRY()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GETCOUNTRY")
        ddlCountry.DataSource = sp.GetDataSet()
        ddlCountry.DataTextField = "CNY_NAME"
        ddlCountry.DataValueField = "CNY_CODE"
        ddlCountry.DataBind()
        ddlCountry.Items.Insert(0, "--Select--")
    End Sub

    Private Sub GetSeatType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "getSeatType")
        ddlShiftType.DataSource = sp.GetDataSet()
        ddlShiftType.DataTextField = "SPACE_TYPE"
        ddlShiftType.DataValueField = "SNO"
        ddlShiftType.DataBind()
        ddlShiftType.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_BY_COUNTRY")
        sp.Command.AddParameter("@COUNTRY_ID", ddlCountry.SelectedItem.Value, DbType.String)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        'GETSHIFTS()
        GETSHIFTS_BY_LOCATION()
    End Sub

    Private Sub GETSHIFTS_BY_LOCATION()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SHIFTS_BY_LOCATION")
        sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        ddlBrand.Items.Clear()
        ddlBrand.DataSource = sp.GetDataSet()
        ddlBrand.DataTextField = "SH_NAME"
        ddlBrand.DataValueField = "SH_CODE"
        ddlBrand.DataBind()
        ddlBrand.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
End Class
