﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ScoreCardMaster.aspx.vb" Inherits="Masters_Mas_Webfiles_ScoreCardMaster" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div class="al-content">
        <div class="widgets">
            <h3>Score Card Masters</h3>
        </div>
        <div class="card">
            <form id="form1" runat="server">
                <div class="box-body">
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchIncentive/Views/BranchIncentiveCategory.aspx">Incentive Category Master</asp:HyperLink>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchIncentive/Views/BranchIncentiveTypeMaster.aspx">Incentive Amount Master</asp:HyperLink>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchIncentive/Views/BranchIncentiveLocationMaster.aspx">Location Incentive Master</asp:HyperLink>
                            </div>

                        </div>
                        <br>
                       <br>

                        <div class="row">
                             <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink4" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchIncentive/Views/BranchIncentiveType.aspx">Scorecard Main Category</asp:HyperLink>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink5" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchIncentive/Views/BranchIncentiveManCategory.aspx">Scorecard Sub Category</asp:HyperLink>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchIncentive/Views/BranchIncentiveSubCategory.aspx">Scorecard Child Category</asp:HyperLink>
                            </div>
                        </div>

                          <br>
                       <br>

                        <div class="row">
                              <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink7" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchIncentive/Views/BranchIncentvieAttendance.aspx">BIC Attendance Master</asp:HyperLink>
                            </div>
                        </div>

                    </div>
                </div>
                <br />
            </form>
        </div>
    </div>
</body>
</html>
