﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions

Partial Class Masters_Mas_Webfiles_frmEditVehicle
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            gvVehicleDetails.Visible = True
            BindGrid()
            lblMsg.Visible = False
        End If
    End Sub
    Private Sub bindgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_VEHICLE_DETAILS_EDIT")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvVehicleDetails.DataSource = ds
        gvVehicleDetails.DataBind()

    End Sub

    Protected Sub gvVehicleDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvVehicleDetails.PageIndexChanging
        gvVehicleDetails.PageIndex = e.NewPageIndex
        bindgrid()

    End Sub

    Protected Sub gvVehicleDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvVehicleDetails.RowCommand
        If e.CommandName = "DELETE" Then
            If gvVehicleDetails.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvVehicleDetails.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As String = lblID.Text
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DELETE_VEHICLE_DETAILS")
                sp.Command.AddParameter("@SNO", id, DbType.Int32)
                sp.ExecuteScalar()
            End If
        End If
        bindgrid()
    End Sub

  
    Protected Sub gvVehicleDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvVehicleDetails.RowDeleting

    End Sub
End Class
