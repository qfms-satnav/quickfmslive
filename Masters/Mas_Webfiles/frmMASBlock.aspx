<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASBlock.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASBlock" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Wing Master</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-bottom: 20px">
                            <div class="col-md-6">
                                <label class="col-md-2 btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                        ToolTip="Please Select Add to add new Wing and Select Modify to modify the existing Wing" />
                                    Add
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                        ToolTip="Please Select Add to add new Wing and Select Modify to modify the existing Wing" />
                                    Modify
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trLName" runat="server">
                                        <asp:Label ID="lblBlockname" runat="server" class="col-md-4 control-label">Select Wing<span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="rfvWing" runat="server"
                                            ErrorMessage="Please Select Wing " ControlToValidate="ddlBName" Display="None" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlBName" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Wing">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Wing Code <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server"
                                            ErrorMessage="Please Enter Wing Code " ControlToValidate="txtBlockcode" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtBlockcode"
                                            Display="None" ErrorMessage="Please enter Block code in alphanumerics" ValidationExpression="^[0-9a-zA-Z]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtBlockcode" MaxLength="15" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Wing Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server"
                                            ControlToValidate="txtBlockName" Display="None" ErrorMessage="Please Enter Wing Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtBlockName"
                                            Display="None" ErrorMessage="Please Enter Block Name in alphanumerics and (space,-,_ ,(,),/,\,, allowed)"
                                            ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/, allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtBlockName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Floor Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvFloor"
                                            runat="server" ControlToValidate="ddlFloor" Display="None" ErrorMessage="Please Select Floor Name "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlFloor" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Floor">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Tower Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTwr" runat="server" ControlToValidate="ddlTower"
                                            Display="None" ErrorMessage="Please Select Tower Name " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlTower" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Tower">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Location Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvLoc"
                                            runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location Name "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Location">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">City Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server"
                                            ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City Name"
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select City">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Country Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                            Display="None" ErrorMessage="Please Select Country Name " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select City">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-4 control-label">Remarks</label>
                                        <div class="col-md-8">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                    MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>


                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"  EmptyDataText="No Wing Found."
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:BoundField DataField="WNG_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Wing Name" />
                                        <asp:BoundField DataField="flr_name" ItemStyle-HorizontalAlign="Left" HeaderText="Floor Name" />
                                        <asp:BoundField DataField="twr_name" ItemStyle-HorizontalAlign="Left" HeaderText="Tower Name" />
                                        <asp:ButtonField HeaderText="Status" ItemStyle-HorizontalAlign="Left" CommandName="Status"
                                            Text="Button" />
                                        <asp:BoundField DataField="WNG_STA_ID" ItemStyle-HorizontalAlign="Left" HeaderText="STID"
                                            Visible="false" />
                                        <asp:BoundField DataField="WNG_CODE" ItemStyle-HorizontalAlign="Left" HeaderText="Block Code"
                                            Visible="false" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblFlrID" Text='<% #Bind("wng_flr_id")%>' Visible="false"></asp:Label>
                                                <asp:Label runat="server" ID="lblTwrID" Text='<% #Bind("wng_twr_id")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
