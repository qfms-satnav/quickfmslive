<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASDepartment.aspx.vb"
    Inherits="Masters_MAS_WebFiles_frmMASDepartment" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%-- <div class="widgets">
                <div ba-panel ba-panel-title="Department Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Department Master</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">--%>
            <div class="widgets">
                <h3>Department Master</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Style="padding-left: 45px;"> 
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-md-6 text-right">
                                    <label class="col-md-2 btn pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Tower and Select Modify to modify the existing Tower" />
                                        Add
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-2 btn pull-left">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Tower and Select Modify to modify the existing Tower" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="trCName" runat="server">
                                        <asp:Label ID="lblDepartment" runat="server" class="col-md-12 control-label">Select Department<span style="color: red;">*</span></asp:Label>
                                        <asp:RequiredFieldValidator ID="rfvDept"
                                            runat="server" ControlToValidate="ddlDepartment" Display="None" ErrorMessage="Please Select Department" ValidationGroup="Val1" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True"
                                                CssClass="form-control selectpicker" data-live-search="true"
                                                ToolTip="Select Department">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Department Code <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvDepCode"
                                            runat="server" ControlToValidate="txtDeptcode" Display="None" ErrorMessage="Please Enter Department Code" ValidationGroup="Val1"></asp:RequiredFieldValidator></font></td>
                                         <div class="col-md-12">
                                             <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                 onmouseout="UnTip()">
                                                 <asp:TextBox ID="txtDeptcode" runat="server" CssClass="form-control"
                                                     MaxLength="15"></asp:TextBox>
                                             </div>
                                         </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Department Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvDepName"
                                            runat="server" ControlToValidate="txtDeptname" Display="None" ErrorMessage="Please Enter Department Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtDeptname" runat="server" CssClass="form-control"
                                                    MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Remarks</label>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%"
                                                    TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12" for="txtcode"></label>
                                        <label class="col-md-12" for="txtcode"></label>
                                        <label class="col-md-12" for="txtcode"></label>
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Department Found."
                                        CssClass="table GridStyle" GridLines="none">
                                        <Columns>
                                            <asp:BoundField DataField="DEP_NAME" HeaderText="Department Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lnkStauts" Text='<% #Bind("Status")%>' AccessKey='<% #Bind("DEP_STA_ID")%>'
                                                        CommandArgument='<% #Bind("DEP_CODE")%>' OnClick="lnkStauts_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
        <%--   </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/ecmascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });

        function refreshSelectpicker() {
            $("#<%=ddlDepartment.ClientID%>").selectpicker();

        }
        refreshSelectpicker();

    </script>
</body>
</html>
