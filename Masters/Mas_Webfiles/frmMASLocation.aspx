<%@ Page EnableEventValidation="false" Language="VB" AutoEventWireup="false" CodeFile="frmMASLocation.aspx.vb" Inherits="Masters_MAS_WebFiles_frmLocation" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
    <script type="text/javascript" defer>
        //Function to allow only numbers to textbox
        function validate(key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            var pinc = document.getElementById('txtpincode');
            //comparing pressed keycodes
            if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
                return false;
            }
            else {
                //Condition to check textbox contains ten numbers or not
                if (pinc.value.length < 10) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Location Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Location Master</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">--%>
            <div class="widgets">
                <h3>Location Master</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <input type="hidden" runat="server" id="hdnAttrVal" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Style="padding-left: 45px;">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-md-6 text-right">
                                    <label class="col-md-2 btn pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Location and Select Modify to modify the existing Location" />
                                        Add
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-2 btn pull-left">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Location and Select Modify to modify the existing Location" />
                                        Modify
                                    </label>
                                </div>
                            </div>

                            <div class="row form-inline">
                                <div class="col-md-3 col-sm-12 col-xs-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row" id="trLName" runat="server">
                                                <asp:Label ID="lblLocationname" runat="server" class="col-md-12 control-label">Select Location <span style="color: red;">*</span></asp:Label>
                                                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlLocation"
                                                    Display="None" ErrorMessage="Please Select Location " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-12">
                                                    <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="form-control selectpicker" data-live-search="true"
                                                        ToolTip="Select Location">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Location Code <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtLocationcode"
                                            Display="None" ErrorMessage="Please Enter Location Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtLocationcode" MaxLength="25" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Location Name<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtLocationName"
                                            Display="None" ErrorMessage="Enter Location Name in alphabets and numbers and (space,-,_,(,),\,, allowed) and upto 50 characters allowed"
                                            ValidationExpression="^[0-9a-zA-Z-_\(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtLocationName"
                                            Display="None" ErrorMessage="Please Enter Location Name  " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Location Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtLocationName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">City Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvcty" runat="server"
                                            ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City Name"
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlCity" runat="server" Width="97%" AutoPostBack="True" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select City Name">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Country Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvcnt" runat="server" ControlToValidate="ddlCountry"
                                            Display="None" ErrorMessage="Please Select Country Name" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlCountry" runat="server" Enabled="true" Width="97%" AutoPostBack="True"
                                                CssClass="form-control selectpicker" data-live-search="true"
                                                ToolTip="Select Country Name">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-6" visible="true" runat="server">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvStatus" runat="server" ControlToValidate="cmbstatus" Display="None" ErrorMessage="Please Select Status  " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="cmbstatus" runat="server" Visible="True" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select Status">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">In-Active</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Pincode</label>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Numbers')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtpincode" MaxLength="15" runat="server" onkeypress="return validate(event)" CssClass="form-control"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- <div class="form-group col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Admins</label>                                           
                                            <div class="col-md-12">
                                                <asp:ListBox ID="ddlEmpids" runat="server" Height="50px" Width="97%" CssClass="Form-control selectpicker" data-live-search="true" SelectionMode="Multiple">
                                                    <%--<asp:ListItem Value="--Select--">--Select--</asp:ListItem>--%>
                                <%-- </asp:ListBox>
                                            </div>
                                        </div>
                                    </div>--%>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Remarks</label>
                                        <div class="col-md-12">
                                            <div onmouseout="UnTip()" onmouseover="Tip('Enter Remarks upto 500 characters')">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="500" Height="30%" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Address</label>
                                        <div class="col-md-12">
                                            <div onmouseout="UnTip()" onmouseover="Tip('Enter Remarks upto 500 characters')">
                                                <asp:TextBox ID="txtaddress" runat="server" CssClass="form-control" MaxLength="500" Height="30%" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%-- <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label"></label>
                                            <label class="col-md-12 control-label"></label>
                                            <label class="col-md-12 control-label"></label>
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Text="Back"></asp:Button>
                                             
                                            <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export To Excel" CausesValidation="False" />
                                             <asp:Button ID="btnPdf" runat="server" OnClick="btnPdf_Click" CssClass="btn btn-primary custom-button-color" Text="Export To PDF" CausesValidation="False" />
                                        </div>
                                    
                                    </div>--%>
                            </div>
                            <div class="row">

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Latitude</label>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Numbers')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtlat" runat="server" onkeypress="return validate(event)" CssClass="form-control"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Longitude</label>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Numbers')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtlong" runat="server" onkeypress="return validate(event)" CssClass="form-control"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="Div1" class="col-md-3 col-sm-3 col-xs-6" visible="true" runat="server">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Zone</label>
                                        <%--  <asp:RequiredFieldValidator ID="rfvzone" runat="server" ControlToValidate="cmbZone" Display="None" ErrorMessage="Please Select Zone" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="cmbZone" runat="server" Visible="True" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select Zone">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="East">East</asp:ListItem>
                                                <asp:ListItem Value="West">West</asp:ListItem>
                                                <asp:ListItem Value="North">North</asp:ListItem>
                                                <asp:ListItem Value="South">South</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Text="Back"></asp:Button>
                                        <asp:Button ID="btnExport" runat="server" Visible="false" CssClass="btn btn-primary custom-button-color" Text="Export To Excel" CausesValidation="False" />
                                        <%--<asp:Button ID="btnPdf" runat="server" OnClick="btnPdf_Click" CssClass="btn btn-primary custom-button-color" Text="Export To PDF" CausesValidation="False" />--%>
                                        <a href="/Location_Data/Location_Data.xls" runat="server" download class="btn btn-primary custom-button-color excel-file-download-a-tag">Export To Excel</a>

                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Location Found."
                                        CssClass="table GridStyle" GridLines="none">
                                        <Columns>
                                            <asp:BoundField DataField="LCM_NAME" HeaderText="Location Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cty_name" HeaderText="City Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                             <asp:BoundField DataField="LAT" HeaderText="Lat">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LONG" HeaderText="Long">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="cny_name" HeaderText="Country Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_STATUS" HeaderText="Location Status" Visible="false">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_TENDATE" HeaderText="Date" Visible="false">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_STATUS" HeaderText="Status">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_STATUS" HeaderText="STID">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_CODE" HeaderText="Location Code">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_ADDR" HeaderText="Location Address">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 hidden">
                                    <asp:GridView ID="ExportGrid" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Location Found."
                                        CssClass="table GridStyle" GridLines="none">
                                        <Columns>
                                            <asp:BoundField DataField="CNY_NAME" HeaderText="Country Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CTY_NAME" HeaderText="City Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_CODE" HeaderText="Location Code">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_NAME" HeaderText="Location Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TWR_NAME" HeaderText="Tower Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FLR_NAME" HeaderText="Floor Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FLR_AREA" HeaderText="Floor Area">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="LCM_ADDR" HeaderText="Address">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_PINCODE" HeaderText="Pincode">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LAT" HeaderText="Latitude">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LONG" HeaderText="Longitude">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCMSTAID" HeaderText="Status">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Admin_Spoc_ID" HeaderText="Admin Spoc ID">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Admin_Spoc_Name" HeaderText="Admin Spoc Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Admin_Spoc_Number" HeaderText="Admin Spoc Number">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Location_Spoc_ID" HeaderText="Location Spoc ID">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Location_Spoc_Name" HeaderText="Location Spoc Name">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Location_Spoc_Number" HeaderText="Location Spoc Number">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LCM_ZONE" HeaderText="Zone">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnExport" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/ecmascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });
    </script>
    <script type="text/ecmascript" >
        function refreshSelectpicker() {
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlCity.ClientID%>").selectpicker();
            $("#<%=ddlCountry.ClientID%>").selectpicker();
            $("#<%=cmbstatus.ClientID%>").selectpicker();
        }
        refreshSelectpicker();

        $(window).on('load', function () {
            var attrval = $('#hdnAttrVal').val();
            $('.excel-file-download-a-tag').attr('href', attrval);
        })
    </script>
</body>

</html>
