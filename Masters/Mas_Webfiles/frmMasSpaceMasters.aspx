﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmMasSpaceMasters.aspx.vb" Inherits="Masters_frmMasSpaceMasters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/assets/css/ag-grid-theme-blue-override.css" rel="stylesheet" />






    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
        function noway(go) {
            if (document.all) {
                if (event.button == 2) {
                    alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                    return false;
                }
            }
            if (document.layers) {
                if (go.which == 3) {
                    alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = noway;
    </script>

</head>
<body>
    <div class="al-content">
        <div class="widgets">
            <h3>Space Masters</h3>
        </div>
        <div class="card">
            <form id="form1" runat="server">
                <div class="row mb-3">
                    <div class="col-md-6 col-sm-12 col-xs-12" id="CountryHLdiv" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/WorkSpace/SMS_Webfiles/frmSpaceTypeMaster.aspx">Space Type Master</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div1" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/WorkSpace/SMS_Webfiles/SpaceSubTypeMaster.aspx">Space Sub Type Master</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div2" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl="~/workspace/sms_webfiles/Vertical_Escalation.aspx">
                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                </asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div3" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Workspace/sms_Webfiles/space_matrix.aspx">Space Matrix</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div4" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASShift.aspx">Shift Master</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div5" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink18" runat="server" NavigateUrl="~/SMViews/Masters/UploadSpaceAllocation.aspx">Upload Space Allocation Data</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div6" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Masters/MAS_Webfiles/frmMASspacetypeGradeMapping.aspx">Space Type Grade Master</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div7" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Workspace/sms_Webfiles/SpaceTypeCost.aspx">Space Type Wise Cost</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row mb-3">
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div8" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/SMViews/Masters/SpaceDateBookingRestriction.aspx">Space Booking Restriction</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12" id="Div9" runat="server">
                        <div class="btn btn-primary btn-block" style="position: relative;">
                            <div class="button-link">
                                <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/SMViews/Masters/aminitiesMaster.aspx">Amenities Master</asp:HyperLink>
                            </div>
                            <div style="position: absolute; top: 6px; right: 9px;">
                                <a href="javascript:void(0)"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
           <%-- <script src="../../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>
            <script src="../../BootStrapCSS/Scripts/bootstrap-datepicker.min.js"></script>
            <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
            <script src="../../BootStrapCSS/Scripts/ObjectKeys.min.js"></script>
            <script src="../../BootStrapCSS/Scripts/wz_tooltip.min.js"></script>
            <script src="../../BootStrapCSS/Scripts/modernizr.min.js"></script>
            <script src="../../Scripts/angular.min.js"></script>
            <script src="../../Scripts/alasql.min.js"></script>
            <script src="../../Scripts/xlsx.core.min.js"></script>
            <script src="../../Scripts/aggrid/ag-grid.min.js"></script>
            <script src="../../Scripts/angular-resource.min.js"></script>
            <script src="../../BootStrapCSS/Messenger/heartcode-canvasloader.js"></script>
            <script src="../../BootStrapCSS/Messenger/messenger.js"></script>--%>
                <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
