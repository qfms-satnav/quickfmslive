Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Masters_Mas_Webfiles_MyVehicles
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindGridClaimedVehicles()
            BindGridUnclaimedVehicles()

        End If
    End Sub
    Private Sub BindGridUnclaimedVehicles()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"get_unclaimedvehicles")
        sp.Command.AddParameter("@emp_id", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvUnClaimedVehicleDetails.DataSource = ds
        gvUnClaimedVehicleDetails.DataBind()

    End Sub

    Protected Sub gvUnClaimedVehicleDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUnClaimedVehicleDetails.PageIndexChanging
        gvUnClaimedVehicleDetails.PageIndex = e.NewPageIndex
        BindGridUnclaimedVehicles()

    End Sub

    Protected Sub gvUnClaimedVehicleDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvUnClaimedVehicleDetails.RowCommand
        If e.CommandName = "Claim" Then
            If gvUnClaimedVehicleDetails.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                'Dim lblID As Label = DirectCast(gvUnClaimedVehicleDetails.Rows(rowIndex).FindControl("lblID"), Label)
                '   Dim id As String = rowIndex
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VehicleUnclaimedStatusUpdate")
                sp.Command.AddParameter("@SNO", rowIndex, DbType.Int32)
                sp.ExecuteScalar()

            End If
        End If
        If e.CommandName = "DELETE" Then
            If gvUnClaimedVehicleDetails.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvUnClaimedVehicleDetails.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As String = lblID.Text
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DELETE_VEHICLE_DETAILS")
                sp.Command.AddParameter("@SNO", id, DbType.Int32)
                sp.ExecuteScalar()
            End If
        End If
        BindGridUnclaimedVehicles()
        BindGridClaimedVehicles()
    End Sub


    Protected Sub gvUnClaimedVehicleDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvUnClaimedVehicleDetails.RowDeleting

    End Sub


    Private Sub BindGridClaimedVehicles()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"get_claimedvehicles")
        sp.Command.AddParameter("@emp_id", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvClaimedVehicles.DataSource = ds
        gvClaimedVehicles.DataBind()

    End Sub

    Protected Sub gvClaimedVehicles_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvClaimedVehicles.PageIndexChanging
        gvClaimedVehicles.PageIndex = e.NewPageIndex
        BindGridClaimedVehicles()

    End Sub

    Protected Sub gvClaimedVehicles_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvClaimedVehicles.RowCommand
        If e.CommandName = "DELETE" Then
            If gvClaimedVehicles.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvClaimedVehicles.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As String = lblID.Text
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DELETE_VEHICLE_DETAILS")
                sp.Command.AddParameter("@SNO", id, DbType.Int32)
                sp.ExecuteScalar()
            End If
        End If
        BindGridClaimedVehicles()
    End Sub


    Protected Sub gvClaimedVehicles_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvClaimedVehicles.RowDeleting

    End Sub
End Class
