﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmMasUploadHRMSData.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasUploadHRMSData" %>

<%@ Register Src="../../Controls/UploadHRMSData.ascx" TagName="UploadHRMSData" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Upload HRMS Data" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Upload HRMS Data</h3>
                        </div>
                        <div class="card">
                        <div class="card-body" style="padding-right: 50px;">--%>
            <div class="widgets">
                <h3>Upload HRMS Data</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                    <uc1:UploadHRMSData ID="UploadHRMSData1" runat="server" />
                </form>
            </div>
        </div>
        <%-- </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


