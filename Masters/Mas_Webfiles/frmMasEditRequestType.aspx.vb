Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Masters_Mas_Webfiles_frmMasEditRequestType
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim validatecode As Integer
            validatecode = validaterequestttype()
            If validatecode = 0 Then
                lblMsg.Text = "RequestType Already Exists please enter another RequestType"
                lblMsg.Visible = True
            Else
                UpdateRecord()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function validaterequestttype()
        Dim validatecode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALLIDATE_REQUEST_TYPE1")
        sp.Command.AddParameter("@REQUEST_TYPE", txtReqType.Text, DbType.String)
        sp.Command.AddParameter("@SNO", Request.QueryString("id"), DbType.Int32)
        validatecode = sp.ExecuteScalar()
        Return validatecode
    End Function
    Private Sub UpdateRecord()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_PN_MAINTENANCE_REQUEST_TYPE")
            sp.Command.AddParameter("@SNO", Request.QueryString("id"), DbType.Int32)
            sp.Command.AddParameter("@REQTYPE", txtReqType.Text, DbType.String)
            sp.Command.AddParameter("@STA_ID", ddlstatus.SelectedItem.Value, DbType.Int32)
            sp.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=34")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDetails()
            lblMsg.Visible = False
        End If
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMasViewRequestType.aspx")
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PN_MAINTENANCE_REQUEST_TYPE1")
            sp.Command.AddParameter("@REQTYPE", Request.QueryString("Type"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtReqType.Text = ds.Tables(0).Rows(0).Item("REQUEST_TYPE")
                Dim stat As Integer = ds.Tables(0).Rows(0).Item("STA_ID")
                If stat = 0 Then
                    ddlstatus.SelectedValue = 0
                Else
                    ddlstatus.SelectedValue = 1
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
