﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmMASspacetypeGradeMapping.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASspacetypeGradeMapping" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function text_changed() {
            searchedword = document.getElementById("ContentPlaceHolder1_search").value;
            SendRequest();
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Space Type Grade Mapping</h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                        ForeColor="red" ValidationGroup="Val1" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="False">
                                    </asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-md-6 text-right">
                            <label class="col-md-2 btn pull-right">
                                <asp:RadioButton value="0" runat="server" name="grades" ID="space_grade" GroupName="grades" AutoPostBack="true" Checked="true"
                                    ToolTip="Please Select Add to space type grade map" />
                                Space Wise</label>
                        </div>
                        <div class="col-md-3">
                            <label class="col-md-2 btn pull-left">
                                <asp:RadioButton value="1" runat="server" name="grades" ID="Role_grade" GroupName="grades" AutoPostBack="true"
                                    ToolTip="Please Select Add employee role wise grade map" />
                                Grade Wise
                            </label>
                        </div>
                    </div>

                    <div class="row" id="SpaceGrade" runat="server">
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Space Type <span style="color: red;">*</span></label>
                                <asp:RequiredFieldValidator ID="rfvcty" runat="server"
                                    ControlToValidate="ddlSpaceType" Display="None" ErrorMessage="Please Select Space Type"
                                    InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <div class="col-md-12">
                                    <asp:DropDownList ID="ddlSpaceType" runat="server" Width="97%" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Grade(s) <span style='color: red;'>*</span></label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="ddlfromGrade" Display="None" ErrorMessage="Please Select a Grade"
                                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <div class="col-md-12">
                                    <asp:ListBox ID="ddlfromGrade" runat="server" Height="50px" Width="97%" CssClass="Form-control" SelectionMode="Multiple">
                                        <%--<asp:ListItem Value="--Select--">--Select--</asp:ListItem>--%>
                                    </asp:ListBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Status<span style="color: red;">*</span></label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="--Select--" ErrorMessage="Select Status" ControlToValidate="ddlstatus" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <div class="col-md-12">
                                    <asp:DropDownList ID="ddlstatus" runat="server" Width="97%" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="--Select--" Text="--Select--"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="In Active"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="RoleGrade" runat="server">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Grades<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="ddlGradeValidator" runat="server"
                                        ControlToValidate="ddlgrades" Display="None" ErrorMessage="Please Select Employee Grade"
                                        ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class="col-md-12">
                                        <%--<asp:DropDownList ID="ddlgrades" runat="server" Width="97%" CssClass="form-control selectpicker" data-live-search="true">
                                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                    </asp:DropDownList>--%>
                                        <asp:ListBox SelectionMode="Multiple" ID="ddlgrades" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                            <%-- <asp:ListItem Value="--Select--" Selected>--Select--</asp:ListItem>--%>
                                            <%-- <asp:ListItem Value="12">12</asp:ListItem>
                                                        <asp:ListItem Value="13">13</asp:ListItem>
                                                        <asp:ListItem Value="14">14</asp:ListItem>
                                                        <asp:ListItem Value="15">15</asp:ListItem>
                                                        <asp:ListItem Value="16">16</asp:ListItem>
                                                        <asp:ListItem Value="17">17</asp:ListItem>
                                                        <asp:ListItem Value="18">18</asp:ListItem>--%>
                                        </asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Total Count<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="countValidator" runat="server" ControlToValidate="totalcount"
                                        Display="none" ErrorMessage="Please Enter Count" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="TypeCheck" runat="server"
                                        ControlToValidate="totalcount" Display="None" ErrorMessage="Only numeric allowed in Count."
                                        ValidationExpression="^[0-9]*$" ValidationGroup="Val1">
                                    </asp:RegularExpressionValidator>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="totalcount" runat="server" CssClass="form-control" autocomplete="off" placeholder="Enter Allocation Count"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear"></asp:Button>
                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/Masters/Mas_Webfiles/frmMasSpaceMasters.aspx"></asp:Button>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div id="SpaceGrid" runat="server">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                    Width="100%" PageSize="10" CssClass="table GridStyle" GridLines="None" EmptyDataText="No Data Found..">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Space Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCTY_NAME" runat="server" Text='<%#Eval("SGM_LAYER")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Grade">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGrdfrm" runat="server" Text='<%#Eval("SGM_GRADE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("SGM_STA_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" CausesValidation="false" CommandArgument='<%#Eval("SPC_TYPE_CODE")%>'
                                                    CommandName="Edit" runat="server">Edit</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div id="GradeGrid" runat="server">
                            <div class="col-md-12">
                                <asp:GridView ID="GradesWise" runat="server" AutoGenerateColumns="False" AllowPaging="True" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                    Width="100%" PageSize="10" CssClass="table GridStyle" GridLines="None" EmptyDataText="No Data Found..">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Employee Grade">
                                            <ItemTemplate>
                                                <asp:Label ID="Employee_Grade" runat="server" Text='<%#Eval("GRD_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allocation Count">
                                            <ItemTemplate>
                                                <asp:Label ID="Allocation_Count" runat="server" Text='<%#Eval("Grd_Count")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" CausesValidation="false" CommandArgument='<%#Eval("SPC_TYPE_CODE")%>'
                                                                CommandName="Edit" runat="server">Edit</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function ShowHelpWindow() {
            window.open('frmPrjHead.aspx', 'Window', 'toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450');
            return false;
        }
    </script>
</body>
</html>


