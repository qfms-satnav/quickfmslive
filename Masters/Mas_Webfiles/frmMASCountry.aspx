<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASCountry.aspx.vb" ValidateRequest="true"
    Inherits="Masters_MAS_WebFiles_frmCountry" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Country Master</h3>
            </div>
            <div class="card" style="padding-right: 50px;">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12" ForeColor="Red" Style="padding-left: 45px;">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 20px">
                                <div class="col-md-6 text-right">
                                    <label class="col-md-2 btn pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                        Add</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-2 btn pull-left">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                            <div class="row form-inline">
                                <div class="form-group col-sm-12 col-xs-6">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="row" id="trCName" runat="server">
                                                <label class="col-md-12" for="txtcode">Select Country <span style="color: red;">*</span></label>

                                                <asp:RequiredFieldValidator ID="rfvctry" runat="server" ErrorMessage="Please Select Country "
                                                    InitialValue="--Select--" ControlToValidate="ddlCN" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-11">
                                                    <asp:DropDownList ID="ddlCN" runat="server" AutoPostBack="True" BorderColor="White" CssClass="form-control selectpicker" data-live-search="true"
                                                        ToolTip="Select Country Name">
                                                        <asp:ListItem Value="">--Select--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12" for="txtcode">Country Code<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="revCode" runat="server" ControlToValidate="txtCCode"
                                            Display="None" ErrorMessage="Please enter country code in alphabets and numbers, upto 15 characters allowed"
                                            ValidationExpression="^[A-Za-z0-9]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvcode" runat="server" ControlToValidate="txtCCode"
                                            Display="None" ErrorMessage="Please Enter Country Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtCCode" runat="server" MaxLength="15" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12" for="txtcode">Country Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtCName"
                                            Display="None" ErrorMessage="Please Enter Country Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="txtCName"
                                            Display="None" ErrorMessage="Please Enter Country Name in alphabets and numbers and (space,-,_ ,(,) allowed), upto 50 characters allowed"
                                            ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Country Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtCName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label class="col-md-12" for="txtcode">Remarks</label>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" MaxLength="500"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Text="Back"></asp:Button>
                                    </div>

                                </div>
                            </div>
                            <br />
                            <div class="row ">
                                <div class="form-group col-md-6">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Country Found."
                                        CssClass="table GridStyle" GridLines="None" PageSize="5">
                                        <Columns>
                                            <asp:BoundField DataField="CNY_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Country Name">
                                                <ItemStyle HorizontalAlign="Center" ForeColor="Black" />
                                            </asp:BoundField>
                                            <asp:ButtonField HeaderText="Status" ItemStyle-HorizontalAlign="Left" CommandName="Status"
                                                Text="Button"></asp:ButtonField>
                                            <asp:BoundField DataField="CNY_STA_ID" ItemStyle-HorizontalAlign="Left" HeaderText="STID" HeaderStyle-ForeColor="#000099">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CNY_CODE" ItemStyle-HorizontalAlign="Left" HeaderText="Country Code">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/ecmascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });

        function refreshSelectpicker() {
            $("#<%=ddlCN.ClientID%>").selectpicker();

        }
        refreshSelectpicker();
    </script>
</body>
</html>
