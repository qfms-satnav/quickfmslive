Imports System.Data
Partial Class Masters_Mas_Webfiles_frmMasSpaceType
    Inherits System.Web.UI.Page
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Page.IsValid = True Then
            Dim validSpaceType As Integer
            validSpaceType = ValidateSpaceType()
            If validSpaceType = 0 Then
                lblmsg.Text = "Space Type Code already Exists"
            Else
                AddSpacetype()
                BindGrid()
                Cleardata()
                lblmsg.Text = "New Space Type Added Succesfully"
            End If
        Else
            lblmsg.Text = "Please Enter all Mandatory Fields"
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Uid") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then
            BindGrid()
            btnSubmit.Visible = True
            btnModify.Visible = False
            txtspctype.ReadOnly = False
        End If
    End Sub
    
    Private Function ValidateSpaceType()
        Dim Validate As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_SPACE_TYPE")
        sp.Command.AddParameter("@SPC_CODE", txtspctype.Text, DbType.String)
        Validate = sp.ExecuteScalar()
        Return Validate
    End Function
    Private Sub AddSpacetype()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SPACE_TYPE")
        sp.Command.AddParameter("@SPC_CODE", txtspctype.Text, DbType.String)
        sp.Command.AddParameter("@SPC_NAME", txtspctypeName.Text, DbType.String)
        sp.Command.AddParameter("@SPC_STATUS", ddlstatus.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../../workspace/sms_webfiles/helpmasters.aspx")
    End Sub
    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_TYPE_DETAILS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
        For i As Integer = 0 To gvitems.Rows.Count - 1
            Dim lblspcnstatus As Label = CType(gvitems.Rows(i).FindControl("lblspcnstatus"), Label)
            If lblspcnstatus.Text = 1 Then
                lblspcnstatus.Text = "Active"
            Else
                lblspcnstatus.Text = "Inactive"
            End If
        Next
    End Sub
    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "Edit" Then

            txtspctype.ReadOnly = True
            btnSubmit.Visible = False
            btnModify.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblspccode As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblspccode"), Label)
            Dim lblspcname As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblspcname"), Label)
            Dim lblspcstat As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblspcstat"), Label)
            txtspctype.Text = lblspccode.Text
            txtspctypeName.Text = lblspcname.Text
            ddlstatus.ClearSelection()
            ddlstatus.Items.FindByValue(lblspcstat.Text).Selected = True
        ElseIf e.CommandName = "Delete" Then
            Cleardata()
            txtspctype.ReadOnly = False
            btnSubmit.Visible = True
            btnModify.Visible = False
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblspccode As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblspccode"), Label)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Delete_Space_Type")
            sp.Command.AddParameter("@Spc_Code", lblspccode.Text, DbType.String)
            sp.ExecuteScalar()
            BindGrid()
        End If

    End Sub
    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        If Page.IsValid = True Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_SPACE_TYPE")
            sp.Command.AddParameter("@SPC_CODE", txtspctype.Text, DbType.String)
            sp.Command.AddParameter("@SPC_NAME", txtspctypeName.Text, DbType.String)
            sp.Command.AddParameter("@SPC_STATUS", ddlstatus.SelectedItem.Value, DbType.String)
            sp.ExecuteScalar()
            lblmsg.Text = "Space type Record Modified Succesfully"
            btnSubmit.Visible = True
            btnModify.Visible = False
            BindGrid()
            Cleardata()
        End If
    End Sub
    Private Sub Cleardata()
        txtspctype.Text = ""
        txtspctypeName.Text = ""
        ddlstatus.SelectedIndex = 0
    End Sub

    Protected Sub gvitems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvitems.RowDeleting

    End Sub

    Protected Sub gvitems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub
End Class
