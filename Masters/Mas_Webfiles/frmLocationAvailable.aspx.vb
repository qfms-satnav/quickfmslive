Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Partial Class Masters_Mas_Webfiles_frmLocationAvailable
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If ddllocation.SelectedIndex > 0 And txtavailable_wt.Text <> "" And txtOccupied_wt.Text <> "" And txtavailable_wi.Text <> "" And txtOccupied_wi.Text <> "" And txtavailable_wb.Text <> "" And txtOccupied_wb.Text <> "" And txtavailable_wstl.Text <> "" And txtOccupied_wstl.Text <> "" Then

                Dim param(8) As SqlParameter
                param(0) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
                param(0).Value = ddllocation.SelectedItem.Value
                param(1) = New SqlParameter("@available_wt", SqlDbType.Int)
                param(1).Value = txtavailable_wt.Text
                param(2) = New SqlParameter("@Occupied_wt", SqlDbType.Int)
                param(2).Value = txtOccupied_wt.Text
                param(3) = New SqlParameter("@available_wi", SqlDbType.Int)
                param(3).Value = txtavailable_wi.Text
                param(4) = New SqlParameter("@Occupied_wi", SqlDbType.Int)
                param(4).Value = txtOccupied_wi.Text
                param(5) = New SqlParameter("@available_wb", SqlDbType.Int)
                param(5).Value = txtavailable_wb.Text
                param(6) = New SqlParameter("@Occupied_wb", SqlDbType.Int)
                param(6).Value = txtOccupied_wb.Text
                param(7) = New SqlParameter("@available_wstl", SqlDbType.Int)
                param(7).Value = txtavailable_wstl.Text
                param(8) = New SqlParameter("@Occupied_wstl", SqlDbType.Int)
                param(8).Value = txtOccupied_wstl.Text
                ObjSubSonic.GetSubSonicExecute("MODIFY_LOCATION_AVAILABLE_OCCUPIED", param)
                lblMsg.Text = "Location Details modified succesfully"
                'Dim validLocation As Integer
                'validLocation = ValidateLocation()
                'If validLocation = 0 Then
                '    ObjSubSonic.GetSubSonicExecute("MODIFY_LOCATION_AVAILABLE_OCCUPIED", param)
                '    lblMsg.Text = "Location Details modified succesfully"
                'ElseIf validLocation = 1 Then
                '    ObjSubSonic.GetSubSonicExecute("ADD_LOCATION_AVAILABLE_OCCUPIED", param)
                '    lblMsg.Text = "Location Details added succesfully"
                'End If
                ClearData()
                BindGrid()
                BindCity()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateLocation()
        Dim validLoc As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LOCATION_AVAILABLE_OCCUPIED")
        sp.Command.AddParameter("@LOCATION", ddllocation.SelectedItem.Value, DbType.String)
        validLoc = sp.ExecuteScalar()
        Return validLoc
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            BindCity()
            BindLocation()
            BindGrid()
        End If
    End Sub
    Private Sub BindCity()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATIONS_CITY")
            ddlcity.DataSource = sp.GetDataSet()
            ddlcity.DataTextField = "CTY_NAME"
            ddlcity.DataValueField = "CTY_CODE"
            ddlcity.DataBind()
            ddlcity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_LOCATIONS_AVAILABLE_OCCUPIED_BIND")
            sp.Command.AddParameter("@LOCATION", "", DbType.String)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_BIND_AVAILABLE_LOCATION")
            sp.Command.AddParameter("@CITY", ddlcity.SelectedItem.Value, DbType.String)
            ddllocation.DataSource = sp.GetDataSet()
            ddllocation.DataTextField = "LCM_NAME"
            ddllocation.DataValueField = "LCM_CODE"
            ddllocation.DataBind()
            ddllocation.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ClearData()
        ddllocation.Items.Clear()
        ddllocation.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        ddllocation.SelectedIndex = 0
        ddlcity.SelectedIndex = -1
        txtavailable_wb.Text = ""
        txtavailable_wi.Text = ""
        txtavailable_wstl.Text = ""
        txtavailable_wt.Text = ""
        txtOccupied_wb.Text = ""
        txtOccupied_wi.Text = ""
        txtOccupied_wstl.Text = ""
        txtOccupied_wt.Text = ""
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblloc As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblloc"), Label)
            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DEL_LOCATION_AVAILABLE_OCCUPIED")
            SP.Command.AddParameter("@LOCATION", lblloc.Text, DbType.String)
            SP.ExecuteScalar()
        ElseIf e.CommandName = "EDIT" Then
            ddllocation.Items.Clear()
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblloc As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblloc"), Label)
            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_AVAILABLE_OCCUPIED")
            SP.Command.AddParameter("@LOCATION", lblloc.Text, DbType.String)
            Dim ds As New DataSet()
            ds = SP.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                ddllocation.Items.Insert(0, New ListItem((ds.Tables(0).Rows(0).Item("LOCATION_NAME")), (ds.Tables(0).Rows(0).Item("LOCATION_CODE"))))
                txtavailable_wb.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WBPO")
                txtavailable_wi.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WI")
                txtavailable_wstl.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WSTL")
                txtavailable_wt.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WT")
                txtOccupied_wb.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WBPO")
                txtOccupied_wi.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WI")
                txtOccupied_wstl.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WSTL")
                txtOccupied_wt.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WT")

            End If
        End If
    End Sub

    Protected Sub gvitems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvitems.RowDeleting

    End Sub


    Protected Sub gvitems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcity.SelectedIndexChanged
        Try
            If ddlcity.SelectedIndex > 0 Then
                BindLocation()
            Else
                ddllocation.Items.Clear()
                ddllocation.Items.Insert(0, New ListItem("--Select--", "--Select--"))
                ddllocation.SelectedIndex = 0
            End If
            txtavailable_wb.Text = ""
            txtavailable_wi.Text = ""
            txtavailable_wstl.Text = ""
            txtavailable_wt.Text = ""
            txtOccupied_wb.Text = ""
            txtOccupied_wi.Text = ""
            txtOccupied_wstl.Text = ""
            txtOccupied_wt.Text = ""
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddllocation.SelectedIndexChanged
        Try

            If ddllocation.SelectedIndex > 0 Then
                Dim validLocation As Integer
                validLocation = ValidateLocation()
                If validLocation = 0 Then
                    Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_AVAILABLE_OCCUPIED")
                    SP.Command.AddParameter("@LOCATION", ddllocation.SelectedItem.Value, DbType.String)
                    Dim ds As New DataSet()
                    ds = SP.GetDataSet()
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddllocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("LOCATION_CODE"))
                        ' ddllocation.Items.Insert(0, New ListItem((ds.Tables(0).Rows(0).Item("LOCATION_NAME")), (ds.Tables(0).Rows(0).Item("LOCATION_CODE"))))
                        txtavailable_wb.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WBPO")
                        txtavailable_wi.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WI")
                        txtavailable_wstl.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WSTL")
                        txtavailable_wt.Text = ds.Tables(0).Rows(0).Item("AVAILABLE_WT")
                        txtOccupied_wb.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WBPO")
                        txtOccupied_wi.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WI")
                        txtOccupied_wstl.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WSTL")
                        txtOccupied_wt.Text = ds.Tables(0).Rows(0).Item("OCCUPIED_WT")
                    End If
                Else
                    txtavailable_wb.Text = 0
                    txtavailable_wi.Text = 0
                    txtavailable_wstl.Text = 0
                    txtavailable_wt.Text = 0
                    txtOccupied_wb.Text = 0
                    txtOccupied_wi.Text = 0
                    txtOccupied_wstl.Text = 0
                    txtOccupied_wt.Text = 0
                End If
            Else
                txtavailable_wb.Text = ""
                txtavailable_wi.Text = ""
                txtavailable_wstl.Text = ""
                txtavailable_wt.Text = ""
                txtOccupied_wb.Text = ""
                txtOccupied_wi.Text = ""
                txtOccupied_wstl.Text = ""
                txtOccupied_wt.Text = ""
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
