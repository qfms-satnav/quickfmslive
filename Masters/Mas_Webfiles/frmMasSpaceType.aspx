<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMasSpaceType.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasSpaceType"
    Title="Space Type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Space Type Master
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Space Type Master</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label id="lblmsg" runat="server" cssclass="clsMessage"></asp:Label>
                    <table id="table4" cellspacing="0" cellpadding="0" width="100%" border="1">
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Enter Space Type Code <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvCmpType" runat="server" ControlToValidate="txtspctype"
                                    Display="None" ErrorMessage="Please Enter Space type Code" ValidationGroup="Val1">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:TextBox id="txtspctype" runat="server" cssclass="clsTextField" width="97%" TabIndex="1">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Enter Space Type Name<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvspaceName" runat="server" ControlToValidate="txtspctypeName"
                                    Display="None" ErrorMessage="Please Enter Space type Name " ValidationGroup="Val1">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:TextBox id="txtspctypeName" runat="server" cssclass="clsTextField" width="97%"
                                    TabIndex="2">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Select Status<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvspcstatus" runat="server" ControlToValidate="ddlstatus"
                                    Display="None" ErrorMessage="Please Enter Space type Status " ValidationGroup="Val1"
                                    InitialValue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList id="ddlstatus" runat="Server" cssclass="clsComboBox" width="99%">
                                    <asp:Listitem Value="--Select--">--Select--</asp:Listitem>
                                    <asp:Listitem Value="1">Active</asp:Listitem>
                                    <asp:Listitem Value="0">Inactive</asp:Listitem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="Center" colspan="2" style="height: 26px">
                                <asp:Button id="btnSubmit" runat="server" Cssclass="button" Text="Submit" CausesValidation="true"
                                    ValidationGroup="Val1" OnClick="btnSubmit_Click" />
                                    <asp:Button id="btnModify" runat="server" Cssclass="button" Text="Modify" CausesValidation="true"
                                    ValidationGroup="Val1" />
                                <asp:Button id="btnBack" runat="Server" Cssclass="button" Text="Back" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel id="pnlitems" runat="server" width="100%">
                        <asp:GridView id="gvitems" runat="server" AutoGenerateColumns="false" Allowpaging="True"
                            Allowsorting="True" Width="100%">
                            <columns>
       <asp:TemplateField HeaderText="Space Type Code">
       <ItemTemplate>
       <asp:Label id="lblspccode" runat="server" Text='<%#Eval("space_layer") %>'></asp:Label>
        
       </ItemTemplate>
       </asp:TemplateField>
       
       <asp:TemplateField HeaderText="Space Type Name">
       <ItemTemplate>
       <asp:Label id="lblspcname" runat="server" Text='<%#Eval("space_type") %>'></asp:Label>
       </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField HeaderText="Space Type Name">
       <ItemTemplate>
       <asp:Label id="lblspcnstatus" runat="server" Text='<%#Eval("spc_type_status") %>'></asp:Label>
       </ItemTemplate>
       </asp:TemplateField>
       <asp:TemplateField Visible="False">
       <ItemTemplate>
       <asp:Label id="lblspcstat" runat="server" Text='<%#Eval("spc_type_status") %>'></asp:Label>
       </ItemTemplate>
       </asp:TemplateField>
       <asp:ButtonField Text="Edit" CommandName="Edit" />
       <asp:ButtonField Text="Delete" CommandName="Delete" />
       </columns>
                        </asp:GridView>
                    </asp:Panel>
                </td>
                <td background="../../Images/table_right_mid_bg.gif">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
