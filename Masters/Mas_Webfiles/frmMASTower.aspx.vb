Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Masters_Mas_Webfiles_frmMASTower
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters
    Dim code, name, remarks, VRM, Vertical As String
    Dim Capacity As Integer ' added by praveen
    Dim parent_entity, child_entity As String 'added by prasanna
    Public Sub Cleardata()
        txtTowercode.Text = String.Empty
        txtTowerName.Text = String.Empty
        ddlCityname.SelectedIndex = 0
        ddlCountryName.SelectedIndex = 0
        ddlLName.SelectedIndex = -1
        ddlTName.SelectedIndex = -1
        txtRemarks.Text = String.Empty
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnSubmit)
        End If
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Try


            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using
            'rfvcity.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            'RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
            'rfvStatus.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            'RegExpNumber.ValidationExpression = User_Validation.GetValidationExpressionForPhone.VAL_EXPR()
            'If Session("uid") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                obj.Tower_LoadGrid(gvItem)
                trLName.Visible = False
                rbActions.Checked = True
                obj.BindCity(ddlCityname)
                obj.BindCountry(ddlCountryName)
                obj.Bindlocation(ddlLName)

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "Page_Load", exp)
        End Try
    End Sub

    Private Sub Modifydata()
        Dim projectPath As String = Server.MapPath("~/")
        Dim folderName As String = Path.Combine(projectPath, "UploadFiles\" & Session("TENANT"))
        If Not Directory.Exists(folderName) Then
            System.IO.Directory.CreateDirectory(folderName)
        End If

        If fu1.PostedFiles IsNot Nothing Then
            For Each File In fu1.PostedFiles
                Dim intsize As Long = CInt(File.ContentLength)
                Dim strFileExt As String
                If intsize <= 20971520 Then
                    Dim strFileName As String = ""
                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmmss")
                    strFileName = System.IO.Path.GetFileName(File.FileName)
                    strFileExt = System.IO.Path.GetExtension(File.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Session("TENANT") & "\" & Upload_Time & "_" & strFileName '& "." & strFileExt
                    File.SaveAs(filePath)

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_TOWER_IMAGES")
                    sp.Command.AddParameter("@TWI_CODE", txtTowercode.Text, DbType.String)
                    sp.Command.AddParameter("@TWI_STATUS", "1", DbType.Int32)
                    sp.Command.AddParameter("@TWI_FILENAME", strFileName, DbType.String)
                    sp.Command.AddParameter("@TWI_UPLOAD_PATH", "~/UploadFiles/" + Session("TENANT") + "/" + Upload_Time + "_" + strFileName, DbType.String)
                    sp.Command.AddParameter("@TWI_REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@TWI_CREATED_BY", Session("Uid"), DbType.String)
                    sp.Command.AddParameter("@TWI_COMPANY_ID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                    sp.ExecuteScalar()
                End If
            Next
        Else
            lblMsg.Text = "Pick a supported file to upload by clicking the Browse button."
        End If

        obj.getname = txtTowerName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyTower(ddlTName, ddlCountryName, ddlCityname, ddlLName, Me) = 1) Then
            Cleardata()
            lblMsg.Text = "Tower Successfully Updated"
            lblMsg.Visible = True
        End If
        obj.BindCity(ddlCityname)
        obj.BindCountry(ddlCountryName)
        obj.Bindlocation(ddlLName)
        obj.BindTower(ddlTName)
        obj.Tower_LoadGrid(gvItem)
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtTowercode.Text
        obj.getname = txtTowerName.Text
        obj.getRemarks = txtRemarks.Text

        Dim intStatus As Integer = InsertTower(ddlCountryName, ddlCityname, ddlLName, Me)
        If intStatus = 0 Then
            Cleardata()
            lblMsg.Text = "Tower Successfully Inserted"
            lblMsg.Visible = True
        ElseIf intStatus = 2 Then
            Cleardata()
            lblMsg.Text = "Tower Successfully Inserted"
            lblMsg.Visible = True
        ElseIf intStatus = 1 Then
            lblMsg.Text = "Tower is in use; try another"
            lblMsg.Visible = True
        End If
        'If (InsertTower(ddlCountryName, ddlCityname, ddlLName, Me) = 0) Then
        '    Cleardata()
        '    lblMsg.Text = "Tower Successfully Inserted"
        '    lblMsg.Visible = True
        'ElseIf (InsertTower(ddlCountryName, ddlCityname, ddlLName, Me) = 2) Then
        '    Cleardata()
        '    lblMsg.Text = "Tower Successfully Inserted"
        '    lblMsg.Visible = True
        'ElseIf (InsertTower(ddlCountryName, ddlCityname, ddlLName, Me) = 1) Then
        '    lblMsg.Text = "Tower is in use; try another"
        '    lblMsg.Visible = True
        'End If
        obj.BindCity(ddlCityname)
        obj.BindCountry(ddlCountryName)
        obj.Bindlocation(ddlLName)
        obj.Tower_LoadGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        ddlCityname.Enabled = True
        ddlCountryName.Enabled = True
        ddlLName.Enabled = True
        Try
            If rbActions.Checked = True Then
                trLName.Visible = False
                txtTowercode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                Cleardata()
                trLName.Visible = True
                txtTowercode.ReadOnly = True
                ddlCityname.Enabled = False
                ddlCountryName.Enabled = False
                ddlLName.Enabled = False
                btnSubmit.Text = "Modify"
                obj.BindTower(ddlTName)
                obj.BindCity(ddlCityname)
                obj.BindCountry(ddlCountryName)
                obj.Bindlocation(ddlLName)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub
    Protected Sub DeleteFile(ByVal sender As Object, ByVal e As EventArgs)
        dialog.Visible = True
        Dim filePath As String = CType(sender, LinkButton).CommandArgument
        Dim Imagepath As New SqlParameter("@ImagePath", SqlDbType.NVarChar)
        Imagepath.Value = filePath
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "DELETE_SEL_TWR_IMG", Imagepath)
        filePath = Path.GetFileName(filePath)
        File.Delete(Request.PhysicalApplicationPath + "\UploadFiles\[TATA_CAP].dbo\" + filePath)
        Response.Redirect(Request.Url.AbsoluteUri)

    End Sub
    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)
        dialog.Visible = True
        Dim filePath As String = CType(sender, LinkButton).CommandArgument
        Response.ContentType = ContentType
        Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(filePath)))
        Response.WriteFile(filePath)
        Response.End()
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASTower", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlTName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTName.SelectedIndexChanged
        Try
            If ddlTName.SelectedItem.Value <> "--Select--" Then
                obj.Tower_SelectedIndex_Changed(ddlTName, ddlCountryName, ddlCityname, ddlLName)
                txtTowercode.Text = obj.getcode
                txtTowerName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindTower(ddlTName)
                obj.BindCountry(ddlCountryName)
                obj.BindCity(ddlCityname)
                obj.Bindlocation(ddlLName)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "ddlTName_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Tower_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub



    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Tower_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(5).Visible = False
                    gvItem.HeaderRow.Cells(6).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(5).Visible = False
                        gvItem.Rows(i).Cells(6).Visible = False
                    Next
                    lblMsg.Text = "Inactivate all the Floors that are part of this Tower"
                    lblMsg.Visible = True
                    Exit Sub
                End If
                obj.Tower_LoadGrid(gvItem)
            End If
            obj.BindTower(ddlTName)
            obj.Bindlocation(ddlLName)
            obj.BindCity(ddlCityname)
            obj.BindCountry(ddlCountryName)
            Cleardata()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASTower", "gvItem_RowCommand", exp)
        End Try

    End Sub

    Protected Sub ddlLName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLName.SelectedIndexChanged
        Try
            If (ddlLName.SelectedIndex <> 0) Then
                Dim dr As SqlDataReader = obj.tower_getloc_dtls(ddlLName.SelectedValue)
                If (dr.Read) Then

                    ddlCityname.SelectedValue = dr("lcm_cty_id")
                    ddlCountryName.SelectedValue = dr("lcm_cny_id")
                End If
            Else
                ddlCityname.SelectedIndex = 0
                ddlCountryName.SelectedIndex = 0
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "ddlLName_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
    Public Function InsertTower(ByVal ddlCny As DropDownList, ByVal ddlCty As DropDownList, ByVal ddlLoc As DropDownList, ByVal page As Page) As Integer
        Dim userid As String = page.Session("uid")

        Dim projectPath As String = Server.MapPath("~/")
        Dim folderName As String = Path.Combine(projectPath, "UploadFiles\" & Session("TENANT"))
        If Not Directory.Exists(folderName) Then
            System.IO.Directory.CreateDirectory(folderName)
        End If

        If fu1.PostedFiles IsNot Nothing Then
            For Each File In fu1.PostedFiles
                Dim intsize As Long = CInt(File.ContentLength)
                Dim strFileExt As String
                If intsize <= 20971520 Then
                    Dim strFileName As String = ""
                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmmss")
                    strFileName = System.IO.Path.GetFileName(File.FileName)
                    strFileExt = System.IO.Path.GetExtension(File.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Session("TENANT") & "\" & Upload_Time & "_" & strFileName '& "." & strFileExt
                    File.SaveAs(filePath)

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_TOWER_IMAGES")
                    sp.Command.AddParameter("@TWI_CODE", txtTowercode.Text, DbType.String)
                    sp.Command.AddParameter("@TWI_STATUS", "1", DbType.Int32)
                    sp.Command.AddParameter("@TWI_FILENAME", strFileName, DbType.String)
                    sp.Command.AddParameter("@TWI_UPLOAD_PATH", "~/UploadFiles/" + Session("TENANT") + "/" + Upload_Time + "_" + strFileName, DbType.String)
                    sp.Command.AddParameter("@TWI_REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@TWI_CREATED_BY", Session("Uid"), DbType.String)
                    sp.Command.AddParameter("@TWI_COMPANY_ID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                    sp.ExecuteScalar()
                End If
            Next
        Else
            lblMsg.Text = "Pick a supported file to upload by clicking the Browse button."
        End If


        Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
        sp1.Value = txtTowercode.Text
        Dim sp2 As New SqlParameter("@vc_Name", SqlDbType.NVarChar, 50)
        sp2.Value = txtTowerName.Text
        Dim sp3 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp3.Value = txtRemarks.Text
        Dim sp4 As New SqlParameter("@vc_CityCode", SqlDbType.NVarChar, 50)
        sp4.Value = ddlCityname.SelectedItem.Value
        Dim sp5 As New SqlParameter("@vc_CnyCode", SqlDbType.NVarChar, 50)
        sp5.Value = ddlCountryName.SelectedItem.Value
        Dim sp6 As New SqlParameter("@vc_User", SqlDbType.NVarChar, 50)
        sp6.Value = Session("Uid")
        Dim sp7 As New SqlParameter("@vc_LocCode", SqlDbType.NVarChar, 50)
        sp7.Value = ddlLName.SelectedItem.Value
        Dim sp8 As New SqlParameter("@vc_Status", SqlDbType.NVarChar, 50)
        sp8.Value = "1"
        Dim sp9 As New SqlParameter("@vc_Output", SqlDbType.Int)
        sp9.Direction = ParameterDirection.Output
        Dim sp10 As New SqlParameter("@companyId", SqlDbType.Int)
        sp10.Value = HttpContext.Current.Session("COMPANYID").ToString()
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertTower", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10)
        'Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_insertTower")
        'sp2.Command.AddParameter("@vc_Code", txtTowercode.Text, DbType.String)
        'sp2.Command.AddParameter("@vc_Name", txtTowerName.Text, DbType.String)
        'sp2.Command.AddParameter("@vc_remarks", txtRemarks.Text, DbType.String)
        'sp2.Command.AddParameter("@vc_CityCode", ddlCityname.SelectedItem.Value, DbType.String)
        'sp2.Command.AddParameter("@vc_CnyCode", ddlCountryName.SelectedItem.Value, DbType.String)
        'sp2.Command.AddParameter("@vc_User", Session("Uid"), DbType.String)
        'sp2.Command.AddParameter("@vc_LocCode", ddlLName.SelectedItem.Value, DbType.String)
        'sp2.Command.AddParameter("@vc_Status", "1", DbType.String)
        'sp2.Command.AddParameter("@vc_Output", ParameterDirection.Output, DbType.Int32)
        'sp2.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        'Dim OutResult = sp2.ExecuteScalar()
        If sp9.Value = 1 Then
            Return 1
            'ElseIf sp9.Value = 2 Then
            '    Return 2
        ElseIf sp9.Value = 0 Then
            Return 0
        End If
        'PopUpMessage("Record has been modified", page)
    End Function

    Protected Sub lnkViewImage_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim CommandArgument As String = btn.CommandArgument
        Dim dsImageList As New DataSet
        Dim SpImage As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERLIST_IMAGES")
        SpImage.Command.AddParameter("@TWR_CODE", CommandArgument, Data.DbType.String)
        dsImageList = SpImage.GetDataSet()
        grdTowerImages.DataSource = dsImageList
        grdTowerImages.DataBind()
        dialog.Visible = True

    End Sub

    Protected Sub grdTowerImages_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdTowerImages.PageIndexChanging
        Try
            grdTowerImages.PageIndex = e.NewPageIndex

        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASTower", "grdTowerImages_PageIndexChanging", exp)
        End Try
    End Sub
End Class

