Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Specialized
Imports System.Configuration

Partial Class Masters_Mas_Webfiles_frmMASMasters
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'TenantModule()
        lblSelVertical.Text = Session("Parent") + " Master"
        Label1.Text = Session("Child") + " Master"
    End Sub

    'Public Sub TenantModule()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Tenant_Module")
    '    sp.Command.AddParameter("@TID", Session("TENANT"), DbType.String)
    '    Dim ds As New DataSet
    '    ds = sp.GetDataSet
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each row As DataRow In ds.Tables(0).Rows
    '            If row.Item("T_MODULE") = "Space" Then
    '                Dim section As New NameValueCollection
    '                section = CType(ConfigurationManager.GetSection(row.Item("T_MODULE")), NameValueCollection)
    '                For Each key In section.Keys
    '                    Dim ctrl As Control
    '                    ctrl = Me.FindControl(key)
    '                    ctrl.Visible = Boolean.Parse(section(key))
    '                Next
    '                Exit Sub
    '            Else
    '                Dim section As New NameValueCollection
    '                section = CType(ConfigurationManager.GetSection("Space"), NameValueCollection)
    '                For Each key In section.Keys
    '                    Dim ctrl As Control
    '                    ctrl = Me.FindControl(key)
    '                    ctrl.Visible = Boolean.Parse(section(key))
    '                Next
    '                Exit Sub
    '            End If
    '        Next
    '    End If
    'End Sub
End Class
