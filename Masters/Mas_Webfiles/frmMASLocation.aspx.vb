Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing
Imports iTextSharp.text

Imports iTextSharp.text.pdf

Imports iTextSharp.text.html

Imports iTextSharp.text.html.simpleparser

Partial Class Masters_MAS_WebFiles_frmLocation
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim obj As clsMasters = New clsMasters
    Private Sub Cleardata()
        txtLocationcode.Text = String.Empty
        txtLocationName.Text = String.Empty
        txtRemarks.Text = String.Empty
        txtaddress.Text = String.Empty
        txtpincode.Text = String.Empty
        txtlat.Text = String.Empty
        txtlong.Text = String.Empty
        ddlCity.SelectedIndex = 0
        ddlCountry.SelectedIndex = 0
        ddlLocation.SelectedIndex = 1
        cmbstatus.SelectedIndex = 1
        cmbZone.SelectedIndex = 0
        'ddlEmpids.SelectedIndex = -1
        'txtDate.Text = String.Empty
    End Sub

    Private Sub Insertdata(selectedIndex As String)
        If txtpincode.Text = "" Then
            txtpincode.Text = 0
        End If
        If txtlat.Text = "" Then
            txtlat.Text = 0
        End If
        If txtlong.Text = "" Then
            txtlong.Text = 0
        End If
        obj.getcode = txtLocationcode.Text
        obj.getname = txtLocationName.Text
        obj.getRemarks = txtRemarks.Text
        obj.getAddress = txtaddress.Text
        obj.getPincode = CInt(txtpincode.Text)
        obj.getLatitude = txtlat.Text
        obj.getLongitude = txtlong.Text
        Dim iStatus As Integer = obj.insertlocation(ddlCity, ddlCountry, cmbstatus, cmbZone, Me)
        If iStatus = 1 Then
            lblMsg.Text = "Location Code is in use; try another"
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "Location Successfully Inserted "
            lblMsg.Visible = True
            Cleardata()
        ElseIf iStatus = 0 Then
            lblMsg.Text = "Location Successfully Inserted "
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.Location_LoadGrid(gvItem)
    End Sub

    Public Sub Location_LoadGrid(ByVal gv As GridView)

        strSQL = "usp_getLocationDetails"
        Make_Active_Inactive_location(gv, strSQL)

    End Sub
    Public Sub MasterData_ExportGrid(ByVal gv As GridView)

        strSQL = "EXPORT_MASTER_DATA"
        Make_Active_Inactive_Export_Master(gv, strSQL)

    End Sub
    Private Sub Make_Active_Inactive_location(ByVal gv As GridView, ByVal strsql As String)
        Dim i As Integer
        gv.Columns(7).Visible = True
        gv.Columns(8).Visible = True

        BindGrid(strsql, gv)
        For i = 0 To gv.Rows.Count - 1
            Dim btn As LinkButton = CType(gv.Rows(i).Cells(6).Controls(0), LinkButton)
            If Trim(Val(gv.Rows(i).Cells(7).Text)) = 1 Then
                btn.Text = "Active"
            ElseIf Trim(Val(gv.Rows(i).Cells(7).Text)) = 2 Then
                btn.Text = "Inactive"
            End If
        Next
        gv.Columns(7).Visible = False
        gv.Columns(8).Visible = True

    End Sub
    Private Sub Make_Active_Inactive_Export_Master(ByVal gv As GridView, ByVal strsql As String)
        Dim i As Integer
        gv.Columns(14).Visible = True
        gv.Columns(15).Visible = True

        BindGrid(strsql, gv)
        For i = 0 To gv.Rows.Count - 1
            'Dim btn As LinkButton = CType(gv.Rows(i).Cells(7).Controls(0), LinkButton)
            'If Trim(Val(gv.Rows(i).Cells(8).Text)) = 1 Then
            '    btn.Text = "Active"
            'ElseIf Trim(Val(gv.Rows(i).Cells(8).Text)) = 2 Then
            '    btn.Text = "Inactive"
            'End If
        Next
        gv.Columns(14).Visible = True
        gv.Columns(15).Visible = True

    End Sub

    Private Sub Modifydata(selectedIndex As String)
        If txtpincode.Text = "" Then
            txtpincode.Text = 0
        End If
        If txtlat.Text = "" Then
            txtlat.Text = 0
        End If
        If txtlong.Text = "" Then
            txtlong.Text = 0
        End If
        obj.getname = txtLocationName.Text
        obj.getRemarks = txtRemarks.Text
        obj.getAddress = txtaddress.Text()
        obj.getPincode = CInt(txtpincode.Text)
        obj.getLatitude = txtlat.Text
        obj.getLongitude = txtlong.Text
        If (obj.Modifyloc(ddlLocation, ddlCity, ddlCountry, cmbstatus, cmbZone, Me) > 0) Then
            lblMsg.Text = "Location Successfully Updated"
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.Location_LoadGrid(gvItem)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsPostBack Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
            End If
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using
            'rfvName.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            'RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
            'rfvStatus.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            'RegExpNumber.ValidationExpression = User_Validation.GetValidationExpressionForPhone.VAL_EXPR()

            'If Session("UID") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                obj.Location_LoadGrid(gvItem)
                trLName.Visible = False
                rbActions.Checked = True
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                btnExport_Click(sender, e)
                'bindEmployees()
                'txtDate.Attributes.Add("onClick", "displayDatePicker('" + txtDate.ClientID + "')")
                'txtDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            End If
        Catch exp As System.Exception
            'Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        ddlCity.Enabled = True
        ddlCountry.Enabled = True
        Try
            If rbActions.Checked = True Then
                trLName.Visible = False
                txtLocationcode.ReadOnly = False
                btnSubmit.Text = "Submit"
                trLName.Visible = False
                Cleardata()
            Else
                Cleardata()
                trLName.Visible = True
                txtLocationcode.ReadOnly = True

                ddlCity.Enabled = False
                ddlCountry.Enabled = False
                btnSubmit.Text = "Modify"
                Cleardata()
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
            btnExport_Click(sender, e)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click



        Dim strEroorMsg As String = String.Empty
        Dim selectedIndex As String = String.Empty

        'For Each item As WebControls.ListItem In ddlEmpids.Items
        '    If item.Selected Then
        '        If selectedIndex = "" Then
        '            selectedIndex = item.Value
        '        Else
        '            selectedIndex = selectedIndex & ", " & item.Value
        '        End If
        '    End If
        'Next
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata(selectedIndex)
                obj.Bindlocation(ddlLocation)
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata(selectedIndex)
                obj.Bindlocation(ddlLocation)
            End If
            obj.Location_LoadGrid(gvItem)
            btnExport_Click(sender, e)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmLocation", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedItem.Value <> "--Select--" Then
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                'bindEmployees()
                'obj.Loc_Selectedindex_changed(ddlLocation, ddlCity, ddlCountry, cmbstatus)
                Loc_Selectedindex_changed()

            Else
                Cleardata()
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "ddlLocation_SelectedIndexChanged", exp)
        End Try
    End Sub
    Public Sub Loc_Selectedindex_changed()
        Try
            Dim dr As SqlDataReader

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@vc_Code", SqlDbType.NVarChar, 200)
            param(0).Value = ddlLocation.SelectedValue
            dr = ObjSubsonic.GetSubSonicDataReader("usp_getLocationDetailforddl", param)
            'If (dr.HasRows) Then
            If dr.Read() Then
                ddlCity.SelectedValue = dr("LCM_CTY_ID").ToString()
                ddlCountry.SelectedValue = dr("LCM_CNY_ID").ToString()
                cmbstatus.SelectedValue = dr("LCM_STATUS").ToString()
                txtLocationcode.Text = dr("LCM_CODE").ToString()
                txtLocationName.Text = dr("LCM_NAME").ToString()
                txtRemarks.Text = dr("LCM_REM").ToString()
                txtpincode.Text = dr("LCM_PINCODE").ToString()
                txtaddress.Text = dr("LCM_ADDR").ToString()
                txtlat.Text = dr("LAT").ToString
                txtlong.Text = dr("LONG").ToString
                cmbZone.SelectedValue = dr("LCM_ZONE").ToString()
            End If
            dr.NextResult()
            '    While (dr.Read())
            '        For Each i As WebControls.ListItem In ddlEmpids.Items
            '            If i.Value = dr("LOCMAP_EMP_ID").ToString() Then
            '                i.Selected = True
            '            End If
            '        Next
            '    End While
            'Else
            '    lblMsg.Text = "Employee not mapped"
            '    lblMsg.Visible = True
            'End If
        Catch ex As Exception


        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Location_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Location_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(6).Visible = False
                    gvItem.HeaderRow.Cells(7).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(6).Visible = False
                        gvItem.Rows(i).Cells(7).Visible = False
                    Next
                    lblMsg.Text = "Inactivate all the Towers that are part of this Location"
                    lblMsg.Visible = True
                    Exit Sub
                End If
                obj.Location_LoadGrid(gvItem)
                Cleardata()
            End If
            obj.BindCity(ddlCity)
            obj.BindCountry(ddlCountry)
            obj.Bindlocation(ddlLocation)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmLocation", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            If (ddlCity.SelectedValue <> "--Select--") Then
                Dim dr As SqlDataReader = obj.location_getcity_dtls(ddlCity.SelectedValue)
                If (dr.Read) Then
                    ddlCountry.SelectedValue = dr("CTY_CNY_ID")
                End If
            Else
                ddlCountry.SelectedValue = "--Select--"

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmLocation", "ddlCity_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
    'Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
    '    HttpContext.Current.Response.Clear()
    '    HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
    '    HttpContext.Current.Response.ContentType = "application/ms-excel"
    '    Dim sw As StringWriter = New StringWriter
    '    Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '    ' Create a form to contain the grid
    '    Dim table As Table = New Table()
    '    table.GridLines = gv.GridLines
    '    '  add the header row to the table
    '    If (Not (gv.HeaderRow) Is Nothing) Then
    '        PrepareControlForExport(gv.HeaderRow)
    '        table.Rows.Add(gv.HeaderRow)
    '    End If
    '    '  add each of the data rows to the table
    '    For Each row As GridViewRow In gv.Rows
    '        PrepareControlForExport(row)
    '        table.Rows.Add(row)
    '    Next
    '    '  add the footer row to the table
    '    If (Not (gv.FooterRow) Is Nothing) Then
    '        PrepareControlForExport(gv.FooterRow)
    '        table.Rows.Add(gv.FooterRow)
    '    End If
    '    '  render the table into the htmlwriter
    '    table.RenderControl(htw)
    '      render the htmlwriter into the response
    '    HttpContext.Current.Response.Write(sw.ToString)
    '    HttpContext.Current.Response.End()

    'End Sub
    Protected Sub btnExport_Click(sender As Object, e As EventArgs) 'Handles btnExport.Click
        MasterData_ExportGrid(ExportGrid)

        'Response.Clear()
        'Response.Buffer = True
        ''Response.AddHeader("content-disposition", "attachment;filename=Location_Data.xls")
        'Response.Charset = ""
        ''Response.ContentType = "application/vnd.ms-excel"
        'Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

        'Response.AppendHeader("content-disposition", "attachment; filename=myfile.xlsx")
        'Response.ContentEncoding = System.Text.Encoding.Unicode
        'Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())
        Using sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)

            'To Export all pages
            ExportGrid.AllowPaging = False
            Me.MasterData_ExportGrid(ExportGrid)

            ExportGrid.HeaderRow.BackColor = System.Drawing.Color.White
            For Each cell As TableCell In ExportGrid.HeaderRow.Cells
                cell.BackColor = ExportGrid.HeaderStyle.BackColor
            Next
            For Each row As GridViewRow In ExportGrid.Rows
                row.BackColor = System.Drawing.Color.White
                For Each cell As TableCell In row.Cells
                    If row.RowIndex Mod 2 = 0 Then
                        cell.BackColor = ExportGrid.AlternatingRowStyle.BackColor
                    Else
                        cell.BackColor = ExportGrid.RowStyle.BackColor
                    End If
                    cell.CssClass = "textmode"
                Next
            Next
            If Not Directory.Exists(HttpContext.Current.Server.MapPath("~\Location_Data")) Then
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~\Location_Data"))
            End If
            ExportGrid.RenderControl(hw)
            For Each deleteFile In Directory.GetFiles(HttpContext.Current.Server.MapPath("~\Location_Data\"), "*.*", SearchOption.TopDirectoryOnly)
                File.Delete(deleteFile)
            Next


            Dim newFileName As String = "Location_Data.xls"
            File.WriteAllText(HttpContext.Current.Server.MapPath("~/Location_Data/" & newFileName), sw.ToString())

            hdnAttrVal.Value = "/Location_Data/" + newFileName

            'style to format numbers to string
            'Dim style As String = "<style> .textmode { } </style>"

            'Response.Write(style)
            'Response.Output.Write(sw.ToString())

            'Response.Flush()
            'Response.[End]()
        End Using
    End Sub
    'Protected Sub btnPdf_Click(ByVal sender As Object, ByVal e As EventArgs)

    '    Response.ContentType = "application/pdf"

    '    Response.AddHeader("content-disposition", "attachment;filename=Location_Data.pdf")

    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)

    '    Dim sw As New StringWriter()

    '    Dim hw As New HtmlTextWriter(sw)


    '    ExportGrid.AllowPaging = False
    '    Me.MasterData_ExportGrid(ExportGrid)
    '  '  ExportGrid.DataBind()

    '    ExportGrid.RenderControl(hw)

    '    Dim sr As New StringReader(sw.ToString())

    '    Dim pdfDoc As New Document(PageSize.LETTER.Rotate(), 10, 10, 42, 35)

    '    Dim htmlparser As New HTMLWorker(pdfDoc)

    '    PdfWriter.GetInstance(pdfDoc, Response.OutputStream)



    '    pdfDoc.Open()


    '    htmlparser.Parse(sr)

    '    pdfDoc.Close()

    '    Response.Write(pdfDoc)

    '    Response.End()

    'End Sub

    Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        ' Verifies that the control is rendered
    End Sub
    'Public Sub bindEmployees()
    '    ddlEmpids.Items.Clear()
    '    ObjSubsonic.BindListBox(ddlEmpids, "LOCATION_EMPLOYEE_LIST", "EMPLOYEE", "EMP_CODE")
    '    'ObjSubsonic.Binddropdown(ddlToGrade, "AMANTRA_EMPLOYEE_GRADE_GETDETAILS", "grade_name", "grade_Code")
    '    'For i As Integer = 1 To 350
    '    '    ddlfromGrade.Items.Insert((i - 1).ToString(), i.ToString())
    '    '    ddlToGrade.Items.Insert((i - 1).ToString(), i.ToString())
    '    'Next

    'End Sub

End Class
