<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMASMaster.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasters" Title="Masters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript" defer> 
    function noway(go)
    {
        if(document.all)
        {
            if (event.button == 2)
            {
                alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                return false;
            }
        }
        if (document.layers) 
        {
            if (go.which == 3)
            {
                alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                return false;
            }
        }
    } 
     if (document.layers) 
     {
        document.captureEvents(Event.MOUSEDOWN); 
     }
     document.onmousedown=noway;
    </script>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Masters
                        <hr align="center" width="75%"/></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                align="center" border="0">
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Primary Masters </strong>
                    </td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                    </td>
                    <td align="center">
                        <br />
                        <table id="table2" cellspacing="0" cellpadding="5" width="100%" border="1" style="border-collapse: collapse">
                            <tr runat="server">
                                <td align="left" class="clsLabel" >
                                    &nbsp;<asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCountry.aspx">Country Master</asp:HyperLink></td>
                                <td align="left" class="clsLabel" style="width: 50%; height: 26px;">
                                    <font class="clsNote">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCity.aspx">City Master</asp:HyperLink></font></td>
                            </tr>
                            <tr>
                                <td align="left" class="clsLabel" style="width: 43%; height: 26px">
                                    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASLocation.aspx"> Location Master</asp:HyperLink></td>
                                <td align="left" style="width: 43%; height: 26px;">
                                    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASTower.aspx">Tower Master</asp:HyperLink></td>
                            </tr>
                            <tr>
                                <td align="left" class="clsLabel" style="height: 26px; width: 50%;">
                                    <span style="font-size: 6pt; color: #ff0000">
                                        <asp:HyperLink ID="hlkFloor" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFloor.aspx">Floor Master</asp:HyperLink></span></td>
                                <td align="left" class="clslabel" style="width: 50%; height: 26px">
                                    <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASBlock.aspx">Wing Master</asp:HyperLink></td>
                            </tr>
                            <%--<tr>
                                    <td align="left" class="clsLabel" style="width: 50%; height: 26px">
                                         <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASSeat.aspx"
                                             >Seat Master</asp:HyperLink></td>
                                    <td align="left" class="clsLabel" style="width: 50%; height: 26px">
                                         </td>
                                     
                                </tr>--%>
                            <tr>
                                <td align="left" class="clsLabel" style="width: 50%; height: 26px">
                                    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASDesignition.aspx">Designation Master</asp:HyperLink></td>
                                <td align="left" class="clsLabel" style="width: 50%; height: 26px">
                                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMASDepartment.aspx">Department Master</asp:HyperLink></td>
                            </tr>
                            <tr>
                                <td align="left" class="clsLabel" style="width: 50%; height: 26px">
                                    <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/WorkSpace/SMS_Webfiles/frmLinkMaster.aspx">Link Master</asp:HyperLink></td>
                            </tr>
                            <%--  <td  visible="false" align="left" class="clsLabel" style="width: 50%; height: 26px">
                                        <asp:HyperLink ID="HyperLink12" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmEditUser.aspx" Visible="False"
                                             >Edit User</asp:HyperLink></td>
                                    
                               
                                --%>
                        </table>
                        <br />
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
