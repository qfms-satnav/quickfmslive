<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmPropMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmPropMasters" title="Property Masters" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Property Masters
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Property Masters</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="<%Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
    
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                ShowMessageBox="true" DisplayMode="List" ForeColor="" ValidationGroup="Val1" />
            <asp:Label ID="lblNote" runat="server" Width="100%" CssClass="clsNote" ToolTip="Provide information for (*) mandatory fields. "
                ForeColor="Red">Provide information for mandatory (*) fields. </asp:Label>
                
                <td align="left">
                <table id="table2" cellspacing="0" cellpadding="5" width="100%" border="1" style="border-collapse: collapse">
                            <tr id="trLName" runat="server">
                                <td align="left" class="label" style="height: 26px; width: 50%;">
                                    &nbsp;<asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmLessorMaster.aspx.">Lessor Master</asp:HyperLink></td>
                                
                            </tr>
                            </table>
                
                
                 </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif") %>">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif") %>"
                    width="9" /></td>
            <td background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>" width="25" /></td>
            <td>
                <img height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</div>
</asp:Content>

