﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmEditVehicle.aspx.vb" Inherits="Masters_Mas_Webfiles_frmEditVehicle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

    <script language="javascript" type="text/javascript" defer> 
     function maxLength(s,args)
     { 
     if(args.Value.length >= 500)
     args.IsValid=false;
     }
    function noway(go)
    {
        if(document.all)
        {
            if (event.button == 2)
            {
                alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                return false;
            }
        }
        if (document.layers) 
        {
            if (go.which == 3)
            {
                alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                return false;
            }
        }
    } 
    
    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript" defer></script>

  <div>
    <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">   <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False" ForeColor="Black"> Edit Vehicle Details <hr align="center" width="60%" /></asp:Label>
</td>
                        </tr>
                        </table>
            
           
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
         
                <table id="table3" cellspacing="0" cellpadding="0"  width="95%"
                    align="center" border="0">
                      <tr>
                        <td  colspan="3" align="left">
<%--            <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
--%>          </td>
                        </tr>
                    <tr>
                        <td>
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td width="100%" class="tableHEADER" align="left">
                            <strong>&nbsp; Edit Vehicle Details</strong>
                        </td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                            &nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                                ForeColor="" ValidationGroup="Val1" /><br />
                             
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                             
                            <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                                
                           </table>
                            
                            <br />
                            <asp:GridView ID="gvVehicleDetails" TabIndex="9" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" EmptyDataText="No Records Found">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Emp ID">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpId" runat="Server" Text='<%#Eval("EMP_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Emp Name">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpName" runat="Server" Text='<%#Eval("EMP_FIRST_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Reg. No">
                            <ItemTemplate>
                                <asp:Label ID="lblRegNo" runat="Server" Text='<%#Eval("VHL_REG_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Eng. No">
                            <ItemTemplate>
                                <asp:Label ID="lblEngNo" runat="Server" Text='<%#Eval("VHL_ENG_NO") %>'></asp:Label>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="CC ">
                            <ItemTemplate>
                                <asp:Label ID="lblCC" runat="Server" Text='<%#Eval("VHL_CC") %>'></asp:Label>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                   
                        
                        <asp:TemplateField ShowHeader="false">
                        <ItemTemplate>
                            <a href='ModifyVehicle.aspx?SNO=<%#Eval("SNO")%>'>EDIT</a>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                  
                    </Columns>
                </asp:GridView>
                
                
                        <td background="../../Images/table_right_mid_bg.gif" 
                            style="width: 10px; height: 100%;">
                            &nbsp;</td>
                      </tr>      
                                                            <tr>
                                                <td style="width: 10px; height: 17px;">
                                                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                                                
                                                
                                                <td style="height: 17px;" background="../../Images/table_bot_mid_bg.gif">
                                                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                                                                <td style="height: 17px">
                                                                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                      </tr> 
                                                                                
                                                            </table></asp:Panel></div>
</asp:Content>

