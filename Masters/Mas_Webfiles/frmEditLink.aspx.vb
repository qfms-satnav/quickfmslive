Imports System.Data
Partial Class WorkSpace_SMS_Webfiles_frmLinkMaster
    Inherits System.Web.UI.Page
    Dim ftime As String = ""
    Dim ttime As String = ""

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        UPDATECLASSIFICATION()
        lblMsg.Text = "Request  Modified Succesfully"
        lblMsg.Visible = True
        Cleardata()
        BindLink()
        BindGrid()
    End Sub
    Private Sub UPDATECLASSIFICATION()
        Try
            Dim StartHr As String = "00"
            Dim EndHr As String = "00"
            Dim StartMM As String = "00"
            Dim EndMM As String = "00"

            If starttimehr.SelectedItem.Value <> "Hr" Then
                StartHr = starttimehr.SelectedItem.Text
            End If
            If starttimemin.SelectedItem.Value <> "Min" Then
                StartMM = starttimemin.SelectedItem.Text
            End If
            If endtimehr.SelectedItem.Value <> "Hr" Then
                EndHr = endtimehr.SelectedItem.Text
            End If
            If endtimemin.SelectedItem.Value <> "Min" Then
                EndMM = endtimemin.SelectedItem.Text
            End If

            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_CATEGORY")
            sp.Command.AddParameter("@cls_Description", txtLinkName.Text, DbType.String)
            sp.Command.AddParameter("@CLS_ID", ddlLinkName.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@CLS_PARENT_ID", ddlparentlink.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@CATEGORYNAME", txtreqname.Text, DbType.String)
            sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@STAT", ddlstatus.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FROM_TIME", ftime, DbType.DateTime)
            sp.Command.AddParameter("@TO_TIME", ttime, DbType.DateTime)
            sp.ExecuteScalar()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Cleardata()
        ddlparentlink.SelectedIndex = -1
        ddlLinkName.SelectedIndex = -1
        txtLinkName.Text = ""
        txtreqname.Text = ""
        starttimehr.SelectedIndex = 0
        endtimehr.SelectedIndex = 0
        starttimemin.SelectedIndex = 0
        endtimemin.SelectedIndex = 0
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack() Then
            BindLink()
            BindParentLink()
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CATEGORIES_GRID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()
    End Sub
    Private Sub BindParentLink()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CLASSIFICATION1")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlparentlink.DataSource = sp.GetDataSet()
        ddlparentlink.DataTextField = "CategoryName"
        ddlparentlink.DataValueField = "idcategory"
        ddlparentlink.DataBind()
        ddlparentlink.Items.Insert(0, New ListItem("--Parent ID--", "0"))
    End Sub

    Private Sub BindLink()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_BINDLINK")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlLinkName.DataSource = sp.GetDataSet()
            ddlLinkName.DataTextField = "categoryName"
            ddlLinkName.DataValueField = "idCategory"
            ddlLinkName.DataBind()
            ddlLinkName.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlLinkName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLinkName.SelectedIndexChanged
        If ddlLinkName.SelectedIndex > 0 Then


         
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LINK_DETAILS")
            sp.Command.AddParameter("@CLS_ID", ddlLinkName.SelectedItem.Value, DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtLinkName.Text = ds.Tables(0).Rows(0).Item("categoryDesc")
                txtreqname.Text = ds.Tables(0).Rows(0).Item("categoryname")
                ddlstatus.ClearSelection()
                ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("active")).Selected = True
                ddlparentlink.ClearSelection()
                ddlparentlink.Items.FindByValue(ds.Tables(0).Rows(0).Item("idParentCategory")).Selected = True
                starttimehr.ClearSelection()
                starttimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_HH")).Selected = True
                starttimemin.ClearSelection()
                starttimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_MM")).Selected = True
                endtimehr.ClearSelection()
                endtimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_HH")).Selected = True
                endtimemin.ClearSelection()
                endtimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_MM")).Selected = True

            End If
        End If
    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
End Class
