Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_Mas_Webfiles_frmMASFloor
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Private Sub Cleardata()
        txtFloorcode.Text = String.Empty
        txtFloorName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlCity.SelectedIndex = 0
        ddlCountry.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        ddlTower.SelectedIndex = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
            End If
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using
            'rfvName.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            'RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
            'rfvStatus.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            'RegExpNumber.ValidationExpression = User_Validation.GetValidationExpressionForPhone.VAL_EXPR()
            'If Session("UID") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                obj.Floor_LoadGrid(gvItem)
                trLName.Visible = False
                obj.BindFloor(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "Load", exp)
        End Try
    End Sub

    Private Sub insertdata()
        obj.getcode = txtFloorcode.Text
        obj.getname = txtFloorName.Text
        obj.getRemarks = txtRemarks.Text
        Dim iStatus As Integer = obj.insertFloor(ddlTower, ddlLocation, ddlCity, ddlCountry, Me)
        If (iStatus = 1) Then
            lblMsg.Text = "Floor Code is in use; try another"
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "Floor Successfully Inserted"
            lblMsg.Visible = True
            Cleardata()
        Else : iStatus = 0
            lblMsg.Text = "Floor Successfully Inserted"
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.BindFloor(ddlFloor)
        obj.BindTower(ddlTower)
        obj.Bindlocation(ddlLocation)
        obj.BindCity(ddlCity)
        obj.BindCountry(ddlCountry)
        obj.Floor_LoadGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getname = txtFloorName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyFloor(ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry, Me) > 0) Then
            lblMsg.Text = "Floor Successfully Updated"
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.BindFloor(ddlFloor)
        obj.BindTower(ddlTower)
        obj.Bindlocation(ddlLocation)
        obj.BindCity(ddlCity)
        obj.BindCountry(ddlCountry)
        obj.Floor_LoadGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        ddlTower.Enabled = True
        ddlCity.Enabled = True
        ddlLocation.Enabled = True
        ddlCountry.Enabled = True
        Try
            If rbActions.Checked = True Then
                trLName.Visible = False
                txtFloorcode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                trLName.Visible = True
                txtFloorcode.ReadOnly = True
                ddlTower.Enabled = False
                ddlCity.Enabled = False
                ddlLocation.Enabled = False
                ddlCountry.Enabled = False
                btnSubmit.Text = "Modify"
                obj.BindFloor(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                Cleardata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
            End If
            txtFloorcode.Text = ""
            txtFloorName.Text = ""
            txtRemarks.Text = ""
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASFloor", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        Try
            If ddlFloor.SelectedItem.Value <> "--Select--" Then
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                obj.Floor_SelectedIndex_Changed(ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry)
                txtFloorcode.Text = obj.getcode
                txtFloorName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindFloor(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)


            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "ddlFloor_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Floor_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Floor_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(5).Visible = False
                    gvItem.HeaderRow.Cells(6).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(5).Visible = False
                        gvItem.Rows(i).Cells(6).Visible = False
                    Next
                    lblMsg.Text = "Inactivate all the Wings that are part of this Floor"
                    lblMsg.Visible = True
                    Exit Sub
                End If
                obj.Floor_LoadGrid(gvItem)
            End If
            obj.BindFloor(ddlFloor)
            obj.BindTower(ddlTower)
            obj.Bindlocation(ddlLocation)
            obj.BindCity(ddlCity)
            obj.BindCountry(ddlCountry)
            txtFloorcode.Text = ""
            txtFloorName.Text = ""
            txtRemarks.Text = ""
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASFloor", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If (ddlTower.SelectedIndex <> 0) Then
                Dim dr As SqlDataReader = obj.floor_gettower_dtls(ddlTower.SelectedValue)
                If (dr.Read) Then
                    ddlLocation.SelectedValue = dr("twr_loc_id")
                    ddlCity.SelectedValue = dr("twr_cty_id")
                    ddlCountry.SelectedValue = dr("twr_cny_id")
                End If
            Else
                ddlLocation.SelectedIndex = 0
                ddlCity.SelectedIndex = 0
                ddlCountry.SelectedIndex = 0
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "ddlTower_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
End Class
