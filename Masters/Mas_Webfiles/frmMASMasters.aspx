<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmMASMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASMasters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div ba-panel ba-panel-title="Primary Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
        <div class="widgets">
            <h3>Primary Masters</h3>
        </div>
        <div class="card">
            <div class="card-body">
                <form id="form1" runat="server">
                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12" id="CountryHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="CountryHL" runat="server" role="button" href="~/Masters/Mas_Webfiles/frmMASCountry.aspx" class="d-flex justify-content-between align-items-center">
                                    <span>Country Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12" id="CityHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="CityHL" runat="server" class="d-flex justify-content-between align-items-center " href="~/Masters/Mas_Webfiles/frmMASCity.aspx">
                                    <span>City Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12" id="LocationHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="LocationHL" runat="server" href="~/Masters/Mas_Webfiles/frmMASLocation.aspx" class="d-flex justify-content-between align-items-center">
                                    <span>Location Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12" id="TowerHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="TowerHL" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/frmMASTower.aspx">
                                    <span>Tower Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12" id="FloorHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="FloorHL" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/frmMASFloor.aspx">
                                    <span>Floor Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12" id="DesignatioHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="DesignatioHL" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/frmMASDesignition.aspx">
                                    <span>Designation Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12" id="DepartmentHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="DepartmentHL" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/frmMASDepartment.aspx">
                                    <span>Department Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12" id="UplaodMasterDataHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="UplaodMasterDataHL" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/uploadmasterdata.aspx">
                                    <span>Upload Master Data</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12" id="UploadHRMSDataHLdiv" runat="server">
                            <div class="btn btn-primary btn-block">
                                <a id="UploadHRMSDataHL" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/frmMasUploadHRMSData.aspx">
                                    <span>Upload HRMS Data</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="btn btn-primary btn-block">
                                <a id="HyperLink3" runat="server" class="d-flex justify-content-between align-items-center" href="~/Workspace/sms_Webfiles/frmEntityAdmin.aspx">
                                    <span>Parent Entity Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="btn btn-primary btn-block">
                                <a id="HyperLink4" runat="server" class="d-flex justify-content-between align-items-center" href="~/Workspace/sms_Webfiles/frmEntity.aspx">
                                    <span>Child Entity Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="btn btn-primary btn-block">
                                <a id="HyperLink11" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/frmMASVertical.aspx">
                                    <asp:Label ID="lblSelVertical" runat="server" Text=""></asp:Label>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="btn btn-primary btn-block">
                                <a id="HyperLink5" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/Mas_Webfiles/frmMASCostCenter.aspx">
                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="btn btn-primary btn-block">
                                <a id="hprBSM" runat="server" class="d-flex justify-content-between align-items-center" href="~/Masters/MAS_Webfiles/frmMasBusinessSpecificMaster.aspx">
                                    <span>Business Specific Master</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="btn btn-primary btn-block">
                                <a id="HyperLink2" runat="server" class="d-flex justify-content-between align-items-center" href="~/FAM/FAM_Webfiles/frmAMGVendorNewRecord1.aspx">
                                    <span>Add Vendor</span>
                                    <span><i class="fa fa-play"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
