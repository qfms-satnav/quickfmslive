﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <script src="../../Scripts/angular.js"></script>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>


    <style>
        .grid-align {
            text-align: left;
           white-space: normal !important;
        }


        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #e7f7f3;
        }

        .ag-blue .ag-header-cell-label {
            color: black;
        }

        .cell-fail {
            text-align: left;
            font-weight: bold;
            background-color: red;
        }

        .cell-pass {
            text-align: left;
            font-weight: bold;
            background-color: #4caf50;
        }

        .cell-less {
            text-align: left;
            font-weight: bold;
            background-color: orange;
        }

        .bxheader {
            width: 100%;          
            background-color:black;
            padding: 5px 10px;
        }
        .ag-blue .ag-row-odd {
            background-color: #fff;
        }
         .panel-title{
            color:black;
            font-weight:bold;
        }
         .ag-header-cell-label {
    text-overflow: clip;
    overflow: visible;
    white-space: normal;   
}
    </style>
</head>
<body data-ng-controller="SpareCurrentInventoryController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="SpareParts Summary" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Current Inventory
                                 
                            </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" name="SpareCurrentInventory" data-valid-submit="LoadData()" novalidate>
                                <div class="clearfix" style="height: 50px;">

                                    <div class="row">

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Site</label>
                                                <div isteven-multi-select data-input-model="Locations" data-output-model="SpareCurrentInventory.Locations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                    data-on-item-click="GetAssetDetails('')" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>

                                                <input type="text" data-ng-model="SpareCurrentInventory.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="SpareCurrentInventory.$submitted && SpareCurrentInventory.Location.$invalid" style="color: red">Please select a location</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': SpareCurrentInventory.$submitted && SpareCurrentInventory.SUBC_NAME.$invalid}">

                                                <label class="control-label">Vendor</label>
                                                <div isteven-multi-select data-input-model="Type" data-output-model="SpareCurrentInventory.Type" data-button-label="icon SUBC_NAME"
                                                    data-item-label="icon SUBC_NAME" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                                <input type="text" data-ng-model="SpareCurrentInventory.Type[0]" name="SUBC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="SpareCurrentInventory.$submitted && SpareCurrentInventory.SUBC_NAME.$invalid" style="color: red">Please select equipment</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <span class="btn btn-info">Total Inventory
                            <br>
                            <span class="badge ng-binding">{{SpareCurrentInventory.Stock || 0}} </span>
                        </span>
                                               <%-- <div style="margin-top: 1px;">
                                                    <label for="txtcode"><strong>Total Inventory</strong></label>
                                                    <label>
                                                        <br />
                                                         {{SpareCurrentInventory.Stock || 0}} 
                                                    </label>
                                                </div>--%>
                                            </div>
                                        </div>
                                         <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                 <span class="btn btn-success">Total Cost
                            <br>
                            <span class="badge ng-binding">{{SpareCurrentInventory.TotCost || 0}} </span>
                        </span>
                                              <%--  <div style="margin-top: 1px;">
                                                    <label for="txtcode"><strong>Total Cost</strong></label>
                                                    <label>
                                                        <br />
                                                         {{SpareCurrentInventory.TotCost || 0}}
                                                    </label>
                                                </div>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group" style="margin-top: 19px;">
                                                <input type="hidden" id="hdCURRENT" name="hdCURRENT" />
                                                <input type="Button" value="Search" class='btn btn-primary custom-button-color' data-ng-click="SearchA('')" />
                                                 <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                                            </div>
                                        </div>
                                        <%--<div class="col-md-2 col-sm-6 col-xs-12">
                                         <div class="form-group">

                                          
                                        </div>
                                            </div>--%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <%--<a data-ng-click="GenReport(OpexFormat,'doc')"><i id="word" data-bs-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-middle"></i></a>--%>
                                            <%-- <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>--%>
                                            <%--<a data-ng-click="GenReport(OpexFormat,'pdf')"><i id="pdf" data-bs-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                                        </div>
                                        <div class="bxheader">
                                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                        </div>
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-danger homeButton" href="#" onclick="fnDetail('../../MaintenanceManagement/Views/MaintainanceDashboard.aspx')" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-home" aria-hidden="true"></i>HOME</a>
                                 <a class="btn btn-danger homeButton" href="#" onclick="goBack();" role="button" style="float:right;color:#fff;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-home" aria-hidden="true"></i>Back</a>
    </div>

    <div id="viewImage" class="modal fade in" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="min-width: 700px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Document image </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="control-group has-feedback">
                        <img id="proimglarg" src="#" width="100%" />
                        <!-- /.input group -->
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/SpareCurrentInventory.js"></script>
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            ////$('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            ////$('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
            var query = window.location.search == "" ? "" : window.location.search.substring(1);
            if (query == "") {
                $('#hdCURRENT').val('0');
            }
            else {
                $('#hdCURRENT').val(query);
            }
            //$('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            //$('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        });
        function fnDetail(url) {
            //progress(0, 'Loading...', true);
            window.location = url;
        }
        function goBack() {
            //window.history.back();
            var strBackUrl = sessionStorage.getItem("strBackUrl");
            window.location = strBackUrl;
        }
    </script>
</body>
</html>

