﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
      <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="Consume_Spare_Controller" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Consume Spare" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Consume Spare</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" name="Consume_Spare">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Site <span style="color: red;"></span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="Consume_Spare_Controller.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getEquipmentFn('P')" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" 
                                                data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="Consume_Spare_Controller.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        </div>
                                    </div>
                                    <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Equipment <span style="color: red;"></span></label>
                                            <div isteven-multi-select data-input-model="Equipment" data-output-model="Consume_Spare_Controller.Equipment" data-button-label="icon GROUP_NAME"
                                                  data-item-label="icon GROUP_NAME maker" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="Consume_Spare_Controller.Equipment[0]" name="Equipment" style="display: none" required="" />

                                        </div>
                                    </div>--%>
                                     <div class="col-md-1 col-sm-6 col-xs-12">
                                        <br />
                                        <div class="box-footer">
                                            <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="Load()" />
                                        </div>
                                    </div>
                                </div>
                                <br /><br />
                                <div class="clearfix">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 20%" />
                                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 630px"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../Scripts/JSONToCSVConvertor.js" defer></script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../../SMViews/Map/BreakDownMaintenance.js"></script>
    <script src="../JS/Consume_Spare.js"></script>
    <script>
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
</body>
