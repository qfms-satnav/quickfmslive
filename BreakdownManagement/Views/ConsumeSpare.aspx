﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<script runat="server">
</script>


<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>


    <style>
        .grid-align {
            text-align: center;
        }


        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .auto-style1 {
            height: 50px;
        }
         .ag-header-cell {
            background-color: #dbdeed;
        }

        .ag-blue .ag-header-cell-label {
            color: black;
        }
        .bxheader {
            width: 100%;          
            background-color:black;
            padding: 5px 10px;
        }
        .ag-blue .ag-row-odd {
            background-color: #fff;
        }
        .panel-title{
            color:black;
            font-weight:bold;
        }
        .swal-wide{
    width:850px !important;
}
    </style>
</head>
<body data-ng-controller="ConsumeSpareController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel-title="SpareParts Summary" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Consume Spare
                               
                            </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1"  name="ConsumeSpare" data-valid-submit="AddItem()" novalidate>
                                <%--data-ng-show="consume10"--%>
                              <div >
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Site</label>
                                                <div isteven-multi-select data-input-model="Locations" data-output-model="ConsumeSpare.Locations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME" selection-mode="single"
                                                    data-on-item-click="GetAssetDetails()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ConsumeSpare.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.Locations.$invalid" style="color: red">Please select a location </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ConsumeSpare.$submitted && ConsumeSpare.SUBC_NAME.$invalid}">
                                                <label class="control-label">Equipment</label>
                                                 <div isteven-multi-select data-input-model="Type" data-output-model="ConsumeSpare.Type" data-button-label="icon SUBC_NAME" data-item-label="icon SUBC_NAME" selection-mode="single"
                                                    data-on-item-click="GetTicketDetails()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                               <%-- <input type="text" data-ng-model="ConsumeSpare.Type[0]" name="SUBC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.Type.$invalid" style="color: red">Please Select Equipment </span>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ConsumeSpare.$submitted && ConsumeSpare.BDMP_PLAN_ID.$invalid}">

                                                <label class="control-label">Attached ticket</label>
                                                <div isteven-multi-select data-input-model="Ticket" data-output-model="ConsumeSpare.Ticket" data-button-label="icon BDMP_PLAN_ID"
                                                    data-item-label="icon BDMP_PLAN_ID" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()" selection-mode="single"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                               <%--  <input type="text" data-ng-model="ConsumeSpare.Ticket[0]" name="BDMP_PLAN_ID" style="display: none" required="" />
                                               <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.BDMP_PLAN_ID.$invalid" style="color: red">Please Select id</span>--%>
                                            </div>
                                        </div>

                                        <div class="col-md-1 col-sm-6 col-xs-12" style="display:none;">
                                            <div>
                                                <input type="hidden" id="hdCONSUME" name="hdCONSUME" />
                                                <input type="submit" value="Get Spare" class="btn btn-primary custom-button-color" style="margin-right: 40px" />
                                            </div>
                                        </div>

                                    </div>
                           </div>
                                </form>
                            <form>
                                <%--data-ng-show="consume20"--%>
                       <div data-ng-show="consume20">
                           <br /><br />
                               <%-- <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': ConsumeSpare.$submitted && ConsumeSpare.SUBC_NAME.$invalid}">

                                            <label class="control-label">Item Descrption</label>
                                            <div isteven-multi-select data-input-model="Descrption" data-output-model="ConsumeSpare.Descrption" data-button-label="icon AAS_SPAREPART_DES"
                                                data-item-label="icon AAS_SPAREPART_DES" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConsumeSpare.Descrption[0]" name="AAS_SPAREPART_DES" style="display: none" required="" />
                                            <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.AAS_SPAREPART_DES.$invalid" style="color: red">Please select Descrption</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': ConsumeSpare.$submitted && ConsumeSpare.AAS_ASP_CODE.$invalid}">

                                            <label class="control-label">Item Code</label>
                                            <div isteven-multi-select data-input-model="Code" data-output-model="ConsumeSpare.Code" data-button-label="icon AAS_ASP_CODE"
                                                data-item-label="icon AAS_ASP_CODE" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConsumeSpare.Code[0]" name="AAS_ASP_CODE" style="display: none" required="" />
                                            <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.AAS_ASP_CODE.$invalid" style="color: red">Please select Code</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': ConsumeSpare.$submitted && ConsumeSpare.AAS_SPAREPART_COST.$invalid}">

                                            <label class="control-label">Cost of Inventory</label>
                                            <div isteven-multi-select data-input-model="Inventory" data-output-model="ConsumeSpare.Inventory" data-button-label="icon AAS_SPAREPART_COST"
                                                data-item-label="icon AAS_SPAREPART_COST" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConsumeSpare.Inventory[0]" name="AAS_SPAREPART_COST" style="display: none" required="" />
                                            <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.AAS_SPAREPART_COST.$invalid" style="color: red">Please select Cost</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': ConsumeSpare.$submitted && ConsumeSpare.AAS_SPAREPART_AVBQUANTITY.$invalid}">

                                            <label class="control-label">Avalible Quantity</label>
                                            <div isteven-multi-select data-input-model="Quantity" data-output-model="ConsumeSpare.Quantity" data-button-label="icon AAS_SPAREPART_AVBQUANTITY"
                                                data-item-label="icon AAS_SPAREPART_AVBQUANTITY" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="ConsumeSpare.Quantity[0]" name="AAS_SPAREPART_AVBQUANTITY" style="display: none" required="" />
                                            <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.AAS_SPAREPART_AVBQUANTITY.$invalid" style="color: red">Please select Quantity</span>
                                        </div>
                                    </div>
                               
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="margin-left:15px">
                                            <label class="control-label">Quantity</label>
                                            <input id="demoInput" type="number" style="width: 50px;">
                                            <button onclick="increment()">+</button>
                                            <button onclick="decrement()">-</button>
                                            <script>
                                                function increment() {
                                                    document.getElementById('demoInput').stepUp();
                                                }
                                                function decrement() {
                                                    document.getElementById('demoInput').stepDown();
                                                }
                                            </script>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div>
                                                <input type="Button" value="ADD" class="btn btn-primary custom-button-color" data-ng-click="SaveA()" style="margin-right: 40px" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div>
                                                <input type="Button" value="cancel" class="btn btn-primary custom-button-color" data-ng-click="Cancel()" style="margin-right: 40px" />
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                           <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <div class="bxheader">
                                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                        </div>
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width:auto"></div>
                                    </div>
                                          <div class="col-md-3 col-sm-3 col-xs-3 pull-left">
                                            
                                                <div class="form-group" data-ng-class="{'has-error': ConsumeSpare.$submitted && ConsumeSpare.SPARE_REM.$invalid}">
                                                <label class="control-label">Requestor remarks </label>
                                                <textarea rows="1" cols="15" name="SPARE_REM" data-ng-model="ConsumeSpare.SPARE_REM" class="form-control" placeholder="Requestor remarks"></textarea>
                                                    <span class="error" data-ng-show="ConsumeSpare.$submitted && ConsumeSpare.CTY_NAME.$invalid" style="color: red">Please enter remarks </span>
                                            </div>
                                        </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 pull-right">
                                            <div>
                                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="SaveA()" style="margin-right: 40px" />
                                            </div>
                                        </div>
                           </div>
                            </form>
                              </div>
                            
                         
                        </div>
                   <div style="margin-top:10px;">
                     <a class="btn btn-danger homeButton" href="#" onclick="fnDetail('../../MaintenanceManagement/Views/MaintainanceDashboard.aspx')" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-home" aria-hidden="true"></i>HOME</a>
                                 &nbsp;<a class="btn btn-danger homeButton" href="#" onclick="goBack();" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>
                   </div>
                    </div>

                 
                </div>

           
            </div>
         
        </div>
   


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/ConsumeSpare.js"></script>

    <%--<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />--%>
  <%--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--%>
    <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://unpkg.com/sweetalert2@7.19.3/dist/sweetalert2.all.js"></script>

    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            ////$('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            ////$('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();

            var query = window.location.search == "" ? "" : window.location.search.substring(1).split('=')[1];
            if (query == "") {
                $('#hdCONSUME').val('0');
            }
            else {
                $('#hdCONSUME').val(query);
            }
            //$('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            //$('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        });
        function fnDetail(url) {
            //progress(0, 'Loading...', true);
            window.location = url;
        }
        function goBack() {
            //window.history.back();
            var strBackUrl = sessionStorage.getItem("strBackUrl");         
            window.location = strBackUrl;
        }
    </script>
</body>
</html>

