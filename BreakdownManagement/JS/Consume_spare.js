﻿app.service('Consume_Spare_Service', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetBreakDownTickets = function (lcmcode) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Consume/GetBreakDownTickets', lcmcode)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
  
}]);

app.controller('Consume_Spare_Controller', function ($scope, $q, $http, UtilityService, $timeout, $filter, BreakDownService, Consume_Spare_Service) {
    $scope.Locations = [];
    $scope.Equipment = [];

    var a = 'Qfms';

    var columnDefs = [
        { headerName: "Space ID", field: "BDMP_PLAN_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="navToMap(data)">{{data.BDMP_PLAN_ID}}</a>' },
        //{ headerName: "Ticket Id", field: "BDMP_PLAN_ID", width: 130, cellClass: 'grid-align', template: '<a ng-click="navToMap(data)">{{data.BDMP_PLAN_ID}}</a>'},
        { headerName: "Equipment", field: "SUBC_NAME", width: 130, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 140, cellClass: 'grid-align' },
        { headerName: "Requested Date", field: "BDMP_CREATED_DT", width: 130, cellClass: 'grid-align' },
        
    ];


    $scope.navToMap = function (det) {
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        enableColResize: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };


    $scope.PageLoad = function () {

        BreakDownService.getLocations().then(function (response) {
            if (response != null) {
                response.push({ LCM_CODE: '0', LCM_NAME: 'All' });
                filterFn(response, 'LCM_CODE');
                $scope.Locations = $scope.keys;
                //$scope.Location = response;
                angular.forEach($scope.Locations, function (value, key) {
                    if (key == 1) {
                        value.ticked = true;
                        $scope.getEquipmentFn('D', 'ALL');
                    }
                });
            }
        });

        $timeout($scope.Load, 500);

    }

    $scope.Load = function () {
        var lcmcode = ($scope.Consume_Spare_Controller.Locations[0].LCM_CODE);
        Consume_Spare_Service.GetBreakDownTickets(lcmcode).then(function (response) {
            if (response.Message = "Ok") {
                $scope.gridOptions.api.setRowData(response.Data);
            } else {
                console.log(' Get Error');
            }
        });
    }


    $scope.getEquipmentFn = function (typ, val) {
        var vals = '';
        if (typ == 'P') vals = ($scope.Consume_Spare_Controller.Locations[0].LCM_CODE == "0" ? 'ALL' : $scope.Consume_Spare_Controller.Locations[0].LCM_CODE);
        else vals = val;
        BreakDownService.getEquipment(vals).then(function (response) {
            if (response != undefined) {
                response.data.push({ GROUP_ID: 'A', GROUP_NAME: 'All' });
                filterFn(response.data, 'GROUP_ID');
                $scope.Equipment = $scope.keys;
                angular.forEach($scope.Equipment, function (value, key) {
                    if (key == 1) {
                        value.ticked = true;
                        //$scope.Equipment.push($scope.Equipment[0]);
                        //$scope.pgVar.getSparePartsFn();
                        //if (typ == 'D') {
                        //    $scope.pgVar.getBreakDownRqstsFn(typ);
                        //}
                    }
                    else { value.ticked = false; }
                });
            } else {
                console.log(' Equipment Names Get Error');
            }
        });
    }

    function filterFn(Arr, Key) {
        $scope.keys = []
        for (language in Arr) {
            $scope.keys.push(Arr[language])
        }
        $scope.keys = $filter('orderBy')($scope.keys, Key, false);
        return $scope.keys
    }
    $scope.PageLoad();
});
