﻿
app.service("ConsumeSpareService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetAssetDetails = function (data) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConsumeSpare/GetAssetDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
    this.GetTicketDetails = function (data) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConsumeSpare/GetTicketDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
    this.GetDescrption = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConsumeSpare/GetDescrption', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
    this.GetCode = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConsumeSpare/GetCode', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
    this.GetCost = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConsumeSpare/GetCost', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
    this.GetQuantity = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConsumeSpare/GetQuantity', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConsumeSpare/SaveData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };

}]);
app.controller('ConsumeSpareController', ['$scope', '$q', '$http', 'ConsumeSpareService', 'UtilityService', '$timeout', '$filter',

    function ($scope, $q, $http, ConsumeSpareService, UtilityService, $timeout, $filter) {

        $scope.ConsumeSpare = {};
        $scope.gridata = [];
        $scope.Priority = [];
        $scope.Locations = [];
        $scope.Type = [];
        $scope.Descrption = [];
        $scope.Code = [];
        $scope.Inventory = [];
        $scope.Quantity = [];
        $scope.DetailsGrid = false;
        $scope.MainGrid = true;
        $scope.ASP_REQ_ID_DTLS = "";
        $scope.consume10 = true;

        $scope.columnDefs = [
            { headerName: "Select", field: "ticked", suppressMenu: true, width: 50, filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' />", cellClass: 'grid-align' },
            { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 120 },
            { headerName: "Spare Name", field: "AAS_SPAREPART_NAME", cellClass: 'grid-align', width: 140 },
            { headerName: "Spare Description", field: "AAS_SPAREPART_DES", cellClass: 'grid-align', width: 150 },
            { headerName: "Vendor", field: "AAS_AAT_CODE", cellClass: 'grid-align', width: 150 },
            { headerName: "Available Qty", field: "AAS_SPAREPART_AVBQUANTITY", cellClass: 'grid-align', width: 120 },
            { headerName: "Quantity", field: "Qty", width: 100, template: "<input type='number' ng-model='data.Qty' />", },
            //{ headerName: "SpareCost", field: "Cost", width: 150, template: "<input type='number' min='1' ng-model='data.Cost' readonly />", },
            { headerName: "SpareCost", field: "Cost", cellClass: 'grid-align', width: 110, template: "{{fn_Set(data.AAS_SPAREPART_COST,data.Qty)}}", },
            { headerName: "Measure(Unit)", field: "AAS_SPAREPART_QUANTITY", width: 130, cellClass: 'grid-align' },
            { headerName: "Spare ID", field: "AAS_SPAREPART_ID", cellClass: 'grid-align', width: 150, hide: "true" },
            //{ headerName: "Cost", field: "AAS_SPAREPART_COST", cellClass: 'grid-align', width: 150, hide: "true" },
        ]




        $scope.gridOptions = {
            columnDefs: $scope.columnDefs,
            enableCellSelection: false,
            enableFilter: true,
            rowData: [],
            enableSorting: true,
            angularCompileRows: true,
            rowSelection: 'multiple',
            enableColResize: true,
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit()
            },

        }

        $("[type='number']").keypress(function (evt) {
            evt.preventDefault();
        });


        $scope.LoadData = function () {
            $scope.consume10 = true;
            $scope.consume20 = false;
            //UtilityService.getLocations(2).then(function (response) {
            UtilityService.getLocationsInventory(2).then(function (response) {
                if (response.data !== null) {
                    $scope.Locations = response.data;
                    var lcm = $('#hdCONSUME').val();//$location.search();                   
                    angular.forEach($scope.Locations, function (value, key) {
                        if (lcm == '0') {
                            var a = _.find($scope.Locations);
                            a.ticked = true;
                        }
                        else {
                            var lcmName = lcm.replace("%20", " ");                            
                            if (value.LCM_NAME.toString() == lcmName) {
                                value.ticked = true;
                            }
                        }

                    });

                    $scope.GetAssetDetails();
                }

            });
        }

        $scope.AddItem = function () {
            $scope.consume10 = false;

            $scope.GetDescrption();
        }

        $scope.Cancel = function () {
            $scope.consume10 = true;
            $scope.consume20 = false;
        }



        $scope.fn_Set = function (cost, qty) {
            return cost == "" ? 0 : parseFloat(cost * qty).toFixed(2);

        }



        $scope.GetAssetDetails = function () {

            var obj = {
                LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),

            }
            ConsumeSpareService.GetAssetDetails(obj).then(function (response) {

                if (response.data != null) {
                    $scope.Type = response.data;
                    angular.forEach($scope.Type, function (value, key) {
                        var a = _.find($scope.Type);
                        a.ticked = true;

                    });
                    $scope.GetTicketDetails();
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.GetTicketDetails = function () {
            var obj1 = {
                LCM_CODE: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                AST_SUBCAT_NAME: _.filter($scope.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_CODE; }).join(',')
            }
            ConsumeSpareService.GetTicketDetails(obj1).then(function (response) {               
                if (response.data != null) {
                    $scope.Ticket = response.data;
                    angular.forEach($scope.Ticket, function (value, key) {
                        var a = _.find($scope.Ticket);
                        a.ticked = true;

                    });
                    setTimeout(function () { $scope.GetDescrption(); }, 500);
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }
        $scope.GetDescrption = function () {
            var obj2 = {
                LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                //AAS_AAT_CODE: _.filter($scope.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_CODE; }).join(',')
            }

            ConsumeSpareService.GetDescrption(obj2).then(function (response) {

                if (response.data.length != 0) {
                    $scope.consume20 = true;
                    $scope.gridOptions.api.setRowData(response.data);
                }
                else {
                    $scope.consume20 = false;
                    showNotification('error', 8, 'bottom-right', 'No data for selected equipment');
                }

            }, function (response) {
                progress(0, '', false);
            });
        }



        //$scope.GetCode = function () {
        //    var object = {
        //        LCM_NAME: _.filter($scope.ConsumeSpare.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_NAME; }).join(','),
        //    }
        //    ConsumeSpareService.GetCode(object).then(function (response) {

        //        if (response.data != null) {
        //            $scope.Code = response.data;
        //            angular.forEach($scope.Code, function (value, key) {
        //                var a = _.find($scope.Code);
        //                a.ticked = true;
        //            });
        //            $scope.GetCost();
        //        }
        //        else
        //            showNotification('error', 8, 'bottom-right', response.Message);
        //    }, function (response) {
        //        progress(0, '', false);
        //    });
        //}
        //$scope.GetCost = function () {
        //    debugger;

        //    var obj3 = {
        //        LCM_NAME: _.filter($scope.ConsumeSpare.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_NAME; }).join(','),
        //    }

        //    ConsumeSpareService.GetCost(obj3).then(function (response) {

        //        if (response.data != null) {
        //            $scope.Inventory = response.data;
        //            angular.forEach($scope.Inventory, function (value, key) {
        //                var a = _.find($scope.Inventory);
        //                a.ticked = true;
        //            });
        //            $scope.GetQuantity();
        //        }
        //        else
        //            showNotification('error', 8, 'bottom-right', response.Message);
        //    }, function (response) {
        //        progress(0, '', false);
        //    });
        //}
        //$scope.GetQuantity = function () {

        //    var obj4 = {
        //        LCM_NAME: _.filter($scope.ConsumeSpare.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_NAME; }).join(','),
        //    }

        //    ConsumeSpareService.GetQuantity(obj4).then(function (response) {

        //        if (response.data != null) {
        //            $scope.Quantity = response.data;
        //            angular.forEach($scope.Quantity, function (value, key) {
        //                var a = _.find($scope.Quantity);
        //                a.ticked = true;
        //            });

        //        }
        //        else
        //            showNotification('error', 8, 'bottom-right', response.Message);
        //    }, function (response) {
        //        progress(0, '', false);
        //    });
        //}

        $scope.SaveA = function () {           
            if ($scope.ConsumeSpare.SPARE_REM == undefined || $scope.ConsumeSpare.SPARE_REM == "") {
                showNotification('error', 8, 'bottom-right', 'Please Enter Remarks');
                return;
            }
            $scope.FinalSelectedSpares = [];
            //$scope.FinalSelected = [];
            var hTable = "<table id='tbl_userListHist' class='table table-condensed table - bordered table - hover table - striped'>";
            hTable += "<tr><th style='text-align:center'>SPARE NAME</th><th style='text-align:center'>DESCRIPTION</th><th style='text-align:center'>QUANTITY</th><th style='text-align:center'>COST</th></tr>";
            hTable += "<tbody>";
            var totqty = 0;
            var totcost = 0;
            angular.forEach($scope.gridOptions.rowData, function (value, key) {

                if (value.ticked == true) {
                    if (value.Qty == undefined) {
                        progress(0, '', false);
                        setTimeout(function () { showNotification('error', 8, 'bottom-right', 'Please use arrows keys'); }, 500);
                        return;
                    }
                    if (parseFloat(value.Qty) <= 0) {
                        progress(0, '', false);
                        setTimeout(function () { showNotification('error', 8, 'bottom-right', 'Please enter more than zero'); }, 500);
                        return;
                    }
                    if (parseFloat(value.AAS_SPAREPART_AVBQUANTITY) < parseFloat(value.Qty)) {
                        progress(0, '', false);
                        setTimeout(function () { showNotification('error', 8, 'bottom-right', 'Please enter less quanity'); }, 500);                        
                        return;
                    }
                    //$scope.FinalSelectedSpares.push({ 'SPAREPARTCODE': value.AAS_SPAREPART_ID, 'CATEGORY': '', 'SUBCATCODE': '', 'BRNDCODE': '', 'MODELCODE': value.AAS_SPAREPART_COST.toString(), 'QTY': value.Qty.toString() });
                    $scope.FinalSelectedSpares.push({ 'SPAREPARTCODE': value.AAS_SPAREPART_ID, 'CATEGORY': '', 'SUBCATCODE': '', 'BRNDCODE': '', 'MODELCODE': (parseFloat(value.Qty) * parseFloat(value.AAS_SPAREPART_COST)).toFixed(2).toString(), 'UNITCOST': value.AAS_SPAREPART_COST.toString(), 'QTY': value.Qty.toString() });
                    //$scope.FinalSelected.push({ 'SPARE DESCRIPTION': value.AAS_SPAREPART_DES, 'QUANTITY': value.Qty.toString() });

                    hTable += "<tr>";
                    hTable += "<td>" + value.AAS_SPAREPART_NAME + "</td>";
                    hTable += "<td>" + value.AAS_SPAREPART_DES + "</td>";
                    hTable += "<td>" + value.Qty.toString() + "</td>";
                    totqty += parseFloat(value.Qty);
                    var cost = (parseFloat(value.Qty) * parseFloat(value.AAS_SPAREPART_COST));
                    totcost += parseFloat(cost);
                    hTable += "<td>" + (parseFloat(value.Qty) * parseFloat(value.AAS_SPAREPART_COST)).toString() + "</td>";
                    hTable += "</tr>";
                }
            });
            hTable += "<tr>";
            hTable += "<td colspan='2'><b>Total</b></td>";
            hTable += "<td>" + totqty.toFixed(2).toString() + "</td>";
            hTable += "<td>" + totcost.toFixed(2).toString() + "</td>";
            hTable += "</tr>";
            hTable += "</tbody></table>";          
            var obj = {
                ASP_REQ_LOC: $scope.ConsumeSpare.Locations[0].LCM_CODE,
                ASP_REMARKS: $scope.ConsumeSpare.SPARE_REM,
                ASP_MAINT_ID: $scope.ConsumeSpare.Ticket == '' ? '' : $scope.ConsumeSpare.Ticket[0].BDMP_PLAN_ID,
                //ASP_MAINT_ID: 0,
                COST_OF_INVENTORY: $scope.ConsumeSpare.COST_OF_INVENTORY,
                SPAREPARTDETAILS: $scope.FinalSelectedSpares,
                FILENAME: ''
            }

            if (obj.SPAREPARTDETAILS.length != 0) {
                //if (confirm("Do You Want To Save  This Spare " + JSON.stringify($scope.FinalSelected))) {
                //var json = JSON.stringify($scope.FinalSelected);
                //var tr;
                //for (var i = 0; i < json.length; i++) {
                //    tr = $('<tr/>');
                //    tr.append("<td>" + json[i].User_Name + "</td>");
                //    tr.append("<td>" + json[i].score + "</td>");
                //    tr.append("<td>" + json[i].team + "</td>");
                //    $('table').append(tr);
                //}

                //var json = JSON.stringify($scope.FinalSelected);
                ////var hTable = "<table id='tbl_userListHist'>";
                ////hTable += "<tr style='background-color:#f39c12; color:#fff'><th>SPARE DESCRIPTION</th><th>QUANTITY</th></tr>";
                ////hTable += "<tbody>";
                //alert(json);
                //alert(json.length);
                //for (var i = 0; i < json.length; i++) {

                //    //hTable += "<tr>";                  
                //    //hTable += "<td>" + dataH[i].str_payment_type + "</td><td align='center'>" + parseFloat(dataH[i].dcAmount) + "</td><td align='center'>" + dataH[i].strPaymentDate + "</td>";
                //    //hTable += "<td>" + dataH[i].strPaymentMode + "</td><td>" + dataH[i].str_payment_status + "</td><td align='center'>" + dataH[i].strcheqRtgsNO + "</td><td>" + dataH[i].strBankNm + "</td>";
                //    //hTable += "<td>" + dataH[i].strBranchNm + "</td>";
                //    //hTable += "<td align='center'>";
                //    //hTable += dataH[i].dcAmount > 0 ? "<a href='/DocPrint/CustomerReceipt/" + dataH[i].numBookingID + '~' + (dataH[i].numTranID) + "' title='Print Customer Receipt' target='_blank'><i class='fa fa-print' aria-hidden='true'></i></a>" : "";
                //    //hTable += "</td>";
                //    //hTable += "</tr>";
                //    //srno++;
                //}

                //var swal_html = '<div class="panel" style="background:aliceblue;font-weight:bold"><div class="panel-heading panel-info text-center btn-info"> <b>Import Status</b> </div> <div class="panel-body"><div class="text-center"><b><p style="font-weight:bold">Total number of not inserted  rows : add data</p><p style="font-weight:bold">Row numbers:Add data</p></b></div></div></div>';
                //var htable = JSON.stringify($scope.FinalSelected).replace("[{", "").replace("}]", "").replace('"', '').replace("},{", ",").replace('":"', '=').replace('","',',');
                Swal.fire({
                    //title: "Are you sure?",
                    ////text: hTable,
                    //icon: "warning",
                    //html: hTable,
                    //buttons: [
                    //    'No, cancel it!',
                    //    'Yes, I am sure!'
                    //],
                    //dangerMode: true,

                    title: "Please Confirm the updated spare?",
                    html: hTable,
                    showDenyButton: true,
                    icon: "warning",
                    customClass: 'swal-wide',
                    showCancelButton: true,
                    confirmButtonText: 'Confirm',
                }).then((result) => {
                    if (result.value) {
                        ConsumeSpareService.SaveData(obj).then(function (response) {
                            progress(0, '', true);
                            if (response.data != null) {
                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                                $scope.consume20 = false;
                                $scope.gridOptions.api.refreshView();
                                $scope.Clear();
                            }
                            else {
                                $scope.gridOptions.api.setRowData([]);
                                $scope.Clear();
                                showNotification('error', 8, 'bottom-right', response.Message);
                            }

                        }, function (response) {
                            progress(0, '', false);

                        });
                    }
                });
                //}
            }
            else {
                //showNotification('error', 8, 'bottom-right', 'Please select checkbox');
                setTimeout(function () { showNotification('error', 8, 'bottom-right', 'Please select checkbox'); }, 10);
            }

        }

        $scope.Clear = function () {

            angular.forEach($scope.Locations, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Type, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Ticket, function (value, key) {
                value.ticked = false;
            });
            $scope.gridOptions.api.setRowData([]);
            $scope.ConsumeSpare.SPARE_REM = '';


        }


        //$scope.GenerateFilterExcel = function () {
        //    progress(0, 'Loading...', true);
        //    var Filterparams = {
        //        columnGroups: true,
        //        allColumns: true,
        //        onlySelected: false,
        //        columnSeparator: ',',
        //        fileName: "ConsumeSpare_Report.csv"
        //    };
        //    $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        //    setTimeout(function () {
        //        progress(0, 'Loading...', false);
        //    }, 1000);
        //}

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();
        }
        $("#filtertxt").change(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keydown(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keyup(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).bind('paste', function () {
            onReq_SelSpacesFilterChanged($(this).val());
        });
        function onReq_SelSpacesFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
        }
        setTimeout(function () { $scope.LoadData(); }, 1000);
    }]);
