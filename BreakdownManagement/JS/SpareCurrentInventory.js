﻿app.service("SpareCurrentInventoryService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetData = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpareCurrentInventory/GetData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetAssetDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpareCurrentInventory/GetAssetDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpareCurrentInventory/SearchData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('SpareCurrentInventoryController', ['$scope', '$q', '$http', 'SpareCurrentInventoryService', 'UtilityService', '$timeout', '$filter',

    function ($scope, $q, $http, SpareCurrentInventoryService, UtilityService, $timeout, $filter) {

        $scope.SpareCurrentInventory = {};
        $scope.gridata = [];
        $scope.Priority = [];
        $scope.Locations = [];
        $scope.Type = [];

        //const cellClassRules = {
        //    "cell-pass": params => params.value != 'Low in Stock',
        //    "cell-less": params => params.value == 'Low in Stock',
        //    "cell-fail": params => params.value == 'Out Of Stock'
        //};

        const cellClassRules = {
            "cell-pass": params => params.value == 'Avalible Stock',
            "cell-less": params => params.value == 'Low in Stock',
            "cell-fail": params => params.value == 'Out Of Stock'
        };


        var columnDefs = [
            { headerName: "SN",field:"SR_NO",width:27},
            { headerName: "Status", field: "Status", width: 52, cellClass: 'grid-align', cellClassRules: cellClassRules ,},
            //{ headerName: "Image", field: "IMAGEPATH", width: 150, cellClass: 'grid-align', filter: 'set', template:'<img src="{{data.IMAGEPATH}}" style="height:20px;width:20px;" />' },
            { headerName: "Image", field: "IMAGEPATH", width: 44, cellClass: 'grid-align', filter: 'set', template: '<a ng-click="showLargeImagePopup({{data.IMAGEPATH}})"><img style="width:30px;height:30px;border-radius:5%" src="{{data.IMAGEPATH}}" /></a>' },
            //{ headerName: "Image", field: "IMAGEPATH", width: 150, cellClass: 'grid-align', filter: 'set', template: '<a onclick="angular.element(this).scope().showLargeImagePopup(\"" + {{data.IMAGEPATH}} + "\")"><img style="width:30px;height:30px;border-radius:5%" src="{{data.IMAGEPATH}}" /></a>' },
            { headerName: "Vendor", field: "AAS_AAT_CODE", width: 52, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Location", field: "LCM_NAME", width: 61, cellClass: 'grid-align', cellRenderer: tooltip }, 
            { headerName: "Priority", field: "VED", width: 52, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "QFMS - Part Number", field: "AAS_ASP_CODE", width: 59, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Part Name", field: "SPARE_NAME", width: 42, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Spare Part No", field: "AAS_SPAREPART_NUMBER", width: 58, cellClass: "grid-align" },
            { headerName: "Part Specification", field: "DESCRIPTION", width: 79, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Make", field: "AAS_MODEL_ID", width: 41, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Supplier", field: "AAS_AAB_NAME", width: 59, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Storage Location", field: "AAS_RACK_NO", width: 59, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "MSQ (min Stock QTY)", field: "MINIQTY", width: 41, cellClass: 'grid-align' },
            { headerName: "ASQ (Actual Stock QTY)", field: "AVAILQTY", width: 50, cellClass: 'grid-align' },
            { headerName: "Measure (Unit)", field: "AAS_SPAREPART_QUANTITY", width: 60, cellClass: 'grid-align' },
            { headerName: "Safety Stock (MSQ-ASQ)", field: "SAFEQTY", width: 49, cellClass: 'grid-align' },             
            { headerName: "Unit Cost", field: "SP_UNITCOST", width: 46, cellClass: 'grid-align' },
            { headerName: "Total Cost", field: "AAS_SPAREPART_TOTCOST", width: 44, cellClass: 'grid-align' },
            { headerName: "Lead Time", field: "ASS_LEAD_TIME", width: 43, cellClass: "grid-align" },
          
            //{ headerName: "Remarks", field: "AAS_REMARKS", width: 100, cellClass: 'grid-align' },
            //{ headerName: "Rack", field: "AAS_RACK_NO", width: 150, cellClass: 'grid-align' },


        ];

        //  code start for add footer which shows total for unit cost and total coast 
        //generatePinnedBottomData() {
            
        //    let result = {};

        //    this.gridColumnApi.getAllGridColumns().forEach(item => {
        //        result[item.colId] = null;
        //    });
        //    return this.calculatePinnedBottomData(result);
        //}
        //code finsh
        $scope.gridOptions = {
            columnDefs: columnDefs,
            enableColResize: true,
            enableSorting: true,
            enableScrollbars: false,
            enableFilter: true,
            angularCompileRows: true,
            enableCellSelection: false,
            groupIncludeFooter: true,
            groupIncludeTotalFooter: true,
            headerHeight: 90,
            rowHeight: 100, 
            //getRowHeight: function (params) {               
            //    return 18 * (Math.floor(params.data.myDataField.length / 45) + 1);
            //},
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();             
                //$scope.gridOptions.autoSizeRows(0, $scope.gridOptions.rows.length - 1, false, 20);
            },
            
        };

        
        function tooltip(params) {
            return `<label title="${params.value}" >${params.value}</label> `
        }

        //function showLargeImagePopup(_imagepath) {
        $scope.showLargeImagePopup = function (_imagepath) {
            alert(_imagepath);
            $('#viewImage').modal('show');
            $('#proimglarg').attr("src", _imagepath);
        }

        $scope.SearchA = function (stock) {
            var obj = {
                LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                SUBC_NAME: _.filter($scope.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_NAME; }).join(','),
            }

            SpareCurrentInventoryService.SearchData(obj).then(function (response) {

                if (response.data != null) {
                    $scope.gridOptions.api.setRowData([]);
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData(response.data);
                    var totStock = 0;
                    var totCost = 0;
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        totStock = totStock + parseFloat(value.AVAILQTY);
                        totCost = totCost + parseFloat(value.AAS_SPAREPART_TOTCOST);
                    });                   
                    $scope.SpareCurrentInventory.Stock = parseFloat(totStock).toFixed(2);
                    $scope.SpareCurrentInventory.TotCost = parseFloat(totCost).toFixed(2);                  
                    if (stock != '') {
                        var stk = stock == "GoodStock" ? "Avalible Stock" : stock == "LowStock" ? "Low in Stock" : "Out Of Stock";                       
                        $("#filtertxt").val(stk);
                        onReq_SelSpacesFilterChanged(stk);
                    }
                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData([]);
                    $scope.SpareCurrentInventory.Stock = 0;
                    $scope.SpareCurrentInventory.TotCost = 0;
                }
            }, function (response) {
                progress(0, '', false);

            });
            //$scope.LoadData();
        }

        $scope.LoadData = function () {

            SpareCurrentInventoryService.GetData().then(function (data) {
                if (data.data == null) {
                    $scope.gridOptions.api.setColumnDefs(data.columnDefs);
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Records Found');
                }
                else {
                    showNotification('', 8, 'bottom-right', '');
                    $scope.gridOptions.api.setColumnDefs(data.columnDefs);
                    $scope.gridOptions.api.setRowData(data.data);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000);

                }
                var searchParams = new URLSearchParams(window.location.search);
                if (searchParams.get('Status')) {
                    $scope.filterFn(searchParams.get('Status'))
                }
                var searchParams = new URLSearchParams(window.location.search);
                if (searchParams.get('status')) {
                    $scope.filter(searchParams.get('status'))
                }

            });

        }

        $scope.filterFn = function (value) {
            $scope.gridOptions.api.setQuickFilter(value);
        }

        UtilityService.getLocationsInventory(2).then(function (response) {
            //UtilityService.getLocations(2).then(function (response) {
            var stock=''
            if (response.data != null) {
                response.data.push({ LCM_CODE: 'All', LCM_NAME: 'All' });
                $scope.Locations = response.data;
                var lcm = $('#hdCURRENT').val();
                angular.forEach($scope.Locations, function (value, key) {
                    if (lcm == '0') {
                        if (key == '0') {
                            value.ticked = true;
                        }
                    }
                    else {
                        stock = lcm.split('&')[1].split('=')[1];
                        var lcmName = lcm.split('&')[0].split('=')[1].replace("%20", " ");
                        if (value.LCM_NAME.toString() == lcmName) {
                            value.ticked = true;
                        }
                    }
                })
                $scope.GetAssetDetails(stock);
            }
        });

        $scope.GetAssetDetails = function (stock) {
            var objs = {
                LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
            }
            SpareCurrentInventoryService.GetAssetDetails(objs).then(function (response) {

                if (response.data != null) {
                    response.data.push({ SUBC_CODE: 'All', SUBC_NAME: 'All' });
                    $scope.Type = response.data;
                    angular.forEach($scope.Type, function (value, key) {
                        //var a = _.find($scope.Type);
                        //a.ticked = true;
                        if (value.SUBC_CODE == 'All') {
                            value.ticked = true;
                        }
                    });
                    $scope.SearchA(stock);
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }



        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);
            var Filterparams = {
                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "currentInventory_Report.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();
        }
        $("#filtertxt").change(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keydown(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keyup(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).bind('paste', function () {
            onReq_SelSpacesFilterChanged($(this).val());
        });
        function onReq_SelSpacesFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
        }
        //setTimeout(function () { $scope.LoadData(); }, 1000);

    }]);
