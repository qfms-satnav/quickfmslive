﻿
Imports System.Data
Imports System.IO
Imports System.Configuration.ConfigurationManager
Imports System.Data.SqlClient

Partial Class Logcdesso
    Inherits System.Web.UI.Page
    Public Function logerrors(ByVal [error] As String)
        Dim filename As String = "Log_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt"

        Dim filepath As String = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") & filename)

        If File.Exists(filepath) Then
            Using stwriter As New StreamWriter(filepath, True)
                stwriter.WriteLine("-------------------START-------------" + DateTime.Now)
                stwriter.WriteLine([error])
                stwriter.WriteLine("-------------------END-------------" + DateTime.Now)
            End Using
        Else
            Dim stwriter As StreamWriter = File.CreateText(filepath)
            stwriter.WriteLine("-------------------START-------------" + DateTime.Now)
            stwriter.WriteLine([error])
            stwriter.WriteLine("-------------------END-------------" + DateTime.Now)
            stwriter.Close()
        End If

        Return [error]
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If (Not IsPostBack) Then

                'CreateLogFile("SSo Start.!")
                Session.Clear()
                'CreateLogFile("SSO 0")

                Dim Sessioncompany As String
                Sessioncompany = Page.Request.QueryString("company")
                'CreateLogFile("SSO Step 1:: CompanyID:-" + Sessioncompany)

                If (Sessioncompany = "CDE-VU") Then
                    Dim empid As String = Page.Request.QueryString("empid")
                    Dim empmail As String = Page.Request.QueryString("email")
                    Dim Tenant_Id1 As String = ValidateTenant(Sessioncompany)
                    'CreateLogFile("SSO Step 2:: empid- " + empid + " empmail:-" + empmail + " Tenant_Id1:-" + Tenant_Id1)
                    Session("TENANT") = Tenant_Id1
                    Session("UID") = empid
                    'txtUsrId.Text = Session("UID")
                    'CreateLogFile("351  TENANT- " + Session("TENANT").ToString() + " UID :-" + Session("UID").ToString())

                    Session("useroffset") = "+05:30"
                    Dim uname = Session("UID")
                    Session("uname") = Session("UID")
                    Dim staid As String = ""

                    Dim COMPANYID As String
                    Dim Tenant_Active As Integer

                    ''Tenant_Active = ValidateIsTenantActive(Request.QueryString("tenant"))
                    Tenant_Active = ValidateIsTenantActive(Session("TENANT").ToString())
                    'CreateLogFile("360 Tenant_Active : " + Tenant_Active.ToString())
                    If Tenant_Active = "0" Then
                        lbl1.Text = "Account has been InActive!"
                        'pnlLic.Visible = False
                        'pnlMain.Visible = True
                        Exit Sub
                    End If

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Get_UserID_By_Email")
                    ''sp1.Command.AddParameter("@Mail", empmail, DbType.String)
                    sp1.Command.AddParameter("@Mail", empid, DbType.String)
                    Dim ds1 As New DataSet
                    ds1 = sp1.GetDataSet()
                    If (ds1.Tables(0).Rows.Count = "0") Then
                        'Server.Transfer("~/login.aspx")
                        lbl1.Text = "User Not exists in tool"
                        'CreateLogFile("User Not exists in tool")
                        Exit Sub
                    Else
                        'CreateLogFile("User exists")
                        Session("UID") = ds1.Tables(0).Rows(0).Item("AUR_ID")
                        uname = Session("UID")
                        Session("uname") = Session("UID")
                        FormsAuthentication.Initialize()
                        Dim authTicket As New FormsAuthenticationTicket(Session("UID").ToString(), True, 60)
                        Dim encryptedticket As String = FormsAuthentication.Encrypt(authTicket)
                        Dim authcookie As New HttpCookie(FormsAuthentication.FormsCookieName, encryptedticket)
                        authcookie.Path = ""
                        COMPANYID = ValidateCompayID(uname)
                        'CreateLogFile("CompanyID :: - " + COMPANYID.ToString())
                        If isMobileBrowser() = True Then
                            Dim script As String = String.Format("alert('{0}');", "hello")
                            Page.ClientScript.RegisterClientScriptBlock(Page.[GetType](), "alert", script, True)
                            Response.Redirect("WebFiles/MobileDetect.aspx?UserId=" + Session("UID") + "&CompanyId=CDE-VU")
                        Else

                            If (COMPANYID <> "NoUser") Then

                                'CreateLogFile("CompanyID :: - " + COMPANYID.ToString() + " UID:-" + Session("UID").ToString())

                                getuseroffsetandculture()
                                Session("COMPANYID") = COMPANYID

                                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CHECK_CATEGORY_DEPRECIATION")
                                Dim ds As New DataSet
                                ds = sp.GetDataSet()
                                Session("DepMethod") = ds.Tables(0).Rows(0).Item("returnstatus")

                                'CreateLogFile("DepMethod :: - " + Session("DepMethod").ToString())

                                Response.Cookies.Add(authcookie)
                                Session("LoginTime") = getoffsetdatetime(DateTime.Now)

                                'CreateLogFile("LoginTime :: - " + Session("LoginTime").ToString())

                                displaybsmdata()
                                GET_ASSET_MODULE_CHECK_FOR_USER()
                                GetuserRoleMappingEdit()

                                Dim stl As String = Validatelicence()
                                If stl = "0" Then
                                    'CreateLogFile("stl :: - " + stl.ToString())
                                    Response.Redirect("~/frmAMTDefault.aspx", False)
                                Else
                                    Server.Transfer("~/frmLicense.aspx")
                                End If
                            Else
                                Server.Transfer("~/UserNotExists.aspx")
                            End If

                        End If
                    End If
                End If


            End If

        Catch ex As Exception
            logerrors("Exception :" + ex.ToString())
        End Try
    End Sub
    Public Function ValidateTenant(ByVal Tenant_Name As String) As String
        Dim Valid_Tenant_Id As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "CHECK_USER_TENANT_ISVALD")
        sp.Command.AddParameter("@TENANT_NAME", Tenant_Name, DbType.String)
        Session("useroffset") = "+05:30"
        Valid_Tenant_Id = sp.ExecuteScalar
        Return Valid_Tenant_Id
    End Function
    Public Function Validatelicence() As String
        Dim ValidateStatus As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "VALIDATE_USER_EXPIRY")
        sp.Command.AddParameter("@TID", Session("Tenant"), DbType.String)
        ValidateStatus = sp.ExecuteScalar
        Return ValidateStatus
    End Function


    Public Function ValidateCompayID(ByVal User_ID As String) As String
        Dim Company_Id As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADM_CHECK_USER_COMPANY_ID")
        sp.Command.AddParameter("@AUR_ID", User_ID, DbType.String)
        Company_Id = sp.ExecuteScalar
        Return Company_Id
    End Function
    Public Function Validatelicence(ByVal UserName As String, ByVal Password As String) As String
        Dim ValidateStatus As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "VALIDATE_USER_EXPIRY")
        sp.Command.AddParameter("@TID", Session("Tenant"), DbType.String)
        ValidateStatus = sp.ExecuteScalar
        Return ValidateStatus
    End Function
    Public Sub GetuserRoleMappingEdit()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USERROLEMAPPINGEDIT")

        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("USER_EDIT") = ds.Tables(0).Rows(0).Item("SYSP_VAL1")
        End If
    End Sub
    Public Sub GET_ASSET_MODULE_CHECK_FOR_USER()
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "GET_ASSET_MODULE_CHECK_FOR_USER")
        sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Dim asset = ds.Tables(0).Rows(0).Item("T_STA_ID")
            If asset = 1 Then
                Get_asset_sysp_value()
            End If
        End If
    End Sub

    Public Sub Get_asset_sysp_value()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AST_SYSP_VALUE")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("Procurement") = ds.Tables(0).Rows(0).Item("AST_SYSP_VAL1")
        End If
    End Sub
    Public Function ValidateIsTenantActive(ByVal Tenant_Name As String) As String
        Dim ValidateTenantStatus As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "VALIDATE_TENANT_ACTIVE")
        sp.Command.AddParameter("@TID", Session("Tenant"), DbType.String)
        ValidateTenantStatus = sp.ExecuteScalar
        Return ValidateTenantStatus
    End Function
    Public Sub getuseroffsetandculture()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_OFFSET_CULTURE_BYID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("useroffset") = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            Session("userculture") = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Session("usercountry") = Convert.ToString(ds.Tables(0).Rows(0).Item("AUR_COUNTRY"))
            Session("location") = Convert.ToString(ds.Tables(0).Rows(0).Item("AUR_LOCATION"))
            'Dim useroffsetcookie As HttpCookie = New HttpCookie("useroffset")
            'useroffsetcookie.Value = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            'Response.Cookies.Add(useroffsetcookie)
            Dim userculturecookie As HttpCookie = New HttpCookie("userculture")
            userculturecookie.Value = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Response.Cookies.Add(userculturecookie)

        End If
    End Sub



    Public Function isMobileBrowser() As Boolean
        Dim context As HttpContext = HttpContext.Current

        If context.Request.Browser.IsMobileDevice Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_X_WAP_PROFILE") IsNot Nothing Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_ACCEPT") IsNot Nothing AndAlso context.Request.ServerVariables("HTTP_ACCEPT").ToLower().Contains("wap") Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_USER_AGENT") IsNot Nothing Then
            Dim mobiles As String() = {"midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows ce", "mmp/", "blackberry", "mib/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-", "SEC-", "samsung", "HTC", "mot-", "mitsu", "sagem", "sony", "alcatel", "lg", "eric", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "dddi", "moto", "iphone"}

            For Each s As String In mobiles

                If context.Request.ServerVariables("HTTP_USER_AGENT").ToLower().Contains(s.ToLower()) Then
                    Return True
                End If
            Next
        End If


        Return False
    End Function

    Public Shared Function CreateLogFile(ByVal message As String) As Boolean
        Try
            'string location = @"C://IRPC//myfile1.txt";
            Dim filename As String = "NewLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt"
            Dim location As String = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") & filename) '''(System.Environment.CurrentDirectory + ("\log.txt"))
            If Not File.Exists(location) Then
                Dim fs As FileStream
                fs = New FileStream(location, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                fs.Close()
            End If

            Console.WriteLine(message)
            'Release the File that is created
            Dim sw As StreamWriter = New StreamWriter(location, True)
            sw.Write((message + Environment.NewLine))
            sw.Close()
            sw = Nothing
            Return True
        Catch ex As Exception
            'EventLog.WriteEntry("MIDocShare", ("Error in CreateLogFile" + ex.Message.ToString), EventLogEntryType.Error, 6000)
            Return False
        End Try

    End Function

    Public Sub displaybsmdata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_BSM_GETALL")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("Parent") = ds.Tables(0).Rows(0).Item("AMT_BSM_PARENT")
            Session("Child") = ds.Tables(0).Rows(0).Item("AMT_BSM_CHILD")
            'Session("GHT") = ds.Tables(1).Rows(0).Item("ASM_PARENT")
            If ds.Tables(1).Rows.Count > 0 Then
                Session("GHT") = ds.Tables(1).Rows(0).Item("ASM_PARENT")
            End If
        End If
    End Sub




End Class
