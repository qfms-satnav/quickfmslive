﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Kotaklogin.aspx.vb" Inherits="Kotaklogin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="google-signin-client_id" content="93729296875-t61jnp8chpjq3744625lab367o2sgn06.apps.googleusercontent.com">



    <title>QuickFMS :: Total Infrastructure Control</title>
    <%=ScriptCombiner.GetScriptTags("login_scripts", "css", 1)%>

   

    <!--[if lt IE 9]>
        <script defer src="BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
            color: black;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 140px;
        }
    </style>
    <style>
        #txtUsrPwd {
            -webkit-text-security: disc;
        }
    </style>
    <style>
        .btn-primary:hover, .btn-primary:focus, .btn-primary.focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
            color: white;
            background-color: #2caa9d;
            border-color: #2caa9d;
        }
    </style>
</head>
<body>
    <main class="auth-main">
        <div class="auth-block">
        <div class="col-md-10">
            <img src="BootStrapCSS/images/logo_quick.gif" alt="logo" style="margin-left: 150px; margin-right: 150px; height: 50px;" />
        </div>
   
      <%--  </div>--%>
        <div class="col-md-2">
            <i class="fa fa-lock fa-5x" aria-hidden="true"></i>
        </div>
        <br />
        <br />
        <form id="loginform" class="form-horizontal" role="form" runat="server" defaultfocus="tenantID">
                    
            <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSignIn">
                <%--<asp:Label ID="lbl1" runat="server" CssClass="control-label" ForeColor="Red"></asp:Label>--%>
              <asp:ValidationSummary ID="ValidationSummary2" CssClass="col-md-12" runat="server" ForeColor="Red"  ValidationGroup="Val1"  />
             <div class="row">
                 <div class="form-group">
                     <div class="row">
                         <asp:Label ID="lbl1" runat="server" CssClass="col-md-12" ForeColor="Red" Style="padding-left: 45px;"></asp:Label>
                     </div>
                 </div>
             </div>
               
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3" style="color: white">Employee ID</label>
                    <div class="col-sm-7">
                        <asp:RequiredFieldValidator ID="rfvusr" runat="server" ControlToValidate="txtUsrId"
                            Display="None" ErrorMessage="Please Enter Username" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtUsrId" runat="server" type="text" class="form-control" autocomplete="off"></asp:TextBox>
                    </div>
                </div>
        <div class="form-group">
                   
                    <div class="col-sm-6 controls">
                        <div>
                            <asp:Button ID="btnSignIn" runat="server" class="btn btn-primary custom-button-color pull-right" Text="Log In" ValidationGroup="Val1" />
                        </div>
                    </div>
                   
                </div>
               
            </asp:Panel>
            
        </form>
    </div>
    </main>

    <script src="BootStrapCSS/Scripts/jquery.min.js"></script>
    <script defer type="text/javascript">
        $(document).keydown(function (event) {
            if (event.keyCode == 123) {
                return false;
            }
            else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                return false;
            }
        });
        $(document).ready(function () {
            $('#content').height($(window).height());
        });
        if (self != top) {
            window.top.location.href = "login.aspx"
        }
    </script>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

  
    <script src="BlurScripts/BlurJs/api.js"></script>

    <script defer type="text/javascript">
        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript'; po.async = true; 

            po.src = 'BlurScripts/BlurJs/clientplusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();

    </script>
    <script defer type="text/javascript">
        function showPopWin() {

            $("#modalcontentframe").attr("src", "frmForgetPwd.aspx");
            $("#myModal").modal('show');
            return false;
            //$("#modelcontainer").load("frmForgetPwd.aspx", function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal('show');
            //});
        }



        gapi.load('auth2', function () {
            gapi.auth2.init();
        });

        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log(profile);

            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

            window.location.href = "GoogleAuth.aspx?email=" + profile.getEmail();
        }
    </script>

</body>
</html>
