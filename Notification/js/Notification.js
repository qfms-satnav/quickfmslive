﻿app.service("NotificationsService", function ($http, $q, UtilityService) {
    this.GetRoleDetails = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/Notifications/GetRole')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }
    this.GetNotificationData = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Notifications/GetNotificationData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.Submit = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Notifications/SubmitNotification', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    
});


app.controller('NotificationsController', function ($scope, $q, $location, UtilityService, NotificationsService, $filter, $http) {
    $scope.Role = [];
    var selecteddata = {};
    var selectedFiles = { Files: [] };
    $scope.data = {};
    $scope.title = 'Yes';
    $scope.data.SelectedRoles = [];
    $scope.data.UploadDoc;
    var GetNotiData = [];
    var Documents = [];
    var status;

    $scope.GetRoleDetails = function () {
        NotificationsService.GetRoleDetails().then(function (response) {
            $scope.Role = response.data;


        });
    }
    $scope.GetRoleDetails();
    const cellClassRules = {
        "cell-pass": params => params.value == 'Sent',        
        "cell-fail": params => params.value == 'Fail'
    };
    $scope.columnDefs = [
        { headerName: "Role", field: "ROLE", cellClass: "grid-align", width: 200 },
        { headerName: "Subject", field: "SUBJECT", cellClass: "grid-align", width: 200 },
        { headerName: "Description", field: "DISCRIPTION", cellClass: "grid-align", width: 200, suppressMenu: true },
        { headerName: "Email", field: "EMAIL", cellClass: "grid-align", width: 200 },
        { headerName: "Status", field: "STATUS", cellClass: "grid-align", width: 200, cellClassRules: cellClassRules },
        { headerName: "Date", field: "DATE", cellClass: "grid-align", width: 200 },
        //    { headerName: "Action", template: '<a ng-click="Remove(data)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 250 }
    ];
    $scope.columnDefs2 = [
        { headerName: "Name", field: "NAME", cellClass: "grid-align", width: 200 },
        { headerName: "Subject", field: "SUBJECT", cellClass: "grid-align", width: 200 },
        //{ headerName: "Discription", field: "DISCRIPTION", cellClass: "grid-align", width: 200 },
        { headerName: "Email", field: "EMAIL", cellClass: "grid-align", width: 200 },
        { headerName: "Status", field: "STATUS", cellClass: "grid-align", width: 200 },
        { headerName: "Date", field: "DATE", cellClass: "grid-align", width: 200 },
        //    { headerName: "Action", template: '<a ng-click="Remove(data)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 250 }
    ];
    function onFilterChanged(value) {
        $scope.gridOptions2.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: [],
        angularCompileRows: true,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: false,
        pagination: true,
        paginationPageSize: 2,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    //$scope.gridOptions2 = {
    //    columnDefs: $scope.columnDefs2,
    //    rowData: [],
    //    angularCompileRows: true,
    //    enableFilter: true,
    //    enableSorting: true,
    //    enableColResize: true,
    //    showToolPanel: false,
    //    pagination: true,
    //    paginationPageSize: 2,
    //    onReady: function () {
    //        $scope.gridOptions2.api.sizeColumnsToFit();
    //    },
    //    onAfterFilterChanged: function () {
    //        if (angular.equals({}, $scope.gridOptions2.api.getFilterModel()))
    //            $scope.DocTypeVisible = 0;
    //        else
    //            $scope.DocTypeVisible = 1;
    //    }
    //};

    $scope.GetNotificationData = function () {
        NotificationsService.GetNotificationData().then(function (respone) {
            if (respone.data != null) {
                GetNotiData = respone.data[0];
                //GetAmtData = respone.data[1]
                $scope.gridOptions.api.setRowData(GetNotiData);
                //$scope.gridOptions2.api.setRowData(GetAmtData);
            }

        });
    }
    $scope.GetNotificationData();

    $scope.clear = function () {
        for (var key in $scope.data) {
            if ($scope.data.hasOwnProperty(key)) {
                if (angular.isArray($scope.data[key])) {
                    angular.forEach($scope.data[key], function (item) { 
                        angular.forEach(item, function (value, prop) {
                            item[prop] = '';
                            item[value] = '';
                        });
                    });
                } else if (angular.isObject($scope.data[key])) {

                    angular.forEach($scope.data[key], function (value, prop) { 
                        $scope.data[key][prop] = '';
                        $scope.data[key][value] = '';
                    });
                }
                else {
                    $scope.data[key] = '';
                   
                }
            }
        }
        
        document.getElementById('file-name').textContent = 'No file chosen';
        document.getElementById('file-content').innerHTML = "";
        angular.forEach($scope.Role, function (roleItem) {
            if (roleItem.ticked) {
                roleItem.ticked = false;
            }
        });
    }
    $scope.fileUpload = function (x, type) {
        var types = type;

        angular.forEach(x.files, function (file) {
            var fileObj = {
                FileName: file.name,
                FileSize: file.size,
                FileType: types,
                data: file
            };
            var fileName = file.name
            var Filetype = types
            var filesize = file.size
            var maxSizeInBytes = 10 * 1024 * 1024; 
            if (filesize > maxSizeInBytes) {
               
                Swal.fire({
                    icon: 'Large File',
                    title: 'Large File!',
                    text: 'File Size is greater than 10 MB',
                    confirmButtonText: 'OK',
                    color: 'Red',
                }).then($scope.GetNotificationData());
                    
                x.value = null;
                return;
            }
            switch (type) {
                case 'Upload_Doc':
                    $scope.data.UploadDoc = fileName;
                    break;
                default:
                    break;
            }

            selecteddata[Filetype] = fileName;
            selectedFiles.Files.push(fileObj);
            Documents.push(fileObj);
        });
    };
    
    $scope.saveImg = function () {
        var deferred = $q.defer(); 
        var frmData = new FormData();
        angular.forEach(selectedFiles.Files, function (fileObj) {
            var file = new Blob([fileObj.data]);
            frmData.append(fileObj.data.name, file, 'file');
        });
        $http.post(UtilityService.path + '/api/Notifications/UploadFiles', frmData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined },
            processData: false
        }).then(
            function (response) { 
                console.log('File uploaded successfully:', response.data);
                deferred.resolve();
            },
            function (error) {
                console.error('File upload failed:', error);
                deferred.reject(error); 
            }
        );
        return deferred.promise;
    };


    $scope.Validations = function () {

        var fields = [
            { name: 'Role', label: 'Role', pattern: null, emptyError: 'Please select Role.' },
            { name: 'Subject', label: 'Subject', pattern: null, emptyError: 'Please enter a Subject.' },
            { name: 'Discription', label: 'Discription', pattern: null, emptyError: 'Please enter a Discription.' }
        ];

        var isValid = true;
        fields.forEach(function (field) {
            if (!isValid) return; // Exit if a previous field has failed validation
            var Value = $scope.data[field.name];

            if (!Value) {
                isValid = false;
                var errorMessage = field.emptyError;
                showNotification('error', 8, 'bottom-right', errorMessage);
                highlightField(field.name);
            }
            
        });
        return isValid;
    };
    function highlightField(fieldName) {
        var invalidElements = document.querySelectorAll('.is-invalid');
        angular.forEach(invalidElements, function (element) {
            angular.element(element).removeClass('is-invalid');
        });
        var inputElement = angular.element(document.querySelector('[data-ng-model="data.' + fieldName + '"]'));
        if (inputElement) {
            if (inputElement) {
                inputElement.addClass('is-invalid');
            }
            else {
                inputElement.removeClass('is-invalid');
            }
        }
    }


    $scope.Submit = function () {
        $scope.data.Discription = document.getElementById('file-content').innerHTML;
        angular.forEach($scope.Role, function (x) {
            if (x.ticked == true) {
                $scope.data.SelectedRoles = x.ROL_DESCRIPTION;
            }
        });
        $scope.data = {
            Role: $scope.data.SelectedRoles,
            Subject: $scope.data.Subject,
            Documents: $scope.data.UploadDoc,
            Discription: $scope.data.Discription,
            Status: "Fail",
            /*Createdby: "Razvi"*/
        };
        $scope.Validations();
        if ($scope.Validations()) {
            $scope.saveImg().then(function () {
                NotificationsService.Submit($scope.data).then(function (respone) {

                    $scope.data2 = respone.data;
                    if (respone.data == 'Details are Submited Successfully') {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            text: 'Mail Sent Successfully',
                            confirmButtonText: 'OK',
                            color: 'green',
                        }).then($scope.GetNotificationData())
                            .then($scope.clear());
                    } else if (respone.data == 'Error') {
                        Swal.fire({
                            icon: 'Error',
                            title: 'Error!',
                            text: 'Message Not Sent',
                            confirmButtonText: 'OK',
                            color: 'Red',
                        }).then($scope.GetNotificationData())
                            .then($scope.clear());
                    } else if (respone.data == "Employee is not assigned with the selected role") {
                        Swal.fire({
                            icon: 'Error',
                            title: 'Error!',
                            text: "Employee is not assigned with the selected role",
                            confirmButtonText: 'OK',
                            color: 'Red',
                        }).then($scope.GetNotificationData())
                            .then($scope.clear());
                    }else {
                        Swal.fire({
                            icon: 'Error',
                            title: 'Error!',
                            text: 'Something Went Wrong',
                            confirmButtonText: 'OK',
                            color: 'Red',
                        }).then($scope.GetNotificationData())
                            .then($scope.clear());
                    }
                });
            });

        }

    }; 

    
});