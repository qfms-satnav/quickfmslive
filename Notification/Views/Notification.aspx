﻿<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title></title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/sweetalert2.min.css.css" rel="stylesheet" />
    <%--       <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.2/angular.min.js"></script>--%>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Notification.css" rel="stylesheet" />
    <%--<link rel='stylesheet' href='https://cdn-uicons.flaticon.com/uicons-regular-rounded/css/uicons-regular-rounded.css'>--%>

    <!-- font flat-ui cdn 2.2.2 -->
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.2.2/css/flat-ui.min.css"
        integrity="sha512-PvB3Q4vTvWD/9aiiELYI3uebup/4mtou3Mc/uGudC/Zl+C9BdKUkAI+5jORfA+fvLK4DpzC5VyEN7P2kK43hjg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />--%>

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        hr {
            border-top: 1px solid black;
            margin-top: 10px;
        }

        .panel-heading {
            padding: 0px 8px;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }

        .cell-pass {
            font-weight: bold;
            background-color: #b2e887;
        }

        .cell-fail {
            font-weight: bold;
            background-color: #f2aba0;
        }
        .rich-text-editor {
            width: 100%;
            min-height: 150px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            font-size: 14px;
            margin-bottom: 10px;
            overflow-y: auto;
        }
    </style>
</head>
<body data-ng-controller="NotificationsController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <h3 class="panel-title" ng-show="title ==='Yes'" style="margin-left: 30px"><b>Notification / Events / Announcements</b></h3>
            <div class="widgets">
                <div ba-panel ba-panel-title="PPM Report" ba-panel-class="with-scroll">
                    <form role="form" id="form" name="addAssetObj">
                        <div class="panel" id="Founder">
                            <div class="panel-heading">
                                <h4><b>Assign Roles</b></h4>
                                <hr>
                            </div>
                            <div style="padding-right: 10px; padding: 15px 22px; min-height: 150px;">
                                <div class="clearfix">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': BCLMainCategory.$submitted && BCLMainCategory.BCL_TP_NAME.$invalid}">
                                                <label class="control-label">Role<span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Role" data-output-model="Role.ROL_ACRONYM" data-button-label="icon ROL_DESCRIPTION" data-item-label="icon ROL_DESCRIPTION" selection-mode="single"
                                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="data.Role" name="BCL_TP_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="BCLMainCategory.$submitted && BCLMainCategory.MainType.$invalid" style="color: red">Please Select Main Type </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-9">
                                            <div class="form-group">
                                                <label for="txtcode">Subject<span style="color: red;">*</span></label>
                                                <input type="text" data-ng-model="data.Subject" class="form-control" />
                                            </div>
                                        </div>
                                        <%--<div class="col-md-3 col-sm-6 col-xs-9">
                                            <div class="form-group">
                                                <label for="txtcode">Upload Documents</label>
                                                <div style="width: 100px">
                                                    <input type="file" id="file6" onchange="angular.element(this).scope().fileUpload(this,'Upload_Doc')" multiple="multiple"
                                                        data-ng-model="data.UploadDoc" name="file" value="Attachment"
                                                        accept=".png,.jpg,.xlsx,.pdf,.docx">
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-3 col-sm-6 col-xs-9">
                                            <div class="form-group">
                                                <label for="txtcode">Upload Documents</label>
                                                <div style="display: flex; align-items: center;">
                                                    <!-- Hide the original file input -->
                                                    <input type="file" id="file6"
                                                        onchange="angular.element(this).scope().fileUpload(this,'Upload_Doc')"
                                                        multiple="multiple" data-ng-model="data.UploadDoc"
                                                        name="file" accept=".png,.jpg,.xlsx,.pdf,.docx"
                                                        style="display: none;" />

                                                    <!-- Custom button for file input -->
                                                    <button id="custom-file-button" class="btn btn-primary" onclick="document.getElementById('file6').click()">
                                                        Add Attachment
                                                    </button>

                                                    <!-- Placeholder for showing the selected file name -->
                                                    <span id="file-name" style="margin-left: 10px;">No file chosen</span>
                                                </div>
                                            </div>
                                        </div>

                                        
                                        <div class="col-md-8 col-sm-6 col-xs-9">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-9">
                                            <div class="form-group">
                                                <label for="txtcode">Description<span style="color: red;">*</span><span style="margin-left:45px;color: gray;">( Use CTRL + <b>B</b>, <u>U</u>, <em>I</em> )</span></label>
                                                <div id="file-content" contenteditable="true" name="file-content" rows="5" cols="20" data-ng-model="data.Discription" class="rich-text-editor form-control"></div>
                                                <%--<textarea id="file-content" name="file-content" rows="5" cols="20" data-ng-model="data.Discription" class="form-control"></textarea>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-9">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-9">
                                            <div class="form-group">
                                                <label for="txtcode"></label>
                                                <input type="button" data-ng-model="user.name" value="Submit" data-ng-click="Submit()" class="form-control btn-primary " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel" id="Gird">
                            <div class="panel-heading" style="height: 71px;">
                                <h4>Notification Status</h4>
                                <hr />
                            </div>
                            <div style="padding-right: 10px; padding: 15px 22px; min-height: 180px;">
                                <div class="clearfix">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 280px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="panel" id="Gird1">
                            <div class="panel-heading" style="height: 71px;">
                                <h4>Overall Mail Status</h4>
                                <hr />
                            </div>
                            <div style="padding-right: 10px; padding: 15px 22px; min-height: 180px;">
                                <div class="clearfix">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <input type="text" class="selectpicker form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%;" />
                                            <div data-ag-grid="gridOptions2" class="ag-blue" style="height: 280px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../js/Notification.js"></script>
    <script>
        document.getElementById('file6').addEventListener('change', function () {
            var fileName = this.files.length > 0 ? this.files[0].name : 'No file chosen';
            document.getElementById('file-name').textContent = fileName;
        });
    </script>
    <script defer src="../../SMViews/Utility.min.js"></script>
    <script defer src="../../Scripts/moment.min.js"></script>
    <script defer src="../../BootStrapCSS/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
</body>
</html>
