<%@ WebHandler Language="VB" Class="AssetLoaction" %>


Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System

Public Class AssetLoaction : Implements IHttpHandler, IRequiresSessionState 
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim nd As String = request("nd")
       
        
        Dim totalRecords As Integer
        Dim clsAssetLocation As Collection(Of clsAssetLocation) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
       
       
        Dim output As String = BuildJQGridResults(clsAssetLocation, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}"))
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of clsAssetLocation)
        Dim users As New Collection(Of clsAssetLocation)()
        Dim str As String = ""
        Dim i As String = "0"
        
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If
        
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@PARENTID", SqlDbType.NVarChar, 200)
        param(0).Value = id
        ds = ObjSubsonic.GetSubSonicDataSet("GETASTCATEGORIES_RPT", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For j As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsAssetLocation
                
                user.CategoryId = ds.Tables(0).Rows(j).Item("CategoryId").ToString
                user.CategoryName = ds.Tables(0).Rows(j).Item("CategoryName").ToString
                
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsAssetLocation As Collection(Of clsAssetLocation), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResultsDrop()
        Dim rows As New List(Of JQGridRowDrop)()
        For Each Summary As clsAssetLocation In clsAssetLocation
            'Company','CompanyName','UserID', 'Password','UserName','Address','City','State','Phone','Email','Role
            Dim row As New JQGridRowDrop()
            row.row = New String(0) {}
            ' row.row(0) = "CTY_CODE" & """:""" & Summary.CTY_NAME.ToString() ' & """,""" & "CTY_NAME" & """:""" & Summary.CTY_NAME.ToString()
            row.row(0) = "branch" & """:""" & Summary.CategoryName.ToString() & """,""" & "branch_code" & """:""" & Summary.CategoryId.ToString()
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        
        result.total_rows = totalRecords
       
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function

End Class