<%@ WebHandler Language="VB" Class="RequestID" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System


Public Class RequestID : Implements IHttpHandler, IRequiresSessionState
    
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim nd As String = request("nd")
       
        
        Dim totalRecords As Integer
        Dim clsRequestID As Collection(Of clsRequestID) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
       
       
        Dim output As String = BuildJQGridResults(clsRequestID, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}"))
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of clsRequestID)
        Dim users As New Collection(Of clsRequestID)()
             
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        'pass parameters
        param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        param(0).Value = 1
        'end here
        ds = ObjSubsonic.GetSubSonicDataSet("AST_GET_REQID_REPORT", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsRequestID
                
                user.srn_req_id = ds.Tables(0).Rows(i).Item("AID_REQ_ID").ToString
                user.SRN_REQ_ID1 = ds.Tables(0).Rows(i).Item("AID_REQ_ID").ToString
                
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsRequestID As Collection(Of clsRequestID), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResultsDrop()
        Dim rows As New List(Of JQGridRowDrop)()
        For Each Summary As clsRequestID In clsRequestID
            'Company','CompanyName','UserID', 'Password','UserName','Address','City','State','Phone','Email','Role
            Dim row As New JQGridRowDrop()
            row.row = New String(0) {}
            ' row.row(0) = "CTY_CODE" & """:""" & Summary.CTY_NAME.ToString() ' & """,""" & "CTY_NAME" & """:""" & Summary.CTY_NAME.ToString()
            row.row(0) = "branch" & """:""" & Summary.SRN_REQ_ID1.ToString() & """,""" & "branch_code" & """:""" & Summary.srn_req_id.ToString()
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        
        result.total_rows = totalRecords
       
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function


End Class