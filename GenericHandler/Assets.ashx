<%@ WebHandler Language="VB" Class="Assets" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System
Public Class Assets : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strcategoryID As String = request("city")
        Dim strcallback As String = request("callback")
        
        Dim totalRecords As Integer
        Dim clsAssets As Collection(Of clsAssets) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, strcategoryID)
       
       
        Dim output As String = BuildJQGridResults(clsAssets, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal categoryID As String) As Collection(Of clsAssets)
        Dim users As New Collection(Of clsAssets)()
             
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@categoryID", SqlDbType.NVarChar, 200)
        param(0).Value = categoryID
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASTCOUNT_BYCATEGORY", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsAssets
                
                user.PRODUCTID = ds.Tables(0).Rows(i).Item("PRODUCTID").ToString
                user.PRODUCTNAME = ds.Tables(0).Rows(i).Item("PRODUCTNAME").ToString
                
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsAssets As Collection(Of clsAssets), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResultsDrop()
        Dim rows As New List(Of JQGridRowDrop)()
        For Each Summary As clsAssets In clsAssets
            Dim row As New JQGridRowDrop()
            row.Row = New String(0) {}
            row.row(0) = "branch" & """:""" & Summary.PRODUCTNAME.ToString() & """,""" & "branch_code" & """:""" & Summary.PRODUCTID.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.total_rows = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function


End Class