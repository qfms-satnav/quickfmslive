<%@ WebHandler Language="VB" Class="SearchAssetstatusunmapped" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System

Public Class SearchAssetstatusunmapped : Implements IHttpHandler
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim s As String = request("sidx")
        Dim sortColumnName As String = s.Replace("unMappedAssets_list_", "")
        Dim sortOrderBy As String = request("sord")
        Dim lt As String = request("ct")
      
        Dim totalRecords As Integer
        Dim clsSearchAssetStatus As Collection(Of clsSearchAssetStatus) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, lt)
       
       
        Dim output As String = BuildJQGridResults(clsSearchAssetStatus, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        'response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
        response.Write(output)
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal lt As String) As Collection(Of clsSearchAssetStatus)
        Dim users As New Collection(Of clsSearchAssetStatus)()
        Dim ds As New DataSet
       
        Dim param(0) As SqlParameter
     
        param(0) = New SqlParameter("@ASTCODE", SqlDbType.NVarChar, 200)
        param(0).Value = lt
       
        'param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        'param(1).Value = pageIndex
        'param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        'param(2).Value = sortColumnName
        'param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        'param(3).Value = sortOrderBy
        'param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        'param(4).Value = numberOfRows
        'param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        'param(5).Value = totalRecords
        
        ds = ObjSubsonic.GetSubSonicDataSet("GET_AST_DETAILS_SEARCH", param)
        If ds.Tables.Count > 0 Then


            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim user As New clsSearchAssetStatus
                  
                    user.PRODUCTNAME = ds.Tables(0).Rows(i).Item("Product Name").ToString
                    user.AAS_AAT_CODE = ds.Tables(0).Rows(i).Item("Asset Code").ToString
               
                    users.Add(user)
                Next
                totalRecords = CInt(ds.Tables(0).Rows.Count)
            End If
        End If
        
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsSearchAssetStatus As Collection(Of clsSearchAssetStatus), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsSearchAssetStatus In clsSearchAssetStatus
            Dim row As New JQGridRow()
            row.cell = New String(1) {}
            row.id = Summary.PRODUCTNAME.ToString()
            row.cell(0) = Summary.PRODUCTNAME.ToString()
            row.cell(1) = Summary.AAS_AAT_CODE.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function

End Class