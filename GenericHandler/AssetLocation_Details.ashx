<%@ WebHandler Language="VB" Class="AssetLocation_Details" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System

Public Class AssetLocation_Details : Implements IHttpHandler, IRequiresSessionState 
  
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim s As String = request("sidx")
        Dim sortColumnName As String = s.Replace("Assets_list_", "")
        Dim sortOrderBy As String = request("sord")
        Dim id As String = request("ct")
      
        Dim totalRecords As Integer
        Dim clsAssetLocation_Details As Collection(Of clsAssetLocation_Details) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, id)
       
       
        Dim output As String = BuildJQGridResults(clsAssetLocation_Details, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        'response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
        response.Write(output)
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal id As String) As Collection(Of clsAssetLocation_Details)
        Dim users As New Collection(Of clsAssetLocation_Details)()
        Dim ds As New DataSet
       
        Dim param(5) As SqlParameter
     
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.NVarChar, 200)
        param(0).Value = id
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = pageIndex
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(2).Value = sortColumnName
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(3).Value = sortOrderBy
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = numberOfRows
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = totalRecords
        
        ds = ObjSubsonic.GetSubSonicDataSet("GET_All_ASTSLIST_PRODUCT_V", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsAssetLocation_Details
                user.PRODUCTID = ds.Tables(0).Rows(i).Item("productid").ToString
                user.ASTCOUNT = ds.Tables(0).Rows(i).Item("ASTCOUNT").ToString
                user.PRODUCTNAME = ds.Tables(0).Rows(i).Item("PRODUCTNAME").ToString
                user.LCM_NAME = ds.Tables(0).Rows(i).Item("LCM_NAME").ToString
                user.LCM_CODE = ds.Tables(0).Rows(i).Item("LCM_CODE").ToString
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsAssetLocation_Details As Collection(Of clsAssetLocation_Details), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsAssetLocation_Details In clsAssetLocation_Details
            Dim row As New JQGridRow()
            row.cell = New String(3) {}
            row.id = Summary.PRODUCTID.ToString()
            row.cell(0) = Summary.ASTCOUNT.ToString()
            row.cell(2) = Summary.PRODUCTNAME.ToString()
            row.cell(1) = Summary.LCM_NAME.ToString()
            row.cell(3)=Summary.LCM_CODE.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function
End Class