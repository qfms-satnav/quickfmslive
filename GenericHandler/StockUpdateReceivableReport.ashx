<%@ WebHandler Language="VB" Class="StockUpdateReceivableReport" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System
Public Class StockUpdateReceivableReport : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim s As String = request("sidx")
        Dim sortColumnName As String = s.Replace("StockReceivableReport_list_", "")
        Dim sortOrderBy As String = request("sord")
        Dim lt As String = request("lt")
        Dim tw As String = request("tw")
        Dim ft As String = request("ft")
        Dim sa As String = request("as")
      
        Dim totalRecords As Integer
        Dim clsStockUpdateAvailableDetails As Collection(Of clsStockUpdateAvailableDetails) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, lt, tw, ft, sa)
       
       
        Dim output As String = BuildJQGridResults(clsStockUpdateAvailableDetails, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        'response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
        response.Write(output)
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal lt As String, ByVal tw As String, ByVal ft As String, ByVal sa As String) As Collection(Of clsStockUpdateAvailableDetails)
        Dim users As New Collection(Of clsStockUpdateAvailableDetails)()
        Dim ds As New DataSet
        Dim param(8) As SqlParameter
        param(0) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(0).Value = pageIndex
        param(1) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(1).Value = sortColumnName
        param(2) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(2).Value = sortOrderBy
        param(3) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(3).Value = numberOfRows
        param(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(4).Value = totalRecords
        param(5) = New SqlParameter("@LCM_CODE ", SqlDbType.NVarChar, 200)
        param(5).Value = lt
        param(6) = New SqlParameter("@TWR_CODE ", SqlDbType.NVarChar, 200)
        param(6).Value = tw
        param(7) = New SqlParameter("@FLR_CODE ", SqlDbType.NVarChar, 200)
        param(7).Value = ft
        param(8) = New SqlParameter("@ASTCODE ", SqlDbType.NVarChar, 200)
        param(8).Value = sa
     
        ds = ObjSubsonic.GetSubSonicDataSet("GET_REC_STOCK_V", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsStockUpdateAvailableDetails
                user.AIM_CODE = ds.Tables(0).Rows(i).Item("AST_CODE").ToString
                user.AIM_NAME = ds.Tables(0).Rows(i).Item("AIM_NAME").ToString
                user.AID_AVLBL_QTY = ds.Tables(0).Rows(i).Item("QTY").ToString
                user.AID_UPDATED_DATE = ds.Tables(0).Rows(i).Item("UPDATED_DT").ToString
                user.AID_REM = ds.Tables(0).Rows(i).Item("REMARKS").ToString
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsStockUpdateAvailableDetails As Collection(Of clsStockUpdateAvailableDetails), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsStockUpdateAvailableDetails In clsStockUpdateAvailableDetails
            Dim row As New JQGridRow()
            row.cell = New String(4) {}
            row.id = Summary.AIM_CODE.ToString()
            row.cell(0) = Summary.AIM_CODE.ToString()
            row.cell(1) = Summary.AIM_NAME.ToString()
            row.cell(2) = Summary.AID_AVLBL_QTY.ToString()
            row.cell(3) = Summary.AID_UPDATED_DATE.ToString()
            row.cell(4) = Summary.AID_REM.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function
End Class