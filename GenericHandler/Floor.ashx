<%@ WebHandler Language="VB" Class="Floor" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System

Public Class Floor : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strloc As String = request("city")
        Dim strcallback As String = request("callback")
        
        Dim totalRecords As Integer
        Dim clsFloor As Collection(Of clsFloor) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, strloc)
       
       
        Dim output As String = BuildJQGridResults(clsFloor, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
       
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal strloc As String) As Collection(Of clsFloor)
        Dim users As New Collection(Of clsFloor)()
        Dim ds As New DataSet
        Dim array(2) As String
        array = strloc.Split("~")
        Dim param(1) As SqlParameter
        
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 200)
        param(0).Value = array(1)
        param(1) = New SqlParameter("@dummy1", SqlDbType.NVarChar, 200)
        param(1).Value = array(0)
        ds = ObjSubsonic.GetSubSonicDataSet("GET_TWRFLR", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsFloor
                
                user.FLR_CODE = ds.Tables(0).Rows(i).Item("FLR_CODE").ToString
                user.FLR_NAME = ds.Tables(0).Rows(i).Item("FLR_NAME").ToString
                
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsFloor As Collection(Of clsFloor), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResultsDrop()
        Dim rows As New List(Of JQGridRowDrop)()
        For Each Summary As clsFloor In clsFloor
            Dim row As New JQGridRowDrop()
            row.Row = New String(0) {}
            row.row(0) = "branch" & """:""" & Summary.FLR_NAME.ToString() & """,""" & "branch_code" & """:""" & Summary.FLR_CODE.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.total_rows = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function


End Class