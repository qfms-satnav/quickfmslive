<%@ WebHandler Language="VB" Class="ViewPOs" %>
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System
Public Class ViewPOs : Implements IHttpHandler
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim s As String = request("sidx")
        Dim sortColumnName As String = s.Replace("AssetUtilization_list_", "")
        
        Dim sortOrderBy As String = request("sord")
      
      
        Dim totalRecords As Integer
        Dim clsViewPOs As Collection(Of clsViewPOs) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
       
       
        Dim output As String = BuildJQGridResults(clsViewPOs, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        'response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
        response.Write(output)
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of clsViewPOs)
        Dim users As New Collection(Of clsViewPOs)()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@PageIndex", SqlDbType.Int)
        'param(0).Value = pageIndex
        'param(1) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        'param(1).Value = sortColumnName
        'param(2) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        'param(2).Value = sortOrderBy
        'param(3) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        'param(3).Value = numberOfRows
        'param(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        'param(4).Value = totalRecords
        param(0) = New SqlParameter("@StatusId", SqlDbType.Int)
        param(0).Value = 2
        
        ds = ObjSubsonic.GetSubSonicDataSet("USP_AMG_ITEM_PO_GetByStatusIdFinalPOS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsViewPOs
                user.AIP_PO_ID = ds.Tables(0).Rows(i).Item("AIP PO ID").ToString
                user.AIP_PO_DATE = ds.Tables(0).Rows(i).Item("AIP PO DATE").ToString
                user.AIP_AUR_ID = ds.Tables(0).Rows(i).Item("AIP AUR ID").ToString
                '-- user.AIP_DEPT = ds.Tables(0).Rows(i).Item("AIP DEPT").ToString
                user.AIPD_VENDORNAME = ds.Tables(0).Rows(i).Item("AIPD VENDORNAME").ToString
                user.AIPD_TOTALCOST = ds.Tables(0).Rows(i).Item("AIPD TOTALCOST").ToString
                user.AIPD_EXPECTED_DATEOF_DELIVERYFormatted = ds.Tables(0).Rows(i).Item("AIPD EXPECTED DATEOF DELIVERY").ToString
                user.AIP_PO_ID = "<a href=../../FAM/FAM_WEBfiles/frmGeneratePOPrint.aspx?id=" + Convert.ToString(ds.Tables(0).Rows(i).Item("AIP PO ID")) + " target=_blank>" + Convert.ToString(ds.Tables(0).Rows(i).Item("AIP PO ID")) + "</a>"
                'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "key", "window.open('../../FAM/FAM_WEBfiles/frmGeneratePOPrint.aspx?rid=" & Convert.ToString(ds.Tables(0).Rows(i).Item("AIP_PO_ID")) & "','_blank');", True)
                'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "key", "window.open('frmPVMViewDetails.aspx?rid=" & e.Item.Cells(0).Text & "','_blank');", True)
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsViewPOs As Collection(Of clsViewPOs), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsViewPOs In clsViewPOs
            Dim row As New JQGridRow()
            row.cell = New String(6) {}
            row.id = Summary.AIP_PO_ID.ToString()
            row.cell(0) = Summary.AIP_PO_ID.ToString()
            row.cell(1) = Summary.AIP_PO_DATE.ToString()
            row.cell(2) = Summary.AIP_AUR_ID.ToString()
            '--row.cell(3) = Summary.AIP_DEPT.ToString()
            row.cell(3) = Summary.AIPD_VENDORNAME.ToString()
            row.cell(4) = Summary.AIPD_TOTALCOST.ToString()
            row.cell(5) = Summary.AIPD_EXPECTED_DATEOF_DELIVERYFormatted.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function



End Class