<%@ WebHandler Language="VB" Class="AssetLocationForGrid" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System
Public Class AssetLocationForGrid : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As Integer = request("page")
        Dim s As String = request("sidx")
        Dim sortColumnName As String =s.Replace("AssetCategory_list_", "")
        Dim sortOrderBy As String = request("sord")
        Dim id As integer = request("ct")
        Dim totalRecords As Integer
       
       Dim clsAssetLocationForGrid As Collection(Of clsAssetLocationForGrid) = GetStoresForUserDefinedRoutes(id,pageIndex,sortColumnName, sortOrderBy,numberOfRows ,totalRecords)
       
        Dim output As String = BuildJQGridResults(clsAssetLocationForGrid, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        'response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
        response.Write(output)
       
    End Sub
 
   Private Function GetStoresForUserDefinedRoutes(ByVal id As Integer,ByVal pageIndex As Integer, ByVal sortColumnName As String, ByVal sortOrderBy As String,ByVal numberOfRows As String, ByRef totalRecords As Integer) As Collection(Of clsAssetLocationForGrid)
   
        Dim users As New Collection(Of clsAssetLocationForGrid)()
        Dim ds As New DataSet
       
        Dim param(5) As SqlParameter
     
        param(0) = New SqlParameter("@categoryID", SqlDbType.Int)
        param(0).Value = id
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = pageIndex
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(2).Value = sortColumnName
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(3).Value = sortOrderBy
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = numberOfRows
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = totalRecords
        
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASTCOUNT_BYCATEGORY_V", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsAssetLocationForGrid
                user.PRODUCTID=ds.Tables(0).Rows(i).Item("PRODUCTID").ToString
                user.PRODUCTNAME = ds.Tables(0).Rows(i).Item("PRODUCTNAME").ToString
                user.ASTCOUNT = ds.Tables(0).Rows(i).Item("ASTCOUNT").ToString
                'Dim H As New HtmlAnchor
                'H.Name = ("ASTCOUNT")
                'H.Name.ToString()
                'H.HRef="../frmAstLocationSummary.aspx"
                'h.HRef="showPopWin('<%=Page.ResolveUrl("frmAstLocationSummary.aspx")%>?id=<%# Eval("ProductId") %>',850,338,'')""

                users.Add(user)
                
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsAssetLocationForGrid As Collection(Of clsAssetLocationForGrid), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsAssetLocationForGrid In clsAssetLocationForGrid
            Dim row As New JQGridRow()
            row.cell = New String(1) {}
            row.id = Summary.PRODUCTID.ToString()
            row.cell(1) = Summary.PRODUCTNAME.ToString()
            row.cell(0) = Summary.ASTCOUNT.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function
End Class