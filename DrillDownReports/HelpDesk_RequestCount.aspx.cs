﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DrillDownReports_HelpDesk : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }

        if (!IsPostBack)
        {
            LoadReport();
        }
    }

    public void LoadReport()
    {
        try
        {           
            SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_HELPDESK_REQUEST_DRILLDOWN");
            DataSet ds1 = new DataSet();
            ds1 = sp1.GetDataSet();
            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = "HelpDeskDS";
            rds1.Value = ds1.Tables[0];

            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Add(rds1);

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/HelpDeskReport.rdlc");
            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Visible = true;
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.LocalReport.EnableHyperlinks = true;
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
}