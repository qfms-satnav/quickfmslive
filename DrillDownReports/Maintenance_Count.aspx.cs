﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DrillDownReports_Maintenance_DDR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }

        if (!IsPostBack)
        {
            LoadReport();
        }
    }

    public void LoadReport()
    {
        try
        {
            System.Threading.Thread.Sleep(2000);
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_MAINTENANCE_SUMMARY_DRILLDOWN");
            sp.Command.AddParameter("@ADMIN_ID", Session["uid"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "DataSet1";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/Maintenace_DDR.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.InnerText = "No Records Found";
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
}