﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DrillDownReports_Property_Count : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }

        if (!IsPostBack)
        {
            LoadCompanies();
            ddlCompany.SelectedValue = Session["COMPANYID"].ToString();
            LoadReport();
        }
    }
    public void LoadCompanies()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_COMPANIES");
            ddlCompany.DataSource = sp.GetDataSet();
            ddlCompany.DataTextField = "CNP_NAME";
            ddlCompany.DataValueField = "CNP_ID";
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, "--Select--");
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    public void LoadReport()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TOTAL_PPTS_LOCWISE_DRILLDOWN");
            sp.Command.AddParameter("@COMPANY_ID", ddlCompany.SelectedValue, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "DataSet1";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/Property_DDR.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();
                RptViewer.Visible = true;
                lblMsg.Visible = false;
                lblMsg.Text = "";
            }
            else
            {
                RptViewer.Visible = false;
                lblMsg.Visible = true;
                lblMsg.Text = "No Records Found...";
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        LoadReport();
    }
}