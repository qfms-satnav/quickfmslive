﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class DrillDownReports_AssetBreakdownReport : System.Web.UI.Page
{
    clsMasters obj = new clsMasters();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            obj.Bindlocation(ddlLName);
            getvendors();
          
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }
      
    }

    public void getvendors()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((Session["TENANT"] + ("." + "GET_ALLVENDORS")));
        // sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlVendor.DataSource = sp.GetDataSet();
        ddlVendor.DataTextField = "AVR_NAME";
        ddlVendor.DataValueField = "AVR_CODE";
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, "--Select--");
    }

    public void LoadReport(string Location,string Vendor,string FromDate,string ToDate)
    {
        try
        {
          
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_BREAKDOWN_REPORT");
            sp.Command.AddParameter("@ADMIN_ID", Session["uid"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.String);
            sp.Command.AddParameter("@startDate", " "+FromDate, DbType.String);
            sp.Command.AddParameter("@endDate", " "+ToDate, DbType.String);
            sp.Command.AddParameter("@LOC_CODE", Location, DbType.String);
            sp.Command.AddParameter("@VEN_CODE", Vendor, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "BreakdownReport";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/AssetBreakdownReport.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();
                lblMsg.Visible = false;
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.InnerText = "No Records Found";
                ReportViewer1.Reset();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadReport(ddlLName.SelectedValue, ddlVendor.SelectedValue, txtFromDate.Text, txtToDate.Text);
    }
}