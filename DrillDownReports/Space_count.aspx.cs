﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DrillDownReports_Space_count : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            LoadReport();
        }
    }

    public void LoadReport()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_SPACE_DRILL_DOWN_REPORT");
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "SpaceDrillDownDS";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/SpaceDrillDownReport.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void ReportViewer1_Drillthrough(object sender, DrillthroughEventArgs e)
    {
        ReportParameterInfoCollection DrillThroughValues = e.Report.GetParameters();

        // string thisConnectionString = ConfigurationManager.ConnectionStrings["CSAmantraFAM"].ConnectionString;
        string CNY_CODE = "";
        string CTY_CODE = "";
        string LCM_CODE = "";
        string TWR_CODE = "";
        string FLR_CODE = "";
        string VER_CODE = "";
        string COST_CENTER_CODE = "";
        string STATUS = "";
        string LEVEL = "";

        foreach (ReportParameterInfo d in DrillThroughValues)
        {
            if (d.Name == "CNY_CODE" && d.Values.Count != 0)
                CNY_CODE = d.Values[0].ToString().Trim();
            if (d.Name == "CTY_CODE" && d.Values.Count != 0)
                CTY_CODE = d.Values[0].ToString().Trim();

            if (d.Name == "LCM_CODE" && d.Values.Count != 0)
                LCM_CODE = d.Values[0].ToString().Trim();
            if (d.Name == "TWR_CODE" && d.Values.Count != 0)
                TWR_CODE = d.Values[0].ToString().Trim();
            if (d.Name == "FLR_CODE" && d.Values.Count != 0)
                FLR_CODE = d.Values[0].ToString().Trim();
            if (d.Name == "VER_CODE" && d.Values.Count != 0)
                VER_CODE = d.Values[0].ToString().Trim();
            if (d.Name == "COST_CENTER_CODE" && d.Values.Count != 0)
                COST_CENTER_CODE = d.Values[0].ToString().Trim();
            if (d.Name == "STATUS")
                STATUS = d.Values[0].ToString().Trim();
            if (d.Name == "LEVEL")
                LEVEL = d.Values[0].ToString().Trim();
        }
        LocalReport localreport = (LocalReport)e.Report;
        //SqlParameter[] Level1SearchValue = new SqlParameter[6];
        //Fill dataset for Level1.rdlc
        // SqlConnection thisConnection = new SqlConnection(thisConnectionString);
        System.Data.DataSet Level1DataSet = new System.Data.DataSet();

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_SPACE_DRILL_DOWN_SUB_REPORT");
        sp.Command.AddParameter("@CNY_CODE", CNY_CODE);
        sp.Command.AddParameter("@CTY_CODE", CTY_CODE);
        sp.Command.AddParameter("@LCM_CODE", LCM_CODE);
        sp.Command.AddParameter("@TWR_CODE", TWR_CODE);
        sp.Command.AddParameter("@FLR_CODE", FLR_CODE);
        sp.Command.AddParameter("@VER_CODE", VER_CODE);
        sp.Command.AddParameter("@COST_CENTER_CODE", COST_CENTER_CODE);
        sp.Command.AddParameter("@LEVEL", LEVEL);
        sp.Command.AddParameter("@STATUS", STATUS);
        Level1DataSet = sp.GetDataSet();


        if (Level1DataSet.Tables[0].Rows.Count > 0)
        {
            localreport.DataSources.Add(new ReportDataSource("SpaceDrillDownSubRpt", Level1DataSet.Tables[0]));
            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Visible = true;
            localreport.Refresh();
        }
    }
}