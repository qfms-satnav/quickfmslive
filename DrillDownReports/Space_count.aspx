﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Space_count.aspx.cs" Inherits="DrillDownReports_Space_count" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Space Drill Down Report </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <br />
                            <form id="form1" class="form-horizontal" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <div class="table table table-condensed table-responsive">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <rsweb:ReportViewer ID="ReportViewer1" AsynRendering="True" ShowExportControls="true" ShowBackButton="true" ShowFindControls="true" SizeToReportContent="True" runat="server" OnDrillthrough="ReportViewer1_Drillthrough"></rsweb:ReportViewer>
                                            <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="500px" OnDrillthrough="ReportViewer1_Drillthrough" ShowBackButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowPrintButton="False" ShowRefreshButton="False" ShowZoomControl="False"></rsweb:ReportViewer>--%>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        window.onload = function () {
            var viewer = document.getElementById("ReportViewer1");
            var frame = document.getElementById("ReportFrame<%=ReportViewer1.ClientID %>");
            if (frame != null && viewer != null) {
                var reportDiv = eval("ReportFrame<%=ReportViewer1.ClientID %>").document.getElementById("report").contentDocument.getElementById("oReportDiv");
                viewer.style.height = reportDiv.scrollHeight;
                viewer.style.width = reportDiv.scrollWidth;
            }

        }
    </script>
</body>
</html>






