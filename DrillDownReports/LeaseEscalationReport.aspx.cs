﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SubSonic;
public partial class DrillDownReports_LeaseEscalationReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }

        if (!IsPostBack)
        {
            BindProperty();
            BindGrid();
        }
    }

    

    private void BindProperty()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_POPERTY_NAMES");
        ddlproperty.DataSource = sp.GetDataSet();
        ddlproperty.DataTextField = "PM_PPT_NAME";
        ddlproperty.DataValueField = "PM_PPT_CODE";
        ddlproperty.DataBind();
        ddlproperty.Items.Insert(0, new ListItem("--All--", "--All--"));
    }

    public void BindGrid()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_LEASE_ESCALATION_DATA_ALL");
            sp.Command.AddParameter("@COMPANY_ID", 1);
            sp.Command.AddParameter("@PRPERTY_CODE", ddlproperty.SelectedItem.Value);
            sp.Command.AddParameter("@FROM_DATE", FromDate.Text);
            sp.Command.AddParameter("@TO_DATE", txtToDate.Text);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "LeaseEscalationReport";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/LeaseEscalationReport.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();
                RptViewer.Visible = true;
                lblMsg.Visible = false;
                lblMsg.Text = "";
            }
            else
            {
                RptViewer.Visible = false;
                lblMsg.Visible = true;
                lblMsg.Text = "No Records Found...";
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    public void LoadReport()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_LEASE_ESCALATION_DATA_ALL");
            sp.Command.AddParameter("@COMPANY_ID", 1);
            sp.Command.AddParameter("@PRPERTY_CODE", ddlproperty.SelectedItem.Value);
            sp.Command.AddParameter("@FROM_DATE", FromDate.Text);
            sp.Command.AddParameter("@TO_DATE", txtToDate.Text);

            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource();
                rds.Name = "LeaseEscalationReport";
                rds.Value = ds.Tables[0];
                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Add(rds);
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/LeaseEscalationReport.rdlc");
                ReportViewer1.SizeToReportContent = true;
                ReportViewer1.Visible = true;
                ReportViewer1.LocalReport.Refresh();
                RptViewer.Visible = true;
                lblMsg.Visible = false;
                lblMsg.Text = "";
            }
            else
            {
                RptViewer.Visible = false;
                lblMsg.Visible = true;
                lblMsg.Text = "No Records Found...";
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        LoadReport();
    }
}
