﻿app.service("BusinessCardUserWiseService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetGriddata = function (param) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BusinessCardUserWise/GetUserWiseDetails', param)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BusinessCardUserWiseController', ['$scope', '$q', '$http', 'UtilityService', '$timeout', 'BusinessCardUserWiseService', function ($scope, $q, $http, UtilityService, $timeout, BusinessCardUserWiseService) {
    $scope.UserWiseReq = {};
    $scope.Type = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.GridVisiblity = true;
    $scope.EnableStatus = 0;
    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.UserWiseReq.CNP_NAME.push(a);
                });

                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 1; }
            }

        });
    }, 500);

    $scope.columnDefs = [
        { headerName: "Employee Name/Id", field: "EMPID", width: 190, cellClass: 'grid-align', width: 170 },
        { headerName: "No.of Cards Requested", field: "CARDS", cellClass: 'grid-align', width: 170 },
        { headerName: "Department/LOB", field: "DEPARTMENT", suppressMenu: true, cellClass: 'grid-align', width: 140 },
        { headerName: "Designation", field: "DESIGNATION", cellClass: 'grid-align', width: 160 },
        { headerName: "Location Code", field: "LCM_CODE", cellClass: 'grid-align', width: 110 },
        { headerName: "Location Name", field: "LOCATION", cellClass: 'grid-align', width: 110 },
        { headerName: "Company", field: "CNP_NAME", cellClass: 'grid-align', width: 110 },
        { headerName: "Address", field: "ADDRESS", cellClass: 'grid-align', width: 110 },
        { headerName: "Fax", field: "BC_FAX", cellClass: 'grid-align', width: 110 },
        { headerName: "Mobile", field: "BC_PHONE_NO", cellClass: 'grid-align', width: 110 },
        { headerName: "Landline Number", field: "BC_LAND_NO", cellClass: 'grid-align', width: 110 },
        { headerName: "Email", field: "BC_EMAIL", cellClass: 'grid-align', width: 110 },
        { headerName: "Created by", field: "BC_CREATED_BY", cellClass: 'grid-align', width: 110 },
        { headerName: "Request Date", field: "BC_CREATED_DT", cellClass: 'grid-align', width: 110 },
        {
            headerName: "Closure Date", field: "CLOSURE_DATE", template: '<span>{{data.CLOSURE_DATE | date:"dd/MM/yyyy" }}</span>',
            cellClass: 'grid-align', width: 200
        },
        { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 200 },

        // { headerName: "Zone", field: "Lcm_zone", cellClass: 'grid-align', width: 100 },
        { headerName: "TAT", field: "TAT", cellClass: 'grid-align', width: 100 },
        { headerName: "Remarks", field: "REMARKS", cellClass: 'grid-align', width: 110 }

    ],

        $scope.LoadData = function () {
            var searchval = $("#filtertxt").val();
            $("#btLast").hide();
            $("#btFirst").hide();
            var dataSource = {
                rowCount: null,
                getRows: function (params) {
                    var params = {
                        SearchValue: searchval,
                        PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                        PageSize: 10,
                        FromDate: $scope.UserWiseReq.FromDate,
                        ToDate: $scope.UserWiseReq.ToDate,
                        CompanyId: $scope.UserWiseReq.CNP_NAME[0].CNP_ID
                    };
                    var fromdate = moment($scope.UserWiseReq.FromDate);
                    var todate = moment($scope.UserWiseReq.ToDate);
                    if (fromdate > todate) {
                        $scope.GridVisiblity = false;
                        showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                    }
                    else {
                        progress(0, 'Loading...', true);
                        $scope.GridVisiblity = true;
                        $scope.gridata = [];
                        BusinessCardUserWiseService.GetGriddata(params).then(function (response) {
                            if (response.data != null) {
                                $scope.gridata = response.data.VMlist;
                                console.log($scope.gridata);
                                if ($scope.gridata.length != 0) {
                                    $scope.gridOptions.api.setRowData([]);
                                    $scope.gridOptions.api.setRowData($scope.gridata);
                                }
                                else {
                                    $("#btNext").attr("disabled", true);
                                    $scope.gridOptions.api.setRowData([]);
                                }
                            }
                            else {
                                $scope.gridOptions.api.setRowData([]);
                                progress(0, '', false);
                                $("#btNext").attr("disabled", true);
                                showNotification('error', 8, 'bottom-right', 'No Records Found');
                            }
                            progress(0, '', false);
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            $scope.gridOptions.api.setDatasource(dataSource);
        };

    $scope.ColumnNames = [];
    $scope.UserWiseReqLoad = function () {
        progress(0, 'Loading...', true);
        $scope.Pageload = {
            FromDate: $scope.UserWiseReq.FromDate,
            ToDate: $scope.UserWiseReq.ToDate,
            CompanyId: $scope.UserWiseReq.CNP_NAME[0].CNP_ID
        };
        BusinessCardUserWiseService.GetGriddata($scope.Pageload).then(function (data) {
            if (data.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                $scope.gridata = data.data.VMlist;
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            }

        }, function (error) {
            console.log(error);
        });
    }

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
    };


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.GenerateFilterPdf = function () {
        var columns = [{ title: "", key: "COL3" }, { title: "", key: "REQCOUNT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("UserWiseRequest.pdf");
    }

    $scope.GenerateFilterExcel = function () {
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "UserWiseRequest.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
    }
    $scope.GenReport = function (UserWiseReq, Type) {
        var searchval = $("#filtertxt").val();
        UserWiseReq.Type = Type;
        woobj = {};
        woobj.CompanyId = UserWiseReq.CNP_NAME[0].CNP_ID;
        woobj.Type = Type;
        woobj.SearchValue = searchval;
        woobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        woobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        woobj.FromDate = UserWiseReq.FromDate;
        woobj.ToDate = UserWiseReq.ToDate;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (opencallsdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/BusinessCardUserWise/GetUserWiseReport',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'UserWiseRequests.' + Type;
                document.body.appendChild(a);
                a.click();
            }),function (error,data, status, headers, config) {
            };
        };
    }
    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.UserWiseReq.FromDate = "";
                $scope.UserWiseReq.ToDate = "";
                break;
            case 'TODAY':
                $scope.UserWiseReq.FromDate = moment().format('MM/DD/YYYY');
                $scope.UserWiseReq.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.UserWiseReq.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.UserWiseReq.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.UserWiseReq.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.UserWiseReq.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.UserWiseReq.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.UserWiseReq.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.UserWiseReq.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.UserWiseReq.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.UserWiseReq.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.UserWiseReq.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
    setTimeout(function () {
        $scope.LoadData();
    }, 1500);

}]);
