﻿app.service("BusinessCardDeptCmprService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetGriddata = function (param) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BusinessCardDeptCmpr/GetUserWiseDetails', param)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BusinessCardDeptCmprController', ['$scope', '$q', '$http', 'UtilityService', '$timeout','BusinessCardDeptCmprService', function ($scope, $q, $http, UtilityService, $timeout, BusinessCardDeptCmprService) {
    $scope.DeptWise = {};
    $scope.Type = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.GridVisiblity = true;
    $scope.SelectedYears = [];
    $scope.varyears = [];
    $scope.EnableStatus = 0;
    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.DeptWise.CNP_NAME.push(a);
                });

                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 500);
  
    $scope.columnDefs = [
        { headerName: "Department/LOB", field: "DEPARTMENT", cellClass: 'grid-align', width: 140 },
        { headerName: "Location Code", field: "LCM_CODE", cellClass: 'grid-align', width: 110 },
        { headerName: "Location Name", field: "LOCATION", cellClass: 'grid-align', width: 110 },
        { headerName: "Company", field: "CNP_NAME", cellClass: 'grid-align', width: 110 },
        { headerName: "January", field: "January", cellClass: 'grid-align', width: 110 },
        { headerName: "Febrary", field: "Febrary", cellClass: 'grid-align', width: 110 },
        { headerName: "March", field: "March", cellClass: 'grid-align', width: 170 },
        { headerName: "April", field: "April", cellClass: 'grid-align', width: 110 },
        { headerName: "May", field: "May", cellClass: 'grid-align', width: 110 },
        { headerName: "June", field: "June", cellClass: 'grid-align', width: 110 },
        { headerName: "July", field: "July", cellClass: 'grid-align', width: 110 },
        { headerName: "August", field: "August", cellClass: 'grid-align', width: 110 },
        { headerName: "September", field: "September", cellClass: 'grid-align', width: 110 },
        { headerName: "October", field: "October", cellClass: 'grid-align', width: 110 },
        { headerName: "November", field: "November", cellClass: 'grid-align', width: 110 },
        { headerName: "December", field: "December", cellClass: 'grid-align', width: 110 }
    ]

       

        var OVERALL_COUNT = 0;
        $scope.LoadData = function () {
            var searchval = $("#filtertxt").val();
            $("#btLast").hide();
            $("#btFirst").hide();

            var dataSource = {
                rowCount: null,
                getRows: function (params) {
                    var params = {
                        SearchValue: searchval,
                        PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                        PageSize: 10,
                        Year: $('#Years').val(),
                        Company: $scope.DeptWise.CNP_NAME[0].CNP_ID
                    };
                    progress(0, 'Loading...', true);
                    $scope.GridVisiblity = true;
                    $scope.gridata = [];
                    console.log(params);
                    BusinessCardDeptCmprService.GetGriddata(params).then(function (response) {
                        if (response.data != null) {
                            $scope.gridata = response.data.VMlist;
                            if ($scope.gridata.length != 0) {
                                $scope.gridOptions.api.setRowData([]);
                                $scope.gridOptions.api.setRowData($scope.gridata);
                                OVERALL_COUNT = $scope.gridata[0].OVERALL_COUNT;
                            }
                        }
                        else {
                            $scope.gridOptions.api.setRowData([]);
                            $("#btNext").attr("disabled", true);
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'No Records Found');
                        }
                        progress(0, '', false);
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
            $scope.gridOptions.api.setDatasource(dataSource);
        };
   
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.ColumnNames = [];

    $scope.DepPageLoad = function () {
        progress(0, 'Loading...', true);
        $scope.Pageload = {
            Year: new Date().getFullYear(),
            Company: $scope.DeptWise.CNP_NAME[0].CNP_ID
        };
        BusinessCardDeptCmprService.GetGriddata($scope.Pageload).then(function (data) {
            if (data.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                $scope.gridata = data.data.VMlist;
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
    };

    $scope.GenReport = function (DeptWise, Type) {
        var searchval = $("#filtertxt").val();
        if ($scope.SelectedYears == "") { varyears = new Date().getFullYear() } else { varyears = $scope.SelectedYears }
        DeptWise.Type = Type;
        woobj = {};
        woobj.SearchValue = searchval;
         woobj.PageNumber = 1;
        woobj.PageSize =OVERALL_COUNT;
        woobj.Company = DeptWise.CNP_NAME[0].CNP_ID;
        woobj.Type = Type;
        woobj.Year = varyears;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (opencallsdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/BusinessCardDeptCmpr/GetDeptCountReport',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'DepartmentWiseCount.' + Type;
                document.body.appendChild(a);
                a.click();
            }) ,function (error,data, status, headers, config) {
            };
        };
    }
    setTimeout(function () {
        $scope.LoadData();
    }, 1000)
}]);