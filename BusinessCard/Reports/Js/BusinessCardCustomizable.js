﻿app.service("BusinessCardCustomizableService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (BCDetails) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BusinessCardCustomizable/GetBCDetails', BCDetails)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.LoadGrid = function (BCDetails) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BusinessCardCustomizable/LoadBCDetails', BCDetails)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BusinessCardCustomizable', ['$scope', '$q', '$http', 'BusinessCardCustomizableService', 'UtilityService', '$timeout','$filter', function ($scope, $q, $http, BusinessCardCustomizableService, UtilityService, $timeout, $filter) {
    $scope.BC_Customizable = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Department = [];
    $scope.Location = [];

        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.BC_Customizable.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 1; }
                UtilityService.getLocations(1).then(function (response) {
                    if (response.data != null) {
                        $scope.Location = response.data;
                        angular.forEach($scope.Location, function (value, key) {
                            value.ticked = true;
                        })
                        UtilityService.getDepartments().then(function (response) {
                            if (response.data != null) {
                                $scope.Department = response.data;
                                angular.forEach($scope.Department, function (value, key) {
                                    value.ticked = true;
                                })
                            }

                        });
                    }
                });
            }
        });
        
    


    //UtilityService.getLocations(1).then(function (response) {
    //    if (response.data != null) {
    //        $scope.Location = response.data;
    //        angular.forEach($scope.Location, function (value, key) {
    //            value.ticked = true;
    //        })
    //    }
    //});

    //UtilityService.getDepartments().then(function (response) {
    //    if (response.data != null) {
    //        $scope.Department = response.data;
    //        angular.forEach($scope.Department, function (value, key) {
    //            value.ticked = true;
    //        })
    //    }
        
    //});

    setTimeout(function () {
        $scope.LoadData(0, 'All');
    }, 2000);

    $scope.Pageload = function (stat, ReqType) {
        console.log($scope.BC_Customizable.CNP_NAME[0]);
        var params = {
            
            Deplst: $scope.BC_Customizable.Department,
            Request_Type: ReqType,
            STAT: stat,
            FromDate: $scope.BC_Customizable.FromDate,
            ToDate: $scope.BC_Customizable.ToDate,
            CNP_NAME: $scope.BC_Customizable.CNP_NAME[0].CNP_ID,
            loclst: $scope.BC_Customizable.Location,
        };
       
      
        BusinessCardCustomizableService.LoadGrid(params).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                angular.forEach($scope.gridata, function (data) {
                    if (data.CLOSURE_DATE == '1900-01-01T00:00:00') {
                        data.CLOSURE_DATE = 'Not Closed'
                    }
                });
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });
    }

    $scope.Cols = [
        { col: "Request Id", value: "REQ_ID", ticked: false },
        { COL: "Location", value: "LOCATION", ticked: false },
        { COL: "Employee Id/Name", value: "EMP_NAME", ticked: false },
        { COL: "Department", value: "DEP_NAME", ticked: false },
        { COL: "No.of Cards", value: "CARDS", ticked: false },
        { COL: "Designation", value: "DSN_AMT_TITLE", ticked: false },
        { COL: "Email", value: "BC_EMAIL", ticked: false },
        { COL: "Mobile", value: "BC_PHONE_NO", ticked: false },
        { COL: "Fax", value: "BC_FAX", ticked: false },
        { COL: "Land Number", value: "BC_LAND_NO", ticked: false },
        { COL: "Address", value: "BC_ADDR", ticked: false },
        { COL: "Remarks", value: "BC_REM", ticked: false },
        { COL: "Created Date", value: "CREATED_DATE", ticked: false },
        { COL: "Closure Date", value: "CLOSURE_DATE", ticked: false },
        { COL: "Status", value: "STATUS", ticked: false },
       // { COL: "Zone", value: "Lcm_zone", ticked: false },
        { COL: "TAT", value: "TAT", ticked: false }

    ];

    $scope.columnDefs = [
        { headerName: "Request Id", field: "REQ_ID", width: 70, cellClass: 'grid-align', width: 210 },
        { headerName: "Location Code", field: "LCM_CODE", width: 100, cellClass: 'grid-align' },
        { headerName: "Location Name", field: "LOCATION", width: 100, cellClass: 'grid-align' },
        { headerName: "Company", field: "CNP_NAME", width: 100, cellClass: 'grid-align' },
        { headerName: "Employee Name/Id", field: "EMP_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Department", field: "DEP_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "No.of Cards", field: "CARDS", cellClass: 'grid-align', width: 200 },
        { headerName: "Designation", field: "DSN_AMT_TITLE", cellClass: 'grid-align', width: 200 },
        { headerName: "Email", field: "BC_EMAIL", cellClass: 'grid-align', width: 200 },
        { headerName: "Mobile no.", field: "BC_PHONE_NO", cellClass: 'grid-align', width: 200 },
        { headerName: "Fax", field: "BC_FAX", cellClass: 'grid-align', width: 200 },
        { headerName: "Land number", field: "BC_LAND_NO", cellClass: 'grid-align', width: 200 },
        { headerName: "Address", field: "BC_ADDR", cellClass: 'grid-align', width: 200 },
        { headerName: "Remarks", field: "BC_REM", cellClass: 'grid-align', width: 200 },
        { headerName: "Created Date", field: "CREATED_DATE", template: '<span>{{data.CREATED_DATE | date:"dd/MM/yyyy" }}</span>', cellFilter: 'CREATED_DATE:\'dd/MM/yyyy\'', cellClass: 'grid-align', width: 200 },
        {
            headerName: "Closure Date", field: "CLOSURE_DATE", template: '<span>{{data.CLOSURE_DATE | date:"dd/MM/yyyy" }}</span>',
            cellClass: 'grid-align', width: 200
        },
        { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 200 },

       // { headerName: "Zone", field: "Lcm_zone", cellClass: 'grid-align', width: 100 },
        { headerName: "TAT", field: "TAT", cellClass: 'grid-align', width: 100 }
];



    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Location", field: "LOCATION",
            cellRenderer: {
                renderer: "group"
            }
        },
        onGridReady: function (params) {
            console.log(params);
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.LoadData = function (stat, ReqType) {

        if (ReqType == 1) {
            ReqType = $scope.BC_Customizable.Request_Type;
        }
        var params = {
            loclst: $scope.BC_Customizable.Location,
            Deplst: $scope.BC_Customizable.Department,
            Request_Type: ReqType,
            STAT: stat,
            FromDate: $scope.BC_Customizable.FromDate,
            ToDate: $scope.BC_Customizable.ToDate,
            CNP_NAME: $scope.BC_Customizable.CNP_NAME[0] == undefined ? "" : $scope.BC_Customizable.CNP_NAME[0].CNP_ID,
        };
        console.log(params);
        var fromdate = moment($scope.BC_Customizable.FromDate);
        var todate = moment($scope.BC_Customizable.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            BusinessCardCustomizableService.GetGriddata(params).then(function (data) {
                $scope.gridata = data.data;
                if ($scope.gridata == null) {
                    $scope.GridVisiblity = true;
                    $scope.gridOptions.api.setRowData([]);
                    $scope.gridOptions.api.refreshView();
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.GridVisiblity = true;
                    angular.forEach($scope.gridata, function (data) {
                        if (data.CLOSURE_DATE == '1900-01-01T00:00:00') {
                            data.CLOSURE_DATE = 'Not Closed'
                        }
                    });
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    var cols = [];
                    var unticked = _.filter($scope.Cols, function (item) {
                        return item.ticked == false;
                    });
                    var ticked = _.filter($scope.Cols, function (item) {
                        return item.ticked == true;
                    });
                    for (i = 0; i < unticked.length; i++) {
                        cols[i] = unticked[i].value;
                    }
                    $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                    cols = [];
                    for (i = 0; i < ticked.length; i++) {
                        cols[i] = ticked[i].value;
                    }
                    $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
                }
                progress(0, '', false);
            });
        }

    };
        $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Location", key: "LOCATION" }, { title: "Tower", key: "TOWER" },
            { title: "Employee", key: "EMP_NAME" },
            { title: "From Date", key: "From_Date" }, { title: "To Date", key: "To_Date" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("CustomizationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "CustomizationReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.BC_Customizable.Request_Type = "All";

    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.GenReport = function (BC_Customizable, Type) {
        progress(0, 'Loading...', true);
        var dataobj = {
            Request_Type: 'all',
            loclst: $scope.BC_Customizable.Location,
            Deplst: $scope.BC_Customizable.Department,
            STAT: 0,
            FromDate: $scope.BC_Customizable.FromDate,
            ToDate: $scope.BC_Customizable.ToDate,
            CNP_NAME: $scope.BC_Customizable.CNP_NAME[0].CNP_ID,
            Type: Type
        };

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            console.log(dataobj)
            $http({
                url: UtilityService.path + '/api/BusinessCardCustomizable/GetCustomizedData',
                method: 'POST',
                data: dataobj,
                responseType: 'arraybuffer'
            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'CustomizedReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {

            };
        };
    }
    //setTimeout(function () {
    //   $scope.Pageload();
    //}, 2000);
    //setTimeout(function () {
    //    $scope.LoadData(1, 'All');
    //}, 2000);
    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.BC_Customizable.FromDate = "";
                $scope.BC_Customizable.ToDate = "";
                break;
            case 'THISMONTH':
                $scope.BC_Customizable.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.BC_Customizable.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'TODAY':
                $scope.BC_Customizable.FromDate = moment().format('MM/DD/YYYY');
                $scope.BC_Customizable.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.BC_Customizable.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.BC_Customizable.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.BC_Customizable.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.BC_Customizable.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.BC_Customizable.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.BC_Customizable.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.BC_Customizable.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.BC_Customizable.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }

}]);