﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        /* .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-container {
            background-color: black;
        }*/

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /* .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }*/
    </style>
</head>
<body data-ng-controller="BusinessCardVendorWiseController" onload="setDateVals()" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="User Wise Request" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Request Summary</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" name="frmVendor" data-valid-submit="LoadData()">
                    <div class="clearfix row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Quick Select</label>
                                <br />
                                <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                    <option value="SELECT">Select Range</option>
                                    <option value="TODAY">Today</option>
                                    <option value="YESTERDAY">Yesterday</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 30 Days</option>
                                    <option value="THISMONTH">This Month</option>
                                    <option value="LASTMONTH">Last Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="VendorReq.FromDate" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="frmVendor.$submitted && frmVendor.FromDate.$invalid" style="color: red;">Please select from date</span>
                            </div>
                        </div>
                        <%-- <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>From Date<span class="text-danger">*</span></label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                    ErrorMessage="Please Select From Date" ValidationGroup="Val1" ControlToValidate="txtFdate"></asp:RequiredFieldValidator>
                                <div class="input-group date" id='fromdate'>
                                    <asp:TextBox ID="txtFdate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate') "></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>To Date<span class="text-danger">*</span></label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none"
                                    ErrorMessage="Please Select To Date" ValidationGroup="Val1" ControlToValidate="txtTdate"></asp:RequiredFieldValidator>
                                <div class="input-group date" id='todate'>
                                    <asp:TextBox ID="txtTdate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="VendorReq.ToDate" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="frmVendor.$submitted && frmVendor.ToDate.$invalid" style="color: red;">Please select to date</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                            <div class="form-group">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="VendorReq.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="VendorReq.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmVendor.$submitted && frmVendor.Company.$invalid" style="color: red">Please select company </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-63 col-xs-12">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="table2">
                            <br />
                            <a data-ng-click="GenReport(VendorReq,'doc')"><i id="word" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(VendorReq,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(VendorReq,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                    </div>
                    <%--</form>
                            <form id="form2" data-ng-show="GridVisiblity">--%>
                    <div class="row" style="padding-left: 17px;">
                        <div class="col-md-12" style="height: 320px">
                            <%--<input id="filtertxt" placeholder="Filter..." type="text" class="form-control" style="width: 25%;" />--%>
                            <div class="input-group" style="width: 20%">
                                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary custom-button-color" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 290px; width: auto"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <%-- </div>
            </div>--%>
    </div>
    <%--  </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Js/BusinessCardVendorWise.js" defer></script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script type="text/javascript" defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        $(document).ready(function () {
            setDateVals();
        });
        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FROM_DATE').datepicker('setDate', new Date(moment().subtract(29, 'days').format('DD-MMM-YYYY')));
            $('#TO_DATE').datepicker('setDate', new Date(moment().format('DD-MMM-YYYY')));
        }
    </script>
</body>
</html>
