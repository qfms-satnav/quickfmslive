﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusinessCardRequest.aspx.cs" Inherits="Business_Card_BusinessCardRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
        <style>
        #AutoCompleteExtender2_completionListElem {
            padding-left:10px !important;
        }
        #AutoCompleteExtender1_completionListElem {
            padding-left:10px !important;
        }
    </style>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div id="header" runat="server" class="panel-heading">
                    <h3 class="panel-title">Business Card Request</h3>
                </div>
            </div>
            <div class="card">
                <form id="Form1" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" />
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="form-group">
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red">
                                    </asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="trLName" runat="server">
                                        <label>Employee Id/Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEmployee"
                                            Display="None" ErrorMessage="Please Select Employee Id/Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ddlEmployee" runat="server" ToolTip="Search Employee" AutoPostBack="true" OnTextChanged="ddlEmployee_SelectedIndexChanged" data-live-search="true" CssClass="form-control"> </asp:TextBox>
                                        <span runat="server" id="cusCustom" style="color: red" visible="false">Please enter valid user</span>
                                        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="ddlEmployee" MinimumPrefixLength="2" EnableCaching="false"
                                            CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetDetails" ServicePath="~/Autocompletetype.asmx">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div1" runat="server">
                                        <label>Department<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDepartment"
                                            Display="None" ErrorMessage="Please Select Department" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ddlDepartment" runat="server" ToolTip="Search Department" AutoPostBack="true" CssClass="form-control">                                                
                                        </asp:TextBox>
                                        <%--   <asp:ScriptManager ID="ScriptManager2" runat="server" EnablePageMethods="true"></asp:ScriptManager>--%>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="ddlDepartment" MinimumPrefixLength="2" EnableCaching="false"
                                            CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetDepartment" ServicePath="~/Autocompletetype.asmx">
                                        </asp:AutoCompleteExtender>

                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div2" runat="server">
                                        <label>Location<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Select Location" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ddlLocation" runat="server" ToolTip="Search Location" AutoPostBack="true" OnTextChanged="ddlLocation_SelectedIndexChanged" CssClass="form-control">                                                
                                        </asp:TextBox>
                                        <span runat="server" id="cusCustom1" style="color: red" visible="false">Please enter valid location</span>
                                        <%--   <asp:ScriptManager ID="ScriptManager2" runat="server" EnablePageMethods="true"></asp:ScriptManager>--%>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="ddlLocation" MinimumPrefixLength="2" EnableCaching="false"
                                            CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetLocations" ServicePath="~/Autocompletetype.asmx">
                                        </asp:AutoCompleteExtender>

                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div3" runat="server">
                                        <label>Designation<span style="color: red;">*</span></label>

                                        <asp:TextBox ID="ddlDesignation" runat="server" ToolTip="Search Designation" AutoPostBack="true" CssClass="form-control">                                                
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlDesignation"
                                            Display="None" ErrorMessage="Please Select Designation " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <%--   <asp:ScriptManager ID="ScriptManager2" runat="server" EnablePageMethods="true"></asp:ScriptManager>--%>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" TargetControlID="ddlDesignation" MinimumPrefixLength="2" EnableCaching="false"
                                            CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetDesignation" ServicePath="~/Autocompletetype.asmx">
                                        </asp:AutoCompleteExtender>

                                    </div>
                                </div>
                                <%--<div class="form-group col-sm-3 col-xs-6">
                                        <div class="form-group" id="Div1" runat="server">
                                            <label>Department/LOB<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDepartment"
                                                Display="None" ErrorMessage="Please Select Department/LOB " InitialValue="--Select Department/LOB--"  ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList enabled="true" ID="ddlDepartment" runat="server" AutoPostBack="True" ToolTip="Select Department/LOB" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                                <asp:ListItem >--Select--</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>--%>


                                <%-- <div class="form-group col-sm-3 col-xs-6">
                                        <div class="form-group" id="Div2" runat="server">
                                            <label>Location<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlLocation"
                                                Display="None" ErrorMessage="Please Select Location" InitialValue="--Select Location--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLocation" runat="server" ToolTip="Select Location" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem >--Select--</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>--%>
                                <%--<div class="form-group col-sm-3 col-xs-6">
                                        <div class="form-group" id="Div3" runat="server">
                                            <label>Designation<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlDesignation"
                                                Display="None" ErrorMessage="Please Select Designation" InitialValue="--Select Designation--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlDesignation" runat="server" ToolTip="Select Designation" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem >--Select--</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>--%>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <asp:TextBox ID="txtEmailId" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Fax</label>
                                        <asp:TextBox ID="txtFax" runat="server" MaxLength="15" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Telephone</label>
                                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="15" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Mobile Number<span style="color: red;">*</span></label>
                                        <asp:TextBox ID="txtMobile" runat="server" MaxLength="10" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMobNo" runat="server" ErrorMessage="Please Enter Mobile Number" Display="None" ValidationGroup="Val1" ForeColor="Red" ControlToValidate="txtMobile"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revMobNo" runat="server" ErrorMessage="Please Enter Valid Mobile Number." ValidationExpression="^([0-9]{10})$" ControlToValidate="txtMobile" ValidationGroup="Val1" ForeColor="Red" Display="None"></asp:RegularExpressionValidator>
                                        <br />
                                    </div>
                                </div>
                                <%--  
                                    <div class="form-group col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Mobile <span style="color: red;">*</span></label>
                                           
                                            <asp:TextBox ID="txtMobile" runat="server" MaxLength="10" CssClass="form-control" ></asp:TextBox>
                                            <span id="errmobile" style="color:red"></span>
                                        </div>
                                    </div>--%>
                            </div>

                            <div class="row">

                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Address <span style="color: red;">*</span></label>
                                        <div class="row"></div>
                                        <asp:TextBox ID="txtAddress" runat="server" MaxLength="500" TextMode="MultiLine" CssClass="form-control" req Rows="5"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpAddress" runat="server" ControlToValidate="txtAddress"
                                            ErrorMessage="Please Enter Valid Address" Display="None"
                                            ValidationGroup="Val5">
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please Enter Address" Display="None" ValidationGroup="Val1" ForeColor="Red" ControlToValidate="txtAddress"></asp:RequiredFieldValidator>
                                    </div>
                                </div>

                                <%--<div class="form-group col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Address <span style="color: red;">*</span></label>
                                           
                                            <asp:TextBox ID="txtAddress" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                             <span id="erraddress" style="color:red"></span>
                                        </div>
                                    </div>--%>
                               <%-- <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Number of cards</label>
                                        <asp:RadioButtonList ID="rbl" runat="server" RepeatDirection="Horizontal"
                                            RepeatLayout="Table" CssClass="RBL">
                                            <asp:ListItem Text="100" Selected="True" />
                                            <asp:ListItem Text="200" />
                                            <asp:ListItem Text="Other" />
                                        </asp:RadioButtonList>
                                    </div>
                                     </div>--%>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="quantityGroup">
                                        <label>Quantity</label>
                                        <asp:RadioButtonList ID="rbl" runat="server" RepeatDirection="Horizontal"
                                            RepeatLayout="Table" CssClass="RBL" onclick="toggleTextField()">
                                            <asp:ListItem Text="100" Selected="True" />
                                            <asp:ListItem Text="200" />
                                            <asp:ListItem Text="Other" />
                                        </asp:RadioButtonList>
                                        <asp:TextBox ID="otherQuantity" runat="server" CssClass="form-control" style="display: none;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                                            ErrorMessage="Please Enter Valid Remarks" Display="None"
                                            ValidationGroup="Val1">
                                        </asp:RegularExpressionValidator>

                                        <asp:TextBox ID="txtRemarks" runat="server" MaxLength="500" TextMode="MultiLine" Height="30%" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="Vendor" runat="server" class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div4" runat="server">
                                        <label>Vendor<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlVendor"
                                            Display="None" ErrorMessage="Please Select Vendor" InitialValue="--Select Vendors--" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlVendor" runat="server" ToolTip="Select Vendor" CssClass="form-control selectpicker" data-live-search="true">
                                            <asp:ListItem Value="1">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="StRem" runat="server" class="row">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div5" runat="server">
                                        <label>Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlStatus"
                                            Display="None" ErrorMessage="Please Select Status" InitialValue="--Select Status--" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlStatus" runat="server" ToolTip="Select Status" CssClass="form-control selectpicker" data-live-search="true">
                                            <asp:ListItem Value="1">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div6" runat="server">
                                        <label>Approver remarks<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="RegularExpAppRem" runat="server" ControlToValidate="txtAppr"
                                            ErrorMessage="Please Enter Valid Remarks" Display="None"
                                            ValidationGroup="Val1">
                                        </asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtAppr" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group" style="padding-top: 20px">
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color" OnClick="btnSubmit_Click"></asp:Button>
                                        <asp:Button ID="btnModify" runat="server" Text="Modify" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color" OnClick="btnModify_Click"></asp:Button>
                                        <asp:Button ID="btnApproval" runat="server" Text="Approve" OnClientClick="return approve();" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color" OnClick="btnApproval_Click"></asp:Button>
                                        <asp:Button ID="btnRejection" runat="server" Text="Reject" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color" OnClick="btnRejection_Click"></asp:Button>
                                        <asp:Button ID="btnAcknow" runat="server" Text="User Acknowledge" CssClass="btn btn-primary custom-button-color" OnClick="btnAcknow_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
        <br />
        <br />
        <br />
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function refreshSelectpicker() {
            $("#<%=ddlVendor.ClientID%>").selectpicker();
            $("#<%=ddlStatus.ClientID%>").selectpicker();

        }
        refreshSelectpicker();
    </script>
     <script>
         function toggleTextField() {
             var otherRadio = document.querySelector('#<%= rbl.ClientID %> input[value="Other"]');
                                        var otherTextField = document.querySelector('#<%= otherQuantity.ClientID %>');
             if (otherRadio.checked) {
                 otherTextField.style.display = 'block';
             } else {
                 otherTextField.style.display = 'none';
             }
         }
     </script>
    <script type="text/javascript" defer>
        $(document).ready(function () {
            //$("#btnApproval").one('click', function (event) {
            //    event.preventDefault();
            //    $(this).prop('disabled', true);
            //    return true;
            //});
        });
        $('#<%=btnSubmit.ClientID%>').click(function () {
            var isValid = false;
            isValid = Page_ClientValidate('Val1');
            if (isValid) {
                $("#btnSubmit").one('click', function (event) {
                    event.preventDefault();
                    $(this).prop('disabled', true);
                });
                return isValid
            }
            else {
                return isValid
            }
        });
        $('#<%=btnModify.ClientID%>').click(function () {
            var isValid = false;
            isValid = Page_ClientValidate('Val1');
            if (isValid) {
                $("#btnModify").one('click', function (event) {
                    event.preventDefault();
                    $(this).prop('disabled', true);
                });
                return isValid
            }
            else {
                return isValid
            }
        });

        function approve() {
            var isValid = false;
            isValid = Page_ClientValidate('Val2');
            if (isValid) {
                $("#btnApproval").one('click', function (event) {
                    Page_BlockSubmit = false;
                    event.preventDefault();
                    $(this).prop('disabled', true);

                });
                return isValid
            }
            else {
                return isValid
            }

        }
        function modify() {
            $("#btnModify").one('click', function (event) {

                event.preventDefault();
                $(this).prop('disabled', true);
            });
            return true;
        }
        function reject() {
            $("#btnRejection").one('click', function (event) {
                event.preventDefault();
                $(this).prop('disabled', true);
            });
            return true;
        }
        function usrAck() {
            $("#btnAcknow").one('click', function (event) {
                event.preventDefault();
                $(this).prop('disabled', true);
            });
            return true;
        }

    </script>
</body>
</html>
