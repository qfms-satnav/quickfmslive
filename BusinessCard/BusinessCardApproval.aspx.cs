﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BusinessCard_BusinessCardApproval : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GetBusinessCardsDetails();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataSet ds;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_Approval_Search");
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@Search_By", txtSearch.Text, DbType.String);
        ds = sp.GetDataSet();
        gvcards.DataSource = sp.GetDataSet();
        gvcards.DataBind();
    }
    
    public void GetBusinessCardsDetails()
    {
        DataSet ds;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_GET_CARD_DETAILS");
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        gvcards.DataSource = sp.GetDataSet();
        gvcards.DataBind();
    }
    protected void gvcards_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GetBusinessCardsDetails();
        gvcards.PageIndex = e.NewPageIndex;
        gvcards.DataBind();
    }
}