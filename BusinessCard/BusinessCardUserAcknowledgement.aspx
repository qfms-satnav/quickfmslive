﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusinessCardUserAcknowledgement.aspx.cs" Inherits="BusinessCard_BusinessCardUserAcknowledgement" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-class="with-scroll">--%>
                <%--<div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Confirm Acknowledgement</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 50px;">--%>
                <%--<div class="clearfix">--%>
                <div class="form-group">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"> </asp:Label>
                </div>
                <%--</div>--%>
                <form id="form1" runat="server">
                    <div class="clearfix">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical">
                            <asp:GridView ID="gvcards" runat="server" AllowPaging="True" AllowSorting="False"
                                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Records Found" CssClass="table GridStyle" GridLines="None" OnRowCommand="gvcards_RowCommand" OnPageIndexChanging="gvcards_PageIndexChanging">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnksurrender" runat="server" Text='<%# Eval("BC_REQ_ID")%> ' CommandArgument="<%# Container.DataItemIndex %>" CommandName="UserAck"></asp:LinkButton>
                                        </ItemTemplate>
                                        <%-- <ItemTemplate>
                                                        <a href="#" onclick="showPopWin('<%# Eval("BC_REQ_ID") %>')">
                                                            <asp:Label ID="lblReq" runat="server" Text='<%# Eval("BC_REQ_ID") %>'></asp:Label>
                                                        </a>
                                                        <%--<asp:LinkButton ID="hLinkDetails" runat="server" CommandName="DataEditing" Text='<%# Eval("BC_REQ_ID")%> ' CommandArgument='<% #Bind("BC_REQ_ID")%>'></asp:LinkButton>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Requested Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" CssClass="lblASTCode" Text='<%#Eval("BC_CREATED_DT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Requested By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReq" runat="server" CssClass="lblStatus" Text='<%#Bind("AUR_KNOWN_AS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLoc" runat="server" CssClass="lblStatus" Text='<%#Bind("LCM_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lolLOB" runat="server" CssClass="lblStatus" Text='<%#Bind("DEP_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDes" runat="server" CssClass="lblStatus" Text='<%#Bind("DSN_AMT_TITLE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddr" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_ADDR")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_EMAIL")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile Number">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMob" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_PHONE_NO")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcards" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_NO_OF_CARDS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSts" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_STA_DESC")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </asp:Panel>
                        <br />
                        <div id="Details" runat="server">
                            <div class="clearfix">


                                <%--  <div class="form-group col-sm-3 col-xs-6">
                                                <div class="form-group" id="Div2" runat="server">
                                                    <label>Department/LOB<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDepartment"
                                                        Display="None" ErrorMessage="Please Select Department/LOB " InitialValue="--Select Department/LOB--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" ToolTip="Select Department/LOB" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                                        <asp:ListItem Value="1">--Select--</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>--%>

                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div4" runat="server">
                                        <label>Department<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlDepartment"
                                            Display="None" ErrorMessage="Please Select Department/LOB" InitialValue="--Select Department--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ddlDepartment" runat="server" ToolTip="Select Employee" CssClass="form-control selectpicker" data-live-search="true">
                                        </asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="trLName" runat="server">
                                        <label>Employee Id/Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEmployee"
                                            Display="None" ErrorMessage="Please Select Employee Id/Name " InitialValue="--Select Employee--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ddlEmployee" runat="server" ToolTip="Select Employee" CssClass="form-control selectpicker" data-live-search="true">
                                        </asp:TextBox>

                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div2" runat="server">
                                        <label>Location<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Please Select ddlLocation " InitialValue="--Searh Location--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ddlLocation" runat="server" ToolTip="Select Location" CssClass="form-control selectpicker" data-live-search="true">
                                        </asp:TextBox>

                                    </div>
                                </div>
                                <%--                                            <div class="form-group col-sm-3 col-xs-6">
                                                <div class="form-group" id="Div3" runat="server">
                                                    <label>Location<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlLocation"
                                                        Display="None" ErrorMessage="Please Select Location" InitialValue="--Select Location--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlLocation" runat="server" ToolTip="Select Location" CssClass="form-control selectpicker" data-live-search="true">
                                                        <asp:ListItem Value="1">--Select--</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>--%>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div3" runat="server">
                                        <label>Designation<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlDesignation"
                                            Display="None" ErrorMessage="Please Select Designation " InitialValue="--Searh Designation--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ddlDesignation" runat="server" ToolTip="Select Designation" CssClass="form-control selectpicker" data-live-search="true">
                                        </asp:TextBox>

                                    </div>
                                </div>
                                <%--  <div class="form-group col-sm-3 col-xs-6">
                                                <div class="form-group" id="Div4" runat="server">
                                                    <label>Designation<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlDesignation"
                                                        Display="None" ErrorMessage="Please Select Designation" InitialValue="--Select Designation--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlDesignation" runat="server" ToolTip="Select Designation" CssClass="form-control selectpicker" data-live-search="true">
                                                        <asp:ListItem Value="1">--Select--</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>--%>
                            </div>

                            <div class="clearfix">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <asp:TextBox ID="txtEmailId" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Fax</label>
                                        <asp:TextBox ID="txtFax" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Telephone</label>
                                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Mobile Number</label>
                                        <asp:TextBox ID="txtMobile" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <asp:TextBox ID="txtAddress" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <asp:RadioButtonList ID="rbl" runat="server" RepeatDirection="Horizontal"
                                            RepeatLayout="Table" CssClass="RBL">
                                            <asp:ListItem Text="100" Selected="True" />
                                            <asp:ListItem Text="200" />
                                            <asp:ListItem Text="Other" />
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <asp:TextBox ID="txtRemarks" runat="server" MaxLength="500" TextMode="MultiLine" Height="30%" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="Vendor" runat="server" class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div5" runat="server">
                                        <label>Vendor<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlVendor"
                                            Display="None" ErrorMessage="Please Select Vendor" InitialValue="--Select Vendors--" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlVendor" runat="server" ToolTip="Select Vendor" CssClass="form-control selectpicker" data-live-search="true">
                                            <asp:ListItem Value="1">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div id="StRem" runat="server" class="clearfix">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div6" runat="server">
                                        <label>Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlStatus"
                                            Display="None" ErrorMessage="Please Select Status" InitialValue="--Select Status--" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlStatus" runat="server" ToolTip="Select Status" CssClass="form-control selectpicker" data-live-search="true">
                                            <asp:ListItem Value="1">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <div class="form-group" id="Div7" runat="server">
                                        <label>Approver Remarks</label>
                                        <asp:TextBox ID="txtAppr" runat="server" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <div class="form-group" style="padding-top: 20px">
                                        <asp:Button ID="btnAcknow" runat="server" Text="User Acknowledge" CssClass="btn btn-primary custom-button-color" OnClick="btnAcknow_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <%-- </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
