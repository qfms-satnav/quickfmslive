﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusinessCardApproval.aspx.cs" Inherits="BusinessCard_BusinessCardApproval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Business Card Approval</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <div class="clearfix">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                        <div class="row" style="padding-top: 10px;">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="row" style="margin-bottom: 5px;">
                                        <label class="col-md-6 control-label">Search by Requisition ID / Employee ID / Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTxtEmpId" runat="server" ControlToValidate="txtSearch" Display="None" ErrorMessage="Please Enter Employee ID"
                                            ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtSearch" runat="Server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-12">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" ValidationGroup="Val1" OnClick="btnSearch_Click" />

                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical">
                            <asp:GridView ID="gvcards" runat="server" AllowPaging="True" AllowSorting="False"
                                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Approvals Found" CssClass="table GridStyle" GridLines="None" OnPageIndexChanging="gvcards_PageIndexChanging">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <a href="#" onclick="showPopWin('<%# Eval("BC_REQ_ID") %>')">
                                                <asp:Label ID="lblReq" runat="server" Text='<%# Eval("BC_REQ_ID") %>'></asp:Label>
                                            </a>
                                            <%--<asp:LinkButton ID="hLinkDetails" runat="server" CommandName="DataEditing" Text='<%# Eval("BC_REQ_ID")%> ' CommandArgument='<% #Bind("BC_REQ_ID")%>'></asp:LinkButton>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Requested Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" CssClass="lblASTCode" Text='<%#Eval("BC_CREATED_DT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Requested By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReq" runat="server" CssClass="lblStatus" Text='<%#Bind("AUR_KNOWN_AS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLoc" runat="server" CssClass="lblStatus" Text='<%#Bind("LCM_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department">
                                        <ItemTemplate>
                                            <asp:Label ID="lolLOB" runat="server" CssClass="lblStatus" Text='<%#Bind("DEP_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDes" runat="server" CssClass="lblStatus" Text='<%#Bind("DSN_AMT_TITLE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddr" runat="server" Width="200" CssClass="lblStatus" Text='<%#Bind("BC_ADDR")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_EMAIL")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile Number">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMob" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_PHONE_NO")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcards" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_NO_OF_CARDS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSts" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_STA_DESC")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rejected Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRejsts" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_STATUS_DESC_2008")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRem" runat="server" CssClass="lblRem" Text='<%#Bind("BC_REM")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                        <div class="align-items-center justify-content-between">
                            <h5 class="modal-title" id="H1">Business Card Details</h5>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" src="#" width="100%" height="450px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "BusinessCardRequest.aspx?ApprovalBID=" + id);
            $("#myModal").modal('show');
        }
    </script>
</body>
</html>
