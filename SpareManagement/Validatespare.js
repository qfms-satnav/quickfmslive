﻿
app.service("ValidatespareService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Validatespare/GetData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetValidateDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Validatespare/GetValidateDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetAssetDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Validatespare/GetAssetDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
    this.SearchData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Validatespare/SearchData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.UpdateStatus = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Validatespare/GetUpdateStatus', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('ValidatespareController', ['$scope', '$q', '$http', 'ValidatespareService', 'UtilityService', '$timeout', '$filter',

    function ($scope, $q, $http, ValidatespareService, UtilityService, $timeout, $filter) {
        $scope.Validatespare = {};
        $scope.gridata = [];
        $scope.Priority = [];
        $scope.Locations = [];
        $scope.DetailsGrid = false;
        $scope.MainGrid = true;
        $scope.divBtn = false;
        $scope.ASP_REQ_ID_DTLS = "";

        //UtilityService.getLocations(2).then(function (response) {
        UtilityService.getLocationsInventory(2).then(function (response) {
            if (response.data != null) {
                response.data.push({ LCM_CODE: 'All', LCM_NAME: 'All' });
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    var a = _.find($scope.Locations);
                    a.ticked = true;
                })
                $scope.GetAssetDetails();
            }
        });

        $scope.GetAssetDetails = function () {
       
            var objs = {

                LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; })
                    .map(function (x) { return x.LCM_CODE; }).join(','),
            }

            ValidatespareService.GetAssetDetails(objs).then(function (response) {

                if (response.data != null) {
                    $scope.Type = response.data;
                    angular.forEach($scope.Type, function (value, key) {
                        var a = _.find($scope.Type);
                        a.ticked = true;
                    });
                    $scope.GetData();
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);

                $scope.gridOptions1.api.setRowData([]);
                $scope.DetailsGrid = false;
                $scope.divBtn = false;


            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.GetData = function () {
            var searchval = $("#filtertxt").val();
            var dataSource = {
                getRows: function (obj2) {
                    var obj2 = {
                        //pagination: true,
                        SearchValue: searchval,
                        PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                        PageSize: 10,
                        LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                    };                
                   
            $scope.gridata = [];
                    console.log(obj2);
            ValidatespareService.GetData(obj2).then(function (data) {

                $scope.gridata = data.data;
                if ($scope.gridata == null) {
                    $scope.gridOptions.api.setColumnDefs(data.columnDefs);
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Records Found');
                }
                else {
                    showNotification('', 8, 'bottom-right', '');
                    $scope.gridOptions.api.setColumnDefs(data.columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000);
                }
                progress(0, 'Loading...', false);
            });
        
                }
            };
            $scope.gridOptions.api.setDatasource(dataSource);

        }

        var columnDefs = [
        //$scope.columnDefs = [

            { headerName: "Ticket Id", align: 'left', width: 143, field: "ASP_REQ_ID_DTLS", cellClass: '"style =" width:1181px""', filter: 'set', template: '<a ng-click="getvalidateDetails(data)" style="cursor: pointer;">{{data.ASP_REQ_ID_DTLS}}</a>' },
        ];
         
        $scope.getvalidateDetails = function (index) {
            var params = {
                ASP_REQ_ID_DTLS: index.ASP_REQ_ID_DTLS
            }
            $scope.ASP_REQ_ID_DTLS = index.ASP_REQ_ID_DTLS;
            ValidatespareService.GetValidateDetails(params).then(function (data) {

                $scope.gridata = data.data;
                if ($scope.gridata == null) {

                    $scope.gridOptions.api.setRowData([]);
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Records Found');
                }
                else {
                    //showNotification('', 8, 'bottom-right', '');
                    $scope.DetailsGrid = true;
                    //$scope.MainGrid = false;
                    $scope.MainGrid = true;

                    $scope.divBtn = true;

                    $scope.gridOptions1.api.setRowData($scope.gridata);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000);
                }
                progress(0, 'Loading...', false);
            });

        }

        //        /* Accept Function */
        //        $scope.getData = function () {
        //          
        //            console.log();

        //            var params = {
        //                ASP_ITEM_REQUISITION: index.ASP_ITEM_REQUISITION
        //            }
        //            ValidatespareService.GetData(params).then(function (data){
        //              
        //                $scope.getData = data.data;
        //                if ($scope.gridata.api.setRowData([]));
        //                progress(0, '', false);
        //                showNotification('error', 8, 'bottom-right', 'No Records Found');
        //            }
        //             else {
        //                showNotification('', 8, 'bottom-right', '');
        //                $scope.DetailsGrid = true;
        //                $scope.MainGrid = false;
        //                $scope.gridOptions1.api.setRowData($scope.gridata);
        //                setTimeout(function () {
        //                    progress(0, 'Loading...', false);
        //                }, 1000);
        //            }
        //            progress(0, 'Loading...', false);
        //        });
        //}

        var columnDefs1 = [
            { headerName: "Ticket Id", field: "AAS_SPAREPART_ID", width: 140, cellClass: 'grid-align' },
            { headerName: "Equipment", field: "AAS_AAT_CODE", width: 140, cellClass: 'grid-align' },
            { headerName: "Part Name", field: "AAS_SPAREPART_NAME", width: 140, cellClass: 'grid-align' },
            { headerName: "Part Descrpition", field: "AAS_SPAREPART_DES", width: 140, cellClass: 'grid-align' },
            //{ headerName: "Available Quantity", field: "AAS_SPAREPART_AVBQUANTITY", width: 130, cellClass: 'grid-align' },
            { headerName: "Utilized Quantity", field: "AAS_SPAREPART_MINQUANTITY", width: 130, cellClass: 'grid-align' },
            { headerName: "Unit Cost", field: "AAS_SPAREPART_COST", width: 120, cellClass: 'grid-align' },
            { headerName: "Total Cost", field: "AAS_SPAREPART_TOTCOST", width: 120, cellClass: 'grid-align' },
            { headerName: "Remarks", field: "AAS_REMARKS", width: 120, cellClass: 'grid-align' },
            { headerName: "Updated Date", field: "ASP_UPT_DATE", width: 120, cellClass: 'grid-align' },
            // { headerName: "Image", field: "AAS_IMAGE_PATH", width: 100, cellClass: 'grid-align' }

        ];

        $scope.gridOptions = {
            //columnDefs:  columnDefs,
            //enableFilter: true,
            //angularCompileRows: true,
            //enableCellSelection: false,
            //enableColResize: false,
            //pagination: true,
            ////paginationPageSize:10,
            columnDefs: columnDefs,
            enableFilter: true,
            angularCompileRows: true,
            enableCellSelection: false,
            rowData: null,
            enableColResize: false,
        };

        $scope.gridOptions1 = {
            columnDefs: columnDefs1,
            enableFilter: true,
            angularCompileRows: true,
            enableCellSelection: false,
            enableColResize: true,
        };

        $scope.LoadData = function () {

            //$scope.chA = function () {
            //    var obj = {

            //        LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
            //        SUBC_NAME: _.filter($scope.Validatespare.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_NAME; }).join(','),
            //    }

            //    ValidatespareService.SearchData(obj).then(function (response) {

            //        if (response.data != null) {
            //            $scope.gridOptions.api.setColumnDefs(columnDefs);
            //            $scope.gridOptions.api.setRowData(response.data);
            //        }
            //        else {
            //            showNotification('error', 8, 'bottom-right', response.Message);
            //            $scope.gridOptions.api.setColumnDefs(columnDefs);
            //            $scope.gridOptions.api.setRowData([]);
            //        }
            //    }, function (response) {
            //        progress(0, '', false);

            //    });

            //    $scope.LoadData();

            //    var dataSource = {
            //        pagination: true,
            //        rowModelType: 'infinite',
            //        cacheBlockSize: 20, // you can have your custom page size
            //        paginationPageSize: 20 //pagesize
            //    };

            //    $scope.gridOptions.api.setDatasource(dataSource);
            //}

            $scope.BackClicked = function () {
                $scope.DetailsGrid = false;
                $scope.MainGrid = true;
                $scope.divBtn = false;
                $scope.ASP_REQ_ID_DTLS = ''
                $scope.GetData();
            }
        }

        $scope.UpdateStatus = function (status) {           
            var obj = {
                REMARKS: $scope.Validatespare.SPARE_REM,
                TYPE: status,
                REQ_ID: $scope.ASP_REQ_ID_DTLS
            }
            progress(0, '', true);
            ValidatespareService.UpdateStatus(obj).then(function (response) {  
                if (response.data != null) {
                    //showNotification('success', 8, 'bottom-right', "Updated Successfully");  
                    if (status == "Approve") {
                        setTimeout(function () { showNotification('success', 8, 'bottom-right', "Updated Successfully"); }, 1500);
                    }
                    else {
                        setTimeout(function () { showNotification('error', 8, 'bottom-right', "Rejected Successfully"); }, 1500);
                    }
                    $scope.gridOptions1.api.setRowData([]);                   
                    $scope.BackClicked();
                    progress(0, '', false);                                     
                } else
                    setTimeout(function () { showNotification('error', 8, 'bottom-right', response.Message); }, 1500);
                    

            });
            //console.log("this id " + obj);
            //var response = ValidatespareService.UpdateStatus(obj);

            //if (status == 'Approve') {
            //    $scope.gridOptions1.api.setRowData([])
            //    showNotification('error', 8, 'bottom-right', "Updated Successfully");
            //}
            //else if (status == 'Reject') {
            //    $scope.gridOptions1.api.setRowData([])
            //    showNotification('error', 8, 'bottom-right', "Updated Successfully");
            //}
            //else{
            //    showNotification('Failed to update');
            //}
        }

        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);
            var Filterparams = {
                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "Validatespare_Report.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();
        }

        $("#filtertxt").change(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keydown(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keyup(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).bind('paste', function () {
            onReq_SelSpacesFilterChanged($(this).val());
        });
        function onReq_SelSpacesFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
        }

        setTimeout(function () { $scope.LoadData(); }, 3000);
    }]);
