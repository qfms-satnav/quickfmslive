﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<script runat="server"></script>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />--%>
    
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />


    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
          
        #btFirst{
            display:none;
        }
        #btLast{
            display:none;
        }

        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .auto-style1 {
            height: 50px;
        }
        .ag-header-row{
            top: 0px;
            height: 25px;
            background-color: #dbdeed; 
        }
        .ag-header-cell-resize
        {
            background-color: #dbdeed;
        }

        #south  {
          text-align: right;
        }
        .ag-header-cell {
            background-color: #1c2b36;
        }
        .ag-blue .ag-header-cell-label {
            padding: 4px 2px 4px 2px; 
            font-weight: bold;
            color: black;
            background-color: #dbdeed;
        }

          .ag-header-cell {
            background-color: #dbdeed;
        }

        .ag-blue .ag-header-cell-label {
            color: black;
        }
        .bxheader {
            width: 100%;          
            background-color:black;
            padding: 5px 10px;
        }
        .ag-blue .ag-row-odd {
            background-color: #fff;
        }
         .panel-title{
            color:black;
            font-weight:bold;
        }
    </style>
</head>
<body data-ng-controller="ValidatespareController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="SpareParts Summary" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Validate Spare</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" name="Validatespare" data-valid-submit="LoadData()" novalidate>
                                <div class="auto-style1">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Site</label>
                                                <div isteven-multi-select data-input-model="Locations" data-output-model="Validatespare.Locations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME" selection-mode="single"
                                                    data-on-item-click="GetAssetDetails()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="Validatespare.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="Validatespare.$submitted && Validatespare.Location.$invalid" style="color: red">Please Select Locaton </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                            <div class="form-group" data-ng-class="{'has-error': Validatespare.$submitted && Validatespare.SUBC_NAME.$invalid}">
                                                <label class="control-label">Equipment</label>
                                                <div isteven-multi-select data-input-model="Type" data-output-model="Validatespare.Type" data-button-label="icon SUBC_NAME" selection-mode="single"
                                                    data-item-label="icon SUBC_NAME" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="Validatespare.Type[0]" name="SUBC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="Validatespare.$submitted && Validatespare.SUBC_NAME.$invalid" style="color: red">Please select Equipment</span>
                                            </div>
                                        </div>
                                        <div class="col-md-1 col-sm-6 col-xs-12">
                                            <br />
                                            <div class="box-footer">
                                                <input type="Button" value="Search" class="btn btn-primary custom-button-color" data-ng-click="BackClicked()" style="margin-top: 6px" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group" style="width: 100%">
                                            
                                            <div data-ng-show="MainGrid">
                                               <div class="bxheader">
                                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                        </div>

                                               <%-- <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px;"></div>--%>
                                                 <div ag-grid="gridOptions" class="ag-blue" style="height: 310px;"></div>

                                                <%--class="ag-blue"--%>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </form>
                            <form>

                                <div data-ng-show="DetailsGrid">
                                    <div class="bxheader">
                                            <input type="text" class="form-control" id="filtertxt2" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                        </div>
                                    <div data-ag-grid="gridOptions1" class="ag-blue" style="height: 310px;"></div>

                                    <div class="col-md-3 col-sm-3 col-xs-3 pull-left">
                                        <label class="control-label">Incharge Remarks </label>
                                        <textarea rows="1" cols="15" name="SPARE_REM" data-ng-model="Validatespare.SPARE_REM" class="form-control" placeholder="Requestor remarks"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right" data-ng-show="divBtn">
                                    <div class="form-group">
                                        <%--<input type="Button" value="Back" class='btn btn-primary custom-button-color' data-ng-click="BackClicked()" style="height: 25px;">--%>
                                        <input type="Button" value="Approve" class='btn btn-primary custom-button-color' data-ng-click="UpdateStatus('Approve')" style="margin-top: 5px">
                                        <input type="Button" value="Reject" data-ng-click="UpdateStatus('Reject')" class="btn btn-primary custom-button-color" style="background-color: black; border: black; height: 32px; width: 71px; margin-top: 5px; margin-left: 6px">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                     <div style="margin-top:15px;padding-left:20px;">

                      <a class="btn btn-danger homeButton" href="#" onclick="fnDetail('../../MaintenanceManagement/Views/MaintainanceDashboard.aspx')" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-home" aria-hidden="true"></i>HOME</a>
                              <a class="btn btn-danger homeButton" href="#" onclick="goBack();" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>
               </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />

    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>

    <script defer>      
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script> 

    <script src="Validatespare.js"></script>
    
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            ////$('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            ////$('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
            //$('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            //$('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        });

        function fnDetail(url) {
            //progress(0, 'Loading...', true);
            window.location = url;
        }
        function goBack() {
            //window.history.back();
            var strBackUrl = sessionStorage.getItem("strBackUrl");           
            window.location = strBackUrl;
        }

    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
</body>
</html>

