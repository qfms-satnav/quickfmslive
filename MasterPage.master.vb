Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.Web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Imports System.Web.Services
Imports System.Configuration
Imports System.Web.Configuration

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Session("LoginUser") = ""
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                Dim UID As String = Session("uid")
            End If

            lblLogTime.Text = Session("Lastlogintime")

            Dim aurid As String = Session("uid")

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 200)
            param(0).Value = aurid
            Dim ds As DataSet = objsubsonic.GetSubSonicDataSet("USP_GET_DETAILS", param)
            lbluser.Text = ds.Tables(0).Rows(0).Item("Name")
            If ds.Tables(0).Rows(0).Item("EMP_IMG") Is "" Then
                img.ImageUrl = "~/Userprofiles/default-user-icon-profile.jpg"
            Else
                img.ImageUrl = "~/userprofiles/" + ds.Tables(0).Rows(0).Item("EMP_IMG")
            End If
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getRoleNoforUser")
            sp2.Command.AddParameter("@usr_id", Session("uid"), DbType.String)
            Dim ds2 As DataSet = sp2.GetDataSet()
            Dim rolename As Integer = ds2.Tables(0).Rows(0).Item("Name")
            BindLogo()
            If rolename = 1 Then
                logoimg.ToolTip = "Click here to change logo"
                rolehf.Value = rolename
            End If
            Session("timeout") = DateTime.Now.AddMinutes(Convert.ToInt32(AppSettings("timeout"))).ToString()
            Session("LoginUser") = lbluser.Text
        End If
    End Sub

    Private Sub BindLogo()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        'sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            logoimg.ImageUrl = ds3.Tables(0).Rows(0).Item("ImagePath")
        Else
            logoimg.ImageUrl = "~/BootStrapCSS/images/yourlogo.png"
        End If
    End Sub

    'Protected Sub lbtnLogOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLogOut.Click
    '    Dim strQuery As String = ""
    '    Dim td As TokenData = TokenDataManager.GetTokenObject()
    '    If td.IsAuthenticated Then
    '        Response.Redirect("~/logout")
    '    Else
    '        strQuery = "Update " & Session("TENANT") & "." & "[user] set Usr_logged='N' WHERE USR_ID='" & Session("uid") & "'"
    '        SqlHelper.ExecuteScalar(CommandType.Text, strQuery)

    '        Dim Mode As String = "2"
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_LOGOUT_TIME")
    '        sp.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
    '        sp.Command.AddParameter("@Mode", Mode, DbType.String)
    '        sp.Command.AddParameter("@LoginUniqueID", Session("LoginUniqueID"), DbType.String)
    '        sp.ExecuteScalar()

    '        Session.Abandon()
    '        Session("UID") = ""
    '        If Session("TENANT") = "Flipkart.dbo" Then
    '            Response.Redirect("~/Login_Popup.aspx")
    '        Else
    '            Response.Redirect("~/login.aspx")
    '        End If
    '    End If
    'End Sub

    Protected Sub lbtnLogOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLogOut.Click
        Dim strQuery As String = ""
        Dim td As TokenData = TokenDataManager.GetTokenObject()
        If td.IsAuthenticated Then
            If Session("TENANT") = "Flipkart.dbo" Or Session("TENANT") = "AXA_UAT.dbo" Or Session("TENANT") = "AGS_Health.dbo" Or Session("TENANT").ToString().ToLower() = "JOHNDEERE.dbo".ToString().ToLower() Or Session("TENANT") = "Apexon.dbo".ToString().ToLower() Or Session("TENANT").ToString().ToLower() = "LTTS.dbo".ToString().ToLower() Or Session("TENANT").ToString().ToLower() = "[TATA_CAP].dbo".ToString().ToLower() Or Session("TENANT").ToString().ToLower() = "[Emids].dbo".ToString().ToLower() Then
                Response.Redirect("~/Login_Popup.aspx")
            Else
                Response.Redirect("~/logout.aspx")
            End If

            '' Response.Redirect("~/logout.aspx")
        Else
            strQuery = "Update " & Session("TENANT") & "." & "[user] set Usr_logged='N' WHERE USR_ID='" & Session("uid") & "'"
            SqlHelper.ExecuteScalar(CommandType.Text, strQuery)

            Dim Mode As String = "2"
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_LOGOUT_TIME")
            sp.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@Mode", Mode, DbType.String)
            sp.Command.AddParameter("@LoginUniqueID", Session("LoginUniqueID"), DbType.String)
            sp.ExecuteScalar()

            Session.Abandon()
            Session("UID") = ""
            If Session("TENANT") = "Flipkart.dbo" Or Session("TENANT") = "AXA_UAT.dbo" Or Session("TENANT") = "AGS_Health.dbo" Or Session("TENANT").ToString().ToLower() = "JOHNDEERE.dbo".ToString().ToLower() Or Session("TENANT") = "Apexon.dbo".ToString().ToLower() Or Session("TENANT").ToString().ToLower() = "LTTS.dbo".ToString().ToLower() Or Session("TENANT").ToString().ToLower() = "[TATA_CAP].dbo".ToString().ToLower() Or Session("TENANT").ToString().ToLower() = "[Emids].dbo".ToString().ToLower() Then
                Response.Redirect("~/Login_Popup.aspx")
            Else
                Response.Redirect("~/login.aspx")
            End If
        End If
    End Sub

End Class