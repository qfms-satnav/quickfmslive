﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .fc-time {
            color: white !important;
        }

        .fc-title {
            color: white !important;
        }

        .timepicker {
            z-index: 1151 !important;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .calmodaldiv {
            /*width: 800px;*/
            height: 400px;
            overflow: auto;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
        input[type=radio]{
            position:relative !important;
        }
        .widthcl{
            width: 6em;
        }
        .widthEN{
            width: fit-content;
        }
    </style>
</head>
<body data-ng-controller="ViewAndModifyController" onload="setDateVals()" class="amantra">

    <div class="al-content">
        <div class="widgets">
            <h3 class="panel-title">View & Modify Booking </h3>
        </div>
        <div class="card">
            <div data-ng-show="Viewstatus==0">
                <form role="form" id="CheckInOut" name="CheckInOut" novalidate>
                    <div class="row" style="padding-left: 15px">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Quick Select <span style="color: red;">**</span></label>
                                <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                    <option value="TODAY">Today</option>
                                    <option value="YESTERDAY">Yesterday</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 30 Days</option>
                                    <option value="THISMONTH">This Month</option>
                                    <option value="NEXTMONTH">Next Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">

                                <label>From Date</label>

                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">

                                <label>To Date</label>

                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 20px">
                            <input type="submit" ng-click="getData()" value="Submit" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div data-ng-show="Viewstatus==1">
                <form id="frmAdminBooking" name="frmAdminBooking" data-valid-submit="LoadDataV(2,'a')" novalidate>

                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label><b>Requisition ID :</b> </label>
                                {{ViewAndModify.BM_REQ_ID}}
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.CNY_NAME.$invalid}">
                                <label>Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="ipCountry" data-output-model="ViewAndModify.OPCountry" data-button-label="icon CNY_NAME"
                                    data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                    data-tick-property="ticked" data-max-labels="2" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="ViewAndModify.OPCountry" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.CTY_NAME.$invalid}">

                                <label>City<span style="color: red;">**</span></label>

                                <div isteven-multi-select data-input-model="ipCity" data-output-model="ViewAndModify.OPCity" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="ViewAndModify.OPCity" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.CTY_NAME.$invalid" style="color: red">Please Select City </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.LCM_NAME.$invalid}">

                                <label>Location <span style="color: red;">**</span></label>

                                <div isteven-multi-select data-input-model="ipLocation" data-output-model="ViewAndModify.OPLocation" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="ViewAndModify.OPLocation" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.RT_NAME.$invalid}">
                                <div class="row">
                                    <label>Facility Type<span style="color: red;">*</span></label>

                                    <div isteven-multi-select data-input-model="RTlst" data-output-model="Customized.RTlst" button-label="icon RT_NAME" item-label="RT_NAME maker" tick-property="ticked"
                                        data-on-item-click="getFacilityNamesbyType()" data-on-select-all="FTSelectAll()" data-on-select-none="FTSelectNone()"
                                        data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="Customized.RTlst[0]" name="RT_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.RT_NAME.$invalid" style="color: red">Please Select Facility Type </span>

                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="clearfix">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.RF_NAME.$invalid}">

                                <label>Facility Name<span style="color: red;">**</span></label>

                                <div isteven-multi-select data-input-model="RFlst" data-output-model="Customized.RFlst" button-label="icon RF_NAME" item-label="RF_NAME maker" tick-property="ticked"
                                    data-on-item-click="FNChange()" data-on-select-all="FNSelectAll()" data-on-select-none="FNSelectNone()"
                                    data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="Customized.RFlst[0]" name="RF_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.RF_NAME.$invalid" style="color: red">Please Select Facility Name </span>


                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                        </div>

                    </div>

                    <div class="clearfix">
                        <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                            <button type="submit" id="btnchkin" class="btn btn-primary custom-button-color">Search</button>
                            <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                        </div>
                    </div>

                </form>
                <br />

                <div data-ng-show="GridVisiblityV">
                    <div class="row">
                        <div class="col-md-12">
                            <%--<input type="text" class="selectpicker" id="Text1" placeholder="Filter by any..." style="width: 25%" />--%>
                            <div data-ag-grid="gridOptionsV" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="button" value="View Calendar" ng-click="viewCalendar(selectedRows)" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="viewCalendar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                        <div class="align-items-center justify-content-between">
                            <h5 class="modal-title" id="H1">{{SaveBookingObj.bookinghead}}</h5>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body calmodaldiv">

                    <div class="clearfix">

                        <div id="bookingdetails">
                            <form id="frmSubmitBooking" name="frmSubmitBooking" novalidate>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">

                                        <div class="radio">
                                            <label>
                                                <input ng-model="SaveBooking.BM_TYPE" type="radio" name="rdoType" value="1">
                                                Personal
                                            </label>
                                            <label>
                                                <input ng-model="SaveBooking.BM_TYPE" type="radio" name="rdoType" value="2">
                                                Official
                                            </label>
                                        </div>

                                    </div>
                                </div>



                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                    </div>
                                </div>



                                <div class="clearfix">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <label><b>Location</b></label>
                                        <br />
                                        {{SaveBooking.LocationName}}
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label><b>Facility Name</b></label>
                                        <br />
                                        {{SaveBooking.FacilityName}}
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <label><b>Room Names</b></label>
                                        <br />
                                        {{SaveBooking.RoomName}}
                                    </div>
                                </div>
                                <br />
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.TITLE.$invalid}">
                                            <label>Booking Title <span style="color: red;">*</span></label>

                                            <input type="text" class="form-control form-control-small" data-ng-model="SaveBooking.BM_TITLE" name="TITLE" required="" />
                                            <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.TITLE.$invalid" style="color: red">Please enter booking title</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <div class="input-group date" id='fromdatebooking'>
                                                <input type="text" class="form-control" data-ng-model="SaveBooking.BM_FROM_DATE" id="Text3" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdatebooking')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                        </div>
                                    </div>



                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>To Date</label>
                                            <div class="input-group date" id='todatebooking'>
                                                <input type="text" class="form-control" data-ng-model="SaveBooking.BM_TO_DATE" id="Text4" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todatebooking')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.FromTime.$invalid}">
                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label>From Time</label>
                                                    <div class="input-group">
                                                        <input type="text" id="Text5" name="FromTime" class="form-control timepicker" data-ng-model="SaveBooking.BM_FROM_TIME"
                                                            required="" />
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix">

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.ToTime.$invalid}">
                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label>To Time</label>
                                                    <div class="input-group">
                                                        <input type="text" id="Text6" name="ToTime" class="form-control timepicker" data-ng-model="SaveBooking.BM_TO_TIME"
                                                            required="" />
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <label>Reference ID</label>
                                        <input type="text" ng-model="SaveBooking.BM_REFRERENCE_ID" id="refID"><br />
                                    </div>

                                </div>
                                <br />
                                <div class="clearfix">

                                    <div class="col-md-12 col-sm-12 col-xs-12">


                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Room Name</th>
                                                    <th>Employee Name</th>
                                                    <th>Type</th>
                                                    <th>Email</th>
                                                    <th>Remarks</th>
                                                    <th>Mobile No.</th>
                                                    <th>Edit/Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="s in selectedRows | filter : {BD_STA_ID : '!8'} " ng-include="getTemplate(s)">
                                                </tr>
                                            </tbody>
                                        </table>

                                        <script type="text/ng-template" id="display">
                                                                            <td>{{s.RR_NAME}}</td>
                                                                            <td>{{s.RB_EMP_NAME}}</td>
                                                                            <td>{{ getEmpType(s.RB_EMP_TYPE)}}</td>
                                                                            <td>{{s.RB_EMP_EMAIL }}</td>
                                                                            <td>{{s.RB_REMARKS}}</td>
                                                                         <td>{{s.RB_MOBILE_NUMBER}}</td>
                                                                            <td>
                                                                                <button type="button" ng-click="editContact(s)" class="btn-xs btn-primary"><i class="fa fa-pencil fa-2" aria-hidden="true"></i></button>
                                                                                <button type="button" ng-click="delete(s)" class="btn-xs btn-danger"><i class="fa fa-trash fa-2" aria-hidden="true"></i></button>
                                                                            </td>
                                        </script>
                                        <script type="text/ng-template" id="edit">
                                                                            <td>  <label>{{model.selected.RR_NAME}}</label></td>
                                                                            <td>
                                                                                <div angucomplete-alt
                                                                                    id="ex7"
                                                                                    placeholder="Search Employee"
                                                                                    pause="500"
                                                                                    selected-object="residentSelected"
                                                                                    remote-url="../../../api/Utility/GetEmployeeNameAndID"
                                                                                    remote-url-request-formatter="remoteUrlRequestFn"
                                                                                    remote-url-data-field="items"
                                                                                    title-field="NAME"
                                                                                    minlength="2"
                                                                                    input-class="form-control form-control-small widthEN"
                                                                                    ng-model="model.selected.RB_EMP_NAME"

                                                                                    match-class="highlight" >
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <select ng-model="model.selected.RB_EMP_TYPE" >
                                                                                    <option selected="selected" class="widthcl">--Select--</option>
                                                                                    <option  ng-repeat="type in EmpTypes"   value="{{type.ET_SNO}}" class="widthcl">{{type.ET_NAME}}</option>
                                                                                </select>
                                                                            </td>
                                                                            <td><input type="text" ng-model="model.selected.RB_EMP_EMAIL" /></td>
                                                                            <td><input type="text" ng-model="model.selected.RB_REMARKS" class="widthcl"/></td>
                                                                          <td> <input ng-model="model.selected.RB_MOBILE_NUMBER"  type="text" class="widthcl"/> </td>
                                                                            
                                                                            <td>
                                                                                <button ng-click="saveContact($index)" class="btn-xs btn-info"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                                                                <button ng-click="reset()" class="btn-xs btn-warning"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                                                            </td>
                                        </script>
                                    </div>
                                </div>

                                <br />
                                <div class="clearfix">
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Modify Booking" ng-click="frmSubmitBooking.$valid && UpdateBookedRequest(4)" class="btn btn-primary custom-button-color" />
                                        <input type="submit" value="Cancel Booking" ng-click="frmSubmitBooking.$valid && UpdateBookedRequest(3)" ng-disabled="disableCancelbutton==true" class="btn btn-primary custom-button-color" />
                                        <input type="button" ng-click="clearData('frmSubmitBooking')" value="Close" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>
                    <br />
                    <div class="clearfix">
                        <div id='calendar' style="width: 100%"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script src="../../../Scripts/bootstrap-timepicker.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt"]);

        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY')));

            $('#Text1').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));

            $(".timepicker").timepicker({
                showInputs: false,
                showMeridian: false,
                // template: 'modal'
            });
        }



    </script>

    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../Js/AdminBooking.js" defer></script>
    <script src="../Js/ViewAndModify.js" defer></script>
    <%--<script src="../../../BootStrapCSS/Scripts/jquery-ui.min.js"></script>--%>
    <script src="../../../BootStrapCSS/qtip/qtip.js" defer></script>
    <script type="text/javascript">
        var sessionValue = '<%= Session["TENANT"] %>';
    </script>
</body>
</html>

