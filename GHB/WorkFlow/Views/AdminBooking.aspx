﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminBooking.aspx.cs" Inherits="GHB_WorkFlow_Views_AdminBooking" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.css" rel="stylesheet" />

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        /*.timepicker {
            z-index: 1151 !important;
        }*/


        .fc-time {
            color: white !important;
        }

        .fc-title {
            color: white !important;
        }

        .grid-align {
            text-align: center;
        }

        .calmodaldiv {
            width: 800px;
            height: 550px;
            overflow: auto;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        #calendar {
            display: block;
        }
    </style>
</head>
<body data-ng-controller="AdminBookingController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3 class="panel-title">Book a Facility</h3>
        </div>
        <div class="card">
            <form id="frmAdminBooking" name="frmAdminBooking" data-valid-submit="LoadData(0,'a')" novalidate>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.CNY_NAME.$invalid}">
                            <label>Country <span style="color: red;">*</span></label>
                            <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                data-tick-property="ticked" data-max-labels="2" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.CNY_NAME.$invalid" style="color: red">Please Select Country</span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.CTY_NAME.$invalid}">

                            <label>City<span style="color: red;">**</span></label>

                            <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.CTY_NAME.$invalid" style="color: red">Please Select City</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.LCM_NAME.$invalid}">

                            <label>Location <span style="color: red;">**</span></label>

                            <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.LCM_NAME.$invalid" style="color: red">Please Select Location</span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.RT_NAME.$invalid}">

                            <label>Facility Type<span style="color: red;">*</span></label>

                            <div isteven-multi-select data-input-model="RTlst" data-output-model="Customized.RTlst" button-label="icon RT_NAME" item-label="RT_NAME maker" tick-property="ticked"
                                data-on-item-click="getFacilityNamesbyType()" data-on-select-all="FTSelectAll()" data-on-select-none="FTSelectNone()" data-max-labels="1" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="Customized.RTlst[0]" name="RT_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.RT_NAME.$invalid" style="color: red">Please select facility type </span>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmAdminBooking.$submitted && frmAdminBooking.RF_NAME.$invalid}">
                            <label>Facility Name<span style="color: red;">**</span></label>
                            <div isteven-multi-select data-input-model="RFlst" data-output-model="Customized.RFlst" button-label="icon RF_NAME" item-label="RF_NAME maker" tick-property="ticked"
                                data-on-item-click="FNChange()" data-on-select-all="FNSelectAll()" data-on-select-none="FNSelectNone()" data-max-labels="1" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="Customized.RFlst[0]" name="RF_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="frmAdminBooking.$submitted && frmAdminBooking.RF_NAME.$invalid" style="color: red">Please select facility name </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Quick Select <span style="color: red;">**</span></label>
                            <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                <option value="TODAY">Today</option>
                                <%-- <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>--%>
                                <option value="THISMONTH">This Month</option>
                                <option value="NEXTMONTH">Next Month</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">

                            <label>From Date</label>

                            <div class="input-group date" id='fromdate'>
                                <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>To Date</label>
                            <div class="input-group date" id='todate'>
                                <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-6 col-xs-12">

                        <div class="form-group">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>From Time</label>
                                    <div class="input-group">
                                        <input type="text" id="Text7" name="FromTimeFilter" class="form-control timepicker" data-ng-model="Customized.FromTimeFilter"
                                            required="" />
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>To Time</label>
                                    <div class="input-group">
                                        <input type="text" id="Text8" name="ToTimeFilter" class="form-control timepicker" data-ng-model="Customized.ToTimeFilter"
                                            required="" />
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">

                        <div class="radio">
                            <input ng-model="SaveBooking.BM_TYPE" type="radio" name="rdoType" ng-value="1">
                            <label>
                                Personal
                            </label>
                            <br />
                            <input ng-model="SaveBooking.BM_TYPE" type="radio" name="rdoType" ng-value="2">
                            <label>
                                Official
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                        <input type="button" id="btnNew" ng-click="Reset()" value="Reset" class="btn btn-primary custom-button-color" />
                    </div>
                </div>
            </form>
            <br />
            <form id="form2">
                <div data-ng-show="GridVisiblity">

                    <div class="row">
                        <div class="col-md-12">
                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div></div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="button" value="View Calendar" ng-click="viewCalendar(selectedRows)" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="viewCalendar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content calmodaldiv">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="H1">{{SaveBookingObj.bookinghead}}</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="clearfix">
                            <div id="bookingdetails">

                                <form id="frmSubmitBooking" name="frmSubmitBooking" data-valid-submit="SaveBookingRequest()" novalidate>
                                    <div class="clearfix" style="padding-left: 15px;">
                                        <b>Location</b>: {{SaveBooking.LocationName}} &nbsp;  <b>Facility Name</b>
                                        :
                                                                        {{SaveBooking.FacilityName}}
                                    </div>


                                    <br />
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.TITLE.$invalid}">
                                                <label>Booking Title <span style="color: red;">*</span></label>

                                                <input type="text" class="form-control form-control-small" data-ng-model="SaveBooking.BM_TITLE" name="TITLE" required="" />
                                                <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.TITLE.$invalid" style="color: red">Please enter booking title.</span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <div class="input-group date" id='fromdatebooking'>
                                                    <input type="text" class="form-control" data-ng-model="SaveBooking.BM_FROM_DATE" id="Text3" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('fromdatebooking')"></i>
                                                    </span>
                                                </div>
                                                <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.FromDate.$invalid" style="color: red;">Please select from date</span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <div class="input-group date" id='todatebooking'>
                                                    <input type="text" class="form-control" data-ng-model="SaveBooking.BM_TO_DATE" id="Text4" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('todatebooking')"></i>
                                                    </span>
                                                </div>
                                                <span class="error" data-ng-show="frmSubmitBooking.$submitted && frmSubmitBooking.ToDate.$invalid" style="color: red;">Please select to date</span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.FromTime.$invalid}">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>From Time</label>
                                                        <div class="input-group">
                                                            <input type="text" id="Text5" name="FromTime" class="form-control timepicker" data-ng-model="SaveBooking.BM_FROM_TIME"
                                                                required="" />
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="clearfix">

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && frmSubmitBooking.ToTime.$invalid}">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>To Time</label>
                                                        <div class="input-group">
                                                            <input type="text" id="Text6" name="ToTime" class="form-control timepicker" data-ng-model="SaveBooking.BM_TO_TIME"
                                                                required="" />
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <label>Reference ID</label>
                                            <input type="text" class="form-control form-control-small" ng-model="SaveBooking.BM_REFRERENCE_ID" id="refID"><br />
                                        </div>


                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <table class="table table-bordered" id="tblRoomlist">
                                                <thead>
                                                    <th>Room Name</th>
                                                    <th>Emp Name</th>
                                                    <th>Type</th>
                                                    <th>Email</th>
                                                    <th>Remarks</th>
                                                    <th>Mobile No.</th>
                                                </thead>
                                                <tr ng-repeat="x in selectedRows" data-ng-form="innerForm">
                                                    <td>

                                                        <div class="form-group">
                                                            <label>{{x.RR_NAME}}</label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div angucomplete-alt
                                                                id="ex{{$index}}"
                                                                placeholder="Search Employee"
                                                                pause="500"
                                                                selected-object="residentSelected"
                                                                remote-url="../../../api/Utility/GetEmployeeNameAndID"
                                                                remote-url-request-formatter="remoteUrlRequestFn"
                                                                remote-url-data-field="items"
                                                                title-field="NAME"
                                                                minlength="2"
                                                                input-class="form-control form-control-small"
                                                                ng-model="selectedRows[$index].BD_EMP_NAME"
                                                                match-class="highlight">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">

                                                            <select ng-model="selectedRows[$index].BD_EMP_TYPE">

                                                                <option ng-repeat="type in EmpTypes" value="{{type.ET_SNO}}">{{type.ET_NAME}}</option>
                                                            </select>

                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group" data-ng-class="{'has-error': frmSubmitBooking.$submitted && innerForm.bdempemail.$error.email}">
                                                            <input type="email" name="bdempemail" class="form-control" ng-model="selectedRows[$index].BD_EMP_EMAIL" />
                                                        </div>

                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <textarea rows="3" class="form-control" ng-model="selectedRows[$index].BD_REMARKS"></textarea>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <input class="form-control" ng-model="selectedRows[$index].BD_MOBILE_NUMBER" type="text" />
                                                        </div>
                                                    </td>

                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                    <br />
                                    <div class="clearfix">
                                        <div class="box-footer text-right">
                                            <input type="submit" value="Book" class="btn btn-primary custom-button-color" ng-disabled="disableSavebutton==true" />
                                            <input type="button" ng-click="clearData('frmSubmitBooking')" value="Close" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div class="col-md-12">
                            <div>
                                <b>Legend: </b><span class="label btn-sm" style="background-color: #008000; color: white;">Booked</span>
                                &nbsp; &nbsp; 
                                                            <span class="label btn-sm" style="background-color: #FF0000; color: white;">Withhold</span>
                                &nbsp; &nbsp; <span class="label btn-sm" style="background-color: white; color: black; border: groove">Vacant</span>
                            </div>
                            <br />
                            <div id='calendar' style="width: 100%"></div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>


    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script src="../../../Scripts/bootstrap-timepicker.js" defer></script>

    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <%-- <script src="../../Scripts/moment.min.js"></script>--%>

    <script src="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt"]);
    </script>
    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../../WorkFlow/Js/AdminBooking.js" defer></script>



    <script src="../../../BootStrapCSS/qtip/qtip.js" defer></script>
    <script type="text/javascript" defer>

        function setDateVals() {

            $('#Text1').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text2').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $(".timepicker").timepicker({
                showInputs: false,
                showMeridian: false
            });

            $('#Text1').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().add(1, 'days').format('MM/DD/YYYY')));


        }
    </script>

    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();

        });
    </script>
    <script type="text/javascript">
        var sessionValue = '<%= Session["TENANT"] %>';
    </script>
</body>
</html>
