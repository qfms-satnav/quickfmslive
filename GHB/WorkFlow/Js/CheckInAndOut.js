﻿
app.service("CheckInAndoutService", function ($http, $q, UtilityService) {

    this.path = window.location.origin;
    this.GetGriddata = function (Dataobj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckInAndOut/GetChkInOutDetails', Dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.GetDetails = function (reqid) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckInAndOut/GetDetailsByReqId?reqId=' + reqid + '')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.SubmitCheckinoutData = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckInAndOut/SubmitCheckInOut', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('CheckInAndoutController', function ($scope, $q, $http, CheckInAndoutService, UtilityService, $timeout, $filter) {
    $scope.ChkInOut = {};
    $scope.Viewstatus = 0;
    $scope.GridVisiblity = true;

    $scope.selectedRows = [];
    $scope.EmpTypes = [];

    $scope.Customized = {};

    $scope.columnDefs = [
      { headerName: "Requisition Id", field: "RB_REQ_ID", width: 165, cellClass: 'grid-align', filter: 'text', template: '<a ng-click="onRowSelectedFunc(data)">{{data.RB_REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
      { headerName: "Reference Id", field: "RB_REFERENCE_ID", cellClass: 'grid-align', width: 180 },
      { headerName: "Facility Type", field: "RT_NAME", width: 150, cellClass: 'grid-align', },
      { headerName: "Facility Name", field: "RF_NAME", width: 150, cellClass: 'grid-align', },
      { headerName: "Room Number/Name", field: "RR_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 100 },
      { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "From Date", field: "RB_FROM_DATE", template: '<span>{{data.RB_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "To Date", field: "RB_TO_DATE", template: '<span>{{data.RB_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
      { headerName: "From Time", field: "RB_FRM_TIME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "To Time", field: "RB_TO_TIME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
      { headerName: "Reserved By", field: "RESERVED_BY", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved By Email", field: "RESERVED_BY_EMAIL", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved For", field: "RESERVED_FOR", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved For Email", field: "RESERVED_FOR_EMAIL", cellClass: 'grid-align', width: 150, },
      { headerName: "Reserved Date", field: "RESERVED_DT", template: '<span>{{data.RESERVED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, },
       //{ headerName: "Action", field: "CHKSTATUS", width: 165, cellClass: 'grid-align', filter: 'text', template: '<a ng-click="onRowSelUpdateStatus(data)">{{data.CHKSTATUS}}</a>', pinned: 'right', suppressMenu: true },
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.setRowData()
        },
    };

    $scope.back = function () {
        $scope.Viewstatus = 0;
    }
    $scope.LoadData = function () {
        var Dataobj = {
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,

        };
        CheckInAndoutService.GetGriddata(Dataobj).then(function (data) {

            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 200);
            }
            else {
                $scope.gridOptions.api.setRowData($scope.gridata);


                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
        });
    }



    setTimeout(function () { progress(0, 'Loading...', true); }, 200);

    $scope.onRowSelUpdateStatus = function (data) {
        console.log(data);



        $scope.SaveCheckInOut = {};
        $scope.ViewReqData = {};
        $scope.ViewReqData = data;
        $scope.ViewReqData.RB_TO_DATE = data.RB_TO_DATE;
        $scope.ViewReqData.RB_MOBILE_NUMBER = data.RB_MOBILE_NUMBER;
        $scope.SaveCheckInOut.RB_REQ_ID = data.RB_REQ_ID;




        if (data.CHKSTATUS == "CHECK OUT") {
            $scope.SaveCheckInOut.checkinout = data.RB_TO_DATE;
            $scope.SaveCheckInOut.Time = data.RB_TO_TIME;
            $scope.SubmitCheckinoutData('CHECKOUT');
            setTimeout(function () {
                $scope.LoadData();
            }, 1000);
        }
        else if (data.CHKSTATUS == "CHECK IN") {
            $scope.SaveCheckInOut.checkinout = data.RB_FROM_DATE;
            $scope.SaveCheckInOut.Time = data.RB_FRM_TIME;
            $scope.SubmitCheckinoutData('CHECKIN');
            setTimeout(function () {
                $scope.LoadData();
            }, 1000);
        }
        else {
            console.log("Completed");
        }
    };

    $scope.enableCheckOutDate = 0;

    $scope.onRowSelectedFunc = function (data) {
        $scope.enableInButton = 1;
        $scope.enableOutButton = 1;
        $scope.model.selected = {};
        CheckInAndoutService.GetDetails(data.RB_SNO).then(function (data) {
            console.log(data);

            if (data.data[0].RB_CHK_IN_OUT_STA_ID == 1) {
                $scope.enableCheckOutDate = 1;
            }
            else {
                $scope.enableCheckOutDate = 0;
            }

            $scope.selectedRows = data.data;
            $scope.ViewReqData.RB_CHK_OUT_DATE = $filter('date')(data.data[0].RB_TO_DATE, "MM/dd/yyyy");
            //$scope.ChkIn_Details = data.Table[0];
            //$scope.ChkOut_Details = data.Table[1];
            $scope.ViewReqData.RB_MOBILE_NUMBER = data.data[0].RB_MOBILE_NUMBER;
            $scope.CHKINOUTTIME = [];
            populate();

        });

        //if (data.CHKSTATUS == "Completed") {
        //    $scope.enableInButton = 0;
        //    $scope.enableOutButton = 0;
        //}
        //else if (data.CHKSTATUS == "CHECK IN") {
        //    $scope.enableOutButton = 0;

        //}
        //else {
        //    $scope.enableInButton = 0;
        //}
        $scope.SaveCheckInOut = {};
        $scope.Viewstatus = 1;

        $scope.ViewReqData = {};
        $scope.ViewReqData = data;
        $scope.SaveCheckInOut.RB_REQ_ID = data.RB_REQ_ID;
        //var d = new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY'));
        //console.log(d);
        //$scope.SaveCheckInOut.checkinout = d;

    };

    $scope.CHKINOUTTIME = [];

    function populate() {
        // var select = $(selector);
        var hours, minutes, ampm;
        for (var i = 60; i <= 1440; i += 15) {
            hours = Math.floor(i / 60);
            minutes = i % 60;
            if (minutes < 10) {
                minutes = '0' + minutes; // adding leading zero
            }

            var obj = {};
            obj.TIME = hours + ':' + minutes;
            console.log(1)
            console.log(obj);
            $scope.CHKINOUTTIME.push(obj);


            //select.append($('<option></option>')
            //    .attr('value', i)
            //    .text(hours + ':' + minutes));
        }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    // new code

    $scope.model = {};

    $scope.model = {
        roomdetails: [],
        selected: {}
    };

    // gets the template to ng-include for a table row / item
    $scope.getTemplate = function (s) {
        if (s.RR_NAME === $scope.model.selected.RR_NAME) return 'edit';
        else return 'display';
    };

    UtilityService.GetEmployeeTypes().then(function (response) {

        if (response.items != null) {
            $scope.EmpTypes = response.items;

        }
    });

    $scope.getEmpType = function (emptype) {
        var obj = _.find($scope.EmpTypes, { ET_SNO: emptype });
        if (obj != null)
            return obj.ET_NAME;
        else
            return "";
    }

    $scope.editContact = function (s) {
        $scope.model.selected = angular.copy(s);
        $scope.model.selected.RB_CHK_OUT_DATE = $scope.ViewReqData.RB_CHK_OUT_DATE;
        console.log($scope.model.selected);
        // setDateVals();
    };

    $scope.saveContact = function (idx, type) {
        console.log($scope.model.selected);
        $scope.model.selected.RB_CHK_OUT_DATE = $scope.ViewReqData.RB_CHK_OUT_DATE;
        if ($scope.model.selected.RB_EMP_NAME == "") {
            showNotification('error', 8, 'bottom-right', "Enter Employee Name");
        }
        else {

            if ($scope.model.selected.RB_CHK_IN_TIME == undefined || $scope.model.selected.RB_CHK_IN_TIME == "NA") {
                showNotification('error', 8, 'bottom-right', "Enter Check-In Time");
            }

                //else if ($scope.model.selected.RB_CHK_OUT_DATE) {
                //}
            else if (moment($scope.model.selected.RB_CHK_OUT_DATE) > moment($scope.model.selected.RB_TO_DATE)) {
                //progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "'Check Out Date' should be less than or equal to 'To Date'");
            }
            else {
                $scope.selectedRows[idx] = angular.copy($scope.model.selected);
                $scope.selectedRows[idx].BD_STA_ID = UtilityService.Modified;
                $scope.selectedRows[idx].Type = type;
                console.log($scope.selectedRows[idx]);
                $scope.reset();

                $scope.SubmitCheckinoutData($scope.selectedRows[idx]);
            }
        }

    };

    $scope.residentSelected = function (selected) {
        console.log(selected);
        if (selected.originalObject.NAME) {
            $scope.selectedRows[this.$parent.$index].RB_RESERVED_FOR = selected.originalObject.AUR_ID;
            var indexval = this.$parent.$index
            UtilityService.GetEmpEmailByID(selected.originalObject.AUR_ID).then(function (response) {
                $scope.selectedRows[indexval].RB_EMP_EMAIL = response.data;
                $scope.model.selected.RB_EMP_EMAIL = response.data;
                $scope.model.selected.RB_RESERVED_FOR = selected.originalObject.AUR_ID;
            });

        } else {
            console.log('overriding selection');
        }
    }

    $scope.reset = function () {
        $scope.model.selected = {};
    };

    $scope.delete = function (obj) {
        var r = confirm("Are you sure you want to delete this Booking of Room " + " " + obj.RR_NAME + " ?");

        if (r == true) {
            obj.BD_STA_ID = UtilityService.Deleted;

        } else {

        }
    };
    $scope.remoteUrlRequestFn = function (str) {
        return { q: str };
    };

    //$scope.SubmitCheckinoutData = function (type) {
    //    $scope.SaveCheckInOut.Type = type;
    //    var params = { reqlcm: $scope.SaveCheckInOut }
    //    console.log(params);
    //    CheckInAndoutService.SubmitCheckinoutData($scope.SaveCheckInOut).then(function (data) {
    //        console.log(data);

    //        $scope.Viewstatus = 0;

    //        setTimeout(function () {
    //            $scope.LoadData();
    //        }, 1000);

    //        setTimeout(function () { showNotification('success', 8, 'bottom-right', data.Message); }, 1000);

    //    });
    //};
    $scope.SubmitCheckinoutData = function (Obj) {

        console.log(Obj);

        CheckInAndoutService.SubmitCheckinoutData(Obj).then(function (data) {
            console.log(data);

            $scope.Viewstatus = 0;

            setTimeout(function () {
                $scope.LoadData();
            }, 1000);

            setTimeout(function () { showNotification('success', 8, 'bottom-right', "Successfully Updated."); }, 1000);

        });
    };
    // quick select
    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().add(1, 'days').format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'NEXTMONTH':
                $scope.Customized.FromDate = moment().add(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().add(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }

    $scope.getData = function () {
        var fromdate = moment($scope.Customized.FromDate);
        var todate = moment($scope.Customized.ToDate);
        if (fromdate > todate) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            $scope.LoadData();
        }
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);


});