﻿app.service("ReservationTypeService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();
    this.getReservationType = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ReservationType')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //SAVE
    this.saveReservationType = function (mncCat) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ReservationType/Create', mncCat)
          .then(function (response) {
              deferred.resolve(response);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    //UPDATE BY ID
    this.updateReservationType = function (repeat) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ReservationType/Update', repeat)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    //Bind Grid
    this.GetReservationTypeBindGrid = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ReservationType/ReservationTypeBindGrid')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);

app.controller('ReservationTypeController', ['$scope', '$q', 'ReservationTypeService', '$timeout', '$http', function ($scope, $q, ReservationTypeService, $timeout, $http) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];


    //$scope.StaDet[Id] = 1;
    $scope.ReservationType = {};
    $scope.repeatCategorylist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    //to Save the data
    $scope.Save = function () {
        if ($scope.IsInEdit) {
            ReservationTypeService.updateReservationType($scope.ReservationType).then(function (repeat) {
                $scope.ShowMessage = true;
                $scope.Success = repeat;
                $scope.LoadData();

                var savedobj = {};
                angular.copy($scope.ReservationType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.ClearData();
                $scope.IsInEdit = false;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
                $scope.ReservationType = {};

            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.ReservationType.RT_STATUS = "1";
            ReservationTypeService.saveReservationType($scope.ReservationType).then(function (response) {
                $scope.ShowMessage = true;
                console.log(response);
                $scope.Success = response.data;
                var savedobj = {};
                //$scope.EditCategory.MNC_UPDATED_DT = response.
                angular.copy($scope.ReservationType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.ReservationType = {};

            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    //for GridView
    var columnDefs = [
       //{ headerName: "Room Type Code", field: "RT_CODE", width: 120, cellClass: 'grid-align' },
       { headerName: "Facility Type Name", field: "RT_NAME", width: 160, cellClass: 'grid-align' },
       { headerName: "Status", template: "{{ShowStatus(data.RT_STATUS)}}", width: 100, cellClass: 'grid-align', suppressMenu: true },//template: "{{ShowStatus(data.RT_STATUS)}}"
       {
           headerName: "Action", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>',
           cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true
       }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        ReservationTypeService.GetReservationTypeBindGrid().then(function (data) {
            $scope.gridata = data;
            //$scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    }


    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $timeout($scope.LoadData, 1000);
    $scope.EditFunction = function (data) {
        $scope.ReservationType = {};
        $scope.EditCategory = data;
        angular.copy(data, $scope.ReservationType);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        $('#RT_CODE').prop('readonly', true);
    }
    $scope.ClearData = function () {
        $scope.ReservationType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $('#RT_CODE').prop('readonly', false);
        $scope.frmReservationType.$setPristine();
        $scope.frmReservationType.$setUntouched();
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

}]);