﻿app.service("AddFacilityService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();
   
    this.CreateReservation = function (crt) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AddFacility/Create', crt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.UpdateReservation = function (upd) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AddFacility/Update', upd)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

  
    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/AddFacility/GetGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.GetRoomDetails = function (crt) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AddFacility/GetRoomDetails', crt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


    this.DownloadFile = function (fileName) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/AddFacility/DownloadFile?fileName=' + fileName + ' ')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

}]);
app.controller('AddFacilityController', ['$scope', '$q', 'AddFacilityService', '$http', '$timeout', '$filter', 'UtilityService', function ($scope, $q, AddFacilityService, $http, $timeout, $filter, UtilityService) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.ReservationRoom = {};
    $scope.Cnylst = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.categorydata = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.ShowAddFacility = false;
    $scope.ShowAddFacilityGrid = true;
    $scope.ShowTable = 1;
    $scope.ShowIsActive = 0;
    $scope.ShowDownloadFile = 0;

    $scope.RTlst = [];

    $scope.CountryChanged = function () {
        $scope.CTYBYCOUNTRY = [];
        $scope.CTYBYCOUNTRY.push({ CNY_CODE: $scope.ReservationRoom.RF_CNY_CODE, CNY_NAME: '', ticked:true });
        console.log($scope.CTYBYCOUNTRY);
        UtilityService.getCitiesbyCny($scope.CTYBYCOUNTRY, 2).then(function (response) {
            $scope.Citylst = response.data
            setTimeout(function () {
                $('.selectpicker').selectpicker('refresh');
            }, 200);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.CityChanged = function () {
        $scope.LOCBYCITY = [];
        $scope.LOCBYCITY.push({ CTY_CODE: $scope.ReservationRoom.RF_CTY_CODE, CTY_NAME: '', CNY_CODE: $scope.ReservationRoom.RF_CNY_CODE, ticked: true });
        UtilityService.getLocationsByCity($scope.LOCBYCITY, 2).then(function (response) {
            $scope.Locationlst = response.data;
            setTimeout(function () {
                $('.selectpicker').selectpicker('refresh');
            }, 200);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.Save = function () {
        if ($scope.IsInEdit) {
            $scope.ReservationRoom.Flag = 2;
            console.log($scope.ReservationRoom);
            AddFacilityService.CreateReservation($scope.ReservationRoom).then(function (category) {
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully uploaded";
                var savedobj = {};
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.IsInEdit = false;
                setTimeout(function () {
                    $scope.$apply(function () {

                        showNotification('success', 8, 'bottom-right', $scope.Success);

                    });
                }, 700);
                $scope.LoadData();
                $scope.ReservationRoom = {};
            }, function (error) {
                console.log(error);
            })
        }
        else {
           // $scope.Reservation.RR_STATUS = "1";
            AddFacilityService.CreateReservation($scope.ReservationRoom).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully inserted";
                var savedobj = {};
             //   $scope.ReservationRoom.HOL_CITY_NAME = $scope.GetCityName($scope.ReservationRoom.HOL_CITY_CODE);
                //angular.copy($scope.ReservationRoom, savedobj)
                //$scope.gridata.unshift(savedobj);
                //$scope.gridOptions.api.setRowData($scope.gridata);
               
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.Reservation = {};
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;

                setTimeout(function () {
                    $scope.$apply(function () {

                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    var columnDefs = [
        { headerName: "Facility Type", field: "RT_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "Facility Name", field: "RF_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "City", field: "CTY_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "No Of Rooms", field: "RF_NO_OF_ROOMS", width: 200, cellClass: 'grid-align' },
        { headerName: "Status", template: "{{ShowStatus(data.RF_STATUS)}}", width: 200, cellClass: 'grid-align' },
        { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 200, suppressMenu: true },
        //{
        //      headerName: "Images", field: "", width: 200, cellClass: 'grid-align', cellRenderer: ghimages,  suppressMenu:true,
        //      //cellRendererParams: {
        //      //    rendererImage: 'Desert.jpg'           // Complementing the Cell Renderer parameters
        //      //},
        //      width:300

        //},
    ];

    $scope.LoadData = function () {
      

        UtilityService.getCountires(2).then(function (response) {
            $scope.Cnylst = response.data;


            UtilityService.getReservationTypes(1).then(function (response) {
                
                $scope.RTlst = response.data;
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Citylst = response.data;
                    }


                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locationlst = response.data;
                        }
                        setTimeout(function () {
                            $('.selectpicker').selectpicker('refresh');
                        }, 200);
                    });

                });

            }, function (error) {
                console.log(error);
            });

        }, function (error) {
            console.log(error);
        });






      //  progress(0, 'Loading...', true);
        AddFacilityService.GetGriddata().then(function (gddata) {
            $scope.gridata = gddata;
            $scope.gridOptions.api.setRowData(gddata);
          //  progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        angularCompileRows: true,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };
    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    function createImageSpan(image) {
        var resultElement = document.createElement("span");
        for (var i = 0; i < 1; i++) {
            var imageElement = document.createElement("img");
            imageElement.src = "/GHB/UploadImages/" + image;
            imageElement.height = 100;
            imageElement.width = 200;
            resultElement.appendChild(imageElement);
        }
        return resultElement;
    }

    function ghimages(params) {
       return createImageSpan(params.data.RRDT_FILE_NAME);
    }

   // $scope.gridOptions.rowHeight = 100;
    $timeout(function () {
        $scope.LoadData();
    }, 200);

    $scope.EditData = function (data) {
        console.log(data);
        console.log(data);
        $scope.ShowAddFacility = true;
        $scope.ShowAddFacilityGrid = false;
        $scope.ActionStatus = 1;
        $scope.ShowIsActive = 1;
        $scope.ShowDownloadFile = 1;
        $scope.IsInEdit = true;
        $scope.ReservationRoom.RF_FILE_NAME = data.RF_FILE_NAME;
        
        $('#downloadfile').text(data.RF_FILE_NAME);
        console.log($scope.ReservationRoom);
        angular.copy(data, $scope.ReservationRoom);

        AddFacilityService.GetRoomDetails(data).then(function (gddata) {
            console.log(gddata);
            $scope.ReservationRoom.RF_NO_OF_ROOMS = gddata.length;
            $scope.ReservationRoom.addroom = gddata;
          
        }, function (error) {
            console.log(error);
        });
        setTimeout(function () {
            $('.selectpicker').selectpicker('refresh');
        }, 200);
    }
    $scope.EraseData = function () {
        $scope.ReservationRoom = {};
        $scope.ReservationRoom.addroom = [{'RR_SNO':'', 'RR_NAME': '', 'RR_CAPCITY': '', 'RR_FACILITIES': '', 'dataImg':'', 'IsActive':'1'}];
        $scope.ReservationRoom.RF_NO_OF_ROOMS = $scope.ReservationRoom.addroom.length;
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        setTimeout(function () {
            $('.selectpicker').selectpicker('refresh');
        }, 200);
        $scope.frm.$setPristine();
        $scope.frm.$setUntouched();

    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }
    $scope.Close = function () {
        $scope.ShowAddFacility = false;
        $scope.ShowAddFacilityGrid = true;
    }

    $scope.Add = function () {
        $scope.ShowDownloadFile = 0;
        $scope.IsInEdit = false;
        $scope.ActionStatus = 0;
        $scope.ShowIsActive = 0;
        $scope.ShowAddFacility = true;
        $scope.ShowAddFacilityGrid = false;
        $scope.ReservationRoom = {};
        setTimeout(function () {
            $('.selectpicker').selectpicker('refresh');
        }, 200);
        $scope.ReservationRoom.addroom = [{'RR_SNO':'', 'RR_NAME': '', 'RR_CAPCITY': '', 'RR_FACILITIES': '', 'dataImg':'', 'IsActive':'1'}];
        $scope.ReservationRoom.RF_NO_OF_ROOMS = $scope.ReservationRoom.addroom.length;
    }

    //get city cat name to display in grid after submit
    $scope.GetCityName = function (ctyCode) {
        for (var i = 0; $scope.CityList != null && i < $scope.CityList.length; i += 1) {
            var result = $scope.CityList[i];
            if (result.CTY_CODE === ctyCode) {
                return result.CTY_NAME;
            }
        }
    }

    $scope.SaveNUpdate = function () {
        if ($scope.IsInEdit) {
            $scope.ReservationRoom.Flag = 2;
            $scope.ActionStatus = 0;
        }
        else {
            $scope.ReservationRoom.Flag = 1;
        }
        AddFacilityService.CreateReservation($scope.ReservationRoom).then(function (response) {
            console.log(response);
            $scope.ShowMessage = true;
            $scope.LoadData();
            if (response.data == null) {
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            $scope.ReservationRoom = {};
            $scope.ReservationRoom.addroom = [{ 'RR_SNO': '', 'RR_NAME': '', 'RR_CAPCITY': '', 'RR_FACILITIES': '', 'dataImg': '', 'IsActive': '1' }];
            $scope.ReservationRoom.RF_NO_OF_ROOMS = $scope.ReservationRoom.addroom.length;
        }, function (error) {
            $scope.ShowMessage = true;
            $scope.Success = error.data;
            setTimeout(function () {
                $scope.$apply(function () {
                    showNotification('error', 8, 'bottom-right', $scope.Success);
                    $scope.ShowMessage = false;
                });
            }, 1000);
            console.log(error);
        });
    };

      
    // add room
    $scope.ReservationRoom.addroom = [{'RR_SNO':'', 'RR_NAME': '', 'RR_CAPCITY': '', 'RR_FACILITIES': '', 'dataImg':'', 'IsActive':'1'}];
    $scope.ReservationRoom.RF_NO_OF_ROOMS = $scope.ReservationRoom.addroom.length;
  
    

    $scope.getCheckedTrue = function (index) {
        return true;
    };

    $scope.remove = function (index) {
        if (index == 0) {
        }
        else {
            $scope.ReservationRoom.addroom[index].dataImg = null;
            var i = _.indexOf($scope.ReservationRoom.addroom, $scope.ReservationRoom.addroom[index]);
            if (i !== -1) {
                $scope.ReservationRoom.addroom.splice(i, 1);
            }
            $scope.ReservationRoom.RF_NO_OF_ROOMS = $scope.ReservationRoom.addroom.length;
        }
   
    };

    $scope.checkAll = function () {
        if (!$scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.ReservationRoom.addroom, function (room) {
            room.selected = $scope.selectedAll;
        });
    };

    $scope.renderTable = function () {
        $scope.ShowTable = 1;
         $scope.addNew();
    }

    $scope.addNew = function (index) {
        var roomObj = {
            'RR_SNO':"",
            'RR_NAME': "",
            'RR_CAPCITY': "",
            'RR_FACILITIES': "",
            'dataImg': "",
            'IsActive': "1",
            
        };
        $scope.ReservationRoom.addroom.splice(index+1, 0, roomObj);
        $scope.ReservationRoom.RF_NO_OF_ROOMS = $scope.ReservationRoom.addroom.length;
        $scope.LoadData();
        console.log("success");
    };

    $scope.FindDuplicate = function (id) {
        
        var oldArr = $scope.ReservationRoom.addroom;
        console.log(oldArr);
        var newArr = _.filter(oldArr, function (element, index) {
            // tests if the element has a duplicate in the rest of the array
            for (index += 1; index < oldArr.length; index += 1) {
                if (_.isEqual(element.RR_NAME, oldArr[index].RR_NAME) && element.RR_NAME != "" && oldArr[index].RR_NAME != "") {
                    return false;
                }
            }
            return true;
        });
        if (oldArr.length == newArr.length) {
        }
        else {
            alert("Duplicate Room Names are not allowed");
            $scope.ReservationRoom.addroom[id].RR_NAME = "";
        }
    }

    // File Upload

    $scope.UploadFile = function () {
        if ($scope.IsInEdit) {
            $scope.ReservationRoom.Flag = 2;
            $scope.ActionStatus = 0;
        }
        else {
            $scope.ReservationRoom.Flag = 1;
        }
            progress(0, 'Loading...', true);
          
                    var formData = new FormData();
                    var UplFile = $('#FileUpl')[0];
                    var CurrObj = { AddFacilityVM: $scope.ReservationRoom };
                    formData.append("UplFile", UplFile.files[0]);
                    formData.append("CurrObj", JSON.stringify(CurrObj));
                    $.ajax({
                        url: UtilityService.path + "/api/AddFacility/UploadTemplate",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            $scope.LoadData();
                            $scope.ShowDownloadFile = 0;
                            $scope.ReservationRoom = {};
                            $scope.ReservationRoom.addroom = [{ 'RR_SNO': '', 'RR_NAME': '', 'RR_CAPCITY': '', 'RR_FACILITIES': '', 'dataImg': '', 'IsActive': '1' }];
                            $scope.ReservationRoom.RF_NO_OF_ROOMS = $scope.ReservationRoom.addroom.length;
                               progress(0, '', false);
                               console.log(response);
                               
                                   if (response.data != "") {
                                       showNotification('success', 8, 'bottom-right', response.Message);
                                   }
                                   else {
                                       showNotification('error', 8, 'bottom-right', response.Message);
                                   }
                               
                        }
                    });
              
           
    }

    // download file

    $scope.downloadFile = function () {
        AddFacilityService.DownloadFile($scope.ReservationRoom.RF_FILE_NAME).then(function (response) {
          
        }, function (error) {
          console.log(error);
        });
    }



}]);
app.directive('ngModelOnblur', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        priority: 1, // needed for angular 1.2.x
        link: function (scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function () {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(elm.val());
                });
            });
        }
    };
});
 