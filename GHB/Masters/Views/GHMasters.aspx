﻿<%@ Page Language="C#" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>
    <div class="al-content">
        <div class="widgets">
            <h3>Masters</h3>
        </div>
        <div class="card">
            <div class="card-body" style="padding-right: 50px; height: 300px;">
                <form id="form1" runat="server">
                    <div class="box-body">
                        <div class="clearfix row">
                            <div class="col-md-3 col-sm-12 col-xs-12 ">

                                <asp:HyperLink ID="HyperLink4" runat="server" class="btn btn-block btn-primary" role="button" NavigateUrl="~/GHB/Masters/Views/ReservationType.aspx">Add Facility Type</asp:HyperLink>

                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">

                                <asp:HyperLink ID="HyperLink11" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/GHB/Masters/Views/AddFacility.aspx">Add Facility & Room</asp:HyperLink>

                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/GHB/Masters/Views/GHBusinessSpecific.aspx">Business Specific</asp:HyperLink>
                            </div>
                        </div>
                        <br />
                    </div>

                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        var GHT = '<%= Session["GHT"]%>';
        $(document).ready(function () {
            $("#Title").append(GHT).append(" Masters");
        });

    </script>
</body>
</html>

