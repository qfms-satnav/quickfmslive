﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="GHBusinessSpecific.vb" Inherits="Masters_Mas_Webfiles_frmMasBusinessSpecificMaster" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Business Specific Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Business Specific Master</h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 50px; height:300px;">--%>
                <form id="form1" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Business Specific<span style="color: red;">*</span></label>
                                <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtParent"
                                    Display="none" ErrorMessage="Please Enter Business Specific Parent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtParent" runat="server" CssClass="form-control" Width="99%"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label"></label>
                                <label class="col-md-12 control-label"></label>
                                <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Update" ValidationGroup="Val1" />
                                <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/GHB/Masters/Views/GHMasters.aspx" CausesValidation="False" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%-- </div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });
    </script>
</body>
</html>
