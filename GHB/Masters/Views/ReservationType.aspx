﻿<%@ Page Language="C#" AutoEventWireup="true"%>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
     <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>



    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->

   <%-- <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>--%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="ReservationTypeController" class="amantra">
     <div class="al-content">
        <div class="widgets">
           <%-- <div ba-panel ba-panel-title="Add Facility Type" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
                <div class="panel">
                    <div class="panel-heading" style="height: 41px;">--%>
                        <h3 class="panel-title">Add Facility Type </h3>
                    </div>
                    <div class="card">
                    <%--<div class="card-body" style="padding-right: 50px;">--%>
                        <form role="form" id="form1" name="frmReservationType" data-valid-submit="Save()" novalidate>


                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmReservationType.$submitted && frmReservationType.RT_NAME.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">

                                        <label>Facility Type Name<span style="color: red;">*</span></label>


                                        <input id="RT_NAME" type="text" name="RT_NAME" maxlength="50" data-ng-model="ReservationType.RT_NAME" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" class="form-control" required="required" />
                                        <span class="error" data-ng-show="frmReservationType.$submitted && frmReservationType.RT_NAME.$invalid" style="color: red">Please enter facility type name</span>



                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">

                                        <label>Remarks</label>

                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="txtrem" data-ng-model="ReservationType.RT_REMARKS" class="form-control" maxlength="500"></textarea>
                                        </div>

                                    </div>


                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div data-ng-show="ActionStatus==1">
                                            <label>Status</label>

                                            <div>
                                                <select id="RT_STATUS" name="RT_STATUS" data-ng-model="ReservationType.RT_STATUS" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                        <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div data-ag-grid="gridOptions" style="height: 329px; width: 100%;" class="ag-blue"></div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
          <%--  </div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>


    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <%--<script src="../Js/ReservationType.js"></script>--%>
    <script src="../Js/ReservationType.min.js" defer></script>
</body>
</html>
