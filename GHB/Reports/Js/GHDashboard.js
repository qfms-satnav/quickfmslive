﻿function funLocationwiseFacilitiesCount(companyid) {
    var chart = c3.generate({
        data: {
            columns: [],
            type: 'pie',
            empty: { label: { text: "No Data Available" } },
            onclick: function (e) {
                $("#PropDetails").modal("show");
                //var selectedService = this.categories()[e.index]
                //$('#divBarGraphHeading').html("Guest House Request details -"+e.id + "  - " + "(" + selectedService + ")");
                $('#HeaderFacility').html("Facilities Details -" + e.id);
                GetPropertyDetailsByPropName(e.id, $('#ddlCompany option:selected').val());
            }
        },
        pie: {
            expand: false,
            label: {
                format: function (value, ratio, id) {
                    return d3.format('')(value);
                }
            }
        },
        axis: {
            x: {
                position: 'outer-center',
            },
        }
    });

    $.ajax({
        url: '../api/GuestHouseDBAPI/GetFacilitiesCountbyLocation',
        data: { "companyid": companyid },
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        success: function (result) {
            setTimeout(function () { chart.load({ columns: result }); }, 500);
        }
    });
    $("#GHfacCountContainer").html('');
    $("#GHfacCountContainer").append(chart.element);
}
function GetTotalFacilitiesBookedCount(companyid) {
    $.ajax({
        url: "../api/GuestHouseDBAPI/GetTotalRequestsCount",
        data: { "companyid": companyid },
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        success: function (data) {
                $("#lblwrtotcount").html(data);
        },
        error: function (errorval) {
            console.log(errorval);
        }
    });
}
function GetToalFacilitiesWithholdCount(companyid) {
    $.ajax({
        url: "../api/GuestHouseDBAPI/GetWithHoldRequestsCount",
        data: {"companyid":companyid},
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        success: function (data) {
            $("#lblWRclosed").html(data);
        },
        error: function (errorval) {
            console.log(errorval);
        }
    });
}

function funGuestHouseUtilization(companyid) {
    $.ajax({
        url: "../api/GuestHouseDBAPI/GHUtilization",
        data: { "companyid": companyid },
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        success: function (data) {
            var chart2 = c3.generate({
                data: {
                    json: JSON.parse(data.locVal),
                    keys: {
                        x: 'name',
                        value: JSON.parse(data.services),
                    },
                    type: 'bar',
                    onclick: function (e) {

                        $("#divBarGraph").modal("show");
                        //console.log(this.categories());
                        var selctedLocation = this.categories()[e.index]
                        console.log(selctedLocation);
                        //$('#divBarGraphHeading').html("Guest House Request details -"+e.id + "  - " + "(" + selectedService + ")");
                        $('#divBarGraphHeading').html("Guest House Request details -" + e.id);
                        //GetGHDetailsForBarChart(e.id, selectedService);
                        console.log(e.id);

                        GetGHDetailsForBarChart(e.id, selctedLocation);
                    }

                },
                axis: {
                    x: {
                        type: 'category'
                    },
                    y: {
                        show: true,
                        label: {
                            text: 'No.of Requests',
                            position: 'outer-middle'
                        }
                    },
                }
            });
            $('#GHUtlContainer').html('');
            $("#GHUtlContainer").append(chart2.element);
        },
        failure: function (response) {

        }
    });

}
function GetGHDetailsForBarChart(type, selectedLocation) {

    $.ajax({
        url: "../api/GuestHouseDBAPI/GetGHDetailsForBarChart",
        data: { "Category": type, "location": selectedLocation, "companyid": $('#ddlCompany option:selected').val() },
        //data: p,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            var table = $('#tblBarGraph');
            $('#tblBarGraph td').remove();
            for (var i = 0; i < data.length; i++) {
                table.append("<tr>" +
                    "<td>" + data[i].REFERENCE_ID + "</td>" +
                    "<td>" + data[i].RT_NAME + "</td>" +
                    "<td>" + data[i].RF_NAME + "</td>" +
                    "<td>" + data[i].RR_NAME + "</td>" +
                    "<td>" + data[i].LCM_NAME + "</td>" +
                    "<td>" + data[i].FROM_DATE + "</td>" +
                    "<td>" + data[i].TO_DATE + "</td>" +
                    "<td>" + data[i].FRM_TIME + "</td>" +
                    "<td>" + data[i].TO_TIME + "</td>" +
                    "<td>" + data[i].REQUESTED_DATE + "</td>" +
                    "<td>" + data[i].REQ_BY + "</td>" +

                    "</tr>");
            }
            if (data.length == 0) {
                table.append("<tr>" + "<td colspan='11' align='center'>No Records Found.</td>" + "</tr>");
            }
        },
        error: function (result) {
        }
    });
}
function GetPropertyDetailsByPropName(type, companyid) {
    console.log(type, companyid);
    $.ajax({
        url: "../api/GuestHouseDBAPI/GetFacilitiesforPie",
        data: { "lcmName": type, "companyid": companyid },
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        success: function (data) {

            var table = $('#tblPropDetails');
            $('#tblPropDetails td').remove();
            for (var i = 0; i < data.length; i++) {
                table.append("<tr>" +
                    "<td>" + data[i].RT_NAME + "</td>" +
                    "<td>" + data[i].RF_NAME + "</td>" +
                    "</tr>");
            }
            if (data.length == 0) {
                table.append("<tr>" + "<td colspan='6' align='center'> No Records Foud.</td>" + "</tr>");
            }

        },
        error: function (result) {
        }
    });
}
function GetBookedRequestDetails(companyid) {
    $.ajax({
        url: "../api/GuestHouseDBAPI/GetTotalRequestsDetails",
        data: { "companyid": companyid },
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        success: function (data) {

            //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
            //$("#lblwrtotcount").html(data);
            var table = $('#tbltotalWRS');
            $('#tbltotalWRS td').remove();
            for (var i = 0; i < data.Table.length; i++) {
                table.append("<tr>" +
                    "<td>" + data.Table[i].BM_REFERENCE_ID + "</td>" +
                    "<td>" + data.Table[i].RT_NAME + "</td>" +
                    "<td>" + data.Table[i].RF_NAME + "</td>" +
                    "<td>" + data.Table[i].RR_NAME + "</td>" +
                       "<td>" + data.Table[i].LCM_NAME + "</td>" +
                    "<td>" + data.Table[i].BM_FROM_DATE + "</td>" +
                       "<td>" + data.Table[i].BM_TO_DATE + "</td>" +
                        "<td>" + data.Table[i].BM_FRM_TIME + "</td>" +
                       "<td>" + data.Table[i].BM_TO_TIME + "</td>" +

                             "<td>" + data.Table[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table[i].REQ_BY + "</td>" +

                    "</tr>");
            }
            if (data.Table.length == 0) {
                table.append("<tr>" + "<td colspan='11' align='center'> No Requests Raised this Month</td>" + "</tr>");
            }
            //if (type == "1") {
            //    $("#lblwrtotcount").html(data.Table.length);
            //    //$("#lblWRclosed").html(data.Table1.length);
            //    //$("#lblpending").html(data.Table2.length);
            //}
        },
        error: function (result) {
        }
    });
}
//Total Work Requests count and View Details
$("#ancWorkReqTotal").click(function (e) {
    e.preventDefault();
    GetBookedRequestDetails($('#ddlCompany option:selected').val());
});
//Total Work Requests count and View Details
$("#ancWorkReqCompleted").click(function (e) {
    e.preventDefault();
    GetWithHoldReqDetails($('#ddlCompany option:selected').val());
});
function GetWithHoldReqDetails(companyid) {
    $.ajax({
        url: "../api/GuestHouseDBAPI/GetWithHoldRequestsDetails",
        data: { "companyid": companyid },
        contentType: "application/json; charset=utf-8",
        type: "GET",
        dataType: 'json',
        success: function (data) {

            //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
            //$("#lblwrtotcount").html(data);
            var table = $('#tblCompletedWRS');
            $('#tblCompletedWRS td').remove();
            for (var i = 0; i < data.Table.length; i++) {
                table.append("<tr>" +
                    "<td>" + data.Table[i].WM_REFERENCE_ID + "</td>" +
                    "<td>" + data.Table[i].RT_NAME + "</td>" +
                    "<td>" + data.Table[i].RF_NAME + "</td>" +
                    "<td>" + data.Table[i].RR_NAME + "</td>" +
                    //"<td>" + data.Table[i].CNY_NAME + "</td>" +
                    //   "<td>" + data.Table[i].CTY_NAME + "</td>" +
                          "<td>" + data.Table[i].LCM_NAME + "</td>" +

                           "<td>" + data.Table[i].WM_FROM_DATE + "</td>" +
                       "<td>" + data.Table[i].WM_TO_DATE + "</td>" +
                        "<td>" + data.Table[i].WM_FRM_TIME + "</td>" +
                       "<td>" + data.Table[i].WM_TO_TIME + "</td>" +

                             "<td>" + data.Table[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table[i].REQ_BY + "</td>" +

                    "</tr>");
            }
            if (data.Table.length == 0) {
                table.append("<tr>" + "<td colspan='11' align='center'> No WithHold Requests Raised this Month</td>" + "</tr>");
            }
            //if (type == "1") {
            //    $("#lblwrtotcount").html(data.Table.length);
            //    //$("#lblWRclosed").html(data.Table1.length);
            //    //$("#lblpending").html(data.Table2.length);
            //}
        },
        error: function (result) {
        }
    });
}
function Print(divName) {

    $('#' + divName).print({
        globalStyles: true,
        mediaPrint: false,
        stylesheet: "http://fonts.googleapis.com/css?family=Inconsolata",
        iframe: true,
        prepend: "<h3>QuickFMS</h3> <hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-bottom: 0.5em;''margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'>" + "<br/>",
        append: "<hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'> <h6 align='center'>© QuickFMS " + year + " </h6>"
    });

}
function BindCompanyForGHModule(valcompany) {
    $.ajax({
        url: '../api/Utility/GetCompanies',
        type: 'GET',
        success: function (result) {

            $.each(result.data, function (key, value) {
                $("#ddlCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });
        }
    });
}
$("#ddlCompany").change(function () {
    Cmpy = $(this).val();
    console.log(Cmpy);
    methodsToCallOnloadAndCompanyChange(Cmpy);
});

function methodsToCallOnloadAndCompanyChange(Cmpy) {
    setTimeout(function () {
        funLocationwiseFacilitiesCount(Cmpy);
    }, 500);
    GetTotalFacilitiesBookedCount(Cmpy);
    GetToalFacilitiesWithholdCount(Cmpy);
    funGuestHouseUtilization(Cmpy);
}



function BindCompanyForGHModule() {
    $.ajax({
        url: '../../../api/Utility/GetCompanies',
        type: 'GET',
        success: function (result) {

            $.each(result.data, function (key, value) {
                $("#ddlCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });
        }
    });
}