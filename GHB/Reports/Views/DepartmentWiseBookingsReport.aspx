﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        /* .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
*/
        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /*  .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }*/
    </style>
</head>
<body data-ng-controller="DepartmentWiseBookingsReportController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3 class="panel-title">Department Wise Bookings Report </h3>
        </div>
        <div class="card">
            <form id="form1" name="Pervsoffical" novalidate>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.CNY_NAME.$invalid}">
                            <label class="control-label">Country <span style="color: red;">*</span></label>
                            <div isteven-multi-select data-input-model="Country" data-output-model="Pervsofficalbook.Country" data-button-label="icon CNY_NAME"
                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="Pervsofficalbook.Country[0]" name="CNY_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.CTY_NAME.$invalid}">
                            <label class="control-label">City<span style="color: red;">**</span></label>
                            <div isteven-multi-select data-input-model="City" data-output-model="Pervsofficalbook.City" data-button-label="icon CTY_NAME"
                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="Pervsofficalbook.City[0]" name="CTY_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.CTY_NAME.$invalid" style="color: red">Please Select City </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.LCM_NAME.$invalid}">
                            <label class="control-label">Location <span style="color: red;">**</span></label>
                            <div isteven-multi-select data-input-model="Locations" data-output-model="Pervsofficalbook.Locations" data-button-label="icon LCM_NAME"
                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="Pervsofficalbook.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Company</label>
                            <select id="ddlCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                        </div>
                    </div>



                </div>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': Pervsoffical.$submitted && Pervsoffical.Booking_Type.$invalid}">
                            <label class="control-label">Booking Type</label>
                            <select id="Booking_Type" name="Booking_Type" required="" data-ng-model="Pervsofficalbook.Booking_Type" class="selectpicker">
                                <option value="ALL">--ALL--</option>
                                <option value="1">Personal</option>
                                <option value="2">Official</option>

                            </select>
                            <span class="error" data-ng-show="Pervsoffical.$submitted && Pervsoffical.Booking_Type.$invalid" style="color: red">Please select booking type</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" id="divMonth">
                        <div class="form-group">
                            <label for="txtcode">Select Month</label>


                            <br />
                            <select id="month" class="selectpicker" data-ng-model="Pervsofficalbook.Month">

                                <option value="1" selected>January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>


                            </select>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12" id="divyear">
                        <div class="form-group">
                            <label for="txtcode">Select Year</label>
                            <br />
                            <select id="year" class="selectpicker" data-ng-model="Pervsofficalbook.Year">
                                <option value="2005">2005</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option>
                                <option value="2010">2010</option>
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024" selected>2024</option>
                            </select>
                        </div>


                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-12">
                        <br />
                        <div class="box-footer text-right">
                            <input type="submit" value="Search" data-ng-click="LoadData(0)" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </div>



                <div class="row" style="padding-left: 18px">
                    <div class="col-md-6">
                        <%--  <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />--%>
                    </div>
                    <%-- <div class="col-md-12">
                                    &nbsp;
                                </div>--%>

                    <div class="col-md-6" id="table2">
                        <br />
                        <a data-ng-click="GenReport(Pervsofficalbook,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                        <a data-ng-click="GenReport(Pervsofficalbook,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                        <a data-ng-click="GenReport(Pervsofficalbook,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                    </div>
                </div>

                <%--</form>
                        <form id="form2">--%>
                <div id="Tabular" data-ng-show="GridVisiblity">
                    <div class="row">
                        <%--<input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>
                        <div class="col-md-12">
                            <div class="input-group" style="width: 20%">
                                <div class="col-md-12 col-sm-6">
                                    <div class="input-group-btn">
                                        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                        <button class="btn btn-primary custom-button-color" data-ng-click="LoadData(1)" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div id="Grid" data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                    <div id="PerOffGraph">&nbsp</div>
                </div>
            </form>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js" defer></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/main.js" defer></script>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY')));


        }


        $(document).ready(function () {
            BindCompanyForGHModule();
            setTimeout(function () {
                $('.selectpicker').selectpicker('refresh');
            }, 200);
        });

        var companyid = '<%=Session["COMPANYID"]%>'

        function enableDisableCompanyDDL(compid) {
            if (compid == "1") {
                $("#ddlCompany").prop('disabled', false);
            }
            else {
                $("#ddlCompany").prop('disabled', true);
                $('#ddlCompany option[value="' + compid + '"]').attr("selected", "selected");
                setTimeout(function () {
                    $('.selectpicker').selectpicker('refresh');
                }, 200);
            }
        }


        setTimeout(function () { enableDisableCompanyDDL(companyid) }, 1000);
    </script>


    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../Js/DepartmentWiseBookingsReport.js" defer></script>
    <%--<script src="../Js/DepartmentWiseBookingsReport.js"></script>--%>
    <script src="../../../Scripts/moment.min.js" defer></script>
</body>
</html>
