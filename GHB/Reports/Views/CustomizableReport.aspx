﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="CustomizableReportController" onload="setDateVals()" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3 class="panel-title">Customizable Report</h3>
        </div>
        <div class="card">
            <form id="form1" name="CustomizableReport" novalidate>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': CustomizableReport.$submitted && CustomizableReport.CNY_NAME.$invalid}">
                            <label class="control-label">Country <span style="color: red;">*</span></label>
                            <div isteven-multi-select data-input-model="Country" data-output-model="Customizable.Country" data-button-label="icon CNY_NAME"
                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="Customizable.Country[0]" name="CNY_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="CustomizableReport.$submitted && CustomizableReport.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': CustomizableReport.$submitted && CustomizableReport.CTY_NAME.$invalid}">
                            <label class="control-label">City<span style="color: red;">**</span></label>
                            <div isteven-multi-select data-input-model="City" data-output-model="Customizable.City" data-button-label="icon CTY_NAME"
                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="Customizable.City[0]" name="CTY_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="CustomizableReport.$submitted && CustomizableReport.CTY_NAME.$invalid" style="color: red">Please Select City </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': CustomizableReport.$submitted && CustomizableReport.LCM_NAME.$invalid}">
                            <label class="control-label">Location <span style="color: red;">**</span></label>
                            <div isteven-multi-select data-input-model="Locations" data-output-model="Customizable.Locations" data-button-label="icon LCM_NAME"
                                data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="Customizable.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="CustomizableReport.$submitted && CustomizableReport.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Quick Select <span style="color: red;">**</span></label>
                            <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                <option value="TODAY">Today</option>
                                <option value="YESTERDAY">Yesterday</option>
                                <option value="7">Last 7 Days</option>
                                <option value="30">Last 30 Days</option>
                                <option value="THISMONTH">This Month</option>
                                <option value="LASTMONTH">Last Month</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="clearfix">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="txtcode">From Date</label>
                            <div class="input-group date" id='fromdate'>
                                <input type="text" class="form-control" data-ng-model="Customizable.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                </span>
                            </div>
                            <span class="error" data-ng-show="CustomizableReport.$submitted && CustomizableReport.FromDate.$invalid" style="color: red;">Please select from date</span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="txtcode">To Date</label>
                            <div class="input-group date" id='todate'>
                                <input type="text" class="form-control" data-ng-model="Customizable.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                </span>
                            </div>
                            <span class="error" data-ng-show="CustomizableReport.$submitted && CustomizableReport.ToDate.$invalid" style="color: red;">Please select to date</span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': CustomizableReport.$submitted && CustomizableReport.Request_Type.$invalid}">
                            <label class="control-label">Status Type</label>
                            <select id="Request_Type" name="Request_Type" required="" data-ng-model="Customizable.Request_Type" class="selectpicker">
                                <option value="ALL" selected>--ALL--</option>
                                <option value="Booked">Booked</option>
                                <option value="Vacant">Vacant</option>
                                <option value="Withhold">Withhold</option>
                                <option value="checkin">Check-in</option>
                                <option value="checkout">Check Out</option>

                            </select>
                            <span class="error" data-ng-show="CustomizableReport.$submitted && CustomizableReport.Request_Type.$invalid" style="color: red">Please select status</span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': CustomizableReport.$submitted && CustomizableReport.Columns.$invalid}">
                            <label class="control-label">Select Columns <span style="color: red;">*</span></label>
                            <div isteven-multi-select data-input-model="Cols" data-output-model="COL" data-button-label="icon COL"
                                data-item-label="icon COL" data-on-select-all="" data-on-select-none=""
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                            <span class="error" data-ng-show="CustomizableReport.$submitted && CustomizableReport.Columns.$invalid" style="color: red">Please select columns </span>
                        </div>
                    </div>


                </div>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Company</label>
                            <select id="ddlCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-12 col-xs-12" style="padding-top: 18px;">
                        <div class="box-footer text-right">
                            <input type="submit" value="Search" data-ng-click="LoadData(0)" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </div>
                <br />
                <%-- </form>
                        <form id="form2">--%>
                <div data-ng-show="GridVisiblity">
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <a data-ng-click="GenReport(Customizable,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(Customizable,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(Customizable,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group" style="width: 20%">
                                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary custom-button-color" data-ng-click="LoadData(1)" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 100%"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
        }
        $(document).ready(function () {
            BindCompanyForGHModule();
            setTimeout(function () {
                $('.selectpicker').selectpicker('refresh');
            }, 200);
        });

        var companyid = '<%=Session["COMPANYID"]%>'

        function enableDisableCompanyDDL(compid) {
            if (compid == "1") {
                $("#ddlCompany").prop('disabled', false);
            }
            else {
                $("#ddlCompany").prop('disabled', true);
                $('#ddlCompany option[value="' + compid + '"]').attr("selected", "selected");
                setTimeout(function () {
                    $('.selectpicker').selectpicker('refresh');
                }, 200);
            }
        }
        setTimeout(function () { enableDisableCompanyDDL(companyid) }, 1000);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <%-- <script src="../Js/CustomizableReport.min.js" defer></script>--%>
    <script src="../Js/CustomizableReport.js"></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
</body>
</html>
