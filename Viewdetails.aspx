﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Viewdetails.aspx.vb" Inherits="Viewdetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

       
.item td { width:30px;word-wrap : break-word ;word-break : normal}


/*.headerStyle 
{
    background-color: #FF6600;
    color: #FFFFFF;
    font-size: 8pt;
    font-weight: bold;
}

.itemStyle
{
    background-color: #FFFFEE;
    color: #000000;
    font-size: 8pt;
}

.alternateItemStyle
{
    background-color: #FFFFFF;
    color: #000000;
    font-size: 8pt;
}*/
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       
           
        
          <table id="table2"  width="95%" align="center" border="0">
              <tr>
                  <td align="left">
                      <img src="images/company_logo.jpg" />

                  </td>

              </tr>

            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Space Demo
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
          <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export" />
        <br />
          <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
         <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
               
                <tr>
                    <td>
                        <img alt="" height="27" src="Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Space Demo</strong>
                        
                    </td>
                    <td>
                        <img alt="" height="27" src="Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
        
                        <br />
                      <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="false"  AllowPaging="false"
                                                    Width="100%" CssClass="item">
                          <Columns>



                             <%-- <asp:BoundField DataField="PAGENAME" HeaderText="PageName" ReadOnly="true" 
    ItemStyle-Width="50px" ItemStyle-CssClass="itemStyle"/>

                               <asp:BoundField DataField="STOREDPROCEDURES" HeaderText="Procedures" ReadOnly="true" 
    ItemStyle-Width="80px" ItemStyle-CssClass="itemStyle"/>

                               <asp:BoundField DataField="HTML_DESIGN_STATUS" HeaderText="HTML Design Status" ReadOnly="true" 
    ItemStyle-Width="50px" ItemStyle-CssClass="itemStyle"/>

                               <asp:BoundField DataField="Code Status" HeaderText="Code Status" ReadOnly="true" 
    ItemStyle-Width="50px" ItemStyle-CssClass="itemStyle"/>--%>






                            <asp:TemplateField HeaderText="PageName">
                                   <HeaderStyle Width="30%" />
<ItemStyle Width="30%" Wrap="false" />
                                  <ItemTemplate>
                                      <asp:Label ID="lblpagename" runat="server" Text='<%#Eval("PAGENAME") %>'></asp:Label>
                                    
                                  </ItemTemplate>
                                 

                             

                              </asp:TemplateField>
                               <asp:TemplateField HeaderText="Procedures">
                                   <HeaderStyle Width="10%" />
<ItemStyle Width="10%" Wrap="false"  />
                                  <ItemTemplate>
                                      <asp:Label ID="LBLPROCEDURE" runat="server" Text='<%#Eval("STOREDPROCEDURES") %>'></asp:Label>

                                  </ItemTemplate>
                                             

                              </asp:TemplateField> <asp:TemplateField HeaderText="HTML Design Status">
                                    <HeaderStyle Width="30%" />
<ItemStyle Width="30%"  Wrap="false" />
                                  <ItemTemplate>
                                      <asp:Label ID="LBLDESIGN" runat="server" Text='<%#Eval("HTML_DESIGN_STATUS") %>'></asp:Label>

                                  </ItemTemplate>
                                            
                              </asp:TemplateField> <asp:TemplateField HeaderText="Code Status">
                                   <HeaderStyle Width="30%" />
<ItemStyle Width="30%"  Wrap="false" />
                                  <ItemTemplate>
                                      <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("CODE STATUS") %>'></asp:Label>

                                  </ItemTemplate>
                                            
                              </asp:TemplateField>


                          </Columns>
                                                   
                                                </asp:GridView>

               </td>
                    <td background="Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>

     </asp:Panel> 
    </div>
    </form>
</body>
</html>
