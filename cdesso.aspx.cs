﻿using SamlByVinod;
using System;
using System.IO;
using System.Web;

public partial class cdesso : System.Web.UI.Page
{
    public static bool CreateLogFile(string message)
    {
        try
        {
            // string location = @"C://IRPC//myfile1.txt";
            string filename = "NewLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            string location = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") + filename); // ''(System.Environment.CurrentDirectory + ("\log.txt"))
            if (!File.Exists(location))
            {
                FileStream fs;
                fs = new FileStream(location, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                fs.Close();
            }

            Console.WriteLine(message);
            // Release the File that is created
            StreamWriter sw = new StreamWriter(location, true);
            sw.Write((message + Environment.NewLine));
            sw.Close();
            sw = null;
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        CreateLogFile("Saml Start At " + DateTime.Now);
        CreateLogFile("Saml Response" + Request.Form["SAMLResponse"]);

        var samlEndpoint = "https://login.microsoftonline.com/e1aaed4a-43f1-415e-9b7e-9cc7dfeb46c8/saml2";

        var request = new AuthRequest(
            "https://live.quickfms.com/cdesso.aspx", //TODO: put your app's "entity ID" here
            "https://live.quickfms.com/cdesso.aspx" //TODO: put Assertion Consumer URL (where the provider should redirect users after authenticating)
        );
        if (Request.Form["SAMLResponse"] == "" || Request.Form["SAMLResponse"] == null)
        {
            request.ForceAuthn = true;
            Response.Redirect(request.GetRedirectUrl(samlEndpoint));
        }
        else
        {
            
            string samlCertificate = @"MIIC8DCCAdigAwIBAgIQT6A7GwL0GqNHkt5LqHqcWTANBgkqhkiG9w0BAQsFADA0MTIwMAYDVQQDEylNaWNyb3NvZnQgQXp1cmUgRmVkZXJhdGVkIFNTTyBDZXJ0aWZpY2F0ZTAeFw0yMzA5MjcxMzQzNDdaFw0yNjA5MjcxMzQzNDdaMDQxMjAwBgNVBAMTKU1pY3Jvc29mdCBBenVyZSBGZWRlcmF0ZWQgU1NPIENlcnRpZmljYXRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwdyLp44UnI0y2YmaKcmRRdDa6vcBr3IT+GVvv6kAz7ESAp+V5mWUEfYuK/rMflkh+YkIyyfbNyCLpsEj30+KHekW9b/FgxuFIpdUDXQNR3H1hJpnaGUZv+SJ6tWPnHlOUR0oD0wlH9vuKf6WZuvnfLoAJ2odh2xdYsTHKhZ+YFEgpz7D2NDIR/1AKtbaamOjzkpSKkQVOckg7JsYe+srDsaCTec35fKYrMp25fKNfwWGCYSVerZVMYJd4BM4nAFinjGcHjY/tOF17XaO8Pp1/qD7plnkBr+0XZ+TUfWC9BVN+qaPWPdu5oJHT+vlhfQqQLWMRtoch4xEuC/+Md03FQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBl9CWGrz30VfsmFhym3p5Vwd7iOPaoPJKpk6gcyzE7wNfBVdH/k+FxVAVkL+SsQ1ejhoxDRIBYp3xTw7RjDJ2dabIQvyZoW364zjYTMoibg7Vfy9fZiUUXTUB4OHahWQ3m8UCE5wqs1/af9k6Abrt5HZVRsMsWVmvkvYsQlVcPp85HwYsQl+er6FIZzXUXmGcqlVgiW8CDIIQPPNa/KuOyQIE9Y6YZp0SROHEODcD5s20P1gPB00hHGt6sx5P9uJBDI7c0sN1gYyU7uQ8Fbr1mTMUfTTjjVUkFA0vco/ISKnNVP50brwvkFh8SWkJU6nP7CFpHegtZBEsHaEOGY5iL";
            // 2. Let's read the data - SAML providers usually POST it into the "SAMLResponse" var
            var samlResponse = new Response(samlCertificate, Request.Form["SAMLResponse"]);
            CreateLogFile("Saml Start At " + DateTime.Now);
            CreateLogFile("Saml Response" + Request.Form["SAMLResponse"]);

            string email_id = "";
            string emp_id = "";
            string filename = "";
            string filepath = "";
            string str1 = "~/Logcdesso.aspx?company=";
            string redirect_url = "";

            if (samlResponse.IsValid())
            {
                //CreateLogFile("Start In IsValid");
                emp_id = samlResponse.GetNameID();
                email_id = samlResponse.GetNameID();
                CreateLogFile("emp_id:- " + emp_id + " email_id : - " + email_id);
                redirect_url = str1 + "CDE-VU" + "&empid=" + emp_id + "&email=" + email_id;
                CreateLogFile("Redirect URL: " + redirect_url);
                //Server.TransferRequest(redirect_url);
                Response.Redirect(redirect_url);
                //CreateLogFile("emp_id:- " + emp_id + " email_id : - " + email_id); 
            }
            else
            {

                Response.Write("114 Failed");
                //CreateLogFile("Error in Isvalid");
            }
        }

    }
}