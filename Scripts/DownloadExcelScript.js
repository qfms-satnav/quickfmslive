﻿app.directive('downloadAsExcel', function ($compile, $sce, $templateRequest) {
    return {
        restrict: 'E',
        scope: {
            template: '@',
            object: '='
        },
        replace: true,
        template: '<a class="xls"><i title="Export to Excel" class="fa fa-file-excel-o fa-2x" style="margin: 0px 0px 0px 160px;"></i></a>',
        link: function (scope, element, attrs) {
            var contentType = 'data:application/vnd.ms-excel;base64';
            var htmlS = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" ><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{sheetname}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>{table}</body></html>';
            var format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };

            var blobbed = function (data) {
                var blob = new Blob([format(htmlS, data)], { type: contentType });
                var blobURL = window.URL.createObjectURL(blob);
                if (blobURL) {
                    element.attr('href', blobURL);
                    element.attr('download', data['name']);
                }
            };

            scope.$watch('object', function (nv, ov) {
                var tUrl = $sce.getTrustedResourceUrl(scope.template);
                $templateRequest(tUrl)
                    .then(function (tmpl) {
                        var t = $('<div/>').append($compile(tmpl)(scope));
                        setTimeout(function () {
                            scope.$apply();
                            blobbed({
                                sheetname: attrs.sheetname,
                                name: attrs.xlname,
                                table: t.html()
                            });
                        }, 100);
                    });
            }, true);
        }
    };
})