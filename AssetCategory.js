﻿app.service("AssetCategoryService", function ($q, $http) {

    this.SaveAssetCategory = function (assetCategory) {
        debugger;
        deferred = $q.defer();
        $http.post('../../api/AssetCategory/SaveAssetCategory', assetCategory)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error);
            });
        return deferred.promise;
    };


    this.ModifyAssetCategories = function (assetCategory) {
        deferred = $q.defer();
        $http.post('../../api/AssetCategory/ModifyAssetCategories', assetCategory)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error);
            });
        return deferred.promise;
    };

    this.GetBRNGrid = function () {
        deferred = $q.defer();
        return $http.get('../../api/AssetCategory/GetGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    };
    this.GetAssetTypes = function () {
        deferred = $q.defer();
        return $http.get('../../api/AssetCategory/AssetTypes')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    };
});


app.controller('AssetCategoryController', ['$scope', 'AssetCategoryService', '$http', 'UtilityService', function ($scope, AssetCategoryService, $http, UtilityService) {


    $scope.assetCategories = [];
    $scope.cat = {};
    $scope.isEditMode = false;
    var selectedFiles = { Files: [] };
    var selecteddata = {};
    var Documents = [];
    $scope.buttonText = "Add";
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.AssetTypes = [
        { Id: 1, Name: 'Consumable Asset' },
        { Id: 0, Name: 'Capital Asset' }
    ];

    $scope.LoadData = function () {
        AssetCategoryService.GetBRNGrid().then(function (data) {

            $scope.assetCategories = data;
            $scope.gridOptions.api.setRowData($scope.assetCategories);

            AssetCategoryService.GetAssetTypes().then(function (Vdata) {
                $scope.AssetTypeListS = Vdata;

            }, function (error) {
                console.log(error);
            });
        })
    };

    $scope.Save = function () {
        var assetCategory = {
            VT_CODE: $scope.cat.VT_CODE,
            VT_TYPE: $scope.cat.VT_TYPE,
            VT_CONS_STATUS: $scope.cat.ASSET_TYPE,
            VT_STATUS: $scope.cat.VT_STATUS,
            VT_REM: $scope.cat.VT_REM,
            VT_DEPR: $scope.cat.VT_DEPR,
            VT_UPLOADED_DOC: $scope.cat.VT_UPLOADED_DOC,
            AST_TYPE_ID: $scope.cat.ASSET_TYPE
        };

        $scope.saveImg();

        if ($scope.isEditMode) {
            var obj = {
                VT_CODE: $scope.cat.VT_CODE,
                VT_TYPE: $scope.cat.VT_TYPE,
                VT_CONS_STATUS: $scope.cat.ASSET_TYPE,
                VT_STATUS: $scope.cat.VT_STATUS,
                VT_REM: $scope.cat.VT_REM,
                VT_DEPR: $scope.cat.VT_DEPR,
                VT_UPLOADED_DOC: $scope.cat.VT_UPLOADED_DOC
            };

            AssetCategoryService.ModifyAssetCategories(obj).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Asset Category Successfully Modified";
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.isEditMode = false;
                $scope.LoadData();
                $scope.resetForm();
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data.Message || "An error occurred.";
                showNotification('error', 8, 'bottom-right', $scope.Success);
                console.log(error);
            });
        } else {
            AssetCategoryService.SaveAssetCategory(assetCategory).then(function (response) {

                if (response.Message == "Asset Category already exists.") {

                    $scope.ShowMessage = true;
                    $scope.error = "Asset Category Code is in use; try another";

                    showNotification('error', 8, 'bottom-right', $scope.error);
                    $scope.LoadData();
                    //$scope.resetForm();
                } else {
                    $scope.Success = "New Asset Category Successfully Added";
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', $scope.Success);
                    $scope.isEditMode = false;
                    $scope.LoadData();
                    $scope.resetForm();
                }
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data.Message || "An error occurred.";
                showNotification('error', 8, 'bottom-right', $scope.Success);
                console.log(error);
            });
        }
    };


    var columnDefs = [
        { headerName: "Category Code", field: "VT_CODE", width: 270, cellClass: 'grid-align' },
        { headerName: "Category Name", field: "VT_TYPE", width: 270, cellClass: 'grid-align' },
        { headerName: "Depreciation (%)", field: "VT_DEPR", width: 270, cellClass: 'grid-align' },
        { headerName: "Asset Type", field: "ASSET_TYPE", width: 270, cellClass: 'grid-align' },
        //{ headerName: "Asset Type", field: "ASSET_TYPE", width: 270 },
        { headerName: "Status", field: "VT_STATUS", template: "{{ShowStatus(data.VT_STATUS)}}", width: 270, cellClass: 'grid-align' },
        /*  { headerName: "Edit", template: '<a data-ng-click="EditData(data)"><i class="Edit">Edit</a>', cellClass: 'grid-align', width: 110 }*/
        { headerName: "Edit", width: 150, template: '<a ng-click="EditData(data)"> <i class="Edit">Edit</i> </a>', cellClass: 'grid-align', suppressMenu: true }

    ];


    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        rowData: null,
        enableSorting: true,
        angularCompileRows: true,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    }
    $scope.ShowStatus = function (value) {
        $('#VT_STATUS').selectpicker('refresh');
        return $scope.StaDet[value == 0 ? 1 : 0].Name;

    }

    $scope.Showtype = function (value) {
        $('#ASSET_TYPE').selectpicker('refresh');
        /* var assetType = $scope.AssetTypes.find(type => type.Id === value);*/
        return $scope.AssetTypes[value == 0 ? 1 : 0].Name;
    };
    $scope.fileUpload = function (x, type) {

        var types = type;
        var now = moment();
        var dateTimePrefix = now.format('YYYYMMDDHHmmss');

        angular.forEach(x.files, function (file) {
            // Create the new filename with date-time prefix
            var originalFileName = file.name;
            var fileExtension = originalFileName.split('.').pop();
            var baseName = originalFileName.substring(0, originalFileName.lastIndexOf('.'));
            var newFileName = dateTimePrefix + '_' + baseName + '.' + fileExtension;

            var fileObj = {
                FileName: newFileName,
                FileSize: file.size,
                FileType: types,
                Type: type,
                data: file
            };
            var fileName = newFileName
            var Filetype = types

            $scope.$apply(function () {
                $scope.cat.VT_UPLOADED_DOC = fileName;
            });

            selecteddata[Filetype] = fileName;
            selectedFiles.Files.push(fileObj);
            Documents.push(fileObj);

        });


    };
    $scope.saveImg = function () {

        var frmData = new FormData();
        angular.forEach(selectedFiles.Files, function (fileObj) {
            var file = new Blob([fileObj.data]);
            frmData.append(fileObj.FileName, file, 'file');
            /*frmData.append(fileObj.Type, file, 'Type');*/
        });

        $http.post(UtilityService.path + '/api/AssetCategory/UploadFiles', frmData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined },
            processData: false
        }).then(function (response) { // Handle success response from the server
            console.log('File uploaded successfully:', response.data);
        }).catch(function (error) { // Handle errors
            console.error('File upload failed:', error);
        });
    }
    $scope.EditData = function (data) {
        $scope.cat.VT_CODE = data.VT_CODE;
        $scope.cat.VT_TYPE = data.VT_TYPE;
        $scope.cat.ASSET_TYPE = data.ASSET_TYPE;
        $scope.cat.VT_STATUS = data.VT_STATUS;
        $scope.cat.VT_REM = data.VT_REM;
        $scope.cat.VT_DEPR = data.VT_DEPR;
        $scope.cat.VT_UPLOADED_DOC = data.VT_UPLOADED_DOC;
        $scope.isEditMode = true;
        $scope.buttonText = "Modify";
    };

    $scope.resetFileInput = function () {
        var fileInput = document.getElementById('file');
        if (fileInput) {
            fileInput.value = '';
        }

        selectedFiles.Files = [];
        Documents = [];
    };

    $scope.resetForm = function () {
        $scope.cat = {};
        $scope.isEditMode = false;
        $scope.buttonText = "Add";
        $scope.resetFileInput();
    };

    $scope.LoadData();

}]);



