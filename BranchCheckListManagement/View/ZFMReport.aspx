﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<script runat="server">


</script>


<html lang="en" data-ng-app="QuickFMS">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::amantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<style>
            .panel-title {
                font-size: 16px;
            }
    </style>--%>
    <%--    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>

    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        /*.grid-align {
            text-align: center;
        }
*/
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            width: 355px;
        }

        /* .ag-header-cell-filtered {
            background-color: #4682B4;
        }*/
    </style>
</head>

<body data-ng-controller="ZFMReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="ZFM Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Validation Report</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1"  data-valid-submit="LoadData()" novalidate>
                <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="txtcode">Type</label>
                                <div isteven-multi-select data-input-model="MainType" data-output-model="ZFMReport.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="GetButton()">
                                </div>
                                <input type="text" data-ng-model="ZFMReport.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                                <%--<span class="error" data-ng-show="frmCentralTeam.$submitted && frmCentralTeam.MainType.$invalid" style="color: red">Please Select Main Type </span>--%>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Select Range</label>
                                <br />
                                <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                    <option value="SELECT">Select range</option>
                                    <option value="TODAY">Today</option>
                                    <option value="YESTERDAY">Yesterday</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 30 Days</option>
                                    <option value="THISMONTH">This Month</option>
                                    <option value="LASTMONTH">Last Month</option>
                                </select>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="ZFMReport.FromDate" id="FromDate" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>--%>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="ZFMReport.ToDate" id="ToDate" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>--%>
                            </div>
                        </div>


                        
                    </div>
                    <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                            <input type="submit" value="Search" class="btn btn-primary custom-button-color"  />
                        </div>
                    <br />
               <%--     <div class="row" style="padding-left: 18px">
                        <div class="box-footer text-right" id="table2" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                            <br />
                            <%--<a data-ng-click="GenReport(LeaseRep,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>--%>
                            <%-- <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            <%--<a data-ng-click="GenReport(LeaseRep,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                    </div>--%>
                    
                    <div class="row" style="padding-left: 18px">
                        <div class="col-md-11 col-sm-6 col-xs-12">
                            <br />
                            <div class="box-footer text-right">

                                <a data-ng-click="GenReportPivot()">
                                    <input type="button" value="Download to Pivot Excel" class="btn btn-primary custom-button-color" /></a>
                                     <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>

                            </div>
                             <%--<div class="box-footer text-right" id="table2" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                            <br />
                            <%--<a data-ng-click="GenReport(LeaseRep,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                            <%--<a data-ng-click="GenReport(LeaseRep,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>--%>
                        </div>
                       
                    </div>
                     </form>
                <%--    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%" />
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 100%">
                                <div data-ag-grid="gridOptions1" class="ag-blue" style="height: 310px; width: 100%; display: none;">
                                </div>
                            </div>
                        </div>
                    </div>--%>
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel-heading ">
                            <ul class="nav nav-tabs">
                                <li><a href="#tabSummary" class="active" data-ng-click="gridOptions" data-toggle="tab">Detail Report</a></li>
                                <%--<li><a href="#tabmonth" data-ng-click="gridOptions" data-toggle="tab">Attachment Details</a></li>--%>
                                <li><a href="#tabmonth" data-toggle="tab">Summary Report</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade in active show" id="tabSummary">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 100%">
<%--                                    <div data-ag-grid="gridOptions1" class="ag-blue" style="height: 310px; width: 100%; display: none;">--%>
                                    </div>
                                </div>
                            
                             <div class="tab-pane" id="tabmonth">                                                              
                                    <div data-ag-grid="gridOptions1" class="ag-blue" style="height: 310px; width: 100%;">
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
               
            </div>
        </div>
        <%--  </div>
            </div>--%>
        <%--  </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
     <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
    <script src="../../Scripts/jspdf.min.js"></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../Scripts/moment.min.js"></script>
    <script src="../../Scripts/moment.min.js"></script>

    <script src="../JS/ZFMReport.js"></script>
    <script src="../../SMViews/Utility.js"></script>

</body>
</html>
