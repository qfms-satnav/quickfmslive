﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        img{
            transform: rotate(-270deg);
        }
        </style>
</head>
<body data-ng-controller="Branch_Inspect_Controller" class="amantra">
    <nav class="navbar bg-body-tertiary" style="background-color: #e3f2fd;">
        <div class="container-fluid">
            <a class="navbar-brand">Branch Inspection Report</a>
            <form class="d-flex" role="search">
                <button class="btn btn-outline-success" ng-click="DownloadMultipleImages()">Click here to download all</button>
            </form>
        </div>
    </nav>
    <div class="p-3 mb-2 bg-info text-dark border border-primary" ng-repeat="item in ListData">
        <div class="card border-dark w-100">
            <div class="card-body">
                <div class="row justify-content-start">
                    <div class="col-6">
                        <h5 class="card-title">Main Category:<b>{{item.BCL_MC_NAME}}</b></h5>
                        <p class="card-text">SubCategory:<b>{{item.BCL_SUB_NAME}}</b></p>
                        <a href="#" class="btn btn-primary" ng-click="DownloadSingleImg(item)">Download</a>
                    </div>
                    <div class="col-6">
                        <img ng-src="{{ URLData + item.File_Folder + '/' + item.BCLD_FILE_UPLD}}" class="rounded float-start" alt="{{item.BCLD_FILE_UPLD}}" style="height: 100vh;  width: 100vw;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%=ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../JS/Branch_Inspect_Images.js" defer></script>
</body>
</html>
