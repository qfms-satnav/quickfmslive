﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="BCLTypeLocMappingController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Type Location Role Validator/Approver Mapping</h3>
            </div>
            <div class="card">
                <div class="clearfix">
                    <div class="box-footer text-right">
                        <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                    </div>
                </div>
                <br />
                <form id="Form1" name="frmMapping" data-valid-submit="ChildCatAppMat()" novalidate>
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BCLTypeLocMapping.$submitted && BCLTypeLocMapping.BCL_TP_NAME.$invalid}">
                                <label class="control-label">Type</label>
                                <div isteven-multi-select data-input-model="MainType" data-output-model="BCLTypeLocMapping.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="GetButton()">
                                </div>
                                <input type="text" data-ng-model="BCLTypeLocMapping.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="BCLTypeLocMapping.$submitted && BCLTypeLocMapping.MainType.$invalid" style="color: red">Please select main type </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="BCLTypeLocMapping.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="BCLTypeLocMapping.Locations" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.LCM_NAME.$invalid" style="color: red">Please select location </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.Nums.$invalid}">
                                <label>No. of Level to Approve<span style="color: red;">*</span></label>
                                <br />
                                {{BCLTypeLocMapping.Nums}}
                            </div>
                        </div>
                    </div>

                    <br />
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table GridStyle">
                                <tr>
                                    <th>Approver Level</th>
                                    <th>Select Role </th>
                                    <th>Select Employees</th>
                                </tr>
                                <tr data-ng-repeat="column in Apprlst" data-ng-form="innerForm">
                                    <td>{{ $index +1}}</td>
                                    <td>
                                        <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.column.ROL_ID.$invalid}">
                                            <select class="form-control input-sm" data-ng-model="column.ROL_ID" name="ROL_ID" data-ng-disabled="$index +1==1?isDisabled:false" required="" data-ng-change="GetEmpOnRole(column,HDMList,$index +1)">
                                                <option id="Option2" value="">--Select--</option>
                                                <option data-ng-repeat="Lst in HDMList" data-ng-selected="column.ROL_ID==Lst.ROL_ID" value="{{Lst.ROL_ID}}">{{Lst.ROL_DESCRIPTION}}</option>
                                            </select>
                                        </div>
                                        <span class="error" data-ng-show="innerForm.column.ROL_ID.$error.pattern">Select All Levels</span>
                                    </td>
                                    <td>
                                        <div isteven-multi-select data-input-model="column.Emplst" data-output-model="column.OpEmplst" data-button-label="icon AUR_KNOWN_AS" data-item-label="icon AUR_KNOWN_AS"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="column.OpEmplst" name="AUR_KNOWN_AS" style="display: none" required="" />
                                        <span class="error" data-ng-show="innerForm.$submitted && innerForm.AUR_KNOWN_AS.$invalid" style="color: red">Select An Employee</span>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="clearfix">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="height: 320px">
                        <div class="col-md-12">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 80%;" class="ag-blue"></div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../BootStrapCSS/Scripts/angular-confirm.js" defer></script>
    <%--  <script src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js"></script>--%>
    <%--<script src="../../BootStrapCSS/qtip/qtip.js"></script>--%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "cp.ngConfirm"]);
        //var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "cp.ngConfirm"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../BranchCheckListManagement/JS/BCLTypeLocMapping.js" defer></script>
    <%--    <script src="../js/HDMUtility.js" defer></script>--%>
</body>
</html>






