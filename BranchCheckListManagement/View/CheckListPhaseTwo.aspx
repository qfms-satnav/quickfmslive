﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckListPhaseTwo.aspx.cs" Inherits="BranchCheckListManagement_View_CheckListPhaseTwo" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .package-table tbody tr:nth-child(odd) {
            background: #FAFAFA;
        }

        .package-table td {
            border-top: 0px !important;
            border-right: 1px solid #fff !important;
        }

        .package-table tbody tr:nth-child(even) {
            background: #DFDFDF;
        }

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .table-fixed thead {
            width: 97%;
        }

        .table-fixed tbody {
            height: 400px;
            overflow-y: auto;
            width: 100%;
        }

        .table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
            display: block;
        }

            .table-fixed tbody td, .table-fixed thead > tr > th {
                float: left;
                border-bottom-width: 0;
            }

        .table th {
            background-color: dimgray;
            color: white;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
        .category-wrapper {
            border: 1px solid grey;
            background: #007bff17;
            margin: 10px 0;
        }

        .subcat-wrapper {
            background: #f8f9fa;
            padding: 15px;
            box-shadow: 0px 10px 10px -8px #ccc;
            border-radius: 0 0 10px 10px;
            margin-bottom: 5px;
        }

        hr {
            border-top: 1px solid #007bff69;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .catName {
            margin-top: 25px;
            background: #2556ad5c;
            padding: 2px 10px;
            letter-spacing: 2px;
            border-radius: 10px 10px 0 0;
        }

        .subcatName {
            margin-top: 25px;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                startDate: 'today'
            });
        };

    </script>
</head>
<body data-ng-controller="CheckListPhaseTwoController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Verify Checklist
                                <button type="button" class="btn btn-primary custom-button-color" data-ng-click="GetDrafts(2)" data-toggle="modal" data-target="#myModal">Pending</button>
                </h3>
            </div>
            <div class="card">
                <form id="form2" name="frmChecklistTwo" data-valid-submit="Submit()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Type <span style="color: red;">*</span></label>
                                <label class="control-label"><span style="display: none;">{{CheckListPhaseTwo.BCL_ID}}</span></label>
                                <br />
                                {{CheckListPhaseTwo.TYPE_NAME}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">City <span style="color: red;">**</span></label>
                                <br />
                                {{CheckListPhaseTwo.CTY_NAME}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <br />
                                {{CheckListPhaseTwo.LCM_NAME}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Inspection By <span style="color: red;">*</span></label>
                                <br />
                                {{CheckListPhaseTwo.BCL_INSPECTED_BY}}
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Visit Date <span style="color: red;">*</span></label>
                                <br />
                                {{CheckListPhaseTwo.BCL_SELECTED_DT}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmChecklistTwo.$submitted && frmChecklistTwo.INSPECTOR.$invalid}">
                                <label class="control-label">Reviewed By <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Inspection" data-output-model="CheckListPhaseTwo.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckListPhaseTwo.Inspection" name="INSPECTOR" style="display: none" required="" />
                                <span class="error" data-ng-show="frmChecklistTwo.$submitted && frmChecklistTwo.INSPECTOR.$invalid" style="color: red">Please select inspection by </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmChecklistTwo.$submitted && frmChecklistTwo.SVR_FROM_DATE.$invalid}">
                                <label class="control-label">Reviewed Date <span style="color: red;">*</span></label>
                                <div class="input-group date" style="width: 150px" id='fromdate'>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="SVR_FROM_DATE" readonly name="SVR_FROM_DATE" ng-model="CheckListPhaseTwo.SVR_FROM_DATE" required  />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="frmChecklistTwo.$submitted && frmChecklistTwo.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div>
                            <%--<table class="table package-table">
                                <thead>
                                    <tr>
                                        <th class="col-md-3 col-sm-6 col-xs-12"><b>Category</b></th>
                                        <th class="col-md-3 col-sm-6 col-xs-12"><b>Subcategory</b></th>
                                        <th class="col-md-3 col-sm-6 col-xs-12"><b>Options</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr data-ng-repeat="(idx,column) in CheckListPhaseTwo.TableData track by $index">
                                        <td>
                                            <b>{{column.BCL_MC_NAME}}</b>
                                        </td>
                                        <td><b>{{column.BCL_SUB_NAME}}</b></td>
                                        <td>
                                            <div ng-if="column.BCL_SUB_TYPE=='Radio'" class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                        <label>
                                                            <input type="radio" data-ng-model="column.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}" disabled="disabled"> 
                                                            {{row.BCL_CH_NAME}}
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div ng-if="column.BCL_SUB_TYPE=='Multiple'" class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                        <label>
                                                            <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_NAME}}" value="{{row.BCL_CH_CODE}}" disabled="disabled">
                                                            {{row.BCL_CH_NAME}}
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div ng-if="column.BCL_SUB_TYPE=='Toggle'" class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                        <label>
                                                            <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_NAME}}" value="{{row.BCL_CH_CODE}}" disabled="disabled">
                                                            {{row.BCL_CH_NAME}}
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div ng-if="column.ISCOMMENTS=='Yes'" class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <label>
                                                            Inspection Comments: 
                                                                <textarea class="form-control" rows="5" data-ng-model="column.TEXT" name="{{column.BCL_SUB_CODE}}" ng-maxlength="100" disabled="disabled"></textarea>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div ng-if="column.ISDATE=='Yes'" class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <div class="form-group">
                                                            <label>Date: </label>
                                                            <div class="input-group date" id="{{column.BCL_SUB_CODE}}">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar" ng-click="DateClick(column.BCL_SUB_CODE)"></i>
                                                                </div>
                                                                 placeholder="dd-mm-yyyy"
                                                                <input class="form-control" data-ng-model="column.DATERESPONSE" data-ng-bind="column.DATERESPONSE| date:'MM/dd/yyyy'" name="{{column.BCL_SUB_CODE}}" type="text" placeholder="dd/mm/yyyy" disabled="disabled" />
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <label>
                                                            Validation Comments:
                                                                <textarea class="form-control" rows="5" data-ng-model="column.ValidateInchareComment" name="{{column.BCL_SUB_CODE}}" ng-maxlength="100"></textarea>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <label>
                                                            Validation Action: 
                                                                     <select data-ng-model="column.ValidateInchareAction" class="form-control">
                                                                         <option data-ng-repeat="sta in CheckListPhaseTwo.items track by $index" value="{{sta.BCL_MC_STATUS}}">{{sta.BCL_MC_STATUS}}</option>
                                                                     </select>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>--%>
                            <div data-ng-repeat="(idx,column) in CheckListPhaseTwo.TableData track by $index">
                                <div class="catName" data-ng-if="$index === 0 || CheckListPhaseTwo.TableData[$index - 1].BCL_MC_NAME !== column.BCL_MC_NAME">
                                    <b>{{column.BCL_MC_NAME}}</b>
                                </div>
                                <div class="subcat-wrapper">
                                    <span class="subcatName">{{column.BCL_SUB_NAME}}</span>
                                    <br>
                                    <hr>
                                    <div class="row">
                                        <div ng-if="column.BCL_SUB_TYPE=='Radio'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="form-check form-check-inline" data-ng-repeat="row in column.checklistDetails">
                                                    <label class="form-check-label" ng-if="column.RESPONSE==row.BCL_CH_NAME">
                                                        <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}" disabled="disabled">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                    <label class="form-check-label" ng-if="column.RESPONSE!=row.BCL_CH_NAME">
                                                        <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" name="{{row.BCL_CH_NAME}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}" disabled="disabled">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.BCL_SUB_TYPE=='Multiple'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                    <label>
                                                        <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}" disabled="disabled">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.BCL_SUB_TYPE=='Toggle'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                    <label>
                                                        <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}" disabled="disabled">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.ISDATE=='Yes'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <div class="form-group">
                                                        <label>Date: </label>
                                                        <div class="input-group date" id="{{column.BCL_SUB_CODE}}_1">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar" ng-click="DateClick(column.BCL_SUB_CODE)"></i>
                                                            </div>
                                                            <input class="form-control" data-ng-model="column.DATERESPONSE" data-ng-bind="column.DATERESPONSE| date:'MM/dd/yyyy'" name="{{column.BCL_SUB_CODE}}_1" type="text" placeholder="dd/mm/yyyy" ng-disabled="true" />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.ISCOMMENTS=='Yes'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <label>
                                                        Inspection Comments: 
                                                        <textarea class="form-control" rows="5" data-ng-model="column.TEXT" name="{{column.BCL_SUB_CODE}}" ng-maxlength="250" disabled="disabled"></textarea>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <label>
                                                        Validation Comments:
                                                                <textarea class="form-control" rows="5" data-ng-model="column.ValidateInchareComment" name="{{column.BCL_SUB_CODE}}" ng-maxlength="250"></textarea>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <label>
                                                        Validation Action: 
                                                                     <select data-ng-model="column.ValidateInchareAction" class="form-control">
                                                                         <option data-ng-repeat="sta in CheckListPhaseTwo.items track by $index" value="{{sta.BCL_MC_STATUS}}">{{sta.BCL_MC_STATUS}}</option>
                                                                     </select>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <br />
                            <br />
                    <div class="clearfix">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 210px;"></div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div id="dvbutton" class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                            <input type="submit" value="Submit" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="margin:-100px;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                        <div class="align-items-center justify-content-between">
                            <h5 class="modal-title" id="hsavedtitle">Validate Request Draft</h5>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body" style="overflow-x:scroll">
                    <div>
                        <div class="col-md-12">
                            <table class="table GridStyle">
                                <thead>
                                    <tr>
                                        <th><b>Location ID</b></th>
                                        <th><b>Type</b></th>
                                        <th><b>Sno</b></th>
                                        <th><b>Location Name</b></th>
                                        <th><b>Submitted Date</b></th>
                                    </tr>
                                </thead>
                                <tr data-ng-repeat="items in CheckListPhaseTwo.SaveList">
                                    <td>
                                        <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="LoadData(items)" data-bs-dismiss="modal"></a>
                                    </td>
                                    <td>
                                        <label data-ng-bind="items.BCL_TP_NAME" class="control-label" data-ng-model="items.BCL_TP_NAME">
                                        </label>
                                    </td>
                                    <td>
                                        <label data-ng-bind="items.BCL_ID" class="control-label" data-ng-model="items.BCL_ID">
                                        </label>
                                    </td>

                                    <td>
                                        <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                        </label>
                                    </td>

                                    <td>
                                        <label data-ng-bind="items.BCL_SELECTED_DT| date:'MM/dd/yyyy'" class="control-label" data-ng-model="items.BCL_SELECTED_DT">
                                        </label>
                                        <a ng-click="DeleteCheckList(items.BCL_ID,items.BCL_SUBMIT)" data-ng-if="items.BCL_SUBMIT==0"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                            </table>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary custom-button-color" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <%=ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/date.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "ui.date"]);
        var companyid = '<%= Session["TENANT"] %>';
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/CheckListPhaseTwo.js" defer></script>

</body>
</html>
