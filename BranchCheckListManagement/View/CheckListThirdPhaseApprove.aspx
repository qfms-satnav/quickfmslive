﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS" data-ng-cloak>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%--<link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />--%>
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

    <script type="text/javascript" defer>

        $(document).ready(function () {
            $("input[type=file]").click(function () {
                $(this).val("");
            });

            //$("input[type=file]").change(function () {
            //    alert($(this).val());
            //});
        });

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                maxDate: 'today'
            });
        };

        function OnlyNumeric(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }



    </script>
    <style>
        .col-xs-3 selected {
            border-color: blue;
        }

        .container {
            width: 432% !important;
        }

        .has-error2 {
            border-style: solid;
            border-color: #ff0000;
        }

        .has-error3 {
        }

        .mystyle {
            border-color: red !important;
            border-width: 2px !important;
        }



        .highlight {
            background-color: red;
        }

        input[type='radio'], label {
            margin: 10px;
        }

        .clearBoth {
            clear: both;
        }

        .Fonts {
            font-size: 14px;
        }

        input {
            height: 35px;
            font-size: 15px;
        }




        .table-bordered th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }

        .table-bordered td {
            border: 1px solid black;
            float: left;
        }

        /*th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }*/

        /*table, th, td {
            border: 1px solid black;
            float: left;
        }*/

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .list-inline {
            display: block;
        }

            .list-inline li {
                display: inline-block;
            }

                .list-inline li:after {
                    content: '|';
                    margin: 0 10px;
                }



        .form-ul {
        }

            .form-ul li {
                color: #6b6b6b;
                display: inline-block;
                margin: 3px 15px 3px 0px; /*white-space: nowrap; overflow: hidden;*/
                vertical-align: middle;
            }

                .form-ul li.li-width {
                    display: block;
                    margin-right: 0px;
                }

              /*   .vb-accordion .accordion-button.collapsed {
            width: 100%;
            background: #63bbb2;
            color: white;
            font-size: 16px;
        }

        .vb-accordion .accordion-button:not(.collapsed) {
            background: #63bbb2;
            color: white;
            font-size: 16px;
        }

        .vb-accordion .accordion-button::after {
            content: "";
            position: absolute;
            background-image: url(../../Userprofiles/down-arrow.png) !important;
            background-size: contain !important;
            background-repeat: no-repeat;
            right: 10px;
            width: 20px;
            height: 20px;
        }

        .vb-accordion .accordion-body {
            padding: 0;
        }

            .vb-accordion .accordion-body .col-md-12 {
                padding: 2rem;
            }

                .vb-accordion .accordion-body .col-md-12 .col-md-3 {
                    padding-left: 0px;
                }
                .accordion-collapse.collapse{padding:15px;}*/
    </style>
</head>
<body data-ng-controller="CheckListCreationController" class="amantra">    
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 46px;width: 1012px;margin-left: 19px;">
                            <h3 class="panel-title" style="height: 35px;font-weight: bold;color: #333;">Checklist Approval
                               <a href="#ex1" rel="modal:open" style="margin-left: 20px;color: #fff;" class="btn btn-primary custom-button-color" ng-click="GetDrafts(3)">Pending</a>
                                 <a href="#ex1" rel="modal:open" style="margin-left: 20px;display:none;color: #fff;" class="btn btn-primary custom-button-color" ng-click="GetDrafts(4)">Submitted</a>
                                <%--<a href="#" style="margin-left: 20px;color: #fff;" class="btn btn-primary custom-button-color" ng-click="AllClear()">Clear All</a>--%>
                                 <span style="color: red;margin-left: 23px;">*</span><span style="font-size:12px;font-weight:normal;">  Mandatory field</span> &nbsp; &nbsp;   
                                  <span style="color: red;">**</span>  <span style="font-size:12px;font-weight:normal;">Select to auto fill the data</span>
                            </h3>
                        </div>

                        <div class="panel-body" style="padding-right: 10px;">
                           <%-- <div class="clearfix">
                                <div class="box-footer text-right">

                                   
                               
                                </div>
                            </div>
                            <br />--%>
                            <form id="Form1" name="frmCheckList" data-valid-submit="Creation()" novalidate style="display:none;">
                                <div class="row" style="margin-left:-4px;">

                                    <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                        <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="CheckList.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.Country" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                        </div>
                                    </div>
                                     <%-- <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.BCL_TP_NAME.$invalid}">
                                                <label class="control-label">Type</label>
                                                <div isteven-multi-select data-input-model="MainType" data-output-model="CheckList.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" 
                                                     data-tick-property="ticked" data-max-labels="1" selection-mode="single" data-on-item-click="LoadData()">
                                                </div>
                                                <input type="text" data-ng-model="CheckList.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.MainType.$invalid" style="color: red">Please Select Main Type </span>
                                            </div>
                                        </div>--%>
                                     <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                             <label class="control-label">Type <span style="color: red;">*</span></label>
                                            <br />
                                             {{CheckList.TYPE_NAME}}
                                        </div>
                                    </div>
                                     <div class="col-md-4 col-sm-6 col-xs-12" style="display:none;">
                                        <div class="form-group">
                                             <label class="control-label">Type</label>
                                            <br />
                                             {{CheckList.TYPE_ID}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12" style="display:none;">
                                        <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.CTY_NAME.$invalid}">
                                            <label class="control-label">City <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="CheckList.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.City" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.CTY_NAME.$invalid" style="color: red">Please select city </span>

                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-6 col-xs-12" style="display:none;">
                                        <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Location" data-output-model="CheckList.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.Location" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid}">
                                            <label class="control-label">Approved By <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Inspection" data-output-model="CheckList.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                                data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.Inspection" name="INSPECTOR" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid" style="color: red">Please select inspection by  </span>
                                        </div>
                                    </div>

                                     
                               
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid}">
                                            <label class="control-label">Approved Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" style="width: 150px" id='fromdate'>
                                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="SVR_FROM_DATE" name="SVR_FROM_DATE" ng-model="CheckList.SVR_FROM_DATE" required />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                                        </div>
                                    </div>
                                    
                                  
                                </div>

                                <div class="row" style="margin-left:-4px;">
                                     <div class="col-md-2 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">City <span style="color: red;">**</span></label>
                                            <br />
                                             {{CheckList.CTY_NAME}}
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <br />
                                            {{CheckList.LCM_NAME}}
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Inspection by <span style="color: red;">*</span></label>
                                            <br />
                                            {{CheckList.BCL_INSPECTED_BY}}
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Visit date <span style="color: red;">*</span></label>
                                            <br />
                                            {{CheckList.BCL_SELECTED_DT}}
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Reviewed by <span style="color: red;">*</span></label>
                                            <br />
                                            {{CheckList.BCLD_REVIEWED_BY}}
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Reviewed date <span style="color: red;">*</span></label>
                                            <br />
                                            {{CheckList.BCLD_REVIEWED_DT}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12" style="display:none;">
                                        <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.CNP_NAME.$invalid}">
                                            <label class="control-label">Company<span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="CNP_NAME" data-output-model="CheckList.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                                item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.CNP_NAME.$invalid" style="color: red">Please select company </span>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <%-- <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-24">
                                        <div class="form-group">
                                            <ul class="list-inline">
                                                <li data-ng-repeat="dat in scorelist">{{dat.BCL_SCORE_CODE}} - {{dat.BCL_SCORE_NAME}}</li>

                                            </ul>
                                        </div>
                                    </div>
                                </div> --%>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <%-- <div id="tableDiv" style="margin-top: 1px;">
                                                Table will generate here.
                                            </div>--%>
                                            <div style="margin-top: -14px;">
                                                <%--Table will generate here.--%>
                                               
                                                <div class="container">
                                                    <div class="accordion vb-accordion" id="accordionExample">
                                                          <div class="accordion-item">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" id="example">
                                                           <%-- <thead>
                                                                <tr>
                                                                    <th class="col-md-3 col-sm-6 col-xs-12"><b>Category</b></th>
                                                                    <th class="col-md-3 col-sm-6 col-xs-12"><b>SubCategory</b></th>
                                                                    <th class="col-md-3 col-sm-6 col-xs-12"><b>Working Condition</b></th>
                                                                </tr>
                                                            </thead>--%>
                                                            <tbody data-ng-repeat="(pidx,items) in SelectedValues track by $index">
                                                                
                                                                <tr class="accordion-header" id="headingOne" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">   
                                                                <%--<tr class="accordion-header" id="items-{{$index}}"> style="background-color: #31b0d5;color: #fff;"
                                                                    <th class="col-md-3 col-sm-6 col-xs-12"><button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#item-{{$index}}" aria-expanded="true" aria-controls="item-{{$index}}">Category</button></th>--%>
                                                                     <th class="col-md-3 col-sm-6 col-xs-12"><b>Category</b></th>
                                                                    <th class="col-md-3 col-sm-6 col-xs-12"><b>Subcategory</b></th>
                                                                    <th class="col-md-3 col-sm-6 col-xs-12"><b>Options</b></th>
                                                                </tr>                                                          
                                                                <tr data-ng-repeat="item in TableDataArr track by $index" data-ng-if="items.SubcatCode==item.BCL_CH_SUB_CODE">
                                                                <%--<tr data-ng-repeat="item in TableDataArr track by $index" data-ng-if="items.SubcatCode==item.BCL_CH_SUB_CODE" id="item-{{$items.$index}}-{{$index}}" class="accordion-collapse collapse show" aria-labelledby="items-{{$index}}" data-bs-parent="#accordionExample">--%>
                                                                    <td class="col-xs-3 Fonts">{{item.BCL_MC_NAME}}</td>
                                                                    <td class="col-xs-3 Fonts">{{item.BCL_SUB_NAME}}</td>
                                                                    <td class="col-xs-6" data-ng-if="item.BCL_SUB_TYPE=='Radio'">
                                                                        <ul class="form-ul">
                                                                            <li data-ng-repeat="child in item.childArr">
                                                                                <label>
                                                                                    <input type="radio" name="{{item.BCL_SUB_NAME}}" value="{{child}}" data-ng-model="SelectedValues[pidx].ScoreCode" disabled="disabled"  />
                                                                                    {{child}}
                                                                               
                                                                                </label>
                                                                            </li>
                                                                            <%--  <li data-ng-if="item.BCL_SUB_FLAG_TYPE2=='Yes'">
                                                                                <textarea type="text" data-ng-model="SelectedValues[pidx].txtdata" data-ng-class="validationFn('V',pidx)?classActive:''"></textarea>
                                                                            </li>--%>
                                                                        </ul>
                                                                        <%--   <label data-ng-if="(item.BCL_SUB_NAME=='S&E Validity Period' || item.BCL_SUB_NAME=='Trade License Validity Period') && SelectedValues[pidx].ScoreCode=='Yes'">
                                                                           <input type="text" ui-date="{changeYear: true, changeMonth: true, yearRange: '-1:+360',minDate:1,dateFormat:'dd-MMM-yyyy'}"
                                                                                class="has-error3" name="{{item.BCL_SUB_NAME}}" data-ng-model="SelectedValues[pidx].Date"  placeholder="dd-mm-yyyy" />Validity
                                                                        </label>--%>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_TYPE1=='Yes'">
                                                                             <span>Upload File:</span>
                                                                            <%--<input type="file" select-ng-files data-ng-model="SelectedValues[pidx].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx" onchange="angular.element(this).scope().fileNameChanged(this,'F')">--%>
                                                                            <input type="file" select-ng-files data-ng-model="SelectedValues[pidx].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx" ng-change="fileNameChanged(this,'F','{{item.BCL_SUB_NAME}}')" style="float: right;">
                                                                        </label>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_TYPE2=='Yes'">
                                                                            <span>Inspection comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].txtdata" data-ng-class="validationFn('V',pidx)?classActive:''" disabled="disabled"></textarea>
                                                                             <br />
                                                                             <span>Validation comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].BCLD_ZF_COMENTS" data-ng-class="validationFn('V',pidx)?classActive:''" disabled="disabled"></textarea>
                                                                             <br />
                                                                             <span>Approval comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].BCLD_CENTRAL_TEAM_CMTS" data-ng-class="validationFn('V',pidx)?classActive:''"></textarea>
                                                                        </label>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_PLANTYPE=='Yes'">
                                                                             <span>Inspection date:</span>
                                                                            <input type="text" ui-date="{changeYear: true, changeMonth: true, yearRange: '-1:+360',minDate:1,dateFormat:'dd-MMM-yyyy'}"
                                                                                class="has-error3" name="Date+{{cidx}}" data-ng-model="SelectedValues[pidx].Date" placeholder="dd-mm-yyyy" disabled="disabled" />

                                                                        </label>
                                                                         <label>
                                                                            <span>Validation action:</span>                                                                               
                                                                              <select name="BCLD_ZF_ACTIONS" data-ng-model="SelectedValues[pidx].BCLD_ZF_ACTIONS" class="form-control" style="margin-left: 130px;margin-top: -25px;width: 189px;" disabled="disabled">
                                                                                      <option value="">Select validation action</option>
                                                                                      <option data-ng-repeat="sta in StaTypeLevelZ" value="{{sta.Id}}">{{sta.Name}}</option>
                                                                                  </select>
                                                                        </label>
                                                                         <label>
                                                                            <span>Approval action:</span>
                                                                                  <select name="BCLD_CENTRAL_ACTIONS" data-ng-model="SelectedValues[pidx].BCLD_CENTRAL_ACTIONS" class="form-control" style="margin-left: 130px;margin-top: -25px;width: 189px;">
                                                                                      <option value="">Select approval action</option>
                                                                                      <option data-ng-repeat="sta in StaTypeLevel" value="{{sta.Id}}">{{sta.Name}}</option>
                                                                                  </select>
                                                                        </label>
                                                                    </td>
                                                                    <%--<td class="col-xs-6" data-ng-if="item.BCL_SUB_TYPE=='File'">
                                                                        <input type="file" select-ng-files data-ng-model="SelectedValues[pidx].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx"  onchange="angular.element(this).scope().fileNameChanged(this,'F')">
                                                                        <label>Attachment</label>
                                                                    </td>--%>
                                                                    <td class="col-xs-6" data-ng-if="item.BCL_SUB_TYPE=='Text'">
                                                                        <ul>
                                                                            <li data-ng-repeat="(chidix,child) in item.childArr" data-ng-if="items.ScoreName==child">{{child}} :
                                                                                <textarea type="text" data-ng-model="SelectedValues[pidx].ScoreCode"></textarea>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                    <%--<input type="Date" data-ng-model="SelectedValues[pidx+cidx].Date" name="Date+{{cidx}}" style="width: 139px;" class="has-error3">--%>
                                                                    <td class="col-xs-6" data-ng-if="item.BCL_SUB_TYPE=='Multiple'">

                                                                         <%-- <ul class="list-group">
                                                                            <li class="list-group-item" data-ng-repeat="(cidx,child) in item.childArr" data-ng-if="items.ScoreName==child">--%>
                                                                        <ul class="form-ul">
                                                                            <li data-ng-repeat="child in item.childArr">
                                                                                <label>
                                                                                    <%--<input type='checkbox' name='{{item.BCL_SUB_NAME}}' checklist-value='{{child}}' checklist-model='SelectedValues[pidx].ScoreCode' data-ng-click='stateChanged(child)' />
                                                                                    {{child}}--%>
                                                                               <input type='checkbox' name='{{item.BCL_SUB_NAME}}' value='{{child}}' checklist-model='SelectedValues[pidx].ScoreCode' data-ng-click='stateChanged(child,item.BCL_SUB_NAME)' disabled="disabled" />
                                                                                    {{child}}
                                                                                </label>
                                                                                <%-- <label>
                                                                                    {{child}}<textarea type="text" name="{{item.BCL_SUB_NAME}}" value="{{child}}" data-ng-model="SelectedValues[pidx].ScoreCode" style="width: 50px;"></textarea>
                                                                                </label>--%>
                                                                               <%-- <label>
                                                                                    Expiry Date :
                                                                                    <input type="text" ui-date="{changeYear: true, changeMonth: true, yearRange: '-1:+360',minDate:1,dateFormat:'dd-MMM-yyyy'}"
                                                                                class="has-error3" name="Date+{{cidx}}" data-ng-model="SelectedValues[pidx].Date"  placeholder="dd-mm-yyyy" />                                                                                   
                                                                                </label>

                                                                                <label>
                                                                                    <input type="radio" name="{{child}}" value="Green" data-ng-model="SelectedValues[pidx].SubScore" class="has-error3" />Green
                                                                                </label>
                                                                                <label>
                                                                                    <input type="radio" name="{{child}}" value="Red" data-ng-model="SelectedValues[pidx].SubScore" class="has-error3" />Red
                                                                                </label>--%>
                                                                            </li>
                                                                        </ul>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_TYPE1=='Yes'" style="color: #fff;">
                                                                             <span>Upload file:</span>
                                                                            <%--<input type="file" select-ng-files data-ng-model="SelectedValues[pidx].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx" onchange="angular.element(this).scope().fileNameChanged(this,'F')">--%>
                                                                            <input type="file" select-ng-files data-ng-model="SelectedValues[pidx].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx" ng-change="fileNameChanged(this,'F','{{item.BCL_SUB_NAME}}')" style="float: right;">
                                                                        </label>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_TYPE2=='Yes'">
                                                                            <span>Inspection comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].txtdata" data-ng-class="validationFn('V',pidx)?classActive:''" disabled="disabled"></textarea>
                                                                             <br />
                                                                             <span>Validation comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].BCLD_ZF_COMENTS" data-ng-class="validationFn('V',pidx)?classActive:''" disabled="disabled"></textarea>
                                                                             <br />
                                                                             <span>Approval comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].BCLD_CENTRAL_TEAM_CMTS" data-ng-class="validationFn('V',pidx)?classActive:''"></textarea>
                                                                        </label>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_PLANTYPE=='Yes'">
                                                                             <span>Inspection date:</span>
                                                                            <input type="text" ui-date="{changeYear: true, changeMonth: true, yearRange: '-1:+360',minDate:1,dateFormat:'dd-MMM-yyyy'}"
                                                                                class="has-error3" name="Date+{{cidx}}" data-ng-model="SelectedValues[pidx].Date" placeholder="dd-mm-yyyy" disabled="disabled" />

                                                                        </label>
                                                                         <label>
                                                                            <span>Validation action:</span>
                                                                              <select name="BCLD_ZF_ACTIONS" data-ng-model="SelectedValues[pidx].BCLD_ZF_ACTIONS" class="form-control" style="margin-left: 130px;margin-top: -25px;width: 189px;" disabled="disabled">
                                                                                      <option value="">Select validation action</option>
                                                                                      <option data-ng-repeat="sta in StaTypeLevelZ" value="{{sta.Id}}">{{sta.Name}}</option>
                                                                                  </select>
                                                                        </label>
                                                                         <label>
                                                                            <span>Approval action:</span>
                                                                                  <select name="BCLD_CENTRAL_ACTIONS" data-ng-model="SelectedValues[pidx].BCLD_CENTRAL_ACTIONS" class="form-control" style="margin-left: 130px;margin-top: -25px;width: 189px;">
                                                                                      <option value="">Select approval action</option>
                                                                                      <option data-ng-repeat="sta in StaTypeLevel" value="{{sta.Id}}">{{sta.Name}}</option>
                                                                                  </select>
                                                                        </label>
                                                                    </td>

                                                                    <td class="col-xs-6" data-ng-if="item.BCL_SUB_TYPE=='Toggle'">
                                                                        <ul class="form-ul">
                                                                            <li data-ng-repeat="child in item.childArr">
                                                                                <label>
                                                                                    <input type="radio" name="{{item.BCL_SUB_NAME}}" value="{{child}}" data-ng-model="SelectedValues[pidx].ScoreCode" disabled="disabled" />
                                                                                    {{child}}
                                                                               <%-- <input type="checkbox" name="{{item.BCL_SUB_NAME}}" data-ng-model="SelectedValues[pidx].ScoreCode" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="{{child}}" data-off="{{child}}">--%>
                                                                                </label>
                                                                            </li>                                                                           
                                                                        </ul>                                                                        
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_TYPE1=='Yes'" style="color: #fff;">    
                                                                             <span>Upload file:</span>
                                                                            <input type="file" select-ng-files data-ng-model="SelectedValues[pidx].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx" ng-change="fileNameChanged(this,'F','{{item.BCL_SUB_NAME}}')" style="float: right;">
                                                                        </label>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_TYPE2=='Yes'">
                                                                            <span>Inspection comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].txtdata" data-ng-class="validationFn('V',pidx)?classActive:''" disabled="disabled"></textarea>
                                                                             <br />
                                                                             <span>Validation comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].BCLD_ZF_COMENTS" data-ng-class="validationFn('V',pidx)?classActive:''" disabled="disabled"></textarea>
                                                                             <br />
                                                                             <span>Approval comments:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                            <textarea type="text" data-ng-model="SelectedValues[pidx].BCLD_CENTRAL_TEAM_CMTS" data-ng-class="validationFn('V',pidx)?classActive:''"></textarea>
                                                                        </label>
                                                                        <label data-ng-if="item.BCL_SUB_FLAG_PLANTYPE=='Yes'">
                                                                            <span>Inspection date:</span>
                                                                            <input type="text" ui-date="{changeYear: true, changeMonth: true, yearRange: '-1:+360',minDate:1,dateFormat:'dd-MMM-yyyy'}"
                                                                                class="has-error3" name="Date+{{cidx}}" data-ng-model="SelectedValues[pidx].Date" placeholder="dd-mm-yyyy" disabled="disabled" />

                                                                        </label>
                                                                         <label>
                                                                            <span>Validation action:</span>
                                                                              <select name="BCLD_ZF_ACTIONS" data-ng-model="SelectedValues[pidx].BCLD_ZF_ACTIONS" class="form-control" style="margin-left: 130px;margin-top: -25px;width: 189px;" disabled="disabled">
                                                                                      <option value="">Select validation action</option>
                                                                                      <option data-ng-repeat="sta in StaTypeLevelZ" value="{{sta.Id}}">{{sta.Name}}</option>
                                                                                  </select>                                                                            
                                                                        </label>
                                                                          <label>
                                                                            <span>Approval action:</span>
                                                                                  <select name="BCLD_CENTRAL_ACTIONS" data-ng-model="SelectedValues[pidx].BCLD_CENTRAL_ACTIONS" class="form-control" style="margin-left: 130px;margin-top: -25px;width: 189px;">
                                                                                      <option value="">Select approval action</option>
                                                                                      <option data-ng-repeat="sta in StaTypeLevel" value="{{sta.Id}}">{{sta.Name}}</option>
                                                                                  </select>
                                                                        </label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                              </div>
                                                     </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 210px;"></div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div style="margin-top: 0px;">
                                                <label for="txtcode" class="custom-file"><strong>Overall comments</strong></label>
                                                <textarea name="OVERALL_CMTS" id="OVERALL_CMTS" class="form-control" data-ng-model="CheckList.OVERALL_CMTS" cols="40" rows="5" style="width:294px;height:177px;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                        <label for="txtcode" class="custom-file">Upload images/document<a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                        <input multiple type="file" name="UPLFILE" data-ng-model="CheckList.UPLFILE[0]" id="UPLFILE" accept=".png,.jpg,.jpeg" class="custom-file-input" onchange="angular.element(this).scope().fileNameChanged(this)">
                                        <span class="custom-file-control"></span>
                                    </div>
                                </div>
                                <div id="dvbutton" class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                                    <input type="button" value="Save as Draft" class="btn btn-primary custom-button-color" data-ng-click="Save()" style="display :none"  />
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="Submit()" />
                                </div>

                               <%-- <div class="clearfix">
                                    <div class="col-md-12">
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 47%"></div>
                                    </div>
                                </div>--%>

                                

                            </form>
                            <div id="ex1" class="modal">

                                    <div class="clearfix">
                                        <div class="col-md-12">
                                            <h4 id="hsavedtitle">Saved items</h4>
                                        </div>
                                        <div class="col-md-12 table-responsive">
                                            <table class="table table-bordered">

                                               <tr>
                                                   <td style="width: 70px;">LCM ID
                                                    </td>
                                                    <td style="width: 70px;">INSP ID
                                                    </td>
                                                    <td style="width: 120px">LCM Name
                                                    </td>
                                                    <td style="width: 120px">INSP Date
                                                    </td>
                                                </tr>
                                                 <tr data-ng-repeat="items in CheckList.SaveList">
                                                    <td style="width: 70px; height: 55px;">
                                                        <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="GetLocationCode(items)" rel="modal:close"></a>
                                                    </td>

                                                      <td style="width: 70px; height: 55px;">
                                                        <label data-ng-bind="items.BCL_ID" class="control-label" data-ng-model="items.BCL_ID">
                                                        </label>
                                                    </td>

                                                    <td style="width: 120px; height: 55px;">
                                                        <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                                        </label>
                                                    </td>

                                                    <td style="width : 120px; height: 55px;">
                                                        <label data-ng-bind="items.BCL_SELECTED_DT| date:'MM/dd/yyyy'" class="control-label" data-ng-model="items.BCL_SELECTED_DT">
                                                        </label>
                                                        <a ng-click="DeleteCheckList(items.BCL_ID,items.BCL_SUBMIT)" data-ng-if="items.BCL_SUBMIT==1"><i class="fa fa-trash"></i></a>
                                                    </td>

                                                </tr>

                                                <%--<thead>
                                                    <tr>
                                                        <th>Location Code</th>
                                                        <th>Location Name</th>
                                                    </tr>
                                                </thead>--%>
                                                <%--<tbody>
                                                    <tr data-ng-repeat="items in CheckList.SaveList">
                                                        <td>
                                                            <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="GetLocationCode(items.LCM_CODE)"></a>
                                                        </td>
                                                        <td>
                                                            <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </tbody>--%>
                                            </table>
                                        </div>
                                    </div>
                                    <a href="#" rel="modal:close">Close</a>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
        <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
        <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
        <script src="../../Scripts/moment.min.js" defer></script>
        <script src="../../Scripts/date.js"></script>
        <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
        <%--<script src="../../BootStrapCSS/Scripts/UnderScoreJs.js" defer></script>--%>

        <script defer>
            var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "ui.date"]);
            var companyid = '<%= Session["TENANT"] %>';
        </script>
        <script src="../../SMViews/Utility.js" defer></script>
        <script src="../JS/CheckListThirdPhaseApprove.js" defer></script>
</body>
</html>
