﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .txt-danger {
            color: red !important;
        }
    </style>
</head>
<body data-ng-controller="BCLRoleMappingController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Checklist Role Mapping</h3>
            </div>
            <div class="card">
                <form id="form1" name="BCLRoleMap" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BCLRoleMap.$submitted && BCLRoleMap.ROL_ACRONYM.$invalid}">
                                <label for="txtcode">Roles<span style="color: red;"> *</span></label>
                                <div>
                                    <isteven-multi-select data-input-model="Rolelist" data-output-model="BclRole.selectedRol" data-button-label="icon ROL_ACRONYM" data-item-label="icon ROL_ACRONYM maker"
                                        data-on-item-click="RolChanged()" data-on-select-all="RolSelectAll()" data-on-select-none="RolSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </isteven-multi-select>
                                </div>
                                <input type="text" data-ng-model="BclRole.selectedRol" name="ROL_ACRONYM" style="display: none" required="" />
                                <span class="error" data-ng-show="BCLRoleMap.$submitted && BCLRoleMap.ROL_ACRONYM.$invalid" style="color: red;">Please select Role </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BCLRoleMap.$submitted && BCLRoleMap.AUR_KNOWN_AS.$invalid}">
                                <label for="txtcode">Users<span style="color: red;"> *</span></label>
                                <div>
                                    <isteven-multi-select data-input-model="Userlist" data-output-model="BclRole.selectedUsr" data-button-label="icon AUR_KNOWN_AS" data-item-label="icon AUR_KNOWN_AS maker"
                                        data-on-item-click="UsrChanged()" data-on-select-all="UsrSelectAll()" data-on-select-none="UsrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </isteven-multi-select>
                                </div>
                                <input type="text" data-ng-model="BclRole.selectedUsr" name="AUR_KNOWN_AS" style="display: none" required="" />
                                <span class="error" data-ng-show="BCLRoleMap.$submitted && BCLRoleMap.AUR_KNOWN_AS.$invalid" style="color: red;">Please select Users </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BCLRoleMap.$submitted && BCLRoleMap.BCL_TP_NAME.$invalid}">
                                <label for="txtcode">Checklist Types<span style="color: red;"> *</span></label>
                                <div>
                                    <isteven-multi-select data-input-model="BCLlist" data-output-model="BclRole.selectedBCL" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME maker"
                                        data-on-item-click="BCLChanged()" data-on-select-all="BCLSelectAll()" data-on-select-none="BCLSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </isteven-multi-select>
                                </div>
                                <input type="text" data-ng-model="BclRole.selectedBCL" name="BCL_TP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="BCLRoleMap.$submitted && BCLRoleMap.BCL_TP_NAME.$invalid" style="color: red;">Please select Checklists </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BCLRoleMap.$submitted && BCLRoleMap.LCM_NAME.$invalid}">
                                <label for="txtcode">Location<span style="color: red;"> *</span></label>
                                <div>
                                    <isteven-multi-select data-input-model="Loctionlist" data-output-model="BclRole.selectedLCM" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                        data-on-item-click="LCMChanged()" data-on-select-all="LCMSelectAll()" data-on-select-none="LCMSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </isteven-multi-select>
                                </div>
                                <input type="text" data-ng-model="BclRole.selectedLCM" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="BCLRoleMap.$submitted && BCLRoleMap.LCM_NAME.$invalid" style="color: red;">Please select Checklists </span>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12"></div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="margin-top: 10px; text-align: end;">
                            <input type="submit" value="Submit" id="btnSubmit" class="btn btn-primary custom-button-color" data-ng-click="SubmitData()" />
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12" style="margin-top: 25px;">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%; margin-bottom: 5px;" />
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../JS/BCLRoleMapping.js"></script>
</body>
</html>
