﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-blue .ag-row-odd {
            background-color: #fff;
        }

        .cell-fail {
            text-align: left;
            font-weight: bold;
            background-color: red;
        }

        .cell-pass {
            text-align: left;
            font-weight: bold;
            background-color: #4caf50;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BCLMainSubCategoryController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3>Subcategory Master</h3>
        </div>
        <div class="card">
            <form role="form" id="form1" name="BCLMainSubCategory" data-valid-submit="Save()" novalidate>

                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': BCLMainSubCategory.$submitted && BCLMainSubCategory.BCL_TP_NAME.$invalid}">
                            <label class="control-label">Type</label>
                            <div isteven-multi-select data-input-model="MainType" data-output-model="BCLMainSubCategory.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="GetMainCategory()">
                            </div>
                            <input type="text" data-ng-model="BCLMainSubCategory.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="BCLMainSubCategory.$submitted && BCLMainSubCategory.MainType.$invalid" style="color: red">Please Select Main Type </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': BCLMainSubCategory.$submitted && BCLMainSubCategory.BCL_MC_NAME.$invalid}">
                            <label class="control-label">Category</label>
                            <div isteven-multi-select data-input-model="Category" data-output-model="BCLMainSubCategory.Category" data-button-label="icon BCL_MC_NAME" data-item-label="icon BCL_MC_NAME" selection-mode="single"
                                data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="SearchA()">
                            </div>
                            <input type="text" data-ng-model="BCLMainSubCategory.Category[0]" name="BCL_MC_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="BCLMainSubCategory.$submitted && BCLMainSubCategory.Category.$invalid" style="color: red">Please Select Main Category </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Subcategory Name <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
<%--                                <input id="BCL_SUB_NAME" class="form-control" type="text" name="BCL_SUB_NAME" maxlength="50" data-ng-model="BCLMainSubCategory.BCL_SUB_NAME" />--%>
                                  <textarea id="BCL_SUB_NAME" class="form-control" name="BCL_SUB_NAME" maxlength="50000" data-ng-model="BCLMainSubCategory.BCL_SUB_NAME" rows="4"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Subcategory Remarks <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <textarea id="BCL_SUB_REM" data-ng-model="BCLMainSubCategory.BCL_SUB_REM" class="form-control" maxlength="500"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" style="display: none">
                        {{BCLMainSubCategory.BCL_MC_ID}}
                                 
                    </div>
                    <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="row" data-ng-show="ActionStatus==1">
                                            <label class="control-label">Status <span style="color: red;">*</span></label>
                                            <div>
                                                <select id="Select1" name="MNC_Status_Id" data-ng-model="MainCategory.MNC_Status_Id" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                    <div class="form-group" data-ng-class="{'has-error': BCLMainSubCategory.$submitted && BCLMainSubCategory.HDM_MAIN_MOD_NAME.$invalid}">
                                        <label class="control-label">Module <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="MainCategory.Company" data-button-label="icon HDM_MAIN_MOD_NAME"
                                            data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="MainCategory.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BCLMainSubCategory.$submitted && BCLMainSubCategory.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select COMPANY</span>
                                    </div>
                                </div>--%>
                </div>

                <div class="row">
                    <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Sub Type <span style="color: red;"></span></label>
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                           <input id="BCL_SUB_TYPE" class="form-control" type="text" name="BCL_SUB_TYPE" maxlength="50" data-ng-model="BCLMainSubCategory.BCL_SUB_TYPE" />
                                        </div>
                                    </div>
                                </div>--%>

                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Add File<span style="color: red;">*</span></label>
                                <div>
                                    <input id="BCL_SUB_FLAG_TYPE1" data-ng-model="BCLMainSubCategory.BCL_SUB_FLAG_TYPE1" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
                                    <%-- <select id="Select2" name="BCL_SUB_FLAG_TYPE1" data-ng-model="BCLMainSubCategory.BCL_SUB_FLAG_TYPE1" class="form-control">
                                                     <option value="">Select Is File Exist</option>
                                                     <option data-ng-repeat="sta in StaflagSubType1" value="{{sta.Id}}">{{sta.Name}}</option>                                                       
                                                </select>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Add Comment<span style="color: red;">*</span></label>
                                <div>
                                    <input id="BCL_SUB_FLAG_TYPE2" data-ng-model="BCLMainSubCategory.BCL_SUB_FLAG_TYPE2" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
                                    <%--  <select id="Select3" name="BCL_SUB_FLAG_TYPE2" data-ng-model="BCLMainSubCategory.BCL_SUB_FLAG_TYPE2" class="form-control">
                                                    <option value="">Select Is Text Exist</option>
                                                     <option data-ng-repeat="sta in StaflagSubType2" value="{{sta.Id}}">{{sta.Name}}</option>                                                       
                                                </select>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Pick Date<span style="color: red;">*</span></label>
                                <div>
                                    <input id="BCL_SUB_FLAG_PLANTYPE" data-ng-model="BCLMainSubCategory.BCL_SUB_FLAG_PLANTYPE" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
                                    <%-- <select id="Select4" name="BCL_SUB_FLAG_PLANTYPE" data-ng-model="BCLMainSubCategory.BCL_SUB_FLAG_PLANTYPE" class="form-control">
                                                     <option value="">Select Is Date Exist</option>
                                                     <option data-ng-repeat="sta in StaflagSubType3" value="{{sta.Id}}">{{sta.Name}}</option>                                                       
                                                </select>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Check Sub Type <span style="color: red;">*</span></label>
                                <div>
                                    <select id="Select1" name="BCL_SUB_TYPE" data-ng-model="BCLMainSubCategory.BCL_SUB_TYPE" class="form-control" data-ng-change="fn_check()">
                                        <option value="">Select Sub Type</option>
                                        <option data-ng-repeat="sta in StaSubType" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" id="disp" style="display: none;">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Add No of Subtype <span style="color: red;">*</span></label>
                                <div>
                                    <select id="Select5" name="BCL_SUB_TYPE_NO" data-ng-model="BCLMainSubCategory.BCL_SUB_TYPE_NO" class="form-control" data-ng-change="fn_Set()">
                                        <option value="">Add No of Subtype</option>
                                        <option data-ng-repeat="sta in StaflagSubType4" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="dvScore" style="display: none;">
                </div>
                <div class="row" id="dvScoreEdit" style="display: none;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div data-ag-grid="gridOptions2" class="ag-blue" style="height: 210px; width: auto"></div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-left: 30px;">
                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                    <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../BranchCheckListManagement/JS/BCLMainSubCategory.js" defer></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
</body>
</html>
