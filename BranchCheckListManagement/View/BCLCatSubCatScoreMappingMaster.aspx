﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BCLCatSubCatScoreMappingController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <div ba-panel ba-panel-title="Main Category" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
                <div class="panel">
                    <div class="panel-heading" style="height: 41px;">
                        <h3 class="panel-title">Checklist score </h3>
                    </div>
                    <div class="panel-body" style="padding-right: 50px;">
                        <form role="form" id="form1" name="BCLCatSubCatScoreMapping" data-valid-submit="Save()" novalidate>

                            <div class="row">
                             
                                   <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.BCL_TP_NAME.$invalid}">
                                                <label class="control-label">Checklist Type</label>
                                                <div isteven-multi-select data-input-model="MainType" data-output-model="BCLCatSubCatScoreMapping.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="GetMainCategory(1)">
                                                </div>
                                                <input type="text" data-ng-model="BCLCatSubCatScoreMapping.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.MainType.$invalid" style="color: red">Please select Checklist Type</span>
                                            </div>
                                        </div>
                                 <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.BCL_MC_NAME.$invalid}">
                                                <label class="control-label">Checklist Category</label>
                                                <div isteven-multi-select data-input-model="Category" data-output-model="BCLCatSubCatScoreMapping.Category" data-button-label="icon BCL_MC_NAME" data-item-label="icon BCL_MC_NAME" selection-mode="single"
                                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="GetSubCategory(1)">
                                                </div>
                                                <input type="text" data-ng-model="BCLCatSubCatScoreMapping.Category[0]" name="BCL_MC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.Category.$invalid" style="color: red">Please select Checklist Category </span>
                                            </div>
                                        </div>
                                 <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.BCL_SUB_NAME.$invalid}">
                                                <label class="control-label">Checklist Subcategory</label>
                                                <div isteven-multi-select data-input-model="SubCategory" data-output-model="BCLCatSubCatScoreMapping.SubCategory" data-button-label="icon BCL_SUB_NAME" data-item-label="icon BCL_SUB_NAME" selection-mode="single"
                                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="SearchA()">
                                                </div>
                                                <input type="text" data-ng-model="BCLCatSubCatScoreMapping.SubCategory[0]" name="BCL_SUB_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.SubCategory.$invalid" style="color: red">Please select Checklist Subcategory</span>
                                            </div>
                                        </div>
                                 <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Checklist Score Name <span style="color: red;"></span></label>
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                           <input id="BCL_CH_NAME" class="form-control" type="text" name="BCL_CH_NAME" maxlength="50" data-ng-model="BCLCatSubCatScoreMapping.BCL_CH_NAME" />
                                        </div>
                                    </div>
                                </div>

                                
                                  <div class="col-md-3 col-sm-6 col-xs-12" style="display:none">
                                     
                                 {{BCLCatSubCatScoreMapping.BCL_MC_ID}}
                                 
                                 </div>
                               <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="row" data-ng-show="ActionStatus==1">
                                            <label class="control-label">Status <span style="color: red;">*</span></label>
                                            <div>
                                                <select id="Select1" name="MNC_Status_Id" data-ng-model="MainCategory.MNC_Status_Id" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                    <div class="form-group" data-ng-class="{'has-error': BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.HDM_MAIN_MOD_NAME.$invalid}">
                                        <label class="control-label">Module <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="MainCategory.Company" data-button-label="icon HDM_MAIN_MOD_NAME"
                                            data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="MainCategory.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BCLCatSubCatScoreMapping.$submitted && BCLCatSubCatScoreMapping.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select COMPANY</span>
                                    </div>
                                </div>--%>
                            </div>

                            <div class="row">  
                                <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Sub Type <span style="color: red;"></span></label>
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                           <input id="BCL_SUB_TYPE" class="form-control" type="text" name="BCL_SUB_TYPE" maxlength="50" data-ng-model="BCLCatSubCatScoreMapping.BCL_SUB_TYPE" />
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Checklist Score Remarks <span style="color: red;"></span></label>
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="BCL_CH_REMARKS" data-ng-model="BCLCatSubCatScoreMapping.BCL_CH_REMARKS" class="form-control" maxlength="500"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="control-label">Subtype<span style="color: red;">*</span></label>
                                            <div>
                                                <select id="Select1" name="BCL_CH_TYPE" data-ng-model="BCLCatSubCatScoreMapping.BCL_CH_TYPE" class="form-control">
                                                    <option data-ng-repeat="sta in StaSubType" value="{{sta.Id}}">{{sta.Name}}</option>                                                   
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                        <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 30px;">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    
    <script defer>

        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        

    </script>


        <script src="../../../SMViews/Utility.min.js" defer></script>
<script src="../../../BranchCheckListManagement/JS/BCLCatSubCatScoreMapping.js" defer></script>
</body>
</html>
