﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        $scope.StaTypeLevel1 = [
            { BCL_GPS_STATUS: '1', BCL_GPS_NAME: 'Active' },
            { BCL_GPS_STATUS: '2', BCL_GPS_NAME: 'Inactive' }
        ];

    </script>
</head>
<body data-ng-controller="BCLMainTypeController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3>Checklist Type</h3>
        </div>
        <div class="card">
            <form role="form" id="form1" name="BCLMainType" data-valid-submit="Save()" novalidate>
                <div class="row">
                    <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Main Type Name<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': BCLMainType.$submitted && BCLMainType.BCL_TP_NAME.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                            <input id="BCL_TP_NAME" type="text" name="BCL_TP_NAME" maxlength="50" data-ng-model="BCLMainType.BCL_TP_NAME" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="BCLMainType.$submitted && BCLMainType.BCL_TP_NAME.$invalid" style="color: red">Please enter valid Name </span>
                                        </div>
                                    </div>
                                </div>--%>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Check Type Name <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <input id="BCL_TP_NAME" class="form-control" type="text" name="BCL_TP_NAME" maxlength="50" data-ng-model="BCLMainType.BCL_TP_NAME" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Check Type Remarks <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <textarea id="txtrem" data-ng-model="BCLMainType.BCL_TP_REMARKS" class="form-control" maxlength="500"></textarea>
                            </div>
                        </div>
                    </div>
                   <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Checklist Approval Level <span style="color: red;">*</span></label>
                            <div>
                                <select id="BCL_TP_LEVEL" name="BCL_TP_LEVEL" data-ng-model="BCLMainType.BCL_TP_LEVEL" class="form-control selectpicker">
                                    <option value="">Select Checklist Approval Level</option>
                                    <option data-ng-repeat="sta in StaTypeLevel" value="{{sta.Id}}">{{sta.Name}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                   <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Checklist GPS Status<span style="color: red;">*</span></label>
                                <!-- Validation for required field -->
                                <div class="col-md-12">
                                    <select
                                        id="gpsstatus" class="form-control selectpicker" ng-model="gpsStatus" ng-options="status.value as status.label for status in gpsStatusOptions"
                                        ng-init="gpsStatus='--Select--'" required title="Select Status">
                                    </select>
                                    <span class="text-danger" ng-show="!gpsStatus || gpsStatus === '--Select--'">Please Select GPS Status
                                    </span>
                                </div>
                            </div>
                        </div>--%>
                      <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Do You Want GPS? <span style="color: red;">*</span></label>
                            <div>
                                <select id="BCL_TP_GPS" name="BCL_TP_GPS" data-ng-model="BCLMainType.BCL_TP_GPS" class="form-control">
                                    <option value="">Nothing Selected</option>
                                    <option data-ng-repeat="sta in StaTypeLevel1" value="{{sta.BCL_GPS_STATUS}}">{{sta.BCL_GPS_NAME}}</option>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                   

                    <div class="col-md-3 col-sm-6 col-xs-12" style="display: none">
                        {{SubCatLocMap.BCL_TP_ID}}
                                 
                    </div>
                    <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="row" data-ng-show="ActionStatus==1">
                                            <label class="control-label">Status <span style="color: red;">*</span></label>
                                            <div>
                                                <select id="Select1" name="MNC_Status_Id" data-ng-model="MainCategory.MNC_Status_Id" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                    <div class="form-group" data-ng-class="{'has-error': BCLMainType.$submitted && BCLMainType.HDM_MAIN_MOD_NAME.$invalid}">
                                        <label class="control-label">Module <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="MainCategory.Company" data-button-label="icon HDM_MAIN_MOD_NAME"
                                            data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="MainCategory.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BCLMainType.$submitted && BCLMainType.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select COMPANY</span>
                                    </div>
                                </div>--%>
                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" style="height: 250px;" class="ag-blue"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../BranchCheckListManagement/JS/BCLMainType.js" defer></script>
</body>
</html>
