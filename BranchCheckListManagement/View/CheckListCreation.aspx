﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS" data-ng-cloak>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
   <style>
        .col-xs-3 selected {
            border-color: blue;
        }

        .container {
            width: 432% !important;
        }

        .has-error2 {
            border-style: solid;
            border-color: #ff0000;
        }

        .has-error3 {
        }

        .mystyle {
            border-color: red !important;
            border-width: 2px !important;
        }



        .highlight {
            background-color: red;
        }

        input[type='radio'], label {
            margin: 10px;
        }

        .clearBoth {
            clear: both;
        }

        .Fonts {
            font-size: 14px;
        }

        input {
            height: 35px;
            font-size: 15px;
        }




        .table-bordered th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }

        .table-bordered td {
            border: 1px solid black;
            float: left;
        }
        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .list-inline {
            display: block;
        }

            .list-inline li {
                display: inline-block;
            }

                .list-inline li:after {
                    content: '|';
                    margin: 0 10px;
                }



        .form-ul {
        }

            .form-ul li {
                color: #6b6b6b;
                display: inline-block;
                margin: 3px 15px 3px 0px; /*white-space: nowrap; overflow: hidden;*/
                vertical-align: middle;
            }

                .form-ul li.li-width {
                    display: block;
                    margin-right: 0px;
                }

        
    </style>
</head>
<body data-ng-controller="BCLMainCategoryController" class="amantra">

    <div class="al-content">
        <div class="widgets">
            <div ba-panel ba-panel-title="Help Desk Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
                <div class="panel">
                    <div class="panel-heading" style="height: 41px;">
                        <h3 class="panel-title">Checklist </h3>
                    </div>
                    <div class="panel-body" style="padding-right: 50px;">
                        <form role="form" id="form1" name="BCLMainCategory" novalidate>
                             <div class="row">
                             
                                   <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': BCLMainCategory.$submitted && BCLMainCategory.BCL_TP_NAME.$invalid}">
                                                <label class="control-label">Type</label>
                                                <div isteven-multi-select data-input-model="MainType" data-output-model="BCLMainCategory.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="GetButton()">
                                                </div>
                                                <input type="text" data-ng-model="BCLMainCategory.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="BCLMainCategory.$submitted && BCLMainCategory.MainType.$invalid" style="color: red">Please select main type </span>
                                            </div>
                                        </div>
                                 </div>
                            <div class="box-body">                               
                                 <div class="clearfix">                                    

                                  <div class="col-md-4 col-sm-12 col-xs-12">
        <%-- <asp:HyperLink ID="HyperLink15" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/CheckListFirstPhaseApprove.aspx">Check List Creation</asp:HyperLink>--%>
                                       <a href="/BranchCheckListManagement/View/CheckListFirstPhaseApprove.aspx" class="btn btn-block btn-primary">Check List Creation</a>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12" id="disp1" style="display:none;">
       <%-- <asp:HyperLink ID="HyperLink16" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/CheckListSecondPhaseApprove.aspx">Check List Validation</asp:HyperLink>--%>
        <a href="/BranchCheckListManagement/View/CheckListSecondPhaseApprove.aspx" class="btn btn-block btn-primary">Checklist validation</a>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12" id="disp2" style="display:none;">
        <%--<asp:HyperLink ID="HyperLink19" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/CheckListThirdPhaseApprove.aspx">Check List Approval</asp:HyperLink>--%>
        <a href="/BranchCheckListManagement/View/CheckListThirdPhaseApprove.aspx" class="btn btn-block btn-primary">Checklist approval</a>
    </div>
                                </div>
                                <br />
                                  <div class="clearfix">
                                    <div class="col-md-4 col-sm-12 col-xs-12 ">
                                        <%-- <asp:HyperLink ID="HyperLink17" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLCatSubCatScoreMappingMaster.aspx">Check List Score Master</asp:HyperLink>--%>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                    </div>
                                       <div class="col-md-4 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                                


                                <div class="clearfix">
                                </div>
                                <br />
                                <div class="clearfix">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
        <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
        <script src="../../Scripts/moment.min.js" defer></script>
        <script src="../../Scripts/date.js"></script>
        <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
        <%--<script src="../../BootStrapCSS/Scripts/UnderScoreJs.js" defer></script>--%>

        <script defer>
            var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "ui.date"]);
            var companyid = '<%= Session["TENANT"] %>';
        </script>
        <script src="../../SMViews/Utility.js" defer></script>
        <script src="../JS/CheckListCreation.js" defer></script>
</body>
</html>
