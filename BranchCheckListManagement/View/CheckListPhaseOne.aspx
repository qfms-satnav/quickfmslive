﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckListPhaseOne.aspx.cs" Inherits="BranchCheckListManagement_View_CheckListPhaseOne" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.49/css/bootstrap-datetimepicker.min.css" integrity="sha512-ipfmbgqfdejR27dWn02UftaAzUfxJ3HR4BDQYuITYSj6ZQfGT1NulP4BOery3w/dT2PYAe3bG5Zm/owm7MuFhA==" crossorigin="anonymous" referrerpolicy="no-referrer" />--%>
    <link href="../JS/DateTimePicker.css" rel="stylesheet" />
    <style>
        .package-table tbody tr:nth-child(odd) {
            background: #e4f7f8;
        }

        .package-table {
            border: 1px solid #6f7ea6;
        }

            .package-table td {
                border-top: 0px !important;
                border-right: 1px solid #6f7ea6 !important;
            }

            /*.package-table tbody tr:nth-child(even) {
            background: #DFDFDF;
        }*/

            .package-table thead tr th {
                color: #4b66a9;
                background-color: #d9edf7;
                border-color: black;
                height: 40px;
                font-size: 1rem;
                border: 2px solid black;
                border-top: 2px solid black !important;
            }

        .list-group-item {
            border: none;
        }

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .table-fixed thead {
            width: 97%;
        }

        .table-fixed tbody {
            height: 400px;
            overflow-y: auto;
            width: 100%;
        }

        .table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
            display: block;
        }

            .table-fixed tbody td, .table-fixed thead > tr > th {
                float: left;
                border-bottom-width: 0;
            }

        .table th {
            background-color: dimgray;
            color: white;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        input[type=radio] {
            margin: 0px 10px 0 !important;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }

        .list-special .list-group-item:first-child {
            border-top-right-radius: 0px !important;
            border-top-left-radius: 0px !important;
        }

        .category-wrapper {
            border: 1px solid grey;
            background: #007bff17;
            margin: 10px 0;
        }

        .subcat-wrapper {
            background: #f8f9fa;
            padding: 15px;
            box-shadow: 0px 10px 10px -8px #ccc;
            border-radius: 0 0 10px 10px;
            margin-bottom: 5px;
        }

        hr {
            border-top: 1px solid #007bff69;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .catName {
            margin-top: 25px;
            background: #2556ad5c;
            padding: 2px 10px;
            letter-spacing: 2px;
            border-radius: 10px 10px 0 0;
        }

        .subcatName {
            margin-top: 25px;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                startDate: 'today'
            });
        };

    </script>
</head>
<body data-ng-controller="CheckListPhaseOneController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Checklist Creation
                                <button type="button" class="btn btn-primary custom-button-color" data-toggle="modal" data-target="#myModal" ng-click="GetDrafts(1)">Drafts</button>
                    <button type="button" class="btn btn-primary custom-button-color" data-toggle="modal" data-target="#myModal" ng-click="GetDrafts(2)">Submitted</button>
                    <button type="button" class="btn btn-primary custom-button-color" ng-click="Clear()">Clear All</button>
                    <%--  <a href="#ex1" rel="modal:open" style="margin-left: 20px; color: #fff;" class="btn btn-primary custom-button-color" ng-click="GetDrafts(1)">Drafts</a>
                                <a href="#ex1" rel="modal:open" style="margin-left: 20px; color: #fff;" class="btn btn-primary custom-button-color" ng-click="GetDrafts(2)">Submitted</a>
                                <a href="#" style="margin-left: 20px; color: #fff;" class="btn btn-primary custom-button-color" ng-click="AllClear()">Clear All</a>--%>
                                
                </h3>
            </div>
            <div class="card">
                <form id="Form1" name="frmCheckList" data-valid-submit="Submit()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.BCL_TP_NAME.$invalid}">
                                <label class="control-label">Type<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="MainType" data-output-model="CheckListPhaseOne.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single" data-on-item-click="LoadData()">
                                </div>
                                <input type="text" data-ng-model="CheckListPhaseOne.MainType" name="BCL_TP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.BCL_TP_NAME.$invalid" style="color: red">Please select main type </span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.CTY_NAME.$invalid}">
                                <label class="control-label">City <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="CheckListPhaseOne.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckListPhaseOne.City" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.CTY_NAME.$invalid" style="color: red">Please select city </span>

                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="CheckListPhaseOne.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckListPhaseOne.Location" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid" style="color: red">Please select location </span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <%--  <div class="clearfix">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid}">
                                <label class="control-label">Submitted by <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Inspection" data-output-model="CheckListPhaseOne.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckListPhaseOne.Inspection" name="INSPECTOR" style="display: none" required="" />
                                <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid" style="color: red">Please select inspection by</span>
                            </div>
                        </div>--%>
                    <div class="clearfix">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid}">
                                <label>Submitted by<span style="color: red;">*</span></label>
                                <div angucomplete-alt id="ex7" placeholder="Search Employees"
                                    remote-url="../../api/CheckListCreation/GetItems?q=" remote-url-request-formatter="remoteUrlRequestFn"
                                    remote-url-data-field="items" title-field="Inspection" minlength="2" input-class="form-control" field-required="true"
                                    match-class="highlight" ng-model="inspectorData" selected-object="selectedItem" data-tick-property="ticked" disable-input="readonlyAngucomplete">
                                    <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.Inspection.$invalid">Please Enter Employee Name </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div>
                                    <label>Designation<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="DesignationList" data-output-model="DesignationList.EMP_ID" button-label="icon EMP_ID" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon EMP_ID" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" style="pointer-events: none; cursor: default;">
                                    </div>
                                    <input type="text" data-ng-model="CheckListPhaseOne.EMP_ID" name="EMP_ID" readonly style="display: none" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid}">
                                <label class="control-label">Submitted date <span style="color: red;">*</span></label>
                                <div class="input-group date" style="width: 150px" id='fromdate'>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </span>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="SVR_FROM_DATE" name="SVR_FROM_DATE" readonly ng-model="CheckListPhaseOne.SVR_FROM_DATE" required />
                                </div>
                                <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please select date</span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <%--new UI--%>

                    <%--<div class="clearfix">
                        <div class="col-md-12 category-wrapper" data-ng-repeat="(groupKey, groupData) in TableData">
                            <h3>{{groupKey}} - {{groupData[0].BCL_MC_NAME}}</h3>
                            <hr>
                            <div data-ng-repeat="column in groupData">
                                <div>{{column.BCL_SUB_NAME}}</div>

                                <div>
                                    <div class="row">
    <div ng-if="column.BCL_SUB_TYPE=='Radio'" class="form-group col-auto">
        <ul class="list-group">
            <li class="form-check form-check-inline" data-ng-repeat="row in column.checklistDetails">
                <label class="form-check-label" ng-if="column.RESPONSE==row.BCL_CH_NAME">
                    <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}">
                    {{row.BCL_CH_NAME}}
                </label>
                <label class="form-check-label" ng-if="column.RESPONSE!=row.BCL_CH_NAME">
                    <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" name="{{row.BCL_CH_NAME}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}">
                    {{row.BCL_CH_NAME}}
                </label>
            </li>
        </ul>
    </div>
    <div ng-if="column.BCL_SUB_TYPE=='Multiple'" class="form-group col-auto">
        <ul class="list-group">
            <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                <label>
                    <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}">
                    {{row.BCL_CH_NAME}}
                </label>
            </li>
        </ul>
    </div>
    <div ng-if="column.BCL_SUB_TYPE=='Toggle'" class="form-group col-auto">
        <ul class="list-group">
            <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                <label>
                    <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}">
                    {{row.BCL_CH_NAME}}
                </label>
            </li>
        </ul>
    </div>

    <div ng-if="column.ISDATE=='Yes'" class="form-group col-auto">
        <ul class="list-group">
            <li class="list-group-item">
                <div class="form-group">
                    <label>Date: </label>
                    <div class="input-group date" id="{{column.BCL_SUB_CODE}}_1">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar" ng-click="DateClick(column.BCL_SUB_CODE)"></i>
                        </div>
                        <input class="form-control" data-ng-model="column.DATERESPONSE" data-ng-bind="column.DATERESPONSE| date:'MM/dd/yyyy'" name="{{column.BCL_SUB_CODE}}_1" type="text" placeholder="dd/mm/yyyy" ng-disabled="true" />
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div ng-if="column.ISFILE=='Yes'" class="form-group col-auto">
        <ul class="list-group">
            <li class="list-group-item">
                <label>
                    File Upload :
                </label>
                <input type="file" data-ng-model="column.FILEPATH" ngif-multiple="false" id="{{column.BCL_SUB_CODE}}" name="{{column.BCL_SUB_CODE}}" onchange="angular.element(this).scope().SelectFile(event)" accept=".png,.jpg,.xlsx,.pdf,.docx">
            </li>
        </ul>
    </div>
    <div ng-if="column.ISCOMMENTS=='Yes'" class="form-group col-auto">
        <ul class="list-group">
            <li class="list-group-item">
                <label>
                    Comments: 
                    <textarea class="form-control" rows="5" data-ng-model="column.TEXT" name="{{column.BCL_SUB_CODE}}" ng-maxlength="20"></textarea>
                </label>
            </li>
        </ul>
    </div>
</div>

                                </div>
                            </div>
                        </div>
                    </div>--%>


                    <div class="clearfix">
                        <div class="col-md-9">
                            <%--<table class="table package-table">--%>
                            <%-- <thead>
                                    <tr>
                                        <th class="col-md-2 col-sm-6 col-xs-12"><b>Category</b></th>
                                        <th class="col-md-2 col-sm-6 col-xs-12"><b>Sub Category</b></th>
                                        <th class="col-md-5 col-sm-6 col-xs-12"><b>Options</b></th>
                                    </tr>
                                </thead>--%>
                            <%--<tbody>--%>
                            <%--<tr data-ng-repeat="(idx,column) in TableData track by $index">--%>
                            <div data-ng-repeat="(idx,column) in TableData track by $index">
                                <%--<td>--%>
                                <div class="catName" data-ng-if="$index === 0 || TableData[$index - 1].BCL_MC_NAME !== column.BCL_MC_NAME">
                                    <b>{{column.BCL_MC_NAME}}</b>
                                </div>
                                <%--</td>--%>

                                <%--<td>--%>
                                <div class="subcat-wrapper">
                                    <span class="subcatName">{{column.BCL_SUB_NAME}}</span>
                                    </br>
                                                <hr>

                                    <%--</td>--%>

                                    <%--<td>--%>
                                    <div class="row">
                                        <div ng-if="column.BCL_SUB_TYPE=='Radio'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="form-check form-check-inline" data-ng-repeat="row in column.checklistDetails">
                                                    <label class="form-check-label" ng-if="column.RESPONSE==row.BCL_CH_NAME">
                                                        <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                    <label class="form-check-label" ng-if="column.RESPONSE!=row.BCL_CH_NAME">
                                                        <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" name="{{row.BCL_CH_NAME}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.BCL_SUB_TYPE=='Multiple'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                    <label>
                                                        <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.BCL_SUB_TYPE=='Toggle'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                    <label>
                                                        <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}">
                                                        {{row.BCL_CH_NAME}}
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>

                                        <div ng-if="column.ISDATE=='Yes'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <div class="form-group">
                                                        <label>Date: </label>
                                                        <div class="input-group date" id="{{column.BCL_SUB_CODE}}_1">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar" ng-click="DateClick(column)"></i>
                                                            </div>
                                                            <%-- placeholder="dd-mm-yyyy"--%>
                                                            <input class="form-control" data-ng-model="column.DATERESPONSE" data-ng-bind="column.DATERESPONSE| date:'MM/dd/yyyy'" name="{{column.BCL_SUB_CODE}}_1" type="text" placeholder="dd/mm/yyyy" ng-disabled="true" />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.ISFILE=='Yes'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <label>
                                                        File Upload :
                                                    </label>
                                                    <input type="file" data-ng-model="column.FILEPATH" ngif-multiple="false" id="{{column.BCL_SUB_CODE}}" name="{{column.BCL_SUB_CODE}}" onchange="angular.element(this).scope().SelectFile(event)" accept=".png,.jpg,.xlsx,.pdf,.docx">
                                                </li>
                                            </ul>
                                        </div>
                                        <div ng-if="column.ISCOMMENTS=='Yes'" class="form-group col-auto">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <label>
                                                        Comments: 
                                                                <textarea class="form-control" rows="5" data-ng-model="column.TEXT" name="{{column.BCL_SUB_CODE}}" ng-maxlength="250"></textarea>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <%--</td>--%>
                                </div>
                                <%--</tr>--%>
                                <%--</tbody>--%>
                                <%--</table>--%>
                            </div>
                        </div>
                    </div>
                    <%--      <br />
                      <div class="clearfix">
                        <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                                <button type="button" class="btn btn-primary custom-button-color" data-ng-click="OnPreviousClick()">Previous</button>
                                 <button type="button" class="btn btn-primary custom-button-color" data-ng-click="OnNextClick()">Next</button>
                        </div>
                    </div>--%>
                    <div class="clearfix">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div>
                                    <label>Overall Comments </label>
                                    <textarea class="form-control" rows="5" placeholder="Overall Comments" data-ng-model="Description" ng-maxlength="250"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 210px;"></div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div id="dvbutton" class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px" data-ng-if="dvbutton">
                            <input type="button" value="Save as Draft" class="btn btn-primary custom-button-color" data-ng-click="Save()" />
                            <input type="submit" value="Submit" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="margin-top: -100px;">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="hsavedtitle">Saved Items</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="col-md-12 table-responsive">
                        <table class="table GridStyle">
                            <thead>
                                <tr class="info">
                                    <th><b>Location ID</b></th>
                                    <th><b>Type</b></th>
                                    <th><b>Inspection ID</b></th>
                                    <th><b>Location name</b></th>
                                    <th><b>Submitted date</b></th>
                                </tr>
                            </thead>
                            <tr data-ng-repeat="items in CheckListPhaseOne.SaveList">
                                <td>
                                    <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="GetLocationCode(items)" data-bs-dismiss="modal"></a>
                                </td>
                                <td>
                                    <label data-ng-bind="items.BCL_TP_NAME" class="control-label" data-ng-model="items.BCL_TP_NAME">
                                    </label>
                                </td>
                                <td>
                                    <label data-ng-bind="items.BCL_ID" class="control-label" data-ng-model="items.BCL_ID">
                                    </label>
                                </td>

                                <td>
                                    <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                    </label>
                                </td>

                                <td>
                                    <label data-ng-bind="items.BCL_SELECTED_DT| date:'MM/dd/yyyy'" class="control-label" data-ng-model="items.BCL_SELECTED_DT">
                                    </label>
                                    <a ng-click="DeleteCheckList(items.BCL_ID,items.BCL_SUBMIT)" data-ng-if="items.BCL_SUBMIT==0"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>
                        </table>
                        <br />
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary custom-button-color" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <%-- <div class="modal" id="myModal"  data-bs-keyboard="false" data-bs-backdrop="static">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="hsavedtitle">Saved Items</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered">
                                <tr class="info">
                                    <th><b>Location ID</b></th>
                                    <th><b>Type</b></th>
                                    <th><b>Sno</b></th>
                                    <th><b>Location Name</b></th>
                                    <th><b>Submitted Date</b></th>
                                </tr>
                                <tr data-ng-repeat="items in CheckListPhaseOne.SaveList">
                                    <td>
                                        <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="GetLocationCode(items)" data-dismiss="modal"></a>
                                    </td>
                                    <td>
                                        <label data-ng-bind="items.BCL_TP_NAME" class="control-label" data-ng-model="items.BCL_TP_NAME">
                                        </label>
                                    </td>
                                    <td>
                                        <label data-ng-bind="items.BCL_ID" class="control-label" data-ng-model="items.BCL_ID">
                                        </label>
                                    </td>

                                    <td>
                                        <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                        </label>
                                    </td>

                                    <td>
                                        <label data-ng-bind="items.BCL_SELECTED_DT| date:'MM/dd/yyyy'" class="control-label" data-ng-model="items.BCL_SELECTED_DT">
                                        </label>
                                        <a ng-click="DeleteCheckList(items.BCL_ID,items.BCL_SUBMIT)" data-ng-if="items.BCL_SUBMIT==0"><i class="fa fa-trash"></i></a>
                                    </td>

                                </tr>
                            </table>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary custom-button-color" data-bs-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>--%>

    <%=ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/date.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js"></script>
    <%--<script defer src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.49/js/bootstrap-datetimepicker.min.js"></script>--%>
    <script src="../JS/DateTimePicker.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "ui.date", "cp.ngConfirm", "angucomplete-alt"]);
        var companyid = '<%= Session["TENANT"] %>';
        var UserId = '<%= Session["UID"] %>';    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/CheckListPhaseOne.js" defer></script>

</body>
</html>
