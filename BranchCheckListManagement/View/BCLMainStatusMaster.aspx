﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BCLMainStatusController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3>Status Master</h3>
        </div>
        <div class="card">
            <form role="form" id="form1" name="BCLMainStatus" data-valid-submit="Save()" novalidate>
                <div class="row">
                    <%--data-on-item-click="SearchA()"--%>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': BCLMainStatus.$submitted && BCLMainStatus.BCL_TP_NAME.$invalid}">
                            <label class="control-label">Check Type</label>
                            <div isteven-multi-select data-input-model="MainType" data-output-model="BCLMainStatus.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="BCLMainStatus.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="BCLMainStatus.$submitted && BCLMainStatus.MainType.$invalid" style="color: red">Please Select Main Type </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Team Status<span style="color: red;">*</span></label>
                                <div style="width: 100%;">
                                    <select id="BCL_MC_TYPENAME" name="BCL_MC_TYPENAME" data-ng-model="BCLMainStatus.BCL_MC_TYPENAME" class="form-control selectpicker" data-ng-change="SearchA()">
                                        <option value="">Select Check List Team Status</option>
                                        <option data-ng-repeat="sta in StaTypeLevel" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Status Name <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <input id="BCL_MC_STATUS" class="form-control" type="text" name="BCL_MC_STATUS" maxlength="50" data-ng-model="BCLMainStatus.BCL_MC_STATUS" />
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3 col-sm-6 col-xs-12" style="display: none">

                        <div class="col-md-3 col-sm-6 col-xs-12" style="display: none">
                            {{BCLMainStatus.BCL_MC_STATUS_ID}}
                                 
                        </div>
                        <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="row" data-ng-show="ActionStatus==1">
                                            <label class="control-label">Status <span style="color: red;">*</span></label>
                                            <div>
                                                <select id="Select1" name="MNC_Status_Id" data-ng-model="MainCategory.MNC_Status_Id" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                    <div class="form-group" data-ng-class="{'has-error': BCLMainStatus.$submitted && BCLMainStatus.HDM_MAIN_MOD_NAME.$invalid}">
                                        <label class="control-label">Module <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="MainCategory.Company" data-button-label="icon HDM_MAIN_MOD_NAME"
                                            data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="MainCategory.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BCLMainStatus.$submitted && BCLMainStatus.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select COMPANY</span>
                                    </div>
                                </div>--%>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../BranchCheckListManagement/JS/BCLMainStatus.js" defer></script>
</body>
</html>
