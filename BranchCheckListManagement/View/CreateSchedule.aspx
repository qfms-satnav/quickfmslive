﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateSchedule.aspx.cs" Inherits="BranchCheckListManagement_View_CreateSchedule" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Facility Management Services::a-mantra </title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
</head>
<body data-ng-controller="CreateScheduleController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel-title="Schedule Visit" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Create Schedule</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form role="form" id="CreateSchedule" name="frmCreateSchedule" data-valid-submit="LoadData()" novalidate>
                                 <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">From Date</label>
                                            <div class="input-group date" id='FROM_DATE'>
                                                <input type="text" class="form-control" data-ng-model="CreateSchedule.FROMDATE" id="FROMDATE" name="FROMDATE" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" ng-click="DateClick('FROM_DATE')"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">To Date</label>
                                            <div class="input-group date" id='TO_DATE'>
                                                <input type="text" class="form-control" data-ng-model="CreateSchedule.TODATE" id="TODATE" name="TODATE" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" ng-click="DateClick('TO_DATE')"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <br />
                                        <div class="form-group">
                                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color" style="height: 40px; width: 100px" data-ng-click="Search()"><b>Search</b></button>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 1000px"></div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="btnsubmit1" class="btn btn-primary custom-button-color" style="height: 40px; width: 120px" data-ng-click="ScheduleVisits()">Schedule visits</button>
                                    </div>
                                    <div class="col-md-12">
                                        <label id="notification" class=" pull-right" style="color: forestgreen !important; border-color: forestgreen; font-size: 15px; padding: 5px 10px; border-radius: 10px;"></label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%=ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../../SMViews/Utility.js"></script>
    <script src="../../../HDM/HDM_Webfiles/js/HDMUtility.min.js"></script>
    <script defer src="../../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../JS/CreateSchedule.js"></script>

</body>
</html>


