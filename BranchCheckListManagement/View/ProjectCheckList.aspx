﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS" data-ng-cloak>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
        .package-table tbody tr:nth-child(odd) {
            background: #FAFAFA;
        }

        .package-table td {
            border-top: 0px !important;
            border-right: 1px solid #fff !important;
        }

        .package-table tbody tr:nth-child(even) {
            background: #DFDFDF;
        }

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .table-fixed thead {
            width: 97%;
        }

        .table-fixed tbody {
            height: 400px;
            overflow-y: auto;
            width: 100%;
        }

        .table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
            display: block;
        }

            .table-fixed tbody td, .table-fixed thead > tr > th {
                float: left;
                border-bottom-width: 0;
            }

        .table th {
            background-color: dimgray;
            color: white;
        }
    </style>
    <script type="text/javascript" defer>
        function setup1(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                maxDate: 'today'
            });
        };
    </script>

</head>
<body data-ng-controller="ProjectCheckListController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Project Work Checklist  <a href="#ex1" rel="modal:open" style="margin-left: 900px; color: green;">Saved Drafts</a></h3>
            </div>
            <div class="card">
                <form id="Form1" name="frmProjectCheckList" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.PROJECTNAM.$invalid}">
                                <label class="control-label">Project Type<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Project" data-output-model="CheckList.Project" data-button-label="icon PROJECTNAM" data-item-label="icon PROJECTNAM"
                                    data-tick-property="ticked" data-max-labels="1" data-on-item-click="PTypeChanged()" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckList.Project" name="PROJECTNAM" style="display: none" required="" />
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.PROJECTNAM.$invalid" style="color: red">Please select project type </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Country" data-output-model="CheckList.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckList.Country" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.CNY_NAME.$invalid" style="color: red">Please select country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.CTY_NAME.$invalid}">
                                <label class="control-label">City<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="CheckList.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                    data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckList.City" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.CTY_NAME.$invalid" style="color: red">Please select city</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.LCM_NAME.$invalid}">
                                <label class="control-label">Location<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="CheckList.Locations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                    data-on-item-click="LcmChanged()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckList.Locations" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.LCM_NAME.$invalid" style="color: red">Please select location</span>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.CNP_NAME.$invalid}">
                                <label class="control-label">Business unit<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Business" data-output-model="CheckList.Business" data-button-label="icon CNP_NAME" data-item-label="icon CNP_NAME"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckList.Business" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.CNP_NAME.$invalid" style="color: red">Please select business unit</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.INSPECTOR.$invalid}">
                                <label class="control-label">Inspection By <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Inspection" data-output-model="CheckList.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CheckList.Inspection" name="INSPECTOR" style="display: none" required="" />
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.INSPECTOR.$invalid" style="color: red">Please select inspection by  </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.SVR_FROM_DATE.$invalid}">
                                <label class="control-label">Visit Date <span style="color: red;">*</span></label>
                                <div class="input-group date" style="width: 150px" id='fromdate'>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="SVR_FROM_DATE" name="SVR_FROM_DATE" ng-model="CheckList.SVR_FROM_DATE" required />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmProjectCheckList.$submitted && frmProjectCheckList.PRJ_NAME.$invalid}">
                                <label class="control-label">Project Name<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Project Name" id="PRJ_NAME" name="PRJ_NAME" ng-model="CheckList.PRJ_NAME" required />
                                <span class="error" data-ng-show="frmProjectCheckList.$submitted && frmProjectCheckList.PRJ_NAME.$invalid" style="color: red">Enter project name</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5>Checklist Details
                                </h5>
                            </div>
                            <table class="table package-table">
                                <thead>
                                    <tr>
                                        <th class="col-xs-4">Category
                                        </th>
                                        <th class="col-xs-4">Subcategory
                                        </th>
                                        <th class="col-xs-4">Status
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr data-ng-repeat="row in MainDataBinding[0]">
                                        <td class="col-xs-4">{{row.catname}} </td>
                                        <td id="{{row.Subcatcode}}" class="col-xs-4">{{row.Subcatname}} </td>
                                        <td class="col-xs-4">
                                            <div data-ng-repeat="(key, val) in row.items track by $index">

                                                <div data-ng-if="val.childtype=='Radio'">
                                                    <label for="{{val.childcode}}" class="radio-inline">
                                                        <input type="radio" class="custom-control-input"
                                                            name="{{row.Subcatname}}" value="{{val.childcode}}"
                                                            data-ng-click="checkchange(val.childcode,row.Subcatcode,row.catcode,'','',val.childtype)"
                                                            data-ng-model="ProjectCheklist.CheckedValue[val.childcode]">
                                                        {{val.childname}}</label>
                                                </div>

                                                <label data-ng-if="val.childtype=='Calender'">
                                                    {{val.childname}}
                                                            <div class="input-group date" data-provide="datepicker" style="width: 200px;">
                                                                <input id="{{val.childcode}}" type="text" class="form-control" data-ng-model="ProjectCheklist.CheckedValue[val.childcode]"
                                                                    data-ng-change="checkchange(val.childcode,row.Subcatcode,row.catcode,'','',val.childtype)">
                                                                <div class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-th"></span>
                                                                </div>
                                                            </div>
                                                </label>

                                                <label data-ng-if="val.childtype=='Textbox'">
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label">{{val.childname}}</label>
                                                        <input type="text" name="novalidation" id="{{val.childcode}}"
                                                            class="form-control" placeholder="Any or no text is fine"
                                                            data-ng-model="ProjectCheklist.CheckedValue[val.childcode]"
                                                            data-ng-change="checkchange(val.childcode,row.Subcatcode,row.catcode,'','',val.childtype)">
                                                    </div>
                                                </label>
                                                <label data-ng-if="val.childtype=='CheckBox'">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" ng-checked="CheckedChe"
                                                            data-ng-model="ProjectCheklist.CheckedValue[val.childcode]" value="{{val.childcode}}"
                                                            data-ng-change="checkchange(val.childcode,row.Subcatcode,row.catcode,'','',val.childtype)">
                                                        <label class="custom-control-label" id="{{val.childcode}}" for="defaultUnchecked">{{val.childname}}</label>
                                                    </div>
                                                </label>

                                                <div data-ng-if="val.childcode==ProjectCheklist.CheckedValue.Childcode">
                                                    <div data-ng-repeat="(key5, val5) in  val.subchildname.split(',') track by $index">
                                                        <div data-ng-repeat="(key6, val6) in  val.subchildcode.split(',') track by $index">
                                                            <div data-ng-if="key5==key6">
                                                                <div data-ng-if="val.subchildtype=='Radio'">
                                                                    <input type="radio" class="custom-control-input" id="{{val5}}"
                                                                        name="val.childcode" value="{{val6}}" data-ng-click="checkchange(val.childcode,row.Subcatcode,row.catcode,val6,'',val.childtype)"
                                                                        data-ng-model="ProjectCheklist.SubchildRadioButtonValue[val6]">
                                                                    <label class="custom-control-label">{{val5}}</label>
                                                                </div>
                                                                <div data-ng-if="val.subchildtype=='CheckBox'">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        name="{{val5}}+options" value="{{val5}}" data-ng-change="checkchange(val.childcode,row.Subcatcode,row.catcode,val6,'',val.childtype,ProjectCheklist.SubchildRadioButtonValue[val6])"
                                                                        data-ng-model="ProjectCheklist.SubchildRadioButtonValue[val6]">

                                                                    <%--<input type="checkbox" data-ng-model="ProjectCheklist.SubchildRadioButtonValue[val6]" data-ng-true-value="'{{val5}}'" data-ng-false-value="'N'" data-ng-change="checkchange(val.childcode,row.Subcatcode,row.catcode,val6,'',val.childtype,ProjectCheklist.SubchildRadioButtonValue[val6])" />--%>
                                                                    <label class="custom-control-label">{{val5}}</label>
                                                                </div>
                                                            </div>

                                                            <div data-ng-if="val6==ProjectCheklist.SubchildRadioButtonValue[val6]">
                                                                <%--{{val6}} {{$scope.ProjectCheklist.CheckedValue.Childsubcode}}--%>
                                                                <div data-ng-repeat="(key10, val10) in  val.subchildtype1.split(',') track by $index">
                                                                    <div data-ng-repeat="(key11, val11) in  val.subchildname1.split(',') track by $index">
                                                                        <div data-ng-if="key10==key11"></div>
                                                                        <div data-ng-repeat="(key12, val12) in  val.subchildcode1.split(',') track by $index">
                                                                            <div data-ng-if="key10==key11">
                                                                                <div data-ng-if="val.subchildtype1=='Radio'">
                                                                                    <input type="radio" class="custom-control-input" id="{{val10}}"
                                                                                        name="{{val10}}+options" value="{{val10}}" data-ng-click="checkchange(val.childcode,row.Subcatcode,row.catcode,val6,'',val.childtype)"
                                                                                        data-ng-model="ProjectCheklist.SubchildRadioButtonValue[val10]">
                                                                                    <label class="custom-control-label">{{val11}}</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div style="margin-top: 1px;">
                                    <label for="txtcode" class="custom-file"><strong>Overall Comments</strong></label>
                                    <textarea name="OVERALL_CMTS" id="OVERALL_CMTS" class="form-control" data-ng-model="CheckList.OVERALL_CMTS" cols="40" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <label for="txtcode" class="custom-file">Upload images/document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                            <input multiple type="file" name="UPLFILE" data-ng-model="CheckList.UPLFILE[0]" id="UPLFILE" accept=".png,.jpg,.jpeg" class="custom-file-input" onchange="angular.element(this).scope().fileNameChanged(this)">
                            <span class="custom-file-control"></span>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="row">
                            <div class="pull-left" style="padding-left: 25px; padding-top: 10px">
                            </div>
                            <br />
                            <br />
                            <div class="row box-footer text-right" style="padding-right: 35px">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="SavingData(0)" />
                                <input type="submit" value="Save as Draft" class="btn btn-primary custom-button-color" data-ng-click="SavingData(1)" />
                                <%--<input type="submit" value="Cancel" class="btn btn-primary custom-button-color" data-ng-click="setStatus('Cancel')" data-ng-disabled="ButtonStatus == 2" />--%>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-12">
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 47%"></div>
                        </div>
                    </div>
                    <div id="ex1" class="modal">

                        <div class="clearfix">
                            <div class="col-md-12">
                                <h4>Saved items</h4>
                            </div>
                            <div class="col-md-12 table-responsive">
                                <table class="table table-bordered">

                                    <tr>
                                        <td style="width: 100px;">Location code
                                        </td>
                                        <td style="width: 198px">Location name
                                        </td>
                                        <td style="width: 110px">Inspected date
                                        </td>
                                    </tr>
                                    <tr data-ng-repeat="items in CheckList.SaveList">
                                        <td style="width: 100px; height: 55px;">
                                            <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="GetLocationCode(items)" rel="modal:close"></a>
                                        </td>
                                        <td style="width: 198px; height: 55px;">
                                            <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                            </label>
                                        </td>

                                        <td style="width: 110px; height: 55px;">
                                            <label data-ng-bind="items.PM_CL_MAIN_VISIT_DT| date:'MM/dd/yyyy'" class="control-label" data-ng-model="items.PM_CL_MAIN_VISIT_DT">
                                            </label>
                                        </td>

                                    </tr>

                                </table>
                            </div>
                        </div>
                        <a href="#" rel="modal:close">Close</a>
                    </div>
                    <%--<div class="row">       
                                    <div class="col-md-3 col-sm-6 col-xs-12">                                       
                                         <table  border="1">
                                          <tr>
                                               <th>
                                                   Category
                                               </th>
                                              <th>
                                                  Sub Category
                                              </th>
                                              <th>
                                                  Child Category
                                              </th>
                                             
                                          </tr>
                                          <tr data-ng-repeat="row in MainDataBinding[0]">       
                                              <td>
                                                 {{row.catname}} 
                                              </td>
                                               <td>
                                                 {{row.Subcatname}} 
                                              </td>
                                              <td>
                                                  <div data-ng-repeat="(key, val) in row.items track by $index">  
                                                      <table>
                                                          <tr>
                                                              <td>
                                                    <div class="custom-control custom-radio custom-control-inline" data-ng-if="val.childtype=='Radio'">
                                                        <input type="radio" class="custom-control-input" id="{{val.childcode}}"
                                                            name="{{row.Subcatname}}" value="val.childcode"
                                                            data-ng-click="checkchange(val.childcode,row.Subcatcode,row.catcode,'')"
                                                            data-ng-model="ProjectCheklist.CheckedValue[val.childcode]">
                                                        <label class="custom-control-label" for="{{val.childcode}}">{{val.childname}}</label>
                                                        </div> 
                                                              </td>
                                                              <td>
                                                            <label data-ng-if="val.childtype=='Calender'">
                                                              {{val.childname}}
                                                            <div class="input-group date" data-provide="datepicker" style="width:200px;">
                                                            <input type="text" class="form-control" data-ng-model="ProjectCheklist.CheckedValue[val.childcode]"
                                                                 data-ng-change="checkchange(val.childcode,row.Subcatcode,row.catcode,'')">
                                                            <div class="input-group-addon">
                                                                <span class="glyphicon glyphicon-th"></span>
                                                            </div>
                                                            </div> 
                                                          </label> 
                                                              </td>
                                                              <td>
                                                            <label data-ng-if="val.childtype=='Textbox'">
                                                                  <div class="form-group">
                                                                    <label for="textfield" class="control-label">{{val.childname}}</label>
                                                                    <input type="text" id="novalidation" name="novalidation" 
                                                                           class="form-control" placeholder="Any or no text is fine"
                                                                        data-ng-model="ProjectCheklist.CheckedValue[val.childcode]"
                                                                     data-ng-change="checkchange(val.childcode,row.Subcatcode,row.catcode,'')">
                                                                </div>
                                                              </label>
                                                              </td>
                                                              <td>
                                                          
                                                               <label data-ng-if="val.childtype=='CheckBox'">
                                                                   <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="defaultUnchecked"
                                                                        data-ng-model="ProjectCheklist.CheckedValue[val.childcode]" value="{{val.childcode}}"
                                                                       data-ng-change= "checkchange(val.childcode,row.Subcatcode,row.catcode,'')">
                                                                    <label class="custom-control-label" for="defaultUnchecked">{{val.childname}}</label>
                                                                     </div>
                                                                </label> 
                                                              </td>
                                                              <td data-ng-if="val.childcode==ProjectCheklist.CheckedValue.Childcode">
                                                                <div class="custom-control custom-radio custom-control-inline"
                                                              data-ng-if="val.subchildtype=='Radio'  && val.Flag==1">
                                                              <div data-ng-repeat="(key5, val5) in  val.subchildname.split(',') track by $index">
                                                                  <div data-ng-repeat="(key6, val6) in  val.subchildcode.split(',') track by $index">
                                                                      <div data-ng-if="key5==key6">
                                                                   <input type="radio" class="custom-control-input" id="{{val5}}"
                                                            name="{{val5}}+options" value="{{val5}}" data-ng-click="checkchange(val.childcode,row.Subcatcode,row.catcode,val6)"
                                                            data-ng-model="ProjectCheklist.SubchildRadioButtonValue[val6]">
                                                        <label class="custom-control-label" >{{val5}}</label>
                                                                          </div>
                                                                  </div                
                                                       </div>
                                                        </div> 
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </div>                                                              
                                              </td>
                                                                            
                                          </tr>
                                        </table>
                                    </div>
                                </div>--%>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>

    <script defer>

        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var tenant ='<%=Session["TENANT"]%>';

        $('.datepicker').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '-3d'
        });
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/ProjectCheckList.js" defer></script>
    <script src="../JS/CheckListCreation.js"></script>
</body>
</html>
