﻿app.service("BCLMainCategoryService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLMainCategory/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
   
}]);
app.controller('BCLMainCategoryController', ['$scope', '$q', 'UtilityService', 'BCLMainCategoryService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, BCLMainCategoryService, $filter, $timeout, $rootScope) {
        $scope.MainType = [];   
        setTimeout(function () { $scope.LoadData(); }, 500);
        $scope.GetMainType = function () {
            $scope.MainType = [];
            BCLMainCategoryService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                angular.forEach($scope.MainType, function (value, key) {
                    var a = _.find($scope.MainType);
                    a.ticked = true;
                });
                setTimeout(function () { $scope.GetButton(); }, 500);
            })
        }
        $scope.LoadData = function () {
            progress(0, 'Loading...', true);           
            $scope.GetMainType();
            progress(0, '', false);
        }
        $scope.GetButton = function () {
            var BCL_TP_LEVEL = _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_LEVEL; }).join(',');
            if (BCL_TP_LEVEL == 0) {
                $('#disp1').hide();
                $('#disp2').hide();
            }
            else if (BCL_TP_LEVEL == 1) {
                $('#disp1').show();
                $('#disp2').hide();
            }
            else {
                $('#disp1').show();
                $('#disp2').show();
            }
        }
    }]);