﻿app.service("CheckListPhaseThreeService", function ($http, $q, UtilityService) {
    this.getSavedListZonalCentral = function (BCLStatus) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getSavedListZonalCentral?BCLStatus=' + BCLStatus + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetTableData = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/GetValidation_Grid?BCL_TP_ID=' + data.BCL_TP_ID + '&BCL_ID=' + data.BCL_ID + '&BCL_SUBMIT=' + data.BCL_SUBMIT)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getZonalCentralStatus = function (BCL_TP_ID, typeName) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getZonalCentralStatus?BCL_TP_ID=' + BCL_TP_ID + '&typeName=' + typeName + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getInspectorsZonalCentral = function (TPM_TP_ID, TPM_LCM_CODE, TPMD_APPR_LEVELS) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getInspectorsZonalCentral?TPM_TP_ID=' + TPM_TP_ID + '&TPM_LCM_CODE=' + TPM_LCM_CODE + '&TPMD_APPR_LEVELS=' + TPMD_APPR_LEVELS + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveApprovalData = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/SaveApprovalData', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
})

app.controller('CheckListPhaseThreeController', function ($scope, $q, $location, UtilityService, $filter, $http, CheckListPhaseThreeService) {
    $scope.CheckListPhaseThree = {};
    $scope.Disabled = true;
    $scope.rowData = [];
    $scope.TableData = [];

    $scope.columnDefs = [
        { headerName: "Selected File", field: "FILEPATH", cellClass: "grid-align", width: 250 },
        {
            headerName: "Download", field: "FILEPATH", cellClass: 'grid-align', width: 250, cellRenderer: function (params) {

                return "<a download='" + params.data.FILEPATH + "' href='../../UploadFiles/" + companyid + "/" + params.data.FILEPATH + "'><span class='glyphicon glyphicon-download'></span></a>";
            }
        },
        { headerName: "Remove", template: '<a ng-click="Remove(data)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 250 }
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: $scope.rowData,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };

    $scope.GetDrafts = function (e) {
        CheckListPhaseThreeService.getSavedListZonalCentral(e).then(function (response) {
            if (response.data != null) {
                $scope.CheckListPhaseThree.SaveList = response.data;
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }
    $scope.CheckListPhaseThree.SVR_FROM_DATE = moment().format('MM/DD/YYYY');
    $scope.GetGridData = function (items) {
        CheckListPhaseThreeService.GetTableData(items).then(function (response) {
            $scope.CheckListPhaseThree.TableData = response.data;
            $scope.rowData = _.filter(response.data, function (o) { return o.FILEPATH.length > 0; });
            $scope.gridOptions.api.setRowData($scope.rowData);
            $('#form3').show();
        });
    }

    $scope.LoadOption = function (BCL_TP_ID) {

        CheckListPhaseThreeService.getZonalCentralStatus(BCL_TP_ID, "Approval Status").then(function (response) {
            if (response.data != null) {
                $scope.CheckListPhaseThree.items = response.data;
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.GetApprovalInchare = function (items) {
        CheckListPhaseThreeService.getInspectorsZonalCentral(items.BCL_TP_ID, items.LCM_CODE, items.BCL_TP_LEVEL).then(function (response) {
            if (response.data != null) {
                $scope.Inspection = response.data;
                angular.forEach($scope.Inspection, function (item, index) {
                    if (index == 0) {
                        item.ticked = true
                    }
                });
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.LoadData = function (items) {
        $scope.LoadOption(items.BCL_TP_ID);
        $scope.CheckListPhaseThree.TYPE_ID = items.BCL_TP_ID;
        $scope.CheckListPhaseThree.TYPE_NAME = items.BCL_TP_NAME;
        $scope.CheckListPhaseThree.CTY_NAME = items.CTY_NAME;
        $scope.CheckListPhaseThree.LCM_NAME = items.LCM_NAME;
        $scope.CheckListPhaseThree.BCL_INSPECTED_BY = items.BCL_INSPECTED_BY;
        $scope.CheckListPhaseThree.BCL_SELECTED_DT = items.BCL_SELECTED_DT;
        $scope.CheckListPhaseThree.BCLD_REVIEWED_BY = items.BCLD_REVIEWED_BY;
        $scope.CheckListPhaseThree.BCL_REVIEWED_DT = items.BCL_REVIEWED_DT;
        $scope.CheckListPhaseThree.BCL_ID = items.BCL_ID;
        $scope.GetGridData(items);
        $scope.GetApprovalInchare(items);
        setTimeout(function () {
            $('#myModal').modal('toggle');
        }, 500);
    }



    $scope.Submit = function () {
        $scope.CheckListPhaseThree.datavalidate = _.filter($scope.CheckListPhaseThree.TableData, function (o) { return o.ApprovalAction.length > 0; });
        if ($scope.CheckListPhaseThree.datavalidate.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select atleast one value');
            return;
        }
        //console.log($scope.CheckListPhaseThree.TableData);
        $scope.SelectedDatas = [];
        angular.forEach($scope.CheckListPhaseThree.TableData, function (Value, Key) {
            $scope.SelectedData = {};
            $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
            $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
            $scope.SelectedData.BCLD_CENTRAL_ACTIONS = Value.ApprovalAction;
            $scope.SelectedData.BCLD_CENTRAL_TEAM_CMTS = Value.ApprovalComment;
            $scope.SelectedDatas.push($scope.SelectedData);
        });
        var data = {
            Seldata: $scope.SelectedDatas,
            Flag: 4,
            BCL_ID: $scope.CheckListPhaseThree.BCL_ID,
            InspectdBy: $scope.Inspection[0].AUR_ID,
            date: $scope.CheckListPhaseThree.SVR_FROM_DATE,
        }
        CheckListPhaseThreeService.SaveApprovalData(data).then(function (response) {
            if (response.data != null) {
                showNotification('success', 8, 'bottom-right', 'Request approved succesfully');
                $scope.Clear();
                $('#form3').hide();
            } else {
                showNotification('error', 8, 'bottom-right', 'Something Went Wrong');
            }
        });


    }
    $scope.Clear = function () {
        $scope.CheckListPhaseThree = {};
        $scope.rowData = [];
        $scope.gridOptions.api.setRowData($scope.rowData);
        angular.forEach($scope.Inspection, function (value, key) {
            value.ticked = false;
        });
    }

    $scope.SelectFile = function (e) {
        if ($scope.gridOptions.rowData.length > 0)
            _.remove($scope.rowData, { SubCatCode: e.target.name });
        $scope.rowData.push({ SubCatCode: e.target.name, Name: e.target.files[0].name, file: e.target.files[0] });
        $scope.gridOptions.api.setRowData($scope.rowData);

        var formData = new FormData();
        var UplFile = $('#' + e.target.id)[0];
        formData.append("UplFile", UplFile.files[0]);
        $http.post(UtilityService.path + '/api/CheckListCreation/UploadFiles', formData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
            .success(function (data, status) {
                showNotification('success', 8, 'bottom-right', data);
            })
            .error(function (data, status) {
            });
    };
    $scope.Remove = function (data) {
        if ($scope.gridOptions.rowData.length > 0)
            _.remove($scope.rowData, { SubCatCode: data.SubCatCode });
        $scope.gridOptions.api.setRowData($scope.rowData);
    }

});

