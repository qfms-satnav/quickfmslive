﻿angular.module('QuickFMS', ["agGrid"]).controller('Branch_Inspect_Controller', function ($scope, $http, $window) {
    debugger;
    $scope.ListData = [];
    $scope.URLData = window.location.origin + "/UploadFiles/";
    if ($window.location.search.split('?').length > 1) {
        var filedetails = {
            BCLID: ($window.location.search.split('?')[1].split('&'))[0].split('=')[1],
            CLIENTNAME: $window.location.search.split('?')[1].split('&')[1].split('=')[1]
        };

        $http.post(window.location.origin + '/api/BCLMainStatus/Get_Branch_Images', JSON.stringify(filedetails), {
            transformRequest: angular.identity,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            if (response.data.Message === 'Ok') {
                $scope.ListData = response.data.data;
            }
        }, function (error) {
            showNotification('error', 8, 'bottom-right', error.data.Message);
        });
    }

    
    $scope.DownloadSingleImg = function (images) {
        // Make a request to get the image as a blob
        $http({
            method: 'GET',
            url: $scope.URLData + images.File_Folder + "/" + images.BCLD_FILE_UPLD,
            responseType: 'arraybuffer'
        }).then(function (response) {
            // Create a blob from the response data
            var blob = new Blob([response.data], { type: response.headers('Content-Type') });

            // Create a link element
            var link = document.createElement("a");

            // Set the download attribute and create a temporary URL for the blob
            link.download = images.BCLD_FILE_UPLD;
            link.href = window.URL.createObjectURL(blob);

            // Append the link to the document
            document.body.appendChild(link);

            // Trigger the click event
            link.click();

            // Remove the link from the document
            document.body.removeChild(link);
        });
    };

    $scope.DownloadMultipleImages = function () {
        $scope.ListData.forEach(function (images, index) {
            // Make a request to get the image as a blob
            $http({
                method: 'GET',
                url: $scope.URLData + images.File_Folder + "/" + images.BCLD_FILE_UPLD,
                responseType: 'arraybuffer'
            }).then(function (response) {
                // Create a blob from the response data
                var blob = new Blob([response.data], { type: response.headers('Content-Type') });

                // Create a link element
                var link = document.createElement("a");

                // Set the download attribute and create a temporary URL for the blob
                link.download = images.BCLD_FILE_UPLD;
                link.href = window.URL.createObjectURL(blob);

                // Append the link to the document
                document.body.appendChild(link);

                // Trigger the click event
                link.click();

                // Remove the link from the document
                document.body.removeChild(link);

                // Check if it's the last image and perform any final actions
                if (index === imageList.length - 1) {
                    showNotification('error', 8, 'bottom-right', "All images downloaded");
                }
            });
        });
        };


});