﻿app.service("BCLMainTypeService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {    
    this.GetGridData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLMainType/GetGridData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.updateData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLMainType/updateData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLMainType/SaveData', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetGPS = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLMainType/GetGPS', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);
app.controller('BCLMainTypeController', ['$scope', '$q', 'UtilityService', 'BCLMainTypeService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, BCLMainTypeService, $filter, $timeout, $rootScope) {
        $scope.StaTypeLevel = [{ Id: '0', Name: 'Inspection Level' }, { Id: '1', Name: 'Validation Level' }, { Id: '2', Name: 'Approval Level' }];
        $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
        $scope.LoadInfo = [];
        $scope.BCLMainType = {};     
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Edit = false;
        function validatinFn() {           
            if ($scope.BCLMainType.BCL_TP_NAME == undefined || $scope.BCLMainType.BCL_TP_NAME == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Check Type Name");
                return false;
            }            
            if ($scope.BCLMainType.BCL_TP_REMARKS == undefined || $scope.BCLMainType.BCL_TP_REMARKS == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Check Type Remarks");
                return false;
            }
            if ($scope.BCLMainType.BCL_TP_LEVEL == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Checklist Approval Level");
                return false;
            }
            if ($scope.BCLMainType.BCL_TP_GPS == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Checklist GPS Status");
                return false;
            }
            return true;
        }        
        $scope.SaveA = function () {            
            if (validatinFn()) {                
                var obj = {
                    BCL_TP_ID: 0,
                    BCL_TP_NAME: $scope.BCLMainType.BCL_TP_NAME,
                    BCL_TP_REMARKS: $scope.BCLMainType.BCL_TP_REMARKS,
                    BCL_TP_LEVEL: $scope.BCLMainType.BCL_TP_LEVEL,
                    BCL_TP_GPS: $scope.BCLMainType.BCL_TP_GPS
                    //SUBCAT_STS_ID: 1,
                }
                BCLMainTypeService.SaveData(obj).then(function (response) {
                    if (response != null) {                       
                        if (response.data == 0) {
                            showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                            setTimeout(function () { $scope.LoadData(); }, 500);
                            $scope.ClearData();
                        }
                        else
                            showNotification('error', 8, 'bottom-right', "Check Type Name Already Exists");
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Some Error Occured");
                }, function (response) {
                    progress(0, '', false);

                });

            }

        }
        BCLMainTypeService.GetGPS().then(function (response) {

            if (response != null) {
                debugger;
                $scope.StaTypeLevel1 = response.data.data
                /* setTimeout(function () { $scope.LoadData(); }, 500);*/
            }
        }, function (response) {
            progress(0, '', false);
        });

        $scope.Update = function () {
            var obj = {
                BCL_TP_ID: $scope.BCLMainType.BCL_TP_ID,
                BCL_TP_NAME: $scope.BCLMainType.BCL_TP_NAME,
                BCL_TP_REMARKS: $scope.BCLMainType.BCL_TP_REMARKS,
                BCL_TP_LEVEL: $scope.BCLMainType.BCL_TP_LEVEL,
                BCL_TP_GPS: $scope.BCLMainType.BCL_TP_GPS
            }
            BCLMainTypeService.updateData(obj).then(function (response) {                
                //if (response.data != null) {  
                if (response != null) {
                    if (response == 0) {
                        showNotification('success', 8, 'bottom-right', 'Data updated Successfully');
                        setTimeout(function () { $scope.SearchA(); }, 500);
                        //$scope.ClearData();
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Check Type Name Already Exists");
                }
                else
                    showNotification('error', 8, 'bottom-right', "Some Error Occured");
            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.ShowStatus = function (value) {
            return $scope.StaDet[value == 0 ? 1 : 0].Name;
        }

     

        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };

        setTimeout(function () { $scope.LoadData(); }, 1000);
        //$timeout($scope.LoadData, 1000);

        $scope.EditFunction = function (data) {
            $scope.BCLMainType = {};
            $scope.BCLMainType.BCL_TP_ID = data.BCL_TP_ID;
            $scope.BCLMainType.BCL_TP_LEVEL = data.BCL_TP_LEVEL;
            setTimeout(function () { $('#BCL_TP_LEVEL').val(data.BCL_TP_LEVEL); }, 500);            
            $scope.EditCategory = data;
            angular.copy(data, $scope.BCLMainType);
            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            $scope.Edit = true;

        }
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };
        var columnDefs = [
            { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Check Type Name", field: "BCL_TP_NAME", cellClass: "grid-align", width: 250 },
            { headerName: "Check List Approval Level", field: "LEVELTYPE", cellClass: "grid-align", width: 250 },
            { headerName: "Check Type Remarks", field: "BCL_TP_REMARKS", cellClass: "grid-align", width: 450 },
            { headerName: 'GPS Status', field: 'GPS_STATUS', cellClass: "grid-align", width: 250 },
           // { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.SUBC_STA_ID)}}", width: 120, cellClass: 'grid-align' },
        ];
        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,          
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,            
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            },
        };
        $scope.SearchA = function () {           
            BCLMainTypeService.GetGridData().then(function (response) {
                if (response != null) {
                    $scope.gridata = response.data;
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                }
                progress(0, 'Loading...', false);
            })
        }        

        $scope.LoadData = function () {           
            progress(0, 'Loading...', true);
            $scope.BCLMainType = {};
            $scope.SearchA();
            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            progress(0, '', false);
        }
        $scope.GenerateFilterPdf = function () {
            ;

        }

        $scope.GenReport = function (data) {
            progress(0, 'Loading...', true);
            setTimeout(function () {
                progress(0, 'Loading...', false);
                if (data == 'pdf') {
                    const songs = [];
                    angular.forEach($scope.LoadInfo, function (val) {
                        songs.push({ "AAT_CODE": val.AAS_SPAREPART_NAME + ',' + val.AAS_SPAREPART_ID, "AAT_NAME": val.AAS_SPAREPART_DES });
                    });
                    qr_generate(songs);
                    progress(0, 'Loading...', false);
                }
            }, 3000);
        }       
        $scope.ClearData = function () {
            $scope.BCLMainType = {};
            $scope.$broadcast('angucomplete-alt:clearInput');
        }
    }]);