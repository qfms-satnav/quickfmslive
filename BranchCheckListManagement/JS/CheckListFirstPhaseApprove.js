﻿app.directive("selectNgFiles", function () {
    return {
        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var files = elem[0].files;
                ngModel.$setViewValue(files[0].name);
            })
        }
    }
});
app.service("CheckListCreationService", function ($http, $q, UtilityService) {
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //this.getInspectors = function () {
    //    var deferred = $q.defer();
    //    return $http.get(UtilityService.path + '/api/CheckListCreation/getInspectors')
    //        .then(function (response) {
    //            deferred.resolve(response.data);
    //            return deferred.promise;
    //        }, function (response) {
    //            deferred.reject(response);
    //            return deferred.promise;
    //        });
    //};
    this.getInspectorsZonalCentral = function (TPM_TP_ID, TPM_LCM_CODE, TPMD_APPR_LEVELS) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getInspectorsZonalCentral?TPM_TP_ID=' + TPM_TP_ID + '&TPM_LCM_CODE=' + TPM_LCM_CODE + '&TPMD_APPR_LEVELS=' + TPMD_APPR_LEVELS + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getCompany = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getCompany')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GetGriddata = function (BCL_TP_ID, SelectedDraftLocation) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/GetGriddata?BCL_TP_ID=' + BCL_TP_ID + ' ', SelectedDraftLocation)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetScore = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/GetScore')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.InsertCheckList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/InsertCheckList', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getLocations = function () {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getLocations')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getSavedList = function (BCLStatus, BCL_TP_ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getSavedList?BCLStatus=' + BCLStatus + '&BCL_TP_ID=' + BCL_TP_ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getCheckedListDetail = function (BCLID) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getCheckedListDetail?BCLID=' + BCLID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteCheckList = function (BCLID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/DeleteCheckList?BCLID=' + BCLID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getSavedListItems = function (LcmCode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getSavedListItems?LcmCode=' + LcmCode + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.LocationByCity = function (citylst) {
        var deferred = $q.defer();
        $http.post(UtilityService.path + '/api/CheckListCreation/LocationByCity', citylst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };



    this.GetLocationsIn = function (ID) {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CheckListCreation/GetLocationsIn?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.GetInspectorsIn = function (ID) {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CheckListCreation/GetInspectorsIn?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

});
app.controller('CheckListCreationController', function ($scope, $q, $location, CheckListCreationService, UtilityService, $filter) {


    $scope.TataCapitalCHK = ''

    UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPref = response.data;
            //console.log($scope.SysPref);
            $scope.TataCapitalCHK = _.find($scope.SysPref, { SYSP_CODE: "Tata Capital" });

        }
    });

    $scope.CheckList = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Inspection = [];
    $scope.CheckList.CNP_NAME = [];
    $scope.CheckList.Country = [];
    $scope.CheckList.City = [];
    $scope.CheckList.Location = [];
    $scope.CheckList.Inspection = [];
    //$scope.CheckList.OVERALL_CMTS = [];
    $scope.SelectedValues = [];
    $scope.TotalData = [];
    $scope.DraftData = [];
    $scope.MainType = [];
    $scope.SelectedDraftLocation = {};
    rowData = [];
    MultiUploadData = [];
    var count = 0;

    $scope.classActive = 'has-error2';

    $scope.TableDataArr = [];
    ///////////File Upload Grid

    $scope.columnDefs = [
        { headerName: "Sub Category", field: "SubCatName", cellClass: "grid-align", width: 227 },
        { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 250 },
        //{ headerName: "Image", field: "file", cellClass: "grid-align", width: 250 },
        {
            headerName: "Download", field: "Name", width: 98, cellRenderer: function (params) {
                //return '<a download=' + params.data.Name + ' href="' + params.data.FilePath + '"><span class="glyphicon glyphicon-download"></span></a>';
                return "<a download='" + params.data.Name + "' href='../../UploadFiles/GMR_UAT.dbo/" + params.data.Name + "'><span class='glyphicon glyphicon-download'></span></a>";
            }
        },
        { headerName: "Remove", template: '<a ng-click="Remove(this)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 98 }
        //{ headerName: "Remove", template: '<a ng-click="Remove(data.Name)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 130 }
    ];

    var rowData = [];
    var data;
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: rowData,
        angularCompileRows: true
    };


    $scope.Remove = function (node) {
        $('#UPLFILE').val("");
        var SelectedRow = _.find(rowData, function (x) { if (x.Name == node.data.Name) return x });
        var SelectedIndex = _.indexOf(rowData, SelectedRow);
        rowData.splice(SelectedIndex, 1);
        SelectedRow = _.find(MultiUploadData, function (x) { if (x.Imagepath == node.data.Name) return x });
        SelectedIndex = _.indexOf(MultiUploadData, SelectedRow);
        MultiUploadData.splice(SelectedIndex, 1);
        if ($scope.gridOptions.rowData.length > 0) {
            var Files = $scope.gridOptions.rowData.length + " Files";
            $('#UPLFILE').text(Files);
        }
        $scope.gridOptions.api.setRowData(rowData);
        for (var item in $scope.TableDataArr) {
            //$scope.SelectedValues[i].FilePath = rowData.map(a => a.Name); 
            $scope.SelectedValues[item].FilePath = "";
            for (var j in rowData) {
                if (rowData[j].SubCatName == $scope.TableDataArr[item].BCL_SUB_NAME) {
                    $scope.SelectedValues[item].FilePath = rowData[j].Name;
                }
            }
        }
    };

    $scope.AllClear = function () {
        $scope.clear();
        $scope.SelectedDraftLocation.InspectdDate = '';
        $scope.SelectedDraftLocation.LcmCode = '';
        $scope.LoadData();
    }

    ///////////////////////////

    $scope.clear = function () {
        $scope.CheckList.OVERALL_CMTS = "";
        $("input:radio").removeAttr("checked");
        $("input:text").val("");
        $("textarea").each(function () {
            $(this).val("");
        });
        $scope.CheckList.SVR_FROM_DATE = "";
        angular.forEach($scope.CNP_NAME, function (CNP_NAME) {
            CNP_NAME.ticked = false;
        });

        angular.forEach($scope.Country, function (Country) {
            Country.ticked = false;
        });
        angular.forEach($scope.City, function (City) {
            City.ticked = false;
        });
        angular.forEach($scope.Location, function (Location) {
            Location.ticked = false;
        });
        angular.forEach($scope.Inspection, function (Inspection) {
            Inspection.ticked = false;
        });
        angular.forEach($scope.SelectedValues, function (SelectedValues) {
            SelectedValues.ticked = false;
        });
        $scope.SelectedValues = [];

        rowData = [];
        MultiUploadData = [];
        $scope.gridOptions.api.setRowData(rowData);
        $('#UPLFILE').val("");
    }

    var MultiUploadData = [];

    $scope.fileNameChanged = function (fu, typ, BCL_SUB_NAME) {
        if (rowData.length === 0) {
            rowData = [];
        }
        data = new FormData($('form')[0]);
        for (i = 0; i < fu.files.length; i++) {
            var gridLength = $scope.gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    //rowData.splice(0, 1);
                    $scope.gridOptions.api.setRowData(rowData);
                }
            }
            rowData.push({ SubCatName: BCL_SUB_NAME, Name: fu.files[i].name, file: fu.files[i] });
            if (typ != 'F')
                MultiUploadData.push({ Imagepath: fu.files[i].name });
            $scope.gridOptions.api.setRowData(rowData);
        }
    };

    CheckListCreationService.getCompany().then(function (response) {
        if (response.data != null) {
            $scope.CNP_NAME = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });


    //CheckListCreationService.getLocations().then(function (response) {
    //    if (response.data != null) {
    //        $scope.Location = response.data;
    //    }
    //    else
    //        showNotification('error', 8, 'bottom-right', response.Message);
    //}, function (response) {
    //    progress(0, '', false);
    //});

    $scope.GetDrafts = function (BCLStatus) {
        if (BCLStatus == 1) {
            //$('#dvbutton').show();
            $('#hsavedtitle').html("Saved Drafts");
        }
        else {
            //$('#dvbutton').hide();
            $('#hsavedtitle').html("Saved CheckLists");
        }
        var BCL_TP_ID = _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',');
        CheckListCreationService.getSavedList(BCLStatus, BCL_TP_ID).then(function (response) {
            if (response.data != null) {
                $scope.CheckList.SaveList = response.data;

            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.DeleteCheckList = function (BCLID, BCLStatus) {
        CheckListCreationService.DeleteCheckList(BCLID).then(function (response) {
            if (response != undefined) {
                $scope.GetDrafts(BCLStatus);
                showNotification('success', 8, 'bottom-right', "Deleted Successfully");
            } else {
                console.log('Submit Error');
            }
        });

    }

    $scope.GetLocationCode = function (items) {
        CheckListCreationService.GetMainType().then(function (response) {
            if (response.data != null) {
                $scope.MainType = response.data;
                var ins = _.find($scope.MainType, { BCL_TP_ID: items.BCL_TP_ID });
                if (ins != undefined) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            ins.ticked = true;
                            $scope.CheckList.MainType.push(ins);
                        });
                    }, 100)
                }
                $scope.EditLoadData(items.BCL_TP_ID, items);
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    var BCLID1 = '';

    $scope.fn_Set = function (items) {
        //$scope.clear();
        if (items.BCL_SUBMIT == 1) {
            $('#dvbutton').show();
        }
        else {
            $('#dvbutton').hide();
        }
        $scope.SelectedDraftLocation = { LcmCode: items.LCM_CODE, InspectdDate: items.BCL_SELECTED_DT };
        //$scope.LoadData('E');
        //$scope.CheckList.SVR_FROM_DATE = $filter('date')(items.BCL_SELECTED_DT, "yyyy-MM-dd");
        $scope.CheckList.SVR_FROM_DATE = $filter('date')(items.BCL_SELECTED_DT, "MM/dd/yyyy");

        //rowData = [];
        //MultiUploadData = [];
        //for (var g = 0; g < data.data[1].length; g++) {
        //    rowData.push({
        //        Name: $scope.UploadedData[g].Name, FilePath: "https://live.quickfms.com/UploadFiles/" + tenant + "/ProjectCheckList/" + $scope.UploadedData[g].Name
        //    });
        //    MultiUploadData.push({ Imagepath: $scope.UploadedData[g].Name });
        //}
        //$scope.gridOptions.api.setRowData(rowData);


        //var lcm = _.find($scope.Location, { LCM_CODE: items.LCM_CODE });
        //if (lcm != undefined) {
        //    setTimeout(function () {
        //        $scope.$apply(function () {
        //            lcm.ticked = true;
        //            $scope.CheckList.Location.push(lcm);
        //        });
        //    }, 100)
        //}

        //var ins = _.find($scope.Inspection, { INSPECTOR: items.BCL_INSPECTED_BY });
        //if (ins != undefined) {
        //    setTimeout(function () {
        //        $scope.$apply(function () {
        //            ins.ticked = true;
        //            $scope.CheckList.Inspection.push(ins);
        //        });
        //    }, 100)
        //}
        $scope.Inspection = [];
        //CheckListCreationService.getInspectors().then(function (response) {
        CheckListCreationService.getInspectorsZonalCentral(items.BCL_TP_ID, items.LCM_CODE, 0).then(function (response) {
            if (response.data != null) {
                $scope.Inspection = response.data;
                var ins = _.find($scope.Inspection, { INSPECTOR: items.BCL_INSPECTED_BY });
                if (ins != undefined) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            ins.ticked = true;
                            $scope.CheckList.Inspection.push(ins);
                        });
                    }, 100)
                }
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });

        var cny = _.find($scope.Country, { CNY_CODE: items.CNY_CODE });

        if (cny != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cny.ticked = true;
                    $scope.CheckList.Country.push(cny);
                });
            }, 100)
        }
        $scope.City = [];
        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                var cty = _.find($scope.City, { CTY_CODE: items.CTY_CODE });
                if (cty != undefined) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            cty.ticked = true;
                            $scope.CheckList.City.push(cty);
                            $scope.Location = [];
                            CheckListCreationService.LocationByCity($scope.CheckList.City).then(function (response) {
                                if (response.data != null) {
                                    $scope.Location = response.data;
                                    var lcm = _.find($scope.Location, { LCM_CODE: items.LCM_CODE });
                                    if (lcm != undefined) {
                                        setTimeout(function () {
                                            $scope.$apply(function () {
                                                lcm.ticked = true;
                                                $scope.CheckList.Location.push(lcm);
                                            });
                                        }, 100)
                                    }
                                    else
                                        $scope.Location = [];
                                }
                            });
                        });
                    }, 100)
                }
            }
        });


        //var cty = _.find($scope.City, { CTY_CODE: items.CTY_CODE });        
        //if (cty != undefined) {
        //    setTimeout(function () {
        //        $scope.$apply(function () {
        //            cty.ticked = true;
        //            $scope.CheckList.City.push(cty);
        //        });
        //    }, 100)
        //}

        var CNP = _.find($scope.CNP_NAME, { CNP_ID: items.LCM_CNP });
        if (CNP != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    CNP.ticked = true;
                    $scope.CheckList.CNP_NAME.push(CNP);
                });
            }, 100)
        }

        var CMTS = items.BCL_OVERALL_CMTS; // _.find($scope.OVERALL_CMTS, { OVERALL_CMTS: items.BCL_OVERALL_CMTS });
        if (CMTS != undefined) {
            //setTimeout(function () {
            //    $scope.$apply(function () { CMTS.ticked = true;
            //        $scope.CheckList.OVERALL_CMTS.push(CMTS);
            //    });
            //}, 100)    
            $scope.CheckList.OVERALL_CMTS = CMTS;
        }

        /////////////////////////////////////////////////////// 
        $scope.GetCheckedListDetail(items.BCL_ID);
        //for (var i in $scope.SelectedValues) {
        //    $scope.SelectedValues[i].txtdata = "ok2";           
        //    $scope.SelectedValues[i].Date = $filter('date')('2022-01-28', "MM/dd/yyyy");
        //    if ($scope.SelectedValues[i].Subtype == 'Multiple') {              
        //        $("input[name='Multiple Sub Cat'][value='Multiple Sub Cat Score1']").prop('checked', true);
        //        $("input[name='Multiple Sub Cat'][value='Multiple Sub Cat Score2']").prop('checked', true);
        //    }
        //    if ($scope.SelectedValues[i].Subtype == 'Radio') {              
        //        $("input[name='Subcategory5'][value='scoretest101']").prop('checked', true);
        //    }
        //}
        //////////////////////////////////////////
        var bclid = items.BCL_ID;
        BCLID1 = bclid
    }
    $scope.CheckedListDetail = [];
    $scope.GetCheckedListDetail = function (BCLID) {
        CheckListCreationService.getCheckedListDetail(BCLID).then(function (response) {
            if (response.data != null) {
                $scope.CheckedListDetail = response.data;
                var rowDataN = [];
                $scope.gridOptions.api.setRowData([]);
                for (var j in $scope.CheckedListDetail) {
                    for (var i in $scope.SelectedValues) {
                        if ($scope.SelectedValues[i].CatCode == $scope.CheckedListDetail[j].BCLD_CAT_CODE && $scope.SelectedValues[i].SubcatCode == $scope.CheckedListDetail[j].BCLD_SUBCAT_CODE) {
                            $scope.SelectedValues[i].txtdata = $scope.CheckedListDetail[j].BCLD_TEXTDATA;
                            $scope.SelectedValues[i].Date = $filter('date')($scope.CheckedListDetail[j].BCLD_DATE, "MM/dd/yyyy");
                            if ($scope.CheckedListDetail[j].BCLD_FILE_UPLD != '' && $scope.CheckedListDetail[j].BCLD_FILE_UPLD != null) {
                                //alert(rowData[1].Name);
                                const index = rowData.findIndex(object => object.Name === $scope.CheckedListDetail[j].BCLD_FILE_UPLD);
                                //if (!rowData.includes($scope.CheckedListDetail[i].BCLD_FILE_UPLD)) {
                                if (index === -1) {
                                    rowData.push({ SubCatName: $scope.CheckedListDetail[j].BCL_SUB_NAME, Name: $scope.CheckedListDetail[j].BCLD_FILE_UPLD });
                                }
                                rowDataN.push({ SubCatName: $scope.CheckedListDetail[j].BCL_SUB_NAME, Name: $scope.CheckedListDetail[j].BCLD_FILE_UPLD });
                                $scope.gridOptions.api.setRowData(rowDataN);
                                $scope.SelectedValues[i].FilePath = $scope.CheckedListDetail[j].BCLD_FILE_UPLD;
                            }
                            if ($scope.SelectedValues[i].Subtype == 'Multiple') {
                                //$scope.BCLDSCORECODE = [];
                                //$("input[name='" + $scope.CheckedListDetail[j].BCL_SUB_NAME + "']").prop('checked', false);
                                //$scope.SelectedValues[i].ScoreCode = $scope.CheckedListDetail[j].BCLD_SCORE_CODE + ",";
                                //$scope.BCLDSCORECODE = $scope.CheckedListDetail[j].BCLD_SCORE_CODE.split(',');
                                //for (var j in $scope.BCLDSCORECODE) {
                                //    $("input[name='" + $scope.CheckedListDetail[j].BCL_SUB_NAME + "'][value='" + $scope.BCLDSCORECODE[j] + "']").attr('checked', true);
                                //    console.log("input[name='" + $scope.CheckedListDetail[j].BCL_SUB_NAME + "'][value='" + $scope.BCLDSCORECODE[j] + "']");
                                //}

                                angular.forEach($scope.TableDataArr, function (values, key) {
                                    if (values.BCL_MC_CODE == $scope.CheckedListDetail[j].BCLD_CAT_CODE && values.BCL_CH_SUB_CODE == $scope.CheckedListDetail[j].BCLD_SUBCAT_CODE) {
                                        angular.forEach(values.childArr, function (values1, key1) {
                                            if (values1.name == $scope.CheckedListDetail[j].BCLD_SCORE_CODE)
                                                //values1.checkedValue = $scope.CheckedListDetail[j].BCLD_SCORE_CODE;
                                                $("input[name='" + $scope.CheckedListDetail[j].BCL_SUB_NAME + "'][value='" + $scope.BCLDSCORECODE[j] + "']").prop('checked', true);
                                        });
                                    }
                                });
                            }
                            if ($scope.SelectedValues[i].Subtype == 'Radio') {
                                $scope.SelectedValues[i].ScoreCode = $scope.CheckedListDetail[j].BCLD_SCORE_CODE;
                                $("input[name='" + $scope.CheckedListDetail[j].BCL_SUB_NAME + "'][value='" + $scope.CheckedListDetail[j].BCLD_SCORE_CODE + "']").attr('checked', true);

                            }
                            if ($scope.SelectedValues[i].Subtype == 'Toggle') {
                                $scope.SelectedValues[i].ScoreCode = $scope.CheckedListDetail[j].BCLD_SCORE_CODE;
                                $("input[name='" + $scope.CheckedListDetail[j].BCL_SUB_NAME + "'][value='" + $scope.CheckedListDetail[j].BCLD_SCORE_CODE + "']").attr('checked', true);

                            }
                        }
                    }
                }
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }


    //UtilityService.getCountires(2).then(function (response) {
    //    if (response.data != null) {
    //        $scope.Country = response.data;

    //        UtilityService.getCities(2).then(function (response) {
    //            if (response.data != null) {
    //                $scope.City = response.data;

    //                angular.forEach($scope.Location, function (Location) {
    //                    Location.ticked = false;
    //                });

    //            }
    //        });
    //    }
    //});



    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;

            angular.forEach($scope.Country, function (value, key) {
                value.ticked = true;
            });
            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;

                    angular.forEach($scope.City, function (value, key) {
                        value.ticked = true;
                    });
                    CheckListCreationService.GetLocationsIn(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;

                            angular.forEach($scope.Location, function (value, key) {
                                value.ticked = true;
                            });
                            CheckListCreationService.GetInspectorsIn(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Inspection = response.data;
                                    angular.forEach($scope.Inspection, function (value, key) {
                                        value.ticked = true;
                                    });
                                }
                            });
                        }
                    });

                }
            });
        }
    });


    $scope.CnyChangeAll = function () {
        $scope.CheckList.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.CheckList.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.CheckList.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.CheckList.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.CheckList.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.CheckList.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        $scope.CheckList.Country = [];

        CheckListCreationService.LocationByCity($scope.CheckList.City).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        //UtilityService.getLocationsByCity($scope.CheckList.City, 2).then(function (response) {
        //    if (response.data != null)
        //        $scope.Location = response.data;
        //    else
        //        $scope.Location = [];
        //});

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.CheckList.Country.push(cny);
            }
        });
    }

    /// Location Events
    $scope.LcmChangeAll = function () {
        $scope.CheckList.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.CheckList.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {

        $scope.CheckList.Country = [];
        $scope.CheckList.City = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.CNP_NAME, function (value, key) {
            value.ticked = false;
        })

        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.CheckList.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.CheckList.City.push(cty);
            }
        });

        angular.forEach($scope.Location, function (value, key) {
            var cnp = _.find($scope.CNP_NAME, { CNP_ID: value.LCM_CNP });
            if (cnp != undefined && value.ticked == true && cnp.ticked == false) {
                cnp.ticked = true;
                $scope.CheckList.CNP_NAME.push(cnp);
            }
        });

        //angular.forEach($scope.Location, function (value, key) {
        //    var CMTS = _.find($scope.OVERALL_CMTS, { OVERALL_CMTS: value.OVERALL_CMTS });
        //    if (CMTS != undefined && value.ticked == true && CMTS.ticked == false) {
        //        CMTS.ticked = true;
        //        $scope.CheckList.OVERALL_CMTS.push(CMTS);
        //    }
        //});
        var tpID = (_.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(','));
        var lcmID = (_.filter($scope.Location, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','));
        CheckListCreationService.getInspectorsZonalCentral(tpID, lcmID, 0).then(function (response) {
            if (response.data != null) {
                $scope.Inspection = response.data;
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    //CheckListCreationService.getInspectors().then(function (response) {
    //    if (response.data != null) {
    //        $scope.Inspection = response.data;
    //    }
    //    else
    //        showNotification('error', 8, 'bottom-right', response.Message);
    //}, function (response) {
    //    progress(0, '', false);
    //});

    CheckListCreationService.GetMainType().then(function (response) {
        if (response.data != null) {
            $scope.MainType = response.data;
            angular.forEach($scope.MainType, function (value, key) {
                var a = _.find($scope.MainType);
                a.ticked = true;
            });
            $scope.LoadData();
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    var sb;
    $scope.LoadData = function (typ) {

        $scope.SelectedValues[0];

        $scope.TableDataArr = [];
        var BCL_TP_ID = _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',');
        CheckListCreationService.GetGriddata(BCL_TP_ID, $scope.SelectedDraftLocation).then(function (response) {

            var data = response.data[0];
            //for (var i in $scope.SelectedValues) {
            //    $scope.SelectedValues[i].childArr = $scope.SelectedValues[0].SCORE.split(',');
            //}
            //console.log($scope.SelectedValues);
            var Ins = _.find($scope.Inspection, { INSPECTOR: data[0].BCL_INSPECTED_BY });
            if (Ins != undefined) {
                setTimeout(function () {
                    $scope.$apply(function () {
                        Ins.ticked = true;
                        $scope.CheckList.Inspection.push(Ins);
                    });
                }, 100)
            }

            $scope.SelectedValues = [];
            var id = 1;
            for (var i in data) {
                var obj = {};
                if (data[i].BCL_SUB_TYPE == 'Text') {//data[i].BCL_SUB_TYPE == 'Multiple' || 
                    data[i].childArr = data[i].SCORE.split(',');
                    ///////////////////////////////////////////////////////
                    //for (var j in data[i].childArr) {
                    //    var obj = {};
                    //    if (typ == 'E') {
                    //        if (data[i].BCL_CH_SUB_CODE == 'S21' || data[i].BCL_CH_SUB_CODE == "S55") {
                    //            obj.ScoreCode = (data[i].BCLD_SCORE_CODE.split(',')[j] ? data[i].BCLD_SCORE_CODE.split(',')[j] : '');
                    //            if (data[i].BCL_CH_SUB_CODE == "S55") {
                    //                obj.Date = (data[i].BCLD_DATE.split(',')[j] ? data[i].BCLD_DATE.split(',')[j] : '');//obj.Date = new window.Date(data[i].BCLD_DATE.split(',')[j]).toISOString();
                    //                obj.SubScore = (data[i].BCLD_SUB_SCORE.split(',')[j] ? data[i].BCLD_SUB_SCORE.split(',')[j] : '');
                    //            }
                    //        }
                    //        else {
                    //            obj.ScoreCode = (data[i].BCLD_SCORE_CODE ? data[i].BCLD_SCORE_CODE : '');
                    //            obj.Date = (data[i].BCLD_DATE ? data[i].BCLD_DATE : '');
                    //            obj.SubScore = (data[i].BCLD_SUB_SCORE ? data[i].BCLD_SUB_SCORE : '');
                    //        }
                    //    }
                    //    else {
                    //        obj.ScoreCode = (data[i].BCLD_SCORE_CODE ? data[i].BCLD_SCORE_CODE : '');
                    //        obj.Date = (data[i].BCLD_DATE ? data[i].BCLD_DATE : '');
                    //        obj.SubScore = (data[i].BCLD_SUB_SCORE ? data[i].BCLD_SUB_SCORE : '');
                    //    }
                    //    obj.SubDateName = "";
                    //    obj.SubScoreName = (data[i].BCL_SUB_NAME ? data[i].BCL_SUB_NAME : '');
                    //    obj.CatCode = (data[i].BCL_MC_CODE ? data[i].BCL_MC_CODE : '');
                    //    obj.ScoreName = (data[i].childArr[j] ? data[i].childArr[j] : '');
                    //    obj.SubcatCode = (data[i].BCL_CH_SUB_CODE ? data[i].BCL_CH_SUB_CODE : '');
                    //    obj.Subtype = (data[i].BCL_SUB_TYPE ? data[i].BCL_SUB_TYPE : '');
                    //    obj.row = id;
                    //    obj.ticked = false;
                    //    obj.txtdata = (data[i].BCLD_TEXTDATA ? data[i].BCLD_TEXTDATA : '');
                    //    id++;                       
                    //    $scope.SelectedValues.push(obj);
                    //}
                    /////////////////////////////////////////////////////////////////
                    //$scope.ScoreName = [];
                    $scope.ScoreName = "";
                    for (var j in data[i].childArr) {
                        //$scope.ScoreName.push((data[i].childArr[j] ? data[i].childArr[j] : ''));
                        var dtVal = (data[i].childArr[j] ? data[i].childArr[j] : '');
                        $scope.ScoreName += dtVal + ",";
                    }
                    $scope.ScoreName = $scope.ScoreName.substr(0, $scope.ScoreName.length - 1);
                    //obj.ScoreCode = (data[i].BCLD_SCORE_CODE ? data[i].BCLD_SCORE_CODE : '');
                    obj.ScoreCode = $scope.ScoreName;
                    obj.Date = (data[i].BCLD_DATE ? data[i].BCLD_DATE : '');
                    obj.SubScore = (data[i].BCLD_SUB_SCORE ? data[i].BCLD_SUB_SCORE : '');
                    obj.SubDateName = "";
                    obj.SubScoreName = (data[i].BCL_SUB_NAME ? data[i].BCL_SUB_NAME : '');
                    obj.CatCode = (data[i].BCL_MC_CODE ? data[i].BCL_MC_CODE : '');
                    //obj.ScoreName = $scope.ScoreName;
                    obj.ScoreName = (data[i].BCLD_SCORE_NAME ? data[i].BCLD_SCORE_NAME : '');
                    obj.SubcatCode = (data[i].BCL_CH_SUB_CODE ? data[i].BCL_CH_SUB_CODE : '');
                    obj.Subtype = (data[i].BCL_SUB_TYPE ? data[i].BCL_SUB_TYPE : '');
                    obj.row = id;
                    obj.ticked = false;
                    obj.txtdata = (data[i].BCLD_TEXTDATA ? data[i].BCLD_TEXTDATA : '');
                    id++;
                    $scope.SelectedValues.push(obj);
                    /////////////////////////////////////////////////////////////////
                }
                else {
                    if (data[i].BCL_SUB_TYPE != 'File') {
                        if (data[i].BCL_SUB_TYPE == 'Multiple') {
                            var dta = data[i].SCORE.split(',');
                            data[i].childArr = [];
                            angular.forEach(dta, function (value, key) {
                                var obj = {};
                                obj.checkedValue = '';
                                obj.name = value;
                                data[i].childArr.push(obj);
                            });
                        }
                        else {
                            data[i].childArr = data[i].SCORE.split(',');
                        }
                        obj.SubDateName = "";
                        obj.SubScoreName = data[i].BCL_SUB_NAME;
                    }
                    else {
                        obj.FilePath = (data[i].BCLD_FILE_UPLD ? data[i].BCLD_FILE_UPLD : '');
                        //MultiUploadData.push({ Imagepath: obj.FilePath });
                        if (obj.FilePath != '') {
                            rowData.push({ Name: obj.FilePath });
                            $scope.gridOptions.api.setRowData(rowData);
                        }

                    }

                    obj.CatCode = (data[i].BCL_MC_CODE ? data[i].BCL_MC_CODE : '');
                    obj.Date = (data[i].BCLD_DATE ? data[i].BCLD_DATE : '');
                    obj.ScoreCode = (data[i].BCLD_SCORE_CODE ? data[i].BCLD_SCORE_CODE : '');
                    obj.ScoreName = (data[i].BCLD_SCORE_NAME ? data[i].BCLD_SCORE_NAME : '');
                    obj.SubScore = (data[i].BCLD_SUB_SCORE ? data[i].BCLD_SUB_SCORE : '');
                    obj.SubcatCode = (data[i].BCL_CH_SUB_CODE ? data[i].BCL_CH_SUB_CODE : '');
                    obj.Subtype = (data[i].BCL_SUB_TYPE ? data[i].BCL_SUB_TYPE : '');
                    obj.row = id;
                    obj.ticked = false;
                    obj.txtdata = (data[i].BCLD_TEXTDATA ? data[i].BCLD_TEXTDATA : '');
                    id++;
                    $scope.SelectedValues.push(obj);
                }
            }
            $scope.TableDataArr = angular.copy(data);

            if (response.data[2].length > 0) {
                rowData.push({ Name: response.data[2][0].Name });
                $scope.gridOptions.api.setRowData(rowData);
            }
            //var z = 1;
            //if (response != null) {
            //    $scope.TotalData = response.data;
            //    var table_body = '<div class="container"><div class="table-responsive"><table class="table table-bordered" id="example"><thead><tr><th class="col-md-3 col-sm-6 col-xs-12"><b>Category</th></b><th class="col-md-3 col-sm-6 col-xs-12"><b>SubCategory</b></th><th class="col-md-3 col-sm-6 col-xs-12"><b>Working Condition</b></th></tr></thead><tbody>';

            //    for (i = 0; i < response.data.length; i++) {
            //        var array = response.data[i]["SCORE"].split(",");
            //        var multiplearray = '';
            //        var multiplescorecode = '';
            //        if (response.data[i]["BCLD_SCORE_NAME"] != null && response.data[i]["BCLD_SCORE_NAME"] != "") {
            //            multiplearray = response.data[i]["BCLD_SCORE_NAME"].split(",");
            //            multiplescorecode = response.data[i]["BCLD_SCORE_CODE"].split(",");
            //        }

            //        if (response.data[i]["BCLD_DATE"] != null) {
            //            var multiplescoredate = response.data[i]["BCLD_DATE"].split(",");
            //        }
            //        table_body += '<tr>';

            //        table_body += '<td style="display:none;" class="col-xs-3">';
            //        table_body += response.data[i]["BCL_MC_CODE"];
            //        table_body += '</td>';

            //        table_body += '<td class="col-xs-3 Fonts">';
            //        table_body += response.data[i]["BCL_MC_NAME"];
            //        table_body += '</td>';

            //        table_body += '<td style="display:none;" class="col-xs-3">';
            //        table_body += response.data[i]["BCL_CH_SUB_CODE"];
            //        table_body += '</td>';

            //        table_body += '<td class="col-xs-3 Fonts">';
            //        table_body += response.data[i]["BCL_SUB_NAME"];
            //        table_body += '</td>';

            //        table_body += '<td style="display:none;" class="col-xs-3">';
            //        table_body += response.data[i]["BCL_SCORE_FLAG"];
            //        table_body += '</td>';


            //        table_body += '<td class="col-xs-6" id="txtRadio">';
            //        $.each(array, function (j) {
            //            if (response.data[i]["BCL_SUB_TYPE"] == 'Radio') {
            //                table_body += '<input type="' + response.data[i]["BCL_SUB_TYPE"] + '" id=Score' + i + ' name=Score' + i + '  value="' + array[j] + '" ';

            //                if (response.data[i].BCLD_SCORE_CODE == array[j] && response.data[i].BCLD_FLAG == 1) {
            //                    table_body += 'checked="checked">' + array[j];
            //                    sb = response.data[i].BCLD_SUB_SCORE
            //                    $("#Score" + i + "").data('events', response.data[i].BCLD_SCORE_CODE).trigger("click");

            //                }

            //                else {
            //                    table_body += '>' + array[j];

            //                }
            //            }
            //            else if (response.data[i]["BCL_SUB_TYPE"] == 'Multiple') {
            //                count = 0;
            //                var textboxid = '<ul class="list-group"><li class="list-group-item">' + array[j] + '<input type="Text" id=' + i + 'textbox' + j + '  name="' + array[j] + '" style="width:30px;"'
            //                var dateid = ' Expiry Date <b>:</b> <input type="Date"  id=' + i + 'Date' + j + '  name=Date' + z + ' style="width:139px;"'
            //                if (multiplearray != undefined && multiplearray != '') {
            //                    if (j < array.length && multiplearray.length >= 1) {
            //                        $.each(multiplearray, function (k) {

            //                            if (array[j] == multiplearray[k] && response.data[i].BCLD_FLAG == 1) {
            //                                table_body += textboxid + 'value="' + multiplescorecode[k] + '"/>'
            //                                $("#" + i + "textbox" + j + "").data('events', multiplescorecode[k]).trigger("change");
            //                                table_body += dateid + 'value="' + $filter('date')(multiplescoredate[k], "yyyy-MM-dd") + '"/>'
            //                                $("#" + i + "Date" + j + "").data('events', multiplescoredate[k]).trigger("change");
            //                                table_body += '<input type="Radio" id="rdScore" value="Green" name=Score' + z + '/>Green'
            //                                table_body += '<input type="Radio" id="rdScore" value="Red" name=Score' + z + '/>Red' + '</li></ul>'
            //                                z++;
            //                                count = count + 1;
            //                            }

            //                        });
            //                        if (count == "0") {
            //                            table_body += textboxid + '/>'
            //                            table_body += dateid + '/>'
            //                            table_body += '<input type="Radio" id="rdScore" value="Green" name=Score' + z + '/>Green'
            //                            table_body += '<input type="Radio" id="rdScore" value="Red" name=Score' + z + '/>Red' + '</li></ul>'
            //                            z++;
            //                        }
            //                    }

            //                }
            //                else {
            //                    table_body += textboxid + '/>'
            //                    table_body += dateid + '/>'
            //                    table_body += '<input type="Radio" id="rdScore" value="Green" name=Score' + z + '/>Green'
            //                    table_body += '<input type="Radio" id="rdScore" value="Red" name=Score' + z + '/>Red' + '</li></ul>'
            //                    z++;
            //                }
            //            }
            //            else if (response.data[i]["BCL_SUB_TYPE"] == 'File') {
            //                table_body += '<input type="' + response.data[i]["BCL_SUB_TYPE"] + '" id="AttachFile" name=Score' + i + '  value="' + array[j] + '" accept=".png,.jpg,.xlsx,.pdf,.docx">' + array[j]
            //            }
            //            else {
            //                count = 0;
            //                var table_Input = '<ul><div class="row"><div class="col-md-3"><li>' + array[j] + '<b>  :</b></div><div class="col-md-6">' + '<input type="Text" onkeypress="AlphaNumeric(event)" id=textbox1' + i + ' name="' + array[j] + '"'
            //                if (multiplearray != undefined && multiplearray != '') {
            //                    if (j < array.length && multiplearray.length >= 1) {
            //                        $.each(multiplearray, function (l) {
            //                            if (array[j] == multiplearray[l] && response.data[i].BCLD_FLAG == 1) {
            //                                table_body += table_Input + 'value="' + multiplescorecode[l] + '">'
            //                                table_body += '</li></div></div></ul>'
            //                                $("#textbox1" + i + "").data('events', response.data[i].BCLD_SCORE_CODE).trigger("change");
            //                                count = count + 1;
            //                            }
            //                            //else {
            //                            //    if (count == "0") {
            //                            //        table_body += table_Input + '></li></div></div></ul>'
            //                            //        count = count + 1;
            //                            //    }
            //                            //}
            //                        });

            //                        if (count == "0") {
            //                            table_body += table_Input + '></li></div></div></ul>'
            //                        }
            //                    }
            //                    //count = count + 1;
            //                }

            //                else {
            //                    table_body += table_Input + '></li></div></div></ul>'
            //                }
            //                //}
            //            }
            //        });
            //        if (response.data[i]["BCL_SUB_TYPE"] == 'Radio') {
            //            table_body += '<textarea id="textarea' + i + '" name=color' + i + ''
            //            if (response.data[i].BCLD_FLAG == 1 && response.data[i].BCLD_TEXTDATA != "") {
            //                table_body += '>' + response.data[i].BCLD_TEXTDATA + '</textarea>'
            //                $("#textarea" + i + "").data('events', response.data[i].BCLD_TEXTDATA).trigger("change");

            //            }
            //            else {
            //                table_body += '></textarea>'

            //            }


            //        }
            //        table_body += '</td>';
            //        table_body += '<td style="display:none;" class="col-xs-3">';
            //        table_body += response.data[i]["BCL_SUB_TYPE"];
            //        table_body += '</td>';

            //        table_body += '</tr>';

            //    }
            //    table_body += '</tbody></table></div>';
            //    $('#tableDiv').html(table_body);

            //}
            //else {
            //    progress(0, '', false);
            //    showNotification('error', 8, 'bottom-right', 'No Records Found');
            //}
            progress(0, '', false);
        }, function (error) {
        });
    }


    $scope.EditLoadData = function (typ, items) {
        $scope.TableDataArr = [];
        var BCL_TP_ID = typ;//_.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',');
        CheckListCreationService.GetGriddata(BCL_TP_ID, $scope.SelectedDraftLocation).then(function (response) {

            var data = response.data[0];
            //for (var i in $scope.SelectedValues) {
            //    $scope.SelectedValues[i].childArr = $scope.SelectedValues[0].SCORE.split(',');
            //}
            //console.log($scope.SelectedValues);
            var Ins = _.find($scope.Inspection, { INSPECTOR: data[0].BCL_INSPECTED_BY });
            if (Ins != undefined) {
                setTimeout(function () {
                    $scope.$apply(function () {
                        Ins.ticked = true;
                        $scope.CheckList.Inspection.push(Ins);
                    });
                }, 100)
            }

            $scope.SelectedValues = [];
            var id = 1;
            for (var i in data) {
                var obj = {};
                if (data[i].BCL_SUB_TYPE == 'Text') {//data[i].BCL_SUB_TYPE == 'Multiple' || 
                    data[i].childArr = data[i].SCORE.split(',');

                    $scope.ScoreName = "";
                    for (var j in data[i].childArr) {
                        //$scope.ScoreName.push((data[i].childArr[j] ? data[i].childArr[j] : ''));
                        var dtVal = (data[i].childArr[j] ? data[i].childArr[j] : '');
                        $scope.ScoreName += dtVal + ",";
                    }
                    $scope.ScoreName = $scope.ScoreName.substr(0, $scope.ScoreName.length - 1);
                    //obj.ScoreCode = (data[i].BCLD_SCORE_CODE ? data[i].BCLD_SCORE_CODE : '');
                    obj.ScoreCode = $scope.ScoreName;
                    obj.Date = (data[i].BCLD_DATE ? data[i].BCLD_DATE : '');
                    obj.SubScore = (data[i].BCLD_SUB_SCORE ? data[i].BCLD_SUB_SCORE : '');
                    obj.SubDateName = "";
                    obj.SubScoreName = (data[i].BCL_SUB_NAME ? data[i].BCL_SUB_NAME : '');
                    obj.CatCode = (data[i].BCL_MC_CODE ? data[i].BCL_MC_CODE : '');
                    //obj.ScoreName = $scope.ScoreName;
                    obj.ScoreName = (data[i].BCLD_SCORE_NAME ? data[i].BCLD_SCORE_NAME : '');
                    obj.SubcatCode = (data[i].BCL_CH_SUB_CODE ? data[i].BCL_CH_SUB_CODE : '');
                    obj.Subtype = (data[i].BCL_SUB_TYPE ? data[i].BCL_SUB_TYPE : '');
                    obj.row = id;
                    obj.ticked = false;
                    obj.txtdata = (data[i].BCLD_TEXTDATA ? data[i].BCLD_TEXTDATA : '');
                    id++;
                    $scope.SelectedValues.push(obj);
                    /////////////////////////////////////////////////////////////////
                }
                else {
                    if (data[i].BCL_SUB_TYPE != 'File') {
                        if (data[i].BCL_SUB_TYPE == 'Multiple') {
                            var dta = data[i].SCORE.split(',');
                            data[i].childArr = [];
                            angular.forEach(dta, function (value, key) {
                                var obj = {};
                                obj.checkedValue = '';
                                obj.name = value;
                                data[i].childArr.push(obj);
                            });
                        } else {
                            data[i].childArr = data[i].SCORE.split(',');
                        }
                        obj.SubDateName = "";
                        obj.SubScoreName = data[i].BCL_SUB_NAME;
                    }
                    else {
                        obj.FilePath = (data[i].BCLD_FILE_UPLD ? data[i].BCLD_FILE_UPLD : '');
                        //MultiUploadData.push({ Imagepath: obj.FilePath });
                        if (obj.FilePath != '') {
                            rowData.push({ Name: obj.FilePath });
                            $scope.gridOptions.api.setRowData(rowData);
                        }

                    }

                    obj.CatCode = (data[i].BCL_MC_CODE ? data[i].BCL_MC_CODE : '');
                    obj.Date = (data[i].BCLD_DATE ? data[i].BCLD_DATE : '');
                    obj.ScoreCode = (data[i].BCLD_SCORE_CODE ? data[i].BCLD_SCORE_CODE : '');
                    obj.ScoreName = (data[i].BCLD_SCORE_NAME ? data[i].BCLD_SCORE_NAME : '');
                    obj.SubScore = (data[i].BCLD_SUB_SCORE ? data[i].BCLD_SUB_SCORE : '');
                    obj.SubcatCode = (data[i].BCL_CH_SUB_CODE ? data[i].BCL_CH_SUB_CODE : '');
                    obj.Subtype = (data[i].BCL_SUB_TYPE ? data[i].BCL_SUB_TYPE : '');
                    obj.row = id;
                    obj.ticked = false;
                    obj.txtdata = (data[i].BCLD_TEXTDATA ? data[i].BCLD_TEXTDATA : '');
                    id++;
                    $scope.SelectedValues.push(obj);
                }
            }
            $scope.TableDataArr = angular.copy(data);

            if (response.data[2].length > 0) {
                rowData.push({ Name: response.data[2][0].Name });
                $scope.gridOptions.api.setRowData(rowData);
            }
            $scope.fn_Set(items);
            progress(0, '', false);
        }, function (error) {
        });
    }

    var ScoreCode = ''
    var ScoreName = '';
    var SubScore = ''
    var txtdata = ''
    var Date = ''
    var SubScoreName = ''
    var SubDateName = ''
    var FilePath = ''
    var data;
    var rowData = [];
    $('#Form1').on('change', 'input[type=File]', function (fu) {
        var newDate = new window.Date();
        var time = newDate.getHours() + ":" + newDate.getMinutes() + ":" + newDate.getSeconds();
        data = new FormData($('form')[0]);
        for (i = 0; i < fu.target.files.length; i++) {
            rowData.push({ Name: fu.target.files[i].name, file: fu.target.files[i] });
            var currentRow = $(this).closest("tr");
            var Index = currentRow[0].rowIndex;
            var CatCode = currentRow.find("td:eq(0)").text();
            var SubcatCode = currentRow.find("td:eq(2)").text();
            var Subtype = currentRow.find("td:eq(6)").text();
            var FilePath = "~/UploadFiles/" + companyid + "/" + fu.target.files[i].name;
            var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
            var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
            if (Exists != undefined) {
                $scope.SelectedValues.splice(ExistingIndex, 1);
                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "FilePath": FilePath, "ticked": "true", "Subtype": Subtype });
            }
            else
                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "FilePath": FilePath, "ticked": "true", "Subtype": Subtype });
        }

    })

    //$scope.TextBoxChange = function (e, thisval) {

    //    var TextValue = thisval.data('events');
    //    var currentRow = thisval.closest("tr");
    //    var Index = currentRow[0].rowIndex;
    //    var CatCode = currentRow.find("td:eq(0)").text();
    //    var SubcatCode = currentRow.find("td:eq(2)").text();
    //    var Subtype = currentRow.find("td:eq(6)").text();
    //    var ScoreCode = thisval.val();

    //    if (TextValue != '' && TextValue != undefined) {
    //        ScoreCode = TextValue;
    //    }
    //    var ScoreName = e.target.name
    //    var SubScore = ''

    //    if (SubcatCode == "S21")
    //        var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index && x.ScoreName == ScoreName) return x });
    //    else
    //        var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
    //    if (Exists != undefined) {

    //        var S = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.ScoreName == ScoreName)) return x });
    //        var ExistingIndex = _.indexOf($scope.SelectedValues, S);
    //        if (ExistingIndex != -1)
    //            $scope.SelectedValues.splice(ExistingIndex, 1);


    //        if (S != undefined) {
    //            $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": S.Date, "SubScore": S.SubScore, "SubScoreName": S.SubScoreName, "SubDateName": S.SubDateName, "ticked": "true", "Subtype": Subtype });
    //        }
    //        else {
    //            var T = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.ScoreName == "" && (x.SubScoreName != "" || x.SubDateName != ""))) return x });
    //            var ExistingIndex = _.indexOf($scope.SelectedValues, T);
    //            if (ExistingIndex != -1)
    //                $scope.SelectedValues.splice(ExistingIndex, 1);

    //            if (T != undefined) {
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": T.Date, "SubScore": T.SubScore, "SubScoreName": T.SubScoreName, "SubDateName": T.SubDateName, "ticked": "true", "Subtype": Subtype });
    //            }


    //            else if (T == undefined) {
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": '', "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
    //            }

    //        }

    //    }
    //    else {
    //        $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": '', "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
    //    }
    //}


    //$('#Form1').on('change', 'input[type=Text]', function (e) {

    //    $scope.TextBoxChange(e, $(this));

    //});

    //$scope.TextareaChange = function (e, thisval) {

    //    var Textarea = thisval.data('events');
    //    var currentRow = thisval.closest("tr");
    //    var Index = currentRow[0].rowIndex;
    //    var CatCode = currentRow.find("td:eq(0)").text();
    //    var SubcatCode = currentRow.find("td:eq(2)").text();
    //    var Subtype = currentRow.find("td:eq(6)").text();
    //    var ScoreCode = ''

    //    if (Textarea != '' && Textarea != undefined) {
    //        ScoreCode = Textarea;
    //    }
    //    txtdata = thisval.val();
    //    var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
    //    var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
    //    if (Exists != undefined) {
    //        $scope.SelectedValues.splice(ExistingIndex, 1);
    //        $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": Exists.ScoreCode, "ScoreName": Exists.ScoreName, "txtdata": txtdata, "Date": Exists.Date, "SubScore": SubScore, "SubScoreName": Exists.SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
    //    }
    //    else {
    //        $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": '', "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
    //    }
    //}

    //$('#Form1').on('change', 'textarea', function (e) {

    //    $scope.TextareaChange(e, $(this));

    //});

    //$scope.DateChange = function (e, thisval) {

    //    var Datedfield = thisval.data('events');
    //    var currentRow = thisval.closest("tr");
    //    var Index = currentRow[0].rowIndex;
    //    var CatCode = currentRow.find("td:eq(0)").text();
    //    var SubcatCode = currentRow.find("td:eq(2)").text();
    //    var Subtype = currentRow.find("td:eq(6)").text();
    //    var SubScore = ''
    //    if (Datedfield != '' && Datedfield != undefined) {
    //        ScoreCode = Datedfield;
    //    }
    //    var SubDateName = ""
    //    Date = thisval.val();
    //    var SubDateName = e.target.name


    //    var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });

    //    if (Exists != undefined) {
    //        var EX = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.SubDateName == SubDateName)) return x });
    //        var ExistingIndex = _.indexOf($scope.SelectedValues, EX);
    //        if (EX != undefined) {
    //            if (ExistingIndex != -1) {
    //                $scope.SelectedValues.splice(ExistingIndex, 1);
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": EX.ScoreCode, "ScoreName": EX.ScoreName, "txtdata": EX.txtdata, "Date": Date, "SubScore": EX.SubScore, "SubScoreName": EX.SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });;
    //            }
    //        }
    //        else {
    //            var EX1 = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.SubDateName == "" && (x.SubScoreName != "" || x.ScoreName != ""))) return x });
    //            var ExistingIndex = _.indexOf($scope.SelectedValues, EX1);
    //            if (ExistingIndex != -1) {
    //                $scope.SelectedValues.splice(ExistingIndex, 1);
    //            }
    //            if (EX1 != undefined) {
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": EX1.ScoreCode, "ScoreName": EX1.ScoreName, "txtdata": EX1.txtdata, "Date": Date, "SubScore": EX1.SubScore, "SubScoreName": EX1.SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });;
    //            }
    //            else {
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });;
    //            }

    //        }

    //    }
    //    else {
    //        $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
    //    }
    //}

    //$('#Form1').on('change', 'input[type=Date]', function (e) {

    //    $scope.DateChange(e, $(this));

    //});

    //$scope.RadioButtonChange = function (e, thisval) {

    //    var subscoreval = thisval.data('events');
    //    var currentRow = thisval.closest("tr");
    //    var Index = currentRow[0].rowIndex;
    //    var CatCode = currentRow.find("td:eq(0)").text();
    //    var SubcatCode = currentRow.find("td:eq(2)").text();
    //    var Subtype = currentRow.find("td:eq(6)").text();
    //    var ScoreCode = thisval.val();
    //    if (subscoreval != '' && subscoreval != undefined) {
    //        ScoreCode = subscoreval;
    //    }
    //    SubScoreName = "";
    //    if (SubcatCode == 'S55' || SubcatCode == 'S14') {
    //        var SubScoreName = e.target.name
    //    }
    //    if (SubcatCode == 'S65' || SubcatCode == 'S66') {
    //        var ScoreName = e.target.name
    //    }
    //    var Existsubradio = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });

    //    if (Existsubradio != undefined && SubScoreName != "ABC") {
    //        $('#LblRed1').remove();
    //        $('#LblRed2').remove();

    //    }

    //    if (SubcatCode == 'S14' && SubScoreName != "ABC") {
    //        if (sb == 'Yes') {
    //            $(thisval).closest("td").append('<label id="LblRed1"><input type="Radio" id="Red1" value="Yes"name="ABC" checked="checked">Yes</label>');
    //            $(thisval).closest("td").append('<label id="LblRed2"><input type="Radio" id="Red2" value="No" name="ABC">No</label>');

    //        }
    //        else if (sb == 'No') {
    //            $(thisval).closest("td").append('<label id="LblRed1"><input type="Radio" id="Red1" value="Yes"name="ABC">Yes</label>');
    //            $(thisval).closest("td").append('<label id="LblRed2"><input type="Radio" id="Red2" value="No" name="ABC" checked="checked">No</label>');

    //        }
    //        else {
    //            $(thisval).closest("td").append('<label id="LblRed1"><input type="Radio" id="Red1" value="Yes"name="ABC">Yes</label>');
    //            $(thisval).closest("td").append('<label id="LblRed2"><input type="Radio" id="Red2" value="No" name="ABC">No</label>');
    //        }

    //    }
    //    else
    //        if (SubcatCode == 'S65' && ScoreCode == 'Yes') {
    //            $(thisval).closest("td").append('<label id="YesDate"><input type="Date" id="DateS65"  name="Validity">Validity</label>');
    //        }
    //        else if (SubcatCode == 'S66' && ScoreCode == 'Yes') {
    //            $(thisval).closest("td").append('<label id="YesDate1"><input type="Date" id="Date1S66" name="Validity">Validity</label>');
    //        }
    //        else {
    //            if (SubcatCode == 'S65' && ScoreCode == 'No') {
    //                $('#YesDate').remove();
    //            }
    //            if (SubcatCode == 'S66' && ScoreCode == 'No') {
    //                $('#YesDate1').remove();
    //            }

    //        }
    //    if (SubcatCode == 'S55')
    //        var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index && x.SubScoreName == SubScoreName) return x });
    //    else
    //        var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
    //    var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
    //    if (Exists != undefined) {
    //        if (ExistingIndex != -1)
    //            $scope.SelectedValues.splice(ExistingIndex, 1);
    //        if (e.target.id == "Red1" || e.target.id == "Red2" || SubcatCode == "S55") {
    //            $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": Exists.ScoreCode, "ScoreName": Exists.ScoreName, "txtdata": txtdata, "Date": Exists.Date, "SubScore": ScoreCode, "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
    //        }
    //        else {
    //            if (ScoreCode == "Yes")
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": Exists.txtdata, "Date": Exists.Date, "SubScore": "", "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
    //            else
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": Exists.txtdata, "Date": "", "SubScore": "", "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
    //        }

    //    }
    //    else {
    //        var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.SubScoreName == "" && (x.ScoreName != "" || x.SubDateName != ""))) return x });
    //        var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
    //        if (Exists != undefined) {
    //            if (ExistingIndex != -1)
    //                $scope.SelectedValues.splice(ExistingIndex, 1);
    //            $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": Exists.ScoreCode, "ScoreName": Exists.ScoreName, "txtdata": '', "Date": Exists.Date, "SubScore": ScoreCode, "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
    //        }
    //        else {
    //            if (SubcatCode == "S55") {
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": '', "ScoreName": '', "txtdata": '', "Date": '', "SubScore": ScoreCode, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
    //            }
    //            else {
    //                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": '', "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
    //            }

    //        }


    //    }
    //}


    //$('#Form1').on('change', 'input[type=Radio]', function (e) {


    //    $scope.RadioButtonChange(e, $(this));

    //})

    //$(document).ready(function () {
    //    $("#tableDiv").on("change", "textarea,input,input[type=Date]", function () {
    //        //var $selects = $(this).closest('tr').find('td select'),
    //        var currentRow = $(this).closest("tr");
    //        var SubcatCode = currentRow.find("td:eq(2)").text();
    //        var curr = $(this).val();
    //        var $textareas = $(this).closest('tr').find('td  textarea'),
    //            $cells = $(this).closest("tr").find("td input");
    //        $dates = $(this).closest('tr').find("td input[type=Date]")
    //        $cells.removeClass("has-error3");
    //        $textareas.removeClass("has-error2");
    //        //$selects.removeClass("has-error");
    //        $cells.each(function () {
    //            if ($(this).val().trim() !== '') {
    //                $(this).addClass("has-error3");
    //            }
    //        });
    //        //$selects.each(function () {
    //        //    if ($(this).val() == 'NA') {
    //        //        $(this).addClass("has-error");
    //        //    }
    //        //});

    //        $textareas.each(function () {
    //            if ($(this).val().trim() === '' && (curr === 'Needs work' || curr === 'New Required' || (curr === 'No' && SubcatCode != 'S68' && SubcatCode != 'S15') || (curr === 'Yes' && SubcatCode === 'S68') || (curr === 'Yes' && SubcatCode === 'S15'))) {
    //                $(this).addClass("has-error2");
    //            }
    //        });


    //    });
    //});
    $scope.stateChanged = function (dtVal, BCL_SUB_NAME) {
        //$scope.strrr = [];
        //const index = array.indexOf(item);
        //if (index !== -1) array.splice(index, 1);       
        for (var i in $scope.SelectedValues) {
            if ($scope.SelectedValues[i].Subtype == "Multiple" && $scope.SelectedValues[i].SubScoreName == BCL_SUB_NAME) { //If it is checked
                if ($scope.SelectedValues[i].ScoreCode.includes(dtVal)) {//string.indexOf(substring) !== -1
                    $scope.SelectedValues[i].ScoreCode = process($scope.SelectedValues[i].ScoreCode, dtVal);
                }
                else {
                    $scope.SelectedValues[i].ScoreCode += dtVal + ",";
                }
            }
            //$scope.SelectedValues[i].ScoreCode = $scope.SelectedValues[i].ScoreCode.substr(0, $scope.SelectedValues[i].ScoreCode.length - 1);
        }

    }
    function process(csv, valueToDelete) {
        var tmp = "," + csv;
        tmp = tmp.replace("," + valueToDelete, "");
        if (tmp.substr(0, 1) == ',') tmp = tmp.substr(1);
        return tmp;
    }
    $scope.validationFn = function (typ, id) {
        var flagchk = false;
        if (typ == 'A') {
            for (var i in $scope.SelectedValues) {
                //if ($scope.SelectedValues[i].Subtype == 'Radio' && $scope.SelectedValues[i].txtdata == '' && ((($scope.SelectedValues[i].SubcatCode == 'S68' || $scope.SelectedValues[i].SubcatCode == 'S15') &&
                //    $scope.SelectedValues[i].ScoreCode == 'Yes') || (($scope.SelectedValues[i].SubcatCode != 'S68' && $scope.SelectedValues[i].SubcatCode != 'S15') &&
                //        $scope.SelectedValues[i].ScoreCode == 'No') || $scope.SelectedValues[i].ScoreCode == 'Needs work' || $scope.SelectedValues[i].ScoreCode == 'New Required')) {
                //    $scope.classActive = 'has-error2';
                //    flagchk = true;
                //    alert("tt");
                //    break;
                //}
                if ($scope.SelectedValues[i].Subtype == 'Radio' && $scope.SelectedValues[i].ScoreCode == '') {
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                    break;
                }
                if ($scope.SelectedValues[i].Subtype == 'Toggle' && $scope.SelectedValues[i].ScoreCode == '') {
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                    break;
                }
                if ($scope.SelectedValues[i].Subtype == "Multiple" && $scope.SelectedValues[i].ScoreCode == '') {//($scope.SelectedValues[i].ScoreCode == false || $scope.SelectedValues[i].Date == "")
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                    break;
                }
                if ($scope.SelectedValues[i].Subtype == "Multiple" && $scope.SelectedValues[i].ScoreCode != '') {
                    $scope.SelectedValues[i].ScoreCode = $scope.SelectedValues[i].ScoreCode.substr(0, $scope.SelectedValues[i].ScoreCode.length - 1);
                }
                if ($scope.SelectedValues[i].Subtype == "Text" && $scope.SelectedValues[i].ScoreCode == '') {
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                    break;
                }

                if ($scope.SelectedValues[i].Subtype == "File" && $scope.SelectedValues[i].FilePath == '') {
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                    break;
                }
            }
            if (flagchk) {
                flagchk = true;
                showNotification('error', 8, 'bottom-right', "Please Enter  All The Fields ");
            }
            else
                flagchk = false;
        }
        if (typ == 'V') {
            if ($scope.TataCapitalCHK.SYSP_VAL1 == 1) {

                if ($scope.SelectedValues[id].txtdata == '' && ((($scope.SelectedValues[id].SubcatCode == 'S68' /*|| $scope.SelectedValues[id].SubcatCode == 'S15'*/) &&
                    $scope.SelectedValues[id].ScoreCode == 'Yes') || (($scope.SelectedValues[id].SubcatCode != 'S68' /*&& $scope.SelectedValues[id].SubcatCode != 'S15'*/) &&
                        $scope.SelectedValues[id].ScoreCode == 'No') || $scope.SelectedValues[id].ScoreCode == 'Needs work' || $scope.SelectedValues[id].ScoreCode == 'New Required')) {
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                }
                else if ($scope.SelectedValues[id].Subtype == "Multiple" && (($scope.SelectedValues[id].SubScore == "") || ($scope.SelectedValues[id].Date == ""))) {
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                }
                else if ((($scope.SelectedValues[id].SubcatCode == "S65") || ($scope.SelectedValues[id].SubcatCode == "S66")) && ($scope.SelectedValues[id].Date == '' && $scope.SelectedValues[id].ScoreCode == "Yes")) {
                    $scope.classActive = 'has-error2';
                    flagchk = true;
                }
                //else if ((($scope.SelectedValues[id].SubcatCode == "S14") && ($scope.SelectedValues[id].SubScore == ""))) {
                //    $scope.classActive = 'has-error2';
                //    flagchk = true;
                //}
                else {
                    $scope.classActive = '';
                    flagchk = false;
                }
            }

            else {
                //if ($scope.SelectedValues[id].txtdata == '' && ((($scope.SelectedValues[id].SubcatCode == 'S68' || $scope.SelectedValues[id].SubcatCode == 'S15') &&
                //    $scope.SelectedValues[id].ScoreCode == 'Yes') || (($scope.SelectedValues[id].SubcatCode != 'S68' && $scope.SelectedValues[id].SubcatCode != 'S15') &&
                //        $scope.SelectedValues[id].ScoreCode == 'No') || $scope.SelectedValues[id].ScoreCode == 'Needs work' || $scope.SelectedValues[id].ScoreCode == 'New Required')) {
                //    $scope.classActive = 'has-error2';
                //    alert('kk');
                //    flagchk = true;
                //}
                //else if ($scope.SelectedValues[id].Subtype == "Multiple" && (($scope.SelectedValues[id].SubScore == "") || ($scope.SelectedValues[id].Date == ""))) {
                //    $scope.classActive = 'has-error2';
                //    alert('mm');
                //    flagchk = true;
                //}
                //else if ((($scope.SelectedValues[id].SubcatCode == "S65") || ($scope.SelectedValues[id].SubcatCode == "S66")) && ($scope.SelectedValues[id].Date == '' && $scope.SelectedValues[id].ScoreCode == "Yes")) {
                //    $scope.classActive = 'has-error2';
                //    alert('nn');
                //    flagchk = true;
                //}
                //else if ((($scope.SelectedValues[id].SubcatCode == "S14") && ($scope.SelectedValues[id].SubScore == ""))) {
                //    $scope.classActive = 'has-error2';
                //    alert('ll');
                //    flagchk = true;
                //}
                //else {
                $scope.classActive = '';
                flagchk = false;
                //}
            }

        }
        else {
            //for (var x in $scope.SelectedValues) {
            //    if ($scope.SelectedValues[x].txtdata == '' && ((($scope.SelectedValues[x].SubcatCode == 'S68' || $scope.SelectedValues[x].SubcatCode == 'S15') &&
            //        $scope.SelectedValues[x].ScoreCode == 'Yes') || (($scope.SelectedValues[x].SubcatCode != 'S68' && $scope.SelectedValues[x].SubcatCode != 'S15') &&
            //            $scope.SelectedValues[x].ScoreCode == 'No') || $scope.SelectedValues[x].ScoreCode == 'Needs work' || $scope.SelectedValues[x].ScoreCode == 'New Required')) {
            //        $scope.classActive = 'has-error2';
            //        flagchk = true;
            //    }
            //    else if ($scope.SelectedValues[x].Subtype == "Multiple" && (($scope.SelectedValues[x].SubScore == "") || ($scope.SelectedValues[x].Date == ""))) {
            //        $scope.classActive = 'has-error2';
            //        flagchk = true;
            //    }
            //    else if ((($scope.SelectedValues[x].SubcatCode == "S65") || ($scope.SelectedValues[x].SubcatCode == "S66")) && ($scope.SelectedValues[x].Date == '' && $scope.SelectedValues[x].ScoreCode == "Yes")) {
            //        $scope.classActive = 'has-error2';
            //        flagchk = true;
            //    }
            //    else if ((($scope.SelectedValues[x].SubcatCode == "S14") && ($scope.SelectedValues[x].SubScore == ""))) {
            //        $scope.classActive = 'has-error2';
            //        flagchk = true;
            //    }
            //    else {
            //        $scope.classActive = '';
            //        flagchk = false;
            //    }
            //}
        }
        return flagchk;
    }

    $scope.Submit = function () {
        //var table = document.getElementById('example'),
        //    rows = table.getElementsByTagName('tr'),
        //    i, j, cells, customerId, flagchk;

        //for (i = 0, j = rows.length; i < j; ++i) {
        //    flagchk = true;
        //    cells = rows[i].getElementsByTagName('td');
        //    if (!cells.length) {
        //        continue;
        //    }
        //    for (var k = 0; k < $scope.SelectedValues.length; k++) {
        //       if ($scope.SelectedValues[k].Subtype == "Multiple" && (($scope.SelectedValues[k].SubScore == "") || ($scope.SelectedValues[k].Date == ""))) {
        //            //cells[5].classList.add("mystyle");
        //            $scope.classActive = 'has-error2';
        //            flagchk = true;

        //        }
        //        else if ((($scope.SelectedValues[k].SubcatCode == "S65") || ($scope.SelectedValues[k].SubcatCode == "S66")) && ($scope.SelectedValues[k].Date == '' && $scope.SelectedValues[k].ScoreCode == "Yes")) {
        //            //cells[5].classList.add("mystyle");
        //            $scope.classActive = 'has-error2';
        //            flagchk = true;

        //        }
        //        else if ((($scope.SelectedValues[k].SubcatCode == "S14") && ($scope.SelectedValues[k].SubScore == ""))) {
        //            //cells[5].classList.add("mystyle");
        //            $scope.classActive = 'has-error2';
        //            flagchk = true;

        //        }
        //        else if ($scope.SelectedValues[k].row == i && $scope.SelectedValues.length != 0) {
        //            //cells[5].classList.remove("mystyle");
        //            $scope.classActive = '';
        //            flagchk = false;
        //        }
        //    }
        //    if (flagchk) {
        //        $scope.classActive = 'has-error2';
        //        //cells[5].classList.add("mystyle");
        //    }

        ////}

        //angular.forEach($scope.SelectedValues, function (x) {
        //    if (x.SubcatCode == "S65" || x.SubcatCode == "S66")
        //        x.ScoreName = "";

        //});


        if ($scope.TotalData.length <= $scope.SelectedValues.length && !$scope.validationFn('A')) {
            $scope.data = {
                LCMLST: $scope.CheckList.Location,
                LcmList: $scope.CheckList.Location[0].LCM_CODE,
                InspectdBy: $scope.CheckList.Inspection[0].INSPECTOR,
                date: $scope.CheckList.SVR_FROM_DATE,
                /*SelCompany: $scope.CheckList.CNP_NAME[0].CNP_ID,*/
                SelCompany: '1',
                Seldata: $scope.SelectedValues,
                Flag: 2,
                OVERALL_CMTS: $scope.CheckList.OVERALL_CMTS,
                imagesList: MultiUploadData,
                BCL_TYPE_ID: $scope.CheckList.MainType[0].BCL_TP_ID,
                BCL_ID: BCLID1
            };
            CheckListCreationService.InsertCheckList($scope.data).then(function (response) {
                if (response != null) {
                    $.ajax({
                        type: "POST",
                        url: UtilityService.path + '/api/CheckListCreation/UploadFiles',
                        // url: 'https://live.quickfms.com/api/CheckListCreation/UploadFiles',    // CALL WEB API TO SAVE THE FILES.
                        contentType: false,
                        processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                        cache: false,
                        data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                        success: function (data, textStatus, xhr) {
                            showNotification('success', 8, 'bottom-right', response.Message);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            //alert(textStatus + ': ' + errorThrown);
                        }
                    });
                    setTimeout(function () {
                        showNotification('success', 8, 'bottom-right', response.data);
                    }, 500);
                    $scope.clear();
                    $scope.SelectedDraftLocation.InspectdDate = '';
                    $scope.SelectedDraftLocation.LcmCode = '';
                    $scope.LoadData();
                }
                progress(0, '', false);
            }, function (error) {
            });
        }
        else {
            showNotification('error', 8, 'bottom-right', "Please Enter  All The Fields ");
        }
    }



    $scope.Save = function () {
        //if ($scope.TableDataArr.length <= 0) {
        //if (!$scope.validationFn) {
        angular.forEach($scope.SelectedValues, function (x) {
            if (x.SubcatCode == "S65" || x.SubcatCode == "S66")
                x.ScoreName = "";
            if (x.Subtype == "Multiple" && x.ScoreCode != '') {
                //x.ScoreCode = x.ScoreCode.substr(0, x.ScoreCode.length - 1);               
                x.ScoreCode = x.ScoreCode.replace(",,", ",");
            }
        });
        $scope.data = {
            LCMLST: $scope.CheckList.Location,
            LcmList: $scope.CheckList.Location[0].LCM_CODE,
            InspectdBy: $scope.CheckList.Inspection[0].INSPECTOR,
            date: $scope.CheckList.SVR_FROM_DATE,
            /*SelCompany: $scope.CheckList.CNP_NAME[0].CNP_ID,*/
            SelCompany: '1',
            Seldata: $scope.SelectedValues,
            Flag: 1,
            OVERALL_CMTS: $scope.CheckList.OVERALL_CMTS,
            //OVERALL_CMTS: $scope('#OVER_CMTS').val(),
            imagesList: MultiUploadData,
            BCL_TYPE_ID: $scope.CheckList.MainType[0].BCL_TP_ID,
            BCL_ID: BCLID1
        };
        CheckListCreationService.InsertCheckList($scope.data).then(function (response) {
            if (response != null) {
                $.ajax({
                    type: "POST",
                    url: UtilityService.path + '/api/CheckListCreation/UploadFiles',
                    //url: 'https://live.quickfms.com/api/CheckListCreation/UploadFiles',    // CALL WEB API TO SAVE THE FILES.                        
                    contentType: false,
                    processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                    cache: false,
                    data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                    success: function (data, textStatus, xhr) {
                        showNotification('success', 8, 'bottom-right', response.Message);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }
                });
                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 500);
                $scope.clear();
                $scope.SelectedDraftLocation.InspectdDate = '';
                $scope.SelectedDraftLocation.LcmCode = '';
                $scope.LoadData();
                //location.reload();
            }
            progress(0, '', false);
        }, function (error) {
        });
        //$scope.GetDrafts();
        //}
        //else {
        //    showNotification('error', 8, 'bottom-right', "You have not any field to enter.Please Add masters. ");
        //}

    }
    //$scope.GetDrafts();
});