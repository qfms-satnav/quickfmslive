﻿app.service("ZFMReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        console.log(data);
        return $http.post(UtilityService.path + '/api/ZFMReport/BindGrid', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGriddataPivotExcel = function (data) {
        deferred = $q.defer();
        console.log(data);
        return $http.post(UtilityService.path + '/api/ZFMReport/BindGridpivot', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ZFMReport/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.DownloadDocuments = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/ZFMReport/DownloadDocuments', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetAttachmentwise = function (data) {
        deferred = $q.defer();
        console.log(data);
        return $http.post(UtilityService.path + '/api/ZFMReport/GetAttachmentwise', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});


app.controller('ZFMReportController', function ($scope, ZFMReportService, UtilityService, $timeout) {

    $scope.ZFMReport = {};
    $scope.ZFMReport.FromDate = moment().format('MM/DD/YYYY');
    $scope.ZFMReport.ToDate = moment().format('MM/DD/YYYY');
   // $scope.ZfmReport.FromDate = moment().format('MM/DD/YYYY'); ZfmReport.SelectedDate
 //  $scope.ZfmReport.UserID = UserID;

  //  $scope.ZfmReport.ToDate = moment().format('MM/DD/YYYY');

    $scope.columnDefs = [
        { headerName: "Inspection ID", field: "BCL_ID", width: 190, cellClass: 'grid-align', width: 110 },
        { headerName: "Zone", field: "Zone", width: 190, cellClass: 'grid-align', width: 110 },
        { headerName: "Inspected By", field: "SecurityName", width: 190, cellClass: 'grid-align', width: 110 },
        { headerName: "Designation", field: "DESIGNATION", width: 190, cellClass: 'grid-align', width: 110 },
        { headerName: "City Name", field: "CITY", cellClass: 'grid-align', width: 110 },
        { headerName: "Location Name", field: "LocationName", cellClass: 'grid-align', width: 110 },
        { headerName: "Login status", field: "Login_status", cellClass: 'grid-align', width: 110 },
        { headerName: "Inspected Status", field: "SG_Status", cellClass: 'grid-align', width: 110 },
        { headerName: "Checklist Type", field: "ChecklistType", cellClass: 'grid-align', width: 110 },
        { headerName: "Main Category", field: "MainCategory", cellClass: 'grid-align', width: 110 },
        { headerName: "Sub Category", field: "SubCategory", cellClass: 'grid-align', width: 110 },
        { headerName: "Inspected Comments", field: "Comments", cellClass: 'grid-align', width: 200 },
        { headerName: "Submitted Date", field: "CreatedDate", cellClass: 'grid-align', width: 150 },
        { headerName: "Reviewed Action", field: "ZFMAction", cellClass: 'grid-align', width: 110 },
        { headerName: "Reviewed Comments", field: "ZfmComments", width: 190, cellClass: 'grid-align' },
        {
            headerName: "Picture", field: "BCLD_FILE_UPLD", cellClass: 'grid-align', width: 200, cellRenderer: function (params) {
                BCLD_FILE_UPLD = params.data.BCLD_FILE_UPLD;
                ID = params.data.ID;
                return params.data.BCLD_FILE_UPLD ?'<a ng-click="Download(\'' + BCLD_FILE_UPLD + '\', \'' + ID + '\')">' + BCLD_FILE_UPLD + '</a>':'';
            }
        },

    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
    };

    $scope.gridOptions1 = {
        columnDefs: '',
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        groupHideGroupColumns: true,
        enableColResize: true,
        cellRenderer: {
            renderer: "group"
        },
        showToolPanel: true
    };


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value)
        $scope.gridOptions1.api.setQuickFilter(value)
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    ZFMReportService.GetMainType().then(function (response) {
        if (response.data != null) {
            $scope.MainType = [];
            $scope.MainType = response.data;
            $scope.MainType[0].ticked = true;
        }
    });
    $scope.LoadData = function () {

        var params = {
            FromDate: $scope.ZFMReport.FromDate,
            ToDate: $scope.ZFMReport.ToDate,
            Type: $scope.ZFMReport.MainType[0].BCL_TP_ID
        };
        ZFMReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                $scope.gridOptions.api.setRowData($scope.gridata[0]);
                progress(0, 'Loading...', false);
                ZFMReportService.GetAttachmentwise(params).then(function (data) {
                    debugger;
                    $scope.gridata = data;
                    if ($scope.gridata == null) {
                        $scope.gridOptions1.api.setRowData([]);
                        progress(0, 'Loading...', false);
                    }
                    else {
                        $scope.gridOptions1.api.setColumnDefs(data.Coldef);
                        $scope.gridOptions1.api.setRowData(data.griddata);
                       /* $scope.gridOptions1.api.setRowData($scope.gridata[0]);*/
                        progress(0, 'Loading...', false);
                    }
                });
            }
        });

    }

    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);
        var ws = XLSX.utils.json_to_sheet($scope.gridOptions.rowData);
        var columnHeaders = Object.keys($scope.gridOptions.rowData[0]);
        /* Iterate over the column headers */
        columnHeaders.forEach(function (header, columnIndex) {
            var Length = [];
            var headerLength = header.length + 2; // Start with the length of the header
            /* Iterate over the rows to find the maximum length of cell content */
            $scope.gridOptions.rowData.forEach(function (row) {
                var cellValue = row[header] ? row[header].toString() : '';
                var maxLength = Math.max(headerLength, cellValue.length + 2); // Add 2 for padding
                Length.push(maxLength);
            });
            let maximumValue = Math.max.apply(null, Length);
            //let maximumValue = Math.max(...Length);
            var columnPixels = maximumValue;
            /* Set the column width in the worksheet */
            ws['!cols'] = ws['!cols'] || [];
            ws['!cols'][columnIndex] = { width: columnPixels };

        });
        /* add to workbook */
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "ZFM_Report_Pivot");
        /* write workbook and force a download */
        XLSX.writeFile(wb, "ZFM_Report_Pivot.xlsx");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReportPivot = function () {
        /*    progress(0, 'Downloading Please wait...', true);*/

        var params = {
            FromDate: $scope.ZFMReport.FromDate,
            ToDate: $scope.ZFMReport.ToDate,
            Type: $scope.ZFMReport.MainType[0].BCL_TP_ID
        };
        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ZFMReport.csv"
        };
        ZFMReportService.GetGriddataPivotExcel(params).then(function (response) {
            debugger;
            if (response != null) {
                debugger;
                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData(response.griddata);
                //$scope.gridOptions1.api.exportDataAsCsv();
                $scope.GenerateFilterExcel1();
                /*  $scope.params = response;*/
                //alasql('SELECT * INTO XLSX("ZFMReport_Pivot.xlsx",{headers:true}) FROM ?', [$scope.gridOptions]);
            }
            else {
                $scope.items = [];
            }
            progress(0, 'Loading...', false);
        });
    };


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ZFMReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function () {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }
    $scope.SearchA = function () {
        var params = {
            FROMDATE: $scope.ZFMReport.FromDate,
            TODATE: $scope.ZFMReport.ToDate,
            Type: $scope.ZFMReport.MainType[0].BCL_TP_ID
        }
        ZFMReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                // $scope.GridVisiblity = false;
                $scope.gridOptions.api.setColumnDefs($scope.columnDefs);
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                //$scope.gridOptions.api.setColumnDefs(columnDefs);
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
            progress(0, 'Loading...', false);
        });
        ZFMReportService.GetAttachmentwise(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                // $scope.GridVisiblity = false;
                $scope.gridOptions1.api.setColumnDefs($scope.columnDefs);
                $scope.gridOptions1.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                //$scope.gridOptions.api.setColumnDefs(columnDefs);
                $scope.gridOptions1.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
            progress(0, 'Loading...', false);
        });
    }

    $scope.selVal = "TODAY";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.ZFMReport.FromDate = "";
                $scope.ZFMReport.ToDate = "";
                break;
            case 'TODAY':
                $scope.ZFMReport.FromDate = moment().format('MM/DD/YYYY');
                $scope.ZFMReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.ZFMReport.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.ZFMReport.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.ZFMReport.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.ZFMReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.ZFMReport.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.ZFMReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.ZFMReport.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.ZFMReport.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.ZFMReport.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.ZFMReport.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
   

    $scope.Download = function (BCLD_FILE_UPLD, ID) {
        debugger;
        if (BCLD_FILE_UPLD == 'undefined' || BCLD_FILE_UPLD == "" || BCLD_FILE_UPLD == null) {
            showNotification('error', 8, 'bottom-right', 'No Files Found');
            return;
        }
        //window.location.assign(UtilityService.path + "/api/ZFMReport/DownloadDocuments?id=" + ID);
        window.location.assign(UtilityService.path + "/api/ZFMReport/DownloadDocuments?id=" + ID);
    };

    $timeout(function () { $scope.LoadData() }, 200);

    //$scope.rptDateRanges();

})