﻿app.service("BCLTypeLocMappingService", function ($http, $q, UtilityService) {
    this.GetHeldeskList = function (intTypeLevel) {       
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BCLTypeLocMapping/GetHMDList?intTypeLevel=' + intTypeLevel + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetEmpOnRole = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLTypeLocMapping/GetRoleBasedEmployees', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLTypeLocMapping/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGridDetails = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLTypeLocMapping/GetGridDetails', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveData = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLTypeLocMapping/InsertDetails', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.UpdateData = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLTypeLocMapping/UpdateDetails', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //For Edit
    this.editData = function (obj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLTypeLocMapping/editData', obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetChildCategoryByModule = function (dataObject) {
        deferred = $q.defer();
        return $http.post('../../../api/BCLTypeLocMapping/GetChildCategoryByModule', dataObject)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteSeat = function (Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLTypeLocMapping/DeleteSeat', Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

});

app.controller('BCLTypeLocMappingController', function ($scope, $q, BCLTypeLocMappingService, UtilityService, $filter, $http, $ngConfirm) {
    $scope.BCLTypeLocMapping = {};
    $scope.ActionStatus = 0;
    $scope.MainType = [];
    $scope.ShowMessage = false;
    $scope.sta_id = [{ "value": "1", "Name": "Active" }, { "value": "2", "Name": "In-Active" }]

    BCLTypeLocMappingService.GetMainType().then(function (response) {
        $scope.MainType = response.data;
        angular.forEach($scope.MainType, function (value, key) {
            var a = _.find($scope.MainType);
            a.ticked = true;
        });
        setTimeout(function () { $scope.GetButton(); }, 500);
    })
    $scope.GetButton = function () {      
        $scope.BCLTypeLocMapping.Nums = 0;
        BCLTypeLocMappingService.GetHeldeskList($scope.BCLTypeLocMapping.Nums).then(function (response) {
            if (response != null) {
                $scope.HDMList = response;
            }
        });
        loadtable($scope.BCLTypeLocMapping.Nums);
    }
    
    $scope.ClearData = function () {
        $scope.BCLTypeLocMapping = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmMapping.$submitted = false;
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        $scope.Apprlst = [];
        $scope.BCLTypeLocMapping.Nums = '';
    }



    
    $scope.GetEmpOnRole = function (data) {
        data.ROL_LEVEL = 0;
        var SelectedRole = { Role: data.ROL_ID };
        BCLTypeLocMappingService.GetEmpOnRole(SelectedRole).then(function (response) {
            if (response != null) {
                data.Emplst = response.data;
            }
        });
    }


    var columnDefs = [
        { headerName: "Type Name", field: "BCL_TP_NAME", width: 290, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 290, cellClass: 'grid-align' },
        //{ headerName: "Designation", field: "DSN_AMT_TITLE", width: 290, cellClass: 'grid-align' },
        { headerName: "No. of Approval Levels", field: "COUNT", width: 380, cellClass: 'grid-align' },
        { headerName: "Edit", width: 114, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, suppressSorting: true, onmouseover: "cursor: hand (a pointing hand)" },
        { headerName: 'Delete', cellclass: "grid-align", filter: 'set', template: '<a ng-click="Delete(data)"><i class="fa fa-times" aria-hidden="true"></i></a>' }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var obj = { TPM_TP_ID: _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',') };
        var params = JSON.stringify(obj);
        BCLTypeLocMappingService.GetGridDetails(params).then(function (response) {
            if (response.data != null) {
                $scope.gridata = response.data;
                $scope.gridOptions.api.setRowData(response.data);
                //console.log(response.data);
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Data Found');
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $scope.ChildCatAppMat = function () {
        progress(0, 'Loading...', true);
        //if ($scope.BCLTypeLocMapping.Nums == '0') {
        //    setTimeout(function () {
        //        $scope.$apply(function () {
        //            showNotification('error', 8, 'bottom-right', 'You Can not Add Or Update.Since No. of Level to Approve is 0');
        //        });
        //    }, 700);
        //    progress(0, '', false);
        //    return false;
        //}       
        if ($scope.IsInEdit) {
            var TPM_TP_ID = _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',');//, HDM_NUM: $scope.BCLTypeLocMapping.Nums
            var obj = { TPM_TP_ID: TPM_TP_ID };
            var params = JSON.stringify(obj);
            $scope.Dataobj = { TPM_TP_ID: TPM_TP_ID, Apprlst: $scope.Apprlst, lcmlst: $scope.BCLTypeLocMapping.Locations };
            BCLTypeLocMappingService.UpdateData($scope.Dataobj).then(function (response) {
                var updatedobj = {};
                angular.copy($scope.BCLTypeLocMapping, updatedobj)
                $scope.gridata.unshift(updatedobj);
                BCLTypeLocMappingService.GetGridDetails(params).then(function (response) {
                    if (response.data != null) {
                        $scope.gridata = response.data;
                        $scope.gridOptions.api.setRowData(response.data);
                        progress(0, '', false);
                    }
                });
                $scope.Success = "Data Updated Successfully";
                //$scope.LoadData();
                $scope.IsInEdit = false;
                $scope.ClearData();
                $scope.BCLTypeLocMapping.Nums = 0;
                setTimeout(function () {
                    $scope.$apply(function () {
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })
        }
        else {
            var TPM_TP_ID = _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',');//, HDM_NUM: $scope.BCLTypeLocMapping.Nums
            var obj = { TPM_TP_ID: TPM_TP_ID };
            var params = JSON.stringify(obj);
            $scope.Dataobj = { TPM_TP_ID: TPM_TP_ID, Apprlst: $scope.Apprlst, lcmlst: $scope.BCLTypeLocMapping.Locations };
            BCLTypeLocMappingService.SaveData($scope.Dataobj).then(function (response) {
                $scope.ShowMessage = true;
                BCLTypeLocMappingService.GetGridDetails(params).then(function (response) {
                    if (response.data != null) {
                        $scope.gridata = response.data;
                        $scope.gridOptions.api.setRowData(response.data);
                        progress(0, '', false);
                    }
                });
                //$scope.LoadData();
                showNotification('success', 8, 'bottom-right', "Data Inserted Successfully");
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                $scope.ClearData();
                $scope.BCLTypeLocMapping.Nums = 0;
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
        progress(0, '', false);
    }



    function loadtable(val) {
        $scope.Apprlst = [];
        var obj = {};
        for (var i = 0; i <= val; i++) {
            obj = {};
            obj.ROL_ID = "";
            obj.ROL_LEVEL = i;
            $scope.Apprlst.push(obj);
        }
        setTimeout(function () {
            $scope.LoadData();
        }, 700);
    }

    $scope.EditFunction = function (data) {        
        var obj = { TPM_TP_ID: data.BCL_TP_ID, LCM_CODE: data.LCM_CODE, LEVEL: $scope.BCLTypeLocMapping.Nums };
        var params = JSON.stringify(obj);
        BCLTypeLocMappingService.editData(params).then(function (response) {
            //$scope.BCLTypeLocMapping.Nums = response.data.COUNT;
            $scope.Apprlst = response.data.Apprlst;

            angular.forEach($scope.Locations, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.lcmlst, function (value, key) {
                var sb = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });

                if (sb != undefined && sb.ticked == false) {
                    sb.ticked = true;

                }
            });
            //angular.forEach($scope.MainType, function (value, key) {
            //    value.ticked = false;
            //});
            //angular.forEach(response.data.ChildCatlst, function (value, key) {
            //    var cc = _.find($scope.MainType, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            //    if (cc != undefined && cc.ticked == false) {
            //        cc.ticked = true;
            //        $scope.ChildCatLocApp.MainType.push(cc);
            //    }
            //});
            $scope.MainType = [];
            BCLTypeLocMappingService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                angular.forEach($scope.MainType, function (value, key) {
                    value.ticked = false;
                    if (value.BCL_TP_ID.toString() == data.BCL_TP_ID) {
                        value.ticked = true;
                    }
                });
            })
            //var ins = _.find($scope.MainType, { BCL_TP_ID: data.BCL_TP_ID });
            //if (ins != undefined) {
            //    setTimeout(function () {
            //        $scope.$apply(function () {
            //            ins.ticked = true;
            //            $scope.BCLTypeLocMapping.MainType.push(ins);
            //        });
            //    }, 100)
            //}
        });

        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;

    }

    $scope.column = {};




    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;

        }
    });



    $scope.locSelectAll = function () {

    }

    $scope.LocationChange = function () {


    }

    $scope.LocationSelectNone = function () {
        $scope.Locationlst = [];

    }

    $scope.DesignationSelectAll = function () {

    }

    $scope.DesignationChange = function () {


    }

    $scope.DesignationSelectNone = function () {
        $scope.Designation = [];

    }


    $scope.Delete = function (data) {
        console.log(data);
        var encoding = encodeURIComponent(data.TPM_ID);
        //var Id = encoding.replace(/%2F/i, "_");
        var Id = encoding.split(/%2F/i).join("__");
        var obj = { TPM_ID: Id };
        //console.log(obj);
        var params = JSON.stringify(obj);
        progress(0, 'Loading...', true);
        $ngConfirm({
            icon: 'fa fa-warning',
            columnClass: 'col-md-6 col-md-offset-3',
            title: 'Confirm!',
            content: 'Are you sure want to Inactive / Delete  - ' + '(' + data.BCL_TP_NAME + ') ' + data.LCM_NAME,   //+ '(' + data.CHC_TYPE_NAME + ') ' + data.AUR_KNOWN_AS
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            type: 'red',
            autoClose: 'cancel|15000',
            buttons: {
                Confirm: {
                    text: 'Delete',
                    btnClass: 'btn-red',
                    action: function (button) {
                        BCLTypeLocMappingService.DeleteSeat(params).then(function (response) {
                            if (response != null) {
                                $ngConfirm({
                                    icon: 'fa fa-success',
                                    title: 'Status',
                                    content: 'Record Deleted Successfully',
                                    closeIcon: true,
                                    closeIconClass: 'fa fa-close'
                                });
                            }
                            setTimeout(function () {
                                $scope.LoadData();
                            }, 800);
                        });
                    }
                },
                cancel: function () {
                    $ngConfirm({
                        title: 'Status!',
                        content: 'Cancelled',
                    });
                }
            }
        });
        progress(0, 'Loading...', false);
    }

});