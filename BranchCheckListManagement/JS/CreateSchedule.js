﻿app.service('CreateScheduleService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {


    this.GetDates = function (data) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateSchedule/GetDates', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.ScheduleVisits = function (data) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateSchedule/ScheduleVisits', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetLocationsall = function (ID) {

        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CreateSchedule/GetLocationsall?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };



}]);

app.controller('CreateScheduleController', ['$scope', '$http', 'HDMUtilityService', 'UtilityService', 'CreateScheduleService', '$parse', function ($scope, $http, HDMUtilityService, UtilityService, CreateScheduleService, $parse) {



    $scope.CreateSchedule = {};
    $scope.CS = {};
    $scope.city = [];
    $scope.Location = [];

    var columnDefs = [
        { headerName: "Date", field: "DATEVALUE", width: 150, cellClass: 'grid-align center-align-cell', suppressMenu: true, suppressSorting: true, },
        { headerName: "City", field: "CTY_CODE", width: 200, cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true, cellStyle: { "overflow": 'visible' }, cellRenderer: customEditor },
        { headerName: "Location", field: "LCM_CODE", width: 200, cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true, cellStyle: { "overflow": 'visible' }, cellRenderer: customEditor },
        { headerName: "Remarks", field: "REMARKS", width: 250, cellClass: 'grid-align', suppressMenu: true, suppressSorting: true, template: "<input type='text' style='width: 300px; height: 30px'  data-ng-model='data.REMARKS' >" }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: false,
        suppressVerticalScroll: false,
        enableFilter: true,
        rowHeight: 50,
        enableScrollbars: true,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },

    };





    //for date calender

    $scope.DateClick = function (id) {
        debugger;
        $('#' + id).datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
        });
    }

    // today date binding in page load

    $scope.CreateSchedule.FROMDATE = moment().format('DD/MM/YYYY');
    $scope.CreateSchedule.TODATE = moment().format('DD/MM/YYYY');


    $scope.LoadData = function () {

        UtilityService.getCities(2).then(function (response) {
            if (response.data !== null)
                $scope.DropdownCity = response.data;

            var obj = {
                FROMDATE: moment($scope.CreateSchedule.FROMDATE, 'DD/MM/YYYY').format('MM/DD/YYYY'),
                TODATE: moment($scope.CreateSchedule.TODATE, 'DD/MM/YYYY').format('MM/DD/YYYY')
            }
            CreateScheduleService.GetDates(obj).then(function (response) {

                $scope.gridOptions.api.setRowData(response.Table);
            });
        });
    }

    CreateScheduleService.GetLocationsall(1).then(function (response) {

        if (response.data !== null)
            $scope.DropdownLocations = response.data;
    });

    $scope.LoadData();

    //search for dates in grid

    $scope.Search = function () {

        var obj = {
            FROMDATE: moment($scope.CreateSchedule.FROMDATE, 'MM/DD/YYYY').format('DD/MM/YYYY'),
            TODATE: moment($scope.CreateSchedule.TODATE, 'MM/DD/YYYY').format('DD/MM/YYYY')
        }
        CreateScheduleService.GetDates(obj).then(function (response) {


            $scope.gridOptions.api.setRowData(response.Table);


        });
    }




    // dropdowns in grid

    var name, model;
    function customEditor(params) {

        var eCell = document.createElement('span');
        var index = params.rowIndex;
        if (params.column.colId === "CTY_CODE") {
            name = 'Cities' + index;
            model = $parse(name);
            $scope.CS = angular.copy($scope.DropdownCity);
            model.assign($scope, $scope.CS);
            eCell.innerHTML = '<div isteven-multi-select data-input-model="' + name + '"  data-output-model="CS.city" data-button-label="CTY_NAME" data-item-label="CTY_NAME"  data-on-item-click="getLocationsByCities(0)" data-on-select-all="CitySelectAll(' + index + ')" data-on-select-none="CitySelectNone(' + index + ')"  data-tick-property="ticked" data-max-labels="1" style="width:200px" ></div >';
            angular.forEach($scope.CS, function (val) {
                val.rowvalue = index;
            });
        }
        if (params.column.colId === "LCM_CODE") {
            name = 'Locations' + index;
            model = $parse(name);
            $scope.LocationsbyCity = [];
            model.assign($scope, $scope.LocationsbyCity);
            eCell.innerHTML = '<div isteven-multi-select data-input-model="' + name + '"  data-output-model="CS.Location" data-button-label="LCM_NAME" data-item-label="LCM_NAME"  data-on-item-click="getLocationsDetails(' + index + ')" data-on-select-all="LocationSelectAll(' + index + ')" data-on-select-none="LocationSelectNone(' + index + ')"  data-tick-property="ticked" data-max-labels="1" style="width:200px"></div >';
            angular.forEach($scope.CS, function (val) {
                val.rowvalue = index;
            });
        }

        return eCell;
    }


    //function when we click on city 

    $scope.getLocationsByCities = function (stsid) {
        var name, model;
        $scope.LocationsBC = [];

        angular.forEach($scope.CS.city, function (val1) {

            angular.forEach($scope.DropdownLocations, function (val2) {

                if (val1.CTY_CODE == val2.CTY_CODE) {

                    $scope.LocationsBC.push({
                        "CTY_CODE": val2.CTY_CODE,
                        "LCM_CODE": val2.LCM_CODE,
                        "LCM_NAME": val2.LCM_NAME
                    });
                }
            });
        });
        if (stsid == 1) {
            name = 'Locations' + SelectAllIndex;
        }
        else {
            name = 'Locations' + $scope.CS.city[0].rowvalue;
        }
        model = $parse(name);
        $scope.LocationsbyCity = angular.copy($scope.LocationsBC);
        model.assign($scope, $scope.LocationsbyCity);


    }

    //function when we click on location 

    $scope.locationsByDate = {}; // Create an object to store locations grouped by date

    $scope.getLocationsDetails = function () {
        angular.forEach($scope.CS.city, function (city) {
            angular.forEach($scope.LocationsbyCity, function (location) {

                if (location.ticked) {
                    var dateValue = $scope.gridOptions.rowData[city.rowvalue].DATEVALUE;

                    if (!$scope.locationsByDate[dateValue]) {
                        $scope.locationsByDate[dateValue] = [];
                    }

                    $scope.locationsByDate[dateValue].push({
                        "CTY_CODE": location.CTY_CODE,
                        "LCM_CODE": location.LCM_CODE,
                        "LCM_NAME": location.LCM_NAME
                    });
                }
            });
        });
    };

    var SelectAllIndex = "";

    $scope.CitySelectAll = function (id) {

        SelectAllIndex = id;
        $scope.CS.city = $scope.DropdownCity;
        $scope.getLocationsByCities("1");
    }
    $scope.CitySelectNone = function () {

        $scope.CS.city = [];
        $scope.getLocationsByCities("1");
    }

    $scope.LocationSelectAll = function (re) {
        $scope.Locations = $scope.LocationsbyCity;
        $scope.getLocationsDetails(re);
    }

    $scope.LocationSelectNone = function (re) {
        $scope.Locations = [];
        $scope.getLocationsDetails(re);
    }

    //submit function

    $scope.ScheduleVisits = function () {

        if (Object.keys($scope.locationsByDate).length === 0) {
            $scope.ShowMessage = true;
            $scope.Success = "Please select Cities and Locations";
            progress(0, '', false);
            showNotification("error", 8, 'bottom-right', $scope.Success);
        }
        else {
            $scope.citiesandlocations = [];
            angular.forEach($scope.gridOptions.rowData, function (row) {
                var dateValue = row.DATEVALUE;

                var locations = $scope.locationsByDate[dateValue] || [];

                angular.forEach(locations, function (location) {
                    $scope.citiesandlocations.push({
                        "CTY_CODE": location.CTY_CODE,
                        "LCM_CODE": location.LCM_CODE,
                        "LCM_NAME": location.LCM_NAME,
                        "DATEVALUE": moment(dateValue, 'DD/MM/YYYY').format('MM/DD/YYYY'),
                        "Remarks": row.REMARKS
                    });
                });
            });

            var obj = {
                ScheduleVisitsList: $scope.citiesandlocations,
            };

            CreateScheduleService.ScheduleVisits(obj).then(function (response) {
                debugger;

                if (response.data[0].MSG == 'SUCCESS') {
                    $scope.ShowMessage = true;
                    $scope.Success = "Data Submitted Successfully";
                    progress(0, '', false);
                    //showNotification("success", 8, 'bottom-right', $scope.Success);
                    $('#notification').text($scope.Success);
                    setTimeout(function () {
                        $('#notification').text('');
                    },3000);
                    $scope.locationsByDate = {};
                    $scope.LoadData();
                }
                else {
                    $scope.ShowMessage = true;
                    $scope.Success = "Already scheduled visits for the selected dates and the selected Locations";
                    progress(0, '', false);
                    //showNotification("error", 8, 'bottom-right', $scope.Success);
                    $('#notification').text($scope.Success);
                    setTimeout(function () {
                        $('#notification').text('');
                    }, 3000);
                   
                }
            });
        }
    }

}]);


















