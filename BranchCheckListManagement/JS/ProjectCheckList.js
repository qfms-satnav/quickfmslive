﻿app.service("ProjectCheckListService", function ($http, $q, UtilityService) {
    this.getInspectors = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ProjectCheckList/getInspectors')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getProjects = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ProjectCheckList/getProjects')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGriddata = function (Id) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectCheckList/GetGriddata?PType=' + Id + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetSavedDraftsdata = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectCheckList/GetSavedDraftsdata', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.InsertCheckList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectCheckList/InsertCheckList', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getSavedList = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectCheckList/getSavedList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.filter('customSplitString', function () {
    return function (input) {
        var arr = input.split(',');
        return arr;
    };
})

app.controller('ProjectCheckListController', function ($scope, $q, $location, ProjectCheckListService, CheckListCreationService, UtilityService, $filter) {
    $scope.CheckedChe = false;
    $scope.CheckList = {};
    $scope.Business = [];
    $scope.Country = [];
    $scope.Locations = [];
    $scope.City = [];
    $scope.Inspection = [];
    $scope.Project = [];
    $scope.ProjectCheklist = {};
    $scope.SelectedValues = [];
    $scope.ProjectCheklist.CheckedValue = [];
    $scope.CheckList = [];
    $scope.TotalList = [];
    $scope.CheckedValue = [];


    ///////////File Upload Grid
    $scope.columnDefs = [
        { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 250 },
        //{ headerName: "Image", field: "file", cellClass: "grid-align", width: 250 },
        {
            headerName: "Download", field: "Name", width: 250, cellRenderer: function (params) {
                return '<a download=' + params.data.Name + ' href="' + params.data.FilePath + '"><span class="glyphicon glyphicon-download"></span></a>';
            }
        },
        { headerName: "Remove", template: '<a ng-click="Remove(this)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 80 }
    ];

    var rowData = [];
    var data;
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: rowData,
        angularCompileRows: true
    };

    $scope.Remove = function (node) {
        $('#UPLFILE').val("");
        var SelectedRow = _.find(rowData, function (x) { if (x.Name == node.data.Name) return x });
        var SelectedIndex = _.indexOf(rowData, SelectedRow);
        rowData.splice(SelectedIndex, 1);
        SelectedRow = _.find(MultiUploadData, function (x) { if (x.Imagepath == node.data.Name) return x });
        SelectedIndex = _.indexOf(MultiUploadData, SelectedRow);
        MultiUploadData.splice(SelectedIndex, 1);
        //if ($scope.gridOptions.rowData.length > 0) {
        //    var Files = $scope.gridOptions.rowData.length + " Files";
        //    $('#UPLFILE').text(Files); 
        //}
        $scope.gridOptions.api.setRowData(rowData);
    };

    ///////////////////////////

    //UtilityService.getVerticals(1).then(function (response) {
    //    if (response.data != null) {
    //        $scope.Business = response.data;
    //        //angular.forEach($scope.Business, function (value, key) {
    //        //    value.ticked = true;
    //        //});          
    //    }
    //});

    CheckListCreationService.getCompany().then(function (response) {
        if (response.data != null) {
            $scope.Business = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;

                }
            });
        }
    });
    UtilityService.getCities(2).then(function (response) {
        if (response.data != null) {
            $scope.City = response.data;
            CheckListCreationService.getLocations().then(function (response) {
                if (response.data != null) {
                    $scope.Locations = response.data;
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
            //UtilityService.getLocations(2).then(function (response) {
            //    if (response.data != null) {
            //        $scope.Locations = response.data;
            //    }
            //});
        }
    });
    $scope.CnyChangeAll = function () {
        $scope.CheckList.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.CheckList.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.CheckList.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.CheckList.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }
    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.CheckList.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.CheckList.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.CheckList.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.GetDrafts = function () {
        ProjectCheckListService.getSavedList().then(function (response) {
            if (response.data != null) {
                $scope.CheckList.SaveList = response.data;
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }
    $scope.GetDrafts();
    ProjectCheckListService.getInspectors().then(function (response) {
        if (response.data != null) {
            $scope.Inspection = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    $scope.LcmChangeAll = function () {
        $scope.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {

        $scope.CheckList.Country = [];
        $scope.CheckList.City = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Business, function (value, key) {
            value.ticked = false;
        })

        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.CheckList.Country.push(cny);
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.CheckList.City.push(cty);
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cnp = _.find($scope.Business, { CNP_ID: value.LCM_CNP });
            if (cnp != undefined && value.ticked == true && cnp.ticked == false) {
                cnp.ticked = true;
                $scope.CheckList.Business.push(cnp);
            }
        });

    };


    $scope.PTypeChanged = function () {
        var s = $scope.CheckList.Project[0].PROJECTCD;
        $scope.GetData();
    };

    ProjectCheckListService.getProjects().then(function (response) {
        if (response.data != null) {
            $scope.Project = response.data;
            angular.forEach($scope.Project, function (value, key) {
                //var a = _.find($scope.Entity, { CHE_CODE: parseInt(CompanySession) });
                value.ticked = true;
                //$scope.Customized.CHE_NAME.push(a);
            });
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    $scope.GetLocationCode = function (items) {
        //$scope.CheckedChe = true;
        $scope.ProjectCheklist.CheckedValue = [];
        $scope.ProjectCheklist.SubchildRadioButtonValue = [];
        ProjectCheckListService.GetSavedDraftsdata(items).then(function (data) {
            $scope.gridata = data.data[0];
            $scope.UploadedData = data.data[1];
            rowData = [];
            MultiUploadData = [];
            for (var g = 0; g < data.data[1].length; g++) {
                rowData.push({
                    Name: $scope.UploadedData[g].Name, FilePath: "https://live.quickfms.com/UploadFiles/" + tenant + "/ProjectCheckList/" + $scope.UploadedData[g].Name
                });
                MultiUploadData.push({ Imagepath: $scope.UploadedData[g].Name });
            }
            $scope.gridOptions.api.setRowData(rowData);

            /////////////////Refresh Grid

            $scope.GetData();
            //////////////////////////////////////////////////

            angular.forEach(data.data[0], function (val) {
                $('#' + val.Subcategory_Code).css('color', 'black');
                $('#' + val.ChildCategoryCode).css({ "border": "0px solid White" });
                if (val.PM_LCD_SAVEASDRAFT == 1) {
                    if (val.childfiled == 'Radio') {
                        $scope.tickedvalue = val.ChildCategoryCode;
                        $scope.CheckedValue = val.ChildCategoryCode;
                        $scope.ProjectCheklist.CheckedValue[val.ChildCategoryCode] = val.ChildCategoryCode;
                        if (val.PM_CLD_SUBCHILDCAT_ID) {
                            $scope.ProjectCheklist.SubchildRadioButtonValue[val.PM_CLD_SUBCHILDCAT_ID] = val.PM_CLD_SUBCHILDCAT_ID;
                        }

                        $scope.SelectedValues.push
                            ({
                                CatCode: val.Category_code,
                                SubCatCode: val.Subcategory_Code,
                                ChildCode: val.ChildCategoryCode,
                                ChildSubCode: val.PM_CLD_SUBCHILDCAT_ID,
                                ChildCatValue: val.PM_CLD_CHILDCAT_VALUE,
                                ChildSubCode1: "",
                                ChildSubValue1: val.PM_CLD_SUBCHILDCAT_VALUE,
                                ChildSubValue2: "",
                                ButtonType: val.childfiled
                            });
                    }
                    else {
                        $scope.ProjectCheklist.CheckedValue[val.ChildCategoryCode] = val.PM_CLD_CHILDCAT_VALUE;
                        if (val.childfiled == 'CheckBox') {
                            $scope.CheckedChe = true;
                            if (val.PM_CLD_SUBCHILDCAT_ID) {
                                var arr = val.PM_CLD_SUBCHILDCAT_ID.split(',');
                                for (var o in arr) {
                                    $scope.ProjectCheklist.SubchildRadioButtonValue[arr[o]] = true;
                                }
                            }
                        }
                        $scope.Cked == true;
                        $scope.SelectedValues.push
                            ({
                                CatCode: val.Category_code,
                                SubCatCode: val.Subcategory_Code,
                                ChildCode: val.ChildCategoryCode,
                                ChildSubCode: val.PM_CLD_SUBCHILDCAT_ID,
                                ChildCatValue: val.PM_CLD_CHILDCAT_VALUE,
                                ChildSubCode1: "",
                                ChildSubValue1: val.PM_CLD_SUBCHILDCAT_VALUE,
                                ChildSubValue2: "",
                                ButtonType: val.childfiled
                            });

                    }
                }
            });
        });

        angular.forEach($scope.Country, function (Country) {
            Country.ticked = false;
        });
        angular.forEach($scope.City, function (City) {
            City.ticked = false;
        });
        angular.forEach($scope.Locations, function (Locations) {
            Locations.ticked = false;
        });
        angular.forEach($scope.Inspection, function (Inspection) {
            Inspection.ticked = false;
        });
        angular.forEach($scope.Business, function (Business) {
            Business.ticked = false;
        });
        angular.forEach($scope.Project, function (Project) {
            Project.ticked = false;
        });

        $scope.SelectedDraftLocation = { LcmCode: items.LCM_CODE, InspectdDate: items.PM_CL_MAIN_VISIT_DT };

        $scope.CheckList.SVR_FROM_DATE = $filter('date')(items.PM_CL_MAIN_VISIT_DT, "yyyy-MM-dd");
        $scope.CheckList.PRJ_NAME = items.PM_CL_MAIN_PRJ_NAME;
        $scope.CheckList.OVERALL_CMTS = items.PM_CL_OVERALL_CMTS;
        $('#OVERALL_CMTS').val(items.PM_CL_OVERALL_CMTS);


        var lcm = _.find($scope.Locations, { LCM_CODE: items.LCM_CODE });
        if (lcm != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    lcm.ticked = true;
                    $scope.CheckList.Locations.push(lcm);
                });
            }, 100)
        }


        var cny = _.find($scope.Country, { CNY_CODE: items.LCM_CNY_ID });

        if (cny != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cny.ticked = true;
                    $scope.CheckList.Country.push(cny);
                });
            }, 100)
        }

        var cty = _.find($scope.City, { CTY_CODE: items.LCM_CTY_ID });
        if (cty != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cty.ticked = true;
                    $scope.CheckList.City.push(cty);
                });
            }, 100)
        }


        var CNP = _.find($scope.Business, { CNP_ID: parseInt(items.PM_CL_MAIN_VER_CODE) });
        if (CNP != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    CNP.ticked = true;
                    // $scope.CheckList.CNP_NAME.push(CNP);
                    $scope.CheckList.Business.push(CNP);
                });
            }, 100)
        }

        var Insp = _.find($scope.Inspection, { INSPECTOR: items.PM_CL_MAIN_INSP_BY });
        if (Insp != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    Insp.ticked = true;
                    $scope.CheckList.Inspection.push(Insp);
                });
            }, 100)
        }

        var PT = _.find($scope.Project, { PROJECTCD: items.PM_CL_MAIN_PRJ_TYPE });
        if (PT != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    PT.ticked = true;
                    $scope.CheckList.Project.push(PT);
                });
            }, 100)
        }


    }

    $scope.GetData = function () {
        var PType = 0;
        if ($scope.CheckList.Project != undefined) {
            if ($scope.CheckList.Project.length > 0) {
                PType = $scope.CheckList.Project[0].PROJECTCD;
            }
        }

        ProjectCheckListService.GetGriddata(PType).then(function (data) {

            $scope.gridata = data.data[0];
            $scope.TotalList = data.data[0];
            var arr3 = [];
            var arr1 = [];

            angular.forEach(data.data[0], function (val) {
                arr1.push({
                    Subcatcode: val.Subcategory_Code,
                    Subcatname: val.SubCategory,
                    catcode: val.Category_code,
                    catname: val.Category,
                    items: [{
                        childcode: val.ChildCategoryCode, childname: val.ChildCategory, childtype: val.childfiled, Flag: val.FLAG,
                        subchildcode: val.PM_CL_SCC_CODE, subchildtype: val.PM_CL_SCC_RC, subchildname: val.PM_CL_SCC_NAME,
                        subchildcode1: val.PM_CL_SCC1_CODE, subchildtype1: val.PM_SCC1_R_C_T, subchildname1: val.PM_CL_SCC1_NAME
                    }]
                });
            });
            arr3.push(_.map(_.groupBy(arr1, 'Subcatcode'),
                val => ({
                    Subcatcode: val[0].Subcatcode, Subcatname: val[0].Subcatname, catcode: val[0].catcode, catname: val[0].catname,
                    items: _.union.apply(this, val.map(v => v.items)),
                    subitems: _.union.apply(this, val.map(v => v.subitems))
                })
            ));
            $scope.MainDataBinding = arr3;
        });
    };

    $scope.GetData();
    var CheckBoxArr = [];
    $scope.checkchange = function (child, sub, cat, childsub, childsub1, Type, ChildText) {
        var ExitsList = [];
        var ChidCatValues = "";
        if ($scope.ProjectCheklist.SubchildRadioButtonValue == undefined) {
            $scope.ProjectCheklist.SubchildRadioButtonValue = { "": "" };
        }
        $scope.ProjectCheklist.CheckedValue.Childcode = [];
        $scope.ProjectCheklist.CheckedValue.Childcode = child;
        $scope.ProjectCheklist.CheckedValue.Childsubcode = [];
        $scope.ProjectCheklist.CheckedValue.Childsubcode = childsub;
        if (Type === "Calender" || Type === "CheckBox" || Type === "Textbox") {
            $scope.SelectedValues = jQuery.grep($scope.SelectedValues, function (value) {
                return value.ChildCode != child;
            });
            if (Type === "CheckBox" && childsub.length > 0) {
                var CheckBoxVal = '';
                if (ChildText) {
                    CheckBoxArr.push({ CD: childsub });
                    for (var i in CheckBoxArr) {
                        if (CheckBoxVal.length > 0) {
                            CheckBoxVal += ',' + CheckBoxArr[i].CD;
                        }
                        else {
                            CheckBoxVal = CheckBoxArr[i].CD;
                        }
                    }
                }
                else {
                    for (var i in CheckBoxArr) {
                        if (CheckBoxArr[i].CD == childsub) {
                            CheckBoxArr.splice(i, 1);
                        }
                        else {
                            if (CheckBoxVal.length > 0) {
                                CheckBoxVal += ',' + CheckBoxArr[i].CD;
                            }
                            else {
                                CheckBoxVal = CheckBoxArr[i].CD;
                            }
                        }
                    }
                }
                childsub = CheckBoxVal;
            }
        }
        else {

            $scope.SelectedValues = $scope.SelectedValues.filter(function (o1) {
                // filter out (!) items in result2
                return !$scope.SelectedValues.some(function (o2) {
                    return o1.SubCatCode === sub && o1.ButtonType === "Radio";
                });
            });

        }
        $scope.SelectedValues.push
            ({
                CatCode: cat,
                SubCatCode: sub,
                ChildCode: child,
                ChildSubCode: childsub.trim(),
                //ChildCatValue: ($scope.ProjectCheklist.CheckedValue[child] == "val.childcode") ? ChidCatValues : $scope.ProjectCheklist.CheckedValue[child],
                ChildCatValue: ($scope.ProjectCheklist.CheckedValue[child] == child) ? "" : $scope.ProjectCheklist.CheckedValue[child],
                ChildSubCode1: (childsub1 == undefined) ? "" : childsub1.trim(),
                ChildSubValue1: ($scope.ProjectCheklist.SubchildRadioButtonValue[childsub] == "val.childcode") ? "" : $scope.ProjectCheklist.SubchildRadioButtonValue[childsub],
                ChildSubValue2: ($scope.ProjectCheklist.SubchildRadioButtonValue[childsub1] == "val.childcode") ? "" : $scope.ProjectCheklist.SubchildRadioButtonValue[childsub1],
                ButtonType: Type
            });
        $('#' + sub).css('color', 'black');
        $('#' + child).css({ "border": "0px solid White" });
        //if (cat === "OP" || cat === "NPIW") {
        var Main = sub + "C";
        if (child === "NA04" || child === "NA2" || child === "NA3" || child === "NA4") {
            $('#' + Main).css({ "border": "1px solid White" });
        }
        else {
            if (Main === child) {
                $('#' + sub).css({ "border": "1px solid White" });
            }
            else if ($('#' + Main).val() !== "") {
                $('#' + sub).css({ "border": "1px solid White" });
            }
            else {
                $('#' + Main).css({ "border": "2px solid red" });
            }
        }
        //}

    }

    $scope.SavingData = function (a) {
        progress(0, 'Loading...', true);

        var result = [];
        if (a === 0) {
            result = $scope.TotalList.filter(function (o1) {
                // filter out (!) items in result2
                return !$scope.SelectedValues.some(function (o2) {
                    return o1.Subcategory_Code === o2.SubCatCode;
                });
            });

            if (result.length == 0) {
                var Selectedresult = $scope.TotalList.filter(function (o1) {
                    // filter out (!) items in result2
                    return $scope.SelectedValues.some(function (o2) {
                        return o1.Mandatory === 1;
                    });
                });
                var Subresult = Selectedresult.filter(function (o1) {
                    // filter out (!) items in result2
                    return !$scope.SelectedValues.some(function (o2) {
                        return o2.ChildCode === o1.ChildCategoryCode;
                    });
                });

                var Subarr = [];
                $.each($(Subresult), function () {
                    var myname = this.ChildCategoryCode;
                    if ($.inArray(myname, Subarr) < 0) {
                        Subarr.push(myname);
                    }
                });
                for (var i = 0; i < Subarr.length; i++) {
                    $('#' + Subarr[i]).css({ "border": "2px solid red" });
                    $('#' + Subarr[i]).focus();
                }

                if (Subarr.length === 0) {
                    $scope.data =
                    {
                        Country: $scope.CheckList.Country[0].CNY_CODE,
                        City: $scope.CheckList.City[0].CTY_CODE,
                        Location: $scope.CheckList.Locations[0].LCM_CODE,
                        BusinessUnit: $scope.CheckList.Business[0].CNP_ID,
                        Inspectionby: $scope.CheckList.Inspection[0].INSPECTOR,
                        ProjectType: $scope.CheckList.Project[0].PROJECTCD,
                        FromDate: $scope.CheckList.SVR_FROM_DATE,
                        ProjectName: $scope.CheckList.PRJ_NAME,
                        ChecklistDetails: $scope.SelectedValues,
                        Flag: a,
                        imagesList: MultiUploadData,
                        OVERALL_CMTS: $('#OVERALL_CMTS').val()

                    };
                    ProjectCheckListService.InsertCheckList($scope.data).then(function (response) {
                        if (response != null) {
                            $.ajax({
                                type: "POST",
                                url: UtilityService.path + '/api/ProjectCheckList/UploadFiles',
                               // url: 'https://live.quickfms.com/api/ProjectCheckList/UploadFiles',    // CALL WEB API TO SAVE THE FILES.                        
                                contentType: false,
                                processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                                cache: false,
                                data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                                success: function (data, textStatus, xhr) {
                                    showNotification('success', 8, 'bottom-right', response.Message);
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    alert(textStatus + ': ' + errorThrown);
                                }
                            });
                            setTimeout(function () {
                                showNotification('success', 8, 'bottom-right', response.data);
                            }, 500);
                        }
                        $scope.GetData();
                        $scope.GetDrafts();
                        $scope.Clear();
                        progress(0, 'Loading...', false);
                        showNotification('success', 8, 'bottom-right', response.data);

                    });
                }

            }
            else {
                var arr = [];
                $.each($(result), function () {
                    var myname = this.Subcategory_Code;
                    if ($.inArray(myname, arr) < 0) {
                        arr.push(myname);
                    }
                });
                //var Errormsg = "";
                //for (var i = 0; i < 5; i++) {
                //    Errormsg = Errormsg + " " +arr[i] + ", ";
                //}
                //alert('Please Select ' + Errormsg);
                //console.log(myname);
                for (var j = 0; j < arr.length; j++) {
                    $('#' + arr[j]).css('color', 'red');
                }
                showNotification('error', 8, 'bottom-right', 'Please Select All Mandatory Fields');
            }
            progress(0, 'Loading...', false);

        }
        else {
            if ($scope.SelectedValues.length > 0 && $scope.CheckList.Country.length > 0 && $scope.CheckList.Locations.length > 0 &&
                $scope.CheckList.Business.length > 0 && $scope.CheckList.Inspection.length > 0 && $scope.CheckList.Project.length > 0 &&
                $scope.CheckList.SVR_FROM_DATE != undefined && $scope.CheckList.PRJ_NAME != undefined) {
                $scope.data =
                {
                    Country: $scope.CheckList.Country[0].CNY_CODE,
                    City: $scope.CheckList.City[0].CTY_CODE,
                    Location: $scope.CheckList.Locations[0].LCM_CODE,
                    BusinessUnit: $scope.CheckList.Business[0].CNP_ID,
                    Inspectionby: $scope.CheckList.Inspection[0].INSPECTOR,
                    ProjectType: $scope.CheckList.Project[0].PROJECTCD,
                    FromDate: $scope.CheckList.SVR_FROM_DATE,
                    ProjectName: $scope.CheckList.PRJ_NAME,
                    ChecklistDetails: $scope.SelectedValues,
                    Flag: a,
                    imagesList: MultiUploadData,
                    OVERALL_CMTS: $('#OVERALL_CMTS').val()

                };
                ProjectCheckListService.InsertCheckList($scope.data).then(function (response) {
                    if (response != null) {
                        $.ajax({
                            type: "POST",
                            url: UtilityService.path + '/api/ProjectCheckList/UploadFiles',
                            //url: 'https://live.quickfms.com/api/ProjectCheckList/UploadFiles',    // CALL WEB API TO SAVE THE FILES.                        
                            contentType: false,
                            processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                            cache: false,
                            data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                            success: function (data, textStatus, xhr) {
                                showNotification('success', 8, 'bottom-right', response.Message);
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert(textStatus + ': ' + errorThrown);
                            }
                        });
                        setTimeout(function () {
                            showNotification('success', 8, 'bottom-right', response.data);
                        }, 500);
                    }
                    // $scope.ProjectCheckListService.GetGriddata();
                    $scope.GetData();
                    $scope.GetDrafts();
                    $scope.Clear();
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', response.data);
                });
            }
            else {
                progress(0, 'Loading...', false);
                if ($scope.SelectedValues.length === 0) {
                    showNotification('error', 8, 'bottom-right', 'Select Atleast One Category');
                }
                else {
                    showNotification('error', 8, 'bottom-right', 'Please Select All Mandatory Fields');
                }
            }
        }


        // progress(0, 'Loading...', true);
        //$scope.data =
        //    {
        //    Country: $scope.CheckList.Country[0].CNY_CODE,
        //    City: $scope.CheckList.City[0].CTY_CODE,
        //    Location: $scope.CheckList.Locations[0].LCM_CODE,
        //    BusinessUnit: $scope.CheckList.Business[0].VER_CODE,
        //    Inspectionby: $scope.CheckList.Inspection[0].INSPECTOR,
        //    ProjectType: $scope.CheckList.Project[0].PROJECTCD,
        //        FromDate: $scope.CheckList.SVR_FROM_DATE,
        //        ProjectName: $scope.CheckList.PRJ_NAME,
        //        ChecklistDetails: $scope.SelectedValues,
        //        Flag:a
        //    };
        ////console.log($scope.data);
        //ProjectCheckListService.InsertCheckList($scope.data).then(function (response) {
        //    progress(0, 'Loading...', false);
        //    showNotification('success', 8, 'bottom-right', response.data);

        //});
        //progress(0, 'Loading...', false);
    };

    $scope.Clear = function () {
        $scope.ProjectCheklist.CheckedValue = [];
        $scope.CheckedChe = false;
        $scope.ProjectCheklist.SubchildRadioButtonValue = [];
        $scope.SelectedValues = [];
        $scope.frmProjectCheckList.$submitted = false;
        $("input:radio").removeAttr("checked");
        $("input:text").val("");
        $("textarea").each(function () {
            $(this).val("");
        });
        $scope.CheckList.SVR_FROM_DATE = "";
        $scope.CheckList.PRJ_NAME = "";

        angular.forEach($scope.Country, function (Country) {
            Country.ticked = false;
        });
        angular.forEach($scope.City, function (City) {
            City.ticked = false;
        });
        angular.forEach($scope.Locations, function (Locations) {
            Locations.ticked = false;
        });
        angular.forEach($scope.Inspection, function (Inspection) {
            Inspection.ticked = false;
        });
        angular.forEach($scope.Business, function (Business) {
            Business.ticked = false;
        });
        angular.forEach($scope.Project, function (Project) {
            Project.ticked = false;
        });

        rowData = [];
        MultiUploadData = [];
        $scope.gridOptions.api.setRowData(rowData);
        $('#UPLFILE').val("");
    };

    var MultiUploadData = [];

    $scope.fileNameChanged = function (fu) {
        if (rowData.length === 0) {
            rowData = [];
        }
        data = new FormData($('form')[0]);
        for (i = 0; i < fu.files.length; i++) {
            var gridLength = $scope.gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    //rowData.splice(0, 1);
                    $scope.gridOptions.api.setRowData(rowData);
                }
            }
            rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
            MultiUploadData.push({ Imagepath: fu.files[i].name });
            $scope.gridOptions.api.setRowData(rowData);
        }
    };


});