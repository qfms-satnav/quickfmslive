﻿app.service("CheckListPhaseTwoService", function ($http, $q, UtilityService) {
    this.getSavedListZonalCentral = function (BCLStatus) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getSavedListZonalCentral?BCLStatus=' + BCLStatus + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetTableData = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/GetValidation_Grid?BCL_TP_ID=' + data.BCL_TP_ID + '&BCL_ID=' + data.BCL_ID)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getZonalCentralStatus = function (BCL_TP_ID, typeName) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getZonalCentralStatus?BCL_TP_ID=' + BCL_TP_ID + '&typeName=' + typeName + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getInspectorsZonalCentral = function (TPM_TP_ID, TPM_LCM_CODE, TPMD_APPR_LEVELS) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getInspectorsZonalCentral?TPM_TP_ID=' + TPM_TP_ID + '&TPM_LCM_CODE=' + TPM_LCM_CODE + '&TPMD_APPR_LEVELS=' + TPMD_APPR_LEVELS + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveValidationData = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/SaveValidationData', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

})

app.controller('CheckListPhaseTwoController', function ($scope, $q, $location, UtilityService, $filter, $http, CheckListPhaseTwoService) {
    $scope.CheckListPhaseTwo = {};
    $scope.Disabled = true;
    $scope.rowData = [];
    //$('#myModal').modal('show');BCL_MC_NAME
    //$('#myModal').modal({ backdrop: 'static', keyboard: false }, 'show');

    $scope.columnDefs = [
        { headerName: "Sub Category", field: "BCL_MC_NAME", cellClass: "grid-align", width: 250 },
        { headerName: "Selected File", field: "FILEPATH", cellClass: "grid-align", width: 250 },
        {
            headerName: "Download", field: "Download", cellClass: 'grid-align', width: 250, cellRenderer: function (params) {

                return "<a download='" + params.data.FILEPATH + "' href='../../UploadFiles/" + companyid + "/" + params.data.FILEPATH + "'><span class='glyphicon glyphicon-download'></span></a>";
            }
        },
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: $scope.rowData,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };

    $scope.GetDrafts = function (e) {
        CheckListPhaseTwoService.getSavedListZonalCentral(e).then(function (response) {
            if (response.data != null) {
                $scope.CheckListPhaseTwo.SaveList = response.data;
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.GetGridData = function (items) {
        CheckListPhaseTwoService.GetTableData(items).then(function (response) {
            $scope.CheckListPhaseTwo.TableData = response.data;
            $scope.rowData = _.filter(response.data, function (o) { return o.FILEPATH.length > 0; });
            $scope.gridOptions.api.setRowData($scope.rowData);
        });
    }

    $scope.LoadOption = function (BCL_TP_ID) {
        CheckListPhaseTwoService.getZonalCentralStatus(BCL_TP_ID, "Validation Status").then(function (response) {
            if (response.data != null) {
                $scope.CheckListPhaseTwo.items = response.data;
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.Submit = function () {
        $scope.CheckListPhaseTwo.datavalidate = _.filter($scope.CheckListPhaseTwo.TableData, function (o) { return o.ValidateInchareAction.length > 0; });
        if ($scope.CheckListPhaseTwo.datavalidate.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select atleast one value');
            return;
        }
        //console.log($scope.CheckListPhaseTwo.TableData);
        $scope.SelectedDatas = [];
        angular.forEach($scope.CheckListPhaseTwo.TableData, function (Value, Key) {
            $scope.SelectedData = {};
            $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
            $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
            $scope.SelectedData.BCLD_ZF_ACTIONS = Value.ValidateInchareAction;
            $scope.SelectedData.BCLD_ZF_COMENTS = Value.ValidateInchareComment;
            $scope.SelectedDatas.push($scope.SelectedData);
        });
        var data = {
            Seldata: $scope.SelectedDatas,
            Flag: 3,
            BCL_ID: $scope.CheckListPhaseTwo.BCL_ID,
            InspectdBy: $scope.CheckListPhaseTwo.Inspection[0].AUR_ID,
            date: $scope.CheckListPhaseTwo.SVR_FROM_DATE,
        }
        CheckListPhaseTwoService.SaveValidationData(data).then(function (response) {
            if (response.data != null) {
                $scope.Clear();
                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 500);

            } else {
                setTimeout(function () {
                    showNotification('error', 8, 'bottom-right', 'Something Went Wrong');
                }, 500);

            }
        });

    }

    $scope.Clear = function () {
        $scope.CheckListPhaseTwo = {};
        $scope.rowData = [];
        $scope.gridOptions.api.setRowData($scope.rowData);
        angular.forEach($scope.Inspection, function (value, key) {
            value.ticked = false;
        });
    }
    $scope.CheckListPhaseTwo.SVR_FROM_DATE = moment().format('MM/DD/YYYY');

    $scope.LoadIncharge = function (items) {
        CheckListPhaseTwoService.getInspectorsZonalCentral(items.BCL_TP_ID, items.LCM_CODE, items.BCL_TP_LEVEL).then(function (response) {
            if (response.data != null) {
                $scope.Inspection = response.data;
                angular.forEach($scope.Inspection, function (item, index) {
                    if (index == 0) {
                        item.ticked = true
                    }
                });
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }


    $scope.LoadData = function (items) {
        $scope.LoadOption(items.BCL_TP_ID);
        $scope.LoadIncharge(items);
        $scope.CheckListPhaseTwo.TYPE_ID = items.BCL_TP_ID;
        $scope.CheckListPhaseTwo.TYPE_NAME = items.BCL_TP_NAME;
        $scope.CheckListPhaseTwo.CTY_NAME = items.CTY_NAME;
        $scope.CheckListPhaseTwo.LCM_NAME = items.LCM_NAME;
        $scope.CheckListPhaseTwo.BCL_INSPECTED_BY = items.BCL_INSPECTED_BY;
        $scope.CheckListPhaseTwo.BCL_SELECTED_DT = items.BCL_SELECTED_DT;
        $scope.CheckListPhaseTwo.BCL_ID = items.BCL_ID;
        $scope.GetGridData(items);
        setTimeout(function () {
            $('#myModal').modal('toggle');
        }, 500);
    }
}
);

