﻿
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
app.service("CheckListPhaseOneService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (BCL_TP_ID, BCL_ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/GetGriddata?BCL_TP_ID=' + BCL_TP_ID + '&BCL_ID=' + BCL_ID)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    var mainTypes;
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                mainTypes = response.data;
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetInspectorsIn = function (ID) {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CheckListCreation/GetInspectorsIn?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetItems = function (searchValue) {

        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CheckListCreation/GetItems?q=' + searchValue)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.getSavedList = function (BCLStatus, BCL_TP_ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getSavedList?BCLStatus=' + BCLStatus + '&BCL_TP_ID=' + BCL_TP_ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.InsertCheckList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/InsertCheckListV2', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteCheckList = function (BCLID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/DeleteCheckList?BCLID=' + BCLID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.BindDesgnation = function () {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/BindDesignation')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getSysPreferences = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetSysPreferences')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };




});


app.controller('CheckListPhaseOneController', function ($scope, $q, $location, CheckListPhaseOneService, UtilityService, $filter, $http, $ngConfirm) {
    $scope.CheckListPhaseOne = {};
    $scope.TableData = [];
    $scope.rowData = [];
    $scope.dvbutton = true;
    $scope.ChecklistID = '';
    $scope.DesignationList = [];
    $scope.sysPreference = [];
    $scope.Description = "";


    $scope.columnDefs = [
        /*{ headerName: "Sub Category", field: "BCL_MC_NAME", cellClass: "grid-align", width: 250 },*/
        { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 250 },
        {
            headerName: "Download", cellClass: 'grid-align', width: 250, cellRenderer: function (params) {

                return "<a download='" + params.data.Name + "' href='../../UploadFiles/" + companyid + "/" + params.data.Name + "'><span class='glyphicon glyphicon-download'></span></a>";
            }
        },
        { headerName: "Delete", template: '<a ng-click="Remove(data)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 250 }
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: $scope.rowData,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };



    $scope.LoadData = function () {
        UtilityService.getSysPreferences().then(function (response) {
            if (response.data != null) {
                $scope.sysPreference = (_.find(response.data, { SYSP_CODE: "Enable Submitted by Checklist Dropdown" }) || {});
                if ($scope.sysPreference && $scope.sysPreference.SYSP_VAL1 === "1") {
                    $scope.readonlyAngucomplete = false;// Make the angucomplete readonly
                } else {
                    $scope.readonlyAngucomplete = true;// Make the angucomplete editable
                }
                CheckListPhaseOneService.GetGriddata($scope.CheckListPhaseOne.MainType[0].BCL_TP_ID, 0).then(function (response) {
                    if (response.data != null) {
                        $scope.TableData = response.data.CHKLST;
                        response.data.CHKLST.sort(function (a, b) {
                            return a.BCL_MC_NAME.localeCompare(b.BCL_MC_NAME);
                        });
                        //$scope.TableData = groupBy(response.data, 'BCL_MC_CODE');
                        //console.log("************* grouped");
                        //console.log($scope.TableData);
                    }
                });
            }
        });
    }
    function groupBy(array, field) {
        return array.reduce(function (result, currentValue) {
            (result[currentValue[field]] = result[currentValue[field]] || []).push(currentValue);
            return result;
        }, {});
    }

    $scope.OnNextClick = function () {
        var nextIndex;
        CheckListPhaseOneService.GetMainType().then(function (response) {
            var currentIndex = response.data.findIndex(function (item) {
                return item.BCL_TP_ID === $scope.CheckListPhaseOne.MainType[0].BCL_TP_ID;
            });
            if (currentIndex < response.data.length) {
                nextIndex = currentIndex + 1;
            } else {
                nextIndex = currentIndex;
            }
            var nextRecord = response.data[nextIndex];
            CheckListPhaseOneService.GetGriddata(nextRecord.BCL_TP_ID, 0).then(function (response) {
                if (response.data != null) {
                    $scope.TableData = response.data;
                }
            });
        })
    }

    $scope.OnPreviousClick = function () {
        var previousIndex;
        CheckListPhaseOneService.GetMainType().then(function (response) {
            var currentIndex = response.data.findIndex(function (item) {
                return item.BCL_TP_ID === $scope.CheckListPhaseOne.MainType[0].BCL_TP_ID;
            });
            if (currentIndex == 0) {
                previousIndex = currentIndex;
            } else {
                previousIndex = currentIndex - 1;
            }
            var previousRecord = response.data[previousIndex];
            CheckListPhaseOneService.GetGriddata(previousRecord.BCL_TP_ID, 0).then(function (response) {
                if (response.data != null) {
                    $scope.TableData = response.data;
                }
            });
        })
    }
    CheckListPhaseOneService.BindDesgnation().then(function (Sdata) {
        if (Sdata != null) {
            $scope.DesignationListS = Sdata;
            CheckListPhaseOneService.GetItems(UserId).then(function (Sdata) {
                if (Sdata != null) {
                    $scope.inspectorData = Sdata.items[0].Inspection;
                    $scope.DesignationList = _.filter($scope.DesignationListS, function (item) {
                        return item.EMP_DESG === Sdata.items[0].Designation;
                    });
                    $scope.DesignationList[0].ticked = true;
                }
            });
        }
    }, function (error) {
        console.error('Error fetching department list:', error);
    });

    $scope.GetDrafts = function (BCLStatus) {

        if (BCLStatus == 1) {
            $('#hsavedtitle').html("Draft Checklist");
        }
        else {
            $('#hsavedtitle').html("Saved Checklist");

        }
        if ($scope.CheckListPhaseOne.MainType.length == 0) {
            showNotification('error', 8, 'bottom-right', "Please Select Main Type");
            return;
        }
        //var BCL_TP_ID = _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',');
        CheckListPhaseOneService.getSavedList(BCLStatus, $scope.CheckListPhaseOne.MainType[0].BCL_TP_ID).then(function (response) {
            if (response.data != null) {
                $scope.CheckListPhaseOne.SaveList = response.data;

            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }


    UtilityService.getCities(2).then(function (response) {
        if (response.data != null) {
            $scope.City = response.data;
            $scope.City[0].ticked = true;

            UtilityService.getLocations(2).then(function (response) {
                if (response.data != null) {
                    $scope.Location = response.data;
                    $scope.Location[0].ticked = true;

                    CheckListPhaseOneService.GetMainType().then(function (response) {
                        if (response.data != null) {
                            if (response.data.length == 2) {
                                let searchParams = window.location.search;

                                // Get the value of the "category" parameter
                                /*var category = searchParams.get('category');*/
                                // Do something with the category value (e.g. display it in an alert)
                                if (searchParams == "?category=branch") {
                                    $scope.MainType = [];
                                    $scope.MainType.push(response.data[1]);
                                    $scope.MainType[0].ticked = true;
                                    setTimeout(function () {
                                        $scope.LoadData();
                                    }, 500);
                                }
                                else if (searchParams == "?category=health") {
                                    $scope.MainType = [];
                                    $scope.MainType.push(response.data[0]);
                                    $scope.MainType[0].ticked = true;
                                    setTimeout(function () {
                                        $scope.LoadData();
                                    }, 500);

                                }
                                else {
                                    $scope.MainType = response.data;
                                    $scope.MainType[0].ticked = true;
                                    setTimeout(function () {
                                        $scope.LoadData();
                                    }, 500);

                                }
                            }
                            else {
                                let searchParams = window.location.search;

                                // Get the value of the "category" parameter
                                /*var category = searchParams.get('category');*/
                                // Do something with the category value (e.g. display it in an alert)
                                if (searchParams == "?category=branch") {
                                    $scope.MainType = [];
                                    $scope.MainType.push(response.data[0]);
                                    $scope.MainType[0].ticked = true;
                                    setTimeout(function () {
                                        $scope.LoadData();
                                    }, 500);
                                }
                                else if (searchParams == "?category=health") {
                                    $scope.MainType = [];
                                    $scope.MainType.push(response.data[0]);
                                    $scope.MainType[0].ticked = true;
                                    setTimeout(function () {
                                        $scope.LoadData();
                                    }, 500);

                                }
                                else {
                                    $scope.MainType = response.data;
                                    $scope.MainType[0].ticked = true;
                                    setTimeout(function () {
                                        $scope.LoadData();
                                    }, 500);

                                }

                            }

                        }
                        else
                            showNotification('error', 8, 'bottom-right', response.Message);
                    }, function (response) {
                        progress(0, '', false);
                    });
                }
            });
            //CheckListPhaseOneService.GetInspectorsIn(2).then(function (response) {

            //    if (response.data != null) {
            //        $scope.Inspection = response.data;
            //        var matchingItem = $scope.Inspection.find(item => item.AUR_ID === UserId);
            //        if (matchingItem) {
            //            matchingItem.ticked = true;
            //            $scope.DesignationList = _.filter($scope.DesignationListS, function (item) {
            //                return matchingItem.Designation === item.EMP_DESG;
            //            });
            //            $scope.DesignationList[0].ticked = true;

            //        }
            //    }

            //});

        }
    });

    $scope.CheckListPhaseOne.SVR_FROM_DATE = moment().format('MM/DD/YYYY');
    //$scope.DateClick = function (id) {
    //    $('#' + id+'_1').datepicker({
    //        format: 'mm/dd/yyyy',
    //        autoclose: true,
    //        todayHighlight: true,
    //       // endDate: 'today',
    //        //startDate: 'today'
    //    });
    //}
    $scope.DateClick = function (id) {
        //$('#' + id + '_1').datepicker({
        //    format: 'mm/dd/yyyy',
        //    autoclose: true,
        //    todayHighlight: true,
        //    // endDate: 'today',
        //    //startDate: 'today'
        //});
        $('#' + id.BCL_SUB_CODE + '_1').flatpickr(
            {
                enableTime: true,
                dateFormat: "Y-m-d H:i",
                minDate: "2020-01",
                time_24hr: true,
                onChange: function (selectedDates, dateStr, instance) {
                    $scope.$apply(function () {
                        id.DATERESPONSE = dateStr
                    })
                }
            }
        );
        //$('#' + id + '_1').datetimepicker('show');
    }


    $scope.selectedItem = function (selected) {
        debugger;
        if (selected && selected.originalObject && selected.originalObject.Designation) {
            $scope.DesignationList = _.filter($scope.DesignationListS, function (item) {
                return selected.originalObject.Designation === item.EMP_DESG;
            });
            if ($scope.DesignationList.length > 0) {
                $scope.DesignationList[0].ticked = true;
            }
        }
    };


    $scope.SelectFile = function (e) {
        var UplFile = $('#' + e.target.id)[0];
        var currentDateTime = moment().format('YYYYMMDDHHmmss_');
        var newFileName = currentDateTime + UplFile.files[0].name

        if ($scope.gridOptions.rowData.length > 0)
            _.remove($scope.rowData, { SubCatCode: e.target.name });
        $scope.rowData.push({ SubCatCode: e.target.name, Name: newFileName, file: e.target.files[0] });
        $scope.gridOptions.api.setRowData($scope.rowData);

        var formData = new FormData();

        formData.append("UplFile", UplFile.files[0], newFileName);
        $http.post(UtilityService.path + '/api/CheckListCreation/UploadFiles', formData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (data, status) {
            showNotification('success', 8, 'bottom-right', data.data);
        }), function (error, data, status) {

        };
    };

    $scope.Remove = function (data) {
        if ($scope.gridOptions.rowData.length > 0)
            _.remove($scope.rowData, { SubCatCode: data.SubCatCode });
        $scope.gridOptions.api.setRowData($scope.rowData);
        angular.element('#' + data.SubCatCode).val(null);
    }

    $scope.fileNameChanged = function (fu, typ, BCL_SUB_NAME) {
        if (rowData.length === 0) {
            $scope.rowData = [];
        }
        data = new FormData($('form')[0]);
        for (i = 0; i < fu.files.length; i++) {
            var gridLength = $scope.gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    rowData.splice(0, 1);
                    $scope.gridOptions.api.setRowData(rowData);
                }
            }
            rowData.push({ SubCatName: BCL_SUB_NAME, Name: fu.files[i].name, file: fu.files[i] });

            $scope.gridOptions.api.setRowData($scope.rowData);
        }

    };

    //$scope.Save = function () {
    //    console.log($scope.TableData);
    //}

    $scope.FindFileName = function (data, param) {
        var name = '';
        angular.forEach(data, function (Value, Key) {
            if (param == Value.SubCatCode) {
                name = Value.Name;
            }
        })
        return name;

    }

    $scope.CommaSperateData = function (typ, data, listdata) {
        if (typ == 'Multiple' || typ == 'Toggle') {
            var newarray = [];
            angular.forEach(listdata, function (Value, Key) {
                if (Value.RESPONSE) {
                    newarray.push(Value.BCL_CH_NAME);
                }
            })
            return newarray.join(",");
        } else {
            return data;
        }
    }

    $scope.Submit = function () {
        debugger;
        $scope.SelectedDatas = [];
        angular.forEach($scope.TableData, function (Value, Key) {
            $scope.SelectedData = {};
            $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
            $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
            $scope.SelectedData.ScoreCode = $scope.CommaSperateData(Value.BCL_SUB_TYPE, Value.RESPONSE, Value.checklistDetails);
            $scope.SelectedData.ScoreName = '';
            $scope.SelectedData.txtdata = Value.TEXT;
            $scope.SelectedData.Date = Value.DATERESPONSE;
            $scope.SelectedData.SubScore = '';
            $scope.SelectedData.FilePath = $scope.FindFileName($scope.gridOptions.rowData, Value.BCL_SUB_CODE);
            $scope.SelectedDatas.push($scope.SelectedData);

        });

        var data = true;
        data = _.every($scope.SelectedDatas, function (o) { return o.ScoreCode.length > 0; });
        if (!data) {
            showNotification('error', 8, 'bottom-right', 'Please fill all the fields to submit the Checklist');
            return;
        }

        $scope.data = {
            LCMLST: $scope.CheckListPhaseOne.Location,
            LcmList: $scope.CheckListPhaseOne.Location[0].LCM_CODE,
            InspectdBy: $scope.inspectorData,
            date: $scope.CheckListPhaseOne.SVR_FROM_DATE,
            SelCompany: '1',
            Seldata: $scope.SelectedDatas,
            Flag: 2,
            OVERALL_CMTS: $scope.Description,
            imagesList: $scope.gridOptions.rowData,
            BCL_TYPE_ID: $scope.CheckListPhaseOne.MainType[0].BCL_TP_ID,
            BCL_ID: $scope.ChecklistID,
        };
        //var fd = new FormData();
        //for (i = 0; i <= $scope.gridOptions.rowData.length; i++) {
        //    fd.append("file", $scope.gridOptions.rowData[i].file)
        //}
        //console.log(fd);
        CheckListPhaseOneService.InsertCheckList($scope.data).then(function (response) {
            if (response != null) {
                $scope.Clear();
                setTimeout(function () {
                    if (response.Message == "5") {
                        $ngConfirm({
                            icon: 'fa fa-warning',
                            title: 'You have already completed your inspection for the day',
                            content: '',
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            buttons: {
                                close: function ($scope, button) {
                                }
                            }
                        });
                    }
                    else {
                        showNotification('success', 8, 'bottom-right', response.data);
                    }
                }, 500);

            }
        })
    }

    $scope.Save = function () {
        if ($scope.CheckListPhaseOne.Location.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select Location');
            return;
        }
        if ($scope.CheckListPhaseOne.City.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select City');
            return;
        }
        if ($scope.inspectorData.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select Inspection By');
            return;
        }
        if ($scope.CheckListPhaseOne.SVR_FROM_DATE == undefined || $scope.CheckListPhaseOne.SVR_FROM_DATE == '') {
            showNotification('error', 8, 'bottom-right', 'Please select Date');
            return;
        }
        $scope.SelectedDatas = [];
        angular.forEach($scope.TableData, function (Value, Key) {
            $scope.SelectedData = {};
            $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
            $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
            $scope.SelectedData.ScoreCode = $scope.CommaSperateData(Value.BCL_SUB_TYPE, Value.RESPONSE, Value.checklistDetails);
            $scope.SelectedData.ScoreName = '';
            $scope.SelectedData.txtdata = Value.TEXT;
            $scope.SelectedData.Date = Value.DATERESPONSE;
            $scope.SelectedData.SubScore = '';
            $scope.SelectedData.FilePath = $scope.FindFileName($scope.gridOptions.rowData, Value.BCL_SUB_CODE);
            $scope.SelectedDatas.push($scope.SelectedData);

        });
        var data = [];
        data = _.filter($scope.SelectedDatas, function (o) { return o.ScoreCode.length > 0; });
        if (data.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select atleast one value');
            return;
        }
        $scope.data = {
            LCMLST: $scope.CheckListPhaseOne.Location,
            LcmList: $scope.CheckListPhaseOne.Location[0].LCM_CODE,
            InspectdBy: $scope.inspectorData,
            date: $scope.CheckListPhaseOne.SVR_FROM_DATE,
            SelCompany: '1',
            Seldata: $scope.SelectedDatas,
            Flag: 1,
            OVERALL_CMTS: $scope.Description,
            imagesList: $scope.gridOptions.rowData,
            BCL_TYPE_ID: $scope.CheckListPhaseOne.MainType[0].BCL_TP_ID,
            BCL_ID: $scope.ChecklistID,
        };
        CheckListPhaseOneService.InsertCheckList($scope.data).then(function (response) {
            if (response != null) {
                $scope.Clear();
                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 500);

            }
        })


    }

    $scope.DeleteCheckList = function (BCLID, BCLStatus) {
        CheckListPhaseOneService.DeleteCheckList(BCLID).then(function (response) {
            if (response != undefined) {
                $scope.GetDrafts(BCLStatus);
                showNotification('success', 8, 'bottom-right', "Deleted Successfully");
            } else {
                console.log('Submit Error');
            }
        });

    }

    $scope.Clear = function () {
        $scope.CheckListPhaseOne = {};
        $scope.TableData = [];
        $scope.rowData = [];
        $scope.gridOptions.api.setRowData($scope.rowData);
        angular.forEach($scope.Inspection, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.MainType, function (value, key) {
            value.ticked = false;
        });
        $scope.dvbutton = true;
        $scope.ChecklistID = '';
    }


    $scope.GetLocationCode = function (data) {
        CheckListPhaseOneService.GetGriddata($scope.CheckListPhaseOne.MainType[0].BCL_TP_ID, data.BCL_ID).then(function (response) {
            if (response.data.CHKLST != null) {
                if (data.BCL_SUBMIT == 2) {
                    $scope.dvbutton = false;
                }
                else {
                    $scope.dvbutton = true;
                    $scope.ChecklistID = data.BCL_ID;
                }
                angular.forEach($scope.Inspection, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.Location, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.Location, function (value, key) {
                    if (data.LCM_CODE == value.LCM_CODE) {
                        value.ticked = true;
                    }

                });
                angular.forEach($scope.City, function (value, key) {
                    if (data.CTY_CODE == value.CTY_CODE) {
                        value.ticked = true;
                    }

                });
                angular.forEach($scope.Inspection, function (value, key) {
                    if (data.BCL_INSPECTED_BY == value.INSPECTOR) {
                        value.ticked = true;
                    }

                });
                $scope.CheckListPhaseOne.SVR_FROM_DATE = data.BCL_SELECTED_DT;
                $scope.TableData = response.data.CHKLST;
                $scope.Description = response.data.Description;;
                var selectfiles = [];
                $scope.rowData = [];
                selectfiles = _.filter(response.data.CHKLST, function (o) { return o.FILEPATH.length > 0; });
                angular.forEach(selectfiles, function (value, key) {
                    $scope.rowData.push({ Name: value.FILEPATH, SubCatCode: value.BCL_SUB_CODE });
                })
                $scope.gridOptions.api.setRowData($scope.rowData);
                $('#myModal').modal('toggle');
            }
        });
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.CheckListPhaseOne.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

    }
})
