﻿app.service("BCLRoleMappingService", function ($http, $q, $filter, UtilityService) {
    this.GetRoles = function (ID) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BCLRoleMapping/GetRoles?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetBCLUsers = function (ID) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BCLRoleMapping/GetBCLUsers?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetBCLType = function (ID) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BCLRoleMapping/GetBCLType?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetLocations = function (ID) {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/BCLRoleMapping/GetLocations?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetData = function () {
        var deferred = $q.defer();
         $http.post(UtilityService.path + '/api/BCLRoleMapping/GetData')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.SubmitData = function (parms) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLRoleMapping/SubmitData', parms)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.DeleteBCL = function (ID) {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/BCLRoleMapping/DeleteBCL?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
});
app.controller('BCLRoleMappingController', function ($scope, $q, $http, BCLRoleMappingService, UtilityService, $timeout, $filter) {
    $scope.Rolelist = [];
    $scope.Userlist = [];
    $scope.BCLlist = [];
    $scope.Loctionlist = [];
    $scope.Pageload = function () {
        BCLRoleMappingService.GetRoles(1).then(function (response) {
            if (response.data != null) {
                $scope.Rolelist = response.data;
                $scope.Rolelistsource = response.data;
            }
        });
        BCLRoleMappingService.GetBCLUsers(1).then(function (response) {
            if (response.data != null) {
                $scope.Userlist = response.data;
                $scope.Userlistsource = response.data;
            }
        });
        BCLRoleMappingService.GetBCLType(1).then(function (response) {
            if (response.data != null) {
                $scope.BCLlist = response.data;
                $scope.BCLlistsource = response.data;
            }
        });
        BCLRoleMappingService.GetLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Loctionlist = response.data;
                $scope.Loctionlistsource = angular.copy(response.data);
            }
        });
    }
    $scope.Pageload();

    $scope.RolChanged = function () {
        $scope.Userlist = [];
        _.forEach($scope.Userlistsource, function (n, key) {
            _.forEach($scope.BclRole.selectedRol, function (n2, key2) {
                if (n.URL_ROL_ID === n2.ROL_ID) {
                    $scope.Userlist.push(n);
                }
            });
        });
        angular.forEach($scope.Userlist, function (value, key) {
            value.ticked = true;
        })
        $scope.BclRole.selectedUsr = $scope.Userlist;
    }
    $scope.RolSelectAll = function () {
        $scope.BclRole.selectedRol = $scope.Rolelist;
        $scope.RolChanged();
    }
    function resetTickedProperty(list) {
        angular.forEach(list, function (item) {
            item.ticked = false;
        });
    };
    $scope.RolSelectNone = function () {
        resetTickedProperty($scope.Userlist);
        resetTickedProperty($scope.BCLlist);
        resetTickedProperty($scope.Loctionlist);
        if ($scope.BCLRoleMap) {
            $scope.BCLRoleMap.$setPristine();
            $scope.BCLRoleMap.$setUntouched();
        }
    }
    $scope.columnDefs = [
        { headerName: "Checklist Id", field: "BCL_MAP_TP_ID", cellClass: 'grid-align', width: 100 },
        { headerName: "Checklist Name", field: "BCL_TP_NAME", cellClass: 'grid-align', width: 220 },
        { headerName: "User Id", field: "AUR_ID", cellClass: 'grid-align', width: 120 },
        { headerName: "User Name", field: "AUR_KNOWN_AS", cellClass: 'grid-align', width: 150 },
        { headerName: "User Role", field: "ROL_DESCRIPTION", cellClass: 'grid-align', width: 150 },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Delete", width: 50, template: '<a ng-click = "DeleteBCL(data)"> <i class="fa fa-trash txt-danger fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true },
    ];
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        onReady: function () {
            $scope.GetData();
            $scope.gridOptions.api.sizeColumnsToFit()
        },

    };
    $scope.SubmitData = function () {
        var params = {}
        params.Rolelst = $scope.BclRole.selectedRol;
        params.BCLUserlst = $scope.BclRole.selectedUsr;
        params.BCLTypelst = $scope.BclRole.selectedBCL;
        params.BCLLocList = $scope.BclRole.selectedLCM;
        BCLRoleMappingService.SubmitData(params).then(function (response) {
            if (response.data == null) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                $scope.GetData();
                showNotification('success', 8, 'bottom-right', response.data[0].Message);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
        })
    }
    $scope.GetData = function () {
        BCLRoleMappingService.GetData().then(function (response) {
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.gridOptions.api.setRowData(response.data);
            }
            $scope.ClearAll();
        })
    }
    $scope.ClearAll = function () {

        resetTickedProperty($scope.Rolelist);
        resetTickedProperty($scope.Userlist);
        resetTickedProperty($scope.BCLlist);
        resetTickedProperty($scope.Loctionlist);
        if ($scope.BCLRoleMap) {
            $scope.BCLRoleMap.$setPristine();
            $scope.BCLRoleMap.$setUntouched();
        }
    };
    $scope.DeleteBCL = function (dataobj) {
        BCLRoleMappingService.DeleteBCL(dataobj.BCL_MAP_ID).then(function (response) {
            if (response.data == null) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Could not delete');
            }
            else {
                $scope.GetData();
                showNotification('success', 8, 'bottom-right', response.data[0].Message);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
        })
    }
});