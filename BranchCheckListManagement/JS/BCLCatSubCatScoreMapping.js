﻿app.service("BCLCatSubCatScoreMappingService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLCatSubCatScoreMapping/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetMainCategory = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLCatSubCatScoreMapping/GetMainCategory', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetSubCategory = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLCatSubCatScoreMapping/GetSubCategory', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGridData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLCatSubCatScoreMapping/GetGridData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.updateData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLCatSubCatScoreMapping/updateData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BCLCatSubCatScoreMapping/SaveData', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BCLCatSubCatScoreMappingController', ['$scope', '$q', 'UtilityService', 'BCLCatSubCatScoreMappingService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, BCLCatSubCatScoreMappingService, $filter, $timeout, $rootScope) {
        //$scope.StaSubType = [{ "Id": "Radio", "Name": "Radio" }, { "Id": "Multiple", "Name": "Multiple"}];
        $scope.StaSubType = [{ Id: 'Radio', Name: 'Radio' }, { Id: 'Multiple', Name: 'Multiple' }, { Id: 'Text', Name: 'Text' }, { Id: 'Date', Name: 'Date' }];
        $scope.LoadInfo = [];
        $scope.BCLCatSubCatScoreMapping = {};
        $scope.search = [];
        $scope.MainType = [];
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Edit = false;
        function validatinFn() {
            if ($scope.BCLCatSubCatScoreMapping.MainType[0] == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Main Type");
                return false;
            }
            if ($scope.BCLCatSubCatScoreMapping.Category[0] == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Main Category");
                return false;
            }
            if ($scope.BCLCatSubCatScoreMapping.SubCategory[0] == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Subcategory");
                return false;
            }
            if ($scope.BCLCatSubCatScoreMapping.BCL_CH_NAME == undefined || $scope.BCLCatSubCatScoreMapping.BCL_CH_NAME == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Subcategory Name");
                return false;
            }
            if ($scope.BCLCatSubCatScoreMapping.BCL_CH_REMARKS == undefined || $scope.BCLCatSubCatScoreMapping.BCL_CH_REMARKS == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Subcategory Remarks");
                return false;
            }
            if ($scope.BCLCatSubCatScoreMapping.BCL_CH_TYPE == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Sub Type");
                return false;
            }
            return true;
        }
        $scope.SaveA = function () {
            if (validatinFn()) {
                var obj = {
                    BCL_CH_ID: 0,
                    BCL_CH_NAME: $scope.BCLCatSubCatScoreMapping.BCL_CH_NAME,
                    BCL_CH_MNC_CODE: $scope.BCLCatSubCatScoreMapping.Category[0].BCL_MC_CODE,
                    BCL_CH_SUB_CODE: $scope.BCLCatSubCatScoreMapping.SubCategory[0].BCL_SUB_CODE,
                    BCL_CH_REMARKS: $scope.BCLCatSubCatScoreMapping.BCL_CH_REMARKS,
                    BCL_CH_TYPE: $scope.BCLCatSubCatScoreMapping.BCL_CH_TYPE,
                }
                BCLCatSubCatScoreMappingService.SaveData(obj).then(function (response) {
                    if (response != null) {
                        if (response.data == 0) {
                            showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                            setTimeout(function () { $scope.GetMainType(1); }, 500);
                            $scope.ClearData();
                        }
                        else
                            showNotification('error', 8, 'bottom-right', "Subcategory already Exists");
                    }
                    else
                        showNotification('error', 8, 'bottom-right', response.Message);
                }, function (response) {
                    progress(0, '', false);

                });
            }

        }

        $scope.Update = function () {
            var obj = {
                BCL_CH_ID: $scope.BCLCatSubCatScoreMapping.BCL_CH_ID,
                BCL_CH_NAME: $scope.BCLCatSubCatScoreMapping.BCL_CH_NAME,
                BCL_CH_MNC_CODE: $scope.BCLCatSubCatScoreMapping.Category[0].BCL_MC_CODE,
                BCL_CH_SUB_CODE: $scope.BCLCatSubCatScoreMapping.SubCategory[0].BCL_SUB_CODE,
                BCL_CH_REMARKS: $scope.BCLCatSubCatScoreMapping.BCL_CH_REMARKS,
                BCL_CH_TYPE: $scope.BCLCatSubCatScoreMapping.BCL_CH_TYPE,
            }

            BCLCatSubCatScoreMappingService.updateData(obj).then(function (response) {
                if (response != null) {
                    if (response == 0) {
                        showNotification('success', 8, 'bottom-right', 'Data update Successfully');
                        setTimeout(function () { $scope.GetMainType(1); }, 500);
                        //$scope.ClearData();
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Main Category Already Exists");

                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.ShowStatus = function (value) {
            return $scope.StaDet[value == 0 ? 1 : 0].Name;
        }

        $scope.locSelectAll = function () {
            $scope.BCLCatSubCatScoreMapping.MainType = $scope.MainType;
            $scope.MainType();
        }

        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };

        setTimeout(function () { $scope.LoadData(); }, 1000);
        $scope.EditFunction = function (data) {
            $scope.BCLCatSubCatScoreMapping = {};
            $scope.BCLCatSubCatScoreMapping.BCL_CH_ID = data.BCL_CH_ID;
            setTimeout(function () { $scope.GetMainType(0); }, 500);
            angular.forEach($scope.MainType, function (value, key) {
                value.ticked = false;
                if (value.BCL_TP_ID.toString() == data.BCL_TP_ID) {
                    value.ticked = true;
                }
            });
            angular.forEach($scope.Category, function (value, key) {
                value.ticked = false;
                if (value.BCL_MC_CODE.toString() == data.BCL_MC_CODE) {
                    value.ticked = true;
                }
            });
            angular.forEach($scope.SubCategory, function (value, key) {
                value.ticked = false;
                if (value.BCL_SUB_CODE.toString() == data.BCL_SUB_CODE) {
                    value.ticked = true;
                }
            });   
            $scope.EditCategory = data;
            angular.copy(data, $scope.BCLCatSubCatScoreMapping);
            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            $scope.Edit = true;

        }
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };

        var columnDefs = [
            { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Check Type", field: "BCL_TP_NAME", cellClass: "grid-align", width: 120 },
            { headerName: "Check Category", field: "BCL_MC_NAME", cellClass: "grid-align", width: 120 },
            { headerName: "Check Sub Cat. Name", field: "BCL_CH_NAME", cellClass: "grid-align", width: 155 },
            { headerName: "Check Socre Code", field: "BCL_CH_CODE", cellClass: "grid-align", width: 135 },
            { headerName: "Check Score Name", field: "BCL_CH_NAME", cellClass: "grid-align", width: 140 },
            { headerName: "Check Sub Type", field: "BCL_CH_TYPE", cellClass: "grid-align", width: 120 },
            { headerName: "Check Score Remarks", field: "BCL_CH_REMARKS", cellClass: "grid-align", width: 158 },
            //{ headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.BCL_MC_STA_ID)}}", width: 120, cellClass: 'grid-align' },
        ];
        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            },
        };
        $scope.SearchA = function () {
            var obj2 = {
                BCL_CH_SUB_CODE: _.filter($scope.SubCategory, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_SUB_CODE; }).join(','),
            }
            BCLCatSubCatScoreMappingService.GetGridData(obj2).then(function (response) {
                if (response != null) {
                    $scope.gridata = response.data;
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                }
                progress(0, 'Loading...', false);
            })
        }

        $scope.GetMainType = function (status) {
            $scope.MainType = [];
            BCLCatSubCatScoreMappingService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                angular.forEach($scope.MainType, function (value, key) {
                    var a = _.find($scope.MainType);
                    a.ticked = true;
                });
                setTimeout(function () { $scope.GetMainCategory(status); }, 500);
            })
        }
        $scope.GetMainCategory = function (status) {
            $scope.Category = [];
            var obj2 = {
                BCL_TP_MC_ID: _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(','),
            }
            BCLCatSubCatScoreMappingService.GetMainCategory(obj2).then(function (response) {
                $scope.Category = response.data;
                angular.forEach($scope.Category, function (value, key) {
                    var a = _.find($scope.Category);
                    a.ticked = true;
                });
                setTimeout(function () { $scope.GetSubCategory(status); }, 500);
            })
        }
        $scope.GetSubCategory = function (status) {
            $scope.SubCategory = [];
            var obj2 = {
                BCL_CH_MNC_CODE: _.filter($scope.Category, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_MC_CODE; }).join(','),
            }
            BCLCatSubCatScoreMappingService.GetSubCategory(obj2).then(function (response) {
                $scope.SubCategory = response.data;
                angular.forEach($scope.SubCategory, function (value, key) {
                    var a = _.find($scope.SubCategory);
                    a.ticked = true;
                });
                if (status == 1) {
                    setTimeout(function () { $scope.SearchA(); }, 500);
                }
            })
        }
        $scope.LoadData = function () {
            progress(0, 'Loading...', true);
            $scope.BCLCatSubCatScoreMapping = {};
            $scope.GetMainType(1);
            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            progress(0, '', false);
        }
        $scope.GenerateFilterPdf = function () {
            ;

        }

        $scope.GenReport = function (data) {
            progress(0, 'Loading...', true);
            setTimeout(function () {
                progress(0, 'Loading...', false);
                if (data == 'pdf') {
                    const songs = [];
                    angular.forEach($scope.LoadInfo, function (val) {
                        songs.push({ "AAT_CODE": val.AAS_SPAREPART_NAME + ',' + val.AAS_SPAREPART_ID, "AAT_NAME": val.AAS_SPAREPART_DES });
                    });
                    qr_generate(songs);
                    progress(0, 'Loading...', false);
                }
            }, 3000);
        }

        $scope.ClearData = function () {
            $scope.BCLCatSubCatScoreMapping = {};
            $scope.$broadcast('angucomplete-alt:clearInput');
        }
    }]);