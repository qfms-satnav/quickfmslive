﻿app.service('ViewAndModifyScheduleService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetScheduleData = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAndModifySchedule/GetScheduleData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.EditScheduleData = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAndModifySchedule/EditScheduleData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.DeleteMySchedule = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAndModifySchedule/DeleteMySchedule', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetLocationsall = function (ID) {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CreateSchedule/GetLocationsall?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.ModifyScheduleVisits = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAndModifySchedule/ModifyScheduleVisits', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);
app.controller('ViewAndModifyScheduleController', ['$scope', '$http', 'UtilityService', 'ViewAndModifyScheduleService', '$parse', function ($scope, $http, UtilityService, ViewAndModifyScheduleService, $parse) {

    $scope.secondgridvisible = false;

    //grid for pageload

    var columnDefs = [

        { headerName: "City", field: "CTY_NAME", width: 120, cellClass: "grid-align", filter: 'set' },
        { headerName: "Branch Code", field: "BCLDS_LCM_CODE", width: 200, cellClass: "grid-align", filter: 'set' },
        { headerName: "Branch Name", field: "LCM_NAME", width: 100, cellClass: "grid-align", filter: 'set' },
        { headerName: "From Date", field: "BCLDS_FROM_DATE", width: 100, cellClass: "grid-align", filter: 'set', template: '<span>{{ data.BCLDS_FROM_DATE | date: "dd/MM/yyyy"}}</span >' },
        { headerName: "To Date", field: "BCLDS_TO_DATE", width: 120, cellClass: "grid-align", filter: 'set', template: '<span>{{ data.BCLDS_TO_DATE | date: "dd/MM/yyyy"}}</span >' },
        { headerName: "Created Date", field: "BCLDS_CREATED_DT", width: 120, cellClass: "grid-align", filter: 'set', template: '<span>{{ data.BCLDS_CREATED_DT | date: "dd/MM/yyyy"}}</span >' },
        { headerName: "Created By", field: "BCLDS_CREATED_BY", width: 120, cellClass: "grid-align", filter: 'set' },
        {
            headerName: "Edit", field: "Edit", width: 100, cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true,
            template: '<a ng-click="Edit($event)" BCLDS_ID="{{data.BCLDS_ID}}" BCLDS_FROM_DATE="{{data.BCLDS_FROM_DATE}}"  BCLDS_LCM_CODE="{{data.BCLDS_LCM_CODE}}"  BCLDS_CTY_CODE="{{data.BCLDS_CTY_CODE}}"><b>Edit</b></a>'

        },
        {
            headerName: "Delete", field: "Delete", width: 100, cellClass: "grid-align", filter: 'set',
            template: '<a ng-click="Delete($event)" BCLDS_ID="{{data.BCLDS_ID}}" ><b>Delete</b></a>'

        },

    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: false,
        suppressVerticalScroll: false,
        enableFilter: true,
        enableScrollbars: true,
        enableColResize: true,

        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };


    $scope.LoadData = function () {

        ViewAndModifyScheduleService.GetScheduleData().then(function (response) {

            if (response.Table != null) {
                $scope.gridOptions.api.setRowData(response.Table);
            }
        });
    }
    $scope.LoadData();

    //Edit function

    var bcldsId;
    $scope.Edit = function (event) {

        $scope.secondgridvisible = true;
        bcldsId = event.currentTarget.getAttribute('BCLDS_ID');
        var bcldsFromDate = event.currentTarget.getAttribute('BCLDS_FROM_DATE');
        var selectedCityCode = event.currentTarget.getAttribute('BCLDS_CTY_CODE');
        var selectedLocationCode = event.currentTarget.getAttribute('BCLDS_LCM_CODE');

        //for ticked city

        $scope.DropdownCity.forEach(function (city) {
            city.ticked = city.CTY_CODE === selectedCityCode;
        });

        // for ticked location 

        var name, model;
        $scope.LocationsBC = [];
        angular.forEach($scope.DropdownLocations, function (val2) {
            if (selectedCityCode == val2.CTY_CODE) {
                $scope.LocationsBC.push({
                    "CTY_CODE": val2.CTY_CODE,
                    "LCM_CODE": val2.LCM_CODE,
                    "LCM_NAME": val2.LCM_NAME
                });
            }
        });

        name = 'Locations' + '0';
        model = $parse(name);
        $scope.LocationsbyCity = angular.copy($scope.LocationsBC);
        model.assign($scope, $scope.LocationsbyCity);

        $scope.LocationsbyCity.forEach(function (location) {
            location.ticked = location.LCM_CODE === selectedLocationCode;
        });



        angular.forEach($scope.LocationsbyCity, function (location) {
            if (location.ticked) {
                $scope.locationsByDate.push({
                    "CTY_CODE": location.CTY_CODE,
                    "LCM_CODE": location.LCM_CODE,
                    "LCM_NAME": location.LCM_NAME
                });
            }
        });
        var obj = {
            BCLDS_ID: bcldsId,
            BCLDS_FROM_DATE: bcldsFromDate
        }
        ViewAndModifyScheduleService.EditScheduleData(obj).then(function (response) {

            if (response.Table != null) {
                $scope.gridOptions1.api.setRowData(response.Table);
            }
        });
    };


    $scope.Delete = function (event) {

        var obj = {
            BCLDS_ID: event.currentTarget.getAttribute('BCLDS_ID')
        }
        ViewAndModifyScheduleService.DeleteMySchedule(obj).then(function (response) {
            if (response.Table[0].Column1 == 1) {
                $scope.secondgridvisible = false;
                $scope.ShowMessage = true;
                $scope.Success = "Schedule  Deleted Successfully";
                progress(0, '', false);
                //showNotification("success", 8, 'bottom-right', $scope.Success);
                $('#notification').text($scope.Success);
                setTimeout(function () {
                    $('#notification').text('');
                }, 3000);
                $scope.LoadData();

            }
        });


    }

    //grid when we click on edit

    var columnDefs1 = [
        { headerName: "Date", field: "BCLDS_FROM_DATE", width: 150, cellClass: 'grid-align center-align-cell', suppressMenu: true, suppressSorting: true, template: '<span>{{ data.BCLDS_FROM_DATE | date: "dd/MM/yyyy"}}</span >' },
        {
            headerName: "City", field: "CTY_CODE", width: 200, cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true, cellStyle: { "overflow": 'visible' }, cellRenderer: customEditor

        },
        {
            headerName: "Location", field: "LCM_CODE", width: 200, cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true, cellStyle: { "overflow": 'visible' }, cellRenderer: customEditor
        },
        {

            headerName: "Remarks", field: "REMARKS", width: 250, cellClass: 'grid-align', suppressMenu: true, suppressSorting: true, template: "<input type='text' style='width: 300px; height: 30px'  data-ng-model='data.REMARKS' >"
        }


    ];
    $scope.gridOptions1 = {
        columnDefs: columnDefs1,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: false,
        suppressVerticalScroll: false,
        enableFilter: true,
        rowHeight: 50,
        enableScrollbars: true,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions1.api.sizeColumnsToFit()
        }
    };



    ViewAndModifyScheduleService.GetLocationsall(1).then(function (response) {

        if (response.data !== null)
            $scope.DropdownLocations = response.data;
    });


    UtilityService.getCities(2).then(function (response) {
        if (response.data !== null)
            $scope.DropdownCity = response.data;
    });



    // dropdowns in grid
    var name, model;
    function customEditor(params) {

        var eCell = document.createElement('span');
        var index = params.rowIndex;
        if (params.column.colId === "CTY_CODE") {
            name = 'Cities' + index;
            model = $parse(name);
            $scope.CS = angular.copy($scope.DropdownCity);
            model.assign($scope, $scope.CS);
            eCell.innerHTML = '<div isteven-multi-select data-input-model="' + name + '"  data-output-model="CS.city" data-button-label="CTY_NAME" data-item-label="CTY_NAME"  data-on-item-click="getLocationsByCities(0)" data-on-select-all="CitySelectAll(' + index + ')" data-on-select-none="CitySelectNone(' + index + ')"  data-tick-property="ticked" data-max-labels="1" style="width:200px" ></div >';
            angular.forEach($scope.CS, function (val) {
                val.rowvalue = index;
            });
        }
        if (params.column.colId === "LCM_CODE") {
            name = 'Locations' + index;
            model = $parse(name);
            /*$scope.LocationsbyCity = [];*/
            $scope.LocationsbyCity = model($scope);
            model.assign($scope, $scope.LocationsbyCity);
            eCell.innerHTML = '<div isteven-multi-select data-input-model="' + name + '"  data-output-model="CS.Location" data-button-label="LCM_NAME" data-item-label="LCM_NAME"  data-on-item-click="getLocationsDetails(' + index + ')" data-on-select-all="LocationSelectAll(' + index + ')" data-on-select-none="LocationSelectNone(' + index + ')"  data-tick-property="ticked" data-max-labels="1" style="width:200px"></div >';
            angular.forEach($scope.CS, function (val) {
                val.rowvalue = index;
            });
        }
        return eCell;
    }





    $scope.getLocationsByCities = function (stsid) {
        var name, model;
        $scope.LocationsBC = [];

        angular.forEach($scope.CS.city, function (val1) {

            angular.forEach($scope.DropdownLocations, function (val2) {

                if (val1.CTY_CODE == val2.CTY_CODE) {

                    $scope.LocationsBC.push({
                        "CTY_CODE": val2.CTY_CODE,
                        "LCM_CODE": val2.LCM_CODE,
                        "LCM_NAME": val2.LCM_NAME
                    });
                }
            });
        });
        if (stsid == 1) {
            name = 'Locations' + SelectAllIndex;
        }
        else {
            name = 'Locations' + $scope.CS.city[0].rowvalue;
        }
        model = $parse(name);
        $scope.LocationsbyCity = angular.copy($scope.LocationsBC);
        model.assign($scope, $scope.LocationsbyCity);


    }


    $scope.locationsByDate = []; // Create an object to store locations 
    $scope.getLocationsDetails = function () {

        $scope.locationsByDate = [];
        angular.forEach($scope.CS.city, function (city) {
            angular.forEach($scope.LocationsbyCity, function (location) {
                if (location.ticked) {
                    $scope.locationsByDate.push({
                        "CTY_CODE": location.CTY_CODE,
                        "LCM_CODE": location.LCM_CODE,
                        "LCM_NAME": location.LCM_NAME
                    });
                }
            });
        });
    };

    var SelectAllIndex = "";

    $scope.CitySelectAll = function (id) {

        SelectAllIndex = id;
        $scope.CS.city = $scope.DropdownCity;
        $scope.getLocationsByCities("1");
    }
    $scope.CitySelectNone = function () {

        $scope.CS.city = [];
        $scope.getLocationsByCities("1");
    }

    $scope.LocationSelectAll = function (re) {
        $scope.Locations = $scope.LocationsbyCity;
        $scope.getLocationsDetails(re);
    }

    $scope.LocationSelectNone = function (re) {
        $scope.Locations = [];
        $scope.getLocationsDetails(re);
    }

    // modify  function
    $scope.ModifyScheduleVisits = function () {

        $scope.citiesandlocations = [];
        angular.forEach($scope.gridOptions1.rowData, function (row) {
            angular.forEach($scope.locationsByDate, function (location) {
                $scope.citiesandlocations.push({
                    "CTY_CODE": location.CTY_CODE,
                    "LCM_CODE": location.LCM_CODE,
                    "LCM_NAME": location.LCM_NAME,
                    "DATEVALUE": row.BCLDS_FROM_DATE,
                    "Remarks": row.REMARKS
                });
            });
        });

        var obj = {
            ModifyScheduleVisitsList: $scope.citiesandlocations,
            BCLDS_ID: bcldsId
        };
        ViewAndModifyScheduleService.ModifyScheduleVisits(obj).then(function (response) {

            if (response.data[0].MSG == 'SUCCESS') {
                $scope.ShowMessage = true;
                $scope.Success = "Schedule  Modified Successfully";
                progress(0, '', false);
                //showNotification("success", 8, 'bottom-right', $scope.Success);  
                $scope.secondgridvisible = false;
                $scope.LoadData();
                $scope.locationsByDate = [];
                $('#notification').text($scope.Success);
                setTimeout(function () {
                    $('#notification').text('');
                }, 3000);
            }
            else {
                $scope.ShowMessage = true;
                $scope.Success = "Schedule is not Modified";
                progress(0, '', false);
                //showNotification("error", 8, 'bottom-right', $scope.Success);
                $('#notification').text($scope.Success);
                setTimeout(function () {
                    $('#notification').text('');
                }, 3000);
            }
        });

    }

}]);