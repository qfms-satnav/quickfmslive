﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LogJD_kartin.aspx.vb" Inherits="LogJD_kartin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="google-signin-client_id" content="817305281174-8k1t7lombpfpo0gij169s90fcqo6imsq.apps.googleusercontent.com">
    <%--<meta name="google-signin-client_id" content="469715905219-73hob6mud1118mn4hoel5f5eg6fks432.apps.googleusercontent.com">--%>

    <title>QuickFMS :: Total Infrastructure Control</title>
    <%=ScriptCombiner.GetScriptTags("login_scripts", "css", 1)%>



    <!--[if lt IE 9]>
        <script defer src="BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
            color: black;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 140px;
        }
    </style>
    <style>
        #txtUsrPwd {
            -webkit-text-security: disc;
        }
    </style>
    <style>
        .btn-primary:hover, .btn-primary:focus, .btn-primary.focus, .btn-primary:active, .btn-primary.active, .open > .dropdown-toggle.btn-primary {
            color: white;
            background-color: #2caa9d;
            border-color: #2caa9d;
        }
    </style>
</head>
<body>
    <main class="auth-main">
        <div class="auth-block">
            <div class="col-md-10">
                <img src="BootStrapCSS/images/logo_quick.gif" alt="logo" style="margin-left: 150px; margin-right: 150px; height: 50px;" />
            </div>

            <%--  </div>--%>
            <div class="col-md-2">
                <i class="fa fa-lock fa-5x" aria-hidden="true"></i>
            </div>
            <br />
            <br />
            <form id="loginform" class="form-horizontal" role="form" runat="server" defaultfocus="tenantID">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>

                <asp:HiddenField ID="hidForModel" runat="server" />


                <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="hidForModel"
                    CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none; color: black">
                    You have already Logged in. Do you want to clear that Session?<br />
                    <br />
                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary " OnClick="btnYes_Click" />
                    <asp:Button ID="btnClose" runat="server" Text="No" CssClass="btn" OnClick="btnClose_Click" />
                </asp:Panel>
                <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSignIn">
                    <%--<asp:Label ID="lbl1" runat="server" CssClass="control-label" ForeColor="Red"></asp:Label>--%>
                    <asp:ValidationSummary ID="ValidationSummary2" CssClass="col-md-12" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                    <div class="row">
                        <div class="form-group">
                            <div class="row">
                                <asp:Label ID="lbl1" runat="server" CssClass="col-md-12" ForeColor="Red" Style="padding-left: 45px;"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tenantID" class="col-sm-3" style="color: white">Company ID</label>
                        <div class="col-sm-7">
                            <asp:RequiredFieldValidator ID="rfvtnt" runat="server" ControlToValidate="tenantID"
                                Display="None" ErrorMessage="Please Enter Company Id" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                            <asp:TextBox ID="tenantID" runat="server" type="text" class="form-control" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtUsrId" class="col-sm-3" style="color: white">Username</label>
                        <div class="col-sm-7">
                            <asp:RequiredFieldValidator ID="rfvusr" runat="server" ControlToValidate="txtUsrId"
                                Display="None" ErrorMessage="Please Enter Username" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtUsrId" runat="server" type="text" class="form-control" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="txtUsrPwd" class="col-sm-3" style="color: white">Password</label>
                        <div class="col-sm-7">
                            <asp:RequiredFieldValidator ID="rfvpwd" runat="server" ControlToValidate="txtUsrPwd"
                                Display="None" ErrorMessage="Please Enter Password" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtUsrPwd" runat="server" TextMode="Password" MaxLength="15" class="form-control" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="checkbox col-sm-6">
                            <div>
                                <a href="#" onclick="showPopWin()">Forgot Password!</a>
                            </div>
                        </div>
                        <div class="col-sm-6 controls">
                            <div>
                                <asp:Button ID="btnSignIn" runat="server" class="btn btn-primary custom-button-color pull-right" Text="Log In" ValidationGroup="Val1" />
                            </div>
                        </div>

                    </div>

                </asp:Panel>
                <!--Start Licence key entering    -->
                <asp:Panel ID="pnlLic" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                        ForeColor="Red" ValidationGroup="Val3" />
                    <div class="form-group text-center">
                        <%--  <asp:Label ID="lblSystemKey" runat="server" CssClass="control-label"></asp:Label>--%>
                    </div>
                    <div class="form-group text-center">
                        <b>
                            <asp:Label ID="lblKeyDesc" runat="server" CssClass="control-label" Text="License Code: "></asp:Label></b>
                        <asp:Label ID="lblKey" runat="server" CssClass="control-label" Text="Key"></asp:Label>
                    </div>
                    <div style="margin-bottom: 25px" class="input-group">

                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <asp:TextBox ID="txtSerialKey" runat="server" type="text" class="form-control" placeholder="Enter Activation Code"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSerialKey"
                            Display="None" ErrorMessage="Please Enter Valid Activation Code" ValidationGroup="Val3"> </asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 controls">
                        </div>
                        <div class="col-sm-6 controls">
                            <asp:Button ID="btnNext" runat="server" class="btn login-btn pull-right" Text="Next" ValidationGroup="Val3" />
                        </div>
                    </div>
                </asp:Panel>
                <!--End License key entering-->
                <div id="gConnect">
                    <div class="g-signin2" data-onsuccess="onSignIn">
                    </div>
                </div>
                <input type="hidden" runat="server" id="ni" />
            </form>
        </div>
    </main>

    <script src="BootStrapCSS/Scripts/jquery.min.js"></script>
    <script defer type="text/javascript">
        $(document).keydown(function (event) {
            if (event.keyCode == 123) {
                return false;
            }
            else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                return false;
            }
        });
        $(document).ready(function () {
            $('#content').height($(window).height());
        });
        if (self != top) {
            window.top.location.href = "login.aspx"
        }
    </script>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 400px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;                
                            <h4 class="modal-title">Forgot Password</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="250" frameborder="0" style="border: none"></iframe>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>


    <script src="https://apis.google.com/js/api.js"></script>
    <%--<script src="https://apis.google.com/js/platform.js" async defer></script>--%>

    <script type="text/javascript">
        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript'; po.async = true;
            po.src = 'https://plus.google.com/js/client:plusone.js';
            //po.src = 'BlurScripts/BlurJs/clientplusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();

    </script>
    <script type="text/javascript">
        function showPopWin() {

            $("#modalcontentframe").attr("src", "frmForgetPwd.aspx");
            $("#myModal").modal('show');
            return false;
            //$("#modelcontainer").load("frmForgetPwd.aspx", function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal('show');
            //});
        }



        gapi.load('auth2', function () {
            gapi.auth2.init();
        });

        function onSignIn(googleUser) {
            console.log(googleUser);
            var profile = googleUser.getBasicProfile();
            var id_token = googleUser.getAuthResponse().id_token;
            console.log(profile);

            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

            window.location.href = "GoogleAuth2.aspx?email=" + profile.getEmail() + "&token=" + id_token;
        }
    </script>

</body>

</html>
