﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script runat="server">
    
       
    </script>
<script type="text/javascript" defer>
            $(document).ready(function () {
                if ('AdityaBirla.dbo' = '<%= Session["COMPANYID"]%>') {
                    $("#hide").hide();
                    $("#hide1").show();
                } else {
                    $("#hide").show();
                    $("#hide1").hide();
                }
            });
    </script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>a-mantra - Error</title>
    <style>
        img.displayed {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        div.title {
            font-family: "Brush Script MT",cursive
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="col-md-12">
        <div style="height: 560px; position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);">
            <img class="displayed" src="BootStrapCSS/images/general_user_error_icon.png" />
            <div style="padding-top: 10px; text-align: center">
                <div class="title">
                    <h1>Sorry! Something went wrong.</h1>
                </div>
                <br />
                <div class="title" id="hide">
                    <h4>Please let us know about it by writing to <a href="mailto:support@quickfms.com" target="_top">support@quickfms.com</a></h4>
                </div>
                <div class="title" id="hide1">
                    <h4>Please let us know about it by writing to <a href="mailto:sahil.mulla@adityabirlacapital.com" target="_top">sahil.mulla@adityabirlacapital.com</a></h4>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />

    <div style="margin-left: 600px;">
    </div>
    </form>
</body>
</html>
