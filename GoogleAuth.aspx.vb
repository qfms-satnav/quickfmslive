﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.Web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Imports System.Net.Mail


Partial Class GoogleAuth
    Inherits System.Web.UI.Page
    Dim email As String


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim Remark As String = ""
        'If Remark = "ManualLogout" Then
        Session("useroffset") = "+05:30"
        email = Request.QueryString("email")
        If Not email Is Nothing Then
            Dim ds As New DataSet
            Dim address As New MailAddress(email)
            Dim ParentDB As String
            ParentDB = ConfigurationSettings.AppSettings("FRMDB").ToString()
            Dim host As String = address.Host
            Dim Tenant_Name As String = ""
            Dim Tenant_Id As String = ""
            Dim sp As New SubSonic.StoredProcedure(ParentDB + "." + "GET_DOMAIN")
            sp.Command.AddParameter("@DOMAIN", host, DbType.String)
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                Session("TENANT") = ds.Tables(0).Rows(0).Item("TENANT_ID")
                Tenant_Name = ds.Tables(0).Rows(0).Item("TENANT_NAME")
                GetDetailsByEmailId(email)
            Else
                Response.Redirect("GoogleAuthError.aspx?redirect=domain")
            End If
        Else
            Response.Redirect("GoogleAuthError.aspx?redirect=error")
        End If
        'End If
        'Response.Redirect("~/login.aspx")
    End Sub
    Public Sub GetDetailsByEmailId(ByVal email As String)
        Try


            Session("useroffset") = "+05:30"
            Dim ds As New DataSet
            Dim Aur_Id As String = ""
            Dim User_Role As String = ""
            Dim Ad_ID As String = ""

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") + "." + "GOOGLE_AUTHENTICATION")
            sp.Command.AddParameter("@email", email, DbType.String)
            ds = sp.GetDataSet
            If ds.Tables.Count > 0 Then
                Aur_Id = ds.Tables(0).Rows(0).Item("AUR_ID")
                Session("UID") = Aur_Id
                User_Role = ds.Tables(0).Rows(0).Item("URL_ROL_ID")
                Ad_ID = ds.Tables(0).Rows(0).Item("AD_ID")


                FormsAuthentication.Initialize()
                Dim authTicket As New FormsAuthenticationTicket(Aur_Id, True, 60)
                Dim encryptedticket As String = FormsAuthentication.Encrypt(authTicket)
                Dim authcookie As New HttpCookie(FormsAuthentication.FormsCookieName, encryptedticket)

                Session("COMPANYID") = ds.Tables(0).Rows(0).Item("COMPANYID")

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CHECK_CATEGORY_DEPRECIATION")
                Dim ds1 As New DataSet
                ds1 = sp1.GetDataSet()
                Session("DepMethod") = ds1.Tables(0).Rows(0).Item("returnstatus")

                Response.Cookies.Add(authcookie)
                Session("LoginTime") = getoffsetdatetime(DateTime.Now)
                Session("uname") = Ad_ID
                displaybsmdata()
                GET_ASSET_MODULE_CHECK_FOR_USER()
                GetuserRoleMappingEdit()
                Dim lastlogin As DateTime = savelastlogin(Session("UID"))
                Session("Lastlogintime") = lastlogin.ToString("dd MMM yyyy HH:mm:ss")
                Response.Redirect("frmAMTDefault.aspx")
            Else
                Response.Redirect("GoogleAuthError.aspx?redirect=invalid")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub displaybsmdata()
        Try


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_BSM_GETALL")
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                Session("Parent") = ds.Tables(0).Rows(0).Item("AMT_BSM_PARENT")
                Session("Child") = ds.Tables(0).Rows(0).Item("AMT_BSM_CHILD")
                'Session("GHT") = ds.Tables(1).Rows(0).Item("ASM_PARENT")
                If ds.Tables(1).Rows.Count > 0 Then
                    Session("GHT") = ds.Tables(1).Rows(0).Item("ASM_PARENT")
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub GET_ASSET_MODULE_CHECK_FOR_USER()
        Try


            Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "GET_ASSET_MODULE_CHECK_FOR_USER")
            sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                Dim asset = ds.Tables(0).Rows(0).Item("T_STA_ID")
                If asset = 1 Then
                    Get_asset_sysp_value()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub Get_asset_sysp_value()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AST_SYSP_VALUE")
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                Session("Procurement") = ds.Tables(0).Rows(0).Item("AST_SYSP_VAL1")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub GetuserRoleMappingEdit()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USERROLEMAPPINGEDIT")

            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                Session("USER_EDIT") = ds.Tables(0).Rows(0).Item("SYSP_VAL1")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function savelastlogin(ByVal UserName As String) As DateTime
        Dim ValidateStatus As DateTime = getoffsetdatetime(DateTime.Now)
        Dim Mode As String = "1"
        Dim ds As New DataSet

        Try

            Dim param As SqlParameter() = New SqlParameter(2) {}
            param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
            param(0).Value = UserName
            param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
            param(1).Value = Mode
            param(2) = New SqlParameter("@TIMEOUT", SqlDbType.NVarChar)
            param(2).Value = AppSettings("TIMEOUT").ToString()
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SAVE_USER_LASTLOGIN", param)
            Session("LoginUniqueID") = ds.Tables(0).Rows(0).Item("SNO")
            ValidateStatus = ds.Tables(0).Rows(0).Item("LASTLOGINTIME")
        Catch ex As Exception
        Finally

        End Try
        Return ValidateStatus
    End Function
End Class
