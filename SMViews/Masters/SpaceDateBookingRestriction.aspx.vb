﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class SMViews_Masters_SpaceDateBookingRestriction
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If


        If Not Page.IsPostBack Then
            FetchData()
            BindAllocGrades()
        End If
    End Sub
    Public Sub BindAllocGrades()
        ddlspctype.Items.Clear()
        ObjSubsonic.BindListBox(ddlspctype, "Get_Space_Type", "SPACE_TYPE", "SNO")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim strEroorMsg As String = String.Empty
        Dim selectedIndex As String = String.Empty

        For Each item As ListItem In ddlspctype.Items
            If item.Selected Then
                If selectedIndex = "" Then
                    selectedIndex = item.Value
                Else
                    selectedIndex = selectedIndex & ", " & item.Value
                End If
            End If
        Next

        Dim SlectSpcType = ddlspctype.GetSelectedIndices().ToList()
        Dim SlectSpcTypeArray = (From c In SlectSpcType Select ddlspctype.Items(c).Value).ToList()
        Dim FinalSpcTypes As String = String.Join(",", SlectSpcTypeArray.ToArray())

        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@DAYS_COUNT", SqlDbType.Int, 200)
        param(1).Value = totalcount.Text
        param(2) = New SqlParameter("@SPC_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = FinalSpcTypes

        Dim iStatus As String = ObjSubsonic.GetSubSonicExecute("INSERT_RESTRICT_DAYS_COUNT", param)
        If iStatus = "1" Then
            lblMsg.Text = "Successfully Uploaded "
            lblMsg.Visible = True
            Cleardata()
            FetchData()
        End If
    End Sub


    Protected Sub FetchData()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "RESTRICT_DAYS_COUNT")
        Dim ds As New DataSet
        ds = sp.GetDataSet

        GradesWise.DataSource = ds
        GradesWise.DataBind()
        'Dim Value As String
        'Value = Convert.ToString(ds.Tables(0).Rows(0)("RESTRICT_DAYS_COUNT"))

        'If (Value > 0) Then
        '    Result.InnerText = "Restricted no of days : " + Value + " Days"
        'Else
        '    Result.InnerText = "Restricted no of days : No Restriction"
        'End If

    End Sub
    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Cleardata()
    End Sub

    Public Sub Cleardata()

        totalcount.Text = ""
    End Sub

End Class
