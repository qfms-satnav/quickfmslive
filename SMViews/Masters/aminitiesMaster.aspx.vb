Imports System.Data
Imports System.Data.SqlClient
Imports ICSharpCode.SharpZipLib.Zip
Imports SubSonic

Partial Class HDM_HDM_Webfiles_Masters_statusMaster
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetAminities()
        End If


    End Sub

    Private Sub GetAminities()
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMINITIES")
        Dim ds1 As DataSet = spSts.GetDataSet()

        rptStatus.DataSource = ds1.Tables(0)
        rptStatus.DataBind()

        For Each row As DataRow In ds1.Tables(0).Rows
            Dim aminity As String = row("AMINITY")
            Dim aminityId As Integer = Convert.ToInt32(row("AMINITY_ID"))

            Dim aminityDesc As String
            If Not IsDBNull(row("AMINITY_DESC")) Then
                aminityDesc = row("AMINITY_DESC").ToString()
            Else
                aminityDesc = ""
            End If
        Next
    End Sub

    Protected Sub SubmitButton_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim aminity As String = Request.Form("AMINITY")
        Dim aminityDesc As String = Request.Form("AMINITY_DESC")

        Try
            Dim connectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString

            Using connection As New SqlConnection(connectionString)
                connection.Open()

                Dim commandText As String = Session("TENANT") & "." & "ADD_AMINITY"
                Using command As New SqlCommand(commandText, connection)
                    command.CommandType = CommandType.StoredProcedure

                    command.Parameters.AddWithValue("@AMINITY", aminity)
                    command.Parameters.AddWithValue("@AMINITY_DESC", aminityDesc)

                    command.ExecuteNonQuery()
                End Using
            End Using
            GetAminities()

        Catch ex As Exception
            ' Handle exceptions
        End Try
    End Sub

    Protected Sub delete_Click(sender As Object, e As EventArgs)
        Dim lnkDelete As LinkButton = CType(sender, LinkButton)
        Dim amenityID As Integer = Convert.ToInt32(lnkDelete.CommandArgument)

        deleteAmenity(amenityID)
    End Sub


    Private Sub deleteAmenity(amenityID As Integer)
        Try
            Dim connectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString

            Using connection As New SqlConnection(connectionString)
                connection.Open()

                Dim commandText As String = Session("TENANT") & "." & "DELETE_AMINITY"
                Using command As New SqlCommand(commandText, connection)
                    command.CommandType = CommandType.StoredProcedure

                    command.Parameters.AddWithValue("@AMINITY_ID", amenityID)

                    command.ExecuteNonQuery()
                End Using
            End Using
            GetAminities()

        Catch ex As Exception
            ' Handle exceptions
        End Try
    End Sub



End Class