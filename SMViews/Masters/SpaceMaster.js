﻿
app.service('SpaceMasterService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.SpaceMasterAllSpaces = function (searchObjs) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceMaster/GetVacantSpaces', searchObjs)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SpaceMaster_SeatType = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceMaster/GetSeatType')
            .then(function (responsea) {
                deferred.resolve(responsea.data);
                return deferred.promise;
            }, function (responsea) {
                deferred.reject(responsea);
                return deferred.promise;
            });
    };

  

    this.SpaceMaster_SubSeattype = function (data) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceMaster/SeatSubTypes')
            .then(function (responsea) {
                deferred.resolve(responsea.data);
                return deferred.promise;
            }, function (responsea) {
                deferred.reject(responsea);
                return deferred.promise;
            });
    };
    this.SpaceMasterSpaceSeattype = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceMaster/SpaceSubTypes')
            .then(function (responsea) {
                deferred.resolve(responsea.data);
                return deferred.promise;
            }, function (responsea) {
                deferred.reject(responsea);
                return deferred.promise;
            });
    };

    this.RaiseRequest = function (searchObjsa) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceMaster/RaiseRequest', searchObjsa)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.RaiseRequest2 = function (searchObjsa) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceMaster/UpdateRequest', searchObjsa)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('SpaceMasterController', ['$scope', '$q', '$location', 'SpaceMasterService', 'UtilityService', '$filter', function ($scope, $q, $location, SpaceMasterService, UtilityService, $filter) {

    $scope.SpaceMaster = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.SelLayers = [];
    $scope.currentblkReq = {};
    //$scope.selectedSpaces = [];
    $scope.SelRowData = [];
    $scope.SpaceMaster.Country = [];
    $scope.SpaceMaster.City = [];
    $scope.SpaceMaster.Location = [];
    $scope.SpaceMaster.Tower = [];
    $scope.SpaceMaster.Floor = [];
    $scope.SPACE_TYPE = [];
    $scope.SST_NAME2 = [];
    $scope.SST_NAME = [];
    $scope.SpaceType = [];
    $scope.expression = 'ST';
    $scope.GridVisiblity = true;
    //$scope.expression2 = false;

    $scope.GenerateExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {
            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SpaceData.csv"
            //moment(new Date()).format('MMMM d, YYYY')
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

   
    $scope.GetSpaceType = function () {
        SpaceMasterService.SpaceMaster_SeatType().then(function (response) {
            if (response != null) {
                $scope.SPACE_TYPE = response;
                setTimeout(function () {
                    $('.selectpicker').selectpicker('refresh');
                }, 200);
            }
        });
    }

    $scope.GetSubSpace = function () {
        SpaceMasterService.SpaceMaster_SubSeattype().then(function (response) {
            if (response != null) {
                //$scope.SST_NAME = response;
                $scope.SST_NAME2 = response;
                setTimeout(function () {
                    $('.selectpicker').selectpicker('refresh');
                }, 200);
            }
        });
    }
    $scope.GetSpaceSubType = function () {
        SpaceMasterService.SpaceMasterSpaceSeattype().then(function (response) {
            if (response != null) {
                $scope.SpaceType = response;
                setTimeout(function () {
                    $('.selectpicker').selectpicker('refresh');
                }, 200);
            }
        });
    }
    $scope.SpaceSubChange = function () {
        $scope.SST_NAME = [];
        //$scope.currentblkReq.SST_CODE = '';
        angular.forEach($scope.SST_NAME2, function (value, key) {
            if (value.SST_ID == $scope.currentblkReq.SPC_TYPE_CODE) {
                $scope.SST_NAME.push(value);
            }
        });
        setTimeout(function () {
            $('.selectpicker').selectpicker('refresh');
        }, 200);
    }
    $scope.Submit = function () {

        if ($scope.currentblkReq.ALLOCSTA==1) {
            $scope.RaiseRequest();
        } else {
            $scope.RaiseRequest2();
        }
    }

    $scope.RaiseRequest = function () {
        {
            $scope.selectedSpaces = [];
            angular.forEach($scope.Markers, function (Value, Key) {
                $scope.selspcObj = {};
                if (Value.ticked) {
                    $scope.selspcObj.SVD_SPC_ID = Value.SVD_SPC_ID;
                    $scope.selspcObj.SVD_SPACE_TYPE_NAME = Value.SVD_SPACE_TYPE_NAME;
                    $scope.selspcObj.SVD_SPC_SUB_TYPE = Value.SVD_SPC_SUB_TYPE;
                    $scope.selspcObj.FLR_ID = Value.FLR_ID;
                    $scope.selectedSpaces.push($scope.selspcObj);
                }
            });
            //$scope.currentblkReq.STACHECK = 4;
            if ($scope.selectedSpaces.length != 0) {
                progress(0, 'Loading...', true);
                var ReqObj = { flrlst: $scope.SpaceMaster.Floor, verreqdet: $scope.selectedSpaces, spclst: $scope.currentblkReq.SNO, ALLOCSTA: $scope.currentblkReq.ALLOCSTA };
                SpaceMasterService.RaiseRequest(ReqObj).then(function (response) {
                    if (response.data != null) {
                        $scope.Markers = [];
                        /* $scope.Clear();*/
                        GetMarkers(response.data)
                        $scope.gridOptions.api.setRowData($scope.Markers);
                        $scope.GridVisiblity = false;
                        progress(0, '', false);
                        showNotification('success', 8, 'bottom-right', response.Message);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }

                }, function (response) {
                    progress(0, '', false);
                });
            }
            else
                showNotification('error', 8, 'bottom-right', 'Please select atleast one space ID');
        }
    }

    $scope.RaiseRequest2 = function () {
        {
            $scope.selectedSpaces = [];
            angular.forEach($scope.Markers, function (Value, Key) {
                $scope.selspcObj = {};
                if (Value.ticked) {
                    $scope.selspcObj.SVD_SPC_ID = Value.SVD_SPC_ID;
                    $scope.selspcObj.SVD_SPACE_TYPE_NAME = Value.SVD_SPACE_TYPE_NAME;
                    $scope.selspcObj.SVD_SPC_SUB_TYPE = Value.SVD_SPC_SUB_TYPE;
                    $scope.selspcObj.FLR_ID = Value.FLR_ID;
                    $scope.selectedSpaces.push($scope.selspcObj);
                }
            });
            if ($scope.selectedSpaces.length != 0) {
                progress(0, 'Loading...', true);
                var ReqObj = { flrlst: $scope.SpaceMaster.Floor, verreqdet: $scope.selectedSpaces, spcsub: $scope.currentblkReq.SST_CODE, ALLOCSTA: $scope.currentblkReq.ALLOCSTA, SpaceType:$scope.currentblkReq.SPC_TYPE_CODE };
                SpaceMasterService.RaiseRequest2(ReqObj).then(function (response) {
                    if (response.data != null) {
                        $scope.Markers = [];
                        /* $scope.Clear();*/
                        GetMarkers(response.data)
                        $scope.gridOptions.api.setRowData($scope.Markers);
                        $scope.GridVisiblity = false;
                        progress(0, '', false);
                        showNotification('success', 8, 'bottom-right', response.Message);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }

                }, function (response) {
                    progress(0, '', false);
                });
            }
            else
                showNotification('error', 8, 'bottom-right', 'Please select atleast one space ID');
        }
    }

    function GetMarkers(data) {
        jQuery.each(data, function (index, value) {
            $scope.marker = {};
            $scope.marker.SVD_SPC_ID = value.SVD_SPC_ID;
            $scope.marker.SVD_SPC_NAME = value.SVD_SPC_NAME;
            $scope.marker.SVD_SPC_TYPE_NAME = value.SVD_SPC_TYPE_NAME;
            $scope.marker.SVD_SPC_SUB_TYPE = value.SVD_SPC_SUB_TYPE;
            $scope.marker.SVD_SPACE_TYPE_NAME = value.SVD_SPACE_TYPE_NAME;
            $scope.marker.SVD_SPC_SUB_TYPE_NAME = value.SVD_SPC_SUB_TYPE_NAME;
            $scope.marker.FLR_ID = value.FLR_ID;
            $scope.marker.FLR_NAME = value.FLR_NAME;
            /*  $scope.marker.STACHECK = value.STACHECK;*/
            $scope.marker.ticked = value.ticked;
            $scope.Markers.push($scope.marker);
        });
    };

    $scope.SearchSpaces = function () {
        {
            progress(0, 'Loading...', true);
            $scope.selectedSpaces = [];
            $scope.Markers = [];
            var searchObjs = { flrlst: $scope.SpaceMaster.Floor, verreq: $scope.currentblkReq };
            SpaceMasterService.SpaceMasterAllSpaces(searchObjs).then(function (response) {
                if (response.data != null) {
                    $scope.GetSpaceType();
                    $scope.GetSpaceSubType();
                    $scope.GetSubSpace();
                    GetMarkers(response.data);
                    $scope.gridOptions.api.setRowData($scope.Markers);
                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }

            }, function (response) {
                progress(0, '', false);
            });
        }
    }
    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = true;
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = false;
                    });

                });
            }
        });
        return eHeader;
    }

    var header = [

        { headerName: "Select All", field: "ticked", suppressMenu: true, width: 150, template: "<input type='checkbox' ng-model='data.ticked'' />", headerCellRenderer: headerCellRendererFunc },
        { headerName: "Space ID", field: "SVD_SPC_NAME", cellClass: "grid-align" },
        { headerName: "Floor Name", field: "FLR_NAME", cellClass: "grid-align" },
        { headerName: "Seat Type", field: "SVD_SPACE_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Space Type", field: "SVD_SPC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SVD_SPC_SUB_TYPE_NAME", cellClass: "grid-align" },

    ];


    $scope.gridOptions = {
        columnDefs: header,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };




    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;

                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;

                            UtilityService.getTowers(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Tower = response.data;

                                    UtilityService.getFloors(2).then(function (response) {
                                        if (response.data != null) {
                                            $scope.Floor = response.data;
                                        }
                                    });

                                }
                            });
                        }
                    });
                }
            });

        }
    });


    $scope.CnyChangeAll = function () {
        $scope.SpaceMaster.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.SpaceMaster.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.SpaceMaster.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.SpaceMaster.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SpaceMaster.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.SpaceMaster.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        $scope.SpaceMaster.Country = [];

        UtilityService.getLocationsByCity($scope.SpaceMaster.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SpaceMaster.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SpaceMaster.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.SpaceMaster.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {

        $scope.SpaceMaster.Country = [];
        $scope.SpaceMaster.City = [];
        UtilityService.getTowerByLocation($scope.SpaceMaster.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SpaceMaster.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SpaceMaster.City.push(cty);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SpaceMaster.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.SpaceMaster.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        $scope.SpaceMaster.Country = [];
        $scope.SpaceMaster.City = [];
        $scope.SpaceMaster.Location = [];

        UtilityService.getFloorByTower($scope.SpaceMaster.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SpaceMaster.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SpaceMaster.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.SpaceMaster.Location.push(lcm);
            }
        });



    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SpaceMaster.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.SpaceMaster.Floor = [];
        $scope.FlrChanged();
    }

    $scope.FlrChanged = function () {
        $scope.SpaceMaster.Country = [];
        $scope.SpaceMaster.City = [];
        $scope.SpaceMaster.Location = [];
        $scope.SpaceMaster.Tower = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SpaceMaster.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SpaceMaster.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.SpaceMaster.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (twr != undefined && value.ticked == true && twr.ticked == false) {
                twr.ticked = true;
                $scope.SpaceMaster.Tower.push(twr);
            }
        });

    }


    $scope.Clear = function () {
        $scope.currentblkReq = {};
        $scope.SpaceMaster = {};
        $scope.selectedSpaces = [];
        $scope.Markers = [];
        //$scope.gridOptions.rowData = [];
        $scope.GridVisiblity = true;
        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Location, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floor, function (floor) {
            floor.ticked = false;
        });

        $scope.SpaceMaster.Country = [];
        $scope.SpaceMaster.City = [];
        $scope.SpaceMaster.Location = [];
        $scope.SpaceMaster.Tower = [];
        $scope.SpaceMaster.Floor = [];
        $scope.frmSearchspc.$submitted = false;
    }

}]);


