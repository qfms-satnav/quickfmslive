﻿app.service("UploadSpacesService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.downloadTemplate = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UploadSpaceAllocation/DownloadTemplate1', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('UploadSpacesController', ['$scope', '$q', '$location', 'UploadSpacesService', 'UtilityService', '$filter', '$http', function ($scope, $q, $location, UploadSpacesService, UtilityService, $filter, $http) {    $scope.UploadSpaces = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.BsmDet = {};
    $scope.ToggleDiv = false;
    $scope.AllocType = [{ CODE: "All", NAME: "All" }, { CODE: "Vacant", NAME: "Vacant" }, { CODE: "Allocated", NAME: "Allocated" }];
    $scope.UplAllocType = [{ CODE: "Alloc", NAME: "Only Allocate" }, { CODE: "RelAlloc", NAME: "Release Allocate" }, { CODE: "Relocate", NAME: "Re-Locate" }];
    $scope.UplOptions = [{ CODE: "ALLOCDB", NAME: "Allocate based on Employee Data", isChecked: false }];
    $scope.ToggleDiv = false;
    $scope.DispDiv = false;

    $scope.Load = function () {
        UtilityService.getCountires(2).then(function (Countries) {
            if (Countries.data != null) {
                $scope.Country = Countries.data;
            }
        });
        UtilityService.getBussHeirarchy().then(function (response) {
            if (response.data != null) {
                $scope.BsmDet = response.data;
            }
        });
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
            }
        });
        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
            }
        });
        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
            }
        });
        UtilityService.getTowers(2).then(function (response) {
            if (response.data != null) {
                $scope.Tower = response.data;
            }
        });

        UtilityService.getFloors(2).then(function (response) {
            if (response.data != null) {
                $scope.Floor = response.data;
            }
        });
    }
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.gridOptions.columnApi.getColumn("VERTICAL").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridOptions.columnApi.getColumn("COSTCENTER").colDef.headerName = $scope.BsmDet.Child;
            $scope.gridOptions.api.refreshHeader();
        }
    });

    /***    Page loading        ***/
    $scope.Load();

    //// Country Events
    $scope.CnyChangeAll = function () {
        $scope.UploadSpaces.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.UploadSpaces.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.UploadSpaces.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.UploadSpaces.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.UploadSpaces.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.UploadSpaces.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.UploadSpaces.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });
    }
    $scope.redirect = function () {
        window.location = "/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx";
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.UploadSpaces.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.UploadSpaces.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.UploadSpaces.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UploadSpaces.City.push(cty);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.UploadSpaces.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.UploadSpaces.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.UploadSpaces.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UploadSpaces.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.UploadSpaces.Location[0].push(lcm);
            }
        });



    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.UploadSpaces.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.UploadSpaces.Floor = [];
        $scope.FlrChanged();
    }



    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UploadSpaces.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.UploadSpaces.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.UploadSpaces.Tower.push(twr);
            }
        });

    }

    /***********  Download data Values   ***************/


    $scope.DownloadExcel = function () {
        $scope.dataobj = { flrlst: $scope.UploadSpaces.Floor, ALLOCSTA: $scope.UploadSpaces.AllocType };
        progress(0, 'Loading...', true);
        $http({
            url: UtilityService.path + '/api/UploadSpaceAllocation/DownloadTemplate1',
            method: 'POST',
            responseType: 'arraybuffer',
            data: $scope.dataobj, //this is your json data string
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            }
        }).then(function (data) {
            if (data.data.byteLength > 27) {
                //if (data != undefined) {
                var blob = new Blob([data.data], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                });
                save(blob, 'SpaceUploadTemplateFor_' + $scope.UploadSpaces.AllocType + '_seats.xlsx');
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "No data Found");

            }
        }).catch(function () {
            progress(0, '', false);
            //Some error log
        });

        //UploadSpacesService.downloadTemplate($scope.dataobj).then(function (result) {
        //    var uriString = parseReturn(result);
        //    location.href = "uriString"

        //});
    }

    function save(blob, fileName) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, fileName);
            progress(0, '', false);
        } else {
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
            window.URL.revokeObjectURL(link.href);
            $scope.ToggleDiv = !$scope.ToggleDiv;
            progress(0, '', false);
        }
    }


    var columnDefs = [
        { headerName: "Remarks", field: "REMARKS", cellClass: "grid-align" },
        { headerName: "City", field: "CITY", cellClass: "grid-align" },
        { headerName: "Location", field: "LOCATION", cellClass: "grid-align" },
        { headerName: "Tower", field: "TOWER", cellClass: "grid-align" },
        { headerName: "Floor", field: "FLOOR", cellClass: "grid-align" },
        { headerName: "Space ID", field: "SPACE_ID", cellClass: "grid-align" },
        { headerName: "Seat Type", field: "SEATTYPE", cellClass: "grid-align" },
        { headerName: "", field: "VERTICAL", cellClass: "grid-align" },
        { headerName: "Entity", field: "ENTITY", cellClass: "grid-align" },
        { headerName: "", field: "COSTCENTER", cellClass: "grid-align" },
        { headerName: "Employee ID", field: "EMP_ID", cellClass: "grid-align" },
        { headerName: "From Date", field: "FROM_DATE", cellClass: "grid-align" },
        { headerName: "To Date", field: "TO_DATE", cellClass: "grid-align" },
        { headerName: "From Time", field: "FROM_TIME", cellClass: "grid-align" },
        { headerName: "To Time", field: "TO_TIME", cellClass: "grid-align" }

    ];

    var SummarycolumnDefs = [
        { headerName: "REMARKS", field: "REMARKS", cellClass: "grid-align" },
        { headerName: "COUNT", field: "COUNT", cellClass: "grid-align" }

    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
    };

    $scope.gridSummaryOptions = {
        columnDefs: SummarycolumnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
        //onReady: function () {
        //    $scope.gridSummaryOptions.api.sizeColumnsToFit()
        //},
    };

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    $scope.Clear = function () {
        $scope.UploadSpaces = {};
        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Location, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floor, function (floor) {
            floor.ticked = false;
        });
        $scope.UploadSpaces.Country = [];
        $scope.UploadSpaces.City = [];
        $scope.UploadSpaces.Location = [];
        $scope.UploadSpaces.Tower = [];
        $scope.UploadSpaces.Floor = [];
    };

    $scope.TotalRecEnabled = true;
    /////  Upload File
    $scope.UploadFile = function () {
        if ($('#FileUpl', $('#FileUsrUpl')).val()) {
            progress(0, 'Loading...', true);
            var selectedoptions = $filter('filter')($scope.UplOptions, { isChecked: true });
            var selopt = _.map(selectedoptions, 'CODE').join(",");
            var filetype = $('#FileUpl', $('#FileUsrUpl')).val().substring($('#FileUpl', $('#FileUsrUpl')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                }
                else {
                    var formData = new FormData();
                    var UplFile = $('#FileUpl')[0];
                    var CurrObj = { UplOptions: selopt, UplAllocType: $scope.UploadSpaces.UplAllocType };
                    console.log(CurrObj);
                    formData.append("UplFile", UplFile.files[0]);
                    formData.append("CurrObj", JSON.stringify(CurrObj));

                    // Check the number of rows in the Excel file
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var data = e.target.result;
                        var workbook = XLSX.read(data, { type: 'binary' });
                        var worksheet = workbook.Sheets[workbook.SheetNames[0]];
                        var rowCount = XLSX.utils.decode_range(worksheet['!ref']).e.r + 1;

                        if (rowCount > 3001) {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Upload limit exceeded. Please upload a file with up to 3000 rows.');
                            return;
                        }

                        $.ajax({
                            url: UtilityService.path + "/api/UploadSpaceAllocation/UploadTemplate1",
                            type: "POST",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if (response.data != null) {
                                    if (response.data.Table != null) {
                                        $scope.TotalRecEnabled = false;
                                        $scope.Summary = [];
                                        $scope.Summary = response.data.Table1[0].TOTAL_RECORDS;

                                        $scope.AllSummary = [];
                                        $scope.AllSummary = response.data.Table2;
                                        $scope.$apply(function () {
                                            $scope.DispDiv = true;
                                            $scope.gridOptions.api.setRowData(response.data.Table);
                                            $scope.gridSummaryOptions.api.setRowData(response.data.Table2);
                                            progress(0, '', false);
                                        });


                                        progress(0, '', false);
                                        showNotification('success', 8, 'bottom-right', response.Message);
                                    }
                                }
                                else {

                                    progress(0, '', false);
                                    showNotification('error', 8, 'bottom-right', response.Message);
                                }
                            }
                        });
                    };
                    reader.readAsBinaryString(UplFile.files[0]);
                }

            }
            else {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Upload Excel Format only (.xlsx)');
            }

        }
        else
            showNotification('error', 8, 'bottom-right', 'Select File To Upload');

    }



    //// exporting to excel
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" },
        { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" },
        { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" },
        { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("CustomizationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "CustomizationReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {

        progress(0, 'Loading...', true);

        if (Type == "pdf") {
            $scope.GenerateFilterPdf();
        }
        else {
            $scope.GenerateFilterExcel();
        }

    }

    $scope.GenReport1 = function (Customized, Type) {
        var Customized = {
            Type: 'xlsx'
        };
        progress(0, 'Loading...', true);
        Customized.Type = 'xlsx';
        $scope.DocTypeVisible = 0
        progress(0, 'Loading...', true);
        $http({
            url: UtilityService.path + '/api/UserRoleMappingNew/GetEmployeeData',
            method: 'POST',
            data: Customized,
            responseType: 'arraybuffer'
        }).then(function (data, status, headers, config) {
            var file = new Blob([data.data], {
                type: 'application/' + Type
            });
            var fileURL = URL.createObjectURL(file);
            $("#reportcontainer").attr("src", fileURL);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = 'EmployeeData.' + Type;
            document.body.appendChild(a);
            a.click();
            progress(0, '', false);
        }), function (error, data, status, headers, config) {
            progress(0, '', false);
        };
        //}
    };

}]);