<%@ Page Language="VB" AutoEventWireup="true" CodeFile="aminitiesMaster.aspx.vb" Inherits="HDM_HDM_Webfiles_Masters_statusMaster" %>


<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Status Master</title>
     <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <style>
        .tablecss {
                width: 100%;
    max-width: 100%;
    border: 1px solid #2556ad;
    border-radius: 5px;
        }
        thead tr {
    height: 48px !important;
    line-height: 48px !important;
    border-radius: 5px;
    background: #ebf0fb !important;
    color: #0f3a9f !important;
    font-size: 12px;
    padding: 0 !important;
    font-weight: bold;
    text-align : center;
}

        thead tr th, tbody tr td{
            text-align : center;
            border-right : 1px solid #2556ad;
        }

                body tr {
    height: 39px !important;
    line-height: 39px !important;
    border-radius: 5px;
    /*background: white !important;*/
    font-size: 12px;
    text-align : center;
    border-bottom: 1px solid #2556ad;
}
    </style>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Amenities Master</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server" data-valid-submit="Save()">
                    <div>

                        <div class="row">
                             <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-show="ActionStatus==1">
                                    <label>Amenities<span style="color: red;">*</span></label>
                                      <input id="AMINITY" type="text" name="AMINITY" class="form-control color-picker" required="required" />
                                </div>
                            </div>

                              <div class="col-md-3 col-sm-12 col-xs-12">
     <div class="form-group" data-ng-show="ActionStatus==1">
         <label>Description</label>
           <input id="AMINITY_DESC" type="text" name="AMINITY_DESC" class="form-control color-picker"/>
     </div>
 </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="button" id="SubmitButton" value="Submit" class="btn btn-primary custom-button-color" runat="server" OnServerClick="SubmitButton_Click" />
<%--                                    <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" onclick="clearForm()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>--%>
                                </div>
                            </div>
                        </div>

                    </div>
                    <%--data table--%> 
                    <div class="row">
                        <div class="col-md-12">
                               <table class="tablecss">
                                <thead>
                                    <tr>
                                        <th>Amenities Id</th>
                                        <th>Amenities</th>
                                        <th>Description</th>
                                         <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptStatus" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                               <td><%# Eval("AMINITY_ID") %></td>
                                               <td><%# Eval("AMINITY") %></td>
                                               <td><%# Eval("AMINITY_DESC") %></td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClick="delete_Click" CommandArgument='<%# Eval("AMINITY_ID") %>'>
                                                    <i class="fa fa-trash" style="color: red;"></i>
                                                        
                                                </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">

    <script>
   


    </script>

</body>
</html>


