﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SpaceDateBookingRestriction.aspx.vb" Inherits="SMViews_Masters_SpaceDateBookingRestriction" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->


</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Shift Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Space Booking Date Restriction</h3>
            </div>
            <div class="card">
                <div class="card-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                            ForeColor="red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="False">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Space Type<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="SpaceType" runat="server"
                                        ControlToValidate="ddlspctype" Display="None" ErrorMessage="Please Select Space Type"
                                        ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class="col-md-12">
                                        <asp:ListBox SelectionMode="Multiple" ID="ddlspctype" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:ListBox>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">No. of Days Restrict<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="countValidator" runat="server" ControlToValidate="totalcount"
                                        Display="none" ErrorMessage="Please Enter No. of Days you want to Restrict" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="TypeCheck" runat="server"
                                        ControlToValidate="totalcount" Display="None" ErrorMessage="Only numeric allowed in Count."
                                        ValidationExpression="^[0-9]*$" ValidationGroup="Val1">
                                    </asp:RegularExpressionValidator>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="totalcount" runat="server" CssClass="form-control" autocomplete="off" placeholder="Enter No. of Days to Restrict"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Update" ValidationGroup="Val1"></asp:Button>
                                <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear"></asp:Button>
                                <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/Masters/Mas_Webfiles/frmMasSpaceMasters.aspx"></asp:Button>
                            </div>
                        </div>

                        <%--<div>
                                    <label id="Result" runat="server" style="color:green"  class="col-md-12 control-label">Restriced no of days :</label>
                                    

                                </div>--%>

                        <div id="GradeGrid" runat="server">
                            <div class="col-md-12">
                                <asp:GridView ID="GradesWise" runat="server" AutoGenerateColumns="False" AllowPaging="True" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                    Width="100%" PageSize="10" CssClass="table GridStyle" GridLines="None" EmptyDataText="No Data Found..">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SPACE TYPE">
                                            <ItemTemplate>
                                                <asp:Label ID="Employee_Grade" runat="server" Text='<%#Eval("SPACE_TYPE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Restricted Count">
                                            <ItemTemplate>
                                                <asp:Label ID="Allocation_Count" runat="server" Text='<%#Eval("Count")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <%--  </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function ShowHelpWindow() {
            window.open('frmPrjHead.aspx', 'Window', 'toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450');
            return false;
        }
    </script>
</body>
</html>


