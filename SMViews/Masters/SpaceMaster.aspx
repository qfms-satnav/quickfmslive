﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/FMS_UI/css/flaticon.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
    <%--<style>
        .modal-body {
            position: absolute !important;
            padding: 2px !important;
        }

    </style>--%>
</head>
<body data-ng-controller="SpaceMasterController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Space Master Change</h3>
            </div>
            <div class="card">
                <div class="clearfix">
                    <div class="box-footer text-right">
                        <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                    </div>
                </div>
                <br />
                <form id="Form1" name="frmSearchspc" data-valid-submit="SearchSpaces()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Country" data-output-model="SpaceMaster.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceMaster.Country" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid">Please select country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid}">
                                <label class="control-label">City <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="SpaceMaster.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceMaster.City" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid">Please select city </span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="SpaceMaster.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceMaster.Location" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid">Please select location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Tower" data-output-model="SpaceMaster.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                    data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceMaster.Tower" name="TWR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select tower </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid}">
                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Floor" data-output-model="SpaceMaster.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                    data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceMaster.Floor" name="FLR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid">Please select floor </span>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                            <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </form>
                <br />
                <div class="clearfix">
                    <div class="row">
                        <div class="col-md-11 col-sm-6 col-xs-12" id="table2" data-ng-show="!GridVisiblity" style="padding-top: 10px">
                            <a data-ng-click="GenerateExcel()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-12 col-xs-12">
                            <div style="height: 333px;">
                                <input class="form-control" id="filtertxt" placeholder="Filter..." type="text" style="width: 35%" />
                                <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="row">
                                <%-- <div class="panel-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li data-ng-click="expression='ST'"><a class="active" href="#bu" data-toggle="tab"><i class="flaticon-people"></i>

                                            Space Type </a></li>
                                        <li data-ng-click="expression='SST'"><a href="#project" data-toggle="tab"><i class="flaticon-home"></i>
                                            Space Sub Type</a></li>
                                    </ul>
                                </div>--%>
                                <ul class="nav nav-tab" role="tablist">

                                    <li class="nav-item"><a class="nav-link active" href="#bu" data-toggle="pill"><i class="flaticon-people"></i>
                                        Space Type</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#project" data-toggle="pill"><i class="flaticon-home"></i>
                                        Space Sub Type</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="bu" data-ng-class="expression=='ST'?'tab-pane fade in active modal-body':'tab-pane fade modal-body'">

                                        <form role="form" id="Form2" name="frmblkreq" data-valid-submit="Submit()" novalidate>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.SNO.$invalid}">
                                                        <label class="control-label">Space Type <span style="color: red;">*</span></label>
                                                        <select name="SNO" data-ng-model="currentblkReq.SNO" class="selectpicker form-control mb-2" id="ddlvert" data-live-search="true" required="">
                                                            <option value="" selected>--Select--</option>
                                                            <option ng-repeat="Vert in SPACE_TYPE" value="{{Vert.SNO}}">{{Vert.SVD_SPACE_TYPE_NAME}}</option>
                                                        </select>
                                                        <span class="error" data-ng-show="frmblkreq.$submitted && frmblkreq.SNO.$invalid">Please select SpaceType </span>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="box-footer text-right" style="padding-left: 16px">
                                                    <button type="submit" id="btnRequest" class="btn btn-primary custom-button-color pull-right text-right" ng-click="currentblkReq.ALLOCSTA = 1">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="project" data-ng-class="expression=='SST'?'tab-pane fade in active modal-body':'tab-pane fade modal-body'">

                                        <form role="form" id="Form3" name="frmblkreq2" data-valid-submit="Submit()" novalidate>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group" data-ng-class="{'has-error': frmblkreq2.$submitted && frmblkreq2.SPC_TYPE_CODE.$invalid}">
                                                        <label class="control-label">Space Type <span style="color: red;">*</span></label>
                                                        <select name="SPC_TYPE_CODE" data-ng-model="currentblkReq.SPC_TYPE_CODE" class="selectpicker form-control mb-2" data-live-search="true" required="" data-ng-change="SpaceSubChange()">
                                                            <option value="" selected>--Select--</option>
                                                            <option ng-repeat="S in SpaceType" value="{{S.SPC_TYPE_CODE}}">{{S.SPC_TYPE_NAME}}</option>
                                                        </select>
                                                        <span class="error" data-ng-show="frmblkreq2.$submitted && frmblkreq2.SPC_TYPE_CODE.$invalid">Please select SpaceType </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group" data-ng-class="{'has-error': frmblkreq2.$submitted && frmblkreq2.SST_CODE.$invalid}">
                                                        <label class="control-label">Space SubType <span style="color: red;">*</span></label>
                                                        <select name="SST_CODE" data-ng-model="currentblkReq.SST_CODE" class="selectpicker form-control mb-2" data-live-search="true" required="">
                                                            <option value="" selected>--Select--</option>
                                                            <option ng-repeat="Costcenter in SST_NAME" value="{{Costcenter.SST_CODE}}">{{Costcenter.SVD_SPC_SUB_TYPE_NAME}}</option>
                                                        </select>
                                                        <span class="error" data-ng-show="frmblkreq2.$submitted && frmblkreq2.SST_CODE.$invalid">Please select SubType </span>
                                                    </div>
                                                </div>
                                                <br />
                                                <br />
                                                <div class="box-footer text-right" style="padding-left: 16px">
                                                    <button type="submit" id="btnBlock" class="btn btn-primary custom-button-color pull-right text-right" ng-click="currentblkReq.ALLOCSTA = 2">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../Utility.min.js"></script>
    <%--<script defer src="Spacemaster.js"></script>--%>
    <script defer src="Spacemaster.js"></script>
</body>

</html>
