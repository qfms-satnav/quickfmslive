﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />

</head>
<body data-ng-controller="LiveOccupancyController" class="amantra">

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Live Occupancy">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Live Occupancy</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">
                            <div id="divDwnTemplt" class="animate-show" data-ng-show="ToggleDiv">
                                <div class="clearfix">
                                    <div class="box-footer text-right">
                                        <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                    </div>
                                </div>
                                <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                                    <legend style="padding-top: 0px; padding-left: 5px; width: 21%; font-size: 14px; font-weight: 500">Download Data Template</legend>

                                    <form id="Form1" name="frmUploadspc" data-valid-submit="DownloadExcel()" novalidate>
                                        <div class="clearfix">

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.CNY_NAME.$invalid}">
                                                    <label class="control-label">Country <span style="color: red;">*</span></label>
                                                    <div isteven-multi-select data-input-model="Country" data-output-model="UploadSpaces.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                        data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UploadSpaces.Country" name="CNY_NAME" style="display: none" required="" />
                                                    <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.CNY_NAME.$invalid">Please select country </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.CTY_NAME.$invalid}">
                                                    <label class="control-label">City <span style="color: red;">**</span></label>
                                                    <div isteven-multi-select data-input-model="City" data-output-model="UploadSpaces.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                        data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UploadSpaces.City" name="CTY_NAME" style="display: none" required="" />
                                                    <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.CTY_NAME.$invalid">Please select city </span>

                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.LCM_NAME.$invalid}">
                                                    <label class="control-label">Location <span style="color: red;">**</span></label>
                                                    <div isteven-multi-select data-input-model="Location" data-output-model="UploadSpaces.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                        data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UploadSpaces.Location" name="LCM_NAME" style="display: none" required="" />
                                                    <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.LCM_NAME.$invalid">Please select location </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.TWR_NAME.$invalid}">
                                                    <label class="control-label">Tower <span style="color: red;">**</span></label>
                                                    <div isteven-multi-select data-input-model="Tower" data-output-model="UploadSpaces.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                        data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UploadSpaces.Tower" name="TWR_NAME" style="display: none" required="" />
                                                    <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.TWR_NAME.$invalid">Please select tower </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmUploadspc.$submitted && frmUploadspc.FLR_NAME.$invalid}">
                                                    <label class="control-label">Floor <span style="color: red;">**</span></label>
                                                    <div isteven-multi-select data-input-model="Floor" data-output-model="UploadSpaces.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                        data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UploadSpaces.Floor" name="FLR_NAME" style="display: none" required="" />
                                                    <span class="error" data-ng-show="frmUploadspc.$submitted && frmUploadspc.FLR_NAME.$invalid">Please select floor </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label></label>
                                                <div class="box-footer text-right">
                                                    <input type="submit" id="btnsubmit" value="Download Template" class="btn btn-primary custom-button-color" />
                                                    <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </fieldset>
                                <br />
                            </div>
                            <div id="divUploadTemplt">
                                <fieldset style="border: 1px solid #D2D2D2; border-radius: 3px 3px 3px 3px; padding: 10px 10px 10px">
                                    <legend style="padding-top: 0px; padding-left: 5px; width: 21%; font-size: 14px; font-weight: 500">Live Occupancy data upload</legend>
                                    <form role="form" id="FileUsrUpl" name="FileUsrUpl" data-valid-submit="UploadFile()" novalidate>

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <a href="" id="lnkdwn" ng-click="ToggleDiv = !ToggleDiv">Click here to download the template </a>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label class="custom-file">
                                                <input type="file" name="UPLFILE" id="FileUpl" required="" class="custom-file-input">
                                                <span class="custom-file-control"></span>
                                            </label>
                                        </div>

                                        <div class="box-footer text-right">
                                            <input type="submit" id="btnUpload" class="btn btn-primary custom-button-color" value="Upload Excel" />
                                        </div>
                                        <div ng-show="DispDiv">
                                            <br />
                                            <div>
                                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px;"></div>
                                            </div>
                                        </div>

                                    </form>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../Utility.js"></script>
    <script defer src="LiveOccupancy.js"></script>
</body>

</html>
