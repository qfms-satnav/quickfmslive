﻿app.service("LiveOccupancyService", function ($http, $q, UtilityService) {

    this.downloadOccpTemplate = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LiveOccupancy/DownloadOccpTemplate', searchObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});

app.controller('LiveOccupancyController', function ($scope, $q, $location, LiveOccupancyService, UtilityService, $filter, $http) {
    $scope.UploadSpaces = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.ToggleDiv = false;
    $scope.ToggleDiv = false;
    $scope.DispDiv = false;
    //$scope.AllocType = [{ CODE: "All", NAME: "All" }, { CODE: "Vacant", NAME: "Vacant" }, { CODE: "Allocated", NAME: "Allocated" }];
    //$scope.UplAllocType = [{ CODE: "Alloc", NAME: "Only Allocate" }, { CODE: "RelAlloc", NAME: "Release Allocate" }];
    //$scope.UplOptions = [{ CODE: "ALLOCDB", NAME: "Allocate based on Employee Data", isChecked: false }];


    $scope.Load = function () {
        UtilityService.getCountires(2).then(function (Countries) {
            if (Countries.data != null) {
                $scope.Country = Countries.data;
            }
        });
        UtilityService.getBussHeirarchy().then(function (response) {
            if (response.data != null) {
                $scope.BsmDet = response.data;
            }
        });
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
            }
        });
        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
            }
        });
        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
            }
        });
        UtilityService.getTowers(2).then(function (response) {
            if (response.data != null) {
                $scope.Tower = response.data;
            }
        });

        UtilityService.getFloors(2).then(function (response) {
            if (response.data != null) {
                $scope.Floor = response.data;
            }
        });
    }

    /***    Page loading        ***/
    $scope.Load();

    //// Country Events
    $scope.CnyChangeAll = function () {
        $scope.UploadSpaces.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.UploadSpaces.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.UploadSpaces.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.UploadSpaces.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.UploadSpaces.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.UploadSpaces.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.UploadSpaces.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.UploadSpaces.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.UploadSpaces.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.UploadSpaces.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UploadSpaces.City.push(cty);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.UploadSpaces.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.UploadSpaces.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.UploadSpaces.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UploadSpaces.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.UploadSpaces.Location[0].push(lcm);
            }
        });

    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.UploadSpaces.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.UploadSpaces.Floor = [];
        $scope.FlrChanged();
    }



    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UploadSpaces.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UploadSpaces.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.UploadSpaces.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.UploadSpaces.Tower.push(twr);
            }
        });

    }

    /***********  Download data Values   ***************/

    $scope.AllocTypeChange = function () {
        switch ($scope.UploadSpaces.AllocType) {
            case "All": showNotification('success', 8, 'bottom-right', "No data Found");
                break;
        }
    }

    $scope.DownloadExcel = function () {
        $scope.dataobj = { flrlst: $scope.UploadSpaces.Floor };
        progress(0, 'Loading...', true);
        $http({
            url: UtilityService.path + '/api/LiveOccupancy/DownloadOccpTemplate',
            method: 'POST',
            responseType: 'arraybuffer',
            data: $scope.dataobj, //this is your json data string
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            }
        }).success(function (data) {
            if (data.byteLength > 27) {
                var blob = new Blob([data], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                });
                save(blob, 'SpaceUploadTemplateFor_' + '_seats.xlsx');
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "No data Found");

            }
        }).error(function () {
            progress(0, '', false);
            //Some error log
        });

        //UploadSpacesService.downloadTemplate($scope.dataobj).then(function (result) {
        //    var uriString = parseReturn(result);
        //    location.href = "uriString"

        //});
    }

    function save(blob, fileName) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, fileName);
            progress(0, '', false);
        } else {
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
            window.URL.revokeObjectURL(link.href);
            $scope.ToggleDiv = !$scope.ToggleDiv;
            progress(0, '', false);
        }
    }


    var columnDefs = [
       { headerName: "City", field: "CITY", cellClass: "grid-align" },
       { headerName: "Location", field: "LOCATION", cellClass: "grid-align" },
       { headerName: "Tower", field: "TOWER", cellClass: "grid-align" },
       { headerName: "Floor", field: "FLOOR", cellClass: "grid-align" },
       { headerName: "Space ID", field: "SPACE_ID", cellClass: "grid-align" },
      { headerName: "Seat Type", field: "SEATTYPE", cellClass: "grid-align" },
       { headerName: "Business Unit", field: "VERTICAL", cellClass: "grid-align" },
       { headerName: "Project", field: "COSTCENTER", cellClass: "grid-align" },
       //{ headerName: "Employee ID", field: "EMP_ID", cellClass: "grid-align" },
       //{ headerName: "From Date", field: "FROM_DATE", cellClass: "grid-align" },
       //{ headerName: "To Date", field: "TO_DATE", cellClass: "grid-align" },
       { headerName: "From Time", field: "FROM_TIME", cellClass: "grid-align" },
       { headerName: "To Time", field: "TO_TIME", cellClass: "grid-align" },
    { headerName: "Port Number", field: "PORT_NUMBER", cellClass: "grid-align" },
    { headerName: "Bay Number", field: "BAY_ID", cellClass: "grid-align" }
       //{ headerName: "Remarks", field: "REMARKS", cellClass: "grid-align" }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
    };

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    /////  Upload File
    $scope.UploadFile = function () {

        var selectedoptions = $filter('filter')($scope.UplOptions, { isChecked: true });
        var selopt = _.map(selectedoptions, 'CODE').join(",");
        if ($('#FileUpl', $('#FileUsrUpl')).val()) {
            progress(0, 'Loading...', true);
            var filetype = $('#FileUpl', $('#FileUsrUpl')).val().substring($('#FileUpl', $('#FileUsrUpl')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                }
                else {
                    console.log($scope.UplOptions);
                    var formData = new FormData();
                    var UplFile = $('#FileUpl')[0];
                    var CurrObj = { UplOptions: selopt };
                    formData.append("UplFile", UplFile.files[0]);
                    formData.append("CurrObj", JSON.stringify(CurrObj));
                    $.ajax({
                        url: UtilityService.path + "/api/LiveOccupancy/UploadTemplate",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            console.log(response);

                            if (response.data != null) {
                                $scope.$apply(function () {

                                    $scope.gridOptions.api.setRowData(response.data);
                                });
                                $scope.DispDiv = true;
                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', response.Message);
                            }
                            else {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', response.Message);
                            }
                        }
                    });
                }

            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
            }
        }
        else
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');

    }


});