﻿app.service('MapMarkerService', ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.getCitywiseLoc = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MapMarker/GetCitywiseLoc')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMarkers = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MapMarker/GetMarkers')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('MapMarkerController', ['$scope', '$q', 'MapMarkerService', 'UtilityService','$filter', function ($scope, $q, MapMarkerService, UtilityService, $filter) {

    var map;
    var markers = [];
    $scope.Markers = [];
    //var map = new L.Map('map', { center: new L.LatLng(23.0885976, 86.1615858), zoom: 4 });

    MapMarkerService.getCitywiseLoc().then(function (response) {
        if (response != undefined && response != null) {
            $scope.CityLst = response;
        }

    });

    MapMarkerService.GetMarkers().then(function (response) {
        if (response != undefined && response != null) {
            angular.forEach(response, function (value, key) {
                addMarker(value);
            });
        }

    });


    $scope.initMap = function () {
        var haightAshbury = { lat: 23.0885976, lng: 86.1615858 };

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: haightAshbury,
            mapTypeId: 'terrain'
        });
        // This event listener will call addMarker() when the map is clicked.
        //map.addListener('click', function (event) {
        //    addMarker(event.latLng);
        //});

        // Adds a marker at the center of the map.
        //addMarker(haightAshbury);
    }

    $scope.initMap();
    var infowindow = new google.maps.InfoWindow();

    // Adds a marker to the map and push to the array.
    function addMarker(data) {
        var marker = new google.maps.Marker({
            position: { lat: data.LAT, lng: data.LONG },
            map: map,
            title: data.LCM_NAME
        });
        marker.LCM_CODE = data.LCM_CODE;
        marker.CTY_NAME = data.CTY_NAME;
        marker.LCM_NAME = data.LCM_NAME;
        marker.CTY_CODE = data.CTY_CODE;

        var content = '<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1 > ' + data.LCM_NAME + ' (' + data.LCM_CODE + ')  <br> <b> Total Number Of Seats:-</b> :' + data.TOTAL_SEATS + ' <br/><b>Total Occupied Seats:-</b> : ' + data.OCCUPIED_SEATS + ' <br/><b>Total Allocated Seats:-</b> : ' + data.ALLOCATED_SEATS + '  <BR/><b>Total Vacant seats:-</b> : ' + data.VACANT_SEATS + ' <br><b>Address:</b> ' + data.LCM_ADDR + '</font>'
        marker.addListener('click', function () {
            if (infowindow)
                infowindow.close();
            infowindow.setContent(content);
            infowindow.open(map, marker);
        });

        $scope.Markers.push(marker);
    }

    $scope.CityClick = function (cty) {
        clearMarkers();
        var markers = $filter('filter')($scope.Markers, function (x) { if (x.CTY_CODE === cty.CTY_CODE) return x; });
        console.log(markers);
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < $scope.Markers.length; i++) {
            $scope.Markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    $scope.showMarkers = function () {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        $scope.Markers = [];
    }

}]);
