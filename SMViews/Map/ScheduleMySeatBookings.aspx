﻿<%@ Page Language="C#" AutoEventWireup="true" %>



<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                setDate: new Date(),
                startDate: new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: '+12m'
            });
        };

    </script>
   
</head>


<body data-ng-controller="ScheduleMySeatBookingsController" class="amantra">

    <div class="widgets">
        <h3 class="panel-title">Search Availability</h3>
    </div>
    <div class="card">
        <div class="card-body">
            <form role="form" id="Form1" name="frmScheduleMySeatBookings" data-valid-submit="SearchSpaces()" novalidate>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.LCM_NAME.$invalid}">
                            <label>Location</label>
                            <div isteven-multi-select data-input-model="Locations" data-output-model="ScheduleMySeatBookings.Locations" data-button-label="icon LCM_NAME"
                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="ScheduleMySeatBookings.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                            <span class="text-danger" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.LCM_NAME.$invalid">Please select Location</span>

                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Tower</label>
                            <div isteven-multi-select data-input-model="Towers" data-output-model="ScheduleMySeatBookings.Towers" data-button-label="icon TWR_NAME "
                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                data-tick-property="ticked" data-max-labels="1">
                            </div>
                            <input type="text" data-ng-model="ScheduleMySeatBookings.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                            <span class="text-danger" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.TWR_NAME.$invalid">Please select Tower</span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Floor</label>
                            <div isteven-multi-select data-input-model="Floors" data-output-model="ScheduleMySeatBookings.Floors" data-button-label="icon FLR_NAME"
                                data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="ScheduleMySeatBookings.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                            <span class="text-danger" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.FLR_NAME.$invalid">Please select Floor</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>From Date</label>
                            <div class="input-group date" id='fromdate'>
                                <input type="text" id="filtertxt" data-ng-model="ScheduleMySeatBookings.FROM_DATE" name="FromDate" class="form-control" placeholder="mm/dd/yyyy" />
                                <%--<div class="input-group-append">
                                        <span class="input-group-text"><i class="fa fa-calendar" onclick="setup('fromdate')"></i></span>
                                    </div>--%>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>To Date</label>
                            <div class="input-group date" id='todate'>
                                <input type="text" class="form-control" data-ng-model="ScheduleMySeatBookings.TO_DATE" id="Text2" name="ToDate"
                                    placeholder="mm/dd/yyyy" />
                                <%--<div class="input-group-append">
                                        <span class="input-group-text"><i class="fa fa-calendar" onclick="setup('todate')"></i></span>
                                    </div>--%>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                </span>
                            </div>
                            <%-- <input type="text" data-ng-model="ScheduleMySeatBookings.todate[0]" name="TO_DATE" style="display: none" required="" />
                                              <span class="error" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.TO_DATE.$invalid" style="color: red">Please select To date</span>--%>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group text-right">
                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                            <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </div>
            </form>

            <form id="Form2" name="ScheduleMySeatBookings" data-valid-submit="SearchSpaces1()" data-ng-show="Form2Enable">
                <div class="card" style="padding:0px;">
                    <div class="card-header">
                        Available Seats and Schedule
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.SPC_ID.$invalid}">
                                    <label class="control-label">Select Space ID's</label>
                                    <div isteven-multi-select data-input-model="SpaceIds" data-output-model="ScheduleMySeatBookings.SpaceId" data-button-label="icon SPC_ID"
                                        data-item-label="icon SPC_ID maker" data-on-item-click="SpaceIdSelected()" data-selection-mode="single"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.AUR_NAME.$invalid}">
                                    <label class="control-label">Search Employee</label>
                                    <div isteven-multi-select data-input-model="EmployeeDetails" data-output-model="ScheduleMySeatBookings.EmployeeDetails" data-button-label="icon AUR_NAME"
                                        data-item-label="icon AUR_NAME maker" data-on-item-click="EmployeeIdSelected()" data-selection-mode="single"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12" data-ng-show="SelectionType">
                                <label class="col-md-4 col-sm-6 col-xs-12 m-0">
                                    Daily
                                                <input type="radio" id="setDaily" value="Daily" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType('Daily')" />
                                </label>
                                <label class="col-md-4  col-sm-6  col-xs-12 m-0">
                                    Select Days
                                                <input type="radio" id="setAlterDays" value="AlterDays" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType('AlterDays')" />
                                </label>
                                <label class="col-md-4 col-sm-6 col-xs-12 m-0">
                                    Weekly
                                                <input type="radio" id="setWeekly" value="Weekly" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType('Weekly')" />
                                </label>
                                <%--<label class="col-md-2 control-label">
                                                    Monthly
                                                <input type="radio" id="setMonthly" value="Monthly" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                                </label>--%>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12" id="AlterDays" data-ng-show="SelectAlterDays">
                                <div class="form-group">
                                    <label class="control-label">Select Days</label>
                                    <div isteven-multi-select data-input-model="AlterDays" data-output-model="ScheduleMySeatBookings.AlterDays" data-button-label="icon Date"
                                        data-item-label="icon Date maker"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="ScheduleMySeatBookings.WeekDates[0]" name="Date" style="display: none" />
                                </div>
                            </div>
                            <%--<div class="col-md-3 col-sm-8 col-xs-12" id="Monthly" data-ng-show="SelectMonth">
                                    <div class="form-group">
                                        <label class="control-label">Select Monthly</label>
                                        <div isteven-multi-select data-input-model="MonthDates" data-output-model="ScheduleMySeatBookings.MonthlyDates" data-button-label="icon Date"
                                            data-item-label="icon Date maker"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="ScheduleMySeatBookings.WeekDates[0]" name="Date" style="display: none" />
                                    </div>
                                </div>--%>
                            <div class="col-md-3 col-sm-6 col-xs-12" id="Weekly" data-ng-show="SelectWeekly">
                                <div class="form-group">
                                    <label class="control-label">Select Weekly</label>
                                    <div isteven-multi-select data-input-model="Weekdays" data-output-model="ScheduleMySeatBookings.WeeklyDays" data-button-label="icon Name"
                                        data-item-label="icon Name maker"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="ScheduleMySeatBookings.WeekDates[0]" name="Date" style="display: none" />
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 box-footer" style="display:flex; align-items:center">
                                <input type="button" data-ng-click="Check()" value="ADD" class="btn btn-primary custom-button-color" data-ng-show="Checing" />
                            </div>
                        </div>
                        <div class="row mt-2 mb-3" data-ng-show="Griddata">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div style="height: 325px; width: inherit" data-ng-show="Checing">
                                    <div data-ag-grid="gridOptions" style="height: 90%; width: 550px;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group text-right">
                                    <button type="button" data-ng-click="scheduleSeats()" id="btnAllocateSeats" class="btn btn-primary" data-ng-show="Griddata">Schedule Seats</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>    <script defer src="//cdnjs.cloudflare.com/ajax/libs/angular-multi-select/4.0.0/isteven-multi-select.js"></script>

    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../Scripts/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>--%>
    <script defer src="../Utility.js"></script>
   <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer> 
        var aur_id = '<%= Session["UID"] %>';
    </script> 
    <script defer src="ScheduleMySeatBookings.js"></script>
    <script src="../DeptAlloc/Js/SpaceRequisition.js"></script>
    <script src="maploader.js"></script>
</body>
</html>


