﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS" data-ng-cloak>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/addons/easy-button.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/main.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/leaflet/Print/easyPrint.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.fullscreen.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>
    <style>
        #qr-code {
            width: 100px !important;
            height: 100px !important;
        }

        .tabledata {
            font-weight: bold;
            font-size: 12px;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 35px;
            height: 20px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 12px;
            width: 12px;
            left: 4.5px;
            bottom: 4.5px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #1C2B36;
            -webkit-transition: .4s;
            transition: .4s;
        }



        input:checked + .slider {
            background-color: #63bbb2;
        }

            input:checked + .slider:before {
                -webkit-transform: translateX(12px);
                -ms-transform: translateX(12px);
                transform: translateX(12px);
            }

        .slider.round:before {
            border-radius: 45%;
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 15px;
        }


        /* Rounded sliders */
        .slider.round {
            border-radius: 15px;
        }

        ul {
            cursor: pointer;
        }

        .card {
            padding: 0px;
        }

        .card-body {
            padding: 0px;
        }

        .widgets {
            padding: 0px 0px;
        }

        #ulThemes .label {
            font-size: 100%;
        }

        #ulThemes1 .label {
            font-size: 100%;
        }

        .list-group-item {
            padding: 0.25rem 1rem;
            border-width: thin !important;
        }

        #detailsdisplay .label {
            font-size: 100%;
        }

        .leaflet-marker-icon {
            width: fit-content !important;
        }

        .unordered__list {
            justify-content: initial;
        }

        .slider.round {
            border-radius: 15px;
        }

        #SeatAllocation.modal.fade .modal-dialog {
            transform: translate(0px, -150px);
        }

        .modal-dialog.modal-lg.Verticalboxsize {
            width: 450px;
        }

        @media (min-width: 576px) {
            .modal-dialog {
                max-width: 1000px;
            }
        }

        .ag-blue .ag-cell {
            display: flex;
            align-items: center;
        }

        .text-labels {
            color: black;
            font-size: 12px;
            font-weight: 700;
            width: 60px !important;
            word-wrap: break-word;
        }

        .text-labels2 {
            font-size: 2px;
            font-weight: 700;
            width: 10px !important;
            word-wrap: break-word;
            position: center;
            /* Use color, background, set margins for offset, etc */
        }

        .checkbox-wrapper {
            display: flex;
            gap: 10px
        }

        .checkbox-wrapper {
            display: flex;
            flex-wrap: wrap;
            gap: 10px;
        }

        .checkbox-wrapper {
            display: flex;
            flex-direction: column;
            gap: 10px;
            margin-bottom: 15px;
        }

        .amenities-table {
            width: 100%;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

            .amenities-table .amenities-row {
                display: flex;
                align-items: center;
                border-bottom: 1px solid #ddd;
                padding: 5px 10px;
            }

            .amenities-table .amenities-cell {
                flex: 1;
            }

            .amenities-table .checkbox-cell {
                flex: 0 0 40px;
            }

            .amenities-table .input-cell {
                padding: 0 10px;
                flex: 2;
            }

        .section-header {
            font-weight: bold;
            margin-bottom: 10px;
            padding: 0 10px;
        }

        .form-group {
            margin-bottom: 15px;
        }
        .ag-pinned-right-header{
             border-left: 1px solid #8997b9;
        }
        .ag-pinned-right-cols-viewport{
            border-left: 1px solid #8997b9;
        }
        .ag-pinned-left-header{
             border-right: 1px solid #8997b9;
        }
        .ag-pinned-left-cols-viewport{
            border-right: 1px solid #8997b9;
        }
    </style>
</head>
<body class="amantra" ng-controller="MaploaderController">
    <div class="animsition">
        <div class="widgets">
            <div ba-panel ba-panel-title="Asset Type Master" ba-panel-class="with-scroll">
                <div class="page__header">
                    <h3 class="page__title" id="LAYOUTNAME"></h3>
                </div>
                <div class="card border-0" style="padding: 10px">
                    <div class="card-body">
                        <div class="row" style="margin-left: 10px;">
                            <ul class="unordered__list font-weight-bold">
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails">
                                        <span class="dots bg-success-light"></span>
                                        <span>SU(%):</span>
                                        <span class="text-black" data-ng-bind="SU"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails2">
                                        <span class="dots bg-primary"></span>
                                        <span>Total:</span>
                                        <span class="text-black" data-ng-bind="SeatSummary.TOTAL"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails2">
                                        <span id="Occupied" class="dots bg-primary"></span>
                                        <span>Occupied:</span>
                                        <span class="text-black" data-ng-bind="SeatSummary.FULLYOCCUPIED" data-ng-if="PartiallyOccupiedSys.SYSP_VAL1=='1'"></span>
                                        <span class="text-black" data-ng-bind="SeatSummary.OCCUPIED" data-ng-if="PartiallyOccupiedSys.SYSP_VAL1 !='1'"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails2">
                                        <span class="dots bg-info"></span>
                                        <span>Allocated:</span>
                                        <span class="text-black" data-ng-bind="SeatSummary.ALLOCATEDVER + SeatSummary.ALLOCATEDCST"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails2">
                                        <span id="Vacant" class="dots bg-info"></span>
                                        <span>Vacant:</span>
                                        <span class="text-black" data-ng-bind="SeatSummary.VACANT"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="SeatSummary.ispartial">
                                        <span id="PartiallyOccupied" class="dots bg-warning"></span>
                                        <span>Partially Occupied:</span>
                                        <span class="text-black" data-ng-bind="SeatSummary.PARTIALLYOCCUPIED"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails">
                                        <span class="dots bg-peal"></span>
                                        <span>Total Area(sq.ft):</span>
                                        <span class="text-black" data-ng-bind="TotalArea"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails">
                                        <span class="dots bg-default"></span>
                                        <span>Work Area(sq.ft):</span>
                                        <span class="text-black" data-ng-bind="WorkArea">sq.ft</span>
                                    </a>
                                </li>
                            </ul>
                            <span class="btn btn-warning" data-ng-show="LastRefresh">Last Refresh Date-
                            <span data-ng-bind="LastRefreshDate" data-ng-show="LastRefresh"></span></span>
                        </div>
                        <%--   </div>--%>
                        <div class="align-right">
                            <label class="switch" data-ng-show="false" style="font-size: medium">
                                <input type="checkbox" id="myCheck" ng-click="myFunction()">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-9">
                                <div id="main" class="card border-info border-width-2 mb-3" style="height: 518px;">
                                    <div class="card-body">
                                        <div id="leafletMap"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="divFilter" style="display: none;">
                                <div class="card border-secondary">
                                    <div class="card-header">
                                        <a data-toggle="collapse" data-parent="#divFilter" href="#divAccFilter">Filter</a>
                                    </div>
                                    <div id="divAccFilter" class="panel-collapse collapse in">
                                        <div id="Div2" class="panel-collapse collapse in">
                                            <div class="card-body p-2">
                                                <form id="Form1" name="frmSearchspc" novalidate>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid}">
                                                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="Country" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Country[0].CNY_NAME" name="CNY_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid">Please select a country </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid}">
                                                                <label class="control-label">City <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="City" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.City[0].CTY_NAME" name="CTY_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid">Please select a city </span>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid}">
                                                                <label class="control-label">Location <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="Location" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Location[0].LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid">Please select a location </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                                                <div isteven-multi-select data-input-model="Tower" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                                    data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Tower[0].TWR_NAME" name="TWR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select a tower </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid}">
                                                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                                                <div isteven-multi-select data-input-model="Floor" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                                    data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Floor[0].FLR_NAME" name="FLR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid">Please select a floor </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="">
                                                        <input type="submit" class="btn btn-primary" value="Apply Filter" data-ng-click="FilterSpaces()" />
                                                        <%--<button type="button" class="btn btn-default" onclick="$('.slide_content').slideToggle();">Close</button>--%>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-3" id="divSearch" style="display: none;">
                                <div class="card border-primary">
                                    <div class="card-header">
                                        <a data-toggle="collapse" data-parent="#divSearch" href="#divSearchdata">Search Employee</a>
                                    </div>
                                    <div id="divSearchdata" class="collapse show">
                                        <div id="Div4" class="panel-collapse">
                                            <div class="card-body" style="padding: 15px;">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group row">
                                                            <label class="control-label col-md-4">Search By<span style="color: red;">*</span></label>
                                                            <div class="col-md-8">
                                                                <select name="SelectedType" data-ng-model="SelectedType" id="ddlvert" required="" data-ng-change="SelectAllocEmp(SelectedType)">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="emp in EmpSearchItems" value="{{emp.CODE}}">{{emp.NAME}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group row">
                                                            <label class="control-label col-md-4">Value<span style="color: red;">*</span></label>
                                                            <div class="col-md-8">
                                                                <select name="mySelect" id="Searchdata" ng-options="det.NAME for det in LoadDet track by det.CODE"
                                                                    ng-model="SelectedID" class="selectpicker" data-live-search="true"
                                                                    ng-change="ShowEmponMap(SelectedID)" required="">
                                                                </select>
                                                                <%--<select name="SelectedID" data-ng-model="SelectedID" class="form-control selectpicker" data-live-search="true"  id="Searchdata" required=""
                                                                    ng-change="ShowEmponMap(SelectedID)">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="det in LoadDet" value="{{det.CODE}}">{{det.NAME}}</option>
                                                                </select>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" ng-show="EmpDet">
                                                    <div class="panel-heading">Employee Details</div>
                                                    <div class="panel-body row">
                                                        <label class="control-label col-md-4 text-right">Name:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.AUR_NAME}} / {{EmpDet.AUR_ID}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">Space:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.SPC_NAME}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">{{BsmDet.Parent}}:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.VER_NAME}}/{{EmpDet.VERTICAL}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">{{BsmDet.Child}}:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.Cost_Center_Name}}/{{EmpDet.Cost_Center_Code}}</label>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                        <label class="control-label col-md-4 text-right">FromDate:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-6 text-left">{{EmpDet.FROM_DATE}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">To Date:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-6 text-left">{{EmpDet.TO_DATE}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">Shift:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{getShiftName(EmpDet.SH_CODE)}}</label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" style="width: 200px; height: 100px;" id="text">
                                <div class="col-md-12 d-none" id="dv-vrdt" style="width: 200px; height: 100px; border: 1px solid #000; border-radius: 25px; background-color: antiquewhite" data-ng-repeat="ver in VerticalData">
                                    <table>
                                        <h5 style="font-weight: bold">{{ver}}</h5>
                                        <thead style="font-weight: bold">
                                            <tr>
                                                <td>Cabin</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Data">
                                                    <span ng-if="ver==vername.VER_NAME&&vername.SPC_LAYER=='CB'">{{vername.CNT}}</span>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>WorkStation</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Data"><span ng-if="ver==vername.VER_NAME&&vername.SPC_LAYER=='WS'">{{vername.CNT}}</span></td>

                                            </tr>
                                            <tr>
                                                <td>Semi Private Desk</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Data"><span ng-if="ver==vername.VER_NAME&&vername.SPC_LAYER=='SPD'">{{vername.CNT}}</span></td>

                                            </tr>
                                            <tr>
                                                <td>Total</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Total"><span ng-if="ver==vername.VER_NAME">{{vername.TOTAL}}</span></td>

                                            </tr>
                                        </thead>
                                    </table>

                                </div>
                                <br />

                            </div>

                            <div class="col-md-3" data-ng-if="Abbvallidations.SYSP_VAL1=='1'">
                                <!-- data-ng-show="GetRoleAndRM[0].AUR_ROLE==0"-->
                                <div class="panel panel-primary panel-map">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#PathFinder">Path Finder</a>
                                        </h4>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="PathFinder" class="panel-collapse collapse ">
                                        <label class="control-label">&nbsp; Emp Id: </label>
                                        <span ng-bind="LoggedInEmpDetails[0].AUR_ID"></span>
                                        <br />
                                        <label class="control-label">&nbsp; Seat No: </label>
                                        <span ng-bind="LoggedInEmpDetails[0].SPC_ID"></span>
                                        <br />
                                        <br />
                                        &nbsp;&nbsp;<input type="button" class="btn btn-secondary" id="btnPathFndr" value="Find Path" ng-click="FindPathOfSeat()" />
                                        <div>&nbsp;</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3" id="accordion" style="display: none"  ng-show="GetRoleAndRM[0].AUR_ROLE!=6 && GetRoleAndRM[0].AUR_ROLE!=3">
                                <div class="card mb-1 border-info text-dark" id="QRPanel" data-ng-show="GetRoleAndRM[0].AUR_ROLE!=6 && QRCodeValid.SYSP_VAL1=='1'">
                                    <div class="card-header bg-info border-0">
                                        <h5 class="card__header-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#QrCode">QR Code</a>
                                        </h5>
                                    </div>
                                    <div class="collapse text-center" id="QrCode">
                                        <canvas id="qr-code"></canvas>
                                    </div>
                                </div>
                                <div class="card mb-1 border-info text-dark" id="DivSeatStatusPanel" ng-show="showStatusPanel==true">
                                    <div class="card-header bg-info border-0">
                                        <h5 class="card__header-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatStatus">Space Status</a>
                                        </h5>
                                    </div>
                                    <div id="divSeatStatus" class="collapse ">
                                        <ul class="list-group mb-0" id="ulSeatStatus">
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail" data-ng-click="ShowTotalOnMap()">Total Seats
                                                <span class="badge badge-pill Total" ng-bind="SeatSummary.TOTAL" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail" data-ng-click="ShowVacantOnMap()">Available / Vacant
                                                <span class="badge badge-pill" id="Vacantstatus" ng-bind="SeatSummary.VACANT" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail" data-ng-click="ShowCostCentre()">{{SeatSummary.ALLOCUNUSED}}
                                                    <span class="badge badge-pill Allocated" ng-bind="SeatSummary.ALLOCATEDVER + SeatSummary.ALLOCATEDCST" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail" data-ng-click="ShowOccupiedOnMap()">{{SeatSummary.OCCUP_EMP}}
                                                    <span class="badge badge-pill" id="occupiedstatus" ng-bind="SeatSummary.OCCUPIED" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail">Overloaded Seats
                                                <span class="badge badge-pill Overload" ng-bind="SeatSummary.OVERLOAD" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail">Inactive/Long Leave Seats
                                                <span class="badge badge-pill Inactive" ng-bind="SeatSummary.LEAVE" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail">Blocked Seats
                                                <span class="badge badge-pill Blocked" ng-bind="SeatSummary.BLOCKED" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail">Selected Seats
                                                <span class="badge badge-pill text-black Selected">{{selectedSpaces.length}}</span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail">Total Location Employee Count
                                                <span class="badge badge-pill Inactive" ng-bind="SeatSummary.EMP_COUNT" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item d-flex align-items-center justify-content-between thumbnail">Total Location Allocation Count
                                                <span class="badge badge-pill Inactive" ng-bind="SeatSummary.ALLOC_COUNT" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card mb-1 border-info text-dark" data-ng-hide="Themes">
                                    <div class="card-header bg-info border-0">
                                        <h5 class="card__header-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Themes</a>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse show">
                                        <ul class="list-group list-group-flush mb-0">
                                            <li class="list-group-item d-flex align-items-center justify-content-between">
                                                <span class="flex-1">Items:</span>
                                                <select class="selectpicker-xs custom-select flex-1" name="ddlItem" ng-model="ddlItem" ng-change="loadThemes()">
                                                    <%--<option value="" ng-selected="">--Select--</option>--%>
                                                    <option ng-repeat="item in Items" value="{{item.CODE}}" title="{{item.NAME}}">{{item.NAME}}</option>
                                                </select>
                                            </li>
                                            <li data-ng-show="ddldisplay" class="list-group-item d-flex align-items-center justify-content-between">
                                                <span class="flex-1">Sub Items:</span>
                                                <select class="selectpicker-xs custom-select flex-1" name="ddlSubItem" ng-model="ddlSubItem" ng-change="loadSubDetails()">
                                                    <option value="" ng-selected="">--Select--</option>
                                                    <option ng-repeat="item in SubItems" value="{{item.CODE}}" ng-selected="{{item.ticked}}">{{item.NAME}}</option>
                                                </select>
                                            </li>
                                            <li data-ng-if="OccupiedByShiftSys.SYSP_VAL1 =='1'" data-ng-show="shshow">
                                                <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails">
                                                    <i class="bi bi-sun text-warning font-weight-bold"></i>
                                                    <span>Day:</span>
                                                    <span class="text-black" data-ng-bind="DAY_SH"></span>
                                                </a>
                                            </li>
                                            <li data-ng-if="OccupiedByShiftSys.SYSP_VAL1 =='1'" data-ng-show="shshow">
                                                <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails">
                                                    <i class="bi bi-moon text-warning font-weight-bold"></i>
                                                    <span>Night:</span>
                                                    <span class="text-black" data-ng-bind="NIGHT_SH"></span>
                                                </a>
                                            </li>
                                            <li data-ng-if="OccupiedByShiftSys.SYSP_VAL1 =='1'" data-ng-show="shshow">
                                                <a href="javascript:void(0)" class="unordered__list-item" data-ng-show="mapPerDetails">
                                                    <i class="bi bi-clock text-warning font-weight-bold"></i>
                                                    <span>24 Hours:</span>
                                                    <span class="text-black" data-ng-bind="HOURS24_SH"></span>
                                                </a>
                                            </li>
                                            <%--</ul>

                                        <div class="col-md-12 item-name-data-main" data-ng-show="detailsdisplay">
                                            <label class="control-label col-md-3 text-danger" style="word-wrap: initial;">Function</label>
                                            <label class="control-label col-md-4" style="word-wrap: initial;"></label>
                                            <label class="control-label col-md-1" style="color: rgb(32, 158, 145); word-wrap: initial">Tot</label>
                                            <label class="control-label col-md-1" style="color: rgb(217, 83, 79); word-wrap: initial">Occ</label>
                                            <label class="control-label col-md-1" style="color: rgb(92, 184, 92); word-wrap: initial">Vac</label>
                                        </div>


                                        <div class="box-body">
                                            <ul class="list-group list-group-flush" id="ulThemes" data-ng-show="nrmaldisplay">
                                                <li class="list-group-item d-flex align-items-center justify-content-between" ng-class="{'list-group-item-info' : ddlSubItem == item.CODE}" ng-repeat="item in SpcCount">{{item.NAME}}
                                                        <span class="badge badge-pill" ng-style="set_color(item.COLOR)" ng-bind="item.CNT" ng-init="0"></span>
                                                </li>
                                            </ul>

                                            <ul class="list-group" id="ulThemes" data-ng-show="detailsdisplay">
                                                <li class="list-group-item d-flex align-items-center justify-content-between item-name-data-main" ng-class="{'list-group-item-info' : ddlSubItem == item.CODE}" ng-repeat="item in SpcCount" ng-style="set_fontcolor(item.COLOR)">
                                                    <div class="item-name-main">
                                                        {{item.NAME}}
                                                    </div>
                                                    <span class="badge badge-pill" style="color: rgb(92, 184, 92)" ng-bind="item.VAC" ng-init="0"></span>
                                                    <span class="badge badge-pill" style="color: rgb(217, 83, 79)" ng-bind="item.ALC" ng-init="0"></span>
                                                    <span class="badge badge-pill" style="color: rgb(32, 158, 145)" ng-bind="item.CNT" ng-init="0"></span>
                                                </li>
                                            </ul>--%>
                                            <li class="col-md-12 p-0" data-ng-show="detailsdisplay">
                                                <span class="control-label col-md-6" style="margin: 0px; padding: 0px; padding-left: 10px; color: red">Function</span>
                                                <%--<span class="control-label col-md-6"></span>--%>
                                                <span class="control-label col-md-2 d-flex justify-content-center" style="margin: 0px; padding: 0px; color: rgb(217, 83, 79)">Occ</span>
                                                <span class="control-label col-md-2 d-flex justify-content-center" style="margin: 0px; padding: 0px; color: rgb(92, 184, 92)">Vac</span>
                                                <span class="control-label col-md-2 d-flex justify-content-center" style="margin: 0px; padding: 0px; color: rgb(32, 158, 145)">Tot</span>
                                            </li>
                                        </ul>
                                        <div class="box-body">
                                            <ul class="list-group mb-0" id="ulThemes1" data-ng-show="nrmaldisplay">
                                                <li class="list-group-item" data-ng-class="{'list-group-item-info' : ddlSubItem == item.CODE}" data-ng-repeat="item in SpcCount">{{item.NAME}}
                                                        <span class="label pull-right countVal" data-ng-style="set_color(item.COLOR)" data-ng-bind="item.CNT" data-ng-init="0"></span>
                                                </li>
                                            </ul>

                                            <ul class="list-group" id="ulThemes" data-ng-show="detailsdisplay">
                                                <li class="list-group-item" style="font-size: 12px;" data-ng-class="{'list-group-item-info' : ddlSubItem == item.CODE}" data-ng-repeat="item in SpcCount" data-ng-style="set_fontcolor(item.COLOR)">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            {{item.NAME}}
                                                        </div>
                                                        <div class="col-md-2 d-flex justify-content-center"><span class="label pull-right countVal" style="color: rgb(217, 83, 79)" data-ng-bind="item.ALC" data-ng-init="0"></span></div>
                                                        <div class="col-md-2 d-flex justify-content-center"><span class="label pull-right countVal" style="color: rgb(92, 184, 92)" data-ng-bind="item.VAC" data-ng-init="0"></span></div>
                                                        <div class="col-md-2 d-flex justify-content-center"><span class="label pull-right countVal" style="color: rgb(32, 158, 145)" data-ng-bind="item.CNT" data-ng-init="0"></span></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                                <div class="card mb-1 border-info text-dark" data-ng-hide="Capacity" data-ng-show="mapPerDetails2">
                                    <div class="card-header bg-info border-0">
                                        <h5 class="card__header-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatcap">Seating Capacity Details</a>
                                        </h5>
                                    </div>

                                    <div id="divSeatcap" class="collapse">
                                        <ul class="list-group list-group-flush" id="ulCapacity">
                                            <li class="list-group-item d-flex align-items-center justify-content-between" ng-repeat="item in seatingCapacity">{{item.SPC_NAME}}
                                                <span class="badge badge-pill" ng-style="set_color(item.COLOR)" data-ng-bind="item.CAPACITY" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card border-info text-dark mb-1" data-ng-hide="WorkArea" data-ng-show="mapPerDetails2">
                                    <div class="card-header bg-info border-0">
                                        <h5 class="card__header-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divWorkarea">Work Area Details in sq. ft.</a>
                                        </h5>
                                    </div>

                                    <div id="divWorkarea" class="panel-collapse collapse">
                                        <ul class="list-group list-group-flush" id="ulArea">
                                            <li class="list-group-item d-flex align-items-center justify-content-between" ng-repeat="item in SpcCountArea">{{item.NAME}}
                                                <span class="badge badge-pill" ng-style="set_color(item.COLOR)" ng-bind="item.SUM" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card border-info text-dark mb-1" data-ng-hide="OtherArea" data-ng-show="mapPerDetails2">
                                    <div class="card-header bg-info border-0">
                                        <h5 class="card__header-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divotherarea">Other Area Details in sq. ft.</a>
                                        </h5>
                                    </div>
                                    <div id="divotherarea" class="panel-collapse collapse">
                                        <ul class="list-group list-group-flush" id="ulC">
                                            <li class="list-group-item d-flex align-items-center justify-content-between" ng-repeat="item in Totalareadet">{{item.AREA}}
                                                <span class="badge badge-pill" ng-bind="item.SFT" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slide_block">
        <div class=" slide_head clearfix">
            <div class="pull-left Icons_right" ng-show="FilterVar == 'spcList'">
                <input id="filtertxt" placeholder="Filter..." type="text" class="form-control input-xs" style="margin-left: 15px;" />
            </div>

            <div class="pull-right Icons_right">
                <%--<div class="input-group">
                            <input type="text" placeholder="Search" />
                            <span class="input-group-addon">
                                <span class="fa fa-search" onclick="setup('fromdate')"></span>
                            </span>
                        </div>--%>
            </div>
        </div>
        <div class="slide_content">
            <div class="col-md-12" ng-show="FilterVar == 'spcList'">
                <div class="form-group">
                    <div style="height: 230px;">
                        <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="SeatAllocation" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" id="adminbookseat">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="H1">Space Allocation
                        <label class="text-danger">{{FlipkartVaccinateName}}</label></h5>
                        <h6 class="modal-title text-right">{{Details}}</h6>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="Form2" name="frmSpaceAlloc">
                    <div class="modal-body" style="padding: 5px;">
                        <div class="row" id="HotDesking">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label>
                                    <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="1" checked />
                                    One Day
                                </label>
                                <label data-ng-show="hotdeskspecifications">
                                    <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="2" />
                                    One Week
                                </label>
                                <label data-ng-show="hotdeskspecifications">
                                    <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="3" />
                                    One Month
                                </label>
                            </div>
                        </div>

                        <%-- <div class="row mb-1">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="input-group" ng-show="selectedSpaces.length != 0">
                                    <span class="input-group-prepend">
                                        <span class="input-group-text bg-transparent text-light"><i class="fa fa-search"></i></span>
                                    </span>
                                    <input type="text" id="txtCountFilter" class="form-control" placeholder="Filter..." />
                                </div>
                            </div>
                        </div>--%>

                        <div class="row mb-3" data-ng-if="gridoptions1==false">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-show="selectedSpaces.length != 0">
                                    <div data-ag-grid="gridSpaceAllocOptions" class="ag-blue" style="height: 250px; width: auto"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3" data-ng-if="gridoptions1==true">


                            <div>
                                <div class="form-group">

                                    <table class="table" data-ng-repeat="data in selectedSpaces ">
                                        <tr>
                                            <td width="14%">
                                                <label>Space ID :</label>
                                            </td>

                                            <td width="25%">
                                                <span><b>{{data.SPC_NAME }} </b></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>From Date :</label>
                                            </td>
                                            <td>
                                                <input type="text" id="fromDate" class="pickDate form-control " ng-model="data.FROM_DATE" onpaste="return 0;" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>To Date :</label>
                                            </td>
                                            <td>
                                                <input type="text" id="ToDate" class="pickDate form-control" ng-model="data.TO_DATE" onpaste="return 0;" readonly />
                                            </td>
                                        </tr>
                                        <tr data-ng-hide="HotdeskingCond">
                                            <td>
                                                <label>From Time :</label>
                                            </td>
                                            <td>
                                                <input type="text" id="fromtime" class="form-control" ng-model="data.FROM_TIME" />
                                            </td>
                                        </tr>
                                        <tr data-ng-hide="HotdeskingCond">
                                            <td>
                                                <label>To Time :</label>
                                            </td>
                                            <td>
                                                <input type="text" id="totime" class="form-control" ng-model="data.TO_TIME" />
                                            </td>
                                        </tr>
                                        <tr data-ng-show="HotdeskingCond">
                                            <td>
                                                <label>Shift Type : </label>
                                            </td>
                                            <td>
                                                <select data-ng-change='shiftChange(selectedSpaces[0])' class='form-control mt-1' ng-model='data.SH_CODE'>
                                                    <option value=''>--Select--</option>
                                                    <option ng-repeat='Shift in ShiftData' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button type="button" class="btn btn-primary" ng-click="ShowHide()">More Information</button>
                                            </td>
                                        </tr>
                                        <tr ng-show="!foremp && IsVisible">
                                            <td>
                                                <label>Employee Name : </label>
                                            </td>
                                            <td>
                                                <span><b>{{data.AUR_NAME}} </b></span>
                                            </td>
                                        </tr>
                                        <tr ng-show="!foremp && IsVisible">
                                            <td>
                                                <label>Space Type : </label>
                                            </td>
                                            <td>
                                                <span><b>{{data.SPC_TYPE_NAME}} </b></span>
                                            </td>
                                        </tr>
                                        <tr ng-show="!foremp && IsVisible">
                                            <td>
                                                <label>Space Sub Type : </label>
                                            </td>
                                            <td>
                                                <span><b>{{data.SST_NAME}} </b></span>
                                            </td>
                                        </tr>
                                        <tr ng-show="!foremp && IsVisible">
                                            <td>
                                                <label>Vertical : </label>
                                            </td>
                                            <td>
                                                <span><b>{{data.VER_NAME}} </b></span>
                                            </td>
                                        </tr>
                                        <tr ng-show="!foremp && IsVisible">
                                            <td>
                                                <label>Coscenter : </label>
                                            </td>
                                            <td>
                                                <span><b>{{data.Cost_Center_Name}} </b></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3" ng-show="EmpAllocSeats.length> 0">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="card" style="overflow-x: scroll">
                                    <div class="card-body">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="d-flex align-items-end justify-content-between">
                                                <h3 class="page__title">Seat(s) allocated to {{SelectedEmp.AUR_NAME}}</h3>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <table id="tblempalloc" width="100%" class="table GridStyle">
                                                <tr>
                                                    <th>Space ID</th>
                                                     <th>emp ID</th>
                                                    <th>Location</th>
                                                    <th>Tower</th>
                                                    <th>Floor</th>
                                                    <th>From Date</th>
                                                    <th>To Date</th>
                                                    <th>Shift</th>
                                                    <th></th>
                                                </tr>
                                                <tr ng-repeat="emp in EmpAllocSeats">
                                                    <td>{{emp.SPC_NAME}}</td>
                                                     <td>{{emp.AUR_ID}}</td>
                                                    <td>{{emp.LOCATION}}</td>
                                                    <td>{{emp.TOWER}}</td>
                                                    <td>{{emp.FLOOR}}</td>
                                                    <td>{{emp.FROM_DATE}}</td>
                                                    <td>{{emp.TO_DATE}}</td>
                                                    <td>{{emp.SH_CODE}}</td>
                                                    <td>
                                                        <input type="button" id="btnRelease" value="Release" ng-click="ReleaseEmp(emp)" ng-show="EmployeeRelease" />
                                                        <input type="button" id="btnRelease1" value="Release from Sub Function" ng-click="ReleaseEmp(emp)" ng-show="UptoVetrialRelease" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="text-right">
                                                <button type="button" class="btn btn-primary" data-ng-hide="ProallocEnabledStatus" ng-click="Proceedalloc(alloc)" <%-- ng-click="EmpAllocSeats = []"--%>>Proceed to allocate additional seat</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3" data-ng-show="empDateArr.length> 0" style="display: block !important;">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="d-flex align-items-end justify-content-between">
                                                <h3 class="page__title">Seat(s) already allocated</h3>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <table id="tblempalloc1" width="100%" class="table table-condensed table-bordered table-hover table-striped">
                                                <tr>
                                                    <th>Space ID</th>
                                                    <th>Employee</th>
                                                    <th>From Date</th>
                                                    <th>To Date</th>
                                                    <th>Shift</th>
                                                </tr>
                                                <tr data-ng-repeat="emp in empDateArr">
                                                    <td>{{emp.SPC_ID}}</td>
                                                    <td>{{emp.VERTICAL}} - ({{emp.AUR_ID}})</td>
                                                    <td>{{emp.FROM_DATE | date:'MM/dd/yyyy'}}</td>
                                                    <td>{{emp.TO_DATE | date:'MM/dd/yyyy'}}</td>
                                                    <td>{{emp.SH_CODE}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="enableamenities.SYSP_VAL1==1">
                                <div class="form-group">
                                    <div class="section-header">Amenities</div>
                                    <div id="checkboxContainer" class="amenities-table">
                                        <!-- Dynamically populated rows will go here -->
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer text-right" data-ng-show="EmpAllocSeats.length <= 0 && GetRoleAndRM[0].AUR_ROLE!=6" style="margin-right: 15px;">
                                <button type="button" class="btn btn-default" data-dismiss="modal" data-ng-click="CloseEmpFn()" data-ng-if="!ReleaseFrom">Close</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" data-ng-click="CloseFn()" data-ng-if="ReleaseFrom">Close</button>
                                <button type="button" data-ng-click="AllocateSeats()" id="btnAllocateSeats" data-ng-hide="AllocateEnabledStatus" data-ng-disabled="ActiveButton" class="btn btn-primary" style="width: 100px">Book Space</button>
                                <%--<input type="button" ng-click="AllocateSeats()" class="btn btn-primary" value="Allocate Seats" ng-disabled="OnEmpAllocSeats" />--%>
                                 <input type="button" id="btnReleasenew" class="btn btn-primary" value="Release" ng-click="Release(1002,BsmDet.Parent)" ng-show="WFRelease" />
                                <div class="btn-group dropup" data-ng-if="ReleaseFrom" style="margin-right: 20px;" ng-show="!WFRelease">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Release From {{relfrom.Name}}  <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a ng-click="Release(1004,'Employee')">Employee</a></li>
                                        <li><a ng-click="Release(1003,BsmDet.Child)">{{BsmDet.Child}}</a></li>
                                        <li><a ng-click="Release(1002,BsmDet.Parent)">{{BsmDet.Parent}}</a></li>
                                        <li><a ng-click="Release(1052,'Blocked')">Blocked</a></li>
                                        <%--<li role="separator" class="divider"></li>
                                            <li><a href="#" ng-click="relfrom = 'Complete Release'; relfrom = 1001">Complete Release</a></li>--%>
                                    </ul>
                                </div>
                            </div>

                            <div class="box-footer text-right" ng-show="(EmpAllocSeats.length <= 0 && GetRoleAndRM[0].AUR_ROLE==6) || ( GetRoleAndRM[0].AUR_ROLE==6 && MultipleBooking.SYSP_VAL1==1)">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input type="button" ng-click="AllocateSeats()" data-ng-hide="AllocateEnabledStatus" class="btn btn-primary" value="Book Seat" data-ng-disabled="ActiveButton" />
                                <%--<div class="btn-group dropup">
                                    <button type="button" class="btn btn-primary" ng-click="Release(1002,BsmDet.Parent)" data-ng-hide="UptoVetrialRelease && GetRoleAndRM[0].AUR_ROLE == 6">
                                        Release
                                    </button>
                                </div>--%>
                            </div>

                        </div>

                    <%--</div>--%>
                    </div>
                    <%--<div class="modal-footer"></div>--%>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="Active" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="H3">Active/Inactive Spaces</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="Form3" name="frmSpaceAlloc">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div style="height: 200px">
                                    <input class="form-control" id="InacFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridSpaceAllocation" style="height: 80%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-right" ng-show="EmpAllocSeats.length <= 0">
                            <input type="button" class="btn btn-danger" value="Inactive Spaces" data-ng-click="InactiveSpaces()" ng-show="selectedSpaces.length > 0" />
                            <input type="button" class="btn btn-primary" value="Activate Space" data-ng-click="ActivateSpaces()" ng-show="selectedSpaces.length==0" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/Leaflet.fullscreen.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../BootStrapCSS/leaflet/addons/easy-button.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/Print/leaflet.easyPrint.js"></script>
    <script defer src="../../Scripts/angular-block-ui.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script defer src="../../Scripts/jQuery.print.js"></script>
    <script defer src="../../BlurScripts/BlurJs/DashboardPrint.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script defer src="//cdnjs.cloudflare.com/ajax/libs/angular-multi-select/4.0.0/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI", "cp.ngConfirm"]);
    </script>

    <script defer src="../Utility.js"></script>
    <%--<script defer src="../../BootStrapCSS/Scripts/AllocateSeatToEmployeeVertical.js"></script>--%>
    <script defer src="maploader.js"></script>
    <script defer src="../DeptAlloc/Js/SpaceRequisition.min.js"></script>
    <script defer>
        var aminities;
        var selectedSpace;
        function slideTogglefn() {
            $('#slide_trigger').click(function () {
                $(this).toggleClass("glyphicon-chevron-down");

                $('.slide_content').slideToggle();
            });
        }
        $(document).ready(function () {
            window.top.$(".sidebar__menu").addClass('active');
            $('#toggleSlide').click(function () {
                $('#slide_trigger').click();
            });
            slideTogglefn();
        });
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
        //$("#country").select2({
        //    placeholder: "Select Country",
        //    allowClear: true
        //});
        $(window).on('load', function () {
            setTimeout(
                function () {
                    $("#dv-vrdt").removeClass("d-none");
                }, 5000);

        });

    </script>
    <script>
        var UserId = '<%= Session["UID"] %>';
        var qr;
        (function () {
            qr = new QRious({
                element: document.getElementById('qr-code'),
                size: 200,
                value: 'https://studytonight.com'
            });
        })();

        function generateQRCode() {
            var qrCode = '';
            let searchParams = new URLSearchParams(window.location.search);
            if (searchParams.has('twr_code'))
                qrCode += searchParams.get('twr_code') ? searchParams.get('twr_code') : '';
            if (searchParams.has('flr_code'))
                qrCode += ', ' + (searchParams.get('flr_code') ? searchParams.get('flr_code') : '');
            qrCode += ', ' + Math.floor((Math.random() * 9999) + 1).toString();
            qr.set({
                foreground: 'black',
                size: 200,
                value: qrCode
            });

        }
        generateQRCode();
    </script>

    <script defer src="../../Scripts/moment.min.js"></script>
</body>
</html>

