﻿'use strict';
app.constant("APT_CONST", {
    "WNGSARR": [{
        FLRNAME: 'BAN-BGO-6F', CHILDARR: [
            { SLCT: 'W1_16,W1_17,W1_17_1,W1_17_2,W1_18,W1_18_1,W1_18_2,W1_19,W1_19_1,W1_19_2,W1_20,W1_20_1,W1_20_2,W1_21,W1_21_1,W1_21_2,W1_22,W1_22_1,W1_22_2', HDVALS: 'W1_15,W1_14,W1_13' },
            { SLCT: 'W2_23,W2_23_1,W2_23_2', HDVALS: 'W2_11,W2_12,W2_13,W2_13_1,W2_13_2,W2_14,W2_14_1,W2_14_2,W2_15,W2_15_1,W2_15_2,W2_16,W2_16_1,W2_16_2,W2_17,W2_17_1,W2_17_2,W2_18,W2_18_1,W2_18_2,W2_19,W2_19_1,W2_19_2,W2_20,W2_20_1,W2_20_2,W2_21,W2_21_1' },
            { SLCT: 'W3_11,W3_12,W3_12_1,W3_12_2,W3_12_3,W3_13,W3_14,W3_14_1,W3_14_2,W3_14_3,W3_15,W3_15_1,W3_15_2,W3_16,W3_16_1,W3_16_2', HDVALS: 'W3_10,W3_10_1,W3_10_2,W3_10_3,W3_10_4,W3_8,W3_9,W3_9_1,W3_9_2,W3_9_3,W3_9_4' }
        ]
    }
    ]
});

app.service('MaploaderService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.bindMap = function (result) {
        var deferred = $q.defer();
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.approvalallocateSeats = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/approvalallocateSeats', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GET_VERTICAL_WISE_ALLOCATIONS = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GET_VERTICAL_WISE_ALLOCATIONS', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.bindMarkers = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetMarkers', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GradeAllocStatus = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.get(UtilityService.path + '/api/MaploaderAPI/GradeAllocStatus')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getSpaceDaysRestriction = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.get(UtilityService.path + '/api/MaploaderAPI/getSpaceDaysRestriction')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SeatRestriction = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/SEAT_RESTRICTION_BOOKING', result)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.EmployeeType = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaploaderAPI/GETEMPLOYEETYPE')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.bindCornerLables = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetCornerLables', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetFlrIdbyLoc = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Space_mapAPI/GetFloorIDBBox?lcm_code=' + GetParameterValues('lcm_code') + '&twr_code=' + GetParameterValues('twr_code') + '&flr_code=' + GetParameterValues('flr_code'))
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySPCID = function (spcid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: $('#ddlfloors').val(), CatValue: spcid };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySPCID', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsByREQID = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsByREQID', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySUBITEM = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySUBITEM', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySHIFT = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySHIFT', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.release = function (reqdet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/ReleaseSelectedseat', reqdet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }

    this.ActivateSpaces = function (reqdet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/ActivateSpaces', reqdet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }

    this.getEmpDetails = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetEmpDetails', selCostCenters)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getAllocEmpDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetAllocEmpDetails', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.allocateSeats = function (selSpaces) {
        var aminities = [];
        $('#checkboxContainer input[type="checkbox"]').each(function () {
            var amenityId = $(this).val();
            var isSelected = $(this).is(':checked');
            var inputField = $(this).closest('.amenities-row').find('input[type="text"]');
            var inputValue = inputField.val();
            aminities.push({
                AMINITY_ID: amenityId,
                SPACE_ID: selSpaces[0].SPC_ID,
                isSelected: isSelected,
                DESCRIPTION: inputValue
            });
        });


        console.log(aminities);

        for (var i = 0; i < selSpaces.length; i++) {
            selSpaces[i].AMINITIES = aminities;
        }
        console.log(selSpaces);

        var requestData = {
            selSpaces: selSpaces,
            aminities: aminities
        };
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/AllocateSeats', requestData)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.allocateSeatsValidation = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/allocateSeatsValidation', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };



    this.spcAvailabilityByShift = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/SpcAvailabilityByShift', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceFromFloorMaps = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceRecords', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getLegendsSummary = function (flrid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: flrid };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsSummary', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getallFilterbyItem = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaploaderAPI/GetallFilterbyItem')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getallFilterbySubItem = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetallFilterbySubItem', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetTotalAreaDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetTotalAreaDetails', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSeatingCapacity = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSeatingCapacity', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getEmpAllocSeat = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetEmpAllocSeat', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.InactiveSeats = function (spcobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/InactiveSpaceSeats', spcobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getVer_And_CC = function (data) {

        //console.log(data);
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/getVer_And_CC', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetEmpSeatDtlsHotDesk = function (dat) {
        //console.log(data);
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaploaderAPI/GetEmpSeatDtlsHotDesk?svm=' + dat)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('MaploaderController', ['$scope', '$q', 'MaploaderService', 'UtilityService', 'SpaceRequisitionService', '$filter', 'blockUI', '$timeout', '$ngConfirm', 'APT_CONST', function ($scope, $q, MaploaderService, UtilityService, SpaceRequisitionService, $filter, blockUI, $timeout, $ngConfirm, APT_CONST) {

    $scope.selectSeatCount = 0;
    $scope.ReleaseFrom = true;
    $scope.empDateArr = [];
    var SPACE_ID = '';
    var polyline2 = {};
    var polyline3 = {};
    var empMarker = {};
    var empMarker1 = {};
    var polyline4 = {};
    $scope.UAMarkers = [];
    $scope.SelLayers = [];
    $scope.Markers = [];
    $scope.marker = {};
    $scope.pageSize = '1000';
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.SpaceAlloc = {};
    $scope.SpaceAlloc.Verticals = [];
    $scope.Verticals = [];
    $scope.CostCenters = [];
    $scope.AurNames = [];
    $scope.Shifts = [];
    $scope.EmpDetails = [];
    $scope.SpaceAlloc.CostCenters = [];
    $scope.selectedSpaces = [];
    $scope.Markerdata = [];
    $scope.MarkersAll = [];
    $scope.Items = [];
    $scope.SubItems = [];
    $scope.SelectedEmp = [];
    $scope.EmpAllocSeatsOtherDay = [];
    $scope.WFRelease = false;
    //$scope.FilterVar = "EmpSearch";
    $scope.ddldisplay = false;
    $scope.shshow = false;
    $scope.detailsdisplay = false;
    $scope.foremp = false;
    $scope.txtCountFilter = false;
    $scope.gridoptions1 = false;
    $scope.nrmaldisplay = true;
    $scope.MarkerLblLayer = [];
    $scope.MarkerMeetingLblLayer = [];
    $scope.SpcCountArea = [];
    $scope.Empcount = [];
    $scope.OnEmpAllocSeats = false;
    $scope.inactive = true;
    $scope.LastRefresh = false;
    $scope.hotdeskspecifications = true;
    $('#btnAllocateSeats').attr('disabled', false);
    $scope.FloorID;
    $scope.floorDetails = [];
    $scope.EmpSearchItems = [{ CODE: 'AUR_ID', NAME: 'User ID' },
    { CODE: 'AUR_KNOWN_AS', NAME: 'Name' },
    { CODE: 'AUR_EMAIL', NAME: 'Email ID' },
    { CODE: 'SPC_ID', NAME: 'Space ID' }];
    $scope.ddlItem = '';
    var cnt = 0;
    $scope.Grade_Allow_status = '';
    $scope.GradeDetails = '';
    $scope.selectedSpacesLength = 0;
    $scope.SeatPopUpDetails = '';
    $scope.FlipKartSmallChanges = '';
    $scope.LibertyChanges = '';
    $scope.mapPerDetails = false;
    $scope.mapPerDetails2 = true;
    $scope.InactivespaceidList = [];
    $scope.ActiveButton = false;
    var LOADLAYERS;
    $scope.DAY_SH = '';
    $scope.NIGHT_SH = '';
    $scope.HOURS24_SH = '';
    $scope.ActiveButton = false;
    var map = L.map('leafletMap');
    $scope.Markerdata = new L.FeatureGroup();

    $scope.MarkerLblLayer = new L.FeatureGroup();
    $scope.MarkerLblLayer_copy = new L.FeatureGroup();

    $scope.MarkerMeetingLblLayer = new L.FeatureGroup();
    $scope.MarkerMeetingLblLayer_copy = new L.FeatureGroup();
    //new layer addition
    $scope.MarkerLblLayerEmpDetails = new L.FeatureGroup();
    $scope.MarkerLblLayerEmpDetails_copy = new L.FeatureGroup();

    $scope.MarkerLblLayerSpace = new L.FeatureGroup();
    $scope.MarkerLblLayerSpace_copy = new L.FeatureGroup();
    $scope.HotdeskingCond = true;

    //$scope.MarkerLblEmpNames = new L.FeatureGroup();
    //$scope.MarkerLblEmpNames_copy = new L.FeatureGroup();

    map.addLayer($scope.Markerdata);
    var overlayMaps = {};
    //var overlayMaps = {
    //    "Markers": $scope.Markerdata,
    //    "Spaceid's & Emp Details": $scope.MarkerLblLayer,
    //    "Emp Details": $scope.MarkerLblLayerEmpDetails,
    //    "Spaceid's": $scope.MarkerLblLayerSpace
    //};
    //L.control.layers({}, overlayMaps).addTo(map);

    var prefval;
    var OccupiedStyle;
    var PartiallyOccupiedStyle;
    var PartiallyOccupiedSys;
    var VacantStyle;
    $scope.GradeAlloc = [];
	$scope.emplotype="";

    UtilityService.getSysPreferences().then(function (response) {

        if (response.data != null) {
            $scope.SysPref = response.data;
       
        prefval = _.find($scope.SysPref, { SYSP_CODE: "display emp details on map" });
        var sysval = _.find($scope.SysPref, { SYSP_CODE: "Seat Color in red for Vacant" });
        var hotdesk = _.find($scope.SysPref, { SYSP_CODE: "Hot Desk Specifications" });

        $scope.GradeAlloc = _.find($scope.SysPref, { SYSP_CODE: "Grade Wise Allocation" });
        $scope.SeatPopUpDetails = _.find($scope.SysPref, { SYSP_CODE: "Seat details on Map" });
        $scope.FlipKartSmallChanges = _.find($scope.SysPref, { SYSP_CODE: "Flipkart Small Changes" });
        $scope.LibertyChanges = _.find($scope.SysPref, { SYSP_CODE: "Liberty Changes" });
        $scope.VerticalWiseAlloc = _.find($scope.SysPref, { SYSP_CODE: "Booking Based on Vertical" });
        $scope.HideBlocking = _.find($scope.SysPref, { SYSP_CODE: "Hide Blocking" });
        $scope.HideDeatilsOnMap = _.find($scope.SysPref, { SYSP_CODE: "HideDeatilsOnMap" });
        $scope.MultipleBooking = _.find($scope.SysPref, { SYSP_CODE: "Multiple Booking" });
        $scope.PartiallyOccupiedSys = (_.find($scope.SysPref, { SYSP_CODE: "PartiallyOccupied" }) || {});
        $scope.OccupiedByShiftSys = (_.find($scope.SysPref, { SYSP_CODE: "OccupiedByShift" }) || {});
        $scope.Abbvallidations = (_.find($scope.SysPref, { SYSP_CODE: "Find Path" }) || {});
        $scope.AtikinsMulBooking = (_.find($scope.SysPref, { SYSP_CODE: "Atkins Multiple Seat booking" }) || {});
        $scope.Manager_Approval = (_.find($scope.SysPref, { SYSP_CODE: "Manager Approval" }) || {});
        $scope.QRCodeValid = _.find($scope.SysPref, { SYSP_CODE: "QRCodeValid" });
        $scope.EmidsAllocate = _.find($scope.SysPref, { SYSP_CODE: "remove proceed to allocate" } || {});
            $scope.ApexonChanges = _.find($scope.SysPref, { SYSP_CODE: "Apexon Changes" } || {});
            $scope.enableamenities = _.find($scope.SysPref, { SYSP_CODE: "SMS_AMENITIES" });
            $scope.WhatfixChanges = _.find($scope.SysPref, { SYSP_CODE: "Whatfix Changes" });
		 
        if ($scope.VerticalWiseAlloc.SYSP_VAL1 == 1) {

            $scope.UptoVetrialRelease = true;
            $scope.EmployeeRelease = false;
        }
        else {
            $scope.UptoVetrialRelease = false;
            $scope.EmployeeRelease = true;
        }

        if ($scope.SeatPopUpDetails.SYSP_VAL1 == 1) {
            var overlayMaps = {
                "Markers": $scope.Markerdata,
                "Spaceid's": $scope.MarkerLblLayerSpace
            };
        }
        else {
            var overlayMaps = {
                "Markers": $scope.Markerdata,
                "Spaceid's & Emp Details": $scope.MarkerLblLayer,
                "Emp Details": $scope.MarkerLblLayerEmpDetails,
                "Spaceid's": $scope.MarkerLblLayerSpace
            };
        }
        L.control.layers({}, overlayMaps).addTo(map);


        if (hotdesk.SYSP_VAL1 == 1) {
            $scope.hotdeskspecifications = false;
        }
        else {
            $scope.hotdeskspecifications = true;
        }
        if (sysval.SYSP_VAL1 == 1) {
            document.getElementById("Occupied").className = "dots bg-success";
            document.getElementById("Vacant").className = "dots bg-danger";
            document.getElementById("PartiallyOccupied").className = "dots bg-warning";
            document.getElementById("occupiedstatus").className = "badge badge-success badge-pill Vacant";
            document.getElementById("Vacantstatus").className = "badge badge-danger badge-pill Occupied";
            OccupiedStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
            PartiallyOccupiedStyle = { fillColor: '#f0ad4e', opacity: 0.8, fillOpacity: 0.8 };
            VacantStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
            $scope.LastRefresh = true;
        }
        else {
            document.getElementById("Occupied").className = "dots bg-danger";
            document.getElementById("Vacant").className = "dots bg-success";
            document.getElementById("PartiallyOccupied").className = "dots bg-warning";
            document.getElementById("occupiedstatus").className = "badge badge-danger badge-pill Occupied";
            document.getElementById("Vacantstatus").className = "badge badge-success badge-pill Vacant";
            OccupiedStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
            PartiallyOccupiedStyle = { fillColor: '#f0ad4e', opacity: 0.8, fillOpacity: 0.8 };
            VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
            $scope.LastRefresh = false;
        }
		$scope.GetRoleReportingManger();
		}
		
    });
	
	
	$scope.GetRoleReportingManger = function () {
        UtilityService.GetRoleAndReportingManger().then(function (response) {
            if (response.data != null) {
                $scope.GetRoleAndRM = response.data;
                if ($scope.HideDeatilsOnMap && $scope.HideDeatilsOnMap.SYSP_VAL1 == 1 && $scope.GetRoleAndRM[0].AUR_ROLE != 1 && $scope.GetRoleAndRM[0].AUR_ROLE != 14) {
                    $("#accordion").fadeOut();
                    $scope.mapPerDetails = false;
                    $scope.ShiftSelect = "True";
                    $scope.mapPerDetails2 = false;
                    $("#accordion").fadeIn();
                }
                else {
                    $("#accordion").fadeIn();
                    $scope.mapPerDetails = true;
                    $scope.ShiftSelect = "False";
                }

                if ($scope.LibertyChanges.SYSP_VAL1 == '1' && ($scope.GetRoleAndRM[0].AUR_ROLE == '6' || $scope.GetRoleAndRM[0].AUR_ROLE == '1')) {
                    MaploaderService.EmployeeType().then(function (data) {
                        if (data.Message == 'Ok') {
                            $scope.emplotype = data.data[0].TYPEE;
                        } else {
                            //
                        }
                    }, function (error) {
                        console.log(error);
                    });


                }

            }
        });
    }
	
    // setTimeout(function () {

        // UtilityService.GetRoleAndReportingManger().then(function (response) {
            // if (response.data != null) {
                // $scope.GetRoleAndRM = response.data;
                // if ($scope.HideDeatilsOnMap && $scope.HideDeatilsOnMap.SYSP_VAL1 == 1 && $scope.GetRoleAndRM[0].AUR_ROLE != 1 && $scope.GetRoleAndRM[0].AUR_ROLE != 14) {
                    // $("#accordion").fadeOut();
                    // $scope.mapPerDetails = false;
                    // $scope.ShiftSelect = "True";
                    // $scope.mapPerDetails2 = false;
                    // $("#accordion").fadeIn();
                // }
                // else {
                    // $("#accordion").fadeIn();
                    // $scope.mapPerDetails = true;
                    // $scope.ShiftSelect = "False";
                // }

                // if ($scope.LibertyChanges.SYSP_VAL1 == '1' && ($scope.GetRoleAndRM[0].AUR_ROLE == '6' || $scope.GetRoleAndRM[0].AUR_ROLE == '1')) {
                    // MaploaderService.EmployeeType().then(function (data) {

                        // if (data.Message == 'Ok') {
                            // $scope.emplotype = data.data[0].TYPEE;
                        // }
                        // //else {

                        // //}
                    // }, function (error) {
                        // console.log(error);
                    // });


                // }

            // }
        // });
    // }, 100);





    MaploaderService.GetFlrIdbyLoc().then(function (response) {

        progress(0, 'Loading...', true);
        $scope.LoadMap(GetParameterValues('flr_code'));

    }, function (error) {
    });

    UtilityService.GetEmployee().then(function (response) {
        if (response.data != null) {
            $scope.Employee = response.data;
        }
    });
    //document.getElementById('SU').className = 'btn btn-info';


    var fullscreen = map.addControl(new L.Control.Fullscreen());


    var AllocateStyle = { fillColor: '#3498DB', opacity: 0.8, fillOpacity: 0.8 };
    var ReservedStyle = { fillColor: '#17202A', opacity: 0.8, fillOpacity: 0.8 };

    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };
    var LeaveStyle = { fillColor: '#800080', opacity: 0.8, fillOpacity: 0.8 };
    var BlockStyle = { fillColor: '#000000', opacity: 0.8, fillOpacity: 0.8 };
    var CstVer = { fillColor: '#c47750', opacity: 0.8, fillOpacity: 0.8 };
    var InactiveStyle = { fillColor: '#a4adbc', opacity: 0.8, fillOpacity: 0.8 };


    //Initialize map

    //This is currently the only way to get access to the drawnItems feature group which
    //is required to implement the edit functionality

    /// Refresh - resets the markers button
    var refresh = L.easyButton('fa fa-refresh', function () {
        $scope.selectedSpaces = [];
        $scope.SelectedType = "";
        $scope.SelectedID = "";
        $scope.LoadDet = [];
        $scope.ddldisplay = false;
        $scope.shshow = false;
        $scope.FilterVar = "";

        $scope.$apply(function () {
            if ($('#divSearch').css('display') != 'none' && $('#accordion').css('display') == 'none') {
                $('#divSearch').hide();
                $('#accordion').show();
            }
        });

        reloadMarkers();
        map.fitBounds($scope.bounds);
        //$scope.$apply(function () {
        $scope.ddlItem = $scope.Items[0].CODE;
        //});
        $scope.loadThemes();
        updateSummaryCount();
    }, "Refresh");
    refresh.addTo(map);

    //// Filter button
    var selectmap = L.easyButton('fa fa-filter', function () {
        $scope.$apply(function () {
            if ($('#divSearch').css('display') != 'none' && $('#accordion').css('display') == 'none') {
                $('#divSearch').hide();
            }
            else
                $('#accordion').toggle("slow");

            setTimeout(function () {
                $('#divFilter').toggle("slow");
            }, 100);
        });
    }, "Select Map");
    selectmap.addTo(map);

    $scope.slideBlockCloseFn = function () {
        $scope.FilterVar = "spcList";
        $('.slide_content').slideToggle();
    }

    ///// View Grid button
 //var filter = L.easyButton('fa fa-list-ul', function () {
    //    $scope.$apply(function () {
    //        $scope.FilterVar = "spcList";

    //        $('.slide_content').slideToggle();
    //    });
    //}, "Search Filter");
    //filter.addTo(map);

    /// Allocate button
    var allocate = L.easyButton('fa fa-user fa-2x', function () {


        $scope.validate = false;
        progress(0, 'Loading...', true);
        //$scope.enddateselection = '';
        if ($scope.selectedSpaces.length != 0) {

            var role = _.find($scope.GetRoleAndRM);
            if (role.AUR_ROLE == 6) {
                //hide row
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('AUR_SEARCH', false);
            }
            if ($scope.FlipKartSmallChanges.SYSP_VAL1 == 1 || $scope.LibertyChanges.SYSP_VAL1 == 1) {
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SPC_TYPE_NAME', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SST_NAME', false);
                //$scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', false);
            }
            if ($scope.HideBlocking.SYSP_VAL1 == 1) {
                if ($scope.HideBlocking.SYSP_VAL2 == 1) {
                    $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('Allocate_Other', false);
                }
                if (role.AUR_ROLE == 6 || role.AUR_ROLE == 0 || role.AUR_ROLE == 3) {
                    $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('blocked', false);
                }
            }
            if ($scope.WhatfixChanges && $scope.WhatfixChanges.SYSP_VAL1 == role.AUR_ROLE) {
                $scope.WFRelease = true;
            }



            if ($scope.selectedSpaces[0].SPACE_TYPE == 4 || $scope.selectedSpaces[0].SPACE_TYPE == 5) {

                $('#HotDesking').show();

                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('FROM_TIME', true);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('TO_TIME', true);
                //$scope.gridSpaceAllocOptions.columnApi.setColumnVisible('blocked', false);

                if ($scope.LibertyChanges.SYSP_VAL1 == 1) {

                    $scope.hotdeskspecifications = false;
                    $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('blocked', false);
                }
                else {

                    $scope.hotdeskspecifications = true;
                }
            }


            else {
                $('#HotDesking').hide();
                $scope.hotdesking = 0;

                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', true);
                //$scope.gridSpaceAllocOptions.columnApi.setColumnVisible('blocked', true);             

                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('FROM_TIME', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('TO_TIME', false);
            }
            progress(0, '', false);
            $('#SeatAllocation').modal('show');
        }
        else {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Select at least one seat to allocate / release");
        }
    }, "Book / Release seat");
    allocate.addTo(map);

    var search = L.easyButton('fa fa-search', function () {
        $scope.$apply(function () {
            if ($('#divFilter').css('display') != 'none' && $('#accordion').css('display') == 'none') {
                $('#divFilter').hide();
            }
            else
                $('#accordion').toggle("slow");
            setTimeout(function () {
                $('#divSearch').toggle("slow");
            }, 100);
        });
    }, "Search Employee");
    search.addTo(map);

    var inactive = L.easyButton('fa fa-check-square-o', function () {
        $scope.validate = false;
        progress(0, 'Loading...', true);
        var role = _.find($scope.GetRoleAndRM);

        if (role.AUR_ROLE == 6 || role.AUR_ROLE == 14) {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Employee cannot Inactive Spaces');
            return;
        }
        else {
            $scope.GetInactiveSpaces();
        }
    }, "Inactive Spaces");
    inactive.addTo(map);

    //L.easyPrint({
    //    title: 'Print Map',
    //    elementsToHide: '.gitButton, .easy-button-button, .leaflet-control-zoom, .leaflet-control-layers-overlays'

    //}).addTo(map);
    var printcount = 0;
    var print1 = L.easyButton('fa fa-print', function () {
        printcount = printcount + 1;

        progress(0, '', true);
        $timeout(function () {
            $scope.Themes = true;
            $scope.Capacity = true;
            $scope.WorkArea = true;
            $scope.OtherArea = true;
            $("#divSeatStatus").collapse("show");
        }, 500);

        refresh.disable();
        selectmap.disable();
        //filter.disable();
        allocate.disable();
        search.disable();
        inactive.disable();
        print1.disable();
        print2.disable();


        if (map.hasLayer($scope.MarkerLblLayer)) {
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);

            map.addLayer($scope.MarkerLblLayer_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerSpace)) {
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
            map.removeLayer($scope.MarkerLblLayer_copy);

            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {

            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer_copy);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);

            map.addLayer($scope.MarkerLblLayerEmpDetails_copy);
        }
        else {
            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }

        //}, 500);
        $timeout(function () {
            if (cnt == 0 && LOADLAYERS == 1) {
                $scope.LoadMapLayers(GetParameterValues('flr_code'));
                setTimeout(function () {
                    $("#divSeatStatus").collapse("hide");
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    //filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);
                cnt = cnt + 1;
            }
            else {
                setTimeout(function () {
                    $scope.MarkerMeetingLblLayer.clearLayers();
                    print('leafletMap')
                }, 1000);
                setTimeout(function () {
                    $("#divSeatStatus").collapse("hide");
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    //filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);

            }


        }, 1000);

    }, "Print Map");
    print1.addTo(map);

    //L.easyButton('fa fa-print', function () {
    //    printWithoutHeaderfooter('leafletMap');

    //}, "Print Only Layout").addTo(map);

    var print2 = L.easyButton('fa fa-print', function () {
        printcount = printcount + 1;
        progress(0, '', true);

        $timeout(function () {
            $scope.Themes = true;
            $scope.Capacity = true;
            $scope.WorkArea = true;
            $scope.OtherArea = true;
            $scope.showStatusPanel = false;
        }, 500);
        refresh.disable();
        selectmap.disable();
        //filter.disable();
        allocate.disable();
        search.disable();
        inactive.disable();
        print1.disable();
        print2.disable();
        if (map.hasLayer($scope.MarkerLblLayer)) {
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);

            map.addLayer($scope.MarkerLblLayer_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerSpace)) {
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
            map.removeLayer($scope.MarkerLblLayer_copy);

            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {

            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer_copy);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);

            map.addLayer($scope.MarkerLblLayerEmpDetails_copy);
        }
        else {
            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }
        $timeout(function () {
            if (cnt == 0 && LOADLAYERS == 1) {
                $scope.LoadMapLayers(GetParameterValues('flr_code'));
                setTimeout(function () {
                    $scope.showStatusPanel = true;
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    //filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);
                cnt = cnt + 1;
            }
            else {
                setTimeout(function () {
                    $scope.MarkerMeetingLblLayer.clearLayers();
                    print('leafletMap')

                }, 1000);
                setTimeout(function () {
                    $scope.showStatusPanel = true;
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    //filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);
            }

        }, 1000);

    }, "Print Only Layout");
    print2.addTo(map);

    $scope.binddata = function (data) {
        var seletedEmp = data.AUR_SEARCH.split("/");
        var selectedData
        selectedData = _.find($scope.AurNames, { AUR_ID: seletedEmp[0].trim() });
        if (selectedData != undefined) {
            data.AUR_ID = selectedData.AUR_ID;
            data.AUR_NAME = selectedData.AUR_NAME;
            data.VERTICAL = selectedData.VER_CODE;
            data.VER_NAME = selectedData.VER_NAME;
            data.Cost_Center_Code = selectedData.Cost_Center_Code;
            data.Cost_Center_Name = selectedData.Cost_Center_Name;
            $scope.gridSpaceAllocOptions.api.refreshView();
            $scope.EmpChange(data, data.AUR_SEARCH);
        }
    }


    $scope.columDefsAlloc = [
        { headerName: "Select", field: "ticked", width: 50, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.disabled' ng-model='data.ticked' ng-change='chkChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPC_NAME", width: 150, cellClass: "grid-align", pinned: 'left' },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", width: 110, cellClass: "grid-align" },
        /*{ headerName: "Space Sub Type", field: "SST_NAME", width: 110, cellClass: "grid-align" },*/
        { headerName: "Space Subtype", field: "SST_NAME", width: 110, cellClass: "grid-align" },
        //{
        //    headerName: "Search", cellClass: "grid-align", width: 100, filter: 'set', field: "AUR_SEARCH",
        //    template: "<input list='employees' data-ng-change='binddata(data)' ng-model='data.AUR_SEARCH' /><datalist  id='employees' ><option ng-repeat='Employee in AurNames' value='{{Employee.AUR_ID}}' text='{{Employee.AUR_NAME}}'>{{Employee.AUR_NAME}} </option> </datalist>"
        //},
        {
            headerName: "Search", cellClass: "grid-align", width: 100, filter: 'set', field: "AUR_SEARCH",
            template: "<input list='employees' class='form-control mt-1' ng-disabled='data.disabledOpt' data-ng-change='binddata(data)' ng-model='data.AUR_SEARCH' /><datalist  id='employees' ><option ng-repeat='Employee in AurNames' value='{{Employee.AUR_NAME}}' text='{{Employee.AUR_ID}}'>{{Employee.AUR_ID}} </option> </datalist>"
        },
        { headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME", cellRenderer: customEditor },
        { headerName: "From Date  <br />(mm/dd/yyyy)", field: "FROM_DATE", width: 120, cellClass: 'grid-align', cellRenderer: createFromDatePicker },
        {
            headerName: "To Date  <br />(mm/dd/yyyy)", field: "TO_DATE", width: 120, cellClass: 'grid-align', cellRenderer: createToDatePicker
            //cellRenderer: (invNum) => "<input type='text' id='TO_DATE' data-ng-click ='GetToDate()' value='${invNum.value}'>"
        },
        { headerName: "From Time", field: "FROM_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createFromTimePicker },
        { headerName: "To Time", field: "TO_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createToTimePicker },
        { headerName: "Ver", width: 110, cellClass: "grid-align", field: "VER_NAME", cellRenderer: customEditor },
        { headerName: "Cost", width: 110, cellClass: "grid-align", field: "Cost_Center_Name", cellRenderer: customEditor },
        
       
        {
            headerName: "Shift Type", field: "SH_CODE", width: 150, cellClass: "grid-align", filter: 'set',
            template: "<select data-ng-change='shiftChange(data)' ng-disabled='data.disabledOpt'  class='form-control mt-1' ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in ShiftData' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>"
        },
        { headerName: "Blocking", field: "blocked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.block' ng-model='data.blocked' ng-change='blkChanged(data)'>" }, // headerCellRenderer: headerCellRendererFunc, 
        {
            headerName: "", field: "Allocate_Other", width: 100, cellClass: "grid-align", filter: 'set', pinned: 'right',
            template: "<a href='' data-ng-click ='AllocAnother(data)'>Allocate Other</a>"
        }
    ];
    text.style.display = "none";
    accordion.style.display = "block";
    $scope.myFunction = function () {
        var checkBox = document.getElementById("myCheck");
        var text = document.getElementById("text");
        if (checkBox.checked == true) {
            text.style.display = "block";
            accordion.style.display = "none";
        } else {

            text.style.display = "none";
            accordion.style.display = "block";
        }
    }

    MaploaderService.GET_VERTICAL_WISE_ALLOCATIONS(GetParameterValues('flr_code')).then(function (response) {

        if (response != null) {
            $scope.VerticalData = response.data;
            $scope.Vertical_Wise_Data = response.data1;
            $scope.Vertical_Wise_Total = response.data2;
        }
    });




    $scope.chkChanged = function (data) {
        if (data.SSA_SRNREQ_ID == "" || data.SSA_SRNREQ_ID == null) {
            if ($scope.GradeAlloc.SYSP_VAL2 == 1 && data.ticked == false && $scope.Eligibility != 'No') {
                $scope.selectedSpacesLength = $scope.selectedSpacesLength - 1;
                if ($scope.Allow_Remaining == undefined) {
                    $scope.AllocateEnabledStatus = false;
                }
                else if ($scope.Allow_Remaining >= $scope.selectedSpacesLength) {
                    $scope.AllocateEnabledStatus = false;
                }
                else {
                    $scope.AllocateEnabledStatus = true;
                }
            } else {
                $scope.selectedSpacesLength = $scope.selectedSpacesLength + 1;
                if ($scope.Allow_Remaining == undefined) {
                    $scope.AllocateEnabledStatus = false;
                }
                else if ($scope.Allow_Remaining >= $scope.selectedSpacesLength) {
                    $scope.AllocateEnabledStatus = false;
                }
                else {
                    $scope.AllocateEnabledStatus = true;
                }
            }
        }
    }
    $scope.GetToDate = function () {


        var prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });

        if (prefvalue == undefined) {


            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        } else {
            $('#TO_DATE').datepicker('setStartDate', fromdate);
            $('#TO_DATE').datepicker('setEndDate', todate);
            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: fromdate,
                endDate: todate
            });
        }
    };

    $scope.columDefSpaceAlloc = [
        { headerName: "Select", field: "ticked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='ChkSpaceChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPACE_ID", width: 350, cellClass: "grid-align", pinned: 'left' },
        { headerName: "Space Type", field: "SPC_TYPE", width: 250, cellClass: "grid-align" },
        /*{ headerName: "Space Sub Type", field: "SPC_SUB_TYPE", width: 239, cellClass: "grid-align" }];*/
        { headerName: "Space Subtype", field: "SPC_SUB_TYPE", width: 239, cellClass: "grid-align" }];
    $scope.selectedActSpaces = [];

    $scope.ChkSpaceChanged = function (data) {
        $scope.selectedActSpaces.push(data);
    }

    $scope.GetInactiveSpaces = function () {

        if ($scope.selectedSpaces.length > 0) {
            $scope.selectedActSpaces = [];
            $('#Active').modal('show');
            angular.forEach($scope.selectedSpaces, function (value, key) {
                var spcdet = {};
                if (value.ticked == true) {
                    spcdet.SPACE_ID = value.SPC_ID;
                    spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                    spcdet.SPC_SUB_TYPE = value.SST_NAME;
                    spcdet.ticked = value.ticked;
                    $scope.selectedActSpaces.push(spcdet);
                }
            });
            setTimeout(function () {
                $scope.gridSpaceAllocation.api.setRowData([]);
                $scope.gridSpaceAllocation.api.setRowData($scope.selectedActSpaces);
                progress(0, '', false);
            }, 200);
        }
        else {
            var params = {
                flr_code: $scope.SearchSpace.Floor[0].FLR_CODE
            };
            MaploaderService.GetSpaceFromFloorMaps(params).then(function (data) {
                if (data.length != 0) {
                    $scope.gridata = data;
                    $scope.gridSpaceAllocation.api.setRowData([]);
                    $scope.gridSpaceAllocation.api.setRowData($scope.gridata);
                    progress(0, '', false);
                    $('#Active').modal('show');
                }
                else {
                    progress(0, '', false);
                    $('#Active').modal('hide');
                    showNotification('error', 8, 'bottom-right', 'No Inactive Spaces were found for the Selected Floor.');
                }
            }, function (error) {
                console.log(error);
            });
        }

    };

    var ActivateSpacescnt = 0;
    $scope.ActivateSpaces = function () {
        ActivateSpacescnt = 1;
        var flrcode = $scope.SearchSpace.Floor[0].FLR_CODE;
        var dataobj = { FLR_CODE: flrcode, SPC_LST: $scope.selectedActSpaces };
        MaploaderService.ActivateSpaces(dataobj).then(function (response) {
            if (response != null) {
                angular.forEach($scope.selectedActSpaces, function (value, key) {

                    _.remove($scope.InactivespaceidList, function (x) {
                        return x.IN_SPC_ID === value.SPACE_ID && value.ticked == true
                    });
                });

                setTimeout(function () {
                    $('#Active').modal('hide');
                    setTimeout(function () {
                        if (LOADLAYERS == 1) {
                            $scope.LoadMarkerLayers($scope.floorDetails);
                        }
                        else
                            $scope.LoadALLMarkers($scope.floorDetails);
                        //$scope.LoadMarkerLayers($scope.SearchSpace.Floor[0].FLR_CODE);
                        $scope.loadThemes();
                    }, 500);
                    showNotification('success', 8, 'bottom-right', 'Selected Spaces Has Been Active');
                }, 1000);

            }
        });

    };

    $scope.InactiveSpaces = function () {
        var flrcode = $scope.SearchSpace.Floor[0].FLR_CODE;

        var dataobj = { FLR_CODE: flrcode, SPC_LST: $scope.selectedActSpaces };
        MaploaderService.InactiveSeats(dataobj).then(function (response) {
            if (response != null) {
                angular.forEach($scope.selectedActSpaces, function (value, key) {
                    _.remove($scope.Markers, { SPC_ID: value.SPACE_ID });
                });
                setTimeout(function () {
                    reloadMarkers();
                }, 1000);

                $scope.selectedActSpaces = [];
                $scope.selectedSpaces = [];
                $('#Active').modal('hide');
                showNotification('success', 8, 'bottom-right', 'Selected Spaces Has Been Inactive');
                setTimeout(function () {
                    $scope.ShowTotalOnMap();
                    $scope.loadThemes();
                });
            }
        });
    };

    function customEditor(params) {
        if ($scope.FlipKartSmallChanges.SYSP_VAL1 == 1) {
            params.data.SH_CODE = $scope.Shifts[0].SH_CODE;
        }
        if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
            params.data.FROM_TIME = '09:00';
            params.data.TO_TIME = '18:00';
        }
        var editing = false;



        var eCell = document.createElement('div');
        var eLabel = document.createTextNode('Please Select');
        if (params.value)
            eLabel.nodeValue = params.value;
        eCell.appendChild(eLabel);

        var eSelect = document.createElement("select");
        eSelect.setAttribute("width", 50);
        var role = _.find($scope.GetRoleAndRM);
        if (role.AUR_ROLE == 6) {
            editing = true;
        }

        eCell.addEventListener('click', function () {
            if (!editing && !params.data.disabledOpt) {
                progress(0, 'Loading...', true);
                if (params.column.colId === "VER_NAME") {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");
                    //eSelect.appendChild(eOption);
                    angular.forEach($scope.Verticals, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.VER_CODE);
                        eOption.setAttribute("text", item.VER_NAME);
                        eOption.innerHTML = item.VER_NAME;
                        eSelect.appendChild(eOption);

                    });
                    progress(0, '', false);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    $scope.getCostcenterByVertical(params.data, eSelect);
                }
                else if (params.column.colId === "AUR_NAME" && params.data.Cost_Center_Code == "" && $scope.FlipKartSmallChanges.SYSP_VAL1 == 0) {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");
                    angular.forEach($scope.AurNames, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);



                    });
                    progress(0, '', false);

                }
                else {
                    if (params.data.Cost_Center_Code != "" && $scope.FlipKartSmallChanges.SYSP_VAL1 == 0)
                        $scope.cstChange(params.data, eSelect);
                }
                setTimeout(function () {
                    eSelect.value = params.value;

                    eCell.removeChild(eLabel);
                    eCell.appendChild(eSelect);

                    //console.log(eCell);
                    eSelect.focus();
                    editing = true;
                }, 200);
            }
        });

        eSelect.addEventListener('blur', function () {

            if (editing) {

                editing = false;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
            }
        });


        eSelect.addEventListener('change', function () {

            if (editing) {
                editing = false;

                var newValue = eSelect.options[eSelect.selectedIndex].text;
                eLabel.nodeValue = newValue;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
                if (params.column.colId === "VER_NAME") {
                    params.data.VERTICAL = eSelect.value;
                    params.data.VER_NAME = newValue;
                    $scope.getCostcenterByVertical(params.data, eSelect);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    var prefval = _.find($scope.SysPref, { SYSP_CODE: "LOB Change Alert" })

                    if (prefval.SYSP_VAL1 == 1) {

                        if ((eSelect.value != params.data.Cost_Center_Code) && (params.data.Cost_Center_Code != "" || params.data.Cost_Center_Code == null) && (params.data.SPACE_TYPE != "2")) {

                            showNotification('error', 40, 'bottom-right', 'Release, seat allocated to another LOB');

                            $('#btnAllocateSeats').attr('disabled', 'disabled');
                            eLabel.nodeValue = params.data.Cost_Center_Name;
                            eCell.appendChild(eLabel);


                        }
                        else {


                            $('#btnAllocateSeats').attr('disabled', false);
                            params.data.Cost_Center_Code = eSelect.value;
                            params.data.Cost_Center_Name = newValue;
                            $scope.cstChange(params.data, eSelect);
                        }
                    }
                    else {

                        params.data.Cost_Center_Code = eSelect.value;
                        params.data.Cost_Center_Name = newValue;
                        $scope.cstChange(params.data, eSelect);
                    }

                }
                else {
                    params.data.AUR_ID = eSelect.value;
                    params.data.AUR_NAME = newValue;
                    $scope.getVer_And_CC(params);
                    $scope.EmpChange(params.data, params.data.AUR_ID);
                }
            }
        });
        return eCell;
    }

    UtilityService.getVerticals(3).then(function (response) {

        if (response.data != null) {
            $scope.Verticals = response.data;
        }
    });

    $scope.SPACETYPE = [];

    UtilityService.getAurNames().then(function (response) {
        if (response.data != null) {
            $scope.AurNames = response.data;
        }
    $scope.SPACETYPE = [];
        MaploaderService.getSpaceDaysRestriction().then(function (response) {
            if (response != null) {
                $scope.SPACETYPE = angular.copy(response);
                _.forEach(response, function (key, value) {
                    if (key.Count > 0) {
                        switch (key.sno) {
                            case 1: $scope.enddate1selection = '+' + (key.Count - 1) + 'd';
                                break;
                            case 2: $scope.enddate2selection = '+' + (key.Count - 1) + 'd';
                                break;
                            case 4: $scope.enddate4selection = '+' + (key.Count - 1) + 'd';
                                break;
                            case 5: $scope.enddate5selection = '+' + (key.Count - 1) + 'd';
                                break;
                        }
                    }
                    else {
                        switch (key.sno) {
                            case 1: $scope.enddate1selection = '';
                                break;
                            case 2: $scope.enddate2selection = '';
                                break;
                            case 4: $scope.enddate4selection = '';
                                break;
                            case 5: $scope.enddate5selection = '';
                                break;
                        }
                    }

                });

            }

            //if (response != null) {
            //    var count = response[0].RESTRICT_DAYS_COUNT;
            //    if (count > 0) {
            //        $scope.enddateselection = '+' + (count - 1) + 'd';
            //    }
            //    else {
            //        $scope.enddateselection = '';
            //    }
            //}
        });
    });

    MaploaderService.getallFilterbyItem().then(function (response) {

        if (response != null) {
            $scope.Items = response;
            $scope.ddlItem = response[0].CODE;
        }
    });

    $scope.getCostcenterByVertical = function (data, eSelect) {
        $scope.CostCenters = [];
        data.STATUS = 1002;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
        else {
            data.SSA_SRNREQ_ID = null;
            data.SSAD_SRN_REQ_ID = null;
        }
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eSelect.appendChild(eOption);
        UtilityService.getCostcenterByVerticalcode({ VER_CODE: data.VERTICAL }, 2).then(function (response) {
            // $timeout(function () {
            $scope.CostCenters = response.data;


            angular.forEach(response.data, function (item, key) {
                var eOption = document.createElement("option");
                eOption.setAttribute("value", item.Cost_Center_Code);
                eOption.setAttribute("text", item.Cost_Center_Name);
                eOption.innerHTML = item.Cost_Center_Name;
                eSelect.appendChild(eOption);
            });
            progress(0, '', false);
            //  }, 500);
        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.getVer_And_CC = function (data) {

        var params = {
            AUR_ID: data.data.AUR_ID
        }
        MaploaderService.getVer_And_CC(params).then(function (response) {

            data.data.AUR_ID = response[0].AUR_ID;
            data.data.AUR_NAME = response[0].AUR_NAME;

            data.data.VERTICAL = response[0].VER_CODE;
            data.data.VER_NAME = response[0].VER_NAME;
            data.data.Cost_Center_Code = response[0].COST_CODE;
            data.data.Cost_Center_Name = response[0].Cost_Center_Name;
            $scope.gridSpaceAllocOptions.api.refreshView();

        }, function (error) {
            progress(0, '', false);
            console.log(error);

        })


    };



    $scope.EmpChange = function (data, OldVal) {
        //$scope.$apply(function () {
        $scope.OnEmpAllocSeats = true;
        //});
        //$scope.SelectedEmp.push({ AUR_ID: OldVal });
        // var valueArr = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });
        var valueArr = _.filter($scope.gridSpaceAllocOptions.rowData, function (o) { return o.ticked == true; }).map(function (x) { return x.AUR_ID; });

        var isDuplicate = valueArr.some(function (item, idx) {
            if (item != '')
                return valueArr.indexOf(item) != idx
        });

        if (isDuplicate == true) {
            showNotification('error', 8, 'bottom-right', 'Employee Should Not be Same');
            data.AUR_NAME = '';
            data.AUR_SEARCH = '';
            data.Cost_Center_Name = '';
            data.Cost_Center_Code = '';
            data.VERTICAL = '';
            data.VER_NAME = '';
        }
        else {
            $scope.SelectedEmp = data;
            data.STATUS = 1004;
            if (data.STACHECK != UtilityService.Added)
                data.STACHECK = UtilityService.Modified;
            for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
                if ($scope.EmpDetails[i].AUR_ID === data.AUR_ID) {
                    $scope.EmpDetails[i].ticked = true;
                    data.ticked = true;
                }
            }

            var lcm_code = '';
            if ($scope.SearchSpace.Location.length == 0) {
                let searchParams = new URLSearchParams(window.location.search);
                if (searchParams.get('lcm_code')) {
                    lcm_code = searchParams.get('lcm_code')
                }
            }

            var dataobj = { lcm_code: ($scope.SearchSpace.Location[0].LCM_CODE || lcm_code), twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: data.AUR_ID, mode: 1 };
            MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                if (result.data.length > 0) {
                    if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1) {
                        if (data.SPACE_TYPE != '4') {
                            $scope.EmpAllocSeats.push(result.data[0]);
                        }
                    }
                    else {
                        if (($scope.EmidsAllocate.SYSP_VAL1 == 1 &&  $scope.EmidsAllocate)) {
                            $scope.ProallocEnabledStatus = true;
                        }
                        $scope.EmpAllocSeats.push(result.data[0]);
                    }
                    
                }
                $scope.EmpAllocSeats = _.remove($scope.EmpAllocSeats, function (n) {
                    return _.find($scope.gridSpaceAllocOptions.rowData, { AUR_ID: n.AUR_ID });
                });
                $scope.OnEmpAllocSeats = false;
                $scope.EmpAllocSeats = _.uniqBy($scope.EmpAllocSeats, 'AUR_ID');
            });
        }
    }

    $scope.blkChanged = function (data) {
        data.STATUS = 1052;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
    }

    function removeOptions(selectbox) {
        var i;
        for (var i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    $scope.cstChange = function (data, eSelect) {
        data.STATUS = 1003;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
        data.AUR_ID = "";
        data.AUR_NAME = "";
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eOption.setAttribute("text", "Please select");
        eSelect.appendChild(eOption);

        var dataobj = {
            lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE,
            Item: data.VERTICAL, subitem: data.Cost_Center_Code
        };
        MaploaderService.getEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {
                if (response.data.length != 0) {
                    $scope.EmpDetails = response.data;
                    angular.forEach($scope.EmpDetails, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);
                        if (data.AUR_ID === item.AUR_ID) {
                            eSelect.value = data.AUR_ID;
                            data.ticked = true;
                            item.ticked = true;
                        }
                        if (_.find($scope.Markers, { AUR_ID: item.AUR_ID }))
                            eOption.disabled = true;
                        progress(0, '', false);
                    });
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Employee not found');
                }
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message + $scope.BsmDet.Child);
            }

        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }
    $scope.checkshiftvalue = 0;
    $scope.shiftChange = function (data) {

        for (var i in $scope.ShiftData) {
            if (data.SH_CODE == $scope.ShiftData[i].SH_CODE) {
                data.FROM_TIME = $scope.ShiftData[i].SH_FRM_HRS;
                data.TO_TIME = $scope.ShiftData[i].SH_TO_HRS;
            }
        }

        if (data.STATUS != 1 && (data.SSA_SRNREQ_ID != "" || data.FROM_DATE != "")) {
            MaploaderService.spcAvailabilityByShift(data).then(function (response) {
                if (response.data == null) {
                    //$scope.checkshiftvalue = 1;
                    //$('#allocatespacebtn').prop("disabled", true);      
                    $scope.AllocateEnabledStatus = true;
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
                else {
                    if ($scope.GradeAlloc.SYSP_VAL1 == 1 && response.data != null) {
                        $scope.GradeAllocStatus();
                    }
                    else {
                        $scope.AllocateEnabledStatus = false;
                        //$('#allocatespacebtn').prop("disabled", false);
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    }

    //$scope.FilterSpaces = function () {
    //    progress(0, 'Loading...', true);
    //    $scope.LoadMap($scope.SearchSpace.Floor[0].FLR_CODE);
    //    updateSummaryCount();
    //    $('#divFilter').toggle("slow");
    //    setTimeout(function () {
    //        $('#accordion').toggle("slow");
    //    }, 100)
    //    //$('#divFilter').toggle("slow");
    //    //$('#accordion').toggle("slow");
    //}

    $scope.FilterSpaces = function () {
        progress(0, 'Loading...', true);


        //$scope.SearchSpace.Location = [];
        //$scope.SearchSpace.Tower = [];
        //$scope.SearchSpace.Floor = [];

        if ($scope.SearchSpace.Location.length > 0 && $scope.SearchSpace.Tower.length > 0 && $scope.SearchSpace.Floor.length > 0) {
            $scope.LoadMap($scope.SearchSpace.Floor[0].FLR_CODE);
            updateSummaryCount();
            var data = {}, bool = true, bool1 = true, bool2 = true;
            angular.forEach($scope.SearchSpace.Location, function (value, key) {
                if (value.ticked && bool) {
                    data.LCM_CODE = value.LCM_CODE;
                    bool = false;
                }
            });
            angular.forEach($scope.SearchSpace.Tower, function (value, key) {
                if (value.ticked && bool1) {
                    data.TWR_CODE = value.TWR_CODE;
                    bool1 = false;
                }
            });
            angular.forEach($scope.SearchSpace.Floor, function (value, key) {
                if (value.ticked && bool2) {
                    data.FLR_CODE = value.FLR_CODE;
                    bool2 = false;
                }
            });
            EditFunction(data);

            reloadMarkers();
            $('#divFilter').toggle("slow");
            setTimeout(function () {
                $('#accordion').toggle("slow");
            }, 100)
        }
        else {
            updateSummaryCount();
            progress(0, 'Loading...', false);
        }
    }
    function EditFunction(data) {
        //if (data.LCM_CODE == 'GGN3' && data.TWR_CODE == 'GGN3-02') {
        //    window.open("../../SMViews/Map/HotDeskMapLoader.aspx?lcm_code=" + encodeURIComponent(data.LCM_CODE) + "&twr_code=" + encodeURIComponent(data.TWR_CODE) + "&flr_code=" + encodeURIComponent(data.FLR_CODE), "_self")
        //}
        /*else {*/
        window.open("../../SMViews/Map/Maploader.aspx?lcm_code=" + encodeURIComponent(data.LCM_CODE) + "&twr_code=" + encodeURIComponent(data.TWR_CODE) + "&flr_code=" + encodeURIComponent(data.FLR_CODE), "_self")
        /*}*/
    }



    function createFromTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.FROM_TIME');
        newTime.setAttribute('ng-disabled', 'data.disabledOpt');
        newTime.type = "text";
        newTime.id = 'fromtime' + params.rowIndex;
        newTime.className = "pickDate form-control mt-1";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime = moment(e.target.value, 'HH:mm').add(total, 'hours').format('HH:mm');
                var selectedspaceexist = _.find($scope.selectedSpaces,
                    function (x) {
                        if (x.FROM_TIME === e.target.value && x.SPC_ID === params.data.SPC_ID) {

                            return x
                        }
                    });
                if (selectedspaceexist) {
                    showNotification('error', 8, 'bottom-right', "Time slot not available");
                    e.target.value = "";
                    return;
                }
                params.data.TO_TIME = endTime;
            });
        return newTime;
    }

    function createToTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.TO_TIME');
        newTime.setAttribute('ng-disabled', 'data.disabledOpt');
        newTime.type = "text";
        newTime.id = 'totime' + params.rowIndex;
        newTime.className = "pickDate form-control mt-1";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime1 = moment(params.data.FROM_TIME, 'HH:mm').add(total, 'hours').format('HH:mm');
                var currentTime = moment(e.target.value, "HH:mm a");
                var startTime = moment(params.data.FROM_TIME, "HH:mm a");
                var endTime = moment(endTime1, "HH:mm a");
                currentTime.toString();
                startTime.toString();
                var c = currentTime.isBetween(startTime, endTime);  // false
                //if (!currentTime.isBetween(startTime, endTime)) {
                //    showNotification('error', 8, 'bottom-right', "To time should not be less than from time and not exceed business hours");
                //}
            });
        return newTime;
    }

    var fromdate, todate;

    function check_validattion_of_MultipleBooking(date, spcid) {
        var params = {
            FROM_DATE: date,
            SPC_ID: spcid
        };
        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/MaploaderAPI/SEAT_RESTRICTION_BOOKING",
            data: JSON.stringify(params),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $scope.Remaining = response.data[0].Remaining;
            }
        });
    }

    function createFromDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.FROM_DATE');
        newDate.setAttribute('ng-disabled', 'data.disabledOpt');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate form-control mt-1";
        fromdate = params.data.FROM_DATE;
        todate = params.data.TO_DATE;
        $('#TO_DATE').val(params.data.TO_DATE);
        var currentDate = new Date();
        var currentHour = currentDate.getHours();
        var today = new Date();
        var tomorrow = new Date(today);
        tomorrow.setDate(today.getDate() + 1);
        if ($scope.ApexonChanges && $scope.ApexonChanges.SYSP_VAL1 == 1 && currentHour >= 18) {
            $(newDate).datepicker({
                //format: 'dd M, yyyy',
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: false,
                startDate: tomorrow,
                keyboardNavigation: false,
                forceParse: false,
                endDate: tomorrow,
            }).datepicker('setDate', tomorrow);
            $(newDate).val(moment(tomorrow).format('mm/dd/yyyy'));
            return newDate;
        }
        else {

            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: new Date(),
            })

            switch (params.data.SPACE_TYPE) {
                case "1": $scope.enddateselection = $scope.enddate1selection; break;
                case "2": $scope.enddateselection = $scope.enddate2selection; break;
                case "4": $scope.enddateselection = $scope.enddate4selection; break;
                case "5": $scope.enddateselection = $scope.enddate5selection; break;
            }

            $(newDate).datepicker({
                //format: 'dd M, yyyy',
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: new Date(),
                keyboardNavigation: false,
                forceParse: false,
                endDate: $scope.enddateselection
            }).on("input change", function (e) {
                var prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
                if (prefvalue != undefined) {

                    if ($scope.LibertyChanges.SYSP_VAL1 == 1) { // && $scope.GetRoleAndRM[0].AUR_ROLE == 6) {
                        check_validattion_of_MultipleBooking(params.data.FROM_DATE, params.data.SPC_ID);
                    }

                    var checkdate = moment(e.target.value).add(90, 'day').format('MM/DD/YYYY');
                    $('#TO_DATE').val(checkdate);
                    fromdate = e.target.value;
                    todate = checkdate;
                    params.data.TO_DATE = checkdate;
                    var d = e.target.value;

                    $('#TO_DATE').datepicker({
                        format: 'mm/dd/yyyy',
                        autoclose: true,
                        todayHighlight: true,
                        startDate: d,
                        endDate: checkdate,
                        //maxDate: '10/19/2019',

                    })

                    $('#TO_DATE').val();
                    //$scope.datedisable = true;               
                }
            });
            return newDate;
        }

    }
    //function createFromDatePicker(params) {
    //    var editing = false;
    //    var newDate;
    //    newDate = document.createElement('input');
    //    newDate.setAttribute('ng-model', 'data.FROM_DATE');
    //    newDate.type = "text";
    //    newDate.id = params.rowIndex;
    //    newDate.className = "pickDate";
    //    $(newDate).datepicker({
    //        //format: 'dd M, yyyy',
    //        format: 'mm/dd/yyyy',
    //        autoclose: true,
    //        todayHighlight: true,
    //    }).on("input change", function (e) {
    //        var prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
    //        if (prefvalue != undefined) {
    //            var checkdate = moment(e.target.value).add(90, 'day').format('MM/DD/YYYY');
    //            params.data.TO_DATE = checkdate;
    //            $scope.datedisable = true;
    //        }
    //    });
    //    return newDate;
    //}

    function createToDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.TO_DATE');
        newDate.setAttribute('ng-disabled', 'data.disabledOpt');
        //newDate.setAttribute('ng-disabled', 'datedisable==true');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate form-control mt-1";
        var prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
        //console.log(prefvalue);
        if (prefvalue != undefined) {

            $scope.datedisable = true;
        }
        var currentDate = new Date();
        var currentHour = currentDate.getHours();
        var today = new Date();
        var tomorrow = new Date(today);
        tomorrow.setDate(today.getDate() + 1);
        if ($scope.ApexonChanges && $scope.ApexonChanges.SYSP_VAL1 == 1 && currentHour >= 18) {
            $(newDate).datepicker({
                //format: 'dd M, yyyy',
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: false,
                startDate: tomorrow,
                keyboardNavigation: false,
                forceParse: false,
                endDate: tomorrow
            }).datepicker('setDate', tomorrow);
            $(newDate).val(moment(tomorrow).format('mm/dd/yyyy'));
            return newDate;
        }
        else {
            $(newDate).datepicker({
                //format: 'dd M, yyyy',
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: new Date(),
                keyboardNavigation: false,
                forceParse: false,
                endDate: $scope.enddateselection
            }).on
            return newDate;
        }
    }

    $scope.gridSpaceAllocOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        rowHeight: 48,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        headerHeight: 40
    };

    $scope.gridSpaceAllocation = {
        columnDefs: $scope.columDefSpaceAlloc,
        rowData: null,
        rowHeight: 48,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
    };
    //Showing Vacant Seats On Map When Click On Seat Status - Vacant
    $scope.ShowVacantOnMap = function () {
        var vacspaces = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1) {
                marker.setStyle(VacantStyle);
                vacspaces.push(marker);
            }
            progress(0, '', false);
        });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(vacspaces, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#228B22", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    };

    //Total Seats On Map
    $scope.ShowTotalOnMap = function () {
        //$scope.LoadMarkerLayers($scope.SearchSpace.Floor[0].FLR_CODE);
        $scope.Markerdata.clearLayers();
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle);
                    break;
                case 1002: marker.setStyle(AllocateStyle);
                    break;
                case 1004: marker.setStyle(OccupiedStyle);
                    break;
                case 1003: marker.setStyle(AllocateStyle);
                    break;
                case 1052: marker.setStyle(BlockStyle);
                    break;
                default: marker.setStyle(VacantStyle);
                    break;
            }
            marker.bindLabel(marker.SPC_DESC, {
                noHide: false,
                direction: 'auto',
                zIndexOffset: 2000
            });//.setOpacity(1);
            $scope.Markerdata.addLayer(marker);
        });
        $scope.loadThemes();
    };

    //Occupied To Employee on Map
    $scope.ShowOccupiedOnMap = function () {
        var occspaces = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1004) {
                marker.setStyle(OccupiedStyle);
                occspaces.push(marker);
            }
            progress(0, '', false);
        });

        //console.log(occspaces);
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(occspaces, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#E74C3C", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    }

    //Vertical Costcenter On Map
    $scope.ShowCostCentre = function () {
        var costcentre = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1003) {
                marker.setStyle(CstVer);
                costcentre.push(marker);
            }
            else if (marker.STATUS == 1002) {
                marker.setStyle(CstVer);
                costcentre.push(marker);
            }
            progress(0, '', false);
        });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(costcentre, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#3ccae7", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    }


    $('#SeatAllocation').on('shown.bs.modal', function () {
        $('#btnAllocateSeats').attr('disabled', false);
        $scope.relfrom = {};
        var role = _.find($scope.GetRoleAndRM);
        var emp = _.find($scope.Employee);
        if ((role.AUR_ROLE == 6 && $scope.selectedSpaces[0].SPACE_TYPE == 1) || (role.AUR_ROLE == 6 && $scope.LibertyChanges && $scope.LibertyChanges.SYSP_VAL1 == 1)) {
            angular.forEach($scope.selectedSpaces, function (value, key) {
                value.ticked = false;
                value.disabled = true;
                if (value.AUR_ID == emp.AUR_ID || value.AUR_ID == "") {
                    value.ticked = true;
                    value.disabled = false;
                }
            });
            $('#fromtime').timepicker(
                {
                    timeFormat: 'H:i',
                    show2400: true
                }).on("input change", function (e) {
                    var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                    var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                    var endTime = moment(e.target.value, 'HH:mm').add(total, 'hours').format('HH:mm');
                    var selectedspaceexist = _.find($scope.selectedSpaces,
                        function (x) {
                            if (x.FROM_TIME === e.target.value && x.SPC_ID === params.data.SPC_ID) {

                                return x
                            }
                        });
                    if (selectedspaceexist) {
                        showNotification('error', 8, 'bottom-right', "Time slot not available");
                        e.target.value = "";
                        return;
                    }
                    /* params.data.TO_TIME = endTime;*/
                });
            $('#totime').timepicker(
                {
                    timeFormat: 'H:i',
                    show2400: true
                }).on("input change", function (e) {
                    var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                    var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                    var endTime = moment(e.target.value, 'HH:mm').add(total, 'hours').format('HH:mm');
                    var selectedspaceexist = _.find($scope.selectedSpaces,
                        function (x) {
                            if (x.FROM_TIME === e.target.value && x.SPC_ID === params.data.SPC_ID) {

                                return x
                            }
                        });
                    if (selectedspaceexist) {
                        showNotification('error', 8, 'bottom-right', "Time slot not available");
                        e.target.value = "";
                        return;
                    }
                    //params.data.TO_TIME = endTime;
                });
            if ($scope.ApexonChanges && $scope.ApexonChanges.SYSP_VAL1 == 1 && currentHour >= 18) {
                $(newDate).datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    todayHighlight: false,
                    startDate: tomorrow,
                    keyboardNavigation: false,
                    forceParse: false,
                    endDate: tomorrow,
                }).datepicker('setDate', tomorrow);
                $(newDate).val(moment(tomorrow).format('mm/dd/yyyy'));
                return newDate;
            } else {
            $('#fromDate').datepicker({
                //format: 'dd M, yyyy',
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: new Date(),
                endDate: $scope.enddateselection
            }).on
            }
            if ($scope.ApexonChanges && $scope.ApexonChanges.SYSP_VAL1 == 1 && currentHour >= 18) {
                $(newDate).datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    todayHighlight: false,
                    startDate: tomorrow,
                    keyboardNavigation: false,
                    forceParse: false,
                    endDate: tomorrow,
                }).datepicker('setDate', tomorrow);
                $(newDate).val(moment(tomorrow).format('mm/dd/yyyy'));
                return newDate;
            } else {
                $('#ToDate').datepicker({
                    //format: 'dd M, yyyy',
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    todayHighlight: true,
                    startDate: new Date(),
                    endDate: $scope.enddateselection
                }).on
            }
            var count = $scope.selectedSpaces.length;
            $scope.selectedSpaces.count = count;
            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            $scope.gridSpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridSpaceAllocOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
            //$scope.gridSpaceAllocOptions.api.refreshView();
            $scope.gridSpaceAllocOptions.api.refreshHeader();
        }
        else {
            if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
                //$scope.selectedSpaces[0].disabled = true;
                //$scope.selectedSpaces[0].ticked = false;
                angular.forEach($scope.selectedSpaces, function (value, key) {
                    value.ticked = false;
                    value.disabled = true;

                });
            }


            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            $scope.gridSpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridSpaceAllocOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
            //$scope.gridSpaceAllocOptions.api.refreshView();
            $scope.gridSpaceAllocOptions.api.refreshHeader();
        }

    });

    function SeattypeVal(sno) {
        for (var i in $scope.SPACETYPE) {
            if ($scope.SPACETYPE[i].sno==sno || $scope.SPACETYPE[i].SPACE_TYPE==sno) {
                return $scope.SPACETYPE[i].SPACE_TYPE;
            }
        }
    }

    $scope.ReleaseEmp = function (Relemp) {
        progress(0, 'Loading...', true);
        if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1 && $scope.StartPoints.length > 1) {
            $scope.WayPoints = [];
            $scope.GetBaysbyWing = [];
            $scope.FindPathOfSeat('C');
        }

        $scope.SelRelEmp = [];
        $scope.SelRelEmp.push(Relemp);
        var role = _.find($scope.GetRoleAndRM);
        if (role.AUR_ROLE == 6) {
            if ($scope.VerticalWiseAlloc.SYSP_VAL1 == 1) {
                var dataobj = { sad: $scope.SelRelEmp, reltype: 1003 };
            } else {
                var dataobj = { sad: $scope.SelRelEmp, reltype: 1002 };
            }
        }
        else {
            var dataobj = { sad: $scope.SelRelEmp, reltype: 1002 };

        }

        _.remove($scope.EmpAllocSeats, function (sel) {
            return sel.SSAD_SRN_REQ_ID === Relemp.SSAD_SRN_REQ_ID
        });
        $scope.selectSeatCount = 0;
        MaploaderService.release(dataobj).then(function (response) {

            $scope.releasecount++;
            var result = $filter('filter')($scope.SelRelEmp, { SPC_FLR_ID: $scope.SearchSpace.Floor[0].FLR_CODE });
            for (var a = $scope.SelRelEmp.length - 1; a >= 0; a--) {


//Ragini
                //if ($scope.SearchSpace.Floor[0].FLR_CODE == $scope.SelRelEmp[a].FLR_CODE) {
                //    var spcid = _.find($scope.Markers, { SPC_ID: $scope.SelRelEmp[a].SPC_ID });
                //    var emp = _.find($scope.selectedSpaces, { ticked: false })


                //    if (role.AUR_ROLE == 6) {

                //        if ($scope.selectedSpaces.length != $scope.SelRelEmp.length) {

                //            if ($scope.selectedSpaces[0].SPC_ID != $scope.SelRelEmp[0].SPC_ID) {
                //                spcid.setStyle(OccupiedStyle);
                //                spcid.ticked = false;
                //            };

                //            spcid.STATUS = 1004;

                //            var seatdes = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SPACE_TYPE) + " ,Occupied by :" + emp.AUR_ID;

                //            spcid.SPC_DESC = seatdes;


                if ($scope.SearchSpace.Floor[0].FLR_CODE == $scope.SelRelEmp[a].FLR_CODE) {
                    var spcid = _.find($scope.Markers, { SPC_ID: $scope.SelRelEmp[a].SPC_ID });
                    var emp = _.find($scope.selectedSpaces, (Relemp) => Relemp.AUR_ID !== $scope.SelRelEmp[0].AUR_ID);


                    if (role.AUR_ROLE == 6) {

                        if ($scope.selectedSpaces.length != $scope.SelRelEmp.length) {
                            var emplist = _.filter($scope.selectedSpaces, (Relemp) => Relemp.AUR_ID !== $scope.SelRelEmp[0].AUR_ID);
                            if ($scope.selectedSpaces[0].SPC_ID != $scope.SelRelEmp[0].SPC_ID) {
                                spcid.setStyle(OccupiedStyle);
                                spcid.ticked = false;
                            };

                            spcid.STATUS = 1004;
                            spcid.setStyle(OccupiedStyle);
                            spcid.ticked = false;
                            var seatdes = '';
                            emplist.forEach((emp, index) => {
                                if (index == 0) {
                                    seatdes = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + ",,Allocated to:" + emp.Cost_Center_Name + '/' + emp.VER_NAME + " ,Occupied by :" + emp.AUR_ID + '/' + emp.AUR_NAME;
                                } else {
                                    seatdes = seatdes + ",,Allocated to:" + emp.Cost_Center_Name + '/' + emp.VER_NAME + " ,Occupied by :" + emp.AUR_ID + '/' + emp.AUR_NAME;
                                }
                                seatdes = seatdes + ",Extn:" + "";
                            })
                            //var seatdes = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SPACE_TYPE) + " ,Occupied by :" + emp.AUR_ID;
                            //seatdes = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + ",,Allocated to:" + ($scope.selectedSpaces[0].Cost_Center_Name + '/' + $scope.selectedSpaces[0].VER_NAME) + " ,Occupied by :" + emp.AUR_ID+'/'+emp.AUR_NAME+ ",Extn:" + "";
                            spcid.SPC_DESC = seatdes;
                            var formattedString = seatdes.split(",").join("\n");
                            var k = formattedString.split("\n");
                            var id = "";
                            var id1 = "";
                            var text = "";
                            var fulltext = "";
                            var str = " ";

                            fulltext = BindMarkerText(formattedString, spcid);

                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;

                        }
                        else {
                            //$scope.selectedSpaces[0].SSA_SRNREQ_ID = '';
                            $scope.selectedSpaces[0].SSAD_SRN_REQ_ID = '';
                            spcid.STATUS = 1;
                            // spcid.setStyle(VacantStyle);

                            if ($scope.VerticalWiseAlloc && parseInt($scope.VerticalWiseAlloc.SYSP_VAL1) == 1) {
                                spcid.STATUS = 1002;
                                spcid.setStyle(AllocateStyle);
                            }
                            else if (($scope.clusterWiseAlloc && parseInt($scope.clusterWiseAlloc.SYSP_VAL1) == 1) && (parseInt(Relemp.SPACE_TYPE) != 4)) {
                                spcid.STATUS = 1069;
                                spcid.setStyle(AllocateStyle);
                            }
                            else if ($scope.CostWiseAlloc && parseInt($scope.CostWiseAlloc.SYSP_VAL1) == 1) {
                                spcid.STATUS = 1003;
                                spcid.setStyle(AllocateStyle);
                            }
                            else {
                                if ($scope.PartiallyOccupiedSys.SYSP_VAL1 == 1) {
                                    spcid.STATUS = 1;
                                    spcid.setStyle(VacantStyle);
                                    spcid.setStyle(PartiallyOccupiedStyle);
                                }
                                else {
                                    $scope.IsSpaceID = false;
                                    spcid.STATUS = 1;
                                    spcid.setStyle(VacantStyle);
                                }
                            }

                            if ($scope.selectedSpaces[0].SPC_ID != $scope.SelRelEmp[0].SPC_ID) {
                                spcid.setStyle(OccupiedStyle);
                                spcid.ticked = false;
                            };

                            if ($scope.CostWiseAlloc && parseInt($scope.CostWiseAlloc.SYSP_VAL1) == 1)
                                var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + ",Allocated to:" + ($scope.selectedSpaces[0].Cost_Center_Name + '/' + $scope.selectedSpaces[0].VER_NAME) + ",Extn:" + "";
                            else if ($scope.VerticalWiseAlloc && parseInt($scope.VerticalWiseAlloc.SYSP_VAL1) == 1)
                                var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + ",Allocated to:" + ($scope.selectedSpaces[0].VER_NAME) + ",Extn:" + "";
                            else if ($scope.clusterWiseAlloc && parseInt($scope.clusterWiseAlloc.SYSP_VAL1) == 1 && $scope.selectedSpaces[0].SHIFT_TYPE != 4)
                                var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + ",Allocated to:" + ($scope.selectedSpaces[0].CLUSTER) + ",Extn:" + "";
                            else {
                                var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + ",Employee :Vacant";
                                spcid.SSA_SRNREQ_ID = "";
                            }

                            //var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal() + ",Employee :Vacant";

                            spcid.SPC_DESC = seatdese;
                            var formattedStringe = seatdese.split(",").join("\n");
                            var ke = formattedStringe.split("\n");
                            var ide = "";
                            var id1e = "";
                            var texte = "";
                            var fulltexte = "";
                            var stre = " ";

                            fulltext = BindMarkerText(formattedStringe, spcid);

                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;
                            //spcid.ticked = false;
                        }
                    }
                    else {
                        if ($scope.selectedSpaces.length != $scope.SelRelEmp.length) {
                            spcid.STATUS = 1004;
                            spcid.setStyle(OccupiedStyle);

                            var seatdes = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + " ,Occupied by: " + emp.AUR_ID;

                            spcid.SPC_DESC = seatdes;
                            var formattedString = seatdes.split(",").join("\n");
                            var k = formattedString.split("\n");
                            var id = "";
                            var id1 = "";
                            var text = "";
                            var fulltext = "";
                            var str = " ";
                            fulltext = BindMarkerText(formattedString, spcid);

                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;
                            spcid.ticked = false;
                        }
                        else {
                            var seatdesc = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE)+ ",Employee :Vacant" ;
                            spcid.SPC_DESC = seatdesc;
                            var formattedStringc = seatdesc.split(",").join("\n");
                            var kc = formattedStringc.split("\n");
                            var idc = "";
                            var id1c = "";
                            var textc = "";
                            var fulltext = "";
                            var strc = " ";
                            fulltext = BindMarkerText(formattedStringc, spcid);

							spcid.SSA_SRNREQ_ID = "";
                            spcid.STATUS = 1;
                            spcid.setStyle(VacantStyle);

                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;
                            spcid.ticked = false;
                        }
                    }

                }

                //$('#SeatAllocation').modal('hide');
                //$scope.selectedSpaces = [];
                //Ragini

                //setTimeout(function () {
                //    progress(0, 'Loading...', false);
                //    showNotification('success', 8, 'bottom-right', "Selected Spaces Released From Employee");
                //}, 1000);

                setTimeout(function () {
                    //$('#SeatAllocation').modal('hide');
                    //$scope.selectedSpaces = [];
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', "Selected Spaces Released From Employee");
                    reloadMarkers();
                }, 1000);

                $('#btnAllocateSeats').attr('enabled', 'enabled');

                if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                    $scope.GradeAllocStatus();
                }
                //$scope.DisableAllocSeats = false;
            }
            //reloadMarkers();
            if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1)
                $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
        });
        //if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
        //    var dataobj = {
        //        flr_code: $scope.SelRelEmp[0].FLR_CODE,
        //        category: $scope.SelRelEmp[0].SPC_ID,
        //        subitem: $scope.SelRelEmp[0].SSA_SRNREQ_ID ? $scope.SelRelEmp[0].SSA_SRNREQ_ID : ""
        //    };

        //    MaploaderService.SeatRestriction(dataobj).then(function (response) {
        //        if (response != null) {
        //            $scope.SeatRestrictionStatus = response[0].Seat_Restr;
        //            console.log($scope.SeatRestrictionStatus);
        //        }
        //    });
        //}

    }
    $scope.ShowHide = function () {
        debugger;
        $scope.foremp = false;
        $scope.IsVisible = !$scope.IsVisible;

    };
    $scope.Proceedalloc = function () {
        var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" })
        if ((prefval == undefined) || prefval.SYSP_VAL1 == undefined) {
            $scope.EmpAllocSeats = []
            progress(0, '', false);
        }
        else if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {
            showNotification('error', 8, 'bottom-right', 'Release the employee');
        }

    }


    $scope.Release = function (reltypecode, reltypename) {
        progress(0, 'Loading...', true);
        $scope.relfrom = {};
        $scope.relfrom.Name = reltypename;
        $scope.relfrom.Value = reltypecode;
		  if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
            var data = _.filter($scope.selectedSpaces, function (o) { return o.SSAD_SRN_REQ_ID == null; });
            if (data.length > 0) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "You don't have any booked seat,so you cannot release ");
                return;
            }
        }

        var dataobj = { sad: $filter('filter')($scope.selectedSpaces, function (x) { if (x.ticked == true) return x; }), reltype: reltypecode };


        if (dataobj.sad.length != 0) {
            $scope.selectSeatCount = 0;
            MaploaderService.release(dataobj).then(function (response) {
                if (response.data != null) {
                    angular.forEach($scope.selectedSpaces, function (value, key) {
                        if (value.ticked == true) {
                            var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });

                            //_.remove($scope.selectedSpaces, function (space) { return space.SPC_ID == e.target.SPC_ID });
                            //var spclst = _.filter($scope.selectedSpaces, function (space) { if (space.SPC_ID == value.SPC_ID && space != value) return space });
                            //var length = 0;
                            //if (spclst != undefined)
                            //    length = spclst.length;
                            //if (length > 0)
                            //    updateSpcDetails(spclst, spcid);
                            //else {
                            if (dataobj.sad.length != $scope.selectedSpaces.length) {
                                var emp = _.find($scope.selectedSpaces, { ticked: false })


                                value.AUR_ID = emp.AUR_ID;
                                value.STACHECK = UtilityService.Deleted;
                                spcid.STATUS = 1004;
                                spcid.setStyle(OccupiedStyle);
                                spcid.ticked = false;
                                var seatdes = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + " ,Occupied by: " + emp.AUR_ID;
                                var formattedString = seatdes.split(",").join("\n");
                                var k = formattedString.split("\n");
                                var id = "";
                                var id1 = "";
                                var text = "";
                                var fulltext = "";
                                var str = " ";
                                fulltext = BindMarkerText(formattedString, spcid);



                                spcid.bindLabel(fulltext.split(",").join("\n"), {
                                    noHide: false,
                                    direction: 'auto',
                                    zIndexOffset: 2000
                                });//.setOpacity(1);
                                spcid.SPC_DESC = fulltext.split(",").join("\n");
                                spcid.STACHECK = UtilityService.Deleted;
                            }
                            else {

                                switch ($scope.relfrom.Value) {
                                    case 1004:

                                        value.AUR_ID = "";
                                        value.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1003;
                                        spcid.setStyle(AllocateStyle);
                                        spcid.ticked = false;
                                        var seatdesc = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + ",Allocated to: " + value.Cost_Center_Name + ' /' + value.VERTICAL;
                                        var formattedStringc = seatdesc.split(",").join("\n");
                                        var kc = formattedStringc.split("\n");
                                        var idc = "";
                                        var id1c = "";
                                        var textc = "";
                                        var fulltextc = "";
                                        var strc = " ";
                                        fulltext = BindMarkerText(formattedStringc, spcid);
                                        spcid.SPC_DESC = fulltext.split(",").join("\n");
                                        spcid.bindLabel(fulltext.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);

                                        spcid.STACHECK = UtilityService.Deleted;
                                        break;
                                    case 1003:


                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1002;
                                        spcid.ticked = false;
                                        spcid.setStyle(AllocateStyle);
                                        var seatdesv = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + " ,Allocated to :" + value.VERTICAL;
                                        var formattedStringv = seatdesv.split(",").join("\n");
                                        var kv = formattedStringv.split("\n");
                                        var idv = "";
                                        var id1v = "";
                                        var textv = "";
                                        var fulltextv = "";
                                        var strv = " ";
                                        for (var i = 0; i < kv.length; i++) {
                                            id1v = kv[i].substr(0, kv[i].indexOf(":"));
                                            idv = kv[i].substr(0, kv[i].indexOf(":")).fontcolor("black");
                                            textv = kv[i].substr(kv[i].indexOf(":") + 1).fontcolor("seagreen");
                                            if (value.STATUS == 1004) {
                                                fulltextv += id1v + "\t" + ":" + textv + ",";
                                            }
                                            else {
                                                fulltextv += idv + " :" + textv + ",";
                                            }
                                        }
                                        spcid.bindLabel(fulltextv.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SPC_DESC = fulltextv.split(",").join("\n");
                                        spcid.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1002;
                                        break;
                                    case 1002:


                                        value.VERTICAL = "";
                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.FROM_DATE = "";
                                        value.TO_DATE = "";
                                        value.STATUS = 1;
                                        value.SSA_SRNREQ_ID = "";
                                        spcid.setStyle(VacantStyle);
                                        spcid.ticked = false;
                                        spcid.STATUS = 1;
                                        var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + " ,Employee:Vacant";
                                        if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
                                            switch (spcid.SHIFT_TYPE) {
                                                case '4': seatdese += ",Seat Type: Partially Work From Home";
                                            }
                                        }




                                        var formattedStringe = seatdese.split(",").join("\n");
                                        var ke = formattedStringe.split("\n");
                                        var ide = "";
                                        var id1e = "";
                                        var texte = "";
                                        var fulltexte = "";
                                        var stre = " ";
                                        for (var i = 0; i < ke.length; i++) {
                                            id1e = ke[i].substr(0, ke[i].indexOf(":"));
                                            ide = ke[i].substr(0, ke[i].indexOf(":")).fontcolor("black");
                                            texte = ke[i].substr(ke[i].indexOf(":") + 1).fontcolor("seagreen");
                                            if (value.STATUS == 1004) {
                                                fulltexte += id1e + "\t" + ":" + texte + ",";
                                            }
                                            else {
                                                fulltexte += ide + " :" + texte + ",";
                                            }
                                        }
                                        spcid.bindLabel(fulltexte.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.VERTICAL = "";
                                        spcid.Cost_Center_Code = "";
                                        spcid.SH_CODE = "";
                                        spcid.AUR_ID = "";
                                        spcid.FROM_DATE = "";
                                        spcid.TO_DATE = "";
                                        spcid.STATUS = 1;
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.SPC_DESC = fulltexte.split(",").join("\n");
                                        spcid.STACHECK = UtilityService.Deleted;

                                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                            if (layer.options.spaceid == value.SPC_ID) {
                                                layer.options.seattype = spcid.SPACE_TYPE;
                                            }
                                        });
                                        $scope.loadThemes();
                                        break;
                                    case 1052:

                                        value.VERTICAL = "";
                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.FROM_DATE = "";
                                        value.TO_DATE = "";
                                        value.STATUS = 1;
                                        value.SSA_SRNREQ_ID = "";
                                        spcid.setStyle(VacantStyle);
                                        spcid.STATUS = 1;
                                        spcid.ticked = false;
										var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Seat Type:" + SeattypeVal(spcid.SHIFT_TYPE) + " ,Employee:Vacant";
                                        if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
                                            switch (spcid.SHIFT_TYPE) {
                                                case '4': seatdese += ",Seat Type: Partially Work From Home";
                                            }
                                        }
										var formattedStringe = seatdese.split(",").join("\n");
                                        var ke = formattedStringe.split("\n");
                                        var ide = "";
                                        var id1e = "";
                                        var texte = "";
                                        var fulltexte = "";
                                        var stre = " ";
                                        for (var i = 0; i < ke.length; i++) {
                                            id1e = ke[i].substr(0, ke[i].indexOf(":"));
                                            ide = ke[i].substr(0, ke[i].indexOf(":")).fontcolor("black");
                                            texte = ke[i].substr(ke[i].indexOf(":") + 1).fontcolor("seagreen");
                                            if (value.STATUS == 1004) {
                                                fulltexte += id1e + "\t" + ":" + texte + ",";
                                            }
                                            else {
                                                fulltexte += ide + " :" + texte + ",";
                                            }
                                        }
                                        spcid.bindLabel(fulltexte.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.VERTICAL = "";
                                        spcid.Cost_Center_Code = "";
                                        spcid.SH_CODE = "";
                                        spcid.AUR_ID = "";
                                        spcid.FROM_DATE = "";
                                        spcid.TO_DATE = "";
                                        spcid.STATUS = 1;
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.SPC_DESC = fulltexte.split(",").join("\n");
                                        spcid.STACHECK = UtilityService.Deleted;

                                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                            if (layer.options.spaceid == value.SPC_ID) {
                                                layer.options.seattype = value.SPACE_TYPE;
                                                //console.log(layer.options.spaceid);
                                            }
                                        });
                                        $scope.loadThemes();
                                        break;

                                }
                                //}
                            }
                        }

                    });
                    setTimeout(function () {
                        $('#SeatAllocation').modal('hide');
                        $scope.selectedSpaces = [];
                        updateSummaryCount();
                        progress(0, '', false);
                        $scope.gridSpaceAllocOptions.api.refreshView();
                        showNotification('success', 8, 'bottom-right', "Selected Spaces Released From " + $scope.relfrom.Name);
                        if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                            $scope.GradeAllocStatus();
                        }
                    }, 300);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }, function (error) {
            });
        }

        else {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "employee cannot release others seat");
        }
        if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1)
            $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
    }


    function updateSpcDetails(spclst, spcid) {
        var labelval = "";
        switch ($scope.relfrom.Value) {
            case 1002: spcid.setStyle(VacantStyle);
                spcid.STATUS = 1;
                labelval = spcid.SPC_NAME + " : Vacant";
                spcid.VERTICAL = "";
                spcid.Cost_Center_Name = "";
                spcid.SSA_SRNREQ_ID = "";
                spcid.SSAD_SRN_REQ_ID = "";
                break;
            case 1004: spcid.setStyle(AllocateStyle);
                labelval = spcid.SPC_NAME + " : Allocated to " + spcid.Cost_Center_Name;
                spcid.STATUS = 1003;
                break;
            case 1003: spcid.setStyle(AllocateStyle);
                labelval = spcid.SPC_NAME + " : Allocated to " + spcid.VER_NAME;
                spcid.STATUS = 1002;
                spcid.Cost_Center_Name = "";
                break;
            case 1052: spcid.setStyle(VacantStyle);
                labelval = spcid.SPC_NAME + " : Vacant";
                spcid.VERTICAL = "";
                spcid.Cost_Center_Name = "";
                spcid.SSA_SRNREQ_ID = "";
                spcid.SSAD_SRN_REQ_ID = "";
                break;
            default: spcid.setStyle(VacantStyle);
                break;
        }
        spcid.bindLabel(labelval, {
            noHide: false,
            direction: 'auto',
            zIndexOffset: 2000
        });//.setOpacity(1);
        spcid.SPC_DESC = labelval;
        spcid.SSA_SRNREQ_ID = "";
        spcid.STACHECK = UtilityService.Deleted;
        spcid.ticked = false;

    }


    function BindMarkerText(formattedString, marker) {
        var k = formattedString.split("\n");
        var id = "";
        var id1 = "";
        var text = "";
        var fulltext = "";
        var str = " ";
        fulltext = '<table class="tabledata">';
        for (var i = 0; i < k.length; i++) {
            id1 = k[i].substr(0, k[i].indexOf(":"));
            id = k[i].substr(0, k[i].indexOf(":")).fontcolor("black");
            text = k[i].substr(k[i].indexOf(":") + 1).fontcolor("seagreen");


            //for (var i = 0; i < k.length; i++) {
            //    id1 = k[i].substr(0, k[i].indexOf(":")).trimLeft();
            //    id = k[i].substr(0, k[i].indexOf(":")).fontcolor("black");
            //    text = k[i].substr(k[i].indexOf(":") + 1).fontcolor("seagreen");
            //    if (value.STATUS == 1004) {
            //        //fulltext += id1 + "  " + ":" + " " + text + ",";
            //        fulltext = fulltext.replace(/,\s*$/, "");
            //        fulltext += '<td>' + id1 + '</td><td>   :' + text + '</td></tr>';

            //    }
            //    else {
            //        //fulltext += id + " :" + text + ",";
            //        fulltext = fulltext.replace(/,\s*$/, "");
            //        fulltext += '<td>' + id1 + '</td><td>   :' + text + '</td></tr>';
            //    }
            //}


            if ($scope.LibertyChanges.SYSP_VAL1 == 1 && (marker.STATUS == 1004 || marker.STATUS == 1002)) {


                if ((marker.SHIFT_TYPE == 1 && (id1 === 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn ')) ||
                    (marker.SHIFT_TYPE == 5 && (id1 == 'Employee' || id1 == 'Division' || id1 == 'Channel' || id1 == 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn '))
                    || (marker.SHIFT_TYPE == 4 && (id1 == 'Extn '))
                ) {
                    //no requirement
                }


                else {

                    if (marker.SHIFT_TYPE == 4 && marker.SPC_TYPE_NAME == 'Meeting Room')//&& (id1 == 'Seat Type' || id1 == 'Division' || id1 == 'Channel' || id1 == 'Extn ')) {
                    {
                        if (id1 == 'Seat Type') {
                            text = marker.SPC_TYPE_NAME.fontcolor("seagreen");
                            fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                        }
                        else if (id1 == 'Division' || id1 == 'Channel' || id1 == 'Extn ') {
                            //
                        }
                        else {
                            fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                        }
                    }
                    else {
                        if (id1 == 'Shift') { }
                        else {
                            fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                        }

                    }

                }

            }

            else if ($scope.LibertyChanges.SYSP_VAL1 == 1 && marker.STATUS == 1) {

                if ((marker.SHIFT_TYPE == 5 || marker.SHIFT_TYPE == 6) && (id1 == 'Employee' || id1 == 'Division' || id1 == 'Channel' || id1 == 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn ')) {
                    //
                }
                //else if (marker.SHIFT_TYPE == 6 && (id1 == 'Employee' || id1 == 'Division' || id1 == 'Channel' || id1 == 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn ')) {
                //    //
                //}
                else {

                    if ($scope.LibertyChanges.SYSP_VAL1 == 1 && marker.SHIFT_TYPE == 4 && marker.SPC_TYPE_NAME == 'Meeting Room' && id1 == 'Seat Type') {
                        text = marker.SPC_TYPE_NAME.fontcolor("seagreen");
                        fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                    }
                    else {
                        fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                    }
                }
            }
            else {
                if ($scope.LibertyChanges.SYSP_VAL1 == 1 && $scope.marker.SHIFT_TYPE == 4 && $scope.marker.SPC_TYPE_NAME == 'Meeting Room' && id1 == 'Seat Type') {
                    text = $scope.marker.SPC_TYPE_NAME.fontcolor("seagreen");
                    fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                }
                else {
                    fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                }

            }
        }

        fulltext += '</table>';

        return fulltext;
    }

    $scope.AllocateSeats = function () {
        if ($scope.gridoptions1 === true) {
            if (!$scope.ActiveButton) {
                $scope.ActiveButton = true;
                progress(0, 'Loading...', true);
                var count = 0;
                var flag = false;
                $scope.FinalSelectedSpaces = [];
                var data = $scope.Shifts.filter(function (item) {
                    return !_.find($scope.selectedSpaces, { SPACE_TYPE: item.SH_SEAT_TYPE });
                });

                var check = $scope.selectedSpaces.filter(function (item) {
                    return _.find(data, { SH_CODE: item.SH_CODE });
                });
                //var valueArr = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });
                var valueArr = _.filter($scope.selectedSpaces, function (o) { return o.ticked == true; }).map(function (x) { return x.AUR_ID; });
                var isDuplicate = valueArr.some(function (item, idx) {
                    if (item != '')
                        return valueArr.indexOf(item) != idx
                });
                //console.log(moment((value.FROM_DATE).add('days', 2));


                var AllData = $scope.selectedSpaces.map(function (item) { return item.AUR_ID });
                if ($scope.selectedSpaces[0].SPACE_TYPE != 4 && $scope.selectedSpaces[0].SPACE_TYPE != 5) {
                    angular.forEach($scope.selectedSpaces, function (value, key) {

                        if (value.ticked == true) {
                            if (value.VERTICAL == undefined || value.VERTICAL == "") {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                                return;
                            }
                            else if (value.FROM_DATE == undefined) {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select "From Date"');
                                return;
                            }
                            else if (value.TO_DATE == undefined) {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select "To Date"');
                                return;
                            }
                            else if (new Date(value.FROM_DATE) > new Date(value.TO_DATE)) {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select "To Date" greater than From Date For ' + value.SPC_ID);
                                return;
                            }
                            else if (value.SH_CODE == undefined || value.SH_CODE == '' || value.SH_NAME == '') {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select Shift');
                                return;
                            }
                            //  else if (value.AUR_ID == undefined || value.AUR_ID == '' || value.AUR_KNOWN_AS == '') {
                            //      progress(0, '', false);
                            //     flag = true;
                            //     showNotification('error', 8, 'bottom-right', 'Select employee');
                            //    return;
                            // }
                            else {
                                $scope.FinalSelectedSpaces.push(value);
                                count = count + 1;
                            }
                        }
                    });
                }

                else {
                    angular.forEach($scope.selectedSpaces, function (value, key) {
                        if (value.ticked == true) {
                            if (value.VERTICAL == undefined || value.VERTICAL == "") {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                                return;
                            }
                            if (value.FROM_DATE == undefined) {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select "From Date"');
                                return;
                            }
                            else if (value.TO_DATE == undefined) {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select "To Date"');
                                return;
                            }
                            else if (new Date(value.FROM_DATE) > new Date(value.TO_DATE)) {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', '"To Date" should be greater than From Date' + value.SPC_ID);
                                return;
                            }
                            else if (value.FROM_TIME == undefined || value.FROM_TIME == '') {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select From Time');
                                return;
                            }
                            else if (value.TO_TIME == undefined || value.TO_TIME == '') {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select To Time');
                                return;
                            }
                            else if (value.AUR_NAME == undefined || value.AUR_NAME == "") {
                                progress(0, '', false);
                                flag = true;
                                $scope.ActiveButton = false;
                                showNotification('error', 8, 'bottom-right', 'Select employee');
                                return;
                            }
                            else {
                                $scope.FinalSelectedSpaces.push(value);
                                count = count + 1;
                            }
                        }
                    });
                }
                if (flag) {
                    return;
                }
                if (count == 0) {
                    $scope.ActiveButton = false;
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Select employee for allocation');
                    return;
                }

                if ($scope.LibertyChanges.SYSP_VAL1 == 1) {


                    var role = _.find($scope.GetRoleAndRM);
                    if (role.AUR_ROLE == 6) {
                        $scope.FinalSelectedSpaces[0].SSA_SRNREQ_ID = '';
                    }

                    $scope.FinalSelectedSpaces[0].SSAD_SRN_REQ_ID = '';


                    progress(0, '', true);
                    if ($scope.Remaining <= 0) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Booking limit for this week has been reached!');
                        $scope.ActiveButton = false;
                        $('#SeatAllocation').modal('hide');
                        return;
                    }
                    else {
                        var BookingDateFlag = true;
                        _.forEach($scope.EmpAllocSeatsOtherDay, function (key, value) {

                            var from = new Date(key.FROM_DATE);
                            var to = new Date(key.TO_DATE);
                            var fromcheck = new Date($scope.FinalSelectedSpaces[0].FROM_DATE);
                            var tocheck = new Date($scope.FinalSelectedSpaces[0].TO_DATE);

                            if ((fromcheck >= from && fromcheck <= to) || (tocheck >= from && tocheck <= to)) {
                                progress(0, '', false);
                                BookingDateFlag = false;

                            }

                            if (key.SPC_ID == $scope.FinalSelectedSpaces[0].SPC_ID) {

                            }

                        });
                        //if (!BookingDateFlag && $scope.EmpAllocSeats[0].SST_NAME == $scope.FinalSelectedSpaces[0].SST_NAME) {
                        if (!BookingDateFlag) {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Dates chosen are already scheduled');
                            $scope.ActiveButton = false;
                            return;
                        }

                        var fromdate = moment(new Date($scope.FinalSelectedSpaces[0].FROM_DATE)).add(($scope.Remaining - 1), 'day').format('MM/DD/YYYY');
                        var todate = moment(new Date($scope.FinalSelectedSpaces[0].TO_DATE)).format('MM/DD/YYYY');
                        if (new Date(fromdate) < new Date(todate)) {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Your available booking limit this week is ' + $scope.Remaining);
                            $scope.ActiveButton = false;
                            return;
                        }
                    }

                }

                //if (check.length > 0) {
                //    progress(0, '', false);
                //    flag = true;
                //    showNotification('error', 8, 'bottom-right', "Please Select Valid Shift");
                //    return;
                //}


                var gridAlldata = $scope.selectedSpaces //$scope.gridSpaceAllocOptions.rowData

                if (isDuplicate == true) {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', "Employees shouldn't be the same");
                    $scope.ActiveButton = false;
                    return;
                }

                $scope.selectSeatCount = 0;

                var approvArr = [];
                if ($scope.Manager_Approval && $scope.Manager_Approval.SYSP_VAL1 == 1) {
                    var allobool = true;
                    if ($scope.Manager_Approval.SYSP_VAL1 == 1) {
                        var arr = [];
                        arr.push({ DATEDIFF: $scope.Manager_Approval.SYSP_OPR })
                    }



                    var role = _.find($scope.GetRoleAndRM);

                    if ($scope.selectedSpaces[0].SPACE_TYPE != 4) {
                        if (role.AUR_ROLE == 6) {
                            for (var j in arr) {
                                var Time = (new Date($scope.FinalSelectedSpaces[0].TO_DATE).getTime() - new Date($scope.FinalSelectedSpaces[0].FROM_DATE).getTime());
                                var Days = (Time / (1000 * 3600 * 24)) + 1; //Diference in Days
                                if (Days > arr[j].DATEDIFF) {
                                    approvArr.push($scope.FinalSelectedSpaces[0]);
                                    $scope.FinalSelectedSpaces.splice(0, 1);
                                    $scope.ActiveButton = false;
                                }
                            }
                        }
                    }
                    else {
                        //if ($scope.flipkartempchns) {
                        //    if (!(((frmtDt(new Date(fromdate), 'Date') == frmtDt(new Date(), 'Date')) || (frmtDt(new Date(), 'Date+1') == fromdate)) && fromdate == todate)) {
                        //        $scope.ActiveButton = false;
                        //        progress(0, 'Loading...', false);
                        //        $scope.selectedSpaces = [];
                        //        $scope.FlipkartVaccinateName = 'Hot-desking seats are allocated only for one day – either on the day of booking or the next day.';
                        //        return;
                        //    }
                        //}

                        //if (!(moment($scope.FinalSelectedSpaces[0].TO_DATE).format('MM/DD/YYYY') == moment($scope.FinalSelectedSpaces[0].FROM_DATE).format('MM/DD/YYYY'))) {
                        //    $scope.ActiveButton = false;
                        //    progress(0, 'Loading...', false);
                        //    //$scope.selectedSpaces = [];
                        //    $scope.FlipkartVaccinateName = 'Hot-desking seats are allocated only for one day.';
                        //    return;
                        //}


                    }


                    if (approvArr.length > 0) {
                        progress(0, 'Loading...', false);

                        if ($scope.Manager_Approval.SYSP_VAL1 == 1)
                            var FlipkartVaccinateName = 'Seat Requisition Raised Sucessfully';
                        else
                            var FlipkartVaccinateName = 'Hybrid Policy restricts your presence in office to 3 days a week - please seek your manager’s approval to book for more than 3 days';
                        this.ticked = true;
                        $ngConfirm({
                            icon: 'fa fa-warning',
                            title: 'Restricted for booking',
                            content: FlipkartVaccinateName,
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            buttons: {
                                Ok: function ($scope, button) {
                                    progress(0, 'Loading...', true);
                                    MaploaderService.allocateSeatsValidation(approvArr).then(function (response1) {
                                        var bool = false;
                                        for (var i in response1.data) {
                                            if (response1.data[i].SH_CODE == 'MANAGER APPROVAL') {
                                                $scope.ActiveButton = false;
                                                progress(0, 'Loading...', false);
                                                bool = true;
                                            }
                                        }
                                        if (bool) {
                                            progress(0, '', false);
                                            showNotification('error', 8, 'bottom-right', 'Dates chosen are already scheduled');
                                            $scope.ActiveButton = false;
                                        }
                                        if (!bool && response1.data == null) {
                                            if ($scope.flipkartempchns) {
                                                for (var i in approvArr) {
                                                    if (approvArr[0].SPACE_TYPE == 4 || approvArr[0].SPACE_TYPE == 5) {
                                                        datevalidation(gridAlldata);
                                                    }
                                                }
                                            } else {
                                                datevalidation(gridAlldata);
                                            }
                                            MaploaderService.approvalallocateSeats(approvArr).then(function (response) {
                                                if (response.data != null) {
                                                    progress(0, 'Loading...', false);
                                                    $scope.selectedSpaces = [];
                                                    setTimeout(function () {
                                                        showNotification('success', 8, 'bottom-right', response.data[0].SPC_ID + '...');
                                                        $scope.ActiveButton = false;
                                                        $scope.FlipkartVaccinateName = '';
                                                        $('#SeatAllocation').modal('hide');
                                                        //$scope.gridSpaceAllocOptions.api.refreshView();
                                                    }, 1000);

                                                }
                                                else {
                                                    $scope.ActiveButton = false;
                                                    progress(0, 'Loading...', false);
                                                    $scope.FlipkartVaccinateName = '';
                                                    $('#SeatAllocation').modal('hide');
                                                    $scope.selectedSpaces = [];
                                                    $scope.gridSpaceAllocOptions.api.refreshView();
                                                    showNotification('error', 8, 'bottom-right', 'error');
                                                }
                                            });
                                        }
                                        else if (!bool) {
                                            $scope.ActiveButton = false;
                                            progress(0, 'Loading...', false);
                                            $scope.FlipkartVaccinateName = '';
                                            $('#SeatAllocation').modal('hide');
                                            $scope.selectedSpaces = [];
                                            //$scope.gridSpaceAllocOptions.api.refreshView();
                                        }
                                    });
                                },
                                close: function ($scope, button) {
                                    setTimeout(function () {
                                        $scope.$apply(function () {
                                            $scope.FlipkartVaccinateName = '';
                                            progress(0, 'Loading...', false);
                                            $scope.ActiveButton = false;
                                        });
                                    });
                                }
                            }
                        });
                    }
                }

                if ($scope.FinalSelectedSpaces.length > 0) {
                    if ($scope.AtikinsMulBooking && $scope.AtikinsMulBooking.SYSP_VAL1 == 1) {
                        MaploaderService.allocateSeatsValidation($scope.FinalSelectedSpaces).then(function (response1) {
                            console.log(response1);
                            if (response1.data == null) {
                                // Atkins 04082021//
                                if ($scope.AtikinsMulBooking && $scope.AtikinsMulBooking.SYSP_VAL1 == 1) {
                                    if ($scope.FinalSelectedSpaces.length > 0) {
                                        var arr = [], bool = false;
                                        for (var i = 0; i < gridAlldata.length; i++) {
                                            // if (gridAlldata[i].SSA_SRNREQ_ID.length == 0)
                                            arr.push({
                                                FROM_DATE: gridAlldata[i].FROM_DATE, TO_DATE: gridAlldata[i].TO_DATE, AUR_ID: gridAlldata[i].AUR_ID,
                                                SH_CODE: gridAlldata[i].SH_CODE, FROM_TIME: gridAlldata[i].FROM_TIME, TO_TIME: gridAlldata[i].TO_TIME
                                            });
                                        }

                                        for (var j in arr) {
                                            for (var x = 0; x < gridAlldata.length; x++) {

                                                if (arr[j].AUR_ID != gridAlldata[x].AUR_ID) {

                                                    var T_1 = parseInt(arr[j].FROM_TIME.split(":"));
                                                    var T_2 = parseInt(arr[j].TO_TIME.split(":"));
                                                    var T_3 = parseInt(gridAlldata[x].FROM_TIME.split(":"));
                                                    var T_4 = parseInt(gridAlldata[x].TO_TIME.split(":"));

                                                    var D_1 = arr[j].FROM_DATE.split("/");
                                                    var D_2 = arr[j].TO_DATE.split("/");
                                                    var D_3 = gridAlldata[x].FROM_DATE.split("/");
                                                    var D_4 = gridAlldata[x].TO_DATE.split("/");

                                                    var d1 = new Date(D_1[2], parseInt(D_1[1]) - 1, D_1[0]);
                                                    var d2 = new Date(D_2[2], parseInt(D_2[1]) - 1, D_2[0]);
                                                    var d3 = new Date(D_3[2], parseInt(D_3[1]) - 1, D_3[0]);
                                                    var d4 = new Date(D_4[2], parseInt(D_4[1]) - 1, D_4[0]);
                                                    //(gridAlldata[x].SH_CODE == arr[j].SH_CODE)
                                                    if (((d3 >= d1 && d3 <= d2) || (d4 >= d1 && d4 <= d2)) && (T_3 >= T_1 && T_3 <= T_2) || (T_4 >= T_1 && T_4 <= T_2)) {
                                                        progress(0, '', false);
                                                        showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                                                        $scope.ActiveButton = false;
                                                        return;
                                                    }
                                                    if (((d1 >= d3 && d1 <= d4) || (d2 >= d3 && d2 <= d4)) && (T_1 >= T_3 && T_1 <= T_4) || (T_2 >= T_3 && T_2 <= T_4)) {
                                                        progress(0, '', false);
                                                        showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                                                        $scope.ActiveButton = false;
                                                        return;
                                                    }
                                                }

                                            }
                                        }

                                        MaploaderService.allocateSeats($scope.FinalSelectedSpaces).then(function (response) {
                                            if (response.data != null) {
                                                angular.forEach(response.data, function (value, key) {
                                                    var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                                                    spcid.STATUS = value.STATUS;
                                                    spcid.VERTICAL = value.VERTICAL;
                                                    spcid.Cost_Center_Name = value.Cost_Center_Name;
                                                    spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                                                    spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                                                    spcid.ticked = false;
                                                    spcid.SHIFT_TYPE = SeattypeVal(value.SHIFT_TYPE);
                                                    spcid.SSAD_AUR_ID = value.emp;
                                                    spcid.SPC_DESC = value.AUR_ID;

                                                    var formattedString = value.AUR_ID.split(",").join("\n");
                                                    var k = formattedString.split("\n");
                                                    var id = "";
                                                    var id1 = "";
                                                    var text = "";
                                                    var fulltext = "";
                                                    var str = " ";

                                                    fulltext = BindMarkerText(formattedString, spcid);


                                                    switch (value.STATUS) {
                                                        case 1: spcid.setStyle(VacantStyle);
                                                            break;
                                                        case 1002: spcid.setStyle(AllocateStyle);
                                                            break;
                                                        case 1004:
                                                            if ($scope.PartiallyOccupiedSys.SYSP_VAL1 == 1) {
                                                                if (value.COUNTS == 2)
                                                                    spcid.setStyle(OccupiedStyle);
                                                                else
                                                                    spcid.setStyle(PartiallyOccupiedStyle);
                                                            }
                                                            else {
                                                                spcid.setStyle(OccupiedStyle);
                                                            }
                                                            break;
                                                        case 1003: spcid.setStyle(AllocateStyle);
                                                            break;
                                                        case 1052: spcid.setStyle(BlockStyle);
                                                            break;
                                                        default: spcid.setStyle(VacantStyle);
                                                            break;
                                                    }
                                                    spcid.bindLabel(fulltext.split(",").join("\n"), {
                                                        noHide: false,
                                                        direction: 'auto',
                                                        className: "my-label",
                                                        zIndexOffset: 2000
                                                    });//.setOpacity(1);


                                                    $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                                        if (layer.options.spaceid == value.SPC_ID) {
                                                            layer.options.seattype = value.SHIFT_TYPE;
                                                        }
                                                    });

                                                    $scope.loadThemes();

                                                });
                                                $('#SeatAllocation').modal('hide');
                                                $scope.selectedSpaces = [];
                                                updateSummaryCount();
                                                $scope.gridSpaceAllocOptions.api.refreshView();
                                                progress(0, '', false);
                                                if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                                                    $scope.GradeAllocStatus();
                                                }

                                                setTimeout(function () {
                                                    showNotification('success', 8, 'bottom-right', 'Selected Spaces Successfully Allocated ');
                                                    $scope.ActiveButton = false;
                                                }, 1000);
                                                reloadMarkers();


                                            }
                                            else {
                                                progress(0, '', false);
                                                showNotification('error', 8, 'bottom-right', response.Message);
                                                $scope.ActiveButton = false;
                                            }
                                            if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1)
                                                $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
                                        });
                                        //}
                                    }
                                }
                                //end//
                            }
                            else {
                                progress(0, '', false);
                                flag = true;
                                $scope.AllocateEnabledStatus = true;
                                $scope.ReleaseFrom = false;
                                $scope.empDateArr = angular.copy(response1.data);
                                return;
                            }
                        });
                    } else {
                        MaploaderService.allocateSeats($scope.FinalSelectedSpaces).then(function (response) {
                            if (response.data != null) {
                                angular.forEach(response.data, function (value, key) {
                                    var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                                    spcid.STATUS = value.STATUS;
                                    spcid.VERTICAL = value.VERTICAL;
                                    spcid.Cost_Center_Name = value.Cost_Center_Name;
                                    spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                                    spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                                    spcid.ticked = false;
                                    spcid.SHIFT_TYPE = value.SHIFT_TYPE;
                                    spcid.SSAD_AUR_ID = value.emp;
                                    spcid.SPC_DESC = value.AUR_ID;

                                    var formattedString = value.AUR_ID.split(",").join("\n");
                                    var k = formattedString.split("\n");
                                    var id = "";
                                    var id1 = "";
                                    var text = "";
                                    var fulltext = "";
                                    var str = " ";

                                    fulltext = BindMarkerText(formattedString, spcid);


                                    switch (value.STATUS) {
                                        case 1: spcid.setStyle(VacantStyle);
                                            break;
                                        case 1002: spcid.setStyle(AllocateStyle);
                                            break;
                                        case 1004: spcid.setStyle(OccupiedStyle);
                                            break;
                                        case 1003: spcid.setStyle(AllocateStyle);
                                            break;
                                        case 1052: spcid.setStyle(BlockStyle);
                                            break;
                                        default: spcid.setStyle(VacantStyle);
                                            break;
                                    }
                                    spcid.bindLabel(fulltext.split(",").join("\n"), {
                                        noHide: false,
                                        direction: 'auto',
                                        className: "my-label",
                                        zIndexOffset: 2000
                                    });//.setOpacity(1);


                                    $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                        if (layer.options.spaceid == value.SPC_ID) {
                                            layer.options.seattype = value.SHIFT_TYPE;
                                        }
                                    });

                                    $scope.loadThemes();

                                });
                                $('#SeatAllocation').modal('hide');
                                $scope.selectedSpaces = [];
                                updateSummaryCount();
                                $scope.gridSpaceAllocOptions.api.refreshView();
                                progress(0, '', false);
                                if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                                    $scope.GradeAllocStatus();
                                }

                                setTimeout(function () {
                                    showNotification('success', 8, 'bottom-right', 'Selected Spaces Successfully Allocated ');
                                    $scope.ActiveButton = false;
                                }, 1000);
                                reloadMarkers();


                            }
                            else {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', response.Message);
                                $scope.ActiveButton = false;
                            }
                            if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1)
                                $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
                        });
                    }
                }
                //end
            }

        }
        if (!$scope.ActiveButton) {
            $scope.ActiveButton = true;
            progress(0, 'Loading...', true);
            var count = 0;
            var flag = false;
            $scope.FinalSelectedSpaces = [];
            var data = $scope.Shifts.filter(function (item) {
                return !_.find($scope.gridSpaceAllocOptions.rowData, { SPACE_TYPE: item.SH_SEAT_TYPE });
            });

            var check = $scope.gridSpaceAllocOptions.rowData.filter(function (item) {
                return _.find(data, { SH_CODE: item.SH_CODE });
            });
            //var valueArr = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });
            var valueArr = _.filter($scope.gridSpaceAllocOptions.rowData, function (o) { return o.ticked == true; }).map(function (x) { return x.AUR_ID; });
            var isDuplicate = valueArr.some(function (item, idx) {
                if (item != '')
                    return valueArr.indexOf(item) != idx
            });
            //console.log(moment((value.FROM_DATE).add('days', 2));


            var AllData = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });
            if ($scope.selectedSpaces[0].SPACE_TYPE != 4 && $scope.selectedSpaces[0].SPACE_TYPE != 5) {
                angular.forEach($scope.selectedSpaces, function (value, key) {

                    if (value.ticked == true) {
                        if (value.VERTICAL == undefined || value.VERTICAL == "") {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                            return;
                        }
                        else if (value.FROM_DATE == undefined) {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select "From Date"');
                            return;
                        }
                        else if (value.TO_DATE == undefined) {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select "To Date"');
                            return;
                        }
                        else if (new Date(value.FROM_DATE) > new Date(value.TO_DATE)) {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select "To Date" greater than From Date For ' + value.SPC_ID);
                            return;
                        }
                        else if (value.SH_CODE == undefined || value.SH_CODE == '' || value.SH_NAME == '') {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select Shift');
                            return;
                        }
                        //  else if (value.AUR_ID == undefined || value.AUR_ID == '' || value.AUR_KNOWN_AS == '') {
                        //      progress(0, '', false);
                        //     flag = true;
                        //     showNotification('error', 8, 'bottom-right', 'Select employee');
                        //    return;
                        // }
                        else {
                            $scope.FinalSelectedSpaces.push(value);
                            count = count + 1;
                        }
                    }
                });
            }

            else {
                angular.forEach($scope.selectedSpaces, function (value, key) {
                    if (value.ticked == true) {
                        if (value.VERTICAL == undefined || value.VERTICAL == "") {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                            return;
                        }
                        if (value.FROM_DATE == undefined) {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select "From Date"');
                            return;
                        }
                        else if (value.TO_DATE == undefined) {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select "To Date"');
                            return;
                        }
                        else if (new Date(value.FROM_DATE) > new Date(value.TO_DATE)) {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', '"To Date" should be greater than From Date' + value.SPC_ID);
                            return;
                        }
                        else if (value.FROM_TIME == undefined || value.FROM_TIME == '') {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select From Time');
                            return;
                        }
                        else if (value.TO_TIME == undefined || value.TO_TIME == '') {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select To Time');
                            return;
                        }
                        else if (value.AUR_NAME == undefined || value.AUR_NAME == "") {
                            progress(0, '', false);
                            flag = true;
                            $scope.ActiveButton = false;
                            showNotification('error', 8, 'bottom-right', 'Select employee');
                            return;
                        }
                        else {
                            $scope.FinalSelectedSpaces.push(value);
                            count = count + 1;
                        }
                    }
                });
            }
            if (flag) {
                return;
            }
            if (count == 0) {
                $scope.ActiveButton = false;
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Select employee for allocation');
                return;
            }

            if ($scope.LibertyChanges.SYSP_VAL1 == 1) {


                var role = _.find($scope.GetRoleAndRM);
                if (role.AUR_ROLE == 6) {
                    $scope.FinalSelectedSpaces[0].SSA_SRNREQ_ID = '';
                }

                $scope.FinalSelectedSpaces[0].SSAD_SRN_REQ_ID = '';


                progress(0, '', true);
                if ($scope.Remaining <= 0) {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Booking limit for this week has been reached!');
                    $scope.ActiveButton = false;
                    $('#SeatAllocation').modal('hide');
                    return;
                }
                else {
                    var BookingDateFlag = true;
                    _.forEach($scope.EmpAllocSeatsOtherDay, function (key, value) {
                        
                        var from = new Date(key.FROM_DATE);
                        var to = new Date(key.TO_DATE);
                        var fromcheck = new Date($scope.FinalSelectedSpaces[0].FROM_DATE);
                        var tocheck = new Date($scope.FinalSelectedSpaces[0].TO_DATE);

                        if ((fromcheck >= from && fromcheck <= to) || (tocheck >= from && tocheck <= to)) {
                            progress(0, '', false);
                            BookingDateFlag = false;

                        }

                        if (key.SPC_ID == $scope.FinalSelectedSpaces[0].SPC_ID) {

                        }

                    });
                    //if (!BookingDateFlag && $scope.EmpAllocSeats[0].SST_NAME == $scope.FinalSelectedSpaces[0].SST_NAME) {
                    if (!BookingDateFlag) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Dates chosen are already scheduled');
                        $scope.ActiveButton = false;
                        return;
                    }

                    var fromdate = moment(new Date($scope.FinalSelectedSpaces[0].FROM_DATE)).add(($scope.Remaining - 1), 'day').format('MM/DD/YYYY');
                    var todate = moment(new Date($scope.FinalSelectedSpaces[0].TO_DATE)).format('MM/DD/YYYY');
                    if (new Date(fromdate) < new Date(todate)) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Your available booking limit this week is ' + $scope.Remaining);
                        $scope.ActiveButton = false;
                        return;
                    }
                }

            }

            //if (check.length > 0) {
            //    progress(0, '', false);
            //    flag = true;
            //    showNotification('error', 8, 'bottom-right', "Please Select Valid Shift");
            //    return;
            //}


            var gridAlldata = $scope.selectedSpaces //$scope.gridSpaceAllocOptions.rowData

            if (isDuplicate == true) {
                progress(0, '', false);
                flag = true;
                showNotification('error', 8, 'bottom-right', "Employees shouldn't be the same");
                $scope.ActiveButton = false;
                return;
            }
			
            $scope.selectSeatCount = 0;

            var approvArr = [];
            if ($scope.Manager_Approval && $scope.Manager_Approval.SYSP_VAL1 == 1) {
                var allobool = true;
                if ($scope.Manager_Approval.SYSP_VAL1 == 1) {
                    var arr = [];
                    arr.push({ DATEDIFF: $scope.Manager_Approval.SYSP_OPR })
                }



                var role = _.find($scope.GetRoleAndRM);

                if ($scope.selectedSpaces[0].SPACE_TYPE != 4) {
                    if (role.AUR_ROLE == 6) {
                        for (var j in arr) {
                            var Time = (new Date($scope.FinalSelectedSpaces[0].TO_DATE).getTime() - new Date($scope.FinalSelectedSpaces[0].FROM_DATE).getTime());
                            var Days = (Time / (1000 * 3600 * 24)) + 1; //Diference in Days
                            if (Days > arr[j].DATEDIFF) {
                                approvArr.push($scope.FinalSelectedSpaces[0]);
                                $scope.FinalSelectedSpaces.splice(0, 1);
                                $scope.ActiveButton = false;
                            }
                        }
                    }
                }
                else {
                    //if ($scope.flipkartempchns) {
                    //    if (!(((frmtDt(new Date(fromdate), 'Date') == frmtDt(new Date(), 'Date')) || (frmtDt(new Date(), 'Date+1') == fromdate)) && fromdate == todate)) {
                    //        $scope.ActiveButton = false;
                    //        progress(0, 'Loading...', false);
                    //        $scope.selectedSpaces = [];
                    //        $scope.FlipkartVaccinateName = 'Hot-desking seats are allocated only for one day – either on the day of booking or the next day.';
                    //        return;
                    //    }
                    //}

                    //if (!(moment($scope.FinalSelectedSpaces[0].TO_DATE).format('MM/DD/YYYY') == moment($scope.FinalSelectedSpaces[0].FROM_DATE).format('MM/DD/YYYY'))) {
                    //    $scope.ActiveButton = false;
                    //    progress(0, 'Loading...', false);
                    //    //$scope.selectedSpaces = [];
                    //    $scope.FlipkartVaccinateName = 'Hot-desking seats are allocated only for one day.';
                    //    return;
                    //}


                }


                if (approvArr.length > 0) {
                    progress(0, 'Loading...', false);

                    if ($scope.Manager_Approval.SYSP_VAL1 == 1)
                        var FlipkartVaccinateName = 'Seat Requisition Raised Sucessfully';
                    else
                        var FlipkartVaccinateName = 'Hybrid Policy restricts your presence in office to 3 days a week - please seek your manager’s approval to book for more than 3 days';
                    this.ticked = true;
                    $ngConfirm({
                        icon: 'fa fa-warning',
                        title: 'Restricted for booking',
                        content: FlipkartVaccinateName,
                        closeIcon: true,
                        closeIconClass: 'fa fa-close',
                        buttons: {
                            Ok: function ($scope, button) {
                                progress(0, 'Loading...', true);
                                MaploaderService.allocateSeatsValidation(approvArr).then(function (response1) {
                                    var bool = false;
                                    for (var i in response1.data) {
                                        if (response1.data[i].SH_CODE == 'MANAGER APPROVAL') {
                                            $scope.ActiveButton = false;
                                            progress(0, 'Loading...', false);
                                            bool = true;
                                        }
                                    }
                                    if (bool) {
                                        progress(0, '', false);
                                        showNotification('error', 8, 'bottom-right', 'Dates chosen are already scheduled');
                                        $scope.ActiveButton = false;
                                    }
                                    if (!bool && response1.data == null) {
                                        if ($scope.flipkartempchns) {
                                            for (var i in approvArr) {
                                                if (approvArr[0].SPACE_TYPE == 4 || approvArr[0].SPACE_TYPE == 5) {
                                                    datevalidation(gridAlldata);
                                                }
                                            }
                                        } else {
                                            datevalidation(gridAlldata);
                                        }
                                        MaploaderService.approvalallocateSeats(approvArr).then(function (response) {
                                            if (response.data != null) {
                                                progress(0, 'Loading...', false);
                                                $scope.selectedSpaces = [];
                                                setTimeout(function () {
                                                    showNotification('success', 8, 'bottom-right', response.data[0].SPC_ID + '...');
                                                    $scope.ActiveButton = false;
                                                    $scope.FlipkartVaccinateName = '';
                                                    $('#SeatAllocation').modal('hide');
                                                    //$scope.gridSpaceAllocOptions.api.refreshView();
                                                }, 1000);

                                            }
                                            else {
                                                $scope.ActiveButton = false;
                                                progress(0, 'Loading...', false);
                                                $scope.FlipkartVaccinateName = '';
                                                $('#SeatAllocation').modal('hide');
                                                $scope.selectedSpaces = [];
                                                $scope.gridSpaceAllocOptions.api.refreshView();
                                                showNotification('error', 8, 'bottom-right', 'error');
                                            }
                                        });
                                    }
                                    else if (!bool) {
                                        $scope.ActiveButton = false;
                                        progress(0, 'Loading...', false);
                                        $scope.FlipkartVaccinateName = '';
                                        $('#SeatAllocation').modal('hide');
                                        $scope.selectedSpaces = [];
                                        //$scope.gridSpaceAllocOptions.api.refreshView();
                                    }
                                });
                            },
                            close: function ($scope, button) {
                                setTimeout(function () {
                                    $scope.$apply(function () {
                                        $scope.FlipkartVaccinateName = '';
                                        progress(0, 'Loading...', false);
                                        $scope.ActiveButton = false;
                                    });
                                });
                            }
                        }
                    });
                }
            }

            if ($scope.FinalSelectedSpaces.length > 0) {
                if ($scope.LibertyChanges && $scope.LibertyChanges.SYSP_VAL1==1) {
                    MaploaderService.allocateSeatsValidation($scope.FinalSelectedSpaces).then(function (response) {
                        if (response.data != null) {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                            $scope.ActiveButton = false;
                            return;
                        } 
                    })
                }
                if ($scope.AtikinsMulBooking && $scope.AtikinsMulBooking.SYSP_VAL1 == 1) {
                    MaploaderService.allocateSeatsValidation($scope.FinalSelectedSpaces).then(function (response1) {
                        if (response1.data == null) {
                            // Atkins 04082021//
                            if ($scope.AtikinsMulBooking && $scope.AtikinsMulBooking.SYSP_VAL1 == 1) {
                                if ($scope.FinalSelectedSpaces.length > 0) {
                                    var arr = [], bool = false;
                                    for (var i = 0; i < gridAlldata.length; i++) {
                                        // if (gridAlldata[i].SSA_SRNREQ_ID.length == 0)
                                        arr.push({
                                            FROM_DATE: gridAlldata[i].FROM_DATE, TO_DATE: gridAlldata[i].TO_DATE, AUR_ID: gridAlldata[i].AUR_ID,
                                            SH_CODE: gridAlldata[i].SH_CODE, FROM_TIME: gridAlldata[i].FROM_TIME, TO_TIME: gridAlldata[i].TO_TIME
                                        });
                                    }

                                    for (var j in arr) {
                                        for (var x = 0; x < gridAlldata.length; x++) {

                                            if (arr[j].AUR_ID != gridAlldata[x].AUR_ID) {

                                                var T_1 = parseInt(arr[j].FROM_TIME.split(":"));
                                                var T_2 = parseInt(arr[j].TO_TIME.split(":"));
                                                var T_3 = parseInt(gridAlldata[x].FROM_TIME.split(":"));
                                                var T_4 = parseInt(gridAlldata[x].TO_TIME.split(":"));

                                                var D_1 = arr[j].FROM_DATE.split("/");
                                                var D_2 = arr[j].TO_DATE.split("/");
                                                var D_3 = gridAlldata[x].FROM_DATE.split("/");
                                                var D_4 = gridAlldata[x].TO_DATE.split("/");

                                                var d1 = new Date(D_1[2], parseInt(D_1[1]) - 1, D_1[0]);
                                                var d2 = new Date(D_2[2], parseInt(D_2[1]) - 1, D_2[0]);
                                                var d3 = new Date(D_3[2], parseInt(D_3[1]) - 1, D_3[0]);
                                                var d4 = new Date(D_4[2], parseInt(D_4[1]) - 1, D_4[0]);
                                                //(gridAlldata[x].SH_CODE == arr[j].SH_CODE)
                                                if (((d3 >= d1 && d3 <= d2) || (d4 >= d1 && d4 <= d2)) && (T_3 >= T_1 && T_3 <= T_2) || (T_4 >= T_1 && T_4 <= T_2)) {
                                                    progress(0, '', false);
                                                    showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                                                    $scope.ActiveButton = false;
                                                    return;
                                                }
                                                if (((d1 >= d3 && d1 <= d4) || (d2 >= d3 && d2 <= d4)) && (T_1 >= T_3 && T_1 <= T_4) || (T_2 >= T_3 && T_2 <= T_4)) {
                                                    progress(0, '', false);
                                                    showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                                                    $scope.ActiveButton = false;
                                                    return;
                                                }
                                            }

                                        }
                                    }

                                    MaploaderService.allocateSeats($scope.FinalSelectedSpaces).then(function (response) {
                                        if (response.data != null) {
                                            angular.forEach(response.data, function (value, key) {
                                                var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                                                spcid.STATUS = value.STATUS;
                                                spcid.VERTICAL = value.VERTICAL;
                                                spcid.Cost_Center_Name = value.Cost_Center_Name;
                                                spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                                                spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                                                spcid.ticked = false;
                                                spcid.SHIFT_TYPE = SeattypeVal(value.SHIFT_TYPE);
                                                spcid.SSAD_AUR_ID = value.emp;
                                                spcid.SPC_DESC = value.AUR_ID;

                                                var formattedString = value.AUR_ID.split(",").join("\n");
                                                var k = formattedString.split("\n");
                                                var id = "";
                                                var id1 = "";
                                                var text = "";
                                                var fulltext = "";
                                                var str = " ";

                                                fulltext = BindMarkerText(formattedString, spcid);


                                                switch (value.STATUS) {
                                                    case 1: spcid.setStyle(VacantStyle);
                                                        break;
                                                    case 1002: spcid.setStyle(AllocateStyle);
                                                        break;
                                                    case 1004:
                                                        if ($scope.PartiallyOccupiedSys.SYSP_VAL1 == 1) {
                                                            if (value.COUNTS == 2)
                                                                spcid.setStyle(OccupiedStyle);
                                                            else
                                                                spcid.setStyle(PartiallyOccupiedStyle);
                                                        }
                                                        else {
                                                            spcid.setStyle(OccupiedStyle);
                                                        }
                                                        break;
                                                    case 1003: spcid.setStyle(AllocateStyle);
                                                        break;
                                                    case 1052: spcid.setStyle(BlockStyle);
                                                        break;
                                                    default: spcid.setStyle(VacantStyle);
                                                        break;
                                                }
                                                spcid.bindLabel(fulltext.split(",").join("\n"), {
                                                    noHide: false,
                                                    direction: 'auto',
                                                    className: "my-label",
                                                    zIndexOffset: 2000
                                                });//.setOpacity(1);


                                                $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                                    if (layer.options.spaceid == value.SPC_ID) {
                                                        layer.options.seattype = value.SHIFT_TYPE;
                                                    }
                                                });

                                                $scope.loadThemes();

                                            });
                                            $('#SeatAllocation').modal('hide');
                                            $scope.selectedSpaces = [];
                                            updateSummaryCount();
                                            $scope.gridSpaceAllocOptions.api.refreshView();
                                            progress(0, '', false);
                                            if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                                                $scope.GradeAllocStatus();
                                            }

                                            setTimeout(function () {
                                                showNotification('success', 8, 'bottom-right', 'Selected Spaces Successfully Allocated ');
                                                $scope.ActiveButton = false;
                                            }, 1000);

                                            reloadMarkers();

                                        }
                                        else {
                                            progress(0, '', false);
                                            showNotification('error', 8, 'bottom-right', response.Message);
                                            $scope.ActiveButton = false;
                                        }
                                        if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1)
                                            $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
                                    });
                                    //}
                                }
                            }
                            //end//
                        }
                        else {
                            progress(0, '', false);
                            flag = true;
                            $scope.AllocateEnabledStatus = true;
                            $scope.ReleaseFrom = false;
                            $scope.empDateArr = angular.copy(response1.data);
                            return;
                        }
                    });
                } else {
                    MaploaderService.allocateSeats($scope.FinalSelectedSpaces).then(function (response) {
                        if (response.data != null) {
                            angular.forEach(response.data, function (value, key) {
                                var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                                spcid.STATUS = value.STATUS;
                                spcid.VERTICAL = value.VERTICAL;
                                spcid.Cost_Center_Name = value.Cost_Center_Name;
                                spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                                spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                                spcid.ticked = false;
                                spcid.SHIFT_TYPE =  value.SHIFT_TYPE;
                                spcid.SSAD_AUR_ID = value.emp;
                                spcid.SPC_DESC = value.AUR_ID;

                                var formattedString = value.AUR_ID.split(",").join("\n");
                                var k = formattedString.split("\n");
                                var id = "";
                                var id1 = "";
                                var text = "";
                                var fulltext = "";
                                var str = " ";

                                fulltext = BindMarkerText(formattedString, spcid);


                                switch (value.STATUS) {
                                    case 1: spcid.setStyle(VacantStyle);
                                        break;
                                    case 1002: spcid.setStyle(AllocateStyle);
                                        break;
                                    case 1004: spcid.setStyle(OccupiedStyle);
                                        break;
                                    case 1003: spcid.setStyle(AllocateStyle);
                                        break;
                                    case 1052: spcid.setStyle(BlockStyle);
                                        break;
                                    default: spcid.setStyle(VacantStyle);
                                        break;
                                }
                                spcid.bindLabel(fulltext.split(",").join("\n"), {
                                    noHide: false,
                                    direction: 'auto',
                                    className: "my-label",
                                    zIndexOffset: 2000
                                });//.setOpacity(1);


                                $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                    if (layer.options.spaceid == value.SPC_ID) {
                                        layer.options.seattype = value.SHIFT_TYPE;
                                    }
                                });

                                $scope.loadThemes();

                            });
                            $('#SeatAllocation').modal('hide');
                            $scope.selectedSpaces = [];
                            updateSummaryCount();
                            $scope.gridSpaceAllocOptions.api.refreshView();
                            progress(0, '', false);
                            if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                                $scope.GradeAllocStatus();
                            }

                            setTimeout(function () {
                                showNotification('success', 8, 'bottom-right', 'Selected Spaces Successfully Allocated ');
                                $scope.ActiveButton = false;
                            }, 1000);

                            reloadMarkers();

                        }
                        else {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', response.Message);
                            $scope.ActiveButton = false;
                        }
                        if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1)
                            $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
                    });
                }
            }
            //end
        }
    }

    function datevalidation(gridAlldata) {

        var arr = [], bool = false;
        for (var i = 0; i < gridAlldata.length; i++) {
            arr.push({
                FROM_DATE: gridAlldata[i].FROM_DATE, TO_DATE: gridAlldata[i].TO_DATE, AUR_ID: gridAlldata[i].AUR_ID,
                SH_CODE: gridAlldata[i].SH_CODE, FROM_TIME: gridAlldata[i].FROM_TIME, TO_TIME: gridAlldata[i].TO_TIME
            });
        }

        for (var j in arr) {
            for (var x = 0; x < gridAlldata.length; x++) {

                if (arr[j].AUR_ID != gridAlldata[x].AUR_ID) {

                    var T_1 = (arr[j].FROM_TIM ? parseInt(arr[j].FROM_TIME.split(":")) : '');
                    var T_2 = (arr[j].TO_TIME ? parseInt(arr[j].TO_TIME.split(":")) : '');
                    var T_3 = (gridAlldata[x].FROM_TIME ? parseInt(gridAlldata[x].FROM_TIME.split(":")) : '');
                    var T_4 = (gridAlldata[x].TO_TIME ? parseInt(gridAlldata[x].TO_TIME.split(":")) : '');

                    var D_1 = arr[j].FROM_DATE.split("/");
                    var D_2 = arr[j].TO_DATE.split("/");
                    var D_3 = gridAlldata[x].FROM_DATE.split("/");
                    var D_4 = gridAlldata[x].TO_DATE.split("/");

                    var d1 = new Date(D_1[2], parseInt(D_1[1]) - 1, D_1[0]);
                    var d2 = new Date(D_2[2], parseInt(D_2[1]) - 1, D_2[0]);
                    var d3 = new Date(D_3[2], parseInt(D_3[1]) - 1, D_3[0]);
                    var d4 = new Date(D_4[2], parseInt(D_4[1]) - 1, D_4[0]);
                    if (((d3 >= d1 && d3 <= d2) || (d4 >= d1 && d4 <= d2)) && (T_3 >= T_1 && T_3 <= T_2) || (T_4 >= T_1 && T_4 <= T_2)) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                        $scope.ActiveButton = false;
                        return;
                    }
                    if (((d1 >= d3 && d1 <= d4) || (d2 >= d3 && d2 <= d4)) && (T_1 >= T_3 && T_1 <= T_4) || (T_2 >= T_3 && T_2 <= T_4)) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                        $scope.ActiveButton = false;
                        return;
                    }
                }
            }
        }
    }

    $scope.CloseEmpFn = function () {
        $scope.AllocateEnabledStatus = false;
        $scope.ReleaseFrom = true;
        $scope.ActiveButton = false;
        $scope.empDateArr = [];
    }

    $scope.AllocAnother = function (data) {
        if (data.SPACE_TYPE == 1) {
            showNotification('error', 8, 'bottom-right', "Select maximum of one person for dedicated");
        }
        else {

            var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" });
            if ((prefval == undefined) || prefval.SYSP_VAL1 == undefined) {

                var refdata = {};
                angular.copy(data, refdata);

                prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
                if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
                    if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
                        refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
                        refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
                    }

                    var role = _.find($scope.GetRoleAndRM);
                    $scope.AllocateEnabledStatus = false;
                    if (role.AUR_ROLE == 6) {
                        var emp = _.find($scope.selectedSpaces, function (x) { if (x.AUR_ID == role.AUR_ID) return x });
                        if (emp != undefined) {
                            showNotification('error', 8, 'bottom-right', 'Employee can not allocate for another');
                            return;
                        }
                        //var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                        //if (ver == undefined) {
                        //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                        //    return;
                        //}
                        refdata.AUR_LEAVE_STATUS = "N";
                        //refdata.SSAD_SRN_REQ_ID = null;
                        refdata.STACHECK = 16;
                        refdata.SSA_SRNREQ_ID = '';
                        refdata.AUR_NAME = role.AUR_KNOWN_AS;
                        refdata.AUR_ID = role.AUR_ID;
                        refdata.VER_CODE = role.VER_CODE;
                        refdata.VER_NAME = role.VER_NAME;
                        refdata.Cost_Center_Code = role.COST_CENTER_CODE;
                        refdata.Cost_Center_Name = role.COST_CENTER_NAME;
                        refdata.SH_CODE = "";
                        refdata.disabledOpt = false;
                        $scope.selectedSpaces.push(refdata);
                        $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                        emp = _.find($scope.Employee);
                        angular.forEach($scope.selectedSpaces, function (value, key) {
                            value.ticked = false;
                            value.disabled = true;
                            if (value.AUR_ID == emp.AUR_ID || value.AUR_ID == "") {
                                value.ticked = true;
                                value.disabled = false;
                            }
                        });
                        var dataobj = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: role.AUR_ID, mode: 1 };
                        MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                            if (result.data) {
                                $scope.EmpAllocSeats = result.data;
                                progress(0, '', false);
                            }
                        });
                    }
                    else {
                        //ragini
                        angular.forEach($scope.selectedSpaces, function (value, key) {
                            value.ticked = false;
                            value.disabled = true;
                            if (value.AUR_ID == "" || value.AUR_ID == null || value.SSA_SRNREQ_ID == "") {
                                value.ticked = true;
                                value.disabled = false;
                            }
                        });
                        refdata.ticked = true;
                        refdata.disabled = false;
                        refdata.AUR_LEAVE_STATUS = "N";
                        //refdata.SSAD_SRN_REQ_ID = null;
                        refdata.SSA_SRNREQ_ID = '';
                        refdata.STACHECK = 16;
                        refdata.AUR_NAME = null;
                        refdata.AUR_ID = null;
                        refdata.SH_CODE = "";
                        refdata.AUR_SEARCH = "";
                        refdata.FROM_DATE = "";
                        refdata.TO_DATE = "";
                        refdata.FROM_TIME = "";
                        refdata.TO_TIME = "";
                        refdata.disabledOpt = false;
                        $scope.selectedSpaces.push(refdata);
                        $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                    }
                }
                else
                    showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");

            }
            else if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {


                showNotification('error', 8, 'bottom-right', 'first release employee');

            }

            else if (prefval.SYSP_VAL1 = 1042) {


                var refdata = {};
                angular.copy(data, refdata);

                var prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
                if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
                    if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
                        refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
                        refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
                    }
                    var role = _.find($scope.GetRoleAndRM);
                    var emp = _.find($scope.Employee);
                    if (role.AUR_ROLE == 6) {
                        var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                        if (ver == undefined) {
                            showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                            return;
                        }
                        refdata.AUR_LEAVE_STATUS = "N";
                        refdata.SSAD_SRN_REQ_ID = null;
                        refdata.STACHECK = 16;
                        refdata.AUR_NAME = role.AUR_KNOWN_AS;
                        refdata.AUR_ID = role.AUR_ID;
                        refdata.VER_CODE = role.VER_CODE;
                        refdata.VER_NAME = role.VER_NAME;
                        refdata.Cost_Center_Code = role.COST_CENTER_CODE;
                        refdata.Cost_Center_Name = role.COST_CENTER_NAME;
                        $scope.selectedSpaces.push(refdata);
                        $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                        MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                            if (result.data) {
                                $scope.EmpAllocSeats = result.data;
                                progress(0, '', false);
                            }
                        });
                    }
                    else {
                        refdata.AUR_LEAVE_STATUS = "N";
                        refdata.SSAD_SRN_REQ_ID = null;
                        refdata.STACHECK = 16;
                        refdata.AUR_NAME = null;
                        refdata.AUR_ID = null;
                        $scope.selectedSpaces.push(refdata);
                        $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                    }

                }
                else
                    showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");

            }
        }

    }
    //$scope.AllocAnother = function (data) {

    //    var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" });
    //    if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {
    //        showNotification('error', 8, 'bottom-right', 'first relese employee');

    //    }
    //    else {
    //        var refdata = {};
    //        angular.copy(data, refdata);

    //        var prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
    //        if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
    //            if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
    //                refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
    //                refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
    //            }

    //            refdata.AUR_LEAVE_STATUS = "N";
    //            refdata.SSAD_SRN_REQ_ID = null;
    //            refdata.STACHECK = 16;
    //            refdata.AUR_NAME = null;
    //            refdata.AUR_ID = null;
    //            $scope.selectedSpaces.push(refdata);
    //            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
    //        }
    //        else
    //            showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");
    //    }
    //}

    ///   Load location data
    $scope.show = function () {
        //UtilityService.getCountires(2).then(function (Countries) {
        //    progress(0, 'Loading...', true);
        //    if (Countries.data != null) {
        //        $scope.Country = Countries.data;
        //    }
        //    UtilityService.getCities(2).then(function (Ctresponse) {
        //        if (Ctresponse.data != null) {
        //            $scope.City = Ctresponse.data;
        //        }

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
            }
            UtilityService.getTowers(2).then(function (response) {
                if (response.data != null) {
                    $scope.Tower = response.data;
                }
                UtilityService.getFloors(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Floor = response.data;
                        angular.forEach($scope.Floor, function (Value, index) {
                            if (Value.FLR_CODE == GetParameterValues('flr_code')) {

                                //var cny = _.find($scope.Country, { CNY_CODE: Value.CNY_CODE });
                                //cny.ticked = true;
                                //_.remove($scope.Country, { ticked: Value.ticked = false });
                                //$scope.SearchSpace.Country[0] = cny;

                                //var cty = _.find($scope.City, { CTY_CODE: Value.CTY_CODE });
                                //cty.ticked = true;
                                //_.remove($scope.City, { ticked: Value.ticked = false });
                                //$scope.SearchSpace.City[0] = cty;

                                var twr = _.find($scope.Tower, { TWR_CODE: Value.TWR_CODE } || { LCM_CODE: Value.LCM_CODE });
                                twr.ticked = true;
                                _.remove($scope.Tower, { ticked: Value.ticked = false });
                                $scope.SearchSpace.Tower[0] = twr;

                                var lcm = _.find($scope.Location, { LCM_CODE: Value.LCM_CODE });
                                lcm.ticked = true;
                                _.remove($scope.Location, { ticked: Value.ticked = false });
                                $scope.SearchSpace.Location[0] = lcm;

                                var flr = _.find($scope.Floor, { FLR_CODE: Value.FLR_CODE } || { LCM_CODE: Value.LCM_CODE } || { TWR_CODE: Value.TWR_CODE });
                                flr.ticked = true;
                                _.remove($scope.Floor, { ticked: Value.ticked == false });
                                $scope.SearchSpace.Floor[0] = flr;
                            }
                        });

                        SpaceRequisitionService.getShifts($scope.SearchSpace.Location).then(function (response) {
                            $scope.Shifts = response.data;
                        }, function (error) {
                            console.log(error);
                        });

                        //MaploaderService.GetFlrIdbyLoc().then(function (response) {
                        //    $scope.LoadMap(response[0].FLR_ID);
                        //}, function (error) {
                        //});
                    }
                    UtilityService.getBussHeirarchy().then(function (response) {
                        if (response.data != null) {
                            $scope.BsmDet = response.data;
                        }

                    });
                });

            });

        });
        //    });
        //});
    };



    //UtilityService.getBussHeirarchy().then(function (response) {
    //    if (response.data != null) {
    //        $scope.BsmDet = response.data;
    //    }
    //});


    //To bind map
    $scope.LoadMap = function (flrid) {

        map.scrollWheelZoom.disable();
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });

        $scope.Markerdata.clearLayers();
        $scope.MarkerLblLayer.clearLayers();
        $scope.MarkerLblLayerSpace.clearLayers();
        $scope.MarkerLblLayerEmpDetails.clearLayers();
        $scope.MarkerLblLayer_copy.clearLayers();
        $scope.MarkerLblLayerSpace_copy.clearLayers();
        $scope.MarkerLblLayerEmpDetails_copy.clearLayers();
        $scope.MarkerMeetingLblLayer.clearLayers();
        $scope.drawnItems = new L.FeatureGroup();
        $scope.drawnItemscheckWS = new L.FeatureGroup();
        $scope.drawnItemscheck = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        map.addLayer($scope.Markerdata);
        $scope.GradeAllocStatus();
        MaploaderService.bindMap(flrid).then(function (response) {
            if (response.mapDetails != null) {
                LOADLAYERS = response.mapDetails[0].LOADLAYERS;

                angular.forEach(response.mapDetails, function (value, index) {
                    // do something

                    var wkt = new Wkt.Wkt();
                    wkt.read(value.Wkt);

                    var theLayer = wkt.toObject();

                    theLayer.dbId = value.ID;
                    theLayer.options.color = '#000000';
                    //theLayer.options.weight = 1;
                    if (prefval == undefined) {
                        theLayer.options.weight = 1;

                    }
                    else theLayer.options.weight = 0.3;
                    theLayer.options.seattype = value.SEATTYPE;
                    theLayer.options.spacetype = value.layer;
                    theLayer.options.seatstatus = value.STAID;
                    theLayer.options.spacesubtype = value.SPC_SUB_TYPE;
                    theLayer.options.spaceid = value.SPACE_ID;
                    theLayer.options.checked = false;

                    //var col = response.COLOR[value.layer] == undefined ? '#E8E8E8' : response.COLOR[value.layer];
                    if (value.layer == "FRE")
                        theLayer.setStyle({ fillColor: '#FA8072', opacity: 0.5, fillOpacity: 0.5 });
                    else
                        theLayer.setStyle({ fillColor: '#E8E8E8', opacity: 0.5, fillOpacity: 0.5 });
                    $scope.drawnItems.addLayer(theLayer);
                    if (value.SPACE_ID != "" || value.layer == "FLR")
                        $scope.drawnItemscheckWS.addLayer(theLayer);
                    //}
                });


                $scope.bounds = [[response.BBOX[0].MinY, response.BBOX[0].MinX], [response.BBOX[0].MaxY, response.BBOX[0].MaxX]];

                var formattedString = response.BBOX[0].LAYOUTNAME.split("/").join("\n");
                var k = formattedString.split("\n");

                var fulltext = "";

                for (var i = 0; i < k.length; i++) {

                    if (i == 0) {
                        fulltext += k[i].bold();
                    }
                    else {
                        fulltext += "/" + k[i];
                    }
                }

                document.getElementById("LAYOUTNAME").innerHTML = fulltext;

                $scope.SU = response.BBOX[0].SU;
                $scope.WorkArea = response.BBOX[0].WorkArea;
                $scope.TotalArea = response.BBOX[0].TotalArea;
                if ($scope.OccupiedByShiftSys && $scope.OccupiedByShiftSys.SYSP_VAL1 == 1) {
                    $scope.DAY_SH = response.BBOX[0].DAY_SH;
                    $scope.NIGHT_SH = response.BBOX[0].NIGHT_SH;
                    $scope.HOURS24_SH = response.BBOX[0].HOURS24_SH;
                }
                $scope.LastRefreshDate = response.BBOX[0].LastRefreshDate;

                map.fitBounds($scope.bounds);
                map.whenReady(function (e) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.ZoomLvl = map.getZoom();
                        });
                    }, 200);
                });

                if (response.mapDetails2 != null)
                    $scope.chairicons = response.mapDetails2;

                $scope.floorDetails = response.FloorDetails;
                if (LOADLAYERS == 1) {
                    $scope.LoadMarkers(response.FloorDetails);
                }
                else
                    $scope.LoadALLMarkers(response.FloorDetails);

            }




        });


    };
    //To bind chair layers
    $scope.LoadMapLayers = function (flrid) {
        map.scrollWheelZoom.disable();
        cnt = cnt + 1;
        angular.forEach($scope.chairicons, function (value, index) {
            // do something

            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);

            var theLayer = wkt.toObject();

            theLayer.dbId = value.ID;
            theLayer.options.color = '#000000';
            //theLayer.options.weight = 1;
            if (prefval == undefined) {
                theLayer.options.weight = 1;

            }
            else theLayer.options.weight = 0.3;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.spacesubtype = value.SPC_SUB_TYPE;
            theLayer.options.spaceid = value.SPACE_ID;
            theLayer.options.checked = false;


            if (value.layer == "FRE")
                theLayer.setStyle({ fillColor: '#FA8072', opacity: 0.5, fillOpacity: 0.5 });
            else
                theLayer.setStyle({ fillColor: '#E8E8E8', opacity: 0.5, fillOpacity: 0.5 });
            $scope.drawnItems.addLayer(theLayer);
            if (value.layer == "CHA" && value.SPACE_ID != '')
                $scope.drawnItemscheck.addLayer(theLayer);
            //}
        });

        $scope.LoadMarkerLayers(GetParameterValues('flr_code'));

    }

    function updateSummaryCount() {

        MaploaderService.getLegendsSummary(GetParameterValues('flr_code')).then(function (response) {
            //console.log(response);
            if (response != null) {

                $scope.SeatSummary = response;

            }
        }, function (error) {
        });
    }
    //To assign markers to chairs
    $scope.LoadMarkerLayers = function (flrdata) {
        printcount = printcount + 1;
        $scope.EmpAllocSeats = [];
        $scope.Markers = [];

        angular.forEach($scope.chairlayers, function (value, index) {
            var style = {};
            if (value.SPC_TYPE_STATUS == 2) {

                switch (value.STATUS) {
                    case 1: chairicon = VacantStyle;
                        break;
                    case 1001: chairicon = AllocateStyle;
                        break;
                    case 1002: chairicon = AllocateStyle;
                        break;
                    case 1004:
                        if ($scope.PartiallyOccupiedSys.SYSP_VAL1 == 1) {
                            if (value.COUNTS == 2)
                                chairicon = OccupiedStyle;
                            else
                                chairicon = PartiallyOccupiedStyle;
                        }
                        else {
                            chairicon = OccupiedStyle;
                        }
                        break;
                    case 1003: chairicon = AllocateStyle;
                        break;
                    case 1006: chairicon = ReservedStyle;
                        break;
                    case 1052: chairicon = BlockStyle;
                        break;
                    default: chairicon = VacantStyle;
                        break;
                }
                if (value.SPC_STA_ID == '0') {
                    switch (value.SPC_STA_ID) {
                        case 0: chairicon = InactiveStyle;
                            break;
                    }
                }

                if (value.AUR_LEAVE_STATUS == 'Y') {
                    switch (value.AUR_LEAVE_STATUS) {
                        case 'Y': chairicon = LeaveStyle;
                            break;
                    }
                }

                $.each($scope.drawnItemscheck._layers, function (Key, layer) {
                    if (layer.options.spaceid == value.SPC_ID && layer.options.spacetype == 'CHA') {
                        $scope.marker = layer;
                    }
                });
                //$scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID, spacetype: 'CHA' } });
                $scope.marker.setStyle(chairicon);
                $scope.marker.lat = value.x;
                $scope.marker.lon = value.y;
                $scope.marker.SPC_ID = value.SPC_ID;
                $scope.marker.SPC_NAME = value.SPC_NAME;
                $scope.marker.SPC_DESC = value.SPC_DESC;
                $scope.marker.layer = value.SPC_TYPE_CODE;
                $scope.marker.VERTICAL = value.VERTICAL;
                $scope.marker.VER_NAME = value.VER_NAME;
                $scope.marker.COSTCENTER = value.COSTCENTER;
                $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                $scope.marker.SST_CODE = value.SST_CODE;
                $scope.marker.SST_NAME = value.SST_NAME;
                $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                $scope.marker.STACHECK = value.STACHECK;
                $scope.marker.STATUS = value.STATUS;
                $scope.marker.STA_DESC = value.STA_DESC;
                $scope.marker.AUR_LEAVE_STATUS = value.AUR_LEAVE_STATUS;
                $scope.marker.ticked = false;

                if (value.SPC_DESC != null && value.SPC_DESC != undefined)
                    var formattedString = value.SPC_DESC.split(",").join("\n");
                else
                    var formattedString = '';
                var k = formattedString.split("\n");
                var id = "";
                var id1 = "";
                var text = "";
                var fulltext = "";
                var str = " ";

                fulltext = BindMarkerText(formattedString, $scope.marker);


                if ($scope.SeatPopUpDetails.SYSP_VAL1 == 1) {
                    var role = _.find($scope.GetRoleAndRM);
                    if (role.AUR_ROLE == 6) {
                        if (value.SSAD_AUR_ID == UserId || $scope.marker.STATUS == 1) {
                            $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);
                        }
                    } else {
                        $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                            noHide: false,
                            direction: 'auto',
                            className: "my-label",
                            zIndexOffset: 2000
                        });
                    }

                } else {
                    $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                        noHide: false,
                        direction: 'auto',
                        className: "my-label",
                        zIndexOffset: 2000
                    });
                }

                $scope.marker.on('contextmenu', function (e) {
                    var role = _.find($scope.GetRoleAndRM);
                    if (role.AUR_ROLE == 6 || role.AUR_ROLE == 14) {
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', 'Employee cannot Inactive Spaces');
                        return;
                    }
                    else {
                        $ngConfirm({
                            icon: 'fa fa-warning',
                            title: 'Confirm!',
                            content: 'Are You Sure You Want To Inactive Seat',
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            buttons: {
                                Confirm: {
                                    text: 'Sure',
                                    btnClass: 'btn-blue',
                                    action: function (button) {
                                        var spcdet = {};
                                        $scope.selectedActSpaces = [];
                                        spcdet.SPACE_ID = value.SPC_ID;
                                        spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                                        spcdet.SPC_SUB_TYPE = value.SST_NAME;
                                        spcdet.ticked = true;
                                        $scope.selectedActSpaces.push(spcdet);

                                        var dataobj = { FLR_CODE: e.target.SPC_FLR_ID, SPC_LST: $scope.selectedActSpaces };
                                        MaploaderService.InactiveSeats(dataobj).then(function (response) {
                                            _.remove($scope.Markers, { SPC_ID: e.target.SPC_ID });

                                            //$scope.InactivespaceidList.push({ IN_SPC_ID: e.target.SPC_ID });

                                            //    if (LOADLAYERS == 1) {
                                            //        $scope.LoadMarkerLayers($scope.floorDetails);
                                            //    }
                                            //    else
                                            //        $scope.LoadALLMarkers($scope.floorDetails);
                                            //    //$scope.LoadMarkerLayers($scope.SearchSpace.Floor[0].FLR_CODE);
                                            //$scope.loadThemes();
                                            //setTimeout(function () {
                                            //    reloadMarkers();
                                            //}, 1000);
                                            setTimeout(function () {
                                                reloadMarkers();
                                            }, 1000);
                                            $ngConfirm({
                                                icon: 'fa fa-check',
                                                title: 'Confirm!',
                                                content: 'Selected Space Has Been Inactive',
                                                buttons: {
                                                    Confirm: {
                                                        text: 'Ok',
                                                        btnClass: 'btn-blue'
                                                    },
                                                }
                                            });
                                        });

                                    }
                                },
                                close: function ($scope, button) {
                                }
                            }
                        });
                    }
                });
                var IN_ACTIVE = _.find($scope.InactivespaceidList, { IN_SPC_ID: value.SPC_ID })
                if (value.STATUS != 1006 && IN_ACTIVE == undefined)
                    $scope.marker.on('click', markerclicked);

                $scope.Markers.push($scope.marker);
                $scope.Markerdata.addLayer($scope.marker);
                //console.log($scope.Markers);
            }

        });
        $scope.gridOptions.api.setRowData($scope.Markers);
        $scope.gridOptions.api.refreshHeader();
        progress(0, '', false);
        if (printcount == 2 && LOADLAYERS == 1 && ActivateSpacescnt == 0) {
            $scope.MarkerMeetingLblLayer.clearLayers();
            print('leafletMap');
        }
        if (GetParameterValues('value') == 1) {

            var marker = _.find($scope.Markers, { SPC_ID: GetParameterValues('spc_id') });

            var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });

            var empMarker = L.marker(L.latLng(marker.lat, marker.lon), {
                icon: chairicon,
                draggable: false
            }).addTo(map);
            if (map.hasLayer($scope.MarkerLblLayerSpace))
                map.removeLayer($scope.MarkerLblLayerSpace);

            setTimeout(function () {
                if (empMarker) { // check
                    map.removeLayer(empMarker); // remove
                    if (!map.hasLayer($scope.MarkerLblLayerSpace)) {
                        map.addLayer($scope.MarkerLblLayerSpace);
                    }
                }
            }, 20000);

            if ($scope.ZoomLvl >= map.getZoom()) {

                //map.fitBounds($scope.bounds);
                setTimeout(function () { map.setView(L.latLng(marker.lat, marker.lon), map.getZoom() + 2); }, 200);
            }
            setTimeout(function () { map.panTo(L.latLng(marker.lat, marker.lon)); }, 400);


            setTimeout(function () {
                $('#Searchdata').selectpicker('refresh');
            }, 200);
        }
        map.scrollWheelZoom.enable();
        ActivateSpacescnt = 0;
    };

    //To assign markers to desks
    $scope.LoadMarkers = function (flrdata) {


        $scope.EmpAllocSeats = [];

        MaploaderService.bindMarkers(flrdata).then(function (response) {

            if (response != null) {
                $scope.chairlayers = response;
                $scope.MarkersAll = response;
                angular.forEach(response, function (value, index) {
                    var style = {};

                    if (value.SPC_TYPE_STATUS == 2) {

                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                            if (layer.options.spaceid == value.SPC_ID) {
                                $scope.marker = layer;
                            }
                        });

                        //$scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID } });
                        $scope.marker.lat = value.x;
                        $scope.marker.lon = value.y;
                        $scope.marker.SPC_ID = value.SPC_ID;
                        $scope.marker.SPC_NAME = value.SPC_NAME;
                        $scope.marker.SPC_DESC = value.SPC_DESC;
                        $scope.marker.layer = value.SPC_TYPE_CODE;
                        $scope.marker.VERTICAL = value.VERTICAL;
                        $scope.marker.VER_NAME = value.VER_NAME;
                        $scope.marker.COSTCENTER = value.COSTCENTER;
                        $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                        $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                        $scope.marker.SST_CODE = value.SST_CODE;
                        $scope.marker.SST_NAME = value.SST_NAME;
                        $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                        $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                        $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                        $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                        $scope.marker.STACHECK = value.STACHECK;
                        $scope.marker.STATUS = value.STATUS;
                        $scope.marker.STA_DESC = value.STA_DESC;
                        $scope.marker.AUR_LEAVE_STATUS = value.AUR_LEAVE_STATUS;
                        $scope.marker.ticked = false;
                        //.fontcolor("green")

                        if (value.SPC_DESC != null && value.SPC_DESC != undefined) {
                            var formattedString = value.SPC_DESC.split(",").join("\n");
                        }
                        else {
                            var formattedString = '';
                        }


                        var k = formattedString.split("\n");
                        var id = "";
                        var id1 = "";
                        var text = "";
                        var fulltext = "";
                        var str = " ";
                        for (var i = 0; i < k.length; i++) {
                            id1 = k[i].substr(0, k[i].indexOf(":"));
                            id = k[i].substr(0, k[i].indexOf(":")).fontcolor("black");
                            text = k[i].substr(k[i].indexOf(":") + 1).fontcolor("seagreen");
                            if ($scope.marker.STATUS == 1004) {
                                fulltext += id1 + "\t" + ":" + text + ",";
                            }
                            else {
                                fulltext += id + " :" + text + ",";
                            }
                        }



                        //$scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                        //    noHide: false,
                        //    direction: 'auto',
                        //    className: "my-label",
                        //    zIndexOffset: 2000
                        //});//.setOpacity(1);

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelspace = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelspace1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3],

                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        $scope.MarkerLblLayerSpace.addLayer($scope.MarkerLabelspace);
                        $scope.MarkerLblLayerSpace_copy.addLayer($scope.MarkerLabelspace1);

                        $scope.MarkerLblLayerEmpDetails.addLayer($scope.MarkerLabelEmp);
                        $scope.MarkerLblLayerEmpDetails_copy.addLayer($scope.MarkerLabelEmp1);


                        $scope.Markers.push($scope.marker);
                        //$scope.Markerdata.addLayer($scope.marker);
                    }
                    else {

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        //$scope.MarkerLabel.bindLabel(value.SPC_DESC, {
                        //    noHide: false,
                        //    direction: 'auto',
                        //    zIndexOffset: 2000
                        //});//.setOpacity(1);
                        $scope.MarkerLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        $scope.MarkerMeetingLblLayer_copy.addLayer($scope.MarkerLabel);
                        $scope.MarkerMeetingLblLayer.addLayer($scope.MarkerLabel);

                    }
                });

                $scope.gridOptions.api.setRowData($scope.Markers);
                $scope.gridOptions.api.refreshHeader();
                progress(0, '', false);
                //$scope.gridOptions.api.sizeColumnsToFit();
            }
            else {
                showNotification('error', 8, 'bottom-right', 'No records Found');
            }
            $scope.loadThemes();
            $scope.show();
            let searchParams = new URLSearchParams(window.location.search);
            if (searchParams.has('spc_id')) {
                var space_id = searchParams.get('spc_id');
                var space = {};
                space = { 'Code': space_id, 'SPC_ID': space_id, 'NAME': '' }
                $scope.ShowEmponMap(space);
            }

        });

        updateSummaryCount();

    };
    //To assign all markers

    $scope.LoadALLMarkers = function (flrdata) {

        $scope.EmpAllocSeats = [];

        MaploaderService.bindMarkers(flrdata).then(function (response) {


            if (response != null) {
                $scope.MarkersAll = response;
                angular.forEach(response, function (value, index) {
                    var style = {};
                    if (value.SPC_TYPE_STATUS == 2) {

                        switch (value.STATUS) {
                            case 1: chairicon = VacantStyle;
                                break;
                            case 1001: chairicon = AllocateStyle;
                                break;
                            case 1002: chairicon = AllocateStyle;
                                break;
                            case 1004: chairicon = OccupiedStyle;
                                break;
                            case 1003: chairicon = AllocateStyle;
                                break;
                            case 1006: chairicon = ReservedStyle;
                                break;
                            case 1052: chairicon = BlockStyle;
                                break;
                            default: chairicon = VacantStyle;
                                break;
                        }
                        if (value.SPC_STA_ID == '0') {
                            switch (value.SPC_STA_ID) {
                                case 0: chairicon = InactiveStyle;
                                    break;
                            }
                        }

                        if (value.AUR_LEAVE_STATUS == 'Y') {
                            switch (value.AUR_LEAVE_STATUS) {
                                case 'Y': chairicon = LeaveStyle;
                                    break;
                            }
                        }

                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == value.SPC_ID && layer.options.spacetype == 'CHA') {
                                $scope.marker = layer;
                            }
                        });
                        // $scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID, spacetype: 'CHA' } });
                        $scope.marker.setStyle(chairicon);
                        $scope.marker.lat = value.x;
                        $scope.marker.lon = value.y;
                        $scope.marker.SPC_ID = value.SPC_ID;
                        $scope.marker.SPC_NAME = value.SPC_NAME;
                        $scope.marker.SPC_DESC = value.SPC_DESC;
                        $scope.marker.layer = value.SPC_TYPE_CODE;
                        $scope.marker.VERTICAL = value.VERTICAL;
                        $scope.marker.VER_NAME = value.VER_NAME;
                        $scope.marker.COSTCENTER = value.COSTCENTER;
                        $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                        $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                        $scope.marker.SST_CODE = value.SST_CODE;
                        $scope.marker.SST_NAME = value.SST_NAME;
                        $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                        $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                        $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                        $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                        $scope.marker.STACHECK = value.STACHECK;
                        $scope.marker.STATUS = value.STATUS;
                        $scope.marker.STA_DESC = value.STA_DESC;
                        $scope.marker.AUR_LEAVE_STATUS = value.AUR_LEAVE_STATUS;
                        $scope.marker.ticked = false;

                        $scope.marker.on('contextmenu', function (e) {
                            var role = _.find($scope.GetRoleAndRM);
                            if (role.AUR_ROLE == 6 || role.AUR_ROLE == 14) {
                                progress(0, 'Loading...', false);
                                showNotification('error', 8, 'bottom-right', 'Employee cannot Inactive Spaces');
                                return;
                            }
                            else {
                                $ngConfirm({
                                    icon: 'fa fa-warning',
                                    title: 'Confirm!',
                                    content: 'Are You Sure You Want To Inactive Seat',
                                    closeIcon: true,
                                    closeIconClass: 'fa fa-close',
                                    buttons: {
                                        Confirm: {
                                            text: 'Sure',
                                            btnClass: 'btn-blue',
                                            action: function (button) {
                                                var spcdet = {};
                                                $scope.selectedActSpaces = [];
                                                spcdet.SPACE_ID = value.SPC_ID;
                                                spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                                                spcdet.SPC_SUB_TYPE = value.SST_NAME;
                                                spcdet.ticked = true;
                                                $scope.selectedActSpaces.push(spcdet);

                                                var dataobj = { FLR_CODE: e.target.SPC_FLR_ID, SPC_LST: $scope.selectedActSpaces };
                                                MaploaderService.InactiveSeats(dataobj).then(function (response) {
                                                    $scope.InactivespaceidList.push({ IN_SPC_ID: e.target.SPC_ID });
                                                    _.remove($scope.Markers, { SPC_ID: e.target.SPC_ID });
                                                    //if (LOADLAYERS == 1) {
                                                    //    $scope.LoadMarkerLayers($scope.floorDetails);
                                                    //}
                                                    //else
                                                    //    $scope.LoadALLMarkers($scope.floorDetails);
                                                    ////$scope.LoadMarkerLayers($scope.SearchSpace.Floor[0].FLR_CODE);
                                                    //$scope.loadThemes();
                                                    setTimeout(function () {
                                                        reloadMarkers();
                                                    }, 1000);
                                                    //reloadMarkers();

                                                    $ngConfirm({
                                                        icon: 'fa fa-check',
                                                        title: 'Confirm!',
                                                        content: 'Selected Space Has Been Inactive',
                                                        buttons: {
                                                            Confirm: {
                                                                text: 'Ok',
                                                                btnClass: 'btn-blue'
                                                            },
                                                        }
                                                    });
                                                });

                                            }
                                        },
                                        close: function ($scope, button) {
                                        }
                                    }
                                });
                            }
                        });
                        if (value.SPC_DESC != null && value.SPC_DESC != undefined) {
                            var formattedString = value.SPC_DESC.split(",").join("\n");
                        }
                        else {
                            var formattedString = '';
                        }

                        //var formattedString = value.SPC_DESC.split(",").join("\n");

                        var k = formattedString.split("\n");
                        var id = "";
                        var id1 = "";
                        var text = "";
                        var fulltext = "";
                        var str = " ";

                        fulltext = BindMarkerText(formattedString, $scope.marker);


                        if ($scope.SeatPopUpDetails.SYSP_VAL1 == 1) {
                            var role = _.find($scope.GetRoleAndRM);
                            if (role.AUR_ROLE == 6) {
                                if (value.SSAD_AUR_ID == UserId || $scope.marker.STATUS == 1) {
                                    $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                        noHide: false,
                                        direction: 'auto',
                                        className: "my-label",
                                        zIndexOffset: 2000
                                    });//.setOpacity(1);
                                }
                            } else {
                                $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                    noHide: false,
                                    direction: 'auto',
                                    className: "my-label",
                                    zIndexOffset: 2000
                                });
                            }

                        } else {
                            $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });
                        }
                        var IN_ACTIVE = _.find($scope.InactivespaceidList, { IN_SPC_ID: value.SPC_ID })
                        if (value.STATUS != 1006 && IN_ACTIVE == undefined)
                            $scope.marker.on('click', markerclicked);



                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLabelspace = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelspace1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        $scope.MarkerLblLayerSpace.addLayer($scope.MarkerLabelspace);
                        $scope.MarkerLblLayerSpace_copy.addLayer($scope.MarkerLabelspace1);

                        $scope.MarkerLblLayerEmpDetails.addLayer($scope.MarkerLabelEmp);
                        $scope.MarkerLblLayerEmpDetails_copy.addLayer($scope.MarkerLabelEmp1);

                        $scope.Markers.push($scope.marker);
                        $scope.Markerdata.addLayer($scope.marker);
                    }
                    else {

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLabel.bindLabel(value.SPC_DESC, {
                            noHide: false,
                            direction: 'auto',
                            zIndexOffset: 2000
                        });//.setOpacity(1);
                        $scope.MarkerMeetingLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerMeetingLblLayer.addLayer($scope.MarkerLabel);

                    }

                });

                $scope.gridOptions.api.setRowData($scope.Markers);
                $scope.gridOptions.api.refreshHeader();

                progress(0, '', false);
                //setTimeout(function () {
                //    $scope.loadThemes();
                //}, 500);
                $scope.show();
                //$scope.gridOptions.api.sizeColumnsToFit();
            } else {
                showNotification('error', 8, 'bottom-right', 'No records Found');
            }
            $scope.loadThemes();
            if (GetParameterValues('value') == 1) {

                var marker = _.find($scope.Markers, { SPC_ID: GetParameterValues('spc_id') });

                var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });

                var empMarker = L.marker(L.latLng(marker.lat, marker.lon), {
                    icon: chairicon,
                    draggable: false
                }).addTo(map);
                if (map.hasLayer($scope.MarkerLblLayerSpace))
                    map.removeLayer($scope.MarkerLblLayerSpace);

                setTimeout(function () {
                    if (empMarker) { // check
                        map.removeLayer(empMarker); // remove
                        if (!map.hasLayer($scope.MarkerLblLayerSpace)) {
                            map.addLayer($scope.MarkerLblLayerSpace);
                        }
                    }
                }, 20000);

                if ($scope.ZoomLvl >= map.getZoom()) {

                    //map.fitBounds($scope.bounds);
                    setTimeout(function () { map.setView(L.latLng(marker.lat, marker.lon), map.getZoom() + 2); }, 200);
                }
                setTimeout(function () { map.panTo(L.latLng(marker.lat, marker.lon)); }, 400);


                setTimeout(function () {
                    $('#Searchdata').selectpicker('refresh');
                }, 200);
            }
        }, function (error) {
        });
        updateSummaryCount();
    }

    UtilityService.GetRoleByUserId().then(function (response) {
        if (response.data != null) {
            $scope.Role = response.data;
            if ($scope.Role.Rol_id == 'N') {
                $scope.gridOptions.columnApi.setColumnVisible('ticked', false)
            }
            else { $scope.gridOptions.columnApi.setColumnVisible('ticked', true) }
        }
    });

    function markerclicked(e) {
        var sta = '';
        var aur = '';
        $scope.ShiftData = $scope.Shifts.filter(
            (item) => item.SH_SEAT_TYPE == this.SHIFT_TYPE
        );

        var IN_ACTIVE = _.find($scope.InactivespaceidList, { IN_SPC_ID: this.SPC_ID })
        if (IN_ACTIVE != undefined) {
            this.ticked = false;
            return;
        }
        var role = _.find($scope.GetRoleAndRM);

        if ($scope.VerticalWiseAlloc.SYSP_VAL1 == 1 && this.VERTICAL != "" && this.VERTICAL != role.VER_CODE && role.AUR_ROLE != 1) {
            $ngConfirm({
                icon: 'fa fa-warning',
                title: 'Allocation Warning',
                content: 'You are not allowed to book this seat',
                closeIcon: true,
                closeIconClass: 'fa fa-close',
                buttons: {
                    close: function ($scope, button) {
                    }
                }
            });
        }
        else if ($scope.LibertyChanges.SYSP_VAL1 == 1 && ((this.SHIFT_TYPE == 1 || this.SHIFT_TYPE == 5 || this.SHIFT_TYPE == 6) || ($scope.emplotype != 'PWFH' && this.layer == 'WS'))) {
            $ngConfirm({
                icon: 'fa fa-warning',
                title: 'Restricted for booking '+ this.SPC_ID,
                content: '**Employees with work status - <b>Partially Work From Home </b>,has no privileges to book Fully Work From Office Seats or Fully Work From Home/Outstation Employees Seats',
                closeIcon: true,
                closeIconClass: 'fa fa-close',
                buttons: {
                    close: function ($scope, button) {
                    }
                }
            });
        }
        else
        {

                    if ($scope.selectedSpaces.length != 0 && role.AUR_ROLE == 6 && this.ticked == false) {
                        showNotification('error', 8, 'bottom-right', 'Employee can not access more than 1 seat');
                        return;
                    }


                    if (this.SSAD_AUR_ID != '' && this.SSAD_AUR_ID != null) {
                        sta = this.STATUS;
                        aur = this.SSAD_AUR_ID.trim();
                    }
                    else {

                        sta = this.STATUS;
                        aur = this.SSAD_AUR_ID;
                    }

            //if (role.AUR_ROLE == 6) {

            //    if (this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 1 && role.AUR_ID != aur) {
            //        showNotification('error', 8, 'bottom-right', 'Employee can not access a dedicated seat');
            //        return;
            //    }

            //    //if (this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 2 && role.AUR_ID != aur) {
            //    //    $scope.ReleaseEnabledStatus = true;
            //    //    return;
            //    //}
            //}


                if (role.AUR_ROLE == 6) {
                    if (this.SSAD_AUR_ID != this.SSAD_AUR_ID && this.SSAD_AUR_ID != this.SSAD_AUR_ID && role.AUR_ID != this.SSAD_AUR_ID && $scope.LibertyChanges.SYSP_VAL1 == 1) {
                        //if (this.SSAD_AUR_ID != '' && this.SSAD_AUR_ID != null && role.AUR_ID != this.SSAD_AUR_ID && $scope.LibertyChanges.SYSP_VAL1 == 0) {
                        showNotification('error', 8, 'bottom-right', 'This seat Allocated to Another Employee');
                        return;
                    }
                    else if (this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 1 && role.AUR_ID != aur) {
                        showNotification('error', 8, 'bottom-right', 'Employee can not access a dedicated seat');
                        return;
                    }

                    //if (this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 2 && role.AUR_ID != aur) {
                    //    $scope.ReleaseEnabledStatus = true;
                    //    return;
                    //}
                }
            //if (role.AUR_ROLE == 6 && this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 1) {
            //    showNotification('error', 8, 'bottom-right', 'Employee can not access a dedicated seat');
            //    return;
            //}
                if (!this.ticked) {
                    $scope.selectSeatCount = $scope.selectSeatCount + 1;
                    if ($scope.Role.Rol_id == 'N') {
                        this.ticked = false;
                    }
                    else {
                        if ($scope.GradeAlloc.SYSP_VAL1 == 1 && $scope.Allow_Remaining <= $scope.selectedSpaces.length && $scope.Grade_Allow_status == 'Allow' && $scope.Eligibility != 'No') {
                            $scope.AllocateEnabledStatus = true;

                            $ngConfirm({
                                icon: 'fa fa-warning',
                                title: 'Allocation Warning',
                                content: 'You have exceeded maxmimum no of allocations.Current limit is ' + $scope.ORG_ALLOC_COUNT + '. You can allocate another ' + $scope.Allow_Remaining + ' seats only',
                                closeIcon: true,
                                closeIconClass: 'fa fa-close',
                                buttons: {
                                    close: function ($scope, button) {
                                    }
                                }
                            });
                        }
                        this.setStyle(selctdChrStyle)
                        this.ticked = true;

                        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, category: this.SPC_ID, subitem: this.SSA_SRNREQ_ID ? this.SSA_SRNREQ_ID : "" };
                        MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {

                            if (response != null) 
                            {
                                angular.forEach(response, function (value, index) {
                                    // $scope.selectedSpaces.push(value);
                                    if (value.SSA_SRNREQ_ID != "" && value.STATUS == 1004 && role.AUR_ROLE == 6) {
                                        value.disabledOpt = true;
                                        $scope.AllocateEnabledStatus = true;
                                    }
                                    if ($scope.selectedSpaces.length != 0) {
                                        //fetch all space ids
                                        var spaceids = $scope.selectedSpaces.map(
                                            (item) => item.SPC_ID);

                                        // check if space id not exist then only it adds list
                                        //if (!spaceids.includes(value.SPC_ID)) {
                                        $scope.selectedSpaces.push(value);
                                        //}
                                    }
                                    else {
                                        $scope.selectedSpaces.push(value);
                                    }
                                    $scope.gridoptions1 = false;
                                    $('#adminbookseat').removeClass('Verticalboxsize');
                                    if (role.AUR_ROLE == 6 && $scope.LibertyChanges.SYSP_VAL1 != 1 && $scope.LibertyChanges && $scope.selectedSpaces[0].SPACE_TYPE == 1) {
                                        debugger;
                                        $('#SeatAllocation').modal('show');
                                        $scope.hotdeskspecifications = false;
                                        $scope.txtCountFilter = false;
                                        $scope.gridoptions1 = true;
                                        $('#adminbookseat').addClass('Verticalboxsize');
                                        if ($scope.selectedSpaces[0].SPACE_TYPE == 4 || $scope.selectedSpaces[0].SPACE_TYPE == 5) {
                                            $scope.HotdeskingCond = false;
                                        }
                                        else {
                                            $scope.HotdeskingCond = true;

                                        }
                                    }
                                    $scope.selectedSpacesLength = $scope.selectedSpaces.length;
                                    if (role.AUR_ROLE == 6 || ($scope.EmidsAllocate.SYSP_VAL1 == 1 && (role.AUR_ROLE == 1 || role.AUR_ROLE == 13) && $scope.EmidsAllocate)) {
                                        value.block = true;


                                        //var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                                        //if (ver == undefined) {
                                        //    $scope.selectedSpaces = [];
                                        //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                                        //    reloadMarkers();
                                        //    return;

                                        //}
                                        //var cost = _.find($scope.selectedSpaces, function (x) { if ((x.STATUS == 1003 || x.STATUS == 1004) && x.Cost_Center_Code == role.COST_CENTER_CODE) return x });
                                        //if (cost == undefined) {
                                        //    $scope.selectedSpaces = [];
                                        //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different costcenter to employee');
                                        //    reloadMarkers();
                                        //    return;

                                        //}
                                        var data = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: role.AUR_ID, mode: 1 };
                                        $scope.EmpAllocSeats = {};
                                        //if (role.AUR_ROLE == 6 && sta != 1 && sta != 1003 && sta != 1002 && role.AUR_ID != aur) {
                                        MaploaderService.getEmpAllocSeat(data).then(function (result) {
                                            if (result.data) {
                                                if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1) {
                                                    if ($scope.selectedSpaces[0].SPACE_TYPE != '4') {
                                                        $scope.EmpAllocSeats = result.data;
                                                    }
                                                }
                                                else {
                                                    $scope.EmpAllocSeats = result.data;
                                                }
                                                if (role.AUR_ROLE == 6 || ($scope.EmidsAllocate.SYSP_VAL1 == 1 && (role.AUR_ROLE == 1 || role.AUR_ROLE == 13) && $scope.EmidsAllocate)) {
                                                    $scope.ProallocEnabledStatus = true;
                                                }
                                                progress(0, '', false);
                                            }
                                        });
                                        //}
                                    }
                                })

                                
                                if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
									var dataobj1 = { SPC_ID: response[0].SPC_ID, FROM_DATE: response[0].FROM_DATE };
                                    MaploaderService.SeatRestriction(dataobj1).then(function (response) {
                                        if (response != null) {
                                            //$scope.SeatRestrictionStatus = response[0].Seat_Restr;
                                            $scope.Remaining = response.data[0].Remaining;
                                        }
                                    });
                                    var dataobj = {
                                        lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE,
                                        Item: (response[0].SPC_ID), mode: 2
                                    };
                                    MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                                        if (result.data) {
                                            $scope.EmpAllocSeatsOtherDay = result.data;
                                        }
                                    });
                                }

                            }
                            $scope.EmpAllocSeats = _.remove($scope.EmpAllocSeats, function (n) {
                                return _.find($scope.gridSpaceAllocOptions.rowData, { AUR_ID: n.AUR_ID });
                            });
                            $scope.EmpAllocSeats = _.uniqBy($scope.EmpAllocSeats, 'AUR_ID');
                        }, function (error) {
                        });
                    }
                }
                else {
                        if ($scope.selectSeatCount > 0)
                            $scope.selectSeatCount = $scope.selectSeatCount - 1;
                        var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });
                        switch (this.STATUS) {
                            case 1: chairicon = VacantStyle;
                                break;
                            case 1002: chairicon = AllocateStyle;
                                break;
                            case 1004:
                                if ($scope.PartiallyOccupiedSys.SYSP_VAL1 == 1) {
                                    if (this.COUNTS == 2)
                                        chairicon = OccupiedStyle;
                                    else
                                        chairicon = PartiallyOccupiedStyle;
                                }
                                else {
                                    chairicon = OccupiedStyle;
                                }
                                break;
                            case 1003: chairicon = AllocateStyle;
                                break;
                            case 1052: chairicon = BlockStyle;
                                break;

                            default: chairicon = VacantStyle;
                                break;
                        }
                        _.remove($scope.selectedSpaces, function (space) { return space.SPC_ID == e.target.SPC_ID });

                        $scope.selectedSpacesLength = $scope.selectedSpaces.length;
                        if ($scope.GradeAlloc.SYSP_VAL1 = 1 && $scope.Allow_Remaining >= ($scope.selectedSpacesLength) && $scope.Grade_Allow_status == 'Allow' && $scope.Eligibility != 'No') {
                            $scope.AllocateEnabledStatus = false;

                        }

                        this.setStyle(chairicon)
                        this.ticked = false;
                    }
            $scope.gridOptions.api.refreshView();
        }
    }

    /****************  Grid Display ***************/

    var columnDefs = [
        { headerName: "Select", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkedChanged(data)' />", cellClass: 'grid-align', suppressFilter: true },
        //{ headerName: "Space ID", field: "SPC_ID", cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" },
        { headerName: "Space ID", field: "SPC_NAME", cellClass: 'grid-align', enableSorting: true },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", enableSorting: true },
        /*{ headerName: "Space Sub Type", field: "SST_NAME", enableSorting: true },*/
        { headerName: "Space Subtype", field: "SST_NAME", enableSorting: true },
        { headerName: "Status", field: "STA_DESC", enableSorting: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        rowHeight: 48,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        showToolPanel: true,
        rowAggregatePanelShow: 'none',
        enableColResize: true,
        enableCellSelection: false,
        suppressRowClickSelection: true,
        onready: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode('Select All');
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = true;
                        value.setStyle(selctdChrStyle)
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = false;
                        value.setStyle(VacantStyle)
                        switch (value.STATUS) {
                            case 1: value.setStyle(VacantStyle);
                                break;
                            case 1002: value.setStyle(AllocateStyle);
                                break;
                            case 1004: value.setStyle(OccupiedStyle);
                                break;
                            case 1003: value.setStyle(AllocateStyle);
                                break;
                            case 1052: value.setStyle(BlockStyle);
                                break;
                            default: value.setStyle(VacantStyle);
                                break;
                        }

                    });
                });
            }
        });
        return eHeader;
    }

    /****************  Filter ***************/

    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    $("#InacFilter").change(function () {
        onSpaceReqFilterChanged($(this).val());
    }).keydown(function () {
        onSpaceReqFilterChanged($(this).val());
    }).keyup(function () {
        onSpaceReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onSpaceReqFilterChanged($(this).val());
    });

    $("#txtCountFilter").change(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keydown(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keyup(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).bind('paste', function () {
        onReq_SelSpacesFilterChanged($(this).val());
    });

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    function onSpaceReqFilterChanged(value) {
        $scope.gridSpaceAllocation.api.setQuickFilter(value);
    }

    function onReq_SelSpacesFilterChanged(value) {
        $scope.gridSpaceAllocOptions.api.setQuickFilter(value);
    }


    /****************  Check change event ***************/
    $scope.chkedChanged = function (data) {
        if (data.ticked) {
            if ($scope.Role.Rol_id == 'N') {
                this.ticked = false;
            }
            else {
                data.setStyle(selctdChrStyle)
                data.ticked = true;
                var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, category: data.SPC_ID, subitem: data.SSA_SRNREQ_ID ? data.SSA_SRNREQ_ID : "" };
                MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {
                    if (response != null) {
                        angular.forEach(response, function (value, index) {
                            $scope.selectedSpaces.push(value);
                        })
                    }
                }, function (error) {
                });
            }
        }
        else {
            switch (data.STATUS) {
                case 1: chairicon = VacantStyle;
                    break;
                case 1002: chairicon = AllocateStyle;
                    break;
                case 1004: chairicon = OccupiedStyle;
                    break;
                case 1003: chairicon = AllocateStyle;
                    break;
                case 1052: chairicon = BlockStyle;
                    break;
                default: chairicon = VacantStyle;
                    break;
            }
            _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SPC_ID: data.SPC_ID }));

            data.setStyle(chairicon)
            data.ticked = false;
        }
    }

    /****************  Filters ***************/

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    $scope.CnyChangeAll = function () {
        $scope.SearchSpace.Country = $scope.Country;
        $scope.CnyChanged();
    }
    $scope.CnySelectNone = function () {
        $scope.SearchSpace.Country = [];
        $scope.CnyChanged();
    }
    $scope.CnyChanged = function () {
        if ($scope.SearchSpace.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.SearchSpace.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SearchSpace.City = $scope.City;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SearchSpace.City = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SearchSpace.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SearchSpace.Location = $scope.Location;
        $scope.LcmChanged();
    }
    $scope.LcmSelectNone = function () {
        $scope.SearchSpace.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SearchSpace.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SearchSpace.Tower = $scope.Tower;
        $scope.TwrChanged();
    }
    $scope.TwrSelectNone = function () {
        $scope.SearchSpace.Tower = [];
        $scope.TwrChanged();
    }
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SearchSpace.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });
    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SearchSpace.Floor = $scope.Floor;
        $scope.FlrChanged();
    }
    $scope.FlrSelectNone = function () {
        $scope.SearchSpace.Floor = [];
        $scope.FlrChanged();
    }
    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SearchSpace.Tower.push(twr);
            }
        });
        $scope.loadThemes();
    }
    $scope.showStatusPanel = true;

    $scope.loadThemes = function () {
        $scope.detailsdisplay = false;
        $scope.nrmaldisplay = true;
        $scope.showStatusPanel = true;
        progress(0, 'Loading...', true);
        $scope.SubItems = [];
        $scope.ddlSubItem = "";
        var item = {};
        $scope.ddlselval = _.find($scope.Items, { CODE: $scope.ddlItem });
        //console.log($scope.ddlselval);
        if ($scope.ddlItem) {
            if ($scope.ddlselval != undefined) {
                if ($scope.ddlselval.CHKDDL == 1) {
                    var dataobj = { flr_code: GetParameterValues('flr_code'), Item: $scope.ddlItem, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
                    $scope.SpcCount = [];
                    MaploaderService.getallFilterbySubItem(dataobj).then(function (response) {
                        $scope.ddldisplay = true;
                        $scope.SubItems = response;
                        $scope.shshow = true;
                        // for (var i in response) {
                            // var obj = {};
                            // obj.CODE = response[i].CODE;
                            // obj.NAME = response[i].NAME;
                            // $scope.SpcCount.push(obj);
                        // }
                        progress(0, '', false);
                    }, function (error) {
                        console.log(error);
                    });
                    progress(0, '', false);
                }
                else {
                    $scope.ddldisplay = false;
                    $scope.shshow = false;
                    setTimeout(function () { $scope.GetLegendCount() }, 200);
                }
            }
            else {
                $scope.ddldisplay = false;
                $scope.shshow = false;
                setTimeout(function () { $scope.GetLegendCount() }, 200);
            }
        }
        map.scrollWheelZoom.enable();


    }

    $scope.loadSubDetails = function () {
        $scope.showStatusPanel = true;
        progress(0, 'Loading...', true);
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: $scope.ddlItem, subitem: $scope.ddlSubItem, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetSpaceDetailsBySUBITEM(dataobj).then(function (response) {
            if (response.Table != null) {
                if ($scope.ddlItem == "login" || $scope.ddlItem == "liveattn") {
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        var spcid = _.find(response.Table, { SPC_ID: layer.options.spaceid });
                        if (spcid == undefined)
                            layer.setStyle({ fillColor: "#E8E8E8" });
                    });

                    angular.forEach($scope.Markers, function (marker, key) {

                        var spcid = _.find(response.Table, { SPC_ID: marker.SPC_ID });
                        if (spcid != null) {
                            marker.ticked = false;
                            switch (spcid.STATUS) {
                                case 1: marker.setStyle(VacantStyle);
                                    break;
                                case 1002: marker.setStyle(AllocateStyle);
                                    break;
                                case 1004: marker.setStyle(OccupiedStyle);
                                    break;
                                case 1003: marker.setStyle(AllocateStyle);
                                    break;
                                case 1052: marker.setStyle(BlockStyle);
                                    break;
                                default: marker.setStyle(VacantStyle);
                                    break;
                            }
                            marker.bindLabel(spcid.SPC_DESC, {
                                noHide: false,
                                direction: 'auto',
                                zIndexOffset: 2000
                            });//.setOpacity(1);
                        }
                    });
                    $scope.showStatusPanel = false;
                    $scope.SpcCount = response.Table1;

                }
                else {
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        var spcid = _.find(response.Table, { SPC_ID: layer.options.spaceid });
                        if (spcid == undefined)
                            layer.setStyle({ fillColor: "#E8E8E8" });
                        else {
                            var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });
                            if (layer.options.spacetype == 'CHA') {
                                switch (spcid.STATUS) {
                                    case 1: chairicon = VacantStyle;
                                        break;
                                    case 1002: chairicon = AllocateStyle;
                                        break;
                                    case 1004: chairicon = OccupiedStyle;
                                        break;
                                    case 1003: chairicon = AllocateStyle;
                                        break;
                                    case 1052: chairicon = BlockStyle;
                                        break;
                                    default: chairicon = VacantStyle;
                                        break;
                                }

                                layer.setStyle(chairicon);
                            }
                            else {
                                if ($scope.ddlItem == "shiftalloc") {
                                    $scope.SpcCount = response.Table1;
                                }
                                else {
                                    var spcclrobj = _.find($scope.SubItemColordet, { SPC_ID: spcid.SPC_ID });
                                    if (spcclrobj != undefined)
                                        layer.setStyle({ fillColor: spcclrobj.COLOR, opacity: 0.65 });
                                }
                            }

                        }
                    });
                    if ($scope.ddlItem == "shiftalloc") {
                        $scope.GetLegendCount();
                    }
                    else if ($scope.ddlItem == "liveattn") {
                        $scope.showStatusPanel = false;
                        $scope.SpcCount = response.Table1;
                    } 
					else if ($scope.ddlItem == "shift") {
                        $scope.showStatusPanel = false;
                        //$scope.SpcCount = response.Table1;
                    }
                }

            }
            progress(0, '', false);
        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.GetLegendCount = function () {



        $scope.SpcCount = [];

        var themeobj = { Item: $scope.ddlItem, flr_code: GetParameterValues('flr_code'), subitem: $scope.ddlSubItem };
        if ($scope.ddlItem == 'verticalalloc') {
            $scope.detailsdisplay = true;
            $scope.nrmaldisplay = false;
        }
        else {
            $scope.detailsdisplay = false;
            $scope.nrmaldisplay = true;
        }
        //console.log(themeobj);
        $.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsCount', themeobj, function (result) {
			
			if($scope.ddlItem == 'seattype' && result!=null && result.Table.length>0){
				$scope.SPACETYPE=[];
				for (var i in result.Table){
					var obj={};
					obj.SPACE_TYPE=result.Table[i].NAME;
					obj.sno=result.Table[i].CODE;
					$scope.SPACETYPE.push(obj);
				}
			}
			
            if ($scope.ddlItem == 'Vacant') {
                $scope.$apply(function () {
                    $scope.SpcCount = result.Table;
					
					for (var i in result.Table){
						var obj={};
						obj.SPACE_TYPE=result.Table[i].NAME;
						obj.sno=result.Table[i].CODE;
						$scope.SPACETYPE.push(obj);
					}
					
                });
                var vacspaces = [];
                angular.forEach($scope.MarkersAll, function (marker, key) {
                    marker.ticked = false;
                    if (marker.STATUS == 1) {
                        marker.setStyle(VacantStyle);
                        vacspaces.push(marker);
                    }
                    progress(0, '', false);
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find(vacspaces, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                    else {
                        layer.setStyle({ fillColor: "#228B22", opacity: 0.65 });
                    }
                    progress(0, '', false);
                });
                reloadMarkers();
            }
            else if ($scope.ddlItem == "entity") {
                if (result.Table) {
                    $scope.$apply(function () {
                        $scope.SpcCount = result.Table;
                        if ($scope.SpcCountArea.length == 0)
                            $scope.SpcCountArea = result.Table;
                    });
                    for (var i = 0; i < result.Table1.length; i++) {
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == result.Table1[i].SPC_ID && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: result.Table1[i].COLOR, opacity: 0.65 });
                            }
                        });
                    }

                    progress(0, '', false);
                }
                reloadMarkers();
            }
            else if ($scope.ddlItem == "release") {
                $scope.$apply(function () {
                    $scope.SubItemColordet = result.Table;
                    $scope.SpcCount = result.Table1;
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.SubItemColordet, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.MarkersAll, function (marker, key) {
                    marker.ticked = false;
                    switch (marker.STATUS) {
                        case 1: marker.setStyle(VacantStyle);
                            break;
                        case 1002: marker.setStyle(AllocateStyle);
                            break;
                        case 1004: marker.setStyle(OccupiedStyle);
                            break;
                        case 1003: marker.setStyle(AllocateStyle);
                            break;
                        case 1052: marker.setStyle(BlockStyle);
                            break;
                        default: marker.setStyle(VacantStyle);
                            break;
                    }
                });

                angular.forEach($scope.SubItemColordet, function (Value, key) {
                    var clrobj = _.find($scope.SpcCount, { CODE: Value.CODE });
                    if (clrobj != undefined)
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == Value.SPC_ID && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: clrobj.COLOR, opacity: 0.65 });
                            }
                        });
                });

                progress(0, '', false);
                reloadMarkers();
            }

            else if ($scope.ddlItem == "Unused") {
                $scope.$apply(function () {
                    $scope.UnusedDetails = result.Table;
                    $scope.SpcCount = result.Table1;
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.UnusedDetails, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.MarkersAll, function (marker, key) {
                    marker.ticked = false;
                    var clrobj = _.find($scope.UnusedDetails, { SPC_ID: marker.SPC_ID });
                    if (clrobj != undefined) {
                        marker.setStyle({ fillColor: clrobj.COLOR });
                    }
                });



                progress(0, '', false);
            }
            else if ($scope.ddlItem == "spacesubtype") {
                $scope.$apply(function () {
                    $scope.spacesubtype = result.Table;
                    $scope.SpcCount = result.Table;
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    if (layer.options.spacetype != "CHA") {
                        var spcid = _.find($scope.spacesubtype, { SPC_ID: layer.options.spaceid });
                        if (spcid == undefined)
                            layer.setStyle({ fillColor: "#E8E8E8" });
                    }
                });
                angular.forEach($scope.MarkersAll, function (marker, key) {
                    //marker.ticked = false;
                    //var clrobj = _.find($scope.UnusedDetails, { SPC_ID: marker.SPC_ID });
                    //if (clrobj != undefined) {
                    //    marker.setStyle({ fillColor: clrobj.COLOR });
                    //}
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        if (layer.options.spaceid == marker.SPC_ID && layer.options.spacetype != "CHA") {
                            var color = _.find(result.Table, { CODE: layer.options.spacesubtype });
                            $scope.marker_spc = layer;
                            $scope.marker_spc.setStyle({ fillColor: color.COLOR });
                        }
                    });

                });



                progress(0, '', false);
            }
            else if ($scope.ddlItem != "verticalalloc" && $scope.ddlItem != "costcenteralloc" && $scope.ddlItem != "shiftalloc") {
                if (result.Table) {
                    $scope.$apply(function () {
                        $scope.SpcCount = result.Table;
                        if ($scope.SpcCountArea.length == 0)
                            $scope.SpcCountArea = result.Table;
                    });

                    for (var i = 0; i < result.Table.length; i++) {
                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {

                            $.each($scope.MarkersAll, function (Key, value) {

                                if (layer.options[$scope.ddlItem] == result.Table[i].CODE && layer.options.spacetype != "CHA" && layer.options.spaceid == value.SPC_ID) {
                                    layer.setStyle({ fillColor: result.Table[i].COLOR, opacity: 0.65 });
                                }
                                if (layer.options[$scope.ddlItem] == result.Table[i].CODE && layer.options.spacetype == "FLR") {
                                    layer.setStyle({ fillColor: result.Table[i].COLOR, opacity: 0.65 });
                                }
                            });
                        });
                    }

                    progress(0, '', false);
                }

                reloadMarkers();
            }

            else {
                $scope.SubItemColordet = result.Table;

                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.SubItemColordet, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.MarkersAll, function (marker, key) {
                    marker.ticked = false;
                    switch (marker.STATUS) {
                        case 1: marker.setStyle(VacantStyle);
                            break;
                        case 1002: marker.setStyle(AllocateStyle);
                            break;
                        case 1004: marker.setStyle(OccupiedStyle);
                            break;
                        case 1003: marker.setStyle(AllocateStyle);
                            break;
                        case 1052: marker.setStyle(BlockStyle);
                            break;
                        default: marker.setStyle(VacantStyle);
                            break;
                    }
                });

                angular.forEach($scope.SubItemColordet, function (Value, key) {
                    $.each($scope.drawnItems._layers, function (Key, layer) {

                        if (layer.options.spaceid == Value.SPC_ID && layer.options.spacetype == Value.SPC_LAYER && layer.options.spacetype != "CHA") {
                            layer.setStyle({ fillColor: Value.COLOR, opacity: 0.65 });
                            return;
                        }

                    });
                });

                $scope.$apply(function () {
                    $scope.ddldisplay = true;
                    $scope.SpcCount = result.Table1;
                    if ($scope.ddlItem != "shiftalloc") {
                        $scope.SubItems = result.Table1;
                    }
                });

                progress(0, '', false);
                reloadMarkers();
            }

        });

    }

    $scope.SDate = 1;

    $('input[type="radio"]').on('click', function (e) {
        if (this.value == 1) {
            for (var i = 0; i < $scope.selectedSpaces.length; i++) {
                if ($scope.selectedSpaces[i].STACHECK == 16 && ($scope.selectedSpaces[i].FROM_TIME == null || $scope.selectedSpaces[i].FROM_TIME == "")) {
                    $scope.selectedSpaces[i].FROM_DATE = moment().format('MM/DD/YYYY');
                    //$scope.selectedSpaces[i].TO_DATE = moment($scope.selectedSpaces[i].FROM_DATE).add(1, 'day').format('MM/DD/YYYY');
                    $scope.selectedSpaces[i].TO_DATE = moment().format('MM/DD/YYYY');
                }
            }
            $scope.gridSpaceAllocOptions.api.setRowData([]);
            setTimeout(function () {
                $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            }, 10);
        }

        else if (this.value == 2) {
            for (var i = 0; i < $scope.selectedSpaces.length; i++) {
                if ($scope.selectedSpaces[i].STACHECK == 16 && ($scope.selectedSpaces[i].FROM_TIME == null || $scope.selectedSpaces[i].FROM_TIME == "")) {
                    $scope.selectedSpaces[i].FROM_DATE = moment().format('MM/DD/YYYY');
                    $scope.selectedSpaces[i].TO_DATE = moment($scope.selectedSpaces[i].FROM_DATE).add(1, 'week').format('MM/DD/YYYY');

                }
            }

            $scope.gridSpaceAllocOptions.api.setRowData([]);
            setTimeout(function () {
                $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            }, 10);

        }

        else if (this.value == 3) {

            for (var i = 0; i < $scope.selectedSpaces.length; i++) {
                if ($scope.selectedSpaces[i].STACHECK == 16 && ($scope.selectedSpaces[i].FROM_TIME == null || $scope.selectedSpaces[i].FROM_TIME == "")) {
                    $scope.selectedSpaces[i].FROM_DATE = moment().format('MM/DD/YYYY');
                    $scope.selectedSpaces[i].TO_DATE = moment($scope.selectedSpaces[i].FROM_DATE).add(1, 'month').format('MM/DD/YYYY');
                }
            }
        }
        $scope.gridSpaceAllocOptions.api.setRowData([]);
        setTimeout(function () {
            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
        }, 10);
    });





    $scope.FilterData = function (filterdata, type) {
        var filterMarkers = $filter('filter')($scope.Markers, { STATUS: filterdata });
    }

    $('#divSeatStatus').on('shown.bs.collapse', function () {

        progress(0, '', true);
        if ($scope.SeatSummary == undefined) {
            updateSummaryCount();
            progress(0, 'Loading...', false);
        }
        else
            progress(0, 'Loading...', false);
    });

    $('#divSeatcap').on('shown.bs.collapse', function () {
        if ($scope.seatingCapacity == undefined)
            $scope.GetSeatingCapacity();
    });

    $('#divWorkarea').on('shown.bs.collapse', function () {
        if ($scope.SpcCountArea == undefined)
            $scope.GetSeatingArea();
    });

    $('#divotherarea').on('shown.bs.collapse', function () {
        if ($scope.Totalareadet == undefined)
            $scope.GetTotalAreaDetails()
    });


    $scope.GetEmpSeatDtlsHotDesk = function (obj) {
        MaploaderService.GetEmpSeatDtlsHotDesk(obj).then(function (response) {
            if (response.data != null && response.data1 != null) {
                //console.log(response.data2);
                $scope.LoggedInEmpDetails = response.data;
                $scope.StartPoints = response.data1;
                $scope.WayPoints = response.data2;
                $scope.GetBaysbyWing = response.data2;
            }
        });
    };

    function loadFn() {
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.get('passVal') == 'Y') {
            SPACE_ID = searchParams.get('spc_id')
            $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
        }
        else {
            $scope.GetEmpSeatDtlsHotDesk(SPACE_ID);
        }
    }

    loadFn();

    $scope.SelectAllocEmp = function (SelectedEmp) {
        $scope.LoadDet = [];
        $scope.SelectedID = "";
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: SelectedEmp, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.getAllocEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {
                $scope.LoadDet = response.data;
                setTimeout(function () {
                    $('#Searchdata').selectpicker('refresh');
                }, 200);
            }
        });
    }

    $scope.GetTotalAreaDetails = function () {
        $scope.Totalareadet = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetTotalAreaDetails(dataobj).then(function (result) {

            if (result.data) {
                $scope.Totalareadet = result.data;
                progress(0, '', false);
            }
        });
    }

    $scope.GetSeatingCapacity = function () {

        progress(0, 'Loading...', false);
        $scope.seatingCapacity = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };

        MaploaderService.GetSeatingCapacity(dataobj).then(function (result) {
            if (result.data) {
                $scope.seatingCapacity = result.data;
                progress(0, '', false);
            }
        });
    }

    $scope.GetSeatingArea = function () {
        progress(0, 'Loading...', false);
        $scope.seatingCapacity = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        var themeobj = { Item: 'spacetype', flr_code: $scope.SearchSpace.Floor[0].FLR_CODE };
        $.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsCount', themeobj, function (result) {
            $scope.$apply(function () {
                if ($scope.SpcCountArea.length == 0)
					$scope.SPACETYPE = [];
                    $scope.SpcCountArea = result.Table;
					for (var i in result.Table){
						var obj={};
						obj.SPACE_TYPE=result.Table[i].NAME;
						obj.sno=result.Table[i].CODE;
						$scope.SPACETYPE.push(obj);
					}
            });
        });
    }

    $scope.ShowEmponMap = function (SelectedID) {



        var marker = _.find($scope.Markers, { SPC_ID: SelectedID.SPC_ID });

        var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });

        var empMarker = L.marker(L.latLng(marker.lat, marker.lon), {
            icon: chairicon,
            draggable: false
        }).addTo(map);
        if (map.hasLayer($scope.MarkerLblLayerSpace))
            map.removeLayer($scope.MarkerLblLayerSpace);

        setTimeout(function () {
            if (empMarker) { // check
                map.removeLayer(empMarker); // remove
                if (!map.hasLayer($scope.MarkerLblLayerSpace)) {
                    map.addLayer($scope.MarkerLblLayerSpace);
                }
            }
        }, 20000);

        if ($scope.ZoomLvl >= map.getZoom()) {

            //map.fitBounds($scope.bounds);
            setTimeout(function () { map.setView(L.latLng(marker.lat, marker.lon), map.getZoom() + 2); }, 200);
        }
        setTimeout(function () { map.panTo(L.latLng(marker.lat, marker.lon)); }, 400);

        setTimeout(function () { marker.setStyle(VacantStyle) }, 500);
        setTimeout(function () { marker.setStyle(AllocateStyle) }, 1000);
        setTimeout(function () { marker.setStyle(OccupiedStyle) }, 1500);
        setTimeout(function () { marker.setStyle(VacantStyle) }, 2000);

        setTimeout(function () {
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle)
                    break;
                case 1002: marker.setStyle(AllocateStyle)
                    break;
                case 1004: marker.setStyle(OccupiedStyle)
                    break;
                case 1003: marker.setStyle(AllocateStyle)
                    break;
                case 1052: marker.setStyle(BlockStyle);
                    break;
                default: marker.setStyle(VacantStyle)
                    break;
            }
        }, 3000);

        var lcm_code = '', twr_code = '', flr_code = '';
        let searchParams = new URLSearchParams(window.location.search);
        if ($scope.SearchSpace.Location.length == 0) {
            lcm_code = searchParams.get('lcm_code')
        }
        if ($scope.SearchSpace.Tower.length == 0) {
            twr_code = searchParams.get('twr_code')
        }
        if ($scope.SearchSpace.Floor.length == 0) {
            flr_code = searchParams.get('flr_code')
        }
        var dataobj = {
            lcm_code: ($scope.SearchSpace.Location.length == 0 ? lcm_code : $scope.SearchSpace.Location[0].LCM_CODE),
            twr_code: ($scope.SearchSpace.Tower.length == 0 ? twr_code : $scope.SearchSpace.Tower[0].TWR_COD),
            flr_code: ($scope.SearchSpace.Floor.length == 0 ? flr_code : $scope.SearchSpace.Tower[0].FLR_CODE),
            Item: ($scope.SelectedID ? $scope.SelectedID.CODE : SelectedID.Code), mode: 2
        };

        MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
            if (result.data) {
                $scope.EmpDet = result.data[0];
                if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1)
                    $scope.FindPathOfSeat();
                progress(0, '', false);
            }
        });

        setTimeout(function () {
            $('#Searchdata').selectpicker('refresh');
        }, 200);

    }
    $scope.set_fontcolor = function (color) {
        return { 'color': color };
    }

    $scope.set_color = function (color) {
        return { 'background-color': color };
    }

    var zoomtreshold = 0;

    map.on('zoomend', function () {

        zoomtreshold = 0;
        map.addLayer($scope.MarkerMeetingLblLayer);

        if ($scope.ZoomLvl >= 14)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else if ($scope.ZoomLvl >= 12)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 3;
        else if ($scope.ZoomLvl >= 9)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;

        if ((map.getZoom() > $scope.ZoomLvl) && cnt == 0 && LOADLAYERS == 1) {
            progress(0, 'Loading...', true);
            setTimeout(function () {
                $scope.LoadMapLayers(GetParameterValues('flr_code'));
            }, 2);
        }

        var prefval = _.find($scope.SysPref, { SYSP_CODE: "Seats enable by role on map" });
        if (prefval == 0 || prefval == undefined) {
            setTimeout(function () {
                if (map.getZoom() < zoomtreshold) {
                    if (map.hasLayer($scope.MarkerLblLayerSpace)) {
                        map.removeLayer($scope.MarkerLblLayerSpace);
                    }
                    if (map.hasLayer($scope.MarkerLblLayer)) {
                        map.removeLayer($scope.MarkerLblLayer);
                    }
                    if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {
                        map.removeLayer($scope.MarkerLblLayerEmpDetails);
                    }

                }

                if (map.getZoom() >= zoomtreshold) {

                    if (map.hasLayer($scope.MarkerLblLayerSpace)) {
                        console.log("layer already added");
                    }
                    else if (map.hasLayer($scope.MarkerLblLayer)) {
                        console.log("layer already added");
                    }
                    else if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {
                        console.log("layer already added");
                    }
                    else {
                        map.addLayer($scope.MarkerLblLayerSpace);
                    }
                }
            }, 200);
        }


    });

    $scope.getShiftName = function (shcode) {
        var shift = _.find($scope.Shifts, { SH_CODE: shcode });
        if (shift)
            return shift.SH_NAME;
        return "";
    }

    var classHighlight = 'highlight';
    var $thumbs = $('.thumbnail').click(function (e) {
        e.preventDefault();
        $thumbs.removeClass(classHighlight);
        $(this).addClass(classHighlight);
    });

    function reloadMarkers() {

        $scope.Markerdata.clearLayers();

        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            var IN_ACTIVE = _.find($scope.InactivespaceidList, { IN_SPC_ID: marker.SPC_ID })
            switch (marker.STATUS) {
                case 1:
                    if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                        marker.setStyle(VacantStyle);
                    else if (IN_ACTIVE != undefined && IN_ACTIVE.IN_SPC_ID == marker.SPC_ID)
                        marker.setStyle(InactiveStyle);
                    break;
                case 1002:
                    if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                        marker.setStyle(AllocateStyle);
                    else if (IN_ACTIVE != undefined && IN_ACTIVE.IN_SPC_ID == marker.SPC_ID)
                        marker.setStyle(InactiveStyle);
                    break;
                case 1004:
                    if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                        marker.setStyle(OccupiedStyle);
                    else if (IN_ACTIVE != undefined && IN_ACTIVE.IN_SPC_ID == marker.SPC_ID)
                        marker.setStyle(InactiveStyle);
                    break;
                case 1003:
                    if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                        marker.setStyle(AllocateStyle);
                    else if (IN_ACTIVE != undefined && IN_ACTIVE.IN_SPC_ID == marker.SPC_ID)
                        marker.setStyle(InactiveStyle);
                    break;
                case 1052:
                    if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                        marker.setStyle(BlockStyle);
                    else if (IN_ACTIVE != undefined && IN_ACTIVE.IN_SPC_ID == marker.SPC_ID)
                        marker.setStyle(InactiveStyle);
                    break;
                default:
                    if (IN_ACTIVE == undefined)
                        marker.setStyle(VacantStyle);
                    else if (IN_ACTIVE != undefined && IN_ACTIVE.IN_SPC_ID == marker.SPC_ID)
                        marker.setStyle(InactiveStyle);
                    break;
            }
            if (marker.AUR_LEAVE_STATUS == 'Y') {
                switch (marker.AUR_LEAVE_STATUS) {
                    case 'Y': marker.setStyle(LeaveStyle);
                        break;
                }
            }

            if (marker.SPC_DESC != null && marker.SPC_DESC != undefined)
                var formattedString = marker.SPC_DESC.split(",").join("\n");
            else
                var formattedString = '';
            var k = formattedString.split("\n");
            var id = "";
            var id1 = "";
            var text = "";
            var fulltext = "";
            var str = " ";

            fulltext = BindMarkerText(formattedString, marker);


            if ($scope.SeatPopUpDetails.SYSP_VAL1 == 1) {
                var role = _.find($scope.GetRoleAndRM);
                if (role.AUR_ROLE == 6) {
                    if (marker.SSAD_AUR_ID == UserId || marker.STATUS == 1) {

                        if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                            marker.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);
                    }
                }
                else {
                    if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                        marker.bindLabel(fulltext.split(",").join("\n"), {
                            noHide: false,
                            direction: 'auto',
                            className: "my-label",
                            zIndexOffset: 2000
                        });
                }

            } else {
                if (marker.options.spacetype == "CHA" && IN_ACTIVE == undefined)
                    marker.bindLabel(fulltext.split(",").join("\n"), {
                        noHide: false,
                        direction: 'auto',
                        className: "my-label",
                        zIndexOffset: 2000
                    });
            }


            if (marker.options.spacetype == "CHA")
                $scope.Markerdata.addLayer(marker);

        });



    }


    $scope.GradeAllocStatus = function () {
        if ($scope.GradeAlloc.SYSP_VAL1 == 1) {

            MaploaderService.GradeAllocStatus().then(function (response) {
                if (response[0].Eligiblity != 'No') {
                    $scope.Grade_Allow_status = response[0].ALLOW_STATUS;
                    $scope.Allow_Remaining = response[0].REMAINING;
                    $scope.ORG_ALLOC_COUNT = response[0].ORG_ALLOC_COUNT;
                    $scope.Eligibility = response[0].Eligiblity;

                    $scope.Details = 'Your total limit is -' + $scope.ORG_ALLOC_COUNT + ',utilized -' + ($scope.ORG_ALLOC_COUNT - $scope.Allow_Remaining) + '  and can allocate more ' + $scope.Allow_Remaining + ' seats';

                    if (response != undefined) {
                        if ($scope.Grade_Allow_status == 'Not Allow') {
                            $scope.AllocateEnabledStatus = true;
                            $ngConfirm({
                                icon: 'fa fa-warning',
                                title: 'Allocation Warning',
                                content: 'You have exceeded maxmimum no of allocations.Current limit is ' + $scope.ORG_ALLOC_COUNT,
                                closeIcon: true,
                                closeIconClass: 'fa fa-close',
                                buttons: {
                                    close: function ($scope, button) {
                                    }
                                }
                            });
                        }
                        else {
                            if ($scope.Allow_Remaining >= $scope.selectedSpacesLength) {
                                $scope.AllocateEnabledStatus = false;
                            }
                            else {
                                $scope.AllocateEnabledStatus = true;
                            }
                            // $scope.AllocateEnabledStatus = false;
                        }
                    }
                    else {
                        console.log("error");
                    }
                } else {
                    $scope.AllocateEnabledStatus = false;
                }
            })

        }
    }

    /* Find Path */
    $scope.FindPathOfSeat = function (typ) {
        if (typ == 'C') {
            map.removeLayer(polyline4);
            map.removeLayer(polyline2);
            map.removeLayer(polyline3);
            polyline4 = {};
            polyline2 = {};
            polyline3 = {};
            map.removeLayer(empMarker);
            map.removeLayer(empMarker1);
        }

        var marker = _.find($scope.Markers, { SPC_ID: $scope.LoggedInEmpDetails[0].SPC_ID });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            if (layer.options.spacetype == 'UA' && (layer.options.spacesubtype == 'BY' || layer.options.spacesubtype == 'SP')) {
                $scope.UAMarkers.push(layer);
            }
        });

        var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });
        if (marker != undefined && marker != null) {
            if (typ != 'C') {
                empMarker = L.marker(L.latLng(marker.lat, marker.lon), {
                    icon: chairicon,
                    draggable: false
                }).addTo(map);

                var EmpPoint = new L.latLng(marker.lat, marker.lon);
                var BAY_LatLon = new L.LatLng($scope.StartPoints[1].X, $scope.StartPoints[1].Y);
                var StartPoint = new L.LatLng($scope.StartPoints[0].X, $scope.StartPoints[0].Y);

                var shapes = [];

                var BayPoint = $scope.StartPoints[1].SPC_NAME.trim();
                var SPC_FLR_ID = $scope.StartPoints[1].SPC_FLR_ID.trim();
                var WayPoints = $scope.WayPoints;
                var Way_LatLon;
                var firstshapes = [];

                var hide = [], bool = false;
                var cldArr = APT_CONST.WNGSARR
                for (var x in cldArr) {
                    if (SPC_FLR_ID == cldArr[x].FLRNAME) {
                        for (var o in cldArr[x].CHILDARR) {
                            var slct = cldArr[x].CHILDARR[o].SLCT.split(',');
                            for (var z in slct) {
                                if (BayPoint == slct[z]) {
                                    var hide = cldArr[x].CHILDARR[o].HDVALS.split(',');
                                    bool = true;
                                }
                            }
                        }
                    }
                }

                for (var i in $scope.WayPoints) {
                    if (i == 0) {
                        firstshapes.push(L.latLng($scope.WayPoints[i].X, $scope.WayPoints[i].Y));
                        firstshapes.push(L.latLng($scope.StartPoints[0].X, $scope.StartPoints[0].Y));
                    }
                    if ($scope.StartPoints[1].X == $scope.WayPoints[i].X && $scope.WayPoints[i].Y == $scope.StartPoints[1].Y) {

                    }
                    else if ($scope.StartPoints[0].X == $scope.WayPoints[i].X && $scope.WayPoints[i].Y == $scope.StartPoints[0].Y) {

                    }
                    else {
                        if (bool) {
                            var hBool = false;
                            for (var z in hide) {
                                if ($scope.WayPoints[i].SPC_NAME == hide[z]) {
                                    hBool = true;
                                }
                            }
                            if (!hBool) {
                                shapes.push(L.latLng($scope.WayPoints[i].X, $scope.WayPoints[i].Y));
                            }
                        }
                        else {
                            shapes.push(L.latLng($scope.WayPoints[i].X, $scope.WayPoints[i].Y));
                        }
                    }
                    if ($scope.WayPoints[i].SPC_SUB_TYPE == 'SP') {
                        shapes.push(L.latLng($scope.WayPoints[i].X, $scope.WayPoints[i].Y));
                    }

                    if (BayPoint == $scope.WayPoints[i].SPC_NAME) {
                        shapes.push(L.latLng($scope.WayPoints[i].X, $scope.WayPoints[i].Y));
                        break;
                    }
                }


                empMarker1 = L.marker(L.latLng($scope.StartPoints[0].X, $scope.StartPoints[0].Y), {
                    icon: chairicon,
                    draggable: false
                }).addTo(map);

                var pointList1 = [EmpPoint, BAY_LatLon];

                polyline2 = L.polyline(shapes).addTo(map);
                polyline3 = L.polyline(firstshapes).addTo(map);
                polyline4 = L.polyline(pointList1, {
                    color: 'red',
                    weight: 4,
                    opacity: .7,
                    dashArray: '10,5',
                    lineJoin: 'round'
                }).addTo(map);
                //polyline4 = L.polyline(pointList1, {color: 'red', weight: 4,}).addTo(map);
            }
        }
    }

    $scope.CloseFn = function () {
        $scope.FlipkartVaccinateName = ''; $scope.ActiveButton = false;
        $scope.selectSeatCount = 0;
        var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });
        var spcid = _.find($scope.Markers, { SPC_ID: $scope.selectedSpaces[0].SPC_ID });
        switch (spcid.STATUS) {
            case 1: chairicon = VacantStyle;
                break;
            case 1002: chairicon = AllocateStyle;
                break;
            case 1004:
                if ($scope.PartiallyOccupiedSys.SYSP_VAL1 == 1) {
                    if (spcid.COUNTS == 2)
                        chairicon = OccupiedStyle;
                    else
                        chairicon = PartiallyOccupiedStyle;
                }
                else {
                    chairicon = OccupiedStyle;
                }
                break;
            case 1003: chairicon = AllocateStyle;
                break;
            case 1052: chairicon = BlockStyle;
                break;
            default: chairicon = VacantStyle;
                break;
        }
        spcid.setStyle(chairicon);
        $scope.selectedSpaces = [];
    }

        function loadAmenities() {
        return $.ajax({
            url: '../../api/MaploaderAPI/GetAminities',
            contentType: "application/json; charset=utf-8",
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $('#checkboxContainer').empty();
                data.forEach(function (amenity) {
                    var rowDiv = $('<div>').addClass('amenities-row');
                    var checkboxCell = $('<div>').addClass('checkbox-cell');
                    var checkbox = $('<input>').attr({
                        type: 'checkbox',
                        value: amenity.AMINITY_ID
                    });
                    checkboxCell.append(checkbox);

                    var labelCell = $('<div>').addClass('amenities-cell').text(amenity.AMINITY);

                    var inputCell = $('<div>').addClass('input-cell');
                    var inputField = $('<input>').attr({
                        type: 'text',
                        class: 'form-control',
                        placeholder: 'Enter value for ' + amenity.AMINITY,
                        value: amenity.DESCRIPTION
                    });
                    inputCell.append(inputField);

                    rowDiv.append(checkboxCell).append(labelCell).append(inputCell);
                    $('#checkboxContainer').append(rowDiv);
                });
            },
            error: function (xhr, status, error) {
                console.error('Error:', error);
            }
        });
    }

    // Function to load amenities by space
    function loadAmenitiesBySpace(spaceId) {
        return $.ajax({
            url: '../../api/MaploaderAPI/GetAminitiesBySpace',
            data: { spaceId: spaceId },
            contentType: "application/json; charset=utf-8",
            type: 'GET',
            dataType: 'json',
            success: function (spaceAminities) {
                spaceAminities.forEach(function (spaceAmenity) {
                    var checkbox = $('#checkboxContainer input[value="' + spaceAmenity.AMINITY_ID + '"]');
                    var inputField = checkbox.closest('.amenities-row').find('input[type="text"]');
                    inputField.val(spaceAmenity.DESCRIPTION);

                    if (checkbox.length > 0) {
                        checkbox.prop('checked', true);
                    }
                });
            },
            error: function (xhr, status, error) {
                console.error('Error:', error);
            }
        });
    }

    // Call an API of Amenities on modal open
    $('#SeatAllocation').on('shown.bs.modal', function (e) {
        var colid = 'SPC_NAME';
        var elements = document.querySelectorAll('[colid="' + colid + '"]');
        var lastElement = elements[elements.length - 1];
        var selectedSpace = lastElement.textContent.trim();

        console.log("Selected space is", selectedSpace);
        if ($scope.enableamenities && $scope.enableamenities.SYSP_VAL1 == 1) {
            loadAmenities().then(function () {
                //return loadAmenitiesBySpace(selectedSpace);
            }).then(function () {
                $('#checkboxContainer').addClass('checkbox-wrapper');
            }).catch(function (error) {
                console.error('Error:', error);
            });
        }
    });

}]);


function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return decodeURIComponent(urlparam[1]);
        }
    }
}
