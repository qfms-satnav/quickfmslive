﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BreakDownMaintenance.aspx.cs" Inherits="SMViews_Map_BreakDownMaintenance" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS" data-ng-cloak>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%--   <link href="../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />--%>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: left;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            width: 400px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell {
            background-color: #dbdeed;
        }

        .ag-blue .ag-header-cell-label {
            color: black;
        }

        .bxheader {
            width: 100%;
            background: #000;
            padding: 5px 10px;
            margin-bottom: 10px;
        }

        .ag-blue .ag-row-odd {
            background-color: #fff;
        }

        .vb-accordion .accordion-button.collapsed {
            width: 100%;
            background: #63bbb2;
            color: white;
            font-size: 16px;
        }

        .vb-accordion .accordion-button:not(.collapsed) {
            background: #63bbb2;
            color: white;
            font-size: 16px;
        }

        .vb-accordion .accordion-button::after {
            content: "";
            position: absolute;
            background-image: url(../../Userprofiles/down-arrow.png) !important;
            background-size: contain !important;
            background-repeat: no-repeat;
            right: 10px;
            width: 20px;
            height: 20px;
        }

        .vb-accordion .accordion-body {
            padding: 0;
        }

            .vb-accordion .accordion-body .col-md-12 {
                padding: 2rem;
            }

                .vb-accordion .accordion-body .col-md-12 .col-md-3 {
                    padding-left: 0px;
                }
                .accordion-collapse.collapse{padding:15px;}

                 .cell-fail {
            text-align: left;
            font-weight: bold;
            background-color: red;
        }

        .cell-pass {
            text-align: left;
            font-weight: bold;
            background-color: #4caf50;
        }

        .cell-less {
            text-align: left;
            font-weight: bold;
            background-color: orange;
        }
    </style>
</head>
<%-- onload="setDateVals()"--%>
<body data-ng-controller="BreakdownUptController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="View Details" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;color:black;">
                            <h3 class="panel-title"><b>View and Update Breakdown</b></h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div data-ng-show="Viewstatus==0">
                               <%-- <form role="form" id="frmBreakdwn" name="frmbreakDown" data-valid-submit="UpdateAllRequests()" novalidate>--%>
                                    <div class="clearfix">
                                        
                                         <%--<div class="col-md-3 col-sm-6 col-xs-12" style="cursor: pointer;">
                                            <div class="form-group">
                                                <br />
                                                <input type="button" value="Search" class='btn btn-primary custom-button-color' data-ng-click="getBreakDownRqstsFn()" />
                                            </div>
                                        </div>--%>
                                    </div>
                                   
                                     <div class="row">
                                        <div class="col-md-12"></div>
                                    </div>
                                   <%-- <div class="clearfix">
                                        <div>
                                             <div class="bxheader">
                                                    <input id="ReqFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%;height:10%" />
                                              </div>
                                            <div data-ag-grid="MyrequisitonsOptions" style="height: 260px; width: 100%" class="ag-blue"></div>
                                        </div>
                                    </div>--%>
                               <%-- </form>--%>
                            </div>
                         <div data-ng-show="Viewstatus==1">
                             <%--data-valid-submit="SearchSpaces()"--%>
                                <form role="form" id="SearchSpaces" name="frmbreakdwn" novalidate>
                                   <%-- <div class="clearfix">--%>
<div class="accordion vb-accordion" id="accordionExample">
  <div class="accordion-item" id="disp">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
       Generate Ticket 
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
                <form role="form" id="frmBreakdwn" name="frmbreakDown" novalidate>
                                           
                                            
                                           

                                             <div class="row">
                                              <%--   <input type="hidden" id="hdsession" name="hdsession" />--%>
                                                 <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.LCM_NAME.$invalid}">
                                                <label class="control-label"><strong>Site Name</strong> <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Locationlst" data-output-model="frmbreakDown.Locationlst" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                    data-on-item-click="getEquipment()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                                <input type="text" data-ng-model="frmbreakDown.Locationlst[0]" name="LCM_NAME" style="display: none" required="" />
                                                <span id="Span2" class="error" data-ng-show="frmbreakdwn.$submitted && frmbreakdwn.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                                            </div>
                                        </div>
                                         <div class="col-md-3 col-sm-6 col-xs-12" style="cursor: pointer;">
                                            <div class="form-group" data-ng-class="{'has-error': frmBreakDown.$submitted && frmbreakDown.GROUP_NAME.$invalid}">
                                                <label class="control-label"><strong>Equipment</strong><span style="color: red;"><!--*--></span></label>
                                                <div isteven-multi-select data-input-model="Equipment" data-output-model="frmbreakDown.Equipment" data-button-label="icon GROUP_NAME" data-item-label="icon GROUP_NAME"
                                                    data-on-item-click="getBreakDownRqstsFn()" data-on-select-all="LCMChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                            </div>
                                        </div>
                                                 <div class="col-md-3 col-sm-12 col-xs-12" style="cursor: pointer;">
                                             <div class="form-group" >
                                                 <label>Sub Equipment</label>
                                                   <span style="color: red;">*</span>
                                                 <input id="SUB_EQUIP" type="text" name="SUB_EQUIP" autofocus class="form-control"
                                                     required="" data-ng-model="frmbreakDown.SUB_EQUIP" />&nbsp;
                                                 <span class="error" data-ng-show="frmbreakDown.$submitted && frmbreakDown.SUB_EQUIP.$invalid" style="color: red">Please enter sub-equipment </span>
                                                <%-- data-ng-pattern="/^[a-zA-Z0-9. ]*$/"--%>
                                             </div>
                                         </div>
 <div class="col-md-3 col-sm-6 col-xs-12" style="cursor: pointer;display:none;">
                                            <div class="form-group" data-ng-class="{'has-error': frmBreakDown.$submitted && frmbreakDown.CHC_TYPE_NAME.$invalid}">
                                                <label class="control-label"><strong>Problem Description</strong><span style="color: red;"><!--*--></span></label>
                                                <div isteven-multi-select data-input-model="Probdesc" data-output-model="frmbreakDown.Probdesc" data-button-label="icon CHC_TYPE_NAME" data-item-label="icon CHC_TYPE_NAME"
                                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                             
                                            </div>
                                        </div>
                                                 <div class="col-md-3 col-sm-12 col-xs-12" style="cursor: pointer;">
                                             <div class="form-group" >
                                                 <label>Problem Description</label>
                                                   <span style="color: red;">*</span>
                                                 <input id="OTHER_DESC" type="text" name="OTHER_DESC" autofocus class="form-control"
                                                     required="" data-ng-model="frmbreakDown.OTHER_DESC" data-ng-disabled="frmbreakDown.Probdesc[0].CHC_TYPE_NAME != 'OTHER ISSUE'" />&nbsp;
                                                 <span class="error" data-ng-show="frmbreakDown.$submitted && frmbreakDown.OTHER_DESC.$invalid" style="color: red">Please enter other description </span>
                                                 <%--data-ng-pattern="/^[a-zA-Z0-9. ]*$/"--%>
                                             </div>
                                         </div>
                                               
                                             </div>
                                             
                                                 
                   <div class="row">
                        
                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.UGC_NAME.$invalid}">
                                                <label for="txtcode"><strong>Priority </strong><span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="AAS_VED" data-output-model="frmbreakDown.AAS_VED" data-button-label="icon UGC_NAME" data-item-label="icon UGC_NAME maker"
                                                     data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                             
                                            </div>
                                        </div>
                          <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.IMP_NAME.$invalid}">
                                                <label for="txtcode"><strong>Type </strong><span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="IMPACT" data-output-model="frmbreakDown.IMPACT" data-button-label="icon IMP_NAME" data-item-label="icon IMP_NAME maker"
                                                     data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                             
                                            </div>
                                        </div>
                                               <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Priority</label>

                                                <br/>
                                                  <select style="width: 100%; height: 30px;" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-maxlength ng-touched" data-ng-model="frmBreakDown.AAS_VED" name="AAS_VED" id="veds">
                                                   
                                                       <option value="P0">P0</option>
                                                     <option value="P1">P1</option>
                                                     <option value="P2">P2</option>
                                                  </select>
                                                <input type="text"  name="AAS_VED" style="display: none" required />

                                                <span class="error" data-ng-show="frmBreakDown.$submitted && frmBreakDown.AAS_VED.$invalid" style="color: red">Please select VED </span>

                                            </div>
                                        </div>--%>
                       
                                             
    </div>
                   <button type="submit" class="btn btn-primary" style="margin: auto;
    text-align: center;
    width: 150px;
    display: block;" data-ng-click="UpdateAllRequests()">SUBMIT</button>

                   <div class="clearfix" style="padding-top:15px">
                                        <div>
                                             <div class="bxheader">
                                                    <input id="ReqFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%;height:10%" />
                                              </div>
                                            <div data-ag-grid="MyrequisitonsOptions" style="height: 260px; width: 100%" class="ag-blue"></div>
                                        </div>
                                    </div>


 

<%--    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="txtcode" style="display: none;">Mobile/Extension Number <span style="color: red;"></span></label>
              
                <asp:TextBox ID="txtMobile" MaxLength="12" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="txtcode" style="display: none;">Repeat Call</label>
             
                        <asp:DropDownList ID="ddlRepeatCalls" runat="server" Visible="false" AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true">
                        </asp:DropDownList>
                 
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <br />
                <asp:HiddenField ID="hdnReqid" runat="server" />
                <a id="ancReqid" visible="false" href="#" onclick="GetHistory()" runat="server"><%=hdnReqid.Value %></a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="txtcode" style="display: none;">Claim Amount<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Claim Amount"
                    ControlToValidate="txtclaimamount" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtclaimamount" runat="server" Visible="false" MaxLength="10" CssClass="form-control" Height="30%" TabIndex="15">0</asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="txtcode" style="display: none;">Estimated Cost</label>
                <asp:TextBox ID="txtEstCst" runat="server" MaxLength="10" Visible="false" CssClass="form-control" Height="30%" TabIndex="15">0</asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
             
                <asp:DropDownList ID="ddlfacility" runat="server" Visible="false" CssClass="form-control selectpicker" data-live-search="true">
                    <asp:ListItem Value="Select">Select</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>


    </div>--%>

    


    </form>
      </div>
    </div>
  </div>
  <div class="accordion-item" id="disp2" style="display:none;">
    <h2 class="accordion-header" id="headingTwo">
      <button id="btn2" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Ticket Details 
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
             <div class="row">

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Ticket No :</b> </label>
                                                {{Breakdownobject.REQ_ID}}
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Site Name:</b> </label>
                                                {{Breakdownobject.LCM_NAME}}
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3 col-sm-6 col-xs-12 " style="display:none;">
                                            <div class="form-group">
                                                <label><b>Equipment :</b> </label>
                                                {{Breakdownobject.SubCategory}}
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                            <div class="form-group">
                                                <label><b>Error Description :</b> </label>
                                                {{Breakdownobject.ErrorDesc}}
                                            </div>
                                        </div>
                                       
                                 <%--   </div>--%>

                    </div>
                                     <div class="row">

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                          <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <%--data-ng-disabled="pgVar.ErrorDescription[0].CHC_TYPE_NAME != 'OTHER ISSUE'"--%>
                                                    <label for="txtcode"><strong>Problem Description</strong></label>
                                                    <textarea name="OTHER_ERROR_DESCRIPTION" id="OTHER_ERROR_DESCRIPTION" 
                                                        class="form-control" data-ng-model="pgVar.OTHER_ERROR_DESCRIPTION" cols="40" rows="5" style="height: 35px !important;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.CTY_NAME.$invalid}">
                                                <label for="txtcode"><strong>Ticket Type</strong> <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="TicketType" data-output-model="VerReq.TicketType" data-button-label="icon PRI_TITLE" data-item-label="icon PRI_TITLE maker"
                                                    data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                                <input type="text" data-ng-model="VerReq.TicketType" name="PRI_TITLE" style="display: none" required="" />
                                                <span id="Span1" class="error" data-ng-show="frmbreakdwn.$submitted && frmbreakdwn.PRI_TITLE.$invalid " style="color: red">Please select ticket type </span>
                                            </div>
                                        </div>
                                         <div class="col-md-3 col-sm-6 col-xs-12" style="cursor: pointer;">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.GROUP_NAME.$invalid}">
                                                <label class="control-label"><strong>Equipment</strong><span style="color: red;"><!--*--></span></label>
                                                <div isteven-multi-select data-input-model="Equipment2" data-output-model="pgVar.Equipment2" data-button-label="icon GROUP_NAME" data-item-label="icon GROUP_NAME"
                                                    data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.LCM_NAME.$invalid}">
                                                <label for="txtcode"><strong>Urgency </strong><span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Urgency" data-output-model="pgVar.Urgency" data-button-label="icon UGC_NAME" data-item-label="icon UGC_NAME maker"
                                                    data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                             
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.TWR_NAME.$invalid}">
                                                <label for="Span3"><strong>Engineer </strong><span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Assignto" data-output-model="pgVar.Assignto" data-button-label="icon ASSIGNED_TO" data-item-label="icon ASSIGNED_TO maker"
                                                    data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="VerReq.selectedTowers" name="ASSIGNED_TO" style="display: none" required="" />
                                                <span id="Span3" class="error" data-ng-show="frmbreakdwn.$submitted && frmbreakdwn.TWR_NAME.$invalid" style="color: red">Please select assign to </span>
                                            </div>
                                        </div>
                                          <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                            <div class="form-group" data-ng-class="{'has-error': frmbreakdwn.$submitted && frmbreakdwn.CHC_TYPE_NAME.$invalid}">
                                                <label class="control-label"><strong>Error Description</strong><span style="color: red;"><!--*--></span></label>
                                                <div isteven-multi-select data-input-model="ErrorDescription" data-output-model="pgVar.ErrorDescription" data-button-label="icon CHC_TYPE_NAME" data-item-label="icon CHC_TYPE_NAME"
                                                    data-on-item-click="pgVar.getAssigntoFn('P',pgVar.ErrorDescription)" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                            </div>
                                        </div>
                                           </div>
                                 
      </div>
    </div>
  </div>
  <div class="accordion-item" id="disp3" style="display:none;">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        Update Ticket 
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="accordion-body">
   
                                        
                  <form role="form" id="frmDownTime" name="frmBreakDown" novalidate>                       
         <div class="row">
                                          
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                     <div class="bxheader">Down Time Detail</div>
                                        <div data-ag-grid="gridOptions4" class="ag-blue" style="height: 210px; width: auto"></div>
                                </div>
                                   
                                        </div>
                <div class="row">
                    
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmBreakDown.$submitted && frmBreakDown.FROM_DATE.$invalid}">
                                                <label class="control-label">
                                                    <strong>Start Date</strong>  <span style="color: red;"></span>
                                                </label>
                                                <div class="input-group date" style="width: 207px" id='fromdate'>
                                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="FROM_DATE" name="FROM_DATE"
                                                        data-ng-model="pgVar.FROM_DATE" data-ng-change="downTimeFn()" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmBreakDown.$submitted && frmBreakDown.FROM_DATE.$invalid}">
                                                <label class="control-label">
                                                    <strong>Start Time</strong>  <span style="color: red;"></span>
                                                </label>
                                                <div style="width: 100px">
                                                    <input type="time" class="form-control" placeholder="00:00" id="FROM_TIME" name="FROM_TIME"
                                                        data-ng-model="pgVar.FROM_TIME" data-ng-change="downTimeFn()" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmBreakDown.$submitted && frmBreakDown.END_DATE.$invalid}">
                                                <label class="control-label">
                                                    <strong>End Date </strong><span style="color: red;"></span>
                                                </label>
                                                <div class="input-group date" id='enddate' style="width: 207px">
                                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="END_DATE" name="END_DATE"
                                                        data-ng-model="pgVar.END_DATE" data-ng-change="downTimeFn()" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('enddate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmBreakDown.$submitted && frmBreakDown.END_DATE.$invalid}">
                                                <label class="control-label">
                                                    <strong>End Time </strong><span style="color: red;"></span>
                                                </label>
                                                <div style="width: 100px">
                                                    <input type="time" class="form-control" placeholder="00:00" id="END_TIME" name="END_TIME"
                                                        data-ng-model="pgVar.END_TIME" data-ng-change="downTimeFn()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                          <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <label for="txtcode"><strong>Total Down Time</strong></label>
                                                    <label style="color:red;">
                                                        <br />
                                                       <strong>  {{pgVar.ADOWNTIME || 0}} Minutes</strong>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <label for="txtcode"><strong>Current Down Time</strong></label>
                                                    <label>
                                                        <br />
                                                         {{pgVar.CDOWNTIME || 0}} Minutes
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                      <%--  <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <label for="txtcode"><strong>(Current+Total) Down Time</strong></label>
                                                    <label>
                                                        <br />
                                                         {{pgVar.DOWNTIME || 0}} Minutes
                                                    </label>
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <label for="txtcode"><strong>Effective Down Time(Minutes)</strong></label>
                                                    <input type="text" name="BDMP_DWNTIME_FACTOR" id="BDMP_DWNTIME_FACTOR" class="form-control" onkeypress="return isNumberKey(event)"
                                                        data-ng-model="pgVar.BDMP_DWNTIME_FACTOR" cols="40" rows="5" style="height: 35px !important;" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for="txtcode"></label>
                                              <button type="submit" class="btn btn-primary" style="text-align: center;display: block;" data-ng-click="SaveDownTime()">Add Downtime</button>

                                                </div>

                                        </div>
                                        

                                        
                                        
                                    
                                    </div>

                      <div class="row">
                          <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmBreakDown.$submitted && frmBreakDown.STA_TITLE.$invalid}">
                                                    <label class="control-label"><strong><span class="error" style="color: red;" data-ng-show="pgVar.showError.Status && pgVar.Status.length==0">Please select </span>Ticket Status</strong><span style="color: red;">*</span></label>
                                                    <div isteven-multi-select data-input-model="Status" data-output-model="pgVar.Status" data-button-label="icon STA_TITLE" data-item-label="icon STA_TITLE"
                                                        data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                    </div>
                                                </div>
                                            </div>
                          <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <label for="txtshift"><strong>Shift </strong></label>
                                                    <input name="ADD_SHIFT" id="ADD_SHIFT" class="form-control"
                                                        data-ng-model="pgVar.ADD_SHIFT"  onkeypress="return isNumberKey(event)" type="text" style="height: 35px !important;" />
                                                </div>
                                            </div>
                                        </div>
                          
                      </div>
         
                  </form>                 

      </div>
    </div>
  </div>
   <div class="accordion-item" id="disp4" style="display:none;">
    <h2 class="accordion-header" id="headingSix">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
        Details
      </button>
    </h2>
    <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
      <div class="accordion-body">
          <div class="row">
                                          <%--  <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>Root Cause</strong></label>
                                                        <textarea name="ROOT_CAUSE" id="ROOT_CAUSE" class="form-control" data-ng-model="pgVar.ROOT_CAUSE" cols="40" rows="5" style="height: 35px !important;"></textarea>
                                                    </div>
                                                </div>
                                            </div>--%>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>Corrective Action</strong> <span style="color: red;">*</span></label>
                                                        <textarea name="CORRECTION_ACTION" id="CORRECTION_ACTION" class="form-control" data-ng-model="pgVar.CORRECTION_ACTION" cols="40" rows="5" style="height: 35px !important;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>Preventive Action</strong> <span style="color: red;">*</span></label>
                                                        <textarea name="PREVENTIVE_ACTION" id="PREVENTIVE_ACTION" class="form-control" data-ng-model="pgVar.PREVENTIVE_ACTION" cols="40" rows="5" style="height: 35px !important;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>Shipment Damage</strong> <span style="color: red;">*</span></label>
                                                         <input name="PRODUCTION_IMPACT" id="PRODUCTION_IMPACT" class="form-control"
                                                        data-ng-model="pgVar.PRODUCTION_IMPACT"  onkeypress="return isNumberKey(event)" type="text" style="height: 35px !important;" />
                                                        <%--<textarea name="PRODUCTION_IMPACT" id="PRODUCTION_IMPACT" class="form-control" data-ng-model="pgVar.PRODUCTION_IMPACT" cols="40" rows="5" style="height: 35px !important;"></textarea>--%>
                                                    </div>
                                                </div>
                                            </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label"><strong>Problem Owner</strong><span style="color: red;"><!--*--></span></label>
                                                    <div isteven-multi-select data-input-model="ProblemOwner" data-output-model="pgVar.ProblemOwner" data-button-label="icon OWN_NAME" data-item-label="icon OWN_NAME"
                                                        data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
          <div class="row">
                                            
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>Other Problem Owner</strong></label>
                                                        <textarea name="OTHER_PROBLEM_OWNER" id="OTHER_PROBLEM_OWNER" class="form-control" data-ng-model="pgVar.OTHER_PROBLEM_OWNER" cols="40" rows="5"
                                                            style="height: 35px !important;" data-ng-disabled="pgVar.ProblemOwner[0].OWN_NAME != 'Others'"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>P2D Breach</strong></label>
                                                          <input name="ADD_BREACH" id="ADD_BREACH" class="form-control"
                                                        data-ng-model="pgVar.ADD_BREACH" onkeypress="return isNumberKey(event)" type="text" style="height: 35px !important;" />
                                                       <%-- <textarea name="ADD_BREACH" id="ADD_BREACH" class="form-control"
                                                            data-ng-model="pgVar.ADD_BREACH" cols="40" rows="5" style="height: 35px !important;" onkeypress="return isNumberKey(event)"></textarea>--%>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                      
                                      <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <label for="txtcode"><strong>Remarks</strong></label>
                                                    <textarea name="REMARKS" id="REMARKS" class="form-control" data-ng-model="pgVar.REMARKS" cols="40" rows="5" style="height: 35px !important;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12" style="width: 100px;">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    <strong>Attachment </strong>
                                                </label>
                                                <div style="width: 100px">
                                                    <%--<input type="file" id="file" onchange="angular.element(this).scope().fileNameChanged(this,'F')" data-ng-model="pgVar.FilePathArr[0].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx">--%>
                                                    <input type="file" id="file" onchange="angular.element(this).scope().fileNameChanged(this,'F')" 
                                                        data-ng-model="pgVar.FilePathArr[0].FilePath" multiple name="file"
                                                        accept=".png,.jpg,.xlsx,.pdf,.docx">
                                                </div>                                              
                                                <%--<img src="../../UploadFiles/Flipkart_Ekart_UAT.dbo/{{FilePathArr[0].FilePath}}" style="height: 150px;border-radius: 10px;" alt="" />--%>
                                            </div>
                                        </div>
                                     </div>
          <div class="row">
                 <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                <label for="txtcode"><strong>Problem Category </strong></label>
                                                <div isteven-multi-select data-input-model="FDNAME" data-output-model="pgVar.FDNAME" data-button-label="icon FD_NAME" data-item-label="icon FD_NAME maker"
                                                     data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                             
                                            </div>
                                            </div>

              <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>CPD Breach</strong></label>
                                                          <input name="ADDI_BREACH" id="ADDI_BREACH" class="form-control"
                                                        data-ng-model="pgVar.ADDI_BREACH" onkeypress="return isNumberKey(event)" type="text" style="height: 35px !important;" />
                                                       <%-- <textarea name="ADD_BREACH" id="ADD_BREACH" class="form-control"
                                                            data-ng-model="pgVar.ADD_BREACH" cols="40" rows="5" style="height: 35px !important;" onkeypress="return isNumberKey(event)"></textarea>--%>
                                                    </div>
                                                </div>
                                            </div>
              <div class="col-md-3 col-sm-6 col-xs-12" style="display:none;">
                                                <div class="form-group">
                                                    <input type="hidden" id="hdASSET" name="hdASSET" value="0" />                                                  
                                                </div>
                                            </div>
               
          </div>
      </div>
    </div>
  </div>

        <div class="accordion-item" id="disp5" style="display:none;">
    <h2 class="accordion-header" id="headingSeven">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSix">
        Add Spare Parts
      </button>
    </h2>
    <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
      <div class="accordion-body">
                                        
          <div class="clearfix">
               <div class="row">
                                          
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                     <div class="bxheader">Spare Parts Detail</div>
                                        <div data-ag-grid="gridOptions5" class="ag-blue" style="height: 210px; width: auto"></div>
                                </div>
                                   
                                        </div>
              <div class="row">
                               <%-- <div class="bxheader">
                                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                        </div>--%>
                   <div class="col-md-12 col-sm-12 col-xs-12">
                                     <div class="bxheader">Add Spare Parts</div>
                                        <div data-ag-grid="gridOptions3" class="ag-blue" style="height: 310px; width:auto"></div>
                       </div>
                                    </div>

               <div class="col-md-3 col-sm-3 col-xs-3 pull-left">                                            
                                                <div class="form-group">
                                                <label class="control-label">Requestor Remarks </label>
                                                <textarea rows="1" cols="15" name="SPARE_REM" data-ng-model="pgVar.SPARE_REM" class="form-control" placeholder="Requestor remarks"></textarea>
                                                   
                                            </div>
                                        </div>

                                        <%--<div class="clearfix" data-ng-if="pgVar.SpareParts.length>0 && !pgVar.activeDiv">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="width: 100%;">
                                            <div class="form-group">
                                                <div style="margin-top: 1px;">
                                                    <label for="txtcode" style="width: 97% !important;">
                                                        <div class="panel panel-default">
                                                            
                                                           
                                                                <div class="panel-body">
                                                                    <table id="tblempalloc" width="100%" class="table table-condensed table-bordered table-hover table-striped">
                                                                        <tr>
                                                                            <td width="30px;" align="left"></td>
                                                                            <td width="123px;" align="left">Item</td>
                                                                            <td width="53px;" align="left">Qty</td>
                                                                            <td width="53px;" align="left">Available Qty</td>
                                                                            <td width="53px;" align="left">Min Qty</td>
                                                                        </tr>
                                                                        <tr data-ng-repeat="sp in pgVar.SpareParts">
                                                                            <td>
                                                                                <input type="checkbox" data-ng-model="sp.VAL" data-ng-true-value="'Y'" data-ng-false-value="'N'" data-ng-click="pgVar.ChangeFn('C',$index)" />
                                                                            </td>
                                                                            <td>{{sp.AAS_SPAREPART_NAME}}</td>
                                                                            <td>
                                                                                <span data-ng-if="sp.VAL=='Y' && sp.AAS_SPAREPART_AVBQUANTITY!=sp.value" data-ng-click="sp.VAL=='Y' && pgVar.ChangeFn('P',$index)" style="font-size: 20px; border-radius: 10px; margin: 0px 10px 0px 0px; cursor: pointer;">+</span>
                                                                                {{sp.value>0?sp.value:''}}
                                                                    <span data-ng-if="sp.VAL=='Y' && sp.value>0" data-ng-click="sp.VAL=='Y' && pgVar.ChangeFn('M',$index)" style="font-size: 30px; border-radius: 10px; margin: 0px 0px 0px 10px; cursor: pointer;">-</span>
                                                                            </td>
                                                                            <td>{{sp.AAS_SPAREPART_AVBQUANTITY}}</td>
                                                                            <td>{{sp.AAS_SPAREPART_MINQUANTITY}}</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                           
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                    </div>
      </div>
    </div>
  </div>

    <div class="accordion-item" id="disp6" style="display:none;">
    <h2 class="accordion-header" id="headingfive">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapsefive">
        RCA
      </button>
    </h2>
    <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingfive" data-bs-parent="#accordionExample">
      <div class="accordion-body">
           <form role="form" id="frmBreakRCA" name="frmRCA" novalidate>
                                 
                                        <div class="row">
                                            
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <div style="margin-top: 1px;">
                                                        <label for="txtcode"><strong>RCA Description </strong></label>
                                                        <textarea name="ROOT_DESC" id="ROOT_DESC" class="form-control" data-ng-model="frmRCA.ROOT_DESC" cols="40" rows="5" style="height: 35px !important;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                              <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                <label for="txtcode"><strong>RCA Status </strong></label>
                                                <div isteven-multi-select data-input-model="RCA_Status" data-output-model="frmRCA.RCA_Status" data-button-label="icon RCASTATUS" data-item-label="icon RCASTATUS maker"
                                                     data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                             
                                            </div>
                                            </div>
                                              
                                          <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                <label class="control-label">
                                                    <strong>RCA Attachment </strong>
                                                </label>
                                                <div style="width: 100px">
                                                    <%--<input type="file" id="file" onchange="angular.element(this).scope().fileNameChanged(this,'F')" data-ng-model="pgVar.FilePathArr[0].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx">--%>
                                                    <input type="file" id="filer" onchange="angular.element(this).scope().fileNameChangedr(this,'F')" 
                                                        data-ng-model="frmRCA.FilePathArrr[0].FilePath" multiple name="file"
                                                        accept=".png,.jpg,.xlsx,.pdf,.docx">
                                                </div>                                              
                                                <%--<img src="../../UploadFiles/Flipkart_Ekart_UAT.dbo/{{FilePathArr[0].FilePath}}" style="height: 150px;border-radius: 10px;" alt="" />--%>
                                            </div>
                                            </div>
                                             <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                              <button type="submit" class="btn btn-primary" style="margin: auto;
    text-align: center;
    width: 150px;
    display: block;" data-ng-click="SaveRCA()">SUBMIT</button></div></div>
                                        </div>
                                        <div class="row">
                                          
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                     <div class="bxheader">RCA Description Detail</div>
                                        <div data-ag-grid="gridOptions2" class="ag-blue" style="height: 210px; width: auto"></div>
                                </div>
                                   
                                        </div>
                                    
               </form>
      </div>
    </div>
  </div>

    <div class="accordion-item" id="disp7" style="display:none;">
    <h2 class="accordion-header" id="headingEight">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
      Attachment  Details 
      </button>
    </h2>
    <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight" data-bs-parent="#accordionExample">
      <div class="accordion-body">
                                        <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12" id="dvImage">
                                            
                                        </div>
                                    </div>

      </div>
    </div>
  </div>

</div>
                                   
                                    
                                      
                  

                                    
                                      
                                    <div class="clearfix" id="disp8" style="display:none;">
                                        <div class="col-md-12 text-right">
                                            <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="setStatus()" />
                                            <%--<input type="submit" value="Back" class="btn btn-primary custom-button-color" data-ng-click="setStatus('D')" />--%>
                                        </div>
                                    </div>
                                    

                             </form>
 
                        </div>
                    </div>
                </div>
                     <div style="margin-top:15px;padding-left:20px;">
                        <a class="btn btn-danger homeButton" href="#" onclick="fnDetail('../../MaintenanceManagement/Views/MaintainanceDashboard.aspx')" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-home" aria-hidden="true"></i>HOME</a>
                        <a class="btn btn-danger homeButton" href="#" onclick="goBack();" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>
                    </div>
            </div>
        </div>
    </div>
  

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);       
        var UID = '<%= Session["UID"] %>';
        //function setDateVals() {
        //    $('#txtFromDate').datepicker({
        //        format: 'mm/dd/yyyy',
        //        autoclose: true,
        //        todayHighlight: true
        //    });           
        //}
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
                //autoclose: true,
                //todayHighlight: true,
                //endDate: 'today',
                //maxDate: 'today'
            });
        };
        $(document).ready(function () {
            <%--var companyid = '<%= Session["TENANT"] %>';           
            $('#hdsession').val(companyid);--%>
            $('#dispChild1').hide();
            $('#dispChild2').hide();
            $('#dispChild3').hide();
            var query = window.location.search==""?"":window.location.search.substring(1).split('=')[1];
            if (query == "") {
                $('#hdASSET').val('0');
            }
            else {
                $('#hdASSET').val(query);
            }
            
        });
        
        function fn_Hide1() {
            $('#dispChild1').hide();
        }
        function fn_Show1() {
            $('#dispChild1').show();
        }

        function fn_Hide2() {
            $('#dispChild2').hide();
        }
        function fn_Show2() {
            $('#dispChild2').show();
        }

        function fn_Hide3() {
            $('#dispChild3').hide();
        }
        function fn_Show3() {
            $('#dispChild3').show();
        }
        function fnDetail(url) {
            //progress(0, 'Loading...', true);
            window.location = url;
        }
        function goBack() {
            //window.history.back();           
            var strBackUrl = sessionStorage.getItem("strBackUrl");
            //sessionStorage.setItem("strBackUrl", "/AssetManagement/Reports/AssetSpareParts.aspx");
            window.location = strBackUrl;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 45 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function onlyNumeric(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else {
                    t.style.border = '1px solid green ';
                    return true;
                }
                if ((charCode > 47 && charCode < 58) || (charCode == 8 || charCode == 46 || charCode == 32)) {
                    t.style.border = '1px solid green ';
                    return true;
                }
                else {
                    t.style.border = '1px solid red';
                    //document.getElementById("errormsg").style.display = 
                    return false;
                }
            }
            catch (err) {
                alert(err.Description);
            }
        }
    </script>
    <script defer src="../Utility.js"></script>
    <script defer src="BreakDownMaintenance.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../Scripts/moment.min.js"></script>
   
</body>

</html>

