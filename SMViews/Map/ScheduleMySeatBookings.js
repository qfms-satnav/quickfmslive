﻿app.service("ScheduleMySeatBookingsService", function ($http, $q, UtilityService) {
    this.searchSpaces = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/SearchSpaces', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getShifts = function (selLocs) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/GetShifts', selLocs)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.allocateSeats = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/allocateSeats', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.allocateSeatsValidation = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/allocateSeatsValidation', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };



    this.spcAvailabilityByShift = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/SpcAvailabilityByShift', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getEmployeeSpaceDetails = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/getEmployeeSpaceDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsByREQID = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/GetSpaceDetailsByREQID', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getSpaceDaysRestriction = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.get(UtilityService.path + '/api/MaploaderAPI/getSpaceDaysRestriction')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});

app.controller('ScheduleMySeatBookingsController', ['$scope', '$q', 'UtilityService', 'ScheduleMySeatBookingsService', 'SpaceRequisitionService', '$filter', 'MaploaderService', function ($scope, $q, UtilityService, ScheduleMySeatBookingsService, SpaceRequisitionService, $filter, MaploaderService) {
    $scope.ScheduleMySeatBookings = {};
    $scope.frmScheduleMySeatBookings = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.MonthDates = [];
    $scope.AlterDays = [];
    $scope.Weekdays = [];
    $scope.SpaceIds = [];
    $scope.EmployeeDetails = [];
    $scope.SelectedDates = [];
    $scope.SelectAlterDays = false;
    $scope.SelectWeekly = false;
    $scope.SelectMonth = false;
    $scope.Form2Enable = false;
    $scope.SelectionType = false;
    $scope.Checing = false;
    $scope.Griddata = false;

    UtilityService.getSysPreferences().then(function (response) {

        if (response.data != null) {
            $scope.SysPref = response.data;
            $scope.AtikinsMulBooking = (_.find($scope.SysPref, { SYSP_CODE: "Atkins Multiple Seat booking" }) || {});
        }
    });

    UtilityService.getAurNames().then(function (response) {
        if (response.data != null) {
            $scope.AurNames = [];
            //$scope.AurNames.push({ "AUR_ID": "", "AUR_NAME": "", "Cost_Center_Code": "", "Cost_Center_Name": "", "VER_CODE": "", "VER_NAME": "" })
            angular.forEach(response.data, function (value, key) {
                $scope.AurNames.push({ "AUR_ID": value.AUR_ID, "AUR_NAME": value.AUR_NAME, "Cost_Center_Code": value.Cost_Center_Code, "Cost_Center_Name": value.Cost_Center_Name, "VER_CODE": value.VER_CODE, "VER_NAME": value.VER_NAME, ticked: false });
            })
        }
        $scope.EmployeeDetails = $scope.AurNames;
        _.forEach($scope.AurNames, function (o) {
            if (o.AUR_ID == aur_id) {
                return o.ticked = true;
                //$scope.EmployeeDetails.push(o);
            }
        })
    });

    UtilityService.GetRoleAndReportingManger().then(function (response) {
        if (response.data != null) {
            $scope.GetRoleAndRM = response.data;
        }
    });

    ScheduleMySeatBookingsService.getSpaceDaysRestriction().then(function (response) {

        if (response != null) {

            _.forEach(response, function (key, value) {
                if (key.Count > 0) {
                    switch (key.sno) {
                        case 1: $scope.enddate1selection = (key.Count);
                            break;
                        case 2: $scope.enddate2selection = (key.Count);
                            break;
                        case 4: $scope.enddate4selection = (key.Count);
                            break;
                    }
                }
                else {
                    switch (key.sno) {
                        case 1: $scope.enddate1selection = '';
                            break;
                        case 2: $scope.enddate2selection = '';
                            break;
                        case 4: $scope.enddate4selection = '';
                            break;
                    }
                }

            });

        }
    });

    $scope.columDefsAlloc = [
        //{ headerName: "Space ID", field: "SPC_ID", width: 150, cellClass: "grid-align", pinned: 'left' },
        // { headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME" },
        { headerName: "Delete", cellclass: "grid-align", suppressMenu: true, pinned: 'left', template: '<a ng-click="DeleteFromGrid(data)"><i class="fa fa fa-trash" style="color:red" aria-hidden="true"></i></a>', width: 50, cellStyle: { 'text-align': "center" } },
        { headerName: "From Time", field: "FROM_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createFromTimePicker },
        { headerName: "To Time", field: "TO_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createToTimePicker },
        {
            headerName: "Shift Type", field: "SH_CODE", width: 200, cellClass: "grid-align", filter: 'set',
            template: "<select data-ng-change='shiftChange(data)'  ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in RequiredShifts' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>"
        },
        { headerName: "From Date", field: "FROM_DATE", width: 150, cellClass: 'grid-align' },
        { headerName: "To Date", field: "TO_DATE", width: 150, cellClass: 'grid-align' }
    ];

    $scope.DeleteFromGrid = function (data) {

        var d = _.find($scope.FinalData, function (o) { return o.FROM_DATE == data.FROM_DATE && o.TO_DATE == data.TO_DATE; });
        d.ticked = false;
        $scope.gridOptions.api.setRowData([]);
        $scope.gridOptions.api.setRowData(_.filter(_.orderBy($scope.FinalData, 'FROM_DATE', 'asc'), function (o) { return o.ticked == true; }));


    };


    var details = function (data) {
        $scope.temp = data;
        var i = 0;
        var datefiltervals = [];
        while (i <= $scope.temp.length - 1) {

            var start = moment($scope.temp[i].Id);
            var end = moment($scope.temp[i].Id);
            var startDate = moment($scope.temp[i].Id);

            if (i < $scope.temp.length && i < $scope.temp.length - 1) {
                var nextDate = moment($scope.temp[i + 1].Id);
                while ((moment(startDate).add(1, 'days').format('MM/DD/YYYY') === nextDate.format('MM/DD/YYYY')) && i < $scope.temp.length - 1) {
                    i++;
                    startDate = moment($scope.temp[i].Id);
                    end = nextDate;
                    if (i < $scope.temp.length - 1) {
                        nextDate = moment($scope.temp[i + 1].Id);
                    }
                    else {
                        break;
                    }
                }
                datefiltervals.push({ "startDate": moment(start).format('MM/DD/YYYY'), "endDate": moment(end).format('MM/DD/YYYY') });
                i++;
            }
            else {
                datefiltervals.push({ "startDate": moment(start).format('MM/DD/YYYY'), "endDate": moment(end).format('MM/DD/YYYY') });
                i++;
            }
        }

        return datefiltervals;
    };


    $scope.gridOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false
    };

    $scope.Clear = function () {
        $scope.Griddata = false;

        $scope.ScheduleMySeatBookings.FROM_DATE = '';
        $scope.ScheduleMySeatBookings.TO_DATE = '';

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Floors, function (value, key) {
            value.ticked = false;
        });

    }


    $scope.binddata = function (data) {
        var selectedData;
        selectedData = _.find($scope.AurNames, { AUR_ID: $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID });
        if (selectedData != undefined) {
            $scope.AUR_ID = selectedData.AUR_ID;
            $scope.AUR_NAME = selectedData.AUR_NAME;
            $scope.VERTICAL = selectedData.VER_CODE;
            $scope.VER_NAME = selectedData.VER_NAME;
            $scope.Cost_Center_Code = selectedData.Cost_Center_Code;
            $scope.Cost_Center_Name = selectedData.Cost_Center_Name;
        }
    }


    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);

        UtilityService.getCountires(2).then(function (response) {

            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;


                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;

                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                progress(0, '', false);
                                            }
                                        });

                                    }
                                });
                            }
                        });

                    }
                });
            }
        });
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.ScheduleMySeatBookings.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });

    }

    $scope.locSelectAll = function () {
        $scope.ScheduleMySeatBookings.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.ScheduleMySeatBookings.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        $scope.ScheduleMySeatBookings.Locations = [];
        angular.forEach($scope.Towers, function (value, key) {

            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });

            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ScheduleMySeatBookings.Locations.push(lcm);

            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.ScheduleMySeatBookings.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {


        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });



        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ScheduleMySeatBookings.Locations.push(lcm);
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ScheduleMySeatBookings.Towers.push(twr);

            }
        });

        SpaceRequisitionService.getShifts($scope.ScheduleMySeatBookings.Locations).then(function (response) {
            $scope.Shifts = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.floorChangeAll = function () {
        $scope.ScheduleMySeatBookings.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    setTimeout(function () {
        $scope.LoadDetails();
        //$scope.ScheduleMySeatBookings.FROM_DATE = moment().format("DD/MM/YYYY");
        //$scope.ScheduleMySeatBookings.TO_DATE = moment().add(1,'M').format("DD/MM/YYYY");

    }, 200);

    $scope.SearchSpaces = function () {

        if (moment($scope.ScheduleMySeatBookings.FROM_DATE) > moment($scope.ScheduleMySeatBookings.TO_DATE)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else if ($scope.ScheduleMySeatBookings.FROM_DATE == undefined || $scope.ScheduleMySeatBookings.TO_DATE == undefined) {
            showNotification('error', 8, 'bottom-right', 'Select "From Date" and "To Date"');
        }
        else {
            progress(0, 'Loading...', true);
            floorslst = _.filter($scope.ScheduleMySeatBookings.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');

            var searchObj = { flrlst: floorslst, FROM_DATE: $scope.ScheduleMySeatBookings.FROM_DATE, TO_DATE: $scope.ScheduleMySeatBookings.TO_DATE };
            ScheduleMySeatBookingsService.searchSpaces(searchObj).then(function (response) {
                if (response != null) {
                    $scope.spaceDetails = response[1];
                    $scope.SpaceId = [];
                    _.forEach($scope.spaceDetails, function (key, value) {
                        $scope.SpaceId.push({ 'SPC_ID': key.SPC_ID });
                    });
                    $scope.SpaceIds = $scope.SpaceId;
                    $scope.Form2Enable = true;

                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
                progress(0, '', false);
            });
        }
    }

    $scope.CheckType = function (value) {
        $scope.Checing = true;
        if (value == 'AlterDays') {
            $scope.SelectAlterDays = true;
            $scope.SelectMonth = false;
            $scope.SelectWeekly = false;


            var enumerateDaysBetweenDates = function (startDate, endDate) {
                var dates = [];
                //startDate = moment(startDate).add(1, 'days');
                startDate = moment(startDate);
                endDate = moment(endDate).add(1, 'days');
                while (startDate.format('M/D/YYYY') !== endDate.format('M/D/YYYY')) {
                    dates.push({ "Id": startDate.format('M/D/YYYY'), "Date": startDate.format("ddd, MMM Do YYYY") });
                    startDate = moment(startDate).add(1, 'days');
                }
                return dates;
            };
            var date = enumerateDaysBetweenDates($scope.ScheduleMySeatBookings.FROM_DATE, $scope.ScheduleMySeatBookings.TO_DATE);
            $scope.AlterDays = date;
        }
        else if (value == 'Weekly') {
            $scope.SelectAlterDays = false;
            $scope.SelectMonth = false;
            $scope.SelectWeekly = true;
            $scope.Weeks = [{
                "Id": "Monday",
                "Name": "Monday"
            }, {
                "Id": "Tuesday",
                "Name": "Tuesday"
            },
            {
                "Id": "Wednesday",
                "Name": "Wednesday"
            }, {
                "Id": "Thursday",
                "Name": "Thursday"
            }, {
                "Id": "Friday",
                "Name": "Friday"
            }, {
                "Id": "Saturday",
                "Name": "Saturday"
            }, {
                "Id": "Sunday",
                "Name": "Sunday"
            }
            ];

            $scope.Weekdays = $scope.Weeks;
        }

        else {
            $scope.SelectAlterDays = false;
            $scope.SelectMonth = false;
            $scope.SelectWeekly = false;
        }
    }



    function getDifferenceInDays(date1, date2) {
        const diffInMs = Math.abs(date2 - date1);
        return (diffInMs / (1000 * 60 * 60 * 24)) + 1;
    }


    $scope.Check = function () {


        $scope.diff_bw_dates = getDifferenceInDays(new Date($scope.ScheduleMySeatBookings.FROM_DATE), new Date($scope.ScheduleMySeatBookings.TO_DATE));
        // console.log($scope.diff_bw_dates);

        if ($scope.SeatType.SPACE_TYPE == 1 && $scope.enddate1selection != '') {


            if ($scope.enddate1selection < $scope.diff_bw_dates) {
                showNotification('error', 8, 'bottom-right', 'Only dedicated seat can book ' + $scope.enddate1selection + ' days');
                $scope.Griddata = false;
                return;
            }

        }
        else if ($scope.SeatType.SPACE_TYPE == 2 && $scope.enddate2selection != '') {
            console.log($scope.SeatType.SPACE_TYPE);

            if ($scope.enddate2selection < $scope.diff_bw_dates) {
                showNotification('error', 8, 'bottom-right', 'Only Shared seat can book ' + $scope.enddate2selection + ' days');
                $scope.Griddata = false;
                return;
            }

        }
        else if ($scope.SeatType.SPACE_TYPE == 4 && $scope.enddate4selection != '') {
            console.log($scope.SeatType.SPACE_TYPE);

            if ($scope.enddate4selection < $scope.diff_bw_dates) {
                showNotification('error', 8, 'bottom-right', 'Only Hotdesk seat can book  ' + $scope.enddate4selection + ' days');
                $scope.Griddata = false;
                return;
            }

        }


        $scope.binddata($scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID);

        $scope.Griddata = true;
        $scope.FinalData = [];
        if ($scope.value == 'Daily') {
            var dates = [];
            $scope.FinalData.push({
                //"SPC_ID": $scope.selectedSpaceID, 
                "FROM_DATE": $scope.ScheduleMySeatBookings.FROM_DATE,
                "TO_DATE": $scope.ScheduleMySeatBookings.TO_DATE,
                //"AUR_NAME": $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID,
                ticked: true

            });
        }
        else if ($scope.value == 'Weekly') {
            if ($scope.ScheduleMySeatBookings.WeeklyDays.length != 0) {
                var FetchDatesBasedOnWeek = function (startDate, endDate) {
                    var dates = [];
                    var start = startDate, end = endDate;
                    _.forEach($scope.ScheduleMySeatBookings.WeeklyDays, function (key, value) {
                        var week = key.Id;
                        startDate = moment(start);
                        endDate = moment(end).add(1, 'days');
                        while (startDate.format('M/D/YYYY') !== endDate.format('M/D/YYYY')) {
                            if (week == moment(startDate).format('dddd')) {
                                dates.push({ "Sort": moment(startDate).format('M/D/YYYY'), "Id": startDate.format('M/D/YYYY'), "Date": startDate.format("ddd, MMM Do YYYY") });

                            }
                            startDate = moment(startDate).add(1, 'days');
                        }

                    });
                    return _.orderBy(dates, 'Id', 'asc');
                };

                $scope.SelectedDates = FetchDatesBasedOnWeek($scope.ScheduleMySeatBookings.FROM_DATE, $scope.ScheduleMySeatBookings.TO_DATE);
                $scope.FinalDates = details($scope.SelectedDates);
            }
            _.forEach($scope.FinalDates, function (key, value) {
                $scope.FinalData.push({
                    //"SPC_ID": $scope.selectedSpaceID, 
                    "FROM_DATE": key.startDate, "TO_DATE": key.endDate, ticked: true
                    //,"AUR_NAME": $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_NAME
                });
            });
        }

        else if ($scope.value == 'AlterDays') {
            $scope.FinalDates = details($scope.ScheduleMySeatBookings.AlterDays);


            _.forEach($scope.FinalDates, function (key, value) {
                $scope.FinalData.push({ //"SPC_ID": $scope.selectedSpaceID,
                    "FROM_DATE": key.startDate, "TO_DATE": key.endDate, ticked: true
                    //"AUR_NAME": $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_NAME
                });
            });
        }


        if ($scope.FinalData != null) {
            $scope.gridOptions.api.setRowData([]);
            $scope.gridOptions.api.setRowData(_.filter(_.orderBy($scope.FinalData, 'FROM_DATE', 'asc'), function (o) { return o.ticked == true; }));
            //$scope.gridOptions.api.setRowData(_.orderBy($scope.FinalData, 'FROM_DATE', 'asc'));
        }

    }
    var arr = [], bool = false;
    $scope.GetSpaceDetails = function () {

        var dataobj = { flr_code: $scope.ScheduleMySeatBookings.Floors[0].FLR_CODE, category: $scope.ScheduleMySeatBookings.SpaceId[0].SPC_ID };
        ScheduleMySeatBookingsService.GetSpaceDetailsByREQID(dataobj).then(function (response) {
            if (response != null) {
                if (response[0].AUR_ID != '') {
                    for (var i = 0; i < response.length; i++) {
                        // if (gridAlldata[i].SSA_SRNREQ_ID.length == 0)
                        arr.push({
                            FROM_DATE: response[i].FROM_DATE, TO_DATE: response[i].TO_DATE, AUR_ID: response[i].AUR_ID,
                            SH_CODE: response[i].SH_CODE, FROM_TIME: response[i].FROM_TIME, TO_TIME: response[i].TO_TIME
                        });

                    }
                }

                //angular.forEach(response, function (value, index) {
                //    // $scope.selectedSpaces.push(value);

                //    //if ($scope.selectedSpaces.length != 0) {
                //    //    //fetch all space ids
                //    //    var spaceids = $scope.selectedSpaces.map((item) => item.SPC_ID);

                //    //    // check if space id not exist then only it adds list
                //    //    //if (!spaceids.includes(value.SPC_ID)) {
                //    //    $scope.selectedSpaces.push(value);
                //    //    //}
                //    //}
                //    //else {
                //    //    $scope.selectedSpaces.push(value);
                //    //}
                //}
            }

        });
    }

    $scope.SpaceIdSelected = function () {
        $scope.RequiredShifts = [];
        $scope.Griddata = false;
        $scope.selectedSpaceID = $scope.ScheduleMySeatBookings.SpaceId[0].SPC_ID;
        $scope.SeatType = _.find($scope.spaceDetails, function (o) { return o.SPC_ID == $scope.selectedSpaceID });
        +        _.forEach($scope.Shifts, function (o) {
            if (o.SH_SEAT_TYPE == $scope.SeatType.SPACE_TYPE) {
                $scope.RequiredShifts.push(o);
            }

            if ($scope.SeatType.SPACE_TYPE == 4) {
                $scope.gridOptions.columnApi.setColumnVisible('SH_CODE', false);
                $scope.gridOptions.columnApi.setColumnVisible('FROM_TIME', true);
                $scope.gridOptions.columnApi.setColumnVisible('TO_TIME', true);
            }
            else {
                $scope.gridOptions.columnApi.setColumnVisible('SH_CODE', true);
                $scope.gridOptions.columnApi.setColumnVisible('FROM_TIME', false);
                $scope.gridOptions.columnApi.setColumnVisible('TO_TIME', false);
            }

        });


        $scope.SelectionType = true;
        $scope.GetSpaceDetails();
    };

    $scope.shiftChange = function (data) {
        data.SPC_ID = $scope.ScheduleMySeatBookings.SpaceId[0].SPC_ID;
        if (data.STATUS != 1 && (data.SSA_SRNREQ_ID != "" || data.FROM_DATE != "")) {
            ScheduleMySeatBookingsService.spcAvailabilityByShift(data).then(function (response) {
                if (response.data == null) {
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.EmployeeIdSelected = function () {
        _.forEach($scope.AurNames, function (o) {
            if (o.AUR_ID == $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID) {
                return o.ticked = true;
                //$scope.EmployeeDetails.push(o);
            }
        })

        var params = { AUR_ID: $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID };

        ScheduleMySeatBookingsService.getEmployeeSpaceDetails(params).then(function (response) {
            if (response[0].length != 0) {
                $scope.EmpAllocSeats = [];
                $scope.EmpAllocSeats = response[0];
            }
            else {
                $scope.EmpAllocSeats = [];
            }

        });


    }

    var number = 0;
    function checkDates() {
        number = 0;
        _.forEach($scope.EmpAllocSeats, function (key, value) {

            var from = new Date(key.FROM_DATE);
            var to = new Date(key.TO_DATE);
            _.forEach($scope.FinalData, function (key1, value1) {
                var fromcheck = new Date(key1.FROM_DATE);
                var tocheck = new Date(key1.TO_DATE);
                if ((fromcheck >= from && fromcheck <= to) || (tocheck >= from && tocheck <= to)) {
                    progress(0, '', false);
                    number = number + 1;
                }
            });

        });
    };


    $scope.scheduleSeats = function () {
        progress(0, 'Loading...', true);
        $scope.selectedData = [];
        checkDates();
        //console.log($scope.gridOptions.rowData);

        if ($scope.gridOptions.rowData.length != 0) {

            _.forEach($scope.gridOptions.rowData, function (o) {
                if (o.SH_CODE == undefined && $scope.SeatType.SPACE_TYPE != 4) {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Select Shift');
                    return;
                } else if ($scope.SeatType.SPACE_TYPE == 4 && (o.FROM_TIME == undefined || o.TO_TIME == undefined)) {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Select Shift Timings');
                    return;
                }
                else {
                    if ($scope.SeatType.SPACE_TYPE != 4) {
                        $scope.Shiftdata = (_.find($scope.Shifts, { SH_CODE: o.SH_CODE }) || {});
                        o.FROM_TIME = $scope.Shiftdata.SH_FRM_HRS;
                        o.TO_TIME = $scope.Shiftdata.SH_TO_HRS;
                    }
                    $scope.selectedData.push({
                        "FROM_DATE": o.FROM_DATE, "TO_DATE": o.TO_DATE, "SH_CODE": o.SH_CODE,
                        "SPC_ID": $scope.ScheduleMySeatBookings.SpaceId[0].SPC_ID, "AUR_ID": $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID,
                        "Cost_Center_Code": $scope.Cost_Center_Code,
                        "Cost_Center_Name": $scope.Cost_Center_Name, "SPACE_TYPE": $scope.SeatType.SPACE_TYPE, "STACHECK": 16, "STATUS": 1004,
                        "VERTICAL": $scope.VERTICAL, "VER_NAME": $scope.VER_NAME, "FROM_TIME": o.FROM_TIME, "TO_TIME": o.TO_TIME,
                    });
                }
            });


            if (number != 0) {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Dates chosen are already scheduled');
            }

            else if ($scope.selectedData.length != 0) {

                if ($scope.AtikinsMulBooking && $scope.AtikinsMulBooking.SYSP_VAL1 == 1) {
                    for (var j in arr) {
                        if (arr[j].FROM_TIME != undefined)
                            for (var x = 0; x < $scope.selectedData.length; x++) {

                                if (arr[j].AUR_ID != $scope.selectedData[x].AUR_ID) {

                                    var T_1 = parseInt(arr[j].FROM_TIME.split(":"));
                                    var T_2 = parseInt(arr[j].TO_TIME.split(":"));
                                    var T_3 = parseInt($scope.selectedData[x].FROM_TIME.split(":"));
                                    var T_4 = parseInt($scope.selectedData[x].TO_TIME.split(":"));

                                    var D_1 = arr[j].FROM_DATE.split("/");
                                    var D_2 = arr[j].TO_DATE.split("/");
                                    var D_3 = $scope.selectedData[x].FROM_DATE.split("/");
                                    var D_4 = $scope.selectedData[x].TO_DATE.split("/");

                                    var d1 = new Date(D_1[2], parseInt(D_1[1]) - 1, D_1[0]);
                                    var d2 = new Date(D_2[2], parseInt(D_2[1]) - 1, D_2[0]);
                                    var d3 = new Date(D_3[2], parseInt(D_3[1]) - 1, D_3[0]);
                                    var d4 = new Date(D_4[2], parseInt(D_4[1]) - 1, D_4[0]);
                                    //(gridAlldata[x].SH_CODE == arr[j].SH_CODE)
                                    if (((d3 >= d1 && d3 <= d2) || (d4 >= d1 && d4 <= d2)) && (T_3 >= T_1 && T_3 <= T_2) || (T_4 >= T_1 && T_4 <= T_2)) {
                                        progress(0, '', false);
                                        showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                                        $scope.ActiveButton = false;
                                        return;
                                    }
                                    if (((d1 >= d3 && d1 <= d4) || (d2 >= d3 && d2 <= d4)) && (T_1 >= T_3 && T_1 <= T_4) || (T_2 >= T_3 && T_2 <= T_4)) {
                                        progress(0, '', false);
                                        showNotification('error', 8, 'bottom-right', 'Since this seat is reserved for the day, Choose a different date or an other vacant seat.');
                                        $scope.ActiveButton = false;
                                        return;
                                    }
                                }
                            }
                    }
                    ScheduleMySeatBookingsService.allocateSeatsValidation($scope.selectedData).then(function (response1) {
                        if (response1.data == null) {
                            ScheduleMySeatBookingsService.allocateSeats($scope.selectedData).then(function (response) {
                                if (response != 1) {
                                    progress(0, 'Loading...', false);
                                    showNotification('error', 8, 'bottom-right', 'Something went wrong');
                                }
                                else {
                                    progress(0, 'Loading...', false);
                                    showNotification('success', 8, 'bottom-right', 'Selected Spaces Successfully Allocated ');
                                    $scope.Griddata = false;
                                    $scope.Form2Enable = false;
                                    arr = [];
                                }
                            });
                        } else {
                            progress(0, 'Loading...', false);
                            showNotification('error', 8, 'bottom-right', 'This shift was assigned to the employee');
                        }
                    })
                }
                else {
                    ScheduleMySeatBookingsService.allocateSeats($scope.selectedData).then(function (response) {
                        if (response == 1) {
                            progress(0, 'Loading...', false);
                            showNotification('success', 8, 'bottom-right', 'Selected Spaces Successfully Allocated ');
                            $scope.Griddata = false;
                            $scope.Form2Enable = false;
                            arr = [];
                        }
                        else if (response == 2) {
                            progress(0, 'Loading...', false);
                            showNotification('error', 8, 'bottom-right', 'Space Already Allocated to Others ');
                        }
                        else{
                            progress(0, 'Loading...', false);
                            showNotification('error', 8, 'bottom-right', 'Something went wrong, please try after some time');
                        }
                    });
                }
            }

        } else {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Select the dates');
        }
    }


    function createFromTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.FROM_TIME');
        newTime.type = "text";
        newTime.id = 'fromtime' + params.rowIndex;
        newTime.className = "pickDate form-control";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime = moment(e.target.value, 'HH:mm').add(total, 'hours').format('HH:mm');
                var selectedspaceexist = _.find($scope.selectedSpaces,
                    function (x) {
                        if (x.FROM_TIME === e.target.value) {

                            return x
                        }
                    });
                if (selectedspaceexist) {
                    showNotification('error', 8, 'bottom-right', "Time slot not available");
                    e.target.value = "";
                    return;
                }
                params.data.TO_TIME = endTime;
            });
        return newTime;
    }

    function createToTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.TO_TIME');
        newTime.type = "text";
        newTime.id = 'totime' + params.rowIndex;
        newTime.className = "pickDate form-control";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime1 = moment(params.data.FROM_TIME, 'HH:mm').add(total, 'hours').format('HH:mm');
                var currentTime = moment(e.target.value, "HH:mm a");
                var startTime = moment(params.data.FROM_TIME, "HH:mm a");
                var endTime = moment(endTime1, "HH:mm a");
                currentTime.toString();
                startTime.toString();
                var c = currentTime.isBetween(startTime, endTime);  // false
                if (!currentTime.isBetween(startTime, endTime)) {
                    showNotification('error', 8, 'bottom-right', "From time should not be shorter than to time, and business hours shouldn't be exceeded.");
                }
            });
        return newTime;
    }


}]);