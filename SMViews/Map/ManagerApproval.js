﻿app.service("ManagerApprovalService", function ($http, $q, UtilityService) {

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.put(UtilityService.path + '/api/ManagerApproval/Getapprovaldata')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.updatedata = function (ID) {
        var deferred = $q.defer();
        $http.post(UtilityService.path + '/api/ManagerApproval/updatedata?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.RejectFn = function (ID) {
        var deferred = $q.defer();
        $http.post(UtilityService.path + '/api/ManagerApproval/RejectFn?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
});


app.controller('ManagerApprovalController', function ($scope, $q, $http, ManagerApprovalService, UtilityService, $timeout, $filter) {



    //UtilityService.getSysPreferences().then(function (response) {
    //    if (response.data != null) {
    //        $scope.SysPref = response.data;
    //        console.log($scope.SysPref);
    //        $scope.VerticalWiseAlloc = _.find($scope.SysPref, { SYSP_CODE: "Booking Based on Vertical" });
    //        $scope.CostCenterWiseAlloc = _.find($scope.SysPref, { SYSP_CODE: "Booking Based on Costcenter" });
    //        $scope.Abbvallidations = (_.find($scope.SysPref, { SYSP_CODE: "Find Path" }) || {});
    //    }
    //});


    var a = [
        { headerName: "Location Name", field: "LOC_NAME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "Space ID", field: "SSMA_SPC_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="navToMap(data)">{{data.SSMA_SPC_ID}}</a>' },
        { headerName: "Employee Name", field: "EMP_NAME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "FROM DATE", field: "SSM_FROM_DATE", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "TO DATE", field: "SSMA_TO_DATE", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "FROM TIME", field: "SSMA_FROM_TIME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "TO TIME", field: "SSMA_TO_TIME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "CREATED DATE", field: "SSMA_CREATED_DT", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "CREATED BY", field: "SSMA_CRETED_BY", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "SEAT STATUS", field: "SEAT_STATUS", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "SSA SRNREQ ID", field: "SSMA_SRNREQ_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "SSAD STA ID", field: "SSMA_ALLOC_STAID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "SSAD SRN REQ ID", field: "SSMA_SRNCC_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "Approved", width: 100, field: "Approved ", cellClass: "grid-align", filter: 'set', template: '<a data-ng-click="Approvedfn(data)">Approved</a>', pinned: 'right', suppressMenu: true },
        { headerName: "Rejected", width: 100, field: "rejected ", cellClass: "grid-align", filter: 'set', template: '<a data-ng-click="rejectedfn(data,\'R\')">Rejected</a>', pinned: 'right', suppressMenu: true }
    ];
    $scope.gridOptions = {
        columnDefs: a,
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    }

    //$scope.navToMap = function (det) {
    //    if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1) {
    //        window.location.href = "../../../SMViews/Map/Maploader.aspx?lcm_code=" + det.SPC_BDG_ID + "&twr_code=" + det.SPC_TWR_ID + "&flr_code=" + det.SPC_FLR_ID + "&spc_id=" + det.Space_ID + "&value=1&passVal=Y"
    //    }
    //}--rejectedfn

    $scope.rejectedfn = function (data, typ) {
        ManagerApprovalService.updatedata(data.SSMA_ID).then(function (response) {
            if (response.Message != null) {
                getapprovaldat();
                if (typ == 'R') {
                    setTimeout(function () {
                        showNotification('success', 8, 'bottom-right', 'Rejected Succesfully.....!');
                    }, 100);
                }
            }
            else {
                showNotification('error', 8, 'bottom-right', 'Something went wrong');
            }
        })
    }

    $scope.UpdateFn = function (data, typ) {
        ManagerApprovalService.RejectFn(data.SSMA_ID).then(function (response) {
            if (response.Message != null) {
                getapprovaldat();
            }
            else {
                showNotification('error', 8, 'bottom-right', 'Something went wrong');
            }
        })
    }

    function insertdata(dataobj, det) {
        debugger;
        var obj = {
            selSpaces: dataobj,
            aminities: []
        };

        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/MaploaderAPI/AllocateSeats",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.data != null) {
                    progress(0, 'Loading...', false);
                    $scope.UpdateFn(det);
                    setTimeout(function () {
                        showNotification('success', 8, 'bottom-right', 'Seat booking Succesfully.....!');
                    }, 100);
                }
                else {
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', 'Something went wrong');
                }
            },
            error: function () {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Something went wrong');
            }
        });
    }

    function getapprovaldat() {
        ManagerApprovalService.GetGriddata().then(function (data) {
            $scope.gridata = data.griddata;
            //progress(0, 'Loading...', true);
            activeBtn = true;
            for (var i in $scope.gridata) {
                $scope.gridata[i].activeBtn = false;
                if ($scope.gridata[i].SSAD_STA_ID == "1004") {
                    $scope.gridata[i].activeBtn = true;
                }
                else {
                    $scope.gridata[i].activeBtn = false;
                }
            }
            if ($scope.gridata == null) {
                $scope.gridOptions.api.setColumnDefs(a);
                $scope.gridOptions.api.setRowData([]);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
                progress(0, '', false);
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            //progress(0, 'Loading...', false);
        });
    }

    $scope.Approvedfn = function (det) {
        progress(0, 'Loading...', true);
        var emp = det.EMP_NAME.split("/");
        var selectddata = [];
        selectddata.push({
            AUR_ID: emp[0].trim(),
            AUR_LEAVE_STATUS: 'N',
            AUR_LONG_LEAVE_FROM_DT: '',
            AUR_LONG_LEAVE_TO_DT: '',
            AUR_NAME: emp[1].trim(),
            Cost_Center_Code: det.Cost_Center_Code,
            Cost_Center_Name: det.Cost_Center_Name,
            FROM_DATE: det.SSM_FROM_DATE,
            TO_DATE: det.SSMA_TO_DATE,
            FROM_TIME: det.SSMA_FROM_TIME,
            TO_TIME: det.SSMA_TO_TIME,
            SH_CODE: det.SHIFT_COSE,
            STATUS: 1004,
            SPACE_TYPE: det.SPACE_TYPE,
            SPC_ID: det.SSMA_SPC_ID,
            SPC_NAME: det.SSMA_SPC_ID,
            SPC_TYPE_NAME: det.SPC_TYPE,
            SSAD_SRN_REQ_ID: det.SSMA_SRNREQ_ID,
            SSA_SRNREQ_ID: det.SSMA_SRNCC_ID,
            SST_NAME: det.SPC_TYPE,
            STACHECK: 16,
            VERTICAL: det.VER_CODE,
            VER_NAME: det.VER_NAME,
            block: true,
            blocked: false,
            disabled: false,
            ticked: true,
            emp: 'MANAGER',
        });

        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/MaploaderAPI/allocateSeatsValidation",
            data: JSON.stringify(selectddata),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.data == null) { insertdata(selectddata, det); }
                else
                    showNotification('error', 8, 'bottom-right', 'Seat already booking your selected dates');
            },
            error: function () {
                showNotification('error', 8, 'bottom-right', 'Something went wrong');
            }
        });
        getapprovaldat();
    }

    function loaddata() {
        getapprovaldat();
    };
    loaddata();


});
