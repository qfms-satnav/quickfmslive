﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" data-ng-app="QuickFMS">
<head runat="server">
    <title></title>
    <style type="text/css">
        #map {
            height: 400px;
        }

        .leaflet-google-layer {
            z-index: 0 !important;
        }

        .leaflet-map-pane {
            z-index: 100;
        }

        #floating-panel {
            position: absolute;
            bottom: 10px;
            left: 5%;
            z-index: 5;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }
    </style>
    <script defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjSNcIFQx2vFKMvglNpWg_79s1E0z_gcA"></script>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body data-ng-controller="MapMarkerController">


    <%-- <script defer type='text/javascript'>
        var map = new L.Map('Div1', { center: new L.LatLng(51.51, -0.11), zoom: 9 });
        var googleLayer = new L.Google('ROADMAP');
        map.addLayer(googleLayer);
    </script>--%>


    <div class="animsition">

        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Asset Type Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">View Locations</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <%-- <div id="floating-panel">
                                <div class="form-group">
                                    <label>Filter By Location</label>
                                    
                                    <input autocomplete ng-model="selected" />
                                </div>
                            </div>--%>
                                <div class="col-md-9">
                                    <div id="map"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="list-group">
                                        <a href="" class="list-group-item" ng-class="list-group-item-danger">
                                            <div class="pull-left"><strong>City Wise Details </strong></div>
                                            <div class="pull-right" ng-click="showMarkers()"><span class="fa fa-refresh"></span></div>
                                            <div class="clearfix"></div>
                                        </a>
                                        <a href="" class="list-group-item" ng-class="{'list-group-item-success' : $index%2==0, 'list-group-item-warning' : $index % 2!=0}"
                                            ng-click="CityClick(cty)" name="Citylst" ng-repeat="cty in CityLst">{{cty.CTY_NAME}}
                                        <span class="badge" ng-bind="cty.LCMCNT" ng-init="0"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', []);
    </script>
    <script defer src="../Utility.min.js"></script>
    <script defer src="MapMarker.min.js"></script>
</body>
</html>
