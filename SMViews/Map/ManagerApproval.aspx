﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <style>
        .grid-align {
            text-align: center;
        }
        
    </style>
    
</head>
    
<body data-ng-controller="ManagerApprovalController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Space Seat Book" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Manager Approval Space Booking</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="row" style="padding-left: 30px">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
   <%-- <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>--%>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../Utility.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="ManagerApproval.js"></script>
</body>
</html>

