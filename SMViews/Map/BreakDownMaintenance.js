﻿app.service('BreakDownUptService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    //this.getLocations = function (result) {
    //    var deferred = $q.defer();
    //    return $http.get(UtilityService.path + '/api/BreakDownUpt/getLocations')
    //        .then(function (response) {
    //            deferred.resolve(response.data);
    //            return deferred.promise;
    //        }, function (response) {
    //            deferred.reject(response);
    //            return deferred.promise;
    //        });
    //};
    this.getImpact = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getImpact')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getRCAStatus = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getRCAStatus')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getStartus = function (result) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getStartus')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getProblemCategory = function (result) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getProblemCategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getTicketType = function (result) {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getTicketType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getEquipment = function (BLDG) {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getEquipment?BLDG=' + BLDG + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getErrorDesc = function (equipment) {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getErrorDesc?equipment=' + equipment + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getErrorDescription = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/getErrorDescription', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getAssignto = function (vals) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/getAssignto', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getSpareParts = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/getSpareParts', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getBreakDownRqsts = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/getBreakDownRqsts', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getUrgency = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getUrgency')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.save = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/save', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveDownTime = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/SaveDownTime', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteDownTime = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/DeleteDownTime', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteRCA = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/DeleteRCA', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteSpare = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/DeleteSpare', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveRCA = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/SaveRCA', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.submit = function (vals) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/submit', vals)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getProblemOwner = function (result) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BreakDownUpt/getProblemOwner')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getRCAData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/getRCAData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getDownTimeData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/getDownTimeData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getSpareData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/getSpareData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetBreakDownDetails = function (result) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/GetBreakDownDetails', result)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetDescrption = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BreakDownUpt/GetDescrption', data)
            .then(function (response) {
                deferred.resolve(response.data);
                console.log(response)
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                console.log(response)
                return deferred.promise;
            });
    };
}]);



app.controller('BreakdownUptController', ['$scope', 'UtilityService', '$filter', 'BreakDownUptService', function ($scope, UtilityService, $filter, BreakDownUptService) {
    //$scope.paramValue = $location.search();
    $scope.REQASSETID = "0";
    $scope.Locationlst = [];
    $scope.Equipment = [];
    $scope.TicketType = [];
    $scope.Viewstatus = 1;
    $scope.Breakdownobject = {};

    $scope.ErrorDescription = [];
    $scope.Assignto = [];
    $scope.Urgency = [];
    $scope.Status = [];
    $scope.FDNAME = [];
    $scope.FilePathArr = [];
    $scope.FilePathArrr = [];
    var hdsession = "";
   
    //const cellClassRules = {
    //    "cell-pass": params => params.value != 'Open',
    //    "cell-fail": params => params.value == 'Open'
    //};

    const cellClassRules = {
        "cell-pass": params => params.value == 'Reviewed and Closed',
        "cell-less": params => params.value == 'In Progress',
        "cell-fail": params => params.value == 'Open'
    };

    var MyrequisitonsDefs = [
        { headerName: "Ticket No", field: "BDMP_PLAN_ID", width: 200, cellClass: "grid-align", filter: 'set', template: '<a ng-click="showDivFn(data.BDMP_PLAN_ID)" style="cursor: pointer;" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">{{data.BDMP_PLAN_ID}}</a>' },
        // { headerName: "Description", field: "ErrorDesc", width: 250, cellClass: 'grid-align' },
        { headerName: "Equipment", field: "SubCategory", width: 200, cellClass: 'grid-align' },
        { headerName: "Description", field: "BDMP_OTHERDESC", width: 250, cellClass: 'grid-align' },
        { headerName: "Status", field: "STA_TITLE", width: 200, cellClass: 'grid-align', cellClassRules: cellClassRules },
        { headerName: "Site Name", field: "LCM_NAME", width: 200, cellClass: 'grid-align', hide: true },       
        { headerName: "Loccode", field: "LCM_CODE", width: 200, cellClass: 'grid-align', suppressMenu: true, hide: true },
        { headerName: "Childcode", field: "CHC_TYPE_CODE", width: 200, cellClass: 'grid-align', suppressMenu: true, hide: true },
        { headerName: "Subcode", field: "SUBC_CODE", width: 200, cellClass: 'grid-align', suppressMenu: true, hide: true },

    ];


    var columnDefs2 = [
        { headerName: "Delete", width: 60, template: '<a ng-click = "DeleteRCA(data.SR_NO)"> <i class="fa fa-trash" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
        { headerName: "RCA Description", field: "RCA_DESCRIPTION", width: 500, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "RCA Status", field: "RCA_STATUS", width: 200, cellClass: 'grid-align', cellRenderer: tooltip },
        //{ headerName: "Root Attachment", field: "RCAPATH", width: 200, cellClass: 'grid-align', cellRenderer: tooltip, filter: 'set', template: '<img style="width:30px;height:30px;border-radius:5%" src="../../UploadFiles/Flipkart_Ekart_UAT_DEV.dbo/{{data.RCAPATH}}" />' },
        //{ headerName: "Root Attachment", field: "RCAPATH", width: 200, cellClass: 'grid-align', cellRenderer: tooltip, filter: 'set', template: '{{ data.RCAPATH }}'.split('.')[1] == "xlsx" || '{{ data.RCAPATH }}'.split('.')[1] == "pdf" || '{{ data.RCAPATH }}'.split('.')[1] == "docx" ? '<a href="../../UploadFiles/Flipkart_Ekart_UAT_DEV.dbo/{{data.RCAPATH}}" title="File">{{ data.RCAPATH }}</a>':'<img style="width:30px;height:30px;border-radius:5%" src="../../UploadFiles/Flipkart_Ekart_UAT_DEV.dbo/{{data.RCAPATH}}" />' },

        //{ headerName: "RCA Attachment", field: "RCAPATH", width: 200, cellClass: 'grid-align', cellRenderer: tooltip, filter: 'set', template: '<a href="../../UploadFiles/Flipkart_Ekart_UAT_DEV.dbo/{{data.RCAPATH}}" title="File">{{ data.RCAPATH }}</a>' },
        //{ headerName: "RCA Attachment", field: "RCAPATH", width: 200, cellClass: 'grid-align', cellRenderer: tooltip, filter: 'set', template: "{{fn_SetLink(data.RCAPATH)}}" },
        //{ headerName: "RCA Attachment", field: "RCARepPath", width: 200, cellRenderer: CallsCellRenderer },
        { headerName: "RCA Attachment", field: "RCA_PATH", width: 200, cellClass: 'grid-align', cellRenderer: tooltip, filter: 'set', template: '<a href="../../UploadFiles/{{data.RCA_PATH}}" title="File">{{ data.RCAPATH }}</a>' },

    ];
    function tooltip(params) {
        return `<label title="${params.value}" >${params.value}</label> `
    }

    //$scope.fn_SetLink = function (RCAPATH) {
    //    var link = '<a title="File" class="ng-binding" href="' + hdsession + '/' + RCAPATH + '" title="File">' + RCAPATH + '</a>';
    //    return link;
    //    //return link.replace(/&lt;/g, '<').replace(/&gt;/g,'>');

    //}
    
    $scope.gridOptions2 = {
        columnDefs: columnDefs2,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableScrollbars: false,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions2.api.sizeColumnsToFit();            
        }
    };

    $scope.columnDefs3 = [
        { headerName: "Select", field: "ticked", suppressMenu: true, width: 50, filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' />", cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align', width: 145 },
        { headerName: "Spare Name", field: "AAS_SPAREPART_NAME", width: 150, cellClass: 'grid-align', width: 145 },
        { headerName: "Spare Description", field: "AAS_SPAREPART_DES", cellClass: 'grid-align', width: 160 },
        { headerName: "Vendor", field: "AAS_AAT_CODE", cellClass: 'grid-align', width: 150 },
        { headerName: "Available Qty", field: "AAS_SPAREPART_AVBQUANTITY", cellClass: 'grid-align', width: 120 },
        { headerName: "Quantity", field: "Qty", width: 110, template: "<input type='number' ng-model='data.Qty' />", },
        //{ headerName: "SpareCost", field: "Cost", width: 150, template: "<input type='number' min='1' ng-model='data.Cost' readonly />", },
        { headerName: "Spare Cost", field: "Cost", cellClass: 'grid-align', width: 100, template: "{{fn_SetVal(data.AAS_SPAREPART_COST,data.Qty)}}", },
        { headerName: "Spare ID", field: "AAS_SPAREPART_ID", cellClass: 'grid-align', width: 100, hide: "true" },
        //{ headerName: "Cost", field: "AAS_SPAREPART_COST", cellClass: 'grid-align', width: 150, hide: "true" },
    ]


    $scope.fn_SetVal = function (cost, qty) {
        return cost == "" ? 0 : parseFloat(cost * qty).toFixed(2);

    }

    $scope.gridOptions3 = {
        columnDefs: $scope.columnDefs3,
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions3.api.sizeColumnsToFit()
        },

    }

    $("[type='number']").keypress(function (evt) {
        evt.preventDefault();
    });


    var columnDefs4 = [
        { headerName: "Delete", width: 60, template: '<a ng-click = "DeleteDownTime(data.SER_DOWN_ID)"> <i class="fa fa-trash" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Start Date", field: "BDMP_PLAN_FDATE", width: 180, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "Start Time", field: "FromTime", width: 100, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "End Date", field: "BDMP_PLAN_TDATE", width: 180, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "End Time", field: "ToTime", width: 100, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "Total Downtime", field: "BDMP_DOWNTIME", width: 180, cellClass: 'grid-align', cellRenderer: tooltip },
        //{ headerName: "Effective Downtime", field: "BDMP_DWNTIME_FACTOR", width: 180, cellClass: 'grid-align', cellRenderer: tooltip },
    ];
    $scope.gridOptions4 = {
        columnDefs: columnDefs4,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableScrollbars: false,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions4.api.sizeColumnsToFit();
        }
    };


    var columnDefs5 = [
        { headerName: "Delete", width: 60, template: '<a ng-click = "DeleteSpare(data.BDMPD_SNO)"> <i class="fa fa-trash" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Spare Name", field: "AAS_SPAREPART_NAME", width: 180, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "Spare Description", field: "AAS_SPAREPART_DES", width: 230, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "Cost", field: "COST", width: 120, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "Quantity", field: "QTY", width: 110, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "Total Cost", field: "TOTALCOST", width: 180, cellClass: 'grid-align', cellRenderer: tooltip },
        { headerName: "Date", field: "BDMPD_CREATED_DT", width: 100, cellClass: 'grid-align', cellRenderer: tooltip },
    ];
    $scope.gridOptions5 = {
        columnDefs: columnDefs5,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableScrollbars: false,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions5.api.sizeColumnsToFit();
        }
    };


    $scope.GetDescrptionValue = function () {

        var obj2 = {
            LCM_NAME: $scope.frmbreakDown.Locationlst[0].LCM_CODE,
            AAS_AAT_CODE: $scope.frmbreakDown.Equipment[0].GROUP_ID,
            //LCM_NAME: $scope.frmbreakDown.Locationlst[0].LCM_CODE,
            //AAS_AAT_CODE: $scope.pgVar.Equipment2[0].GROUP_ID
            //LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
            //AAS_AAT_CODE: _.filter($scope.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_CODE; }).join(',')
        }

        BreakDownUptService.GetDescrption(obj2).then(function (response) {
            if (response == undefined) {
                progress(0, 'Loading...', false);
                $scope.gridOptions3.api.setRowData([]);
            }
            else {
                var dta = angular.copy(response);
                $scope.gridOptions3.api.setRowData(dta);
                progress(0, 'Loading...', false);
            }
            //if (response.data.length != 0) {
            //    $scope.consume20 = true;
            //    $scope.gridOptions3.api.setRowData(response.data);
            //}
            //else {
            //    $scope.consume20 = false;
            //    showNotification('error', 8, 'bottom-right', 'no data for selected equipment');
            //}

        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.GetRCADataValue = function (dta) {
        var obj = {
            REQID: dta
        }
        BreakDownUptService.getRCAData(obj).then(function (response) {
            //if (response.data2 != null) {
            //    $scope.gridOptions2.api.setColumnDefs(columnDefs2);
            //    $scope.gridOptions2.api.setRowData(response.data2);
            //}
            //else {
            //    showNotification('error', 8, 'bottom-right', response.Message);
            //    $scope.gridOptions2.api.setColumnDefs(columnDefs2);
            //    $scope.gridOptions2.api.setRowData([]);
            //}

            if (response == undefined) {
                progress(0, 'Loading...', false);
                $scope.gridOptions2.api.setRowData([]);
            }
            else {
                var dtaa = angular.copy(response);
                $scope.gridOptions2.api.setRowData(dtaa);
                progress(0, 'Loading...', false);
            }
            //$scope.GetDescrptionValue();
            $scope.GetSpareDataValue(dta);
        }, function (response) {
            progress(0, '', false);

        });
        // $scope.LoadData();

    }

    $scope.GetDownTimeDataValue = function (dta) {
        var obj = {
            REQID: dta
        }
        BreakDownUptService.getDownTimeData(obj).then(function (response) {
            //if (response.data2 != null) {
            //    $scope.gridOptions2.api.setColumnDefs(columnDefs2);
            //    $scope.gridOptions2.api.setRowData(response.data2);
            //}
            //else {
            //    showNotification('error', 8, 'bottom-right', response.Message);
            //    $scope.gridOptions2.api.setColumnDefs(columnDefs2);
            //    $scope.gridOptions2.api.setRowData([]);
            //}

            if (response == undefined) {
                progress(0, 'Loading...', false);
                $scope.gridOptions4.api.setRowData([]);
            }
            else {
                var dtaa = angular.copy(response.data);
                $scope.gridOptions4.api.setRowData(dtaa);
                var dtaa2 = angular.copy(response.data2);
                $scope.pgVar.ADOWNTIME = dtaa2[0].BDMP_DOWNTIME;
                $scope.pgVar.BDMP_DWNTIME_FACTOR = dtaa2[0].BDMP_DOWNTIME;
                //$scope.pgVar.DOWNTIME = parseInt($scope.pgVar.CDOWNTIME) + parseInt($scope.pgVar.ADOWNTIME);
                progress(0, 'Loading...', false);
            }
            $scope.GetRCADataValue(dta);
        }, function (response) {
            progress(0, '', false);

        });
        // $scope.LoadData();       
       
    }

    $scope.GetSpareDataValue = function (dta) {
        var obj = {
            REQID: dta
        }
        BreakDownUptService.getSpareData(obj).then(function (response) {
            if (response == undefined) {
                progress(0, 'Loading...', false);
                $scope.gridOptions5.api.setRowData([]);
            }
            else {
                var dtaa = angular.copy(response);
                $scope.gridOptions5.api.setRowData(dtaa);
                progress(0, 'Loading...', false);
            }
            $scope.GetDescrptionValue();
        }, function (response) {
            progress(0, '', false);

        });
       
        // $scope.LoadData();

    }

    //$scope.MyrequisitonsOptions = {

    //    columnDefs: toDueList,
    //    rowData: [],
    //    enableFilter: true,
    //    enableSorting: true,
    //    angularCompileRows: true,
    //    enableCellSelection: false,
    //    enableColResize: true,
    //    groupHideGroupColumns: false,
    //};

    $scope.getTicketTypeFn = function (TICKET_CODE) {
        BreakDownUptService.getTicketType().then(function (response) {
            if (response != undefined) {
                $scope.TicketType = response;
                angular.forEach($scope.TicketType, function (value, key) {
                    //if (key == 0) {
                    if (value.PRI_CODE.toString() == TICKET_CODE) {
                        value.ticked = true;
                        //$scope.pgVar.TicketType.push($scope.TicketType[key]);                  
                    }
                });
                //angular.forEach($scope.TicketType, function (value, key) {
                //    if (key == 0) {
                //        value.ticked = true;
                //        $scope.pgVar.TicketType.push($scope.TicketType[0]);
                //        getUrgencyFn();
                //        $scope.pgVar.getErrorDescriptionFn();
                //    }
                //}); 

            } else {
                console.log('Ticket Type Get Error');
            }
        });
    }
    $scope.getProblemOwner = function (ProblemDescOwn) {
        BreakDownUptService.getProblemOwner().then(function (response) {
            if (response != undefined) {
                $scope.ProblemOwner = response;
                angular.forEach($scope.ProblemOwner, function (value, key) {
                    //if (key == 0) {
                    if (value.OWN_CODE.toString() == ProblemDescOwn) {
                        value.ticked = true;
                        //$scope.pgVar.TicketType.push($scope.TicketType[key]);                  
                    }
                });
                //angular.forEach($scope.ProblemOwner, function (value, key) {
                //    if (key == 0) {
                //        value.ticked = true;
                //    }
                //});
            } else {
                console.log('Location Get Error');
            }
        });
    }

    $scope.getStartusFn = function (Status) {
        BreakDownUptService.getStartus().then(function (response) {
            if (response != undefined) {
                $scope.Status = response;
                var bool = false;
                angular.forEach($scope.Status, function (value, key) {
                    if (value.STA_ID.toString() == Status && !bool) {
                        value.ticked = true;
                        bool = true;
                    }
                });
                //angular.forEach($scope.Status, function (value, key) {
                //    if (value.STA_TITLE.toString() == $scope.pgVar.reqstStatus.toString() && !bool) {
                //        value.ticked = true;
                //        bool = true;
                //    }
                //});
            } else {
                console.log('Startus Get Error');
            }
        });
    }

    $scope.getProblemCategory = function (FD_CODE) {
        BreakDownUptService.getProblemCategory().then(function (response) {
            if (response != undefined) {
                $scope.FDNAME = response;
                var bool = false;
                angular.forEach($scope.FDNAME, function (value, key) {
                    if (value.FD_CODE.toString() == FD_CODE && !bool) {
                        value.ticked = true;
                        bool = true;
                    }
                });
                //angular.forEach($scope.Status, function (value, key) {
                //    if (value.STA_TITLE.toString() == $scope.pgVar.reqstStatus.toString() && !bool) {
                //        value.ticked = true;
                //        bool = true;
                //    }
                //});
            } else {
                console.log('Startus Get Error');
            }
        });
    }

    $scope.getUrgencyFn = function (IMPCT_CODE) {
        BreakDownUptService.getUrgency().then(function (response) {
            if (response != undefined) {
                $scope.Urgency = response;
                angular.forEach($scope.Urgency, function (value, key) {
                    if (value.UGC_CODE.toString() == IMPCT_CODE) {
                        value.ticked = true;
                        //$scope.pgVar.Urgency.push($scope.Urgency[key1]);
                    }
                });
                //angular.forEach($scope.Urgency, function (value, key) {
                //    if (key == 0) {
                //        value.ticked = true;
                //    }
                //});
            } else {
                console.log('Urgency Get Error');
            }
        });
    }

    /* Error Description */
    $scope.getErrorDescriptionFn = function (ERRORCODE) {
        if ($scope.pgVar.Equipment[0].GROUP_ID != 'A') {
            var vals = {
                CATEGORY: $scope.pgVar.Equipment[0].GROUP_ID,
                TICKETTYPE: $scope.pgVar.TicketType[0].MainCatCode
            }
        }
        else {
            var vals = {
                CATEGORY: 'ALL',
                TICKETTYPE: 'ALL'
            }
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.getErrorDescription(params).then(function (response) {
            if (response != undefined) {
                $scope.ErrorDescription = response;
                var bool = false;
                angular.forEach($scope.ErrorDescription, function (value, key) {
                    if (value.CHC_TYPE_CODE.toString() == ERRORCODE && !bool) {
                        value.ticked = true;
                        bool = true;
                    }
                    //if (value.CHC_TYPE_NAME.toString() == $scope.pgVar.errorDis.toString() && !bool) {
                    //    value.ticked = true;
                    //    bool = true;
                    //    $scope.pgVar.ErrorDescription.push($scope.ErrorDescription[0]);
                    //    $scope.pgVar.getAssigntoFn();
                    //}
                });
            } else {
                console.log('Description Get Error');
            }
        });
    }

    //$scope.getAssigntoFn = function (typ, val) {
    //    if ($scope.pgVar.Equipment[0].GROUP_ID != 'A') {
    //        var vals = {
    //            LOC: $scope.pgVar.LCM_CODE,
    //            TICKET_TYPE: $scope.pgVar.TicketType[0].MainCatCode,
    //            EQUIPMENT: $scope.pgVar.Equipment[0].GROUP_ID,
    //            ERRORDESCRIPTION: $scope.pgVar.ErrorDescription[0].CHC_TYPE_CODE
    //        }
    //    }
    //    else {
    //        var vals = { LOC: 'ALL' }
    //    }
    //    var params = JSON.stringify(vals);
    //    BreakDownUptService.getAssignto(params).then(function (response) {
    //        if (response != undefined) {
    //            $scope.Assignto = response;
    //        } else {
    //            console.log('Assign to Get Error');
    //        }
    //    });
    //}

    $scope.getAssigntoFn = function (LOC_CODE, AssginEmp) {
        var vals = { LOC: LOC_CODE }
        var params = JSON.stringify(vals);
        BreakDownUptService.getAssignto(params).then(function (response) {
            if (response != undefined) {
                $scope.Assignto = response;
                angular.forEach($scope.Assignto, function (value, key) {
                    if (value.AUR_ID.toString() == AssginEmp) {
                        value.ticked = true;
                        //$scope.pgVar.Urgency.push($scope.Urgency[key1]);
                    }
                });
            } else {
                console.log('Assign to Get Error');
            }
        });
    }

    ////////////////START//////////////////////////////
    /* Get Equipment Names */
    $scope.getEquipmentFn = function (typ, val, BDMP_GRP_TYPE_ID) {
        var vals = $scope.frmbreakDown.Locationlst[0].LCM_CODE;
        //if (typ == 'P') vals = ($scope.frmbreakDown.Locationlst[0].LCM_CODE == "0" ? 'ALL' : $scope.frmbreakDown.Locationlst[0].LCM_CODE);
        //else vals = val;
        BreakDownUptService.getEquipment(vals).then(function (response) {
            if (response != undefined) {
                //response.data.push({ GROUP_ID: 'A', GROUP_NAME: 'All' });
                filterFn(response.data, 'GROUP_ID');
                $scope.Equipment2 = $scope.keys;
                angular.forEach($scope.Equipment2, function (value, key) {
                    if (value.GROUP_ID.toString() == BDMP_GRP_TYPE_ID) {
                        value.ticked = true;
                        //$scope.pgVar.Urgency.push($scope.Urgency[key1]);
                    }
                    //if (key == 0) {
                    //    value.ticked = true;
                    //    $scope.pgVar.Equipment.push($scope.Equipment[0]);
                    //    $scope.pgVar.getSparePartsFn();
                    //    //if (typ == 'D') {
                    //    //    $scope.pgVar.getBreakDownRqstsFn(typ);
                    //    //}
                    //}
                    //else { value.ticked = false; }
                });
            } else {
                console.log(' Equipment Names Get Error');
            }
        });
    }
    ///* Get Add Spare Parts */
    //$scope.getSparePartsFn = function () {
    //    if ($scope.frmbreakDown.Equipment[0].GROUP_ID != 'A') {
    //        var vals = {
    //            LOC: $scope.frmbreakDown.Locationlst[0].LCM_CODE,
    //            EQUIPMENT: $scope.frmbreakDown.Equipment2[0].GROUP_ID
    //        }
    //    }
    //    else {
    //        var vals = {
    //            LOC: 'ALL'
    //        }
    //    }        
    //    var params = JSON.stringify(vals);
    //    BreakDownUptService.getSpareParts(params).then(function (response) {
    //        if (response != undefined) {
    //            for (var i in response) {
    //                response[i].value = 0;
    //                response[i].VAL = '';
    //            }
    //            $scope.pgVar.SpareParts = response;
    //        } else {
    //            console.log('Spare Parts Get Error');
    //        }
    //    });
    //}
    //Save

    /* data formate */
    function frmtDt(dt, typ) {
        if (dt != '') {
            if (typ == 'Date')
                return (dt.getDate() > 9 ? dt.getDate() : '0' + dt.getDate()) + '' + ((dt.getMonth() + 1) > 9 ? (dt.getMonth() + 1) : '0' + (dt.getMonth() + 1)) + '' + dt.getFullYear();
            else if (typ == 'DateTime')
                return (dt.getMonth() + 1) + '-' + dt.getDate() + '-' + dt.getFullYear() + " " + (dt.getHours() > 9 ? dt.getHours() : '0' + dt.getHours()) + ':' + (dt.getMinutes() > 9 ? dt.getMinutes() : '0' + dt.getMinutes());
            else
                return (dt.getHours() > 9 ? dt.getHours() : '0' + dt.getHours()) + ':' + (dt.getMinutes() > 9 ? dt.getMinutes() : '0' + dt.getMinutes());
        } else
            return dt;
    }

    var newDate = frmtDt(new Date(), 'Date');
    /* Date  Difference*/
    function dateDiff(tDt, fDt) {
        var dt1 = fDt ? new Date(fDt) : new Date();
        var dt2 = new Date(tDt);
        var ms = (dt1 - dt2);
        var s, m, h, d;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        return { day: d, hrs: h, min: m, sec: s };
    }
    ///* Down Time*/

    $scope.downTimeFn = function () {   
        var e = new Date($scope.pgVar.END_DATE + ' ' + frmtDt($scope.pgVar.END_TIME, 'Time'));
        var f = new Date($scope.pgVar.FROM_DATE + ' ' + frmtDt($scope.pgVar.FROM_TIME, 'Time'));        
        var diff = e-f;
        var diffSeconds = diff / 1000;
        var HH = Math.floor(diffSeconds / 3600);
        var MM = Math.floor(diffSeconds % 3600) / 60;
        //var formatted = ((HH < 10) ? ("0" + HH) : HH) + ":" + ((MM < 10) ? ("0" + MM) : MM);
        var formatted = HH*60 + MM;
        $scope.pgVar.CDOWNTIME = formatted;

        //var DOWNTIME = 0;
        //var DOWNTIMES = 0;
        //if ($scope.pgVar.FROM_DATE && $scope.pgVar.END_DATE) {
        //    DOWNTIME = dateDiff(new Date($scope.pgVar.FROM_DATE), new Date($scope.pgVar.END_DATE));
        //    if (DOWNTIME.day < 0) {
        //        DOWNTIME.day = 0;
        //    }
        //    else {
        //        DOWNTIME.min = DOWNTIME.min + (DOWNTIME.day * 1440)
        //    }
        //}
        //if ($scope.pgVar.FROM_TIME && $scope.pgVar.END_TIME) {
        //    DOWNTIMES = dateDiff($scope.pgVar.FROM_TIME, $scope.pgVar.END_TIME);
        //    if (DOWNTIMES.hrs < 0) {
        //        DOWNTIMES.hrs = 0;
        //    }
        //    else {
        //        DOWNTIMES.min = DOWNTIMES.min + (DOWNTIMES.hrs * 60)
        //    }
        //    if (DOWNTIMES.min < 0) {
        //        DOWNTIMES.min = 0;
        //    }
        //}
        //if (DOWNTIME || DOWNTIMES) {
        //    if (DOWNTIMES == undefined) DOWNTIMES = { min: 0 };
        //    if (DOWNTIME == undefined) DOWNTIME = { min: 0 };
        //    $scope.pgVar.CDOWNTIME = DOWNTIME.min + DOWNTIMES.min;
        //    //$scope.pgVar.DOWNTIME = parseInt($scope.pgVar.CDOWNTIME) + parseInt($scope.pgVar.ADOWNTIME);
        //}
    }

    //// start time and end time
    //var startTime = moment("12:16:59 am", "HH:mm:ss a");
    //var endTime = moment("06:12:07 pm", "HH:mm:ss a");
    //// calculate total duration
    //var duration = moment.duration(endTime.diff(startTime));
    //// duration in hours
    //var hours = parseInt(duration.asHours());
    //// duration in minutes
    //var minutes = parseInt(duration.asMinutes()) % 60;
    //alert(hours + ' hour and ' + minutes + ' minutes.');
    //$scope.UpdateTimeSheet = function (rowEntity) {
    //    if (rowEntity.StartTime.toString().length != 11) {
    //        rowEntity.StartTime = moment(rowEntity.StartTime).format("hh:mm:ss a");
    //    }
    //    if (rowEntity.EndTime.toString().length != 11) {
    //        rowEntity.EndTime = moment(rowEntity.EndTime).format("hh:mm:ss a");
    //    }
    //    var startTime = moment(rowEntity.StartTime, "hh:mm:ss a");
    //    var endTime = moment(rowEntity.EndTime, "hh:mm:ss a");
    //    var mins = moment.utc(moment(endTime, "HH:mm:ss").diff(moment(startTime, "HH:mm:ss"))).format("mm")
    //    rowEntity.TotalHours = endTime.diff(startTime, 'hours') + " Hrs and " + mins + " Mns";
    //}

    /* Save File in Folder */
    function UploadFile() {
        if ($("#file").get(0).files.length > 0) {
            $.ajax({
                type: "POST",
                url: UtilityService.path + '/api/BreakDownUpt/UploadFiles',
                contentType: false,
                processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                cache: false,
                data: fileData, 		        // DATA OR FILES IN THIS CONTEXT.
                success: function (data, textStatus, xhr) {
                    //ClearFn();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //ClearFn();
                }
            });
        }
    }

    function UploadFiler() {
        if ($("#filer").get(0).files.length > 0) {
            $.ajax({
                type: "POST",
                url: UtilityService.path + '/api/BreakDownUpt/UploadFiles',
                contentType: false,
                processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                cache: false,
                data: fileData, 		        // DATA OR FILES IN THIS CONTEXT.
                success: function (data, textStatus, xhr) {
                    //ClearFn();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //ClearFn();
                }
            });
        }
    }

    /* Save File Chanege */
    $scope.fileNameChanged = function (fu, typ) {
        var fileUpload = $("#file").get(0);
        var files = fileUpload.files;
        fileData = new FormData();
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
            //var FilePath = newDate + files[i].name + "~" + files[i].type; 
            var FilePath = newDate + files[i].name;
            $scope.FilePathArr.push({ "FilePath": FilePath });
        }
    };

    $scope.fileNameChangedr = function (fu, typ) {
        var fileUpload = $("#filer").get(0);
        var files = fileUpload.files;
        fileData = new FormData();
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
            //var FilePath = newDate + files[i].name + "~" + files[i].type; 
            var FilePath = newDate + files[i].name;
            $scope.FilePathArrr.push({ "FilePath": FilePath });
        }
    };

    //function fn_Set() {
    //    if (attachedfile.length > 0) {           
    //        var strattached = [];
    //        strattached = attachedfile.split(',');
    //        var hTable = "<table id='dvImage' class='table table-condensed table - bordered table - hover table - striped'>";
    //        hTable += "<tr><th>SR. NO.</th><th style='text-align:center'>FILE NAME</th><th style='text-align:center'>DOC</th><th style='text-align:center'>REMOVE</th></tr>";
    //        hTable += "<tbody>";
    //        var srno = 0;
    //        for (var i = 0; i < strattached.length; i++) { 
    //            hTable += "<tr>";
    //            hTable += "<td>" + (srno + 1) + "</td><td align='center'>" + strattached[i] + "</td>";
    //            if (strattached[i].split('.')[1] == "xlsx" || strattached[i].split('.')[1] == "pdf" || strattached[i].split('.')[1] == "docx") {
    //                hTable += "<td align='center'><a href='#' title='File'>" + strattached[i] + "</a></td>";
    //            }
    //            else {
    //                hTable += "<td align='center'><img src='../../UploadFiles/Flipkart_Ekart_UAT.dbo/" + strattached[i] + "' style='height: 100px; border - radius: 10px;' alt='' /></td>";
    //            }
    //            hTable += "<td style=padding:1px;>";                
    //            //var template = angular.element('<a href = "#" ng-click = "angular.element(this).scope().generate()" > Generate new user</a >');
    //            //var linkFn = $compile(template);
    //            //var element = linkFn($scope);
    //            //hTable += element;
    //            hTable += "<a title='Delete File' (click)='angular.element(this).scope().fn_DeleteFile(\"" + strattached[i] + "\")' ><i class='fa fa-trash' aria-hidden='true' style='margin-left: 15px;height: 22px;cursor:pointer;'></i></a>";
    //            //hTable += "<input type='button' id='btnDelete' ng-click='fn_DeleteFile(\"" + strattached[i] + "\")' class='btn btn-primary custom-button-color' value='Delete' />";
    //            //hTable += "<a title='Delete File' onclick='fn_DeleteFile(\"" + strattached[i] + "\",\"" + attachedfile + "\")' ><i class='fa fa-trash' aria-hidden='true' style='margin-left: 15px;height: 22px;cursor:pointer;'></i></a>";
    //            hTable += "</td>";
    //            hTable += "</tr>";
    //            srno++;
    //        }
    //        hTable += "</tbody></table>";
    //        $("#dvImage").html(hTable);
    //    }
    //}


    //.js
    //angular.module("testApp", []).controller("testContoller", function ($scope, $compile) {
    //        $scope.tabList = '<a ng-click="getActiveTag()">Test</a>';
    //        $compile($scope.tabList)($scope).appendTo(angular.element("#appendHere"));
    //        //$scope.tabList = temp[0];
    //    });

    //$scope.getActiveTag = function () {
    //    alert('calling anchor click');
    //}
    //var testVar = angular.element('<a ng-click="getActiveTag()">Test</a>');
    //$compile(testVar)($scope).appendTo(angular.element("#appendHere"));
    // Step 1: parse HTML into DOM element
    //var template = angular.element('' < a ng - click="getActiveTag()" > Test</a > '');
    //// Step 2: compile the template
    //var linkFn = $compile(template);
    //// Step 3: link the compiled template with the scope.
    //var element = linkFn($scope);
    //// Step 4: Append to DOM (optional)
    //$("#content").append(element);

    //$scope.generate = function () {
    //    alert('ss');
    //}



    function fn_Set() { 
        if (attachedfile.length > 0) {
            var strattached = [];
            strattached = attachedfile.split(',');
            var hTable = "<table id='tblImage' class='table table-condensed table - bordered table - hover table - striped'>";
            hTable += "<tr><th>SR. NO.</th><th style='text-align:center'>FILE NAME</th><th style='text-align:center'>DOC</th><th style='text-align:center'>REMOVE</th></tr>";
            hTable += "<tbody>";
            var srno = 0;
            for (var i = 0; i < strattached.length; i++) {
                hTable += "<tr>";
                hTable += "<td>" + (srno + 1) + "</td><td align='center'>" + strattached[i] + "</td>";
                //if (strattached[i].split('.')[1] == "xlsx" || strattached[i].split('.')[1] == "pdf" || strattached[i].split('.')[1] == "docx") {
                //hTable += "<td align='center'><a href='#' title='File'>" + strattached[i] + "</a></td>";                   
                hTable += "<td align='center'><a href='../../UploadFiles/Flipkart_Ekart_UAT_DEV.dbo/" + strattached[i] + "' title='File'>" + strattached[i] + "</a></td>";
                //}
                //else {
                //    hTable += "<td align='center'><img src='../../UploadFiles/Flipkart_Ekart_UAT_DEV.dbo/" + strattached[i] + "' style='height: 100px; border - radius: 10px;' alt='' /></td>";
                //}
                hTable += "<td style=padding:1px;>";
                //hTable += "<a title='Delete File' onclick='angular.element(this).scope().fn_DeleteFile(\"" + strattached[i] + "\")' ><i class='fa fa-trash' aria-hidden='true' style='margin-left: 15px;height: 22px;cursor:pointer;'></i></a>";
                hTable += "<input type='checkbox' id='DelFile" + i + "' name='DelFile' onclick='angular.element(this).scope().fn_DeleteFile(\"" + strattached[i] + "\",\"" + i + "\")' />";
                hTable += "</td>";
                hTable += "</tr>";
                srno++;
            }
            hTable += "</tbody></table>";
            $("#dvImage").html(hTable);
        }
    }
    $scope.fn_DeleteFile = function (filNm, ind) {
        $("#DelFile" + ind).attr("disabled", "disabled");
        var finalstring = '';
        var firstpart = '';
        var secondpart = '';
        if (attachedfile.length > 0) {
            var index = attachedfile.indexOf(filNm);
            var lastindex = index + filNm.length;
            firstpart = attachedfile.substring(0, index);
            secondpart = attachedfile.substring(lastindex, attachedfile.length);
            finalstring = firstpart + secondpart;
        }
        var fnStr = finalstring.replace(",,", ",");
        if (fnStr[0] == ',') {
            fnStr = fnStr.substring(1, fnStr.length);
        }
        //var lastin = fnStr.lastIndexOf(',');
        if (fnStr[fnStr.length - 1] == ',') {
            //fnStr = fnStr.substring(0, fnStr.length-1);          
            fnStr = fnStr.slice(0, -1);
        }
        if (fnStr != '') {
            $scope.FilePathArr = [];
            $scope.FilePathArr.push({ "FilePath": fnStr });
            attachedfile = fnStr;
        }
    }


    $scope.UpdateAllRequests = function () {
        if ($scope.frmbreakDown.SUB_EQUIP == undefined || $scope.frmbreakDown.SUB_EQUIP == "") {
            showNotification('error', 8, 'bottom-right', 'Please Enter Sub Equipment');
            return;
        }
        if ($scope.frmbreakDown.OTHER_DESC == undefined || $scope.frmbreakDown.OTHER_DESC == "") {
            showNotification('error', 8, 'bottom-right', 'Please Enter Problem Description');
            return;
        }
        var vals = {
            LOC_CODE: $scope.frmbreakDown.Locationlst[0].LCM_CODE,
            SUB_CAT: $scope.frmbreakDown.Equipment[0].GROUP_ID,
            ERROR_DESC: $scope.frmbreakDown.Probdesc[0].CHC_TYPE_CODE,
            AAS_VED: $scope.frmbreakDown.AAS_VED[0].UGC_CODE,
            SUB_EQUIP: $scope.frmbreakDown.SUB_EQUIP,
            IMPACT: $scope.frmbreakDown.IMPACT[0].IMP_CODE,
            OTHER_DESC: $scope.frmbreakDown.OTHER_DESC
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.save(params).then(function (response) {
            if (response != undefined) {
                //error 
                $scope.frmbreakDown.OTHER_DESC = "";
                $scope.frmbreakDown.SUB_EQUIP = "";
                $scope.getBreakDownRqstsFn();
                showNotification('success', 8, 'bottom-right', "Submitted Successfully");
              
            } else {
                console.log('Submit Error');
            }
        });
    }

    $scope.SaveRCA = function () {
        if ($scope.frmRCA.ROOT_DESC == undefined || $scope.frmRCA.ROOT_DESC == "") {
            showNotification('error', 8, 'bottom-right', 'Please Enter RCA Description');
            return;
        }
        if ($scope.frmRCA.RCA_Status[0] == undefined) {
            showNotification('error', 8, 'bottom-right', 'Please Select RCA Status');
            return;
        }
        var vals = {
            RCA_DESCRIPTION: $scope.frmRCA.ROOT_DESC,
            RCA_STATUS: $scope.frmRCA.RCA_Status[0].RCASTATUS,
            RCA_BDMP_ID: $scope.Breakdownobject.REQ_ID,
            RCAPATH: ($scope.FilePathArrr.length > 0 ? $scope.FilePathArrr[$scope.FilePathArrr.length - 1].FilePath : ''),
            NEWDATE: newDate,
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.SaveRCA(params).then(function (response) {
            if (response != undefined) {
                //error 
                UploadFiler();
                $scope.GetRCADataValue($scope.Breakdownobject.REQ_ID);
                showNotification('success', 8, 'bottom-right', "Submitted Successfully");
            } else {
                console.log('Submit Error');
            }
        });
    }

    $scope.SaveDownTime = function () {
        if ($scope.pgVar.CDOWNTIME <= 0) {
            showNotification('error', 8, 'bottom-right', 'Invalid Down Time');
            return;
        }
        var vals = {
            SER_REQ_ID: $scope.Breakdownobject.REQ_ID,
            TODATE: ($scope.pgVar.END_DATE.length > 0 ? ($scope.pgVar.END_DATE + ' ' + frmtDt($scope.pgVar.END_TIME, 'Time')) : ''),
            FROMDATE: ($scope.pgVar.FROM_DATE.length > 0 ? ($scope.pgVar.FROM_DATE + ' ' + frmtDt($scope.pgVar.FROM_TIME, 'Time')) : ''),
            BDMP_DWNTIME_FACTOR: ($scope.pgVar.BDMP_DWNTIME_FACTOR || ''),
            BDMP_DOWNTIME: (($scope.pgVar.CDOWNTIME || 0)),
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.SaveDownTime(params).then(function (response) {
            if (response != undefined) {
                //error                
                $scope.GetDownTimeDataValue($scope.Breakdownobject.REQ_ID);
                showNotification('success', 8, 'bottom-right', "Submitted Successfully");
            } else {
                console.log('Submit Error');
            }
        });
    }

    $scope.DeleteDownTime = function (det) {
        var vals = {
            SER_DOWN_ID: det,
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.DeleteDownTime(params).then(function (response) {
            if (response != undefined) {
                //error                
                $scope.GetDownTimeDataValue($scope.Breakdownobject.REQ_ID);
                showNotification('success', 8, 'bottom-right', "Submitted Successfully");
            } else {
                console.log('Submit Error');
            }
        });
    }

    $scope.DeleteRCA = function (det) {
        var vals = {
            SR_NO: det,
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.DeleteRCA(params).then(function (response) {
            if (response != undefined) {
                //error                
                $scope.GetRCADataValue($scope.Breakdownobject.REQ_ID);
                showNotification('success', 8, 'bottom-right', "Submitted Successfully");
            } else {
                console.log('Submit Error');
            }
        });
    }

    $scope.DeleteSpare = function (det) {
        var vals = {
            BDMPD_SNO: det,
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.DeleteSpare(params).then(function (response) {
            if (response != undefined) {
                //error                
                $scope.GetSpareDataValue($scope.Breakdownobject.REQ_ID);
                showNotification('success', 8, 'bottom-right', "Submitted Successfully");
            } else {
                console.log('Submit Error');
            }
        });
    }

    $scope.setStatus = function (sta) {
        $scope.pgVar.sparePartsArr = [];
        //for (var i in $scope.pgVar.SpareParts) {
        //    if ($scope.pgVar.SpareParts[i].VAL == 'Y') {
        //        var obj = {};
        //        obj.SPAREPARTCODE = $scope.pgVar.SpareParts[i].ID,
        //            obj.CATEGORY = '',
        //            obj.SUBCATCODE = '',
        //            obj.BRNDCODE = '',
        //            obj.MODELCODE = '',
        //            obj.QTY = $scope.pgVar.SpareParts[i].value
        //        $scope.pgVar.sparePartsArr.push(obj);
        //    }
        //}
        //angular.forEach($scope.Assignto, function (value, key) {
        //    if (value.ticked != true) {
        //        showNotification('error', 8, 'bottom-right', 'Please Select Engineer');
        //        return;
        //    }
        //});

        if ($scope.pgVar.Assignto[0] == undefined) {
            showNotification('error', 8, 'bottom-right', 'Please Select Engineer');
            return;
        }
        if ($scope.pgVar.CORRECTION_ACTION == undefined || $scope.pgVar.CORRECTION_ACTION == "") {
            showNotification('error', 8, 'bottom-right', 'Please Enter Corrective Action');
            return;
        }
        if ($scope.pgVar.PREVENTIVE_ACTION == undefined || $scope.pgVar.PREVENTIVE_ACTION == "") {
            showNotification('error', 8, 'bottom-right', 'Please Enter Preventive Action');
            return;
        }
        if ($scope.pgVar.PRODUCTION_IMPACT == undefined || $scope.pgVar.PRODUCTION_IMPACT == "") {
            showNotification('error', 8, 'bottom-right', 'Please Enter Shipment Damage');
            return;
        }       
        if ($scope.pgVar.Status[0].STA_ID == 9 && $scope.pgVar.ADOWNTIME>=30) {
            //if ($scope.frmRCA.RCA_Status[0] == undefined) {            
            if ($scope.gridOptions2.rowData.length==0) {
                showNotification('error', 8, 'bottom-right', 'Please update RCA details' );
                return;
            }           
        }
        if ($scope.pgVar.Status[0].STA_ID == 7 || $scope.pgVar.Status[0].STA_ID == 9) {            
            if ($scope.pgVar.ADOWNTIME <= 0) {
                showNotification('error', 8, 'bottom-right', 'Invalid Down Time');
                return;
            }
            if ($scope.pgVar.ProblemOwner[0] == undefined) {
                showNotification('error', 8, 'bottom-right', 'Please Select Problem Owner');
                return;
            }
        }
        angular.forEach($scope.gridOptions3.rowData, function (value, key) {
            if (value.ticked == true) {
                if (value.Qty == undefined) {
                    progress(0, '', false);
                    setTimeout(function () { showNotification('error', 8, 'bottom-right', 'Please use arrows keys'); }, 500);
                    return;
                }
                if (parseFloat(value.Qty) <= 0) {
                    progress(0, '', false);
                    setTimeout(function () { showNotification('error', 8, 'bottom-right', 'Please enter more than zero'); }, 500);
                    return;
                }
                if (parseFloat(value.AAS_SPAREPART_AVBQUANTITY) < parseFloat(value.Qty)) {
                    progress(0, '', false);
                    setTimeout(function () { showNotification('error', 8, 'bottom-right', 'Please enter less quanity'); }, 500);
                    return;
                }
                //$scope.pgVar.sparePartsArr.push({ 'SPAREPARTCODE': value.AAS_SPAREPART_ID, 'CATEGORY': '', 'SUBCATCODE': '', 'BRNDCODE': '', 'MODELCODE': value.AAS_SPAREPART_COST.toString(), 'QTY': value.Qty.toString() });
                $scope.pgVar.sparePartsArr.push({ 'SPAREPARTCODE': value.AAS_SPAREPART_ID, 'CATEGORY': '', 'SUBCATCODE': '', 'BRNDCODE': '', 'MODELCODE': (parseFloat(value.Qty) * parseFloat(value.AAS_SPAREPART_COST)).toFixed(2).toString(), 'UNITCOST': value.AAS_SPAREPART_COST.toString(), 'QTY': value.Qty.toString() });
            }
        });
        if ($scope.pgVar.sparePartsArr.length > 0) {            
            if ($scope.pgVar.SPARE_REM == undefined || $scope.pgVar.SPARE_REM == "") {
                showNotification('error', 8, 'bottom-right', 'Please Enter Requestor Remarks');
                return;
            }
        }
        var strAttachment = '';
        if ($scope.FilePathArr.length > 0) {
            for (var i in $scope.FilePathArr) {
                strAttachment += $scope.FilePathArr[i].FilePath + ",";
            }
            strAttachment = strAttachment.substring(0, strAttachment.length - 1);
        }
        var vals = {
            spd: $scope.pgVar.sparePartsArr,
            REQID: $scope.Breakdownobject.REQ_ID,
            STATUS: $scope.pgVar.Status[0].STA_ID,
            INCHARE_ID: $scope.pgVar.Assignto[0].ASSIGNED_TO,
            ROOT_CAUSE: ($scope.frmRCA.RCA_Status[0] == undefined) ? "" : $scope.frmRCA.RCA_Status[0].RCASTATUS,//$scope.pgVar.ROOT_CAUSE,
            PREVENTIVE_ACTION: $scope.pgVar.PREVENTIVE_ACTION,
            CORRECTIVE_ACTION: $scope.pgVar.CORRECTION_ACTION,
            PRODUCTION_IMPCT: $scope.pgVar.PRODUCTION_IMPACT,
            PROBLEM_OWNER: ($scope.pgVar.ProblemOwner.length > 0 ? $scope.pgVar.ProblemOwner[0].OWN_CODE : ''),
            REMARKS: $scope.pgVar.REMARKS,
            REQREMARKS: $scope.pgVar.SPARE_REM,
            //ATTACHMENT: ($scope.FilePathArr.length > 0 ? $scope.FilePathArr[$scope.FilePathArr.length-1].FilePath : ''),//'',//
            ATTACHMENT: strAttachment,//'',//
            TODATE: ($scope.pgVar.END_DATE.length > 0 ? ($scope.pgVar.END_DATE + ' ' + frmtDt($scope.pgVar.END_TIME, 'Time')) : ''),
            FROMDATE: ($scope.pgVar.FROM_DATE.length > 0 ? ($scope.pgVar.FROM_DATE + ' ' + frmtDt($scope.pgVar.FROM_TIME, 'Time')) : ''),
            //TODATE: ($scope.pgVar.END_DATE.length > 0 ? ($scope.pgVar.END_DATE + ' ' + frmtDt($('#END_TIME').val(), 'Time')) : ''),
            //FROMDATE: ($scope.pgVar.FROM_DATE.length > 0 ? ($scope.pgVar.FROM_DATE + ' ' + frmtDt($('#FROM_TIME').val(), 'Time')) : ''),
            BDMP_DWNTIME_FACTOR: ($scope.pgVar.BDMP_DWNTIME_FACTOR || ''),
            BDMP_DOWNTIME: (($scope.pgVar.ADOWNTIME || 0)),
            BDMP_BRACHED: ($scope.pgVar.ADD_BREACH || ''),
            NEWDATE: newDate,
            EQUIPMENT_CODE: $scope.pgVar.Equipment2[0].GROUP_ID,//$scope.frmbreakDown.Equipment[0].GROUP_ID,//
            ERROR_DESCRIPTION_CODE: $scope.frmbreakDown.Probdesc[0].CHC_TYPE_CODE,//$scope.pgVar.ErrorDescription[0].CHC_TYPE_CODE,//$scope.Breakdownobject.ErrorDesc,//
            BDMP_OTHERPROB_OWNER: $scope.pgVar.OTHER_PROBLEM_OWNER,
            OTHER_ERROR_DESCRIPTION: $scope.pgVar.OTHER_ERROR_DESCRIPTION,//$scope.frmbreakDown.OTHER_DESC,//
            BDMP_SHIFT: $scope.pgVar.ADD_SHIFT,
            LOC_CODE: $scope.frmbreakDown.Locationlst[0].LCM_CODE,
            SER_PREV_ACT: $scope.frmRCA.ROOT_DESC,
            ADDI_BREACH: ($scope.pgVar.ADDI_BREACH || 0),
            PROBLEMCAT: ($scope.pgVar.FDNAME[0] == undefined) ? "" : $scope.pgVar.FDNAME[0].FD_CODE,
        }
        var params = JSON.stringify(vals);
        BreakDownUptService.submit(params).then(function (response) {
            if (response != undefined) {
                //error
                UploadFile();
                //$scope.showDivFn($scope.Breakdownobject.REQ_ID);  
                angular.element("input[type='file']").val(null);
                $scope.pgVar.activeDiv = true;
                $('#disp2').hide();
                $('#disp3').hide();
                $('#disp4').hide();
                $('#disp5').hide();
                $('#disp6').hide();
                $('#disp7').hide();
                $('#disp8').hide();
                $('#disp').show();
                $scope.getBreakDownRqstsFn();
                $('#collapseOne').addClass("show");
                $scope.pgVar.showError = response.Message;
                showNotification('success', 8, 'bottom-right', "Submitted Successfully");
            } else {
                console.log('Submit Error');
            }
        });
    }
    ////////////////END/////////////////////////////////  

    var attachedfile = '';
    $scope.showDivFn = function (BDMP_PLAN_ID) {
        //$scope.getTicketTypeFn();
        //$scope.getProblemOwner();
        //$scope.getStartusFn();
        //$scope.getUrgencyFn();
        //$scope.getAssigntoFn();
        progress(0, 'Loading...', true);
        //hdsession = "../../UploadFiles/"+$('#hdsession').val();
        $('#disp2').show();
        $('#disp3').show();
        $('#disp4').show();
        $('#disp5').show();
        $('#disp6').show();
        $('#disp7').show();
        $('#disp8').show();
        //$scope.pgVar.getEquipmentFn('D', 'ALL');
        $scope.Viewstatus = 1;
        $scope.frmRCA.ROOT_DESC = '';
        //$scope.Breakdownobject.SubCategory = dta.SubCategory;        
        BreakDownUptService.GetBreakDownDetails({ 'REQID': BDMP_PLAN_ID }).then(function (response) {
            //$scope.Breakdownobject.REQ_ID = dta.BDMP_PLAN_ID              
            $scope.Breakdownobject.REQ_ID = BDMP_PLAN_ID;
            $scope.Breakdownobject.LCM_NAME = response[0].LCM_NAME;
            $scope.Breakdownobject.SubCategory = response[0].EUIPEMENT;
            $scope.Breakdownobject.ErrorDesc = response[0].OtherErrorDesc;
            $scope.pgVar.OTHER_ERROR_DESCRIPTION = response[0].OtherErrorDesc;
            $scope.frmbreakDown.OTHER_DESC = "";//response[0].OtherErrorDesc;
            $scope.frmbreakDown.SUB_EQUIP = "";//response[0].SER_FAC;
            $scope.pgVar.ROOT_CAUSE = response[0].RootCause;
            $scope.pgVar.CORRECTION_ACTION = response[0].PERMNT_FIX;
            $scope.pgVar.PREVENTIVE_ACTION = response[0].PreventiveAction;
            $scope.pgVar.PRODUCTION_IMPACT = response[0].ProductionImp;
            $scope.frmRCA.ROOT_DESC = response[0].RCADESC;
            $scope.pgVar.OTHER_PROBLEM_OWNER = response[0].OtherProblemOwner;
            $scope.pgVar.ADD_BREACH = response[0].BREACHED;
            $scope.pgVar.ADDI_BREACH = response[0].ADDI_BREACH;
            //$scope.pgVar.ADOWNTIME = response[0].DOWNTIME;
            //$scope.pgVar.DOWNTIME = response[0].DOWNTIME;
            $scope.pgVar.BDMP_DWNTIME_FACTOR = parseFloat(response[0].DOWNTIMEFACTOR);
            $scope.pgVar.REMARKS = response[0].Remarks;            
            $scope.pgVar.ADD_SHIFT = response[0].BDMP_SHIFT;
            $scope.pgVar.FROM_DATE = response[0].FromDate;
            $scope.pgVar.END_DATE = response[0].ToDate;
            //$('#FROM_TIME').val(response[0].FromTime);
            //$('#END_TIME').val(response[0].ToTime);          
            $scope.pgVar.FROM_TIME = new Date(response[0].FromTime);
            $scope.pgVar.END_TIME = new Date(response[0].ToTime);
            $scope.pgVar.SPARE_REM = '';
            $scope.FilePathArr = [];
            $("#dvImage").html('');
            if (response[0].AttachmentFile != '') {
                $scope.FilePathArr.push({ "FilePath": response[0].AttachmentFile });
                attachedfile = response[0].AttachmentFile;
                fn_Set();
            }
            //$scope.pgVar.FilePathArr[0].FilePath = response[0].AttachmentFile;
            //angular.forEach($scope.TicketType, function (value, key) {
            //    //if (key == 0) {
            //    if (value.PRI_CODE.toString() == response[0].TICKET_CODE) {
            //        value.ticked = true;
            //        //$scope.pgVar.TicketType.push($scope.TicketType[key]);                  
            //    }                
            //});            
            //angular.forEach($scope.Urgency, function (value, key) {
            //    if (value.UGC_CODE.toString() == response[0].IMPCT_CODE) {
            //        value.ticked = true;
            //        //$scope.pgVar.Urgency.push($scope.Urgency[key1]);
            //    }
            //});

            $scope.getEquipmentFn('D', 'ALL', response[0].BDMP_GRP_TYPE_ID);
            $scope.getTicketTypeFn(response[0].TICKET_CODE);
            $scope.getUrgencyFn(response[0].IMPCT_CODE);
            $scope.getAssigntoFn(response[0].LOC_CODE, response[0].AssginEmp);
            $scope.getProblemOwner(response[0].ProblemDescOwn);
            $scope.getStartusFn(response[0].Status);
            $scope.getProblemCategory(response[0].FD_CODE);
            // $scope.getErrorDescriptionFn(response[0].ERRORCODE);

            //$scope.getErrorDesc(response[0].ERRORDESCRIPTION);
            $scope.downTimeFn();
            $scope.getRCA_Status(response[0].RootCause);
            setTimeout(function () { $scope.GetDownTimeDataValue(BDMP_PLAN_ID); }, 500);            
            //$scope.GetRCADataValue(BDMP_PLAN_ID);           
            //setTimeout(function () { $scope.GetDescrptionValue(); }, 1500);
            //$scope.getSparePartsFn();
            progress(0, 'Loading...', false);

        });

    }

    function filterFn(Arr, Key) {
        $scope.keys = []
        for (language in Arr) {
            $scope.keys.push(Arr[language])
        }
        $scope.keys = $filter('orderBy')($scope.keys, Key, false);
        return $scope.keys
    }

    function onReqFilterChanged(value) {
        $scope.MyrequisitonsOptions.api.setQuickFilter(value);
    }
    $("#ReqFilter").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    })



    $scope.MyrequisitonsOptions = {
        columnDefs: MyrequisitonsDefs,
        rowData: null,
        angularCompileRows: true,
        enableFilter: true,
        enableScrollbars: true,
        rowSelection: 'single',
        enableSorting: true,
        enableColResize: true,
        onReady: function () {
            $scope.MyrequisitonsOptions.api.sizeColumnsToFit()
        },

    };    

    UtilityService.getLocationsInventory(2).then(function (response) {        
        //$scope.pgVar.ASSET = $('#hdASSET').val();
        $scope.REQASSETID=$('#hdASSET').val();
        //alert(JSON.stringify($scope.paramValue));
        //UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            {
                //response.data.push({ LCM_CODE: '0', LCM_NAME: 'All' });
                $scope.Locationlst = _.orderBy(response.data, ['LCM_CODE', 'LCM_NAME'], ['asc', 'desc']);
                var a = _.find($scope.Locationlst);
                a.ticked = true;
            }
            setTimeout(function () { $scope.getEquipment(); }, 1000);


        }
    });

    BreakDownUptService.getUrgency().then(function (response) {
        if (response != undefined) {
            $scope.AAS_VED = response;
            angular.forEach($scope.AAS_VED, function (value, key) {
                if (key == 0) {
                    value.ticked = true;
                    //if (typ == 'D') {
                    //    $scope.pgVar.getBreakDownRqstsFn(typ);
                    //}
                }
            });
            //angular.forEach($scope.Urgency, function (value, key) {
            //    if (key == 0) {
            //        value.ticked = true;
            //    }
            //});
        } else {
            console.log('Urgency Get Error');
        }
    });
    BreakDownUptService.getImpact().then(function (response) {
        if (response != undefined) {
            $scope.IMPACT = response;
            angular.forEach($scope.IMPACT, function (value, key) {
                if (key == 0) {
                    value.ticked = true;
                    //if (typ == 'D') {
                    //    $scope.pgVar.getBreakDownRqstsFn(typ);
                    //}
                }
            });
            //angular.forEach($scope.Urgency, function (value, key) {
            //    if (key == 0) {
            //        value.ticked = true;
            //    }
            //});
        } else {
            console.log('Urgency Get Error');
        }
    });

    $scope.getRCA_Status = function (RCAStatus) {
        BreakDownUptService.getRCAStatus().then(function (response) {
            if (response != undefined) {
                $scope.RCA_Status = response;
                angular.forEach($scope.RCA_Status, function (value, key) {
                    if (value.RCASTATUS.toString() == RCAStatus) {
                        value.ticked = true;
                        //$scope.pgVar.Urgency.push($scope.Urgency[key1]);
                    }
                });
                //angular.forEach($scope.RCA_Status, function (value, key) {
                //    if (key == 0) {
                //        value.ticked = true;
                //        //if (typ == 'D') {
                //        //    $scope.pgVar.getBreakDownRqstsFn(typ);
                //        //}
                //    }
                //});
                //angular.forEach($scope.Urgency, function (value, key) {
                //    if (key == 0) {
                //        value.ticked = true;
                //    }
                //});
            } else {
                console.log('Urgency Get Error');
            }
        });
    }


    $scope.getEquipment = function () {

        BreakDownUptService.getEquipment($scope.frmbreakDown.Locationlst[0].LCM_CODE).then(function (response) {
            if (response.data != undefined) {
                //response.data.push({ GROUP_ID: 'A', GROUP_NAME: 'All' });
                //$scope.Equipment = response.data;
                $scope.Equipment = _.orderBy(response.data, ['GROUP_ID', 'GROUP_NAME'], ['asc', 'asc']);
                angular.forEach($scope.Equipment, function (value, key) {
                    if (key == 0) {
                        value.ticked = true;
                        //if (typ == 'D') {
                        //    $scope.pgVar.getBreakDownRqstsFn(typ);
                        //}
                    }

                });
                setTimeout(function () { $scope.getBreakDownRqstsFn(); }, 500);
                //setTimeout(function () { $scope.getErrorDesc("OTHER ISSUE"); }, 500);
            }
        });
    }

    $scope.getErrorDesc = function (ERRORNAME) {
        BreakDownUptService.getErrorDesc($scope.frmbreakDown.Equipment[0].GROUP_ID).then(function (response) {
            if (response.data != undefined) {
                //response.data.push({ CHC_TYPE_CODE: 'A', CHC_TYPE_NAME: 'All' });
                //$scope.Equipment = response.data;
                $scope.Probdesc = _.orderBy(response.data, ['CHC_TYPE_CODE', 'CHC_TYPE_NAME'], ['asc', 'asc']);
                angular.forEach($scope.Probdesc, function (value, key) {
                    if (value.CHC_TYPE_NAME.toString() == "OTHER ISSUE") {
                        value.ticked = true;
                    }
                    if (value.CHC_TYPE_NAME.toString() == ERRORNAME) {
                        value.ticked = true;
                    }
                });
                setTimeout(function () { $scope.setReqAssetIDFn($scope.REQASSETID); }, 500);
                //setTimeout(function () { $scope.getBreakDownRqstsFn(); }, 500);
            }
        });
    }

    $scope.setReqAssetIDFn = function (REQASSETID) {       
        if (REQASSETID != '0') {           
            $scope.showDivFn(REQASSETID);
            //data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"  
            $('#collapseOne').collapse();
            $('#collapseTwo').toggle();
            $("#btn2").attr("aria-expanded", "false");          
            $("#btn2").attr("aria-controls", "collapseTwo");
            
        }
    }

    $scope.getBreakDownRqstsFn = function () {
        {
            var vals = {
                LOC: ($scope.frmbreakDown.Locationlst[0].LCM_CODE == "0" ? 'ALL' : $scope.frmbreakDown.Locationlst[0].LCM_CODE),
                EQUIPMENT: ($scope.frmbreakDown.Equipment[0].GROUP_ID == 'A' ? 'ALL' : $scope.frmbreakDown.Equipment[0].GROUP_ID)

            }
        }
        var params = JSON.stringify(vals);
        progress(0, 'Loading...', true);
        BreakDownUptService.getBreakDownRqsts(params).then(function (response) {
            if (response == undefined) {
                progress(0, 'Loading...', false);
                $scope.MyrequisitonsOptions.api.setRowData([]);
            }
            else {
                var dta = angular.copy(response);
                for (var i in dta) {
                    dta[i].active = false;
                    if (dta[i].STA_TITLE == 'other') {
                        dta[i].active = true;
                    }
                }
                $scope.MyrequisitonsOptions.api.setRowData(dta);
                progress(0, 'Loading...', false);
            }
            var searchParams = new URLSearchParams(window.location.search);
            if (searchParams.get('status')) {
                $scope.filterName = angular.copy(searchParams.get('status'))
                $scope.filterFn($scope.pgVar.filterName)
            }
            setTimeout(function () { $scope.getErrorDesc("OTHER ISSUE"); }, 500);
        }, function (error) {
            console.log(error);
        });

    }
    $scope.rbActionschange = function () {
        alert('Hi');
    }




}]);