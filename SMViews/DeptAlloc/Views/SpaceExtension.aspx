﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
   <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

        function SelectAllExtnDates(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }


    </script>

</head>
<body data-ng-controller="SpaceExtensionController" class="amantra" onload="setDateVals()">
    <div class="animsition" data-ng>
        <div class="al-content">
            <div class="widgets">
                <h3>Space Extension</h3>
            </div>
            <div class="card">
                <div class="clearfix">
                    <div class="box-footer text-right">
                        <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                    </div>
                </div>
                <br />
                <form role="form" id="spaceExtension" name="frmSpaceExtension" data-valid-submit="GetSpaceExtensionDetails()" novalidate>
                    <div class="clearfix">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.CNY_NAME.$invalid}">
                                <label for="txtcode">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Getcountry" data-output-model="SEM.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                    data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SEM.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.CNY_NAME.$invalid" style="color: red">Please select country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.CTY_NAME.$invalid}">
                                <label for="txtcode">City <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Citylst" data-output-model="SEM.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                    data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SEM.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.LCM_NAME.$invalid}">
                                <label for="txtcode">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Locationlst" data-output-model="SEM.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                    data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SEM.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.TWR_NAME.$invalid}">
                                <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Towerlist" data-output-model="SEM.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                    data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SEM.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.TWR_NAME.$invalid" style="color: red">Please select a tower </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.FLR_NAME.$invalid}">
                                <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Floorlist" data-output-model="SEM.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                    data-on-item-click="FloorChange()" data-on-select-all="FloorChange()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SEM.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.FLR_NAME.$invalid" style="color: red">Please select a floor </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.VER_NAME.$invalid}">
                                <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Verticallist" data-output-model="SEM.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                    data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SEM.selectedVerticals" name="VER_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.VER_NAME.$invalid" style="color: red">Please select {{BsmDet.Parent |lowercase}} </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.Cost_Center_Name.$invalid}">
                                <label for="txtcode">{{BsmDet.Child}} <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="SEM.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                    data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SEM.selectedCostcenter" name="Cost_Center_Name" style="display: none" required="" />
                                <span id="message" class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.Cost_Center_Name.$invalid" style="color: red">Please select {{BsmDet.Child |lowercase}} </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.FROM_DATE.$invalid}">
                                <label for="txtcode">From Date <span style="color: red;">*</span></label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="SEM.FROM_DATE" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>

                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.FROM_DATE.$invalid" style="color: red">Please select from date </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.TO_DATE.$invalid}">
                                <label for="txtcode">To Date <span style="color: red;">*</span></label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="SEM.TO_DATE" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.TO_DATE.$invalid" style="color: red">Please select to date </span>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <button type="submit" value="Search" class="btn btn-primary custom-button-color">Search</button>
                            <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                </form>
                <br />

                <form role="form" id="spaceExtensionDetails" name="frmSpaceExtensionDetails">
                    <div data-ng-show="SEShowGrid">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" style="visibility: hidden">Filter </div>
                                <div class="form-group">
                                    <input class="form-control" id="SpcExtnFilter" placeholder="Filter..." type="text" style="width: 80%;" />
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12 pull-right">
                                <div class="form-group">Extension Date</div>
                                <div class="input-group date " id='EXTDATE'>

                                    <input type="text" class="form-control" data-ng-model="SEM.SSED_EXTN_DT" data-ng-change="setAllDates()" id="EXTN_DATE" name="EXTN_DATE" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('EXTDATE')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="height: 320px">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <textarea data-ng-model="SSE_REQ_REM" class="form-control" placeholder="Remarks"></textarea>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                            </div>
                            <div class="col-md-12 text-right">
                                <input type="button" id="btnviewinmap" data-ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                <input type="submit" data-ng-click="RaiseRequest()" value="Raise Request" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                    </div>

                </form>
            </div>



        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Plot on the Map</h4>
                </div>

                <form role="form" name="form2" id="form3">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="filter" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                        data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="button" id="Button2" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                </div>
                                <div class="col-md-4">
                                    Legends:
                                        <span class="label label-success pull-right">Available </span>
                                    <span class="label selectedseat pull-right">Selected Seats </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="main">
                                    <div id="leafletMap"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#EXTN_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>

    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/SpaceExtension.js"></script>
</body>
</html>
