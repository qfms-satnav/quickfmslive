﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }
    </style>
    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>

</head>
<body data-ng-controller="ViewSpaceExtReqController" class="amantra" onload="setDateVals()">
    <div class="animsition" data-ng>
        <div class="al-content">
            <div class="widgets">
                <h3>Space Extension Approval</h3>
            </div>
            <div class="card">
                <div class="clearfix">
                    <div class="box-footer text-right">
                        <span style="color: red;">*</span>  Mandatory field
                    </div>
                </div>
                <br />
                <div data-ng-show="Viewstatus==0">
                    <form role="form" id="ViewSpcExtnReq" name="frmViewVerReq" data-valid-submit="UpdateAllRequests()" novalidate>
                        <h5>My Requisitions</h5>
                        <div style="height: 200px">
                            <input id="ReqFilter" class="form-control" placeholder="Filter..." type="text" style="width: 25%" />
                            <div data-ag-grid="MyrequisitonsOptions" style="height: 104%; width: 100%" class="ag-blue"></div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <h5>Pending Requisitions / Waiting For Approval</h5>
                        <div style="height: 200px">
                            <input id="ApprvlFilter" class="form-control" placeholder="Filter..." type="text" style="width: 25%;" />
                            <div data-ag-grid="approvalOptions" style="height: 104%;" class="ag-blue"></div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">Remarks</div>
                            <div class="form-group">
                                <textarea style="width: 275px; height: 20%" data-ng-model="blkReq.SSE_APPR_REM" class="form-control" placeholder="Approval Remarks"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 52px; padding-top: 20px">
                            <input type="submit" data-ng-click="setStatus('ApproveAll')" value="Approve" class="btn btn-primary custom-button-color" />
                            <input type="submit" data-ng-click="setStatus('RejectAll')" value="Reject" class="btn btn-primary custom-button-color" />
                        </div>

                    </form>
                </div>
                <div data-ng-show="Viewstatus==1">
                    <form role="form" id="SearchSpaces" name="frmblksearch" data-valid-submit="SearchSpaces()">

                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Request ID:</b> </label>
                                    {{currentblkReq.SSE_REQ_ID}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Request By:</b> </label>
                                    {{currentblkReq.AUR_KNOWN_AS}}
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Requested Date: </b></label>
                                    {{currentblkReq.SSE_REQ_DT | date: dateformat}}
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">

                                <div class="form-group">
                                    <label><b>Status:</b> </label>
                                    {{currentblkReq.STA_DESC}}
                                </div>

                            </div>

                        </div>
                        <br />
                        <div class="clearfix">

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.CNY_NAME.$invalid}">
                                    <label for="txtcode">Country <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Getcountry" data-output-model="SEReq.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                        data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="disable">
                                    </div>
                                    <input type="text" data-ng-model="SEReq.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.CTY_NAME.$invalid}">
                                    <label for="txtcode">City <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Citylst" data-output-model="SEReq.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                        data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="disable">
                                    </div>
                                    <input type="text" data-ng-model="SEReq.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.LCM_NAME.$invalid}">
                                    <label for="txtcode">Location <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Locationlst" data-output-model="SEReq.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                        data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="disable">
                                    </div>
                                    <input type="text" data-ng-model="SEReq.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.TWR_NAME.$invalid}">
                                    <label for="txtcode">Tower <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Towerlist" data-output-model="SEReq.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                        data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="disable">
                                    </div>
                                    <input type="text" data-ng-model="SEReq.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.TWR_NAME.$invalid" style="color: red">Please select a tower </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.FLR_NAME.$invalid}">
                                    <label for="txtcode">Floor <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Floorlist" data-output-model="SEReq.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                        data-on-item-click="FloorChange()" data-on-select-all="FloorChange()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="disable">
                                    </div>
                                    <input type="text" data-ng-model="SEReq.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.FLR_NAME.$invalid" style="color: red">Please select a floor </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.VER_NAME.$invalid}">
                                    <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Verticallist" data-output-model="SEReq.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                        data-on-item-click="VerticalChange()" data-on-select-all="VerticalChange()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="disable">
                                    </div>
                                    <input type="text" data-ng-model="SEReq.selectedVerticals" name="VER_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.VER_NAME.$invalid" style="color: red">Please select Vertical </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.Cost_Center_Name.$invalid}">
                                    <label for="txtcode">{{BsmDet.Child}} <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="SEReq.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                        data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="disable">
                                    </div>
                                    <input type="text" data-ng-model="SEReq.selectedCostcenter" name="Cost_Center_Name" style="display: none" required="" />
                                    <span id="message" class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.Cost_Center_Name.$invalid" style="color: red">Please select Costcenter </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.FROM_DATE.$invalid}">
                                    <label for="txtcode">From Date <span style="color: red;">*</span></label>
                                    <div class="input-group date" id='fromdate'>
                                        <input type="text" class="form-control" data-ng-model="SEReq.FROM_DATE" data-ng-disabled="disable" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                        </span>
                                    </div>

                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.FROM_DATE.$invalid" style="color: red">Please select from date </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceExtension.$submitted && frmSpaceExtension.TO_DATE.$invalid}">
                                    <label for="txtcode">To Date <span style="color: red;">*</span></label>
                                    <div class="input-group date" id='todate'>
                                        <input type="text" class="form-control" data-ng-model="SEReq.TO_DATE" data-ng-disabled="disable" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="frmSpaceExtension.$submitted && frmSpaceExtension.TO_DATE.$invalid" style="color: red">Please select to date </span>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color" data-ng-disabled="true">Search Spaces</button>
                            <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                        </div>
                    </form>

                    <form role="form" id="ApproveSelected" name="frmApproveSelected" data-valid-submit="UpdateRequest()" novalidate>
                        <br />
                        <div class="row">
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <div class="form-group" style="visibility: hidden">Filter</div>
                                <input id="UpdteFilter" class="form-control" placeholder="Filter..." type="text" style="width: 50%;" />

                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    Extension Date
                                    <div class="input-group date " id='EXTDATE'>

                                        <input type="text" class="form-control" data-ng-model="SEM.SSED_EXTN_DT" data-ng-change="setAllDates()" id="EXTN_DATE" name="EXTN_DATE" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('EXTDATE')"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="height: 200px">
                                <div data-ag-grid="UpdateapprOptions" style="height: 104%; width: 100%" class="ag-blue"></div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="row">
                            <label id="lblErrormsg" for="lblMsg" data-ng-show="ShowDateValidationMessage" class="col-md-12 control-label" style="color: red">{{ShowDateValidationMessage}}</label>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <textarea class="form-control input-sm col-md-5" placeholder="Requestor Remarks" data-ng-model="currentblkReq.SSE_REQ_REM" name="SSE_REQ_REM" data-ng-disabled="EnableStatus == 1"></textarea>
                            </div>
                            <div data-ng-hide="EnableStatus==0 || EnableStatus==2" class="col-md-5">
                                <textarea class="form-control input-sm col-md-5" placeholder="Approver Remarks" style="height: 20%" data-ng-model="currentblkReq.SSE_APPR_REM" name="SSE_APPR_REM"></textarea>
                            </div>


                        </div>
                        <br />
                        <div class="box-footer text-right">
                            <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                            <input type="submit" data-ng-click="setStatus('Update')" value="Update" class="btn btn-primary custom-button-color" id="btnUpdate" data-ng-disabled="EnableStatus != 0" data-ng-show="EnableButton">
                            <input type="submit" data-ng-click="setStatus('Cancel')" value="Cancel" class="btn btn-primary custom-button-color" id="btnCancel" data-ng-disabled="EnableStatus != 0" data-ng-show="EnableButton">
                            <input type="submit" data-ng-click="setStatus('Approve')" value="Approve" class="btn btn-primary custom-button-color" id="btnApprove" data-ng-disabled="EnableStatus == 2" data-ng-show="!EnableButton" />
                            <input type="submit" data-ng-click="setStatus('Reject')" value="Reject" class="btn btn-primary custom-button-color" id="btnReject" data-ng-disabled="EnableStatus== 2" data-ng-show="!EnableButton" />
                        </div>

                    </form>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Plot on the Map</h4>
                </div>

                <form role="form" name="form2" id="form3">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="filter" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                        data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="button" id="Button2" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                </div>
                                <div class="col-md-4">
                                    Legends:
                                        <span class="label label-success pull-right">Available </span>
                                    <span class="label selectedseat pull-right">Selected Seats </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="main">
                                    <div id="leafletMap"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>


    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
        function setDateVals() {
            $('#EXTN_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>
    <script defer src="../Js/SpaceExtension.min.js"></script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/ViewSpaceExtReq.js"></script>
</body>

</html>

