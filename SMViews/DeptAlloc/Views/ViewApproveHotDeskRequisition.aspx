﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%--    <link href="../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

   <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
     <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
   <%--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" />
    
    <style>
        .grid-align {
            text-align: center;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            width: 400px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>

    

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                startDate: new Date(),
                format: 'mm/dd/yyyy',
                endDate: '+3m',
                autoclose: true,
                todayHighlight: true
            });
        };
        function timesetup(id) {
            //alert(1);
            $('#' + id).timepicker({
                'timeFormat': 'H:i',
                'show2400': true,
                'showDuration': true
            });
            //$('#FROM_TIME').timepicker({
            //    'showDuration': true,
            //    'timeFormat': 'g:ia'
            //});
        };


    </script>

</head>
<body data-ng-controller="ViewApproveHotDeskRequisitionController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px; background-color: #3f93f2; color: white">
                            <h3 class="panel-title">Approval request</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div data-ng-show="Viewstatus==0">
                                <%--<form role="form" id="ViewVerticalReq" name="frmViewVerReq" data-valid-submit="UpdateAllRequests()" novalidate>--%>
                                    <div class="clearfix">
                                        <h4>My Requisitions</h4>
                                        <div>
                                            <input id="ReqFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                            <div data-ag-grid="MyrequisitonsOptions" style="height: 260px;"  class="ag-blue"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12"></div>
                                    </div>
                                    <div class="clearfix" data-ng-show="GetRoleAndRM[0].AUR_ROLE!=6">
                                        <h4>Pending Requisitions / Waiting For Approval</h4>
                                        <div >
                                            <input id="ApprvlFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                            <div data-ag-grid="approvalOptions" style="height: 260px;" class="ag-blue" data-ng-init="init()"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"></div>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                
                                                <textarea style="height: 20%" data-ng-model="HD_L1_REM" class="form-control" placeholder="L1 Approval Remarks"></textarea>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                               
                                                <textarea style="height: 20%" data-ng-model="HD_L2_REM" class="form-control" placeholder="L2 Approval Remarks"></textarea>
                                            </div>
                                            <input type="submit" value="Approve" class="btn btn-primary custom-button-color" data-ng-show="Approve" data-ng-click="setStatus('ApproveAll')" />
                                            <input type="submit" value="Reject" class="btn btn-primary custom-button-color" data-ng-click="setStatus('RejectAll')" />
                                        </div>
                                    </div>
                                <%--</form>--%>
                            </div>
                            <div data-ng-show="Viewstatus==1">
                               
                                 <div class="panel-body" style="padding-right: 10px; min-height: 138px; !important">
                                <div>
                                    <h5 class="panel-title">Employee Details:</h5>
                                </div>
                                <br />
                                <br />
                                <div class="clearfix">
                                    <div class="col-md-4 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp:</b> </label>
                                            {{HDRequest.AUR_ID}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Employee Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp:</b> </label>
                                            {{HDRequest.AUR_KNOWN_AS}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-8 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Email Id &nbsp:</b> </label>
                                            {{HDRequest.AUR_EMAIL}}
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-4 col-sm-8 col-xs-12 ">
                                        <div class="form-group">
                                            <label><b>Employee Location&nbsp;&nbsp:</b> </label>
                                            {{HDRequest.LCM_NAME}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-8 col-xs-12 ">
                                        <div class="form-group">
                                            <label><b>Employee Vertical&nbsp;&nbsp:</b> </label>
                                            {{HDRequest.VER_NAME}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-8 col-xs-12 ">
                                        <div class="form-group">
                                            <label><b>Employee Project&nbsp;&nbsp:</b> </label>
                                            {{HDRequest.Cost_Center_Name}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="panel-heading" style="height: 41px; background-color: #209e91; color: white">
                                    <h3 class="panel-title">Search Spaces</h3>
                                </div>
                                <div class="panel-body" style="padding-right: 10px;">
                                    <div class="clearfix">
                                        <div class="col-md-4 col-sm-8 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Location</label>
                                                <div isteven-multi-select data-input-model="Locations" data-output-model="HotDeskRequisition.Locations" data-button-label="icon LCM_NAME"
                                                    data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-8 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Tower</label>
                                                <div isteven-multi-select data-input-model="Towers" data-output-model="HotDeskRequisition.Towers" data-button-label="icon TWR_NAME "
                                                    data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-8 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Floor</label>
                                                <div isteven-multi-select data-input-model="Floors" data-output-model="HotDeskRequisition.Floors" data-button-label="icon FLR_NAME"
                                                    data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange(HotDeskRequisition.Floors)" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-sm-6 col-xs-12">
                                        <br />
                                        <div class="box-footer text-right">
                                            <input type="button" value="Search" class="btn btn-primary custom-button-color" data-ng-click="GetVacantSeats()"  />
                                            <%--data-ng-disabled="Searchbutton"--%>
                                        </div>
                                    </div>
                                </div>
                            </div>


                           <div class="clearfix">   
                                  
                               <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="col-md-4 col-sm-3 col-xs-3 pull-left" >
                                         <div class="form-group">
                                             <label class="control-label">Requested Employee ID/Name </label>
                                             <input type="text" class="form-control" name="HD_AUR_ID" data-ng-model="HD_AUR_ID" data-ng-disabled="true" />
                                         </div>
                                   </div>
                                   
                                   <div class="col-md-4 col-sm-3 col-xs-3 pull-left" >
                                         <div class="form-group">
                                             <label class="control-label">Requested Employee Division </label>
                                             <input type="text" class="form-control" name="VER_NAME" data-ng-model="VER_NAME" data-ng-disabled="true"  /> 
                                          </div>
                                    </div>

                                   <div class="col-md-4 col-sm-3 col-xs-3 pull-left">

                                        <div class="form-group">
                                              <label class="control-label">Requested Employee TeamNo </label>
                                              <input type="text"  class="form-control" name="Cost_Center_Name" data-ng-model="Cost_Center_Name" data-ng-disabled="true"  /> 
                                        </div>
                                   </div>

                                   <%--<div class="col-md-3 col-sm-3 col-xs-3 pull-left">

                                        <div class="form-group">
                                              <label class="control-label">Requested FromDate </label>
                                              <input type="text"  class="form-control" name="HD_FROM_DATE" data-ng-model="HD_FROM_DATE" data-ng-disabled="true" /> 
                                        </div>
                                   </div>

                                   <div class="col-md-3 col-sm-3 col-xs-3 pull-left">

                                        <div class="form-group">
                                              <label class="control-label">Requested ToDate </label>
                                              <input type="text"  class="form-control" name="HD_TO_DATE"" data-ng-model="HD_TO_DATE" data-ng-disabled="true"/> 
                                        </div>
                                   </div>

                                   <div class="col-md-3 col-sm-3 col-xs-3 pull-left">

                                        <div class="form-group">
                                              <label class="control-label">Requested FromTime </label>
                                              <input type="text"  class="form-control" name="HD_FROM_TIME" data-ng-model="HD_FROM_TIME" data-ng-disabled="true" /> 
                                        </div>
                                   </div>

                                   <div class="col-md-3 col-sm-3 col-xs-3 pull-left">

                                        <div class="form-group">
                                              <label class="control-label">Requested ToTime </label>
                                              <input type="text"  class="form-control" name="HD_TO_TIME" data-ng-model="HD_TO_TIME" data-ng-disabled="true"/> 
                                        </div>
                                   </div>--%>


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <div class="input-group date" id='fromdate'>
                                                    <input type="text" class="form-control" data-ng-model="HD_FROM_DATE" id="FromDate" name="FromDate"
                                                        placeholder="mm/dd/yyyy"/>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <div class="input-group date" id='todate'>
                                                    <input type="text" class="form-control" data-ng-model="HD_TO_DATE" id="ToDate" name="ToDate"
                                                        placeholder="mm/dd/yyyy"/>
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>From Time</label>
                                                        <div class="input-group">
                                                            <input type="{{texttype}}" id="fromtime" name="FromTimeFilter" class="form-control timepicker" data-ng-model="HD_FROM_TIME"
                                                                required="" data-ng-disabled="statsss == 2" onclick="timesetup('fromtime')" data-ng-click="changeFunctin()" />
                                                          <%--  <div class="input-group-addon">
                                                                <i class="fa fa-clock-o" onclick="timesetup('fromtime')"></i>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <label>To Time</label>
                                                        <div class="input-group">
                                                            <input type="{{texttype}}" id="totime" name="FromTimeFilter" class="form-control timepicker" data-ng-model="HD_TO_TIME"
                                                                required="" data-ng-disabled="statsss == 2"  onclick="timesetup('totime')" data-ng-click="changeFunctin()"   />
                                                         <%--   <div class="input-group-addon">
                                                                <i class="fa fa-clock-o" onclick="timesetup('totime')"></i>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                               </div>
                           </div>
                       </div>


                            <div class="col-md-8 col-sm-12 col-xs-12" style="width: 100%" data-ng-show="grid">
                                <div style="height: 325px;">
                                    <input id="filtertxt" class="form-control" placeholder="Searh By Any..." type="text" style="width: 30%"  data-ng-show="GetRoleAndRM[0].AUR_ROLE !=6 && UpdateapprOptionsShow" />
                                      <div data-ag-grid="UpdateapprOptions"   style="height: 260px; width: 80%" class="ag-blue" data-ng-show="GetRoleAndRM[0].AUR_ROLE !=6 && UpdateapprOptionsShow"></div>
                                    <br />
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-3 col-xs-3 pull-left">
                                            <div class="form-group">
                                                <label class="control-label">Requestor Remarks </label>
                                                <textarea rows="1" cols="15" name="HD_REQ_REM" data-ng-model="HD_REQ_REM" class="form-control" placeholder="Requestor remarks"></textarea>
                                            </div>
                                        </div>
                                         <div class="col-md-3 col-sm-3 col-xs-3 pull-left" ng-show="L1_REM">
                                            <div class="form-group">
                                                <label class="control-label">L1 Remarks </label>
                                                <textarea rows="1" cols="15" name="L1_REQ_REM" data-ng-model="L1_REQ_REM" class="form-control" placeholder="L1 remarks"></textarea>
                                            </div>
                                        </div>
                                         <div class="col-md-3 col-sm-3 col-xs-3 pull-left" ng-show="L2_REM">
                                            <div class="form-group">
                                                <label class="control-label">L2 Remarks </label>
                                                <textarea rows="1" cols="15" name="L2_REQ_REM" data-ng-model="L2_REQ_REM" class="form-control" placeholder="L2 remarks"></textarea>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="box-footer text-right">
                                                <input type="submit" value="Update" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus != 0" data-ng-hide="EnableStatus ==1" data-ng-click="setStatus('Update')" />
                                                <input type="submit" value="Cancel" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus != 0" data-ng-hide="EnableStatus ==1" data-ng-click="setStatus('Cancel')" />
                                                <input type="submit" value="Approve" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus ==2" data-ng-hide="EnableStatus !=1" data-ng-click="setStatus('Approve')" />
                                                <input type="submit" value="Reject" class="btn btn-primary custom-button-color" data-ng-disabled="EnableStatus ==2 " data-ng-hide="EnableStatus !=1" data-ng-click="setStatus('Reject')" />
                                                <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="slide_block">
        <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H1">Plot On the Map</h4>
                    </div>

                    <form role="form" name="form2" id="form3">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                            data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                    </div>
                                    <div class="col-md-4">
                                        Legends:
                                        <span class="label label-success pull-right">Available </span>
                                        <span class="label selectedseat pull-right">Selected Seats </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="main">
                                        <div id="leafletMap"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
     <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
     <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
   
     <script defer>
         var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
         var UID = '<%= Session["UID"] %>';

         (function ($) {
             $(function () {
                 $('#timed').timepicker();
             });
         })(jQuery);

    </script>
    <script defer src="../../Utility.js"></script>
    <script src="../Js/HotDeskRequisition.js"></script>
    <script src="../Js/ViewApproveHotDeskRequisition.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
</body>

</html>

