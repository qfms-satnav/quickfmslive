﻿app.service("HotDeskRequisitionService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetEmployeeDetails = function () {

        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HotDeskRequisition/GetEmployeeDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetEmployeeDetails = function (data) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HotDeskRequisition/GetEmployeeDetails?AUR_ID=' + data + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.EmpChange = function (data) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HotDeskRequisition/EmpChange?AUR_ID=' + data + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetVacantSeats = function (data) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HotDeskRequisition/GetVacantSeats', data)
            .then(function (response) {

                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetSelectedfloors = function (data) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HotDeskRequisition/GetSelectedfloors', data)
            .then(function (response) {

                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.RaiseRequst = function (data) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HotDeskRequisition/RaiseRequst', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetVerCosByEmpName = function (data) {

        //console.log(data);
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HotDeskRequisition/GetVerCosByEmpName?AUR_ID=' + data + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


}]);

app.controller('HotDeskRequisitionController', ['$scope', '$q', 'HotDeskRequisitionService', 'UtilityService', '$filter', function ($scope, $q, HotDeskRequisitionService, UtilityService, $filter) {

    $scope.HotDeskRequisition = {};

    $scope.HotDeskRequisition1 = [];

    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.grid = false;
    $scope.selectedspaces = [];
    $scope.RetStatus = UtilityService.Added;

    UtilityService.GetRoleAndReportingManger().then(function (response) {
        if (response.data != null) {
            $scope.GetRoleAndRM = response.data;
        }
    });
    $scope.columDefsAlloc = [
        { headerName: "Select", field: "ticked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.disabled' ng-model='data.ticked' ng-change='chkChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPC_ID", width: 150, cellClass: "grid-align", pinned: 'left' },
        { headerName: "From Date", field: "FROM_DATE", width: 150, cellClass: 'grid-align', cellRenderer: createFromDatePicker },
        { headerName: "To Date", field: "TO_DATE", width: 150, cellClass: 'grid-align', cellRenderer: createToDatePicker },
        { headerName: "From Time", field: "FROM_TIME", width: 150, cellClass: 'grid-align', filter: 'set', cellRenderer: createFromTimePicker },
        { headerName: "To Time", field: "TO_TIME", width: 150, cellClass: 'grid-align', filter: 'set', cellRenderer: createToTimePicker },
        { headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME", cellRenderer: customEditor },
        { headerName: "Division", cellClass: "grid-align", width: 110, field: "VER_NAME", template: "<label> {{data.VER_NAME}}</label>" },
        { headerName: "Team No", cellClass: "grid-align", width: 110, field: "Cost_Center_Name", template: "<label> {{data.Cost_Center_Name}}</label>" },


    ];

    $scope.chkChanged = function (data) {
        var role = _.find($scope.GetRoleAndRM);
        var count = _.countBy($scope.gridOptions.rowData, { ticked: true });


        if (role.AUR_ROLE == 6 && count.true >= 2) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select only one space id for employee");
            data.ticked = false;
            $scope.gridOptions.api.refreshView();
            return;
        }
    }
    $scope.gridOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false
    };
    $scope.Clear = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Floors, function (value, key) {
            value.ticked = false;
        });

    }
    UtilityService.getAurNames().then(function (response) {
        if (response.data != null) {
            $scope.AurNames = [];
            $scope.AurNames.push({ "AUR_ID": "", "AUR_NAME": "", "Cost_Center_Code": "", "Cost_Center_Name": "", "VER_CODE": "", "VER_NAME": "" })
            angular.forEach(response.data, function (value, key) {
                $scope.AurNames.push({ "AUR_ID": value.AUR_ID, "AUR_NAME": value.AUR_NAME, "Cost_Center_Code": value.Cost_Center_Code, "Cost_Center_Name": value.Cost_Center_Name, "VER_CODE": value.VER_CODE, "VER_NAME": value.VER_NAME });
            });
        }
    });
    function customEditor(params) {

        var editing = false;
        var eCell = document.createElement('div');
        var eLabel = document.createTextNode('Please Select');
        if (params.value)
            eLabel.nodeValue = params.value;
        eCell.appendChild(eLabel);

        var eSelect = document.createElement("select");
        eSelect.setAttribute("width", 50);
        eCell.addEventListener('click', function () {
            if (!editing) {

                progress(0, 'Loading...', true);

                if (params.column.colId === "AUR_NAME") {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");

                    angular.forEach($scope.AurNames, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);

                        angular.forEach($scope.gridOptions.rowData, function (val, key) {
                            if (val.AUR_ID == item.AUR_ID) {
                                eOption.setAttribute("disabled", item.AUR_ID);
                            }
                        });
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);




                    });
                    progress(0, '', false);

                }

                else {
                    $scope.GetVerCosByEmpName(params.data, eSelect);
                }


                setTimeout(function () {
                    eSelect.value = params.value;

                    eCell.removeChild(eLabel);
                    eCell.appendChild(eSelect);

                    //console.log(eCell);
                    eSelect.focus();
                    editing = true;
                }, 200);
            }
        });

        eSelect.addEventListener('blur', function () {

            if (editing) {

                editing = false;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
            }
        });
        eSelect.addEventListener('change', function () {
            if (editing) {
                editing = false;

                var newValue = eSelect.options[eSelect.selectedIndex].text;
                eLabel.nodeValue = newValue;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
                if (params.column.colId === "AUR_NAME") {
                    HotDeskRequisitionService.EmpChange(eSelect.value).then(function (response) {
                        if (response.data != null && response.data.length != 0) {
                            if (response.data[0].SSAD_STA_ID != null) {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', "Employee Already occupied");
                                params.data.AUR_ID = ""
                                params.data.AUR_NAME = ""
                                $scope.gridOptions.api.refreshView();
                                return;
                            }
                            else if (response.data[0].HD_STATUS != null) {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', "Employee Already under requisition");
                                params.data.AUR_ID = ""
                                params.data.AUR_NAME = ""
                                $scope.gridOptions.api.refreshView();
                                return;
                            }

                        }
                        else {
                            params.data.AUR_ID = eSelect.value;
                            params.data.AUR_NAME = newValue;
                            $scope.GetVerCosByEmpName(params.data, eSelect);
                        }
                    });


                }
            }
        });
        return eCell;
    }

    $scope.GetVerCosByEmpName = function (data, eSelect) {


        HotDeskRequisitionService.GetVerCosByEmpName(data.AUR_ID).then(function (response) {



            data.AUR_ID = response.data[0].AUR_ID;
            data.AUR_NAME = response.data[0].AUR_NAME;

            data.VERTICAL = response.data[0].VER_CODE;
            data.VER_NAME = response.data[0].VER_NAME;
            data.Cost_Center_Code = response.data[0].COST_CODE;
            data.Cost_Center_Name = response.data[0].Cost_Center_Name;

            $scope.gridOptions.api.refreshView();


        }, function (error) {
            progress(0, '', false);
            console.log(error);

        })


    };


    function removeOptions(selectbox) {
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    var fromdate, todate, fromtime, totime;
    function createFromDatePicker(params) {

        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.FROM_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        fromdate = params.data.FROM_DATE;


        $(newDate).datepicker({
            startDate: new Date(),
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        }).on("input change", function (e) {

            if (params.data.TO_DATE == null || params.data.TO_DATE == "" || params.data.TO_DATE == undefined) {

                fromdate = e.target.value;
            }
            else {
                if (params.data.FROM_DATE > e.target.value) {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', "From Date should be greater than To Date");
                    params.data.FROM_DATE = ""
                    $scope.gridOptions.api.refreshView();
                    return;
                }
                else {
                    fromdate = e.target.value;
                }
            }
        });
        return newDate;
    }

    function createToDatePicker(params) {

        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.TO_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        fromdate = params.data.FROM_DATE;


        $(newDate).datepicker({
            startDate: new Date(),
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true

        }).on("input change", function (e) {

            if (params.data.FROM_DATE == null || params.data.FROM_DATE == "" || params.data.FROM_DATE == undefined) {

                todate = e.target.value;
            }
            else {
                if (params.data.FROM_DATE > e.target.value) {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', "From Date should be greater than To Date");
                    params.data.TO_DATE = ""
                    $scope.gridOptions.api.refreshView();
                    return;
                }
                else {
                    todate = e.target.value;
                }
            }


        });
        return newDate;
    }
    function createFromTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.FROM_TIME');
        newTime.type = "text";
        newTime.id = 'fromtime' + params.rowIndex;
        newTime.className = "pickDate";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true,
                //scrollbar: true

            }).on("input change", function (e) {

                if (params.data.TO_TIME == null || params.data.TO_TIME == "" || params.data.TO_TIME == undefined) {

                    fromtime = e.target.value;
                }
                else {
                    if (params.data.FROM_TIME > e.target.value) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', "From Time should be lesser than To Time");
                        params.data.FROM_TIME = ""
                        $scope.gridOptions.api.refreshView();
                        return;
                    }
                    else {
                        fromtime = e.target.value;
                    }
                }

            });
        return newTime;
    }

    function createToTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.TO_TIME');
        newTime.type = "text";
        newTime.id = 'totime' + params.rowIndex;
        newTime.className = "pickDate";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true,
                //scrollbar: true

            }).on("input change", function (e) {


                if (params.data.FROM_TIME == null || params.data.FROM_TIME == "" || params.data.FROM_TIME == undefined) {

                    totime = e.target.value;
                }
                else {
                    if (params.data.FROM_TIME > e.target.value) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', "From Time should be lesser than To Time");
                        params.data.TO_TIME = ""
                        $scope.gridOptions.api.refreshView();
                        return;
                    }
                    else {
                        totime = e.target.value;
                    }
                }
            });
        return newTime;
    }
    HotDeskRequisitionService.GetEmployeeDetails().then(function (response) {
        if (response.data != null) {
            $scope.HDRequest = response.data[0];
        }
    });


    $scope.RaiseRequst = function () {

         // if ($scope.HotDeskRequisition1.FROM_TIME && $scope.HotDeskRequisition1.FROM_TIME.length < 4) {
            // $scope.HotDeskRequisition1.FROM_TIME = $scope.HotDeskRequisition1.FROM_TIME + ':00';
        // }
        // if ($scope.HotDeskRequisition1.TO_TIME && $scope.HotDeskRequisition1.TO_TIME.length < 4) {
            // $scope.HotDeskRequisition1.TO_TIME = $scope.HotDeskRequisition1.TO_TIME + ':00';
        // }
		
		 var dt = new Date($scope.HotDeskRequisition1.FROM_TIME);
        var dt1 = new Date($scope.HotDeskRequisition1.TO_TIME);
        $scope.HD_FROM_TIME = (dt.getHours() > 9 ? dt.getHours() : '0' + dt.getHours()) + ':' + (dt.getMinutes() > 9 ? dt.getMinutes() : '0' + dt.getMinutes());
        $scope.HD_TO_TIME = (dt1.getHours() > 9 ? dt1.getHours() : '0' + dt1.getHours()) + ':' + (dt1.getMinutes() > 9 ? dt1.getMinutes() : '0' + dt1.getMinutes());

        var param = {
            SPC_ID: null,
            FROM_DATE: $scope.HotDeskRequisition1.FROM_DATE,
            TO_DATE: $scope.HotDeskRequisition1.TO_DATE,
            FROM_TIME:  $scope.HD_FROM_TIME,
            TO_TIME: $scope.HD_TO_TIME,
        };
        $scope.HotDeskRequisition1[0] = param;
        
        if ($scope.HotDeskRequisition.Locations == "" || $scope.HotDeskRequisition.Locations == undefined || $scope.HotDeskRequisition.Locations == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select Location");
            return;
        }

        if ($scope.HotDeskRequisition.Towers == "" || $scope.HotDeskRequisition.Towers == undefined || $scope.HotDeskRequisition.Towers == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select Tower");
            return;
        }

        if ($scope.HotDeskRequisition.Floors == "" || $scope.HotDeskRequisition.Floors == undefined || $scope.HotDeskRequisition.Floors == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select Floor");
            return;
        }

        if ($scope.HotDeskRequisition1.FROM_DATE == "" || $scope.HotDeskRequisition1.FROM_DATE == undefined || $scope.HotDeskRequisition1.FROM_DATE == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select From Date");
            return;
        }

        if ($scope.HotDeskRequisition1.TO_DATE == "" || $scope.HotDeskRequisition1.TO_DATE == undefined || $scope.HotDeskRequisition1.TO_DATE == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select To Date");
            return;
        }
        if ($scope.HotDeskRequisition1.FROM_TIME == "" || $scope.HotDeskRequisition1.FROM_TIME == undefined || $scope.HotDeskRequisition1.FROM_TIME == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select From Time");
            return;
        }

        if ($scope.HotDeskRequisition1.TO_TIME == "" || $scope.HotDeskRequisition1.TO_TIME == undefined || $scope.HotDeskRequisition1.TO_TIME == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select To Time");
            return;
        }
		

       
        if ($scope.HotDeskRequisition.REQ_REM == "" || $scope.HotDeskRequisition.REQ_REM == undefined || $scope.HotDeskRequisition.REQ_REM == null) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please enter Remarks");
            return;
        }

        if ($scope.RetStatus != 32) {
            var data = { 
                LCMlst: $scope.HotDeskRequisition.Locations, twrlst: $scope.HotDeskRequisition.Towers, flrlst: $scope.HotDeskRequisition.Floors, REM: $scope.HotDeskRequisition.REQ_REM, ALLOCSTA: $scope.RetStatus, HDLN: $scope.HotDeskRequisition1 , REQ_ID: "", L1_REM: "", L2_REM: "" };
            HotDeskRequisitionService.RaiseRequst(data).then(function (response) {
                if (response != null) {
                    showNotification('success', 8, 'bottom-right', response.Message);
                    $scope.selectedspaces = [];
                    $scope.grid = false;
                    $scope.Clear();

                }
            });

        }


    }

    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);
        UtilityService.getSysPreferences().then(function (response) {
            if (response.data != null) {
                $scope.SysPreference = response.data;
                $scope.AllocationType = _.find($scope.SysPreference, { SYSP_CODE: "Allocation Type" })
                $scope.ReqCountType = _.find($scope.SysPreference, { SYSP_CODE: "Requisition Count Type" })
            }
        });

        UtilityService.getCountires(2).then(function (response) {

            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;


                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;

                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                progress(0, '', false);
                                            }
                                        });

                                    }
                                });
                            }
                        });

                    }
                });
            }
        });



    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.HotDeskRequisition.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });

    }

    $scope.locSelectAll = function () {
        $scope.HotDeskRequisition.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.HotDeskRequisition.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        $scope.HotDeskRequisition.Locations = [];
        angular.forEach($scope.Towers, function (value, key) {

            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });

            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.HotDeskRequisition.Locations.push(lcm);

            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.HotDeskRequisition.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }





    $scope.FloorChange = function () {


        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });



        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.HotDeskRequisition.Locations.push(lcm);
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.HotDeskRequisition.Towers.push(twr);

            }
        });
    }
    $scope.floorChangeAll = function () {
        $scope.HotDeskRequisition.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    $scope.CostCenterChagned = function () {
        angular.forEach($scope.Verticals, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (value, key) {
            var ver = _.find($scope.Verticals, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.HotDeskRequisition.Verticals[0] = ver;
            }
        });
    };

    setTimeout(function () {
        $scope.LoadDetails();
    }, 200);


    ///// For map layout
    var map = L.map('leafletMap');
    $scope.ViewinMap = function () {


        $scope.SelRowData = [];
        setTimeout(function () {
            if ($scope.SelRowData.length == 0) {
                $scope.MapFloors = [];
                $scope.MapFloor = [];
                angular.forEach($scope.HotDeskRequisition.Floors, function (Value, Key) {
                    Value.ticked = false;
                    $scope.MapFloors.push(Value);
                });
                $scope.MapFloors[0].ticked = true;
                $scope.MapFloor.push($scope.MapFloors[0]);
            }
        }, 100);
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {

        if ($scope.SelRowData.length == 0) {
            setTimeout(function () {
                if ($scope.MapFloor.length > 0 && $scope.MapFloor[0].FLR_CODE)
                    $scope.loadmap($scope.MapFloor[0].FLR_CODE);
                else
                    showNotification('error', 8, 'bottom-right', "Please select Floor");
            }, 1000);
        }

    });

    $scope.FlrSectMap = function (data) {

        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        setTimeout(function () {
            $scope.loadmap(data.FLR_CODE);
        }, 1000);

    }


    $scope.loadmap = function (FLR_CODE) {
        progress(0, 'Loading...', true);
        //$('#leafletMap').empty();

        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: FLR_CODE, key_value: 1 };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {

            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SPACE_ID = value.SPACE_ID;
            theLayer.options.checked = false;

            //var SeattypeLayer = $.extend(true, {}, theLayer);

            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            function (results) {

                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);

                //$scope.SelRowData = $filter('filter')($scope.selectedfloors, { FLR_CODE: $scope.MapFloor[0].FLR_CODE });

                angular.forEach($scope.gridOptions.rowData, function (value, key) {

                    $scope.marker1 = _.find($scope.drawnItems._layers, { options: { SPACE_ID: value.SPC_ID, spacetype: 'CHA' } });

                    if (value.ticked == true && $scope.marker1 != undefined) {
                        $scope.marker1.setStyle(selctdChrStyle);
                        $scope.marker1.bindLabel('Vacant: ' + value.SPC_ID);
                        $scope.marker1.on('click', markerclicked);
                        $scope.marker1.addTo(map);
                    }
                    else if (value.ticked == false && $scope.marker1 != undefined) {
                        $scope.marker1.setStyle(VacantStyle);
                        $scope.marker1.bindLabel('Vacant: ' + value.SPC_ID);
                        $scope.marker1.on('click', markerclicked);
                        $scope.marker1.addTo(map);
                    }


                });
            },
            function (response) {
            }
        );
    };
    function markerclicked(e) {
        console.log(e)
        console.log(this.options.SPACE_ID)
        var marker = _.find($scope.gridOptions.rowData, { SPC_ID: this.options.SPACE_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }



    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };
    var OccupiedStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
    var AllocateStyle = { fillColor: '#3498DB', opacity: 0.8, fillOpacity: 0.8 };

}]);