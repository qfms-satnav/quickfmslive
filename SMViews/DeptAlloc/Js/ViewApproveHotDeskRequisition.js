﻿app.service("ViewApproveHotDeskRequisitionService", function ($http, $q, UtilityService) {

    this.GetMyReqList = function (id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewApproveHotDeskRequisition/GetMyReqList?id=' + id + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetDetailsOnSelection = function (Req_Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewApproveHotDeskRequisition/GetDetailsOnSelection?Req_Id=' + Req_Id + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.ApproveRequests = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewApproveHotDeskRequisition/ApproveRequests', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.UpdateHdmFloor = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewApproveHotDeskRequisition/UpdateHdmFloor', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GetVerCosByEmpName = function (data) {

        //console.log(data);
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HotDeskRequisition/GetVerCosByEmpName?AUR_ID=' + data + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.RaiseRequstValidatin = function (data) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HotDeskRequisition/RaiseRequstValidatin', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});
app.controller('ViewApproveHotDeskRequisitionController', function ($scope, $q, ViewApproveHotDeskRequisitionService, UtilityService, $filter, HotDeskRequisitionService) {
    $scope.Viewstatus = 0;
    $scope.Searchbutton = true;
    $scope.RetStatus = UtilityService.Added;
    $scope.L1_REM = false;
    $scope.L2_REM = false;
    $scope.Approve = true;
    $scope.UpdateapprOptionsShow = true;


    UtilityService.GetRoleAndReportingManger().then(function (response) {
        if (response.data != null) {
            $scope.GetRoleAndRM = response.data;

        }
    });

    $scope.FloorChange = function () {


        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });



        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.HotDeskRequisition.Locations.push(lcm);
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.HotDeskRequisition.Towers.push(twr);

            }
        });
    }



    //setTimeout(function ()
    //{
    //    UtilityService.GetRoleAndReportingManger().then(function (response) {
    //        if (response.data != null) {
    //            $scope.GetRoleAndRM = response.data;
    //        }
    //    });

    //}, 1000);


    //$scope.$watch("GetRoleAndRM", function () {
    //    console.log($scope.GetRoleAndRM);
    //});

    $scope.MyrequisitonsDefs = [

        { headerName: "Requisition ID", width: 350, field: "REQ_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data,false)">{{data.REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
        { headerName: "Status", width: 350, field: "STA_DESC", cellClass: "grid-align", pinned: 'left' },
        { headerName: "Requested By", width: 350, field: "REQ_BY", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", width: 375, template: '<span>{{data.REQ_DATE | date:"dd MMM, yyyy"}}</span>', field: "REQ_DATE", cellClass: "grid-align", suppressMenu: true }
    ];

    $scope.MyrequisitonsOptions = {
        columnDefs: $scope.MyrequisitonsDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        onready: function () {
            $scope.MyrequisitonsOptions.api.sizeColumnsToFit();
        }
    };


    $scope.approvalsDefs = [

        { headerName: "Select All", field: "ticked", template: "<input type='checkbox' ng-model='data.ticked'>", cellClass: 'grid-align', filter: 'set', suppressMenu: true, pinned: 'left' },
        { headerName: "Requisition ID", field: "REQ_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data,false)">{{data.REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
        { headerName: "Status", field: "STA_DESC", cellClass: "grid-align", pinned: 'left' },
        { headerName: "Requested By", field: "REQ_BY", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Location", field: "REQ_LOC", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Tower", field: "REQ_TWR", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Floor", field: "REQ_FLR", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requestor Remarks", field: "REQ_REM", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", template: '<span>{{data.REQ_DATE | date:"dd MMM, yyyy"}}</span>', field: "REQ_DATE", cellClass: "grid-align", suppressMenu: true }

    ];

    //if ($scope.GetRoleAndRM == 14) {

    //        $scope.approvalsDefs = [

    //            { headerName: "Select All", field: "ticked", template: "<input type='checkbox' ng-model='data.ticked'>", cellClass: 'grid-align', filter: 'set', suppressMenu: true, pinned: 'left' },
    //            { headerName: "Requisition ID", field: "REQ_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data,false)">{{data.REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
    //            { headerName: "Status", field: "STA_DESC", cellClass: "grid-align", pinned: 'left' },
    //            { headerName: "Requested By", field: "REQ_BY", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Location", field: "REQ_LOC", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Tower", field: "REQ_TWR", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Floor", field: "REQ_FLR", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requestor Remarks", field: "REQ_REM", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Date ", template: '<span>{{data.REQ_DATE | date:"dd MMM, yyyy"}}</span>', field: "REQ_DATE", cellClass: "grid-align", suppressMenu: true }

    //        ];
    //    }

    //    else {

    //        $scope.approvalsDefs = [

    //            { headerName: "Select All", field: "ticked", template: "<input type='checkbox' ng-model='data.ticked'>", cellClass: 'grid-align', filter: 'set', suppressMenu: true, pinned: 'left' },
    //            { headerName: "Requisition ID", field: "REQ_ID", cellClass: "grid-align", filter: 'set', pinned: 'left', suppressMenu: true },
    //            { headerName: "Status", field: "STA_DESC", cellClass: "grid-align", pinned: 'left' },
    //            { headerName: "Requested By", field: "REQ_BY", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Location", field: "REQ_LOC", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Tower", field: "REQ_TWR", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Floor", field: "REQ_FLR", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requestor Remarks", field: "REQ_REM", cellClass: "grid-align", suppressMenu: true, },
    //            { headerName: "Requested Date ", template: '<span>{{data.REQ_DATE | date:"dd MMM, yyyy"}}</span>', field: "REQ_DATE", cellClass: "grid-align", suppressMenu: true }

    //        ];

    //    }


    $scope.approvalOptions = {
        columnDefs: $scope.approvalsDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        onReady: function () {
            $scope.approvalOptions.api.sizeColumnsToFit()
        },

    };

    //console.log($scope.GetRoleAndRM);

    $scope.columDefsAlloc = [
        { headerName: "Select", field: "ticked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.disabled' ng-model='data.ticked' ng-change='chkChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPC_ID", width: 150, cellClass: "grid-align", pinned: 'left' },
        { headerName: "Seat Type", field: "Seat_Type", width: 150, cellClass: "grid-align", pinned: 'left' },
        { headerName: "From Date", field: "FROM_DATE", width: 150, cellClass: 'grid-align', cellRenderer: createFromDatePicker },
        { headerName: "To Date", field: "TO_DATE", width: 150, cellClass: 'grid-align', cellRenderer: createToDatePicker },
        { headerName: "From Time", field: "FROM_TIME", width: 150, cellClass: 'grid-align', cellRenderer: createFromTimePicker, cellStyle: { "overflow": 'visible' }, },
        { headerName: "To Time", field: "TO_TIME", width: 150, cellClass: 'grid-align', cellRenderer: createToTimePicker },
        //{ headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_ID", cellRenderer: customEditor },
        //{ headerName: "Division", cellClass: "grid-align", width: 110, field: "VER_NAME", template: "<label> {{data.VER_NAME}}</label>" },
        //{ headerName: "Team No", cellClass: "grid-align", width: 110, field: "Cost_Center_Name", template: "<label> {{data.Cost_Center_Name}}</label>" },


    ];

    $scope.UpdateapprOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,


    };


    $scope.chkChanged = function (data) {

        data.FROM_DATE = $scope.HD_FROM_DATE;
        data.TO_DATE = $scope.HD_TO_DATE;
        data.FROM_TIME = $scope.HD_FROM_TIME;
        data.TO_TIME = $scope.HD_TO_TIME;


        for (var i = 0; i < $scope.UpdateapprOptions.rowData.length; i++) {

            if ($scope.UpdateapprOptions.rowData[i].SPC_ID != data.SPC_ID) {
                //$scope.UpdateapprOptions.rowData[i].Seat_Type = data.Seat_Type;

                $scope.UpdateapprOptions.rowData[i].ticked = false;
                $scope.UpdateapprOptions.rowData[i].FROM_DATE = null;
                $scope.UpdateapprOptions.rowData[i].TO_DATE = null;
                $scope.UpdateapprOptions.rowData[i].FROM_TIME = null;
                $scope.UpdateapprOptions.rowData[i].TO_TIME = null;
            }
        }


        var role = _.find($scope.GetRoleAndRM);
        var count = _.countBy($scope.UpdateapprOptions.rowData, { ticked: true });

        $scope.UpdateapprOptions.api.refreshView();

        if (role.AUR_ROLE == 6 && count.true >= 2) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select only one space id for employee");
            data.ticked = false;
            if (role.AUR_ROLE == 14) {
                $scope.UpdateapprOptions.api.refreshView();
            }
            return;
        }
    }

    UtilityService.getAurNames().then(function (response) {
        if (response.data != null) {
            $scope.AurNames = response.data;

        }
    });


    function customEditor(params) {

        var editing = false;
        var eCell = document.createElement('div');
        var eLabel = document.createTextNode('Please Select');
        if (params.value)
            eLabel.nodeValue = params.value;
        eCell.appendChild(eLabel);

        var eSelect = document.createElement("select");
        eSelect.setAttribute("width", 50);
        eCell.addEventListener('click', function () {
            if (!editing) {
                progress(0, 'Loading...', true);
                if (params.column.colId === "AUR_ID") {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");
                    angular.forEach($scope.AurNames, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);
                        angular.forEach($scope.UpdateapprOptions.rowData, function (val, key) {
                            if (val.AUR_ID == item.AUR_ID) {
                                eOption.setAttribute("disabled", item.AUR_ID);
                            }
                        });
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);



                    });
                    progress(0, '', false);

                }

                else {
                    $scope.GetVerCosByEmpName(params.data, eSelect);
                }



                setTimeout(function () {
                    eSelect.value = params.value;

                    eCell.removeChild(eLabel);
                    eCell.appendChild(eSelect);

                    //console.log(eCell);
                    eSelect.focus();
                    editing = true;
                }, 200);
            }
        });

        eSelect.addEventListener('blur', function () {

            if (editing) {

                editing = false;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
            }
        });
        eSelect.addEventListener('change', function () {
            if (editing) {
                editing = false;

                var newValue = eSelect.options[eSelect.selectedIndex].text;
                eLabel.nodeValue = newValue;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
                if (params.column.colId === "AUR_ID") {

                    HotDeskRequisitionService.EmpChange(eSelect.value).then(function (response) {
                        if (response.data != null && response.data.length != 0) {

                            if (response.data[0].SSAD_STA_ID != null) {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', "Employee Already occupied");
                                params.data.AUR_ID = ""
                                params.data.AUR_NAME = ""
                                $scope.UpdateapprOptions.api.refreshView();

                                return;
                            }
                            else if (response.data[0].HD_STATUS != null) {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', "Employee Already under requisition");
                                params.data.AUR_ID = ""
                                params.data.AUR_NAME = ""
                                $scope.UpdateapprOptions.api.refreshView();
                                return;
                            }

                        }
                        else {
                            params.data.AUR_ID = eSelect.value;
                            params.data.AUR_NAME = newValue;
                            $scope.GetVerCosByEmpName(params.data, eSelect);
                        }
                    });



                }
            }
        });
        return eCell;
    }

    $scope.GetVerCosByEmpName = function (data, eSelect) {


        HotDeskRequisitionService.GetVerCosByEmpName(data.AUR_ID).then(function (response) {



            data.AUR_ID = response.data[0].AUR_ID;
            data.AUR_NAME = response.data[0].AUR_NAME;

            data.VERTICAL = response.data[0].VER_CODE;
            data.VER_NAME = response.data[0].VER_NAME;
            data.Cost_Center_Code = response.data[0].COST_CODE;
            data.Cost_Center_Name = response.data[0].Cost_Center_Name;

            $scope.UpdateapprOptions.api.refreshView();


        }, function (error) {
            progress(0, '', false);
            console.log(error);

        })


    };



    function removeOptions(selectbox) {
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }


    var fromdate, todate, fromtime, totime;
    function createFromDatePicker(params) {

        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.FROM_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        fromdate = params.data.FROM_DATE;


        $(newDate).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        }).on("input change", function (e) {
            if (params.data.TO_DATE == null || params.data.TO_DATE == "" || params.data.TO_DATE == undefined) {

                fromdate = e.target.value;
            }
            else {
                if (params.data.FROM_DATE > e.target.value) {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', "From Date should be greater than To Date");
                    params.data.FROM_DATE = ""
                    $scope.UpdateapprOptions.api.refreshView();
                    return;
                }
                else {
                    fromdate = e.target.value;
                }
            }
        });
        return newDate;
    }



    function createToDatePicker(params) {

        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.TO_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        fromdate = params.data.FROM_DATE;


        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            endDate: '+3m',
            autoclose: true,
            todayHighlight: true

        }).on("input change", function (e) {
            if (params.data.FROM_DATE == null || params.data.FROM_DATE == "" || params.data.FROM_DATE == undefined) {

                todate = e.target.value;
            }
            else {

                if (new Date(params.data.FROM_DATE) > new Date(e.target.value)) {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', "From Date should be greater than To Date");
                    params.data.TO_DATE = ""
                    $scope.UpdateapprOptions.api.refreshView();
                    return;
                }
                else {
                    todate = e.target.value;
                }
            }
        });
        return newDate;
    }


    function createFromTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.FROM_TIME');
        newTime.type = "text";
        newTime.id = 'fromtime' + params.rowIndex;
        newTime.className = "pickDate";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true,


            }).on("input change", function (e) {
                if (params.data.TO_TIME == null || params.data.TO_TIME == "" || params.data.TO_TIME == undefined) {

                    fromtime = e.target.value;
                }
                else {
                    //if (params.data.FROM_TIME > e.target.value) {
                    //    progress(0, '', false);
                    //    showNotification('error', 8, 'bottom-right', "From Time should be lesser than To Time");
                    //    params.data.FROM_TIME = ""
                    //    $scope.UpdateapprOptions.api.refreshView();
                    //    return;
                    //}
                    //else {
                    fromtime = e.target.value;
                    //}
                }

            });
        return newTime;
    }

    function createToTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.TO_TIME');
        newTime.type = "text";
        newTime.id = 'totime' + params.rowIndex;
        newTime.className = "pickDate";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true,

            }).on("input change", function (e) {
                if (params.data.FROM_TIME == null || params.data.FROM_TIME == "" || params.data.FROM_TIME == undefined) {

                    totime = e.target.value;
                }
                else {
                    //if (params.data.FROM_TIME > e.target.value) {
                    //    progress(0, '', false);
                    //    showNotification('error', 8, 'bottom-right', "From Time should be lesser than To Time");
                    //    params.data.TO_TIME = ""
                    //    $scope.UpdateapprOptions.api.refreshView();
                    //    return;
                    //}
                    //else {
                    totime = e.target.value;
                    //}
                }
            });
        return newTime;
    }
    // To display grid row data  
    $scope.LoadMyData = function () {
        ViewApproveHotDeskRequisitionService.GetMyReqList(1).then(function (data) {
            if (data != null) {

                $scope.MyrequisitonsOptions.api.setRowData([]);
                $scope.MyrequisitonsOptions.api.setRowData(data.Table);
                progress(0, '', false);
            } else {
                $scope.MyrequisitonsOptions.api.setRowData([]);
                progress(0, '', false);
            }
        }, function (error) {
            progress(0, '', false);
        });

    }
    $scope.LoadPendingData = function () {
        progress(0, 'Loading...', true);
        ViewApproveHotDeskRequisitionService.GetMyReqList(2).then(function (data) {
            progress(0, 'Loading...', true);
            if (data != null) {
                $scope.approvalOptions.api.setRowData([]);
                $scope.approvalOptions.api.setRowData(data.Table);
                progress(0, '', false);
            }
            progress(0, '', false);
        });

    }

    $scope.LoadMyData();

    setTimeout(function () {
        $scope.LoadPendingData();
    }, 1000);

    function onReqFilterChanged(value) {
        $scope.MyrequisitonsOptions.api.setQuickFilter(value);
    }
    $("#ReqFilter").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    })


    $scope.texttype = 'text';

    $scope.changeFunctin = function () {
        $scope.texttype = 'time';
    }

    $scope.onRowSelectedFunc = function (data, flag) {
        HotDeskRequisitionService.EmpChange(data.AUR_ID).then(function (response) {
            if (response.data != null && response.data.length != 0) {
                if (response.data[0].SSAD_STA_ID != null) {
                    $scope.Approve = false;
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', "Employee Already occupied");
                    //params.data.AUR_ID = ""
                    //params.data.AUR_NAME = ""
                    $scope.UpdateapprOptions.api.refreshView();
                    return;
                }
                else if (response.data[0].HD_STATUS != null) {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', "Employee Already under requisition");
                    //params.data.AUR_ID = ""
                    //params.data.AUR_NAME = ""
                    $scope.UpdateapprOptions.api.refreshView();
                    return;
                }
            }
        });

        //console.log($scope.GetRoleAndRM);
        var role = _.find($scope.GetRoleAndRM);

        if (data.AUR_ID == role.AUR_ID) {
            $scope.UpdateapprOptionsShow = false;
        }
        else {
            $scope.UpdateapprOptionsShow = true;
        }

        if (role.AUR_ROLE == 36 && data.AUR_ID != role.AUR_ID) {
            showNotification('error', 8, 'bottom-right', "Line Manager don't have access to modify seat details");
        }
        else {

            $scope.REQ_ID = data.REQ_ID;
            $scope.Viewstatus = 1;
            HotDeskRequisitionService.GetEmployeeDetails(data.AUR_ID).then(function (response) {
                if (response.data != null) {
                    $scope.HDRequest = response.data[0];
                }
            });
            if (data.HD_STATUS != '1063' && data.AUR_ID == UID) {

                $scope.EnableStatus = 2;

            }

            else if ((data.HD_STATUS == '1063' || data.HD_STATUS == '1064') && data.AUR_ID != UID) {

                $scope.EnableStatus = 1;
                $scope.L1_REM = true;
                $scope.L2_REM = false;
            }
            else if (data.HD_STATUS == '1065' && data.AUR_ID != UID) {

                $scope.EnableStatus = 1;
                $scope.L1_REM = true;
                $scope.L2_REM = true;
            }
            else if (data.HD_STATUS == '1063' && data.AUR_ID == UID) {

                $scope.EnableStatus = 0;
            }
            ViewApproveHotDeskRequisitionService.GetDetailsOnSelection(data.REQ_ID).then(function (response) {

                $scope.grid = true;
                $scope.HD_REQ_REM = response.data.REM;
                $scope.L1_REQ_REM = response.data.L1_REM;
                $scope.L2_REQ_REM = response.data.L2_REM;
                $scope.HD_AUR_ID = response.data.AUR_ID;
                $scope.VER_NAME = response.data.VER_NAME;
                $scope.Cost_Center_Name = response.data.Cost_Center_Name;
                $scope.Seat_Type = response.data.Seat_Type;

                $scope.HD_FROM_DATE = response.data.FROM_DATE;
                $scope.HD_TO_DATE = response.data.TO_DATE;
				
                // var dt = new Date(response.data.FROM_TIME);
                // var dt1 = new Date(response.data.TO_TIME);
                // $scope.HD_FROM_TIME = (dt.getHours() > 9 ? dt.getHours() : '0' + dt.getHours()) + ':' + (dt.getMinutes() > 9 ? dt.getMinutes() : '0' + dt.getMinutes());
                // $scope.HD_TO_TIME = (dt1.getHours() > 9 ? dt1.getHours() : '0' + dt1.getHours()) + ':' + (dt1.getMinutes() > 9 ? dt1.getMinutes() : '0' + dt1.getMinutes());
                $scope.HD_FROM_TIME = response.data.FROM_TIME;
                $scope.HD_TO_TIME = response.data.TO_TIME;


                $scope.selectedfloors = [];
                $scope.selectedfloors = response.data.selectedfloors
                if (response.data != null) {
                    $scope.UpdateapprOptions.api.setRowData([]);
                    $scope.PreviousSelectedSpaces = [];

                    angular.forEach(response.data.DETAILS, function (value, key) {
                        if (value.ticked == true) {
                            $scope.PreviousSelectedSpaces.push(value);
                        }
                    });

                    $scope.UpdateapprOptions.api.setRowData(response.data.DETAILS);


                    UtilityService.getCountires(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Country = response.data;
                            if ($scope.selectedfloors != null) {
                                for (i = 0; i < $scope.selectedfloors.length; i++) {
                                    var a = _.find($scope.Country, { CNY_CODE: $scope.selectedfloors[i].CNY_CODE });
                                    a.ticked = true;
                                }
                            }

                            UtilityService.getCities(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.City = response.data;
                                    if ($scope.selectedfloors != null) {
                                        for (i = 0; i < $scope.selectedfloors.length; i++) {
                                            var a = _.find($scope.City, { CTY_CODE: $scope.selectedfloors[i].CTY_CODE });
                                            a.ticked = true;
                                        }
                                    }

                                    UtilityService.getLocations(2).then(function (response) {
                                        if (response.data != null) {
                                            $scope.Locations = response.data;
                                            if ($scope.selectedfloors != null) {
                                                for (i = 0; i < $scope.selectedfloors.length; i++) {
                                                    var a = _.find($scope.Locations, { LCM_CODE: $scope.selectedfloors[i].LCM_CODE });
                                                    a.ticked = true;
                                                }
                                            }
                                            UtilityService.getTowers(2).then(function (response) {
                                                if (response.data != null) {
                                                    $scope.Towers = response.data;
                                                    if ($scope.selectedfloors != null) {
                                                        for (i = 0; i < $scope.selectedfloors.length; i++) {
                                                            var a = _.find($scope.Towers, { TWR_CODE: $scope.selectedfloors[i].TWR_CODE });
                                                            a.ticked = true;
                                                        }
                                                    }

                                                    UtilityService.getFloors(2).then(function (response) {
                                                        if (response.data != null) {
                                                            $scope.Floors = response.data;
                                                        }
                                                        if ($scope.selectedfloors != null) {
                                                            for (i = 0; i < $scope.selectedfloors.length; i++) {
                                                                var a = _.find($scope.Floors, { FLR_CODE: $scope.selectedfloors[i].FLR_CODE });
                                                                a.ticked = true;
                                                            }
                                                        }
                                                    });

                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });


                }


            }, function (error) {
                progress(0, '', false);
            });
        }
    };

    $scope.setStatus = function (status) {
        switch (status) {
            case 'Approve':
                $scope.RetStatus = UtilityService.Approved;
                $scope.UpdateRequest();

                break;
            case 'Reject':
                $scope.RetStatus = UtilityService.Rejected;
                $scope.UpdateRequest();
                break;
            case 'Update':
                $scope.RetStatus = UtilityService.Modified;
                $scope.UpdateRequest('U');
                break;
            case 'Cancel':
                $scope.RetStatus = UtilityService.Canceled;
                $scope.UpdateRequest('U');
                break;
            case 'ApproveAll':
                $scope.RetStatus = UtilityService.Approved;

                $scope.UpdateAllRequests();

                break;
            case 'RejectAll':
                $scope.RetStatus = UtilityService.Rejected;
                $scope.UpdateAllRequests();
                break;
        }
    }
    $scope.GetVacantSeats = function () {

        $scope.UpdateapprOptions.api.setRowData([]);

        var role = _.find($scope.GetRoleAndRM);
        if (role.AUR_ROLE == 14) {
            $scope.Searchbutton = false;
        }
        var data = {
            flrlst: $scope.HotDeskRequisition.Floors, REQ_ID: $scope.REQ_ID
        };
        //ViewApproveHotDeskRequisitionService.UpdateHdmFloor(data).then(function (response) {
        //    if (response.data != null) {
        //        showNotification('success', 8, 'bottom-right', 'Floor Updated Sucessfully');
        //    }
        //});


        if (role.AUR_ROLE == 36) {
            showNotification('error', 8, 'bottom-right', "Line Manager don't have access to modify seat details");
        }

        else {

            //$scope.REQ_ID = data.REQ_ID;
            //$scope.Viewstatus = 1;
            //HotDeskRequisitionService.GetEmployeeDetails(data.AUR_ID).then(function (response) {
            //    if (response.data != null) {
            //        $scope.HDRequest = response.data[0];
            //    }
            //});
            //if (data.HD_STATUS != '1063' && data.AUR_ID == UID) {

            //    $scope.EnableStatus = 2;

            //}

            //else if ((data.HD_STATUS == '1063' || data.HD_STATUS == '1064') && data.AUR_ID != UID) {

            //    $scope.EnableStatus = 1;
            //    $scope.L1_REM = true;
            //    $scope.L2_REM = false;
            //}
            //else if (data.HD_STATUS == '1065' && data.AUR_ID != UID) {

            //    $scope.EnableStatus = 1;
            //    $scope.L1_REM = true;
            //    $scope.L2_REM = true;
            //}
            //else if (data.HD_STATUS == '1063' && data.AUR_ID == UID) {

            //    $scope.EnableStatus = 0;
            //}

            ViewApproveHotDeskRequisitionService.UpdateHdmFloor(data).then(function (response) {
                if (response.data != null) {
                    ViewApproveHotDeskRequisitionService.GetDetailsOnSelection($scope.REQ_ID).then(function (response) {

                        $scope.grid = true;
                        $scope.HD_REQ_REM = response.data.REM;
                        $scope.L1_REQ_REM = response.data.L1_REM;
                        $scope.L2_REQ_REM = response.data.L2_REM;
                        $scope.HD_AUR_ID = response.data.AUR_ID;
                        $scope.VER_NAME = response.data.VER_NAME;
                        $scope.Cost_Center_Name = response.data.Cost_Center_Name;
                        $scope.Seat_Type = response.data.Seat_Type;

                        $scope.HD_FROM_DATE = response.data.FROM_DATE;
                        $scope.HD_TO_DATE = response.data.TO_DATE;
						
                // var dt = new Date(response.data.FROM_TIME);
                // var dt1 = new Date(response.data.TO_TIME);
                // $scope.HD_FROM_TIME = (dt.getHours() > 9 ? dt.getHours() : '0' + dt.getHours()) + ':' + (dt.getMinutes() > 9 ? dt.getMinutes() : '0' + dt.getMinutes());
                // $scope.HD_TO_TIME = (dt1.getHours() > 9 ? dt1.getHours() : '0' + dt1.getHours()) + ':' + (dt1.getMinutes() > 9 ? dt1.getMinutes() : '0' + dt1.getMinutes());
                $scope.HD_FROM_TIME = response.data.FROM_TIME;
                $scope.HD_TO_TIME = response.data.TO_TIME;


                        $scope.selectedfloors = [];
                        $scope.selectedfloors = response.data.selectedfloors
                        if (response.data != null) {
                            $scope.UpdateapprOptions.api.setRowData([]);
                            //$scope.PreviousSelectedSpaces = [];

                            angular.forEach(response.data.DETAILS, function (value, key) {
                                if (value.ticked == true) {
                                    //$scope.PreviousSelectedSpaces.push(value);
                                }
                            });



                            $scope.UpdateapprOptions.api.setRowData(response.data.DETAILS);
                            //UtilityService.getCountires(2).then(function (response) {
                            //    if (response.data != null) {
                            //        $scope.Country = response.data;
                            //        if ($scope.selectedfloors != null) {
                            //            for (i = 0; i < $scope.selectedfloors.length; i++) {
                            //                var a = _.find($scope.Country, { CNY_CODE: $scope.selectedfloors[i].CNY_CODE });
                            //                a.ticked = true;
                            //            }
                            //        }

                            //        //UtilityService.getCities(2).then(function (response) {
                            //        //    if (response.data != null) {
                            //        //        $scope.City = response.data;
                            //        //        if ($scope.selectedfloors != null) {
                            //        //            for (i = 0; i < $scope.selectedfloors.length; i++) {
                            //        //                var a = _.find($scope.City, { CTY_CODE: $scope.selectedfloors[i].CTY_CODE });
                            //        //                a.ticked = true;
                            //        //            }
                            //        //        }

                            //        //        //UtilityService.getLocations(2).then(function (response) {
                            //        //        //    if (response.data != null) {
                            //        //        //        $scope.Locations = response.data;
                            //        //        //        if ($scope.selectedfloors != null) {
                            //        //        //            for (i = 0; i < $scope.selectedfloors.length; i++) {
                            //        //        //                var a = _.find($scope.Locations, { LCM_CODE: $scope.selectedfloors[i].LCM_CODE });
                            //        //        //                a.ticked = true;
                            //        //        //            }
                            //        //        //        }
                            //        //        //        //UtilityService.getTowers(2).then(function (response) {
                            //        //        //        //    if (response.data != null) {
                            //        //        //        //        $scope.Towers = response.data;
                            //        //        //        //        if ($scope.selectedfloors != null) {
                            //        //        //        //            for (i = 0; i < $scope.selectedfloors.length; i++) {
                            //        //        //        //                var a = _.find($scope.Towers, { TWR_CODE: $scope.selectedfloors[i].TWR_CODE });
                            //        //        //        //                a.ticked = true;
                            //        //        //        //            }
                            //        //        //        //        }

                            //        //        //        //        //UtilityService.getFloors(2).then(function (response) {
                            //        //        //        //        //    if (response.data != null) {
                            //        //        //        //        //        $scope.Floors = response.data;
                            //        //        //        //        //    }
                            //        //        //        //        //    if ($scope.selectedfloors != null) {
                            //        //        //        //        //        for (i = 0; i < $scope.selectedfloors.length; i++) {
                            //        //        //        //        //            var a = _.find($scope.Floors, { FLR_CODE: $scope.selectedfloors[i].FLR_CODE });
                            //        //        //        //        //            a.ticked = true;
                            //        //        //        //        //        }
                            //        //        //        //        //    }
                            //        //        //        //        //});

                            //        //        //        //    }
                            //        //        //        //});
                            //        //        //    }
                            //        //        //});
                            //        //    }
                            //        //});
                            //    }
                            //});


                        }


                    }, function (error) {
                        progress(0, '', false);
                    });
                    showNotification('success', 8, 'bottom-right', 'Floor Updated Sucessfully');
                }
            });

        }
    }

    $scope.Clear = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Floors, function (value, key) {
            value.ticked = false;
        });

    }
    $scope.UpdateRequest = function (typ) {
        console.log($scope.REQ_ID);
        $scope.tickedspaces = [];
        angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
            if (value.ticked == true)
                $scope.tickedspaces.push(value);
        });
        var cnt = _.filter($scope.UpdateapprOptions.rowData, { 'ticked': true }).length;

        if (typ == 'U') {
            var obj = {
                AUR_ID: null,
                Cost_Center_Name: null,
                FROM_DATE: $scope.HD_FROM_DATE,
                FROM_TIME: $scope.HD_FROM_TIME,
                SPC_ID: null,
                TO_DATE: $scope.HD_TO_DATE,
                TO_TIME: $scope.HD_TO_TIME,
                VER_NAME: null,
                Seat_Type: null,
                ticked: true
            };
            $scope.tickedspaces.push(obj);
            cnt = 1;
        }

        if (cnt == 0) {
            if ($scope.RetStatus == 32) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Please Select Spaces To Approve');
            } else if ($scope.RetStatus == 16) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Please Select Spaces To Update');
            }

        }
        else {

            var ReqObj =
            {
                HDLN: $scope.tickedspaces, ALLOCSTA: $scope.RetStatus, REM: $scope.HD_REQ_REM, L1_REM: $scope.L1_REQ_REM, L2_REM: $scope.L2_REQ_REM,
                REQ_ID: $scope.REQ_ID,
                flrlst: $scope.HotDeskRequisition.Floors, LCMlst: $scope.HotDeskRequisition.Locations, twrlst: $scope.HotDeskRequisition.Towers
            };

            var bool = true;
            if ($scope.RetStatus == 32) {
                ViewApproveHotDeskRequisitionService.RaiseRequstValidatin(ReqObj).then(function (response1) {
                    if (response1 && response1.data.length > 0 && response1.data[0].ID == 0) {
                        bool = true;
                    }
                    else {
                        bool = false;
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response1.data[0].MESSAGE);
                    }
                });
            }
            else {
                bool = true;
            }

            if (bool) {
                HotDeskRequisitionService.RaiseRequst(ReqObj).then(function (response) {
                    if (response != null) {
                        progress(0, '', false);
                        ViewApproveHotDeskRequisitionService.GetMyReqList(1).then(function (data) {
                            if (data.Table != null) {
                                $scope.gridata = data.Table;
                                $scope.MyrequisitonsOptions.api.setRowData([]);
                                $scope.MyrequisitonsOptions.api.setRowData($scope.gridata);
                            }
                            else { $scope.MyrequisitonsOptions.api.setRowData([]); }
                        }, function (error) {
                        });
                        ViewApproveHotDeskRequisitionService.GetMyReqList(2).then(function (data) {
                            if (data.Table != null) {
                                $scope.Pendinggridata = data.Table;
                                $scope.approvalOptions.api.setRowData([]);
                                $scope.approvalOptions.api.setRowData($scope.Pendinggridata);
                                $scope.grid = false;
                                $scope.Clear();
                            }
                            else {
                                $scope.approvalOptions.api.setRowData([]);
                            }

                        }, function (error) {
                        });
                        showNotification('success', 8, 'bottom-right', response.Message);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                });
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response1.data[0].MESSAGE);
            }
        }
    }

    $scope.UpdateAllRequests = function () {
        $scope.selectedapprovespaces = [];
        angular.forEach($scope.approvalOptions.rowData, function (value, key) {
            if (value.ticked == true)
                $scope.selectedapprovespaces.push(value);
        });
        progress(0, 'Loading...', true);

        var ReqObj = { ALLOCSTA: $scope.RetStatus, hdreqlst: $scope.selectedapprovespaces, L1_REM: $scope.HD_L1_REM, L2_REM: $scope.HD_L2_REM };
        console.log(ReqObj);
        if ($scope.selectedapprovespaces.length == 0) {
            progress(0, '', false);
            if ($scope.RetStatus == 32) {
                showNotification('error', 8, 'bottom-right', 'Please Select  At Least One Requisition to Approve');
            } else if ($scope.RetStatus == 64) {
                showNotification('error', 8, 'bottom-right', 'Please Select  At Least One Requisition to  Reject');
            }

        }
        else {
            ViewApproveHotDeskRequisitionService.ApproveRequests(ReqObj).then(function (response) {
                if (response != null) {
                    progress(0, '', false);
                    ViewApproveHotDeskRequisitionService.GetMyReqList(2).then(function (data) {
                        if (data.Table != null) {
                            $scope.approvalOptions.api.setRowData([]);
                            $scope.approvalOptions.api.setRowData(data.Table);
                        }
                        else {
                            $scope.approvalOptions.api.setRowData([]);
                        }

                    }, function (error) {
                    });

                    showNotification('success', 8, 'bottom-right', response.Message);
                }
            });
        }

    }






    ///// For map layout
    var map = L.map('leafletMap');
    $scope.ViewinMap = function () {

        $scope.SelRowData = [];
        setTimeout(function () {
            if ($scope.SelRowData.length == 0) {
                $scope.MapFloors = [];
                $scope.MapFloor = [];
                angular.forEach($scope.selectedfloors, function (Value, Key) {
                    Value.ticked = false;
                    $scope.MapFloors.push(Value);
                });
                $scope.MapFloors[0].ticked = true;
                $scope.MapFloor.push($scope.MapFloors[0]);
            }
        }, 100);
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {

        if ($scope.SelRowData.length == 0) {
            setTimeout(function () {
                $scope.loadmap($scope.MapFloor[0].FLR_CODE);
            }, 1000);
        }

    });

    $scope.FlrSectMap = function (data) {

        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        setTimeout(function () {
            $scope.loadmap(data.FLR_CODE);
        }, 1000);

    }


    $scope.loadmap = function (FLR_CODE) {
        progress(0, 'Loading...', true);
        //$('#leafletMap').empty();

        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: FLR_CODE, key_value: 1 };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadMapDetails = function (result) {
        var promises = [];

        angular.forEach(result.mapDetails, function (value, index) {


            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SPACE_ID = value.SPACE_ID;
            theLayer.options.checked = false;

            //var SeattypeLayer = $.extend(true, {}, theLayer);

            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            function (results) {

                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);

                //$scope.SelRowData = $filter('filter')($scope.selectedfloors, { FLR_CODE: $scope.MapFloor[0].FLR_CODE });

                angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {

                    $scope.marker1 = _.find($scope.drawnItems._layers, { options: { SPACE_ID: value.SPC_ID, spacetype: 'CHA' } });

                    if (value.ticked == true && $scope.marker1 != undefined) {
                        $scope.marker1.setStyle(selctdChrStyle);
                        $scope.marker1.bindLabel('Vacant: ' + value.SPC_ID);
                        $scope.marker1.on('click', markerclicked);
                        $scope.marker1.addTo(map);
                    }
                    else if (value.ticked == false && $scope.marker1 != undefined) {
                        $scope.marker1.setStyle(VacantStyle);
                        $scope.marker1.bindLabel('Vacant: ' + value.SPC_ID);
                        $scope.marker1.on('click', markerclicked);
                        $scope.marker1.addTo(map);
                    }


                });
            },
            function (response) {
            }
        );
    };
    function markerclicked(e) {
        console.log(e)
        console.log(this.options.SPACE_ID)
        var marker = _.find($scope.UpdateapprOptions.rowData, { SPC_ID: this.options.SPACE_ID });
        //if (!this.ticked) {
        //    this.setStyle(selctdChrStyle)
        //    this.ticked = true;
        //    marker.ticked = true;
        //}
        //else {
        this.setStyle(VacantStyle)
        this.ticked = false;
        marker.ticked = false;
        //}
        $scope.UpdateapprOptions.api.refreshView();
    }



    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };
    var OccupiedStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
    var AllocateStyle = { fillColor: '#3498DB', opacity: 0.8, fillOpacity: 0.8 };


});