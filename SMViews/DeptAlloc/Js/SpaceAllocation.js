﻿app.service("SpaceAllocationService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetPendingList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceAllocation/GetPendingSpaceAllocationDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveSelectedSpaces = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocation/SaveSelectedSpaces', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetDetailsOnSelection = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocation/GetDetailsOnSelection', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SendMail = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocation/SendMail', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getVacantSpaces = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocation/GetVacantSpaces', searchObj)
            .then(function (response) {
                //console.log(response.data);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetDetailsOnSelectionAlloc = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocation/GetDetailsOnSelectionAlloc', searchObj)
            .then(function (response) {
                //console.log(response.data);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.Allocatedclosed = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocation/Allocatedclosed', searchObj)
            .then(function (response) {
                //console.log(response.data);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('SpaceAllocationController', ['$scope', '$q', 'UtilityService', 'L1ApprovalService', 'SpaceRequisitionService', 'SpaceAllocationService','$filter', function ($scope, $q, UtilityService, L1ApprovalService, SpaceRequisitionService, SpaceAllocationService, $filter) {
    $scope.Viewstatus = 0;
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.Costcenterlist = [];
    $scope.AllocatedSeats = [];
    $scope.Flrlist = [];
    $scope.currentblkReq = {};
    $scope.RetStatus = UtilityService.Added;
    $scope.EnableStatus = 0;
    $scope.DrpEnableStatus = false;
    $scope.tempspace = {};
    $scope.selectedSpaces = [];
    $scope.tickedSpaces = [];
    $scope.selectedLocations = [];
    $scope.SpaceReqCount = [];
    $scope.sendCheckedValsObj = [];
    $scope.L1Approve = {};
    $scope.EmpDetails = [];
    $scope.SysPreference = [];
    $scope.Type = [];
    $scope.Shifts = [];
    $scope.SelectedSeatCount = {};
    $scope.allocid = false;
    $scope.MapFloors = [];
    $scope.SelRowData = [];
    $scope.Markers = [];
    $scope.SelLayers = [];
    var map = L.map('leafletMap');
    $scope.selValues = [];
    $scope.data2 = [];

    $scope.SpaceAllocation = {};

    $scope.CityChanged = function () {
        console.log($scope.SpaceAllocation.selectedCities);
        UtilityService.getLocationsByCity($scope.SpaceAllocation.selectedCities, 2).then(function (data) {
            console.log($scope.SpaceAllocation.selectedCities);
            if (data.data != null) {
                $scope.Locationlst = data.data;

            }
            else {
                $scope.Locationlst = [];
                $scope.Towerlist = [];
                $scope.Floorlist = [];
            }

        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceAllocation.selectedCountries.push(cny);
            }
        });
    }
    $scope.CtyChangeAll = function () {
        $scope.SpaceAllocation.selectedCities = $scope.Citylst;
        $scope.CityChanged();
    }

    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.SpaceAllocation.selectedLocations, 2).then(function (data) {
            if (data.data != null) {
                $scope.Towerlist = data.data;

            } else {
                $scope.Towerlist = [];
                $scope.Floorlist = [];
            }
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceAllocation.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceAllocation.selectedCities.push(cty);
            }
        });
    }
    $scope.LCMChangeAll = function () {
        $scope.SpaceAllocation.selectedLocations = $scope.Locationlst;
        $scope.LocChange();
    }
    //$scope.lcmSelectNone = function () {
    //    $scope.Towerlist = [];
    //    $scope.Floorlist = [];
    //}

    $scope.TwrChange = function () {
        UtilityService.getFloorByTower($scope.SpaceAllocation.selectedTowers, 2).then(function (data) {
            if (data.data != null) { $scope.Floorlist = data.data } else { $scope.Floorlist = [] }
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceAllocation.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceAllocation.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceAllocation.selectedLocations.push(lcm);
            }
        });
    }
    $scope.TwrChangeAll = function () {
        $scope.SpaceAllocation.selectedTowers = $scope.Towerlist;
        $scope.TwrChange();
    }
    //$scope.twrSelectNone = function () {
    //    $scope.Floorlist = [];
    //}

    $scope.FloorChange = function () {

        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceAllocation.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceAllocation.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceAllocation.selectedLocations.push(lcm);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SpaceAllocation.selectedTowers.push(twr);
            }
        });

    }
    $scope.FloorChangeAll = function () {
        $scope.SpaceAllocation.selectedFloors = $scope.Floorlist;
        $scope.FloorChange();
    }

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    $scope.SpaceAllocDefs = [
        {
            headerName: "Requisition ID", width: 200, field: "SRN_REQ_ID", cellClass: "grid-align", filter: 'set',
            template: '<a ng-click="onRowSelectedFunc(data)">{{data.SRN_REQ_ID}}</a>', pinned: 'left', suppressMenu: true,
        },
        { headerName: "Status", width: 145, field: "STA_DESC", cellClass: "grid-align", pinned: 'left', suppressMenu: true, },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", width: 138, template: '<span>{{data.SRN_REQ_DT | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "", width: 138, field: "VER_NAME", cellClass: "grid-align" },
        { headerName: "", width: 138, field: "COST_CENTER_NAME", cellClass: "grid-align" },
        { headerName: "From Date", width: 138, template: '<span>{{data.SRN_FROM_DATE | date: "dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "To Date", width: 138, template: '<span>{{data.SRN_TO_DATE | date: "dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },

    ];
    $scope.SpaceAllocOptions = {
        columnDefs: $scope.SpaceAllocDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        enableColResize: true,
        rowSelection: 'multiple',

    };
    function onUpdateFilterChanged(value) {
        $scope.SpaceAllocOptions.api.setQuickFilter(value);
    }
    $("#SpaceFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    var gridCountDefsCount = [
        { headerName: "Country", width: 120, field: "SRC_CNY_NAME", cellClass: "grid-align" },
        { headerName: "City", width: 120, field: "SRC_CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", width: 180, field: "SRC_LCM_NAME", cellClass: "grid-align" },
        { headerName: "Tower", width: 120, field: "SRC_TWR_NAME", cellClass: "grid-align" },
        { headerName: "Floor", width: 120, field: "SRC_FLR_NAME", cellClass: "grid-align" },

        { headerName: "", width: 150, field: "SPC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "", width: 150, field: "SPC_SUB_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "", width: 150, field: "SRC_REQ_CNT", cellClass: "grid-align", suppressMenu: true, pinned: 'right' },
        {
            headerName: "Selected Seat Count", cellClass: "grid-align", filter: 'set', template: '<span >{{data.SelectedSeatCount}}</span>',
            suppressMenu: true, pinned: 'right', width: 120,
        },
        {
            headerName: "Allocate Seats", template: '<a class="btn btn-primary btn-xs custom-button-color" data-ng-click ="Search(data)">Search</a>',
            cellClass: 'grid-align', suppressMenu: true, pinned: 'right', width: 120
        }

    ];

    $scope.gridCountOptions = {
        columnDefs: gridCountDefsCount,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        //onReady: function () {
        //    $scope.gridCountOptions.api.sizeColumnsToFit()
        //}
    };

    var SpacesColumnDefs = [
        {
            headerName: "Select", field: "ticked", width: 50, cellClass: 'grid-align', filter: 'set',
            template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>", suppressMenu: true, pinned: 'left'
        },
        { headerName: "Space ID", field: "SRD_SPC_NAME", width: 200, cellClass: "grid-align" },
        { headerName: "Space Type", field: "SRD_SPC_TYPE_NAME", width: 90, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SRD_SPC_SUB_TYPE_NAME", width: 100, cellClass: "grid-align" },

    ];
    $scope.SpacesOptions = {
        columnDefs: SpacesColumnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.SpacesOptions.api.sizeColumnsToFit()
        }
    };
    var gridCountDefsalloc = [
        { headerName: "Country", width: 120, field: "SRC_CNY_NAME", cellClass: "grid-align" },
        { headerName: "City", width: 120, field: "SRC_CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", width: 180, field: "SRC_LCM_NAME", cellClass: "grid-align" },
        { headerName: "Tower", width: 120, field: "SRC_TWR_NAME", cellClass: "grid-align" },
        { headerName: "Floor", width: 120, field: "SRC_FLR_NAME", cellClass: "grid-align" },

        { headerName: "", width: 150, field: "SPC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "", width: 150, field: "SPC_SUB_TYPE_NAME", cellClass: "grid-align" },
        //{ headerName: "", width: 150, field: "SRC_REQ_CNT", cellClass: "grid-align", suppressMenu: true, pinned: 'right' },
        //{
        //    headerName: "Selected Seat Count", cellClass: "grid-align", filter: 'set', template: '<span >{{data.SelectedSeatCount}}</span>',
        //    suppressMenu: true, pinned: 'right', width: 120,
        //},
        {
            headerName: "Allocated Space", field: "SSA_SPC_ID", cellClass: 'grid-align',
        },
        //{
        //    headerName: "Close", template: '<a class="btn btn-primary btn-xs custom-button-color" data-ng-click ="Close(data)">Close</a>',
        //       cellClass: 'grid-align', suppressMenu: true, pinned: 'right', width: 120
        // }

    ];
    $scope.SpacesOptionsalloc = {
        columnDefs: gridCountDefsalloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.SpacesOptionsalloc.api.sizeColumnsToFit()
        }
    };
    UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPreference = response.data;
            $scope.AllocationType = _.find($scope.SysPreference, { SYSP_CODE: "Allocation Type" })
            $scope.ReqCountType = _.find($scope.SysPreference, { SYSP_CODE: "Requisition Count Type" })
        }
    });


    SpaceAllocationService.GetPendingList().then(function (data) {
        progress(0, 'Loading...', true);
        if (data.data != null) {
            $scope.gridata = data.data;
            $scope.SpaceAllocOptions.api.setRowData([]);
            $scope.SpaceAllocOptions.api.setRowData($scope.gridata);
            $scope.SpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.SpaceAllocOptions.columnApi.getColumn("COST_CENTER_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.SpaceAllocOptions.api.refreshHeader()
            progress(0, '', false);
        }
        else {
            $scope.SpaceAllocOptions.api.setRowData([]);
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', data.Message);

        }
    }, function (response) {
        progress(0, '', false);
    });
    var SRC_SRN_REQ_ID;
    $scope.onRowSelectedFunc = function (data) {
        progress(0, 'Loading...', true);
        $scope.Viewstatus = 1;
        SRC_SRN_REQ_ID = data.SRN_REQ_ID;
        SpaceAllocationService.GetDetailsOnSelection(data).then(function (response) {
            $scope.currentblkReq = data;
            $scope.currentblkReq.SRN_FROM_DATE = $filter('date')(data.SRN_FROM_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_TO_DATE = $filter('date')(data.SRN_TO_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_REQ_DATE = $filter('date')(data.SRN_REQ_DT, "MM/dd/yyyy");

            angular.forEach(response.data.SELSPACES.spcreqcountList, function (data) {
                data.SelectedSeatCount = 0
            });

            UtilityService.getCountires(2).then(function (Condata) {
                $scope.countrylist = Condata.data;
                if (response.data != null) {
                    console.log(response.data.SELSPACES.flrlst);
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.countrylist, { CNY_CODE: response.data.SELSPACES.flrlst[i].CNY_CODE });
                        a.ticked = true;
                    }
                    //////***** City details ****//////////
                    UtilityService.getCities(2).then(function (ctydata) {
                        $scope.Citylst = ctydata.data;
                        if (response.data != null) {
                            for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                                var a = _.find($scope.Citylst, { CTY_CODE: response.data.SELSPACES.flrlst[i].CTY_CODE });
                                a.ticked = true;
                            }
                            //////***** Location details ****//////////
                            UtilityService.getLocations(2).then(function (locdata) {
                                $scope.Locationlst = locdata.data;
                                if (response.data != null) {
                                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                                        //$scope.Locationlst = $filter('filter')($scope.Locationlst, { CTY_CODE: response.data.SELSPACES.flrlst[i].CTY_CODE });
                                        var a = _.find($scope.Locationlst, { LCM_CODE: response.data.SELSPACES.flrlst[i].LCM_CODE });
                                        a.ticked = true;
                                        $scope.selectedLocations.push(a);
                                    }
                                    SpaceRequisitionService.getShifts($scope.selectedLocations).then(function (shft) {
                                        $scope.Shifts = [];
                                        $scope.Shifts = shft.data;
                                    });
                                }
                                //////***** Tower details ****//////////
                                UtilityService.getTowers(2).then(function (twrdata) {
                                    $scope.Towerlist = twrdata.data;
                                    if (response.data != null) {
                                        for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                                            //$scope.Towerlist = $filter('filter')($scope.Towerlist, { CTY_CODE: response.data.SELSPACES.flrlst[i].CTY_CODE });
                                            var a = _.find($scope.Towerlist, { TWR_CODE: response.data.SELSPACES.flrlst[i].TWR_CODE });
                                            a.ticked = true;
                                        }

                                        UtilityService.getFloors(2).then(function (Flrdata) {
                                            $scope.Floorlist = Flrdata.data;
                                            if (response.data != null) {
                                                for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                                                    //$scope.Floorlist = $filter('filter')($scope.Floorlist, { CTY_CODE: response.data.SELSPACES.flrlst[i].CTY_CODE });
                                                    var flr = _.find($scope.Floorlist, { FLR_CODE: response.data.SELSPACES.flrlst[i].FLR_CODE });
                                                    flr.ticked = true;
                                                    $scope.Flrlist.push(flr);
                                                }

                                                UtilityService.getVerticals(2).then(function (Verdata) {
                                                    $scope.Verticallist = Verdata.data;
                                                    if (response.data != null) {
                                                        for (i = 0; i < response.data.SELSPACES.verlst.length; i++) {
                                                            var ver = _.find($scope.Verticallist, { VER_CODE: response.data.SELSPACES.cstlst[i].Vertical_Code });
                                                            ver.ticked = true;
                                                            $scope.L1Approve.selectedVerticals = ver;
                                                        }

                                                    }
                                                });
                                                UtilityService.getCostCenters(2).then(function (Cosdata) {
                                                    $scope.Costcenterlist = Cosdata.data;
                                                    if (response.data != null) {
                                                        for (i = 0; i < response.data.SELSPACES.cstlst.length; i++) {
                                                            var cos = _.find($scope.Costcenterlist, { Cost_Center_Code: response.data.SELSPACES.cstlst[i].Cost_Center_Code });
                                                            cos.ticked = true;
                                                        }

                                                        progress(0, '', false);

                                                    }

                                                });

                                            }


                                        });
                                    }

                                });


                            });
                        }
                    });

                }

            });


            if (data.SRN_STA_ID == '1048' || data.SYSP_VAL1 == '1300') {
                var param = {
                    SRN_REQ_ID: data.SRN_REQ_ID
                }

                SpaceAllocationService.GetDetailsOnSelectionAlloc(param).then(function (response) {
                    console.log(response);
                    $scope.EnableSpacesOptions = false;
                    $scope.SpacesOptionsallocData = response.data;
                    $scope.SpacesOptionsalloc.api.setRowData([]);
                    $scope.SpacesOptionsalloc.api.setRowData($scope.SpacesOptionsallocData);
                    //$scope.SpacesOptionsalloc.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Space Count";
                    $scope.SpacesOptionsalloc.columnApi.getColumn("SPC_TYPE_NAME").colDef.headerName = "Space Type";
                    $scope.SpacesOptionsalloc.columnApi.getColumn("SPC_SUB_TYPE_NAME").colDef.headerName = "Space Sub Type";
                    $scope.SpacesOptionsalloc.api.refreshHeader();
                    $scope.EnableGridCount = true;
                });


            } else {
                if (response.data.SELSPACES.spcreqcountList != null) {
                    $scope.EnableSpacesOptions = true;
                    $scope.EnableGridCount = false;
                    $scope.gridCountData = response.data.SELSPACES.spcreqcountList;
                    $scope.gridCountOptions.api.setRowData([]);
                    $scope.gridCountOptions.api.setRowData($scope.gridCountData);
                    if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Employee Count";
                        $scope.gridCountOptions.columnApi.getColumn("SPC_TYPE_NAME").colDef.headerName = "Grade Type";
                        $scope.gridCountOptions.columnApi.getColumn("SPC_SUB_TYPE_NAME").colDef.headerName = "Request Type";
                        $scope.gridCountOptions.api.refreshHeader();
                        $scope.Type = [];
                    }
                    else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Space Count";
                        $scope.gridCountOptions.columnApi.getColumn("SPC_TYPE_NAME").colDef.headerName = "Space Type";
                        $scope.gridCountOptions.columnApi.getColumn("SPC_SUB_TYPE_NAME").colDef.headerName = "Space Sub Type";
                        $scope.gridCountOptions.api.refreshHeader();
                        $scope.Type = [];
                        $scope.SubType = [];
                    }
                }
            }

        },


            function (response) {
                progress(0, '', false);
            });
        // }

    };


    $scope.tempSeats = [];
    $scope.Close = function () {
        // var searchObj = { REq: selctedRow.SRC_SRN_REQ_ID }
        var param = {
            SRC_SRN_REQ_ID: SRC_SRN_REQ_ID,
            APP_REM: $scope.currentblkReq.SRN_ALLC_REM
        }
        console.log(param)
        SpaceAllocationService.Allocatedclosed(param).then(function (response) {
            if (response.data != null) {
                $scope.back();
                SpaceAllocationService.GetPendingList().then(function (data) {
                    if (data.data != null) {
                        $scope.gridata = data.data;
                        $scope.SpaceAllocOptions.api.setRowData([]);
                        $scope.SpaceAllocOptions.api.setRowData($scope.gridata);

                    }
                    else {
                        $scope.SpaceAllocOptions.api.setRowData([]);
                    }
                }, function (response) {
                });
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (response) {
            progress(0, '', false);
        });
    }
    $scope.Search = function (selctedRow) {
        progress(0, 'Loading...', true);
        var searchObj = { flrlst: $scope.SpaceAllocation.selectedFloors, verlst: $scope.SpaceAllocation.selectedVerticals, cstlst: $scope.SpaceAllocation.selectedCostcenters, spcreq: $scope.currentblkReq };
        SpaceRequisitionService.getSpaces(searchObj).then(function (response) {
            if (response.data != null) {
                $scope.Markers = [];
                GetMarkers(response.data);
            }
        }, function (response) {
            progress(0, '', false);
        });
        setTimeout(function () {
            console.log($scope.currentblkReq.VAC);
            console.log($scope.ReqCountType.SYSP_VAL1);
            if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                if ($scope.DrpEnableStatus) {
                    $scope.AllocatedSeats = $scope.Markers;
                }
                else {
                    $scope.AllocatedSeats = $filter('filter')($scope.Markers, { SSA_FLR_CODE: selctedRow.SRC_FLR_CODE, layer: selctedRow.SRC_REQ_SEL_TYPE, SRD_SPC_SUB_TYPE: selctedRow.SRC_REQ_TYPE });
                }
                console.log($scope.currentblkReq.VAC);
                if ($scope.currentblkReq.VAC) {
                    var searchObj = {
                        Floor: selctedRow.SRC_FLR_CODE,
                        Tower: selctedRow.SRC_TWR_CODE,
                        Location: selctedRow.SRC_LCM_CODE,
                        City: selctedRow.SRC_CTY_CODE
                    };
                    SpaceAllocationService.getVacantSpaces(searchObj).then(function (response) {
                        $scope.vacantseats = [];
                        $scope.vacantseats = response.data;
                        var tempdata = $scope.vacantseats.concat($scope.tempSeats);
                        angular.forEach($scope.tempSeats, function (value, key) {
                            $scope.vacantseats.push(value);
                        });
                        $scope.SpacesOptions.api.setRowData([]);
                        $scope.SpacesOptions.api.setRowData($scope.vacantseats);
                    });
                    progress(0, '', false);
                }
                if ($scope.AllocatedSeats.length > 0) {
                    $scope.$apply(function () {
                        $scope.EnableStatus = 1;
                        $scope.SpacesOptions.api.setRowData([]);
                        $scope.SpacesOptions.api.setRowData($scope.AllocatedSeats);
                        progress(0, '', false);
                        $scope.AllocatedSeats = [];
                    });
                }
                else {
                    $scope.$apply(function () {
                        progress(0, '', false);
                        $scope.EnableStatus = 2;
                        $scope.DrpEnableStatus = true;
                        showNotification('error', 8, 'bottom-right', 'There are no spaces available on the chosen floor.');
                    });
                }
            }
            else if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                console.log($scope.currentblkReq.VAC);

                if ($scope.DrpEnableStatus) {
                    $scope.AllocatedSeats = $scope.Markers;
                }
                else {
                    $scope.AllocatedSeats = $filter('filter')($scope.Markers, { SSA_FLR_CODE: selctedRow.SRC_FLR_CODE });
                }
                if ($scope.currentblkReq.VAC == true) {
                    var searchObj = {
                        Floor: selctedRow.SRC_FLR_CODE,
                        Tower: selctedRow.SRC_TWR_CODE,
                        Location: selctedRow.SRC_LCM_CODE,
                        City: selctedRow.SRC_CTY_CODE,
                        SRN_REQ_ID: $scope.currentblkReq.SRN_REQ_ID

                    };
                    SpaceAllocationService.getVacantSpaces(searchObj).then(function (response) {
                        if (response.data.length > 0) {
                            console.log('sai');
                            $scope.EnableStatus = 1;
                            $scope.vacantseats = [];
                            $scope.vacantseats = response.data;

                            angular.forEach($scope.vacantseats, function (value, key) {
                                $scope.data2.push(value);

                            });

                            var tempdata = $scope.vacantseats.concat($scope.tempSeats);
                            angular.forEach($scope.tempSeats, function (value, key) {
                                $scope.vacantseats.push(value);
                            });
                            $scope.SpacesOptions.api.setRowData([]);
                            $scope.SpacesOptions.api.setRowData($scope.data2);
                        }
                        else {
                            showNotification('error', 8, 'bottom-right', 'There are no spaces available on the chosen floor.');
                        }

                    });
                    progress(0, '', false);
                }
                if ($scope.AllocatedSeats.length > 0 && $scope.currentblkReq.VAC == undefined) {
                    $scope.$apply(function () {
                        console.log('bhanu');
                        $scope.EnableStatus = 1;
                        $scope.SpacesOptions.api.setRowData([]);
                        $scope.SpacesOptions.api.setRowData($scope.AllocatedSeats);
                        angular.forEach($scope.AllocatedSeats, function (value, key) {
                            $scope.tempSeats.push(value);
                        });
                        progress(0, '', false);
                        $scope.AllocatedSeats = [];
                    });
                }
                else if ($scope.AllocatedSeats.length == 0 && $scope.currentblkReq.VAC == undefined) {
                    $scope.$apply(function () {
                        progress(0, '', false);
                        $scope.EnableStatus = 2;
                        $scope.DrpEnableStatus = true;
                        showNotification('error', 8, 'bottom-right', 'There are no spaces available on the chosen floor.');
                    });
                }
            }

        }, 1500)
    }

    $scope.chkChanged = function (selctedRow) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SRD_SPC_ID, spacetype: 'CHA' } });
            if (selctedRow.ticked) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
            }
        }

        if (!selctedRow.ticked) {

            var selrow = _.find($scope.selectedSpaces, { SRD_SSA_SRNREQ_ID: selctedRow.SRD_SSA_SRNREQ_ID });
            selrow.STACHECK = UtilityService.Deleted;

            if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                $scope.SelectedRowAlloc = _.find($scope.gridCountData, { SRC_FLR_CODE: selctedRow.SSA_FLR_CODE });

            } else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                $scope.SelectedRowAlloc = _.find($scope.gridCountData, { SRC_FLR_CODE: selctedRow.SSA_FLR_CODE, SRC_REQ_SEL_TYPE: selctedRow.layer, SRC_REQ_TYPE: selctedRow.SRD_SPC_SUB_TYPE });
            }
            _.remove($scope.SelectedRowAlloc.SelectedSpacesList, function (item) {
                return item === selctedRow.SRD_SSA_SRNREQ_ID;
            });
            $scope.SelectedRowAlloc.SelectedSeatCount = $scope.SelectedRowAlloc.SelectedSeatCount - 1



        }
        else {
            if ($scope.ReqCountType.SYSP_VAL1 == "1038") {

                $scope.SelectedRowAlloc = _.find($scope.gridCountData, { SRC_FLR_CODE: selctedRow.SSA_FLR_CODE });

            }
            else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                $scope.SelectedRowAlloc = _.find($scope.gridCountData, { SRC_FLR_CODE: selctedRow.SSA_FLR_CODE, SRC_REQ_SEL_TYPE: selctedRow.layer, SRC_REQ_TYPE: selctedRow.SRD_SPC_SUB_TYPE });
            }

            if ($scope.SelectedRowAlloc.SelectedSpacesList.length >= $scope.SelectedRowAlloc.SRC_REQ_CNT) {
                selctedRow.ticked = false;
                if ($scope.chkr != undefined) {
                    $scope.chkr.setStyle(VacantStyle);
                    $scope.chkr.ticked = false;
                }
                showNotification('error', 8, 'bottom-right', 'Selected Spaces Should be Less Than or Equal to Requested Spaces');
                return false;
            }
            else {
                //if (selctedRow.SRD_SSA_SRNREQ_ID == null)
                //{
                //    $scope.saveVacantSpaces()
                //}

                var selrow = _.find($scope.selectedSpaces, { SRD_SSA_SRNREQ_ID: selctedRow.SRD_SSA_SRNREQ_ID });
                $scope.SelectedRowAlloc.SelectedSpacesList.push(selctedRow.SRD_SSA_SRNREQ_ID);
                $scope.SelectedRowAlloc.SelectedSeatCount = $scope.SelectedRowAlloc.SelectedSpacesList.length;
                if (selrow != undefined) {
                    $scope.tempspace = selctedRow;
                    $scope.tempspace.STACHECK = UtilityService.Added;
                    $scope.selectedSpaces.push($scope.tempspace);
                    $scope.tempspace = {};
                }
                else {
                    $scope.tempspace = selctedRow;
                    $scope.tempspace.STACHECK = UtilityService.Added;
                    $scope.selectedSpaces.push($scope.tempspace);
                    $scope.tempspace = {};
                }
                return true;
            }
        }

    }
    $scope.SaveSelectedSpaces = function () {
        progress(0, 'Loading...', true);
        console.log($scope.selectedSpaces);
        angular.forEach($scope.selectedSpaces, function (data, key) {
            $scope.selspcObj = {};
            $scope.selspcObj.SRD_REQ_ID = data.SRD_REQ_ID;
            $scope.selspcObj.SRD_SRNREQ_ID = data.SRD_SRNREQ_ID;
            $scope.selspcObj.SRD_SSA_SRNREQ_ID = data.SRD_SSA_SRNREQ_ID;
            $scope.selspcObj.SRD_SPC_ID = data.SRD_SPC_ID;
            $scope.selspcObj.SRD_SPC_NAME = data.SRD_SPC_NAME;
            $scope.selspcObj.SRD_SPC_TYPE = data.layer;
            $scope.selspcObj.lat = data.lat;
            $scope.selspcObj.lon = data.lon;
            $scope.selspcObj.SRD_SPC_TYPE = data.SRD_SPC_TYPE;
            $scope.selspcObj.SRD_SPC_TYPE_NAME = data.SRD_SPC_TYPE_NAME;
            $scope.selspcObj.SRD_SPC_TYPE = data.SRD_SPC_TYPE;
            $scope.selspcObj.SRD_SPC_SUB_TYPE = data.SRD_SPC_SUB_TYPE;
            $scope.selspcObj.SRD_SPC_SUB_TYPE_NAME = data.SRD_SPC_SUB_TYPE_NAME;
            $scope.selspcObj.SRD_SH_CODE = data.SRD_SH_CODE;
            $scope.selspcObj.SRD_AUR_ID = data.SRD_AUR_ID;
            $scope.selspcObj.SSA_FLR_CODE = data.SSA_FLR_CODE;
            $scope.selspcObj.ticked = data.ticked;
            $scope.selspcObj.STACHECK = data.STACHECK;
            $scope.selspcObj.SRN_ALLC_REM = data.SRN_ALLC_REM;
            $scope.tickedSpaces.push($scope.selspcObj);

        });
        angular.forEach($scope.gridCountData, function (data, key) {
            if (data.SelectedSpacesList.length != 0) {
                $scope.selValues.push(data.SelectedSpacesList);
            }
        });
        if ($scope.selValues.length != $scope.gridCountData.length) {
            $scope.selValues = [];
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', 'Select Spaces to Allocate');
        }
        else {
            console.log($scope.currentblkReq);
            $scope.currentblkReq.ticked = true;
            var ReqObj = { spcreqdet: $scope.tickedSpaces, spcreq: $scope.currentblkReq };
            console.log(ReqObj);
            SpaceAllocationService.SaveSelectedSpaces(ReqObj).then(function (response) {
                if (response != null) {
                    progress(0, '', false);
                    $scope.back();
                    SpaceAllocationService.GetPendingList().then(function (data) {
                        if (data.data != null) {
                            $scope.gridata = data.data;
                            $scope.SpaceAllocOptions.api.setRowData([]);
                            $scope.SpaceAllocOptions.api.setRowData($scope.gridata);

                        }
                        else {
                            $scope.SpaceAllocOptions.api.setRowData([]);
                        }
                    }, function (response) {
                    });
                    showNotification('success', 8, 'bottom-right', response.Message);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }

            });
        }


    }
    $scope.SendMail = function () {
        SpaceAllocationService.SendMail($scope.currentblkReq).then(function (response) {
            if (response != null) {
                progress(0, '', false);
                $scope.back();
                showNotification('success', 8, 'bottom-right', 'Mail Sent Successfuly for ' + response.Message);
            }
            else {
                $scope.back();
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        });
    }

    $scope.back = function () {
        progress(0, '', false);
        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.Getcountry = [];
        $scope.EnableStatus = 0;
        $scope.tickedSpaces = [];
        $scope.Markers = [];
        $scope.DrpEnableStatus = false;
        $scope.currentblkReq = {};
    }


    ///// For map layout
    $scope.ViewinMap = function () {
        //$scope.MapFloors = [];
        //angular.forEach($scope.Flrlist, function (Value, Key) {
        //    Value.ticked = false;
        //    $scope.MapFloors.push(Value);
        //});
        //$scope.MapFloors[0].ticked = true;
        //$scope.Map.Floor.push($scope.MapFloors[0]);

        //$("#historymodal").modal('show');


        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.SpaceAllocation.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }
        $("#historymodal").modal('show');

    }
    $('#historymodal').on('shown.bs.modal', function () {

        progress(0, 'Loading...', true);
        if ($scope.SelRowData.length == 0) {
            $scope.loadmap();
        }
        else {
            progress(0, 'Loading...', false);
        }

    });
    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }
    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1};
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }
    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                $scope.SelRowData = $filter('filter')($scope.Markers, { SSA_FLR_CODE: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (marker, key) {

                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: marker.SRD_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SRD_REQ_ID = marker.SRD_REQ_ID;
                    $scope.marker.SRD_SRNREQ_ID = marker.SRD_SRNREQ_ID;
                    $scope.marker.SRD_SSA_SRNREQ_ID = marker.SRD_SSA_SRNREQ_ID;
                    $scope.marker.SRD_SPC_ID = marker.SRD_SPC_ID;
                    $scope.marker.SRD_SPC_NAME = marker.SRD_SPC_NAME;
                    $scope.marker.layer = marker.SRD_SPC_TYPE;
                    $scope.marker.SRD_SPC_TYPE_NAME = marker.SRD_SPC_TYPE_NAME;
                    $scope.marker.SRD_SPC_SUB_TYPE = marker.SRD_SPC_SUB_TYPE;
                    $scope.marker.SRD_SPC_SUB_TYPE_NAME = marker.SRD_SPC_SUB_TYPE_NAME;
                    $scope.marker.SRD_SH_CODE = marker.SRD_SH_CODE;
                    $scope.marker.SRD_AUR_ID = marker.SRD_AUR_ID;
                    $scope.marker.SSA_FLR_CODE = marker.SSA_FLR_CODE;
                    $scope.marker.STACHECK = marker.STACHECK;
                    $scope.marker.ticked = marker.ticked;
                    $scope.marker.SelectedSpacesList = null;
                    if (marker.ticked)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);
                    $scope.marker.bindLabel(marker.SRD_SPC_NAME);
                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);
                });
            },
            // error
            function (response) {
            }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});

    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};
            $scope.marker.SRD_REQ_ID = value.SRD_REQ_ID;
            $scope.marker.SRD_SRNREQ_ID = value.SRD_SRNREQ_ID;
            $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
            $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
            $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
            $scope.marker.layer = value.SRD_SPC_TYPE;
            $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
            $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
            $scope.marker.SRD_SPC_TYPE = value.SRD_SPC_TYPE;
            $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
            $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
            $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
            $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ticked = value.ticked;
            $scope.marker.SelectedSpacesList = null;
            $scope.Markers.push($scope.marker);

        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SRD_SPC_ID: this.SRD_SPC_ID });
        if (this.ticked == true) {
            $scope.SelectedRowAlloc = this;
            this.ticked = false;
            marker.ticked = false;
            $scope.chkChanged(marker);
        }
        else {
            $scope.SelectedRowAlloc = this;
            $scope.SelectedRowAlloc = this;
            this.ticked = true;
            this.setStyle(selctdChrStyle)
            marker.ticked = true;
            this.ticked = $scope.chkChanged(marker);
        }
        $scope.SpacesOptions.api.refreshView();
    }



}]);

