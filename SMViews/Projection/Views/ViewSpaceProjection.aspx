﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/privillegechecking.aspx.cs" Inherits="PrivilegeChecking" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
</head>
<body data-ng-controller="ViewSpaceProjectionController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">View Space Projection</h3>
            </div>
            <div class="card" style="padding-right: 10px;">
                <%-- <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>--%>
                <br />
                <div data-ng-show="Viewstatus==1">
                    <form role="form" id="Form1" name="frmViewGrid" novalidate>
                        <h4></h4>
                        <div>
                            <input id="ViewGridFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%;" />
                        </div>
                        <div style="height: 350px">
                            <div data-ag-grid="ViewOfficePlanReqs" style="height: 100%; width: 100%" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
                <form role="form" id="frmViewPlanReq" name="frmViewPlanReq" data-ng-show="EnableStatus == 1" novalidate>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <input id="txtModifyFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%;" />
                            </div>
                            <div style="height: 320px">
                                <div data-ag-grid="gridModifyOptions" style="height: 100%; width: 100%" class="ag-blue"></div>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix mt-12">
                        <div class="row btn-toolbar" style="padding-right: 35px">
                             <div class="col-md-12">
                            <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-click="UpdatePlanRequisition()" data-ng-disabled="ButtonStatus == 2" />
                            <input type="submit" value="Cancel" class="btn btn-primary custom-button-color" data-ng-click="Cancel()" data-ng-disabled="ButtonStatus == 2" />
                            <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                        </div>
                            </div>
                    </div>
            </form>
        </div>
    </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <%--    <script defer src="../../Utility.js"></script>
    <script defer src="../Js/ViewSpaceProjection.js"></script>--%>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/ViewSpaceProjection.js"></script>
</body>
</html>
