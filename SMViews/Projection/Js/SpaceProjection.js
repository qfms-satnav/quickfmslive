﻿app.service("SpaceProjectionService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    //to save   
    this.SaveDetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceProjection/SaveDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
}]);

app.controller('SpaceProjectionController', ['$scope', '$q', 'SpaceProjectionService', 'UtilityService','$filter', function ($scope, $q, SpaceProjectionService, UtilityService, $filter) {
    /////  Upload File
    $scope.OSPdata = [];
    $scope.ShowGrid = 0;
    $scope.uploadEnable = 1;
    $scope.Datastore = [];

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    $scope.UploadFile = function () {
        if ($('#FileUpl', $('#FileUsrUpl')).val()) {
            progress(0, 'Loading...', true);
            var filetype = $('#FileUpl', $('#FileUsrUpl')).val().substring($('#FileUpl', $('#FileUsrUpl')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                }
                else {
                    var formData = new FormData();
                    var UplFile = $('#FileUpl')[0];
                    formData.append("UplFile", UplFile.files[0]);
                    $.ajax({
                        url: UtilityService.path + "/api/SpaceProjection/UploadTemplate",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {

                            if (response.data != null) {
                                $scope.$apply(function () {
                                    $scope.ShowGrid = 1;
                                    $scope.uploadEnable = 0;
                                    $scope.Datastore = response.data.LST;
                                    $scope.REQ_ID = response.data.REQID;
                                    $scope.gridOptions.api.setRowData($scope.Datastore);
                                    $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                                    $scope.gridOptions.columnApi.getColumn("COST_CENTER_NAME").colDef.headerName = $scope.BsmDet.Child;
                                    $scope.gridOptions.api.refreshHeader();
                                    progress(0, '', false);
                                });
                                $scope.DispDiv = true;
                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', response.Message);

                            }
                            else {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', response.Message);
                            }
                        }
                    });
                }

            }
            else
                progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
        }
        else
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');

    }

    var columnDefs = [

        { headerName: "City", cellClass: "grid-align", field: "CITY", width: 130, suppressMenu: true },
        { headerName: "Location", cellClass: "grid-align", field: "LOCATION", width: 210, suppressMenu: true },
        { headerName: "Facility", cellClass: "grid-align", field: "TOWER", width: 210, suppressMenu: true },
        { headerName: "", cellClass: "grid-align", field: "VER_NAME", width: 500, suppressMenu: true, },
        { headerName: "", cellClass: "grid-align", field: "COST_CENTER_NAME", width: 550, suppressMenu: true, },
        { headerName: "Grade", cellClass: "grid-align", field: "GRADE", width: 50, suppressMenu: true },
        { headerName: "Q1", cellClass: "grid-align", field: "Q1", width: 50, suppressMenu: true },
        { headerName: "Q2", cellClass: "grid-align", field: "Q2", width: 50, suppressMenu: true },
        { headerName: "Q3", cellClass: "grid-align", field: "Q3", width: 50, suppressMenu: true },
        { headerName: "Q4", cellClass: "grid-align", field: "Q4", width: 50, suppressMenu: true },
        { headerName: "FY1", cellClass: "grid-align", field: "FY1", width: 50, suppressMenu: true },
        { headerName: "FY2", cellClass: "grid-align", field: "FY2", width: 50, suppressMenu: true },
        { headerName: "FY3", cellClass: "grid-align", field: "FY3", width: 50, suppressMenu: true },
        { headerName: "Built Capacity", cellClass: "grid-align", field: "BUILD_CAPACITY", width: 150, suppressMenu: true },
        { headerName: "Utilized Capacity", cellClass: "grid-align", field: "UTILIZED_SEATS", width: 150, suppressMenu: true },
        { headerName: "Available Capacity", cellClass: "grid-align", field: "AVAILABLE_SEATS", width: 150, suppressMenu: true },
        { headerName: "New Capacity Required", cellClass: "grid-align", field: "TOTAL_SEATS_REQUIRED", width: 150, suppressMenu: true },
        { headerName: "Vacant WS", cellClass: "grid-align", field: "AVAILABLE_WS", width: 100, suppressMenu: true },
        { headerName: "Allocated WS", cellClass: "grid-align", width: 100, template: "<input type='textbox' ng-model='data.REQWS' ng-blur ='refreshGrid(data,\"WS\")' />", suppressMenu: true },
        //ng-model='data.SRC_REQ_CNT'
        { headerName: "Vacant Cabins", cellClass: "grid-align", field: "AVAILABLE_CB", width: 150, suppressMenu: true },
        { headerName: "Allocated Cabins", cellClass: "grid-align", template: "<input type='textbox' ng-model='data.REQCAB' ng-blur ='refreshGrid(data,\"CB\")' />", width: 150, suppressMenu: true },
        //{ headerName: "Available TWD", cellClass: "grid-align", field: "AVAILABLE_TWD", width: 100, suppressMenu: true },
        //{ headerName: "Allocated TWD", cellClass: "grid-align", template: "<input type='textbox' ng-model='data.REQTWD' ng-blur ='refreshGrid(data,\"TWD\")' />", width: 100, suppressMenu: true },
        { headerName: "Available Capacity After Allocation(Available Capacity-(Allocated WS+CB))", cellClass: "grid-align", field: "TOTALSEAT", width: 450, suppressMenu: true },
        { headerName: "Pending(Required-(Allocated WS+CB))", cellClass: "grid-align", field: "TOTALSEAT_PENDING", width: 300, suppressMenu: true },
        //{
        //    headerName: "Required WS",
        //    field: "REQWS",

        //    cellRenderer: createReqWS,
        //}, 
        //{
        //    headerName: "Required Cabins",
        //    field: "REQCAB",

        //    cellRenderer: createReqCAB,
        //}
    ];





    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,

    };


    $scope.ComputeData = function () {
        progress(0, 'Loading...', true);

        $scope.dataobject = { savelst: $scope.SpcTypeCntLst, REQID: $scope.REQ_ID };
        $.ajax({
            url: UtilityService.path + "/api/SpaceProjection/ComputeData",
            type: "POST",
            data: $scope.dataobject,
            cache: false,
            success: function (response) {
                console.log(response);
                $scope.GridData = response.Table;

                $scope.ShowGrid = 1;
                $scope.uploadEnable = 0;
                console.log($scope.GridData);
                $scope.gridOptions.api.setRowData($scope.GridData);
                //angular.forEach($scope.GridData, function (Value, index) {
                //    Value.TOTALSEAT_PENDING = Value.TOTAL_SEATS_REQUIRED;
                //});
                //$scope.gridOptions.api.refreshView();
                progress(0, '', false);
            }
        });

    }

    $scope.REQ_ID = "";
    //To save a record
    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);



        $scope.gridOptions.api.forEachNodeAfterFilterAndSort(function (val, index) {
            $scope.OSPdata.push(val.data);
        });

        $scope.dataobject = { savelst: $scope.SpcTypeCntLst, REQID: $scope.REQ_ID };

        console.log($scope.dataobject);

        SpaceProjectionService.SaveDetails($scope.dataobject).then(function (dataobj) {
            console.log(dataobj);
            if (dataobj.Table[0].FLAG == 1) {
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', "Saved Successfully and Req Id is:-" + dataobj.Table[0].REQID);
                $('#FileUpl').val('');
                $scope.uploadEnable = 1;
                $scope.ShowGrid = 0;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', "Somethig went wrong. Please try again later.");
            }
        }, function (response) {
            progress(0, '', false);
        });
    }
    //function createReqWS(params) {

    //    var editing = false;
    //    var RWS;
    //    RWS = document.createElement('input');
    //    RWS.setAttribute('ng-model', 'data.REQWS');
    //   // RWS.setAttribute('ng-change', 'refreshGrid(data)');
    //    RWS.type = "input";
    //    RWS.id = params.rowIndex;

    //    return RWS;
    //}
    //function createReqCAB(params) {

    //    var editing = false;
    //    var RWS;
    //    RWS = document.createElement('input');
    //    RWS.setAttribute('ng-model', 'data.REQCAB');
    //    // RWS.setAttribute('ng-change', 'refreshGrid(data)');
    //    RWS.type = "input";
    //    RWS.id = params.rowIndex;

    //    return RWS;
    //}
    $scope.SpcTypeCntLst = [];
    $scope.SpcTypeCnt = {};
    $scope.refreshGrid = function (data, spacetype) {
        console.log(spacetype);
        //_.remove($scope.SpcTypeCntLst, function (space) { return space.ID == data.EU_ID });
        $scope.SpcTypeCntLst = _.reject($scope.SpcTypeCntLst, function (d) {
            return d.ID == data.EU_ID;
        });
        $scope.SpcTypeCnt = {};
        $scope.SpcTypeCnt.ID = data.EU_ID;
        $scope.SpcTypeCnt.REQWS = data.REQWS;
        $scope.SpcTypeCnt.REQCAB = data.REQCAB;
        //$scope.SpcTypeCnt.REQTWD = data.REQTWD;
        $scope.SpcTypeCntLst.push($scope.SpcTypeCnt);
        var AVAILABLE_WS = 0, AVAILABLE_CB = 0; // AVAILABLE_TWD=0
        if (data.refAVAILABLE_WS == null)
            data.refAVAILABLE_WS = data.AVAILABLE_WS;

        var result = $filter('filter')($scope.GridData, { LOCATION: data.LOCATION, TOWER: data.TOWER });

        total_CB = _.sumBy(result, function (o) {
            if (o.REQCAB == "")
                return 0;
            else
                return parseInt(o.REQCAB);
        });

        total_WS = _.sumBy(result, function (o) {
            if (o.REQWS == "")
                return 0;
            else
                return parseInt(o.REQWS);
        });
        //total_TWD = _.sumBy(result, function (o) {
        //    if (o.REQTWD == "")
        //        return 0;
        //    else
        //        return parseInt(o.REQTWD);
        //});

        if (spacetype == "WS") {
            if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQWS != null && data.REQWS != "") {
                AVAILABLE_WS = data.AVAILABLE_SEATS - (total_WS + total_CB);//+ total_TWD
                PENDING_WS = data.TOTAL_SEATS_REQUIRED - (total_WS + total_CB); //+total_TWD
                angular.forEach(result, function (Value, index) {
                    Value.TOTALSEAT = AVAILABLE_WS;
                    Value.TOTALSEAT_PENDING = PENDING_WS;
                });
                $scope.gridOptions.api.refreshView();
            }
        }
        //else if (spacetype == "TWD") {
        //    if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQTWD != null && data.REQTWD != "") {
        //        AVAILABLE_TWD = data.AVAILABLE_SEATS - (total_WS + total_CB + total_TWD);
        //         angular.forEach(result, function (Value, index) {
        //             Value.TOTALSEAT = AVAILABLE_TWD;
        //         });
        //         $scope.gridOptions.api.refreshView();
        //     }
        // }
        else {
            if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQCAB != null && data.REQCAB != "") {

                AVAILABLE_CB = data.AVAILABLE_SEATS - (total_WS + total_CB);//+ total_TWD
                PENDING_CB = data.TOTAL_SEATS_REQUIRED - (total_WS + total_CB); //+total_TWD
                angular.forEach(result, function (Value, index) {
                    Value.TOTALSEAT = AVAILABLE_CB;
                    Value.TOTALSEAT_PENDING = PENDING_CB;
                });
                $scope.gridOptions.api.refreshView();
            }
        }


    }

}]);