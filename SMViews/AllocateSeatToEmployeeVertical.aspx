﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Allocate Seat to Employee</title>
   <%-- <link href="../BootStrapCSS/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/bootstrap-select.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/amantra.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
    <link href="../BootStrapCSS/datepicker.min.css" rel="stylesheet" />
    <link href="../BootStrapCSS/jquery-ui.min.css" rel="stylesheet" />   --%>
     <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/jquery-ui.min.css" rel="stylesheet" />
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>

<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-10">
                        <fieldset class="text-center">
                            <legend>Allocate seat to Employee / Vertical
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal" runat="server">
                            <div id="PNLCONTAINER">


                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="lblMsg" class="control-label" style="color: red;"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div id="empdetails">
                                    <div class="col-md-12">
                                        <div class="panel-group" id="accordion">

                                            <div class="panel panel-primary" id="searchemppnl">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Search Employee / Vertical</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>


                                                                        <div class="col-md-7">
                                                                            <div class='input-group date datepicker' id='extdate'>
                                                                                <input name='txtFrmDate' type='text' id='txtFrmDate' class='form-control' required="required" />
                                                                                <span class='input-group-addon'><span class='fa fa-calendar' onclick="setup('extdate')"></span></span>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>

                                                                        <div class="col-md-7">
                                                                            <div class='input-group date datepicker' id='indate'>
                                                                                <input name='txtToDate' type='text' id='txtToDate' class='form-control' required="required" />
                                                                                <span class='input-group-addon'><span class='fa fa-calendar' onclick="setup('indate')"></span></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="trshift" class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="col-md-5 control-label">Shift<span style="color: red;">*</span></label>

                                                                        <div class="col-md-7">

                                                                            <select id="ddlshift" class="selectpicker" data-live-search="true">
                                                                                <option value='--Select--'>--Select--</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="trTimeSlot" class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="col-md-5 control-label">From:<span style="color: red;">*</span></label>
                                                                        <div class="col-md-7">
                                                                            <select name="starttimehr" class="selectpicker" data-live-search="true" required="required">
                                                                                <option value="Hr" selected>Hr</option>
                                                                                <option value="00">00</option>
                                                                                <option value="01">01</option>
                                                                                <option value="02">02</option>
                                                                                <option value="03">03</option>
                                                                                <option value="04">04</option>
                                                                                <option value="05">05</option>
                                                                                <option value="06">06</option>
                                                                                <option value="07">07</option>
                                                                                <option value="08">08</option>
                                                                                <option value="09">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                                <option value="13">13</option>
                                                                                <option value="14">14</option>
                                                                                <option value="15">15</option>
                                                                                <option value="16">16</option>
                                                                                <option value="17">17</option>
                                                                                <option value="18">18</option>
                                                                                <option value="19">19</option>
                                                                                <option value="20">20</option>
                                                                                <option value="21">21</option>
                                                                                <option value="22">22</option>
                                                                                <option value="23">23</option>
                                                                            </select>
                                                                            <select name="starttimemin" class="selectpicker" data-live-search="true" required="required">
                                                                                <option value="Min">Min</option>
                                                                                <option value="00">00</option>
                                                                                <option value="15">15</option>
                                                                                <option value="30">30</option>
                                                                                <option value="45">45</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="col-md-5 control-label">To:<span style="color: red;">*</span></label>
                                                                        <div class="col-md-7">
                                                                            <select name="endtimehr" class="selectpicker" data-live-search="true" required="required">
                                                                                <option value="Hr" selected>Hr</option>
                                                                                <option value="00">00</option>
                                                                                <option value="01">01</option>
                                                                                <option value="02">02</option>
                                                                                <option value="03">03</option>
                                                                                <option value="04">04</option>
                                                                                <option value="05">05</option>
                                                                                <option value="06">06</option>
                                                                                <option value="07">07</option>
                                                                                <option value="08">08</option>
                                                                                <option value="09">09</option>
                                                                                <option value="10">10</option>
                                                                                <option value="11">11</option>
                                                                                <option value="12">12</option>
                                                                                <option value="13">13</option>
                                                                                <option value="14">14</option>
                                                                                <option value="15">15</option>
                                                                                <option value="16">16</option>
                                                                                <option value="17">17</option>
                                                                                <option value="18">18</option>
                                                                                <option value="19">19</option>
                                                                                <option value="20">20</option>
                                                                                <option value="21">21</option>
                                                                                <option value="22">22</option>
                                                                                <option value="23">23</option>
                                                                            </select>

                                                                            <select name="endtimemin" class="selectpicker" data-live-search="true" required="required">
                                                                                <option value="Min">Min</option>
                                                                                <option value="00">00</option>
                                                                                <option value="15">15</option>
                                                                                <option value="30">30</option>
                                                                                <option value="45">45</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">

                                                                        <label for="lblSelVertical" class="col-md-5 control-label"><span style="color: red;">*</span></label>

                                                                        <div class="col-md-7">
                                                                            <select id="ddlVertical" class="selectpicker" data-live-search="true" required="required">
                                                                                <option value='--Select--'>--Select--</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="col-md-5 control-label">Select Employee </label>
                                                                        <div class="col-md-7">
                                                                            <input type='text' id='txtEmpName' class='form-control' />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-right">
                                                                            <button type='button' id="Button1" class="btn btn-primary custom-button-color">Allocate to Vertical</button>
                                                                            <button type='button' id="btnallocate" class="btn btn-primary custom-button-color">Check Employee Details</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="divemployeedetails" class="panel panel-default">
                                                            <div class="panel-heading">Employee Details</div>
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblEmpSearchId"></label>
                                                                                </span>
                                                                                Employee Id
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblEmpName"></label>
                                                                                </span>
                                                                                Employee Name
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblEmpEmailId"></label>
                                                                                </span>
                                                                                Email Id
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblReportingTo"></label>
                                                                                </span>
                                                                                Reporting To
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblDesignation"></label>
                                                                                </span>
                                                                                Designation
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblDepartment"></label>
                                                                                </span>
                                                                                Department
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <%--<div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblEmpExt"></label>
                                                                                </span>
                                                                                Employee Extension
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblResNumber"></label>
                                                                                </span>
                                                                                Contact Number
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>--%>

                                                                <div class="row">

                                                                    <%--<div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblState"></label>
                                                                                </span>
                                                                                State
                                                                            </li>
                                                                        </ul>
                                                                    </div>--%>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblPrjCode"></label>
                                                                                </span>
                                                                                <label for="lblSelProject"></label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblAUR_VERT_CODE"></label>
                                                                                </span>
                                                                                <label for="lblSelVertical1"></label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblCity"></label>
                                                                                </span>
                                                                                City
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblAUR_BDG_ID"></label>
                                                                                </span>
                                                                                Building
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblEmpTower"></label>
                                                                                </span>
                                                                                Tower
                                                                            </li>
                                                                        </ul>

                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblEmpFloor"></label>
                                                                                </span>
                                                                                Floor
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <%--<div class="col-md-6">
                                                                        <ul>
                                                                            <li class="list-group-item">
                                                                                <span class="badge">
                                                                                    <label for="lblDoj"></label>
                                                                                </span>
                                                                                Date of Joining
                                                                            </li>
                                                                        </ul>
                                                                    </div>--%>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <label class="col-md-5 control-label">Space</label>
                                                                                <a href="#" id="hypSpaceId"></a>
                                                                                <div class="col-md-7">
                                                                                    <label for="lblSpaceAllocated" style="font: bold; font-family: Arial; font-size: medium; text-underline-position: below; color: red"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-md-12 text-right">
                                                                                    <button type='button' id="btnSubmit" name="btnSubmit" class="btn btn-primary custom-button-color">Allocate to Employee</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label for="lblSeatStatus"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-danger" id="divgrid">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Details</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row" style="margin-top: 10px; display: none;">
                                                            <div class="col-md-10">
                                                                <table id="gdavail" class="table table-condensed table-bordered table-hover table-striped">
                                                                    <tr>
                                                                        <th>Spaces Available</th>
                                                                        <th>Seat Type</th>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-top: 10px">
                                                            <div class="col-md-10">
                                                                <table id="gvSharedEmployee" class="table table-condensed table-bordered table-hover table-striped">
                                                                    <tr>
                                                                        <th>Employee Id</th>
                                                                        <th>Employee Name</th>
                                                                        <th>Email ID</th>
                                                                        <th>Space ID</th>
                                                                        <th>From Date</th>
                                                                        <th>To Date</th>
                                                                        <th>From Time</th>
                                                                        <th>To Time</th>
                                                                        <th>Space Release</th>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-top: 10px">
                                                            <div class="col-md-10">
                                                                <table id="gvAllocatedSeats" class="table table-condensed table-bordered table-hover table-striped">
                                                                    <tr>
                                                                        <th style="display: none;">Reqid</th>
                                                                        <th>Vertical</th>
                                                                        <th>Space ID</th>
                                                                        <th>From Date</th>
                                                                        <th>To Date</th>
                                                                        <th>From Time</th>
                                                                        <th>To Time</th>
                                                                        <th>Space Release</th>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-10">
                                                            <h4><strong>Asset Details</strong></h4>
                                                        </div>
                                                        <div class="row" style="margin-top: 10px">
                                                            <div class="col-md-10">
                                                                <table id="gvAssets" class="table table-condensed table-bordered table-hover table-striped">
                                                                    <tr>
                                                                        <th>Asset Code</th>
                                                                        <th>Asset Name</th>
                                                                        <th>Space Asset Id</th>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div>
                            </div>




                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<%--<script defer src="../BootStrapCSS/Scripts/jquery.min.js"></script>
<script defer src="../BootStrapCSS/Scripts/bootstrap.min.js"></script>
<script defer src="../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>
<script defer src="../BootStrapCSS/Scripts/bootstrap-datepicker.min.js"></script>
<script defer src="../BootStrapCSS/Scripts/ObjectKeys.min.js"></script>
<script defer src="../BootStrapCSS/Scripts/wz_tooltip.min.js"></script>
<script defer src="../BootStrapCSS/Scripts/modernizr.min.js"></script>--%>
  <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>    
<script defer>
    $(document).ready(function () {       
        var cty = '';
        var vercode = '';
        var PrjCode = '';
        var deptname = '';
        var spacetyp = 0;
        var bdgid;
        var hypSpcId = '';

        $("#divemployeedetails").hide();

        var spcid = $(location).attr('search');
        //$("#btnSubmit").hide();

        // Vertical/Cost center labels
        GetlblVertical();

        //ddlshift binding
        GetURL();

        //ddlvertical binding
        GetVerticals();

        //
        fnBindSA()

        //Auto complete empid
        SearchText();

        $("#btnSubmit").hide();

        //gvAssets  binding
        fnbindgvAssets();
               
        //gvAssets binding function 
        function fnbindgvAssets() {
            $.ajax({
                type: "POST",
                url: "../api/AllSeattoEmpVertAPI/GetgvAssets",
                data: "=" + spcid,
                dataType: "json",
                success: function (data) {
                    var table = $('#gvAssets');
                    table.find("tr:gt(0)").remove();
                    for (var i = 0; i < data.length; i++) {
                        $("#gvAssets").append("<tr>" +
                            "<td>" + data[i].AAT_CODE + "</td>" +
                             "<td>" + data[i].AAT_NAME + "</td>" +
                             "<td>" + data[i].SPC_NAME + "</td>" + "</tr>");
                    }
                },
                failure: function (response) {

                }
            });
        }


        var lblparent;
        //Vertical/Cost center labels
        function GetlblVertical() {
            $.ajax({
                type: "get",
                url: "../api/AllSeattoEmpVertAPI/GetlblVerticalCC",
                data: {},
                contentType: "application/json; charset=utf-8",
                // async:false,
                success: function (data) {
                    $("label[for='lblSelVertical']").html(data[0]);
                    $("label[for='lblSelVertical1']").html(data[0]);
                    $("label[for='lblSelProject']").html(data[1]);
                    lblparent = data[0];
                },
                failure: function (response) {
                }
            });
        }

        //url post and fill ddlshift 
        function GetURL() {
            var url = $(location).attr('search');
            var val = url.split('/', 1);
            $.ajax({
                type: "POST",
                url: '../api/AllSeattoEmpVertAPI/GetURL',
                data: { 'URL': val },
                dataType: "json",
                success: function (data) {
                    var item = JSON.stringify(data);
                    var $select = $('#ddlshift');
                    $.each(data, function (key, val) {
                        $select.append('<option id="' + val.sh_code + '">' + val.sh_name + '</option>');
                    })
                    $("#ddlshift").selectpicker('refresh');
                },
            });
        }

        //fill ddlVertical
        function GetVerticals() {
            $.ajax({
                type: "GET",
                url: '../api/AllSeattoEmpVertAPI/GetVerticals',
                data: {},
                dataType: "json",
                success: function (data) {
                    var item = JSON.stringify(data);
                    var $select = $('#ddlVertical');
                    $.each(data, function (key, val) {
                        $select.append('<option id="' + val.VER_CODE + '">' + val.VER_NAME + '</option>');
                    })
                    $("#ddlVertical").selectpicker('refresh');
                },
            });
        }

        //AUTO COMPLETE EMPID-NAME
        function SearchText() {
            $("#txtEmpName").autocomplete({
                source: function (request, response) {
                    var vertcode = $('#ddlVertical').val();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        //url: "AllocateSeatToEmployeeVertical.aspx/SearchCustomers",
                        url: '../api/AllSeattoEmpVertAPI/SearchCustomers',
                        data: "{'AurId':'" + document.getElementById('txtEmpName').value + "', 'VerticalCode':'" + $('#ddlVertical').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data);
                        },
                        error: function (result) {
                            //alert(result.data);
                        }
                    });
                }
            });
        }

        //btnallocate check emp details tab - 2 Binding
        function btnalloc() {
            $("#divemployeedetails").hide();
            if ($("#txtFrmDate").val() != "") {
                if ($("#txtToDate").val() != "") {
                    if ($("#txtEmpName").val() != "") {
                        var employeeid = $("#txtEmpName").val().split(" ", 1);
                        //fnGetSpaceType();
                        $.ajax({
                            type: "POST",
                            url: '../api/AllSeattoEmpVertAPI/GetEmpDetails',
                            data: { 'EmployeeID': employeeid },
                            dataType: "json",
                            async: false,
                            success: function (data) {
                                if (data.length > 0) {

                                    $("label[for='lblEmpSearchId']").html(data[0].AUR_ID);
                                    $("label[for='lblEmpName']").html(data[0].AUR_FIRST_NAME);
                                    $("label[for='lblEmpEmailId']").html(data[0].AUR_EMAIL);
                                    $("label[for='lblReportingTo']").html(data[0].REPORTTO);
                                    $("label[for='lblAUR_BDG_ID']").html(data[0].AUR_BDG_ID);
                                    $("label[for='lblAUR_VERT_CODE']").html(data[0].VER_NAME);
                                    cty = data[0].AUR_CITY;
                                    vercode = data[0].AUR_VERT_CODE;
                                    $("label[for='lblCity']").html(data[0].AUR_CITY);
                                    $("label[for='lblDepartment']").html(data[0].AUR_DEP_ID);
                                    deptname = data[0].AUR_DEP_ID;
                                    $("label[for='lblDesignation']").html(data[0].AUR_DESGN_ID);
                                    $("label[for='lblEmpExt']").html(data[0].AUR_EXTENSION);
                                    $("label[for='lblEmpFloor']").html(data[0].AUR_FLOOR);
                                    $("label[for='lblPrjCode']").html(data[0].AUR_PRJ_CODE);
                                    PrjCode = data[0].AUR_PRJ_CODE;
                                    $("label[for='lblResNumber']").html(data[0].AUR_RES_NUMBER);
                                    $("label[for='lblState']").html(data[0].AUR_STATE);
                                    $("label[for='lblEmpTower']").html(data[0].AUR_TOWER);
                                    $("label[for='lblDoj']").html(data[0].AUR_DOJ);
                                    // $("label[for='hypSpaceId']").html(data[0].AUR_LAST_NAME);
                                    $("#hypSpaceId").text(data[0].AUR_LAST_NAME);
                                    hypSpcId = data[0].AUR_LAST_NAME;

                                    // alert("hyspcid- " + hypSpcId +"leng- " + hypSpcId.length);

                                    $('#ddlVertical').prop('selectedIndex', 0);
                                    $("#ddlVertical").selectpicker('refresh');

                                    if (hypSpcId.length == 0) {
                                        $("label[for='lblSpaceAllocated']").html("Space not allocated to " + data[0].AUR_FIRST_NAME);
                                        $("label[for='hypSpaceId']").html("");
                                        $("#hypSpaceId").hide();
                                    }
                                    else {
                                        $("label[for='lblSpaceAllocated']").html("Space ID");
                                        $("#hypSpaceId").show();
                                    }

                                    var verticl = data[0].VER_NAME;

                                    if ($("select[id$='ddlVertical'] option:contains(" + verticl + ")").length > 0) {
                                        $("#ddlVertical option:contains(" + verticl + ")").attr('selected', true);

                                        $("#ddlVertical").selectpicker('refresh');
                                    }
                                    else {
                                        var str = new Array[3];
                                        arr = verticl.Split("/");
                                        if (arr.Length > 0) {
                                            var $select = $('#ddlVertical');
                                            $select.append('<option id="' + $('#ddlVertical').children('option').length + '">' + arr + '</option>');
                                        }
                                    }

                                    //$("#collapseOne").removeClass('in');
                                    //$("#collapsetwo").addClass('in');
                                    //$("#collapseThree").removeClass('in');

                                }
                                else {
                                    $("label[for='lblmsg']").html("");
                                }
                            },
                        });

                        $("#divemployeedetails").show();
                        gdavailBind(cty, vercode);
                        //gdavail bind
                    }
                    else {
                        $("label[for='lblMsg']").html('. Please select employee to search.');
                    }
                }
                else {
                    $("label[for='lblMsg']").html('. Please Enter To Date');
                }
            }
            else {
                $("label[for='lblMsg']").html('. Please Enter From Date');
            }
        }

        //check employee details btn- tab2 
        $("#btnallocate").click(function () {
            $("label[for='lblMsg']").html('');
            var $btn = $(this).html('loading');            
            btnalloc();
            var $btn = $(this).html('Check Employee Details');
        });

        // load() functinality in aspx.cs, Binding gdavail  
        function gdavailBind(city, vercode) {
            //var url = $(location).attr('search');

            var ftime = "";
            var ttime = "";
            var StartHr = "00";
            var EndHr = "00";
            var StartMM = "00";
            var EndMM = "00";

            if (spacetyp == "1") {
                ftime = "00:00"
                ttime = "23:30"
            }
            else {
                ftime = StartHr + ":" + StartMM
                ttime = EndHr + ":" + EndMM
            }

            if ($("#starttimehr").val() != 'Hr') {
                StartHr = $("#starttimehr").val();
            }
            if ($("#starttimemin").val() != "Min") {
                StartMM = $("#starttimemin").val();
            }
            if ($("#endtimehr").val() != "Hr") {
                EndHr = $("#endtimehr").val();
            }
            if ($("#endtimemin").val() != "Min") {
                EndMM = $("#endtimemin").val();
            }


            var fromdt = $("#txtFrmDate").val();
            var todt = $("#txtToDate").val();

            $.ajax({
                type: "POST",
                url: '../api/AllSeattoEmpVertAPI/GetSeattoAllocateOccupied',
                data: { 'city': city, 'location': bdgid, 'fromdate': fromdt, 'todate': todt, 'fromtime': ftime, 'totime': ttime, 'spacetype': spacetyp, 'vertcode': vercode, 'spaceid': spcid },
                dataType: "json",
                async: false,
                success: function (data) {
                    var table = $('#gdavail');
                    table.find("tr:gt(0)").remove();

                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].SPC_ID + "</td>" +
                             "<td>" + data[i].SPC_BDG_ID + "</td>" +
                            "</tr>");
                    }

                    fnBindSA();
                    if ($('#gdavail tr').length > 1) {
                        $("#btnSubmit").show();
                    }
                    else {
                        $("#btnSubmit").hide();
                    }

                },
            });
        }


        //get spacetype and Binding gvSharedEmployee/gvAllocatedSeats
        function fnBindSA() {
            $.ajax({
                type: "POST",
                url: '../api/AllSeattoEmpVertAPI/GetSpaceType',
                data: "=" + spcid,
                dataType: "json",
                async: false,
                success: function (data) {
                    spacetyp = data.Table[0].SPACE_TYPE;
                    bdgid = data.Table[0].SPC_BDG_ID;

                    if (spacetyp == "2") {
                        $("#trshift").show();
                        $("#trTimeSlot").show();
                    }
                    else {
                        $("#trshift").hide();
                        $("#trTimeSlot").hide();
                    }
                    //gvSharedEmployee Binding
                    var table = $('#gvSharedEmployee');
                    table.find("tr:gt(0)").remove();
                    if (data.Table1.length == 0) {
                        table.append("<tr>" + "<td colspan='9' align='left'> Not Occupied By Any Employee.</td>" + "</tr>");
                    }
                    else {
                        for (var i = 0; i < data.Table1.length; i++) {
                            table.append("<tr>" + "<td>" + data.Table1[i].AUR_ID + "</td>" +
                                  "<td>" + data.Table1[i].AUR_FIRST_NAME + "</td>" +
                                  "<td>" + data.Table1[i].AUR_EMAIL + "</td>" +
                                 "<td>" + data.Table1[i].AUR_LAST_NAME + "</td>" +
                                  "<td>" + data.Table1[i].SSA_FROM_DATE + "</td>" +
                                  "<td>" + data.Table1[i].SSA_TO_DT + "</td>" +
                                 "<td>" + data.Table1[i].FROM_TIME + "</td>" +
                                  "<td>" + data.Table1[i].TO_TIME + "</td>" +
                                 //"<td>" + "<a id='Spacerelease' href=''>Space Release</a>" + "</td>" + "</tr>");
                                 "<td>" + "<button type='button' class='btn btn-primary' data-toggle='modal' >Space Release</button>" + "</td>" + "</tr>");

                            $("#searchemppnl").hide();
                            $("#collapseThree").addClass('in');

                            var splitspcid = spcid.split("=");

                            if (data.Table1[i].SSA_STA_ID == "7") {

                                $("label[for='lblSeatStatus']").html("( " + splitspcid[1] + " ) Occupied status.");
                            }
                            else if (data.Table1[i].SSA_STA_ID == "6") {
                                $("label[for='lblSeatStatus']").html("( " + splitspcid[1] + " ) Allocated status.");
                                // gvSharedEmployee.Visible = False
                            }
                        }
                    }


                    if (spacetyp == "1") {
                        //PNLCONTAINER hide
                    }


                    //gvAllocatedSeats Binding
                    var tableAS = $('#gvAllocatedSeats');
                    tableAS.find("tr:gt(0)").remove();
                    if (data.Table2.length == 0) {
                        tableAS.append("<tr>" + "<td colspan='8' align='left'> Not Allocated To Any Vertical.</td>" + "</tr>");
                    }
                    else {
                        $('#gvAllocatedSeats td').remove();
                        for (var i = 0; i < data.Table2.length; i++) {

                            //alert($("#hfid").val());
                            tableAS.append("<tr>" +
                                // "<td><input name=txtreqid type=text id=txtextemplimit text=" + data.Table2[i].SSA_SRNREQ_ID + " /></td>" + 
                                 "<td  style='display:none;'><input type='hidden' id='hdnreqid' value=" + data.Table2[i].SSA_SRNREQ_ID + "></td>" +
                                "<td>" + data.Table2[i].SSA_VERTICAL + "</td>" +
                                 "<td>" + data.Table2[i].SSA_SPC_ID + "</td>" +
                                  "<td>" + data.Table2[i].SSA_FROM_DATE + "</td>" +
                                 "<td>" + data.Table2[i].SSA_TO_DT + "</td>" +
                                  "<td>" + data.Table2[i].FROM_TIME + "</td>" +
                                 "<td>" + data.Table2[i].TO_TIME + "</td>" +
                                 //"<td>" + "<a id='releasell' href='#'>Release</a>" + "</td>" + "</tr>");
                                   "<td>" + "<button type='button' class='btn btn-primary' data-toggle='modal' >Release</button>" + "</td>" + "</tr>");
                        }
                        fnrelease1();
                    }
                },
            });
        }

        //Allocate to Vertical btn functionality
        $("#Button1").click(function () {
            $("label[for='lblMsg']").html('');
            var verticalcode = $("#ddlVertical").val();

            if ($("#txtFrmDate").val() != "") {
                if ($("#txtToDate").val() != "") {
                    if (verticalcode != "--Select--") {
                        //loadgrid gdavail
                        gdavailBind('', '');

                        var empcode = $("#txtEmpName").val().split(" ");


                        if ($('#gdavail').length > 0) {
                            // SubmitAllocationOccupied(ddlVertical.SelectedItem.Value, Session("uid"), 6)

                            $.ajax({
                                type: "POST",
                                url: '../api/AllSeattoEmpVertAPI/SubmitAllocationOccupied',
                                data: "=" + verticalcode,
                                dataType: "json",
                                async: false,
                                success: function (data) {
                                    var resl = data.split("@");

                                    fnsubmitAO(verticalcode, empcode[0], 6, resl[0], resl[1], resl[3]);
                                },
                            });
                        }
                        else {
                            $("label[for='lblMsg']").html('Time slot not available.');
                        }

                    }
                    else {
                        $("label[for='lblMsg']").html('. Please select ' + lblparent);
                    }
                }
                else {
                    $("label[for='lblMsg']").html('. Please Enter To Date');
                }
            }
            else {
                $("label[for='lblMsg']").html('. Please Enter From Date');
            }
        });

        //SubmitAllocationOccupied('','','') in aspx.cs
        function fnsubmitAO(verc, aurid, sta, reqid, osdate, tmpreqid) {
            var verticalreqid = reqid;
            var cntWst = 0
            var cntHCB = 0
            var cntFCB = 0
            var twr = ""
            var flr = ""
            var wng = ""
            var intCount = 0
            var ftime = ""
            var ttime = ""
            var StartHr = "00"
            var EndHr = "00"
            var StartMM = "00"
            var EndMM = "00"

            if ($("#starttimehr").val() != 'Hr') {
                StartHr = $("#starttimehr").val();
            }
            if ($("#starttimemin").val() != "Min") {
                StartMM = $("#starttimemin").val();
            }
            if ($("#endtimehr").val() != "Hr") {
                EndHr = $("#endtimehr").val();
            }
            if ($("#endtimemin").val() != "Min") {
                EndMM = $("#endtimemin").val();
            }

            if (spacetyp == "1") {
                ftime = "00:00"
                ttime = "23:30"
            }
            else {
                ftime = StartHr + ":" + StartMM
                ttime = EndHr + ":" + EndMM
            }

            var fromdatee = new Date($("#txtFrmDate").val());

            var todatee = new Date($("#txtToDate").val());

            var frmdtc = ((fromdatee.getMonth() + 1) + '/' + fromdatee.getDate() + '/' + fromdatee.getFullYear());
            var todtc = ((todatee.getMonth() + 1) + '/' + todatee.getDate() + '/' + todatee.getFullYear());
            var osdt = new Date(osdate);

            if ((fromdatee.getMonth() + 1) < (osdt.getMonth() + 1) & todatee.getFullYear() < osdt.getFullYear()) {                
                $("label[for='lblMsg']").html('You cant request for the past month');                
                return false;
            }
            else if ((fromdatee.getMonth() + 1) < (osdt.getMonth() + 1) & todatee.getFullYear() == osdt.getFullYear()) {
                $("label[for='lblMsg']").html('You cant request for the past month');                
                return false;
            }
            else if (fromdatee.getDate() < osdt.getDate()) {
                $("label[for='lblMsg']").html('Please Select Valid Date From Date Cannot be less than Current Date');
                return false;
            }
            
            else if (frmdtc < osdt || todtc < frmdtc) {
                $("label[for='lblMsg']").html('Selected To Date Cannot be less than From Date');
                return false;
            }
            //alert("fromdt- " + frmdtc + ". offsetdt- " + osdt + " todate- " + todtc);

            var cntWst = 0
            var cntHCB = 0
            var cntFCB = 0
            var sptype = '';
            //alert($("#gdavail").closest('tr').find('td:eq(1)').val());
            var table = $("#gdavail");

            table.find('tr').each(function (i) {
                if (i != 0) {
                    var $tds = $(this).find('td'),
                        //Id = $tds.eq(0).text(),
                        stype = $tds.eq(1).text();
                    sptype = stype;
                    if (stype == "WORK STATION") {
                        cntWst += 1;
                    }
                    if (stype == "HALF CABIN") {
                        cntHCB += 1;
                    }
                    if (stype == "FULL CABIN") {
                        cntFCB += 1;
                    }
                    //spacetyp

                }
            });            
            $("#collapseOne").removeClass('in');            
            $("#collapseThree").addClass('in');

            $.ajax({
                type: "POST",
                url: '../api/AllSeattoEmpVertAPI/BlockSeatsRequestPart1_Updated',
                data: { 'reqid': reqid, 'VerticalCode': $('#ddlVertical').val(), 'cntWst': cntWst, 'cntFCB': cntFCB, 'cntHCB': cntHCB, 'fromdate': $("#txtFrmDate").val(), 'todate': $("#txtToDate").val(), 'fromtime': ftime, 'totime': ttime, 'spacetype': spacetyp, 'costcenter': PrjCode, 'spaceid': spcid, 'AurId': aurid, 'BDGID': bdgid, 'city': cty, 'Statusid': sta, 'shift': $('#ddlshift').val(), 'DeptName': deptname, 'Messege': $("label[for='lblMsg']").text(), 'TmpReqseqid': tmpreqid },
                dataType: "json",
                async: false,
                success: function (data) {
                    //if (obj.Messege == "") {                       
                    //    HttpContext.Current.Response.Redirect("AllocateSeatToEmployeeVertical.html?id=" + HttpContext.Current.Request.QueryString[obj.spaceid]);
                    //}
                    //else {

                    //}

                    fnBindSA();
                },
                failure: function (data) {
                    alert('fail' + data);
                }
            });
        }


        //Allocate to employee
        $("#btnSubmit").click(function () {
            $("label[for='lblMsg']").html('');
            var verticalcode = $("#ddlVertical").val();
            if (verticalcode != "--Select--") {

                var empcode = $("#txtEmpName").val().split(" ");
                $.ajax({
                    type: "POST",
                    url: '../api/AllSeattoEmpVertAPI/VacantEmpToAllocateNewSeat',
                    data: { 'reqid': hypSpcId, 'AurId': empcode[0], 'DeptName': deptname },
                    dataType: "json",
                    async: false,
                    success: function (data) {

                        $.ajax({
                            type: "POST",
                            url: '../api/AllSeattoEmpVertAPI/SubmitAllocationOccupied',
                            data: "=" + verticalcode,
                            dataType: "json",
                            async: false,
                            success: function (data) {
                                var resl = data.split("@");
                                // fncheckvertical(resl[0], resl[1], resl[2]);

                                //fnsubmitAO(verticalcode, resl[2], 6, resl[0], resl[1], resl[3]);
                                fnsubmitAO(verticalcode, empcode[0], 7, resl[0], resl[1], resl[3]);

                               

                            },
                        });
                    },
                });
            }
            else {
                $("label[for='lblMsg']").html('Please select ' + lblparent);
            }
        });

        //Spacerelease gvSharedEmployee

        //$("#Spacerelease").click(function () {
        $("#gvSharedEmployee").on("click", 'button[type="button"]', function (e) {
            var Aurcode = $(this).closest('tr').find('td:eq(0)').text();
            var Spaceid = $(this).closest('tr').find('td:eq(3)').text();
            //spacetyp            

            $.ajax({
                type: "POST",
                url: '../api/AllSeattoEmpVertAPI/SharedEmployeeSpaceRelease',
                data: { 'spaceid': Spaceid, 'AurId': Aurcode, 'spacetype': spacetyp, 'spaceid': spcid },
                dataType: "json",
                async: false,
                success: function (data) {
                    fnBindSA();
                },
            });

            //return false;
        });

        //gvAllocatedSeats release click
        function fnrelease1() {
            // $("#releasell").on("click", function () {
            $("#gvAllocatedSeats").on("click", 'button[type="button"]', function (e) {
                var reqid = $(this).parents('tr').find('#hdnreqid').val();
                var Vert = $(this).closest('tr').find('td:eq(1)').text();
                var Spid = $(this).closest('tr').find('td:eq(2)').text();
                var fromdatet = $(this).closest('tr').find('td:eq(3)').text();
                var todatet = $(this).closest('tr').find('td:eq(4)').text();
                var fromtym = $(this).closest('tr').find('td:eq(5)').text();
                var totym = $(this).closest('tr').find('td:eq(6)').text();

                var employeeid = $("#txtEmpName").val().split(" ", 1);

                $.ajax({
                    type: "POST",
                    url: '../api/AllSeattoEmpVertAPI/AllocatedSeatsRelease',
                    data: { 'VerticalCode': Vert, 'spaceid': Spid, 'fromdate': fromdatet, 'todate': todatet, 'fromtime': fromtym, 'totime': totym, 'spacetype': spacetyp, 'DeptName': deptname, 'AurId': employeeid, 'reqid': reqid },
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        fnBindSA();
                    }
                });

                //return false;
            });

        }

        return false;
    });

    //date control
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>

