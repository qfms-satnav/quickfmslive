﻿app.service("CustomizedReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CustomizedReport/GetCustomizedDetails', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGriddataPivotExcel = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CustomizedReport/GetCustomizedDetailsPivotExcel', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CustomizedReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('CustomizedReportController', ['$scope', '$q', '$http', 'CustomizedReportService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, CustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Customized.CNP_NAME = [];
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.Customized.CHE_NAME = [];
    $scope.Download_Pivot = {};

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {

            $scope.BsmDet = response.data;
            $scope.Cols[11].COL = $scope.BsmDet.Parent;
            $scope.Cols[12].COL = $scope.BsmDet.Child;

            $scope.statuslst = [
                { Code: "All", Name: "All" },
                { Code: "1", Name: "Vacant" },
                { Code: "1002", Name: "Allocated To " + $scope.BsmDet.Parent },
                { Code: "1003", Name: "Allocated To " + $scope.BsmDet.Child },
                { Code: "1004", Name: "Occupied By Employee" },
                { Code: "1052", Name: "Blocked" }
            ];

        }
    });



    UtilityService.getSysPreferences().then(function (response) {

        if (response.data != null) {
            $scope.SysPref = response.data;
        }
        $scope.Download_Pivot = _.find($scope.SysPref, { SYSP_CODE: "Download to Pivot Excel" });
    });



    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;

            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                a.ticked = true;
                $scope.Customized.CNP_NAME.push(a);
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }

    });
    UtilityService.getChildEntity(parseInt(CompanySession)).then(function (response) {
        //console.log(response);
        if (response.data != null) {
            $scope.Entity = response.data;
            angular.forEach($scope.Entity, function (value, key) {
                //var a = _.find($scope.Entity, { CHE_CODE: parseInt(CompanySession) });
                value.ticked = true;
                //$scope.Customized.CHE_NAME.push(a);
            });
            //if (CompanySession == "1") { $scope.EnableStatus = 1; }
            //else { $scope.EnableStatus = 0; }
        }

    });
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var dataObj = {
            flrlst: $scope.Customized.Floors,
            Request_Type: $scope.Customized.Request_Type,
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            enylist: $scope.Customized.CHE_NAME,
            Type: $scope.flag
        };
        if (moment($scope.Customized.FromDate) > moment($scope.Customized.ToDate)) {
            $scope.GridVisiblity = false;
            $scope.GridVisiblity2 = false;
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        else {
            CustomizedReportService.GetGriddata(dataObj).then(function (response) {
                if (response.data != null) {
                    $scope.GridVisiblity = true;
                    $scope.GridVisiblity2 = true;
                    $scope.gridOptions.api.setRowData(response.data);
                }
                else {
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);
            });
        }
        $scope.columnDefs[13].headerName = $scope.BsmDet.BH1;
        $scope.columnDefs[14].headerName = $scope.BsmDet.BH2;
        $scope.columnDefs[15].headerName = $scope.BsmDet.PE;
        $scope.columnDefs[16].headerName = $scope.BsmDet.CE;
        $scope.columnDefs[17].headerName = $scope.BsmDet.Parent;
        $scope.columnDefs[18].headerName = $scope.BsmDet.Child;

        $scope.gridOptions.columnDefs = $scope.columnDefs;
        $scope.gridOptions.api.setColumnDefs();


        //$scope.gridOptions.columnApi.getColumn("VERTICAL").colDef.headerName = $scope.BsmDet.Parent;
        //$scope.gridOptions.columnApi.getColumn("COSTCENTER").colDef.headerName = $scope.BsmDet.Child;
        //$scope.gridOptions.columnApi.getColumn("BHO_NAME").colDef.headerName = $scope.BsmDet.BH1;
        //$scope.gridOptions.columnApi.getColumn("BHT_NAME").colDef.headerName = $scope.BsmDet.BH2;
        //$scope.gridOptions.columnApi.getColumn("PE_CODE").colDef.headerName = $scope.BsmDet.PE;
        //$scope.gridOptions.columnApi.getColumn("CHE_CODE").colDef.headerName = $scope.BsmDet.CE;
        //$scope.gridOptions.api.refreshHeader();


        //$scope.gridOptions.api.setDatasource(response.data);

    };


    $scope.ColumnNames = [];

    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.forEach($scope.Towers, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.forEach($scope.Floors, function (value, key) {
                                                    value.ticked = true;
                                                })
                                                setTimeout(function () {
                                                    $scope.LoadData();
                                                }, 500);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }



    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.Customized.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.Customized.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.Customized.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.Customized.Towers[0] = twr;
            }
        });

    }
    $scope.floorChangeAll = function () {
        $scope.Customized.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    $scope.Cols = [
        { COL: "Country", value: "COUNTRY", ticked: false },
        { COL: "City", value: "CITY", ticked: false },
        { COL: "Location", value: "LOCATION", ticked: false },
        { COL: "TOWER", value: "TOWER", ticked: false },
        { COL: "FLOOR", value: "FLOOR", ticked: false },
        { COL: "Space", value: "SPACE", ticked: false },
        { COL: "SeatType", value: "SNO", ticked: false },
        { COL: "Space Type", value: "SPACE_TYPE", ticked: false },
        { COL: "Space Sub Type", value: "SPACE_SUB_TYPE", ticked: false },
        { COL: "Area", value: "AREA", ticked: false },
        { COL: "Parent Entity", value: "PE_CODE", ticked: false },
        { COL: "Child Entity", value: "CHE_CODE", ticked: false },
        { COL: "", value: "VERTICAL", ticked: false },
        { COL: "", value: "COSTCENTER", ticked: false },
        { COL: "Employee Id", value: "EMP_ID", ticked: false },
        { COL: "Employee Name", value: "EMP_NAME", ticked: false },
        //{ COL: "From Date", value: "FromDate", ticked: false },
        //{ COL: "To Date", value: "ToDate", ticked: false },
        { COL: "Employee Mail", value: "MAIL", ticked: false },
        { COL: "Reporting To Id", value: "REPORTING", ticked: false },
        { COL: "Reporting To Name", value: "REPORTING_NAME", ticked: false },
        { COL: "Status", value: "STATUS", ticked: false },
        { COL: "From Date", value: "FromDate", template: '<span>{{data.SSA_FROM_DATE | date:"dd-MM-yyyy"}}</span>', ticked: false },
        { COL: "To Date", value: "ToDate", ticked: false },
        { COL: "Shift Time", value: "ShiftTime", ticked: false },
        { COL: "Allocation Date", value: "AllocationDate", ticked: false },
        { COL: "Employee Type", value: "AUR_TYPE", ticked: false },
        { COL: "Grade", value: "AUR_GRADE", ticked: false },
        { COL: "Long Leave From Date", value: "AUR_LONG_LEAVE_FROM_DT", ticked: false },
        { COL: "Long Leave To Date", value: "AUR_LONG_LEAVE_TO_DT", ticked: false },
        { COL: "Leave Status", value: "AUR_LEAVE_STATUS", ticked: false },
        { COL: "Leave Reason", value: "AUR_LONG_LEAVE_REASON", ticked: false },
        { COL: "Business Unit One", value: "BHO_NAME", ticked: false },
        { COL: "Business Unit Two", value: "BHT_NAME", ticked: false }


    ];

    $scope.columnDefs = [
        { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', suppressMenu: true, sno: 1 },
        { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100, sno: 2 },
        { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 150, sno: 3 },
        { headerName: "Tower", field: "TOWER", cellClass: 'grid-align', width: 100, sno: 4 },
        { headerName: "Floor", field: "FLOOR", cellClass: 'grid-align', width: 100, sno: 5 },
        { headerName: "Space", field: "SPACE", cellClass: 'grid-align', width: 200, suppressMenu: true, sno: 6 },
        { headerName: "Seat Type", field: "SPACE_TYPE", cellClass: 'grid-align', width: 150, sno: 34 },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", cellClass: 'grid-align', width: 150, sno: 7 },
        { headerName: "Space Sub Type", field: "SST_NAME", cellClass: 'grid-align', width: 150, sno: 8 },
        { headerName: "Area", field: "AREA", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 9 },
        { headerName: "Unit Cost", field: "UNITCOST", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "Seat Cost", field: "SEATCOST", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 10 },
        { headerName: "Aisle Area", field: "AISLEAREA", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 11 },
        { headerName: "Common Area", field: "COMMONAREA", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 12 },
        { headerName: "", field: "BHO_NAME", cellClass: 'grid-align', width: 120, suppressMenu: true, aggFunc: 'sum', sno: 13 },
        { headerName: "", field: "BHT_NAME", cellClass: 'grid-align', width: 120, suppressMenu: true, aggFunc: 'sum', sno: 14 },
        { headerName: "", field: "PE_CODE", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 15 },
        { headerName: "", field: "CHE_CODE", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 16 },
        { headerName: "", field: "VERTICAL", cellClass: 'grid-align', width: 150, sno: 17 },
        { headerName: "", field: "COSTCENTER", cellClass: 'grid-align', width: 200, sno: 18 },
        { headerName: "Employee Department/ Costcenter", field: "DEPARTMENT", cellClass: 'grid-align', width: 200, sno: 19 },
        { headerName: "Employee Id", field: "EMP_ID", cellClass: 'grid-align', width: 100, suppressMenu: true, sno: 20 },
        { headerName: "Employee Name", field: "EMP_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 21 },
        //{ headerName: "From Date", field: "FromDate", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        // { headerName: "To Date", field: "ToDate", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        { headerName: "Employee Mail", field: "MAIL", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 20 },
        { headerName: "Reporting To Id", field: "REPORTING", cellClass: 'grid-align', width: 100, sno: 21 },
        { headerName: "Reporting To Name", field: "REPORTING_NAME", cellClass: 'grid-align', width: 150, sno: 22 },
        { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 23 },
        { headerName: "From Date", field: "SSA_FROM_DATE", template: '<span>{{data.SSA_FROM_DATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 24 },
        { headerName: "To Date", field: "SSA_TO_DATE", template: '<span>{{data.SSA_TO_DATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 25 },
        { headerName: "Shift Time", field: "SHIFT_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 26 },
        { headerName: "Allocation Date", field: "ALLOCATION_DATE", template: '<span>{{data.ALLOCATION_DATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 27 },
        { headerName: "Employee Type", field: "AUR_TYPE", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 28 },
        { headerName: "Grade", field: "AUR_GRADE", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 29 },
        { headerName: "Long Leave From Date", field: "AUR_LONG_LEAVE_FROM_DT", template: '<span>{{data.AUR_LONG_LEAVE_FROM_DT | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 30 },
        { headerName: "Long Leave To Date", field: "AUR_LONG_LEAVE_TO_DT", template: '<span>{{data.AUR_LONG_LEAVE_TO_DT | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 31 },
        { headerName: "Leave Status", field: "AUR_LEAVE_STATUS", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 32 },
        { headerName: "Leave Reason", field: "AUR_LONG_LEAVE_REASON", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 33 }


    ];


    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $scope.gridOptions = {
        columnDefs: [],
        enableCellSelection: false,
        enableFilter: true,
        rowData: null,
        enableSorting: true,
        enableColResize: true,
        //showToolPanel: true,
        //paginationPageSize: 500,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        //floatingFilter: false,
        //debug: true,
        //enableServerSideSorting: true,
        //enableServerSideFilter: true,
        //rowModelType: 'infinite',
        //pagination: true,
        //paginationPageSize: 10,
        //cacheOverflowSize: 2,
        //maxConcurrentDatasourceRequests: 2,
        //infiniteInitialRowCount: 1,
        //maxBlocksInCache: 2,
    };


    function groupAggFunction(rows) {
        var sums = {
            AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.AREA += parseFloat((data.AREA).toFixed(2));
        });
        return sums;
    }

    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "COUNTRY" }, { title: "City", key: "CITY" }, { title: "Location", key: "LOCATION" }, { title: "Tower", key: "TOWER" },
        { title: "Floor", key: "FLOOR" }, { title: "Space Sub Type", key: "SST_NAME" }, { title: "Space Id", key: "SPACE" }, { title: "Space Type", key: "SPC_TYPE_NAME" },
        { title: "Area", key: "AREA" }, { title: "Unit Cost", key: "UNITCOST" }, { title: "Seat Cost", key: "SEATCOST" }, { title: "Aisle Cost", key: "AISLEAREA" }, { title: "Common Area", key: "COMMONAREA" },
        { title: "Parent Entity", key: "PE_CODE" }, { title: "Employee Id", key: "EMP_ID" }, { title: "Employee Name", key: "EMP_NAME" }, { title: "Employee Email", key: "MAIL" },
        { title: "Child Entity", key: "CHE_CODE" }, { title: "BU", key: "COSTCENTER" }, { title: "LOB", key: "DEPARTMENT" }, { title: "From Date", key: "SSA_FROM_DATE" },
        { title: "Reporting To Id", key: "REPORTING" }, { title: "Reporting To Name", key: "REPORTING_NAME" }, { title: "Allocation Date", key: "ALLOCATION_DATE" },
        { title: "Employee Type", key: "AUR_TYPE" }, { title: "Grade", key: "AUR_GRADE" }, { title: "To Date", key: "SSA_TO_DATE" }, { title: "Shift", key: "SHIFT_NAME" }, { title: "Status", key: "STATUS" }, { title: "Grade", key: "AUR_GRADE" }, { title: "Long Leave From Date", key: "AUR_LONG_LEAVE_FROM_DT" }, { title: "Long Leave To Date", key: "AUR_LONG_LEAVE_TO_DT" },
        { title: "Leave Status", key: "AUR_LEAVE_STATUS" }, { title: "Leave Reason", key: "AUR_LONG_LEAVE_REASON" }, { title: "Business Unit One", key: "BHO_NAME" }, { title: "Business Unit Two", key: "BHT_NAME" }

        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "A3");
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("CustomizationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });

        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("CustomizationReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.gridOptions.onColumnVisible = function (event) {
        if (event.visible) {
            $scope.HideColumns = 0;
            //console.log(event.column.colId + ' was made visible');     

            $scope.gridOptions.columnDefs.push(
                {
                    headerName: event.column.colDef.headerName,
                    field: event.column.colId,
                    cellClass: "grid-align",
                    width: 110,
                    sno: event.column.colDef.sno,
                    suppressMenu: true
                }
            );
            $scope.gridOptions.columnDefs.visible = true;
            _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
        } else {
            //console.log($scope.gridOptions.columnDefs.length);
            $scope.HideColumns = 1;
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                    $scope.gridOptions.columnDefs[i].visible = false;
                    $scope.gridOptions.columnDefs.splice(i, 1);
                }
            }
        }
    };

    $scope.GenReportPivot = function (Customized, Type) {
        progress(0, 'Downloading Please wait...', true);

        $scope.items = [];
        var dataObj1 = {
            flrlst: $scope.Customized.Floors,
            Request_Type: $scope.Customized.Request_Type,
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            enylist: $scope.Customized.CHE_NAME,
            Type: $scope.flag
        };
        CustomizedReportService.GetGriddataPivotExcel(dataObj1).then(function (response) {
            if (response != null) {
                $scope.items = response;
                alasql('SELECT * INTO XLSX("CustomizedReport_Pivot.xlsx",{headers:true}) FROM ?', [$scope.items]);
            }
            else {
                $scope.items = [];
            }
            progress(0, 'Loading...', false);
        });
    };


    $scope.GenReport = function (Customized, Type) {
        if (moment($scope.Customized.FromDate) > moment($scope.Customized.ToDate)) {
            $scope.GridVisiblity = false;
            $scope.GridVisiblity2 = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        progress(0, 'Downloading Please wait...', true);
        eaobj = {};
        eaobj.CNP_NAME = $scope.Customized.CNP_NAME[0].CNP_ID;
        eaobj.Type = Type;
        eaobj.Request_Type = $scope.Customized.Request_Type;
        eaobj.flrlst = Customized.Floors;
        eaobj.VERTICAL = $scope.BsmDet.Parent;
        eaobj.COSTCENTER = $scope.BsmDet.Child;
        eaobj.BH1 = $scope.BsmDet.BH1;
        eaobj.BH2 = $scope.BsmDet.BH2;
        eaobj.PE = $scope.BsmDet.PE;
        eaobj.CE = $scope.BsmDet.CE;
        eaobj.FromDate = $scope.Customized.FromDate;
        eaobj.ToDate = $scope.Customized.ToDate;
        eaobj.enylist = $scope.Customized.CHE_NAME;

        $http({
            //url: UtilityService.path + '/api/CustomizedReport/GetCustomizedData',
            url: UtilityService.path + '/api/CustomizedReport/GetCustomizedDatadwn',
            method: 'POST',
            data: eaobj,
            responseType: 'arraybuffer'

        }).then(function (data, status, headers, config) {
            var file = new Blob([data.data], {
                type: 'application/' + Type
            });
            var fileURL = URL.createObjectURL(file);
            $("#reportcontainer").attr("src", fileURL);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = 'CustomizedReport.' + Type;
            document.body.appendChild(a);
            a.click();
            progress(0, '', false);
        }),function (error,data, status, headers, config) {

        };
        // };
    }

    $scope.Customized.Request_Type = "All";
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.Pageload();

    $scope.selVal = "TODAY";
    $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
    $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.Customized.FromDate = "";
                $scope.Customized.ToDate = "";
                break;
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
}]);
