﻿app.service("EntityWiseReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetSummary = function (Params) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EntityWise/GetSummary', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchData = function (Params) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EntityWise/SearchData', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('EntityWiseReportController', ['$scope', '$q', '$http', 'EntityWiseReportService', 'UtilityService', '$timeout', '$filter',
    function ($scope, $q, $http, EntityWiseReportService, UtilityService, $timeout, $filter) {
        $scope.Customized = {};
        $scope.Customized.CNP_NAME = [];
        $scope.Request_Type = [];
        $scope.GridVisiblity = true;
        $scope.Columns = [];
        $scope.countrylist = [];
        $scope.Citylst = [];
        $scope.Locations = [];
        $scope.EntityWiseReport = {};
        $scope.ChartParam = {};
        $scope.search = [];

        //var columnDefs = [
        //    { headerName: "Country", field: "Country", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, suppressMenu: true},
        //    { headerName: "City", field: "City", width: 100, cellClass: 'grid-align', rowGroupIndex: 1, hide: true, suppressMenu: true },
        //    { headerName: "Location", field: "Location", width: 100, cellClass: 'grid-align', rowGroupIndex: 2, hide: true },
        //    { headerName: "Tower", field: "Tower", width: 100, cellClass: 'grid-align', rowGroupIndex: 3, hide: true },
        //    { headerName: "Floor", field: "Floor", width: 100, cellClass: 'grid-align' },
        //    { headerName: "Entity", field: "Entity Name", width: 100, cellClass: 'grid-align', suppressMenu: true },
        //    { headerName: "Total Seats", field: "Total Seats", width: 100, cellClass: 'grid-align', suppressMenu: true },
        //    { headerName: "Total Allocated Seats", field: "Allocated Seats", width: 100, cellClass: 'grid-align', suppressMenu: true },
        //    { headerName: "Vacant Seats", field: "Vacant Seats", width: 100, cellClass: 'grid-align', suppressMenu: true },
        //    //{ headerName: "Blocked Seats", field: "[BLOCKED_SEATS]", width: 100, cellClass: 'grid-align', suppressMenu: true },
        //    { headerName: "Total Cabin", field: "Total Cabin", width: 100, cellClass: 'grid-align', suppressMenu: true }
        //];


        var columnDefs = [
            { headerName: "Entity", field: "entity", width: 100, cellClass: 'grid-align' },
            { headerName: "City", field: "City", width: 100, cellClass: 'grid-align' },
            { headerName: "Location", field: "Location", width: 100, cellClass: 'grid-align' },
            { headerName: "Tower", field: "Tower", width: 100, cellClass: 'grid-align' },
            { headerName: "Floor", field: "Floor", width: 100, cellClass: 'grid-align' },
            { headerName: "Total Seats", field: "Total Seats", width: 100, cellClass: 'grid-align' },
            { headerName: "Total Allocated Seats", field: "Allocated Seats", width: 100, cellClass: 'grid-align' },
            { headerName: "Occupied Seats", field: "Occupied to Employee", width: 100, cellClass: 'grid-align', hide: true, },
            { headerName: "Vacant Seats", field: "Vacant Seats", width: 100, cellClass: 'grid-align' },
            { headerName: "Total Cabin", field: "Total_Cabin", width: 100, cellClass: 'grid-align' },
            { headerName: "Total WorkStation", field: "Total_Workstation", width: 100, cellClass: 'grid-align' }
        ];


        $scope.pageSize = '10';
        $scope.gridOptions = {
            columnDefs: columnDefs,
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,
            enableCellSelection: false,
            groupDefaultExpanded: 4,
            groupColumnDef: {
                headerName: "Country", field: "CNY_NAME",
                cellRenderer: {
                    renderer: "group"
                }
            },
            onAfterFilterChanged: function () {
                if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                    $scope.DocTypeVisible = 0;
                else
                    $scope.DocTypeVisible = 1;
            }
        };

        $scope.Pageload = function () {
            UtilityService.getCountires(2).then(function (response) {

                if (response.data != null) {
                    $scope.Country = response.data;
                    angular.forEach($scope.Country, function (value, key) {
                        value.ticked = true;
                    });
                    UtilityService.getCities(2).then(function (response) {
                        if (response.data != null) {
                            $scope.City = response.data;
                            angular.forEach($scope.City, function (value, key) {
                                value.ticked = true;
                            });
                            UtilityService.getLocations(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Locations = response.data;
                                    angular.forEach($scope.Locations, function (value, key) {
                                        value.ticked = true;
                                    });
                                }
                            });
                        }
                    });
                }
            });
            //$scope.gridOptions = {
            //    columnDefs: '',
            //    enableColResize: true,
            //    enableCellSelection: false,
            //    enableFilter: true,
            //    enableSorting: true,
            //    enableScrollbars: false,
            //    groupHideGroupColumns: true,
            //    suppressHorizontalScroll: false,
            //    onReady: function () {
            //        $scope.gridOptions.api.sizeColumnsToFit();
            //    }
            //};

            $scope.getLocationsByCity = function () {
                UtilityService.getLocationsByCity($scope.Branch.City, 2).then(function (response) {
                    $scope.Locations = response.data;
                }, function (error) {
                    console.log(error);
                });
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.City, function (value, key) {
                    var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
                    if (cny != undefined && value.ticked == true) {
                        cny.ticked = true;
                        $scope.EntityWise.Country[0] = cny;
                    }
                });
            }
            $scope.getTowerByLocation = function () {
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.Locations, function (value, key) {
                    var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
                    if (cty != undefined && value.ticked == true) {
                        cty.ticked = true;
                        $scope.EntityWise.City[0] = cty;
                    }
                });
            }

        }

        $scope.LoadData = function () {
            progress(0, 'Loading...', true);
            var params = {
                loclst: $scope.Locations,
            };

            EntityWiseReportService.GetSummary(params).then(function (response) {
                if (response != null) {
                    //$scope.gridOptions.api.setColumnDefs(response.Coldef);
                    $scope.gridOptions.api.setRowData(response.griddata);
                }
                else {
                    //$scope.gridOptions.api.setColumnDefs(response.Coldef);
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);
            });
        };


        $scope.search = function () {
            var params = {
                //LCM_NAME: _.filter($scope.Customized.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                LCM_CODE: _.filter($scope.EntityWise.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
            };

            EntityWiseReportService.SearchData(params).then(function (response) {
                if (response != null) {

                    //$scope.gridOptions.api.setColumnDefs(response.Coldef);
                    $scope.gridOptions.api.setRowData(response.griddata);
                }
                else {
                    //$scope.gridOptions.api.setColumnDefs(response.Coldef);
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);
            });
        }
        $("#filtertxt").change(function () {
            onFilterChanged($(this).val());
        }).keydown(function () {
            onFilterChanged($(this).val());
        }).keyup(function () {
            onFilterChanged($(this).val());
        }).bind('paste', function () {
            onFilterChanged($(this).val());
        })

        function onFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
        }


        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);

            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "Entity_Wise.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();
        }
        $scope.Pageload();
        setTimeout(function () { $scope.LoadData(); }, 500);
    }]);
