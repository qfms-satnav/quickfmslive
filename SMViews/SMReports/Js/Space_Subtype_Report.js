﻿app.service("Space_SubtypeRptService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Space_Subtype_Report/BindGrid', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    
}]);

app.controller('Space_SubtypeRptController', ['$scope', 'UtilityService', 'Space_SubtypeRptService', '$http', function ($scope, UtilityService, Space_SubtypeRptService,$http) {
    $scope.SpaceSubtypeRpt = {};
    $scope.SpaceSubtypeRpt.CNP_NAME = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.GridVisiblity2 = true;

    var columnDefs = [
        { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align' },
        { headerName: "City", field: "CITY", width: 100, cellClass: 'grid-align' },
        { headerName: "Location", field: "LOCATION", width: 150, cellClass: 'grid-align' },
        { headerName: "Tower", field: "TOWER", width: 100, cellClass: 'grid-align' },
        { headerName: "Floor", field: "FLOOR", width: 100, cellClass: 'grid-align' },
        { headerName: "Space ID", field: "SPACE", width: 100, cellClass: 'grid-align' },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", width: 150, cellClass: 'grid-align' },
        { headerName: "SpaceSub Type", field: "SST_NAME", width: 150, cellClass: 'grid-align' },
        { headerName: "Status", field: "STATUS", width: 150, cellClass: 'grid-align' }
  
    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: true,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        showToolPanel: true
    };


    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var dataObj = {
            flrlst: $scope.SpaceSubtypeRpt.Floors,
            
        };
        Space_SubtypeRptService.GetGriddata(dataObj).then(function (response) {
                if (response.data != null) {
                    $scope.gridOptions.api.setRowData([]);
                    $scope.gridOptions.api.setRowData(response.data);
                }
                else {
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);
            });
        //}
       
    };

    $scope.GenReport = function () {
        progress(0, 'Downloading Please wait...', true);
        var dataObj = {
            flrlst: $scope.SpaceSubtypeRpt.Floors,

        };
        $http({
            url: UtilityService.path + '/api/Space_Subtype_Report/GetReportDownload',
            method: 'POST',
            data: dataObj,
            responseType: 'arraybuffer'

        }).then(function (data, status, headers, config) {
            var file = new Blob([data.data], {
                type: 'application/' + 'xlsx'
            });
            var fileURL = URL.createObjectURL(file);
            $("#reportcontainer").attr("src", fileURL);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = 'SpaceSubTypeReport_' + new Date().toISOString() + '.xlsx';
            document.body.appendChild(a);
            a.click();
            progress(0, '', false);
        }) ,(function (error,data, status, headers, config) {

        });

    };

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }



    $scope.Pageload = function () {
        
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.forEach($scope.Towers, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.forEach($scope.Floors, function (value, key) {
                                                    value.ticked = true;
                                                })
                                                setTimeout(function () {
                                                    $scope.LoadData();
                                                }, 500);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.SpaceSubtypeRpt.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.SpaceSubtypeRpt.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.SpaceSubtypeRpt.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceSubtypeRpt.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.SpaceSubtypeRpt.City = $scope.City;
        $scope.getLocationsByCity();
    }



    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.SpaceSubtypeRpt.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceSubtypeRpt.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceSubtypeRpt.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.SpaceSubtypeRpt.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.SpaceSubtypeRpt.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceSubtypeRpt.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceSubtypeRpt.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceSubtypeRpt.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.SpaceSubtypeRpt.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceSubtypeRpt.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceSubtypeRpt.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceSubtypeRpt.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SpaceSubtypeRpt.Towers[0] = twr;
            }
        });

    }
    $scope.floorChangeAll = function () {
        $scope.SpaceSubtypeRpt.Floors = $scope.Floors;
        $scope.FloorChange();
    }
  
    $scope.Pageload();

   
}]);