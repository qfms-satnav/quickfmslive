﻿app.service("OverloadReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetGridData = function (OLRpt) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/OverloadReport/GetOverloadReportBindGrid', OLRpt)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('OverloadReportController', ['$scope', '$q', 'OverloadReportService', 'UtilityService', '$timeout', '$http', function ($scope, $q, OverloadReportService, UtilityService, $timeout, $http) {
    $scope.OLReport = {};
    $scope.OLReport.CNP_NAME = [];
    $scope.rptArea = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.Viewstatus = 0;
    $scope.Type = [];
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.GridVisiblity = false;


    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            //$scope.OLReport.CNP_NAME = parseInt(CompanySession);
            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                a.ticked = true;
                $scope.OLReport.CNP_NAME.push(a);
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }
    });

    $scope.LoadData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        progress(0, 'Loading...', true);
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.OLReport.CNP_NAME[0].CNP_ID
                };

                OverloadReportService.GetGridData(params).then(function (response) {
                    //$scope.RptByUsrGrid = true;
                    $scope.gridata = response.data;
                    if (response.data == null) {
                        $scope.gridOptions.api.setRowData([]);
                        $scope.GridVisiblity = false;
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                    else {
                        $scope.GridVisiblity = true;
                        progress(0, 'Loading...', true);
                        $scope.gridOptions.api.setRowData($scope.gridata);

                        progress(0, 'Loading...', false);
                    }
                    $scope.OverloadChartDetails();
                    //$scope.CostVerticalChartData();
                }, function (error) {
                    console.log(error);
                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);

    }

    var columnDefs = [
        { headerName: "Country", field: "CNY_NAME", width: 50, cellClass: 'grid-align', filter: 'set', suppressMenu: true, sno: 1 },
        { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', sno: 2 },
        { headerName: "Location", field: "LCM_NAME", width: 100, cellClass: 'grid-align', sno: 3 },
        { headerName: "Tower", field: "TWR_NAME", width: 50, cellClass: 'grid-align', suppressMenu: true, sno: 4 },
        { headerName: "Floor", field: "FLR_NAME", width: 50, cellClass: 'grid-align', sno: 5 },
        { headerName: "Space Type", field: "SPC_TYPE", width: 50, cellClass: 'grid-align', sno: 6 },
        { headerName: "Space Id", field: "SPACE_ID", width: 120, cellClass: 'grid-align', suppressMenu: true, sno: 7 },
        { headerName: "Overload Count", field: "OVERLOAD_COUNT", width: 80, cellClass: 'grid-align', suppressMenu: true, sno: 8 }
    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();
    var chart;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
            labels: true
        },
        legend: {
            show: false,
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Overload Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });

    function toggle(id) {
        chart.toggle(id);
    }

    d3.select('.container').insert('div', '.chart').attr('class', 'legend').selectAll('div').data({
        columns: [],
        cache: false,
        type: 'bar',
        empty: { label: { text: "Sorry, No Data Found" } },
        labels: true
    })
        .enter().append('div')
        .attr('data-id', function (id) {
            return id;
        })
        .html(function (id) {
            return id;
        })
        .each(function (id) {
            //d3.select(this).append('span').style
            d3.select(this).append('span').style('background-color', chart.color(id));
        })
        .on('mouseover', function (id) {
            chart.focus(id);
        })
        .on('mouseout', function (id) {
            chart.revert();
        })
        .on('click', function (id) {
            $(this).toggleClass("c3-legend-item-hidden")
            chart.toggle(id);
        });

    $scope.OverloadChartDetails = function () {
        $http({
            url: UtilityService.path + '/api/OverloadReport/GetOverloadChartData/' + $scope.OLReport.CNP_NAME[0].CNP_ID,
            method: 'POST',
            data: ''
        }).
            then(function (result) {
                chart.unload();
                chart.load({ columns: result });
            });

        setTimeout(function () {
            $("#SpcGraph").append(chart.element);
        }, 700);

    }
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });
    $scope.gridOptions.onColumnVisible = function (event) {
        if (event.visible) {
            $scope.HideColumns = 0;
            //console.log(event.column.colId + ' was made visible');     

            $scope.gridOptions.columnDefs.push(
                {
                    headerName: event.column.colDef.headerName,
                    field: event.column.colId,
                    cellClass: "grid-align",
                    width: 110,
                    sno: event.column.colDef.sno,
                    suppressMenu: true
                }
            );
            $scope.gridOptions.columnDefs.visible = true;
            _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
        } else {
            //console.log($scope.gridOptions.columnDefs.length);
            $scope.HideColumns = 1;
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                    $scope.gridOptions.columnDefs[i].visible = false;
                    $scope.gridOptions.columnDefs.splice(i, 1);
                }
            }
        }
    };
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" },
        { title: "Tower", key: "TWR_NAME" },
        { title: "Floor", key: "FLR_NAME" },
        { title: "Space Id", key: "SPACE_ID" }, { title: "Space Type", key: "SPC_TYPE" },
        { title: "Overload Count", key: "OVERLOAD_COUNT" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A3');
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("OverloadReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        //var Filterparams = {
        //    skipHeader: false,
        //    skipFooters: false,
        //    skipGroups: false,
        //    allColumns: false,
        //    onlySelected: false,
        //    columnSeparator: ",",
        //    fileName: "OverloadReport.csv"
        //}; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        //}, 1000);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("OverloadReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (OLReport, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        eaobj = {};
        eaobj.CompanyId = $scope.OLReport.CNP_NAME[0].CNP_ID;
        eaobj.Type = Type;
        eaobj.SearchValue = searchval;
        eaobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        eaobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        eaobj.Type = Type;
        if ($scope.HideColumns == 1 || $scope.HideColumns == 0) {
            progress(0, 'Loading...', true);
            if (Type == "pdf") {
                var columns = [];
                for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                    var dt = ({ title: $scope.gridOptions.columnDefs[i].headerName, key: $scope.gridOptions.columnDefs[i].field, sno: $scope.gridOptions.columnDefs[i].sno });
                    columns.push(dt);
                }
                var sortorder = _.sortBy(columns, [function (o) { return o.sno; }]);
                var model = $scope.gridOptions.api.getModel();
                var data = [];
                model.forEachNodeAfterFilter(function (node) {
                    data.push(node.data);
                });
                var jsondata = JSON.parse(JSON.stringify(data));
                var doc = new jsPDF('p', 'pt', 'A3');
                doc.autoTable(sortorder, jsondata, {
                    startY: 43,
                    margin: { horizontal: 7 },
                    styles: { overflow: 'linebreak', columnWidth: 'auto' },
                    columnStyles: { 0: { columnWidth: '15%' } }
                });
                doc.save("OverloadReport.pdf");
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

            }
            else {
                var mapvalues = [];
                var columns = "";
                _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

                    columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

                });
                mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
                alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("OverloadReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }

        }
        else if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/OverloadReport/OverloadReportData',
                method: 'POST',
                data: eaobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'OverloadReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);

}]);

