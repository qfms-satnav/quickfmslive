﻿app.service("EmployeeLocationReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EmployeeLocationReport/GetCustomizedDetails', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EmployeeLocationReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('EmployeeLocationReportController', ['$scope', '$q', '$http', 'EmployeeLocationReportService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, EmployeeLocationReportService, UtilityService, $timeout, $filter) {
    $scope.Branch = {};
    $scope.Branch.CNP_NAME = [];
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.ColumnNames = [];
    $scope.Towerlist = [];
    $scope.Locationlst = [];

    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                               
                            }
                        });
                    }
                });
            }
        });
    }

    $scope.gridOptions = {
        columnDefs: [],
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    }
   

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Branch.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Branch.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Branch.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Branch.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Branch.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.getTowerByLocation = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Branch.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Branch.City[0] = cty;
            }
        });
    }

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var dataObj = {
            flrlst: $scope.Branch.Locations
           
        };
       
        EmployeeLocationReportService.GetGriddata(dataObj).then(function (response) {
            $scope.gridata = response.griddata;
                console.table(response);
                if (response != null) {
                    $scope.gridOptions.api.setColumnDefs(response.Coldef);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(response.Coldef);
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);
            });
     
        $scope.gridOptions.api.setColumnDefs();
    };

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Branch_Employee_Data.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }

    $scope.Pageload();
    setTimeout(function () { $scope.LoadData(); }, 3000);

}]);
