﻿
app.service("DuplicateRecordsReportService", function ($http, $q, UtilityService) {

    this.raiseRequest = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DuplicateRecordsReport/GetData', ReqObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

});

app.controller('DuplicateRecordsReportController', function ($scope, $q, DuplicateRecordsReportService, UtilityService, $filter, $http) {

    $scope.DuplicateRecord = {};
    $scope.DocTypeVisible = 1;
    $scope.GridVisiblity = true;
    $scope.Excel = false;
    $scope.PDF = false;
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];

    $scope.Clear = function () {

        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });
        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Locations, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Towers, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floors, function (floor) {
            floor.ticked = false;
        });

    }

    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                console.log($scope.Country);
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                progress(0, '', false);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridOptions.columnApi.getColumn("COST_CENTER_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.gridOptions.api.refreshHeader();
        }
    });

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.DuplicateRecord.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.DuplicateRecord.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.DuplicateRecord.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DuplicateRecord.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.DuplicateRecord.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.DuplicateRecord.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DuplicateRecord.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DuplicateRecord.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.DuplicateRecord.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.DuplicateRecord.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DuplicateRecord.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DuplicateRecord.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.DuplicateRecord.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.DuplicateRecord.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DuplicateRecord.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DuplicateRecord.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.DuplicateRecord.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.DuplicateRecord.Towers[0] = twr;
            }
        });



    }
    $scope.floorChangeAll = function () {
        $scope.DuplicateRecord.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    setTimeout(function () {
        $scope.LoadDetails();
    }, 200);

    $scope.GetData = function () {
        progress(0, 'Loading...', true);
        var ReqObj = { flrlst: $scope.DuplicateRecord.Floors };
        DuplicateRecordsReportService.raiseRequest(ReqObj).then(function (response) {
            if (response != null) {
                $scope.gridata = response;
                console.log($scope.gridata.length);
                if ($scope.gridata.length == 0) {
                    $scope.DocTypeVisible = 1;
                    $scope.Excel = false;
                    $scope.PDF = false;
                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    $scope.DocTypeVisible = 0;
                    $scope.Excel = true;
                    $scope.PDF = true;
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.columnDefs = [
         { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 150 },
         { headerName: "Space Id", field: "SSA_SPC_ID", cellClass: 'grid-align', width: 200, },
         { headerName: "", field: "VER_NAME", cellClass: 'grid-align', width: 200 },
         { headerName: "", field: "COST_CENTER_NAME", cellClass: 'grid-align', width: 150 },
         { headerName: "Child Entity", field: "VER_CHE_CODE", cellClass: 'grid-align', width: 170, suppressMenu: true, },
         { headerName: "Employee Id", field: "SSAD_AUR_ID", width: 150, cellClass: 'grid-align' },
         { headerName: "Employee Name", field: "AUR_KNOWN_AS", cellClass: 'grid-align', width: 150 },
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Location", key: "LCM_NAME" }, { title: "Space Id", key: "SSA_SPC_ID" }, { title: "BU", key: "VER_NAME" }, { title: "LOB", key: "COST_CENTER_NAME" },
            { title: "Child Entity", key: "VER_CHE_CODE" }, { title: "Employee Id", key: "SSAD_AUR_ID" }, { title: "Employee Name", key: "AUR_KNOWN_AS" }
        ];

        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("DuplicateReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "DuplicateReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (ghudata, Type) {
        progress(0, 'Loading...', true);
        ghudata.Type = Type;

        Dataobj = {};

        Dataobj.VERTICAL = $scope.BsmDet.Parent;
        Dataobj.COSTCENTER = $scope.BsmDet.Child;
        Dataobj.flrlst = $scope.DuplicateRecord.Floors
        Dataobj.Type = ghudata.Type

        //var Dataobj = {
        //    flrlst: $scope.DuplicateRecord.Floors,
        //    Type: ghudata.Type,           
        //};

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (ghudata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/DuplicateRecordsReport/GetDuplicateReportdata',
                method: 'POST',
                data: Dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'DuplicateReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


});