﻿app.service("InfraAdministrationReportService", function ($http, $q, UtilityService) {

    this.GetInfraAdministrationReport = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/InfraAdministrationReport/GetInfraAdministrationReport')
            .then(function (response) {
                console.log(response);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetInfraAdministrationReportBySearch = function (dataobj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/InfraAdministrationReport/GetInfraAdministrationReportBySearch', dataobj)
            .then(function (response) {
                console.log(response);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});
app.controller('InfraAdministrationReportController', function ($scope, $q, $http, InfraAdministrationReportService, UtilityService, $timeout, $filter) {
    $scope.InfraAdministrationReport = {};
    $scope.InfraAdministrationReport.Locations = [];
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;

    var Options = {
        //onGridReady: function () {
        //    Options.api.sizeColumnsToFit();
        //}
    };

    UtilityService.GetLocationsall(1).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;
        }
    });

    //UtilityService.GetCompanies().then(function (response) {
    //    if (response.data != null) {
    //        $scope.Company = response.data;
    //        angular.forEach($scope.Company, function (value, key) {
    //            console.log(response);
    //            var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
    //            a.ticked = true;
    //            $scope.InfraAdministrationReport.CNP_NAME.push(a);
    //        });
    //        if (CompanySession == "1") { $scope.EnableStatus = 1; }
    //        else { $scope.EnableStatus = 0; }
    //    }
    //});

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);


        InfraAdministrationReportService.GetInfraAdministrationReport().then(function (response) {
            console.log(response);
            ExportColumns = response.exportCols;
            $scope.Options.api.setColumnDefs(response.Coldef);
            $scope.gridata = response.griddata;

            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.Options.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = true;
               
                $scope.Options.api.setRowData($scope.gridata);
                //$scope.Options.api.sizeColumnsToFit();
                progress(0, 'Loading...', false);
            }

        })

    }, function (error) {
        console.log(error);
        progress(0, '', false);
    }
    
    setTimeout(function () {
        $scope.LoadData();
    }, 100);

    function onFilterChanged(value) {
        $scope.Options.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.Options = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupHideGroupColumns: true,
        angularCompileRows: true,

        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.Options.api.getFilterModel()))
                $scope.DocTypeVisible = 1;
            else
                $scope.DocTypeVisible = 0;
        },
        onReady: function () {
            $scope.Options.api.sizeColumnsToFit()
        }


    };
    $scope.GenerateReport = function (InfraAdministrationReport, Type) {
        progress(0, 'Loading...', true);
        $scope.InfraAdministrationReport.Type = Type;
        if ($scope.Options.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf1();
            }
            else {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterExcel1();
                }
            }
        }
        else {
            if (Type == 'xls') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    JSONToCSVConvertor($scope.gridata, "Infrastructure Administration Report", true, "InfraAdministrationReport");
                }
                else {
                    JSONToCSVConvertor($scope.gridata, "Infrastructure Administration Report", true, "InfraAdministrationReport");
                }
            }
            else if (Type == 'pdf') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterPdf1();
                }
                else {
                    $scope.GenerateFilterPdf1();
                }
            }
        };
        progress(0, '', false);
    }
    $scope.GenerateFilterPdf1 = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns;
        var model = $scope.Options.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a3");
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("Infrastructure Administration Report.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Infrastructure Administration Report.csv"
        };
        $scope.Options.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

   // $timeout($scope.LoadData, 100);

    $scope.SearchData = function () {
        progress(0, 'Loading...', true);
        var param = {
            LCMLst: $scope.InfraAdministrationReport.Locations 
        }
        console.log(param);
        InfraAdministrationReportService.GetInfraAdministrationReportBySearch(param).then(function (response) {
            ExportColumns = response.exportCols;
            $scope.Options.api.setColumnDefs(response.Coldef);
            $scope.gridata = response.griddata;

            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.Options.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = true;
                $scope.Options.api.setRowData($scope.gridata);
                //$scope.Options.api.sizeColumnsToFit();
                progress(0, 'Loading...', false);
            }

        })

    }, function (error) {
        console.log(error);
        progress(0, '', false);
        }


    $scope.GenReportproject = function (Type) {
        saobj = {};
        saobj.Locations = $scope.InfraAdministrationReport.Locations;
        saobj.DocType = Type;
        $scope.InfraAdministrationReport.DocType = Type;
        if ($scope.Options.api.isAnyFilterPresent($scope.columnDefs)) {
            progress(0, 'Loading...', true);
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPrjPdf();
            }
            else {
                $scope.GenerateFilterPrjExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/InfraAdministrationReport/Export_SpaceConsolidatedPrjRpt',
                method: 'POST',
                data: saobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'InfraAdministrationReport.' + Type;
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }).error(function (data, status, headers, config) {

            });
        }
    };

});
