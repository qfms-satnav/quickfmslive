﻿app.service("SpaceRequisitionReportService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (SpaceRequisition) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisitionReport/GetGrid', SpaceRequisition)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisitionReport/GetDetailsOnSelection/', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('SpaceRequisitionReportController', ['$scope', '$q', '$http', 'SpaceRequisitionReportService', 'UtilityService','$timeout', function ($scope, $q, $http, SpaceRequisitionReportService, UtilityService, $timeout) {
    $scope.SpaceRequisition = {};
    $scope.GridVisiblity = true;
    $scope.GridVisiblity2 = false;
    $scope.Spcdata = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.REQUEST_TYPE_For_PopUP = "";
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];

    $scope.columnDefs = [
        {
            headerName: "Requisition Id", field: "REQ_ID", width: 200, cellClass: 'grid-align', template: '<a ng-click="ShowPopup(data)">{{data.REQ_ID}}</a>', filter: 'text', pinned: 'left', suppressMenu: true
        },
        { headerName: "Requisition Date", field: "REQ_DATE", template: '<span>{{data.REQ_DATE | date:"dd-MM-yyyy"}}</span>', width: 110, cellClass: 'grid-align', suppressMenu: true, },
        { headerName: "Child Entity", field: "VER_CHE_CODE", width: 100, cellClass: 'grid-align', width: 100 },
        { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 300 },
        { headerName: "Vertical", field: "VER_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "Total Count", field: "REQ_COUNT", cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 200 },
        { headerName: "Request Type", field: "REQUEST_TYPE", cellClass: 'grid-align', width: 150, suppressMenu: true, },];

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.SpaceRequisition.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 500);

    $scope.LoadData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    FromDate: $scope.SpaceRequisition.FromDate,
                    ToDate: $scope.SpaceRequisition.ToDate,
                    Request_Type: $scope.SpaceRequisition.Request_Type,
                    CNP_NAME: $scope.SpaceRequisition.CNP_NAME[0].CNP_ID
                };
                var fromdate = moment($scope.SpaceRequisition.FromDate);
                var todate = moment($scope.SpaceRequisition.ToDate);
                if (fromdate > todate) {
                    $scope.GridVisiblity = false;
                    showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.GridVisiblity = true;
                    SpaceRequisitionReportService.GetGriddata(params).then(function (data) {
                        $scope.gridata = data;
                        if (data.length == 0) {
                            $("#btNext").attr("disabled", true);
                            $scope.GridVisiblity2 = false;
                            $scope.gridOptions.api.setRowData([]);
                        }
                        else {
                            $scope.GridVisiblity2 = true;
                            $scope.gridOptions.api.setRowData($scope.gridata);
                        }
                        $scope.SpaceChartDetails(params);

                        progress(0, 'Loading...', false);
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    $scope.SpcdataLoad = function () {
        progress(0, 'Loading...', true);
        $scope.Pageload = {
            FromDate: $scope.SpaceRequisition.FromDate,
            ToDate: $scope.SpaceRequisition.ToDate,
            Request_Type: $scope.SpaceRequisition.Request_Type,
            CNP_NAME: $scope.SpaceRequisition.CNP_NAME[0].CNP_ID
        };
        SpaceRequisitionReportService.GetGriddata($scope.Pageload).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity2 = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity2 = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                if ($scope.SpaceRequisition.Request_Type == "ALL") {
                    if ($scope.gridata.REQUEST_TYPE == "Vertical Requisition") {
                        $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                        $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                        $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                        $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                        $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                        $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
                    }
                    else if ($scope.gridata.REQUEST_TYPE == "Space Extension") {
                        $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                        $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', true);
                        $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                        $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                        $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                        $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
                    }
                    else if ($scope.gridata.REQUEST_TYPE == "Space Requisition") {
                        $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                        $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', true);
                        $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                        $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                        $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                        $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);

                    }
                }
                progress(0, '', false);
            }
        }, function (error) {
            console.log(error);
        });
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    var PopDefs = [
        { headerName: "Space Id", field: "SPACE_ID", width: 190, cellClass: 'grid-align', suppressMenu: true, },
        { headerName: "Employee Name", field: "REQ_BY", width: 190, cellClass: 'grid-align' },
        { headerName: "Space Type", field: "SPACE_TYPE", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },
        { headerName: "Space Sub Type", field: "SPACE_SUB_TYPE", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },
        { headerName: "Shift Type", field: "SHIFT", width: 190, cellClass: 'grid-align', hide: false },
        { headerName: "Seat Extension Date", field: "EXT_DT", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, },
        { headerName: "Designation", field: "DESIGNATION", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: true, }];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        onReady: function () {
            $scope.PopOptions.api.sizeColumnsToFit()
        }
    }

    $scope.ShowPopup = function (data) {
        $scope.SelValue = data;
        $scope.REQUEST_TYPE_For_PopUP = data.REQUEST_TYPE;
        $scope.CurrentProfile = [];
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        SpaceRequisitionReportService.GetDetailsOnSelection($scope.SelValue).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.popdata = response.data;
            $scope.PopOptions.api.setRowData($scope.popdata);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 500);

            if ($scope.REQUEST_TYPE_For_PopUP == "Vertical Requisition") {
                $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
            }
            else if ($scope.REQUEST_TYPE_For_PopUP == "Space Extension") {

                $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', true);
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
            }
            else if ($scope.REQUEST_TYPE_For_PopUP == "Space Requisition") {
                if ($scope.SelValue.PREF_CODE == 1040) {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', true)
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', true);
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', true);
                }
                else if ($scope.SelValue.PREF_CODE == 1039) {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', true);
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);
                }
                else if ($scope.SelValue.PREF_CODE == 1041) {
                    $scope.PopOptions.columnApi.setColumnVisible('REQ_BY', false)
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SPACE_SUB_TYPE', true);
                    $scope.PopOptions.columnApi.setColumnVisible('SHIFT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('EXT_DT', false);
                    $scope.PopOptions.columnApi.setColumnVisible('DESIGNATION', false);

                }
            }
        });
    });

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();
    var chart;
    chart = c3.generate({
        data: {
            x: 'LCM_NAME',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                height: 130,
                show: true,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            },
            y: {
                show: true,
                label: {
                    text: 'Requisition Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });
    $scope.SpaceChartDetails = function (spcData) {
        $scope.Columndata = [];
        $http({
            url: UtilityService.path + '/api/SpaceRequisitionReport/GetSpaceChartData',
            method: 'POST',
            data: spcData
        }).then(function (response) {

            $scope.Columndata.push(response.data.data.Columnnames);
            angular.forEach(response.data.data.Details, function (value, key) {
                $scope.Columndata.push(value);
            });
            chart.unload();
            chart.load({ columns: $scope.Columndata });
            $scope.ColumnNames = response.data.Columnnames;
        });
        setTimeout(function () {
            $("#SpcGraph").empty();
            $("#SpcGraph").append(chart.element);
        }, 700);
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a3");
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("SpaceRequisitionReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SpaceRequisitionReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (spcdata, Type) {
        if (moment($scope.SpaceRequisition.FromDate) > moment($scope.SpaceRequisition.ToDate)) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        var dataobj = {
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: $scope.gridata[0].OVERALL_COUNT,
            FromDate: $scope.SpaceRequisition.FromDate,
            ToDate: $scope.SpaceRequisition.ToDate,
            Request_Type: $scope.SpaceRequisition.Request_Type,
            CNP_NAME: $scope.SpaceRequisition.CNP_NAME[0].CNP_ID,
            Type: Type
        };
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (spcdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpaceRequisitionReport/GetSpaceRequisitionReportdata',
                method: 'POST',
                data: dataobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                //var popupwin = window.open(fileURL, "SpaceRequisitionReport", "toolbar=no,width=500,height=500");
                //setTimeout(function () {
                //    popupwin.focus();
                //}, 500);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceRequisitionReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {

            };
        };
    }
    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.SpaceRequisition.FromDate = "";
                $scope.SpaceRequisition.ToDate = "";
                break;
            case 'TODAY':
                $scope.SpaceRequisition.FromDate = moment().format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.SpaceRequisition.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.SpaceRequisition.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.SpaceRequisition.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.SpaceRequisition.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.SpaceRequisition.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.SpaceRequisition.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.Customized.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;

        }
    }
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
    $scope.SpaceRequisition.Request_Type = "ALL";
}]);