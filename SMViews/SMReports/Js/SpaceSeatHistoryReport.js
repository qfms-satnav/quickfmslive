﻿app.service("SpaceSeatHistoryService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        console.log(data);
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceSeatHistoryReport/GetGridData',data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});
app.controller('SpaceSeatHistoryController', function ($scope, $q, $http, SpaceSeatHistoryService, $timeout, $filter) {
    $scope.SpaceSeatHistory = {};

    $scope.Types = [{
        "Id": 1,
        "Name": "By Created Date"
    }, {
        "Id": 2,
        "Name": "By Booked Date"
        }];

    $scope.SpaceSeatHistory.Type = $scope.Types[0];


    var columnDefs = [
        { headerName: "Space Id", field: "SPC_ID", width: 200, cellClass: 'grid-align', pinned: 'left', /*rowGroupIndex: 0, hide: true,*/},
        { headerName: "Employee Id", field: "EMP_NAME", width: 180, cellClass: 'grid-align', },
        { headerName: "From Date", field: "FROM_DATE", width: 130, cellClass: 'grid-align', },
        { headerName: "To Date", field: "TO_DATE", width: 130, cellClass: 'grid-align',  },
        { headerName: "From Time", field: "FROM_TIME", width: 100, cellClass: 'grid-align', },
        { headerName: "To Time", field: "TO_TIME", width: 100, cellClass: 'grid-align', },
        { headerName: "Booked Date", field: "CREATED_DT", width: 130 , cellClass: 'grid-align',  },
        { headerName: "Booked By ", field: "CREATED_BY", width: 180, cellClass: 'grid-align',  },
        { headerName: "Status", field: "SEAT_STATUS", width: 160, cellClass: 'grid-align',  },
    ]

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        angularCompileRows: true,
        rowData: null,

       // groupDefaultExpanded: 3,
        //groupHideOpenParents: true,
        //groupColumnDef: {
        //    headerName: "Space Id",
        //    //field: "PLANID",
        //    cellRenderer: {
        //        renderer: "group"
        //    }
        //},
    }


    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {
            FromDate: $scope.SpaceSeatHistory.FromDate,
            ToDate: $scope.SpaceSeatHistory.ToDate,
            Status: $scope.SpaceSeatHistory.Type.Id,           
        };
        console.log(params);

        SpaceSeatHistoryService.GetGriddata(params).then(function (response) {
            if (response != null) {
                $scope.gridOptions.api.setRowData(response);
                $scope.gridOptions.api.sizeColumnsToFit();
            }
            else {
                $scope.gridOptions.api.setRowData([]);
            }
            progress(0, 'Loading...', false);
        });
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.selVal = '30';
    
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.SpaceSeatHistory.FromDate = "";
                $scope.SpaceSeatHistory.ToDate = "";
                break;
            case 'TODAY':
                $scope.SpaceSeatHistory.FromDate = moment().format('MM/DD/YYYY');
                $scope.SpaceSeatHistory.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.SpaceSeatHistory.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.SpaceSeatHistory.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.SpaceSeatHistory.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.SpaceSeatHistory.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.SpaceSeatHistory.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.SpaceSeatHistory.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.SpaceSeatHistory.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.SpaceSeatHistory.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.SpaceSeatHistory.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.SpaceSeatHistory.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }

    setTimeout(function () {       
        $scope.rptDateRanges($scope.selVal);
        $scope.LoadData();
    }, 1000);


    $("#anchExportExcel").click(function (event) {
        if ($scope.usable == false) {
            event.preventDefault();
        } else {
            progress(0, 'Downloading Please Wait...', true);
            var Filterparams = {
                skipHeader: false,
                skipFooters: false,
                skipGroups: false,
                allColumns: false,
                onlySelected: false,
                columnSeparator: ",",
                fileName: "SpaceHistorySeatWise.csv"
            };
            //$scope.gridOptions.api.exportDataAsCsv(Filterparams);

            var csvString = $scope.gridOptions.api.getDataAsCsv(Filterparams);
            var fileNamePresent = Filterparams && Filterparams.fileName && Filterparams.fileName.length !== 0;
            var fileName = fileNamePresent ? Filterparams.fileName : 'export.csv';
            var blobObject = new Blob([csvString], {
                type: "text/csv;charset=utf-8;"
            });
            // Internet Explorer
            if (window.navigator.msSaveOrOpenBlob) {
                var fileData = [csvString];
                var blobObject = new Blob(fileData);
                window.navigator.msSaveOrOpenBlob(blobObject, fileName);
            } else {
                // Chrome
                var url = "data:text/plain;charset=utf-8," + encodeURIComponent(csvString);
                var downloadLink = document.createElement("a");
                downloadLink.href = url;
                downloadLink.href = window.URL.createObjectURL(blobObject);
                (downloadLink).download = fileName;

                document.body.appendChild(downloadLink);
                downloadLink.click();


                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
        }

    });


    $scope.GenReport = function (RptByCategory, Type) {
        progress(0, 'Loading...', true);
        if (Type == "pdf") {
            $scope.GenerateFilterPdf();
        }
        else {
            $scope.GenerateFilterExcel();
        }

    }


});
