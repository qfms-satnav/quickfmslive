﻿app.service("SpaceConsolidatedReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.BindGrid = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/BindGrid', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.BindGridSummary = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/BindGridSummary', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetVerticals = function (data) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/GetVerticals', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetCostcenter = function (data) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/GetCostcenter', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSummary = function (Params) {
        console.log(Params);
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/GetSummary', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SpaceConsolidatedChart = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/SpaceConsolidatedChart')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMancom = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/GetMancom', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


}]);

app.controller('SpaceConsolidatedReportController', ['$scope', '$q', 'SpaceConsolidatedReportService', '$filter', 'UtilityService', '$timeout', '$http',
    function ($scope, $q, SpaceConsolidatedReportService, $filter, UtilityService, $timeout, $http) {
        $scope.Consolidated = {};
        $scope.SpaceConsolidatedReport = {};
        $scope.ChartParam = {};
        $scope.Viewstatus = 0;
        $scope.rptArea = {};
        $scope.rptareaprj = {};
        $scope.ProjectReport = {};
        $scope.GridVisiblity = false;
        $scope.Type = [];
        $scope.DocTypeVisible = 0;
        $scope.CompanyVisible = 0;
        $scope.EnableStatus = 0;
        $scope.Company = [];
        $scope.Vertical = [];
        $scope.CostCenter = [];
        $scope.VER_CODE = [];
        $scope.Cost_Center_Code = [];
        $scope.showsummary = true;
        $scope.GridVisiblityproject = false;
        $scope.GridVisiblitylocation = false;
        $scope.GridVisiblitymancom = false;

        //$scope.exportDataAsCsv = [];

        setTimeout(function () {
            var bool = false;
            UtilityService.GetCompanies().then(function (response) {
                if (response.data != null) {
                    $scope.Company = response.data;
                    //$scope.Consolidated.CNP_NAME = parseInt(CompanySession);
                    angular.forEach($scope.Company, function (value, key) {
                        var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                        a.ticked = true;
                    });
                    if (CompanySession == "1") { $scope.EnableStatus = 1; }
                    else { $scope.EnableStatus = 0; }
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locations = response.data;
                            angular.forEach($scope.Locations, function (value, key) {
                                value.ticked = true;
                            });
                            UtilityService.getVerticals(2).then(function (response2) {
                                if (response2.data != null) {
                                    $scope.Vertical = response2.data;
                                    angular.forEach($scope.Vertical, function (value, key) {
                                        value.ticked = true;
                                    });
                                    UtilityService.getCostCenters(2).then(function (response1) {
                                        console.log(response1)
                                        if (response1.data != null) {
                                            $scope.CostCenter = response1.data;
                                            angular.forEach($scope.CostCenter, function (value1, key) {
                                                value1.ticked = true;
                                                bool = true;
                                            });
                                            if (bool)
                                                $scope.BindGrid();
                                        }
                                    });
                                }
                            });
                        }
                    })
                }

            });
        }, 1000);

        $scope.Month = [
            { id: 0, name: 'January' },
            { id: 1, name: 'February' },
            { id: 2, name: 'March' },
            { id: 3, name: 'April' },
            { id: 4, name: 'May' },
            { id: 5, name: 'June' },
            { id: 6, name: 'July' },
            { id: 7, name: 'August' },
            { id: 8, name: 'September' },
            { id: 9, name: 'October' },
            { id: 10, name: 'November' },
            { id: 11, name: 'December' },

        ];

        angular.forEach($scope.Month, function (value, key) {
            value.ticked = true;
        });

        //var a = _.find($scope.Month, function (o) { return o.id == new Date().getMonth() });
        //console.log(a);
        //a.ticked = true;

        $scope.getyears = [];
        function getyears() {
            var myDate = new Date();
            var year = myDate.getFullYear();
            for (var i = 2000; i <= year + 1; i++) {
                $scope.getyears.push({ name: i.toString() });
            }
            angular.forEach($scope.getyears, function (value, key) {
                var a = _.find($scope.getyears, function (o) { return o.name == new Date().getFullYear() });
                a.ticked = true;
            });
        }
        getyears();


        $scope.BindGrid = function () {
            var params = {
                CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID,
                LCM_NAME: _.filter($scope.Consolidated.LCM_NAME, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                VER_CODE: _.filter($scope.SpaceConsolidatedReport.VER_NAME, function (o) { return o.ticked == true; }).map(function (x) { return x.VER_CODE; }).join(','),
                Cost_Center_Code: _.filter($scope.CostCenter, function (o) { return o.ticked == true; }).map(function (x) { return x.COST_CENTER_CODE; }).join(','),
                FLAG: 1,
                month: _.filter($scope.SpaceConsolidatedReport.Month, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(','),
                year: _.filter($scope.SpaceConsolidatedReport.getyears, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(','),
            };

            $scope.ChartParam = params;
            progress(0, 'Loading...', true);
            SpaceConsolidatedReportService.BindGrid(params).then(function (response) {
                progress(0, 'Loading...', true);
                $scope.gridata = response.data;
                if (response.data == null) {
                    $scope.GridVisiblity = false;
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, 'Loading...', false);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.GridVisiblity = true;
                    $scope.GridVisiblityproject = false;
                    $scope.GridVisiblitylocation = false;
                    $scope.GridVisiblitymancom = false;
                    $scope.gridOptions.api.setRowData([]);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000)
                }
                $scope.SpaceConsolidatedChart();
            }, function (error) {
                console.log(error);
            });
        };


        var columnDefsLoc = [
            { headerName: "City", field: "CTY_NAME", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Location Code", field: "LCM_CODE", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Location", field: "LCM_NAME", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Space Type", field: "SPC_TYPE", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Total Seats", field: "TOTAL_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Vacant Seats", field: "VACANT_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 160, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Employee Shared Seats", field: "EMPLOYEE_OCCUPIED_SEAT_COUNT", width: 180, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Company Name", field: "CNP_NAME", width: 170, cellClass: 'grid-align', suppressMenu: true }
        ];
        $scope.pageSize = '10';

        $scope.gridLocation = {
            columnDefs: columnDefsLoc,
            rowData: [],
            enableFilter: true,
            enableSorting: true,
            angularCompileRows: true,
            enableCellSelection: false,
            enableColResize: true,
            //groupAggFunction: groupaggfunctionloc,
            groupHideGroupColumns: false,
            groupColumnDef: {
                headerName: "City",
                cellRenderer: {
                    renderer: "group"
                }

            },
            onAfterFilterChanged: function () {
                if (angular.equals({}, $scope.gridLocation.api.getFilterModel()))
                    $scope.DocTypeVisible = 0;
                else
                    $scope.DocTypeVisible = 1;
            }
        }

        $scope.BindGrid_Location = function () {
            var params = {
                CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID,
                FLAG: 2
            };

            $scope.ChartParam = params;
            progress(0, 'Loading...', true);
            // $scope.GridVisiblity = true;
            SpaceConsolidatedReportService.BindGrid(params).then(function (response) {
                progress(0, 'Loading...', true);
                //$scope.RptByUsrGrid = true;
                $scope.gridata = response.data;
                if (response.data == null) {
                    $scope.GridVisiblity = false;
                    $scope.GridVisiblityproject = false;
                    $scope.GridVisiblitylocation = true;
                    $scope.GridVisiblitymancom = false;
                    $scope.gridLocation.api.setRowData([]);
                    progress(0, 'Loading...', false);
                    //showNotification('error', 8, 'bottom-right', response.Message);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.GridVisiblity = false;
                    $scope.GridVisiblityproject = false;
                    $scope.GridVisiblitylocation = true;
                    $scope.GridVisiblitymancom = false;
                    $scope.gridLocation.api.setRowData([]);
                    var TOTAL_SEATS_SUM = _.sumBy($scope.gridata, function (o) { return o.TOTAL_SEATS; });
                    var TOTAL_SEATS_VACANT = _.sumBy($scope.gridata, function (o) { return o.VACANT_SEATS; });
                    var TOTAL_SEATS_OCCUPIED = _.sumBy($scope.gridata, function (o) { return o.OCCUPIED_SEATS; });


                    $scope.gridata.push({ CTY_NAME: '', LCM_NAME: 'Total', TOTAL_SEATS: TOTAL_SEATS_SUM, VACANT_SEATS: TOTAL_SEATS_VACANT, OCCUPIED_SEATS: TOTAL_SEATS_OCCUPIED, });
                    $scope.gridLocation.api.setRowData($scope.gridata);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000)
                }
                //$scope.SpaceConsolidatedChart();
            }, function (error) {
                console.log(error);
            });
        };

        var columnDefsMancom = [
            { headerName: "Mancom", field: "MANCOM", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Space Type", field: "SPC_TYPE", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Total Seats", field: "TOTAL_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Allocated Seats", field: "ALLOCATED_VACANT_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Vacant", field: "VACANT", width: 170, cellClass: 'grid-align', suppressMenu: true }
        ];
        $scope.pageSize = '10';


        $scope.gridmancom = {
            columnDefs: columnDefsMancom,
            rowData: [],
            enableFilter: true,
            enableSorting: true,
            angularCompileRows: true,
            enableCellSelection: false,
            enableColResize: true,
            //groupAggFunction: groupaggfunctionloc,
            groupHideGroupColumns: false,

        };


        //setTimeout(function () {
        $scope.BindGrid_Mancom = function () {
            var params = {
                CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID
                //FLAG: 1
                //CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID
            };

            $scope.ChartParam = params;
            progress(0, 'Loading...', true);
            SpaceConsolidatedReportService.GetMancom(params).then(function (response) {
                progress(0, 'Loading...', true);
                //$scope.RptByUsrGrid = true;
                $scope.gridata2 = response;
                if (response == null) {
                    $scope.GridVisiblity = false;
                    $scope.gridmancom.api.setRowData([]);
                    progress(0, 'Loading...', false);
                    //showNotification('error', 8, 'bottom-right', response.Message);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.GridVisiblity = false;
                    $scope.GridVisiblityproject = false;
                    $scope.GridVisiblitylocation = false;
                    $scope.GridVisiblitymancom = true;

                    $scope.gridmancom.api.setRowData([]);
                    $scope.gridmancom.api.setRowData($scope.gridata2);
                    progress(0, 'Loading...', false);
                }
                //$scope.SpaceConsolidatedChart();
            }, function (error) {
                console.log(error);
            });
        };

        var columnDefs = [
            {
                headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, suppressMenu: true
            },
            { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 1, hide: true, suppressMenu: true },
            { headerName: "Location", field: "LCM_NAME", width: 200, cellClass: 'grid-align', rowGroupIndex: 2, hide: true },
            { headerName: "Tower", field: "TWR_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 3, hide: true },
            { headerName: "Floor", field: "FLR_NAME", width: 150, cellClass: 'grid-align' },
            { headerName: "Space Type", field: "SPC_TYPE", width: 100, cellClass: 'grid-align' },
            { headerName: "Total Seats", field: "TOTAL_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Total Allocated Seats", field: "ALLOCATED_SEATS", width: 180, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Vacant Seats", field: "VACANT_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Employee Shared Seats", field: "EMPLOYEE_OCCUPIED_SEAT_COUNT", width: 130, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Allocated But Not Occupied Seats", field: "ALLOCATED_VACANT", width: 250, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Blocked Seats", field: "BLOCKED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Requested Seats", field: "REQUESTED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true }
        ];

        $scope.pageSize = '10';


        $scope.gridOptions = {
            columnDefs: columnDefs,
            rowData: [],
            //enableFilter: true,
            enableSorting: true,
            angularCompileRows: true,
            enableCellSelection: false,
            enableColResize: true,
            groupAggFunction: groupAggFunction,
            groupHideGroupColumns: true,
            groupColumnDef: {
                headerName: "Country", field: "CNY_NAME",
                cellRenderer: {
                    renderer: "group"
                }

            },
            onAfterFilterChanged: function () {
                if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                    $scope.DocTypeVisible = 0;
                else
                    $scope.DocTypeVisible = 1;
            }
        };




        function groupAggFunction(rows) {

            var sums = {
                ALLOCATED_SEATS: 0,
                OCCUPIED_SEATS: 0,
                OCCUPIED_SHARED_SEATS: 0,
                EMPLOYEE_OCCUPIED_SEAT_COUNT: 0,
                ALLOCATED_VACANT: 0,
                VACANT_SEATS: 0,
                TOTAL_SEATS: 0,
                REQUESTED_SEATS: 0,
                BLOCKED_SEATS: 0,
                SU: 0
            };

            rows.forEach(function (row) {
                var data = row.data;
                sums.ALLOCATED_SEATS += parseInt(data.ALLOCATED_SEATS);
                sums.OCCUPIED_SEATS += parseInt(data.OCCUPIED_SEATS);
                sums.EMPLOYEE_OCCUPIED_SEAT_COUNT += parseInt(data.EMPLOYEE_OCCUPIED_SEAT_COUNT);
                sums.ALLOCATED_VACANT += parseInt(data.ALLOCATED_VACANT);
                sums.VACANT_SEATS += parseInt(data.VACANT_SEATS);
                sums.TOTAL_SEATS += parseInt(data.TOTAL_SEATS);
                sums.REQUESTED_SEATS += parseInt(data.REQUESTED_SEATS);
                sums.BLOCKED_SEATS += parseInt(data.BLOCKED_SEATS);
                sums.SU += parseFloat(data.SU);
            });
            return sums;
        };


        function onFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
            if (value) {
                $scope.DocTypeVisible = 1
            }
            else { $scope.DocTypeVisible = 0 }
        }

        $("#filtertxt").change(function () {
            onFilterChanged($(this).val());
        }).keydown(function () {
            onFilterChanged($(this).val());
        }).keyup(function () {
            onFilterChanged($(this).val());
        }).bind('paste', function () {
            onFilterChanged($(this).val());
        });

        function onFilterChanged2(value) {
            $scope.Options.api.setQuickFilter(value);
            if (value) {
                $scope.DocTypeVisible = 1
            }
            else { $scope.DocTypeVisible = 0 }
        }

        $("#filteredproj").change(function () {
            onFilterChanged2($(this).val());
        }).keydown(function () {
            onFilterChanged2($(this).val());
        }).keyup(function () {
            onFilterChanged2($(this).val());
        }).bind('onFilterChanged1', function () {
            onFilterChanged2($(this).val());
        });

        function onFilterChanged1(value) {
            $scope.gridLocation.api.setQuickFilter(value);
            if (value) {
                $scope.DocTypeVisible = 1
            }
            else { $scope.DocTypeVisible = 0 }
        }

        $("#filteredloc").change(function () {
            onFilterChanged1($(this).val());
        }).keydown(function () {
            onFilterChanged1($(this).val());
        }).keyup(function () {
            onFilterChanged1($(this).val());
        }).bind('paste', function () {
            onFilterChanged1($(this).val());
        });

        $("#Tabular").fadeIn();
        $("#Graphicaldiv").fadeOut();
        $("#table1").fadeIn();

        $scope.chart;
        $scope.chart = c3.generate({
            data: {
                x: "ALLOCSTA",
                columns: [],
                cache: false,
                type: 'bar',
                empty: { label: { text: "Sorry, No Data Found" } },
            },
            legend: {
                position: 'top'
            },
            axis: {
                x: {
                    type: 'category',
                    height: 130,
                    show: true,
                    tick: {
                        rotate: 75,
                        multiline: false
                    }
                },
                y: {
                    show: true,
                    label: {
                        text: 'Count',
                        position: 'outer-middle'
                    }
                }
            },
            width:
            {
                ratio: 0.5
            }
        });



        function tooltip_contents(d, defaultTitleFormat, defaultValueFormat, color) {
            var $$ = this, config = $$.config, CLASS = $$.CLASS,
                titleFormat = config.tooltip_format_title || defaultTitleFormat,
                nameFormat = config.tooltip_format_name || function (name) { return name; },
                valueFormat = config.tooltip_format_value || defaultValueFormat,
                text, i, title, value, name, bgcolor;

            for (i = 0; i < d.length; i++) {
                if (!(d[i] && (d[i].value || d[i].value === 0))) { continue; }

                if (!text) {
                    text = "<table class='" + CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
                }

                name = nameFormat(d[i].name);
                value = d[i].value;
                bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

                text += "<tr class='" + CLASS.tooltipName + "-" + d[i].id + "'>";
                text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
                text += "<td class='value'>" + value + "</td>";
                text += "</tr>";
            }
            return text + "</table>";
        }



        $scope.SpaceConsolidatedChart = function () {

            $.ajax({
                url: UtilityService.path + '/api/SpaceConsolidatedReport/SpaceConsolidatedChart',
                type: 'POST',
                data: '',
                success: function (result) {
                    var chart = c3.generate({
                        data: {
                            x: 'x',
                            columns: [
                                result.Locations,
                                result.Occupied,
                                result.Vacant,
                            ],
                            type: 'bar',
                            types: {
                                'Vacant Count': 'line',
                            },
                            labels: true,

                        },
                        axis: {
                            x: {
                                type: 'category',
                                tick: {
                                    rotate: 65,
                                    multiline: false
                                },
                                height: 100
                            }
                        },
                        bindto: '#SpaceConsolidatedGraph'
                    });
                }
            });
        }


        $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                $("#Graphicaldiv").fadeOut(function () {
                    $("#Tabular").fadeIn();
                    $("#table1").fadeIn();
                });
            }
            else {
                $("#Tabular").fadeOut(function () {
                    $("#Graphicaldiv").fadeIn();
                    $("#table1").fadeOut();
                });
            }

        });

        //var Coldefrrr = [
        //    { headerName: "Vertical", field: "Vertical", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, suppressMenu: true },
        //    { headerName: "CostCenter", field: "CostCenter", width: 100, cellClass: 'grid-align', rowGroupIndex: 1, hide: true, suppressMenu: true },
        //    { headerName: "Floor", field: "Floor", width: 100, cellClass: 'grid-align' },
        //    { headerName: "Building", field: "Building", width: 100, cellClass: 'grid-align' },
        //    { headerName: "Encloser", field: "Encloser", width: 100, cellClass: 'grid-align', suppressMenu: true },
        //    { headerName: "Allocated to Vertical", field: "Allocated_to_Vertical", width: 100, cellClass: 'grid-align' },
        //    { headerName: " If Encloser/Vacant", field: "If_Encloser_Vacant", width: 100, cellClass: 'grid-align' },
        //    { headerName: "Total", field: "Total", width: 100, cellClass: 'grid-align' }
        //];

        var columnDefs1 = [
            { headerName: "Building", field: "TWR_NAME", width: 150, cellClass: 'grid-align', },
            { headerName: "Floor", field: "FLR_NAME", width: 150, cellClass: 'grid-align' },
            { headerName: "Space Type", field: "SPC_TYPE_NAME", width: 150, cellClass: 'grid-align' },
            { headerName: "Seating Capacity", field: "TOTAL_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Teams", field: "COST_CENTER_NAME", width: 310, cellClass: 'grid-align' },
            { headerName: "Seats Occupied", field: "OCCUPIED_SEATS", width: 150, cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Surplus", field: "VACANT_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        ];

        $scope.Options = {
            columnDefs: columnDefs1,
            rowData: [],
            enableFilter: true,
            enableSorting: true,
            angularCompileRows: true,
            enableCellSelection: false,
            enableColResize: true,
            groupAggFunction: groupAggFunction,
            groupHideGroupColumns: true,

            onAfterFilterChanged: function () {
                if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                    $scope.DocTypeVisible = 0;
                else
                    $scope.DocTypeVisible = 1;
            }
        };
        //$scope.Options = {
        //    columnDefs: Coldefrrr,
        //    rowData: [],
        //    enableSorting: true,
        //    enableFilter: true,
        //    angularCompileRows: true,
        //    enableCellSelection: false,
        //    enableColResize: true,
        //    groupAggFunction: groupAggFunction1,
        //    groupHideGroupColumns: true,
        //    groupColumnDef: {
        //        headerName: "Vertical", field: "Vertical",
        //        cellRenderer: {
        //            renderer: "group"
        //        }
        //    },
        //    onAfterFilterChanged: function () {
        //        if (angular.equals({}, $scope.gridOptions1.api.getFilterModel()))
        //            $scope.DocTypeVisible = 0;
        //        else
        //            $scope.DocTypeVisible = 1;
        //    }
        //};

        //function groupAggFunction1(rows) {
        //    var sums = {
        //        //Floor: 0,
        //        //Building: 0,
        //        //Encloser: 0,
        //        Allocated_to_Vertical: 0,
        //        If_Encloser_Vacant: 0,
        //        Total: 0
        //    };
        //    rows.forEach(function (row) {
        //        var data = row.data;
        //        //sums.Floor = data.Floor;
        //        //sums.Building = data.Building;
        //        //sums.Encloser = data.Encloser;
        //        sums.Allocated_to_Vertical += parseFloat((data.Allocated_to_Vertical));
        //        sums.If_Encloser_Vacant += parseFloat((data.If_Encloser_Vacant));
        //        sums.Total += parseFloat((data.Total));
        //    });
        //    return sums;
        //}

        //$scope.Options = {
        //    columnDefs: Coldefrrr,
        //    rowData: [],
        //    enableColResize: true,
        //    enableCellSelection: false,
        //    enableFilter: true,
        //    enableSorting: true,
        //    enableScrollbars: false,
        //    angularCompileRows: true,
        //    groupHideGroupColumns: true,
        //    suppressHorizontalScroll: false,
        //    enableCellSelection: false,
        //    groupDefaultExpanded: 2,
        //    groupColumnDef: {
        //        headerName: "Vertical", field: "VER_NAME",
        //        cellRenderer: {
        //            renderer: "group"
        //        }
        //    },

        //};

        $scope.BindGrid_Project = function () {
            var params = {
                CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID,
                LCM_NAME: _.filter($scope.Consolidated.LCM_NAME, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                VER_CODE: _.filter($scope.SpaceConsolidatedReport.VER_NAME, function (o) { return o.ticked == true; }).map(function (x) { return x.VER_CODE; }).join(','),
                Cost_Center_Code: _.filter($scope.SpaceConsolidatedReport.Cost_Center_Name, function (o) { return o.ticked == true; }).map(function (x) { return x.COST_CENTER_CODE; }).join(','),
                month: _.filter($scope.SpaceConsolidatedReport.Month, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(','),
                year: _.filter($scope.SpaceConsolidatedReport.getyears, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(','),
            };

            $scope.ChartParam = params;
            progress(0, 'Loading...', true);
            SpaceConsolidatedReportService.GetSummary(params).then(function (response) {
                $scope.GridVisiblity = false;
                $scope.GridVisiblitylocation = false;
                $scope.GridVisiblityproject = true;
                ExportColumns = response.exportCols;
                //$scope.Options.api.setColumnDefs(response.Coldef);
                $scope.gridata1 = response.griddata;
                //$scope.BindGrid();

                //if ($scope.gridata1 == null) {
                //    $scope.GridVisiblity = false;
                //    $scope.GridVisiblitylocation = false;
                //    $scope.GridVisiblityproject = false;
                //    $scope.GridVisiblitymancom = false;
                //    progress(0, 'Loading...', false);
                //    $scope.Options.api.setRowData([]);
                //}
                //else {
                //    $scope.GridVisiblity = false;
                //    $scope.GridVisiblitylocation = false;
                //    $scope.GridVisiblityproject = true;
                //    $scope.GridVisiblitymancom = false;
                //    $scope.Options.api.setRowData($scope.gridata1);
                //    progress(0, 'Loading...', false);
                //}
                if (response != null) {
                    $scope.Options.api.setRowData(response.griddata);
                }
                else {
                    $scope.Options.api.setRowData([]);
                }
                progress(0, 'Loading...', false);

            }, function (error) {
                console.log(error);
            });
        };



        //$scope.GenReportproject = function (Type) {
        //    debugger;
        //    saobj = {};
        //    saobj.CNP_NAME = $scope.Consolidated.CNP_NAME[0].CNP_ID;
        //    saobj.DocType = Type;
        //    $scope.rptArea.DocType = Type;
        //    if ($scope.Options.api.isAnyFilterPresent($scope.columnDefs)) {
        //        progress(0, 'Loading...', true);
        //        if ($scope.rptArea.DocType == "pdf") {
        //            $scope.GenerateFilterPrjPdf();
        //        }
        //        else {
        //            $scope.GenerateFilterPrjExcel();
        //        }
        //    }
        //    else {
        //        progress(0, 'Loading...', true);
        //        $http({
        //            url: UtilityService.path + '/api/SpaceConsolidatedReport/Export_SpaceConsolidatedPrjRpt',
        //            method: 'POST',
        //            data: saobj,
        //            responseType: 'arraybuffer'

        //        }).success(function (data, status, headers, config) {
        //            var file = new Blob([data], {
        //                type: 'application/' + Type
        //            });
        //            var fileURL = URL.createObjectURL(file);

        //            $("#reportcontainer").attr("src", fileURL);
        //            var a = document.createElement('a');
        //            a.href = fileURL;
        //            a.target = '_blank';
        //            a.download = 'SpaceConsolidatedProjectReport.' + Type;
        //            document.body.appendChild(a);
        //            a.click();
        //            setTimeout(function () {
        //                progress(0, 'Loading...', false);
        //            }, 1000)
        //        }).error(function (data, status, headers, config) {

        //        });
        //    }
        //};

        $scope.GenerateFilterPrjExcel = function () {

            progress(0, 'Loading...', true);
            //var Filterparams = {
            //    skipHeader: false,
            //    skipFooters: false,
            //    skipGroups: false,
            //    allColumns: false,
            //    onlySelected: false,
            //    columnSeparator: ',',
            //    fileName: "SpaceConsolidatedReport.csv"
            //};
            //$scope.gridOptions.api.exportDataAsCsv(Filterparams);
            //setTimeout(function () {
            //    progress(0, 'Loading...', false);
            var mapvalues = [];
            var columns = "";
            _.forEach($scope.Options.api.columnController.allColumns, function (value) {

                columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

            });
            mapvalues.push(_.map($scope.Options.api.inMemoryRowController.rowsAfterFilter, 'data'));
            alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceConsolidatedPrjReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
            //}, 1000);
        }


        $scope.GenerateFilterPdf = function () {
            progress(0, 'Loading...', true);
            var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Total Allocated Seats", key: "ALLOCATED_SEATS" }, { title: "Occupied Seats", key: "OCCUPIED_SEATS" }, { title: "Employee Shared Seats", key: "EMPLOYEE_OCCUPIED_SEAT_COUNT" }, { title: "Allocated But Not Occupied", key: "ALLOCATED_VACANT" }, { title: "Vacant Seats", key: "VACANT_SEATS" }, { title: "Total Seats", key: "TOTAL_SEATS" }];
            var model = $scope.gridOptions.api.getModel();
            var data = [];
            model.forEachNodeAfterFilter(function (node) {
                data.push(node.data);
            });
            var jsondata = JSON.parse(JSON.stringify(data));
            console.log(jsondata);
            var doc = new jsPDF('p', 'pt', 'A4');
            doc.autoTable(columns, jsondata, {
                startY: 43,
                margin: { horizontal: 7 },
                styles: { overflow: 'linebreak', columnWidth: 'auto' },
                columnStyles: { 0: { columnWidth: '15%' } }
            });
            doc.save("SpaceConsolidatedReport.pdf");
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);
            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "Summary_Report.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
            //var Filterparams = {
            //    skipHeader: false,
            //    skipFooters: false,
            //    skipGroups: false,
            //    allColumns: false,
            //    onlySelected: false,
            //    columnSeparator: ',',
            //    fileName: "SpaceConsolidatedReport.csv"
            //};
            //$scope.gridOptions.api.exportDataAsCsv(Filterparams);
            //setTimeout(function () {
            //    progress(0, 'Loading...', false);
            //var mapvalues = [];
            //var columns = "";
            //_.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            //    columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

            //});
            //mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
            //alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceConsolidatedReport.xlsx",{headers:true}) \
            //        FROM ?', JSON.parse(JSON.stringify(mapvalues)));
            //setTimeout(function () {
            //    progress(0, 'Loading...', false);
            //}, 1000);
            //}, 1000);
        }


        $scope.GenReport = function (Type) {
            saobj = {};
            var searchval = $("#filtertxt").val();
            saobj.SearchValue = searchval,
                saobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                saobj.PageSize = $scope.gridata[0].OVERALL_COUNT,
                saobj.CNP_NAME = $scope.Consolidated.CNP_NAME[0].CNP_ID;
            saobj.DocType = Type;
            saobj.FromDate = $scope.Consolidated.FromDate;
            //saobj.VERTICAL = $scope.BsmDet.Parent;
            //saobj.COSTCENTER = $scope.BsmDet.Child;
            console.log(saobj);
            $scope.rptArea.DocType = Type;
            if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
                progress(0, 'Loading...', true);
                if ($scope.rptArea.DocType == "pdf") {
                    $scope.GenerateFilterPdf();
                }
                else {
                    $scope.GenerateFilterExcel();
                }
            }
            else {
                progress(0, 'Loading...', true);
                //$http({
                //    url: UtilityService.path + '/api/SpaceConsolidatedReport/Export_SpaceConsolidatedRpt',
                //    method: 'POST',
                //    data: saobj,
                //    responseType: 'arraybuffer'

                //}).success(function (data, status, headers, config) {
                //    var file = new Blob([data], {
                //        type: 'application/' + Type
                //    });
                //    var fileURL = URL.createObjectURL(file);

                //    $("#reportcontainer").attr("src", fileURL);
                //    var a = document.createElement('a');
                //    a.href = fileURL;
                //    a.target = '_blank';
                //    a.download = 'SpaceConsolidatedReport.' + Type;
                //    document.body.appendChild(a);
                //    a.click();
                //    setTimeout(function () {
                //        progress(0, 'Loading...', false);
                //    }, 1000)
                //}).error(function (data, status, headers, config) {

                //});

                $scope.GenerateFilterExcel();
            }
        };


        $scope.GenReportlocation = function (Type) {
            saobj = {};
            var searchval = $("#filteredloc").val();
            saobj.SearchValue = searchval,
                saobj.PageNumber = $scope.gridLocation.api.grid.paginationController.currentPage + 1,
                saobj.PageSize = $scope.gridata[0].OVERALL_COUNT,
                saobj.CNP_NAME = $scope.Consolidated.CNP_NAME[0].CNP_ID;
            saobj.DocType = Type;
            saobj.FromDate = $scope.Consolidated.FromDate;
            //saobj.VERTICAL = $scope.BsmDet.Parent;
            //saobj.COSTCENTER = $scope.BsmDet.Child;
            console.log(saobj);
            $scope.rptareaprj.DocType = Type;
            if ($scope.gridLocation.api.isAnyFilterPresent($scope.columnDefsproject)) {
                progress(0, 'Loading...', true);
                if ($scope.rptareaprj.DocType == "pdf") {
                    $scope.GenerateFilterLocationPdf();
                }
                else {
                    $scope.GenerateFilterLocationExcel();
                }
            }
            else {
                progress(0, 'Loading...', true);
                $http({
                    url: UtilityService.path + '/api/SpaceConsolidatedReport/Export_SpaceConsolidatedLocationRpt',
                    method: 'POST',
                    data: saobj,
                    responseType: 'arraybuffer'

                }).success(function (data, status, headers, config) {
                    var file = new Blob([data], {
                        type: 'application/' + Type
                    });
                    var fileURL = URL.createObjectURL(file);

                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'SpaceConsolidatedLocationReport.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000)
                }).error(function (data, status, headers, config) {

                });
            }
        };


        $scope.GenerateFilterLocationPdf = function () {
            progress(0, 'Loading...', true);
            var columns = [{ title: "CITY", key: "CTY_NAME" }, { title: "LOCATION", key: "LCM_NAME" }, { title: "TOTAL SEATS", key: "TOTAL_SEATS" }, { title: "VACANT SEATS", key: "VACANT_SEATS" }, { title: "OCCUPIED SEATS", key: "OCCUPIED_VACANT" }];
            var model = $scope.gridLocation.api.getModel();
            var data = [];
            model.forEachNodeAfterFilter(function (node) {
                data.push(node.data);
            });
            var jsondata = JSON.parse(JSON.stringify(data));
            console.log(jsondata);
            var doc = new jsPDF('p', 'pt', 'A4');
            doc.autoTable(columns, jsondata, {
                startY: 43,
                margin: { horizontal: 7 },
                styles: { overflow: 'linebreak', columnWidth: 'auto' },
                columnStyles: { 0: { columnWidth: '15%' } }
            });
            doc.save("SpaceConsolidatedLocationReport.pdf");
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }


        $scope.GenerateFilterLocationExcel = function () {
            progress(0, 'Loading...', true);
            var mapvalues = [];
            var columns = "";
            _.forEach($scope.gridLocation.api.columnController.allColumns, function (value) {

                columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

            });
            mapvalues.push(_.map($scope.gridLocation.api.inMemoryRowController.rowsAfterFilter, 'data'));
            alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceConsolidatedlocationReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);

        }


        $scope.GenerateFilterMancomPdf = function () {
            progress(0, 'Loading...', true);
            var columns = [{ title: "Mancom", key: "MANCOM" }, { title: "Space Type", key: "SPC_TYPE" }, { title: "Total Seats", key: "TOTAL_SEATS" }, { title: "Occupied Seats", key: "OCCUPIED_SEATS" }, { title: "Allocated Vacant Seats", key: "ALLOCATED_VACANT_SEATS" }, { title: "Vacant", key: "VACANT" }];
            var model = $scope.gridmancom.api.getModel();
            var data = [];
            model.forEachNodeAfterFilter(function (node) {
                data.push(node.data);
            });
            var jsondata = JSON.parse(JSON.stringify(data));
            console.log(jsondata);
            var doc = new jsPDF('p', 'pt', 'A4');
            doc.autoTable(columns, jsondata, {
                startY: 43,
                margin: { horizontal: 7 },
                styles: { overflow: 'linebreak', columnWidth: 'auto' },
                columnStyles: { 0: { columnWidth: '15%' } }
            });
            doc.save("SpaceConsolidatedMancomReport.pdf");
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenerateFilterExcelMancom = function () {
            progress(0, 'Loading...', true);
            var mapvalues = [];
            var columns = "";
            _.forEach($scope.gridmancom.api.columnController.allColumns, function (value) {

                columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

            });
            mapvalues.push(_.map($scope.gridmancom.api.inMemoryRowController.rowsAfterFilter, 'data'));
            alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceConsolidatedMancomReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
            //}, 1000);
        }

        $scope.GenReportmancom = function (Type) {
            saobj = {};
            var searchval = $("#filtertxt").val();
            saobj.SearchValue = searchval,
                saobj.PageNumber = $scope.gridmancom.api.grid.paginationController.currentPage + 1,
                saobj.PageSize = $scope.gridata2[0].OVERALL_COUNT,
                saobj.CNP_NAME = $scope.Consolidated.CNP_NAME[0].CNP_ID;
            saobj.DocType = Type;
            saobj.FromDate = $scope.Consolidated.FromDate;
            //saobj.VERTICAL = $scope.BsmDet.Parent;
            //saobj.COSTCENTER = $scope.BsmDet.Child;
            console.log(saobj);
            $scope.rptArea.DocType = Type;
            if ($scope.gridmancom.api.isAnyFilterPresent($scope.columnDefsMancom)) {
                progress(0, 'Loading...', true);
                if ($scope.rptArea.DocType == "pdf") {
                    $scope.GenerateFilterMancomPdf();
                }
                else {
                    $scope.GenerateFilterExcelMancom();
                }
            }
            else {
                progress(0, 'Loading...', true);
                $http({
                    url: UtilityService.path + '/api/SpaceConsolidatedReport/Export_SpaceConsolidatedMancomRpt',
                    method: 'POST',
                    data: saobj,
                    responseType: 'arraybuffer'

                }).success(function (data, status, headers, config) {
                    var file = new Blob([data], {
                        type: 'application/' + Type
                    });
                    var fileURL = URL.createObjectURL(file);

                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'SpaceConsolidatedMancomReport.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000)
                }).error(function (data, status, headers, config) {

                });
            }
        };
        //$scope.GenerateFilterExcels = function () {

        //    progress(0, 'Loading...', true);
        //    var Filterparams = {

        //        columnGroups: true,
        //        allColumns: true,
        //        onlySelected: false,
        //        columnSeparator: ',',
        //        fileName: "Project_Report.csv"
        //    };
        //    $scope.Options.api.exportDataAsCsv(Filterparams);
        //    setTimeout(function () {
        //        progress(0, 'Loading...', false);
        //    }, 1000);
        //}



        $scope.GenerateFilterExcels = (Type) => {
            var gridData = [];

            // Collecting grid data
            $scope.Options.api.forEachNode(function (rowNode) {
                gridData.push(rowNode.data);
            });

            var workbook = new ExcelJS.Workbook();
            var worksheet = workbook.addWorksheet("Report");

            // Get header names and their corresponding fields
            const headerNames = $scope.Options.columnDefs.map(col => col.headerName);
            const fields = $scope.Options.columnDefs.map(col => col.field);

            // Set header row
            const headerRow = worksheet.addRow(headerNames);
            headerRow.eachCell((cell) => {
                cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FF387cb4' }
                };
                cell.font = { color: { argb: 'FFFFFFFF' } };
            });


            const columnWidths = new Array(headerNames.length).fill(0);

            gridData.forEach(dataRow => {

                const rowValues = headerNames.map((header, index) => {
                    const columnDef = $scope.Options.columnDefs.find(col => col.headerName === header);
                    const value = columnDef ? dataRow[columnDef.field] : '';


                    columnWidths[index] = Math.max(columnWidths[index], value ? value.toString().length : 0);
                    return value;
                });
                worksheet.addRow(rowValues);
            });


            columnWidths.forEach((width, index) => {
                worksheet.getColumn(index + 1).width = width + 2;
            });

            workbook.xlsx.writeBuffer().then(function (buffer) {
                var file = new Blob([buffer], {
                    type: 'application/' + Type
                });

                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Project_Report.xlsx';
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                progress(0, 'Loading...', false);
            });
        };


        $scope.GenReportproject = function (Type) {

            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcels();
        }


        $scope.getCompany = function () {
            $scope.getLocation('A');
        }

        $scope.getLocation = function (typ) {
            if (typ == 'A') {
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = false;
                });
            }
            $scope.GetVerticals('A');
        }


        $scope.GetVerticals = function (typ) {
            if (typ == 'A') {
                angular.forEach($scope.Vertical, function (value, key) {
                    value.ticked = false;
                });
            }
            $scope.GetCostcenter('A');
        }

        $scope.GetCostcenter = function (typ) {
            if (typ == 'A') {
                angular.forEach($scope.CostCenter, function (value, key) {
                    value.ticked = false;
                });
            }
        }
        $scope.GetCostCenterByVerticals = function () {
            UtilityService.getCostcenterByVertical($scope.SpaceConsolidatedReport.VER_NAME, 2).then(function (response) {
                if (response.data != null)
                    $scope.CostCenter = response.data;

            }, function (error) {
                console.log(error);
            });
        }

        $scope.GetCostCenterByVerticals = function () {
            UtilityService.getCostcenterByVertical($scope.SpaceConsolidatedReport.VER_NAME, 2).then(function (response) {
                if (response.data != null)
                    $scope.CostCenter = response.data;

            }, function (error) {
                console.log(error);
            });
        }

    }]);

