﻿app.service('SeatQRCodeService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SeatQRCode/GetGriddata', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

   

}]);


app.controller('SeatQRCodeController', ['$scope', 'UtilityService', 'SeatQRCodeService', function ($scope, UtilityService, SeatQRCodeService) {
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.LoadInfo = [];
    $scope.SeatQRCode = {};
    $scope.SeatQRCode.Type = 1;
    $scope.PDFVisiblity = false;



    $scope.SpaceClicked = function () {
        $scope.SeatQRCode.Type = 1;
        $scope.PDFVisiblity = false;
        $scope.gridOptions.api.setColumnDefs([]);
        $scope.gridOptions.api.setRowData([]);
        $scope.LoadInfo = [];

    }
    $scope.FloorClicked = function () {
        $scope.SeatQRCode.Type = 2;
        $scope.PDFVisiblity = false;
        $scope.gridOptions.api.setColumnDefs([]);
        $scope.gridOptions.api.setRowData([]);
        $scope.LoadInfo = [];

    }

    $scope.GenReport = function (data) {
        if (data == 'pdf') {
            const CodeVales = [];
            if ($scope.SeatQRCode.Type == 1) {
                angular.forEach($scope.LoadInfo, function (val) {
                    CodeVales.push({ "AAT_CODE": val.SPC_ID, "AAT_NAME": val.SPC_ID });

                });

            } else {
                angular.forEach($scope.LoadInfo, function (val) {
                    CodeVales.push({ "AAT_NAME": val.FLR_CODE, "AAT_CODE": val.TWR_CODE + ', ' + val.FLR_CODE + ', ' + Math.floor((Math.random() * 9999) + 1).toString() });

                });
            }
            qr_generate(CodeVales);
        }

    }

    $scope.Load = function () {
        progress(0, 'Loading...', true);
        var params = { flrlst: $scope.SpaceQR.Floor, Type: $scope.SeatQRCode.Type };
        SeatQRCodeService.GetGriddata(params).then(function (response) {
            if (response.data != null) {
                $scope.LoadInfo = [];
                if ($scope.SeatQRCode.Type == 1) {
                    $scope.LoadInfo = response.data;
                    $scope.PDFVisiblity = true;
                    //$scope.gridOptions.api.setColumnDefs();
                    $scope.gridOptions.api.setColumnDefs(columnDefsSpace);
                    $scope.gridOptions.api.setRowData(response.data);
                    $scope.gridOptions.api.sizeColumnsToFit();

                } else {
                    $scope.LoadInfo = response.data;
                    //$scope.gridOptions.api.setColumnDefs();
                    $scope.PDFVisiblity = true;
                    $scope.gridOptions.api.setColumnDefs(columnDefsFloor);
                    $scope.gridOptions.api.setRowData(response.data);
                    $scope.gridOptions.api.sizeColumnsToFit();
                }
                
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
           

        }, function (error) {
            console.log(error);
        });
    }

    function SpaceQR(params) {

        var elementt = document.createElement("span");
        var DivElementt = document.createElement("div");
        $(DivElementt).qrcode(
            {
                width: 70,
                height: 70,
                text: params.data.SPC_ID
            });
        elementt.appendChild(DivElementt);
        //elementt.appendChild(document.createTextNode(params.value));
        return elementt;
    }

    function FloorQR(params) {
        var element = document.createElement("span");
        var DivElement = document.createElement("div");
        $(DivElement).qrcode(
            {
                width: 120,
                height: 120,
                text: params.data.TWR_CODE + ', ' + params.data.FLR_CODE + ', ' + Math.floor((Math.random() * 9999) + 1).toString()

            });
        element.appendChild(DivElement);
        //element.appendChild(document.createTextNode(params.value));
        return element;
    }

    var columnDefsSpace = [
        { headerName: 'SpaceId', field: 'SPC_ID', width: 150, height: 150 },
        { headerName: 'Location', field: 'LCM_NAME', width: 150, height: 150 },
        { headerName: 'Tower', field: 'TWR_NAME', width: 150, height: 150 },
        { headerName: 'Floor', field: 'FLR_NAME', width: 150, height: 150 },
        { headerName: 'QR Code', field: 'QrCode', width: 150, height: 150, cellRenderer: SpaceQR }
    ];

    var columnDefsFloor = [
        { headerName: 'City', field: 'CTY_NAME', width: 150, height: 150 },
        { headerName: 'Location', field: 'LCM_NAME', width: 150, height: 150 },
        { headerName: 'Tower', field: 'TWR_NAME', width: 150, height: 150 },
        { headerName: 'Tower code', field: 'TWR_CODE', width: 150, height: 150, hide: true },
        { headerName: 'Floor', field: 'FLR_NAME', width: 150, height: 150 },
        { headerName: 'Floor Code', field: 'FLR_CODE', width: 150, height: 150,hide:true },
        { headerName: 'QR Code', field: 'QrCode', width: 150, height: 150, cellRenderer: FloorQR }
    ];


    $scope.gridOptions = {
        columnDefs: [],
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        enableColResize: true,
        rowHeight: 80
    };



   

    $scope.LcmChangeAll = function () {
        $scope.SpaceQR.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.SpaceQR.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {

        $scope.SpaceQR.Country = [];
        $scope.SpaceQR.City = [];
        UtilityService.getTowerByLocation($scope.SpaceQR.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SpaceQR.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SpaceQR.City.push(cty);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SpaceQR.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.SpaceQR.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        $scope.SpaceQR.Country = [];
        $scope.SpaceQR.City = [];
        $scope.SpaceQR.Location = [];

        UtilityService.getFloorByTower($scope.SpaceQR.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SpaceQR.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SpaceQR.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.SpaceQR.Location.push(lcm);
            }
        });



    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SpaceQR.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.SpaceQR.Floor = [];
        $scope.FlrChanged();
    }

    $scope.FlrChanged = function () {
        $scope.SpaceQR.Country = [];
        $scope.SpaceQR.City = [];
        $scope.SpaceQR.Location = [];
        $scope.SpaceQR.Tower = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SpaceQR.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SpaceQR.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.SpaceQR.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (twr != undefined && value.ticked == true && twr.ticked == false) {
                twr.ticked = true;
                $scope.SpaceQR.Tower.push(twr);
            }
        });

    }


    
  UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;
                            UtilityService.getTowers(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Tower = response.data;

                                    UtilityService.getFloors(2).then(function (response) {
                                        if (response.data != null) {
                                            $scope.Floor = response.data;
                                        }
                                    });

                                }
                            });
                           
                        }
  });

    $scope.Clear = function () {
        $scope.SpaceQR = {};
        
        angular.forEach($scope.Location, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floor, function (floor) {
            floor.ticked = false;
        });
        $scope.SpaceQR.Location = [];
        $scope.SpaceQR.Tower = [];
        $scope.SpaceQR.Floor = [];
        $scope.PDFVisiblity = false;
        $scope.QRCodeGenerator.$submitted = false;
        $scope.gridOptions.api.setColumnDefs([]);
        $scope.gridOptions.api.setRowData([]);
    }
                
}]);