﻿app.service("StateWiseAllocationReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/GetCustomizedDetails', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.StateByCny = function (ID) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/GetStatesBycountry?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.CityByState = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/CityByState?id= ' + data + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.VerticalByFloor = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/GetVerticalByFloor?id=' + data + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.CostcenterByVertical = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/GetCostCenterByVertical?id=' + data + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetCitiesByState = function (stylst, ID) {
        var deferred = $q.defer();
        $http.post(UtilityService.path + '/api/StateWiseAllocationReport/getCitiesByState?id=' + ID + '', stylst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetCostcenterByMancom = function (vertical, ID) {
        var deferred = $q.defer();
        $http.post(UtilityService.path + '/api/StateWiseAllocationReport/GetCostcenterByMancom?id=' + ID + '', vertical)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.BindGrid = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/BindGrid', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetProjectSummary = function (Params) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/GetProjectSummary', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SpaceConsolidatedChart = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/SpaceConsolidatedChart')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetMancom = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/GetMancom', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.BindGridSummary = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/StateWiseAllocationReport/BindGridSummary', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);
app.controller('StateWiseAllocationReportController', ['$scope', '$q', '$http', 'StateWiseAllocationReportService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, StateWiseAllocationReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Customized.CNP_NAME = [];
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.Customized.CHE_NAME = [];
    $scope.rptArea = {};
    $scope.rptareaprj = {};
    $scope.ProjectReport = {};

    $scope.ColumnNames = [];
    $scope.Customized.Request_Type = "All";
    $scope.selVal = "30";
    $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
    $scope.Customized.ToDate = moment().format('MM/DD/YYYY');

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {

            $scope.BsmDet = response.data;
            $scope.Cols[11].COL = $scope.BsmDet.Parent;
            $scope.Cols[12].COL = $scope.BsmDet.Child;

            $scope.statuslst = [
                { Code: "All", Name: "All" },
                { Code: "1", Name: "Vacant" },
                { Code: "1002", Name: "Allocated To " + $scope.BsmDet.Parent },
                { Code: "1003", Name: "Allocated To " + $scope.BsmDet.Child },
                { Code: "1004", Name: "Occupied By Employee" },
                { Code: "1052", Name: "Blocked" }
            ];

        }
    });

    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;

            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                a.ticked = true;
                $scope.Customized.CNP_NAME.push(a);
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }

    });

    UtilityService.getChildEntity(parseInt(CompanySession)).then(function (response) {
        if (response.data != null) {
            $scope.Entity = response.data;

            angular.forEach($scope.Entity, function (value, key) {
                //var a = _.find($scope.Entity, { CHE_CODE: parseInt(CompanySession) });
                value.ticked = true;
                //$scope.Customized.CHE_NAME.push(a);
            });
            //if (CompanySession == "1") { $scope.EnableStatus = 1; }
            //else { $scope.EnableStatus = 0; }
        }

    });

    $scope.LoadData = function () {
        if ($scope.GridVisiblity)
            $scope.BindGrid_Summary();

        if ($scope.GridVisiblityproject)
            $scope.BindGrid_Project();

        if ($scope.GridVisiblitylocation)
            $scope.BindGrid_Location();

        if ($scope.GridVisiblitymancom)
            $scope.BindGrid_Mancom();

        if ($scope.GridConsolidated)
            $scope.Consolidated();
    }

    $scope.Consolidated = function () {
        progress(0, 'Loading...', true);
        $scope.GridVisiblity = false;
        $scope.GridVisiblityproject = false;
        $scope.GridVisiblitylocation = false;
        $scope.GridVisiblitymancom = false;
        $scope.GridConsolidated = false;
        verlst = _.filter($scope.Customized.Verticals, function (o) { return o.ticked == true; }).map(function (x) { return x.VER_CODE; }).join(',');
        costlist = _.filter($scope.Customized.CostCenters, function (o) { return o.ticked == true; }).map(function (x) { return x.Cost_Center_Code; }).join(',');

        var dataObj = {
            flrlst: $scope.Customized.Floors,
            Request_Type: $scope.Customized.Request_Type,
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            enylist: $scope.Customized.CHE_NAME,
            VERTICAL: verlst,
            COSTCENTER: costlist,
            Type: $scope.flag

        };
        var fromdate = moment($scope.Customized.FromDate);
        var todate = moment($scope.Customized.ToDate);
        if (fromdate > todate) {

            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            StateWiseAllocationReportService.GetGriddata(dataObj).then(function (response1) {
                if (response1.data != null) {
                    $scope.GridVisiblity = false;
                    $scope.GridVisiblityproject = false;
                    $scope.GridVisiblitylocation = false;
                    $scope.GridVisiblitymancom = false;
                    $scope.GridConsolidated = true;
                    $scope.gridOptions.api.setRowData(response1.data);
                    $scope.gridOptions.api.refreshView();
                }
                else {
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);
            });
        }

        $scope.gridOptions.columnDefs = $scope.columnDefs;


        //$scope.gridOptions.api.setDatasource(response.data);
        $scope.gridOptions.api.setColumnDefs();
    };

    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                StateWiseAllocationReportService.StateByCny(2).then(function (response) {
                    if (response.data != null) {
                        $scope.State = response.data;
                        angular.forEach($scope.State, function (value, key) {
                            value.ticked = true;
                        });
                        StateWiseAllocationReportService.CityByState(2).then(function (response) {
                            if (response.data != null) {
                                $scope.City = response.data;
                                angular.forEach($scope.City, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.getLocations(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Locations = response.data;
                                        angular.forEach($scope.Locations, function (value, key) {
                                            value.ticked = true;
                                        })
                                    }
                                });
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.forEach($scope.Towers, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.forEach($scope.Floors, function (value, key) {
                                                    value.ticked = true;
                                                })
                                                StateWiseAllocationReportService.VerticalByFloor(2).then(function (response) {
                                                    if (response.data != null) {
                                                        $scope.Verticals = response.data;
                                                        angular.forEach($scope.Verticals, function (value, key) {
                                                            value.ticked = true;
                                                        })
                                                    }
                                                    StateWiseAllocationReportService.CostcenterByVertical(2).then(function (response) {
                                                        if (response.data != null) {
                                                            $scope.CostCenters = response.data;
                                                            $scope.BindGrid_Summary();
                                                            angular.forEach($scope.CostCenters, function (value, key) {
                                                                value.ticked = true;
                                                            })
                                                        }

                                                    })

                                                })

                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    $scope.Pageload();

    $scope.GetCitiesByState = function () {
        StateWiseAllocationReportService.GetCitiesByState($scope.Customized.State, 2).then(function (response) {
            if (response.data) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                });
            }
            else {
                $scope.City = [];
            };

            setTimeout(function () {
                $scope.getLocationsByCity()
            }, 100)

        }, function (error) {
            console.log(error);
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.State = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.CityByState = function () {
        StateWiseAllocationReportService.CityByState($scope.Customized.State, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.State, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.StateSelectAll = function () {
        $scope.Customized.State = $scope.State;
        $scope.CityByState();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                });


            } else {
                $scope.Locations = [];
            }
            setTimeout(function () {
                $scope.getTowerByLocation();
            }, 100);

        }, function (error) {
            console.log(error);
        });
        //angular.forEach($scope.Country, function (value, key) {
        //    value.ticked = false;
        //});
        //angular.forEach($scope.City, function (value, key) {
        //    var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
        //    if (cny != undefined && value.ticked == true) {
        //        cny.ticked = true;
        //        $scope.Customized.Country[0] = cny;
        //    }
        //});
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.ctySelectNone = function () {
        $scope.Locations = [];
        $scope.Towers = [];
        $scope.Floors = [];
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.Customized.Locations, 2).then(function (response) {
            if (response.data != null) {
                $scope.Towers = response.data;
                angular.forEach($scope.Towers, function (value, key) {
                    value.ticked = true;
                });
                setTimeout(function () { $scope.getFloorByTower() }, 100);
            }
            else { $scope.Towers = []; }


        }, function (error) {
            console.log(error);
        });


        //angular.forEach($scope.Country, function (value, key) {
        //    value.ticked = false;
        //});
        //angular.forEach($scope.City, function (value, key) {
        //    value.ticked = false;
        //});
        //angular.forEach($scope.Locations, function (value, key) {
        //    var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
        //    if (cny != undefined && value.ticked == true) {
        //        cny.ticked = true;
        //        $scope.Customized.Country[0] = cny;
        //    }
        //});

        //angular.forEach($scope.Locations, function (value, key) {
        //    var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
        //    if (cty != undefined && value.ticked == true) {
        //        cty.ticked = true;
        //        $scope.Customized.City[0] = cty;
        //    }
        //});
    }

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.Customized.Towers, 2).then(function (response) {
            if (response.data != null) {
                $scope.Floors = response.data;
                angular.forEach($scope.Floors, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.Floors = [];

        }, function (error) {
            console.log(error);
        });
        //angular.forEach($scope.Country, function (value, key) {
        //    value.ticked = false;
        //});
        //angular.forEach($scope.City, function (value, key) {
        //    value.ticked = false;
        //});
        //angular.forEach($scope.Locations, function (value, key) {
        //    value.ticked = false;
        //});
        //angular.forEach($scope.Towers, function (value, key) {
        //    var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
        //    if (cny != undefined && value.ticked == true) {
        //        cny.ticked = true;
        //        $scope.Customized.Country[0] = cny;
        //    }
        //});

        //angular.forEach($scope.Towers, function (value, key) {
        //    var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
        //    if (cty != undefined && value.ticked == true) {
        //        cty.ticked = true;
        //        $scope.Customized.City[0] = cty;
        //    }
        //});

        //angular.forEach($scope.Towers, function (value, key) {
        //    var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
        //    if (lcm != undefined && value.ticked == true) {
        //        lcm.ticked = true;
        //        $scope.Customized.Locations[0] = lcm;
        //    }
        //});
    }

    $scope.GetCostcenterByMancom = function () {
        StateWiseAllocationReportService.GetCostcenterByMancom($scope.Customized.Verticals, 2).then(function (response) {
            if (response.data != null) {
                $scope.CostCenters = response.data;
                angular.forEach($scope.CostCenters, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.CostCenters = [];

        }, function (error) {
            console.log(error);
        });

    }

    $scope.twrSelectAll = function () {
        $scope.Customized.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.Customized.Towers[0] = twr;
            }
        });

    };

    $scope.floorChangeAll = function () {
        $scope.Customized.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    $scope.Cols = [
        { COL: "Country", value: "COUNTRY", ticked: false },
        { COL: "Region", value: "REGION", ticked: false },
        { COL: "Zone", value: "ZONENAME", ticked: false },
        { COL: "State", value: "STATENAME", ticked: false },
        { COL: "City", value: "CITY", ticked: false },
        { COL: "Location", value: "LOCATION", ticked: false },
        { COL: "TOWER", value: "TOWER", ticked: false },
        { COL: "FLOOR", value: "FLOOR", ticked: false },
        { COL: "Space", value: "SPACE", ticked: false },
        { COL: "Space Type", value: "SPACE_TYPE", ticked: false },
        { COL: "Space Sub Type", value: "SPACE_SUB_TYPE", ticked: false },
        { COL: "Area", value: "AREA", ticked: false },
        { COL: "Parent Entity", value: "PE_CODE", ticked: false },
        { COL: "Child Entity", value: "CHE_CODE", ticked: false },

        { COL: "", value: "VERTICAL", ticked: false },
        { COL: "", value: "COSTCENTER", ticked: false },
        { COL: "Employee Id", value: "EMP_ID", ticked: false },
        { COL: "Employee Name", value: "EMP_NAME", ticked: false },
        //{ COL: "From Date", value: "FromDate", ticked: false },
        //{ COL: "To Date", value: "ToDate", ticked: false },
        { COL: "Employee Mail", value: "MAIL", ticked: false },
        { COL: "Reporting To Id", value: "REPORTING", ticked: false },
        { COL: "Reporting To Name", value: "REPORTING_NAME", ticked: false },
        { COL: "Status", value: "STATUS", ticked: false },
        { COL: "From Date", value: "FromDate", ticked: false },
        { COL: "To Date", value: "ToDate", ticked: false },
        { COL: "Shift Time", value: "ShiftTime", ticked: false },
        { COL: "Allocation Date", value: "AllocationDate", ticked: false },
        { COL: "Employee Type", value: "AUR_TYPE", ticked: false },
        { COL: "Grade", value: "AUR_GRADE", ticked: false },
        { COL: "Long Leave From Date", value: "AUR_LONG_LEAVE_FROM_DT", ticked: false },
        { COL: "Long Leave To Date", value: "AUR_LONG_LEAVE_TO_DT", ticked: false },
        { COL: "Leave Status", value: "AUR_LEAVE_STATUS", ticked: false },
        { COL: "Leave Reason", value: "AUR_LONG_LEAVE_REASON", ticked: false },
        { COL: "Business Unit One", value: "BHO_NAME", ticked: false },
        { COL: "Business Unit Two", value: "BHT_NAME", ticked: false }

    ];

    $scope.columnDefs = [
        { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', suppressMenu: true, sno: 1 },
        //{ headerName: "Region", field: "REGION", width: 100, cellClass: 'grid-align', suppressMenu: true, sno: 1 },
        { headerName: "Zone", field: "ZONENAME", width: 100, cellClass: 'grid-align', suppressMenu: true, sno: 1 },
        { headerName: "State", field: "STATENAME", width: 100, cellClass: 'grid-align', suppressMenu: true, sno: 1 },

        { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100, sno: 2 },
        { headerName: "Location Code", field: "LOCATION_CODE", cellClass: 'grid-align', width: 150, sno: 3 },
        { headerName: "Location Name", field: "LOCATION", cellClass: 'grid-align', width: 150, sno: 4 },
        { headerName: "Tower", field: "TOWER", cellClass: 'grid-align', width: 100, sno: 5 },
        { headerName: "Floor", field: "FLOOR", cellClass: 'grid-align', width: 100, sno: 6 },
        { headerName: "Space", field: "SPACE", cellClass: 'grid-align', width: 200, suppressMenu: true, sno: 7 },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", cellClass: 'grid-align', width: 150, sno: 8 },
        { headerName: "Space Sub Type", field: "SST_NAME", cellClass: 'grid-align', width: 150, sno: 9 },
        { headerName: "Area", field: "AREA", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 10 },
        //{ headerName: "Unit Cost", field: "UNITCOST", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        //{ headerName: "Seat Cost", field: "SEATCOST", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 11 },
        //{ headerName: "Aisle Area", field: "AISLEAREA", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 12 },
        //{ headerName: "Common Area", field: "COMMONAREA", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 13 },
        { headerName: "Parent Entity", field: "PE_CODE", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 14 },
        { headerName: "Child Entity", field: "CHE_CODE", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum', sno: 15 },

        { headerName: "Mancom", field: "VERTICAL", cellClass: 'grid-align', width: 150, sno: 16 },
        { headerName: "COSTCENTER", field: "COSTCENTER", cellClass: 'grid-align', width: 150, },
        { headerName: "Department", field: "DEPARTMENT", cellClass: 'grid-align', width: 200, sno: 17 },
        { headerName: "Employee Id", field: "EMP_ID", cellClass: 'grid-align', width: 100, suppressMenu: true, sno: 18 },
        { headerName: "Employee Name", field: "EMP_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 19 },
        //{ headerName: "From Date", field: "FromDate", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        // { headerName: "To Date", field: "ToDate", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        { headerName: "Employee Mail", field: "MAIL", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 20 },
        { headerName: "Reporting To Id", field: "REPORTING", cellClass: 'grid-align', width: 100, sno: 21 },
        { headerName: "Reporting To Name", field: "REPORTING_NAME", cellClass: 'grid-align', width: 150, sno: 22 },
        { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 23 },
        { headerName: "From Date", field: "SSA_FROM_DATE", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 24 },
        { headerName: "To Date", field: "SSA_TO_DATE", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 25 },
        { headerName: "Shift Time", field: "SHIFT_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 26 },
        { headerName: "Allocation Date", field: "ALLOCATION_DATE", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 27 },
        { headerName: "Employee Type", field: "AUR_TYPE", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 28 },
        { headerName: "Grade", field: "AUR_GRADE", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 29 }
        //{ headerName: "Long Leave From Date", field: "AUR_LONG_LEAVE_FROM_DT", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 30 },
        //{ headerName: "Long Leave To Date", field: "AUR_LONG_LEAVE_TO_DT", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 31 },
        //{ headerName: "Leave Status", field: "AUR_LEAVE_STATUS", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 32 },
        //{ headerName: "Leave Reason", field: "AUR_LONG_LEAVE_REASON", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 33 },
        //{ headerName: "Company", field: "CNP_NAME", cellClass: 'grid-align', width: 150, suppressMenu: true, sno: 34 },
        //{ headerName: "Business Unit One", field: "BHO_NAME", cellClass: 'grid-align', width: 120, suppressMenu: true, aggFunc: 'sum', sno: 35 },
        //{ headerName: "Business Unit Two", field: "BHT_NAME", cellClass: 'grid-align', width: 120, suppressMenu: true, aggFunc: 'sum', sno: 36 }
    ];


    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $scope.gridOptions = {
        columnDefs: [],
        enableCellSelection: false,
        enableFilter: true,
        rowData: null,
        enableSorting: true,
        enableColResize: true,
        //showToolPanel: true,
        //paginationPageSize: 500,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },

    };


    function groupAggFunction(rows) {
        var sums = {
            AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.AREA += parseFloat((data.AREA).toFixed(2));
        });
        return sums;
    }

    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }

    $scope.gridOptions.onColumnVisible = function (event) {
        if (event.visible) {
            $scope.HideColumns = 0;
            $scope.gridOptions.columnDefs.push(
                {
                    headerName: event.column.colDef.headerName,
                    field: event.column.colId,
                    cellClass: "grid-align",
                    width: 110,
                    sno: event.column.colDef.sno,
                    suppressMenu: true
                }
            );
            $scope.gridOptions.columnDefs.visible = true;
            _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
        } else {
            $scope.HideColumns = 1;
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                    $scope.gridOptions.columnDefs[i].visible = false;
                    $scope.gridOptions.columnDefs.splice(i, 1);
                }
            }
        }
    };

    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'Select':
                $scope.Customized.FromDate = "";
                $scope.Customized.ToDate = "";
                break;
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }

    var columnDefsMancom = [
        { headerName: "Mancom", field: "MANCOM", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Space Type", field: "SPC_TYPE", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Allocated Seats", field: "TOTAL_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
        // { headerName: "Mancom Allocated Seats", field: "ALLOCATED_VACANT_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Vacant", field: "VACANT", width: 170, cellClass: 'grid-align', suppressMenu: true }
    ];

    var columnDefsCostCenter = [
        { headerName: "Mancom", field: "MANCOM", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Cost Center Code", field: "COST_CENTER_CODE", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Cost Center Name", field: "COST_CENTER_NAME", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Space Type", field: "SPC_TYPE", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Allocated Seats", field: "TOTAL_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Vacant", field: "VACANT", width: 170, cellClass: 'grid-align', suppressMenu: true }
    ];

    var columnDefsLoc = [
        { headerName: "City", field: "CTY_NAME", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Location Code", field: "LCM_CODE", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Location", field: "LCM_NAME", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Space Type", field: "SPC_TYPE", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Seats", field: "TOTAL_SEATS", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Vacant Seats", field: "VACANT_SEATS", width: 250, cellClass: 'grid-align', suppressMenu: true }, ,
        { headerName: "Allocated But Not Occupied", field: "ALLOCATED_SEATS", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Employee Shared Seats", field: "EMPLOYEE_OCCUPIED_SEAT_COUNT", width: 250, cellClass: 'grid-align', suppressMenu: true }
    ];

    var columnDefs1 = [
        {
            headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, suppressMenu: true
        },
        { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 1, hide: true, suppressMenu: true },
        { headerName: "Location", field: "LCM_NAME", width: 200, cellClass: 'grid-align', rowGroupIndex: 2, hide: true },
        { headerName: "Tower", field: "TWR_NAME", width: 100, cellClass: 'grid-align',/* rowGroupIndex: 3, hide: true*/ },
        { headerName: "Floor", field: "FLR_NAME", width: 150, cellClass: 'grid-align' },
        { headerName: "Space Type", field: "SPC_TYPE", width: 100, cellClass: 'grid-align' },
        { headerName: "Total Seats", field: "TOTAL_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Allocated Seats", field: "ALLOCATED_SEATS", width: 180, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Allocated But Not Occupied Seats", field: "ALLOCATED_VACANT", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Vacant Seats", field: "VACANT_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Employee Shared Seats", field: "EMPLOYEE_OCCUPIED_SEAT_COUNT", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Blocked Seats", field: "BLOCKED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Requested Seats", field: "REQUESTED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true }
    ];


    $scope.gridOptions1 = {
        columnDefs: columnDefs1,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        groupAggFunction: groupAggFunction1,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "CNY_NAME",
            cellRenderer: {
                renderer: "group"
            }

        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions1.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


    function groupAggFunction1(rows) {
        var sums = {
            TOTAL_SEATS: 0,
            ALLOCATED_SEATS: 0,
            VACANT_SEATS: 0,
            OCCUPIED_SEATS: 0,
            ALLOCATED_VACANT: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.TOTAL_SEATS += parseFloat((data.TOTAL_SEATS));
            sums.ALLOCATED_SEATS += parseFloat((data.ALLOCATED_SEATS));
            sums.VACANT_SEATS += parseFloat((data.VACANT_SEATS));
            sums.OCCUPIED_SEATS += parseFloat((data.OCCUPIED_SEATS));
            sums.ALLOCATED_VACANT += parseFloat((data.ALLOCATED_VACANT));
        });
        return sums;
    }

    $scope.gridLocation = {
        columnDefs: columnDefsLoc,
        rowData: [],
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        groupHideGroupColumns: false,
        groupColumnDef: {
            headerName: "City",
            cellRenderer: {
                renderer: "group"
            }

        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridLocation.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    }


    $scope.gridmancom = {
        columnDefs: columnDefsMancom,
        rowData: [],
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        groupHideGroupColumns: false,

    };

    $scope.Options = {
        columnDefs: columnDefsCostCenter,
        rowData: [],
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        groupHideGroupColumns: false,

        //columnDefs: columnDefsCostCenter,
        //enableFilter: true,
        //rowData: [],
        //rowSelection: 'multiple',
        //rowDeselection: true,
        //enableColResize: true,
        //floatingFilter: true,

    };

    $scope.BindGrid_Summary = function () {

        $scope.GridVisiblity = false;
        $scope.GridVisiblityproject = false;
        $scope.GridVisiblitylocation = false;
        $scope.GridVisiblitymancom = false;
        $scope.GridConsolidated = false;

        locations = _.filter($scope.Customized.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(',');
        towers = _.filter($scope.Customized.Towers, function (o) { return o.ticked == true; }).map(function (x) { return x.TWR_CODE; }).join(',');
        floors = _.filter($scope.Customized.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');
        verticals = _.filter($scope.Customized.Verticals, function (o) { return o.ticked == true; }).map(function (x) { return x.VER_CODE; }).join(',');
        costcenters = _.filter($scope.Customized.CostCenters, function (o) { return o.ticked == true; }).map(function (x) { return x.Cost_Center_Code; }).join(',');
        var params = {
            CNP_NAME: 1,
            LOCATION: locations,
            TOWERS: towers,
            FLOORS: floors,
            VERTICAL: verticals,
            COSTCENTER: costcenters
        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);
        StateWiseAllocationReportService.BindGridSummary(params).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.gridata = response;
            if (response == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData([]);
                $scope.gridOptions1.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
            //$scope.SpaceConsolidatedChart();
        }, function (error) {
            console.log(error);
        });
    };

    $scope.BindGrid_Mancom = function () {

        $scope.GridVisiblity = false;
        $scope.GridVisiblityproject = false;
        $scope.GridVisiblitylocation = false;
        $scope.GridVisiblitymancom = false;
        $scope.GridConsolidated = false;

        verlst = _.filter($scope.Customized.Verticals, function (o) { return o.ticked == true; }).map(function (x) { return x.VER_CODE; }).join(',');
        costlist = _.filter($scope.Customized.CostCenters, function (o) { return o.ticked == true; }).map(function (x) { return x.Cost_Center_Code; }).join(',');
        locations = _.filter($scope.Customized.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(',');
        twrlist = _.filter($scope.Customized.Towers, function (o) { return o.ticked == true; }).map(function (x) { return x.TWR_CODE; }).join(',');
        flrlist = _.filter($scope.Customized.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');
        var params = {
            CNP_NAME: 1,
            LCM_NAME: locations,
            Towers: twrlist,
            FLOORS: flrlist,
            VERTICAL: verlst,
            COSTCENTER: costlist
        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);
        StateWiseAllocationReportService.GetMancom(params).then(function (response) {
            progress(0, 'Loading...', true);

            $scope.gridata2 = response;
            if (response == null) {
                $scope.GridVisiblitymancom = true;
                $scope.gridmancom.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblitymancom = true;
                $scope.gridmancom.api.setRowData([]);

                //$scope.gridata2.push({ MANCOM: 'Complete Vacant', SPC_TYPE: '', TOTAL_SEATS: $scope.gridata2[0].ALLOCATED_VACANT_SEATS, OCCUPIED_SEATS: '', VACANT: $scope.gridata2[0].ALLOCATED_VACANT_SEATS });
                var TOTAL_SEATS_SUM = _.sumBy($scope.gridata2, function (o) { return parseInt(o.TOTAL_SEATS != '' ? o.TOTAL_SEATS : 0); });
                var TOTAL_OCCUPIED_SEATS = _.sumBy($scope.gridata2, function (o) { return parseInt(o.OCCUPIED_SEATS != '' ? o.OCCUPIED_SEATS : 0); });
                var TOTAL_VACANT = _.sumBy($scope.gridata2, function (o) { return parseInt(o.VACANT != '' ? o.VACANT : 0); });
                $scope.gridata2.push({ MANCOM: 'Total', SPC_TYPE: '', TOTAL_SEATS: TOTAL_SEATS_SUM, OCCUPIED_SEATS: TOTAL_OCCUPIED_SEATS, VACANT: TOTAL_VACANT });

                $scope.gridmancom.api.setRowData($scope.gridata2);
                progress(0, 'Loading...', false);
            }
        }, function (error) {
            console.log(error);
        });
    };

    $scope.GenReportmancom = function () {

        progress(0, 'Loading...', true);
        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Mancom_Report.csv"
        };
        $scope.gridmancom.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.BindGrid_Project = function () {

        $scope.GridVisiblity = false;
        $scope.GridVisiblityproject = false;
        $scope.GridVisiblitylocation = false;
        $scope.GridVisiblitymancom = false;
        $scope.GridConsolidated = false;

        locations = _.filter($scope.Customized.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(',');
        twrlist = _.filter($scope.Customized.Towers, function (o) { return o.ticked == true; }).map(function (x) { return x.TWR_CODE; }).join(',');
        flrlist = _.filter($scope.Customized.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');
        verlst = _.filter($scope.Customized.Verticals, function (o) { return o.ticked == true; }).map(function (x) { return x.VER_CODE; }).join(',');
        costlist = _.filter($scope.Customized.CostCenters, function (o) { return o.ticked == true; }).map(function (x) { return x.Cost_Center_Code; }).join(',');
        var params = {

            CNP_NAME: 1,
            LCM_NAME: locations,
            Towers: twrlist,
            FLOORS: flrlist,
            VERTICAL: verlst,
            COSTCENTER: costlist
        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);
        StateWiseAllocationReportService.GetProjectSummary(params).then(function (response) {

            //ExportColumns = response.exportCols;
            //$scope.Options.api.setColumnDefs(response.Coldef);
            $scope.gridata1 = response.griddata;

            if ($scope.gridata1 == null) {
                $scope.GridVisiblityproject = true;
                progress(0, 'Loading...', false);
                $scope.Options.api.setRowData([]);
            }
            else {

                $scope.GridVisiblityproject = true;

                //$scope.gridata1.push({ MANCOM: 'Complete Vacant', COST_CENTER_NAME: '', SPC_TYPE: '', TOTAL_SEATS: $scope.gridata1[0].COMPLETE_VACANT, OCCUPIED_SEATS: '', VACANT: $scope.gridata1[0].COMPLETE_VACANT });
                //$scope.gridata1.push({ MANCOM: 'Complete Mancom', COST_CENTER_NAME: '', SPC_TYPE: '', TOTAL_SEATS: $scope.gridata1[0].COMPLETE_MANCOM, OCCUPIED_SEATS: '', VACANT: $scope.gridata1[0].COMPLETE_MANCOM });
                var TOTAL_SEATS_SUM = _.sumBy($scope.gridata1, function (o) { return parseInt(o.TOTAL_SEATS != '' ? o.TOTAL_SEATS : 0); });
                var TOTAL_OCCUPIED_SEATS = _.sumBy($scope.gridata1, function (o) { return parseInt(o.OCCUPIED_SEATS != '' ? o.OCCUPIED_SEATS : 0); });
                var TOTAL_VACANT = _.sumBy($scope.gridata1, function (o) { return parseInt(o.VACANT != '' ? o.VACANT : 0); });
                $scope.gridata1.push({ MANCOM: 'Total', COST_CENTER_NAME: '', SPC_TYPE: '', TOTAL_SEATS: TOTAL_SEATS_SUM, OCCUPIED_SEATS: TOTAL_OCCUPIED_SEATS, VACANT: TOTAL_VACANT });

                $scope.Options.api.setRowData($scope.gridata1);
                progress(0, 'Loading...', false);
            }

        }, function (error) {
            console.log(error);
        });
    };

    $scope.GenReportproject = function () {

        progress(0, 'Loading...', true);
        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Project_Report.csv"
        };
        $scope.Options.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.BindGrid_Location = function () {

        $scope.GridVisiblity = false;
        $scope.GridVisiblityproject = false;
        $scope.GridVisiblitylocation = false;
        $scope.GridVisiblitymancom = false;
        $scope.GridConsolidated = false;

        locations = _.filter($scope.Customized.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(',');
        twrlist = _.filter($scope.Customized.Towers, function (o) { return o.ticked == true; }).map(function (x) { return x.twr_CODE; }).join(',');
        flrlist = _.filter($scope.Customized.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');
        verlst = _.filter($scope.Customized.Verticals, function (o) { return o.ticked == true; }).map(function (x) { return x.VER_CODE; }).join(',');
        costlist = _.filter($scope.Customized.CostCenters, function (o) { return o.ticked == true; }).map(function (x) { return x.Cost_Center_Code; }).join(',');
        var params = {

            CNP_NAME: 1,
            FLAG: 2,
            LCM_NAME: locations,
            Towers: twrlist,
            FLOORS: flrlist,
            VERTICAL: verlst,
            COSTCENTER: costlist,

        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);

        StateWiseAllocationReportService.BindGrid(params).then(function (response) {
            progress(0, 'Loading...', true);

            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.GridVisiblitylocation = true;
                $scope.gridLocation.api.setRowData([]);
                progress(0, 'Loading...', false);

            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblitylocation = true;
                $scope.gridLocation.api.setRowData([]);
                var TOTAL_SEATS_SUM = _.sumBy($scope.gridata, function (o) { return o.TOTAL_SEATS; });
                var TOTAL_SEATS_VACANT = _.sumBy($scope.gridata, function (o) { return o.VACANT_SEATS; });
                var TOTAL_SEATS_OCCUPIED = _.sumBy($scope.gridata, function (o) { return o.OCCUPIED_SEATS; });
                var ALLOCATED_SEATS = _.sumBy($scope.gridata, function (o) { return o.ALLOCATED_SEATS; });

                $scope.gridata.push({ CTY_NAME: '', LCM_NAME: 'Total', TOTAL_SEATS: TOTAL_SEATS_SUM, VACANT_SEATS: TOTAL_SEATS_VACANT, OCCUPIED_SEATS: TOTAL_SEATS_OCCUPIED, ALLOCATED_SEATS: ALLOCATED_SEATS });
                $scope.gridLocation.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }

        }, function (error) {
            console.log(error);
        });
    };

    $scope.GenReportlocation = function () {

        progress(0, 'Loading...', true);
        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "LocationWise_Report.csv"
        };
        $scope.gridLocation.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.BindGrid = function () {
        var params = {
            CNP_NAME: 1,
            FLAG: 1
        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);
        StateWiseAllocationReportService.BindGrid(params).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.gridata = response.data;
            if (response.data == null) {

                $scope.GridVisiblityproject = false;
                $scope.GridVisiblitylocation = false;
                $scope.GridVisiblitymancom = false;
                $scope.GridConsolidated = false;
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData([]);
                progress(0, 'Loading...', false);

            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.GridVisiblityproject = false;
                $scope.GridVisiblitylocation = false;
                $scope.GridVisiblitymancom = false;
                $scope.gridOptions1.api.setRowData([]);
                $scope.gridOptions1.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
            //$scope.SpaceConsolidatedChart();
        }, function (error) {
            console.log(error);
        });
    };

    $scope.GenReport = function () {

        progress(0, 'Loading...', true);
        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Consolidated_Report.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReportSummery = function () {

        progress(0, 'Loading...', true);
        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Summary_Report.csv"
        };
        $scope.gridOptions1.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

}]);
