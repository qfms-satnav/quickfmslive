﻿app.service("SpaceOccupancyReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    this.GetGriddata = function (SpaceOccupancy) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceOccupancyReport/GetOccupGrid', SpaceOccupancy)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceOccupancyReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('SpaceOccupancyReportController', ['$scope', '$q', '$http', 'SpaceOccupancyReportService', '$filter', 'UtilityService', '$timeout', function ($scope, $q, $http, SpaceOccupancyReportService, $filter, UtilityService, $timeout) {
    $scope.SpaceOccupancy = {};
    $scope.Type = [];
    $scope.Cost = [];
    $scope.GridVisiblity = true;
    $scope.GridVisiblity2 = false;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.SpaceOccupancy.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
    $scope.SpaceOccupancy.ToDate = moment().format('MM/DD/YYYY');
    $scope.columnDefs = [

        { headerName: "Space Id", field: "SPC_ID", cellClass: 'grid-align', width: 150, suppressMenu: true },
        { headerName: "Employee Id", field: "AUR_ID", cellClass: 'grid-align', width: 150 },
        { headerName: "Employee Name", field: "EMP_ID", cellClass: 'grid-align', width: 180 },
        { headerName: "Extension", field: "EXTENSION", cellClass: 'grid-align', width: 200 },
        { headerName: "Employee Type", field: "AUR_TYPE", cellClass: 'grid-align', width: 100 },
        { headerName: "Manager Name/ID", field: "AUR_REPORTING_TO", cellClass: 'grid-align', width: 200 },
        { headerName: "Shift", field: "SHIFT", cellClass: 'grid-align', width: 150 },
        { headerName: "Space Type", field: "SPACE_TYPE", cellClass: 'grid-align', width: 120 },
        //{ headerName: "Entity", field: "ENTITY", cellClass: 'grid-align', width: 80, suppressMenu: true },
        //{ headerName: "Child Entity", field: "CHILD_ENTITY", cellClass: 'grid-align', width: 90, suppressMenu: true },
        { headerName: "", field: "VER_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "", field: "DEP_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "", field: "BHO_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "", field: "BHT_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "", field: "ENTITY", cellClass: 'grid-align', width: 200 },
        { headerName: "", field: "CHILD_ENTITY", cellClass: 'grid-align', width: 200 },
        { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align',suppressMenu: true },
        { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 160 },
        { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true },
        { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110 },
        { headerName: "From Date", field: "SSAD_FROM_DATE", cellClass: 'grid-align', width: 110 },
        { headerName: "To Date", field: "SSAD_TO_DATE", cellClass: 'grid-align', width: 110 },
        //{ headerName: "Request Type", field: "REQUEST_TYPE", cellClass: 'grid-align', width: 150, suppressMenu: true }
    ];

    //setTimeout(function () {
    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            $scope.SpaceOccupancy.CNP_NAME = parseInt(CompanySession);
            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                a.ticked = true;
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }
        UtilityService.getBussHeirarchy().then(function (response) {
            if (response.data != null) {
                $scope.BsmDet = response.data;
                $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
                $scope.gridOptions.columnApi.getColumn("BHO_NAME").colDef.headerName = $scope.BsmDet.BH1;
                $scope.gridOptions.columnApi.getColumn("BHT_NAME").colDef.headerName = $scope.BsmDet.BH2;
                $scope.gridOptions.columnApi.getColumn("ENTITY").colDef.headerName = $scope.BsmDet.PE;
                $scope.gridOptions.columnApi.getColumn("CHILD_ENTITY").colDef.headerName = $scope.BsmDet.CE;
                $scope.gridOptions.api.refreshHeader();
            }
            UtilityService.getCostCenters(2).then(function (response) {

                if (response.data != null) {
                    $scope.Cost = response.data;
                    angular.forEach($scope.Cost, function (value, key) {
                        value.ticked = true;
                    });
                    // $scope.LoadData();
                }

            });
        });

    });
    //}, 500);
    $scope.CostChangeAll = function () {
        $scope.SpaceOccupancy.Cost = $scope.Cost;
        console.log($scope.SpaceOccupancy.Cost);
        // $scope.FloorChange();
    }
    $scope.LoadData = function () {
        progress(0, 'loading...', true);
        var dataObj = {
            FromDate: $scope.SpaceOccupancy.FromDate,
            ToDate: $scope.SpaceOccupancy.ToDate,
            CNP_NAME: $scope.SpaceOccupancy.CNP_NAME[0].CNP_ID,
            Costcenterlst: $scope.SpaceOccupancy.Cost,
            Type: $scope.flag
        };
        var fromdate = moment($scope.SpaceOccupancy.FromDate);
        var todate = moment($scope.SpaceOccupancy.ToDate);
        if (fromdate > todate) {
            $scope.gridvisiblity = false;
            $scope.gridvisiblity2 = false;
            progress(0, 'loading...', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        else {
            $scope.gridvisiblity = true;
            SpaceOccupancyReportService.GetGriddata(dataObj).then(function (data) {
                $scope.gridata = data;
                $scope.OccupChartDetails(dataObj);
                if ($scope.gridata == null) {
                    $scope.GridVisiblity2 = false;
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, 'loading...', false);
                }
                else {
                    $scope.GridVisiblity2 = true;
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    progress(0, 'loading...', false);
                }

            }, function (error) {
                console.log(error);
            });
        }
    };
    $scope.Search = function () {
        $scope.LoadData();
    }

    //$scope.loaddata = function () {
    //    var dataobj = $filter('filter')($scope.cost, { ticked: true });
    //    progress(0, 'loading...', true);
    //    var params = {
    //        fromdate: $scope.spaceoccupancy.fromdate,
    //        todate: $scope.spaceoccupancy.todate,
    //        cnp_name: $scope.spaceoccupancy.cnp_name[0].cnp_id,
    //        costcenterlst: dataobj
    //        //costcenterlst:$scope.spaceoccupancy.cost
    //    };
    //    var fromdate = moment($scope.spaceoccupancy.fromdate);
    //    var todate = moment($scope.spaceoccupancy.todate);
    //    if (fromdate > todate) {
    //        $scope.gridvisiblity = false;
    //        $scope.gridvisiblity2 = false;
    //        shownotification('error', 8, 'bottom-right', utilityservice.datevalidationonsubmit);
    //    }
    //    else {

    //        $scope.gridvisiblity = true;
    //        spaceoccupancyreportservice.getgriddata(params).then(function (data) {
    //            $scope.gridata = data;
    //            $scope.occupchartdetails(params);
    //            if ($scope.gridata == null) {
    //                $scope.gridvisiblity2 = false;
    //                $scope.gridoptions.api.setrowdata([]);
    //                settimeout(function () {
    //                    progress(0, 'loading...', false);
    //                }, 600);
    //            }
    //            else {
    //                progress(0, 'loading...', true);
    //                $scope.gridvisiblity2 = true;
    //                $scope.gridoptions.api.setrowdata($scope.gridata);
    //                settimeout(function () {
    //                    progress(0, 'loading...', false);
    //                }, 600);
    //            }

    //        }, function (error) {
    //            console.log(error);
    //        });
    //    }
    //}

    $scope.ColumnNames = [];
    //$scope.OccupDataLoad = function () {
    //    progress(0, 'Loading...', true);
    //    $scope.Pageload = {
    //        FromDate: $scope.SpaceOccupancy.FromDate,
    //        ToDate: $scope.SpaceOccupancy.ToDate,
    //        CNP_NAME: $scope.SpaceOccupancy.CNP_NAME[0].CNP_ID
    //    };
    //    SpaceOccupancyReportService.GetGriddata($scope.Pageload).then(function (data) {
    //        $scope.gridata = data;
    //        if ($scope.gridata == null) {
    //            $scope.gridOptions.api.setRowData([]);
    //            progress(0, '', false);
    //        }
    //        else {
    //            progress(0, 'Loading...', true);
    //            $scope.gridOptions.api.setRowData([]);
    //            $scope.gridOptions.api.setRowData($scope.gridata);
    //            progress(0, '', false);
    //        }
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        showToolPanel: true
    };

    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    chart = c3.generate({
        data: {
            columns: [],
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
            labels: true
        },
        legend: {
            show: false,
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
            },
            y: {
                show: true,
                label: {
                    text: 'Occupancy Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    function toggle(id) {
        chart.toggle(id);
    }

    d3.select('.container').insert('div', '.chart').attr('class', 'legend').selectAll('div').data({
        columns: [],
        cache: false,
        type: 'bar',
        empty: { label: { text: "Sorry, No Data Found" } },
        labels: true
    })
        .enter().append('div')
        .attr('data-id', function (id) {
            return id;
        })
        .html(function (id) {
            return id;
        })
        .each(function (id) {
            //d3.select(this).append('span').style
            d3.select(this).append('span').style('background-color', chart.color(id));
        })
        .on('mouseover', function (id) {
            chart.focus(id);
        })
        .on('mouseout', function (id) {
            chart.revert();
        })
        .on('click', function (id) {
            $(this).toggleClass("c3-legend-item-hidden")
            chart.toggle(id);
        });

    $scope.OccupChartDetails = function (spcData) {
        console.log(spcData);
        $http({
            url: UtilityService.path + '/api/SpaceOccupancyReport/GetDetailsCount',
            method: 'POST',
            data: spcData
        }).then(function (response) {
            chart.unload();
            chart.load({ columns: response.data });
        });
        setTimeout(function () {
            $("#OccupGraph").append(chart.element);
        }, 700);

    }
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" },
        { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Department", key: "DEP_NAME" }, { title: "Space Type", key: "SPACE_TYPE" },
        { title: "Employee Id", key: "AUR_ID" }, { title: "Employee", key: "EMP_ID" }, { title: "Employee Type", key: "AUR_TYPE" }, { title: "Manager Name", key: "AUR_REPORTING_TO" },
        { title: "Space Id", key: "SPC_ID" }, { title: "Shift", key: "SHIFT" }, { title: "Entity", key: "ENTITY" }, { title: "Child Entity", key: "CHILD_ENTITY" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A3');
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("Space Occupancy Report.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);

    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Space Occupancy Report.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Occupdata, Type) {
        if (moment($scope.SpaceOccupancy.FromDate) > moment($scope.SpaceOccupancy.ToDate)) {
            $scope.gridvisiblity = false;
            $scope.gridvisiblity2 = false;
            progress(0, 'loading...', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        progress(0, 'Loading...', true);
        soobj = {};
        angular.copy(Occupdata, soobj);
        soobj.CNP_NAME = Occupdata.CNP_NAME[0].CNP_ID;
        soobj.Type = Type;
        soobj.FromDate = Occupdata.FromDate;
        soobj.ToDate = Occupdata.ToDate;
        soobj.Costcenterlst = Occupdata.Cost;
        soobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        soobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        soobj.VERTICAL = $scope.BsmDet.Parent;
        soobj.COSTCENTER = $scope.BsmDet.Child;
        soobj.BH1 = $scope.BsmDet.BH1;
        soobj.BH2 = $scope.BsmDet.BH2;
        soobj.PE = $scope.BsmDet.PE;
        soobj.CE = $scope.BsmDet.CE;

        //saobj.VERTICAL = $scope.BsmDet.Parent;
        //saobj.COSTCENTER = $scope.BsmDet.Child;
        //saobj.BH1 = $scope.BsmDet.BH1;
        //saobj.BH2 = $scope.BsmDet.BH2;
        //saobj.PE = $scope.BsmDet.PE;
        //saobj.CE = $scope.BsmDet.CE;
        //Occupdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpaceOccupancyReport/GetSpaceOccupancyReportdata',
                method: 'POST',
                data: soobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Space Occupancy Report.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {
            };
        };
    }

    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.SpaceAllocation.FromDate = "";
                $scope.SpaceAllocation.ToDate = "";
                break;
            case 'TODAY':
                $scope.SpaceAllocation.FromDate = moment().format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.SpaceAllocation.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.SpaceAllocation.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.SpaceAllocation.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.SpaceAllocation.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.SpaceAllocation.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }   
    setTimeout(function () { $scope.LoadData(); }, 2000);
}]);