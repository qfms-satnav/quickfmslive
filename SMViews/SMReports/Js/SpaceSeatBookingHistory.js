﻿app.service("SpaceSeatBookingHistroyService", function ($http, $q, UtilityService) {

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.put(UtilityService.path + '/api/SpaceSeatBookingHistroy/SpaceSeatBookingHistroy')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.allocateSeatsValidation = function (selSpaces) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceSeatBookingHistroy/allocateSeatsValidation', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getSpaceDaysRestriction = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.get(UtilityService.path + '/api/MaploaderAPI/getSpaceDaysRestriction')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});


app.controller('SpaceSeatBookingHistoryController', function ($scope, $q, $http, SpaceSeatBookingHistroyService, UtilityService, $timeout, $filter) {

    $scope.VerticalWiseAlloc = ''
    $scope.selectddata = [];
    $scope.FinalSelectedSpaces = [];
    $scope.Edit = []
    $scope.Shift = [];
    $scope.Shifts = [];
    /*$scope.selectedSpacesLength = 0*/;

    $timeout(UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPref = response.data;
            console.log($scope.SysPref);
            $scope.VerticalWiseAlloc = _.find($scope.SysPref, { SYSP_CODE: "Booking Based on Vertical" });
            $scope.Abbvallidations = (_.find($scope.SysPref, { SYSP_CODE: "Find Path" }) || {});
            $scope.Edit = (_.find($scope.SysPref, { SYSP_CODE: "EDIT SEAT DETAILS" }) || {});
            loaddata();
        }
    }), 1000);


    function loaddata() {
        progress(0, 'Loading...', true);
        SpaceSeatBookingHistroyService.GetGriddata().then(function (data) {
            $scope.gridata = data.griddata;
            for (var i in $scope.gridata) {
                $scope.gridata[i].active = true;
                $scope.gridata[i].activeBtn = false;
                $scope.gridata[i].Editbtn = false;

                if ($scope.gridata[i].SSAD_STA_ID == "1004") {
                    $scope.gridata[i].Editbtn = true;
                    $scope.gridata[i].activeBtn = true;
                }
                else {
                    $scope.gridata[i].Editbtn = false;
                    $scope.gridata[i].activeBtn = false;
                }
            }
            //a.push({ headerName: "RELEASE_SEAT", width: 100, field: "Released ", cellClass: "grid-align", filter: 'set', template: '<a ng-click="release(data)" data-ng-if="data.activeBtn">RELEASE</a>', pinned: 'right', suppressMenu: true })
            /*  if ($scope.Edit == '') {*/
            if ($scope.Edit && $scope.Edit.SYSP_VAL1 == 1 && $scope.Edit.SYSP_VAL2 == 1) {
                $scope.gridOptions.columnApi.setColumnVisible('Edit', true);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            else {
                $scope.gridOptions.columnApi.setColumnVisible('Edit', false);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }

            if ($scope.gridata == null) {
                $scope.gridOptions.api.setColumnDefs(a);
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Records not found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                //$scope.gridOptions.api.setColumnDefs(data.Coldef);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        });
    };


    var a = [
        /*{ headerName: "Space ID", field: "Space_ID", width: 0, cellClass: "grid-align", suppressMenu: false, template: '<a ng-click="navToMap(data)">{{data.Space_ID}}</a>' },---- removed because of AAA SOW suggestions--*/
        { headerName: "Space ID", field: "Space_ID", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "Employee Name", field: "Employee_Name", width: 0, cellClass: "grid-align", suppressMenu: false },
        {
            headerName: "From Date", field: "FROM_DATE", width: 0, cellClass: "grid-align", suppressMenu: false, cellRenderer: function (params) {

                const originalDate = params.value;
                const formattedDate = formatMyDate(originalDate);
                return formattedDate;
            },
        },
        {
            headerName: "To Date", field: "TO_DATE", width: 0, cellClass: "grid-align", suppressMenu: false,
            cellRenderer: function (params) {

                const originalDate = params.value;
                const formattedDate = formatMyDate(originalDate);
                return formattedDate;
            },
        },
        { headerName: "From Time", field: "FROM_TIME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "To Time", field: "TO_TIME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "Created Date", field: "CREATED_DATE", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "Created By", field: "CREATED_BY", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "Seat Status", field: "SEAT_STATUS", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "SSA SRNREQ ID", field: "SSA_SRNREQ_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "SSAD STA ID", field: "SSAD_STA_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "Space Type", field: "SPCTYPE", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "SSAD SRN REQ ID", field: "SSAD_SRN_REQ_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "Edit Details", width: 150, field: "Edit", cellClass: "grid-align", filter: 'set', template: '<a ng-click="EditFunction(data)" data-ng-if="data.Editbtn">EDIT</a>', pinned: 'right', suppressMenu: true },
        { headerName: "Release Seat", width: 150, field: "Released ", cellClass: "grid-align", filter: 'set', template: '<a ng-click="data.active && release(data)" data-ng-if="data.activeBtn">RELEASE</a>', pinned: 'right', suppressMenu: true }
    ];
    $scope.gridOptions = {
        columnDefs: a,
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        rowHeight: 48,
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'single',
        enableColResize: true,

    }


    $scope.navToMap = function (det) {
        if (parseInt($scope.Abbvallidations.SYSP_VAL1) == 1) {
            window.location.href = "../../../SMViews/Map/Maploader.aspx?lcm_code=" + det.SPC_BDG_ID + "&twr_code=" + det.SPC_TWR_ID + "&flr_code=" + det.SPC_FLR_ID + "&spc_id=" + det.Space_ID + "&value=1&passVal=Y"
        }
    }


    $scope.columDefsAlloc = [{ headerName: "Space ID", field: "SPC_ID", width: 0, cellClass: "grid-align", suppressMenu: false },
    { headerName: "Employee Name", field: "AUR_NAME", width: 0, cellClass: "grid-align", suppressMenu: false },
    {
        headerName: "From Date  <br />(mm/dd/yyyy)", field: "FROM_DATE", width: 0, cellClass: "grid-align", suppressMenu: false,
        cellRenderer: createFromDatePicker
    },
    { headerName: "To Date  <br />(mm/dd/yyyy)", field: "TO_DATE", width: 0, cellClass: "grid-align", suppressMenu: false, cellRenderer: createToDatePicker },
    { headerName: "From Time", field: "FROM_TIME", width: 0, cellClass: "grid-align", suppressMenu: false, cellRenderer: createFromTimePicker },
    { headerName: "To Time", field: "TO_TIME", width: 0, cellClass: "grid-align", suppressMenu: false, cellRenderer: createToTimePicker },
    {
        headerName: "Shift Type", field: "SH_CODE", width: 200, cellClass: "grid-align", filter: 'set', cellRenderer: TemplateRenderer
        //template: "<select data-ng-change='shiftChange(data)' class='form-control mt-1' ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in ShiftData' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>"
    },

    //{ headerName: "CREATED DATE", field: "CREATED_DATE", width: 0, cellClass: "grid-align", suppressMenu: false },
    //{ headerName: "CREATED BY", field: "CREATED_BY", width: 0, cellClass: "grid-align", suppressMenu: false },

    { headerName: "Seat Status", field: "SEAT_STATUS", width: 0, cellClass: "grid-align", suppressMenu: false },

    ];

    function TemplateRenderer(params) {
        if (params.data.SH_CODE_CURR == undefined) {
            params.data.SH_CODE_CURR = params.data.SH_CODE;
        }
        return "<select data-ng-change='shiftChange(data)' class='form-control mt-1' ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in ShiftData' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>"
    }

    function formatMyDate(dateString) {
        const date = new Date(dateString);
        const day = date.getDate();
        const month = new Intl.DateTimeFormat('en-GB', { month: 'short' }).format(date);
        const year = date.getFullYear();
        return `${day}, ${month} ${year}`;
    }

    $scope.shiftChange = function (data) {
        debugger;

        for (var i in $scope.ShiftData) {
            if (data.SH_CODE == $scope.ShiftData[i].SH_CODE) {
                data.FROM_TIME = $scope.ShiftData[i].SH_FRM_HRS;
                data.TO_TIME = $scope.ShiftData[i].SH_TO_HRS;

            }
        }
        //if (empid != data.AUR_ID) {
        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/MaploaderAPI/SpcAvailabilityByShiftEdit",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                if (data.STATUS != 1 && (data.SSA_SRNREQ_ID != "" || data.FROM_DATE != "")) {
                    if (response.data == null) {

                        $scope.AllocateEnabledStatus = true;
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                    else {
                        $scope.AllocateEnabledStatus = false;
                    }

                }
            }
        });
        //}
        //else {
        //    $scope.AllocateEnabledStatus = false;
        //}

    }
    SpaceSeatBookingHistroyService.getSpaceDaysRestriction().then(function (response) {
        if (response != null) {
            $scope.SPACETYPE = angular.copy(response);
            _.forEach(response, function (key, value) {
                if (key.Count > 0) {
                    switch (key.sno) {
                        case 1: $scope.enddate1selection = '+' + (key.Count - 1) + 'd';
                            break;
                        case 2: $scope.enddate2selection = '+' + (key.Count - 1) + 'd';
                            break;
                        case 4: $scope.enddate4selection = '+' + (key.Count - 1) + 'd';
                            break;
                    }
                }
                else {
                    switch (key.sno) {
                        case 1: $scope.enddate1selection = '';
                            break;
                        case 2: $scope.enddate2selection = '';
                            break;
                        case 4: $scope.enddate4selection = '';
                            break;
                    }
                }

            });

        }
    });

    $scope.gridSpaceAllocOptions = {
        columnDefs: $scope.columDefsAlloc,
        enableCellSelection: false,
        /* enableFilter: true,*/
        rowData: null,
        rowHeight: 48,
        cellClass: 'grid-align',
        /*   enableSorting: true,*/
        angularCompileRows: true,
        /*rowSelection: 'single',*/
        headerHeight: 40,
        enableColResize: true,

    }


    function createFromTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.FROM_TIME');
        newTime.type = "text";
        newTime.id = 'fromtime' + params.rowIndex;
        newTime.className = "pickDate form-control mt-1";
        if (params.data.FROM_TIME_CURR == undefined) {
            params.data.FROM_TIME_CURR = params.data.FROM_TIME;
        }
        if (params.data.TO_TIME_CURR == undefined) {
            params.data.TO_TIME_CURR = params.data.TO_TIME;
        }
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime = moment(e.target.value, 'HH:mm').add(total, 'hours').format('HH:mm');
                var selectedspaceexist = _.find($scope.selectedSpaces,
                    function (x) {
                        if (x.FROM_TIME === e.target.value && x.SPC_ID === params.data.SPC_ID) {

                            return x
                        }
                    });
                if (selectedspaceexist) {
                    showNotification('error', 8, 'bottom-right', "Time slot not available");
                    e.target.value = "";
                    return;
                }
                params.data.TO_TIME = endTime;
            });
        return newTime;
    }

    function createToTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.TO_TIME');
        newTime.type = "text";
        newTime.id = 'totime' + params.rowIndex;
        newTime.className = "pickDate form-control mt-1";
        if (params.data.FROM_TIME_CURR == undefined) {
            params.data.FROM_TIME_CURR = params.data.FROM_TIME;
        }
        if (params.data.TO_TIME_CURR == undefined) {
            params.data.TO_TIME_CURR = params.data.TO_TIME;
        }
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime1 = moment(params.data.FROM_TIME, 'HH:mm').add(total, 'hours').format('HH:mm');
                var currentTime = moment(e.target.value, "HH:mm a");
                var startTime = moment(params.data.FROM_TIME, "HH:mm a");
                var endTime = moment(endTime1, "HH:mm a");
                currentTime.toString();
                startTime.toString();
                var c = currentTime.isBetween(startTime, endTime);  // false
                //if (!currentTime.isBetween(startTime, endTime)) {
                //    showNotification('error', 8, 'bottom-right', "To time should not be less than from time and not exceed business hours");
                //}
            });
        return newTime;
    }

    var fromdate, todate;

    function check_validattion_of_MultipleBooking(date, spcid) {
        var params = {
            FROM_DATE: date,
            SPC_ID: spcid
        };
        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/MaploaderAPI/SEAT_RESTRICTION_BOOKING",
            data: JSON.stringify(params),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $scope.Remaining = response.data[0].Remaining;
            }
        });
    }


    function createFromDatePicker(params) {

        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.FROM_DATE');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate form-control mt-1";
        fromdate = params.data.FROM_DATE;
        todate = params.data.TO_DATE;
        if (params.data.FROM_DATE_CURR == undefined) {
            params.data.FROM_DATE_CURR = params.data.FROM_DATE;
        }
        if (params.data.TO_DATE_CURR == undefined) {
            params.data.TO_DATE_CURR = params.data.TO_DATE;
        }
        $('#TO_DATE').val(params.data.TO_DATE);

        // Use Moment.js to format the existing date to 'MM/DD/YYYY'
        var formattedFromDate = moment(fromdate).format('MM/DD/YYYY');
        newDate.value = formattedFromDate;

        // Set the format for newDate
        //$(newDate).datepicker({
        //    format: 'mm/dd/yyyy',
        //    autoclose: true,
        //    todayHighlight: true,
        //    startDate: new Date(),
        //});

        switch (params.data.SPCTYPE) {
            case "1": $scope.enddateselection = $scope.enddate1selection; break;
            case "2": $scope.enddateselection = $scope.enddate2selection; break;
            case "4": $scope.enddateselection = $scope.enddate4selection; break;
        }

        $(newDate).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: new Date(),
            endDate: $scope.enddateselection
        }).on
        //("input change", function (e) {
        //    // Clear the value
        //    $('#TO_DATE').val('');

        //    var prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });

        //    if (prefvalue != undefined) {
        //        var checkdate = moment(e.target.value).add(90, 'day').format('MM/DD/YYYY');
        //        $('#TO_DATE').val(checkdate);

        //        fromdate = e.target.value;
        //        todate = checkdate;
        //        params.data.TO_DATE = checkdate;
        //        /*  $('#TO_DATE').change();*/
        //        var d = e.target.value;

        //        $('#TO_DATE').datepicker({
        //            format: 'mm/dd/yyyy',
        //            autoclose: true,
        //            todayHighlight: true,
        //            startDate: d,
        //            endDate: checkdate,
        //        });

        //        $('#TO_DATE').val();

        //    }
        //});

        return newDate;
    }


    function createToDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.TO_DATE');
        //newDate.setAttribute('ng-disabled', 'datedisable==true');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate form-control mt-1";
        var prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
        //console.log(prefvalue);
        if (params.data.FROM_DATE_CURR == undefined) {
            params.data.FROM_DATE_CURR = params.data.FROM_DATE;
        }
        if (params.data.TO_DATE_CURR == undefined) {
            params.data.TO_DATE_CURR = params.data.TO_DATE;
        }
        if (prefvalue != undefined) {

            $scope.datedisable = true;
        }
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: new Date(),
            endDate: $scope.enddateselection
        }).on
        return newDate;
    }
    /*$scope.SDate = 1;*/
    $scope.EditFunction = function (deta) {
        debugger;
        deta.active = false;
        progress(0, 'Loading...', true);
        var emp = deta.Employee_Name.split("/");
        var fromDate = new Date(deta.FROM_DATE);
        $scope.AllocateEnabledStatus = false;
        // Format the date as 'mm/dd/yyyy' using AngularJS date filter
        var formattedFromDate = $filter('date')(fromDate, 'MM/dd/yyyy');
        var todate = new Date(deta.TO_DATE);
        // Format the date as 'mm/dd/yyyy' using AngularJS date filter
        var formattedToDate = $filter('date')(todate, 'MM/dd/yyyy');
        $scope.selectddata = [];
        $scope.selectddata.push({
            SPC_ID: deta.Space_ID,
            SSAD_SRN_REQ_ID: deta.SSAD_SRN_REQ_ID,
            SSA_SRNREQ_ID: deta.SSA_SRNREQ_ID,
            AUR_ID: emp[0].trim(),
            AUR_NAME: emp[1].trim(),
            FROM_DATE: formattedFromDate,
            TO_DATE: formattedToDate,
            FROM_TIME: deta.FROM_TIME,
            TO_TIME: deta.TO_TIME,
            SEAT_STATUS: deta.SEAT_STATUS,
            STATUS: deta.SSAD_STA_ID,
            VERTICAL: deta.VERTICAL,
            SH_CODE: deta.SH_CODE,
            Cost_Center_Code: deta.Cost_Center_Name,
            STACHECK: deta.STACHECK,
            SPCTYPE: deta.SPCTYPE,
            ticked: true
        });


        /* var allocate = L.easyButton('fa fa-user fa-2x', function () {*/


        $scope.validate = false;
        progress(0, 'Loading...', true);

        //$scope.enddateselection = '';
        if ($scope.selectddata.length != 0) {

            //if (selectddata[0].SPCTYPE == 4) {

            $('#EditFunction').modal('show');

            if ($scope.selectddata[0].SPCTYPE == 4) {
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('FROM_TIME', true);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('TO_TIME', true);
                $scope.gridSpaceAllocOptions.api.setRowData($scope.selectddata);
            }
            else {
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', true);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('FROM_TIME', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('TO_TIME', false);
                $scope.gridSpaceAllocOptions.api.setRowData($scope.selectddata);
            }


            progress(0, '', false);
            $('#SeatAllocation').modal('show');
        }
        $scope.Shift.push({
            LCM_CODE: deta.LCM_CODE,
            LCM_NAME: deta.LCM_NAME,
            CTY_CODE: deta.CTY_CODE,
            CNY_CODE: deta.CNY_CODE
        })
        $.ajax({
            url: window.location.origin + "/api/SpaceRequisition/GetShifts",
            type: "POST",
            data: JSON.stringify($scope.Shift),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {

                if ($scope.selectddata[0].SPCTYPE == 1) {
                    $scope.ShiftData = _.filter(result.data, value => value.SH_SEAT_TYPE == 1)
                }
                else if ($scope.selectddata[0].SPCTYPE == 2) {
                    $scope.ShiftData = _.filter(result.data, value => value.SH_SEAT_TYPE == 2)
                }



            }
        });

        $scope.gridSpaceAllocOptions.api.refreshCells();
        /*console.log('Updated data:', selectddata);*/
        console.log('Grid API:', $scope.gridSpaceAllocOptions.api);
        $scope.gridSpaceAllocOptions.api.refreshCells();

    };




    $scope.AllocateSeats = function () {
        debugger;
        var selectddata = $scope.selectddata
        if ($scope.selectddata.length > 0) {
            SpaceSeatBookingHistroyService.allocateSeatsValidation($scope.selectddata).then(function (response1) {
                console.log(response1);
                if (response1.data == null) {
                    if ($scope.selectddata[0].FROM_DATE != '' && $scope.selectddata[0].TO_DATE != '') {
                        if (moment($scope.selectddata[0].TO_DATE) >= moment($scope.selectddata[0].FROM_DATE)) {
                            if ($scope.selectddata[0].TO_TIME > $scope.selectddata[0].FROM_TIME) {

                                $.ajax({
                                    type: "POST",
                                    url: window.location.origin + "/api/MaploaderAPI/AllocateSeats",
                                    data: JSON.stringify({ selSpaces: selectddata }),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function () {
                                        showNotification('success', 8, 'bottom-right', 'Space Successfully allocated ');
                                        $('#EditFunction').modal('hide');
                                        /* $scope.selectddata = [];*/
                                        /*updateSummaryCount();*/
                                        $scope.gridSpaceAllocOptions.api.refreshView();
                                        /*progress(0, 'Loading...', false);*/
                                        loaddata();
                                    },

                                    error: function () {
                                        showNotification('error', 8, 'bottom-right', 'Something went wrong');
                                    }

                                });
                            }
                            else {
                                showNotification('error', 10, 'bottom-right', 'To time  should be greater than from time');
                                $scope.ActiveButton = false;
                            }
                        }
                        else {
                            showNotification('error', 10, 'bottom-right', 'To date should be greater than or equal to from date');

                        }


                    }
                    else {
                        showNotification('error', 10, 'bottom-right', 'From date and To date should not be Empty');
                        /* $scope.AllocateEnabledStatus = true;*/
                        $scope.ActiveButton = false;
                    }

                }

                else {
                    $('#errorModal').find('.modal-title').text('Error');
                    $('#errorModal').find('.modal-body').text('Since this seat is reserved for the day, choose a different date or another vacant seat.');
                    $('#errorModal').modal('show');
                }

            });

        }

    }




    $scope.release = function (det) {
        det.active = false;
        progress(0, 'Loading...', true);
        var emp = det.Employee_Name.split("/");
        var selectddata = [];
        selectddata.push({
            SPC_ID: det.Space_ID,
            SSAD_SRN_REQ_ID: det.SSAD_SRN_REQ_ID,
            SSA_SRNREQ_ID: det.SSA_SRNREQ_ID,
            AUR_ID: emp[0].trim(),
            AUR_NAME: emp[1].trim(),
            FROM_DATE: det.FROM_DATE,
            TO_DATE: det.TO_DATE,
            FROM_TIME: det.FROM_TIME,
            TO_TIME: det.TO_TIME,
            ticked: true
        });



        if (parseInt($scope.VerticalWiseAlloc.SYSP_VAL1) == 1) {
            var dataobj = { sad: selectddata, reltype: 1003 };
        }
        else {
            var dataobj = { sad: selectddata, reltype: 1002 };

        }


        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/MaploaderAPI/ReleaseSelectedseat",
            data: JSON.stringify(dataobj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', 'Space Successfully Released ');
            },
            error: function () {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Something went wrong');
            }
        });

        loaddata();

    }


});
