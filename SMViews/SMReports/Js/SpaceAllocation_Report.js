﻿app.service("SpaceAllocationReportService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    this.GetGriddata = function (SpaceAllocation) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocationReport/GetAllocGrid', SpaceAllocation)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAllocationReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('SpaceAllocationReportController', ['$scope', '$q', '$http', 'SpaceAllocationReportService', 'UtilityService', '$timeout','$filter', function ($scope, $q, $http, SpaceAllocationReportService, UtilityService, $timeout, $filter) {
    $scope.SpaceAllocation = {};
    $scope.Type = [];
    $scope.customColumns = [];
    $scope.DocTypeVisible = 0;
    $scope.HideColumns = 2;
    $scope.GridVisiblity = true;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.SpaceAllocation.FromDate = moment().format('MM/DD/YYYY');
    $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
    $scope.columnDefs = [
        { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align',  suppressMenu: true, sno: 1 },
        { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 2 },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 160, sno: 3 },
        { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 4 },
        { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 5 },
        { headerName: "Space Type", field: "SPACE_TYPE", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 6 },
        { headerName: "", field: "BHO_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 7 },
        { headerName: "", field: "BHT_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 8 },
        { headerName: "", field: "ENTITY", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 9 },
        { headerName: "", field: "CHILD_ENTITY", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 10 },
        { headerName: "", field: "VER_NAME", cellClass: 'grid-align', width: 110, rowGroupIndex: 0, sno: 11 },
        { headerName: "", field: "DEP_NAME", cellClass: 'grid-align', width: 110, rowGroupIndex: 1, sno: 12 },
        { headerName: "Shift", field: "SHIFT", cellClass: 'grid-align', width: 200, sno: 13 },
        { headerName: "Allocated Count", field: "ALLOC_COUNT", cellClass: 'grid-align', width: 110, suppressMenu: true, pinned: 'right', aggFunc: 'sum', sno: 14 },
        { headerName: "Occupied Count", field: "OCCUP_COUNT", cellClass: 'grid-align', width: 110, suppressMenu: true, pinned: 'right', aggFunc: 'sum', sno: 15 },
        { headerName: "Allocated but Vacant", field: "VAC_COUNT", cellClass: 'grid-align', width: 110, suppressMenu: true, pinned: 'right', aggFunc: 'sum', sno: 16 }
        //{ headerName: "Request Type", field: "REQUEST_TYPE", cellClass: 'grid-align', width: 110, suppressMenu: true }
    ];

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.SpaceAllocation.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 500);

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.gridOptions.columnApi.getColumn("BHO_NAME").colDef.headerName = $scope.BsmDet.BH1;
            $scope.gridOptions.columnApi.getColumn("BHT_NAME").colDef.headerName = $scope.BsmDet.BH2;
            $scope.gridOptions.columnApi.getColumn("ENTITY").colDef.headerName = $scope.BsmDet.PE;
            $scope.gridOptions.columnApi.getColumn("CHILD_ENTITY").colDef.headerName = $scope.BsmDet.CE;
            $scope.gridOptions.api.refreshHeader();
        }
    });

    

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
                setTimeout(function () {
                    var lastRow = -1;
                    var dataObj = {
                        FromDate: $scope.SpaceAllocation.FromDate,
                        ToDate: $scope.SpaceAllocation.ToDate,
                        CNP_NAME: $scope.SpaceAllocation.CNP_NAME[0].CNP_ID,
                        Type: $scope.flag
                    };
                    var fromdate = moment($scope.SpaceAllocation.FromDate);
                    var todate = moment($scope.SpaceAllocation.ToDate);
                    if (fromdate > todate) {
                        $scope.GridVisiblity = false;
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                        return;
                    }
                    else {

                            SpaceAllocationReportService.GetGriddata(dataObj).then(function (data) {
                                $scope.gridata = data;
                                if ($scope.gridata == null) {
                                    $scope.GridVisiblity2 = false;
                                    $scope.gridOptions.api.setRowData([]);
                                    progress(0, 'Loading...', false);
                                }
                                else {
                                    $scope.GridVisiblity2 = true;
                                    $scope.gridOptions.api.setRowData([]);
                                    $scope.gridOptions.api.setRowData($scope.gridata);
                                    progress(0, 'Loading...', false);
                                }
                                $scope.AllocChartDetails(dataObj);
                                setTimeout(function () {
                                    progress(0, 'Loading...', false);
                                }, 500);
                            }, function (error) {
                                console.log(error);
                            });

                        
                    }

                }, 1);
    };


    $scope.ColumnNames = [];

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value)
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }


    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        groupHeaders: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        showToolPanel: true
    };
    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $scope.chart;
    $scope.chart = c3.generate({
        data: {
            x: 'VER_NAME',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
            labels: true
        },
        legend: {
            show: false,
        },
        axis: {
            x: {
                type: 'category',
                height: 130,
                show: true,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            },
            y: {
                show: true,
                label: {
                    text: 'Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    function toggle(id) {
        chart.toggle(id);
    }

    d3.select('.container').insert('div', '.chart').attr('class', 'legend').selectAll('div').data({
        columns: [],
        cache: false,
        type: 'bar',
        empty: { label: { text: "Sorry, No Data Found" } },
        labels: true
    })
        .enter().append('div')
        .attr('data-id', function (id) {
            return id;
        })
        .html(function (id) {
            return id;
        })
        .each(function (id) {
            //d3.select(this).append('span').style
            d3.select(this).append('span').style('background-color', chart.color(id));
        })
        .on('mouseover', function (id) {
            chart.focus(id);
        })
        .on('mouseout', function (id) {
            chart.revert();
        })
        .on('click', function (id) {
            $(this).toggleClass("c3-legend-item-hidden")
            chart.toggle(id);
        });


    $scope.AllocChartDetails = function (spcData) {
        $scope.Columndata = [];
        $http({
            url: UtilityService.path + '/api/SpaceAllocationReport/GetDetailsCount',
            method: 'POST',
            data: spcData
        }).then(function (response) {
            console.log(response);
            $scope.Columndata.push(response.data.data.Columnnames);
            angular.forEach(response.data.data.Details, function (value, key) {
                $scope.Columndata.push(value);
            });
            $scope.chart.unload();
            $scope.chart.load({ columns: $scope.Columndata });
            $scope.ColumnNames = response.data.Columnnames;
        });
        setTimeout(function () {
            $("#AllocGraph").empty();
            $("#AllocGraph").append($scope.chart.element);
        }, 700);

    }
    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" },
        { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" },
        { title: "Floor", key: "FLR_NAME" }, { title: "Space Type", key: "SPACE_TYPE" },
        { title: "Entity", key: "ENTITY" }, { title: "Child Entity", key: "CHILD_ENTITY" },
        { title: "Vertical", key: "VER_NAME" }, { title: "Department", key: "DEP_NAME" },
        { title: "Shift", key: "SHIFT" }, { title: "Allocated Count", key: "ALLOC_COUNT" },
        { title: "Occupied Count", key: "OCCUP_COUNT" }, { title: "Vacant Count", key: "VAC_COUNT" },
        { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A3');
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("SpaceAllocationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);

    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceAvailabilityReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
        //var Filterparams = {
        //    skipHeader: false,
        //    skipFooters: false,
        //    skipGroups: false,
        //    allColumns: false,
        //    onlySelected: false,
        //    columnSeparator: ',',
        //    fileName: "SpaceAllocationReport.csv"
        //};
        //$scope.gridOptions.api.exportDataAsCsv(Filterparams);
        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        //}, 1000);
    }
    $scope.gridOptions.onColumnVisible = function (event) {
        console.log(event);
        if (event.visible) {
            $scope.HideColumns = 0;
            //console.log(event.column.colId + ' was made visible');     

            $scope.gridOptions.columnDefs.push(
                {
                    headerName: event.column.colDef.headerName,
                    field: event.column.colId,
                    cellClass: "grid-align",
                    width: 110,
                    sno: event.column.colDef.sno,
                    suppressMenu: true
                }
            );
            $scope.gridOptions.columnDefs.visible = true;
            _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
        } else {
            //console.log($scope.gridOptions.columnDefs.length);
            $scope.HideColumns = 1;
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                    $scope.gridOptions.columnDefs[i].visible = false;
                    $scope.gridOptions.columnDefs.splice(i, 1);
                }
            }
        }
    };

    $scope.GenReport = function (Customized, Type) {
        var fromdate = moment($scope.SpaceAllocation.FromDate);
        var todate = moment($scope.SpaceAllocation.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        progress(0, 'Loading...', true);
        saobj = {};
        saobj.CNP_NAME = $scope.SpaceAllocation.CNP_NAME[0].CNP_ID;
        saobj.Type = Type;
        saobj.FromDate = $scope.SpaceAllocation.FromDate;
        saobj.ToDate = $scope.SpaceAllocation.ToDate;
        saobj.VERTICAL = $scope.BsmDet.Parent;
        saobj.COSTCENTER = $scope.BsmDet.Child;
        saobj.BH1 = $scope.BsmDet.BH1;
        saobj.BH2 = $scope.BsmDet.BH2;
        saobj.PE = $scope.BsmDet.PE;
        saobj.CE = $scope.BsmDet.CE;

        $http({

            url: UtilityService.path + '/api/SpaceAllocationReport/GetSpaceAllocationReport',
            method: 'POST',
            data: saobj,
            responseType: 'arraybuffer'

        }).then(function (data, status, headers, config) {
            var file = new Blob([data.data], {
                type: 'application/' + Type
            });
            var fileURL = URL.createObjectURL(file);
            $("#reportcontainer").attr("src", fileURL);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = 'SpaceAllocationReport.' + Type;
            document.body.appendChild(a);
            a.click();
            progress(0, '', false);
        }), function (error,data, status, headers, config) {

        };

    }

    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.SpaceAllocation.FromDate = "";
                $scope.SpaceAllocation.ToDate = "";
                break;
            case 'TODAY':
                $scope.SpaceAllocation.FromDate = moment().format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.SpaceAllocation.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.SpaceAllocation.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.SpaceAllocation.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.SpaceAllocation.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.SpaceAllocation.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.SpaceAllocation.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
}]);