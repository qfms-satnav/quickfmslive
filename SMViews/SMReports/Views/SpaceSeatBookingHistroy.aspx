﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" />
    <style>
    .ag-cell:has(input[type="text"]) {
       padding:0 5px ;
    }

    .ag-cell:has(select) {
       padding:0 5px !important;
    }

  </style>
    <script type="text/javascript" defer>

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>

<body data-ng-controller="SpaceSeatBookingHistoryController" class="amantra">
    <div ba-panel-title="Space Seat Book" ba-panel-class="with-scroll">
        <div class="widgets">
            <h3 class="page__title">My Seat Booking History </h3>
        </div>
        <div class="card">
            <div class="card-body">
                <form id="form1" name="SpaceSeatBookingHistory" novalidate>
                </form>

                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </div>
                </div>
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="EditFunction" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="align-items-center justify-content-between">
                                    <h5 class="modal-title" id="H1">Edit Details
                        <label class="text-danger">{{FlipkartVaccinateName}}</label></h5>
                                    <h6 class="modal-title text-right">{{Details}}</h6>
                                </div>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form id="Form2" name="frmSpaceAlloc">
                                <div class="modal-body" style="padding: 5px;">
                                    <%-- <div class="row" id="HotDesking">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label>
                                                <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="1" checked />
                                                One Day
                                            </label>
                                            <label data-ng-show="hotdeskspecifications">
                                                <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="2" />
                                                One Week
                                            </label>
                                            <label data-ng-show="hotdeskspecifications">
                                                <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="3" />
                                                One Month
                                            </label>
                                        </div>
                                    </div>--%>

                                    <%--<div class="row mb-1">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="input-group" ng-show="selectddata.length != 0">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text bg-transparent text-light"><i class="fa fa-search"></i></span>
                                                </span>
                                                <input type="text" id="txtCountFilter" class="form-control" placeholder="Filter..." />
                                            </div>
                                        </div>
                                    </div>--%>

                                    <div class="row mb-3">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-show="selectddata.length != 0">
                                                <div data-ag-grid="gridSpaceAllocOptions" class="ag-blue" style="height: 250px; width: auto"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="errorModal" class="modal" tabindex="-1">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Error</h5>
                                                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Since this seat is reserved for the day, choose a different date or another vacant seat.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- <div class="row mb-3" ng-show="selectddata.length> 0">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="d-flex align-items-end justify-content-between">
                                                            <h3 class="page__title">Seat(s) allocated to {{SelectedEmp.AUR_NAME}}</h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <table id="tblempalloc" width="100%" class="table GridStyle">
                                                            <tr>
                                                                <th>Space ID</th>
                                                                <th>Location</th>
                                                                <th>Tower</th>
                                                                <th>Floor</th>
                                                                <th>From Date</th>
                                                                <th>To Date</th>
                                                                <th>Shift</th>
                                                                <th></th>
                                                            </tr>
                                                            <tr ng-repeat="emp in EmpAllocSeats">
                                                                <td>{{emp.SPC_NAME}}</td>
                                                                <td>{{emp.LOCATION}}</td>
                                                                <td>{{emp.TOWER}}</td>
                                                                <td>{{emp.FLOOR}}</td>
                                                                <td>{{emp.FROM_DATE}}</td>
                                                                <td>{{emp.TO_DATE}}</td>
                                                                <td>{{emp.SH_CODE}}</td>
                                                               <td>
                                                        <input type="button" id="btnRelease" value="Release" ng-click="ReleaseEmp(emp)" ng-show="EmployeeRelease" />
                                                        <input type="button" id="btnRelease1" value="Release from Sub Function" ng-click="ReleaseEmp(emp)" ng-show="UptoVetrialRelease" />
                                                    </td>
                                                          </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="text-right">
                                                            <button type="button" class="btn btn-primary" data-ng-hide="ProallocEnabledStatus" ng-click="Proceedalloc(alloc)" <%-- ng-click="EmpAllocSeats = []"--%><%-->Proceed to allocate additional seat</button>--%>
                                    <%--</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>

                                    <%--<div class="row mb-3" data-ng-show="empDateArr.length> 0" style="display: block !important;">
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="d-flex align-items-end justify-content-between">
                                                            <h3 class="page__title">Seat(s) already allocated</h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <table id="tblempalloc1" width="100%" class="table table-condensed table-bordered table-hover table-striped">
                                                            <tr>
                                                                <th>Space ID</th>
                                                                <th>Employee</th>
                                                                <th>From Date</th>
                                                                <th>To Date</th>
                                                                <th>Shift</th>
                                                            </tr>
                                                            <tr data-ng-repeat="emp in empDateArr">
                                                                <td>{{emp.SPC_ID}}</td>
                                                                <td>{{emp.VERTICAL}} - ({{emp.AUR_ID}})</td>
                                                                <td>{{emp.FROM_DATE | date:'MM/dd/yyyy'}}</td>
                                                                <td>{{emp.TO_DATE | date:'MM/dd/yyyy'}}</td>
                                                                <td>{{emp.SH_CODE}}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--%>

                                    <div class="box-footer text-right" data-ng-show="selectddata.length != 0">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" data-ng-click="CloseEmpFn()" data-ng-if="!ReleaseFrom">Close</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal" data-ng-click="CloseFn()" data-ng-if="ReleaseFrom">Close</button>
                                        <button type="button" data-ng-click="AllocateSeats()" id="btnAllocateSeats" data-ng-hide="AllocateEnabledStatus" class="btn btn-primary" style="width: 100px">Update</button>
                                        <%--<input type="button" ng-click="AllocateSeats()" class="btn btn-primary" value="Allocate Seats" ng-disabled="OnEmpAllocSeats" />

<%--                                            <div class="btn-group dropup" data-ng-if="ReleaseFrom">
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Release From {{relfrom.Name}}  <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a ng-click="Release(1004,'Employee')">Employee</a></li>
                                                    <li><a ng-click="Release(1003,BsmDet.Child)">{{BsmDet.Child}}</a></li>
                                                    <li><a ng-click="Release(1002,BsmDet.Parent)">{{BsmDet.Parent}}</a></li>
                                                    <li><a ng-click="Release(1052,'Blocked')">Blocked</a></li>
                                                    <%--<li role="separator" class="divider"></li>
                                            <li><a href="#" ng-click="relfrom = 'Complete Release'; relfrom = 1001">Complete Release</a></li>
                                              <%--  </ul>
                                            </div>--%>
                                    </div>

                                    <%-- <div class="box-footer text-right" ng-show="(EmpAllocSeats.length <= 0 && GetRoleAndRM[0].AUR_ROLE==6) || ( GetRoleAndRM[0].AUR_ROLE==6 && MultipleBooking.SYSP_VAL1==1)">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <input type="button" ng-click="AllocateSeats()" data-ng-hide="AllocateEnabledStatus" class="btn btn-primary" value="Allocate Seats" data-ng-disabled="ActiveButton" />
                                            <div class="btn-group dropup">
                                                <button type="button" class="btn btn-primary" ng-click="Release(1002,BsmDet.Parent)" data-ng-hide="UptoVetrialRelease && GetRoleAndRM[0].AUR_ROLE == 6">
                                                    Release
                                                </button>
                                            </div>
                                        </div>--%>

                                    <%-- </div>--%>
                                </div>
                                <%--<div class="modal-footer"></div>--%>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%-- <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>--%>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>

    <script type="text/javascript">
        var empid = '<%= Session["UID"] %>';
    </script>


    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Utility.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script src="../Js/SpaceSeatBookingHistory.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
</body>
</html>

