﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>





</head>
<body data-ng-controller="StateWiseAllocationReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Summary Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" name="CustomizedReport" data-valid-submit="LoadData()" novalidate>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="margin-right: 55px;">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="margin-right: 55px;">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.AM_ST_NAME.$invalid}">
                                            <label class="control-label">State<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="State" data-output-model="Customized.State" data-button-label="icon AM_ST_NAME"
                                                data-item-label="icon AM_ST_NAME maker" data-on-item-click="GetCitiesByState()" data-on-select-all="StateSelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.State[0]" name="AM_ST_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.AM_ST_NAME.$invalid" style="color: red">Please select a city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" >
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="margin-right: 55px;">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="margin-right: 55px;">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Towers" data-output-model="Customized.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid" style="color: red">Please select a tower </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floors" data-output-model="Customized.Floors" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="GetVerticalByFloor()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FLR_NAME.$invalid" style="color: red">Please select a floor </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="margin-right: 55px;">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.VER_NAME.$invalid}">
                                            <label class="control-label">Mancomm <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Verticals" data-output-model="Customized.Verticals" data-button-label="icon VER_NAME"
                                                data-item-label="icon VER_NAME maker" data-on-item-click="GetCostcenterByMancom()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.Verticals[0]" name="VER_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.VER_NAME.$invalid" style="color: red">Please select a Mancomm </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="margin-right: 55px;">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CostCenter_Name.$invalid}">
                                            <label class="control-label">CostCenter <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="CostCenters" data-output-model="Customized.CostCenters" data-button-label="icon CostCenter_Name"
                                                data-item-label="icon CostCenter_Name maker" data-on-item-click="" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.CostCenters[0]" name="CostCenter_Name" style="display: none" required="" />
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CostCenter_Name.$invalid" style="color: red">Please select CostCenter </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="false">
                                        <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.Request_Type.$invalid}">
                                            <label class="control-label">Status Type <span style="color: red;">*</span></label>

                                            <select id="Request_Type" name="Request_Type" required="" data-ng-model="Customized.Request_Type" class="form-control">
                                                <option value="">--Select--</option>
                                                <option ng-repeat="sta in statuslst" value="{{sta.Code}}">{{sta.Name}}</option>
                                            </select>
                                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Request_Type.$invalid" style="color: red">Please select status</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="false" style="margin-right: 55px;">
                                        <div class="form-group">
                                            <label for="txtcode">From Date</label>
                                            <div class="input-group date" id='fromdate'>
                                                <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="FromDate" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                </span>
                                            </div>
                                            <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>--%>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="false" style="margin-right: 55px;">
                                        <div class="form-group">
                                            <label for="txtcode">To Date</label>
                                            <div class="input-group date" id='todate'>
                                                <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="ToDate" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                </span>
                                            </div>
                                            <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>--%>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-sm-6 col-xs-12">
                                        <div class="box-footer text-right">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                                <div id="Tabular" class="row">
                                    <div class="col-md-12">
                                        <div class="panel-heading ">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a href="#tabSummary" data-ng-click="BindGrid_Summary()" data-toggle="tab">Summary 
                                                    <i data-ng-show="GridVisiblity" data-ng-click="GenReportSummery('xls')" data-toggle="tooltip" title="Export to Excel for summery" class="excel fa fa-file-excel-o fa-2x pull-right" style="font-size: 20px;"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tabHistory" data-ng-click="Consolidated()" data-toggle="tab">Consolidated
                                                    <i data-ng-show="GridConsolidated" data-ng-click="GenReport('xls')" id="excel" data-toggle="tooltip" title="Export to Excel for consolidated" class="fa fa-file-excel-o fa-2x pull-right" style="font-size: 20px;"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tabMancom" data-ng-click="BindGrid_Mancom()" data-toggle="tab">Mancom
                                                    <i data-ng-show="GridVisiblitymancom" data-ng-click="GenReportmancom('xls')" data-toggle="tooltip" title="Export to Excel for mancon" class="excel fa fa-file-excel-o fa-2x pull-right" style="font-size: 20px;"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tabProject" data-ng-click="BindGrid_Project()" data-toggle="tab">CostCenter Wise
                                                    <i data-ng-show="GridVisiblityproject" data-ng-click="GenReportproject('xls')" data-toggle="tooltip" title="Export to Excel for project" class="excel fa fa-file-excel-o fa-2x pull-right" style="font-size: 20px;"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tabLocation" data-ng-click="BindGrid_Location()" data-toggle="tab">Location Wise
                                                   <i data-ng-show="GridVisiblitylocation" data-ng-click="GenReportlocation('xls')" data-toggle="tooltip" title="Export to Excel for location" class="excel fa fa-file-excel-o fa-2x pull-right" style="font-size: 20px;"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div class="tab-pane fade in active" id="tabSummary">
                                                    <input type="text" class="form-control" id="filtersummery" placeholder="Filter by any..." style="width: 25%" />
                                                    <div data-ag-grid="gridOptions1" class="ag-blue" style="height: 310px; width: auto"></div>
                                                </div>
                                                <div class="tab-pane fade" id="tabMancom">
                                                    <input type="text" class="form-control" id="filtermanom" placeholder="Filter by any..." style="width: 25%" />
                                                    <div data-ag-grid="gridmancom" class="ag-blue" style="height: 310px; width: auto"></div>
                                                </div>
                                                <div class="tab-pane fade" id="tabLocation">
                                                    <input type="text" class="form-control" id="filteredloc" placeholder="Filter by any..." style="width: 25%" />
                                                    <div data-ag-grid="gridLocation" class="ag-blue" style="height: 310px; width: auto"></div>
                                                </div>
                                                <div class="tab-pane fade" id="tabProject">
                                                    <input type="text" class="form-control" id="filteredproj" placeholder="Filter by any..." style="width: 25%" />
                                                    <div data-ag-grid="Options" class="ag-blue" style="height: 310px; width: auto"></div>
                                                </div>
                                                <div class="tab-pane fade" id="tabHistory">
                                                    <input type="text" class="form-control" id="filtertxt" placeholder="Filter by HistoryData..." style="width: 25%" />
                                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="Graphicaldiv">
                                    <div id="SpaceConsolidatedGraph">&nbsp</div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';

    </script>
    <script defer src="../../Utility.min.js"></script>
    <script src="../Js/StateWiseAllocationReport.js"></script>
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

        }
    </script>
</body>
</html>
