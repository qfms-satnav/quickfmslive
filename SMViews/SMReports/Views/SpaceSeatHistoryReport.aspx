﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }
    </style>



</head>
<body data-ng-controller="SpaceSeatHistoryController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Space Booking History Report</h3>
            </div>
            <div class="card">
                <form id="form1" name="SpaceSeatHistoryReport" novalidate>
                    <div class="clearfix">

                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode" class="lang-change">Select Type</label>
                                <select name="pm" ng-options="pm.Name for pm in Types" ng-model="SpaceSeatHistory.Type" class="selectpicker">
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Quick Select <span style="color: red;">**</span></label>
                                <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                    <option value="SELECT">Select range</option>
                                    <option value="TODAY">Today</option>
                                    <option value="YESTERDAY">Yesterday</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 30 Days</option>
                                    <option value="THISMONTH">This Month</option>
                                    <option value="LASTMONTH">Last Month</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="SpaceSeatHistory.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="SpaceSeatHistoryReport.$submitted && SpaceSeatHistoryReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="SpaceSeatHistory.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="SpaceSeatHistoryReport.$submitted && SpaceSeatHistoryReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 18px;">
                            <div class="box-footer text-right">
                                <input type="submit" value="Search" data-ng-click="LoadData()" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                    </div>
                </form>

                <div class="row" style="padding-left: 30px;">
                    <%--<input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="input-group" style="width: 100%">
                                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                <div class="input-group-btn">
                                    <%-- <button class="btn btn-primary custom-button-color" data-ng-click="LoadData(1)" type="submit">
                                                        <i class="glyphicon glyphicon-search"></i>
                                                    </button>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6 col-xs-12" style="padding-top: 10px">
                            <div class="form-group text-right">
                                <a ng-class="{'adisabled': !usable}" id="anchExportExcel"><i id="I3" style="padding-right: 20px" data-toggle="tooltip" title="Export to Excel"
                                    class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto; padding-top: 10px"></div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
        }
    </script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../../Utility.js"></script>
    <script defer src="../Js/SpaceSeatHistoryReport.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
</body>
</html>
