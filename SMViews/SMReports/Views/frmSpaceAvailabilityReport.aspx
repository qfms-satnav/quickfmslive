﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        /*  .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-container {
            color: black;
        }*/

        .modal-header-primary {
            color: #1D1C1C;
            /*padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        #wrd {
            color: #4813CA;
        }

        #exl {
            color: #2AE214;
        }

        #pf {
            color: #FF0023;
        }

        /*.ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }*/
    </style>
</head>
<body data-ng-controller="SpaceAvailabilityReportController" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Space Allocation" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Space Availability Report</h3>
            </div>
            <div class="card">
                <%-- <div class="card-body" style="padding-right: 10px;">--%>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                        <div class="form-group">
                            <label class="control-label">Company</label>
                            <div isteven-multi-select data-input-model="Company" data-output-model="SpaceAvailability.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="SpaceAvailability.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="frmAvailability.$submitted && frmAvailability.Company.$invalid" style="color: red">Please Select Company </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 18px">
                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="LoadData()" />
                    </div>
                </div>
                <br />
                <br />
                <div class="row" style="padding-left: 18px">
                    <div class="col-md-6">
                        <label>View In : </label>
                        <input id="viewswitch" type="checkbox" checked data-size="small"
                            data-on-text="<span class='fa fa-table'></span>"
                            data-off-text="<span class='fa fa-bar-chart'></span>" />
                        <%--<div class="col-md-12">
                                    &nbsp;
                                </div>--%>
                    </div>
                    <div class="col-md-6" id="table2" data-ng-show="gridata.length > 0">
                        <br />

                        <a data-ng-click="GenReport(SpaceAvailability,'doc')"><i id="word" data-ng-show="DocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>

                        <a data-ng-click="GenReport(SpaceAvailability,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                        <a data-ng-click="GenReport(SpaceAvailability,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                    </div>
                </div>
                <%--  </form>--%>
                <br />
                <form id="form2">
                    <div id="Tabular">
                        <div class="input-group" style="width: 20%">
                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                            <div class="input-group-btn">
                                <button class="btn btn-primary custom-button-color" type="submit" data-ng-click="LoadData()">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <%-- <div style="height: 565px">
                                        <div>--%>

                            <div class="col-md-12">
                                <%--<input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 250px; width: auto"></div>
                                <%--   </div>
                                    </div>--%>
                            </div>
                        </div>
                    </div>
                    <div id="Graphicaldiv">
                        <div id="AvalGraph">&nbsp</div>
                    </div>
                </form>
            </div>


        </div>
    </div>
    <%--  </div>
        </div>
    </div>--%>
    <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="height: 400px">
                    <div class="panel-group box box-primary" id="Div2">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="panel-heading ">
                            <h4 class="panel-title modal-header-primary" data-target="#collapseTwo">Vacant Seat Details</h4>
                            <form role="form" name="form3" id="form3">
                                <div class="clearfix">
                                    <div class="row" data-ng-show="popdata.length>0">
                                        <a data-ng-click="GenReportVacant(SpaceAvailability,'doc')"><i id="wrd" data-ng-show="PopDocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReportVacant(SpaceAvailability,'xlsx')"><i id="exl" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReportVacant(SpaceAvailability,'pdf')"><i id="pf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    </div>
                                    <div class="row">
                                        <div data-ag-grid="PopOptions" class="ag-blue" style="height: 300px; width: auto"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <%--<script defer src="../Js/SpaceAvailability_Report.js"></script>--%>
    <script defer src="../Js/SpaceAvailability_Report.js"></script>
    <%--<script defer src="../../Utility.js"></script>--%>
    <script defer src="../../Utility.min.js"></script>
</body>
</html>
