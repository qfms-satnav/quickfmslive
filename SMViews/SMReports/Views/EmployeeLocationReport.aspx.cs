﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SMViews_SMReports_Views_EmployeeLocationReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null || Session["UID"] == " ")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
    }
}