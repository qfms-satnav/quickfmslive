﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EntityWiseReport.aspx.cs" Inherits="SMViews_SMReports_Views_EntityWiseReport" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }

    </style>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>





</head>
<body data-ng-controller="EntityWiseReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Entity Wise Location Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" name="EntityWiseReport" data-valid-submit="LoadData()" novalidate>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': EntityWiseReport.$submitted && EntityWiseReport.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="EntityWise.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="EntityWiseReport.$submitted && EntityWiseReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': EntityWiseReport.$submitted && EntityWiseReport.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="EntityWise.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="EntityWiseReport.$submitted && EntityWiseReport.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-11 col-sm-6 col-xs-12">
                                        <br />
                                        <div class="box-footer text-right">
                                             
                                            <input type="Button" value="Search" data-ng-click="search()" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                   

                                </div>
                                 <div class="row" style="padding-left: 18px">
                                <div class="box-footer text-right"  style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    
                                    <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip"  title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    
                                </div>
                            </div>
                                 <div class="row" style="height: 310px; width: auto">
                                     <input id="filtertxt" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                    </div>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script defer src="../../../Scripts/jspdf.min.js"></script>
<script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
<script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
<script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
<script defer src="../../../Scripts/moment.min.js"></script>
<script defer>
    var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    var CompanySession = '<%= Session["COMPANYID"]%>';
</script>
<script defer src="../../Utility.min.js"></script>
<script src="../Js/EntityWiseReport.js"></script>


|</html>