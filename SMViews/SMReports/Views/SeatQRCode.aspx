﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
    <style>
        canvas {
            width: 70px !important;
            height: 70px !important;
            margin-top:5px;
        }

        #pdf {
            color: #FF0023;
        }
        .ag-blue .ag-row{
            height:80px !important;
        }
    </style>
</head>
<body data-ng-controller="SeatQRCodeController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">QR Code </h3>
            </div>
            <div class="card">
                <form role="form" name="QRCodeGenerator" data-valid-submit="Load()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': QRCodeGenerator.$submitted && QRCodeGenerator.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="SpaceQR.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceQR.Location" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="QRCodeGenerator.$submitted && QRCodeGenerator.LCM_NAME.$invalid">Please select location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': QRCodeGenerator.$submitted && QRCodeGenerator.TWR_NAME.$invalid}">
                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Tower" data-output-model="SpaceQR.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                    data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceQR.Tower" name="TWR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="QRCodeGenerator.$submitted && QRCodeGenerator.TWR_NAME.$invalid">Please select tower </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': QRCodeGenerator.$submitted && QRCodeGenerator.FLR_NAME.$invalid}">
                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Floor" data-output-model="SpaceQR.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                    data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SpaceQR.Floor" name="FLR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="QRCodeGenerator.$submitted && QRCodeGenerator.FLR_NAME.$invalid">Please select floor </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search</button>
                            <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                            <a data-ng-click="GenReport('pdf')" data-ng-show="PDFVisiblity"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                    </div>
                </form>
                <div class="col-md-12">
                    <div class="panel-heading ">
                        <ul class="nav nav-tabs">
                            <li><a class="active" ng-click="SpaceClicked()" data-toggle="tab">Space</a></li>
                            <li><a ng-click="FloorClicked()" data-toggle="tab">Floor</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active show" id="tabSummary">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/jspdf.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/FileSaver.min.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/qr_generator.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/qrcode.js"></script>
    <script defer src="../../../BlurScripts/BlurJs/jquery.qrcode.min.js"></script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/SeatQRCode.js"></script>
    <%--<script defer src="../Js/SeatQRCode.js"></script>--%>
</body>

</html>
