﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
      <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-container {
            color: black;
        }

        .modal-header-primary {
            color: #1D1C1C;
            /*padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        #wrd {
            color: #4813CA;
        }

        #exl {
            color: #2AE214;
        }

        #pf {
            color: #FF0023;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        /*#AvalGraph,
        .container {
            float: left;
        }

        .legend span {
            display: inline-block;
            margin-left: 7px;
            margin-right: 7px;
            padding: 5px;
        }

        .container .legned div {
            width: 100%;
        }

        .container .legend {
            width: 150px;
            height: 230px;
            overflow: scroll;
        }*/
    </style>
</head>
<body data-ng-controller="AreaWiseCostController" class="amantra">
    <div class="animsition" id="page-wrapper">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Area Wise Cost Report</h3>
                        </div>
                        <div class="panel-body">
                            <form id="form1" name="frmAreaWiseCostReport" data-valid-submit="LoadData('Monthly')" novalidate>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="AreaWiseCostReport.Country" data-button-label="icon CNY_NAME"
                                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AreaWiseCostReport.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.CNY_NAME.$invalid" style="color: red">Please select a country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="AreaWiseCostReport.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AreaWiseCostReport.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.LCM_NAME.$invalid}">
                                            <label class="control-label">Location<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="AreaWiseCostReport.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AreaWiseCostReport.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.LCM_NAME.$invalid" style="color: red">Please select a Location </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.VER_NAME.$invalid}">
                                            <label class="control-label">{{BsmDet.Parent}}<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Verticals" data-output-model="AreaWiseCostReport.Verticals" data-button-label="icon VER_NAME"
                                                data-item-label="icon VER_NAME maker" data-on-item-click="getCostcenterByVertical()" data-on-select-all="VerSelectAll()"
                                                data-on-select-none="VerSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AreaWiseCostReport.Verticals[0]" name="VER_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.VER_NAME.$invalid" style="color: red">Please select {{BsmDet.Parent}} </span>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.COST_CENTER_NAME.$invalid}">
                                            <label class="control-label">{{BsmDet.Child}}<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Costcenters" data-output-model="AreaWiseCostReport.Costcenters" data-button-label="icon Cost_Center_Name"
                                                data-item-label="icon Cost_Center_Name maker" data-on-select-all="CostcenterSelectAll()"
                                                data-on-select-none="VerSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>

                                            <input type="text" data-ng-model="AreaWiseCostReport.Costcenters[0]" name="COST_CENTER_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.COST_CENTER_NAME.$invalid" style="color: red">Please select {{BsmDet.Child}} </span>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.Name.$invalid}">
                                            <label class="control-label">Grouping Option <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="SearchOptions" data-output-model="SelOptions" data-button-label="icon Name"
                                                data-item-label="icon Name maker" data-on-item-click="setGridByOption()" data-on-select-all="OptionSelectAll()" data-helper-elements=""
                                                data-on-select-none="OptionSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SelOptions[0]" name="Name" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmAreaWiseCostReport.$submitted && frmAreaWiseCostReport.Name.$invalid" style="color: red">Please select an option to group </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="box-footer text-right">
                                        <div class="box-footer text-right">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                                <br />                              

                            </form>
                            <form id="form2">
                                <br />

                                  <div class="clearfix" data-ng-show="showgrid==1">                                    
                                    <a data-ng-click="GenReport(AreaWiseCostReport,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>                                   
                                </div>

                                <div style="height: 320px" data-ng-show="showgrid==1">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" style="height: 300px;" class="ag-blue"></div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="height: 400px">
                    <div class="panel-group box box-primary" id="Div2">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="panel-heading ">
                            <h4 class="panel-title modal-header-primary" data-target="#collapseTwo">Area wise Cost Details</h4>
                            <form role="form" name="form3" id="form3">
                                <div class="clearfix">
                                    <div class="row" data-ng-show="popdata.length>0">
                                        <a data-ng-click="GenAreaWiseCostReport(AreaWiseCostReport,'doc')"><i id="wrd" data-ng-show="PopDocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenAreaWiseCostReport(AreaWiseCostReport,'xls')"><i id="exl" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenAreaWiseCostReport(AreaWiseCostReport,'pdf')"><i id="pf" data-ng-show="PopDocTypeVisible==0" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    </div>
                                    <div class="row">
                                        <div data-ag-grid="PopOptions" class="ag-blue" style="height: 300px; width: auto"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--    <script defer src="../../../Scripts/tableExport.js"></script>
    <script defer src="../../../Scripts/jquery.base64.js"></script>--%>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.min.css" rel="stylesheet" />
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../../Utility.js"></script>
    <script defer src="../Js/AreaWiseCostReport.js"></script>
</body>

</html>
