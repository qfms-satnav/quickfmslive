﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>




<html lang="en" data-ng-app="QuickFMS">
<head id="Head"><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta name="description" /><meta name="author" /><title>
	Facility Management Services::a-mantra
</title><link href="ScriptCombiner.axd?s=css_scripts&v=1&type=text/css" rel="stylesheet" type="text/css"/><link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" /><link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" /><link href="../../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" /><link href="../../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" /><link href="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.css" rel="stylesheet" />

</head>
<body data-ng-controller="AddHerenceController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Daily Attendence Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Daily Attendance Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">


                                <form id="form1" name="AddHerence" data-valid-submit="LoadData()" novalidate>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': AddHerence.$submitted && AddHerence.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="AddHerence.Country" data-button-label="icon CNY_NAME"
                                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AddHerence.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="AddHerence.$submitted && AddHerence.CNY_NAME.$invalid" style="color: red">Please select a country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': AddHerence.$submitted && AddHerence.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="AddHerence.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AddHerence.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="AddHerence.$submitted && AddHerence.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': AddHerence.$submitted && AddHerence.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="AddHerence.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AddHerence.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="AddHerence.$submitted && AddHerence.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': AddHerence.$submitted && AddHerence.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Towers" data-output-model="AddHerence.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AddHerence.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="AddHerence.$submitted && AddHerence.TWR_NAME.$invalid" style="color: red">Please select a tower </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': AddHerence.$submitted && AddHerence.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floors" data-output-model="AddHerence.Floors" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="AddHerence.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="AddHerence.$submitted && AddHerence.FLR_NAME.$invalid" style="color: red">Please select a floor </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">From Date</label>
                                            <div class="input-group date" id='fromdate'>
                                                <input type="text" class="form-control" data-ng-model="AddHerence.FromDate" id="FromDate" name="FromDate" required="" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="AddHerence.$submitted && AddHerence.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">To Date</label>
                                            <div class="input-group date" id='todate'>
                                                <input type="text" class="form-control" data-ng-model="AddHerence.ToDate" id="ToDate" name="ToDate" required="" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="AddHerence.$submitted && AddHerence.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                        </div>
                                    </div>


                                    </div>

                                    <div class="col-md-12 col-sm-6 col-xs-12" style="padding-bottom:5px">
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                                    </form>

                              
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12" style ="padding-top:5px ">
                                        <div class="input-group" style="width: 100%">
                                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                           
                                        </div>
                                    </div>
                                     <div class="col-md-3  text-left" style ="padding-top:5px">
                                                <label>Total number of Requests Fetched: {{TotalReq}}</label>
                                            </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="padding-top:10px"">
                                        <div class="form-group text-right">
                                            <a ng-class="{'adisabled': !usable}" id="anchExportExcel"><i id="I3" style="padding-right: 20px" data-toggle="tooltip" title="Export to Excel"
                                            class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto;padding-top:10px"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="ScriptCombiner.axd?s=js_Scripts&v=1&type=text/javascript"></script>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>

     <script type="text/javascript" defer>
         function setup(id) {
             $('#' + id).datepicker({
                 format: 'mm/dd/yyyy',
                 autoclose: true,
                 todayHighlight: true
             });
         };
     </script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt"]);

    </script>
    
    <script src="../../Utility.js" defer></script>
    <script src="../Js/AddHerence.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script>

    </script>
</body>

</html>