﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmployeeLocationReport.aspx.cs" Inherits="SMViews_SMReports_Views_EmployeeLocationReport" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>





</head>
<body data-ng-controller="EmployeeLocationReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Branch Employee List</h3>
            </div>
            <div class="card" style="padding-right: 10px;">
                <form id="form1" name="BranchReport" data-valid-submit="LoadData()" novalidate>

                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BranchReport.$submitted && BranchReport.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Country" data-output-model="Branch.Country" data-button-label="icon CNY_NAME"
                                    data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Branch.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="BranchReport.$submitted && BranchReport.CNY_NAME.$invalid" style="color: red">Please select a country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BranchReport.$submitted && BranchReport.CTY_NAME.$invalid}">
                                <label class="control-label">City<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="Branch.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Branch.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="BranchReport.$submitted && BranchReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BranchReport.$submitted && BranchReport.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="Branch.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Branch.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="BranchReport.$submitted && BranchReport.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-6 col-xs-12">
                            <br />
                            <div class="box-footer text-right">

                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>


                    </div>
                    <div class="row" style="padding-left: 18px">
                        <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">

                            <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input id="filtertxt" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script defer src="../../../Scripts/jspdf.min.js"></script>
<script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
<script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
<script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
<script defer src="../../../Scripts/moment.min.js"></script>
<script defer>
    var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    var CompanySession = '<%= Session["COMPANYID"]%>';
</script>
<script defer src="../../Utility.min.js"></script>
<script src="../Js/EmployeeLocationReport.js"></script>


|</html>