﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
</head>

<body data-ng-controller="DuplicateRecordsReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Duplicate Records" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Duplicate Record Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>
                            <br />

                            <form id="Form1" name="frmDuplicateRecord" data-valid-submit="GetData()" novalidate>
                                <div class="row">
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDuplicateRecord.$submitted && frmDuplicateRecord.CNY_NAME.$invalid}">
                                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Country" data-output-model="DuplicateRecord.Country" data-button-label="icon CNY_NAME"
                                                    data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="DuplicateRecord.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmDuplicateRecord.$submitted && frmDuplicateRecord.CNY_NAME.$invalid">Please select country </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDuplicateRecord.$submitted && frmDuplicateRecord.CTY_NAME.$invalid}">
                                                <label class="control-label">City<span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="City" data-output-model="DuplicateRecord.City" data-button-label="icon CTY_NAME"
                                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="DuplicateRecord.City[0]" name="CTY_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmDuplicateRecord.$submitted && frmDuplicateRecord.CTY_NAME.$invalid">Please select city </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDuplicateRecord.$submitted && frmDuplicateRecord.LCM_NAME.$invalid}">
                                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Locations" data-output-model="DuplicateRecord.Locations" data-button-label="icon LCM_NAME"
                                                    data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="DuplicateRecord.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmDuplicateRecord.$submitted && frmDuplicateRecord.LCM_NAME.$invalid">Please select location </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDuplicateRecord.$submitted && frmDuplicateRecord.TWR_NAME.$invalid}">
                                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Towers" data-output-model="DuplicateRecord.Towers" data-button-label="icon TWR_NAME "
                                                    data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="DuplicateRecord.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmDuplicateRecord.$submitted && frmDuplicateRecord.TWR_NAME.$invalid">Please select tower </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmDuplicateRecord.$submitted && frmDuplicateRecord.FLR_NAME.$invalid}">
                                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Floors" data-output-model="DuplicateRecord.Floors" data-button-label="icon FLR_NAME"
                                                    data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="DuplicateRecord.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmDuplicateRecord.$submitted && frmDuplicateRecord.FLR_NAME.$invalid">Please select floor </span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12 box-footer pull-right">
                                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search</button>
                                            <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-11 col-sm-6 col-xs-12" id="table2" data-ng-show="GridVisiblity" style="padding-top: 10px">
                                        <a data-ng-click="GenReport(DuplicateRecord,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(DuplicateRecord,'xlsx')"><i id="excel" data-toggle="tooltip"  data-ng-show="Excel" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(DuplicateRecord,'pdf')"><i id="pdf" data-toggle="tooltip" data-ng-show="PDF" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    </div>
                                </div>
                                <div id="Tabular">
                                    <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--<script defer src="../../../Scripts/aggrid/ag-grid.min.js"></script>--%>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

    </script>
    <script defer src="../../Utility.js"></script>
    <script defer src="../Js/DuplicateRecordsReport.js"></script>

</body>
</html>
