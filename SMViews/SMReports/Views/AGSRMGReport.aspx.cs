﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SMViews_SMReports_Views_AGSRMGReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string host = HttpContext.Current.Request.Url.Host;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = path;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
        {
            if ((HttpContext.Current.Session["UID"].ToString() == ""))
            {
                Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
            }
            else
            {
                if (sdr.HasRows)
                {

                }
                else
                {
                    Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
                }
            }
        }
    }
}