﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Space_Subtype_Report.aspx.cs" Inherits="SMViews_SMReports_Views_Space_Subtype_Report" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>
</head>
<body data-ng-controller="Space_SubtypeRptController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Seat Report </h3>
            </div>
            <div class="card">
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" name="Space_SubtypeRpt" data-valid-submit="LoadData()" novalidate>

                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': Space_SubtypeRpt.$submitted && Space_SubtypeRpt.CNY_NAME.$invalid}">
                                    <label class="control-label">Country <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Country" data-output-model="SpaceSubtypeRpt.Country" data-button-label="icon CNY_NAME"
                                        data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SpaceSubtypeRpt.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="Space_SubtypeRpt.$submitted && Space_SubtypeRpt.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': Space_SubtypeRpt.$submitted && Space_SubtypeRpt.CTY_NAME.$invalid}">
                                    <label class="control-label">City<span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="City" data-output-model="SpaceSubtypeRpt.City" data-button-label="icon CTY_NAME"
                                        data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SpaceSubtypeRpt.City[0]" name="CTY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="Space_SubtypeRpt.$submitted && Space_SubtypeRpt.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': Space_SubtypeRpt.$submitted && Space_SubtypeRpt.LCM_NAME.$invalid}">
                                    <label class="control-label">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Locations" data-output-model="SpaceSubtypeRpt.Locations" data-button-label="icon LCM_NAME"
                                        data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SpaceSubtypeRpt.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="Space_SubtypeRpt.$submitted && Space_SubtypeRpt.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': Space_SubtypeRpt.$submitted && Space_SubtypeRpt.TWR_NAME.$invalid}">
                                    <label class="control-label">Tower <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Towers" data-output-model="SpaceSubtypeRpt.Towers" data-button-label="icon TWR_NAME "
                                        data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SpaceSubtypeRpt.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="Space_SubtypeRpt.$submitted && Space_SubtypeRpt.TWR_NAME.$invalid" style="color: red">Please select a tower </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': Space_SubtypeRpt.$submitted && Space_SubtypeRpt.FLR_NAME.$invalid}">
                                    <label class="control-label">Floor <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Floors" data-output-model="SpaceSubtypeRpt.Floors" data-button-label="icon FLR_NAME"
                                        data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SpaceSubtypeRpt.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="Space_SubtypeRpt.$submitted && Space_SubtypeRpt.FLR_NAME.$invalid" style="color: red">Please select a floor </span>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <br />
                                <div class="box-footer text-right">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12" id="table2" style="padding-top: 10px">
                                <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            </div>
                        </div>

                        <div id="Tabular" data-ng-show="GridVisiblity2">
                            <div class="row" style="padding-bottom: 30px">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../../Utility.min.js"></script>
    <%--<script defer src="../Js/Space_Subtype_Report.js"></script>--%>
    <script defer src="../Js/Space_Subtype_Report.js"></script>
</body>
</html>
