﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InfraAdministrationReport.aspx.cs" Inherits="SMViews_SMReports_Views_InfraAdministrationReport" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>


</head>
<body data-ng-controller="InfraAdministrationReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Infrastructure Administration Report </h3>
            </div>
            <div class="card">
                <form id="form1" name="InfraAdministrationReport">
                    <div class="clearfix">

                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                            <div class="form-group" data-ng-class="{'has-error': InfraAdministrationRpt.$submitted && InfraAdministrationRpt.LCM_NAME.$invalid}">
                                <label class="control-label">Location</label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="InfraAdministrationReport.Locations" button-label="icon LCM_NAME"
                                    item-label="icon LCM_NAME" tick-property="ticked" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="InfraAdministrationReport.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="InfraAdministrationRpt.$submitted && InfraAdministrationRpt.Company.$invalid" style="color: red">Please Select Location </span>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-6 col-xs-12">
                            <br />
                            <div class="box-footer">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="SearchData()" />
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-11 col-sm-6 col-xs-12" id="table2" data-ng-show="GridVisiblity" style="padding-top: 10px">

                            <%--<a data-ng-click="GenReportproject(InfraAdministrationReport,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReportproject(InfraAdministrationReport,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>

                            <%--<a data-ng-click="GenerateReport(InfraAdministrationReport,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>--%>
                            <a download="InfraAdministrationReport.xlsx" href="/UploadFiles/[TATA_CAP].dbo/InfraAdministrationReport.xlsx"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenerateReport(InfraAdministrationReport,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="Options" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    -
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Scripts/JSONToCSVConvertor.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';


        function setDateVals() {
            $('#from_date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#to_date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>
    <script defer src="../../Utility.js"></script>
    <script defer src="../Js/InfraAdministrationReport.js"></script>
    <%--<script defer src="../Js/TrailMomentOfEmployeeRpt.js"></script>--%>
</body>
</html>

