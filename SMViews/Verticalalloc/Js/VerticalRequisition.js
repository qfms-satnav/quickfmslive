﻿
app.service("VerticalRequisitionService", ['$http','$q', function ($http, $q) {

    this.searchSpaces = function (searchObj) {
        var deferred = $q.defer();
        return $http.post('../../../api/SearchSpaces/GetVacantSpaces', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.raiseRequest = function (searchObj) {
        var deferred = $q.defer();
        return $http.post('../../../api/SearchSpaces/RaiseRequest', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('VerticalRequisitionController', ['$scope', '$q', '$location', 'VerticalRequisitionService', 'UtilityService','$filter', function ($scope, $q, $location, VerticalRequisitionService, UtilityService, $filter) {
    $scope.VertRequest = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.currentblkReq = {};
    $scope.SelLayers = [];
    $scope.Verticals = [];
    $scope.selectedSpaces = [];
    $scope.tempspace = {};
    $scope.MapFloors = [];
    $scope.Markers = [];
    $scope.SelRowData = [];

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });


    UtilityService.getCountires(2).then(function (Countries) {
        if (Countries.data != null) {
            $scope.Country = Countries.data;

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;

                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;

                            UtilityService.getTowers(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Tower = response.data;
                                    UtilityService.getFloors(2).then(function (response) {
                                        if (response.data != null) {
                                            $scope.Floor = response.data;
                                        }
                                    });
                                }
                            });

                        }
                    });

                }
            });

        }
    });




    var map = L.map('leafletMap');//.setView([17.561298804683357, 79.6684030798511], 11);
    //// Country Events
    $scope.CnyChangeAll = function () {
        $scope.VertRequest.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.VertRequest.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.VertRequest.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.VertRequest.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.VertRequest.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.VertRequest.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        $scope.VertRequest.Country = [];

        UtilityService.getLocationsByCity($scope.VertRequest.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VertRequest.Country[0] = cny;
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.VertRequest.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.VertRequest.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {
        $scope.VertRequest.Country = [];
        $scope.VertRequest.City = [];

        UtilityService.getTowerByLocation($scope.VertRequest.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VertRequest.Country[0] = cny;
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.VertRequest.City[0] = cty;
            }
        });
    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.VertRequest.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.VertRequest.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        $scope.VertRequest.Country = [];
        $scope.VertRequest.City = [];
        $scope.VertRequest.Location = [];

        UtilityService.getFloorByTower($scope.VertRequest.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VertRequest.Country = cny;
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.VertRequest.City = cty;
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.VertRequest.Location = lcm;
            }
        });

    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.VertRequest.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.VertRequest.Floor = [];
        $scope.FlrChanged();
    }

    $scope.FlrChanged = function () {
        $scope.VertRequest.Country = [];
        $scope.VertRequest.City = [];
        $scope.VertRequest.Location = [];
        $scope.VertRequest.Tower = [];
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VertRequest.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.VertRequest.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.VertRequest.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (twr != undefined && value.ticked == true && twr.ticked == false) {
                twr.ticked = true;
                $scope.VertRequest.Tower.push(twr);
            }
        });

    }


    $scope.SearchSpaces = function () {
        if (moment($scope.currentblkReq.SVR_FROM_DATE) > moment($scope.currentblkReq.SVR_TO_DATE)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {

            progress(0, 'Loading...', true);
            $scope.selectedSpaces = [];
            $scope.Markers = [];
            $scope.SelRowData = [];
            map.eachLayer(function (layer) {
                map.removeLayer(layer);
            });

            var searchObj = { flrlst: $scope.VertRequest.Floor, verreq: $scope.currentblkReq };
            VerticalRequisitionService.searchSpaces(searchObj).then(function (response) {
                if (response.data != null) {
                    $scope.GetVerticals();
                    GetMarkers(response.data);
                    $scope.gridOptions.api.setRowData($scope.Markers);
                    //// for map
                    //if (map != undefined) {
                    //    map.remove();
                    //}
                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }

            }, function (response) {
                progress(0, '', false);
            });
        }

    }

    $scope.GetVerticals = function () {
        UtilityService.getVerticals(3).then(function (response) {
            if (response.data != null) {
                $scope.Verticals = response.data;
                setTimeout(function () {
                    $('#ddlvert').selectpicker('refresh');
                }, 200);
            }
        });
    }

    $scope.RaiseRequest = function () {
        $scope.selectedSpaces = [];
        angular.forEach($scope.Markers, function (Value, Key) {
            $scope.selspcObj = {};
            if (Value.ticked) {
                $scope.selspcObj.SVD_REQ_ID = Value.SVD_REQ_ID;
                $scope.selspcObj.SVD_SPC_ID = Value.SVD_SPC_ID;
                $scope.selspcObj.SVD_SPC_TYPE = Value.layer;
                $scope.selspcObj.SVD_SPC_SUB_TYPE = Value.SVD_SPC_SUB_TYPE;
                $scope.selspcObj.FLR_ID = Value.FLR_ID;
                $scope.selspcObj.SVD_STA_ID = Value.SVD_STA_ID;
                $scope.selspcObj.STACHECK = Value.STACHECK;
                $scope.selspcObj.ticked = Value.ticked;
                $scope.selectedSpaces.push($scope.selspcObj);
            }
        });
        $scope.currentblkReq.STACHECK = 4;
        if ($scope.selectedSpaces.length != 0) {
            progress(0, 'Loading...', true);
            var ReqObj = { flrlst: $scope.VertRequest.Floor, verreq: $scope.currentblkReq, verreqdet: $scope.selectedSpaces, ALLOCSTA: 4, MODE: 2 };
            VerticalRequisitionService.raiseRequest(ReqObj).then(function (response) {
                if (response.data != null) {
                    $scope.Clear();
                    progress(0, '', false);
                    showNotification('success', 8, 'bottom-right', response.Message);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }

            }, function (response) {
                progress(0, '', false);
            });
        }
        else
            showNotification('error', 8, 'bottom-right', 'Select at least one space ID to allocate');
    }

    $scope.Clear = function () {
        $scope.currentblkReq = {};
        $scope.VertRequest = {};

        $scope.selectedSpaces = [];
        $scope.Markers = [];
        $scope.gridOptions.rowData = [];

        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Location, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floor, function (floor) {
            floor.ticked = false;
        });

        $scope.VertRequest.Country = [];
        $scope.VertRequest.City = [];
        $scope.VertRequest.Location = [];
        $scope.VertRequest.Tower = [];
        $scope.VertRequest.Floor = [];
        $scope.frmVertReq.$submitted = false;
    }

    //For Grid column Headers
    var columnDefs = [
        {
            headerName: "Select All", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)' />",
            cellClass: 'grid-align', headerCellRenderer: headerCellRendererFunc, suppressMenu: true,
        },
        { headerName: "Space ID", field: "SVD_SPC_NAME", cellClass: "grid-align" },
        { headerName: "Space Type", field: "SVD_SPC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SVD_SPC_SUB_TYPE_NAME", cellClass: "grid-align" },
    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableSorting: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    $scope.chkChanged = function (data) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: data.SVD_SPC_ID, spacetype: 'CHA' } });
            if (data.ticked) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
            //data.setIcon(selctdChricon)
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
                //data.setIcon(Vacanticon)
            }
        }
    }

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        //  var cb1 = document.createElement('input');
        var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        // cb1.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        //    var eTitle = document.createTextNode('Select only this page');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        //  eHeader.appendChild(eTitle);
        //eHeader.appendChild(br);
        //  eHeader.appendChild(cb1);
        eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = true;

                    });

                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = true;
                                value.setStyle(selctdChrStyle);
                            }
                        });
                    }

                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = false;
                        //value.setIcon(VacantStyle)
                    });

                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = false;
                                value.setStyle(VacantStyle);
                            }
                        });
                    }

                });
            }
        });

        return eHeader;
    }


    ///// For map layout
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.VertRequest.Floor, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        //if (map == undefined) {
        if ($scope.SelRowData.length == 0)
            $scope.loadmap();
        //}
        //if (map == undefined)
        //    $scope.loadmap();
    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }


    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        //$('#leafletMap').empty();

        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1 };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;

            //var SeattypeLayer = $.extend(true, {}, theLayer);

            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                $scope.SelRowData = $filter('filter')($scope.Markers, { FLR_ID: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (marker, key) {

                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: marker.SVD_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SVD_REQ_ID = marker.SVD_REQ_ID;
                    $scope.marker.SVD_SPC_ID = marker.SVD_SPC_ID;
                    $scope.marker.SVD_SPC_NAME = marker.SVD_SPC_NAME;
                    $scope.marker.layer = marker.SVD_SPC_TYPE;
                    $scope.marker.SVD_SPC_TYPE_NAME = marker.SVD_SPC_TYPE_NAME;
                    $scope.marker.SVD_SPC_SUB_TYPE = marker.SVD_SPC_SUB_TYPE;
                    $scope.marker.SVD_SPC_SUB_TYPE_NAME = marker.SVD_SPC_SUB_TYPE_NAME;
                    $scope.marker.FLR_ID = marker.FLR_ID;
                    $scope.marker.STACHECK = marker.STACHECK;
                    $scope.marker.ticked = marker.ticked;
                    $scope.marker.bindLabel(marker.SVD_SPC_ID);
                    if (marker.ticked)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);

                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);

                });
            },
            // error
            function (response) {
            }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});


    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {
        //$filter('filter')(myObjects, { color: "red" });
        jQuery.each(data, function (index, value) {

            $scope.marker = {};
            $scope.marker.SVD_REQ_ID = value.SVD_REQ_ID;
            $scope.marker.SVD_SPC_ID = value.SVD_SPC_ID;
            $scope.marker.SVD_SPC_NAME = value.SVD_SPC_NAME;
            $scope.marker.layer = value.SVD_SPC_TYPE;
            $scope.marker.SVD_SPC_TYPE_NAME = value.SVD_SPC_TYPE_NAME;
            $scope.marker.SVD_SPC_SUB_TYPE = value.SVD_SPC_SUB_TYPE;
            $scope.marker.SVD_SPC_SUB_TYPE_NAME = value.SVD_SPC_SUB_TYPE_NAME;
            $scope.marker.FLR_ID = value.FLR_ID;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ticked = value.ticked;
            $scope.Markers.push($scope.marker);
        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SVD_SPC_ID: this.SVD_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

}]);