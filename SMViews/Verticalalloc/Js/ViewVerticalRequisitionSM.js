﻿
app.service("ViewVerticalReqService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    //For my Requisitions Grid

    this.GetMyReqList = function (id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewVerticalRequisition/GetMyReqList?id=' + id + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetUpdateApprList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewVerticalRequisition/GetUpdateApprovalsList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetDetailsOnSelection = function (selectedid) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewVerticalRequisition/GetDetailsOnSelection', selectedid)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //this.searchSpaces = function (searchObj) {
    //    var deferred = $q.defer();
    //    return $http.post(UtilityService.path + '/api/SearchSpaces/GetVacantSpaces', searchObj)
    //     .then(function (response) {
    //         deferred.resolve(response.data);
    //         return deferred.promise;
    //     }, function (response) {
    //         deferred.reject(response);
    //         return deferred.promise;
    //     });
    //};


    this.searchSpaces = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewVerticalRequisition/SearchSpaces', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.ApproveRequests = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SearchSpaces/ApproveRequests', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


}]);
app.controller('ViewVerticalRequisitionController', ['$scope', '$q', 'ViewVerticalReqService', 'SearchSpacesService', 'UtilityService','$filter', function ($scope, $q, ViewVerticalReqService, SearchSpacesService, UtilityService, $filter) {
    $scope.Viewstatus = 0;
    $scope.VerReq = {};
    $scope.Getcountry = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.selectedSeats = [];
    $scope.currentblkReq = {};
    $scope.RetStatus = UtilityService.Added;
    $scope.EnableStatus = 0;
    $scope.tempspace = {};
    $scope.selectedSpaces = [];
    $scope.sendCheckedValsObj = [];
    $scope.MapFloors = [];
    $scope.Markers = [];
    $scope.SelRowData = [];
    $scope.tickedSpaces = [];
    $scope.SelectedRowData = {};

    var map = L.map('leafletMap');//.setView([17.561298804683357, 79.6684030798511], 11);



    $scope.MyrequisitonsDefs = [

        { headerName: "Requisition ID", width: 250, field: "SVR_REQ_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data,false)">{{data.SVR_REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
        { headerName: "Status", field: "STA_DESC", cellClass: "grid-align", pinned: 'left' },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", template: '<span>{{data.SVR_REQ_DATE | date:"dd MMM, yyyy"}}</span>', field: "SVR_REQ_DATE", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Vertical", field: "VER_NAME", cellClass: "grid-align" },
        { headerName: "From Date", width: 146, template: '<span>{{data.SVR_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "To Date", width: 145, template: '<span>{{data.SVR_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },

    ];
    $scope.MyrequisitonsOptions = {
        columnDefs: $scope.MyrequisitonsDefs,
        rowData: null,
        angularCompileRows: true,
        enableFilter: true,
        enableScrollbars: true,
        rowSelection: 'single',
        enableSorting: true,
        enableColResize: true,
        onReady: function () {
            $scope.MyrequisitonsOptions.api.sizeColumnsToFit()
        },

    };


    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    // To display grid row data  
    $scope.LoadMyData = function () {
        ViewVerticalReqService.GetMyReqList(1).then(function (data) {
            if (data.data != null) {
                $scope.gridata = data.data;
                $scope.MyrequisitonsOptions.api.setRowData([]);
                $scope.MyrequisitonsOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            } else {
                $scope.MyrequisitonsOptions.api.setRowData([]);
                progress(0, '', false);
            }
        }, function (error) {
            progress(0, '', false);
        });

    }
    $scope.LoadPendingData = function () {
        progress(0, 'Loading...', true);
        ViewVerticalReqService.GetMyReqList(2).then(function (data) {
            progress(0, 'Loading...', true);
            if (data.data != null) {
                $scope.Pendinggridata = data.data;
                $scope.approvalOptions.api.setRowData([]);
                $scope.approvalOptions.api.setRowData($scope.Pendinggridata);
                progress(0, '', false);
            }
            else {
                $scope.approvalOptions.api.setRowData([]);
                progress(0, '', false);

            }

        }, function (error) {
            progress(0, '', false);
        });

    }
    if ($scope.gridata == null && $scope.Pendinggridata) {
        progress(0, '', false);
        showNotification('error', 8, 'bottom-right', 'Records not found');
    }

    setTimeout(function () {
        $scope.LoadMyData();
    }, 400);
    setTimeout(function () {
        $scope.LoadPendingData();
    }, 700);
    function onReqFilterChanged(value) {
        $scope.MyrequisitonsOptions.api.setQuickFilter(value);
    }
    $("#ReqFilter").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    })



    /////////////////////////////////////////////////////
    $scope.approvalsDefs = [
        { headerName: "Select All", field: "ticked", template: "<input type='checkbox' ng-model='data.ticked'>", cellClass: 'grid-align', filter: 'set', headerCellRenderer: headerAppCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Requisition ID", width: 350, field: "SVR_REQ_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data,true)">{{data.SVR_REQ_ID}}</a>', pinned: 'left', suppressMenu: true },
        { headerName: "Status", field: "STA_DESC", cellClass: "grid-align", pinned: 'left',width: 250 },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, width: 250},
        { headerName: "Requested Date ", template: '<span>{{data.SVR_REQ_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true },
        { headerName: "Vertical", field: "VER_NAME", cellClass: "grid-align" },
        { headerName: "From Date", template: '<span>{{data.SVR_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true },
        { headerName: "To Date", template: '<span>{{data.SVR_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true },

    ];
    $scope.approvalOptions = {
        columnDefs: $scope.approvalsDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: true,
        enableFilter: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.approvalOptions.api.sizeColumnsToFit()
        },

    };
    function onAppFilterChanged(value) {
        $scope.approvalOptions.api.setQuickFilter(value);
    }
    $("#ApprvlFilter").change(function () {
        onAppFilterChanged($(this).val());
    }).keydown(function () {
        onAppFilterChanged($(this).val());
    }).keyup(function () {
        onAppFilterChanged($(this).val());
    }).bind('paste', function () {
        onAppFilterChanged($(this).val());
    })
    ////////////////////////////////////////
    $scope.UpdateapprDefs = [
        { headerName: "Select All", width: 90, field: "ticked", template: "<input type='checkbox' data-ng-disabled='EnableStatus != 0' ng-model='data.ticked'  ng-change='chkChanged(data)'>", cellClass: 'grid-align', filter: 'set', headerCellRenderer: headerCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Space ID", width: 270, field: "SVD_SPC_NAME", cellClass: "grid-align", filter: 'set' },
        { headerName: "Space Type", width: 165, field: "SVD_SPC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Space Sub Type", width: 165, field: "SVD_SPC_SUB_TYPE_NAME", cellClass: "grid-align" }
    ];
    $scope.UpdateapprOptions = {
        columnDefs: $scope.UpdateapprDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        enableColResize: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.UpdateapprOptions.api.sizeColumnsToFit()
        },
    };
    function onUpdateFilterChanged(value) {
        $scope.UpdateapprOptions.api.setQuickFilter(value);
    }
    $("#UpdteFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })


    $scope.chkChanged = function (selctedRow) {

        if (!selctedRow.ticked) {
            //selctedRow.setIcon(Vacanticon)
            selctedRow.STACHECK = UtilityService.Deleted;
            if ($scope.drawnItems) {
                $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SVD_SPC_ID, spacetype: 'CHA' } });
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
            }
        }
        else {
            //selctedRow.setIcon(selctdChricon);
            if ($scope.drawnItems) {
                $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SVD_SPC_ID, spacetype: 'CHA' } });
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
            if (selctedRow.SVD_REQ_ID != null && selctedRow.SVD_REQ_ID != "") {
                selctedRow.STACHECK = UtilityService.Unchanged;
            }
            else {
                selctedRow.STACHECK = UtilityService.Added;
            }
        }
    }



    $scope.onRowSelectedFunc = function (data, flag) {
        progress(0, 'Loading...', true);
        var selectedid = data;
        $scope.SelectedRowData = data;
        $scope.Viewstatus = 1;
        if (selectedid.SVR_STA_ID == '1006' && flag) {
            $scope.EnableStatus = 1;
        }
        else if (selectedid.SVR_STA_ID != '1006' && selectedid.SVR_REQ_BY == UID) {
            $scope.EnableStatus = 2;
        }
        else if (selectedid.SVR_STA_ID == '1006' && selectedid.SVR_REQ_BY == UID) {
            $scope.EnableStatus = 0;
        }
        ViewVerticalReqService.GetDetailsOnSelection(selectedid).then(function (response) {

            $scope.Markers = [];
            $scope.SelRowData = [];
            map.eachLayer(function (layer) {
                map.removeLayer(layer);
            });
            if (response.data != null) {
                $scope.VerReq = response.data.SELSPACES.VerticalDetails;
                $scope.currentblkReq = response.data.SELSPACES.VerticalRequsition;
                $scope.currentblkReq.SVR_FROM_DATE = $filter('date')($scope.currentblkReq.SVR_FROM_DATE, "MM/dd/yyyy");
                $scope.currentblkReq.SVR_TO_DATE = $filter('date')($scope.currentblkReq.SVR_TO_DATE, "MM/dd/yyyy");
                $scope.currentblkReq.VER_NAME = response.data.SELSPACES.VerticalRequsition.VER_NAME;
                $scope.Markers = [];
                $scope.UpdateapprOptions.api.setRowData([]);
                GetMarkers(response.data.DETAILS);
                $scope.UpdateapprOptions.api.setRowData($scope.Markers);
                $scope.selectedSpaces = response.data.SELSPACES.VerticalDetails.selectedSeats;

                UtilityService.getCountires(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Getcountry = response.data;
                        if ($scope.VerReq.selectedCountries != null) {
                            for (i = 0; i < $scope.VerReq.selectedCountries.length; i++) {
                                var a = _.find($scope.Getcountry, { CNY_CODE: $scope.VerReq.selectedCountries[i].CNY_CODE });
                                a.ticked = true;
                            }
                        }

                        UtilityService.getCities(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Citylst = response.data;
                                if ($scope.VerReq.selectedCities != null) {
                                    for (i = 0; i < $scope.VerReq.selectedCities.length; i++) {
                                        var a = _.find($scope.Citylst, { CTY_CODE: $scope.VerReq.selectedCities[i].CTY_CODE });
                                        a.ticked = true;
                                    }
                                }

                                UtilityService.getLocations(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Locationlst = response.data;
                                        if ($scope.VerReq.selectedLocations != null) {
                                            for (i = 0; i < $scope.VerReq.selectedLocations.length; i++) {
                                                var a = _.find($scope.Locationlst, { LCM_CODE: $scope.VerReq.selectedLocations[i].LCM_CODE });
                                                a.ticked = true;
                                            }
                                        }
                                        UtilityService.getTowers(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Towerlist = response.data;
                                                if ($scope.VerReq.selectedTowers != null) {
                                                    for (i = 0; i < $scope.VerReq.selectedTowers.length; i++) {
                                                        var a = _.find($scope.Towerlist, { TWR_CODE: $scope.VerReq.selectedTowers[i].TWR_CODE });
                                                        a.ticked = true;
                                                    }
                                                }

                                                UtilityService.getFloors(2).then(function (response) {
                                                    if (response.data != null) {
                                                        $scope.Floorlist = response.data;
                                                    }
                                                    if ($scope.VerReq.selectedTowers != null) {
                                                        for (i = 0; i < $scope.VerReq.selectedTowers.length; i++) {
                                                            var a = _.find($scope.Floorlist, { FLR_CODE: $scope.VerReq.selectedFloors[i].FLR_CODE });
                                                            a.ticked = true;
                                                        }
                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });

                UtilityService.getVerticals(3).then(function (Verdata) {
                    $scope.Verticallist = Verdata.data;
                    setTimeout(function () {
                        $('.selectpicker').selectpicker('refresh');
                    }, 200);
                    for (var i = 0; i < $scope.Verticallist.length; i += 1) {
                        var result = $scope.Verticallist[i];
                        if (result.VER_CODE == response.data.SELSPACES.VerticalRequsition.SVR_VER_CODE) {
                            return result.VER_NAME;
                        }
                    }


                });
            }
            setTimeout(function () {
                progress(0, '', false);
            }, 1500)

        }, function (error) {
            progress(0, '', false);
        });

    };



    $scope.CountryChanged = function () {
        UtilityService.getCitiesbyCny($scope.VerReq.selectedCountries, 2).then(function (response) {
            $scope.Citylst = response.data
        }, function (error) {
            console.log(error);
        });
    }
    $scope.CnyChangeAll = function () {
        UtilityService.getCitiesbyCny($scope.Getcountry, 2).then(function (data) {
            $scope.Citylst = data.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectNone = function () {
        $scope.VertRequest.City = [];
        $scope.CityChanged();

    }
    $scope.CityChanged = function () {

        $scope.VerReq.selectedCountries = [];
        UtilityService.getLocationsByCity($scope.VerReq.selectedCities, 2).then(function (data) {
            $scope.Locationlst = data.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VertRequest.Country[0] = cny;
            }
        });
    }
    $scope.CtyChangeAll = function () {
        $scope.Citylst = $scope.Citylst;
        $scope.CityChanged();
    }
    $scope.ctySelectNone = function () {
        $scope.Citylst = [];
        $scope.CityChanged();
    }
    $scope.LocChange = function () {

        $scope.VerReq.selectedCountries = [];
        $scope.VerReq.selectedCities = [];
        //UtilityService.getTowerByLocation($scope.VerReq.selectedLocations, 2).then(function (data) {
        //    $scope.Towerlist = data.data;
        //}, function (error) {
        //    console.log(error);
        //});
        UtilityService.getTowerByLocation($scope.VerReq.selectedLocations, 2).then(function (response) {
            if (response.data != null)
                $scope.Towerlist = response.data;
            else
                $scope.Towerlist = [];
        });

        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VerReq.selectedCountries = cny;
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.VerReq.selectedCities = cty;
            }
        });
    }
    $scope.LCMChangeAll = function () {
        //UtilityService.getTowerByLocation($scope.Locationlst, 2).then(function (data) {
        //    $scope.Towerlist = data.data;
        //}, function (error) {
        //    console.log(error);
        //});
        $scope.VerReq.selectedLocations = $scope.Locationlst;
        $scope.LocChange();
    }
    $scope.lcmSelectNone = function () {
        $scope.VerReq.selectedLocations = [];
        $scope.LocChange();
    }
    $scope.TwrChange = function () {
        $scope.VerReq.selectedCountries = [];
        $scope.VerReq.selectedCities = [];
        $scope.VerReq.selectedLocations = [];

        //UtilityService.getFloorByTower($scope.VerReq.selectedTowers, 2).then(function (data) {
        //    $scope.Floorlist = data.data;
        //}, function (error) {
        //    console.log(error);
        //});
        UtilityService.getFloorByTower($scope.VerReq.selectedTowers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VerReq.selectedCountries = cny;
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.VerReq.selectedCities = cty;
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.VerReq.selectedLocations = lcm;
            }
        });
    }
    $scope.TwrChangeAll = function () {
        $scope.VerReq.selectedTowers = $scope.Towerlist;
        $scope.TwrChange();

    }
    $scope.twrSelectNone = function () {
        $scope.VerReq.selectedTowers = [];
        $scope.TwrChange();
    }


    $scope.FlrChanged = function () {
        $scope.VerReq.selectedCountries = [];
        $scope.VerReq.selectedCities = [];
        $scope.VerReq.selectedLocations = [];
        $scope.VerReq.selectedTowers = [];

        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.VerReq.selectedCountries = cny;
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.VerReq.selectedCities = cty;
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.VerReq.selectedLocations = lcm;
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true && twr.ticked == false) {
                twr.ticked = true;
                $scope.VerReq.selectedTowers = twr;
            }
        });

    }

    $scope.FlrChangeAll = function () {
        $scope.VerReq.selectedFloors = $scope.Floorlist;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.VerReq.selectedFloors = [];
        $scope.FlrChanged();
    }


    $scope.setStatus = function (status) {
        switch (status) {
            case 'Approve': $scope.RetStatus = UtilityService.Approved;
                $scope.currentblkReq.SVR_APPR_BY = UID;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;

                break;
            case 'Reject': $scope.RetStatus = UtilityService.Rejected;
                $scope.currentblkReq.SVR_APPR_BY = UID;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;
                break;
            case 'Update': $scope.RetStatus = UtilityService.Modified;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;
                break;
            case 'Cancel': $scope.RetStatus = UtilityService.Canceled;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;
                break;
            case 'ApproveAll':
                $scope.RetStatus = UtilityService.Approved;
                angular.forEach($scope.Pendinggridata, function (data) {
                    if (data.ticked == true) {
                        $scope.sendCheckedValsObj.push(data);
                    }
                });
                $scope.currentblkReq.SVR_APPR_BY = UID;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;

                break;
            case 'RejectAll': $scope.RetStatus = UtilityService.Rejected;
                angular.forEach($scope.Pendinggridata, function (data) {
                    if (data.ticked == true) {
                        $scope.sendCheckedValsObj.push(data);
                    }
                });

                $scope.currentblkReq.SVR_APPR_BY = UID;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;
                break;
        }
    }
    $scope.UpdateRequest = function () {
        $scope.tickedSpaces = [];
        progress(0, 'Loading...', true);
        angular.forEach($scope.Markers, function (data, key) {
            $scope.selspcObj = {};
            $scope.selspcObj.SVD_REQ_ID = data.SVD_REQ_ID;
            $scope.selspcObj.SVD_SPC_ID = data.SVD_SPC_ID;
            $scope.selspcObj.SVD_SPC_NAME = data.SVD_SPC_NAME;
            $scope.selspcObj.SRD_SPC_TYPE = data.layer;
            $scope.selspcObj.lat = data.lat;
            $scope.selspcObj.lon = data.lon;
            $scope.selspcObj.SVD_SPC_SUB_TYPE = data.SVD_SPC_SUB_TYPE;
            $scope.selspcObj.SVD_SPC_SUB_TYPE_NAME = data.SVD_SPC_SUB_TYPE_NAME;
            $scope.selspcObj.SVD_SPC_TYPE = data.layer;
            $scope.selspcObj.SVD_SPC_TYPE_NAME = data.SVD_SPC_TYPE_NAME;
            $scope.selspcObj.SVD_STA_ID = data.SVD_STA_ID;
            $scope.selspcObj.ticked = data.ticked;
            $scope.selspcObj.STACHECK = data.STACHECK;

            if (data.STACHECK == 8 || data.STACHECK == 4) {
                $scope.tickedSpaces.push($scope.selspcObj);
            }

        });

        var cnt = _.filter($scope.Markers, { 'ticked': true }).length;
        if (moment($scope.currentblkReq.SVR_FROM_DATE) > moment($scope.currentblkReq.SVR_TO_DATE)) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else if (cnt == 0) {
            if ($scope.RetStatus == 32) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Select Spaces to Approve');
            } else if ($scope.RetStatus == 16) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Select Spaces to Update');
            }

        } else {

            var ReqObj = { flrlst: $scope.VerReq.selectedFloors, verreq: $scope.currentblkReq, verreqdet: $scope.tickedSpaces, ALLOCSTA: $scope.RetStatus };

            SearchSpacesService.raiseRequest(ReqObj).then(function (response) {

                if (response != null) {
                    progress(0, '', false);
                    ViewVerticalReqService.GetMyReqList(1).then(function (data) {
                        if (data.data != null) {
                            $scope.gridata = data.data;
                            $scope.MyrequisitonsOptions.api.setRowData([]);
                            $scope.MyrequisitonsOptions.api.setRowData($scope.gridata);
                        } else { $scope.MyrequisitonsOptions.api.setRowData([]); }
                    }, function (error) {
                    });
                    ViewVerticalReqService.GetMyReqList(2).then(function (data) {
                        if (data.data != null) {
                            $scope.Pendinggridata = data.data;
                            $scope.approvalOptions.api.setRowData([]);
                            $scope.approvalOptions.api.setRowData($scope.Pendinggridata);
                        }
                        else {
                            $scope.approvalOptions.api.setRowData([]);
                        }

                    }, function (error) {
                    });
                    showNotification('success', 8, 'bottom-right', response.Message);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }

                $scope.back();
                $scope.tickedSpaces = [];

            });
        }
    }
    $scope.UpdateAllRequests = function () {
        progress(0, 'Loading...', true);
        var ReqObj = { ALLOCSTA: $scope.RetStatus, verreqList: $scope.sendCheckedValsObj };
        if ($scope.sendCheckedValsObj.length == 0) {
            progress(0, '', false);
            if ($scope.RetStatus == 32) {
                showNotification('error', 8, 'bottom-right', 'Select atleast One Requisition to Approve');
            } else if ($scope.RetStatus == 64) {
                showNotification('error', 8, 'bottom-right', 'Select atleast One Requisition to Reject');
            }

        }
        else {
            ViewVerticalReqService.ApproveRequests(ReqObj).then(function (response) {
                if (response != null) {
                    progress(0, '', false);
                    ViewVerticalReqService.GetMyReqList(2).then(function (data) {
                        if (data.data != null) {
                            $scope.Pendinggridata = data.data;
                            $scope.approvalOptions.api.setRowData([]);
                            $scope.approvalOptions.api.setRowData($scope.Pendinggridata);
                        }
                        else {
                            $scope.approvalOptions.api.setRowData([]);
                        }

                    }, function (error) {
                    });
                    $scope.back();
                    showNotification('success', 8, 'bottom-right', response.Message);
                }
            });
        }
    }


    $scope.SearchSpaces = function () {
        progress(0, 'Loading...', true);
        if (moment($scope.currentblkReq.SVR_FROM_DATE) > moment($scope.currentblkReq.SVR_TO_DATE)) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            var searchObj = { flrlst: $scope.VerReq.selectedFloors, verreq: $scope.currentblkReq };
            console.log(searchObj);
            SearchSpacesService.searchSpaces(searchObj).then(function (Searchresponse) {
                //ViewVerticalReqService.GetDetailsOnSelection($scope.SelectedRowData).then(function (Searchresponse) {
                if (Searchresponse.data != null) {
                    // $scope.Searchgridata = Searchresponse.data;
                    console.log(Searchresponse.data);
                    $scope.Markers = [];
                    GetMarkers(Searchresponse.data);
                    $scope.UpdateapprOptions.api.setRowData([]);
                    $scope.UpdateapprOptions.api.setRowData($scope.Markers);
                    progress(0, '', false);

                }
                else {
                    $scope.UpdateapprOptions.api.setRowData([]);
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', Searchresponse.Message);
                }

            });
            $scope.selectedSpaces = [];
        }
    }
    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.ticked = true;
                        //value.setIcon(selctdChricon);
                        $scope.tempspace = value;
                        $scope.tempspace.STACHECK = UtilityService.Added;
                        $scope.selectedSpaces.push($scope.tempspace);
                        $scope.tempspace = {};
                    });

                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = true;
                                value.setStyle(selctdChrStyle);
                            }
                        });

                    }
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.ticked = false;
                        //value.setIcon(Vacanticon)
                        _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SVD_REQ_ID: value.SVD_REQ_ID }));
                        value.STACHECK = UtilityService.Deleted;
                        $scope.selectedSpaces.push(value);
                    });

                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = false;
                                value.setStyle(VacantStyle);
                            }
                        });
                    }
                });
            }
        });
        return eHeader;
    }

    function headerAppCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.approvalOptions.rowData, function (value, key) {
                        value.ticked = true;
                        $scope.tempspace = value;
                        $scope.tempspace.STACHECK = UtilityService.Added;
                        $scope.selectedSpaces.push($scope.tempspace);
                        $scope.tempspace = {};
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.approvalOptions.rowData, function (value, key) {
                        value.ticked = false;
                        _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SVR_REQ_ID: value.SVR_REQ_ID }));
                        value.STACHECK = UtilityService.Deleted;
                        $scope.selectedSpaces.push(value);
                    });
                });
            }
        });
        return eHeader;
    }


    //For Back Button
    $scope.back = function () {

        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.Getcountry = [];
        $scope.SelectAll = [];
        $scope.SelectAllApp = false;

    }

    //Map
    $scope.ViewinMap = function () {
        $scope.MapFloors = [];
        angular.forEach($scope.VerReq.selectedFloors, function (Value, Key) {
            Value.ticked = false;
            $scope.MapFloors.push(Value);
        });
        $scope.MapFloors[0].ticked = true;
        $scope.Map.Floor.push($scope.MapFloors[0]);

        $("#historymodal").modal('show');
    }

    $('#historymodal').on('shown.bs.modal', function () {
        if ($scope.SelRowData.length == 0)
            $scope.loadmap();
    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);


        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1  };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });
    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                $scope.SelRowData = $filter('filter')($scope.Markers, { FLR_ID: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (marker, key) {
                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: marker.SVD_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SVD_REQ_ID = marker.SVD_REQ_ID;
                    $scope.marker.SVD_SPC_ID = marker.SVD_SPC_ID;
                    $scope.marker.SVD_SPC_NAME = marker.SVD_SPC_NAME;
                    $scope.marker.layer = marker.SVD_SPC_TYPE;
                    $scope.marker.SVD_SPC_TYPE_NAME = marker.SVD_SPC_TYPE_NAME;
                    $scope.marker.SVD_SPC_SUB_TYPE = marker.SVD_SPC_SUB_TYPE;
                    $scope.marker.SVD_SPC_SUB_TYPE_NAME = marker.SVD_SPC_SUB_TYPE_NAME;
                    $scope.marker.FLR_ID = marker.FLR_ID;
                    $scope.marker.STACHECK = marker.STACHECK;
                    $scope.marker.ticked = marker.ticked;
                    if (marker.ticked)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);
                    $scope.marker.bindLabel(marker.SVD_SPC_ID);
                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);
                });
            },
            // error
            function (response) {
            }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});


    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {
        jQuery.each(data, function (index, value) {
            $scope.marker = {};
            $scope.marker.SVD_REQ_ID = value.SVD_REQ_ID;
            $scope.marker.SVD_SPC_ID = value.SVD_SPC_ID;
            $scope.marker.SVD_SPC_NAME = value.SVD_SPC_NAME;
            $scope.marker.layer = value.SVD_SPC_TYPE;
            $scope.marker.SVD_SPC_TYPE_NAME = value.SVD_SPC_TYPE_NAME;
            $scope.marker.SVD_SPC_SUB_TYPE = value.SVD_SPC_SUB_TYPE;
            $scope.marker.SVD_SPC_SUB_TYPE_NAME = value.SVD_SPC_SUB_TYPE_NAME;
            $scope.marker.FLR_ID = value.FLR_ID;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ticked = value.ticked;
            $scope.Markers.push($scope.marker);
        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SVD_SPC_ID: this.SVD_SPC_ID });
        if (!this.ticked) {
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.chkChanged(this);
        $scope.UpdateapprOptions.api.refreshView();
    }

}]);
function setup(id) {
    $('#' + id).datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        todayHighlight: true
    });
};
