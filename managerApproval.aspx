﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="managerApproval.aspx.cs" Inherits="managerApproval" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div class="center-div">
        <form id="form1" runat="server">
            <div style="font-family: Open Sans,sans-serif; position: relative; font-size: 12px; height: 810px; mso-line-height-rule: exactly; line-height: 25px">
                <table style="border-bottom: #466788 1px solid; border-left: #466788 1px solid; width: 730px; border-collapse: collapse; border-top: #466788 1px solid; border-right: #466788 1px solid" border="0" bordercolor="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td bgcolor="#0069aa">
                            <div style="background: white;">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <table align="center" width="90%" style="border-bottom: #466788 4px solid; border-left: #466788 4px solid; width: 730px; border-collapse: collapse; border-top: #466788 4px solid; border-right: #466788 4px solid" border="0" bordercolor="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <div runat="server" id="Success" visible="false" align="center">
                                                            <label>Thank you. You approved the request. Seat reservation confirmation will be intimated to the employee.</label>
                                                        </div>
                                                        <div runat="server" id="Reject" visible="false" align="center">
                                                            <label>Thank you. You rejected the request. The same will be intimated to the employee.</label>
                                                        </div>
                                                        <div runat="server" id="Successwarning" visible="false" align="center">
                                                            <label>Already actioned</label>
                                                        </div>
                                                        <div runat="server" id="Rejectwarning" visible="false" align="center">
                                                             <label>Already actioned</label>
                                                        </div>
                                                        <div runat="server" id="Fail" visible="false" align="center">
                                                            <label>
                                                                An error occurred
                                                                <br />
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#0069aa" height="50">
                            <div align="right" style="color: #fff; font-weight: 700; font-size: 11px">
                                <div align="center">www.QuickFMS.com</div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</body>
</html>
