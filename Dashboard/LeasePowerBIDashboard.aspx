﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1180" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
		{ CompanyID: "Demo41.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMjJmMjI5N2MtNDc3ZS00Y2Y0LTk1NDYtYmZlODAyNTBjODg3IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectiona21186e82a785ac360fb" },
        { CompanyID: "BhartiAXA.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYjEwNTM0ODItZGFhYS00YTkxLThjZGMtNTA3NzU2YzkzMGVmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectiona21186e82a785ac360fb" },
        { CompanyID: "BNP.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZGE0MTQyMDEtYTkwMC00YmZmLWFjMmUtZGNiNzhlYTBkZGU2IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectiona21186e82a785ac360fb" },
        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNWJhNDk1MjEtOGJhOS00Y2YyLTk4M2MtZTBiN2ZhZWUwNjA4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNWJhNDk1MjEtOGJhOS00Y2YyLTk4M2MtZTBiN2ZhZWUwNjA4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "Tata_Tech.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYTkyMmIzZTYtN2QyYS00YmQ0LWE0YjktOTNhYzVlMjUwYzI3IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[TATA_CAP].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMmU4N2NmYjctMzZmMi00NzQyLWE3MTYtMjU0MGQ2ZDQ2NWVkIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
		{ CompanyID: "Aadhar.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiODhkYTNmZTgtNThlZC00MTc2LTk4NzUtZTYwNmQ2ODI3NmNhIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectiona21186e82a785ac360fb" },
		{ CompanyID: "Demo26.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNmUyZjUyMmYtMDMyNy00NTA2LTgzM2UtZDhiNjM2ZTFmM2Y3IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectiona21186e82a785ac360fb" },

		,];
        //{ CompanyID: "[MAXLIFE].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNWJhNDk1MjEtOGJhOS00Y2YyLTk4M2MtZTBiN2ZhZWUwNjA4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },];


    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>

