﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<style>
    /*#spce .c3 path, .c3 line {
        stroke: #F0AE7E !important;
    }

    hr {
        margin-top: 0px;
        margin-bottom: 0px;
    }*/


    .popUpHeight {
        width: 800px;
        height: 400px;
        overflow: auto;
    }
</style>

<div>

    <form id="form1" runat="server">
        <div class="row">
            <h5 id="Title" class="col-md-6"></h5>
        </div>
        <hr />
        <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                </div>
            </div>
        </div>
        <div id="ghdashboard">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="form-group">
                    <button type="button" class="btn btn-default btn-sm" onclick="Print('ghdashboard')">
                        <span class="glyphicon glyphicon-print hvr-icon-bounce" style="color: rgba(58, 156, 193, 0.87)"></span><span style="color: rgba(58, 156, 193, 0.87)">Print All</span>
                    </button>
                </div>
            </div>
            <div class="row" id="facilityCount">
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Facilities Count by Location
                                   
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('facilityCount')">
                                <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <div id="GHfacCountContainer"></div>
                        </div>
                    </div>
                </div>



                <div class="col-md-3 col-md-6">
                    <div class="panel panel-primary">


                        <div class="panel-heading" style="height: 88px">
                            <div class="row">
                                <div style="font-size: 12px" class="col-md-3">
                                    <i class="fa fa-warning fa-5x hvr-pulse-grow"></i>
                                </div>
                                <div class="col-md-9 text-center">
                                    <div>
                                        <label class="huge" id="lblwrtotcount"></label>
                                        <div></div>
                                        <label style="font-size: 11px;" class="small">Total Facilities Booked</label>
                                        <label style="font-size: 11px" class="small">(Current Month)</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#" id="ancWorkReqTotal">
                            <div class="panel-footer" role="alert" data-toggle="modal" data-target="#TotalWRS">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>

                    </div>
                </div>
                <div class="col-md-3 col-md-6">
                    <div class="panel panel-primary">

                        <div class="panel-heading" style="height: 88px">
                            <div class="row">
                                <div style="font-size: 12px" class="col-md-3">
                                    <i class="fa fa-warning fa-5x hvr-pulse-grow"></i>
                                </div>
                                <div class="col-md-9 text-center">
                                    <div>
                                        <label class="huge" id="lblWRclosed"></label>
                                        <div></div>
                                        <label style="font-size: 11px;" class="small">Facilities Withhold</label>
                                        <label style="font-size: 11px" class="small">(Current Month)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#" id="ancWorkReqCompleted">
                            <div class="panel-footer" role="alert" data-toggle="modal" data-target="#CompletedWRS">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


            </div>

            <div class="modal fade" id="PropDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                            <h4 class="modal-title" id="HeaderFacility"></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblPropDetails')">
                                    <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblPropDetails" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Facility Type</th>
                                    <th>Facility Name</th>

                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="divBarGraph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                            <h4 class="modal-title" id="divBarGraphHeading"></h4>

                        </div>
                        <div class="modal-body popUpHeight">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblBarGraph')">
                                    <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblBarGraph" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Reference ID</th>
                                    <th>Facility Type</th>
                                    <th>Facility Name</th>
                                    <th>Room Name/Number</th>
                                    <th>Location</th>
                                    <th>From date</th>
                                    <th>To date</th>
                                    <th>From Time</th>
                                    <th>To Time</th>
                                    <th>Booked Date</th>
                                    <th>Booked By</th>
                                </tr>
                            </table>



                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="div1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart fa-fw"></i><span id="spnbargraphtitle">  </span>
                        </div>
                        <div class="pull-right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('GHUtlContainer')">
                                <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <div id="GHUtlContainer"></div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal fade" id="TotalWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H2">Total Facilities Booked (Current Month)</h4>
                        </div>
                        <div class="modal-body popUpHeight">

                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tbltotalWRS')">
                                    <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>

                            <table id="tbltotalWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Reference ID</th>
                                    <th>Facility Type</th>
                                    <th>Facility Name</th>
                                    <th>Room Name/Number</th>
                                    <th>Location</th>
                                    <th>From date</th>
                                    <th>To date</th>
                                    <th>From Time</th>
                                    <th>To Time</th>
                                    <th>Booked Date</th>
                                    <th>Booked By</th>

                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="CompletedWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H1">Facilities Withhold (Currnet Month)</h4>
                        </div>
                        <div class="modal-body popUpHeight">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblCompletedWRS')">
                                    <span style="color: rgba(58, 156, 193, 0.87)" class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblCompletedWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Reference ID</th>
                                    <th>Facility Type</th>
                                    <th>Facility Name</th>
                                    <th>Room Name/Number</th>
                                    <th>Location</th>
                                    <th>From date</th>
                                    <th>To date</th>
                                    <th>From Time</th>
                                    <th>To Time</th>
                                    <th>Booked Date</th>
                                    <th>Booked By</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

</div>

<script src="../GHB/Reports/Js/GHDashboard.js" defer></script>
<script type="text/javascript" defer>
    var year
    $(document).ready(function () {
        year = (new Date().getFullYear()).toString();
    });
    $(document).ready(function () {
        var Cmpy = '<%= Session["COMPANYID"]%>';
        BindCompanyForGHModule(Cmpy);
        methodsToCallOnloadAndCompanyChange(Cmpy);

    });
</script>
<script type="text/javascript" defer>
    var GHT = '<%= Session["GHT"]%>';
    $(document).ready(function () {
        $("#Title").append(GHT).append(" Dashboard");
        $("#spnbargraphtitle").append(GHT).append(" Utilization (Current Month)");
    });

</script>

