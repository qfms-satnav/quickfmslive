﻿
<div>

    <form id="form1" runat="server">
        <div class="row">
            <h5 class="col-md-3">Energy Management</h5>
        </div>
        <hr />
         <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 10px;">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        Potable Water Consumption
                    </div>
                    <div class="panel-body">
                        <div id="TagWat"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        Electricity Consumption
                    </div>
                    <div class="panel-body">
                        <div class="clearfix">
                            <div id="TagEle">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        Reclaimable Water Consumption
                    </div>
                    <div class="panel-body">
                        <div class="clearfix">
                            <div id="TagFuel">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        Natural Gas Consumption
                    </div>
                    <div class="panel-body">
                        <div class="clearfix">
                            <div id="TagGas">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        Solar Consumption
                    </div>
                    <div class="panel-body">
                        <div class="clearfix">
                            <div id="TagSolar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<%--<script src="C3/d3.v3.min.js"></script>
    <script src="C3/c3.min.js"></script>
    <link href="C3/c3.css" rel="stylesheet" />--%>
<script src="../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
<script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
<script src="../BootStrapCSS/FChart/js/fusioncharts.js" defer></script>
<script src="../BootStrapCSS/FChart/js/themes/fusioncharts.theme.fint.js" defer></script>


<script type="text/javascript" defer>


    var chart;
    var chartEle;
    var chartFuel;
    var chartGas;
    var chartSolar;
    var fusioncharts;
    $(document).ready(function () {
        var Cmpy = '<%= Session("COMPANYID")%>';
        
        $("#ddlCompany").change(function () {
            Cmpy = $(this).val();
            funFillGraphs(Cmpy);
        });

        function funfillCompany(valcompany) {
            $.each(Companies, function (key, value) {
                $("#ddlCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });

        }
        function funFillGraphs(Cmpy)
        {
            FusionCharts.ready(function () {
                $.ajax({
                    url: '../api/EnegryMgmtDBAPI/GetWaterCount/' + Cmpy,
                    type: 'GET',
                    success: function (result) {
                        var graphval = {
                            EM_UM_MEASUR: 0, EM_THRD_QUNTY: 0, EM_DUD_QUNTY: 0
                        }
                        if (result.length != 0) {
                            graphval = result[0];
                        }

                        fusionWatercharts = new FusionCharts({
                            type: 'angulargauge',
                            renderAt: 'TagWat',
                            width: '100%',
                            height: '300',
                            dataFormat: 'json',
                            dataSource: {
                                "chart": {
                                    "caption": "Potable Water Consumption ( in " + graphval.EM_UM_MEASUR + " )",
                                    "subcaption": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                    "lowerLimit": "0",
                                    "upperLimit": "100",
                                    "showValue": "1",
                                    "valueBelowPivot": "1",
                                    "gaugeFillMix": "{dark-40},{light-40},{dark-20}",
                                    "theme": "fint"
                                },
                                "colorRange": {
                                    "color": [{
                                        "minValue": "0",
                                        "maxValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "code": "#6baa01"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "maxValue": graphval.EM_THRD_QUNTY,
                                        "code": "#f8bd19"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY,
                                        "maxValue": "400",
                                        "code": "#e44a00"
                                    }]
                                },
                                "dials": {
                                    "dial": [{
                                        "value": graphval.EM_DUD_QUNTY
                                    }]
                                },
                                "trendpoints": {
                                    "point": [{
                                        "startValue": graphval.EM_THRD_QUNTY,
                                        "useMarker": "1",
                                        "markerColor": "#F1f1f1",
                                        "markerBorderColor": "#666666",
                                        "markerRadius": "10",
                                        "markerTooltext": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                        "displayValue": "Budget",
                                        "color": "#0075c2",
                                        "thickness": "2",
                                        "alpha": "100"
                                    }]
                                }
                            }
                        }
                     );

                        fusionWatercharts.render();
                    }
                });


                $.ajax({
                    url: '../api/EnegryMgmtDBAPI/GetElectricalCount/' + Cmpy,
                    type: 'GET',
                    success: function (result) {
                        var graphval = {
                            EM_UM_MEASUR: 0, EM_THRD_QUNTY: 0, EM_DUD_QUNTY: 0
                        }
                        if (result.length != 0) {
                            graphval = result[0];
                        }

                        fusionElectricalcharts = new FusionCharts({
                            type: 'angulargauge',
                            renderAt: 'TagEle',
                            width: '100%',
                            height: '300',
                            dataFormat: 'json',
                            dataSource: {
                                "chart": {
                                    "caption": "Electricity Consumption ( in " + graphval.EM_UM_MEASUR + " )",
                                    "subcaption": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                    "lowerLimit": "0",
                                    "upperLimit": "100",
                                    "showValue": "1",
                                    "valueBelowPivot": "1",
                                    "gaugeFillMix": "{dark-40},{light-40},{dark-20}",
                                    "theme": "fint"
                                },
                                "colorRange": {
                                    "color": [{
                                        "minValue": "0",
                                        "maxValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "code": "#6baa01"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "maxValue": graphval.EM_THRD_QUNTY,
                                        "code": "#f8bd19"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY,
                                        "maxValue": "400",
                                        "code": "#e44a00"
                                    }]
                                },
                                "dials": {
                                    "dial": [{
                                        "value": graphval.EM_DUD_QUNTY
                                    }]
                                },
                                "trendpoints": {
                                    "point": [{
                                        "startValue": graphval.EM_THRD_QUNTY,
                                        "useMarker": "1",
                                        "markerColor": "#F1f1f1",
                                        "markerBorderColor": "#666666",
                                        "markerRadius": "10",
                                        "markerTooltext": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                        "displayValue": "Budget",
                                        "color": "#0075c2",
                                        "thickness": "2",
                                        "alpha": "100"
                                    }]
                                }
                            }
                        }
                   );

                        fusionElectricalcharts.render();
                    }
                });

                $.ajax({
                    url: '../api/EnegryMgmtDBAPI/GetFuelCount/' + Cmpy,
                    type: 'GET',
                    success: function (result) {
                        var graphval = {
                            EM_UM_MEASUR: 0, EM_THRD_QUNTY: 0, EM_DUD_QUNTY: 0
                        }
                        if (result.length != 0) {
                            graphval = result[0];
                        }
                        fusionFuelcharts = new FusionCharts({
                            type: 'angulargauge',
                            renderAt: 'TagFuel',
                            width: '100%',
                            height: '300',
                            dataFormat: 'json',
                            dataSource: {
                                "chart": {
                                    "caption": "Reclaimable Water Consumption ( in " + graphval.EM_UM_MEASUR + " )",
                                    "subcaption": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                    "lowerLimit": "0",
                                    "upperLimit": "100",
                                    "showValue": "1",
                                    "valueBelowPivot": "1",
                                    "gaugeFillMix": "{dark-40},{light-40},{dark-20}",
                                    "theme": "fint"
                                },
                                "colorRange": {
                                    "color": [{
                                        "minValue": "0",
                                        "maxValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "code": "#6baa01"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "maxValue": graphval.EM_THRD_QUNTY,
                                        "code": "#f8bd19"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY,
                                        "maxValue": "400",
                                        "code": "#e44a00"
                                    }]
                                },
                                "dials": {
                                    "dial": [{
                                        "value": graphval.EM_DUD_QUNTY
                                    }]
                                },
                                "trendpoints": {
                                    "point": [{
                                        "startValue": graphval.EM_THRD_QUNTY,
                                        "useMarker": "1",
                                        "markerColor": "#F1f1f1",
                                        "markerBorderColor": "#666666",
                                        "markerRadius": "10",
                                        "markerTooltext": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                        "displayValue": "Budget",
                                        "color": "#0075c2",
                                        "thickness": "2",
                                        "alpha": "100"
                                    }]
                                }
                            }
                        }
                   );

                        fusionFuelcharts.render();
                    }
                });


                $.ajax({
                    url: '../api/EnegryMgmtDBAPI/GetGasCount/' + Cmpy,
                    type: 'GET',
                    success: function (result) {
                        var graphval = {
                            EM_UM_MEASUR: 0, EM_THRD_QUNTY: 0, EM_DUD_QUNTY: 0
                        }
                        if (result.length != 0) {
                            graphval = result[0];
                        }
                        fusionGascharts = new FusionCharts({
                            type: 'angulargauge',
                            renderAt: 'TagGas',
                            width: '100%',
                            height: '300',
                            dataFormat: 'json',
                            dataSource: {
                                "chart": {
                                    "caption": "Natural Gas Consumption ( in " + graphval.EM_UM_MEASUR + " )",
                                    "subcaption": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                    "lowerLimit": "0",
                                    "upperLimit": "100",
                                    "showValue": "1",
                                    "valueBelowPivot": "1",
                                    "gaugeFillMix": "{dark-40},{light-40},{dark-20}",
                                    "theme": "fint"
                                },
                                "colorRange": {
                                    "color": [{
                                        "minValue": "0",
                                        "maxValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "code": "#6baa01"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "maxValue": graphval.EM_THRD_QUNTY,
                                        "code": "#f8bd19"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY,
                                        "maxValue": "400",
                                        "code": "#e44a00"
                                    }]
                                },
                                "dials": {
                                    "dial": [{
                                        "value": graphval.EM_DUD_QUNTY
                                    }]
                                },
                                "trendpoints": {
                                    "point": [{
                                        "startValue": graphval.EM_THRD_QUNTY,
                                        "useMarker": "1",
                                        "markerColor": "#F1f1f1",
                                        "markerBorderColor": "#666666",
                                        "markerRadius": "10",
                                        "markerTooltext": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                        "displayValue": "Budget",
                                        "color": "#0075c2",
                                        "thickness": "2",
                                        "alpha": "100"
                                    }]
                                }
                            }
                        }
                   );

                        fusionGascharts.render();

                    }
                });

                $.ajax({
                    url: '../api/EnegryMgmtDBAPI/GetSolarCount/' + Cmpy,
                    type: 'GET',
                    success: function (result) {
                        var graphval = {
                            EM_UM_MEASUR: 0, EM_THRD_QUNTY: 0, EM_DUD_QUNTY: 0
                        }
                        if (result.length != 0) {
                            graphval = result[0];
                        }
                        fusionSolarcharts = new FusionCharts({
                            type: 'angulargauge',
                            renderAt: 'TagSolar',
                            width: '100%',
                            height: '300',
                            dataFormat: 'json',
                            dataSource: {
                                "chart": {
                                    "caption": "Solar Consumption ( in " + graphval.EM_UM_MEASUR + " )",
                                    "subcaption": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                    "lowerLimit": "0",
                                    "upperLimit": "100",
                                    "showValue": "1",
                                    "valueBelowPivot": "1",
                                    "gaugeFillMix": "{dark-40},{light-40},{dark-20}",
                                    "theme": "fint"
                                },
                                "colorRange": {
                                    "color": [{
                                        "minValue": "0",
                                        "maxValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "code": "#6baa01"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY * 0.8,
                                        "maxValue": graphval.EM_THRD_QUNTY,
                                        "code": "#f8bd19"
                                    }, {
                                        "minValue": graphval.EM_THRD_QUNTY,
                                        "maxValue": "400",
                                        "code": "#e44a00"
                                    }]
                                },
                                "dials": {
                                    "dial": [{
                                        "value": graphval.EM_DUD_QUNTY
                                    }]
                                },
                                "trendpoints": {
                                    "point": [{
                                        "startValue": graphval.EM_THRD_QUNTY,
                                        "useMarker": "1",
                                        "markerColor": "#F1f1f1",
                                        "markerBorderColor": "#666666",
                                        "markerRadius": "10",
                                        "markerTooltext": "Max Budget Value  - " + graphval.EM_THRD_QUNTY,
                                        "displayValue": "Budget",
                                        "color": "#0075c2",
                                        "thickness": "2",
                                        "alpha": "100"
                                    }]
                                }
                            }
                        }
                   );

                        fusionSolarcharts.render();

                    }
                });

            });
        }
        funfillCompany(Cmpy);
        funFillGraphs(Cmpy);
    });

   
</script>

