﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1180" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
		 { CompanyID: "Demo41.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYzUyMTQ2YTQtYmU5ZS00MjJiLTgxNzgtY2RjZTMyYjRiYjQ5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
         { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZTk0MjMwMzQtNGMxMi00MjUxLTg3NTMtNDc4MTM0MWYwMTRmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZTk0MjMwMzQtNGMxMi00MjUxLTg3NTMtNDc4MTM0MWYwMTRmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
         { CompanyID: "Aadhar.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNmVhMTYyYmEtN2NhYS00NDdhLTkyMTMtYjhjMDRlN2ZkODcyIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" }

       ];

    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>


