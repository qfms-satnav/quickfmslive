﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1180" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
        { CompanyID: "[TATA_CAP].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNTQ3M2U2MmYtNmRmMy00NjM0LWJmZmEtMGEzMjc5NDA1MzQ2IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },];
        // { CompanyID: "Mahindra_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNjk2MzA2NjktNTczYS00MmVmLTgwNDQtMjg2ZDEzMjE3OGIwIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
        // { CompanyID: "Mahindra.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNWIyODk2YTctNmE3ZC00MWExLTllZWItMjM3ZjhiZWIyNTM4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },];
                                      
    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>

