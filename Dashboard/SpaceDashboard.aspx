﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SpaceDashboard.aspx.cs" Inherits="Dashboard_SpaceDashboard" %>


<script src="../BlurScripts/BlurJs/bootstrap-multiselect.js" defer></script>
<link href="../BlurScripts/BlurCss/bootstrap-multiselect.css" rel="stylesheet" />
<div id="spce">
    <form id="form1" runat="server">
        <div class="row" style="display: none;">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>City </label>
                    <asp:listbox id="ddlCity" runat="server" selectionmode="Multiple">
                    </asp:listbox>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>location </label>
                    <asp:listbox id="ddlLocation" runat="server" selectionmode="Multiple">
                    </asp:listbox>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>Vertical </label>
                    <asp:listbox id="ddlVertical" runat="server" selectionmode="Multiple">
                    </asp:listbox>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>CostCenter </label>
                    <asp:listbox id="ddlCostCenter" runat="server" selectionmode="Multiple">
                    </asp:listbox>
                </div>
            </div>

            <%--<button type="button" id="CTYID" class="btn btn-primary btn-block">
                    <span id="SPCTY" class="glyphicon glyphicon-ok"></span>City
                </button>--%>

            <%-- <div class="col-md-3 col-sm-3 col-xs-3">
                    <button type="button" id="LOCID" class="btn btn-primary btn-block">
                        <span id="SPLCM" class="glyphicon glyphicon-ok"></span>Location
                    </button>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <button type="button" id="VERID" class="btn btn-primary btn-block">
                        <span id="SPVER" class="glyphicon glyphicon-ok"></span>Vertical
                    </button>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <button type="button" id="DEPID" class="btn btn-primary btn-block">
                        <span id="SPDEP" class="glyphicon glyphicon-ok"></span><%=Session["child"] %>
                    </button>
                </div>--%>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlsmsCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>Sort</label>
                    <select id="droplst" class="form-control selectpicker with-search" data-live-search="true">
                        <option value="1">High to Low</option>
                        <option value="2">Low to High</option>
                    </select>
                </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>Value</label>
                    <input type="text" class="form-control selectpicker with-search" id="txtval" />
                </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <input type="button" id="btnsubmit" class="btn btn-primary" value="Submit" />
                </div>
            </div>

        </div>

        <%-- <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Company</label>
                <select id="Select1" class="form-control selectpicker with-search" data-live-search="true"></select>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="form-group">
                <label>Company</label>
                <select id="Select2" class="form-control selectpicker with-search" data-live-search="true"></select>
            </div>
        </div>--%>

        <%--  <div class="col-md-4 col-md-6">
                <div class="row">
                    <div class="col-md-9 text-center">
                        <button type="button" id="30daysreldetails" class="alert alert-info" style="width: 230px;">
                            <i class="fa fa-tasks pull-left" aria-hidden="true"></i>seats releasing in next 30 days.  
                            <span class="badge pull-right" id="lbl30dayscount"></span>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-md-6">
                <div class="row">
                    <div class="col-md-9 text-center">
                        <button type="button" id="60daysreldetails" class="alert alert-info" style="width: 230px;">
                            <i class="fa fa-tasks pull-left" aria-hidden="true"></i>seats releasing in next 60 days.  
                            <span class="badge pull-right" id="lbl60dayscount"></span>
                        </button>
                    </div>
                </div>
            </div>--%>

        <%--        <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <i class="fa fa-pie-chart fa-fw"></i>
                    <label>Space Utilization Type</label>
                    <select id="droplst" class="form-control selectpicker with-search" data-live-search="true">
                        <option value="0">City wise utilization</option>
                        <option value="1">Location wise utilization</option>
                        <option value="2">Vertical wise utilization</option>
                        <option value="3">Department wise utilization</option>
                    </select>
                </div>
            </div>
        </div>--%>
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <%--  <div class="col-md-12 col-sm-12 col-xs-12" align="right">
                        <a id="btnQueryString">Full View</a>
                    </div>--%>
                    <label>Utilization</label>
                    <div class="col-md-12 col-sm-12 col-xs-12" align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('SpcUtn')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>
                    </div>
                    <div class="panel-body">
                        <div id="SpcUtn"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
                <%--  <asp:button id="30daysreldetails" class="btn btn-primary" value="Submit"> <i class="fa fa-tasks pull-left" aria-hidden="true"></i>seats releasing in next 30 days.  
                            <span class="badge pull-right" id="lbl30dayscount"></span></asp:button>--%>
                <button type="button" id="btnrelin30Days" class="alert alert-info" style="width: 230px;">
                    <i class="fa fa-tasks pull-left" aria-hidden="true"></i>seats releasing in next 30 days.  
                            <span class="badge pull-right" id="lbl30dayscount"></span>
                </button>
                <%--<input type="button" id="btnrel" class="btn btn-primary" value="30daysreldetails" />--%>
                <button type="button" id="btnrel" class="alert alert-info" style="width: 230px;">
                    <i class="fa fa-tasks pull-left" aria-hidden="true"></i>seats releasing in next 60 days.  
                            <span class="badge pull-right" id="lbl60dayscount"></span>
                </button>

            </div>



        </div>


        <div class="row" style="padding-top: 10px;">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        <i class="fa fa-pie-chart fa-fw"></i>Unused Area
                    </div>
                    <div align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('SpcUnused')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>
                    </div>
                    <div class="panel-body">
                        <div id="SpcUnused"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        <i class="fa fa-pie-chart fa-fw"></i>Allocated But Not Occupied Cost
                    </div>
                    <div style="padding-top: 10px;">
                        <%--    <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="form-group">
                                <asp:label id="txtvertical" runat="server" text="Label">Vertical</asp:label>
                                <select id="ddlVertical" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>--%>
                        <div align="right" style="padding-right: 14px">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('divalloc')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div id="divalloc"></div>
                    </div>

                </div>
            </div>
        </div>


        <div class="modal fade" id="divBarGraph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="divBarGraphHeading"></h4>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-3 pull-left">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('tblBarGraph')">
                                <span class="glyphicon glyphicon-print"></span>Print
                            </button>
                        </div>
                        <table id="tblBarGraph" class="table table-condensed table-bordered table-hover table-striped">
                            <tr>
                                <th>Space Id</th>
                                <th>Employee Name</th>
                                <th>Department Name</th>
                                <th>Vertical Name</th>
                                <th>Floor Name</th>
                                <th>Location Name</th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DivCityUnused" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="DivCityUnusedHeading"></h4>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-3 pull-left">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('tblcitybargraph')">
                                <span class="glyphicon glyphicon-print"></span>Print
                            </button>
                        </div>
                        <table id="tblcitybargraph" class="table table-condensed table-bordered table-hover table-striped">
                            <tr>
                                <th>Space Id</th>
                                <th>Space Type</th>
                                <th>Floor Name</th>
                                <th>Tower Name</th>
                                <th>Location Name</th>
                                <th>City Name</th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DivCityvertial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H1"></h4>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-3 pull-left">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('DivCityvertialbar')">
                                <span class="glyphicon glyphicon-print"></span>Print
                            </button>
                        </div>
                        <table id="DivCityvertialbar" class="table table-condensed table-bordered table-hover table-striped">
                            <tr>
                                <th>Space Id</th>
                                <th>Vertical Name</th>
                                <th>Space Type</th>
                                <th>Floor Name</th>
                                <th>Tower Name</th>
                                <th>Location Name</th>
                                <th>City Name</th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="../BlurScripts/BlurJs/DashboardPrint.js" defer></script>
<script src="../Scripts/jQuery.print.js" defer></script>
<%--<link href="jquery.dropdown.min.css" rel="stylesheet" />--%>
<%--<script src="jquery.dropdown.min.js"></script>
<script src="mock.js"></script>--%>
<style>
    .multiselect-container {
        overflow-y: auto;
        overflow-x: auto;
    }
</style>
<script type="text/javascript" defer>

    $(document).ready(function () {

        $('#ddlCity').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: 200
        });

        $('#ddlLocation').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: 200
        });

        $('#ddlVertical').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: 200
        });
        $('#ddlCostCenter').multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: 200
        });

        var companyid = '<%= Session["COMPANYID"]%>';
        var flag = 1;
        $("#droplst").change(function (value) {

        });

        var drpval = $("#txtval").val('25');

        function Getcities() {
            $.ajax({
                url: '../api/SpaceDBAPI/getCities',
                type: 'GET',
                success: function (result) {
                    
                    $.each(result.data, function (key, value) {
                        //$("#ddlLocation").append($("<option></option>").val(value.VER_CODE).html(value.VER_NAME));
                        $("#ddlCity").append($("<option></option>").val(value.CTY_CODE).html(value.CTY_NAME));
                        $("#ddlCity").multiselect('rebuild')
                    });
                }
            });
        }

        function getLocations() {
            $.ajax({
                url: '../api/SpaceDBAPI/getLocations',
                type: 'GET',
                success: function (result) {
                   
                    $.each(result.data, function (key, value) {
                        //$("#ddlLocation").append($("<option></option>").val(value.VER_CODE).html(value.VER_NAME));
                        $("#ddlLocation").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
                        $("#ddlLocation").multiselect('rebuild')
                    });
                }
            });
        }

        function getVerticals() {
            $.ajax({
                url: '../api/SpaceDBAPI/getVerticals',
                type: 'GET',
                success: function (result) {
                   
                    $.each(result.data, function (key, value) {
                        //$("#ddlLocation").append($("<option></option>").val(value.VER_CODE).html(value.VER_NAME));
                        $("#ddlVertical").append($("<option></option>").val(value.VER_CODE).html(value.VER_NAME));
                        $("#ddlVertical").multiselect('rebuild')
                    });
                }
            });
        }

        function getCostCenters() {
            $.ajax({
                url: '../api/SpaceDBAPI/getCostCenters',
                type: 'GET',
                success: function (result) {
                    
                    $.each(result.data, function (key, value) {
                        //$("#ddlLocation").append($("<option></option>").val(value.VER_CODE).html(value.VER_NAME));
                        $("#ddlCostCenter").append($("<option></option>").val(value.Cost_Center_Code).html(value.Cost_Center_Name));
                        $("#ddlCostCenter").multiselect('rebuild')
                    });
                }
            });
        }

        $("#ddlCity").change(function () {
            $('#ddlCostCenter option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlCostCenter').multiselect('refresh');
            $('#ddlVertical option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlVertical').multiselect('refresh');
            $('#ddlLocation option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlLocation').multiselect('refresh');
        });

        $("#ddlLocation").change(function () {
            $('#ddlCostCenter option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlCostCenter').multiselect('refresh');
            $('#ddlVertical option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlVertical').multiselect('refresh');
            $('#ddlCity option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlCity').multiselect('refresh');
        });

        $("#ddlVertical").change(function () {
            $('#ddlCostCenter option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlCostCenter').multiselect('refresh');
            $('#ddlCity option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlCity').multiselect('refresh');
            $('#ddlLocation option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlLocation').multiselect('refresh');
        });

        $("#ddlCostCenter").change(function () {
            $('#ddlCity option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlCity').multiselect('refresh');
            $('#ddlVertical option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlVertical').multiselect('refresh');
            $('#ddlLocation option:selected').each(function () {
                $(this).prop('selected', false);
            })
            $('#ddlLocation').multiselect('refresh');
        });

        Getcities();
        getLocations();
        getVerticals();
        getCostCenters();
        GetUtilizationData();

        var selectedgraph;
        var selectedvalue;

        function GetUtilizationData() {
            var selectedCities = [];
            $('#ddlCity :selected').each(function (i, selected) {
                selectedCities[i] = $(selected).val();
            });
            var Cities = selectedCities.join("','");

            var selectedLocation = [];
            $('#ddlLocation :selected').each(function (i, selected) {
                selectedLocation[i] = $(selected).val();
            });
            var Locations = selectedLocation.join("','");

            var selectedVertical = [];
            $('#ddlVertical :selected').each(function (i, selected) {
                selectedVertical[i] = $(selected).val();
            });
            var Vertical = selectedVertical.join("','");

            var selectedCostCenter = [];
            $('#ddlCostCenter :selected').each(function (i, selected) {
                selectedCostCenter[i] = $(selected).val().replace(/'/g, '"');
            });
            var CostCenter = selectedCostCenter.join("','");


            var param = { COMPANY: companyid, VALUE: $("#txtval").val(), TYPE: $("#droplst").val(), FLAG: flag, citylist: Cities, LocationLst: Locations, VerticalLst: Vertical, CostCenterLst: CostCenter };
            $.ajax({
                url: '../api/SpaceDBAPI/Get_Space_Utilization',
                type: 'POST',
                data: param,
                success: function (result) {
                    var chart = c3.generate({
                        data: {
                            x: 'y',
                            columns: [
                                result.Citydata,
                                result.Utilization
                            ],
                            type: 'line',
                            labels: true,
                            color: function (inColor, data) {
                                var colors = result.Dynamiccols;
                                if (data.index !== undefined) {
                                    return colors[data.index];
                                }
                                return inColor;
                            }
                        },
                        axis: {
                            rotated: false,
                            x: {
                                type: 'category',
                                tick: {
                                    rotate: 65,
                                    multiline: false
                                },
                            }
                        },
                        scrollbar: {
                            enabled: true
                        },
                        bindto: '#SpcUtn'
                    });
                }
            });
        }

        function GetCityWiseUnusedArea() {
            var param = { COMPANY: companyid, VALUE: $("#txtval").val(), TYPE: $("#droplst").val(), FLAG: flag };
            $.ajax({
                url: '../api/SpaceDBAPI/Get_City_Wise_UnusedArea',
                type: 'POST',
                data: param,
                success: function (result) {
                    var chart = c3.generate({
                        data: {
                            x: 'x',
                            columns: [
                                result.Citydata,
                                result.UNUSED,
                                result.UNUSERAREA,
                            ],
                            type: 'bar',
                            labels: true,

                            onclick: function (data) {
                                flag = 1
                                var data = this.categories()[data.index]
                                var url = "/Dashboard/Views/ViewFullData.aspx?selectedvalue=" + data + "&flag=" + flag + "&Value=" + result.obj + "";
                                window.open(url);
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                tick: {
                                    rotate: 65,
                                    multiline: false
                                },
                                height: 100
                            }
                        },
                        bindto: '#SpcUnused'
                    });
                }
            });
        }

        function GetCityWiseAllocatedButNotOccupied() {
            var param = { COMPANY: companyid, VALUE: $("#txtval").val(), TYPE: $("#droplst").val(), FLAG: flag };
            $.ajax({
                url: '../api/SpaceDBAPI/Get_Vertical_Wise_Allocated_But_Not_Occupied',
                type: 'POST',
                data: param,
                success: function (result) {
                   
                    var chart = c3.generate({
                        data: {
                            x: 'x',
                            columns: [
                                result.Citydata,
                                result.UNUSED,
                                result.UNUSERAREA,
                            ],
                            type: 'bar',
                            labels: true,
                            onclick: function (data) {
                                flag = 2;
                                var data = this.categories()[data.index]
                                var url = "/Dashboard/Views/ViewFullData.aspx?selectedvalue=" + data + "&flag=" + flag + "&Value=" + result.obj + "";
                                window.open(url);
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                tick: {
                                    rotate: 65,
                                    multiline: false
                                },
                                height: 100
                            }
                        },
                        bindto: '#divalloc'
                    });
                }
            });
        }

        function GetReleaseData(companyid) {
            $.ajax({
                url: '../api/SpaceDBAPI/Get_Space_Release_Details/' + companyid,
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    $("#lbl30Dayscount").html(data.Table1[0].CNT);
                    $("#lbl60Dayscount").html(data.Table2[0].CNT);
                },
                error: function (result) {
                }
            });
        }

        //$("#30DaysRelDetails").click(function () {
        //    alert('hi');
        //    //e.preventDefault();
        //    $("#divBarGraph").modal("show");
        //    GetRequestDetails(companyid, 30);
        //});

        $("#btnrelin30Days").click(function () {
            $("#divBarGraph").modal("show");
            GetRequestDetails(companyid, 30);
        });

        // $("#30DaysRelDetails").click(function () {
        //     alert('hi');
        //    $("#divBarGraph").modal("show");
        //    GetRequestDetails(companyid, 30);
        //});

        $("#btnrel").click(function () {
            $("#divBarGraph").modal("show");
            GetRequestDetails(companyid, 60);
        });


        //$("#btnrelin60Days").click(function () {
        //    alert('hi');
        //    $("#divBarGraph").modal("show");
        //    GetRequestDetails(companyid, 60);
        //});

        function GetRequestDetails(companyid, value) {
            var params = { companyid: companyid, value: value };
            $.ajax({
                url: "../api/SpaceDBAPI/Get_Space_Release_Details_Onclick",
                type: "POST",
                data: params,
                dataType: 'json',
                success: function (data) {
                    var table = $('#tblBarGraph');
                    $('#tblBarGraph td').remove();
                    for (var i = 0; i < data.Table.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table[i].SSA_SPC_ID + "</td>" +
                            "<td>" + data.Table[i].SSAD_AUR_ID + "</td>" +
                            "<td>" + data.Table[i].COST_CENTER_NAME + "</td>" +
                            "<td>" + data.Table[i].VER_NAME + "</td>" +
                            "<td>" + data.Table[i].FLR_NAME + "</td>" +
                            "<td>" + data.Table[i].LCM_NAME + "</td>" +
                            "</tr>");
                    }
                    if (data.Table.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'>No Records Found.</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }

        function GetCityUnusedDetails(Code) {
            var params = { selectedval: Code };
            $.ajax({
                url: "../api/SpaceDBAPI/Get_Space_Unused_Details_Onclick",
                type: "POST",
                data: params,
                success: function (data) {
                    var table = $('#tblcitybargraph');
                    $('#tblcitybargraph td').remove();
                    for (var i = 0; i < data.Table.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table[i].SPC_ID + "</td>" +
                            "<td>" + data.Table[i].SPC_TYPE + "</td>" +
                            "<td>" + data.Table[i].FLR_NAME + "</td>" +
                            "<td>" + data.Table[i].TWR_NAME + "</td>" +
                            "<td>" + data.Table[i].LCM_NAME + "</td>" +
                            "<td>" + data.Table[i].CTY_NAME + "</td>" +
                            "</tr>");
                    }
                    if (data.Table.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'>No Records Found.</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }

        function GetCityWiseVerDetails(Code, Cty_code) {
            var params = { Ver_code: Code, Cty_code: Cty_code };
            $.ajax({
                url: "../api/SpaceDBAPI/Get_City_Wise_Ver_Onclick",
                type: "POST",
                data: params,
                success: function (data) {
                    var table = $('#DivCityvertialbar');
                    $('#DivCityvertialbar td').remove();
                    for (var i = 0; i < data.Table.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table[i].SPC_ID + "</td>" +
                            "<td>" + data.Table[i].VER_NAME + "</td>" +
                            "<td>" + data.Table[i].SPC_TYPE + "</td>" +
                            "<td>" + data.Table[i].FLR_NAME + "</td>" +
                            "<td>" + data.Table[i].TWR_NAME + "</td>" +
                            "<td>" + data.Table[i].LCM_NAME + "</td>" +
                            "<td>" + data.Table[i].CTY_NAME + "</td>" +
                            "</tr>");
                    }
                    if (data.Table.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'>No Records Found.</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }

        var count = 1;

        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (count == 1) {
                    GetCityWiseUnusedArea();
                    count++;
                }
                else if (count == 2) {
                    GetCityWiseAllocatedButNotOccupied();
                    count++;
                }
            }
        });

        function funfillverticals() {
            $.ajax({
                url: '../api/SpaceDBAPI/getVerticals',
                type: 'GET',
                success: function (result) {
                    $.each(result.data, function (key, value) {
                        $("#ddlVertical").append($("<option></option>").val(value.VER_CODE).html(value.VER_NAME));
                    });
                }
            });
        }

        GetReleaseData(companyid);
        funfillCompany(companyid);

        $("#ddlVertical").change(function () {
            GetCityWiseAllocatedButNotOccupied($(this).val());
        });

        function funfillCompany(valcompany) {
            $.each(Companies, function (key, value) {
                $("#ddlsmsCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });
        }
        function enableDisableCompanyDDL(compid) {
            if (compid == "1") {
                $("#ddlsmsCompany").prop('disabled', false);
            }
            else {
                $("#ddlsmsCompany").prop('disabled', true);
                $('#ddlsmsCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }
        setTimeout(function () { enableDisableCompanyDDL(companyid) }, 1000);

        //$("#btnQueryString").bind("click", function () {
        //    var url = "FullViewGraphs.aspx?selectedvalue=" + selectedvalue + "&value=" + selectedgraph + "";
        //    window.open(url);
        //});

        $("#ddlsmsCompany").change(function () {
            companyid = $(this).val();
        });

        $('#SPCTY').show();
        $('#SPLCM').hide();
        $("#SPVER").hide();
        $("#SPDEP").hide();

        function RepeatData() {
            count = 1;
            $("#SpcUnused").empty();
            $("#divalloc").empty();
        }

        $("#CTYID").click(function () {
            var selectednumbers = [];
            $('#ddlCity :selected').each(function (i, selected) {
                selectednumbers[i] = $(selected).val();
            });
            var a = selectednumbers.join(", ");
           
            //flag = 1;
            //RepeatData();
            //GetUtilizationData();
            //$('#SPCTY').show();
            //$("#SPLCM").hide();
            //$("#SPVER").hide();
            //$("#SPDEP").hide();
        });

        $("#LOCID").click(function () {
            flag = 2;
            RepeatData();
            GetUtilizationData();
            $('#SPLCM').show();
            $('#SPCTY').hide();
            $("#SPVER").hide();
            $("#SPDEP").hide();
        });

        $("#VERID").click(function () {
            flag = 3;
            RepeatData();
            GetUtilizationData();
            $('#SPLCM').hide();
            $('#SPCTY').hide();
            $("#SPVER").show();
            $("#SPDEP").hide();
        });

        $("#DEPID").click(function () {
            flag = 4;
            RepeatData();
            GetUtilizationData();
            $('#SPLCM').hide();
            $('#SPCTY').hide();
            $("#SPVER").hide();
            $("#SPDEP").show();
        });

        $("#btnsubmit").click(function () {
            RepeatData();
            GetUtilizationData();
        });


    });

</script>
