﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DashboardV2.aspx.vb" Inherits="DashboardV2" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../BootStrapCSS/FMS_UI/css/flaticon.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/notify.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/prettify.css" rel="stylesheet" />
    <style>
        a {
             font-size: 1rem;
        }
        .card{
            padding:0px;
        }
    </style>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div ba-panel-title="Dashboard" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
        <div class="card" style="padding: 10px;">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a href="#profile" class="nav-link active" data-toggle="tab"><i class="flaticon-people"></i>
                        User Profile
                    </a>
                </li>
                <li class="nav-item" id="propertytab">
                    <a href="#property" class="nav-link" data-toggle="tab"><i class="flaticon-home"></i>
                        Properties
                    </a>
                </li>
                <li class="nav-item" id="spacetab">
                    <a href="#space" class="nav-link" data-toggle="tab"><i class="flaticon-office"></i>
                        Space
                    </a>
                </li>
                <li class="nav-item" id="assettab">
                    <a href="#asset" class="nav-link" data-toggle="tab"><i class="flaticon-gold"></i>
                        Assets
                    </a>
                </li>
                <li class="nav-item" id="maintenancetab">
                    <a href="#maintenance" class="nav-link" data-toggle="tab"><i class="flaticon-settings-work-tool"></i>
                        Maintenance
                    </a>
                </li>
                <li class="nav-item" id="helpdesktab">
                    <a href="#helpdesk" class="nav-link" data-toggle="tab"><i class="flaticon-life-preserver"></i>
                        Help Desk
                    </a>

                </li>
                <li class="nav-item" id="conferencetab">
                    <a href="#conference" class="nav-link" data-toggle="tab"><i class="flaticon-businessmen-having-a-group-conference"></i>
                        Reservation
                    </a>
                </li>
                <li class="nav-item" id="businesscardtab">
                    <a href="#businesscard" class="nav-link" data-toggle="tab"><i class="flaticon-newspaper"></i>
                        Business Card
                    </a>
                </li>
                <li class="nav-item" id="GuestHousetab">
                    <a href="#GuestHouse" class="nav-link" data-toggle="tab" id="ghTitle"><i class="flaticon-home"></i>
                        Guest House
                    </a>
                </li>
                <li class="nav-item" id="Energytab">
                    <a href="#Energy" class="nav-link" data-toggle="tab"><i class="flaticon-lightning-electric-energy"></i>
                        Energy Management
                    </a>
                </li>
                <li class="nav-item" id="HelpdeskPowerbii">
                    <a href="#HelpdeskPowerbi" class="nav-link" data-toggle="tab"><i class="flaticon-lightning-electric-energy"></i>
                        Helpdesk Dashboard v2
                    </a>
                </li>
                <li class="nav-item" id="Branch">
                    <a href="#BranchPerformance" class="nav-link" data-toggle="tab"><i class="flaticon-lightning-electric-energy"></i>
                        BranchPerformance
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade in active show" id="profile"></div>
                <div class="tab-pane fade" id="property" runat="server"></div>
                <div class="tab-pane fade" id="space" runat="server"></div>
                <div class="tab-pane fade" id="asset" runat="server"></div>
                <div class="tab-pane fade" id="maintenance" runat="server"></div>
                <div class="tab-pane fade" id="helpdesk" runat="server"></div>
                <div class="tab-pane fade" id="conference" runat="server"></div>
                <div class="tab-pane fade" id="businesscard" runat="server"></div>
                <div class="tab-pane fade" id="GuestHouse" runat="server"></div>
                <div class="tab-pane fade" id="Energy" runat="server"></div>
                <div class="tab-pane fade" id="HelpdeskPowerbi" runat="server"></div>
                <div class="tab-pane fade" id="BranchPerformance" runat="server"></div>
            </div>

            <div class="modal fade" id="confirmationModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <b><span style="font-size: larger;">My Recent Bookings:</span></b>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <%-- <h4 class="modal-title">Confirmation</h4>--%>
                        </div>
                        <div class="modal-body">

                            <table class="table" id="spacedetails">
                                 <thead>
                                    <tr>
                                        <th scope="col">Select</th>
                                        <%--  <th scope="col">Seats</th>--%>
                                        <th scope="col">Space ID</th>
                                        <th scope="col">Shift Name</th>
                                        <th scope="col">Location</th>
                                        <th scope="col">Floor</th>
                                        <th scope="col">From Date</th>
                                        <th scope="col">To Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <%--<button type="button" class="btn btn-primary" onclick="No()">No</button>--%>
                            <button type="button" class="btn btn-primary"  onclick="proceedWithAction()">Book Selected Seat</button>
                            <button type="button" class="btn btn-primary" onclick="bookVacantSeat()">Book Any Seat</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="NewUser" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                              <b><span style="font-size: larger;">My Recent Bookings:</span></b>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                            <%-- <h4 class="modal-title">Confirmation</h4>--%>
                           <div class="modal-header">
                          <b><span >No Recent Bookings Are avaiable Please click on 'Book any Seat' to book a seat </span></b>
                        </div>
                       
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            <button type="button" class="btn btn-primary" onclick="bookVacantSeat()">Book Any Seat</button>
                        </div>

                    </div>
                </div>
            </div>

             <div class="modal fade" id="confirmation" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                              
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                            <%-- <h4 class="modal-title">Confirmation</h4>--%>
                           <div class="modal-header">
                          <b><span >Do you want to access the booking Screen ?</span></b>
                        </div>
                       
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            <button type="button" class="btn btn-primary" onclick="BookingScreen()">YES</button>
                        </div>

                    </div>
                </div>
            </div>


        </div>




    </div>

    <%--C3 charts start here--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../BlurScripts/BlurJs/notify.js" defer></script>
    <script src="../BlurScripts/BlurJs/prettify.js" defer></script>
    <script src="C3/d3.v3.min.js" defer></script>
    <link href="C3/c3.min.css" rel="stylesheet" />
    <script src="C3/c3.min.js" defer></script>
    <script src="C3/jshint.js" defer></script>
    <script src="../Scripts/moment.min.js"></script>

    <script type="text/javascript">
        //const { json } = require("modernizr");

        $(document).ready(function () {
            var space = '<%=Session("space")%>';
            var conference = '<%=Session("conference")%>';
            var businesscard = '<%=Session("businesscard")%>';
            var GuestHouse = '<%=Session("GuestHouse")%>';
            var Energy = '<%=Session("Energy")%>';
            var maintenance = '<%=Session("maintenance")%>';
            var property = '<%=Session("property")%>';
            var helpdesk = '<%=Session("helpdesk")%>';
            var assets = '<%=Session("Assets")%>';
            var companyid = '<%= Session("TENANT")%>';


            fillCompanyDet();
            /*enablesys_preference();*/
            validateUserFirstLogin();
            /*validateuserspacedetails();*/
            /* validateUserrole();*/

            if (space == 1) {
                validateUserrole();
                $("#spacetab").show();
            } else {
                $("#spacetab").hide();
            }
            if (conference == 1) {
                $("#conferencetab").show();
            } else {
                $("#conferencetab").hide();
            }
            if (businesscard == 1) {
                $("#businesscardtab").show();
            } else {
                $("#businesscardtab").hide();
            }
            if (GuestHouse == 1) {
                $("#GuestHousetab").show();
            } else {
                $("#GuestHousetab").hide();
            }
            if (Energy == 1) {
                $("#Energytab").show();
            } else {
                $("#Energytab").hide();
            }
            if (maintenance == 1) {
                $("#maintenancetab").show();
            } else {
                $("#maintenancetab").hide();
            }
            if (property == 1) {
                $("#propertytab").show();
            } else {
                $("#propertytab").hide();
            }
            if (helpdesk == 1) {
                $("#helpdesktab").show();
            } else {
                $("#helpdesktab").hide();
            }
            if (assets == 1) {
                $("#assettab").show();
            } else {
                $("#assettab").hide();
            }
            if ('Aadhar.dbo' == companyid) {
                $("#HelpdeskPowerbii").show();
            } else {
                $("#HelpdeskPowerbii").hide();
            }
            if ('MAX_UAT.dbo' == companyid) {
                $("#Branch").show();
            }
            else {
                $("#Branch").hide();
            }
        });
        var Companies = [];

        function fillCompanyDet(valcompany) {

            $.ajax({
                url: '../api/Utility/GetCompanies',
                type: 'GET',
                success: function (result) {
                    Companies = result.data;
                }
            });
        }

        var sys_preference = [];
        //function enablesys_preference() {
        //    debugger;
        //    $.ajax({
        //        url: '../api/Utility/GetSysPreferences',
        //        type: 'GET',
        //        success: function (result) {
        //            sys_preference = result.data;
        //            if (result.data[2].SYSP_CODE == 'Auto allocation of seats option while booking' && result.data[2].SYSP_VAL1 == 1 && result.data[2].SYSP_VAL2 == 1) {
        //                validateUserrole();
        //            }
        //        }
        //    });
        //}



        function showConfirmationModalDelayed() {
            setTimeout(function () {
                $('#confirmationModal').modal('show');
            }, 3000);
        }
        function shownewuserpopup() {
            setTimeout(function () {
                $('#NewUser').modal('show');
            }, 3000);
        }

        function No() {
            setTimeout(function () {

                $('#confirmation').modal('show');
                $('#confirmationModal').modal('hide');
            }, 3000);
        }
        Status = {};
        function validateUserFirstLogin(valstatus) {

            $.ajax({
                url: '../api/Utility/ValidateUserFirstLogin',
                type: 'GET',
                success: function (result) {
                    Status = result.data[0].AUR_ID;
                    //console.log(result.data[0].AUR_ID)
                    //var sta = _.find($scope.Country, { AUR_ID: value.AUR_ID });
                    //console.log(sta)
                 <%--   if (result.data[0].AUR_ID == 1) {
                        $.notify('&nbsp  Welcome <% =Session("LoginUser") %> , Kindly update the current system generated password ', { color: '#fff', background: '#20d67b', close: true, icon: "user", position: 'middle', delay: '50000' });
                    }
                    else
                    {--%>
                    $.notify('&nbsp  Welcome <% =Session("LoginUser") %> ', { color: '#fff', background: '#20d67b', close: true, icon: "user", position: 'middle' });
                    //}
                }
            });
        }

        function validateUserrole() {
            debugger;
            $.ajax({
                url: '../api/Utility/GetRoleAndReportingManger',
                type: 'GET',
                success: function (result) {
                    debugger;
                    Status = result.data[0].AUR_ROLE;
                    if (Status == '1' || Status == '6' || Status == '3' || Status == '13' || Status == '16' || Status == '14' || Status == '15') {
                        validateuserspacedetails();
                    }


                }
            });
        }




        var valstatus = {};
        var vacantstatus = {};
        function validateuserspacedetails() {
            $.ajax({
                url: '../api/MaploaderAPI/validateuserspacedetails',
                type: 'GET',
                success: function (result) {
                    console.log(result);
                    if (result.data.Table[0].SeatBook == 0) {

                    }
                    else {

                        var L3BArray = [];
                        var ANYSPCBOOKArray = [];

                        for (var i = 0; i < result.data.Table.length; i++) {
                            var seatBooking = result.data.Table[i].SeatBooking;

                            if (seatBooking == 'L3B') {
                                L3BArray.push(result.data.Table[i]);
                            } else if (seatBooking == 'ANYSPCBOOK') {
                                ANYSPCBOOKArray.push(result.data.Table[i]);
                            }
                        }
                        var output = ''
                        console.log('L3BArray:', L3BArray);
                        function bindDataToSelectedRow(row) {
                            var selectedSpaceID = row.find('td:eq(1)').text();
                            var selectedFormattedFromDate = row.find('td:eq(5)').text();
                            var selectedFormattedToDate = row.find('td:eq(6)').text();

                            valstatus = L3BArray.find(function (value) {
                                var formattedFromDate = formatDate(value.FROM_DATE);
                                var formattedToDate = formatDate(value.TO_DATE);

                                return value.Space_ID === selectedSpaceID &&
                                    formattedFromDate === selectedFormattedFromDate &&
                                    formattedToDate === selectedFormattedToDate;
                            });
                            console.log(valstatus);
                        }

                        if (L3BArray.length > 0) {
                            function formatDate(dateString) {
                                var date = new Date(dateString);
                                return (
                                    (date.getMonth() + 1).toString().padStart(2, '0') +
                                    '/' +
                                    (date.getDate()).toString().padStart(2, '0') +
                                    '/' +
                                    date.getFullYear()
                                );
                            }

                            L3BArray.forEach(function (value) {

                                var formattedFromDate = formatDate(value.FROM_DATE);
                                var formattedToDate = formatDate(value.TO_DATE);

                                output += "<tr>" +
                                    "<td><input type='radio' name='rowRadio'></td>" +
                                    "<td>" + value.Space_ID + "</td>" +
                                    "<td>" + value.SH_NAME + "</td>" +
                                    "<td>" + value.LCM_NAME + "</td>" +
                                    "<td>" + value.FLR_NAME + "</td>" +
                                    "<td>" + formattedFromDate + "</td>" +
                                    "<td>" + formattedToDate + "</td>" +
                                    "</tr>";
                            });

                            $('tbody').append(output);
                            $('input[type="radio"]:first').prop('checked', true);


                            bindDataToSelectedRow($('input[type="radio"]:first').closest('tr'));

                            $('#spacedetails').on('click', 'input[type="radio"]', function () {


                                $('tbody tr').removeClass('selected');

                                var selectedRow = $(this).closest('tr');
                                selectedRow.addClass('selected');
                                bindDataToSelectedRow(selectedRow);

                                var selectedSpaceID = selectedRow.find('td:eq(1)').text();
                                var selectedFormattedFromDate = selectedRow.find('td:eq(5)').text();
                                var selectedFormattedToDate = selectedRow.find('td:eq(6)').text();

                                valstatus = L3BArray.find(function (value) {
                                    var formattedFromDate = formatDate(value.FROM_DATE);
                                    var formattedToDate = formatDate(value.TO_DATE);

                                    return value.Space_ID === selectedSpaceID &&
                                        formattedFromDate === selectedFormattedFromDate &&
                                        formattedToDate === selectedFormattedToDate;
                                });
                                console.log(valstatus);
                            });
                            if (ANYSPCBOOKArray.length > 0) {
                                showConfirmationModalDelayed();
                                vacantstatus = ANYSPCBOOKArray[0]
                            }
                            else {
                                vacantstatus = ANYSPCBOOKArray[0]
                                shownewuserpopup();
                            }
                        }

                       
                        console.log('ANYSPCBOOKArray:', ANYSPCBOOKArray);


                    }
                }


            });
        }

        function BookingScreen() {
            window.location.href = '../SMViews/Map/frmUserMapFloorList.aspx';
        }


        function proceedWithAction() {
            debugger;
            var obj = {
                SPC_ID: valstatus.Space_ID,
                SSAD_SRN_REQ_ID: valstatus.SSAD_SRN_REQ_ID,
                SSA_SRNREQ_ID: valstatus.SSA_SRNREQ_ID,
                AUR_ID: valstatus.AUR_ID,
                FROM_DATE: moment().format('MM/DD/YY'),
                TO_DATE: moment().format('MM/DD/YY'),
                FROM_TIME: valstatus.FROM_TIME,
                TO_TIME: valstatus.TO_TIME,
                /*SEAT_STATUS: valstatus.SEAT_STATUS,*/
                STATUS: valstatus.STATUS,
                VERTICAL: valstatus.VERTICAL,
                SH_CODE: valstatus.SH_CODE,
                Cost_Center_Code: valstatus.Cost_Center_Code,
                STACHECK: valstatus.STACHECK,
                ticked: true


            }

            $.ajax({

                url: '../api/MaploaderAPI/AllocateSeatsPopUp',
                type: 'POST',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result != null) {
                        $.notify(result.Data, { color: "#fff", background: "#20D67B", close: true });
                        $('#confirmationModal').modal('hide');
                    }
                    else {
                        $.notify('Something Went Wrong', { color: "#fff", background: "#20D67B", close: true });
                        $('#confirmationModal').modal('hide');
                    }
                },
                error: function (err) {
                    $.notify('Failed to book  seat !', { color: "#fff", background: "#D44950", close: true });
                }

            });
        }

        function bookVacantSeat() {
            var obj = {
                SPC_ID: vacantstatus.Space_ID,
                SSAD_SRN_REQ_ID: vacantstatus.SSAD_SRN_REQ_ID,
                SSA_SRNREQ_ID: vacantstatus.SSA_SRNREQ_ID,
                AUR_ID: '<% =Session("uid") %>',
                 FROM_DATE: moment().format('MM/DD/YY'),
                 TO_DATE: moment().format('MM/DD/YY'),
                 FROM_TIME: vacantstatus.FROM_TIME,
                 TO_TIME: vacantstatus.TO_TIME,
                 STATUS: vacantstatus.STATUS,
                 VERTICAL: vacantstatus.VERTICAL,
                 SH_CODE: vacantstatus.SH_CODE,
                 Cost_Center_Code: vacantstatus.Cost_Center_Code,
                 STACHECK: vacantstatus.STACHECK,
                 ticked: true


             }

            $.ajax({
                url: '../api/MaploaderAPI/AllocateSeatsPopUp',
                type: 'POST',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result != null) {
                        $.notify(result.Data, { color: "#fff", background: "#20D67B", close: true });
                        $('#confirmationModal').modal('hide');
                    }
                    else {
                        $.notify('Something Went Wrong', { color: "#fff", background: "#20D67B", close: true });
                        $('#confirmationModal').modal('hide');
                    }
                },
                error: function (err) {
                    $.notify('Failed to book  seat !', { color: "#fff", background: "#D44950", close: true });
                }

            });

        }


        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };
        $(function () {
            $('a[title]').tooltip();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href")
                if (target != "#profile")
                    $(target).load(target.substring(1) + "Dashboard.aspx");
                else
                    $("#profile").load("../WebFiles/frmMyProfile.aspx?dummy=" + Math.random());
            });
            if (getParameterByName("back") != "") {
                //activaTab("maintenance");
                var dbname = getParameterByName("back");
                activaTab(dbname);
            }
            else
                $("#profile").load("../WebFiles/frmMyProfile.aspx?dummy=" + Math.random());
        });
    </script>
    <script type="text/javascript">
        var GHT = '<%= Session("GHT")%>';
        $(document).ready(function () {

            $("#ghTitle").append(GHT);
        });

    </script>
    <%--C3 charts end here--%>
      

    <script src="../Scripts/jQuery.print.js"></script>
    <script src="../BlurScripts/BlurJs/DashboardPrint.js"></script>
</body>
</html>