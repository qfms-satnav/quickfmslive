﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<style>
    #spce .c3 path, .c3 line {
        stroke: #F0AE7E !important;
    }

    hr {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>--%>
<div id="spce">
    <form id="form1" runat="server">
        <div class="row">
            <h5 class="col-md-3">Business Card Dashboard</h5>
        </div>
        <hr />
        <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlBCCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        <i class="fa fa-pie-chart fa-fw"></i>Vendor Wise Request's Count
                    </div>
                    <div align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('VendorCount')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>

                    </div>
                    <div class="panel-body" data-backdrop="false">
                        <div id="VendorCount"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        <i class="fa fa-pie-chart fa-fw"></i>No.of Status Count
                    </div>
                    <div align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('StatusCount')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>

                    </div>
                    <div class="panel-body" data-backdrop="false">
                        <div id="StatusCount"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" defer>
    var year
    $(document).ready(function () {
        year = (new Date().getFullYear()).toString();

        $("#ddlBCCompany").change(function () {
            Cmpy = $(this).val();
            var CompanyId = Cmpy;
            funbindVendorCount(CompanyId);
            funbindstatuscount(CompanyId);
        });

        function funfillCompany(valcompany) {
            $.each(Companies, function (key, value) {
                $("#ddlBCCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });

        }
        var Cmpy = '<%= Session["COMPANYID"]%>';
        funfillCompany(Cmpy);
        funbindVendorCount(Cmpy);
        funbindstatuscount(Cmpy);

        function funbindVendorCount(CompanyId) {
            var param = { companyid: parseInt(CompanyId) };
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'bar',
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/BusinessCardAPI/GetVendorwiseCount',
                type: 'POST',
                data: param,
                success: function (result) {
                    chart.unload();
                    chart.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#VendorCount").empty();
                $("#VendorCount").append(chart.element);
                $("#VendorCount").load();
            }, 500);
           
        }

        function funbindstatuscount(CompanyId) {
            var param = { companyid: parseInt(CompanyId) };
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/BusinessCardAPI/GeStaCount',
                type: 'POST',
                data: param,
                success: function (result) {
                    chart.unload();
                    chart.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#StatusCount").empty();
                $("#StatusCount").append(chart.element);
                $("#StatusCount").load();
            }, 500);
        }

        function enableDisableCompanyDDL(compid) {
            if (compid == "1") {
                $("#ddlBCCompany").prop('disabled', false);
            }
            else {
                $("#ddlBCCompany").prop('disabled', true);
                $('#ddlBCCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }
        function Print(divName) {
            $('#' + divName).print({
                globalStyles: true,
                mediaPrint: false,
                stylesheet: "http://fonts.googleapis.com/css?family=Inconsolata",
                iframe: true,
                prepend: "<h3>QuickFMS</h3> <hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-bottom: 0.5em;''margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'>" + "<br/>",
                append: "<hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'> <h6 align='center'>© QuickFMS " + year + " </h6>"
            });

        }
        setTimeout(function () { enableDisableCompanyDDL(Cmpy) }, 1000);


    });
</script>
