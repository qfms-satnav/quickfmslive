<script type="text/javascript">
        $(function () {
            var chart, merge = Highcharts.merge;
            //<![CDATA[ 
            $(document).ready(function () {
                var perShapeGradient = {
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                };
                var colors = Highcharts.getOptions().colors;
                colors = [{
                    linearGradient: perShapeGradient,
                    stops: [
                        [0, '#F2A32D'],
                        [1, '#E2D15B']
                    ]
                }, {
                    linearGradient: merge(perShapeGradient),
                    stops: [
                        [0, '#1384CA'],
                        [1, '#54AA87']
                    ]
                }, {
                    linearGradient: merge(perShapeGradient),
                    stops: [
                        [0, '#3C3E3B'],
                        [1, '#8E8D93']
                    ]
                }]
                chart = new Highcharts.Chart({
                    chart: {

                        renderTo: 'container',
                        defaultSeriesType: 'bar',
						 marginTop: 100
                    },
                    title: {
                        text: '@@TOTWS',
						style: {
                                fontSize: '25px',
                                fontFamily: 'Malgun Gothic, sans-serif',
								fontWeight: 'bold'
                            },
							pointFormat: "{point.y:,.0f}"
                    },
                    subtitle: {
                        text: 'Workstations',
						style: {
                                fontSize: '16px',
                                fontFamily: 'Malgun Gothic, sans-serif',
								color:'#000000'
                            }
                    },

                    xAxis: {
                        type: 'category',
                        labels: {
                            rotation: 0,
                            style: {
                                fontSize: '12px',
                                fontFamily: 'Malgun Gothic, sans-serif'
                            },
							formatter: function () {
                                if ('India' === this.value || 'Philippines' === this.value) {
                                    return '<span style="fill:black; font-weight:700">' + this.value + '</span>';
                                } else {
                                    return this.value;
                                }
                            }

                        }

                    },
                    yAxis: {
                        min: 0,
                        title: {
                            enabled: false
                        },
                        lineWidth: 0,
                        minorGridLineWidth: 0,
                        lineColor: 'transparent',
                        labels: {
                            enabled: false
                        },
                        minorTickLength: 0,
                        gridLineColor: 'transparent',
                        tickLength: 0,
						gridLineWidth:0
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                         pointFormat:  ": {point.y:,.0f} Seat(s)"
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.1,
                            borderWidth: 0
                        }
                    },
                    series: [{

                        name: 'China',

                        data: @@dataWS,
                        color: 'white',


                        dataLabels: {
                            enabled: true,
                            rotation: 0,
                            color: colors[0],
                            align: 'right',
                            x: 0,
                            y: 0,
                            style: {
                                fontSize: '9px',
                                fontFamily: 'Malgun Gothic, sans-serif',
                                textShadow: '0 0 0px'
                            }
                        }
                    }]

                }, function (chart) {
							  

				var ws =  chart.series[0].data[0].graphic
    ws.attr('width',parseInt(ws.attr('width'))+10);
    ws.attr('x',parseInt(ws.attr('x'))-5);
                   
				   var ws1 =  chart.series[0].data[1].graphic
    ws1.attr('width',parseInt(ws1.attr('width'))+10);
    ws1.attr('x',parseInt(ws1.attr('x'))-5);

   //chart.renderer.text('<span>@@CNT</span>', chart.plotSizeY*0.52 , 85)
     //       .attr({
					
        
     //       })
     //       .css({
     //           color: '#777',
	//			fontFamily: 'Malgun Gothic, sans-serif',
      //          fontSize: '13px'
      //      })
      //TyGd1$6%k      .add();

			//chart.renderer.image('@@IMG', chart.plotSizeY*0.55, 73 , 15, 15)
            //.add();
			
                });


            });
            //]]> 
        });
    </script>
