﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO
Partial Class DashboardV2
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim mode As Integer
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If

        If Not Page.IsPostBack Then
            Try
                GetTenantModules()
            Catch ex As Exception

            Finally
            End Try
        End If
    End Sub
    Private Sub GetTenantModules()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Tenant_Module")
        sp.Command.AddParameter("@TID", Session("TENANT"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        Session("space") = 0
        Session("conference") = 0
        Session("businesscard") = 0
        Session("GuestHouse") = 0
        Session("Energy") = 0
        Session("maintenance") = 0
        Session("property") = 0
        Session("Assets") = 0
        Session("helpdesk") = 0
        Session("Branchchecklist") = 0

        If (Session("TENANT") = "AdityaBirla.dbo") Then
            Session("space") = 0
        End If
        If ds.Tables(0).Rows.Count > 0 Then

            For Each dr1 As DataRow In ds.Tables(0).Rows
                Select Case LCase(dr1("T_MODULE"))

                    Case "space"
                        Session("space") = 1

                    Case "property"

                        Session("property") = 1
                    Case "asset"

                        Session("Assets") = 1

                    Case "conference"

                        Session("conference") = 1

                    Case "maintenance"

                        Session("maintenance") = 1

                    Case "helpdesk"

                        Session("helpdesk") = 1

                    Case "businesscard"

                        Session("businesscard") = 1

                    Case "guesthouse"

                        Session("GuestHouse") = 1
                    Case "energy"
                        Session("Energy") = 1

                    Case "branchchecklist"
                        Session("Branchchecklist") = 1


                    Case Else

                End Select
            Next
        End If
    End Sub
End Class
