﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1180" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
		{ CompanyID: "Demo37.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiM2IwOGQzMWEtMzJlMy00YjA1LWI1YjEtY2JmNmJmMWVlMGM0IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection415fb3e7dc06c76b9540" },
        { CompanyID: "Demo9.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMDBhMjFhMTQtMjM5YS00NjlhLTgxZTYtYzViNWQxMzE2ODkzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "Demo36.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNWZlMzE4ZjktODcxYy00NTdjLTkzY2EtMDRlMjVkOTIzYzYzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection415fb3e7dc06c76b9540" },
        { CompanyID: "Tavant.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYmY5ZDYzOGMtYzU0OS00NTdiLWJhMzEtYTk1YjY0OTEyMzZjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "JohnDeere.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiODU3ZDlmODgtNDI3MS00ZTNlLWEwNzItNGU3NmJlYzY0NDBiIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "LTTS.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMGFiNTlmYjYtODU5YS00MTUxLTg5ZWItYjhhOGUyMmU0N2RiIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMDBhMjFhMTQtMjM5YS00NjlhLTgxZTYtYzViNWQxMzE2ODkzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMDBhMjFhMTQtMjM5YS00NjlhLTgxZTYtYzViNWQxMzE2ODkzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "BNP.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZmFmNjdkYzUtODk0Ni00Y2MyLWEwNTUtMTE0MzhhYzFkZjUzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "[TATA_CAP].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiY2RkODQzNjQtNmFlMi00NDVkLThmYzgtMzE2Mjg5MzBkYjUzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "MAXLIFE.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZGIzOWJhZDktOWVkNi00YjQ3LTg1MTUtMDQ1OTU0MjRkNzc5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "Apexon.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNzZmM2QxODgtM2RkYi00YTVjLTkxMmUtM2RhNDgzOTkzYTdmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "AXA.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiM2M2MTI0ZGEtYzA0YS00NTZmLThhZDQtMDhiNDYwMDZmOTY4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "HONDA.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOTEzYTcwMmQtYmMyZC00ZDgxLTkyMDgtOGZlOGE1OTA3NzdjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[LIBERTY].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYzE2NmZjMzktN2NjMC00MDVkLWFjOTctYTk5YTdjNGQxZDYyIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "PernodRicard.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMzViMDJmMjMtNGY5Zi00NjRkLWJlNzQtMTg2MDYwNzkzYjYxIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "PropTiger.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMWJmODEwY2UtOTk3Ny00ZmY2LTg2NzMtYzI4NWJlMTRkYWQ2IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[RBL_UAT].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMjRhOTQ0YzMtNTZjMi00MWFmLTk4YjItMWE4ZDc3Zjk4ZjkyIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[EMIDS].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMGQyMjQ5ZTUtMjY2NS00M2MzLThlOWMtZjA4NTA5NGE0OTg3IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "Impelsys.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZThiOGE3NTMtZWE5Yy00MTU4LTgzZjYtNmQzMWY5NDhjMDcxIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "Kalpataru.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZmY4YmVlNjktNzI5Ny00MDg4LTk0NTAtMjFhZjIxMGIxMmE1IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "KLI.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMTk1M2Y0ZDUtYTVlNS00NjdkLTk4N2ItMzg0NDU2MzhjYTQzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "Ujjivan.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMTEzYmVhZGYtOWYxZS00ODYwLWE4M2YtMTYxMDI4YWE4MTJiIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "Samsung.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMTEyMGY2OTQtNzk1Ny00YzQ5LTgzYjUtZGI2YmI1OWZmNzk5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
	    { CompanyID: "[Tavant].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYmY5ZDYzOGMtYzU0OS00NTdiLWJhMzEtYTk1YjY0OTEyMzZjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "AGS_Health.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNzQ0ZmNmZjgtMDdjZS00MTNhLTg2ZTUtOTYyYjAyOWE3NGM4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
		 {CompanyID: "Tata_Tech.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZDI2Nzk5NGQtMzM0My00NzBiLTk2ZjEtYjA1NGQwNjhiNGViIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },

];

    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>




