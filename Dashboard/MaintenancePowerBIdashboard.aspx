﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1100" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
          { CompanyID: "Aadhar.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiM2E5MzMyYjItODFlYi00ZDY3LTg5NDItNDJlMGMxNDVjNTY1IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "SKAB.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMmZjODI0MzAtYTBjZS00NmE0LThlMmQtNTU3NWYzZGEzMTI5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[AEQUS].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMjU3YzE1MjktOGFjNi00MDA2LTkwZGQtMWExMmVkY2UyMmM3IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[ABM_INDIA].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNzVjY2Q1NGEtNDNmMi00ZjFkLWFjN2EtYjFjOTI0YmM1Mzk1IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMmQyYmRkNzMtOWU0Ni00NWFhLTlkOWItZWFjNGM3N2M5NjYwIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMmQyYmRkNzMtOWU0Ni00NWFhLTlkOWItZWFjNGM3N2M5NjYwIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
{ CompanyID: "Demo27.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOWVmNThhZjItNjI1Yi00MjI5LWIyNDQtZjNlYjBjZGFkODcxIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
{ CompanyID: "Marks_Spencer.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOTVkYTg2MzItYWNjOC00MjRkLTk4NzQtYjE0YWQ3MzcxZmVjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
];


    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>

