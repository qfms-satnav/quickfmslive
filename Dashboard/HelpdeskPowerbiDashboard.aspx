﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1100" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
		{ CompanyID: "Demo40.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMjQxZDlhODctOWExNS00MzIxLTkxODUtYTMzNGI3ODRlYWI1IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },
        { CompanyID: "Aadhar.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiM2YxMDBhMTctYTgyNi00MGE5LTgwNGItZmVhMGRlOTc0ZWU5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[TATA_CAP].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYTUxZWFhYzctNGIzMS00OTA5LWI5NzUtNjFkZTViYzkyNzE1IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[AEQUS].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOGM0ZmYzNzMtMDBiYy00ZGJjLWEzY2QtM2E2MmNkNmEzMDRmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },
        { CompanyID: "BhartiAXA.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNzIwNGIxNjYtZmYwZi00NTljLThlOWYtNzU5NDk2MDg4ZTNlIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },
        { CompanyID: "SKAB.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNWFjNDk5NjEtMmYyMC00OTg4LTk2MjMtYjk5NTcxMWU4YTEzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZWY0MWUxMjMtNmM2ZC00OTA1LTk1YzMtN2M3MmQ5OTQ5NDc2IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZWY0MWUxMjMtNmM2ZC00OTA1LTk1YzMtN2M3MmQ5OTQ5NDc2IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },
		{ CompanyID: "BITS.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZjI3ZWUyYmItOTQwMC00NmY2LWE1ZmQtNzYwNDYwYmZjMzc5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },
		{ CompanyID: "[Oliva].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYmNmYzdjMTEtNzIxMS00MjViLWFmMDktOTk3NzVlM2MwYzdmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
{ CompanyID: "Demo27.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYjk4OWQxNjctNGNmYi00ODljLTkyOTUtZDk0ZTE0MTVmMGFjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
{ CompanyID: "Marks_Spencer.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNGZjYWE0NDktYmM2OS00ZDFiLWI3NjQtMWMyOTExOTZmY2NjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },
		];


    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>

