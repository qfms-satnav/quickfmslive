﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1100" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
         { CompanyID: "Aadhar.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNTA3NDMxYWEtNTEwNS00NDdlLTk5NTAtNGJhYjU2ZTYwNzk3IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "[AEQUS].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMjUxOTdiMjktOTM2Ny00ODY5LThlZDAtODg1Nzc2MDkxOTBhIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "[ABM_INDIA].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiODRhYzZhZDItZTFmZC00NGU3LThiNjEtNzQzZTNiMzgwMTMzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOWUxMzMzZjMtMmEyMy00OTNmLThhNzEtODMwNGI5ZDQ3YzQzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOWUxMzMzZjMtMmEyMy00OTNmLThhNzEtODMwNGI5ZDQ3YzQzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection" },
{ CompanyID: "Demo27.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOWVmNThhZjItNjI1Yi00MjI5LWIyNDQtZjNlYjBjZGFkODcxIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
];
         

    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>
