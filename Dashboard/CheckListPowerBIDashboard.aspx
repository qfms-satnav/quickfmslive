﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1180" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
		{ CompanyID: "Demo41.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNGZkMmI2YzItNDk4ZC00M2FmLWE2ZGEtMzUyOWEzNmE5MTVmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
		{ CompanyID: "Demo37.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMzA4OTkxMDgtYTU5YS00NGYzLWJhZjItYTIyZGExMzk3ZmZmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
       { CompanyID: "[TATA_CAP].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNjY0MWNiYzYtMDA2OC00YTA4LWFkNmQtZGNiZDdiYjI1ZTE5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
        { CompanyID: "Aadhar.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOWQ2NjU4YTktZDIxYy00MzVhLWFmYjItNDQyM2RkNDliMjU5IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
        { CompanyID: "[MAXLIFE].dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiMjc1YzFjY2YtMjM4OC00M2RmLWFmNWYtMGMxOTcxZDI2YTQ1IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYjgyN2FkYmYtZWFkZi00NTc4LWI5MGUtZDZmZDA2M2VjMzExIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYjgyN2FkYmYtZWFkZi00NTc4LWI5MGUtZDZmZDA2M2VjMzExIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
        { CompanyID: "BhartiAXA.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiOGMzYjk2MjUtN2VkNC00N2VmLWFkMGEtNmJlNmI1YjY1NzZmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection58b72ea7e15416e1fc9c" },
{ CompanyID: "Demo27.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNTlkMDZlZjUtOTcwZi00OTIxLWIxNmYtOGU4MDIzYmU5Y2UwIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
];

    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>

