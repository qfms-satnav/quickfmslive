﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1100" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [

        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiODY3MGY4YjItYjQ2YS00OTAwLWFlOGYtOTYxYTU5NmE3NmNjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectionhttps://app.powerbi.com/view?r=eyJrIjoiNWJhNDk1MjEtOGJhOS00Y2YyLTk4M2MtZTBiN2ZhZWUwNjA4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "Product_UAT.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiODY3MGY4YjItYjQ2YS00OTAwLWFlOGYtOTYxYTU5NmE3NmNjIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectionhttps://app.powerbi.com/view?r=eyJrIjoiNWJhNDk1MjEtOGJhOS00Y2YyLTk4M2MtZTBiN2ZhZWUwNjA4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
{ CompanyID: "Demo27.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNDg5NDE0ZWUtZjRkYS00MGZiLWFhMDMtYWVhMTZmZjQ4NGRmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
];

    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>

