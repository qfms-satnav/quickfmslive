﻿Imports System.Data
Imports System.Web.Script.Serialization


Partial Class Dashboard_C3_Default
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        BindProperties()
    End Sub

    Private Sub BindProperties()
        Dim dt As DataTable = getpropertytype()
        Dim conc As String = ""
        Dim totl As String = ""
        Dim ppty As String = ""

        If dt.Rows.Count > 0 Then

            For Each dr1 As DataRow In dt.Rows
                conc = conc + "['" + (dr1("PN_PROPERTYTYPE")) & "'," & CStr(dr1("TOTAL")) + "],"
            Next
            Dim jss As New JavaScriptSerializer()

            chartData = conc
        End If
    End Sub

    Private Function getpropertytype() As DataTable
        Dim ds As DataTable = New DataTable()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTIES_DASHBOARD")
            sp.Command.AddParameter("@ADMIN_ID", Session("Uid").ToString(), DbType.String)
            ds = sp.GetDataSet().Tables(0)

        Catch ex As Exception

        End Try
        Return ds
    End Function
    Public Property chartData() As String
        Get
            Return m_chartData
        End Get
        Set(value As String)
            m_chartData = value
        End Set
    End Property
    Private m_chartData As String
End Class
