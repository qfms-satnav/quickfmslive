﻿<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
        <iframe id="MyDashboardBI" title="AvailabilitySummary" width="1180" height="600" frameborder="0" allowfullscreen="true"></iframe>

<script>  
    var companyid = '<%= Session("TENANT")%>';
    var list = [
		{ CompanyID: "Demo40.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNDExMTEzZDAtYzAxYi00MDc3LTllODgtZDRhMWVlYjBlN2I4IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "Apexon.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiYzdmZGYyZGYtMWYxYS00NTMyLTg5OGQtMTk0MjBhMjk2ZmFmIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "AXA.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZDUyMTg1ODktZjVjOS00MzkwLThlNjgtMzkxZGRjMTg5MGZiIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "TATA_CAP.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiZDljNzAwZmItMjk4ZS00Y2RhLWI4ODItOWI2YzdlZDFmOGFlIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" },
        { CompanyID: "QuickFMS_Demo.dbo", src: "https://app.powerbi.com/view?r=eyJrIjoiNWUxYmYwZGItZDcxZS00NjljLTk2MzMtZDYzYzg2OTIzYzJhIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSection1266d93ba90a01ecd154" },];
];

    list.forEach(function (element) {
        if (element.CompanyID.toLowerCase() == companyid.toLowerCase()) {
            document.getElementById("MyDashboardBI").src = element.src;
        }
    });

</script>
</body>
</html>


