﻿<style>
    hr {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>
<div>
    <form id="form1" runat="server">
        <div class="row">
            <h5 class="col-md-9">Maintenance Management</h5>
        </div>
        <hr />
        <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlAMCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                </div>
            </div>

            <div class="col-md-9 col-sm-12 col-xs-12" align="right">
                <div class="form-group">
                    <button type="button" class="btn btn-default btn-sm" onclick="Print('Maintenance_Dashboard')" style="color: rgba(58, 156, 193, 0.87)">
                        <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: green"></span><span style="color: rgba(58, 156, 193, 0.87)"></span>
                    </button>
                </div>
            </div>
        </div>
        <div id="Maintenance_Dashboard">
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Category-Wise Assets Count
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('maintenancecontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body" data-backdrop="false">
                            <div id="maintenancecontainer"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" id="AMC_Amount_Print">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" id="ancWorkReqTotalMain" class="alert alert-info" style="width: 230px;">
                                <i class="fa fa-tasks pull-left" aria-hidden="true"></i>Total Work Orders  
                            <span class="badge pull-right" id="lblwrtotcountMain"></span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="alert alert-success" id="ancWorkReqCompletedMain" style="width: 230px;">
                                <i class="fa fa-check pull-left" aria-hidden="true"></i>Completed Work Orders
                           
                                <span class="badge pull-right" id="lblWRclosedMain"></span>

                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="alert alert-warning" id="ancWorkReqPendingMain" style="width: 230px;">
                                <i class="fa fa-exclamation-triangle pull-left" aria-hidden="true"></i>Pending Work Orders
                            <span class="badge pull-right" id="lblpendingMain"></span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="alert alert-danger" id="ancamcvd" style="width: 230px;">
                                <i class="fa fa-calendar pull-left" aria-hidden="true"></i>AMC Expenditures This Month
                            <span class="badge pull-right" id="lblamcamount"></span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="alert" id="ancAssetsexp" style="width: 230px; background-color: silver">
                                <i class="fa fa-calendar pull-left" aria-hidden="true"></i>AMCs Expiring This Month
                            <span class="badge pull-right" id="lblAmcExpThisMonth"></span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="alert alert-info" id="ancAssetsUnderAMC" style="width: 230px;">
                                <i class="fa fa-cog pull-left" aria-hidden="true"></i>Assets Under AMC
                            <span class="badge pull-right" id="lblAssetsUnderAMC"></span>
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="alert alert-success" id="ancAssetsUnderPrvntMaint" style="width: 230px;">
                                <i class="fa fa-cog pull-left" aria-hidden="true"></i>Assets Under Preventive Maintenance
                            <span class="badge pull-right" id="lblAstUnderPrvntMaint"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Location-wise AMC Asset Categories
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('amccatdiv')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body" data-backdrop="false">
                            <div id="amccatdiv"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Location-wise NONAMC Asset Categories
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('nonamccatdiv')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body" data-backdrop="false">
                            <div id="nonamccatdiv"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade bs-example-modal-lg col-md-12 " id="totalamcs" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="panel-group box box-primary" id="Div7">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <div class="panel-heading ">
                                    <h4 class="modal-title" id="H2">AMC Amount Received this Month</h4>
                                </div>
                                <div class="clearfix">
                                    <button type="button" class="btn btn-default btn-sm" onclick="Print('tblamcamounts')" style="color: rgba(58, 156, 193, 0.87)">
                                        <i class="fa fa-print fa-1x" aria-hidden="true"></i>Print
                                    </button>
                                    <table id="tblamcamounts" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>
                                            <th>Advice No</th>
                                            <th>Work Order No</th>
                                            <th>Vendor</th>
                                            <th>Mode Of Payment</th>
                                            <th>Amount</th>
                                            <th>Updated Date</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade bs-example-modal-lg col-md-12 " id="divamcexpiring" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="panel-group box box-primary" id="Div4">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <div class="panel-heading ">
                                    <h4 class="panel-title modal-header-primary" style="padding-left: 17px" id="H1">AMC Assets Expiring this Month</h4>
                                </div>
                                <div class="modal-body">
                                    <button type="button" class="btn btn-default btn-sm" onclick="Print('tblamcexpThisMonth')" style="color: rgba(58, 156, 193, 0.87)">
                                        <i class="fa fa-print fa-1x" aria-hidden="true"></i>Print
                                    </button>
                                    <table id="tblamcexpThisMonth" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>
                                            <th>Plan ID</th>
                                            <th>Asset</th>
                                            <th>Vendor</th>
                                            <th>Location</th>
                                            <th>From Date</th>
                                            <th>To Date</th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade bs-example-modal-lg col-md-12 " id="Div1" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="panel-group box box-primary" id="Div3">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="panel-heading ">
                            <h4 class="panel-title modal-header-primary" style="padding-left: 17px" id="H3"><span id="selectedType"></span> Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md- pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblAssetsByCat')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-1x" aria-hidden="true"></i>Print
                                </button>
                            </div>
                            <table id="tblAssetsByCat" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Asset Code</th>
                                    <th>Asset Name</th>
                                    <th>Date</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg col-md-12 " id="Div2" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="panel-group box box-primary" id="Div5">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="panel-heading ">
                            <h4 class="panel-title modal-header-primary" style="padding-left: 17px" id="div2heading"></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-1 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblworkorders')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-1x" aria-hidden="true"></i>Print
                                </button>
                            </div>
                            <table id="tblworkorders" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Work Order Req.No.</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg col-md-12 " id="divAssetsUnderPrvntMaint" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="panel-group box box-primary" id="Div6">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="panel-heading ">
                            <h4 class="panel-title modal-header-primary" style="padding-left: 17px" id="H5">Assets Under Preventive Maintenance</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-1 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblAssetsPrvntMaint')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-1x" aria-hidden="true"></i>Print
                                </button>
                            </div>
                            <table id="tblAssetsPrvntMaint" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>PVM Plan ID</th>
                                    <th>Asset Code</th>
                                    <th>Asset Name</th>

                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" defer>
    var CompanySessionId = '<%= Session("COMPANYID")%>'
    var year
    $(document).ready(function () {
        year = (new Date().getFullYear()).toString();

    });

    $(document).ready(function () {

        $("#ddlAMCompany").change(function () {
            CompanySessionId = $(this).val();
            var CompanySessionId = CompanySessionId;

            GetWRCount("1", CompanySessionId);
            funAMCAssets(CompanySessionId);
            funAMCamountsrec(CompanySessionId);

            fillAmvNoncatgs(CompanySessionId);
            funfillAMCTypes(CompanySessionId);
            GetAssets_Under_Prvnt_Maint_Count("1", CompanySessionId);

        });
        GetWRCount("1", CompanySessionId);
        //Pie


        //bar chart
        //funfillAMCNonAMCAssets();


        //Assets Exp this month
        funAMCAssets(CompanySessionId);

        //AMC Amounts received
        funAMCamountsrec(CompanySessionId);



        GetAssets_Under_Prvnt_Maint_Count("1", CompanySessionId);

        //work orders
        $("#ancWorkReqTotalMain").click(function (e) {
            e.preventDefault();
            $("#Div2").modal("show");
            $('#div2heading').html("Total Work Orders");
            GetWRDetails("1", $("#ddlAMCompany option:selected").val());
        });

        //Total Work Requests count and View Details
        $("#ancWorkReqCompletedMain").click(function (e) {
            $("#Div2").modal("show");
            $('#div2heading').html("Completed Work Orders");
            e.preventDefault();

            GetWRDetails("2", $("#ddlAMCompany option:selected").val());

        });

        //Pending Work Requests count and View Details
        $("#ancWorkReqPendingMain").click(function (e) {
            $("#Div2").modal("show");
            $('#div2heading').html("Pending Work Orders");
            e.preventDefault();
            GetWRDetails("3", $("#ddlAMCompany option:selected").val());
        });

        //amc amounts rec this month      
        $("#ancamcvd").click(function (e) {
            $("#totalamcs").modal("show");
            e.preventDefault();
            funAMCamountsrec($("#ddlAMCompany option:selected").val());
        });

        //AMC Assets Under AMC     
        $("#ancAssetsUnderAMC").click(function (e) {
            $("#Div1").modal("show");
            e.preventDefault();
            $('#selectedType').text('AMC');
            GetAssetsByCategory('AMC', $("#ddlAMCompany option:selected").val());
        });

        //AMC Assets Under Preventive Maintenance   
        $("#ancAssetsUnderPrvntMaint").click(function (e) {
            $("#divAssetsUnderPrvntMaint").modal("show");
            e.preventDefault();
            GetAssets_Under_Prvnt_Maint_Count("2", $("#ddlAMCompany option:selected").val());
        });

        //AMC Assets Exp Current Month        
        $("#ancAssetsexp").click(function (e) {
            //alert("S");
            e.preventDefault();
            $("#divamcexpiring").modal("show");
            funAMCAssets($("#ddlAMCompany option:selected").val());
        });


        //st
        function fillAmvNoncatgs(CompanySessionId) {
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" } },
                    onclick: function (e) {
                        $("#Div1").modal("show");
                        GetAssetsByCategoryLocationAndAmcorNonAMC(e.id, 'AMC', CompanySessionId);
                    }
                },
                //color: {
                //    //BLUE ORAN GREEN RED
                //    pattern: ['#ff7f0e']
                //},
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            //CHART 2
            var NONAMCchart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" } },

                    onclick: function (e) {
                        $("#Div1").modal("show");
                        GetAssetsByCategoryLocationAndAmcorNonAMC(e.id, 'Non-AMC', CompanySessionId);
                    }
                },
                //color: {
                //    //BLUE ORAN GREEN RED //pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']                    
                //    pattern: ['#2ca02c']
                //},
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });
            var param = { companyId: parseInt(CompanySessionId) };

            $.ajax({
                url: '../api/MaintenanceDBAPI/BindAMCNonAMCBarCh',
                data: param,
                type: 'POST',
                success: function (result) {
                    chart.unload();
                    NONAMCchart.unload();
                    chart.load({ columns: result[0] });
                    NONAMCchart.load({ columns: result[1] });
                }
            });

            $("#amccatdiv").html("");
            $("#nonamccatdiv").html("");
            $("#amccatdiv").append(chart.element);
            $("#nonamccatdiv").append(NONAMCchart.element);

        }

        //end

        function funfillAMCTypes(CompanySessionId) {

            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" } },
                    onclick: function (e) {
                        $("#Div1").modal("show");
                        $('#selectedType').text(e.id);
                        GetAssetsByCategory(e.id, CompanySessionId);
                    }
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });
            var param = { companyId: parseInt(CompanySessionId) };

            $.ajax({
                url: '../api/MaintenanceDBAPI/BindAMCTypes',

                data: param,
                type: 'POST',
                empty: { label: { text: "No Data Available" } },
                success: function (result) {
                    $("#lblAssetsUnderAMC").html(result[1][1]);
                    chart.unload();
                    chart.load({ columns: result });
                },

            });
            $("#maintenancecontainer").html("");
            $("#maintenancecontainer").append(chart.element);
            $("#maintenancecontainer").load();
        }





        function funfillAMCNonAMCAssets() {
            var chart = c3.generate({
                data: {
                    x: 'x',
                    columns: [
                    //['x', 'www.site1.com', 'www.site2.com', 'www.site3.com'],
                    //['AMC', 30, 200, 100],
                    //['NONAMC', 90, 100, 140],
                    ],

                    groups: [['AMC', 'Non-AMC']],
                    type: 'bar',
                },
                axis: {
                    x: {
                        type: 'category',
                        position: 'outer-center',
                    },
                    y: {
                        show: true,
                        label: {
                            text: 'Count',
                            position: 'outer-middle'
                        }
                    },
                }
            });

            $.ajax({
                url: '../api/MaintenanceDBAPI/BindAMCNonAMCBarCh',
                type: 'GET',
                success: function (result) {
                    chart.unload();
                    NONAMCchart.unload();
                    chart.load({ columns: result[0] });
                    NONAMCchart.load({ columns: result[1] });
                }
            });

            $("#amccatdiv").append(chart.element);
            $("#nonamccatdiv").append(NONAMCchart.element);
        }
        function funAMCamountsrec(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/MaintenanceDBAPI/GetAMCAmounts',
                data: param,
                type: "POST",
                
                success: function (data) {
                    var table = $('#tblamcamounts');
                    $('#tblamcamounts td').remove();
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].APM_MPAPAYADVICE_NO + "</td>" +
                                "<td>" + data[i].APA_MPWWRKORD_ID + "</td>" +
                                "<td>" + data[i].CONTRACTOR + "</td>" +
                                "<td>" + data[i].APM_PAY_MODE + "</td>" +
                                "<td>" + data[i].APM_NET_PAYABLE + "</td>" +
                                "<td>" + data[i].ACD_UPDATED_DT + "</td>" +
                                "</tr>"); 

                        }
                        var total = 0;
                        $('#tblamcamounts tr').each(function () {
                            //var value = parseInt($('td', this).eq(4).text());
                            var value = data.length;
                            if (!isNaN(value)) {
                                total += value;
                            }
                            $("#lblamcamount").html(total);
                            total = 0;
                        });
                    }
                    else {
                        $("#lblamcamount").html("0");
                        table.append("<tr>" + "<td colspan='6' align='center'> No AMC Amounts Received Current Month</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }

        function funAMCAssets(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/MaintenanceDBAPI/GetAMCAssetsExp',
                data: param,
                type: "GET",
                success: function (data) {
                    var table = $('#tblamcexpThisMonth');
                    $('#tblamcexpThisMonth td').remove();
                    $("#lblAmcExpThisMonth").html(data.length);

                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].AMN_PLAN_ID + "</td>" +
                                "<td>" + data[i].ASSETNAME + "</td>" +
                                "<td>" + data[i].AVR_NAME + "</td>" +
                                "<td>" + data[i].BDG_NAME + "</td>" +
                                "<td>" + data[i].FROMDATE + "</td>" +
                                "<td>" + data[i].TODATE + "</td>" +
                                "</tr>");
                        }
                    }
                    else {
                        table.append("<tr>" + "<td colspan='6' align='center'> No AMC Assets Expiring Current Month</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }
        setTimeout(function () {
            funfillAMCTypes(CompanySessionId);
        }, 500);
        setTimeout(function () {
            fillAmvNoncatgs(CompanySessionId);
        }, 700);
    });
    function Print(divName) {
        $('#' + divName).print({
            globalStyles: true,
            mediaPrint: false,
            stylesheet: "http://fonts.googleapis.com/css?family=Inconsolata",
            iframe: true,
            prepend: "<h3>QuickFMS</h3> <hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-bottom: 0.5em;''margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'>" + "<br/>",
            append: "<hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'> <h6 align='center'>© QuickFMS " + year + " </h6>"
        });

    }

    function GetWRCount(type, CompanySessionId) {
        var param = { companyId: parseInt(CompanySessionId) };

        $.ajax({
            url: "../api/MaintenanceDBAPI/GetWorkRequests",
            data: param,
            type: "POST",
            success: function (data) {

                //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                if (type == "1") {
                    $("#lblwrtotcountMain").html(data.Table[0].NOOFWO);
                    $("#lblWRclosedMain").html(data.Table1[0].NOOF_CLOSED_WO);
                    $("#lblpendingMain").html(data.Table2[0].NOOF_PENDING_WO);
                }
                //Total WR
                if (type == "2") {
                    var table = $('#tblworkorders');
                    $('#tblworkorders td').remove();
                    for (var i = 0; i < data.Table1.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table1[i].AWO_MAMPLAN_ID + "</td>" +
                            "<td>" + data.Table1[i].AWO_DATE + "</td>" +
                            "<td>" + data.Table1[i].AWO_AMC_COST + "</td>" +
                            "</tr>");
                    }
                    if (data.Table.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Work Requests Raised this Month</td>" + "</tr>");
                    }
                }

            },
            error: function (result) {
            }
        });
    }

    function GetWRDetails(type, CompanySessionId) {
        var param = { companyId: parseInt(CompanySessionId) };

        $.ajax({
            url: "../api/MaintenanceDBAPI/GetWorkOrdersDetails",
            data: param,
            type: "POST",
            success: function (data) {
                if (type == "1") {
                    var table = $('#tblworkorders');
                    $('#tblworkorders td').remove();
                    for (var i = 0; i < data.Table.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table[i].AWO_MAMPLAN_ID + "</td>" +
                            "<td>" + data.Table[i].AWO_DATE + "</td>" +
                            "<td>" + data.Table[i].AWO_AMC_COST + "</td>" +
                            "</tr>");
                    }
                    if (data.Table.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Records Found</td>" + "</tr>");
                    }
                }
                else if (type == "2") {
                    var table = $('#tblworkorders');
                    $('#tblworkorders td').remove();
                    for (var i = 0; i < data.Table1.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table1[i].AWO_MAMPLAN_ID + "</td>" +
                            "<td>" + data.Table1[i].AWO_DATE + "</td>" +
                            "<td>" + data.Table1[i].AWO_AMC_COST + "</td>" +
                            "</tr>");
                    }
                    if (data.Table1.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Records Found</td>" + "</tr>");
                    }
                }
                else if (type == "3") {
                    var table = $('#tblworkorders');
                    $('#tblworkorders td').remove();
                    for (var i = 0; i < data.Table2.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table2[i].AWO_MAMPLAN_ID + "</td>" +
                            "<td>" + data.Table2[i].AWO_DATE + "</td>" +
                            "<td>" + data.Table2[i].AWO_AMC_COST + "</td>" +
                            "</tr>");
                    }
                    if (data.Table2.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Records Found</td>" + "</tr>");
                    }
                }

            },
            error: function (result) {
            }
        });
    }

    function GetAssets_Under_Prvnt_Maint_Count(type, CompanySessionId) {
        var param = { companyId: parseInt(CompanySessionId) };
        $.ajax({
            url: "../api/MaintenanceDBAPI/GetAssetsUnderAMC_and_PrvntMain",
            data: param,
            type: "POST",
            success: function (data) {

                if (type == "1") {
                    $("#lblAstUnderPrvntMaint").html(data.Table[0].UNDER_PRVNT_MAINT);
                }
                //Total WR
                if (type == "2") {
                    var table = $('#tblAssetsPrvntMaint');
                    $('#tblAssetsPrvntMaint td').remove();
                    for (var i = 0; i < data.Table1.length; i++) {
                        table.append("<tr>" +
                        "<td>" + data.Table1[i].PVM_PLAN_ID + "</td>" +
                              "<td>" + data.Table1[i].AAT_CODE + "</td>" +
                        "<td>" + data.Table1[i].AAT_NAME + "</td>" +

                            "</tr>");
                    }
                    if (data.Table1.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Records Found</td>" + "</tr>");
                    }
                }

            },
            error: function (result) {
            }
        });
    }
    // popup -- category wise assets  count
    function GetAssetsByCategory(type, CompanySessionId) {

        $.ajax({
            url: "../api/MaintenanceDBAPI/GetAssetsByCategory",
            data: { "category": type, "company": CompanySessionId },
            contentType: "application/json; charset=utf-8",
            type: "GET",
            dataType: 'json',
            success: function (data) {

                var table = $('#tblAssetsByCat');
                $('#tblAssetsByCat td').remove();
                for (var i = 0; i < data.length; i++) {
                    table.append("<tr>" +
                         "<td>" + data[i].AAT_CODE + "</td>" +
                        "<td>" + data[i].AAT_NAME + "</td>" +
                        "<td>" + data[i].END_DATE + "</td>" +
                        "</tr>");
                }
                if (data.length == 0) {
                    table.append("<tr>" + "<td colspan='3' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                }

            },
            error: function (result) {
            }
        });
    }
    //LOC wise amc categories
    function GetAssetsAMC_COUNT(type) {

        $.ajax({
            url: "../api/MaintenanceDBAPI/GetAssetsAMC_COUNT",
            data: { "category": type },
            contentType: "application/json; charset=utf-8",
            type: "GET",
            dataType: 'json',
            success: function (data) {

                var table = $('#tblAssetsByCat');
                $('#tblAssetsByCat td').remove();
                for (var i = 0; i < data.length; i++) {
                    table.append("<tr>" +
                         "<td>" + data[i].AAT_CODE + "</td>" +
                        "<td>" + data[i].AAT_NAME + "</td>" +
                        "<td>" + data[i].END_DATE + "</td>" +
                        "</tr>");
                }
                if (data.length == 0) {
                    table.append("<tr>" + "<td colspan='3' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                }

            },
            error: function (result) {
            }
        });
    }
    //LOC wise Non_Amc categories
    function GetAssetsNON_AMC_COUNT(type) {

        $.ajax({
            url: "../api/MaintenanceDBAPI/GetAssetsNON_AMC_COUNT",
            data: { "category": type },
            contentType: "application/json; charset=utf-8",
            type: "GET",
            dataType: 'json',
            success: function (data) {

                var table = $('#tblAssetsByCat');
                $('#tblAssetsByCat td').remove();
                for (var i = 0; i < data.length; i++) {
                    table.append("<tr>" +
                         "<td>" + data[i].AAT_CODE + "</td>" +
                        "<td>" + data[i].AAT_NAME + "</td>" +
                        "<td>" + data[i].END_DATE + "</td>" +
                        "</tr>");
                }
                if (data.length == 0) {
                    table.append("<tr>" + "<td colspan='3' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                }

            },
            error: function (result) {
            }
        });
    }
    function GetAssetsByCategoryLocationAndAmcorNonAMC(location, type, CompanySessionId) {
        p = {
            location: location,
            category: type,
            company: CompanySessionId
        }

        $.ajax({
            url: "../api/MaintenanceDBAPI/GetAssetsByCategoryLocationAndAmcorNonAMC",
            data: p,
            type: "POST",
            dataType: 'json',
            success: function (data) {

                var table = $('#tblAssetsByCat');
                $('#tblAssetsByCat td').remove();
                for (var i = 0; i < data.length; i++) {
                    table.append("<tr>" +
                         "<td>" + data[i].AAT_CODE + "</td>" +
                        "<td>" + data[i].AAT_NAME + "</td>" +
                        "<td>" + data[i].AMN_TO_DATE + "</td>" +
                        "</tr>");
                }
                if (data.length == 0) {
                    table.append("<tr>" + "<td colspan='3' align='center'> No Records Found</td>" + "</tr>");
                }

            },
            error: function (result) {
            }
        });
    }


    function funfillCompany(valcompany) {

        $.each(Companies, function (key, value) {
            $("#ddlAMCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));

        });

    }

    var companyid = '<%= Session("COMPANYID")%>'
    funfillCompany(companyid);
    function enableDisableCompanyDDL(compid) {
        if (compid == "1") {
            $("#ddlAMCompany").prop('disabled', false);
        }
        else {
            $("#ddlAMCompany").prop('disabled', true);
            $('#ddlAMCompany option[value="' + compid + '"]').attr("selected", "selected");
        }
    }


    setTimeout(function () { enableDisableCompanyDDL(companyid) }, 1000);

</script>
