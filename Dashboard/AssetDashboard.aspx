﻿<style>
    hr {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>
<div>
    <form id="form1" runat="server">
        <div style="align-content:center" id="Aadhar">
<iframe width="1024" height="612" src="https://app.powerbi.com/view?r=eyJrIjoiNzg2YTlhNWItNDM0Ni00ZjIxLThiNWMtY2U0MzNiZjEwMmE0IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9" frameborder="0" allowFullScreen="true"></iframe>
</div>
        <div class="row">
            <h5 class="col-md-9">Asset Management</h5>
        </div>
        <hr />

        <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlAstCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                </div>
            </div>

            <div class="col-md-9 col-sm-12 col-xs-12" align="right">
                <div class="form-group">
                    <button type="button" class="btn btn-default btn-sm" onclick="Print('Asset_Dashboard')" style="color: rgba(58, 156, 193, 0.87)">
                        <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: green"></span><span style="color: rgba(58, 156, 193, 0.87)"></span>
                    </button>
                </div>
            </div>
        </div>

        <div id="Asset_Dashboard">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Asset Types - Count
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('AssetTypesCntContainer')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body">
                            <div id="AssetTypesCntContainer"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Mapped - Unmapped Capital Assets
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('MMAssets')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body">
                            <div id="MMAssets"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Asset Po Status
                        </div>
                        <br />
                       <%-- <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="AST.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>--%>

                        <div class="col-md-3 col-sm-3 col-xs-6">
                           <%-- <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="AST.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                            </div>--%>

                        </div>


                        <%--<div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Location</label>
                                <select id="ddllocation1" class="form-control selectpicker with-search" data-live-search="true"></select>
                                
                            </div>
                        </div>--%>

                        <br />
                       <%-- <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <input type="button" id="btnsubmit" value="Submit" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>--%>

                       <%-- <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('TotalAssets')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>--%>
                        <div class="panel-body">
                            <div id="AssetPo"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>No of Asset Dispose
                        </div>
                        <br />
                       <%-- <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate2'>
                                    <input type="text" class="form-control" data-ng-model="AST.FromDate2" id="FromDate2" name="FromDate2" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate2')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>--%>

                       <%-- <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate2'>
                                    <input type="text" class="form-control" data-ng-model="AST.ToDate2" id="ToDate2" name="ToDate2" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate2')"></i>
                                    </span>
                                </div>
                            </div>

                        </div>--%>

                       <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Location</label>
                                <select id="ddllocation2" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>
                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <input type="button" id="btnsubmit2" value="Submit" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>--%>

                       <%-- <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('TotalAssets')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>--%>
                        <div class="panel-body">
                            <div id="AssetDispose"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Asset Movement
                        </div>
                        <br />
                        <%--<div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate3'>
                                    <input type="text" class="form-control" data-ng-model="AST.FromDate3" id="FromDate3" name="FromDate3" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate3')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>--%>

                       <%-- <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate3'>
                                    <input type="text" class="form-control" data-ng-model="AST.ToDate3" id="ToDate3" name="ToDate3" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate3')"></i>
                                    </span>
                                </div>
                            </div>

                        </div>--%>

                       <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Location</label>
                                <select id="ddllocation3" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>

                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <input type="button" id="btnsubmit3" value="Submit" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>

                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('TotalAssets')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>--%>
                        <div class="panel-body">
                            <div id="AssetMovement"></div>
                        </div>
                    </div>
                </div>

              <%--  <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>High Demand Assets
                        </div>
                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate4'>
                                    <input type="text" class="form-control" data-ng-model="AST.FromDate4" id="FromDate4" name="FromDate4" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate4')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate4'>
                                    <input type="text" class="form-control" data-ng-model="AST.ToDate4" id="ToDate4" name="ToDate4" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate4')"></i>
                                    </span>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Location</label>
                                <select id="ddllocation4" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>

                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <input type="button" id="btnsubmit4" value="Submit" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>

                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('TotalAssets')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body">
                            <div id="TotalAssets4"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Same Asset with different Prices
                        </div>
                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate5'>
                                    <input type="text" class="form-control" data-ng-model="AST.FromDate5" id="FromDate5" name="FromDate5" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate5')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate5'>
                                    <input type="text" class="form-control" data-ng-model="AST.ToDate5" id="ToDate5" name="ToDate5" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate5')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Location</label>
                                <select id="ddllocation5" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>
                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <input type="button" id="btnsubmit5" value="Submit" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>

                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('TotalAssets')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body">
                            <div id="TotalAssets5"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i>Location wise Asset Details
                        </div>
                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate6'>
                                    <input type="text" class="form-control" data-ng-model="AST.FromDate6" id="FromDate6" name="FromDate6" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate6')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate6'>
                                    <input type="text" class="form-control" data-ng-model="AST.ToDate6" id="ToDate6" name="ToDate6" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate6')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Location</label>
                                <select id="ddllocation6" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>


                        <br />
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <input type="button" id="btnsubmit6" value="Submit" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>

                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('TotalAssets')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>

                        </div>
                        <div class="panel-body">
                            <div id="TotalAssets6"></div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
        <div class="modal fade" id="assetTypeDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H1">
                            <label for="lblASTmsg" class="control-label"></label>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-3 pull-left">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('tblassetTypes')">
                                <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                            </button>
                        </div>
                        <table id="tblassetTypes" class="table table-condensed table-bordered table-hover table-striped">
                            <tr>
                                <th>SNO</th>
                                <th>Asset Name</th>
                                <th>Asset Code</th>
                                <th>Asset Tagged To</th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<style>
    .multiselect-container {
        overflow-y: auto;
        overflow-x: auto;
    }
</style>
<script defer src="../../../Scripts/moment.min.js"></script>
<script type="text/javascript" defer>
    var Subcat = [];
    var CompanySessionId = '<%= Session("COMPANYID")%>'
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true
        });


    };
    var companyid = '<%= Session("TENANT")%>';
    if ('Aadhar.dbo' == companyid) {
        $("#Aadhar").show();
    } else {
        $("#Aadhar").hide();
    }
    $('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
    $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

    $('#FromDate2').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
    $('#ToDate2').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

    $('#FromDate3').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
    $('#ToDate3').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

    $('#FromDate4').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
    $('#ToDate4').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

    $('#FromDate5').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
    $('#ToDate5').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

    $('#FromDate6').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
    $('#ToDate6').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));


    var year;
    $(document).ready(function () {
        year = (new Date().getFullYear()).toString();
    });

    $(document).ready(function () {

        $("#ddlAstCompany").change(function () {
            Cmpy = $(this).val();
            var CompanyId = Cmpy;
            funfillAssetTypes(CompanyId);
            funfillMapUnmapAssets(CompanyId);
        });

        function funfillCompany1(valcompany) {

            $.each(Companies, function (key, value) {
                $("#ddlAstCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });

        }

        var Cmpy = '<%= Session("COMPANYID")%>'
        funfillCompany1(Cmpy);
        //Asset Types pie chart
        funfillAssetTypes(Cmpy);
        //Mapped Unmapped Assets
        funfillMapUnmapAssets(Cmpy);
        fillLocationDet();
        demoreport2();
        demoreport3();
        demoreport4();

        function funfillAssetTypes(CompanyId) {
            var param = { companyid: parseInt(CompanyId) };
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    onclick: function (e) {
                        $("#assetTypeDetails").modal("show");
                        getAssetTypeDetails(CompanyId, e.id);
                    }
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/AssetDBAPI/GetAssetTypes',
                type: 'POST',
                data: param,
                success: function (result) {
                    chart.unload();
                    chart.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#AssetTypesCntContainer").empty();
                $("#AssetTypesCntContainer").append(chart.element);
            }, 1500);
        }

        function getAssetTypeDetails(CompanyId, type) {
            console.log(type);
            $("label[for='lblASTmsg']").html('Asset Category Type Details');
            $.ajax({
                url: "../api/AssetDBAPI/GetAssetType_Categories?category=" + type + "&Companyid=" + CompanyId + "",
                // data: { "category": type, "Companyid": CompanyId },
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    var table = $('#tblassetTypes');
                    $('#tblassetTypes td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + (i + 1) + "</td>" +
                            "<td>" + data[i].NAME + "</td>" +
                            "<td>" + data[i].CODE + "</td>" +
                            "<td>" + 'NA' + "</td>" +
                            "</tr>");
                    }
                    $('#tblassetTypes td:nth-child(4),#tblassetTypes th:nth-child(4)').hide();
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='3' align='center'> No Asset types found</td>" + "</tr>");
                    }

                },
                error: function (result) {
                }
            });
        }

        function funfillMapUnmapAssets(CompanyId) {
            var param = { companyid: parseInt(CompanyId) };
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'donut',
                    onclick: function (e) {
                        $("#assetTypeDetails").modal("show");
                        getMappedUnmapAssets(CompanyId, e.id);
                    }
                },
                donut: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                },

            });

            $.ajax({
                url: '../api/AssetDBAPI/GetMapunmapassets',
                type: 'POST',
                data: param,
                success: function (result) {
                    chart.unload();
                    chart.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#MMAssets").empty();
                $("#MMAssets").append(chart.element);
            }, 1500);
        }

        function getMappedUnmapAssets(CompanyId, type) {
            $.ajax({
                //url: "../api/AssetDBAPI/GetMap_umMapAssets_DB",  
                url: "../api/AssetDBAPI/GetMap_umMapAssets_DB?Companyid=" + CompanyId + "",
                //data: { "category": type },
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    var table = $('#tblassetTypes');
                    $('#tblassetTypes td').remove();
                    var dt;
                    if (type == 'MAPPED') {
                        dt = data.Table1;
                        $("label[for='lblASTmsg']").html('Mapped Asset Details');
                    }
                    else {
                        dt = data.Table2;
                        $("label[for='lblASTmsg']").html('Unmapped Asset Details');
                    }
                    for (var i = 0; i < dt.length; i++) {
                        table.append("<tr>" +
                            "<td>" + (i + 1) + "</td>" +
                            "<td>" + dt[i].ASSET_NAME + "</td>" +
                            "<td>" + dt[i].AAS_AAT_CODE + "</td>" +
                            "<td>" + dt[i].TAGGED_TO + "</td>" +
                            "</tr>");
                    }
                    if (type == 'MAPPED') {
                        $('#tblassetTypes td:nth-child(4),#tblassetTypes th:nth-child(4)').show();
                    }
                    else {
                        $('#tblassetTypes td:nth-child(4), #tblassetTypes th:nth-child(4)').hide();
                    }

                    if (dt.length == 0) {
                        table.append("<tr>" + "<td colspan='3' align='center'> No mapped/unmapped assets found</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }
        function GetTotalAssets() {

            var param = { From_Date: $("#FromDate").val(), To_Date: $("#ToDate").val() };
            console.log(param);
            $.ajax({
                url: '../api/AssetDBAPI/GetTotalAssets_by_Sub_category',
                type: 'POST',
                data: param,
                success: function (result) {
                    Subcat = result.Subcat;
                    var chart = c3.generate({
                        data: {
                            x: 'x',
                            columns: [
                                result.Subcat,
                                result.Available,
                                result.CONSUMED,
                                result.Total,
                            ],
                            groups: [
                                ['Total Assets', 'Consumed Assets', 'Available Assets']
                            ],
                            order: 'null',
                            type: 'bar',
                            labels: true,


                        },
                        axis: {
                            x: {
                                type: 'category',
                                tick: {
                                    rotate: -65,
                                    multiline: false
                                },
                                height: 100
                            }
                        },

                        bindto: '#TotalAssets'
                    });
                }
            });
        }
        $('#btnsubmit').click(function () {
            GetTotalAssets();
        })
        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                if (Subcat.length == 0)
                    GetTotalAssets();
            }
        });

        function Print(divName) {
            $('#' + divName).print({
                globalStyles: true,
                mediaPrint: false,
                stylesheet: "http://fonts.googleapis.com/css?family=Inconsolata",
                iframe: true,
                prepend: "<h3>QuickFMS</h3> <hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-bottom: 0.5em;''margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'>" + "<br/>",
                append: "<hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'> <h6 align='center'>© QuickFMS " + year + " </h6>"
            });

        }

        var companyid = '<%=Session("COMPANYID")%>'

        function enableDisableCompanyDDL(compid) {
            if (compid == "1") {
                $("#ddlAstCompany").prop('disabled', false);
            }
            else {
                $("#ddlAstCompany").prop('disabled', true);
                $('#ddlAstCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }
        setTimeout(function () { enableDisableCompanyDDL(Cmpy) }, 1000);
    });

    var Location = [];
    function fillLocationDet() {
        var params = { id: 2 };
        $.ajax({
            url: '../api/Utility/GetLocations',
            data: params,
            type: 'Get',
            success: function (result) {
                Location = result.data;

                console.log(result);
            }

        });
        setTimeout(function () {
            $.each(Location, function (key, value) {

                $("#ddllocation1").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
        }, 2000);

        setTimeout(function () {
            $.each(Location, function (key, value) {

                $("#ddllocation2").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
        }, 2000);
        setTimeout(function () {
            $.each(Location, function (key, value) {

                $("#ddllocation3").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
        }, 2000);
        setTimeout(function () {
            $.each(Location, function (key, value) {

                $("#ddllocation4").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
        }, 2000);
        setTimeout(function () {
            $.each(Location, function (key, value) {

                $("#ddllocation5").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
        }, 2000);
        setTimeout(function () {
            $.each(Location, function (key, value) {

                $("#ddllocation6").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
        }, 2000);
    }

     function demoreport2() {
            $.ajax({
                url: '../api/AssetDBAPI/Asset_Po_Status',
                //data: param,
                type: 'post',
                datatype: 'json',
                success: function (data) {
                    console.log(data);

                    var chart5 = c3.generate({
                        data: {
                            
                            columns: data,
                            type: 'donut',

                             donut: {
                                expand: false,
                                label: {
                                    format: function (value, ratio, id) {
                                        return d3.format('')(value);
                                    }
                                }

                            },
                        }
                       
                       

                    });
                    $("#AssetPo").html("");
                    $("#AssetPo").append(chart5.element);
                    $("#AssetPo").load();
                }

            });
    }

    function demoreport3() {
        
        $.ajax({
            url: '../api/AssetDBAPI/Asset_Dispose_details',
            //data: param,
            type: 'post',
            datatype: 'json',
            success: function (data) {
                console.log(data);

                var chart5 = c3.generate({
                    data: {

                        columns: data,
                        type: 'donut',

                        donut: {
                            expand: false,
                            label: {
                                format: function (value, ratio, id) {
                                    return d3.format('')(value);
                                }
                            }

                        },
                    }



                });
                $("#AssetDispose").html("");
                $("#AssetDispose").append(chart5.element);
                $("#AssetDispose").load();
            }

        });
       
    }

     function demoreport4() {
        $.ajax({
            url: '../api/AssetDBAPI/Asset_Movement_Details',
            //data: param,
            type: 'post',
            datatype: 'json',
            success: function (data) {
                console.log(data);

                var chart5 = c3.generate({
                    data: {

                        columns: data,
                        type: 'donut',

                        donut: {
                            expand: false,
                            label: {
                                format: function (value, ratio, id) {
                                    return d3.format('')(value);
                                }
                            }

                        },
                    }



                });
                $("#AssetMovement").html("");
                $("#AssetMovement").append(chart5.element);
                $("#AssetMovement").load();
            }

        });
    }

</script>
