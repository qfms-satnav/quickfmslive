﻿<%@ Page Language="C#" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
     <link href="../BlurScripts/BlurCss/bootstrap.css" rel="stylesheet" />
    <link href="../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../BootStrapCSS/FMS_UI/css/flaticon.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/notify.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/prettify.css" rel="stylesheet" />
    
    <link href="../BlurScripts/BlurCss/DashboardMaster.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <style>
    hr {
        display: block;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        margin-left: auto;
        margin-right: auto;
        border-style: inset;
        border-width: 1px;
    }
</style>

<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-6" style="margin-left: 30% ;margin-top: 4%;">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-pie-chart fa-fw"></i> Support 
                        </div>
                        <br />
                        <table id="tblSPRT" class="table table-condensed table-bordered table-hover table-striped">
                            <tr style='text-align:center'>
                                <th>Escalation</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone no</th>
                            </tr>
                        </table>
                    </div>
                </div>
                </div>
        
    </form>
</body>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script type="text/javascript" defer>
    
    demoreport7();
    async function demoreport7() {
        var result;
        try {
            result = await $.ajax({
                url: '../api/HelpDeskDBAPI/BindSupport',
                //data: param,
                //contentType: "application/json; charset=utf-8",
                type: 'post',
                datatype: 'json'
            });
            var table = $('#tblSPRT');
            $('#tblSPRT td').remove();
            for (var i = 0; i < result.length; i++) {
                table.append("<tr style='text-align:center'>" +

                    "<td >" + result[i].ESCALATION_MATRIX + "</td>" +
                    "<td>" + result[i].SC_NAME + "</td>" + "<td>" + result[i].EMAIL + "</td>" + "<td>" + result[i].PH_NO + "</td>" +
                    "</tr>");
            }
            if (result.length == 0) {
                table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found</td>" + "</tr>");
            }
        }
        catch (error) {
            console.error(error);
        }

    }
  
</script>
</html>


