﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style>
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }
    </style>
</head>
<body>
   
    <script type="text/javascript">
        var companyid = '<%= Session("TENANT")%>';
        if (companyid.toLowerCase() == "bnp.dbo") {
            document.getElementById("myFrame").src = "https://app.powerbi.com/view?r=eyJrIjoiNWNkZGNkNjgtNDRlMC00NWRkLTg2MzctMjY3Y2U2MGFjZjM0IiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9"
        }
        else if (companyid.toLowerCase() == "aadhar.dbo") {
            document.getElementById("myFrame").src = "https://app.powerbi.com/view?r=eyJrIjoiNjkxNWYyMzAtY2E0Yy00NjMwLTg2MWQtYmJjOWJmMDNhOTIzIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9"
        }
        else {
            $("#Dashboard").hide();
            //document.getElementById("Dashboard").style.display = None;
        }
    </script>
    <div>
        <form id="form1" runat="server">

            <%--<div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-inverse" id="btnLease">
                        <i class="fa fa-check" aria-hidden="true" id="selectedLease"></i>&nbsp Lease
                    </button>
                </div>

                <div class="col-md-3">
                    <button type="button" class="btn btn-inverse" id="btnTenant">
                        <i class="fa fa-check" aria-hidden="true" id="selectedTenant"></i>&nbsp Tenant
                    </button>
                </div>

            </div>--%>


            <div id="Dashboard" style="align-content: center">
                <iframe id="myFrame" width="1024" height="700" frameborder="0" allowfullscreen="true"></iframe>
                <%-- <h5 id="selectedDashboard" class="col-md-2"></h5>--%>
            </div>

            <hr />
            <br />
            <div>
                <%--Lease Tag--%>
                <div id="divLease">
                    <%--Lease PrintProp Functionality--%>
                    <div class="row" style="padding-top: 10px;">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Company</label>
                                <select id="LeaseCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12" align="right">
                            <div class="form-group">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('Lease')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: green"></span><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="Lease">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                                        <i class="fa fa-bar-chart fa-fw"></i>Property Units by City
                            
                                    </div>
                                    <div align="right">
                                        <button type="button" class="btn btn-default btn-sm" onclick="Print('citywiseproperties')" style="color: rgba(58, 156, 193, 0.87)">
                                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                        </button>
                                    </div>
                                    <div class="panel-body">
                                        <div id="citywiseproperties"></div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />

                            <div class="col-md-3 col-md-6">
                                <div class="row">
                                    <div class="col-md-9 text-center">
                                        <div class="col-md-9 text-left">
                                            (Current Month)
                                        <button type="button" id="ancWorkReqTotalMain" class="alert alert-danger" style="width: 230px;">
                                            <i class="fa fa-exclamation-triangle pull-left" aria-hidden="true"></i>About to Expire Leases
                                                    <span class="badge pull-right" id="lblexpleasesCnt"></span>
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="row">
                                    <div class="col-md-9 text-center">
                                        <div class="col-md-9 text-left">
                                            (Current Year)
                                            <button type="button" id="AncLeasesDues1" class="alert alert-danger" style="width: 230px;">
                                                <i class="fa fa-tasks pull-left" aria-hidden="true"></i>Lease Dues
                                                    <span class="badge pull-right" id="lblleaseDues"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <%--Tenant Tag--%>
                <div id="divTenant">
                    <%--Tenant Print Functionality--%>
                    <div class="row" style="padding-top: 10px;">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Company</label>
                                <select id="TenantCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                            </div>
                        </div>

                        <div class="col-md-9 col-sm-12 col-xs-12" align="right">
                            <div class="form-group">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('Tenant')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: green"></span><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="Tenant">
                        <div class="row">
                            <div class="col-md-3">
                                <button type="button" id="btnVacantProperties" class="btn btn-info">
                                    Vacant Properties
                            <span class="badge" id="spnVacantProperties"></span>
                                </button>
                            </div>

                            <div class="col-md-3">
                                <button type="button" id="btnUpcomingVacancies" class="btn btn-danger">
                                    Upcoming Vacancies
                            <span class="badge" id="spnUpcomingVacancies"></span>
                                </button>
                            </div>

                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3 col-md-6">
                                <div class="row">
                                    <div class="col-md-9 text-center">
                                        <div class="col-md-9 text-left">
                                            <div class="align-right">(Current Month)</div>
                                            <button type="button" id="tenantDues" class="alert alert-info" style="width: 230px;">
                                                <i class="fa fa-tasks pull-left" aria-hidden="true"></i>Tenant Dues
                                                    <span class="badge pull-right" id="lblTenantDues"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-md-6">
                                <div class="row">
                                    <div class="col-md-9 text-center">
                                        <div class="col-md-9 text-left">
                                            (Current Month)
                                            <button type="button" id="ancWorkReqTotal" class="alert alert-tasks" style="width: 230px;">
                                                <i class="fa fa-tasks pull-left" aria-hidden="true"></i>Total Work Requests
                                                    <span class="badge pull-right" id="lblwrtotcount"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="row">
                                    <div class="col-md-9 text-center">
                                        <div class="col-md-9 text-left">
                                            (Current Month)
                                        <button type="button" id="ancWorkReqCompleted" class="alert alert-success" style="width: 230px;">
                                            <i class="fa fa-tasks pull-left" aria-hidden="true"></i>Completed Work Requests
                                                    <span class="badge pull-right" id="lblWRclosed"></span>
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-6">
                                <div class="row">
                                    <div class="col-md-9 text-center">
                                        <div class="col-md-9 text-left">
                                            (Current Month)
                                        <button type="button" id="ancWorkReqPending" class="alert alert-danger" style="width: 230px;">
                                            <i class="fa fa-tasks pull-left" aria-hidden="true"></i>Pending Work Requests
                                                    <span class="badge pull-right" id="lblpending"></span>
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                                <i class="fa fa-pie-chart fa-fw"></i>Properties Count
                            </div>
                            <div align="right">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('propertycontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                            <div class="panel-body">
                                <div id="propertycontainer"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" id="divTenantBarChart">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Received and Pending Amounts (Current Month)
                            </div>
                            <div align="right">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('rentscontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                            <div class="panel-body">
                                <div id="rentscontainer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" id="divLeaseBarChart">
                        <div class="panel panel-default" style="padding-top: 12px; padding-left: 10px;">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i>Received and Pending Amounts (Current Year)
                            </div>
                            <div align="right">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('LeaseDuecontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                    <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                                </button>
                            </div>
                            <div class="panel-body">
                                <div id="LeaseDuecontainer">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-bar-chart fa-fw"></i>Expenses Utilized by Location                                 
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('expenseutilitycontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <div id="expenseutilitycontainer"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="TotalWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H2">Work Requests Raised This Month</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tbltotalWRS')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tbltotalWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Requested Date</th>
                                    <th>Requested By</th>
                                    <th>Estimated Amount</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="CompletedWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H1">Completed Work Requests</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblCompletedWRS')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblCompletedWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Requested Date</th>
                                    <th>Requested By</th>
                                    <th>Last Updated</th>
                                    <th>Estimated Amount</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="PendingWRS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H3">Pending Work Requests</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblPendingWRS')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblPendingWRS" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Requested Date</th>
                                    <th>Requested By</th>
                                    <th>Last Updated</th>
                                    <th>Estimated Amount</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ExpLeases1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H4">Leases Expiring This Month</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblExpLeasesThisMonth')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblExpLeasesThisMonth" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Lease Start Date</th>
                                    <th>Lease End Date</th>
                                    <th>Property Name</th>
                                    <th>Building ID</th>
                                    <th>Monthly Rent</th>
                                    <th>Security Deposit</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divLeaseCurrentYear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H10">Lease Dues This Year</h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblLeaseDuesThisYr')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblLeaseDuesThisYr" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Lease </th>
                                    <th>Property</th>
                                    <th>Total Rent</th>
                                    <th>Due Amount</th>
                                    <th>Agreement Start Date</th>
                                    <th>Agreement End Date</th>

                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="PropDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H5">Property Details <span id="selectedTypeProp"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblPropDetails')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblPropDetails" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Property Name</th>
                                    <th>Location Name</th>
                                    <th>Last Updated</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="receivedAmtDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H6">Amount Details <span id="Span1"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('Table0')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="Table0" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Property Name</th>
                                    <th>Tenant Name</th>
                                    <th>Tenant Email</th>
                                    <th>Rent Amount</th>
                                </tr>
                            </table>

                            <table id="Table1" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Request ID</th>
                                    <th>Property Type</th>
                                    <th>Property Name</th>
                                    <th>Work Title</th>
                                    <th>Estimated Amount</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Phone</th>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="DivCityWiseProperties" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H7">Properties by City <span id="Span2"></span></h4>
                        </div>

                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblcityprops')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblcityprops" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>City Name</th>
                                    <th>Location Name</th>
                                    <th>Prop. Type</th>
                                    <th>Prop. Name</th>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="divVacantProperties" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H8">Property Details <span id="Span3"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblVacantProps')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblVacantProps" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Location Name</th>
                                    <th>Prop. Type</th>
                                    <th>Prop. Code</th>
                                    <th>Prop. Name</th>
                                    <th>Owner Name</th>
                                    <th>Ph. No.</th>
                                    <th>Last Updated Dt.</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divUpcomingVacancies" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="H9">Tenant Details <span id="Span4"></span></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblUpcomingVacancies')">
                                    <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                                </button>
                            </div>
                            <table id="tblUpcomingVacancies" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>

                                    <th>Prop. Type</th>
                                    <th>Prop. Name</th>
                                    <th>Tenant Name</th>
                                    <th>Tenant Code</th>
                                    <th>Tenant Phone</th>
                                    <th>Tenant Email</th>
                                    <th>Rent Amount</th>
                                    <th>Security Deposit</th>
                                    <th>Joined Date</th>
                                    <th>End Date</th>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divBarGraph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="divBarGraphHeading"></h4>
                        </div>
                        <div class="modal-body">
                            <div class=" col-md-3 pull-left">
                                <button type="button" class="btn btn-default btn-sm" onclick="Print('tblBarGraph')">
                                    <span class="glyphicon glyphicon-print"></span>Print
                                </button>
                            </div>
                            <table id="tblBarGraph" class="table table-condensed table-bordered table-hover table-striped">
                                <tr>
                                    <th>Location</th>
                                    <th>Expense Head</th>
                                    <th>Bill Date</th>
                                    <th>Bill No.</th>
                                    <th>Bill Invoice</th>
                                    <th>Amount</th>
                                    <th>Bill Generated By</th>
                                    <th>Remarks</th>


                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript" defer>
        var CompanySessionId = '<%= Session("COMPANYID")%>'

        var year
        $(document).ready(function () {
            year = (new Date().getFullYear()).toString();
        });
        $(document).ready(function () {
            $("#TenantCompany").change(function () {
                CompanySessionId = $(this).val();
                var CompanySessionId = CompanySessionId;
                funfillProperties(CompanySessionId);
                FunBindAmounts(CompanySessionId);
                funExpLeases(CompanySessionId);
                vacantProperties(CompanySessionId);
                tenanctVacancies(CompanySessionId);
                ExpenseUtilityChart(CompanySessionId);
            });

            $("#LeaseCompany").change(function () {
                CompanySessionId = $(this).val();
                var LeaseCompanyId = CompanySessionId;
                FuncBindLeaseDuesDet(LeaseCompanyId);
                funfillProperties(LeaseCompanyId);
                FunBindAmounts(LeaseCompanyId);
                funExpLeases(LeaseCompanyId);
                vacantProperties(LeaseCompanyId);
                tenanctVacancies(LeaseCompanyId);
                ExpenseUtilityChart(LeaseCompanyId);
                citywise_properties(LeaseCompanyId);
            });
            ExpenseUtilityChart(CompanySessionId);
            funfillProperties(CompanySessionId);
            //citywise_props();
            citywise_properties(CompanySessionId);
            $('#divTenant').hide();
            $('#divTenantBarChart').hide();
            // $('#divLeaseBarChart').hide();

            $('#selectedTenant').hide();

            $('#selectedDashboard').text("Lease Dashboard");



            $('#btnTenant').click(function () {
                $('#selectedDashboard').text("Tenant Dashboard");
                $('#selectedTenant').show();
                $('#selectedLease').hide();
                $('#divLease').hide();
                $('#divTenant').show();
                $('#divLeaseBarChart').hide();
                $('#divTenantBarChart').show();

                tenanctVacancies(CompanySessionId);
                vacantProperties(CompanySessionId);


            });

            $('#btnLease').click(function () {
                $('#selectedDashboard').text("Lease Dashboard");
                $('#selectedTenant').hide();
                $('#selectedLease').show();
                $('#divTenant').hide();
                $('#divLease').show();
                $('#divTenantBarChart').hide();
                $('#divLeaseBarChart').show();
            });


            //Pending/Received Amounts

            FunBindAmounts(CompanySessionId);
            //LeaseDues
            FuncBindLeaseDuesDet(CompanySessionId);
            // properties tenant
            //funfillProperties_Tenant();

            //Work Request Count Binding
            GetWRCount("1");

            //Exp leases count/Details
            funExpLeases(CompanySessionId);

            function fillPropertyCompany(valcompany) {

                $.each(Companies, function (key, value) {
                    $("#TenantCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
                });

            }

            function fillLeaseCompany(valcompany) {

                $.each(Companies, function (key, value) {
                    $("#LeaseCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
                });

            }
            var companyid = '<%= Session("COMPANYID")%>'
        fillPropertyCompany(companyid);
        fillLeaseCompany(companyid);
        //Properties Count func
        function funfillProperties(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            var comp = param.companyId
            var properties = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',

                    empty: { label: { text: "No Data Available" } },
                    // onclick: function (d, i) { console.log("onclick", d, i); },
                    //onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    //onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                    onclick: function (e) {
                        $("#PropDetails").modal("show");
                        GetPropertyDetailsByPropName(e.id);
                    }
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/PropertyDBAPI/BindProperties',
                data: param,
                type: 'POST',
                success: function (result) {
                    properties.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#propertycontainer").html("");
                $("#propertycontainer").append(properties.element);
                $("#propertycontainer").load();
            }, 1000);
        }

        //Pending/Received Amounts Bar chart  tenant    
        function FunBindAmounts(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/PropertyDBAPI/BindAmounts',
                type: 'POST',
                data: param,
                success: function (summarydata) {
                    console.log(summarydata);
                    $('#lblTenantDues').html(summarydata.ChartData[1].PendingReq);
                    var chart2 = c3.generate({
                        data: {
                            columns: [["Rents", summarydata.ChartData[0].TotalReq, summarydata.ChartData[1].PendingReq, summarydata.ChartData[2].WRExpenses, 0]],
                            type: 'bar',
                            onclick: function (e) {
                                $("#receivedAmtDetails").modal("show");
                                PendingAndReceivedAmounts(e.index);
                            },
                        },
                        bar: {
                            width: {
                                ratio: 0.5
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                categories: ["Total Rents Received", "Pending Rents", "Work Request Expenses", "Other Expenses"],
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Amount',
                                    position: 'outer-middle'
                                }
                            },

                        },
                        bindto: '#rentscontainer'
                    });
                }
            });
        }

        //Lease dues bar chart

        function FuncBindLeaseDuesDet(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/PropertyDBAPI/GetLeaseDueDetails',
                type: 'POST',
                data: param,
                success: function (summarydata) {
                    //console.log(summarydata);
                    $('#lblleaseDues').html(summarydata.ChartData[1].PendingReq);
                    var chart2 = c3.generate({
                        data: {
                            columns: [["Rents", summarydata.ChartData[0].TotalReq, summarydata.ChartData[1].PendingReq, 0, 0]],
                            type: 'bar',
                            onclick: function (e) {
                                $("#divLeaseCurrentYear").modal("show");
                                funLeasesDues(e.index);
                            }

                        },
                        bar: {
                            width: {
                                ratio: 0.5
                            }
                        },
                        axis: {
                            x: {
                                type: 'category',
                                categories: ["Total Amount Received", " Lease Due", "Maintenance Expenses", "Other Expenses"],
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Amount',
                                    position: 'outer-middle'
                                }
                            },

                        },
                        bindto: '#LeaseDuecontainer'
                    });
                }
            });
        }


        function GetWRCount(type) {
            $.ajax({
                url: "../api/PropertyDBAPI/GetWorkRequests",
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                    if (type == "1") {
                        $("#lblwrtotcount").html(data.Table.length);
                        $("#lblWRclosed").html(data.Table1.length);
                        $("#lblpending").html(data.Table2.length);
                    }
                    //Total WR
                    if (type == "2") {
                        var table = $('#tbltotalWRS');
                        $('#tbltotalWRS td').remove();
                        for (var i = 0; i < data.Table.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data.Table[i].PN_NAME + "</td>" +
                                "<td>" + data.Table[i].BDG_ID + "</td>" +
                                "<td>" + data.Table[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table[i].AUR_FIRST_NAME + "</td>" +
                                "<td>" + data.Table[i].ESTIMATED_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.Table.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Work Requests Raised this Month</td>" + "</tr>");
                        }
                    }
                    //Completed
                    if (type == "3") {
                        var table = $('#tblCompletedWRS');
                        $('#tblCompletedWRS td').remove();
                        for (var i = 0; i < data.Table1.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data.Table1[i].PN_NAME + "</td>" +
                                "<td>" + data.Table1[i].BDG_ID + "</td>" +
                                "<td>" + data.Table1[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table1[i].AUR_FIRST_NAME + "</td>" +
                                "<td>" + data.Table1[i].LAST_UPDATED_DATE + "</td>" +
                                "<td>" + data.Table1[i].ESTIMATED_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.Table1.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Completed Requests Found this Month</td>" + "</tr>");
                        }
                    }

                    //Pending
                    if (type == "4") {
                        var table = $('#tblPendingWRS');
                        $('#tblPendingWRS td').remove();
                        for (var i = 0; i < data.Table2.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data.Table2[i].PN_NAME + "</td>" +
                                "<td>" + data.Table2[i].BDG_ID + "</td>" +
                                "<td>" + data.Table2[i].REQUESTED_DATE + "</td>" +
                                "<td>" + data.Table2[i].AUR_FIRST_NAME + "</td>" +
                                "<td>" + data.Table2[i].LAST_UPDATED_DATE + "</td>" +
                                "<td>" + data.Table2[i].ESTIMATED_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.Table2.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                        }
                    }
                },
                error: function (result) {
                }
            });
        }


        function GetPropertyDetailsByPropName(type) {
            var param = { Type: type, companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetPropDetailsByPropName",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    var table = $('#tblPropDetails');
                    $('#tblPropDetails td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].PN_NAME + "</td>" +
                            "<td>" + data[i].LCM_NAME + "</td>" +
                            "<td>" + data[i].LAST_UPDATE_DATE + "</td>" +
                            //"<td>" + data.Table2[i].LAST_UPDATED_DATE + "</td>" +
                            //"<td>" + data.Table2[i].ESTIMATED_AMOUNT + "</td>" +
                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                    }

                },
                error: function (result) {
                }
            });
        }
        //Tenant View Details
        function PendingAndReceivedAmounts(type) {
            var param = { Type: type, companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetPendingAndReceivedAmounts",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#Table0').show();
                    $('#Table1').show();


                    if (type == 0 || type == 1) {
                        var table = $('#Table0');
                        $('#Table0 td').remove();
                        $('#Table1').hide();
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].PN_NAME + "</td>" +
                                "<td>" + data[i].TEN_NAME + "</td>" +
                                "<td>" + data[i].TEN_EMAIL + "</td>" +
                                "<td>" + data[i].TEN_OUTSTANDING_AMOUNT + "</td>" +
                                "</tr>");
                        }
                        if (data.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                        }
                    }
                    else if (type == 2) {
                        $('#Table0').hide();
                        var table = $('#Table1');
                        $('#Table1 td').remove();
                        for (var i = 0; i < data.length; i++) {
                            table.append("<tr>" +
                                "<td>" + data[i].PN_WORKREQUEST_REQ + "</td>" +
                                "<td>" + data[i].PN_PROPERTYTYPE + "</td>" +
                                "<td>" + data[i].PN_NAME + "</td>" +
                                "<td>" + data[i].WORK_TITLE + "</td>" +
                                "<td>" + data[i].ESTIMATED_AMOUNT + "</td>" +
                                "<td>" + data[i].VENDOR_NAME + "</td>" +
                                "<td>" + data[i].VENDOR_PHONE + "</td>" +
                                "</tr>");
                        }
                        if (data.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Pending Requests Found this Month</td>" + "</tr>");
                        }

                    }
                    else {
                    }

                },
                error: function (result) {
                }
            });
        }


        //View Leases and count
        function funExpLeases(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetExpLeasesDetails",
                data: param,
                //contentType: "application/json; charset=utf-8",
                empty: { label: { text: "No Data Available" } },
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $("#lblexpleasesCnt").html(data.length);
                    var table = $('#tblExpLeasesThisMonth');
                    $('#tblExpLeasesThisMonth td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].LEASE_START_DATE + "</td>" +
                            "<td>" + data[i].LEASE_END_DATE + "</td>" +
                            "<td>" + data[i].PN_NAME + "</td>" +
                            "<td>" + data[i].BDG_ID + "</td>" +
                            "<td>" + data[i].MONTHLY_RENT + "</td>" +
                            "<td>" + data[i].SECURITY_DEPOSIT + "</td>" +
                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='7' align='center'> No Leases to Expire This Month </td>" + "</tr>");
                    }
                }
            });
        }
        // Lease Dues details
        function funLeasesDues(type) {
            $.ajax({
                url: "../api/PropertyDBAPI/GetDueLeaseReceivedAmounts",
                data: { "category": type },
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    //$("#lblleaseDues").html(data.length);
                    var table = $('#tblLeaseDuesThisYr');
                    $('#tblLeaseDuesThisYr td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +

                            "<td>" + data[i].LEASE + "</td>" +
                            "<td>" + data[i].PROPERTY + "</td>" +
                            "<td>" + data[i].TOTAL_RENT + "</td>" +
                            "<td>" + data[i].PAID_AMOUNT + "</td>" +
                            "<td>" + data[i].EFFECTIVE_AGREEMENT_DATE + "</td>" +
                            "<td>" + data[i].EXTESNION_TODATE + "</td>" +

                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='7' align='center'> No Lease Dues Found This Year </td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }


        function citywise_properties(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/PropertyDBAPI/BindCityWise_Properties',
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    var chart2 = c3.generate({

                        data: {
                            columns: data, //[['BANG', 30],['IND', 130]],
                            type: 'bar',
                        },
                        //bar: {
                        //    width: {
                        //        ratio: 0.5 // this makes bar width 50% of length between ticks
                        //    }
                        //},
                        axis: {
                            x: {
                                show: true,
                                label: {
                                    text: 'Cities',
                                }
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Count of Properties',
                                    position: 'outer-middle'
                                }
                            },
                        }

                    });
                    $("#citywiseproperties").html("");
                    $("#citywiseproperties").append(chart2.element);
                    $("#citywiseproperties").load();

                },
                error: function (result) {

                }
            });
        }

        // city wise properties - pie
        function citywise_props() {
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" } },
                    onclick: function (e) {
                        $("#DivCityWiseProperties").modal("show");
                        // $('#selectedType').text(e.id);
                        GetPropertiesByCity(e.id);
                    }
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/PropertyDBAPI/BindCityWise_Properties',
                type: 'POST',
                success: function (result) {
                    chart.load({ columns: result });
                }
            });
            setTimeout(function () {
                $("#citywiseproperties").append(chart.element);
            }, 1000);
        }


        function GetPropertiesByCity(type) {

            $.ajax({
                url: "../api/PropertyDBAPI/GetProperties_City",
                data: { "cityName": type },
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {

                    var table = $('#tblcityprops');
                    $('#tblcityprops td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].CTY_NAME + "</td>" +
                            "<td>" + data[i].LCM_NAME + "</td>" +
                            "<td>" + data[i].PN_PROPERTYTYPE + "</td>" +
                            "<td>" + data[i].PN_NAME + "</td>" +
                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='4' align='center'> No Records Found</td>" + "</tr>");
                    }

                },
                error: function (result) {
                }
            });
        }


        //Total Work Requests count and View Details
        $("#ancWorkReqTotal").click(function (e) {
            e.preventDefault();
            $("#TotalWRS").modal("show");
            GetWRCount("2");
        });

        //Total Work Requests count and View Details
        $("#ancWorkReqCompleted").click(function (e) {
            e.preventDefault();
            $("#CompletedWRS").modal("show");
            GetWRCount("3");
        });

        //Pending Work Requests count and View Details
        $("#ancWorkReqPending").click(function (e) {
            e.preventDefault();
            $("#PendingWRS").modal("show");
            GetWRCount("4");
        });

        //AncExpLeases
        $("#ancWorkReqTotalMain").click(function (e) {
            e.preventDefault();
            $("#ExpLeases1").modal("show");
            funExpLeases(CompanySessionId);
        });
        //Lease Dues
        $("#AncLeasesDues1").click(function (e) {
            e.preventDefault();
            $("#divLeaseCurrentYear").modal("show");
            funLeasesDues(1);
        });

        //Tenant Dues
        $("#tenantDues").click(function (e) {
            e.preventDefault();
            $("#receivedAmtDetails").modal("show");
            PendingAndReceivedAmounts(1);
        });


        //vacant Properties
        $("#btnVacantProperties").click(function (e) {
            e.preventDefault();
            $("#divVacantProperties").modal("show");
            vacantProperties(CompanySessionId);

        });

        //upcoming vacancies
        $("#btnUpcomingVacancies").click(function (e) {
            e.preventDefault();
            $("#divUpcomingVacancies").modal("show");
            tenanctVacancies(CompanySessionId);
        });

        // tenant vacancies count and view details
        function tenanctVacancies(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/GetTenant_Vacancies",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $("#spnUpcomingVacancies").html(data.Table[0].NOOFTENANTS);
                    var table = $('#tblUpcomingVacancies');
                    $('#tblUpcomingVacancies td').remove();
                    for (var i = 0; i < data.Table1.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table1[i].PN_PROPERTYTYPE + "</td>" +
                            "<td>" + data.Table1[i].PN_NAME + "</td>" +
                            "<td>" + data.Table1[i].TEN_NAME + "</td>" +
                            "<td>" + data.Table1[i].TEN_CODE + "</td>" +
                            "<td>" + data.Table1[i].TEN_PHNO + "</td>" +
                            "<td>" + data.Table1[i].TEN_EMAIL + "</td>" +
                            "<td>" + data.Table1[i].RENT + "</td>" +
                            "<td>" + data.Table1[i].DEPOSIT + "</td>" +
                            "<td>" + data.Table1[i].TEN_JOINED_DATE + "</td>" +
                            "<td>" + data.Table1[i].TEN_END_DATE + "</td>" +
                            "</tr>");
                    }
                    if (data.Table1.length == 0) {
                        table.append("<tr>" + "<td colspan='10' align='center'> No Records Found </td>" + "</tr>");
                    }
                }
            });
        }

        //vacant properties count and details
        function vacantProperties(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: "../api/PropertyDBAPI/Get_Vacant_Properties",
                data: param,
                //contentType: "application/json; charset=utf-8",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $("#spnVacantProperties").html(data.Table[0].VACANT_PROPERTIES);
                    var table = $('#tblVacantProps');
                    $('#tblVacantProps td').remove();
                    for (var i = 0; i < data.Table1.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data.Table1[i].LCM_NAME + "</td>" +
                            "<td>" + data.Table1[i].PN_PROPERTYTYPE + "</td>" +
                            "<td>" + data.Table1[i].PROP_CODE + "</td>" +
                            "<td>" + data.Table1[i].PN_NAME + "</td>" +
                            "<td>" + data.Table1[i].OWNERNAME + "</td>" +
                            "<td>" + data.Table1[i].PHNO + "</td>" +
                            "<td>" + data.Table1[i].LAST_UPDATED_DATE + "</td>" +
                            "</tr>");
                    }
                    if (data.Table1.length == 0) {
                        table.append("<tr>" + "<td colspan='7' align='center'> No Records Found </td>" + "</tr>");
                    }
                }
            });
        }

        return false;


        function ExpenseUtilityChart(CompanySessionId) {
            var param = { companyId: parseInt(CompanySessionId) };
            $.ajax({
                url: '../api/PropertyDBAPI/ExpenseUtilityChartByLoc',
                data: param,
                //contentType: "application/json; charset=utf-8",
                empty: { label: { text: "No Data Available" } },
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    var chart2 = c3.generate({

                        data: {
                            json: JSON.parse(data.locVal),
                            keys: {
                                x: 'name',
                                value: JSON.parse(data.services),
                            },
                            type: 'bar',
                            onclick: function (e) {
                                $("#divBarGraph").modal("show");
                                var selectedService = this.categories()[e.index]
                                $('#divBarGraphHeading').html(e.id + "  - " + "(" + selectedService + ")");
                                GetExpenseUtilityDetailsByLoc(e.id, selectedService);
                            }
                        },
                        axis: {
                            x: {
                                type: 'category'
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Amount',
                                    position: 'outer-middle'
                                }
                            },
                        }
                    });
                    $("#expenseutilitycontainer").html("");
                    $("#expenseutilitycontainer").append(chart2.element);
                    $("#expenseutilitycontainer").load();
                },
                error: function (result) {
                }
            });
        }


        function GetExpenseUtilityDetailsByLoc(expense, location) {
            p = {
                location: location,
                expense: expense
            }
            $.ajax({
                url: "../api/PropertyDBAPI/GetExpenseUtilityDetailsByLoc",
                data: p,
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    var table = $('#tblBarGraph');
                    $('#tblBarGraph td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                            "<td>" + data[i].LOCATION + "</td>" +
                            "<td>" + data[i].EXPENSEHEAD + "</td>" +
                            "<td>" + data[i].BILLDATE + "</td>" +
                            "<td>" + data[i].BILL_NO + "</td>" +
                            "<td>" + data[i].BILL_INVOICE + "</td>" +
                            "<td>" + data[i].AMOUNT + "</td>" +
                            "<td>" + data[i].AURID + "</td>" +
                            "<td>" + data[i].REMARKS + "</td>" +

                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'>No Records Found.</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }


        function DDLDisable(compid) {
            if (compid == "1") {
                $("#TenantCompany").prop('disabled', false);
            }
            else {
                $("#TenantCompany").prop('disabled', true);
                $('#TenantCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }

        function LeaseCompanyDisable(compid) {
            if (compid == "1") {
                $("#LeaseCompany").prop('disabled', false);
            }
            else {
                $("#LeaseCompany").prop('disabled', true);
                $('#LeaseCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }
        setTimeout(function () { DDLDisable(CompanySessionId) }, 700);
        setTimeout(function () { LeaseCompanyDisable(CompanySessionId) }, 700);
    });
    </script>
</body>
</html>

