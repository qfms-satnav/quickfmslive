﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Tracker_Reports.aspx.vb" Inherits="Tracker_Reports_Tracker_Reports" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Bindrai Request Tracker</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">General</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table id="General" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th>
                                                    <asp:HyperLink ID="hpl" runat="server" NavigateUrl="~/Tracker_Reports/Monthly_Users_Count.aspx">Monthly Wise Employees's Status</asp:HyperLink></th>
                                                <th>
                                                    <asp:HyperLink ID="hplspctype" runat="server" NavigateUrl="~/Tracker_Reports/TransactionsByModules.aspx">Module Wise Usage Tracker</asp:HyperLink></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Property</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table id="Property" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th>
                                                    <asp:HyperLink ID="hplviewreqtypes" runat="server" NavigateUrl="~/Tracker_Reports//VacantProperties.aspx">Vacant Properties</asp:HyperLink></th>
                                                <th>
                                                    <asp:HyperLink ID="HYP2" runat="server" NavigateUrl="~/Tracker_Reports/Payments_Due_Properties.aspx">Pending Payments</asp:HyperLink></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Asset</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table id="Table1" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th>
                                                    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Tracker_Reports//ProcuredAssetCount.aspx">Monthly Procured Assets</asp:HyperLink></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Maintenance</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table id="Table2" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th>
                                                    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Tracker_Reports/AssetNotUnderAMCWrnty.aspx">No AMC & Warranty</asp:HyperLink></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Help desk</h3>
                                    </div>
                                    <div class="panel-body">
                                        <table id="Table3" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th>
                                                    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Tracker_Reports/Loc_CityWiseRequests.aspx">Location/City wise Requests</asp:HyperLink></th>
                                                <th>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Tracker_Reports//SLATrackerReport.aspx">SLA Breached Requests</asp:HyperLink></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
