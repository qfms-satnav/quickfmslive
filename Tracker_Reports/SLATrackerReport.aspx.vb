﻿Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports MaintenanceRpt
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class Tracker_Reports_SLATrackerReport
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtfromdate.Text = getoffsetdatetime(DateTime.Now).AddMonths(-1).ToString("dd/MM/yyyy")
            txttodate.Text = getoffsetdatetime(DateTime.Now).ToString("dd/MM/yyyy")
            Dim sp As New SubSonic.StoredProcedure(ConfigurationManager.AppSettings("FRMDB") & "." & "GET_TENANTS_LIST")
            ddltenant.DataSource = sp.GetDataSet()
            ddltenant.DataTextField = "TENANT_NAME"
            ddltenant.DataValueField = "TENANT_ID"
            ddltenant.DataBind()
            ddltenant.Items.Insert(0, "--Select--")
        End If
    End Sub

    Protected Sub ReportViewer1_Load(sender As Object, e As EventArgs)
        ScriptManager.RegisterStartupScript(Me, [GetType](), "ColorRow", "ColorRow();", True)
        bindGrid()
    End Sub


    Public Sub bindGrid()
        Dim fdt As DateTime = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
        Dim tdt As DateTime = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(ddltenant.SelectedValue & "." & "SLA_BREACHED_REPORT")
        sp.Command.Parameters.Add("@FROMDATE", fdt, DbType.DateTime)
        sp.Command.Parameters.Add("@TODATE", tdt, DbType.DateTime)
        ds = sp.GetDataSet()

        Dim rds As New ReportDataSource()
        rds.Name = "TrackerSLADS"
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/TrackerSLAReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        bindGrid()
    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/Tracker_Reports.aspx")
    End Sub
End Class
