﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SLATrackerReport.aspx.vb" Inherits="Tracker_Reports_SLATrackerReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/mm/yyyy',
                //todayHighlight: true,
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Age of Open Calls Report" ba-panel-class="with-scroll" style="padding-right: 18px;">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">SLA Breached Requests</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="Server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                                <label>Select Tenant<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddltenant"
                                                    Display="None" ErrorMessage="Please Select Tenant" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddltenant" runat="server" CssClass="selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                                <label>From Date</label>
                                                <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ControlToValidate="txtfromdate"
                                                    Display="None" ErrorMessage="Please Select From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <div class='input-group date' id='fromdate'>
                                                        <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                                <label>To Date</label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttodate"
                                                    Display="None" ErrorMessage="Please Select To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <div class='input-group date' id='todate'>
                                                        <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                                        <asp:Button ID="btnback" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />
                                    </div>
                                </div>
                                <br />
                                <div class="row table table table-condensed">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
