Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Net.Mail
Imports System.Web.Services
Imports System.Web
Imports System.Collections.Generic

Partial Class WorkSpace_SMS_Webfiles_Requisition
    Inherits System.Web.UI.Page
    Dim objData As SqlDataReader
    Dim obj As New clsWorkSpace
    Dim clsObj As New clsMasters
    Dim dtProject As DataTable
    Dim strRedirect As String = String.Empty
    Dim strQuery As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        Panel1.Visible = False
        If Not Page.IsPostBack Then
            txtFrmDt.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDt.ClientID + "')")
            txtFrmDt.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtToDt.Attributes.Add("onClick", "displayDatePicker('" + txtToDt.ClientID + "')")
            txtToDt.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtFromdate.Attributes.Add("onClick", "displayDatePicker('" + txtFromdate.ClientID + "')")
            txtFromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtTodate.Attributes.Add("onClick", "displayDatePicker('" + txtTodate.ClientID + "')")
            txtTodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            btnCancel.Attributes.Add("onclick", "return confirm('Are you sure to cancel the requisition? Click OK to Cancel');")
            btnSubmit.Attributes.Add("onclick", "return confirm('Are you sure to submit the requisition? Click OK to Submit');")
            txtNowDate.Text = getoffsetdatetime(DateTime.Now)
            lnkSearch.Visible = False
            pnlEmp.Visible = False
            pnlEmplst.Visible = False
            pnlForemp.Visible = False
            pnlFornonemp.Visible = False
            Panel1.Visible = False
            btnForpmp.Attributes.Add("onclick", "CheckDate()")

            LoadCity()
            clsObj.Bindlocation(ddlloc1)
            'txtFrmDt.Text = getoffsetdatetime(DateTime.Now)
            'txtToDt.Text = getoffsetdatetime(DateTime.Now)
            'txtFromdate.Text = getoffsetdatetime(DateTime.Now)
            'txtTodate.Text = getoffsetdatetime(DateTime.Now)
            strSQL = "select isnull(aur_reporting_to,'') aur_reporting_to,isnull((select isnull(aur_known_as,'') aur_known_as from  " & Session("TENANT") & "."  & "AMANTRA_USER b where a.aur_reporting_to=b.aur_id),'Not Available') as aur_known_as from  " & Session("TENANT") & "."  & "AMANTRA_USER a where a.aur_id='" & Session("uid") & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            While ObjDR.Read
                txtGSC.Text = ObjDR("aur_known_as")
                txtRMid.Text = ObjDR("aur_reporting_to")
            End While
            btnSubmit.Visible = False
            btnCancel.Visible = False

            visibleTF(0)
            Panel1.Visible = False
        End If

    End Sub
    Public Sub LoadCity()
        clsObj.BindVerticalCity(ddlCity)
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        '' ddlLoc.Items.Clear()
        'ddlTower1.Items.Clear()
        txtCity1.Text = ""
        txtCountry1.Text = ""
        txtLocation1.Text = ""
        Try
            If ddlCity.SelectedIndex <> 0 Then
                'obj.loadtower1(ddlTower1, ddlCity.SelectedValue.ToString().Trim())
                'obj.loadlocation_City(ddlLoc, ddlCity.SelectedValue.ToString().Trim())
                'If ddlLoc.Items.Count = 2 Then
                '    ddlLoc.SelectedIndex = 1
                '    ddlLoc_SelectedIndexChanged(sender, e)
                'End If
                'If ddlTower1.Items.Count = 1 Then
                '    lblMsg.Text = "No towers in this City"
                '    lblMsg.Visible = True
                'End If
                strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
                BindCombo(strSQL, ddlLoc, "LCM_name", "lcm_code")
            Else
                lblMsg.Text = "Please Select City"
                lblMsg.Visible = True
            End If
            Panel1.Visible = True

        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Requisition", "Load", ex)

        End Try

    End Sub
    'Protected Sub ddlTower1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower1.SelectedIndexChanged
    '    Try
    '        Dim loc_code As String = String.Empty
    '        If ddlTower1.SelectedIndex <> 0 Then
    '            loc_code = obj.tower1_selectedindexchanged(ddlTower1, txtCountry1, txtCity1, txtLocation1)
    '        Else
    '            lblMsg.Text = "Please Select Tower"
    '            lblMsg.Visible = True
    '        End If
    '    Catch ex As Exception
    '        Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Requisition", "Load", ex)

    '    End Try


    'End Sub

    Private Sub visibleTF(ByVal status As Integer)
        If status = 1 Then
            dgEmpMap.Columns(7).Visible = True
            dgEmpMap.Columns(8).Visible = True
            dgEmpMap.Columns(9).Visible = True
            dgEmpMap.Columns(10).Visible = True
            Panel1.Visible = True

            'dgEmpMap.Columns(12).Visible = True
        Else
            dgEmpMap.Columns(7).Visible = False
            dgEmpMap.Columns(8).Visible = False
            dgEmpMap.Columns(9).Visible = False
            dgEmpMap.Columns(10).Visible = False
            'dgEmpMap.Columns(12).Visible = False
            Panel1.Visible = True
        End If


    End Sub
    Private Sub radSpcToSpc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSpcToSpc.CheckedChanged
        If radSpcToSpc.Checked = True Then
            lnkSearch.Visible = False
            pnlEmp.Visible = True
            pnlEmplst.Visible = False
            pnlForemp.Visible = False
            pnlFornonemp.Visible = False
            Panel1.Visible = False


        End If
    End Sub

    Private Sub radSpcToStore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSpcToStore.CheckedChanged
        If radSpcToStore.Checked = True Then
            lnkSearch.Visible = False
            pnlEmp.Visible = True
            pnlEmplst.Visible = False
            pnlForemp.Visible = False
            pnlFornonemp.Visible = False
            Panel1.Visible = True
            'strSQL = "SELECT DEP_CODE,(DEP_CODE+'/'+DEP_NAME)AS DEP_NAME FROM  " & Session("TENANT") & "."  & "DEPARTMENT"
            cboDept.Items.Clear()
            'BindCombo(strSQL, cboDept, "DEP_NAME", "DEP_CODE")
            cboDept.Items.Add("CPA")
            If cboDept.Items.Count > 0 Then
                cboDept.Visible = True
            End If

        End If
    End Sub
    Private Sub btnSearchemp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchemp.Click
        visibleTF(0)
        If cbocat.SelectedItem.Text = "--Select--" Then
            PopUpMessage("Please Select Criteria", Me.Page)
            Exit Sub
        ElseIf txtSearch.Text = "" Then
            PopUpMessage("Please Select " & lblCriteria.Text, Me.Page)
            Exit Sub
        End If

        '---------------------------- message and check for the

        Dim arParms1() As SqlParameter = New SqlParameter(3) {}
        If cbocat.SelectedItem.Value = "Location" Then
            arParms1(0) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
            arParms1(0).Value = 1
        ElseIf cbocat.SelectedItem.Value = "Employee ID" Then
            arParms1(0) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
            arParms1(0).Value = 2
        ElseIf cbocat.SelectedItem.Value = "Employee Name" Then
            arParms1(0) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
            arParms1(0).Value = 3
        End If
        arParms1(1) = New SqlParameter("@PERM1", SqlDbType.NVarChar, Len(txtSearch.Text.Trim))
        arParms1(1).Value = txtSearch.Text.Trim
        arParms1(2) = New SqlParameter("@COUT", SqlDbType.NVarChar, 2000)
        arParms1(2).Direction = ParameterDirection.Output
        arParms1(3) = New SqlParameter("@AUR_TYPE", SqlDbType.NVarChar, 100)
        If radSpcToSpc.Checked = True Then
            arParms1(3).Value = "Employee"
        ElseIf radSpcToStore.Checked = True Then
            arParms1(3).Value = "Probationer"
        End If


        Dim msg As Integer

        msg = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "SPACE_REQ_USER_CHECK_SP", arParms1)
        If arParms1(2).Value.ToString <> "NA" Then
            'PopUpMessage(cbocat.SelectedItem.Text.ToString() & " does not exist!", Me.Page)
            PopUpMessage(arParms1(2).Value.ToString, Me.Page)
            Exit Sub
        End If


        '----------------------------

        Dim arParms() As SqlParameter = New SqlParameter(2) {}
        If cbocat.SelectedItem.Value = "Location" Then
            arParms(0) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
            arParms(0).Value = 1
        ElseIf cbocat.SelectedItem.Value = "Employee ID" Then
            arParms(0) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
            arParms(0).Value = 2
        ElseIf cbocat.SelectedItem.Value = "Employee Name" Then
            arParms(0) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
            arParms(0).Value = 3
        End If
        arParms(1) = New SqlParameter("@PERM1", SqlDbType.NVarChar, Len(txtSearch.Text.Trim))
        arParms(1).Value = txtSearch.Text.Trim
        arParms(2) = New SqlParameter("@AUR_TYPE", SqlDbType.NVarChar, 100)
        If radSpcToSpc.Checked = True Then
            arParms(2).Value = "Employee"
        ElseIf radSpcToStore.Checked = True Then
            arParms(2).Value = "Probationer"
        End If
        'arParms(2) = New SqlParameter("@PERM1", SqlDbType.NVarChar, Len(txtSearch.Text.Trim))
        'arParms(2).Value = txtSearch.Text.Trim
        lstEmp.DataSource = SqlHelper.ExecuteReader(CommandType.StoredProcedure, Session("TENANT") & "."  & "SPACE_REQ_USER_SP", arParms)

        lstEmp.DataTextField = "ENAME"
        lstEmp.DataValueField = "AUR_ID"
        lstEmp.DataBind()

        If lstEmp.Items.Count > 0 Then
            pnlEmplst.Visible = True
        Else
            pnlEmplst.Visible = False
            PopUpMessage("Employee(s) Not Found", Me.Page)
        End If
        If dgEmpMap.Rows.Count > 0 Then

            For i As Integer = 0 To dgEmpMap.Rows.Count - 1
                'Dim chk As CheckBox = CType(dgEmpMap.Rows(i).FindControl("chkSelect"), CheckBox)
                ' If chk.Checked = True Then
                If dgEmpMap.Rows(i).Visible = True Then
                    Dim K
                    For K = lstSelemp.Items.Count() - 1 To 0 Step K - 1
                        If lstSelemp.Items(K).Value = dgEmpMap.Rows(i).Cells(7).Text.Trim Then
                            lstSelemp.Items.Add(lstEmp.Items.FindByValue(dgEmpMap.Rows(i).Cells(7).Text.Trim))
                            lstEmp.Items.Remove(lstEmp.Items.FindByValue(dgEmpMap.Rows(i).Cells(7).Text.Trim))
                        End If
                    Next
                End If
            Next

        End If

    End Sub

    Private Sub btnForpmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnForpmp.Click
        Dim j As Integer
        Dim varLOC As String
        For j = 0 To dgEmpMap.Rows.Count - 1
            Dim chk As CheckBox = CType(dgEmpMap.Rows(j).FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                varLOC = dgEmpMap.Rows(j).Cells(10).Text
                If varLOC = txtTEmpLocation.Text Or txtTEmpLocation.Text = "" Then
                    txtTEmpLocation.Text = dgEmpMap.Rows(j).Cells(10).Text
                    txtdept.Text = dgEmpMap.Rows(j).Cells(9).Text
                Else
                    PopUpMessage("Please Select the Employee(s) of Same Location", Me.Page)
                    Exit Sub
                End If
            End If
        Next

        visibleTF(1)
        addFromGrid()
        If txtWst.Text = 0 And txtHalfcab.Text = 0 And txtFullcab.Text = 0 Then
            PopUpMessage("Please Select the Employee(s)", Me.Page)
            Exit Sub
        End If
       

        Dim varBGTID As String = txtTEmpLocation.Text
        Dim varStatusId As Integer = 5
        Dim RID As String = Session("RID")
        Dim depid As String = txtdept.Text

     


        Dim strReqId As String = INSERTSPACEREQUISITION_Values(ddlTower1.SelectedValue, depid, txtHalfcab.Text, txtFullcab.Text, txtWst.Text, Page, CType(txtFrmDt.Text, Date), CType(txtToDt.Text, Date), txtRem.Text, varBGTID)
        'sendMail(strReqId, txtFromdate.Text, txtTodate.Text, txtCity1.Text, ddlTower1.SelectedItem.Text, txtWstations.Text, txtHcabins.Text, txtFcabins.Text, ddlProject.SelectedItem.Text)
        'cleardata()
        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("5") & "&rid=" & clsSecurity.Encrypt(strReqId)
        visibleTF(0)
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub
    Public Function INSERTSPACEREQUISITION_Values(ByVal tower As String, ByVal ddldept As String, ByVal cubical As Integer, ByVal cabins As Integer, ByVal workstations As Integer, ByVal page As Page, ByVal txtfromdate As Date, ByVal txttodate As Date, ByVal txtrem As String, ByVal strBdg As String) As String
        Dim obj As New clsWorkSpace
        Dim sta As Integer = 5
        Dim userid As String = page.Session("uid")
        Dim REQID As String = obj.getNumber(page)
        Dim sp1 As New SqlParameter("@vc_reqid", SqlDbType.NVarChar, 50)
        sp1.Value = REQID
        Dim sp2 As New SqlParameter("@vc_fromdate", SqlDbType.DateTime)
        sp2.Value = txtfromdate
        Dim sp3 As New SqlParameter("@vc_todate", SqlDbType.DateTime)
        sp3.Value = txttodate
        Dim sp4 As New SqlParameter("@vc_status", SqlDbType.NVarChar, 10)
        sp4.Value = sta
        Dim sp5 As New SqlParameter("@vc_user", SqlDbType.NVarChar, 50)
        sp5.Value = userid
        Dim sp6 As New SqlParameter("@vc_tower", SqlDbType.NVarChar, 50)
        sp6.Value = tower
        Dim sp7 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp7.Value = txtrem
        Dim sp8 As New SqlParameter("@vc_vertical", SqlDbType.NVarChar, 50)
        sp8.Value = ""
        Dim sp9 As New SqlParameter("@vc_proj", SqlDbType.NVarChar, 50)
        sp9.Value = ""
        Dim sp10 As New SqlParameter("@vc_dept", SqlDbType.NVarChar, 50)
        sp10.Value = ddldept
        Dim sp11 As New SqlParameter("@vc_bdg", SqlDbType.NVarChar, 50)
        sp11.Value = strBdg
        Dim sp12 As New SqlParameter("@cubicals", SqlDbType.Int)
        sp12.Value = cubical
        Dim sp13 As New SqlParameter("@cabins", SqlDbType.Int)
        sp13.Value = cabins
        Dim sp14 As New SqlParameter("@WorkStations", SqlDbType.Int)
        sp14.Value = workstations
        Dim sp15 As New SqlParameter("@COSTCENTER", SqlDbType.NVarChar, 150)
        sp15.Value = ""
        Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12, sp13, sp14, sp15}
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertSpaceDetails", parms)


        Return REQID
    End Function
    Private Sub lnkSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSearch.Click
        If radSpcToSpc.Checked = False And radSpcToStore.Checked = False Then
            PopUpMessage("Select Employee or Non Employee First", Me.Page)
        Else
            pnlEmp.Visible = True
            pnlEmplst.Visible = True

        End If

    End Sub



    Private Sub cbocat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbocat.SelectedIndexChanged
        txtSearch.Text = ""
        If cbocat.SelectedItem.Text <> "--Select--" Then
            lblCriteria.Text = cbocat.SelectedItem.Text
            txtSearch.ReadOnly = False
        Else
            txtSearch.Text = ""
            lstEmp.Items.Clear()
            lstSelemp.Items.Clear()
            txtSearch.ReadOnly = True
            lblCriteria.Text = "Search Criteria"
        End If

    End Sub



    Private Sub btnRight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRight.Click
        If lstEmp.SelectedIndex > -1 Then
            Dim i
            For i = 0 To lstEmp.Items.Count() - 1
                If lstEmp.Items(i).Selected = True Then
                    lstSelemp.Items.Add(lstEmp.Items(i))
                End If
            Next i
            For i = 0 To lstSelemp.Items.Count() - 1
                If lstSelemp.Items(i).Selected = True Then
                    lstEmp.Items.Remove(lstSelemp.Items(i))
                End If
            Next i
        End If
        dgEmpMap.Visible = False
        Panel1.Visible = True
    End Sub

    Private Sub btnLeft_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLeft.Click
        If lstSelemp.SelectedIndex > -1 Then
            Dim i
            For i = 0 To lstSelemp.Items.Count() - 1
                If lstSelemp.Items(i).Selected = True Then
                    lstEmp.Items.Add(lstSelemp.Items(i))
                End If
            Next i
            For i = 0 To lstEmp.Items.Count() - 1
                If lstEmp.Items(i).Selected = True Then
                    lstSelemp.Items.Remove(lstEmp.Items(i))
                End If
            Next i
        End If
        dgEmpMap.Visible = False
        Panel1.Visible = True
    End Sub

    Private Sub btnAddSelected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddSelected.Click
        ' If radSpcToSpc.Checked = True Then
        txtSearch.Text = ""
        If lstSelemp.Items.Count > 0 Then

            strSQL = ""
            For i As Integer = 0 To lstSelemp.Items.Count - 1
                strSQL = "'" & lstSelemp.Items(i).Text.Substring(lstSelemp.Items(i).Text.IndexOf("/") + 1).ToString & "'," & strSQL
            Next
            If Session("tempid") <> "" Then
            Else
                Session("tempid") = "'0'"
            End If
            strSQL = strSQL.Remove(strSQL.Length - 1, 1)

            visibleTF(1) 'to set visibility property of the required columns of the grid
            strSQL1 = "select  distinct AUR_ID,AUR_KNOWN_AS ENAME,DEP_CODE,DEP_NAME DEPARTMENT, AUR_DESGN_ID JOB,AUR_TYPE GRADE,AUR_LOCATION,lcm_code,lcm_name AS LOCATION FROM " & Session("TENANT") & "."  & "AMANTRA_USER inner join " & Session("TENANT") & "."  & " DEPARTMENT on " & Session("TENANT") & "."  & " AMANTRA_USER.AUR_DEP_ID= " & Session("TENANT") & "."  & " DEPARTMENT.DEP_CODE inner join " & Session("TENANT") & "."  & " location on aur_location=lcm_code where AUR_ID in (" & strSQL & "," & Session("tempid") & ")"
            BindGridSet(strSQL1, dgEmpMap)
            Dim tempid As String = "'0'"
            Session("tempid") = "'0'"

            If dgEmpMap.Rows.Count > 0 Then
                dgEmpMap.Visible = True
            Else
                dgEmpMap.Visible = False
            End If
            btnSubmit.Visible = True
            btnCancel.Visible = True
            visibleTF(0)
        Else
            visibleTF(0)
            dgEmpMap.Visible = False
            PopUpMessage("Please Select the Employee(s)", Me.Page)

        End If

        'End If
        'If radSpcToStore.Checked = True Then
        '    If lstSelemp.Items.Count > 0 Then

        '        strSQL = ""
        '        For i As Integer = 0 To lstSelemp.Items.Count - 1
        '            strSQL = "'" & lstSelemp.Items(i).Text.Substring(lstSelemp.Items(i).Text.IndexOf("/") + 1).ToString & "'," & strSQL
        '        Next
        '        If Session("tempid") <> "" Then
        '        Else
        '            Session("tempid") = "'0'"
        '        End If
        '        strSQL = strSQL.Remove(strSQL.Length - 1, 1)

        '        visibleTF(1) 'to set visibility property of the required columns of the grid
        '        strSQL1 = "select  distinct AUR_ID,AUR_KNOWN_AS ENAME,DEP_CODE,DEP_NAME DEPARTMENT, AUR_DESGN_ID JOB,AUR_TYPE GRADE,AUR_LOCATION,lcm_code,lcm_name AS LOCATION FROM " & Session("TENANT") & "."  & "AMANTRA_USER inner join " & Session("TENANT") & "."  & " DEPARTMENT on " & Session("TENANT") & "."  & " AMANTRA_USER.AUR_DEP_ID= " & Session("TENANT") & "."  & " DEPARTMENT.DEP_CODE inner join " & Session("TENANT") & "."  & " location on aur_location=lcm_code where AUR_ID in (" & strSQL & "," & Session("tempid") & ")"
        '        BindGridSet(strSQL1, GridView1)
        '        Dim tempid As String = "'0'"
        '        Session("tempid") = "'0'"

        '        If GridView1.Rows.Count > 0 Then
        '            GridView1.Visible = True
        '        Else
        '            GridView1.Visible = False
        '        End If
        '        Button1.Visible = True
        '        Button2.Visible = True
        '        visibleTF(0)
        '        pnlFornonemp.Visible = True
        '    Else
        '        visibleTF(0)
        '        GridView1.Visible = False
        '        PopUpMessage("Please Select the Employee(s)", Me.Page)

        '    End If
        'End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        visibleTF(1)
        ' cbocat.SelectedIndex = 0
        'txtSearch.Text = ""
        'lstEmp.Items.Clear()
        'lstSelemp.Items.Clear()
        txtSearch.ReadOnly = True
        lblCriteria.Text = "Search Criteria"

        dgEmpMap.Visible = True
        btnSubmit.Visible = True
        btnCancel.Visible = True
        pnlForemp.Visible = False
        pnlEmplst.Visible = True

        txtWst.Text = "0"
        txtHalfcab.Text = "0"
        txtFullcab.Text = "0"
        'txtManagertab.Text = "0"
        txtFromDate.Text = ""
        txtTodate.Text = ""
        txtPAddress.Text = ""
        If dgEmpMap.Rows.Count > 0 Then

            For i As Integer = 0 To dgEmpMap.Rows.Count - 1
                Dim chk As CheckBox = CType(dgEmpMap.Rows(i).FindControl("chkSelect"), CheckBox)
                If chk.Checked = True Then
                    'lstEmp.Items.Add(dgEmpMap.Rows(i).Cells(1).Text + "/" + dgEmpMap.Rows(i).Cells(7).Text)
                    chk.Checked = False
                    Dim K As Integer
                    For K = 0 To lstSelemp.Items.Count() - 1
                        If lstSelemp.Items(K).Value = dgEmpMap.Rows(i).Cells(7).Text.Trim Then
                            'lstEmp.Items.Add(lstSelemp.Items(K))
                            'lstSelemp.Items.Remove(lstSelemp.Items(K))
                            lstEmp.Items.Add(lstSelemp.Items.FindByValue(dgEmpMap.Rows(i).Cells(7).Text.Trim))
                            lstSelemp.Items.Remove(lstSelemp.Items.FindByValue(dgEmpMap.Rows(i).Cells(7).Text.Trim))
                            Exit For
                        End If
                    Next
                    ' For K = 0 To lstEmp.Items.Count() - 1
                    'If lstEmp.Items(K).Selected = True Then
                    'lstSelemp.Items.Remove(lstEmp.Items(i))
                    ' End If
                    '  Next K
                    dgEmpMap.Rows(i).Visible = False

                End If
            Next

            Dim tempid As String = "'0'"

            If dgEmpMap.Rows.Count > 0 Then
                For i As Integer = 0 To dgEmpMap.Rows.Count - 1
                    If dgEmpMap.Rows(i).Visible = True Then
                        tempid = tempid & ",'" & dgEmpMap.Rows(i).Cells(7).Text() & "'"
                    End If
                Next
                Session("tempid") = tempid
            End If

            'strSQL = strSQL.Remove(strSQL.Length - 1, 1)
        Else
            PopUpMessage("Please Select the Employee(s)", Me.Page)
            Exit Sub
        End If
        visibleTF(0)
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        pnlForemp.Visible = True
        visibleTF(1)
        addFromGrid()
        visibleTF(0)
    End Sub
    Private Sub addFromGrid()
        If radSpcToSpc.Checked = True Then
            pnlForemp.Visible = True
            pnlFornonemp.Visible = False
        Else
            pnlForemp.Visible = False
            pnlFornonemp.Visible = True
        End If
        'pnlForemp.Visible = True
        Dim Wst, Hc, Fc, MT As Integer

        Dim varLOC As String
        txtTEmpLocation.Text = ""
        txtdept.Text = ""
        For i As Integer = 0 To dgEmpMap.Rows.Count - 1
            Dim chk As CheckBox = CType(dgEmpMap.Rows(i).FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                varLOC = dgEmpMap.Rows(i).Cells(10).Text
                If varLOC = txtTEmpLocation.Text Or txtTEmpLocation.Text = "" Then
                    txtTEmpLocation.Text = dgEmpMap.Rows(i).Cells(10).Text
                    txtdept.Text = dgEmpMap.Rows(i).Cells(9).Text
                Else
                    PopUpMessage("Please Select the Employee(s) of Same Location", Me.Page)
                    Exit Sub
                End If
            End If
        Next
        For i As Integer = 0 To dgEmpMap.Rows.Count - 1
            Dim chk As CheckBox = CType(dgEmpMap.Rows(i).FindControl("chkSelect"), CheckBox)
            Dim ddlSpcType As DropDownList = CType(dgEmpMap.Rows(i).FindControl("ddlSpcType"), DropDownList)

            If chk.Checked = True And dgEmpMap.Rows(i).Visible = True Then
                If ddlSpcType.SelectedItem.Text.Trim = "WORK STATION" Then
                    Wst += 1
                ElseIf ddlSpcType.SelectedItem.Text.Trim = "HALF CABIN" Then
                    Hc += 1
                ElseIf ddlSpcType.SelectedItem.Text.Trim = "FULL CABIN" Then
                    Fc += 1
                    'ElseIf ddlSpcType.SelectedItem.Text.Trim = "MANAGER TABLE" Then
                    '    MT += 1
                End If
            End If
        Next
        txtWst.Text = Wst
        txtHalfcab.Text = Hc
        txtFullcab.Text = Fc
        ' txtManagertab.Text = MT
        'txtFromDate.Text = getoffsetdatetime(DateTime.Now)
        'txtToDt.Text = getoffsetdatetime(DateTime.Now)
        txtRem.Text = "none"
        If txtWst.Text = 0 And txtHalfcab.Text = 0 And txtFullcab.Text = 0 Then
            pnlForemp.Visible = False
            PopUpMessage("Please Select the Employee(s)", Me.Page)
            Exit Sub
        End If

    End Sub

    Private Sub btnFornonemp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFornonemp.Click

        Dim i As String

        'strSQL = "select isnull(lcm_CODE,'0') lcm_code from " & Session("TENANT") & "."  & "location where lcm_code='" & txtLoc.Text & "'"

        strSQL = "select count(lcm_code) from " & Session("TENANT") & "."  & "location where lcm_code='" & ddlloc1.SelectedValue & "'"

        If SqlHelper.ExecuteScalar(CommandType.Text, strSQL) > 0 Then
        Else
            PopUpMessage("Please Enter Proper Location Code", Me.Page)
            Exit Sub
        End If

        strSQL = "select isnull(lcm_CODE,'0') lcm_code from " & Session("TENANT") & "."  & "location where lcm_code='" & ddlloc1.SelectedValue & "'"
        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        While ObjDR.Read
            i = ObjDR("lcm_code")
        End While
        If i <> "0" Or i <> "" Then
            txtTEmpLocation.Text = i
        Else
            PopUpMessage("Please Enter Proper Location Code", Me.Page)
            Exit Sub
        End If

      
        Dim RID As String = Session("RID")

        'End If
        Dim Halfcabin As String = "0"
        Dim fullcabin As String = "0"
        Dim Wst As String = "0"
        If cboSpcTypenonemp.SelectedValue = "WORK STATION" Then
            Wst = 1
        ElseIf cboSpcTypenonemp.SelectedValue = "FULL CABIN" Then
            fullcabin = 1
        ElseIf cboSpcTypenonemp.SelectedValue = "HALF CABIN" Then
            Halfcabin = 1

        End If
        Dim strReqId As String = INSERTSPACEREQUISITION_Values(ddlTower1.SelectedValue, cboDept.SelectedItem.Value, Halfcabin, fullcabin, Wst, Page, CType(txtFromdate.Text, Date), CType(txtTodate.Text, Date), txtRem.Text, "")

        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    Private Sub grd_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim i As Integer = 0
            If chk.Checked = True Then
                For i = 0 To dgEmpMap.Rows.Count - 1
                    Dim chkselect As CheckBox = CType(dgEmpMap.Rows(i).FindControl("chkselect"), CheckBox)
                    If dgEmpMap.Rows(i).Visible = True Then
                        chkselect.Checked = True
                    Else
                        chkselect.Checked = False
                    End If
                Next
                chk.Checked = True
            Else
                For i = 0 To dgEmpMap.Rows.Count - 1
                    Dim chkselect As CheckBox = CType(dgEmpMap.Rows(i).FindControl("chkselect"), CheckBox)
                    If dgEmpMap.Rows(i).Visible = True Then
                        chkselect.Checked = False
                    Else
                        chkselect.Checked = False
                    End If
                Next
                chk.Checked = False
            End If
        Catch ex As Exception
            '   ErrorLog.LogError(ex)

        End Try
    End Sub


    Protected Sub dgEmpMap_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgEmpMap.RowCreated
        Try
            If e.Row.RowType = ListItemType.Header Then
                AddHandler CType(e.Row.FindControl("cbSelAll"), CheckBox).CheckedChanged, AddressOf grd_CheckedChanged
            End If
        Catch ex As Exception

            '  ErrorLog.LogError(ex)

        End Try
    End Sub

    Protected Sub ddlLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLoc.SelectedIndexChanged
        ddlTower1.Items.Clear()
        txtCity1.Text = ""
        txtCountry1.Text = ""
        txtLocation1.Text = ""
        Try
            If ddlLoc.SelectedIndex <> 0 Then
                obj.loadtower1(ddlTower1, ddlCity.SelectedValue.ToString().Trim(), ddlLoc.SelectedValue.ToString().Trim())
                'HiddenField1.Value = ddlLoc.SelectedValue
                If ddlLoc.Items.Count = 2 Then
                    ddlLoc.SelectedIndex = 1
                    'ddlTower1_SelectedIndexChanged(sender, e)
                End If
                If ddlTower1.Items.Count = 1 Then
                    lblMsg.Text = "No towers in this Location"
                    lblMsg.Visible = True
                End If
                'If (ddlVertical.SelectedIndex > 0 And ddlLoc.SelectedIndex > 0) Then
                '    loadgrid()
                'End If
            Else
                lblMsg.Text = "Please Select Location"
                lblMsg.Visible = True
            End If
            Panel1.Visible = True
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Requisition", "Load", ex)

        End Try

    End Sub

End Class
