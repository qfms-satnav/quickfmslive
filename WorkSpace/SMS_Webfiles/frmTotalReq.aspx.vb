Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmTotalReq
    Inherits System.Web.UI.Page
    Dim obj As New clsReports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""
        Try
            If Not Page.IsPostBack Then
                obj.bindVertical(ddlReqID)
                ddlReqID.Items.Insert(1, "--All Verticals--")
                trOnDate.Visible = False
                trFromDate.Visible = False
                trToDate.Visible = False
                btnExport.Visible = False
                btnViewCriteria.Visible = False
                btnPrint.Visible = False
                txtOnDate.Text = getoffsetdatetime(DateTime.Now)
                gvExcel.Visible = False
                txtFromDate.Attributes.Add("onClick", "displayDatePicker('" + txtFromDate.ClientID + "')")
                txtFromDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
                txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                txtOnDate.Attributes.Add("onClick", "displayDatePicker('" + txtOnDate.ClientID + "')")
                txtOnDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repVerticalwiseRequisition", "Page_Load", exp)
        End Try
    End Sub
    Protected Sub ddlCriteria_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCriteria.SelectedIndexChanged
        Try
            If ddlCriteria.SelectedValue = 0 Then
                trOnDate.Visible = False
                trFromDate.Visible = False
                trToDate.Visible = False
                txtOnDate.Text = ""
                txtFromDate.Text = ""
                txtToDate.Text = ""
            ElseIf ddlCriteria.SelectedValue = 2 Then
                trOnDate.Visible = True
                trFromDate.Visible = False
                trToDate.Visible = False
                btnViewReport.Visible = True

                txtFromDate.Text = ""
                txtToDate.Text = ""
            ElseIf ddlCriteria.SelectedValue = 1 Then
                trOnDate.Visible = False
                trFromDate.Visible = True
                trToDate.Visible = True
                btnViewReport.Visible = True
                txtOnDate.Text = ""

            End If
            txtOnDate.Text = getoffsetdatetime(DateTime.Now)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repVerticalwiseRequisition", "Page_Load", exp)
        End Try
    End Sub
    Dim dt As DataTable
    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        '        -- 1 all verticals on date 
        '-- 2 all verticals between dates 
        '-- 3 selected vertical ondate
        '-- 4 selected vertical between dates 
        '--EXEC USP_VERTICAL_REQUISITIONS 4,'09/8/2008','09/18/2008','amt'

        Try
            If ddlReqID.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Please select the Vertical "
                Exit Sub
            End If
            If ddlCriteria.SelectedItem.Value = 0 Then
                lblMsg.Text = "Please select the Criteria to view the report"
                Exit Sub
            End If
            Dim spID As SqlParameter = New SqlParameter("@CHECKID", SqlDbType.Int)
            Dim spFromDate As SqlParameter = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            Dim spToDate As SqlParameter = New SqlParameter("@TODATE", SqlDbType.DateTime)
            Dim spVertical As SqlParameter = New SqlParameter("@VC_VERTICAL_NAME", SqlDbType.NVarChar, 50)
            If ddlCriteria.SelectedValue = 1 Then
                If txtFromDate.Text = String.Empty Or txtToDate.Text = String.Empty Then
                    lblMsg.Text = "Select the Dates"
                    Exit Sub
                End If
                Dim frmdt As Date
                Dim todt As Date
                Try
                    frmdt = txtFromDate.Text
                Catch ex As Exception
                    lblMsg.Text = "Please enter valid From Date"
                    Exit Sub
                End Try
                Try
                    todt = txtToDate.Text
                Catch ex As Exception
                    lblMsg.Text = "Please enter valid To Date"
                    Exit Sub
                End Try
                If frmdt > todt Then
                    lblMsg.Text = "From date should be less than To date"
                    Exit Sub
                End If


                If ddlReqID.SelectedValue = "--All Verticals--" Then
                    spID.Value = 2
                    spFromDate.Value = frmdt
                    spToDate.Value = todt
                    spVertical.Value = "All"
                    dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_VERTICAL_REQUISITIONS", spID, spFromDate, spToDate, spVertical)
                    gvSpaceExtend.DataSource = dt
                    gvSpaceExtend.DataBind()
                    gvExcel.DataSource = dt
                    gvExcel.DataBind()
                    cleardata()
                Else
                    spID.Value = 4
                    spFromDate.Value = frmdt
                    spToDate.Value = todt
                    spVertical.Value = ddlReqID.SelectedValue.Trim()
                    dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_VERTICAL_REQUISITIONS", spID, spFromDate, spToDate, spVertical)
                    gvSpaceExtend.DataSource = dt
                    gvSpaceExtend.DataBind()
                    gvExcel.DataSource = dt
                    gvExcel.DataBind()
                    cleardata()
                End If

            ElseIf ddlCriteria.SelectedValue = 2 Then
                If txtOnDate.Text = String.Empty Then
                    lblMsg.Text = "Select the Valid date"
                    Exit Sub
                End If
                Dim ondt As Date
                Try
                    ondt = txtOnDate.Text
                Catch ex As Exception
                    lblMsg.Text = "Please enter valid Date"
                    Exit Sub
                End Try
                If ddlReqID.SelectedValue = "--All Verticals--" Then
                    spID.Value = 1
                    spFromDate.Value = ondt
                    spToDate.Value = getoffsetdatetime(DateTime.Now)
                    spVertical.Value = "All"
                    dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_VERTICAL_REQUISITIONS", spID, spFromDate, spToDate, spVertical)
                    gvSpaceExtend.DataSource = dt
                    gvSpaceExtend.DataBind()
                    gvExcel.DataSource = dt
                    gvExcel.DataBind()
                    'obj.binddata_verticalwise(gvSpaceExtend, 3, ddlReqID.SelectedValue, 7, ondt, getoffsetdatetime(DateTime.Now))
                    cleardata()
                Else
                    spID.Value = 3
                    spFromDate.Value = ondt
                    spToDate.Value = getoffsetdatetime(DateTime.Now)
                    spVertical.Value = ddlReqID.SelectedValue.Trim()
                    dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_VERTICAL_REQUISITIONS", spID, spFromDate, spToDate, spVertical)
                    gvSpaceExtend.DataSource = dt
                    gvSpaceExtend.DataBind()
                    gvExcel.DataSource = dt
                    gvExcel.DataBind()
                    'obj.binddata_verticalwise(gvSpaceExtend, 2, ddlReqID.SelectedValue, 7, ondt, getoffsetdatetime(DateTime.Now))
                    cleardata()
                End If

            End If

            If gvSpaceExtend.Rows.Count = 0 Then
                trOnDate.Visible = False
                trFromDate.Visible = False
                trOnDate.Visible = False
                ddlCriteria_SelectedIndexChanged(sender, e)
                lblMsg.Text = "Data found with the selection criteria"
                Exit Sub
            Else
                btnViewReport.Visible = False
                tblDetails.Visible = False
                btnExport.Visible = True
                btnViewCriteria.Visible = True
                btnPrint.Visible = True
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repVerticalwiseRequisition", "btnViewReport_Click", exp)
        End Try
    End Sub

    Protected Sub btnViewCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            btnViewReport.Visible = True
            btnExport.Visible = False
            btnViewCriteria.Visible = False
            btnPrint.Visible = False
            'gvSpaceExtend.Visible = False
            gvSpaceExtend.DataSource = Nothing
            gvSpaceExtend.DataBind()
            cleardata()
            gvExcel.Visible = False
            trFromDate.Visible = False
            trToDate.Visible = False
            trOnDate.Visible = False
            tblDetails.Visible = True
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repVerticalwiseRequisition", "btnViewCriteria_Click", exp)
        End Try
    End Sub
    Public Sub cleardata()
        ddlCriteria.SelectedValue = 0
        ddlReqID.SelectedIndex = 0
        txtFromDate.Text = String.Empty
        txtToDate.Text = String.Empty
        txtOnDate.Text = String.Empty
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
            gvExcel.Visible = True
            gvSpaceExtend.Visible = False
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repVerticalwiseRequisition", "btnExport_Click", exp)
        End Try
    End Sub
    Protected Sub gvSpaceExtend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpaceExtend.PageIndexChanging
        Try
            gvSpaceExtend.PageIndex = e.NewPageIndex
            btnViewReport_Click(sender, e)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repVerticalwiseRequisition", "gvSpaceExtend_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub ddlReqID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqID.SelectedIndexChanged
        Try
            If ddlReqID.SelectedIndex = 0 Then
                cleardata()
                trFromDate.Visible = False
                trToDate.Visible = False
                trOnDate.Visible = False
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "repVerticalwiseRequisition", "ddlReqID_SelectedIndexChanged", exp)
        End Try
    End Sub
End Class
