﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web

Partial Class WorkSpace_SMS_Webfiles_frmConference_finalpage
    Inherits System.Web.UI.Page
    Dim rid As String = String.Empty
    Dim UID, reqid As String
    Dim FROMEmail, TOEmail, msg, subj, cc, mailFLAG, UID_NAME, location, loc_type, capacity, STE_CODE, STE_NAME As String


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        rid = clsSecurity.Decrypt(Request.QueryString("rid"))
        UID = Session("uid")
        reqid = clsSecurity.Decrypt(Request.QueryString("reqid"))
        lbl1.Text = lbl1.Text & "<body onLoad=""history.go(+1)"">"
        Dim strSta As String = clsSecurity.Decrypt(Request.QueryString("sta"))

        If strSta = "2" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Conference Room(s) has been Booked By " & UID & "  Successfully.<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "3" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Conference Room(s) has been Modified By " & UID & "  Successfully.<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "4" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Conference Room(s) has been Canceled By " & UID & " Successfully.<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "5" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Conference Room(s) has been Modified By " & UID & " Successfully.<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "6" Then
            lbl1.Text = "<b><center><br><br><br>Conference Room(s) has been Withheld By " & UID & " Successfully.<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "7" Then
            lbl1.Text = "<b><center><br><br><br>Withheld Conference Room(s) has been Modified By " & UID & " Successfully.<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "8" Then
            lbl1.Text = "<b><center><br><br> <br> Withheld Conference Room(s) has been Released By " & UID & " Successfully.<br><br>Thank You for using the system.</center>"


        End If


    End Sub
End Class
