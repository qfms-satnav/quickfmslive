Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmReleaseRequisition
    Inherits System.Web.UI.Page
    Dim strSQL As String
    Dim varrid As String
    Dim varUID As String
    Dim iVar As Integer = 0
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            Dim timeOut As Integer
            timeOut = Server.ScriptTimeout
            Server.ScriptTimeout = 90000000


            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If

            If Not Page.IsPostBack Then
                visibleTF(1)
                BindData1()
                visibleTF(0)
            End If
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            ' fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Public Function CHECK_ADMIN_ROLE(ByVal UID As String) As Integer

        Dim objdata As SqlDataReader
        Dim strSQL As String
        strSQL = "SELECT URL_USR_ID,ROL_ACRONYM,URL_SCOPEMAP_ID " & _
          "FROM  " & Session("TENANT") & "."  & "USER_ROLE, " & Session("TENANT") & "."  & "ROLE " & _
          "WHERE URL_ROL_ID=ROL_ID AND ROL_ACRONYM IN ('GADMIN','NADMIN') AND URL_USR_ID = '" & UID & "' ORDER BY ROL_ACRONYM"
        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        Dim locs As String = "'0'"
        Dim rol As String
        Dim userid As String
        While objdata.Read
            locs = locs & ",'" & objdata("URL_scopemap_ID").ToString & "'"
            userid = objdata("url_usr_id").ToString
            If rol = "" Then
                rol = objdata("rol_acronym").ToString
            Else
                rol = rol + "," + objdata("rol_acronym").ToString
            End If
        End While
        objdata.Close()

        If UCase(rol) = "GADMIN" Or UCase(rol) = "NADMIN" Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Public Function CHECK_ANY_ROLE(ByVal UID As String, ByVal ROLE As String) As Integer

        Dim objdata As SqlDataReader
        Dim strSQL As String
        strSQL = "SELECT URL_USR_ID,ROL_ACRONYM,URL_SCOPEMAP_ID " & _
          "FROM  " & Session("TENANT") & "."  & "USER_ROLE, " & Session("TENANT") & "."  & "ROLE " & _
          "WHERE URL_ROL_ID=ROL_ID AND ROL_ACRONYM IN ('" & ROLE & "') AND URL_USR_ID = '" & UID & "' ORDER BY ROL_ACRONYM"
        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        Dim locs As String = "'0'"
        Dim rol As String
        Dim userid As String
        While objdata.Read
            locs = locs & ",'" & objdata("URL_scopemap_ID").ToString & "'"
            userid = objdata("url_usr_id").ToString
            If rol = "" Then
                rol = objdata("rol_acronym").ToString
            Else
                rol = rol + "," + objdata("rol_acronym").ToString
            End If
        End While
        objdata.Close()

        If UCase(rol) = UCase(ROLE) Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Private Sub BindData1()
        If CHECK_ADMIN_ROLE(Session("UID")) Then
            strSQL1 = "SELECT DISTINCT LCM_CODE,LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION, " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW " & _
                             " WHERE SSA_STA_ID in (6,14) and AUR_ID in " & _
                             " (select distinct top 1  url_usr_id from  " & Session("TENANT") & "."  & "user_role, " & Session("TENANT") & "."  & "role,  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW,  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & _
                             "  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where url_rol_id=rol_id  and rol_acronym =('GAdmin') " & _
                             "  and url_scopemap_id=21) AND SSA_BDG_ID=LCM_code and SSA_SRNREQ_ID=SRN_req_id  ORDER BY LCM_NAME"


        ElseIf CHECK_ANY_ROLE(Session("UID"), "HR") Or CHECK_ANY_ROLE(Session("UID"), "ADMIN") Then

            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
            arParms(0).Value = Session("uid")
            arParms(1) = New SqlParameter("@TYPE", SqlDbType.NVarChar, 10)
            arParms(1).Value = "LOC"

            Dim STR As String = ""
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, Session("TENANT") & "."  & "ADMIN_CREATE", arParms)
            While ObjDR.Read
                If STR = "" Then
                    STR = "'" & ObjDR("CODE") & "'"
                Else
                    STR = STR & ",'" & ObjDR("CODE") & "'"
                End If

            End While
            strSQL1 = "SELECT DISTINCT LCM_CODE,LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION, " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW " & _
                                          " WHERE SSA_STA_ID in (6,14) and LCM_CODE in (" & STR & ") AND SSA_BDG_ID=LCM_code and SSA_SRNREQ_ID=SRN_req_id ORDER BY LCM_NAME"
        Else

            strSQL1 = "SELECT DISTINCT LCM_CODE,LCM_NAME  FROM  " & Session("TENANT") & "."  & "LOCATION, " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW " & _
                  " WHERE SSA_STA_ID in (6,14)and AUR_ID = SRN_AUR_ID AND SSA_SRNREQ_ID=SRN_REQ_ID " & _
                  " AND SSA_BDG_ID=LCM_code and SSA_SRNREQ_ID=SRN_req_id  " & _
                  " and ( SRN_AUR_ID='" & Session("UID") & "') ORDER BY LCM_NAME"
        End If

        If iVar = 0 Then
            Try
                BindCombo(strSQL1, cboLOC, "LCM_NAME", "LCM_CODE")
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub BindData(Optional ByVal Condition As String = "", Optional ByVal iVar As Integer = 1)
        If CHECK_ADMIN_ROLE(Session("UID")) Then

            strSQL = "SELECT DISTINCT SSA_SRNREQ_ID,LCM_ID,LCM_CODE,LCM_NAME,SSA_BDG_ID,SRN_APP_BDG,SRN_REQ_DT," & _
                     " (SELECT distinct top 1 STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_sta_id) STAM_NAME,srn_type_id " & _
                     " FROM  " & Session("TENANT") & "."  & "LOCATION, " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW " & _
                     " WHERE SSA_STA_ID in (6,14) and AUR_ID in " & _
                     " (select distinct top 1  url_usr_id from  " & Session("TENANT") & "."  & "user_role, " & Session("TENANT") & "."  & "role,  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW,  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & _
                     "  " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION where url_rol_id=rol_id  and rol_acronym =('GAdmin') " & _
                     "  and url_scopemap_id=21) AND SSA_BDG_ID=LCM_code and SSA_SRNREQ_ID=SRN_req_id  "

        ElseIf CHECK_ANY_ROLE(Session("UID"), "HR") Or CHECK_ANY_ROLE(Session("UID"), "ADMIN") Then

            strSQL = "SELECT DISTINCT SSA_SRNREQ_ID,LCM_Name," & _
                                           " (SELECT distinct top 1 STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_sta_id) STAM_NAME,srn_type_id " & _
                                            " FROM  " & Session("TENANT") & "."  & "LOCATION, " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW " & _
                                           " WHERE SSA_STA_ID in (6,14)  AND SSA_BDG_ID=LCM_code and SSA_SRNREQ_ID=SRN_req_id "
        Else

            strSQL = "SELECT DISTINCT SSA_SRNREQ_ID,LCM_ID,LCM_CODE,LCM_NAME,SSA_BDG_ID,SRN_APP_BDG,SRN_REQ_DT," & _
         " (SELECT distinct top 1 STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_sta_id) STAM_NAME,srn_type_id " & _
         " FROM  " & Session("TENANT") & "."  & "LOCATION, " & Session("TENANT") & "."  & "SMS_SPACE_ALLOCATION, " & AppSettings("APPDB") & "SMS_SPACEREQUISITION, " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW " & _
         " WHERE SSA_STA_ID in (6,14)and AUR_ID = SRN_AUR_ID AND SSA_SRNREQ_ID=SRN_REQ_ID " & _
         " AND SSA_BDG_ID=LCM_code and SSA_SRNREQ_ID=SRN_req_id  " & _
         " and ( SRN_AUR_ID='" & Session("UID") & "')"

            'NOTE: THIS IA FOR COMBO FILL FOR FIRST TIME 



        End If
        strSQL = strSQL & Condition & " order by SSA_SRNREQ_ID"
        strSQL1 = strSQL1 & Condition & " order by SSA_SRNREQ_ID"
        Dim Ds As New DataSet
        Try
            Ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            grdReqList.DataSource = Ds
            grdReqList.DataBind()
        Catch ex As Exception

        End Try



        If grdReqList.Rows.Count > 0 Then
            Dim i As Integer
            grdReqList.Visible = True
            For i = 0 To grdReqList.Rows.Count - 1
                If grdReqList.Rows(i).Cells(4).Text = 1 Then
                    grdReqList.Rows(i).Cells(3).Text = "Employee(s)"
                ElseIf grdReqList.Rows(i).Cells(4).Text = 2 Then
                    grdReqList.Rows(i).Cells(3).Text = "Non Employee(s)"
                Else
                    grdReqList.Rows(i).Cells(3).Text = "Na"
                End If
            Next

        Else
            grdReqList.Visible = False
            Response.Write("<p align=center class=clsmessage><br><br><br><br><br>No Requests For Releasing The Space</p>")
        End If
    End Sub

    Private Sub cboLOC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLOC.SelectedIndexChanged
        Try
            visibleTF(1)
            grdReqList.PageIndex = 0
            If cboLOC.SelectedIndex <> 0 Then
                BindData(" AND LCM_CODE='" & cboLOC.SelectedItem.Value & "'")
            Else
                BindData()
            End If
            visibleTF(0)
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            ' fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Response.Write("<script language=javascript>javascript:window.open('../../../Masters/MAS_WebFiles/searchloc.aspx?',Window','toolbar=no,resizable=yes,statusbar=yes,top=0,left=0,width=790,height=550')</script>")
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Protected Sub grdReqList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdReqList.PageIndexChanging
        Try
            visibleTF(1)
            grdReqList.PageIndex = e.NewPageIndex
            If cboLOC.SelectedIndex <> 0 Then
                BindData(" AND LCM_CODE='" & cboLOC.SelectedItem.Value & "'")
            Else
                BindData()
            End If
            visibleTF(0)
        Catch ex As SqlException
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub visibleTF(ByVal status As Integer)
        If status = 1 Then
            grdReqList.Columns(4).Visible = True
        Else
            grdReqList.Columns(4).Visible = False
        End If
    End Sub
End Class
