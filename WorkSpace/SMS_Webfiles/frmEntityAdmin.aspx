﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="EntityAdminController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%-- <div class="widgets">
                <div ba-panel ba-panel-title="Parent Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Parent Entity Master</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">--%>
            <div class="widgets">
                <h3>Parent Entity Master</h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmEntityAdmin" data-valid-submit="Save()" novalidate>
                    <div class="row">
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Parent Entity Code <span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <div data-ng-class="{'has-error': frmEntityAdmin.$submitted && frmEntityAdmin.ADM_Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers)" onmouseout="UnTip()">
                                        <input id="ADM_Code" type="text" name="ADM_Code" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="EntityAdmin.ADM_Code" autofocus class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmEntityAdmin.$submitted && frmEntityAdmin.ADM_Code.$invalid" style="color: red">Please enter a valid parent entity code </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Parent Entity Name<span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <div data-ng-class="{'has-error': frmEntityAdmin.$submitted && frmEntityAdmin.ADM_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers)" onmouseout="UnTip()">
                                        <input id="ADM_Name" type="text" name="ADM_Name" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="EntityAdmin.ADM_Name" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmEntityAdmin.$submitted && frmEntityAdmin.ADM_Name.$invalid" style="color: red">Please enter a valid parent entity name </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Remarks</label>
                                <div class="col-md-12">
                                    <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                        <textarea id="Textarea1" runat="server" data-ng-model="EntityAdmin.ADM_REM" style="height: 60px" class="form-control" maxlength="500"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group" data-ng-show="ActionStatus==1">
                                <label class="col-md-12 control-label">Status<span style="color: red;">*</span></label>
                                <div class="col-md-12">
                                    <div>
                                        <select id="ADM_Status_Id" name="ADM_Status_Id" data-ng-model="EntityAdmin.ADM_Status_Id" class="form-control">
                                            <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                <a class='btn btn-primary custom-button-color' href="../../Masters/Mas_Webfiles/frmMASMasters.aspx">Back</a>

                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 30px;">

                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>

                    </div>
                </form>
            </div>
        </div>
        <%--</div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
        //app.directive('validSubmit', ['$parse', function ($parse) {
        //    return {

        //        require: 'form',

        //        link: function (scope, element, iAttrs, form) {
        //            form.$submitted = false;
        //            var fn = $parse(iAttrs.validSubmit);
        //            element.on('submit', function (event) {
        //                scope.$apply(function () {
        //                    form.$submitted = true;
        //                    if (form.$valid) {
        //                        fn(scope, { $event: event });
        //                        form.$submitted = false;
        //                    }
        //                });
        //            });
        //        }
        //    };
        //}
        //])

        app.service("EntityAdminService", function ($http, $q) {
            var deferred = $q.defer();
            this.getEntityAdmin = function () {
                deferred = $q.defer();
                return $http.get('../../api/EntityAdmin')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            //SAVE
            this.saveEntityAdmin = function (EntyAdmin) {
                deferred = $q.defer();
                return $http.post('../../api/EntityAdmin/Create', EntyAdmin)
                    .then(function (response) {
                        deferred.resolve(response);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };
            //UPDATE BY ID
            this.updateEntityAdmin = function (EntyAdminUpdate) {
                deferred = $q.defer();
                return $http.post('../../api/EntityAdmin/UpdateEntityAdminData/', EntyAdminUpdate)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };
            //Bind Grid
            this.GetEntityAdminGridData = function () {
                deferred = $q.defer();
                return $http.get('../../api/EntityAdmin/BindEntityGrid')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };
        });

        app.controller('EntityAdminController', function ($scope, $q, EntityAdminService, $timeout) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.EntityAdmin = {};
            $scope.repeatCategorylist = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;

            //to Save the data
            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    EntityAdminService.updateEntityAdmin($scope.EntityAdmin).then(function (repeat) {
                        var updatedobj = {};
                        angular.copy($scope.EntityAdmin, updatedobj)
                        $scope.gridata.unshift(updatedobj);
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";

                        EntityAdminService.GetEntityAdminGridData().then(function (data) {
                            $scope.gridata = data;
                            //$scope.createNewDatasource();
                            $scope.gridOptions.api.setRowData(data);
                        }, function (error) {
                            console.log(error);
                        });



                        $scope.IsInEdit = false;
                        $scope.ClearData();
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                            });
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.EntityAdmin.ADM_Status_Id = "1";
                    EntityAdminService.saveEntityAdmin($scope.EntityAdmin).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        angular.copy($scope.EntityAdmin, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.EntityAdmin = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            //for GridView
            var columnDefs = [
                { headerName: "Parent Entity Code", field: "ADM_Code", width: 100, cellClass: 'grid-align' },
                { headerName: "Parent Entity Name", field: "ADM_Name", width: 150, cellClass: 'grid-align' },
                //{ headerName: "Entity Admin", template: "{{data.ADM_Code}} / {{data.ADM_Name}}", width: 300, cellClass: 'grid-align' },
                { headerName: "Status", template: "{{ShowStatus(data.ADM_Status_Id)}}", width: 100, cellClass: 'grid-align' },
                { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true }
            ];

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                EntityAdminService.GetEntityAdminGridData().then(function (data) {
                    $scope.gridata = data;
                    //$scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 700);
                }, function (error) {
                    console.log(error);
                });
            }
            //$scope.pageSize = '10';

            //$scope.createNewDatasource = function () {
            //    var dataSource = {
            //        pageSize: parseInt($scope.pageSize),
            //        getRows: function (params) {
            //            setTimeout(function () {
            //                var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
            //                var lastRow = -1;
            //                if ($scope.gridata.length <= params.endRow) {
            //                    lastRow = $scope.gridata.length;
            //                }
            //                params.successCallback(rowsThisPage, lastRow);
            //            }, 500);
            //        }
            //    };
            //    $scope.gridOptions.api.setDatasource(dataSource);
            //}

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableCellSelection: false,
                suppressHorizontalScroll: true,
                enableFilter: true,

                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };
            $timeout($scope.LoadData, 1000);
            $scope.EditFunction = function (data) {
                $scope.EntityAdmin = {};
                angular.copy(data, $scope.EntityAdmin);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
            }
            $scope.ClearData = function () {
                $scope.EntityAdmin = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.frmEntityAdmin.$submitted = false;
            }
            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
        });

    </script>
    <%--<script defer src="../../SMViews/Utility.js"></script>--%>
    <script defer src="../../SMViews/Utility.min.js"></script>
</body>
</html>



