Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports System.Threading
Partial Class WorkSpace_SMS_Webfiles_editconference_employee
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Binddailygrid()
        End If
    End Sub
    Private Sub Binddailygrid()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@aur_id", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(1).Value = ""
        objsubsonic.BindGridView(gvDaily, "VIEW_CONFERENCE_employee", param)
    End Sub

    Protected Sub gvDaily_PageIndexChanging(ByVal sender As Object, ByVal e As Web.UI.WebControls.GridViewPageEventArgs) Handles gvDaily.PageIndexChanging
        gvdaily.pageindex = e.NewPageIndex()
        Binddailygrid()
    End Sub


    Protected Sub gvDaily_RowCommand(ByVal sender As Object, ByVal e As Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDaily.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblreqid As Label = DirectCast(gvdaily.Rows(rowIndex).FindControl("lblreqid"), Label)
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CANCEL_CONFERENCE")
            SP1.Command.AddParameter("@REQ_ID", lblreqid.Text, DbType.String)

            SP1.ExecuteScalar()
        End If
        Binddailygrid()
    End Sub

    Protected Sub gvDaily_RowEditing(ByVal sender As Object, ByVal e As Web.UI.WebControls.GridViewEditEventArgs) Handles gvDaily.RowEditing

    End Sub

    Protected Sub gvDaily_RowDeleting(ByVal sender As Object, ByVal e As Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDaily.RowDeleting

    End Sub
End Class
