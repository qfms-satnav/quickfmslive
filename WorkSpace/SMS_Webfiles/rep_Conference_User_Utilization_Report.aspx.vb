﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_rep_Conference_User_Utilization_Report
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim obj As New clsReports
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        txtTdate.Attributes.Add("readonly", "readonly")
        txtFdate.Attributes.Add("readonly", "readonly")
        lblMsg.Text = String.Empty
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            LoadCity()
            Bindlocation(ddlCity.SelectedValue)
            'BindTower(ddlSelectLocation.SelectedValue)
            ddlSelectLocation.Items.Insert(0, "--All--")
            ddlSelectLocation.Items.RemoveAt(1)
            ddlTower.Items.Insert(0, "--All--")
            ddlFloor.Items.Insert(0, "--All--")
            txtFdate.Text = getoffsetdatetime(DateTime.Now).AddYears(-1).Date
            txtTdate.Text = getoffsetdatetime(DateTime.Now).Date
            BindGrid()
        End If
    End Sub
    Public Sub LoadCity()
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
        ddlCity.Items(0).Text = "--All--"
    End Sub
    Public Sub Bindlocation(ByVal strcity As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = strcity
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    'Public Sub BindTower(ByVal strLoc As String)
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
    '    param(0).Value = strLoc
    '    ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)

    'End Sub

    'Private Sub BindFloor(ByVal tower As String, ByVal loc As String)
    '    Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
    '    verDTO = verBll.GetDataforFloors(tower, loc.Trim())
    '    ddlFloor.DataSource = verDTO
    '    ddlFloor.DataTextField = "Floorname"
    '    ddlFloor.DataValueField = "Floorcode"
    '    ddlFloor.DataBind()

    'End Sub


    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        ReportViewer1.Visible = False
        ddlSelectLocation.Items.Clear()
        Bindlocation(ddlCity.SelectedItem.Value)

        If ddlSelectLocation.Items.Count = 0 Then
            lblMsg.Text = "No Locations Available for Selected City"
            lblMsg.Visible = True
        Else
            ddlSelectLocation.Items(0).Text = "--All--"
            lblMsg.Visible = False
        End If
        obj.bindTower_Locationwise(ddlTower, ddlSelectLocation.SelectedValue)
        ddlTower.Items(0).Text = "--All--"
        If ddlTower.Items.Count = 2 Then
            ddlTower.SelectedIndex = 0
        End If
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
        ddlFloor.Items(0).Text = "--All--"
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
        End If

    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectLocation.SelectedIndexChanged

      ReportViewer1.Visible = False
        obj.bindTower_Locationwise(ddlTower, ddlSelectLocation.SelectedValue)
        ddlTower.Items(0).Text = "--All--"
        If ddlTower.Items.Count = 2 Then
            ddlTower.SelectedIndex = 0
        End If
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
        ddlFloor.Items(0).Text = "--All--"
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
         ReportViewer1.Visible = False
        obj.bindfloor(ddlFloor, ddlTower.SelectedValue)
        ddlFloor.Items(0).Text = "--All--"
        If ddlFloor.Items.Count = 2 Then
            ddlFloor.SelectedIndex = 0
        End If
    End Sub


    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        BindGrid()
    End Sub
    Private Sub BindGrid()
        Dim Mcity As String = ""
        Dim Mloc As String = ""
        Dim Mtwr As String = ""
        Dim Mflr As String = ""

        If ddlCity.SelectedValue = "--All--" Then
            Mcity = ""
        Else
            Mcity = ddlCity.SelectedValue
        End If

        If ddlSelectLocation.SelectedValue = "--All--" Then
            Mloc = ""
        Else
            Mloc = ddlSelectLocation.SelectedValue
        End If

        If ddlTower.SelectedValue = "--All--" Then
            Mtwr = ""
        Else
            Mtwr = ddlTower.SelectedValue
        End If

        If ddlFloor.SelectedValue = "--All--" Then
            Mflr = ""
        Else
            Mflr = ddlFloor.SelectedValue
        End If

        If CDate(txtFdate.Text) <= CDate(txtTdate.Text) Then
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@CONF_CTY", SqlDbType.NVarChar, 200)
            param(0).Value = Mcity
            param(1) = New SqlParameter("@CONF_LOC", SqlDbType.NVarChar, 200)
            param(1).Value = Mloc
            param(2) = New SqlParameter("@CONF_TWR", SqlDbType.NVarChar, 200)
            param(2).Value = Mtwr
            param(3) = New SqlParameter("@CONF_FLR", SqlDbType.NVarChar, 200)
            param(3).Value = Mflr
            param(4) = New SqlParameter("@CONF_FROMDATE", SqlDbType.DateTime, 200)
            param(4).Value = txtFdate.Text
            param(5) = New SqlParameter("@CONF_TODATE", SqlDbType.DateTime, 200)
            param(5).Value = txtTdate.Text
            'ObjSubSonic.BindGridView(gvspacereport, "GET_CONF_USER_UTILIZE_REPORT", param)
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("GET_CONF_USER_UTILIZE_REPORT", param)
            Dim rds As New ReportDataSource()
            rds.Name = "USER_CONF_REPORT"
            'This refers to the dataset name in the RDLC file
            rds.Value = ds.Tables(0)
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Conf_Mgmt/UserConferenceUtilReport.rdlc")
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True
        Else
            lblMsg.Text = "To Date Should Be Greater Than From Date"
        End If
    End Sub
End Class
