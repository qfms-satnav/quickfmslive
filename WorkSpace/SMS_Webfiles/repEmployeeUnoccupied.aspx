﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="repEmployeeUnoccupied.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_repEmployeeUnoccupied" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-container {
            color: black;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
            /*border-bottom: 1px solid #eee;*/
            /*background-color: #428bca;*/
            /*-webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>

</head>
<body>

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Location Wise Employee Un-occupied Report</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server"
                        CssClass="alert alert-danger"
                        ForeColor="red" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                    </asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 control-label">City</label>
                                    <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                        Display="None" ErrorMessage="Please Select  City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    <div class="col-md-10">
                                        <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                            OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 control-label">Location</label>
                                    <asp:RequiredFieldValidator ID="rfvlocation" runat="server" ControlToValidate="ddlLocation"
                                        Display="None" ErrorMessage="Please Select  Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    <div class="col-md-10">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-2 control-label">Tower</label>
                                                <asp:RequiredFieldValidator ID="rfvtower" runat="server" ControlToValidate="ddlTower"
                                                    Display="None" ErrorMessage="Please Select Tower" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <div class="col-md-10">
                                                    <asp:DropDownList ID="ddlTower" runat="server"
                                                        CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-2 control-label">Floor</label>
                                                <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlFloor"
                                                    Display="None" ErrorMessage="Please Select  Floor" InitialValue="-- Select --"></asp:RequiredFieldValidator>
                                                <div class="col-md-10">
                                                    <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>

                    <div class="row">
                        <div class="col-md-10 text-right">
                            <div class="form-group">
                                <asp:Button ID="btnsubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="View Report" ValidationGroup="Val1" />

                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp</div>
                    <div class="row" style="overflow-x:scroll;margin:0px 5px;">
                        <div class="col-md-12">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server"></rsweb:ReportViewer>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
    <script defer src="../../Utility.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
</body>
</html>
