Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmAdminApproval
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If Not Page.IsPostBack Then
                visibleTF(1)
                Bindgrid()
                visibleTF(0)
            End If
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            ' fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub Bindgrid()
        'Dim arParms() As SqlParameter = New SqlParameter(1) {}
        'arParms(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
        'arParms(0).Value = Session("uid")
        'arParms(1) = New SqlParameter("@TYPE", SqlDbType.NVarChar, 10)
        'arParms(1).Value = "LOC"

        'Dim STR As String = ""
        'Dim sql As String = ""



        'If CHECK_ADMIN_ROLE(Session("UID")) Then
        '    sql = "SELECT DISTINCT SRN_REQ_ID,SRN_RM_ID,SRN_REQ_DT,SRN_REM," & _
        '    " (SELECT DISTINCT LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE) PRM_DESC1," & _
        '        " (SELECT DISTINCT LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO) PRM_DESC2, " & _
        '        " (SELECT DISTINCT STA_DESC from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_sta_id) STAM_NAME,srn_type_id " & _
        '        " FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "LOCATION PRM " & _
        '        " WHERE SRN_STA_ID in (5,23) order by SRN_REQ_DT DESC"
        '    '" And (SRM.SRN_RM_id ='" & Session("uid") & "' or SRM.SRN_AUR_id ='" & Session("uid") & "') order by SRN_REQ_DT"


        'ElseIf CHECK_ANY_ROLE(Session("UID"), "OSSA") Then
        '    sql = "SELECT DISTINCT SRN_REQ_ID,SRN_RM_ID,SRN_REQ_DT,SRN_REM," & _
        '    " (SELECT DISTINCT LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE) PRM_DESC1," & _
        '        " (SELECT DISTINCT LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO) PRM_DESC2, " & _
        '        " (SELECT DISTINCT STA_DESC from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_sta_id) STAM_NAME,srn_type_id " & _
        '        " FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM " & _
        '        " WHERE SRN_STA_ID in (5,23) and  SRM.SRN_BGT_ID in (" & STR & ") order by SRN_REQ_DT DESC"
        'ElseIf CHECK_ANY_ROLE(Session("UID"), "OSSI") Then

        '    sql = "SELECT DISTINCT SRN_REQ_ID,SRN_RM_ID,SRN_REQ_DT,SRN_REM," & _
        '    " (SELECT DISTINCT LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE) PRM_DESC1," & _
        '        " (SELECT DISTINCT LCM_NAME FROM  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO) PRM_DESC2," & _
        '        " (SELECT DISTINCT STA_DESC from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_sta_id) STAM_NAME,srn_type_id " & _
        '        " FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM  " & _
        '        " WHERE SRN_STA_ID in (5,23) and  SRM.SRN_BGT_ID in  (" & STR & ") order by SRN_REQ_DT DESC"
        'Else
        '    sql = ""
        'End If


        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 50)
        arParms(0).Value = Session("uid")
        'commented for now
        'If CHECK_ADMIN_ROLE(Session("UID")) Then
        '    arParms(1) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
        '    arParms(1).Value = 1
        'ElseIf CHECK_ANY_ROLE(Session("UID"), "OSSA") Or CHECK_ANY_ROLE(Session("UID"), "OSSI") Or CHECK_ANY_ROLE(Session("UID"), "ADMIN") Then
        '    arParms(1) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
        '    arParms(1).Value = 2
        'Else
        '    arParms(1) = New SqlParameter("@TYPE", SqlDbType.Int, 4)
        '    arParms(1).Value = 3
        'End If

        Dim capDS As New DataSet
        capDS = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, AppSettings("APPDB") & "SPACE_ADMINAPP_BINDDATA_SP", arParms)
        grdReqList.DataSource = capDS
        grdReqList.DataBind()

        If grdReqList.Rows.Count > 0 Then

            grdReqList.Visible = True
            'Dim i As Integer
            'For i = 0 To grdReqList.Rows.Count - 1
            '    If grdReqList.Rows(i).Cells(7).Text = "1" Then
            '        grdReqList.Rows(i).Cells(6).Text = "Employee(s)"
            '    ElseIf grdReqList.Rows(i).Cells(7).Text = "2" Then
            '        grdReqList.Rows(i).Cells(6).Text = "Non Employee(s)"
            '    Else
            '        grdReqList.Rows(i).Cells(6).Text = "Na"
            '    End If
            'Next
        Else
            grdReqList.Visible = False
            Response.Write("<p align=center class=clsmessage><br><br><br><br><br><br><br><br><br><br><br><br><br>No requisitions for approval</p>")
        End If
    End Sub

    Protected Sub grdReqList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdReqList.PageIndexChanging
        Try
            visibleTF(1)
            grdReqList.PageIndex = e.NewPageIndex
            Bindgrid()
            visibleTF(0)
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub visibleTF(ByVal status As Integer)
        If status = 1 Then
            grdReqList.Columns(3).Visible = True
            grdReqList.Columns(4).Visible = True
            grdReqList.Columns(7).Visible = True
        Else
            grdReqList.Columns(3).Visible = False
            grdReqList.Columns(4).Visible = False
            grdReqList.Columns(7).Visible = False
        End If


    End Sub
End Class
