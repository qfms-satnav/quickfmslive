﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_VerticalWiseutilization
    Inherits System.Web.UI.Page

    Dim obj As New clsReports
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""
        Label1.Text = Session("Parent")

        If Not Page.IsPostBack Then

            obj.bindVertical(ddlverticals)
            lblSelParent.Text = Session("Parent")
            ddlverticals.Items(0).Text = "--All " & Session("Parent") & "(s)--"
            ddlverticals.SelectedIndex = 0
            bindreport()
            lbltotal_util.ForeColor = Drawing.Color.Black
            lbltotal_util.Font.Bold = True

        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        Try
            If ddlverticals.SelectedIndex = 0 Then
                lbl1.Visible = False
                'Exit Sub
            End If


            bindreport()

        Catch ex As Exception

        End Try
    End Sub
    Public Sub bindreport()
        Dim rds As New ReportDataSource()
        rds.Name = "VerticalUtilizationDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/VerticalUtilizationReport.rdlc")

        'Setting Header Column value dynamically
        Dim p2 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True


        Dim verticalid As String = ""
        Dim towerid As String = ""
        If ddlverticals.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            verticalid = ""
        Else
            verticalid = ddlverticals.SelectedValue
        End If

        Dim param(2) As SqlParameter

        param(0) = New SqlParameter("@vc_Vername", SqlDbType.NVarChar, 200)
        param(0).Value = verticalid
        param(1) = New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 200)
        param(1).Value = "VERTICAL"
        param(2) = New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 200)
        param(2).Value = "asc"

        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("USP_GET_VERTICALUTILITY_REPORT", param)
        rds.Value = ds.Tables(0)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VERTICAL_UTI_COUNT")
        sp.Command.AddParameter("@VER_ID", verticalid, DbType.String)
        ds = sp.GetDataSet()

        If ds.Tables(0).Rows.Count > 0 Then
            lbltotal_util.Text = ds.Tables(0).Rows(0).Item("util") & "%"
            tutility.Visible = True
        Else
            tutility.Visible = False
        End If
    End Sub
    Protected Sub ddlverticals_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlverticals.SelectedIndexChanged
        lbl1.Visible = False
        ReportViewer1.Visible = False
        tutility.Visible = False
        If ddlverticals.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            lbl1.Visible = False
           
        End If


        Dim sp1 As New SqlParameter("@vc_VerticalName", SqlDbType.NVarChar, 50)
        If ddlverticals.SelectedIndex = 1 Then
            sp1.Value = ""
        Else
            sp1.Value = ddlverticals.SelectedValue
        End If
       
    End Sub

End Class
