﻿Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data.OleDb
Imports System.IO
Imports System.Collections.Generic

Partial Class WorkSpace_SMS_Webfiles_ProcessOwnerMaster
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(sender As Object, e As System.EventArgs) Handles btnsubmit.Click
        txtempsearch.Text = ""
        If Page.IsValid = True Then
            Dim valid As Integer
            valid = ValidateEmployee()
            If valid = 0 Then
                Modify_User_costcenter()
                BindGrid()
                lblMsg.Text = "Employee " + Session("Child") + " Modified Successfully"
                Cleardata()
                txtempid.ReadOnly = False
            Else
                Dim cnt As Integer = 0
                For Each li As ListItem In liitems.Items
                    If li.Selected = True Then
                        cnt = cnt + 1
                    End If
                Next
                If cnt = 1 Then
                    Assign_User_costcenter()
                    BindGrid()
                    lblMsg.Text = Session("Child") + " Mapped to Employee Successfully"
                    Cleardata()
                Else
                    lblMsg.Text = "Please select one " + Session("Child") + " to map..."
                End If
            End If
        End If
    End Sub

    Private Function ValidateEmployee()
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EMPLOYEE_VALIDATE_COSTCENTER")
        sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)

        valid = sp.ExecuteScalar()
        Return valid
    End Function

    Private Sub Assign_User_costcenter()
        For Each li As ListItem In liitems.Items
            If li.Selected = True Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_EMPLOYEE_COSTCENTER")
                sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
                sp.Command.AddParameter("@COSTCENTER", li.Value, DbType.String)
                sp.ExecuteScalar()
            End If
        Next
    End Sub

    Private Sub Modify_User_costcenter()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MODIFY_EMPLOYEE_COSTCENTER")
        sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
        sp.ExecuteScalar()
        Assign_User_costcenter()
    End Sub

    Private Sub Cleardata()
        txtempid.Text = ""
        liitems.ClearSelection()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        lblHeader.Text = Session("Child") + " Owner Master"
        lblProcess.Text = " Select " + Session("Child")
        If Not IsPostBack() Then
            txtempid.ReadOnly = False
            BindCostcenter()
            BindGrid()
            btnsubmit.Visible = True
            btnmodify.Visible = False
            lblMsg.Text = ""
        End If
    End Sub

    Private Sub BindCostcenter()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ALLCOSTCENTER_AURID")
        liitems.DataSource = sp.GetDataSet()
        liitems.DataTextField = "COST_CENTER_NAME"
        liitems.DataValueField = "COST_CENTER_CODE"
        liitems.DataBind()
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COSTCENTER_EMPLOYEE")
        sp.Command.AddParameter("@aur_id", txtempsearch.Text, DbType.String)
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.pageindex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "Delete" Then
            txtempid.readonly = False
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblpowid As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblpowid"), Label)
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DEL_EMPLOYEE_COSTCENTER")
            SP1.Command.AddParameter("@SNO", lblpowid.Text, DbType.String)
            SP1.ExecuteScalar()
            lblMsg.Text = "Deleted successfully"
        ElseIf e.CommandName = "Edit" Then
            lblMsg.Text = ""
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblpowid As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblpowid"), Label)
            Dim lblempid As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblempid"), Label)
            Dim lblcostcenter As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblcostcenter"), Label)
            txtempid.text = lblempid.text
            txtempid.readonly = True
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_COSTCENTER_EMPLOYEE")
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            liitems.clearselection()
            While dr.Read()
                liitems.Items.FindByValue(dr("POW_PSY_ID")).Selected = True
            End While
        End If
        BindGrid()
    End Sub

    Protected Sub btnsearch_Click(sender As Object, e As System.EventArgs) Handles btnsearch.Click
        If txtempsearch.text <> "" Then
            BindGrid()
        End If
    End Sub

    Protected Sub gvitems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvitems.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(2).Text = Session("Child")
        End If
    End Sub

    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            ActiveTab = 0
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename
                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                msheet = listSheet(0).ToString()
                mfilename = msheet
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    sheetname = mfilename
                    str = "Select * from [" & sheetname & "]"
                End If
                Dim empid As Integer = 1
                Dim cost_code As Integer = 1
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "employee id" Then
                            empid = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Employee ID"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "cost center" Then
                            cost_code = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Cost Center"
                            Exit Sub
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "employee id" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Employee ID is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "cost center" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Cost Center Name is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_EMP_COSTCENTER")
                    sp.Command.AddParameter("@AUR_ID", ds.Tables(0).Rows(i).Item("Employee Id").ToString, DbType.String)
                    sp.Command.AddParameter("@COSTCENTER", ds.Tables(0).Rows(i).Item("Cost Center").ToString, DbType.String)
                    sp.ExecuteScalar()
                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_INSERT_EMP_COSTCENTER_SM")
                Dim rst As New DataSet
                rst = sp1.GetDataSet()
                unsuccessgridview.DataSource = rst
                unsuccessgridview.DataBind()
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                BindGrid()
                If rst.Tables(0).Rows.Count > 0 Then
                    ActiveTab = 1
                End If
            Else
                lblMsg.Text = ""
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private _activetab As Integer

    Public Property ActiveTab As Integer
        Get
            Return _activetab
        End Get
        Set(value As Integer)
            _activetab = value
        End Set
    End Property

    Protected Sub gvitems_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvitems.SelectedIndexChanged

    End Sub

    Protected Sub gvitems_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub

    Protected Sub gvitems_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvitems.RowDeleting

    End Sub
End Class
