﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmEmployeeSwap.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmEmployeeSwap" ValidateRequest="true" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function CheckDate() {
            var dtFrom = document.getElementById("txtFromdate").Value;
            var dtTo = document.getElementById("txtTodate").Value;
            if (dtFrom < dtTo) {
                alert("Invalid Dates");
            }
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)

            var theForm = document.forms['aspnetForm'];

            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function CheckDataGrid() {
            document.getElementById("lblMsg").innerText = '';
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                document.getElementById("lblMsg").innerText = 'Please check at least one checkbox to swap employee';
                return false;
            }
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Employee Swapping</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="EmpSwapPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Country</label>
                                        <asp:RequiredFieldValidator
                                            ID="ReqCountry" runat="server" ControlToValidate="ddlCountry" Display="None" ErrorMessage="Please Select Country"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlCountry" runat="server" data-toggle="dropdown" AutoPostBack="True" CssClass=" form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">City </label>
                                        <asp:RequiredFieldValidator
                                            ID="ReqCity" runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlCity" runat="server" data-toggle="dropdown" AutoPostBack="True" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem Text="--Select--" Value="--Select--"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Location</label>
                                        <asp:RequiredFieldValidator
                                            ID="ReqLocation" runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem Text="--Select--" Value="--Select--"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tower</label>
                                        <asp:RequiredFieldValidator
                                            ID="ReqTower" runat="server" ControlToValidate="ddlTower" Display="None" ErrorMessage="Please Select Tower"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlTower" runat="server" AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem Text="--Select--" Value="--Select--"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Floor </label>
                                        <asp:RequiredFieldValidator ID="ReqFloor" runat="server" ControlToValidate="ddlFloor" Display="None"
                                            ErrorMessage="Please Select Floor" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlFloor" AutoPostBack="True" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem Text="--Select--" Value="--Select--"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label CssClass="col-md-12 control-label" runat="server" ID="lblvert"> </asp:Label>
                                        <asp:RequiredFieldValidator ID="ReqVertical" runat="server" ControlToValidate="ddlVertical"
                                            Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlVertical" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label CssClass="col-md-12 control-label" runat="server" ID="lbldept"> </asp:Label>
                                        <asp:RequiredFieldValidator ID="ReqDept" runat="server" ControlToValidate="ddldept" Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Search by Username</label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="ddlEmpSwap" runat="server" CssClass="form-control" Width="200px" AutoPostBack="true"></asp:TextBox>
                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="ddlEmpSwap" MinimumPrefixLength="3" EnableCaching="false"
                                                CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetDetails" ServicePath="~/Autocompletetype.asmx">
                                            </cc1:AutoCompleteExtender>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="box-footer text-left" style="margin-left: 30px !important;">
                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" />
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvswap" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found." PageSize="10"
                                        AllowPaging="true" CssClass="table GridStyle" GridLines="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Emp Id/Emp Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="EmpId" Text='<% #Bind("EMPNAME")%>'></asp:Label>
                                                    <asp:Label runat="server" Visible="false" ID="lblEmpidVal" Text='<% #Bind("EMPID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Dept" Text='<% #Bind("DEPARTMENT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Space Id">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="SpcId" Text='<% #Bind("SPACE_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="To Whom">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlEmp" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true" OnSelectedIndexChanged="ddlEmp_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtEmpName" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDept" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Swapped Space Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSpc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                </HeaderTemplate>
                                                <ItemStyle Width="1px" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnswap" runat="server" CssClass="btn btn-primary custom-button-color" Text="Swap" OnClientClick="return CheckDataGrid()" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

</body>
</html>
