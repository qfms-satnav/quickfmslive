Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmTenantDetails
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindPropType()
            BindUser()
            BindCity()
            BindDetails(Request.QueryString("tenant"))
            ' btnprint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
        End If
        txtDate.Attributes.Add("readonly", "readonly")
        txtPayableDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ' ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindUser()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENUSER")
            sp1.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.Int32)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindPaymentTerms()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PAYMENT_TERMS")
            ddlPaymentTerms.DataSource = sp.GetDataSet()
            ddlPaymentTerms.DataTextField = "PM_PT_NAME"
            ddlPaymentTerms.DataValueField = "PM_PT_SNO"
            ddlPaymentTerms.DataBind()
            ddlPaymentTerms.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails(ByVal tenant As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENANT_DETAILS")
            sp.Command.AddParameter("@SNO", tenant, DbType.String)
            sp.Command.AddParameter("@flag", 1, DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                BindPropType()
                Dim li4 As ListItem = Nothing
                li4 = ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPERTY_TYPE"))
                If Not li4 Is Nothing Then
                    li4.Selected = True
                End If
                Dim li2 As ListItem = Nothing
                li2 = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_CITY_CODE"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                    'ddlCity.Enabled = False
                End If
                BindCityLoc()
                Dim li6 As ListItem = Nothing
                li6 = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_LOC_CODE"))
                If Not li6 Is Nothing Then
                    li6.Selected = True
                    'ddlCity.Enabled = False
                End If

                BindProp()
                Dim li10 As ListItem = Nothing
                li10 = ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPRTY"))
                If Not li10 Is Nothing Then
                    li10.Selected = True
                End If

                BindUser()
                Dim li3 As ListItem = Nothing
                li3 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_NAME"))
                If Not li3 Is Nothing Then
                    li3.Selected = True
                End If
                BindPaymentTerms()
                Dim li8 As ListItem = Nothing
                li8 = ddlPaymentTerms.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TD_PAY_TERMS"))
                If Not li8 Is Nothing Then
                    li8.Selected = True
                End If
                txtTenantOccupiedArea.Text = ds.Tables(0).Rows(0).Item("PM_TEN_OCCUP_AREA")
                'txtSubject.Text = ds.Tables(0).Rows(0).Item("PM_TEN_SUB_OF_AGREE")
                'txtPurpose.Text = ds.Tables(0).Rows(0).Item("PM_TEN_PUR_OF_AGREE")
                txtRent.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_RENT"), 2, MidpointRounding.AwayFromZero)
                txtDate.Text = ds.Tables(0).Rows(0).Item("PM_TD_JOIN_DT")
                txtSecurityDeposit.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_SECURITY_DEPOSIT"), 2, MidpointRounding.AwayFromZero)
                txtcode.Text = ds.Tables(0).Rows(0).Item("PM_TEN_CODE")
                ddlPaymentTerms.SelectedValue = ds.Tables(0).Rows(0).Item("PM_TD_PAY_TERMS")
                'TntFrm.Text = ds.Tables(0).Rows(0).Item("PM_TEN_FRM_DT")
                txtPayableDate.Text = ds.Tables(0).Rows(0).Item("PM_TEN_TO_DT")
                'txttcode.Text = ds.Tables(0).Rows(0).Item("PM_TEN_CODE")
                txtNoofparking.Text = ds.Tables(0).Rows(0).Item("PM_TEN_NO_OF_PARKING")
                txtfees.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_MAINT_FEES"), 2, MidpointRounding.AwayFromZero)
                txtamount.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT"), 2, MidpointRounding.AwayFromZero)
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("PM_TD_REMARKS")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            'BindProp()
            BindCityLoc()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindProp()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTY_CITY")
            sp.Command.AddParameter("@PROPERTYTYPE", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()
            'ddlBuilding.Items.Insert(0, New ListItem("--Select Property--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ' ddlCity.Items.Insert(0, New ListItem("--Select City--", "--Select City--"))

    End Sub
End Class
