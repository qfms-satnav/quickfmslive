Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports System.Threading
Partial Class WorkSpace_SMS_Webfiles_conference_view
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB", False)


        If Not Page.IsPostBack Then


            lbldate.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.LongDate)
            BindDailyGrid()

            lbldate.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.LongDate)
        End If
    End Sub
    Protected Sub PrevDay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PrevDay.Click
        lbldate.Text = FormatDateTime(CDate(lbldate.Text).Date.AddDays(-1), DateFormat.LongDate)
        BindDailyGrid()
    End Sub

    Protected Sub NextDay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles NextDay.Click
        lbldate.Text = FormatDateTime(CDate(lbldate.Text).Date.AddDays(1), DateFormat.LongDate)
        BindDailyGrid()
    End Sub

    Private Sub BindDailyGrid()
        'btnToday.CssClass = "Initial"
        'btnDaily.CssClass = "Clicked"
        'btnWeekly.CssClass = "Initial"
        'btnMonthly.CssClass = "Initial"
        'MainView.ActiveViewIndex = 1
        'Dim ds As New DataSet

        'Dim param(2) As SqlParameter
        'param(0) = New SqlParameter("@FROMDATE", SqlDbType.date)
        'param(0).Value = CDate(lbldate.Text).date
        'param(1) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        'param(1).Value = Request.QueryString("cty")
        'param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        'param(2).Value = Request.QueryString("loc")

        'ds = objsubsonic.GetSubSonicDataSet("GetVerticalCount_Today", param)
        'gvDaily.DataSource = ds
        'gvDaily.DataBind()

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@FROM_DATE", SqlDbType.Date)
        param(0).Value = CDate(lbldate.Text).Date
        param(1) = New SqlParameter("@MODE", SqlDbType.NVarChar, 200)
        param(1).Value = "1"
        ObjSubSonic.BindGridView(gvDaily, "SPACE_CONFERENCE_CHART_dt", param)


    End Sub
End Class
