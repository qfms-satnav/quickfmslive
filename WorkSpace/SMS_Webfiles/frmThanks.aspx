<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmThanks.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmThanks" Title="Building Management System" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Status</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="form-group">
                                <strong>
                                    <asp:Label ID="lbl1" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label></strong>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

