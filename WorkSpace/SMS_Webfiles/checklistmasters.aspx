﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="checklistmasters.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_checklistmasters" %>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%-- <style>
        .btn {
            border-radius: 4px;
            background-color: #3A618F;
        }
    </style>--%>
</head>
<body>

    <div class="al-content">
        <div class="widgets">
            <div style="height: 41px;">
                <h3>Check List Masters </h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <div class="box-body">
                        <div class="clearfix">

                            <div class="col-md-4 col-sm-12 col-xs-12 ">

                                <asp:HyperLink ID="HyperLink15" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLMainTypeMaster.aspx">Check List Type </asp:HyperLink>

                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12">

                                <asp:HyperLink ID="HyperLink16" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLMainCategoryMaster.aspx">Category Master</asp:HyperLink>

                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 ">

                                <asp:HyperLink ID="HyperLink19" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLMainSubCategoryMaster.aspx">Subcategory Master</asp:HyperLink>

                            </div>


                        </div>
                        <br />
                        <div class="clearfix">

                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLTypeLocInspectorMapping.aspx">Approval Matrix</asp:HyperLink>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLTypeLocMapping.aspx">Check List Type Location Mapping</asp:HyperLink>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">

                                <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLMainStatusMaster.aspx">Status Master</asp:HyperLink>

                            </div>
                        </div>
                        <br />
                        <div class="clearfix">

                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <asp:HyperLink ID="HyperLink4" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/BranchCheckListManagement/View/BCLRoleMapping.aspx">Checklist Role Mapping</asp:HyperLink>
                            </div>
                        </div>


                        <div class="clearfix">
                        </div>
                        <br />
                        <div class="clearfix">
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

