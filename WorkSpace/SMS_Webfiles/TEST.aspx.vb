﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports cls_OLEDB_postgresSQL

Partial Class WorkSpace_SMS_Webfiles_TEST
    Inherits System.Web.UI.Page
    Dim ObjPostGres As New cls_OLEDB_postgresSQL


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ds As New DataSet
        ds = ObjPostGres.PostGresGetDataSet("Get_spacecount_spacetype")

        'If ds.Tables(0).Rows.Count > 0 Then
        dlspacetype.DataSource = ds
        dlspacetype.DataBind()

        'End If

        For i As Integer = 0 To dlspacetype.Rows.Count - 1
            Dim lblspctype As Label = CType(dlspacetype.Rows(i).FindControl("lblspctype"), Label)
            If lblspctype.Text = 1 Then
                lblspctype.Text = "Dedicated"
            ElseIf lblspctype.Text = 2 Then
                lblspctype.Text = "2:Sharing"
            ElseIf lblspctype.Text = 3 Then
                lblspctype.Text = "3:Sharing"
            ElseIf lblspctype.Text = 4 Then
                lblspctype.Text = "BCP"
            End If

        Next

        
    End Sub
End Class
