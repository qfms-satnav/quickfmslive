﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_RepConsoleRelease
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim objMasters As clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim fdate As DateTime
    Dim tdate As DateTime
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            txtFdate.Text = getoffsetdatetime(DateTime.Now).AddYears(-1).Date
            txtTdate.Text = getoffsetdatetime(DateTime.Now).Date
            BindGrid()

        End If
        txtTdate.Attributes.Add("readonly", "readonly")
        txtFdate.Attributes.Add("readonly", "readonly")
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        fdate = CDate(txtFdate.Text)
        tdate = CDate(txtTdate.Text)
        If fdate > tdate Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('From Date should be less than to date');", True)
        Else
            BindGrid()
        End If

    End Sub

    Private Sub BindGrid()


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CONSOLE_RELEASE_REPORT")
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.Date)
        Dim ds As DataSet = sp.GetDataSet()


        Dim rds As New ReportDataSource()
        rds.Name = "ConsolidatedReleaseDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/ConsolidatedReleaseReport.rdlc")

        'Setting Header Column value dynamically

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

    End Sub
End Class
