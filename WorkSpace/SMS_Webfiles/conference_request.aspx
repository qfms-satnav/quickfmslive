<%@ Page Language="VB" AutoEventWireup="false" CodeFile="conference_request.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_conference_request" Title="Reservation Request" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

   <%-- <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer lang="javascript" type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Reservation Type Master" ba-panel-class="with-scroll">
                    <%--style="padding-right: 45px;">--%>
                    <%--<div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">Reservation Booking</h3>
                        </div>
                        <div class="card">
                       <%-- <div class="panel-body" style="padding-right: 10px;">--%>
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" />

                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblmsg" ForeColor="RED" class="col-md-12 control-label" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Panel1" runat="server" visible="true">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtEmpId"
                                                            Display="none" ErrorMessage="Please Enter Employee Id" ValidationGroup="Val1">
                                                        </asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i id="id" class="fa fa-tag"></i></div>
                                                            <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control" placeholder="Enter Employee Id" Enabled="false" AutoComplete="on" AutoPostBack="True"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i id="user" class="fa fa-user"></i></div>
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" AutoPostBack="True" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i id="dept" class="fa fa-bookmark"></i>
                                                            </div>
                                                            <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <img src="../../images/Chair_Blue.gif" />
                                                            </div>
                                                            <asp:TextBox ID="txtSpaceID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>City<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlCity"
                                                            Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ddlCity" AutoPostBack="true" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Location<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                                            Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ddlSelectLocation" AutoPostBack="true" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Tower<span style="color: red;">*</span></label>
                                                        <asp:CompareValidator ID="cmpflr" runat="server" ControlToValidate="ddlTower" Display="None"
                                                            ErrorMessage="Please Select Tower " ValueToCompare="--Select--" Operator="NotEqual">cmpflr</asp:CompareValidator>
                                                        <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="true">
                                                        </asp:DropDownList>

                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Floor<span style="color: red;">*</span></label>
                                                        <asp:CompareValidator ID="cmptwr" runat="server" ControlToValidate="ddlFloor" Display="None"
                                                            ErrorMessage="Please Select Floor " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>
                                                        <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">

                                                        <label>Capacity<span style="color: red;">*</span></label>

                                                        <asp:CompareValidator ID="cmpcap" runat="server" ControlToValidate="ddlCapacity" Display="None"
                                                            ErrorMessage="Please Select Capacity" ValueToCompare="--Select--" Operator="NotEqual">cmpcap</asp:CompareValidator>
                                                        <asp:DropDownList ID="ddlCapacity" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>

                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">

                                                        <label>Reservation Room<span style="color: red;">*</span></label>

                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlConf" Display="None"
                                                            ErrorMessage="Select Reservation Room " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>

                                                        <asp:DropDownList ID="ddlConf" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>


                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">

                                                        <label>Booking On <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                                            Display="None" ErrorMessage="Please Enter From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <div class='input-group date' id='Fromdate'>
                                                            <asp:TextBox ID="txtFrmDate" runat="server" MaxLength="10" CssClass="form-control">
                                                            </asp:TextBox>
                                                            <%-- <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('Fromdate')"></span>
                                                    </span>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>From Time (HH):<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr"
                                                            Display="None" ErrorMessage="Please Enter From Time" ValidationGroup="Val1" InitialValue="HH"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="starttimehr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">

                                                        <label>To Time (HH):<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr"
                                                            Display="None" ErrorMessage="Please Enter To Time " ValidationGroup="Val1" InitialValue="HH"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="endtimehr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Select Internal Attendees  </label>
                                                        <asp:TextBox ID="Textinternal" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="Textinternal_SelectedIndexChanged" data-live-search="true"></asp:TextBox>
                                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="Textinternal" MinimumPrefixLength="2" EnableCaching="false"
                                                            CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetintAttendees" ServicePath="~/Autocompletetype.asmx">
                                                        </asp:AutoCompleteExtender>
                                                        <%--<asp:ListBox ID="lstInternal" runat="server" SelectionMode="Multiple" Height="50px" CssClass="form-control" Rows="5"></asp:ListBox>--%>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Internal Attendees
                                                            <asp:LinkButton ID="btnLogout" Text="Remove" OnClick="Remove_Click" runat="server" /></label>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtInternal" ValidationGroup="Val1"
                                                            Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                                        </asp:RegularExpressionValidator>
                                                        <%--<asp:TextBox ID="txtInternal" runat="server" Height="50px" CssClass="form-control" TextMode="MultiLine" Rows="5" ToolTip="Please Enter Mail IDs With Comma (,) Separtion"></asp:TextBox>--%>
                                                        <asp:ListBox ID="txtInternal" runat="server" SelectionMode="Multiple" Height="50px" CssClass="form-control" Rows="5"></asp:ListBox>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>External Attendees <a title="Please Enter Mail IDs With Comma (,) Separation">Help</a></label>
                                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtAttendees" ValidationGroup="Val1"
                                                        Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                                    </asp:RegularExpressionValidator>
                                                    <asp:TextBox ID="txtAttendees" runat="server" Height="50px" CssClass="form-control" TextMode="MultiLine" Rows="5" ToolTip="Please Enter Mail IDs With Comma (,) Separtion"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <span style="color: red;">*</span>
                                                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription"
                                                            Display="None" ErrorMessage="Please Enter Description" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <div class="form-group">

                                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" CausesValidation="true" />
                                                        <%--<asp:Button ID="btnback" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/WorkSpace/SMS_Webfiles/frmconferencetimeslot.aspx" CausesValidation="False" />--%>
                                                        <input onclick="window.history.go(-1); return false;" class="btn btn-primary custom-button-color" type="button" value="Back" />

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="row">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-9">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:Label ID="lblconfalert" ForeColor="RED" class="col-md-12 control-label" runat="server" Visible="false"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" EmptyDataText="No Records Found."
                                                        CssClass="table GridStyle" GridLines="None" Visible="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Booked From Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBookedfromdate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_FROM_DATE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Booked To Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBookedtodate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_TO_DATE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="From Time">
                                                                <ItemTemplate>
                                                                    <%--<asp:Label ID="lblfromtime" runat="server" CssClass="bodyText" Text='<%#(DateTime.Parse(Eval("FTIME").ToString()).ToShortTimeString())%>'></asp:Label>--%>
                                                                    <asp:Label ID="lblfromtime" runat="server" CssClass="bodyText" Text='<%#Eval("FTIME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="To Time">
                                                                <ItemTemplate>
                                                                   <%-- <asp:Label ID="lbltotime" runat="server" CssClass="bodyText" Text='<%#(DateTime.Parse(Eval("TTIME").ToString()).ToShortTimeString())%>'></asp:Label>--%>
                                                                     <asp:Label ID="lbltotime" runat="server" CssClass="bodyText" Text='<%#Eval("TTIME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblstatus" runat="server" CssClass="bodyText" Text='<%#Eval("ConfStatus")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                        <PagerStyle CssClass="pagination-ys" />
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
                    </div>
               <%-- </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


