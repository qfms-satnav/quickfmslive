<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Requisition.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Requisition"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script defer language="javascript" type="text/javascript">
      function maxLength(s,args)
      { 
       if(args.Value.length >= 500)
       args.IsValid=false;
      }
        function CheckDate()
     {
         var dtFrom=document.getElementById("txtFromdate").Value;
         var dtTo=document.getElementById("txtTodate").Value;
          var Fromdt=document.getElementById("txtFromdate").Value;
         var Todt=document.getElementById("txtTodate").Value;
         if (dtFrom < dtTo)
         {
            alert("Invalid Dates");
         }
          if (Fromdt < Todt)
         {
            alert("Invalid Dates");
         }
     }
    </script>
    
    <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <script defer src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

 <div>
             <table id="table2" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">  <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
                            <asp:TextBox ID="txtgridcnt" runat="server" CssClass="clstextfield" Visible="False" Width="136px"></asp:TextBox>
            <asp:TextBox ID="txtTEmpLocation" runat="server" CssClass="clstextfield" Visible="False" Width="136px"></asp:TextBox>
            <asp:TextBox ID="txtNowDate" runat="server" CssClass="clstextfield" Visible="False" Width="136px"></asp:TextBox>
     <asp:TextBox ID="txtdept" runat="server" CssClass="clstextfield" Visible="False"
         Width="136px"></asp:TextBox>
                
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
          
                <table id="Table33" style="vertical-align: top;" align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
                     <tr>
                        <td  colspan="3" align="left">
            <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
          </td>
                        </tr>
                    <tr>
                        <td style="height: 27px">
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td width="100%" class="tableHEADER" style="height: 27px" align="left">
                            <strong>&nbsp;Space Allocations</strong>
                        </td>
                        <td style="height: 27px">
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                          <td  background="../../Images/table_left_mid_bg.gif" style="width: 10px; height: 100%;">
                            &nbsp;</td>
                        <td align="left" >
                            <br />
                            <table id="Table1" style="vertical-align: top;" border="1" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="radSpcToSpc" runat="server" AutoPostBack="True" CssClass="clsRadioButton"
                                            Font-Bold="True" GroupName="rbActions" Text="For Employees" Width="248px" /></td>
                                    <td>
                                        <asp:RadioButton ID="radSpcToStore" runat="server" AutoPostBack="True" CssClass="clsRadioButton"
                                            Font-Bold="True" GroupName="rbActions" Text="For Non-Employees" Width="244px" /></td>
                                </tr>
                            </table>
                            <asp:LinkButton ID="lnkSearch" runat="server">Search Employees for Space Requisition</asp:LinkButton><br />
                            <asp:Panel ID="pnlEmp" runat="server"  HorizontalAlign="Center" Visible="False"
                                Width="100%" Wrap="False">
                                <table id="tblEmp" border="1" cellpadding="0" cellspacing="0" 
                                    width="100%">
                                    <tr>
                                        <td style="width: 25%" align="left">
                                            &nbsp;Select Criteria
                                            <asp:RequiredFieldValidator ID="rfvSelCriteria" runat="server" ControlToValidate="cbocat"
                                                Display="None" ErrorMessage="Please Select  Criteria !"></asp:RequiredFieldValidator></td>
                                        <td >
                                            <asp:DropDownList ID="cbocat" runat="server" AutoPostBack="True" CssClass="clsComboBox"
                                                Width="98%">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Employee ID">Employee Login ID</asp:ListItem>
                                                <asp:ListItem Value="Employee Name">Employee Name</asp:ListItem>
                                                <asp:ListItem Value="Location">Location Code</asp:ListItem>
                                            </asp:DropDownList></td>
                                        <td width="25%" align="left">
                                            &nbsp;Enter
                                            <asp:Label ID="lblCriteria" runat="server">Criteria</asp:Label><asp:RequiredFieldValidator
                                                ID="rfvCriteria" runat="server" ControlToValidate="txtSearch" Display="None"
                                                ErrorMessage="Please Enter Criteria To Search!"></asp:RequiredFieldValidator></td>
                                        <td>
                                            <asp:TextBox ID="txtSearch" runat="server" CssClass="clstextfield" Width="96%"></asp:TextBox><a
                                                id="A22" href="#" onclick="return getCal('Form1','txtFromDate');"></a></td>
                                    </tr>
                                    <tr>
                                        <td align="center"  colspan="4">
                                            <asp:Button ID="btnSearchemp" runat="server" CssClass="button" Text="Search" Width="100px" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <br />
                            <asp:Panel ID="Panel1" runat="server"  HorizontalAlign="Center"
                                Visible="False" Width="95%" >
                                <asp:Label ID="lblMsg" runat="server" Font-Bold="True" Visible="False" CssClass="clsMessage"></asp:Label>
                                 <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 100%" align="center"
                                border="1">
                                <tr>
                                    <td align="left" style="height: 24px" width="50%">
                                        &nbsp;City <strong><span style="font-size: 8pt; color: #ff0000">*<asp:RequiredFieldValidator
                                            ID="rfvCity" runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City"
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator></span></strong></td>
                                    <td align="left" style="height: 24px">
                                        <asp:DropDownList ID="ddlCity" runat="server" Width="98%" CssClass="clsComboBox"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left"  style="height: 24px" width="50%">
                                        &nbsp;Location <strong><span style="font-size: 8pt; color: #ff0000">*</span></strong><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLoc" Display="None"
                                            ErrorMessage="Please Select Location" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left"  style="height: 24px">
                                        <asp:DropDownList ID="ddlLoc" runat="server" Width="98%" CssClass="clsComboBox" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlLoc_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="height: 24px;" align="left"  width="50%">
                                        &nbsp;Tower <font class="clsNote">*</font><asp:RequiredFieldValidator ID="rfvT1"
                                            runat="server" ErrorMessage="Please Select Tower" ControlToValidate="ddlTower1"
                                            Display="None" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="height: 24px;" align="left" colspan="2">
                                        <asp:DropDownList ID="ddlTower1" runat="server" Width="98%" CssClass="clsComboBox">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="Tr1" visible="FALSE" runat="server">
                                    <td align="left" visible="FALSE">
                                        &nbsp;Country&nbsp;
                                        <asp:TextBox ID="txtCity1" runat="server" Width="2px" ReadOnly="True" CssClass="clsTextField"
                                            Visible="False"></asp:TextBox></td>
                                    <td class="label" style="width: 218px">
                                        <asp:TextBox ID="txtCountry1" runat="server" Width="96%" ReadOnly="True" CssClass="clsTextField"></asp:TextBox></td>
                                    <td align="left" visible="FALSE">
                                        &nbsp;Preferred Location&nbsp;
                                    </td>
                                    <td class="label">
                                        <asp:TextBox ID="txtLocation1" runat="server" Width="96%" ReadOnly="True" CssClass="clsTextField"></asp:TextBox>
                                    </td>
                                </tr>
</table>
                                </asp:Panel>
                            <asp:Panel ID="pnlEmplst" runat="server"  HorizontalAlign="Center"
                                Visible="False" Width="95%" Wrap="False">
                                <table id="tblemplst" border="1" cellpadding="1" cellspacing="1" 
                                    width="100%">
                                    <tr>
                                        <td align="center"  class="label" colspan="4">
                                            Select Employee(s) To View Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox ID="lstEmp" runat="server" Rows="8" SelectionMode="Multiple" Width="100%">
                                            </asp:ListBox></td>
                                        <td align="center"  width="10%">
                                            <asp:Button ID="btnRight" runat="server" CausesValidation="False" CssClass="button"
                                                Text=">" Width="56px" /><br />
                                            <br />
                                            <asp:Button ID="btnLeft" runat="server" CausesValidation="False" CssClass="button"
                                                Text="<" Width="56px" /></td>
                                        <td>
                                            <asp:ListBox ID="lstSelemp" runat="server" Rows="8" SelectionMode="Multiple" Width="100%">
                                            </asp:ListBox></td>
                                    </tr>
                                    <tr>
                                        <td align="center"  colspan="3">
                                            <br />
                                            <asp:Button ID="btnAddSelected" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Add Selected" Width="100px" />&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table id="tblSelectedEmp" runat="server" align="center" border="1" width="100%">
                                    <tr>
                                        <td align="center" >
                                            <asp:GridView ID="dgEmpMap" runat="server" AutoGenerateColumns="False" Font-Names="Zurich BT"
                                                Font-Size="XX-Small" HorizontalAlign="Center" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <HeaderStyle Font-Bold="True" />
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="cbSelAll" runat="server" AutoPostBack="True" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="EName" HeaderText="Employee Name">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Job" HeaderText="Job">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Department" HeaderText="Department">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Location" HeaderText="Location">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Grade">
                                                        <HeaderStyle Font-Bold="True" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtGrade" runat="server" CssClass="clsTextField" Enabled="False"
                                                                MaxLength="5" Text='<%#DataBinder.Eval(Container.DataItem,"Grade")%>'>k</asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Space Type">
                                                        <HeaderStyle Font-Bold="True" />
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlSpcType" runat="server">
                                                                <asp:ListItem Value="WORK STATION">WORK STATION</asp:ListItem>
                                                                <asp:ListItem Value="FULL CABIN">FULL CABIN</asp:ListItem>
                                                                <asp:ListItem Value="HALF CABIN">HALF CABIN</asp:ListItem>
                                                                
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="AUR_ID" HeaderText="AUR_ID-7" ></asp:BoundField>
                                                    <asp:BoundField DataField="JOB" HeaderText="jobcode-8" ></asp:BoundField>
                                                    <asp:BoundField DataField="dep_code" HeaderText="dep_code-9" ></asp:BoundField>
                                                    <asp:BoundField DataField="LCM_CODE" HeaderText="LCM_ID-10" ></asp:BoundField>
                                                 <%--   <asp:BoundField HeaderText="Ratio">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="rto_ratio2-12" ></asp:BoundField>--%>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" >
                                            <br />
                                            <asp:Button ID="btnSubmit" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Submit" Width="100px" />&nbsp;
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Cancel" Width="100px" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            
                            <asp:Panel ID="pnlForemp" runat="server" Height="100%" HorizontalAlign="left" Visible="False"
                                Width="95%" Wrap="False">
                                <table id="tblForemp1" border="1" cellpadding="1" cellspacing="1" width="100%">
                                    <tr>
                                        <td align="Center" class="label" colspan="4">
                                            For Employees
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" style="width: 25%">
                                            &nbsp;Work Stations<font class="clsNote"> *</font> 
                                            <asp:RegularExpressionValidator ID="reWst" runat="server" ControlToValidate="txtWst"
                                                Display="None" ErrorMessage="Please Enter a Numeric Value for Work Stations "
                                                ValidationExpression="^[0-9]+" ValidationGroup="Val1"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="rfvWst" runat="server" ControlToValidate="txtWst" Display="None" ErrorMessage="Enter Work Stations Quantity" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                        <td class="label">
                                            <asp:TextBox ID="txtWst" runat="server"  CssClass="clstextfield"
                                                MaxLength="2" Width="96%">0</asp:TextBox></td>
                                        <td class="label" width="25%">
                                            &nbsp;From Date <font class="clsNote"> *</font> 
                                            <asp:CompareValidator ID="cvFrdate" runat="server" ControlToCompare="txtNowDate"
                                                ControlToValidate="txtFrmDt" Display="None" ErrorMessage="From Date Should be Greater Than Todays Date "
                                                Operator="GreaterThanEqual" Type="Date" ValidationGroup="Val1"></asp:CompareValidator><asp:RequiredFieldValidator
                                                    ID="rvFrdate" runat="server" ControlToValidate="txtFrmDt" Display="None" ErrorMessage="Select From-Date" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                        <td class="label">
                                            <asp:TextBox ID="txtFrmDt" runat="server" CssClass="clstextfield" ToolTip="Entered Date Must be lessthan Or equal to todays Date"
                                                Width="96%"></asp:TextBox>
                                            <%--<asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../images/imgAMTcalendar.GIF"
                                                CausesValidation="False" />
                                                  <asp:TextBox ID="TextBox1" runat="server" Width="96%" CssClass="clsTextField" MaxLength="10"></asp:TextBox>--%>
                                           <%-- <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtFromDate"
                                                PopupButtonID="ImageButton4" Format="MM/dd/yyyy">
                                            </cc1:CalendarExtender>--%>
                                            <%--<%--<cc1:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Mask="99/99/9999"
                                                MaskType="Date" PromptCharacter="_" TargetControlID="txtFromDate" AcceptAMPM="True"
                                                ClearMaskOnLostFocus="False">
                                            </cc1:MaskedEditExtender>--%>
                                           <%-- <cc1:MaskedEditValidator ID="MaskedEditValidator4" runat="server" ControlExtender="MaskedEditExtender1"
                                                ControlToValidate="txtFromDate" Display="None" EmptyValueMessage="Date is required"
                                                InvalidValueMessage="Expected Date Of Delivery is invalid" IsValidEmpty="False"
                                                SetFocusOnError="True" ToolTip="Input a Date"></cc1:MaskedEditValidator>--%></td>
                                    </tr>
                                    <tr>
                                        <td class="label" style="width: 25%">
                                            &nbsp;Half Cabins <font class="clsNote"> *</font> 
                                            <asp:RegularExpressionValidator ID="reHc" runat="server" ControlToValidate="txtHalfcab"
                                                Display="None" ErrorMessage="Please Enter A Numeric Value For Half Cabins" ValidationExpression="^[0-9]+" ValidationGroup="Val1"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="rfvHc" runat="server" ControlToValidate="txtHalfcab" Display="None" ErrorMessage="Enter Half Cabins Quantity" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                        <td class="label" style="height: 19px">
                                            <asp:TextBox ID="txtHalfcab" runat="server" CssClass="clstextfield"
                                                MaxLength="2" Width="96%">0</asp:TextBox></td>
                                        <td class="label" style="height: 19px">
                                            &nbsp;To Date <font class="clsNote"> *</font> 
                                            <asp:CompareValidator ID="cvtoDate" runat="server" ControlToCompare="txtFrmDt"
                                                ControlToValidate="txtToDt" Display="None" ErrorMessage="To Date Should be Greater Than From Date "
                                                Operator="greaterThanEqual" Type="Date" ValidationGroup="Val1"></asp:CompareValidator><asp:RequiredFieldValidator
                                                    ID="rfvTodate" runat="server" ControlToValidate="txtToDt" Display="None" ErrorMessage="Select To-Date" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                        <td class="label" style="height: 19px">
                                            <asp:TextBox ID="txtToDt" runat="server" CssClass="clstextfield" ToolTip="Entered Date Must be lessthan Or equal to todays Date"
                                                Width="96%"></asp:TextBox>
                                           <%-- <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../images/imgAMTcalendar.GIF"
                                                CausesValidation="False" /><cc1:CalendarExtender ID="CalendarExtender3" runat="server"
                                                    TargetControlID="txtToDt" PopupButtonID="ImageButton3" Format="MM/dd/yyyy">
                                                </cc1:CalendarExtender>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                                MaskType="Date" PromptCharacter="_" TargetControlID="txtToDt" AcceptAMPM="True"
                                                ClearMaskOnLostFocus="False">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlExtender="MaskedEditExtender1"
                                                ControlToValidate="txtToDt" Display="None" EmptyValueMessage="Date is required"
                                                InvalidValueMessage="Expected Date Of Delivery is invalid" IsValidEmpty="False"
                                                SetFocusOnError="True" ToolTip="Input a Date"></cc1:MaskedEditValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label" style="width: 25%">
                                            &nbsp;Full Cabins <font class="clsNote"> *</font> 
                                            <asp:RegularExpressionValidator ID="rvFc" runat="server" ControlToValidate="txtFullcab"
                                                Display="None" ErrorMessage="Please Enter A Numeric Value For Full Cabins" ValidationExpression="^[0-9]+" ValidationGroup="Val1"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="rfvFc" runat="server" ControlToValidate="txtFullcab" Display="None" ErrorMessage="Enter Full Cabins Quantity" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                        <td class="label" >
                                            <asp:TextBox ID="txtFullcab" runat="server" CssClass="clstextfield"
                                                MaxLength="2" Width="96%">0</asp:TextBox></td>
                                        <td class="label" style="height: 19px">
                                            &nbsp;Reporting Manager <font class="clsNote"> *</font> </td>
                                        <td class="label" >
                                            <asp:TextBox ID="txtGSC" runat="server" CssClass="clstextfield"
                                                MaxLength="50" Width="96%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                       
                                        <td class="label">
                                            &nbsp;Remarks
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="maxLength"
                                                ControlToValidate="txtPAddress" Display="None" ErrorMessage="Remarks should not be greater than 500 Characters" ValidationGroup="Val1"></asp:CustomValidator></td>
                                        <td class="label">
                                            <asp:TextBox ID="txtPAddress" runat="server" CssClass="clstextfield" MaxLength="100"  Width="200px"
                                                TextMode="MultiLine" ></asp:TextBox></td>
                                                <td></td>
                                                <td></td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="label" colspan="4">
                                            <asp:Button ID="btnForpmp" runat="server" CssClass="button" Text="Submit" Width="100px" ValidationGroup="Val1" /></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlFornonemp" runat="server"  HorizontalAlign="left"
                                Visible="False" Width="100%" Wrap="False">
                                
                                <%--  <table id="Table3" runat="server" align="center" border="1" width="100%">
                                    <tr>
                                        <td align="center" >
                                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Font-Names="Zurich BT"
                                                Font-Size="XX-Small" HorizontalAlign="Center" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <HeaderStyle Font-Bold="True" />
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="cbSelAll" runat="server" AutoPostBack="True" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="EName" HeaderText="Employee Name">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Job" HeaderText="Job">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Department" HeaderText="Department">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Location" HeaderText="Location">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Grade">
                                                        <HeaderStyle Font-Bold="True" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtGrade" runat="server" CssClass="clsTextField" Enabled="False"
                                                                MaxLength="5" Text='<%#DataBinder.Eval(Container.DataItem,"Grade")%>'>k</asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Space Type">
                                                        <HeaderStyle Font-Bold="True" />
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlSpcType" runat="server">
                                                                <asp:ListItem Value="WORK STATION">WORK STATION</asp:ListItem>
                                                                <asp:ListItem Value="FULL CABIN">FULL CABIN</asp:ListItem>
                                                                <asp:ListItem Value="HALF CABIN">HALF CABIN</asp:ListItem>
                                                                
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="AUR_ID" HeaderText="AUR_ID-7" Visible="false" ></asp:BoundField>
                                                    <asp:BoundField DataField="JOB" HeaderText="jobcode-8"   Visible="false"></asp:BoundField>
                                                    <asp:BoundField DataField="dep_code" HeaderText="dep_code-9"  Visible="false" ></asp:BoundField>
                                                    <asp:BoundField DataField="LCM_CODE" HeaderText="LCM_ID-10"  Visible="false" ></asp:BoundField>
                                                 <%--   <asp:BoundField HeaderText="Ratio">
                                                        <HeaderStyle Font-Bold="True" />
                                                    </asp:BoundField>
                                                    <asp:BoundField HeaderText="rto_ratio2-12" ></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" >
                                            <br />
                                            <asp:Button ID="Button1" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Submit" Width="100px" />&nbsp;
                                            <asp:Button ID="Button2" runat="server" CausesValidation="False" CssClass="button"
                                                Text="Cancel" Width="100px" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>--%>
                                <table id="tblForemp" border="1" cellpadding="0" cellspacing="0" 
                                    width="100%">
                                    <tr>
                                        <td align="center" class="label" colspan="4">
                                            For Non-Employees
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%" align="left">
                                            &nbsp;Consultant Name <font class="clsNote"> *</font> 
                                            <asp:RegularExpressionValidator ID="rgXName" runat="server" ControlToValidate="txtConsulname"
                                                Display="Dynamic" ErrorMessage="Consultant Name : Only Alphabets,Numbers,Spaces,-,_ are allowed.. !"
                                                ValidationExpression="^[a-zA-Z \-_0-9]+" ValidationGroup="Val3"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="regName" runat="server" ControlToValidate="txtConsulname" Display="Dynamic"
                                                    ErrorMessage="Enter Consultant Name" ValidationGroup="Val3"></asp:RequiredFieldValidator></td>
                                        <td>
                                            <asp:TextBox ID="txtConsulname" runat="server" CssClass="clstextfield"
                                                MaxLength="35" Width="95%"></asp:TextBox></td>
                                        <td  width="25%" align="left">
                                            &nbsp;Consultant Code <font class="clsNote"> *</font> 
                                            <asp:RegularExpressionValidator ID="rgXConCod" runat="server" ControlToValidate="txtConsulcode"
                                                Display="Dynamic" ErrorMessage="Consultant Code : Only Alphabets(Caps),Numbers are allowed.. !"
                                                ValidationExpression="^[A-Za-z0-9]+" ValidationGroup="Val3"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="rgCode" runat="server" ControlToValidate="txtConsulcode" Display="Dynamic" ErrorMessage="Enter Consultant Code" ValidationGroup="Val3"></asp:RequiredFieldValidator></td>
                                        <td >
                                            <asp:TextBox ID="txtConsulcode" runat="server" CssClass="clstextfield"
                                                MaxLength="15" Width="96%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%; height: 24px;" align="left">
                                            &nbsp;Location Code <font class="clsNote"> *</font> 
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlLoc"
                                                Display="Dynamic" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                                ValidationGroup="Val3"></asp:RequiredFieldValidator><%--<asp:RegularExpressionValidator ID="rgXLCode" runat="server" ControlToValidate="txtLoc"
                                                Display="None" ErrorMessage="Location Code : Only Alphabets(Caps),Numbers are allowed.. !"
                                                ValidationExpression="^[A-Z0-9]+"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="rgLCode" runat="server" ControlToValidate="txtLoc" Display="None" ErrorMessage="Enter Location Code"></asp:RequiredFieldValidator>--%></td>
                                        <td  style="height: 24px"><asp:DropDownList ID="ddlloc1" runat="server" Width="98%" CssClass="clsComboBox" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                        <td  style="height: 24px" align="left"> 
                                            &nbsp;Department <font class="clsNote"> *</font> 
                                            <asp:CompareValidator ID="cfvCategory" runat="server" ControlToValidate="cboDept"
                                                Display="Dynamic" ErrorMessage="Select Department" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val3"></asp:CompareValidator></td>
                                        <td class="label" style="height: 24px">
                                            <asp:DropDownList ID="cboDept" runat="server" Visible="False" Width="98%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            &nbsp;Space Type <font class="clsNote"> *</font> </td>
                                        <td align="left" style="height: 28px">
                                            <asp:DropDownList ID="cboSpcTypenonemp" runat="server" Width="98%">
                                                <asp:ListItem Value="WORK STATION">WORK STATION</asp:ListItem>
                                                <asp:ListItem Value="FULL CABIN">FULL CABIN</asp:ListItem>
                                                <asp:ListItem Value="HALF CABIN">HALF CABIN</asp:ListItem>
                                               
                                            </asp:DropDownList></td>
                                        <td >
                                            &nbsp;Remarks
                                            <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="maxLength"
                                                ControlToValidate="txtRem" Display="Dynamic" ErrorMessage="Remarks should not be greater than 500 Characters" ValidationGroup="Val3"></asp:CustomValidator></td>
                                        <td >
                                            <asp:TextBox ID="txtRem" runat="server" CssClass="clstextfield"
                                                TextMode="MultiLine" Width="96%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            &nbsp;From Date <font class="clsNote"> *</font> 
                                            <asp:CompareValidator ID="cvFdate" runat="server" ControlToCompare="txtNowDate" ControlToValidate="txtFromdate"
                                                Display="Dynamic" ErrorMessage="From Date Should be Greater Than Todays Date " Operator="GreaterThanEqual"
                                                Type="Date" ValidationGroup="Val3"></asp:CompareValidator><asp:RequiredFieldValidator ID="rfvFdate" runat="server"
                                                    ControlToValidate="txtFromdate" Display="Dynamic" ErrorMessage="Select From Date" ValidationGroup="Val3"></asp:RequiredFieldValidator></td>
                                        <td class="label" style="height: 19px">
                                            <asp:TextBox ID="txtFromdate" runat="server" ToolTip="Entered Date Must be lessthan Or equal to todays Date"
                                               Width="96%" CssClass="clsTextField" MaxLength="10">
                                            </asp:TextBox>
                                          
                                        </td>
                                        <td align="left" style="height: 19px">
                                            &nbsp;To Date <font class="clsNote"> *</font> 
                                            <asp:CompareValidator ID="cfvtodt" runat="server" ControlToCompare="txtFromdate" ControlToValidate="txtTodate"
                                                Display="Dynamic" ErrorMessage="To Date Should be Greater Than From Date " Operator="GreaterThanEqual"
                                                Type="Date" ValidationGroup="Val3"></asp:CompareValidator><asp:RequiredFieldValidator ID="rfvTdate" runat="server"
                                                    ControlToValidate="txtTodate" Display="Dynamic" ErrorMessage="Select to Date" ValidationGroup="Val3"></asp:RequiredFieldValidator></td>
                                        <td  style="height: 19px">
                                            <asp:TextBox ID="txtTodate" runat="server" CssClass="clstextfield" Width="96%"  MaxLength="10"></asp:TextBox>
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center"   colspan="4" style="height: 24px">
                                            <asp:Button ID="btnFornonemp" runat="server" CssClass="button" Text="Submit" Width="100px" ValidationGroup="Val3" /></td>
                                    </tr>
                                </table>
                          </asp:Panel>
                        </td>
                      
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 17px; width: 10px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                    ShowSummary="False" Width="240px" />
            </asp:Panel>
          
        </div>
        <asp:textbox id="txtRMid" runat="server" Width="136px" CssClass="clstextfield" Visible="False" ReadOnly="True" BorderWidth="1px"></asp:textbox>
</asp:Content>

