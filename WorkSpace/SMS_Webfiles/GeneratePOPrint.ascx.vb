Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports NumToWordRupees
Partial Class Controls_GeneratePOPrint
    Inherits System.Web.UI.UserControl
    Dim ntw As New NumToWordRupees




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnprint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
        Dim reqid = Request.QueryString("id")
        ' BindVendorDetails()
        BindOtherDetails(reqid)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEMPO_DETAILS_GetByPO")
        sp.Command.AddParameter("@PO", reqid, DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        Dim total As Double = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim txtTotPrice As Label = DirectCast(row.FindControl("txtTotPrice"), Label)
            Dim tot As Double
            tot = CDbl(txtTotPrice.Text)
            total = total + tot

            'lblTotalPrice.Text = total
        Next
        Dim lblTotalPrice As Label = DirectCast(gvItems.FooterRow.FindControl("lblTotalPrice"), Label)
        Dim lblTotalPriceInw As Label = DirectCast(gvItems.FooterRow.FindControl("lblTotalPriceInw"), Label)
        lblTotalPrice.Text = Convert.ToDecimal(total).ToString("#,##0.00")
        Try

            lblTotalPriceInw.Text = ntw.AmtInWord(lblTotalPrice.Text)
        Catch ex As Exception

        End Try
        'Response.Write(total)
    End Sub
    Private Sub BindOtherDetails(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_GetBYPOPRINT")
        sp.Command.AddParameter("@PO", ReqId, DbType.String)
    
        Dim dr As SqlDataReader = sp.GetReader()
        If dr.Read() Then
            'txtPORaisedBy.Text = dr("AIP_AUR_ID")
            'txtRemarks.Text=
            'txtRequesterId.Text = dr("AIP_AUR_ID")
            'txtRequesterName.Text = dr("AIP_AUR_ID")
            'txtRequestorRemarks.Text = dr("AIP_PO_REMARKS")
            'txtDeptId.Text = dr("AIP_DEPT")
            'txtLocation.Text = dr("AIP_LOCATION")
            'txtAdvance.Text = dr("AIPD_PLI")
            'txtCommissioning.Text = dr("AIPD_PAY_COMMISSION")
            'txtCst.Text = dr("AIPD_TAX_CST")
            'txtDelivery.Text = dr("AIPD_PAY_ONDELIVERY")
            'txtDOD.SelectedDate = CDate(dr("AIPD_EXPECTED_DATEOF_DELIVERY"))
            'txtExchange.Text = dr("AIPD_EXCHANGE_CHARGES")
            'txtFFIP.Text = dr("AIPD_TAX_FFIP")
            'txtInstallation.Text = dr("AIPD_PAY_INSTALLATION")
            'txtOctrai.Text = dr("AIPD_TAX_OCTRAI")
            'txtOthers.Text = dr("AIPD_TAX_OTHERS")
            'txtPayments.Text = dr("AIPD_PAY_OTHERS")
            'txtRetention.Text = dr("AIPD_PAY_RETENTION")
            'txtServiceTax.Text = dr("AIPD_TAX_SERVICETAX")
            'txtTotalCost.Text = dr("AIPD_TOTALCOST")
            'txtWst.Text = dr("AIPD_TAX_WST")
            lblVendorName.Text = dr("AIPD_VENDORNAME")
            lblVendorAdd.Text = dr("AIPD_DADDRESS")
            lblPONo.Text = dr("AIP_PO_ID")
            lblPODate.Text = dr("AIPD_EXPECTED_DATEOF_DELIVERYFormatted")
            lblVendorCustName.Text = dr("AIPD_VENDORNAME")
            lblVendorPhone.Text = dr("AIPD_TELNO")
            lblSalesCont.Text = dr("AIP_AUR_ID")
            lblPhone.Text = dr("AUR_EXTENSION")
            'lblVnedorAccnt.Text = dr("AUR_EXTENSION")
            'txtVenCode.Text = dr("AIP_AVR_CODE")

            'Dim li As ListItem = ddlVendor.Items.FindByValue(Trim(CStr(dr("AIP_AVR_CODE"))))
            'If Not li Is Nothing Then
            '    li.Selected = True
            'End If
        End If
    End Sub

    Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound
        Dim total As Integer = 0
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                total += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "AIPD_RATE")) * Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "AIPD_ACT_QTY"))
            Catch ex As Exception
                total = 0
            End Try
        End If
        If e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalPrice As Label = e.Row.FindControl("lblTotalPrice")
            lblTotalPrice.Text = total
        End If

    End Sub
    '        If(e.Row.RowType==DataControlRowType.DataRow)
    '{
    'total += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Amount"));
    '}
    'if(e.Row.RowType==DataControlRowType.Footer)
    '{
    'Label lblamount = (Label)e.Row.FindControl("lblTotal");
    'lblamount.Text = total.ToString();
    '}
    '    End Sub


End Class
