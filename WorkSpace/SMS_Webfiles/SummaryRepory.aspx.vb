Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Partial Class WorkSpace_SMS_Webfiles_SummaryRepory
    Inherits System.Web.UI.Page
    Dim obj As clsMasters
    Private grdTotal As Decimal = 0
    Private grdTotal1 As Decimal = 0
    Private grdTotal2 As Decimal = 0
    Private grdTotal3 As Decimal = 0
    Private grdTotal4 As Decimal = 0

    Private grdCAP_WS As Decimal = 0
    Private grdCAP_FC As Decimal = 0
    Private grdCAP_HC As Decimal = 0
    Private grdCAP_TOTAL As Decimal = 0
    Private grdOCC_WS As Decimal = 0
    Private grdOCC_FC As Decimal = 0
    Private grdOCC_HC As Decimal = 0
    Private grdOCC_TOTAL As Decimal = 0
    Private grdVACANT_WS As Decimal = 0
    Private grdVACANT_FC As Decimal = 0
    Private grdVACANT_HC As Decimal = 0
    Private grdVACANT_TOTAL As Decimal = 0
    Private grdTOTAL_WS As Decimal = 0
    Private grdTOTAL_FC As Decimal = 0
    Private grdTOTAL_HC As Decimal = 0
    Private grdTOTAL_TOTAL As Decimal = 0



    Protected Sub gdavail_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdavail.PageIndexChanging

    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging

    End Sub
    Public Sub loadlocation()
        ddlLocation.Items.Clear()
        strSQL = "usp_getActiveLocation"
        ' BindCombo(strSQL, ddlLocation, "LCM_NAME", "LCM_CODE")
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
        ddlLocation.Items.Insert(1, "--All--")
        ddlLocation.SelectedIndex = 0
    End Sub
    Public Sub LoadCity()
        strSQL = "usp_getcity"
        BindCombo(strSQL, ddlCity, "cty_NAME", "cty_code")
        BindCombo(strSQL, ddlCty, "cty_NAME", "cty_code")
        ddlCity.SelectedIndex = 0
        ddlCty.SelectedIndex = 0
        ddlCity.Items.Insert(1, "--All--")
        ddlCty.Items.Insert(1, "--All--")
    End Sub
 
    Protected Sub rblstSummary_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblstSummary.SelectedIndexChanged
        If rblstSummary.SelectedValue = 1 Then
            trCity.Visible = False
            trLocation.Visible = False
            trCityCriteria.Visible = True
            trLocCity.Visible = False
            trTowerWise.Visible = False
            trTowerCity.Visible = False
            trBusinessUnitWise.Visible = False
        ElseIf rblstSummary.SelectedValue = 2 Then

            trCity.Visible = False
            trLocation.Visible = False
            trCityCriteria.Visible = False
            trLocCity.Visible = True
            trTowerWise.Visible = False
            trTowerCity.Visible = False
            trBusinessUnitWise.Visible = False
            LoadCity()
        ElseIf rblstSummary.SelectedValue = 3 Then
            trCity.Visible = False
            trLocation.Visible = False
            trCityCriteria.Visible = False
            trLocCity.Visible = False
            trBusinessUnitWise.Visible = False
            'loadgrid7()
            LoadTowerCity()
            trTowerCity.Visible = True

        ElseIf rblstSummary.SelectedValue = 4 Then
            trCity.Visible = False
            trLocation.Visible = False
            trCityCriteria.Visible = False
            trLocCity.Visible = False
            trBusinessUnitWise.Visible = True
            trTowerWise.Visible = False
            trTowerCity.Visible = False
            FillBusinessUnit()
            'FillGrid()
        End If
    End Sub
    Public Sub LoadTowerCity()
        strSQL = "usp_getcity"
        BindCombo(strSQL, ddlTowerCity, "cty_NAME", "cty_code")
        ddlTowerCity.SelectedIndex = 0
        ddlTowerCity.Items.Insert(1, "--All--")
    End Sub
    Public Sub FillBusinessUnit()
        Try
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_get_VER_NAME")
            ddlBusinessUnit.DataTextField = "VER_NAME"
            ddlBusinessUnit.DataValueField = "VER_CODE"
            ddlBusinessUnit.DataSource = ds
            ddlBusinessUnit.DataBind()
            ddlBusinessUnit.Items.Insert(0, "--All--")
        Catch ex As Exception

        End Try
    End Sub

    Public Sub FillGrid()
        Try
            Dim spCount1 As New SqlParameter("@VER_NAME", SqlDbType.VarChar, 500)
            spCount1.Value = ddlBusinessUnit.SelectedItem.Value
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_TotalReport_VER_NAME", spCount1)
            gvBusinessUnit.DataSource = ds
            gvBusinessUnit.DataBind()
        Catch ex As Exception

        End Try
    End Sub



    Private Sub loadgrid7()

        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        Dim spCount4 As New SqlParameter("@LOC_TYPE", SqlDbType.VarChar, 500)
        spCount.Value = 7
        Dim MCity As String = ""

        If ddlTowerCity.SelectedItem.Value <> "--All--" Then
            MCity = ddlTowerCity.SelectedItem.Value
        End If


        spCount1.Value = MCity
        spCount2.Value = String.Empty
        spCount3.Value = String.Empty
        spCount4.Value = ""

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status_ByType", spCount, spCount1, spCount2, spCount3, spCount4)
        gvTower_STPI.DataSource = ds.Tables(0)
        gvTower_STPI.DataBind()

        spCount4.Value = ""
        Dim ds_sez As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status_ByType", spCount, spCount1, spCount2, spCount3, spCount4)
        gvTower_SEZ.DataSource = ds_sez.Tables(0)
        gvTower_SEZ.DataBind()

        If gvTower_SEZ.Rows.Count > 0 Or gvTower_STPI.Rows.Count > 0 Then
            trTowerWise.Visible = True
            lblMsg.Text = String.Empty
            trTowerCity.Visible = True
        Else
            lblMsg.Text = "No records found for your search criteria"
            Exit Sub
        End If
    End Sub

    Private Sub loadgrid1()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        spCount.Value = 1
        spCount1.Value = ddlCty.SelectedItem.Value.Trim()
        spCount2.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount3.Value = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        GridView1.DataSource = ds.Tables(0)
        GridView1.DataBind()
    End Sub
    Private Sub loadgrid2()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        spCount.Value = 2
        spCount1.Value = ddlCty.SelectedItem.Value.Trim()
        spCount2.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount3.Value = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        GridView1.DataSource = ds.Tables(0)
        GridView1.DataBind()
    End Sub
    Private Sub loadgrid3()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        spCount.Value = 3
        spCount1.Value = ddlCty.SelectedItem.Value.Trim()
        spCount2.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount3.Value = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        GridView1.DataSource = ds.Tables(0)
        GridView1.DataBind()
    End Sub
    Private Sub loadgrid9()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        spCount.Value = 9
        spCount1.Value = ddlCty.SelectedItem.Value.Trim()
        spCount2.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount3.Value = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        GridView1.DataSource = ds.Tables(0)
        GridView1.DataBind()
    End Sub
    Private Sub loadgrid4()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        spCount.Value = 4
        spCount1.Value = ddlCty.SelectedItem.Value.Trim()
        spCount2.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount3.Value = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        GridView1.DataSource = ds.Tables(0)
        GridView1.DataBind()
    End Sub
    Private Sub loadgrid5()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        spCount.Value = 5
        spCount1.Value = ddlCty.SelectedItem.Value.Trim()
        spCount2.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount3.Value = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        GridView1.DataSource = ds.Tables(0)
        GridView1.DataBind()
    End Sub
    Private Sub loadgrid()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        spCount.Value = 1
        spCount1.Value = String.Empty
        spCount2.Value = String.Empty

        spCount3.Value = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        gdavail.DataSource = ds.Tables(0)
        gdavail.DataBind()


        spCount3.Value = ""
        Dim ds_sez As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        gdavail_SEZ.DataSource = ds_sez.Tables(0)
        gdavail_SEZ.DataBind()



    End Sub

    Private Sub loadgrid6()
        Dim spCount As New SqlParameter("@i_Status", SqlDbType.Int)
        Dim spCount1 As New SqlParameter("@vc_city", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@vc_loc", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@vc_Loctype", SqlDbType.VarChar, 500)
        Dim spCount4 As New SqlParameter("@LOC_TYPE", SqlDbType.VarChar, 500)
        spCount.Value = 3
        spCount1.Value = ddlCity.SelectedItem.Value.Trim()
        spCount2.Value = String.Empty
        spCount3.Value = String.Empty
        spCount4.Value = ""

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"USP_City_Location_Seats_Status", spCount, spCount1, spCount2, spCount3)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status_ByType", spCount, spCount1, spCount2, spCount3, spCount4)
        gdavail.DataSource = ds.Tables(0)
        gdavail.DataBind()




        spCount4.Value = ""
        Dim ds_sez As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_City_Location_Seats_Status_ByType", spCount, spCount1, spCount2, spCount3, spCount4)
        gdavail_SEZ.DataSource = ds_sez.Tables(0)
        gdavail_SEZ.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            LoadCity()
            'loadlocation()
            ' LoadLocationType()
            trCityCriteria.Visible = False
            trLocCity.Visible = False
            trTowerWise.Visible = False
            trCity.Visible = False
            trTowerCity.Visible = False
        End If
    End Sub
    Protected Sub gdavail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdavail.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available Seats"))
            grdTotal = grdTotal + rowTotal
            Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allotted Seats"))
            grdTotal1 = grdTotal1 + rowTotal1
            Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Occupied Seats"))
            grdTotal2 = grdTotal2 + rowTotal2
            Dim rowTotal3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available for Allocation"))
            grdTotal3 = grdTotal3 + rowTotal3
            Dim rowTotal4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allocated but not Occupied"))
            grdTotal4 = grdTotal4 + rowTotal4
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            lbl.Text = grdTotal.ToString()
            lbl.ForeColor = Drawing.Color.Black
            lbl.Font.Bold = True

            Dim lbl1 As Label = DirectCast(e.Row.FindControl("lblAllotted"), Label)
            lbl1.Text = grdTotal1.ToString()
            lbl1.ForeColor = Drawing.Color.Black
            lbl1.Font.Bold = True
            Dim lbl2 As Label = DirectCast(e.Row.FindControl("lblOccupied"), Label)
            lbl2.Text = grdTotal2.ToString()
            lbl2.ForeColor = Drawing.Color.Black
            lbl2.Font.Bold = True
            Dim lbl3 As Label = DirectCast(e.Row.FindControl("lblAvailAlloc"), Label)
            lbl3.Text = grdTotal3.ToString()
            lbl3.ForeColor = Drawing.Color.Black
            lbl3.Font.Bold = True
            Dim lbl4 As Label = DirectCast(e.Row.FindControl("lblAllocNotOcc"), Label)
            lbl4.Text = grdTotal4.ToString()
            lbl4.ForeColor = Drawing.Color.Black
            lbl4.Font.Bold = True
        End If
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available Seats"))
            grdTotal = grdTotal + rowTotal
            Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allotted Seats"))
            grdTotal1 = grdTotal1 + rowTotal1
            Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Occupied Seats"))
            grdTotal2 = grdTotal2 + rowTotal2
            Dim rowTotal3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available for Allocation"))
            grdTotal3 = grdTotal3 + rowTotal3
            Dim rowTotal4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allocated but not Occupied"))
            grdTotal4 = grdTotal4 + rowTotal4
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            lbl.Text = grdTotal.ToString()
            lbl.ForeColor = Drawing.Color.Black
            lbl.Font.Bold = True

            Dim lbl1 As Label = DirectCast(e.Row.FindControl("lblAllotted"), Label)
            lbl1.Text = grdTotal1.ToString()
            lbl1.ForeColor = Drawing.Color.Black
            lbl1.Font.Bold = True
            Dim lbl2 As Label = DirectCast(e.Row.FindControl("lblOccupied"), Label)
            lbl2.Text = grdTotal2.ToString()
            lbl2.ForeColor = Drawing.Color.Black
            lbl2.Font.Bold = True
            Dim lbl3 As Label = DirectCast(e.Row.FindControl("lblAvailAlloc"), Label)
            lbl3.Text = grdTotal3.ToString()
            lbl3.ForeColor = Drawing.Color.Black
            lbl3.Font.Bold = True
            Dim lbl4 As Label = DirectCast(e.Row.FindControl("lblAllocNotOcc"), Label)
            lbl4.Text = grdTotal4.ToString()
            lbl4.ForeColor = Drawing.Color.Black
            lbl4.Font.Bold = True
        End If
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        If (ddlCty.SelectedItem.Text = "--All--" And ddlLocation.SelectedItem.Text = "--All--") Then
            loadgrid1()
        ElseIf (ddlCty.SelectedItem.Text = "--All--" And ddlLocation.SelectedItem.Text <> "--All--") Then
            loadgrid2()
        ElseIf (ddlCty.SelectedItem.Text <> "--All--" And ddlLocation.SelectedItem.Text = "--All--") Then
            loadgrid3()
        ElseIf (ddlCty.SelectedItem.Text <> "--All--" And ddlLocation.SelectedItem.Text = "--All--") Then
            loadgrid4()
        ElseIf (ddlCty.SelectedItem.Text <> "--All--" And ddlLocation.SelectedItem.Text <> "--All--") Then
            loadgrid5()
        ElseIf (ddlCty.SelectedItem.Text <> "--All--" And ddlLocation.SelectedItem.Text <> "--All--") Then
            loadgrid5()
        ElseIf (ddlCty.SelectedItem.Text = "--All--" And ddlLocation.SelectedItem.Text = "--All--") Then
            loadgrid9()
        Else
            loadgrid1()
        End If
        If ddlCty.SelectedIndex <= 0 Then
            lblMsg.Text = "Please Select Valid City"
            Exit Sub
            'ElseIf (ddlLocType.SelectedItem.Text = "--Select--") Then
            '    lblMsg.Text = "Please Select Valid Location Type"
            '    Exit Sub
        ElseIf (ddlLocation.SelectedItem.Text = "--Select--") Then
            lblMsg.Text = "Please Select Valid Location"
            Exit Sub
        End If
        If GridView1.Rows.Count > 0 Then
            trLocation.Visible = True
            lblMsg.Text = String.Empty
        Else
            lblMsg.Text = "No records found for your search criteria"
            Exit Sub
        End If

    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedItem.Text = "--All--" Then
            loadgrid()
            'loadgrid6()
        Else

            loadgrid6()
        End If

        If gdavail.Rows.Count > 0 Then
            trCity.Visible = True
            lblMsg.Text = String.Empty
        Else
            lblMsg.Text = "No records found for your search criteria"
            Exit Sub
        End If
    End Sub
    Protected Sub ddlCty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCty.SelectedIndexChanged
        'strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCty.SelectedValue & "'"
        'BindCombo(strSQL, ddlLocation, "LCM_name", "lcm_code")
        ' LoadLocationType()
        trLocation.Visible = False
        'loadlocation()
        ' LoadLocationType()
        ddlLocation.Items.Clear()
        strSQL = "select lcm_code,lcm_name from " & Session("TENANT") & "." & "location where lcm_cty_id='" & ddlCty.SelectedValue & "'"
        BindCombo(strSQL, ddlLocation, "lcm_name", "lcm_code")
        ddlLocation.Items.Insert(1, "--All--")
    End Sub

    'Protected Sub ddlLocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocType.SelectedIndexChanged

    '    If ddlLocType.SelectedItem.Text = "--All--" Then
    '        ' loadlocation()
    '        If ddlCty.SelectedItem.Text = "--All--" Then

    '            ddlLocation.Items.Insert(0, "--All--")
    '        Else
    '            ddlLocation.Items.Clear()
    '            strSQL = "select lcm_code,lcm_name from " & Session("TENANT") & "."  & "location where lcm_cty_id='" & ddlCty.SelectedValue & "'"
    '            BindCombo(strSQL, ddlLocation, "lcm_name", "lcm_code")
    '            ddlLocation.Items.Insert(1, "--All--")
    '        End If

    '    Else
    '        If ddlCty.SelectedItem.Text = "--All--" Then
    '            strSQL = "select lcm_code,lcm_name from " & Session("TENANT") & "."  & "location where lcm_code in (select loc_id from " & Session("TENANT") & "."  & "location_type where loc_type='" & ddlLocType.SelectedItem.Text.Trim() & "')"
    '            BindCombo(strSQL, ddlLocation, "lcm_name", "lcm_code")
    '            ddlLocation.Items.Insert(1, "--All--")
    '        Else
    '            ddlLocation.Items.Clear()
    '            strSQL = "select lcm_code,lcm_name from " & Session("TENANT") & "."  & "location where lcm_cty_id='" & ddlCty.SelectedValue & "' and lcm_code in (select loc_id from " & Session("TENANT") & "."  & "location_type where loc_type='" & ddlLocType.SelectedItem.Text.Trim() & "')"
    '            BindCombo(strSQL, ddlLocation, "lcm_name", "lcm_code")
    '            ddlLocation.Items.Insert(0, "--All--")
    '        End If

    '    End If


    'End Sub

    Dim grdTotal_SEZ As Decimal = 0
    Dim grdTotal1_SEZ As Decimal = 0
    Dim grdTotal2_SEZ As Decimal = 0
    Dim grdTotal3_SEZ As Decimal = 0
    Dim grdTotal4_SEZ As Decimal = 0

    Dim grdTotal_Tower_STPI As Decimal = 0
    Dim grdTotal1_Tower_STPI As Decimal = 0
    Dim grdTotal2_Tower_STPI As Decimal = 0
    Dim grdTotal3_Tower_STPI As Decimal = 0
    Dim grdTotal4_Tower_STPI As Decimal = 0
    Dim grdTotal5_Tower_STPI As Decimal = 0

    Protected Sub gdavail_SEZ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdavail_SEZ.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available Seats"))
            grdTotal_SEZ = grdTotal_SEZ + rowTotal
            Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allotted Seats"))
            grdTotal1_SEZ = grdTotal1_SEZ + rowTotal1
            Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Occupied Seats"))
            grdTotal2_SEZ = grdTotal2_SEZ + rowTotal2
            Dim rowTotal3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available for Allocation"))
            grdTotal3_SEZ = grdTotal3_SEZ + rowTotal3
            Dim rowTotal4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allocated but not Occupied"))
            grdTotal4_SEZ = grdTotal4_SEZ + rowTotal4
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            lbl.Text = grdTotal_SEZ.ToString()
            lbl.ForeColor = Drawing.Color.Black
            lbl.Font.Bold = True

            Dim lbl1 As Label = DirectCast(e.Row.FindControl("lblAllotted"), Label)
            lbl1.Text = grdTotal1_SEZ.ToString()
            lbl1.ForeColor = Drawing.Color.Black
            lbl1.Font.Bold = True
            Dim lbl2 As Label = DirectCast(e.Row.FindControl("lblOccupied"), Label)
            lbl2.Text = grdTotal2_SEZ.ToString()
            lbl2.ForeColor = Drawing.Color.Black
            lbl2.Font.Bold = True
            Dim lbl3 As Label = DirectCast(e.Row.FindControl("lblAvailAlloc"), Label)
            lbl3.Text = grdTotal3_SEZ.ToString()
            lbl3.ForeColor = Drawing.Color.Black
            lbl3.Font.Bold = True
            Dim lbl4 As Label = DirectCast(e.Row.FindControl("lblAllocNotOcc"), Label)
            lbl4.Text = grdTotal4_SEZ.ToString()
            lbl4.ForeColor = Drawing.Color.Black
            lbl4.Font.Bold = True
        End If
    End Sub

    Protected Sub gvTower_STPI_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTower_STPI.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available Seats"))
            grdTotal_Tower_STPI = grdTotal_Tower_STPI + rowTotal
            Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allotted Seats"))
            grdTotal1_Tower_STPI = grdTotal1_Tower_STPI + rowTotal1
            Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Occupied Seats"))
            grdTotal2_Tower_STPI = grdTotal2_Tower_STPI + rowTotal2
            Dim rowTotal3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available for Allocation"))
            grdTotal3_Tower_STPI = grdTotal3_Tower_STPI + rowTotal3
            Dim rowTotal4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allocated but not Occupied"))
            grdTotal4_Tower_STPI = grdTotal4_Tower_STPI + rowTotal4
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            lbl.Text = grdTotal_Tower_STPI.ToString()
            lbl.ForeColor = Drawing.Color.Black
            lbl.Font.Bold = True

            Dim lbl1 As Label = DirectCast(e.Row.FindControl("lblAllotted"), Label)
            lbl1.Text = grdTotal1_Tower_STPI.ToString()
            lbl1.ForeColor = Drawing.Color.Black
            lbl1.Font.Bold = True
            Dim lbl2 As Label = DirectCast(e.Row.FindControl("lblOccupied"), Label)
            lbl2.Text = grdTotal2_Tower_STPI.ToString()
            lbl2.ForeColor = Drawing.Color.Black
            lbl2.Font.Bold = True
            Dim lbl3 As Label = DirectCast(e.Row.FindControl("lblAvailAlloc"), Label)
            lbl3.Text = grdTotal3_Tower_STPI.ToString()
            lbl3.ForeColor = Drawing.Color.Black
            lbl3.Font.Bold = True
            Dim lbl4 As Label = DirectCast(e.Row.FindControl("lblAllocNotOcc"), Label)
            lbl4.Text = grdTotal4_Tower_STPI.ToString()
            lbl4.ForeColor = Drawing.Color.Black
            lbl4.Font.Bold = True
        End If
    End Sub

    Dim grdTotal_Tower_SEZ As Decimal = 0
    Dim grdTotal1_Tower_SEZ As Decimal = 0
    Dim grdTotal2_Tower_SEZ As Decimal = 0
    Dim grdTotal3_Tower_SEZ As Decimal = 0
    Dim grdTotal4_Tower_SEZ As Decimal = 0


    Protected Sub gvTower_SEZ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTower_SEZ.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowTotal As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available Seats"))
            grdTotal_Tower_SEZ = grdTotal_Tower_SEZ + rowTotal
            Dim rowTotal1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allotted Seats"))
            grdTotal1_Tower_SEZ = grdTotal1_Tower_SEZ + rowTotal1
            Dim rowTotal2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Occupied Seats"))
            grdTotal2_Tower_SEZ = grdTotal2_Tower_SEZ + rowTotal2
            Dim rowTotal3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Available for Allocation"))
            grdTotal3_Tower_SEZ = grdTotal3_Tower_SEZ + rowTotal3
            Dim rowTotal4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Allocated but not Occupied"))
            grdTotal4_Tower_SEZ = grdTotal4_Tower_SEZ + rowTotal4
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lbl As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
            lbl.Text = grdTotal_Tower_SEZ.ToString()
            lbl.ForeColor = Drawing.Color.Black
            lbl.Font.Bold = True

            Dim lbl1 As Label = DirectCast(e.Row.FindControl("lblAllotted"), Label)
            lbl1.Text = grdTotal1_Tower_SEZ.ToString()
            lbl1.ForeColor = Drawing.Color.Black
            lbl1.Font.Bold = True
            Dim lbl2 As Label = DirectCast(e.Row.FindControl("lblOccupied"), Label)
            lbl2.Text = grdTotal2_Tower_SEZ.ToString()
            lbl2.ForeColor = Drawing.Color.Black
            lbl2.Font.Bold = True
            Dim lbl3 As Label = DirectCast(e.Row.FindControl("lblAvailAlloc"), Label)
            lbl3.Text = grdTotal3_Tower_SEZ.ToString()
            lbl3.ForeColor = Drawing.Color.Black
            lbl3.Font.Bold = True
            Dim lbl4 As Label = DirectCast(e.Row.FindControl("lblAllocNotOcc"), Label)
            lbl4.Text = grdTotal4_Tower_SEZ.ToString()
            lbl4.ForeColor = Drawing.Color.Black
            lbl4.Font.Bold = True
        End If
    End Sub


    Public Function GetTotalWST_HCB_FCB(ByVal SPC_LAYER As String) As Integer
        'Dim spCount1 As New SqlParameter("@SPC_BDG_ID", SqlDbType.VarChar, 500)
        'Dim spCount2 As New SqlParameter("@LCM_CTY_ID", SqlDbType.VarChar, 500)
        Dim spCount1 As New SqlParameter("@SPC_LAYER", SqlDbType.VarChar, 500)


        spCount1.Value = SPC_LAYER
        Dim MCount As Integer = 0
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetTotalWST_HCB_FCB_sum_rep", spCount1)

        If ds.Tables(0).Rows.Count > 0 Then
            MCount = ds.Tables(0).Rows(0).Item("CNT")
        End If

        Return MCount
    End Function


    Protected Sub gvBusinessUnit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBusinessUnit.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Try

                Dim rowTOTAL_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_WS"))
                grdTOTAL_WS = grdTOTAL_WS + rowTOTAL_WS

                Dim rowTOTAL_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_FC"))
                grdTOTAL_FC = grdTOTAL_FC + rowTOTAL_FC

                Dim rowTOTAL_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_HC"))
                grdTOTAL_HC = grdTOTAL_HC + rowTOTAL_HC

                Dim rowTOTAL_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_TOTAL"))
                grdTOTAL_TOTAL = grdTOTAL_TOTAL + rowTOTAL_TOTAL


                Dim rowCAP_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_WS"))
                grdCAP_WS = grdCAP_WS + rowCAP_WS

                Dim rowCAP_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_FC"))
                grdCAP_FC = grdCAP_FC + rowCAP_FC

                Dim rowCAP_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_HC"))
                grdCAP_HC = grdCAP_HC + rowCAP_HC

                Dim rowCAP_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_TOTAL"))
                grdCAP_TOTAL = grdCAP_TOTAL + rowCAP_TOTAL

                Dim rowOCC_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_WS"))
                grdOCC_WS = grdOCC_WS + rowOCC_WS

                Dim rowOCC_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_FC"))
                grdOCC_FC = grdOCC_FC + rowOCC_FC

                Dim rowOCC_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_HC"))
                grdOCC_HC = grdOCC_HC + rowOCC_HC
                Dim rowOCC_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_TOTAL"))
                grdOCC_TOTAL = grdOCC_TOTAL + rowOCC_TOTAL
                Dim rowVACANT_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_WS"))
                grdVACANT_WS = grdVACANT_WS + rowVACANT_WS
                Dim rowVACANT_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_FC"))
                grdVACANT_FC = grdVACANT_FC + rowVACANT_FC

                Dim rowVACANT_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_HC"))
                grdVACANT_HC = grdVACANT_HC + rowVACANT_HC

                Dim rowVACANT_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_TOTAL"))
                grdVACANT_TOTAL = grdVACANT_TOTAL + rowVACANT_TOTAL
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


        End If



        If e.Row.RowType = DataControlRowType.Footer Then
            Dim grdTotalTOTAL_TOTAL As Integer = 0
            Dim lblTotalTOTAL_WS As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_WS"), Label)
            lblTotalTOTAL_WS.Text = grdTOTAL_WS 'GetTotalWST_HCB_FCB("WST").ToString()
            lblTotalTOTAL_WS.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_WS.Font.Bold = True

            Dim lblTotalTOTAL_FC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_FC"), Label)
            lblTotalTOTAL_FC.Text = grdTOTAL_FC  'GetTotalWST_HCB_FCB("FCB").ToString()
            lblTotalTOTAL_FC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_FC.Font.Bold = True

            Dim lblTotalTOTAL_HC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_HC"), Label)
            lblTotalTOTAL_HC.Text = grdTOTAL_HC ' GetTotalWST_HCB_FCB("HCB").ToString()
            lblTotalTOTAL_HC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_HC.Font.Bold = True

            Dim lblTotalTOTAL_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_TOTAL"), Label)
            lblTotalTOTAL_TOTAL.Text = Integer.Parse(lblTotalTOTAL_HC.Text) + Integer.Parse(lblTotalTOTAL_FC.Text) + Integer.Parse(lblTotalTOTAL_WS.Text)
            lblTotalTOTAL_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_TOTAL.Font.Bold = True


            Dim lblTotalCAP_WS As Label = DirectCast(e.Row.FindControl("lblTotalCAP_WS"), Label)
            lblTotalCAP_WS.Text = grdCAP_WS.ToString()
            lblTotalCAP_WS.ForeColor = Drawing.Color.Black
            lblTotalCAP_WS.Font.Bold = True

            Dim lblTotalCAP_FC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_FC"), Label)
            lblTotalCAP_FC.Text = grdCAP_FC.ToString()
            lblTotalCAP_FC.ForeColor = Drawing.Color.Black
            lblTotalCAP_FC.Font.Bold = True

            Dim lblTotalCAP_HC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_HC"), Label)
            lblTotalCAP_HC.Text = grdCAP_HC.ToString()
            lblTotalCAP_HC.ForeColor = Drawing.Color.Black
            lblTotalCAP_HC.Font.Bold = True


            Dim lblTotalCAP_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalCAP_TOTAL"), Label)
            lblTotalCAP_TOTAL.Text = grdCAP_TOTAL.ToString()
            lblTotalCAP_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalCAP_TOTAL.Font.Bold = True

            Dim lblTotalOCC_WS As Label = DirectCast(e.Row.FindControl("lblTotalOCC_WS"), Label)
            lblTotalOCC_WS.Text = grdOCC_WS.ToString()
            lblTotalOCC_WS.ForeColor = Drawing.Color.Black
            lblTotalOCC_WS.Font.Bold = True


            Dim lblTotalOCC_FC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_FC"), Label)
            lblTotalOCC_FC.Text = grdOCC_FC.ToString()
            lblTotalOCC_FC.ForeColor = Drawing.Color.Black
            lblTotalOCC_FC.Font.Bold = True

            Dim lblTotalOCC_HC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_HC"), Label)
            lblTotalOCC_HC.Text = grdOCC_HC.ToString()
            lblTotalOCC_HC.ForeColor = Drawing.Color.Black
            lblTotalOCC_HC.Font.Bold = True

            Dim lblTotalOCC_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalOCC_TOTAL"), Label)
            lblTotalOCC_TOTAL.Text = grdOCC_TOTAL.ToString()
            lblTotalOCC_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalOCC_TOTAL.Font.Bold = True

            Dim lblTotalVACANT_WS As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_WS"), Label)
            lblTotalVACANT_WS.Text = grdVACANT_WS.ToString()
            lblTotalVACANT_WS.ForeColor = Drawing.Color.Black
            lblTotalVACANT_WS.Font.Bold = True

            Dim lblTotalVACANT_FC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_FC"), Label)
            lblTotalVACANT_FC.Text = grdVACANT_FC.ToString()
            lblTotalVACANT_FC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_FC.Font.Bold = True


            Dim lblTotalVACANT_HC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_HC"), Label)
            lblTotalVACANT_HC.Text = grdVACANT_HC.ToString()
            lblTotalVACANT_HC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_HC.Font.Bold = True

            Dim lblTotalVACANT_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_TOTAL"), Label)
            lblTotalVACANT_TOTAL.Text = grdVACANT_TOTAL.ToString()
            lblTotalVACANT_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalVACANT_TOTAL.Font.Bold = True
        End If
    End Sub

    Protected Sub btnBusinessUnit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBusinessUnit.Click

        FillGrid()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        ExportPanel1.FileName = "SummaryReport"
    End Sub

    Protected Sub ddlTowerCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTowerCity.SelectedIndexChanged
        loadgrid7()
    End Sub

  
End Class
