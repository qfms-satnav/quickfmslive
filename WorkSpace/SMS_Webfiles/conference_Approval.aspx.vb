﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_conference_Approval
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If

        If Not Page.IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@SPACE_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ""
        param(1) = New SqlParameter("@FROM_DATE", SqlDbType.Date)
        param(1).Value = getoffsetdate(Date.Today)
        param(2) = New SqlParameter("@MODE", SqlDbType.Int)
        param(2).Value = 2

        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("SPACE_CONFERENCE_CHART", param)

        ObjSubSonic.BindGridView(gvitems, "SPACE_CONFERENCE_CHART", param)
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

End Class
