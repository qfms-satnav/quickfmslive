﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_Utility_Payments
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Not IsPostBack Then

            txtFDate.Attributes.Add("onClick", "displayDatePicker('" + txtFDate.ClientID + "')")
            txtFDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtTDate.Attributes.Add("onClick", "displayDatePicker('" + txtTDate.ClientID + "')")
            txtTDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            gvPropType.PageIndex = 0
            ddlutility.Enabled = True
            fillgrid()
            BindUtility()
            gvPropType.Visible = True
            obj.Bindlocation(ddlLocation)
            btnSubmit.Visible = True
            btnmodify.Visible = False
        End If
    End Sub
    Private Sub BindUtility()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_UTILITY")
        ddlutility.DataSource = sp.GetDataSet()
        ddlutility.DataTextField = "PN_UTILITY"
        ddlutility.DataValueField = "PN_TYPEID"
        ddlutility.DataBind()
        ddlutility.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim units As Decimal = 0
        If txtunits.Text = "" Then
            units = 0
        Else
            units = CDbl(txtunits.Text)
        End If

        Dim da As Decimal = 0
        If txtutility.Text = "" Then
            da = 0
        Else
            da = CDbl(txtutility.Text)

        End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_UTILITY_PAYMENTS")
        sp.Command.AddParameter("@UTILITY", ddlutility.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LOC", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMOUNT", da, DbType.Decimal)
        sp.Command.AddParameter("@UNITS", units, DbType.Decimal)
        sp.Command.AddParameter("@MODE", "ADD", DbType.String)
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.Date)
        sp.Command.AddParameter("@SNO", 0, DbType.Int32)

        Dim i As Integer = sp.ExecuteScalar()
        If i = 0 Then
            lblMsg.Text = "Utility Payment already exists between these dates"
        ElseIf i = 1 Then
            lblMsg.Text = "Utility Payment added succesfully"
        ElseIf i = 1 Then
            lblMsg.Text = "Utility Payment details modified succesfully"
        End If

        fillgrid()
        Cleardata()
        ' lblMsg.Text = "Payment Added Succesfully"
    End Sub
    Private Sub cleardata()
        ddlutility.SelectedIndex = -1
        ddlLocation.SelectedIndex = -1
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        txtFdate.Text = ""
        txtTdate.Text = ""
        txtunits.Text = ""
        txtutility.Text = ""
        ' lblMsg.Text = ""
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_UTILITY_PAYMENT")
        sp.Command.AddParameter("@PN_PROPERTYTYPE", "", DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvPropType.DataSource = ds
        gvPropType.DataBind()
    End Sub

    Protected Sub gvPropType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvPropType.PageIndexChanging
        gvPropType.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged


        If ddlLocation.SelectedIndex > 0 Then
            ddlTower.Items.Clear()
            BindTower(ddlLocation.SelectedItem.Value)
           
        Else
            ddlTower.Items.Clear()
            ddlFloor.Items.Clear()
        End If
        
    End Sub
    Private Sub bindtower(ByVal loc As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@loc_id", SqlDbType.NVarChar, 200)
        param(0).Value = loc
        objsubsonic.Binddropdown(ddlTower, "USP_getTowers", "TWR_NAME", "TWR_CODE", param)

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            ddlFloor.Items.Clear()
            Bindfloor(ddlLocation.SelectedItem.Value, ddlTower.SelectedItem.Value)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub
    Private Sub BindFloor(ByVal loc As String, ByVal tower As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@FLR_LOC_ID", SqlDbType.NVarChar, 200)
        param(0).Value = loc
        param(1) = New SqlParameter("@FLR_TWR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = tower
        objsubsonic.Binddropdown(ddlFloor, "GET_FLOOR_BYLOCTWR", "FLR_NAME", "FLR_CODE", param)
    End Sub

    Protected Sub gvPropType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPropType.RowCommand
        If e.CommandName = "EDIT" Then
            lblMsg.Text = ""
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblID"), Label)
            Dim lblloccode As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblloccode"), Label)
            Dim lbltwrcode As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lbltwrcode"), Label)
            Dim lblflrcode As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblflrcode"), Label)
            Dim lblunits As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblunits"), Label)
            Dim lblamount As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblamount"), Label)
            Dim lblfdate As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblfdate"), Label)
            Dim lbltdate As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lbltdate"), Label)
            Dim lblsno As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblsno"), Label)
            txtstore1.Text = lblsno.Text
            ddlutility.ClearSelection()
            ddlutility.Items.FindByValue(lblID.Text).Selected = True
            ddlutility.Enabled = False
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(lblloccode.Text).Selected = True
            bindtower(ddlLocation.SelectedValue)
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(lbltwrcode.Text).Selected = True
            BindFloor(ddlLocation.SelectedValue, ddlTower.SelectedValue)
            ddlFloor.ClearSelection()
            ddlFloor.Items.FindByValue(lblflrcode.Text).Selected = True
            txtunits.Text = lblunits.Text
            txtutility.Text = lblamount.Text
            txtFdate.Text = lblfdate.Text
            txtTdate.Text = lbltdate.Text
            btnSubmit.Visible = False
            btnmodify.Visible = True
        End If
    End Sub

   
    Protected Sub btnmodify_Click(sender As Object, e As EventArgs) Handles btnmodify.Click
        Dim units As Decimal = 0
        If txtunits.Text = "" Then
            units = 0
        Else
            units = CDbl(txtunits.Text)
        End If

        Dim da As Decimal = 0
        If txtutility.Text = "" Then
            da = 0
        Else
            da = CDbl(txtutility.Text)

        End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_UTILITY_PAYMENTS")
        sp.Command.AddParameter("@UTILITY", ddlutility.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LOC", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMOUNT", da, DbType.Decimal)
        sp.Command.AddParameter("@UNITS", units, DbType.Decimal)
        sp.Command.AddParameter("@MODE", "MODIFY", DbType.String)
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.Date)
        sp.Command.AddParameter("@SNO", CInt(txtstore1.Text), DbType.Int32)
        Dim i As Integer = sp.ExecuteScalar()
        If i = 0 Then
            lblMsg.Text = "Utility Payment already exists between these dates"
        ElseIf i = 1 Then
            lblMsg.Text = "Utility Payment added succesfully"
        ElseIf i = 1 Then
            lblMsg.Text = "Utility Payment details modified succesfully"
        End If

        fillgrid()
        cleardata()
        ' lblMsg.Text = "Payment Modified Succesfully"
    End Sub

    Protected Sub gvPropType_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvPropType.RowEditing

    End Sub
End Class
