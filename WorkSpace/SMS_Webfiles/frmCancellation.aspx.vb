Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmCancellation
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(AppSettings("logout"))
            End If
            If Not IsPostBack Then
                visibleTF(1)
                BindData()
                visibleTF(0)
            End If
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Private Sub BindData()
        Dim objdata As SqlDataReader

        strSQL = "SELECT URL_USR_ID,ROL_ACRONYM,URL_SCOPEMAP_ID " & _
           "FROM " & Session("TENANT") & "."  & "USER_ROLE," & Session("TENANT") & "."  & "ROLE " & _
           "WHERE URL_ROL_ID=ROL_ID AND ROL_ACRONYM IN ('OSSI','OSSA','GADMIIN','All') AND URL_USR_ID = '" & Session("uid") & "'"

        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        Dim locs As String = ""
        Dim rol As String = ""
        Dim userid As String
        While objdata.Read
            locs = locs & objdata("URL_scopemap_ID").ToString & ","
            userid = objdata("url_usr_id").ToString
            rol = objdata("rol_acronym").ToString
        End While
        objdata.Close()
        If rol <> "GAdmin" Then
            strSQL = "SELECT DISTINCT SRN_REQ_ID,SRN_REQ_DT,SRN_REM," & _
            " (SELECT DISTINCT LCM_NAME from " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE ) BDG_DESC1," & _
            " (SELECT  DISTINCT LCM_NAME from " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id , " & _
            " (SELECT DISTINCT STA_TITLE from " & Session("TENANT") & "."  & "Status where sta_id=srN_sta_id) STA_NAME " & _
            " FROM " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM " & _
            " WHERE SRN_AUR_ID ='" & Session("uid") & "' AND SRN_STA_ID in (5,10) " & _
            "  order by SRN_REQ_DT DESC"
        End If
        If rol <> "OSSA" Then
            strSQL = "SELECT DISTINCT SRN_REQ_ID,SRN_REQ_DT,SRN_REM," & _
            " (SELECT DISTINCT LCM_NAME from " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE ) BDG_DESC1," & _
            " (SELECT DISTINCT LCM_NAME from " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id , " & _
            " (SELECT DISTINCT STA_TITLE from " & Session("TENANT") & "."  & "Status where sta_id=srN_sta_id) STA_NAME " & _
            " FROM " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM " & _
            " WHERE SRN_AUR_ID ='" & Session("uid") & "' AND SRN_STA_ID in (5,10) " & _
            " order by SRN_REQ_DT DESC"
        Else
            strSQL = "SELECT DISTINCT SRN_REQ_ID,SRN_REQ_DT,SRN_REM," & _
            " (SELECT DISTINCT LCM_NAME from " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE ) BDG_DESC1," & _
            " (SELECT DISTINCT LCM_NAME from " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id , " & _
            " (SELECT DISTINCT STA_TITLE from " & Session("TENANT") & "."  & "Status where sta_id=SRN_sta_id) STA_NAME " & _
            " FROM " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM WHERE SRN_STA_ID in (5,10)" & _
            " order by SRN_REQ_DT DESC"
        End If
        grdReqList.Columns(7).Visible = True
        BindGridSet(strSQL, grdReqList)
        If grdReqList.Rows.Count > 0 Then
            Dim i As Integer
            grdReqList.Visible = True
            For i = 0 To grdReqList.Rows.Count - 1
                If grdReqList.Rows(i).Cells(7).Text = 1 Then
                    grdReqList.Rows(i).Cells(6).Text = "Employee(s)"
                ElseIf grdReqList.Rows(i).Cells(7).Text = 2 Then
                    grdReqList.Rows(i).Cells(6).Text = "Non Employee(s)"
                Else
                    grdReqList.Rows(i).Cells(6).Text = "Na"
                End If
            Next
            Label2.Visible = False
            lnkDelAll.Visible = True
        Else
            grdReqList.Visible = False
            lnkDelAll.Visible = False
            Label2.Visible = True
            Label2.Text = "No Requisitions For Cancellation"
        End If
        grdReqList.Columns(7).Visible = False
    End Sub


    Private Sub lnkDelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDelAll.Click
        Try
            If grdReqList.Rows.Count > 0 Then
                visibleTF(1)
                Label2.Visible = False
                Dim i As Integer
                Dim strtemp As String = "0"
                For i = 0 To grdReqList.Rows.Count - 1

                    If strtemp = "0" Then
                        strtemp = "'" & grdReqList.Rows(i).Cells(8).Text & "'"
                    Else
                        strtemp = strtemp & ",'" & grdReqList.Rows(i).Cells(8).Text & "'"
                    End If

                Next
                'strSQL = "UPDATE " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SET SRN_STA_ID = 8 WHERE SRN_REQ_ID in (" & strtemp & ")"
                'SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)

                strSQL1 = " delete from " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_EMPDETAILS where SSE_SRNREQ_ID  in (" & strtemp & ")"

                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL1)


                strSQL1 = "delete from " & AppSettings("APPDB") & "SMS_SPACEREQUISITION_DETAILS where SRD_SRNREQ_ID  in (" & strtemp & ")"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL1)

                strSQL1 = "delete from " & AppSettings("APPDB") & "SMS_SPACEREQUISITION where SRN_REQ_ID  in (" & strtemp & ")"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL1)

                visibleTF(0)
                Server.Transfer("frmSMSNewFinalpage.aspx?staid=CancelledAll&rid=" & strtemp & "")
            Else
                Label2.Visible = True
                Label2.Text = "No Requisitions For Cancellation"
            End If
            visibleTF(0)
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            ' fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub


    Protected Sub grdReqList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdReqList.PageIndexChanging
        Try
            visibleTF(1)
            grdReqList.PageIndex = e.NewPageIndex
            BindData()
            visibleTF(0)

        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            ' fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            visibleTF(0)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub visibleTF(ByVal status As Integer)
        If status = 1 Then
            grdReqList.Columns(2).Visible = True
            grdReqList.Columns(4).Visible = True
            grdReqList.Columns(7).Visible = True
            grdReqList.Columns(8).Visible = True
        Else
            grdReqList.Columns(2).Visible = False
            grdReqList.Columns(4).Visible = False
            grdReqList.Columns(7).Visible = False
            grdReqList.Columns(8).Visible = False

        End If


    End Sub

End Class
