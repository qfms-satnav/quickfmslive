﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CheckStatus.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_CheckStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table id="table4" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                    <tr>
                        <td width="100%" align="center">
                            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                                ForeColor="Black">Check Employees Status
                                <hr align="center" width="60%" /></asp:Label></td>
                    </tr>
                </table>
                <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                    <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="100%"
                        align="center" border="0">
                        <tr>
                            <td colspan="3" align="left">                                
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 27px">
                                <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                            <td class="tableHEADER" style="height: 27px;" align="left">
                                <strong>&nbsp;Check Employees Status</strong></td>
                            <td style="height: 27px">
                                <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                        </tr>
                        <tr>
                            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                            <td align="left" style="width: 962px">
                                <br />
                                <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                    border="0" runat="server">
                                    <tr>
                                        <td valign="top" align="left" width="50%">Select Employee type<font class="clsNote">*</font>
                                        </td>
                                        <td valign="top" align="left" width="50%">
                                            <asp:DropDownList ID="ddlType" runat="server" CssClass="clsComboBox" Width="97%" AutoPostBack="true">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <%--<asp:ListItem Value="0">--All--</asp:ListItem>--%>
                                                <asp:ListItem Value="1">Allocated</asp:ListItem>
                                                <asp:ListItem Value="2">Not Allocated</asp:ListItem>
                                                <asp:ListItem Value="3">Terminated</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <table id="Table1" cellspacing="0" cellpadding="0" style="width: 98%" align="center"
                                    border="0" runat="server">
                                    <tr>                                        
                                        <td align="center" colspan="4" headers="24px" style="height: 24px">
                                            <div id="printdiv">
                                                <br />
                                                <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="False" AllowSorting="true" Width="100%" EmptyDataText="No records found.." Visible="false" AllowPaging="True">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Employee Name" DataField="NAME" />
                                                        <asp:BoundField HeaderText="Employee ID" DataField="ID" />
                                                        <asp:BoundField HeaderText="Employee EmailID" DataField="MAIL" />
                                                        <asp:BoundField HeaderText="Employee Location" DataField="LOCATION" />
                                                        <asp:BoundField HeaderText="Employee Reporting Manager" DataField="RM" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="4" headers="24px" style="height: 24px">&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;"></td>
                        </tr>
                        <tr>
                            <td style="height: 17px;">
                                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                            <td style="height: 17px; width: 962px;" background="../../Images/table_bot_mid_bg.gif">
                                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                            <td style="height: 17px">
                                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
