Imports System.Data
Partial Class WorkSpace_SMS_Webfiles_frmLinkMaster
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLink()
        End If
    End Sub
    Private Sub BindLink()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CLASSIFICATION")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLinkName.DataSource = sp.GetDataSet()
        ddlLinkName.DataTextField = "CLS_DESCRIPTION"
        ddlLinkName.DataValueField = "CLS_ID"
        ddlLinkName.DataBind()
        ddlLinkName.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        UPDATECLASSIFICATION()
        UPDATESERVICE()
        lblMsg.Text = "Link Updated Succesfully"
        lblMsg.Visible = True
        Cleardata()
        BindLink()
    End Sub
    Private Sub Cleardata()
        ddlLinkName.SelectedIndex = 0
        txtLinkName.Text = ""
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("~/MASTERS/MAS_WEBFILES/frmMasMasters.aspx")
    End Sub
    Private Sub UPDATECLASSIFICATION()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_CLASSIFICATION")
            sp.Command.AddParameter("@cls_Description", txtLinkName.Text, DbType.String)
            sp.Command.AddParameter("@CLS_ID", ddlLinkName.SelectedItem.Value, DbType.Int32)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UPDATESERVICE()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_SERVICE")
            sp.Command.AddParameter("@SER_DESCRIPTION", txtLinkName.Text, DbType.String)
            sp.Command.AddParameter("@CLS_ID", ddlLinkName.SelectedItem.Value, DbType.Int32)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
