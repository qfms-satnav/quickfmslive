Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_space_matrix
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If

        'If Session("UID") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If
        If Not IsPostBack Then
            lblMsg.Text = ""
            BindGrid()
            Binddropdown()
            BindCompany()
            If Session("COMPANYID") = 1 Then
                ddlCompany.ClearSelection()
                ddlCompany.Items.FindByValue(Session("COMPANYID")).Selected = True
                ddlCompany.Enabled = True
            Else
                ddlCompany.ClearSelection()
                ddlCompany.Items.FindByValue(Session("COMPANYID")).Selected = True
                ddlCompany.Enabled = False
            End If

        End If
    End Sub

    Private Sub BindCompany()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COMPANYS")
        ddlCompany.DataSource = sp.GetDataSet()
        ddlCompany.DataTextField = "CNP_NAME"
        ddlCompany.DataValueField = "CNP_ID"
        ddlCompany.DataBind()
        ddlCompany.Items.Insert(0, New ListItem("--Select--", "0"))
        If ddlCompany.Items.Count > 1 Then
            ddlCompany.SelectedValue = Session("COMPANYID")
        End If
    End Sub

    Private Sub Binddropdown()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ROLES_SPACE_MATRIX")
            Dim Ds As New DataSet
            Ds = sp.GetDataSet
            RoleDropDowns(ddlSpc, Ds, "ROL_DESCRIPTION", "ROL_ID")
            RoleDropDowns(ddllvl1, Ds, "ROL_DESCRIPTION", "ROL_ID")
            RoleDropDowns(ddllvl2, Ds, "ROL_DESCRIPTION", "ROL_ID")
            RoleDropDowns(ddlalloc, Ds, "ROL_DESCRIPTION", "ROL_ID")
            RoleDropDowns(ddlmap, Ds, "ROL_DESCRIPTION", "ROL_ID")
            RoleDropDowns(ddlrls, Ds, "ROL_DESCRIPTION", "ROL_ID")
            RoleDropDowns(ddlext, Ds, "ROL_DESCRIPTION", "ROL_ID")

        Catch ex As Exception

        End Try

    End Sub

    Private Function Submitdata() As Integer
        Dim param(10) As SqlParameter
        param(0) = New SqlParameter("@REQ", SqlDbType.NVarChar, 200)
        param(0).Value = ddlSpc.SelectedValue
        param(1) = New SqlParameter("@LVL1APP", SqlDbType.NVarChar, 200)
        param(1).Value = ddllvl1.SelectedValue
        param(2) = New SqlParameter("@LVL2APP", SqlDbType.NVarChar, 200)
        param(2).Value = ddllvl2.SelectedValue
        param(3) = New SqlParameter("@SPCALLOC", SqlDbType.NVarChar, 200)
        param(3).Value = ddlalloc.SelectedValue
        param(4) = New SqlParameter("@EMPMAP", SqlDbType.NVarChar, 200)
        param(4).Value = ddlmap.SelectedValue
        param(5) = New SqlParameter("@SPCRLS", SqlDbType.NVarChar, 200)
        param(5).Value = ddlrls.SelectedValue
        param(6) = New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)
        param(6).Value = ddlsta.SelectedValue
        param(7) = New SqlParameter("@REM", SqlDbType.NVarChar, 200)
        param(7).Value = txtrem.Text
        param(8) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
        param(8).Value = Session("Uid").ToString().Trim()
        param(9) = New SqlParameter("@SPCEXT", SqlDbType.NVarChar, 200)
        param(9).Value = ddlext.SelectedValue
        param(10) = New SqlParameter("@COMPANY", SqlDbType.NVarChar, 200)
        param(10).Value = ddlCompany.SelectedValue
        Dim flag As Integer = ObjSubsonic.GetSubSonicExecute("INSERT_SPACE_MATRIX_DETAILS", param)
        If flag = 1 Then
            lblMsg.Text = "Selected Space Requisition is in use; try another"
        ElseIf flag = 0 Then
            lblMsg.Text = "Data successfully inserted"
        End If
        Return flag
    End Function

    Private Sub Modifydata()
        Dim param(10) As SqlParameter
        param(0) = New SqlParameter("@REQ", SqlDbType.NVarChar, 200)
        param(0).Value = ddlSpc.SelectedValue
        param(1) = New SqlParameter("@LVL1APP", SqlDbType.NVarChar, 200)
        param(1).Value = ddllvl1.SelectedValue
        param(2) = New SqlParameter("@LVL2APP", SqlDbType.NVarChar, 200)
        param(2).Value = ddllvl2.SelectedValue
        param(3) = New SqlParameter("@SPCALLOC", SqlDbType.NVarChar, 200)
        param(3).Value = ddlalloc.SelectedValue
        param(4) = New SqlParameter("@EMPMAP", SqlDbType.NVarChar, 200)
        param(4).Value = ddlmap.SelectedValue
        param(5) = New SqlParameter("@SPCRLS", SqlDbType.NVarChar, 200)
        param(5).Value = ddlrls.SelectedValue
        param(6) = New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)
        param(6).Value = ddlsta.SelectedValue
        param(7) = New SqlParameter("@REM", SqlDbType.NVarChar, 200)
        param(7).Value = txtrem.Text
        param(8) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
        param(8).Value = Session("Uid").ToString().Trim()
        param(9) = New SqlParameter("@SPCEXT", SqlDbType.NVarChar, 200)
        param(9).Value = ddlext.SelectedValue
        param(10) = New SqlParameter("@COMPANY", SqlDbType.NVarChar, 200)
        param(10).Value = ddlCompany.SelectedValue
        ObjSubsonic.GetSubSonicExecute("UPDATE_SPACE_MATRIX", param)
        lblMsg.Text = "Data successfully uploaded"
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_MATRIX_DETAILS")
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvitems.DataSource = ds
        gvitems.DataBind()
        'ObjSubsonic.BindGridView(gvitems, "GET_SPACE_MATRIX_DETAILS")
    End Sub

    Private Sub Cleardata()
        ddlSpc.SelectedIndex = 0
        ddllvl1.SelectedIndex = 0
        ddllvl2.SelectedIndex = 0
        ddlalloc.SelectedIndex = 0
        ddlmap.SelectedIndex = 0
        ddlrls.SelectedIndex = 0
        ddlsta.SelectedIndex = 0
        ddlext.SelectedIndex = 0
        txtrem.Text = ""
        lblMsg.Text = ""
    End Sub

    Private Sub RoleDropDowns(ByRef cboCombo As DropDownList, ByVal reader As DataSet, ByVal TxtField As String, ByVal ValField As String)
        cboCombo.DataSource = reader
        cboCombo.DataTextField = "ROL_DESCRIPTION"
        cboCombo.DataValueField = "ROL_ID"
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub rbActions_CheckedChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            lblMsg.Text = ""
            Cleardata()
            btnsubmit.Text = "Submit"
        Else
            lblMsg.Text = ""
            Cleardata()
            btnsubmit.Text = "Modify"
        End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        If rbActions.Checked = True Then
            lblMsg.Text = ""
            Submitdata()
            BindGrid()
        ElseIf rbActionsModify.Checked = True Then
            lblMsg.Text = ""
            Modifydata()
            BindGrid()
        End If
    End Sub

    Protected Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
        Cleardata()
        rbActions.Checked = True
        rbActionsModify.Checked = False
        btnsubmit.Text = "Submit"
    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx")
    End Sub

    Protected Sub ddlSpc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSpc.SelectedIndexChanged
        Try
            If rbActionsModify.Checked = True Then
                lblMsg.Text = ""
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_DROPDOWN_BY_SPC_REQ")
                sp.Command.AddParameter("@REQ_ID", ddlSpc.SelectedValue, DbType.String)
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                Dim Ds As New DataSet
                Ds = sp.GetDataSet
                ddllvl1.SelectedValue = Ds.Tables(0).Rows(0)("L1")
                ddllvl2.SelectedValue = Ds.Tables(0).Rows(0)("L2")
                ddlalloc.SelectedValue = Ds.Tables(0).Rows(0)("ALLOC")
                ddlmap.SelectedValue = Ds.Tables(0).Rows(0)("MAP")
                ddlrls.SelectedValue = Ds.Tables(0).Rows(0)("REL")
                ddlsta.SelectedValue = Ds.Tables(0).Rows(0)("SM_STA_ID")
                ddlext.SelectedValue = Ds.Tables(0).Rows(0)("EXTN")
                ddlCompany.SelectedValue = Ds.Tables(0).Rows(0)("COMPANYID")
                txtrem.Text = Ds.Tables(0).Rows(0)("SM_REM")
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class