<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddVisitor.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmAddVisitor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Visitor </title>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black"> Add Visitors 
                    </asp:Label>
                    <hr align="center" width="60%" />
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                        width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong> Add Visitors </strong>
                </td>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                        width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;</td>
                <td align="left">
                    <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                        <tr>
                            <td align="center" colspan="2">
                                <asp:RadioButtonList ID="radvis" runat="server" Width="95%" AutoPostBack="True" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1">Manual</asp:ListItem>
                                    <asp:ListItem Value="2">Excel</asp:ListItem>
                                </asp:RadioButtonList><br>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Panel ID="pnlexcel" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="Label2" runat="server" Font-Bold="True">Number Of  Visitors</asp:Label>:
                                                <asp:TextBox ID="txtAdd" runat="server" MaxLength="3" CssClass="clsTextField"></asp:TextBox>&nbsp;
                                                <asp:Button ID="btnAdd" runat="server" CssClass="clsButton" Text="Add"></asp:Button><br>
                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please Enter Only Numbers"
                                                    Display="None" Operator="DataTypeCheck" ControlToValidate="txtAdd"></asp:CompareValidator></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlform" runat="server" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:Label ID="lblvis" runat="server" Font-Bold="True">Upload Visitors</asp:Label>:<input
                                                    id="uploadexcel" type="file" runat="server"><br>
                                                <asp:Button ID="btnupload" runat="server" CausesValidation="false" CssClass="clsButton"
                                                    Text="Upload"></asp:Button><br><br>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please Upload only Excel Files"
                                                    Display="None" ControlToValidate="uploadexcel" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.xls)$"></asp:RegularExpressionValidator>
													<br>
													<a href='<%=Page.ResolveUrl("~/UploadFiles/GuestList.xls")%>' target="_blank" > Excel Format </a>
													
													</td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Panel ID="ExPanel" Visible="false" runat="server" Width="95%" Height="13">
                                    <table id="Table1" cellspacing="0" cellpadding="0" border="0">
                                        <colgroup>
                                            <tr height="5" width="100%">
                                                <td class="label" colspan="7">
                                                    Sample Excel Format:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Note: Sheet&nbsp;Name must be
                                                    'Visitors' inside the excel)
                                                </td>
                                            </tr>
                                            <tr style="height: 12.75pt" height="17">
                                                <td class="label" width="226" height="17">
                                                    <font face="Arial" size="2">Visitor Name</font></td>
                                                <td class="label" width="121">
                                                    <font face="Arial" size="2">E-Mail</font></td>
                                                <td class="label" width="108">
                                                    <font face="Arial" size="2">Designation/Organization</font></td>
                                                <td class="label" width="100">
                                                    <font face="Arial" size="2">Category of Visitor</font></td>
                                                <td class="label" width="98">
                                                    <font face="Arial" size="2">Event Organizer</font></td>
                                                <td class="label" width="154">
                                                    <font face="Arial" size="2">Contact Person</font></td>
                                            </tr>
                                            <tr style="height: 12.75pt" height="17">
                                                <td class="xl23" height="17">
                                                    <font face="Arial" size="2">YSR</font></td>
                                                <td class="xl25">
                                                    <a href="mailto:xyz@abc.com"><font face="Arial" color="#0000ff" size="2">xyz@abc.com</font></a></td>
                                                <td class="xl23">
                                                    <font face="Arial" size="2">Chief Minister</font></td>
                                                <td class="xl23">
                                                    <font face="Arial" size="2">Politician</font></td>
                                                <td class="xl23">
                                                    <font face="Arial" size="2">CEE/Deepak Chandra</font></td>
                                                <td class="xl23">
                                                    <font face="Arial" size="2">Madhavi</font></td>
                                            </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Panel ID="pnlEx" Visible="false" runat="server">
                                    <table width="100%">
                                        <tr style="height: 12.75pt" height="17">
                                            <td class="xl23" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                                                border-left: windowtext 0.5pt solid; width: 104.157pt; border-bottom: windowtext 0.5pt solid;
                                                height: 12.75pt; background-color: transparent" height="17">
                                                <font face="Arial" size="2">Ex:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    YSR</font></td>
                                            <td class="xl25" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                                                border-left: windowtext; width: 72.402pt; border-bottom: windowtext 0.5pt solid;
                                                background-color: transparent">
                                                &nbsp;&nbsp;<a href="mailto:xyz@abc.com"><font face="Arial" color="#0000ff" size="2">xyz@abc.com</font></a></td>
                                            <td class="xl23" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                                                border-left: windowtext; width: 229px; border-bottom: windowtext 0.5pt solid;
                                                background-color: transparent">
                                                <font face="Arial" size="2">&nbsp;&nbsp;&nbsp; Chief Minister</font></td>
                                            <td class="xl23" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                                                border-left: windowtext; width: 103px; border-bottom: windowtext 0.5pt solid;
                                                background-color: transparent">
                                                <font face="Arial" size="2">Politician</font></td>
                                            <td class="xl23" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                                                border-left: windowtext; width: 100.4pt; border-bottom: windowtext 0.5pt solid;
                                                background-color: transparent">
                                                <font face="Arial" size="2">CEE/Deepak Chandra</font></td>
                                            <td class="xl23" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                                                border-left: windowtext; width: 109px; border-bottom: windowtext 0.5pt solid;
                                                background-color: transparent">
                                                <font face="Arial" size="2">Madhavi</font></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <asp:DataGrid ID="dgMain" runat="server" Width="100%" AutoGenerateColumns="False"
                                    Visible="False">
                                    <Columns>
                                        <asp:BoundColumn DataField="sno" HeaderText="S.No">
                                            <HeaderStyle Width="3%"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Visitor Name">
                                            <ItemTemplate>
                                                <asp:TextBox Width="100" MaxLength="50" ID="txtName" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="E-Mail">
                                            <ItemTemplate>
                                                <asp:TextBox Width="100" MaxLength="50" ID="txtEmail" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Designation/Organization">
                                            <ItemTemplate>
                                                <asp:TextBox Width="100" MaxLength="50" ID="txtDomain" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Category of Visitor">
                                            <ItemTemplate>
                                                <asp:TextBox Width="100" MaxLength="50" ID="txtGroup" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Event Organizer">
                                            <ItemTemplate>
                                                <asp:TextBox Width="100" MaxLength="50" ID="txtRelation" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Contact Person">
                                            <ItemTemplate>
                                                <asp:TextBox Width="100" MaxLength="50" ID="txtRemarks" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:DataGrid ID="dgexcel" runat="server" Width="100%" Visible="False">
                                </asp:DataGrid><br>
                                <div align="center">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="clsButton" Text="Submit" Visible="False">
                                    </asp:Button></div>
                                <br>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblMessage" runat="server" Width="100%" CssClass="clsMessage"></asp:Label>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                    height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" /></td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
