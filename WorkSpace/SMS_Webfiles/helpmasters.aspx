<%@ Page Language="VB" AutoEventWireup="false" CodeFile="helpmasters.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_helpmasters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>

    <div class="al-content">
        <div class="widgets">
            <h3>Help Desk Masters </h3>
        </div>
        <div class="card" style="padding-right: 50px;">
            <form id="form1" runat="server">
                <div class="box-body">

                    <div class="clearfix">

                        <div class="col-md-4 col-sm-12 col-xs-12" id="CountryHLdiv" runat="server">
                            <asp:HyperLink ID="HyperLink4" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHDMMainCategoryMaster.aspx">HD Main Category</asp:HyperLink>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="HyperLink11" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmSubCategory.aspx">HD Sub Category</asp:HyperLink>

                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="HyperLink12" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmChildCategory.aspx">HD Child Category</asp:HyperLink>

                        </div>

                    </div>
                    <br />

                    <div class="clearfix">

                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="HyperLink5" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHDMAssetLocation.aspx">Asset Location</asp:HyperLink>

                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="hplServiceEscalation" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmServiceEscalationMapping.aspx">Service Escalation Mapping</asp:HyperLink>

                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="hplviewreqtypes" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmSLA.aspx">SLA Time Definition</asp:HyperLink>

                        </div>

                    </div>
                    <br />


                    <div class="clearfix">


                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHDMRepeatCallMaster.aspx">Repeat Call</asp:HyperLink>

                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="HyperLink9" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmImpact.aspx">Impact</asp:HyperLink>

                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="HyperLink10" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmUrgency.aspx">Urgency</asp:HyperLink>

                        </div>
                    </div>
                    <br />

                    <div class="clearfix">
                        <div class="col-md-4 col-sm-12 col-xs-12 ">

                            <asp:HyperLink ID="HyperLink13" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmFacilityMaster.aspx">Facility </asp:HyperLink>

                        </div>

                        <div class="col-md-4 col-sm-12 col-xs-12 ">

                            <asp:HyperLink ID="HyperLink8" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmHolidayMaster.aspx">Holiday </asp:HyperLink>

                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <asp:HyperLink ID="HyperLink7" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/frmFeedBackMaster.aspx">Feedback </asp:HyperLink>

                        </div>
                    </div>
                    <br />
                       <div class="clearfix">
                        <div class="col-md-4 col-sm-12 col-xs-12 ">
                            <asp:HyperLink ID="HyperLink20" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/statusMaster.aspx">Status </asp:HyperLink>
                        </div>
                    </div>

                    <div class="clearfix" id="ChildCatLoc" runat="server" visible="false">
                        <div class="col-md-4 col-sm-12 col-xs-12 ">
                            <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/ChildCatApprovalLocMatrix.aspx">Child Category Location Approval Mapping </asp:HyperLink>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix" id="Designation" runat="server" visible="false">
                        <div class="col-md-4 col-sm-12 col-xs-12 ">
                            <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/CatDsgMaster.aspx">Child Category Designation Mapping </asp:HyperLink>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 ">
                            <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/HDM/HDM_Webfiles/Masters/ChildCatApprovalMat.aspx">Child Category Approval Matrix </asp:HyperLink>
                        </div>
                    </div>


                    <div class="clearfix">
                    </div>
                    <br />
                    <div class="clearfix">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
