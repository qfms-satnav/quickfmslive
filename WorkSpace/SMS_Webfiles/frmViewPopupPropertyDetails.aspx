<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewPopupPropertyDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmViewPopupPropertyDetails" %>

<%@ Register Src="../../Controls/ViewPopupPropertyDetails.ascx" TagName="ViewPopupPropertyDetails"
    TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:ViewPopupPropertyDetails ID="ViewPopupPropertyDetails1" runat="server" />
    
    </div>
    </form>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
