<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmRMRenewalApproval.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmRMRenewalApproval" title="Renewal of Lease" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
<table width="98%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">Supervisor Recomendation for Renewal of Lease
             <hr align="center" width="60%" /></asp:Label>
            &nbsp;
            <br />
        </td>
    </tr>
</table>
<asp:Panel ID="PanelGridview" runat="server" Width="90%">
    <table cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%" align="center"
        border="0">
        <tr>
            <td style="width: 10px; height: 27px;">
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left" style="height: 27px">
                &nbsp;<strong>Supervisor Recomendation</strong></td>
            <td style="height: 27px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table width="100%" cellpadding="1" cellspacing="0" align="center" border="1">
                    <tr id="Tr1" runat="Server" visible="FALSE" >
                        <td align="left" style="height: 26px; width: 50%">
                            Select Lease Type <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="clsComboBox" Width="99%"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                 <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="0" runat="server">
                    <tr>
                        <td align="center" style="height: 20px">
                           <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="5" EmptyDataText="No Records Found">
                                <PagerSettings Mode="NumericFirstLast" />
                             <Columns>
                                 
                                    <asp:TemplateField HeaderText="Lease">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllname" runat="server" CssClass="lblCODE" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CTS Number">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                 
                                    <asp:TemplateField HeaderText="city">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcity" runat="server" CssClass="lblcity" Text='<%#Eval("CITY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Employee No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpNo" runat="server"  Text='<%#Eval("LESSE_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Lesse">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLesseName" runat="server" CssClass="lblLesseName" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Start Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsdate" runat="server" CssClass="lblstartdate" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Expiry Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEdate" runat="server" CssClass="lblExpiryDate" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLstatus" runat="server" CssClass="lblLstatus" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Created By">
                                        <ItemTemplate>
                                            <asp:Label ID="Lbluser" runat="server" CssClass="lbluser" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                               
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <a href='frmRMRenewalLeaseDetails.aspx?id=<%#Eval("LEASE_NAME")%>'>View Details</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                </Columns>
                                <FooterStyle CssClass="GVFixedFooter" />
                                <HeaderStyle CssClass="GVFixedHeader" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                
                
            
                
                
                 </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</asp:Panel>
</div>
</asp:Content>

