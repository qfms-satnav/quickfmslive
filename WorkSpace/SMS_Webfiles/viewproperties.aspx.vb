﻿Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_viewproperties
    Inherits System.Web.UI.Page
    Dim filepath As String
    Dim strtxt As String
    Dim Subsonic As clsSubSonicCommonFunctions = New clsSubSonicCommonFunctions

    Dim strResponse As String = ""
    Dim strpoints As String = ""
    Dim strResponse1 As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            divcity.Visible = False
            divmap.Visible = False
            ' Subsonic.Binddropdown(ddlproperty, "GET_ACTIVE_PROPERTY_TYPE", "PN_PROPERTYTYPE", "PN_TYPEID")
			 Subsonic.Binddropdown(ddlproperty, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")
            ' ddlproperty.Items.Insert(1, New ListItem("ALL", "ALL"))
            lblmsg.Text = ""
            'Bindmap()
            GetData("")
        End If
    End Sub

    Private Sub GetData(city As String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTIVE_PROPERTIES")
        sp.Command.AddParameter("@PROPERTY", city, DbType.String)
        Dim ds As DataSet = sp.GetDataSet
        rptMarkers.DataSource = ds
        rptMarkers.DataBind()

        lblmsg.Text = IIf(ds.Tables(0).Rows.Count = 0, "No Properties Available", "")

    End Sub

    'Private Sub BindMap(ByVal city As String)
    '    LBLMSG.Text = ""

    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTIVE_PROPERTIES")
    '    sp.Command.AddParameter("@PROPERTY", city, DbType.String)


    '    Dim dr As SqlDataReader = sp.GetReader


    '    Dim lat As Double = "0"
    '    Dim lon As Double = "0"
    '    Dim icnt As Integer = 1
    '    Dim Propertyname As String = ""
    '    Dim Propertyaddress As String = ""
    '    Dim propertycity As String = ""
    '    Dim propertydate As String = ""
    '    Dim propertyamount As String = ""
    '    Dim arr1 As Array
    '    Dim latlon As String = ""
    '    Dim propertytype As Int16 = 0
    '    Dim strText As String = ""
    '    Dim strdata As String = ""


    '   While dr.Read()
    '        Try
    '            lat = dr("LATITUDE")
    '            lon = dr("LONGITUDE")

    '            Propertyname = dr("PN_NAME")

    '            Propertyaddress = dr("PROPERTY_DESCRIPTION")



    '            strText = "<b>Address:</b> " & CStr(Propertyname) & "," & CStr(Propertyaddress) & " ," & CStr(propertycity) & "<b></b>"

    '            strdata = strdata + "['" + strText + "'," + CStr(lat) + "," + CStr(lon) + "," + CStr(icnt) + "],"

    '            icnt += 1
    '        Catch ex As Exception
    '            Response.Write(ex.Message)
    '        End Try

    '    End While

    '    If dr.HasRows = True Then
    '        strdata = strdata.Remove(strdata.Length - 1, 1)
    '        strdata = strdata + "]"
    '        If File.Exists(MapPath("MapGoogleViewProperties.html")) Then
    '            strResponse1 = File.ReadAllText(MapPath("MapGoogleViewProperties.html"))
    '            strResponse = strResponse1.Replace("@@item", strdata)
    '        End If
    '        'File.WriteAllText(MapPath("MapGoogleViewProperties.html"), strResponse)
    '    End If

    '    Literal1.Text = strResponse

    'End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim strproperty As String = ""
        If ddlproperty.SelectedItem.Value = "--Select--" Then
            strproperty = ""
        ElseIf ddlproperty.SelectedItem.Value = "ALL" Then
            strproperty = ""
        Else
            strproperty = ddlproperty.SelectedItem.Value
        End If
        'BindMap(strproperty)
        GetData(strproperty)
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        'Dim iLoop As Integer
        'For iLoop = 0 To rbActions.Items.Count - 1
        '    If rbActions.Items(iLoop).Selected = True Then
        '        If rbActions.Items(iLoop).Text = "Add" Then
        If rbActions.Checked = True Then
            divcity.Visible = False
            divmap.Visible = False
            divlist.Visible = True

            ReportViewer1.Visible = True

        Else
            divlist.Visible = False
            divcity.Visible = True
            divmap.Visible = True


            'GetData("")
            'BindMap("")
        End If

    End Sub

    'Private Sub Bindmap()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PRPTYPE_DETGRD_DETAILS_NEW")
    '    sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
    '    Dim ds As New DataSet
    '    ds = sp.GetDataSet

    '    'gvitems.DataSource = ds
    '    'gvitems.DataBind()
    '    'ds = sp.GetDataSet()
    '    Dim rds As New ReportDataSource()
    '    rds.Name = "ViewProperties"

    '    'This refers to the dataset name in the RDLC file
    '    rds.Value = ds.Tables(0)
    '    ReportViewer1.Reset()
    '    ReportViewer1.LocalReport.DataSources.Add(rds)
    '    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/ViewProperties.rdlc")
    '    'Dim cur As String = CultureInfo.GetCultureInfo(Session("userculture"))
    '    Dim ci As New CultureInfo(Session("userculture").ToString())
    '    Dim nfi As NumberFormatInfo = ci.NumberFormat
    '    Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
    '    ReportViewer1.LocalReport.SetParameters(p1)
    '    ReportViewer1.LocalReport.Refresh()

    '    ReportViewer1.SizeToReportContent = True
    '    ReportViewer1.Visible = True
    'End Sub
End Class
