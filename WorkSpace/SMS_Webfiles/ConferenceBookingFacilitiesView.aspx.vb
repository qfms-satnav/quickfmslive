Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Partial Class WorkSpace_SMS_Webfiles_ConferenceBookingFacilitiesView
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    ' ------------------- Json ------------------------------------
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/themes/flick/jquery-ui.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/css/ui.jqgrid.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.min.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/i18n/grid.locale-en.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.jqGrid.min.js")))
    '    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js_source/TowerwiseUtilization.js")))
    '    '--------------------------------------------------------------
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))

    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    'End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Try
                getConferenceDetails()
                BindParticipants()
                BindVisitors()
                BindConSumableAssets()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
    End Sub

    Private Sub BindConSumableAssets()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@reqid", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("req_id")
        objsubsonic.BindDataGrid(dgConSumableAssets, "getConsumbleAstList", param)
        If dgConSumableAssets.Items.Count > 0 Then
            dgConSumableAssets.Visible = True
        Else
            dgConSumableAssets.Visible = False
        End If
    End Sub

    Private Sub getConferenceDetails()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("req_id")
        ds = objsubsonic.GetSubSonicDataSet("get_conferencedetails", param)
        If ds.Tables(0).Rows.Count > 0 Then
            confdetails.Visible = True
            lblReqId.Text = ds.Tables(0).Rows(0).Item("REQ_ID")
            lblAurKnownAs.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
            lblConferenceRoom.Text = ds.Tables(0).Rows(0).Item("SPC_NAME")
            lblBDateTime.Text = "From: " & ds.Tables(0).Rows(0).Item("FROM_DATE") & " (" & ds.Tables(0).Rows(0).Item("FROM_TIME") & ") To: " & ds.Tables(0).Rows(0).Item("TO_DATE") & " (" & ds.Tables(0).Rows(0).Item("TO_TIME") & ") "
        Else
            confdetails.Visible = False
        End If
    End Sub

    Private Sub BindVisitors()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("req_id")
        objsubsonic.BindDataGrid(dgVisitors, "GETVISITORS", param)
        If dgVisitors.Items.Count > 0 Then
            dgVisitors.Visible = True
        Else
            dgVisitors.Visible = False
        End If
    End Sub


    Private Sub BindParticipants()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Reqid", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("req_id")
        objsubsonic.BindDataGrid(dgParticipants, "getSelectedPartcipantslist", param)
        If dgParticipants.Items.Count > 0 Then
            dgParticipants.Visible = True
        Else
            dgParticipants.Visible = False
        End If
    End Sub

    Protected Sub dgParticipants_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgParticipants.PageIndexChanged
        dgParticipants.CurrentPageIndex = e.NewPageIndex
        BindParticipants()
    End Sub

    Private Sub dgParticipants_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgParticipants.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
                param(0).Value = e.Item.Cells(1).Text
                param(1) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param(1).Value = Request.QueryString("req_id")
                objsubsonic.GetSubSonicExecuteScalar("DELETEPARTICIPANTS_LIST", param)
                BindParticipants()
            End If
        Catch ex As Exception
            lblmsg.Text = ex.Message
        End Try
    End Sub


    Protected Sub dgVisitors_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgVisitors.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter("@VIS_ID", SqlDbType.NVarChar, 200)
                param(0).Value = e.Item.Cells(1).Text
                param(1) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param(1).Value = Request.QueryString("req_id")
                objsubsonic.GetSubSonicExecuteScalar("DELETE_VISITORS", param)
                BindVisitors()
            End If
        Catch ex As Exception
            lblmsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub dgConSumableAssets_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgConSumableAssets.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter("@reqid", SqlDbType.NVarChar, 200)
                param(0).Value = Request.QueryString("req_id")
                param(1) = New SqlParameter("@sno", SqlDbType.NVarChar, 200)
                param(1).Value = e.Item.Cells(0).Text
                objsubsonic.GetSubSonicExecuteScalar("getConsumbleAstList_Delete", param)
                BindConSumableAssets()
            End If
        Catch ex As Exception
            lblmsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub dgConSumableAssets_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgConSumableAssets.PageIndexChanged
        dgConSumableAssets.CurrentPageIndex = e.NewPageIndex
        BindConSumableAssets()
    End Sub

    Protected Sub dgVisitors_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgVisitors.PageIndexChanged
        dgVisitors.CurrentPageIndex = e.NewPageIndex
        BindVisitors()
    End Sub
End Class
