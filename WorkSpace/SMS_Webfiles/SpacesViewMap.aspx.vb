Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports System.io


Partial Class WorkSpace_SMS_Webfiles_SpacesViewMap
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetMap()
        End If
    End Sub

    Private Sub GetMap()
        Dim strspaces As String
        Dim strbox_bounds As String
        Dim spcid As String
        Dim filter As String
        spcid = Request.QueryString("spcid")
        spcid = spcid.ToString.Substring(0, spcid.Length - 1)
        Dim spaces As Array
        spaces = spcid.Split(";")
        For i As Integer = 0 To spaces.Length - 1
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@spc_id", SqlDbType.NVarChar, 200)
            param(0).Value = spaces(i)
            Dim ds As New DataSet
            ds = ObjSubsonic.GetSubSonicDataSet("getSpaceDetails_space", param)
            If ds.Tables(0).Rows.Count > 0 Then
                strspaces = strspaces & "{""longlat"":""" & ds.Tables(0).Rows(0).Item("lat") & "," & ds.Tables(0).Rows(0).Item("lon") & """,""space_id"":""<table id=table1 border=1 bgcolor=#FFFFE0 width=100%><tr><td width=100%><center><b><font color=red>Seat Information for Allocation/Deallocation</font></b></center></td></tr><tr><td width=50%><center>" & ds.Tables(0).Rows(0).Item("spc_id") & "</center></td></tr></table>"",""imgurl"":""" & ds.Tables(0).Rows(0).Item("IMGURL") & """},"
                strbox_bounds = ds.Tables(0).Rows(0).Item("BBOX")
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                filter = ds.Tables(1).Rows(0).Item("flr_id")
            End If
        Next
        strspaces = strspaces.ToString.Substring(0, strspaces.Length - 1)
        Dim strResponse As String = ""
        If File.Exists(MapPath("ViewMap.txt")) Then
            strResponse = File.ReadAllText(MapPath("ViewMap.txt"))
        End If
        strResponse = Replace(strResponse, "@@data", "[" & strspaces & "]")
        strResponse = Replace(strResponse, "@@box_bounds", strbox_bounds)
        strResponse = Replace(strResponse, "@@filter", filter)
        strResponse = Replace(strResponse, "@@TenantId", Session("TENANT"))
        litmap.Text = strResponse
    End Sub
End Class

'  strspaces = "[ {""longlat"":""2750209.01695914,833594.337946481"",""space_id"":""<table id=table1 border=1 bgcolor=#FFFFE0 width=100%><tr><td width=100%><center><b><font color=red> Seat Information for Allocation/Deallocation</font></b></center></td></tr><tr><td width=50%><center>space_id</center></td></tr></table>"",""imgurl"":""http://projects.a-mantra.com/spacedemo/images/chair_green.gif""},{""longlat"":""2753020.77133167,843129.950386578"",""space_id"":""space_id2"",""imgurl"":""http://projects.a-mantra.com/spacedemo/images/chair_yellow.gif""},{""longlat"":""2753020.67723535,841586.560330311"",""space_id"":""space_id3"",""imgurl"":""http://projects.a-mantra.com/spacedemo/images/chair_orange.gif""}	 ]"

