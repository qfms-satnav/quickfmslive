<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Verticalvacantseats.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Verticalvacantseats" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Vertical Vacant seats</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">
             <hr align="center" width="60%" />Vacant Space for verticals</asp:Label></td>
            </tr>
        </table>
        
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">
                       
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Vacant Space for verticals </strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val2" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
       <asp:Button ID="btn3finish" runat="server" Text="Close" CssClass="button" TabIndex="96"
                            OnClientClick="parent.hidePopWin(true);" CausesValidation="false" />
    
    <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="False" AllowPaging="true" Width="100%" EmptyDataText="No Records Found" PageSize="20">
                        <Columns>
                        <asp:TemplateField HeaderText="Location code">
                        <ItemTemplate>
                         <ItemStyle Width="15%" />
                        <asp:Label ID="lblloccode" runat="server" Text=<%#Eval("LCM_CODE") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Location name">
                        <ItemTemplate>
                        <asp:Label ID="lbllocname" runat="server" Text=<%#Eval("LCM_NAME") %>></asp:Label>
                        </ItemTemplate>
                         <ItemStyle Width="20%" />
                        </asp:TemplateField>
                         <%--<asp:TemplateField HeaderText="Tower code">
                        <ItemTemplate>
                        <asp:Label ID="lbltcode" runat="server" Text=<%#Eval("TWR_CODE") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>--%>
                         <asp:TemplateField HeaderText="Tower name">
                        <ItemTemplate>
                        <ItemStyle Width="10%" />
                        <asp:Label ID="lbltname" runat="server" Text=<%#Eval("TWR_NAME") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <%--<asp:TemplateField HeaderText="Floor code">
                        <ItemTemplate>
                        <asp:Label ID="lblfcode" runat="server" Text=<%#Eval("FLR_CODE") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>--%>
                         <asp:TemplateField HeaderText="Floor name">
                        <ItemTemplate>
                         <ItemStyle Width="10%" />
                        <asp:Label ID="lblfname" runat="server" Text=<%#Eval("FLR_NAME") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Space ID">
                        <ItemTemplate>
                        <ItemStyle Width="50%" />
                        <asp:Label ID="lbtncount" runat="server" Text=<%#Eval("SPC_ID") %>></asp:Label>
                        
                        </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                         </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
                       
                        
    </div>
    </form>
</body>
</html>
