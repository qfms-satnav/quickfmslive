﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmEnergyMasters.aspx.cs" Inherits="WorkSpace_SMS_Webfiles_frmEnergyMasters" Title="Energy Management Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>

<body>
    <div class="al-content">
        <div class="widgets">
            <h3>Energy Management Masters</h3>
        </div>
        <div class="card" style="padding-right: 50px;">
            <form id="form2" runat="server">
                <div class="box-body">
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="hpl" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/EM/Masters/Views/Threshold.aspx">Energy Budget Master</asp:HyperLink>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
