<%@ Page EnableEventValidation="false" Language="VB" AutoEventWireup="false" CodeFile="frmAddPropertyType.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmAddPropertyType" Title="Add Property type" %>

<%@ Register Src="~/Controls/AddPropertyType.ascx" TagName="AddPropertyType"
    TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>



    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Add Property Type</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <uc1:AddPropertyType ID="AddPropertyType1" runat="server" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>


