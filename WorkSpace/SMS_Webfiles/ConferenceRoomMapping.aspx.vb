﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_ConferenceRoomMapping
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If


        txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        txttodate.Attributes.Add("onClick", "displayDatePicker('" + txttodate.ClientID + "')")
        txttodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")



        If Not Page.IsPostBack Then
            lblMsg.Text = ""
            LoadCity()


        End If
    End Sub
    Public Sub LoadCity()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSelectLocation.SelectedIndexChanged

        Try
            'usp_getActiveTower_LOC

            If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
                ' objMsater.BindTowerLoc(ddlTower, ddlSelectLocation.SelectedItem.Value)
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
                param(0).Value = ddlSelectLocation.SelectedItem.Value
                ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)


            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        Try

            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    'Protected Sub btnchkavail_Click(sender As Object, e As EventArgs) Handles btnchkavail.Click

    '    Dim ftime As String = ""
    '    Dim ttime As String = ""
    '    Dim StartHr As String = "00"
    '    Dim EndHr As String = "00"
    '    Dim StartMM As String = "00"
    '    Dim EndMM As String = "00"

    '    If starttimehr.SelectedItem.Value <> "Hr" Then
    '        StartHr = starttimehr.SelectedItem.Text
    '    End If
    '    If starttimemin.SelectedItem.Value <> "Min" Then
    '        StartMM = starttimemin.SelectedItem.Text
    '    End If
    '    If endtimehr.SelectedItem.Value <> "Hr" Then
    '        EndHr = endtimehr.SelectedItem.Text
    '    End If
    '    If endtimemin.SelectedItem.Value <> "Min" Then
    '        EndMM = endtimemin.SelectedItem.Text
    '    End If

    '    ftime = StartHr + ":" + StartMM
    '    ttime = EndHr + ":" + EndMM



    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECK_AVAIL_CONFERENCE_ASSETS")
    '    sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@LOC", ddlSelectLocation.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@TWR", ddlTower.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@FLR", ddlFloor.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@FDATE", txtFrmDate.Text, DbType.DateTime)
    '    sp.Command.AddParameter("@FTIME", ftime, DbType.DateTime)
    '    sp.Command.AddParameter("@TTIME", ttime, DbType.DateTime)

    '    Dim ds As DataSet = sp.GetDataSet()
    '    ddlConference.DataSource = ds.Tables(0)
    '    ddlConference.DataTextField = "CONFERENCE_ROOM_NAME"
    '    ddlConference.DataValueField = "CONFERENCE_ROOM_CODE"
    '    ddlConference.DataBind()
    '    ddlConference.Items.Insert(0, New ListItem("--Select--", "0"))


    '    lstassets.DataSource = ds.Tables(1)
    '    lstassets.DataTextField = "AAT_NAME"
    '    lstassets.DataValueField = "AAT_CODE"
    '    lstassets.DataBind()

    '    confroom.Visible = True
    '    assets.Visible = True

    '    If ddlConference.Items.Count > 0 Then
    '        btnsubmit.Visible = True
    '    Else
    '        btnsubmit.Visible = False

    '    End If
    'End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click

        Dim ast As String = ""

        Dim count As Int32 = 0


        If ddlConference.SelectedIndex > 0 And lstassets.Items.Count > 0 Then



            For Each li As ListItem In lstassets.Items
                If li.Selected = True Then
                    count += 1
                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MAP_ASSETS_CONFERENCE")
                    sp1.Command.AddParameter("@CROOM", ddlConference.SelectedItem.Value, DbType.String)
                    sp1.Command.AddParameter("@ASSETS", li.Value.ToString(), DbType.String)
                    sp1.Command.AddParameter("@F_DATE", txtFrmDate.Text, DbType.DateTime)
                    li.Value.ToString()
                    sp1.ExecuteScalar()
                End If
            Next

            If count = 0 Then
                lblMsg.Text = "Please Select minimum of one asset to map"
                Exit Sub
            End If


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MAP_ASSET_CONFERENCE")
            sp.Command.AddParameter("@LOC", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@TWR", ddlTower.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FLR", ddlFloor.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FDATE", txtFrmDate.Text, DbType.DateTime)
            sp.Command.AddParameter("@TDATE", txtFrmDate.Text, DbType.DateTime)
            sp.Command.AddParameter("@CROOM", ddlConference.SelectedItem.Value, DbType.String)

            sp.ExecuteScalar()


            lblMsg.Text = " Assets Mapped Succesfully"
            Cleardata()
        Else
            lblMsg.Text = "Please select Conference Room and assets to map"
        End If


    End Sub
    Private Sub Cleardata()
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCity.SelectedIndex = -1
        ddlConference.Items.Clear()
        lstassets.Items.Clear()
        txtFrmDate.Text = ""
        txttodate.Text = ""
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECK_AVAIL_CONFERENCE_ASSETS")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LOC", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR", ddlFloor.SelectedItem.Value, DbType.String)
       

        Dim ds As DataSet = sp.GetDataSet()
        ddlConference.DataSource = ds.Tables(0)
        ddlConference.DataTextField = "CONFERENCE_ROOM_NAME"
        ddlConference.DataValueField = "CONFERENCE_ROOM_CODE"
        ddlConference.DataBind()
        ddlConference.Items.Insert(0, New ListItem("--Select--", "0"))


      
        confroom.Visible = True
     
    End Sub

    Protected Sub ddlConference_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlConference.SelectedIndexChanged
        If ddlConference.SelectedIndex > 0 Then



            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECK_AVAIL_CONFERENCE_ASSETS_MAP")
            sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LOC", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@TWR", ddlTower.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FLR", ddlFloor.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CONF", ddlConference.SelectedItem.Value, DbType.String)
            lstassets.DataSource = sp.GetDataSet()
            lstassets.DataTextField = "AAT_NAME"
            lstassets.DataValueField = "AAT_CODE"
            lstassets.DataBind()
            assets.Visible = True


            If ddlConference.Items.Count > 0 Then
                btnsubmit.Visible = True
            Else
                btnsubmit.Visible = False

            End If
        End If
    End Sub
End Class
