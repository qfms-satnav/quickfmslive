﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmdownloadtemplates.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmdownloadtemplates" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>

    <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>QuickFMS Templates</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">

                                    <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/QuickFMSTemplates/QuickFMS_HRMS.xls">HRMS</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">

                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/QuickFMSTemplates/QuickFMS_Space_Management.xls">Space Management</asp:HyperLink>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">

                                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/QuickFMSTemplates/QuickFMS_Property_Management.xls">Property Management</asp:HyperLink>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/QuickFMSTemplates/QuickFMS_Asset_Management.xls">Asset Management</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/QuickFMSTemplates/QuickFMS_Maintenance_Management.xls">Maintenance Management</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/QuickFMSTemplates/QuickFMS_HelpDesk_Management.xls">Help Desk Management</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/QuickFMSTemplates/QuickFMS_Conference_Management.xls">Conference Management</asp:HyperLink>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
