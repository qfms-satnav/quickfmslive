<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="editconference_employee.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_editconference_employee" title="Edit Booking Requests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">  
             <hr align="center" width="60%" />Edit Booking Requests</asp:Label></td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">&nbsp; <strong>Edit Booking Requests</strong>
            </td>
            <td style="width: 16px">
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">&nbsp;</td>
            <td align="left">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="clsMessage"
                            ForeColor="" />
                        <br />
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                        <br />

                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td colspan="4">
                                    <asp.panel id="pnlemp" runat="server" groupingtext="Booked Time Slots">
                            <asp:GridView ID="gvDaily" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDataText="No Bookings Done for the selected Conference room">
                                <Columns>
                                      <asp:TemplateField visible="false ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQ_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Employee Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblssa_emp_map" runat="server" Text='<%# Eval("ssa_emp_map") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Conference " >
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("SPC_name") %>'></asp:Label>
                                            <asp:Label ID="lblssa_spc_id" Visible="false" runat="server" Text='<%# Eval("ssa_spc_id") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Date" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbldate" runat="server" Text='<%# Eval("FROM_date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Time" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblTime" runat="server" Text='<%# Eval("FROM_TIME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Time" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTIME" runat="server" Text='<%# Eval("TO_TIME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblstat" runat="server" Text='<%# Eval("stat") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                       <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <a href='modifyconference.aspx?id=<%#Eval("REQ_ID")%>&rtnurl=editconference_employee.aspx'>EDIT</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                     <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                                </Columns>
                            </asp:GridView>
                                </asp.panel>
                                </td>
                            </tr>


                        </table>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 16px; height: 100%;">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 16px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>

</asp:Content>


