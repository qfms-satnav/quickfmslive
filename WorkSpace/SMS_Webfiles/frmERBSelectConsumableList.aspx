<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmERBSelectConsumableList.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmERBSelectConsumableList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Consumable List</title>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Consumable Assets 
                    </asp:Label>
                    <hr align="center" width="60%" />
                    <br />
                </td>
            </tr>
        </table>
        <br />
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                        width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong> Consumable Assets </strong>
                </td>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                        width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;</td>
                <td align="left">
                    <asp:Panel ID="Panel1" runat="server" Width="95%" HorizontalAlign="Center">
                        <table class="tbl" id="Table1" cellspacing="0" cellpadding="0" width="100%" align="center">
                            <tr>
                                <td align="center">
                                    <asp:DataGrid ID="dgListGrid" runat="server" Width="100%" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundColumn Visible="False" DataField="id" HeaderText="ID">
                                                <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="name" SortExpression="name" HeaderText="Name">
                                                <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="Image">
                                                <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                <ItemTemplate>
                                                    <asp:Image ID="recImage" runat="server" Width="100" Height="100"></asp:Image>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Quantity">
                                                <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtQty" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Remarks">
                                                <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Select">
                                                <%--<HeaderStyle CssClass="label"></HeaderStyle>--%>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkChoice" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <asp:Panel ID="pnlRad" runat="server" HorizontalAlign="Center" Width="95%">
                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="True">Above Requests are </asp:Label>&nbsp;<strong>:</strong></td>
                                                <td valign="middle">
                                                    <asp:RadioButtonList ID="radType" runat="server" Width="112px" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="Personal">Personal</asp:ListItem>
                                                        <asp:ListItem Value="Official">Official</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsButton"></asp:Button><br>
                                    <br>
                                    <asp:Label ID="lblmsg" runat="server" CssClass="clsmessage"></asp:Label>
                                    <asp:Label ID="lblError" runat="server" CssClass="clsNote"></asp:Label></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                    height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" /></td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
