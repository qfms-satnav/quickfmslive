<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmSpaceverticalRequisition.aspx.vb" Inherits="frmSpaceverticalRequisition" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer src="../../Scripts/jquery-2.2.0.min.js"></script>
    <link href="../../BootStrapCSS/jtable/jquery-ui.css" rel="stylesheet" />

    <script defer type="text/javascript">

        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)

            var theForm = document.forms['aspnetForm'];

            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };

    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Blocking Request </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" />
                        <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Space Type <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlspacetype"
                                            Display="None" ErrorMessage="Please Select Space Type" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlseattype" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Country<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCountry"
                                            Display="None" ErrorMessage="Please Select Country" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlLocation"
                                            Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Tower<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlTower" Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlTower" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="Floor">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Floor<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlFloor"
                                            Display="None" ErrorMessage="Please Select Floor" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true" SelectionMode="Multiple"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate" Display="None" ErrorMessage="Please Enter From Date "> </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtFrmDate" Display="None" ErrorMessage="Please Enter Valid From Date " Operator="DataTypeCheck" Type="Date"> </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFrmDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvtodate" runat="server" ControlToValidate="txtToDate" Display="None" ErrorMessage="Please Enter To Date "> </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtToDate" Display="None" ErrorMessage="Please Enter Valid To Date " Operator="DataTypeCheck" Type="Date"> </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="trshift" runat="server" class="row" visible="false">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Shift<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlshift"
                                            Display="None" ErrorMessage="Please Select Shift" InitialValue="--Select--"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlshift" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="trTimeSlot" runat="server" visible="false" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From:<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr" Display="None" ErrorMessage="Please Select From Time " InitialValue="Hr"> </asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimemin" Display="None" ErrorMessage="Please Select From Minutes" InitialValue="Min"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="starttimehr" runat="server" Width="60" Enabled="False" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="starttimemin" runat="server" Width="60" Enabled="False" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To:<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr" Display="None" ErrorMessage="Please Enter To Time " InitialValue="Hr"> </asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimemin" Display="None" ErrorMessage="Please Enter To Minutes" InitialValue="Min"> </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="endtimehr" runat="server" Width="60" Enabled="False" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="endtimemin" runat="server" Width="60" Enabled="False" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div onmouseover="Tip('Click for more Requistions')" onmouseout="UnTip()">
                            <asp:LinkButton ID="lnkmore" runat="server" Text="Add More" CausesValidation="False"
                                Visible="False">
                            </asp:LinkButton>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnavail" runat="server" Text="Check Availability" CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvEnter" Visible="true" runat="server" AutoGenerateColumns="False" Width="100%" AllowSorting="true"
                                    PageSize="1" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Space Type" SortExpression="SPACETYPE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSpaceType" runat="server" Text='<%# Eval("SPACETYPE").ToString()%>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space Sub Type" SortExpression="SPACESUBTYPE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSpaceSubType" runat="server" Text='<%# Eval("SPACESUBTYPE").ToString()%>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Count">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSpaceSubType" runat="server" Text='<%# Eval("COUNT").ToString()%>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row" id="Carved" runat="server">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblfilter" runat="server" Text="Filter by : " class="col-md-4 control-label"></asp:Label>
                                        <div class="col-md-5">
                                            <asp:DropDownList ID="ddlspacetype" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <asp:DropDownList ID="ddlspacesubtype" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <asp:Button ID="btncheck" runat="server" Text="Filter" CssClass="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="DisplayData" class="row" runat="server">
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-6">
                                    <asp:GridView ID="gdavail" runat="server" AutoGenerateColumns="False" AllowSorting="true" EmptyDataText="No Space Record found."
                                        CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Space" SortExpression="SPC_ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblspcid" runat="server" Text='<%# Eval("SPC_ID").ToString()%>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Space Type" SortExpression="SPC_TYPE_NAME">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSpaceType" runat="server" Text='<%# Eval("SPC_TYPE_NAME").ToString()%>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Space Sub Type" SortExpression="SST_NAME">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSpaceSubType" runat="server" Text='<%# Eval("SST_NAME").ToString()%>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Seat Type" SortExpression="SEAT_TYPE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblseatype" runat="server" Text='<%# Eval("SEAT_TYPE").ToString()%>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <%--<span style="color: red;">*</span>--%>
                                            <asp:Label ID="lblSelVertical" class="col-md-5 control-label" runat="server" Text=""></asp:Label>
                                            <%--<asp:RequiredFieldValidator ID="rfvVTl" runat="server" ControlToValidate="ddlVertical" Display="None" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlVertical" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks</label>
                                            <div class="col-md-7">
                                                <div onmouseover="Tip('Enter remarks up to 500 characters')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                        MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit Request" />
                                        <asp:Button ID="btnViewInMap" runat="server" CssClass="btn btn-primary custom-button-color" Text="View in Map" OnClientClick="aspnetForm.target ='_blank';" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer type="text/javascript">
        $(document).ready(function () {
            jQuery('#txtNumberofReq').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });
        });
    </script>
</body>
</html>

