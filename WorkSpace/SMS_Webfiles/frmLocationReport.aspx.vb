Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports System.IO
Partial Class WorkSpace_SMS_Webfiles_frmLocationReport
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim AWT As Decimal = 0
    Dim AWT1 As Decimal = 0
    Dim AWT2 As Decimal = 0
    Dim AWT3 As Decimal = 0
    Dim AWT4 As Decimal = 0
    Dim AWT5 As Decimal = 0
    Dim AWT6 As Decimal = 0
    Dim AWT7 As Decimal = 0



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            BindLocation()
            btnexport.Visible = False
            pnlgrid.Visible = False
            txtFdate.Attributes.Add("onClick", "displayDatePicker('" + txtFdate.ClientID + "')")
            txtFdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtTdate.Attributes.Add("onClick", "displayDatePicker('" + txtTdate.ClientID + "')")
            txtTdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            t1.Visible = False

            lblawt1.ForeColor = Drawing.Color.Black
            lblawt1.Font.Bold = True
            lblowt1.ForeColor = Drawing.Color.Black
            lblowt1.Font.Bold = True
            lblAwi1.ForeColor = Drawing.Color.Black
            lblAwi1.Font.Bold = True
            lblowi1.ForeColor = Drawing.Color.Black
            lblowi1.Font.Bold = True
            lblawbpo1.ForeColor = Drawing.Color.Black
            lblawbpo1.Font.Bold = True
            lblowbpo1.ForeColor = Drawing.Color.Black
            lblowbpo1.Font.Bold = True
            lblawstl1.ForeColor = Drawing.Color.Black
            lblawstl1.Font.Bold = True
            lblowstl1.ForeColor = Drawing.Color.Black
            lblowstl1.Font.Bold = True
            

           
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
            If txtFdate.Text = "" Then
                param(0).Value = DBNull.Value
            Else
                param(0).Value = txtFdate.Text
            End If


            param(1) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
            If txtTdate.Text = "" Then
                param(1).Value = DBNull.Value
            Else
                param(1).Value = txtTdate.Text
            End If


            param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
            param(2).Value = ddllocation.SelectedItem.Value

            param(3) = New SqlParameter("@SORT_EXP", SqlDbType.NVarChar, 200)
            param(3).Value = "LOCATION_CODE"

            param(4) = New SqlParameter("@SORT_DIR", SqlDbType.NVarChar, 200)
            param(4).Value = "ASC"

            ObjSubsonic.BindGridView(gvitems, "SP_LOCATION_AVAILABLE_OCCUPIED_HISTORY", param)
            If gvitems.Rows.Count > 0 Then
                btnexport.Visible = True
            Else
                btnexport.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_BIND_LOCATION_HISTORY")
            ddllocation.DataSource = sp.GetDataSet()
            ddllocation.DataTextField = "LCM_NAME"
            ddllocation.DataValueField = "LCM_CODE"
            ddllocation.DataBind()
            ddllocation.Items.Insert(0, New ListItem("--Select--", ""))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtFdate.Text <> "" Or txtTdate.Text <> "" Or ddllocation.SelectedIndex > 0 Then
            If txtFdate.Text > txtTdate.Text Then
                lblMsg.Text = "To Date Should be greater than from date"
            Else
                lblMsg.Text = ""
                BindGrid()
                Bindtot()
                pnlgrid.Visible = True
                t1.Visible = True
            End If
        Else
            lblMsg.Text = "Please Enter From Date and To Date or Select Location"
        End If
    End Sub
    Private Sub BindTot()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_LOCATION_AVAILABLE_OCCUPIED_HISTORY_TOT")
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.DateTime)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.DateTime)
        sp.Command.AddParameter("@LOCATION", ddllocation.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblawt1.Text = "Total Available WT :" & ds.Tables(0).Rows(0).Item("AVAILABLE_WT")
            lblowt1.Text = "Total Occupied WT :" & ds.Tables(0).Rows(0).Item("OCCUPIED_WT")
            lblAwi1.Text = "Total Available WI :" & ds.Tables(0).Rows(0).Item("AVAILABLE_WI")
            lblowi1.Text = "Total Occupied WT :" & ds.Tables(0).Rows(0).Item("OCCUPIED_WI")
            lblawbpo1.Text = "Total Available WBPO :" & ds.Tables(0).Rows(0).Item("AVAILABLE_WBPO")
            lblowbpo1.Text = "Total Occupied WBPO :" & ds.Tables(0).Rows(0).Item("OCCUPIED_WBPO")
            lblawstl1.Text = "Total Available WSTL :" & ds.Tables(0).Rows(0).Item("AVAILABLE_WSTL")
            lblowstl1.Text = "Total Occupied WSTL :" & ds.Tables(0).Rows(0).Item("OCCUPIED_WSTL")
        End If
    End Sub
    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Protected Sub gvitems_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvitems.Sorting
        Dim expression As String = e.SortExpression
        Dim dir As String = ""
        If e.SortDirection = SortDirection.Descending Then
            dir = "ASC"
        Else
            dir = "DESC"
        End If
        BindSorting(expression, dir)
    End Sub
    Private Sub BindSorting(ByVal expression As String, ByVal dir As String)
        Try
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
            If txtFdate.Text = "" Then
                param(0).Value = DBNull.Value
            Else
                param(0).Value = txtFdate.Text
            End If


            param(1) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
            If txtTdate.Text = "" Then
                param(1).Value = DBNull.Value
            Else
                param(1).Value = txtTdate.Text
            End If

            param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
            param(2).Value = ddllocation.SelectedItem.Value

            param(3) = New SqlParameter("@SORT_EXP", SqlDbType.NVarChar, 200)
            param(3).Value = expression

            param(4) = New SqlParameter("@SORT_DIR", SqlDbType.NVarChar, 200)
            param(4).Value = dir

            ObjSubsonic.BindGridView(gvitems, "SP_LOCATION_AVAILABLE_OCCUPIED_HISTORY", param)
            If gvitems.Rows.Count > 0 Then
                btnexport.Visible = True
            Else
                btnexport.Visible = False
            End If
            pnlgrid.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim gv As New GridView
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
        If txtFdate.Text = "" Then
            param(0).Value = DBNull.Value
        Else
            param(0).Value = txtFdate.Text
        End If


        param(1) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
        If txtTdate.Text = "" Then
            param(1).Value = DBNull.Value
        Else
            param(1).Value = txtTdate.Text
        End If


        param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        param(2).Value = ddllocation.SelectedItem.Value

        param(3) = New SqlParameter("@SORT_EXP", SqlDbType.NVarChar, 200)
        param(3).Value = "LOCATION_CODE"

        param(4) = New SqlParameter("@SORT_DIR", SqlDbType.NVarChar, 200)
        param(4).Value = "ASC"

        ObjSubsonic.BindGridView(gv, "SP_LOCATION_AVAILABLE_OCCUPIED_HISTORY", param)
        Export("Available & Occupied Locations History" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
    End Sub
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub gvitems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvitems.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
           
            Dim lblloc As Label = DirectCast(e.Row.FindControl("lblloc"), Label)

            Dim rowArea As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AVAILABLE_WT"))
            AWT = AWT + rowArea

            Dim rowArea1 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCCUPIED_WT"))
            AWT1 = AWT1 + rowArea1


            Dim rowArea2 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AVAILABLE_WI"))
            AWT2 = AWT2 + rowArea2

            Dim rowArea3 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCCUPIED_WI"))
            AWT3 = AWT3 + rowArea3

            Dim rowArea4 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AVAILABLE_WBPO"))
            AWT4 = AWT4 + rowArea4

            Dim rowArea5 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCCUPIED_WBPO"))
            AWT5 = AWT5 + rowArea5

            Dim rowArea6 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AVAILABLE_WSTL"))
            AWT6 = AWT6 + rowArea6


            Dim rowArea7 As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCCUPIED_WSTL"))
            AWT7 = AWT7 + rowArea7

           

        End If



        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTawt As Label = DirectCast(e.Row.FindControl("lblTawt"), Label)
            lblTawt.Text = AWT.ToString()
            lblTawt.ForeColor = Drawing.Color.Black
            lblTawt.Font.Bold = True

            Dim lblTowt As Label = DirectCast(e.Row.FindControl("lblTowt"), Label)
            lblTowt.Text = AWT1.ToString()
            lblTowt.ForeColor = Drawing.Color.Black
            lblTowt.Font.Bold = True

            Dim lblTawi As Label = DirectCast(e.Row.FindControl("lblTawi"), Label)
            lblTawi.Text = AWT2.ToString()
            lblTawi.ForeColor = Drawing.Color.Black
            lblTawi.Font.Bold = True

            Dim lblTowi As Label = DirectCast(e.Row.FindControl("lblTowi"), Label)
            lblTowi.Text = AWT3.ToString()
            lblTowi.ForeColor = Drawing.Color.Black
            lblTowi.Font.Bold = True

            Dim lblTawbpo As Label = DirectCast(e.Row.FindControl("lblTawbpo"), Label)
            lblTawbpo.Text = AWT4.ToString()
            lblTawbpo.ForeColor = Drawing.Color.Black
            lblTawbpo.Font.Bold = True

            Dim lblTowbpo As Label = DirectCast(e.Row.FindControl("lblTowbpo"), Label)
            lblTowbpo.Text = AWT5.ToString()
            lblTowbpo.ForeColor = Drawing.Color.Black
            lblTowbpo.Font.Bold = True

            Dim lblTawstl As Label = DirectCast(e.Row.FindControl("lblTawstl"), Label)
            lblTawstl.Text = AWT6.ToString()
            lblTawstl.ForeColor = Drawing.Color.Black
            lblTawstl.Font.Bold = True

            Dim lblTowstl As Label = DirectCast(e.Row.FindControl("lblTowstl"), Label)
            lblTowstl.Text = AWT7.ToString()
            lblTowstl.ForeColor = Drawing.Color.Black
            lblTowstl.Font.Bold = True

        End If
    End Sub
End Class
