﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_RentHistory
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If

        If Not IsPostBack Then
            btnsubmit.Visible = False
            btnpaid.Visible = False
            lblunpaid.Visible = False
            lblpaid.Visible = False
            BindGrid()
            Bindgrid1()
        End If
        txtFdate.Attributes.Add("readonly", "readonly")
        txtTdate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RENT_HISTORY")
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.String)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()

        gvitems.DataSource = ds.Tables(0)
        gvitems.DataBind()
        If gvitems.Rows.Count > 0 Then
            btnsubmit.Visible = True
            lblunpaid.Visible = True

        Else
            btnsubmit.Visible = False
            lblunpaid.Visible = False
        End If

      

    End Sub
    Private Sub Bindgrid1()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RENT_HISTORY")
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.String)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        gvpaid.DataSource = ds.Tables(1)
        gvpaid.DataBind()
        If gvpaid.Rows.Count > 0 Then
            btnpaid.Visible = True
            lblpaid.Visible = True
        Else
            btnpaid.Visible = False
            lblpaid.Visible = False
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim d As Integer = gvitems.PageCount
        Dim texts As String() = New String(gvitems.PageSize - 1) {}
        Dim count As Integer = 0
        For Each row As GridViewRow In gvitems.Rows
            Dim lblproperty As Label = DirectCast(row.FindControl("lblproperty"), Label)
            Dim lbllease As Label = DirectCast(row.FindControl("lbllease"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblID As Label = DirectCast(row.FindControl("lblID"), Label)
            Dim lbldate As Label = DirectCast(row.FindControl("lbldate"), Label)
            Dim lblrent As Label = DirectCast(row.FindControl("lblrent"), Label)
            Dim lblstat As Label = DirectCast(row.FindControl("lblstat"), Label)
            If chkSelect.Checked = True Then
                count = count + 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_LEASE_RENT_STATUS")
                sp.Command.AddParameter("@EMP_NO", lblID.Text, DbType.String)
                sp.Command.AddParameter("@PROPERTY", lblproperty.Text, DbType.String)
                sp.Command.AddParameter("@LEASE", lbllease.Text, DbType.String)
                sp.Command.AddParameter("@DATE", lbldate.Text, DbType.DateTime)
                sp.Command.AddParameter("@STATUS", lblstat.Text, DbType.String)
                sp.ExecuteScalar()
            End If
        Next

        If count = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please select any of the checkbox to update the status');", True)
            Exit Sub
        End If

        BindGrid()
        Bindgrid1()
        If gvitems.Rows.Count > 0 Then
            btnsubmit.Visible = True
            lblunpaid.Visible = True
        Else
            btnsubmit.Visible = False
            lblunpaid.Visible = False
        End If
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnsearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        If txtfdate.text = "" Or txttdate.text = "" Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please select from date and to date');", True)
            Exit Sub
        ElseIf CDate(txtFdate.Text) > CDate(txtTdate.Text) Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('To Date should be greater than from date');", True)
            Exit Sub
        Else
            BindGrid()
            Bindgrid1()
        End If
    End Sub

    Protected Sub gvpaid_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvpaid.PageIndexChanging
        gvpaid.PageIndex = e.NewPageIndex()
        Bindgrid1()
    End Sub

    Protected Sub btnpaid_Click(sender As Object, e As EventArgs) Handles btnpaid.Click
        Dim d As Integer = gvpaid.PageCount
        Dim texts As String() = New String(gvpaid.PageSize - 1) {}
        Dim count As Integer = 0
        For Each row As GridViewRow In gvpaid.Rows
            Dim lblproperty As Label = DirectCast(row.FindControl("lblproperty"), Label)
            Dim lbllease As Label = DirectCast(row.FindControl("lbllease"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblID As Label = DirectCast(row.FindControl("lblID"), Label)
            Dim lbldate As Label = DirectCast(row.FindControl("lbldate"), Label)
            Dim lblrent As Label = DirectCast(row.FindControl("lblrent"), Label)
            Dim lblstat As Label = DirectCast(row.FindControl("lblstat"), Label)
            If chkSelect.Checked = True Then
                count = count + 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_LEASE_RENT_STATUS")
                sp.Command.AddParameter("@EMP_NO", lblID.Text, DbType.String)
                sp.Command.AddParameter("@PROPERTY", lblproperty.Text, DbType.String)
                sp.Command.AddParameter("@LEASE", lbllease.Text, DbType.String)
                sp.Command.AddParameter("@DATE", lbldate.Text, DbType.DateTime)
                sp.Command.AddParameter("@STATUS", lblstat.Text, DbType.String)
                sp.ExecuteScalar()
            End If
        Next

        If count = 0 Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('Please select any of the checkbox to update the status');", True)
            Exit Sub
        End If

        Bindgrid1()
        BindGrid()
        If gvpaid.Rows.Count > 0 Then
            btnpaid.Visible = True
            lblpaid.Visible = True
        Else
            btnpaid.Visible = False
            lblpaid.Visible = False
        End If
    End Sub
End Class
