
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports System.Threading
Partial Class WorkSpace_SMS_Webfiles_CalendarReportDetails
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub btnToday_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnToday.Click
        GetToday()
    End Sub

    Private Sub GetToday()
        btnToday.CssClass = "Clicked"
        btnDaily.CssClass = "Initial"
        btnWeekly.CssClass = "Initial"
        btnMonthly.CssClass = "Initial"
        MainView.ActiveViewIndex = 0
        Dim ds As New DataSet
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@FROMDATE", SqlDbType.date)
        param(0).Value = getoffsetdatetime(DateTime.Now)
        param(1) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("cty")
        param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        param(2).Value = Request.QueryString("loc")
        ds = objsubsonic.GetSubSonicDataSet("GetVerticalCount_Today", param)
        gvToday.DataSource = ds
        gvToday.DataBind()
    End Sub
    Protected Sub btnDaily_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDaily.Click
        'lbldate.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.LongDate)
        lbldate.Text = FormatDateTime(Request.QueryString("dt"), DateFormat.LongDate)
        BindDailyGrid()
    End Sub

    Private Sub BindDailyGrid()
        btnToday.CssClass = "Initial"
        btnDaily.CssClass = "Clicked"
        btnWeekly.CssClass = "Initial"
        btnMonthly.CssClass = "Initial"
        MainView.ActiveViewIndex = 1
        Dim ds As New DataSet

        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@FROMDATE", SqlDbType.date)
        param(0).Value =CDate(lbldate.Text).date
        param(1) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("cty")
        param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        param(2).Value = Request.QueryString("loc")

        ds = objsubsonic.GetSubSonicDataSet("GetVerticalCount_Today", param)
        gvDaily.DataSource = ds
        gvDaily.DataBind()
    End Sub

    Protected Sub btnWeekly_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWeekly.Click
        btnToday.CssClass = "Initial"
        btnDaily.CssClass = "Initial"
        btnWeekly.CssClass = "Clicked"
        btnMonthly.CssClass = "Initial"
        MainView.ActiveViewIndex = 2
        GetWeekly(Request.QueryString("dt"))

    End Sub

    Private Sub GetWeekly(ByVal todaydate As Date)
        lblweekday.Text = todaydate
        Dim GivenDate As DateTime = todaydate
        Dim delta As Integer = Convert.ToInt32(GivenDate.DayOfWeek)
        If delta = 0 Then
            delta = delta + 7
        End If
        Dim moday As DateTime = GivenDate.AddDays(1 - delta)
        Dim sunday As DateTime = GivenDate.AddDays(7 - delta)
        lblWeek.Text = MonthName(todaydate.Date.Month) & " (" & CDate(moday).Day & "-" & CDate(sunday).Day & ") " & todaydate.Date.Year
        '--------------------------

        Dim intDayOfWeek As Integer
        intDayOfWeek = todaydate.DayOfWeek
        Dim strTodaysDate As String = ""
        Dim arrList As New ArrayList()
        For i As Integer = 0 To 6
            strTodaysDate = ""
            'strTodaysDate = FormatDateTime(todaydate.Date.AddDays(-intDayOfWeek + 1), DateFormat.LongDate)

            strTodaysDate = FormatDateTime(CDate(FormatDateTime(todaydate, DateFormat.LongDate)).Date.AddDays(-intDayOfWeek + 1), DateFormat.LongDate)

            If i <= intDayOfWeek Then
                strTodaysDate = FormatDateTime(CDate(FormatDateTime(strTodaysDate, DateFormat.LongDate)).Date.AddDays(i), DateFormat.LongDate)
            Else
                strTodaysDate = FormatDateTime(CDate(FormatDateTime(strTodaysDate, DateFormat.LongDate)).Date.AddDays(i), DateFormat.LongDate)
            End If
            arrList.Add(New ListItem(strTodaysDate, strTodaysDate.Split(",")(0).ToString))
        Next
        gvWeekly.DataSource = arrList
        gvWeekly.DataBind()
    End Sub

    Protected Sub btnMonthly_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMonthly.Click
        btnToday.CssClass = "Initial"
        btnDaily.CssClass = "Initial"
        btnWeekly.CssClass = "Initial"
        btnMonthly.CssClass = "Clicked"
        MainView.ActiveViewIndex = 3
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB", False)


        If Not Page.IsPostBack Then

            btnToday.CssClass = "Clicked"
            GetToday()
            MainView.ActiveViewIndex = 0
            lbldate.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.LongDate)
        End If
    End Sub

    Protected Sub PrevDay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PrevDay.Click
        lbldate.Text = FormatDateTime(CDate(lbldate.Text).Date.AddDays(-1), DateFormat.LongDate)
        BindDailyGrid()
    End Sub

    Protected Sub NextDay_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles NextDay.Click
        lbldate.Text = FormatDateTime(CDate(lbldate.Text).Date.AddDays(1), DateFormat.LongDate)
        BindDailyGrid()
    End Sub

    Protected Sub NextWeek_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles NextWeek.Click
        GetWeekly(CDate(lblweekday.Text).Date.AddDays(DayOfWeek.Saturday))
    End Sub

    Protected Sub PrevWeek_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PrevWeek.Click
        GetWeekly(CDate(lblweekday.Text).Date.AddDays(-DayOfWeek.Saturday))
    End Sub

    Protected Sub gvWeekly_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvWeekly.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbldate1 As Label = CType(e.Row.FindControl("lbldate1"), Label)
            Dim lblDay As Label = CType(e.Row.FindControl("lblDay"), Label)

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@FROMDATE", SqlDbType.date)
            param(0).Value = CDate(Convert.ToDateTime(lbldate1.Text)).Date
            param(1) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
            param(1).Value = Request.QueryString("cty")
            param(2) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
            param(2).Value = Request.QueryString("loc")
            Dim ds As New DataSet
            ds = objsubsonic.GetSubSonicDataSet("GetVerticalCount_Today", param)
            If ds.Tables(0).Rows.Count > 0 Then
                lbldate1.Text = " " & lbldate1.Text & " (" & ds.Tables(0).Rows.Count & ") "
            End If

            'Create a new GridView for displaying the expanded details
            Dim gv As New GridView
            gv.DataSource = ds
            gv.ID = "grdSQLOverLimitDetail" & e.Row.RowIndex 'Since a gridview is 
            'being created for each row they each need a unique ID, so append the row index
            gv.AutoGenerateColumns = False
            gv.CssClass = "subgridview"
            'AddHandler gv.RowDataBound, AddressOf grdOverLimitDetails_RowDataBound
            'Add a          'rowdatabound method for the new GridView

            'Add fields to the expanded details GridView
            Dim bf1 As New BoundField
            bf1.DataField = "SSA_VERTICAL"
            'bf1.DataFormatString = "{0:d}"
            bf1.HeaderText = "Vertical"
            gv.Columns.Add(bf1)

            Dim bf2 As New BoundField
            bf2.DataField = "SSA_FROM_DATE"
            'bf1.DataFormatString = "{0:d}"
            bf2.HeaderText = "From Date"
            gv.Columns.Add(bf2)

            Dim bf3 As New BoundField
            bf3.DataField = "SSA_TO_DT"
            'bf1.DataFormatString = "{0:d}"
            bf3.HeaderText = "To date"
            gv.Columns.Add(bf3)

            Dim bf4 As New BoundField
            bf4.DataField = "FROM_TIME"
            'bf1.DataFormatString = "{0:d}"
            bf4.HeaderText = "From Time"
            gv.Columns.Add(bf4)

            Dim bf5 As New BoundField
            bf5.DataField = "TO_TIME"
            'bf1.DataFormatString = "{0:d}"
            bf5.HeaderText = "To Time"
            gv.Columns.Add(bf5)

            Dim bf6 As New BoundField
            bf6.DataField = "cnt"
            'bf1.DataFormatString = "{0:d}"
            bf6.HeaderText = "Count"
            gv.Columns.Add(bf6)

           

            gv.EmptyDataText = "No Records"

            'Create the show/hide button which will be displayed on each row of the main GridView
            Dim btn As Web.UI.WebControls.Image = New Web.UI.WebControls.Image
            btn.ID = "btnDetail"
            btn.ImageUrl = "~/Images/detail.gif"
            btn.Attributes.Add("onclick", "javascript: gvrowtoggle	(" & e.Row.RowIndex + (e.Row.RowIndex + 2) & ")") 'Adds the javascript 
            'function to the show/hide button, passing the row to be toggled as a parameter

            'Add the expanded details row after each record in the main GridView
            Dim tbl As Table = DirectCast(e.Row.Parent, Table)
            Dim tr As New GridViewRow(e.Row.RowIndex + 1, -1, _
         DataControlRowType.EmptyDataRow, DataControlRowState.Normal)
            tr.CssClass = "hidden"
            Dim tc As New TableCell()
            tc.ColumnSpan = gvWeekly.Columns.Count
            tc.BorderStyle = BorderStyle.None
            tc.BackColor = Drawing.Color.AliceBlue
            tc.Controls.Add(gv) 'Add the expanded details GridView to the newly-created cell
            tr.Cells.Add(tc) 'Add the newly-created cell to the newly-created row
            tbl.Rows.Add(tr) ' Add the newly-ccreated row to the main GridView
            e.Row.Cells(0).Controls.Add(btn) 'Add the show/hide button to the main GridView row
            gv.DataBind() 'Bind the expanded details GridView to its datasource


        End If

    End Sub
    Protected Sub grdOverLimitDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

    End Sub

    Protected Sub gvToday_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvToday.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then

            e.Row.Cells(2).Text = Session("Parent")
        End If
    End Sub
End Class


''Create a new GridView for displaying the expanded details
'Dim gv As New GridView
'            gv.DataSource = ds
'            gv.ID = "grdSQLOverLimitDetail" & e.Row.RowIndex 'Since a gridview is 
''being created for each row they each need a unique ID, so append the row index
'            gv.AutoGenerateColumns = True
''gv.CssClass = "subgridview"
'            gv.Width = "200"
''AddHandler gv.RowDataBound, AddressOf grdOverLimitDetails_RowDataBound

''Create the show/hide button which will be displayed on each row of the main GridView
'Dim btn As Web.UI.WebControls.Image = New Web.UI.WebControls.Image
'            btn.ID = "btnDetail"
'            btn.ImageUrl = "~/Images/detail.gif"
'            btn.Attributes.Add("onclick", "javascript: gvrowtoggle	(" & e.Row.RowIndex + (e.Row.RowIndex + 2) & ")")
''Adds the javascript 
''function to the show/hide button, passing the row to be toggled as a parameter

''Add the expanded details row after each record in the main GridView
'Dim tbl As Table = DirectCast(e.Row.Parent, Table)
'Dim tr As New GridViewRow(e.Row.RowIndex + 1, -1, _
'DataControlRowType.EmptyDataRow, DataControlRowState.Normal)
'            tr.CssClass = "hidden"
'Dim tc As New TableCell()
'            tc.ColumnSpan = gvWeekly.Columns.Count
'            tc.BorderStyle = BorderStyle.None
'            tc.BackColor = Drawing.Color.AliceBlue
''
'            tc.Controls.Add(gv) 'Add the expanded details GridView to the newly-created cell
'            tr.Cells.Add(tc) 'Add the newly-created cell to the newly-created row
'            tbl.Rows.Add(tr) ' Add the newly-ccreated row to the main GridView
'            e.Row.Cells(0).Controls.Add(btn) 'Add the show/hide button to the main GridView row
'            gv.DataBind() 'Bind the expanded details GridView to its datasource