<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SearchEmployee.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_SearchEmployee" Title="Search Employee" %>

<%@ Register Src="~/WorkSpace/SMS_Webfiles/Controls/SearchEmployee.ascx" TagName="SearchEmployee" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Search Employee</h3>
            </div>
            <div class="card">
                <form id="form1" class="form-horizontal" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                        ForeColor="Red" ValidationGroup="Val1" />
                    <uc1:SearchEmployee runat="server" ID="SearchEmployee" />
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
