Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Partial Class WorkSpace_SMS_Webfiles_frmERBFac_Participants
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Public strSQL As String = ""
    Public strSQL1 As String = ""
    Public str As String = ""
    Public strSelImg As String = ""
    Public strRem As String = ""

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    ' ------------------- Json ------------------------------------
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/themes/flick/jquery-ui.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/css/ui.jqgrid.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.min.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/i18n/grid.locale-en.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.jqGrid.min.js")))
    '    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js_source/TowerwiseUtilization.js")))
    '    '--------------------------------------------------------------
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))

    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'TextBox1.Attributes.Add("style", "display:none")


        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Try
                BindFilter()
                'Dim strScript As String
                'strScript = "@SCRIPT>window.opener.document.forms(0).submit();" & _
                '"self.close()@/SCRIPT>"
                'btnSubmit.Attributes.Add("CloseChild", strScript.Replace("@", "<"))
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub BindFilter()
        objsubsonic.Binddropdown(cboFilter, "GetUsersDepartment", "DEP_NAME", "DEP_CODE")
    End Sub

    Protected Sub cboFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilter.SelectedIndexChanged
        BindGrid()
    End Sub

    Private Sub BindGrid()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@dep_code", SqlDbType.NVarChar, 200)
        param(0).Value = cboFilter.SelectedItem.Value
        param(1) = New SqlParameter("@aur_id", SqlDbType.NVarChar, 200)
        param(1).Value = Session("uid")
        param(2) = New SqlParameter("@req_id", SqlDbType.NVarChar, 200)
        param(2).Value = Request.QueryString("req_id")
        objsubsonic.BindDataGrid(dgParticipants, "GetUsersByFilter", param)
        If dgParticipants.Items.Count > 0 Then
            btnSubmit.Visible = True
        Else
            btnSubmit.Visible = False
        End If
    End Sub

    Protected Sub dgParticipants_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgParticipants.PageIndexChanged
        Try
            Dim i As Integer
            Dim struid As String
            strSelImg = ""
            For i = 0 To dgParticipants.Items.Count - 1

                Dim chkSelect As CheckBox = CType(dgParticipants.Items(i).FindControl("chkSelectPar"), CheckBox)
                If chkSelect.Checked = True Then

                    strSelImg = strSelImg & "," & Val(dgParticipants.Items(i).Cells(0).Text)
                    strRem = strRem & "," & CType(dgParticipants.Items(i).FindControl("txtRemPar"), TextBox).Text

                    If struid = "" Then
                        struid = dgParticipants.Items(i).Cells(0).Text
                    Else
                        struid = struid & "," & dgParticipants.Items(i).Cells(0).Text
                    End If
                End If
            Next
            Session("struid") = struid
            dgParticipants.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            lblError.Text = ex.Message

        End Try

        'dgParticipants.CurrentPageIndex = e.NewPageIndex
        'BindGrid()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try

      
            If radSelection.SelectedItem Is Nothing Then
                lblmsg.Text = "Select whether you want your name to be included in the participants? "
                lblmsg.Visible = True
                Exit Sub
            Else
                lblmsg.Visible = False
            End If

            For c As Integer = 0 To dgParticipants.Items.Count - 1
                Dim chkSelect As CheckBox = CType(dgParticipants.Items(c).FindControl("chkSelectPar"), CheckBox)
                If chkSelect.Checked = True Then
                    strSelImg = strSelImg & "," & dgParticipants.Items(c).Cells(0).Text
                    strRem = strRem & "," & CType(dgParticipants.Items(c).FindControl("txtRemPar"), TextBox).Text
                End If
            Next


            Dim sel As Array = strSelImg.Split(",")
            Dim remar As Array = strRem.Split(",")


            For c As Integer = 1 To sel.Length - 1

                Dim param(2) As SqlParameter
                param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
                param(0).Value = Request.QueryString("req_id")
                param(1) = New SqlParameter("@AUR_ID_Part", SqlDbType.NVarChar, 200)
                param(1).Value = sel(c)
                param(2) = New SqlParameter("@aur_dept", SqlDbType.NVarChar, 200)
                param(2).Value = cboFilter.SelectedItem.Value
                objsubsonic.GetSubSonicExecute("PARTICIPANTS_LIST_Insert", param)



            Next
            If radSelection.SelectedItem.Value = 0 Then
                Dim param(2) As SqlParameter
                param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
                param(0).Value = Request.QueryString("req_id")
                param(1) = New SqlParameter("@AUR_ID_Part", SqlDbType.NVarChar, 200)
                param(1).Value = Session("uid")
                param(2) = New SqlParameter("@aur_dept", SqlDbType.NVarChar, 200)
                param(2).Value = cboFilter.SelectedItem.Value
                objsubsonic.GetSubSonicExecute("PARTICIPANTS_LIST_Insert", param)
            End If
            If sel.Length = 0 Then
                lblmsg.Text = "Select the Participants for the Event"
                lblmsg.Visible = True
                Exit Sub
            Else
                Dim strScript As String
                strScript = "@SCRIPT>window.opener.document.forms(0).submit();" & _
                "self.close()@/SCRIPT>"
                Page.RegisterClientScriptBlock("CloseChild", strScript.Replace("@", "<"))
            End If
            BindParticipants()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim StrSearchQry As String = txtSearch.Text


        If String.IsNullOrEmpty(txtSearch.Text) = False Then
            '*** Get emp id 
            Dim aur_id As String = ""
            Dim a1
            a1 = StrSearchQry.Split(" ")
           
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("req_id")
            param(1) = New SqlParameter("@AUR_ID_Part", SqlDbType.NVarChar, 200)
            param(1).Value = a1(0).ToString
            param(2) = New SqlParameter("@aur_dept", SqlDbType.NVarChar, 200)
            param(2).Value = ""
            objsubsonic.GetSubSonicExecute("PARTICIPANTS_LIST_Insert", param)
            BindParticipants()


        End If

        txtSearch.Text = ""


    End Sub

    Private Sub BindParticipants()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Reqid", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("req_id")
        objsubsonic.BindDataGrid(dgParticipantsSearch, "getSelectedPartcipantslist", param)
        If dgParticipantsSearch.Items.Count > 0 Then
            dgParticipantsSearch.Visible = True
        Else
            dgParticipantsSearch.Visible = False
        End If
    End Sub

    Protected Sub dgParticipantsSearch_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgParticipantsSearch.PageIndexChanged
        dgParticipantsSearch.CurrentPageIndex = e.NewPageIndex
        BindParticipants()
    End Sub

    Private Sub dgParticipantsSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgParticipantsSearch.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim param(1) As SqlParameter
                param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
                param(0).Value = e.Item.Cells(1).Text
                param(1) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param(1).Value = Request.QueryString("req_id")
                objsubsonic.GetSubSonicExecuteScalar("DELETEPARTICIPANTS_LIST", param)
                BindParticipants()
            End If
        Catch ex As Exception
            lblmsg.Text = ex.Message
        End Try
    End Sub

End Class
