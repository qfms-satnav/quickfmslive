﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Conferencreate.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Conferencreate" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Add Conference Room
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Conference Room and Select Modify to modify the existing Conference Room" />
                                        Add
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Conference Room and Select Modify to modify the existing Conference Room" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="trLName" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">

                                            <asp:Label ID="lblConfname" class="col-md-5 control-label" runat="server">Select Conference Room<span style="color: red;">*</span></asp:Label>
                                             <asp:RequiredFieldValidator ID="rfconfcode" runat="server" ControlToValidate="ddlCName" Display="None" ErrorMessage="Please Select Conference Room" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlCName" runat="server" AutoPostBack="True" CssClass="selectpicker" TabIndex="1" data-live-search="true" ToolTip="Select Conference Room">

                                                    <asp:ListItem Value="1">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="trctype" runat="server">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="LblConftype" class="col-md-5 control-label" runat="server">Select Conference Type <span style="color: red;">*</span></asp:Label>

                                            <asp:RequiredFieldValidator ID="rfvCode0" runat="server" ControlToValidate="ddlConftype" Display="None" ErrorMessage="Please Select Conference Type" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlConftype" runat="server" CssClass="selectpicker" TabIndex="2" data-live-search="true" ToolTip="Select Conference Type">

                                                    <asp:ListItem Value="1">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Conference Code <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server"
                                            ErrorMessage="Please Enter Conference Code " ControlToValidate="txtConfrencecode" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtConfrencecode"
                                            Display="None" ErrorMessage="Please enter block code in alphanumerics" ValidationExpression="^[0-9a-zA-Z]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtConfrencecode" MaxLength="15" runat="server" TabIndex="3" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Conference Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server"
                                            ControlToValidate="txtConferenceName" Display="None" ErrorMessage="Please Enter Conference Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtConferenceName"
                                            Display="None" ErrorMessage="Please Enter Conference Name in alphanumerics and (space,-,_ ,(,),/,\,, allowed)"
                                            ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtConferenceName" MaxLength="50" TabIndex="4" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">City Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select City" TabIndex="5">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvLoc" runat="server"
                                            ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location "
                                            InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Location" TabIndex="6">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Tower Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTwr" runat="server" ControlToValidate="ddlTower" Display="None" ErrorMessage="Please Select Tower " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlTower" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true" ToolTip="Select Tower" TabIndex="7">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Floor Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvFloor" runat="server" ControlToValidate="ddlFloor" Display="None" ErrorMessage="Please Select Floor " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Location" TabIndex="8">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Conference Capacity<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtconfcapacity"
                                            Display="None" ErrorMessage="Please Enter Capacity  Of The Conference" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtconfcapacity" Display="None" ErrorMessage="Please Enter Only Numericals" ValidationExpression="^[0-9]{1,2}$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter Capacity for the conference')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtconfcapacity" runat="server" CssClass="form-control" TabIndex="9"
                                                    MaxLength="2"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" TabIndex="10"></asp:Button>
                                    <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" TabIndex="11" Text="Back" PostBackUrl="~/WorkSpace/SMS_Webfiles/ConferenceMasterr.aspx"></asp:Button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" EmptyDataText="No Conference Room Found."
                                    PageSize="15" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:BoundField DataField="LCM_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Location Name" />
                                        <asp:BoundField DataField="FLR_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Floor Name" />
                                        <asp:BoundField DataField="CONFERENCE_ROOM_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Conference Name" />
                                        <asp:BoundField DataField="CAPACITY" ItemStyle-HorizontalAlign="Left" HeaderText="Capacity" />
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
