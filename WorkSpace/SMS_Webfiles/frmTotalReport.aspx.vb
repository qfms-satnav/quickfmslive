Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail


Partial Class WorkSpace_SMS_Webfiles_frmTotalReport
    Inherits System.Web.UI.Page
    Dim objMaster As New clsMasters()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            Try
                FillGrid()

                objMaster.Bindlocation(ddlLocation)
                ddlLocation.Items.Insert(1, "--All Locations--")
                LoadCity()
            Catch ex As Exception
                Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Requisition", "Load", ex)
            End Try
        End If
    End Sub

    Public Sub FillGrid()
        Try
            'Dim Parms(1) As SqlParameter
            'Dim MLoc As String
            'Dim MCheck As String
            'Try
            '    If ddlLocation.SelectedItem.Text <> "--All Locations--" Then
            '        MLoc = ddlLocation.SelectedItem.Value
            '        MCheck = "0"
            '    Else
            '        MLoc = "BANG"
            '        MCheck = "1"
            '    End If
            'Catch ex As Exception
            '    MLoc = "BANG"
            '    MCheck = "1"
            'End Try

            'Parms(0) = New SqlParameter("@VC_LOCATION", MLoc)
            'Parms(1) = New SqlParameter("@VC_CHECK", MCheck)
            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_TotalReport", Parms)
            'GridView1.DataSource = ds
            'GridView1.DataBind()


            Dim MLoc As String = ""
            Dim MLoc_Type As String = ""
            Dim parms(1) As SqlParameter
            'Dim sp1 As New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
            'Dim sp2 As New SqlParameter("@LOC_TYPE", SqlDbType.NVarChar, 200)
            If ddlLocation.SelectedItem.Text <> "--All Locations--" Then
                MLoc = ddlLocation.SelectedItem.Value
            End If

            'sp1.Value = MLoc
            'sp2.Value = MLoc_Type

            parms(0) = New SqlParameter("@CITY", MLoc)
            parms(1) = New SqlParameter("@LOC_TYPE", MLoc_Type)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "TOTAL_CONSOLIDATE_REPORT", parms)
            GV_ConsolidateRpt_STPI.DataSource = ds
            GV_ConsolidateRpt_STPI.DataBind()

            If GV_ConsolidateRpt_STPI.Rows.Count > 0 Then
                ' FreezeGridviewHeader(GV_ConsolidateRpt_STPI, tb1, PanelContainer1)
                lblSTPI.Visible = True
                lblSTPI.Text = ddlCity.SelectedItem.Text & "_" & ddlLocation.SelectedItem.Text
            Else
                lblSTPI.Visible = False
            End If

            'MLoc_Type = "SEZ"


            'Parms(0) = New SqlParameter("@CITY", MLoc)
            'Parms(1) = New SqlParameter("@LOC_TYPE", MLoc_Type)
            'Dim ds1 As New DataSet
            'ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "TOTAL_Consolidate_Report", Parms)
            'GV_ConsolidateRpt_SEZ.DataSource = ds1
            'GV_ConsolidateRpt_SEZ.DataBind()

            'If GV_ConsolidateRpt_SEZ.Rows.Count > 0 Then
            '    lblSEZ.Visible = True
            '    lblSEZ.Text = ddlCity.SelectedItem.Text & "_" & ddlLocation.SelectedItem.Text & " :SEZ"

            'Else
            '    lblSEZ.Visible = False
            'End If


        Catch ex As Exception
        End Try
    End Sub


    Protected Sub bindData()
        'Dim strTable As String
        'Dim strTower1 As String = String.Empty
        'Dim strTower2 As String = String.Empty
        'Dim icap_ws As Int32 = 0
        'Dim icap_fc As Int32 = 0
        'Dim icap_hc As Int32 = 0
        'Dim icap_total As Int32 = 0
        'Dim iocc_ws As Int32 = 0
        'Dim iocc_fc As Int32 = 0
        'Dim iocc_hc As Int32 = 0
        'Dim iocc_total As Int32 = 0
        'Dim ivacant_ws As Int32 = 0
        'Dim ivacant_fc As Int32 = 0
        'Dim ivacant_hc As Int32 = 0
        'Dim ivacant_total As Int32 = 0

        'Dim iSno As Int32 = 0

        'Dim iGcap_ws As Int32 = 0
        'Dim iGcap_fc As Int32 = 0
        'Dim iGcap_hc As Int32 = 0
        'Dim iGcap_total As Int32 = 0
        'Dim iGocc_ws As Int32 = 0
        'Dim iGocc_fc As Int32 = 0
        'Dim iGocc_hc As Int32 = 0
        'Dim iGocc_total As Int32 = 0
        'Dim iGvacant_ws As Int32 = 0
        'Dim iGvacant_fc As Int32 = 0
        'Dim iGvacant_hc As Int32 = 0
        'Dim iGvacant_total As Int32 = 0
        'Dim iGTotal_ws As Int32 = 0
        'Dim iGTotal_fc As Int32 = 0
        'Dim iGTotal_hc As Int32 = 0
        'dim iGTotal_total as Int32=0

        'strTable = "<br /><br /> "
        'strTable = strTable & "<table border='1'><tr style='background-color:#EFC8F2'><td rowspan='2'><b>SNo</b></td><td rowspan='2'><b>Tower</b></td><td rowspan='2'><b>Floor</b></td><td rowspan='2'><b>Wing</b></td><td rowspan='2'><b>Project/Occupant</b></td><td rowspan='2'><b>Vertical</b></td><td colspan='4'><b>Total Available Seats</b></td><td colspan='4'><b>Allocated Seats</b></td><td colspan='4'><b>Occupied Seats</b></td><td colspan='4'><b>Allocated but not Occupied</b></tr>"
        'strTable = strTable & "<tr style='background-color:#EFC8F2'><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td><td><b>Total</b></td><td><b>FC</b></td><td><b>HC</b></td><td><b>WS</b></td></tr>"
        'Dim parms As SqlParameter() = {New SqlParameter("@VC_LOCATION", SqlDbType.NVarChar, 150), New SqlParameter("@VC_CHECK", SqlDbType.NVarChar, 10)}
        'If ddlLocation.SelectedItem.Text <> "--All Locations--" Then
        '    parms(0).Value = ddlLocation.SelectedValue
        '    parms(1).Value = 0
        'Else
        '    parms(0).Value = String.Empty
        '    parms(1).Value = 1
        'End If

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_TotalReport", parms)
        'For i As Integer = 0 To ds.Tables(0).Rows.Count

        '    If i <> ds.Tables(0).Rows.Count Then


        '        If (strTower1 = String.Empty) Then
        '            strTower1 = ds.Tables(0).Rows(i)("twr_name")

        '        End If
        '        strTower2 = ds.Tables(0).Rows(i)("twr_name")
        '        If (strTower1.Trim() <> strTower2.Trim()) Then

        '            iGcap_ws = iGcap_ws + icap_ws
        '            iGcap_fc = iGcap_fc + icap_fc
        '            iGcap_hc = iGcap_hc + icap_hc
        '            iGcap_total = iGcap_total + icap_total
        '            iGocc_ws = iGocc_ws + iocc_ws
        '            iGocc_fc = iGocc_fc + iocc_fc
        '            iGocc_hc = iGocc_hc + iocc_hc
        '            iGocc_total = iGocc_total + iocc_total
        '            iGvacant_ws = iGvacant_ws + ivacant_ws
        '            iGvacant_fc = iGvacant_fc + ivacant_fc
        '            iGvacant_hc = iGvacant_hc + ivacant_hc
        '            iGvacant_total = iGvacant_total + ivacant_total
        '            i = i - 1
        '            If (icap_fc = 0 And icap_hc = 0 And icap_ws = 0 And icap_total = 0 And iocc_fc = 0 And iocc_hc = 0 And iocc_ws = 0 And iocc_total = 0 And ivacant_fc = 0 And ivacant_hc = 0 And ivacant_ws = 0 And ivacant_total = 0) Then
        '            Else
        '                strTable = strTable & "<tr style='background-color:#FFFFC0' ><td colspan='6' align='center'> <b>Total</b> </td><td></td><td></td><td></td><td></td><td> " & icap_total.ToString() & "</td><td> " & icap_fc.ToString() & "</td><td> " & icap_hc.ToString() & "</td><td> " & icap_ws.ToString() & "</td><td> " & iocc_total.ToString() & "</td><td> " & iocc_fc.ToString() & "</td><td> " & iocc_hc.ToString() & "</td><td> " & iocc_ws.ToString() & "</td><td> " & ivacant_total.ToString() & "</td><td> " & ivacant_fc.ToString() & "</td><td> " & ivacant_hc.ToString() & "</td><td> " & ivacant_ws.ToString() & "</td></tr>"
        '            End If

        '            strTower1 = String.Empty
        '            icap_ws = 0
        '            icap_fc = 0
        '            icap_hc = 0
        '            icap_total = 0

        '            iocc_ws = 0
        '            iocc_fc = 0
        '            iocc_hc = 0
        '            iocc_total = 0

        '            ivacant_ws = 0
        '            ivacant_fc = 0
        '            ivacant_hc = 0
        '            ivacant_total = 0


        '        Else
        '            If (ds.Tables(0).Rows(i)("CAP_FC") = 0 And ds.Tables(0).Rows(i)("CAP_WS") = 0 And ds.Tables(0).Rows(i)("CAP_TOTAL") = 0 And ds.Tables(0).Rows(i)("OCC_FC") = 0 And ds.Tables(0).Rows(i)("OCC_HC") = 0 And ds.Tables(0).Rows(i)("OCC_WS") = 0 And ds.Tables(0).Rows(i)("OCC_TOTAL") = 0 And ds.Tables(0).Rows(i)("VACANT_FC") = 0 And ds.Tables(0).Rows(i)("VACANT_HC") = 0 And ds.Tables(0).Rows(i)("VACANT_WS") = 0 And ds.Tables(0).Rows(i)("VACANT_TOTAL") = 0) Then
        '            Else
        '                iSno = iSno + 1
        '                strTable = strTable & "<tr> <td> " & iSno & "</td><td>" & ds.Tables(0).Rows(i)("twr_name") & "</td><td>" & ds.Tables(0).Rows(i)("flr_name") & "</td><td>" & ds.Tables(0).Rows(i)("wng_name") & "</td><td>" & ds.Tables(0).Rows(i)("SSA_PROJECT") & "</td><td>" & ds.Tables(0).Rows(i)("ver_name") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_FC") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_HC") & "</td><td>" & ds.Tables(0).Rows(i)("TOTAL_WS") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_FC") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_HC") & "</td><td>" & ds.Tables(0).Rows(i)("CAP_WS") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_FC") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_HC") & "</td><td>" & ds.Tables(0).Rows(i)("OCC_WS") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_TOTAL") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_FC") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_HC") & "</td><td>" & ds.Tables(0).Rows(i)("VACANT_WS") & "</td></tr>"

        '                icap_ws = icap_ws + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_WS"))
        '                icap_fc = icap_fc + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_FC"))
        '                icap_hc = icap_hc + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_HC"))
        '                icap_total = icap_total + Convert.ToInt32(ds.Tables(0).Rows(i)("CAP_TOTAL"))
        '                iocc_ws = iocc_ws + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_WS"))
        '                iocc_fc = iocc_fc + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_FC"))
        '                iocc_hc = iocc_hc + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_HC"))
        '                iocc_total = iocc_total + Convert.ToInt32(ds.Tables(0).Rows(i)("OCC_TOTAL"))
        '                ivacant_ws = ivacant_ws + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_WS"))
        '                ivacant_fc = ivacant_fc + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_FC"))
        '                ivacant_hc = ivacant_hc + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_HC"))
        '                ivacant_total = ivacant_total + Convert.ToInt32(ds.Tables(0).Rows(i)("VACANT_TOTAL"))

        '            End If
        '        End If
        '    Else
        '        iGcap_ws = iGcap_ws + icap_ws
        '        iGcap_fc = iGcap_fc + icap_fc
        '        iGcap_hc = iGcap_hc + icap_hc
        '        iGcap_total = iGcap_total + icap_total
        '        iGocc_ws = iGocc_ws + iocc_ws
        '        iGocc_fc = iGocc_fc + iocc_fc
        '        iGocc_hc = iGocc_hc + iocc_hc
        '        iGocc_total = iGocc_total + iocc_total
        '        iGvacant_ws = iGvacant_ws + ivacant_ws
        '        iGvacant_fc = iGvacant_fc + ivacant_fc
        '        iGvacant_hc = iGvacant_hc + ivacant_hc
        '        iGvacant_total = iGvacant_total + ivacant_total
        '        If (icap_fc = 0 And icap_hc = 0 And icap_ws = 0 And icap_total = 0 And iocc_fc = 0 And iocc_hc = 0 And iocc_ws = 0 And iocc_total = 0 And ivacant_fc = 0 And ivacant_hc = 0 And ivacant_ws = 0 And ivacant_total = 0) Then
        '        Else
        '            strTable = strTable & "<tr style='background-color:#FFFFC0' ><td colspan='6' align='center'> <b>Total</b> </td><td></td><td></td><td></td><td></td><td> " & icap_total.ToString() & "</td><td> " & icap_fc.ToString() & "</td><td> " & icap_hc.ToString() & "</td><td> " & icap_ws.ToString() & "</td><td> " & iocc_total.ToString() & "</td><td> " & iocc_fc.ToString() & "</td><td> " & iocc_hc.ToString() & "</td><td> " & iocc_ws.ToString() & "</td><td> " & ivacant_total.ToString() & "</td><td> " & ivacant_fc.ToString() & "</td><td> " & ivacant_hc.ToString() & "</td><td> " & ivacant_ws.ToString() & "</td></tr>"
        '        End If


        '    End If
        'Next
        'strTable = strTable & "<tr style='background-color:#80FFBF' ><td colspan='6' align='center'> <b>Grand Total</b> </td><td></td><td></td><td></td><td></td><td> " & iGcap_total.ToString() & "</td><td> " & iGcap_fc.ToString() & "</td><td> " & iGcap_hc.ToString() & "</td><td> " & iGcap_ws.ToString() & "</td><td> " & iGocc_total.ToString() & "</td><td> " & iGocc_fc.ToString() & "</td><td> " & iGocc_hc.ToString() & "</td><td> " & iGocc_ws.ToString() & "</td><td> " & iGvacant_total.ToString() & "</td><td> " & iGvacant_fc.ToString() & "</td><td> " & iGvacant_hc.ToString() & "</td><td> " & iGvacant_ws.ToString() & "</td></tr>"

        'strTable = strTable & "</table>"
        ''Response.Write(strTable)
        'lblReport.Text = strTable
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        lblReport.Visible = True
        If lblSEZ.Visible = True Then
            ExportPanel1.FileName = "ConsolidatedSpaceReport" & ddlCity.SelectedItem.Text & "_" & ddlLocation.SelectedItem.Text
        ElseIf lblSTPI.Visible = True Then

            ExportPanel1.FileName = "ConsolidatedSpaceReport" & ddlCity.SelectedItem.Text & "_" & ddlLocation.SelectedItem.Text
        End If




    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex <= 0 Then
            lblReport.Visible = False
        ElseIf (ddlCity.SelectedIndex <= 0) Then
            lblError.Text = "Please Select Valid City"
            lblError.Visible = True
        Else
            FillGrid()
            If GV_ConsolidateRpt_STPI.Rows.Count > 0 Then
                'tb1.Visible = True
                GV_ConsolidateRpt_STPI.Visible = True
            End If
            If GV_ConsolidateRpt_SEZ.Rows.Count > 0 Then
                'tb2.Visible = True
                GV_ConsolidateRpt_SEZ.Visible = True
            End If
            lblReport.Visible = True
            lblError.Visible = False
        End If

    End Sub

    Public Sub LoadCity()
        objMaster.BindVerticalCity(ddlCity)
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex <= 0 Then
            lblReport.Visible = False
            ddlLocation.SelectedIndex = 0
            lblReport.Visible = False
        Else
            strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location where lcm_cty_id= '" & ddlCity.SelectedValue & "'"
            BindCombo(strSQL, ddlLocation, "LCM_name", "lcm_code")
            lblReport.Visible = False
            lblError.Visible = False
        End If
      
    End Sub

#Region " Consolidate  RowDataBound STPI "
    Private grdTotal_WS As Decimal = 0
    Private grdTotal_HC As Decimal = 0
    Private grdTotal_FC As Decimal = 0
    Private grdTotal_TOTAL As Decimal = 0
    Private grdCAP_WS As Decimal = 0
    Private grdCAP_FC As Decimal = 0
    Private grdCAP_HC As Decimal = 0
    Private grdCAP_TOTAL As Decimal = 0
    Private grdOCC_WS As Decimal = 0
    Private grdOCC_FC As Decimal = 0
    Private grdOCC_HC As Decimal = 0
    Private grdOCC_TOTAL As Decimal = 0
    Private grdVACANT_WS As Decimal = 0
    Private grdVACANT_FC As Decimal = 0
    Private grdVACANT_HC As Decimal = 0
    Private grdVACANT_TOTAL As Decimal = 0
    Private grdAll_Occ_TOTAL As Decimal = 0
    Private grdTotalAllOccHC As Decimal = 0
    Private grdTotalAllOccFC As Decimal = 0
    Private grdTotalAllOccTOTAL As Decimal = 0
    Protected Sub GV_ConsolidateRpt_STPI_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ConsolidateRpt_STPI.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then


            'If e.Row.RowIndex = 1 Then
            '    FreezeGridviewHeader(GV_ConsolidateRpt_STPI, tb1, PanelContainer1)
            'End If

            Try

                Dim rowTOTAL_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_WS"))
                grdTotal_WS = grdTotal_WS + rowTOTAL_WS

                Dim rowTOTAL_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_HC"))
                grdTotal_HC = grdTotal_HC + rowTOTAL_HC

                Dim rowTOTAL_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_FC"))
                grdTotal_FC = grdTotal_FC + rowTOTAL_FC

                Dim rowTOTAL_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_TOTAL"))
                grdTotal_TOTAL = grdTotal_TOTAL + rowTOTAL_TOTAL


                Dim rowCAP_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_WS"))
                grdCAP_WS = grdCAP_WS + rowCAP_WS

                Dim rowCAP_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_FC"))
                grdCAP_FC = grdCAP_FC + rowCAP_FC

                Dim rowCAP_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_HC"))
                grdCAP_HC = grdCAP_HC + rowCAP_HC

                Dim rowCAP_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_TOTAL"))
                grdCAP_TOTAL = grdCAP_TOTAL + rowCAP_TOTAL

                Dim rowOCC_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_WS"))
                grdOCC_WS = grdOCC_WS + rowOCC_WS

                Dim rowOCC_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_FC"))
                grdOCC_FC = grdOCC_FC + rowOCC_FC

                Dim rowOCC_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_HC"))
                grdOCC_HC = grdOCC_HC + rowOCC_HC

                Dim rowOCC_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_TOTAL"))
                grdOCC_TOTAL = grdOCC_TOTAL + rowOCC_TOTAL




                Dim rowVACANT_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_WS"))
                grdVACANT_WS = grdVACANT_WS + rowVACANT_WS

                Dim rowVACANT_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_FC"))
                grdVACANT_FC = grdVACANT_FC + rowVACANT_FC

                Dim rowVACANT_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_HC"))
                grdVACANT_HC = grdVACANT_HC + rowVACANT_HC

                Dim rowVACANT_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_TOTAL"))
                grdVACANT_TOTAL = grdVACANT_TOTAL + rowVACANT_TOTAL

                grdAll_Occ_TOTAL = grdAll_Occ_TOTAL + (rowCAP_WS - rowOCC_WS)
                grdTotalAllOccHC = grdTotalAllOccHC + (rowCAP_HC - rowOCC_HC)
                grdTotalAllOccFC = grdTotalAllOccFC + (rowCAP_FC - rowOCC_FC)
                grdTotalAllOccTOTAL = grdTotalAllOccTOTAL + (rowCAP_TOTAL - rowOCC_TOTAL)


                'Dim rowVACANT_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_WS"))
                'grdVACANT_WS = grdVACANT_WS + rowVACANT_WS

                'Dim rowVACANT_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_FC"))
                'grdVACANT_FC = grdVACANT_FC + rowVACANT_FC

                'Dim rowVACANT_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_HC"))
                'grdVACANT_HC = grdVACANT_HC + rowVACANT_HC

                'Dim rowVACANT_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_TOTAL"))
                'grdVACANT_TOTAL = grdVACANT_TOTAL + rowVACANT_TOTAL



            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If


        If e.Row.RowType = DataControlRowType.Footer Then
            Dim grdTotalTOTAL_TOTAL As Integer = 0
            Dim lblTotalTOTAL_WS As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_WS"), Label)
            lblTotalTOTAL_WS.Text = grdTotal_WS 'GetTotalWST_HCB_FCB("WST").ToString()
            lblTotalTOTAL_WS.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_WS.Font.Bold = True

            Dim lblTotalTOTAL_FC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_FC"), Label)
            lblTotalTOTAL_FC.Text = grdTotal_FC 'GetTotalWST_HCB_FCB("FCB").ToString()
            lblTotalTOTAL_FC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_FC.Font.Bold = True

            Dim lblTotalTOTAL_HC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_HC"), Label)
            lblTotalTOTAL_HC.Text = grdTotal_HC 'GetTotalWST_HCB_FCB("HCB").ToString()
            lblTotalTOTAL_HC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_HC.Font.Bold = True

            Dim lblTotalTOTAL_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_TOTAL"), Label)
            lblTotalTOTAL_TOTAL.Text = Integer.Parse(lblTotalTOTAL_HC.Text) + Integer.Parse(lblTotalTOTAL_FC.Text) + Integer.Parse(lblTotalTOTAL_WS.Text)
            lblTotalTOTAL_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_TOTAL.Font.Bold = True


            Dim lblTotalCAP_WS As Label = DirectCast(e.Row.FindControl("lblTotalCAP_WS"), Label)
            lblTotalCAP_WS.Text = grdCAP_WS.ToString()
            lblTotalCAP_WS.ForeColor = Drawing.Color.Black
            lblTotalCAP_WS.Font.Bold = True

            Dim lblTotalCAP_FC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_FC"), Label)
            lblTotalCAP_FC.Text = grdCAP_FC.ToString()
            lblTotalCAP_FC.ForeColor = Drawing.Color.Black
            lblTotalCAP_FC.Font.Bold = True

            Dim lblTotalCAP_HC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_HC"), Label)
            lblTotalCAP_HC.Text = grdCAP_HC.ToString()
            lblTotalCAP_HC.ForeColor = Drawing.Color.Black
            lblTotalCAP_HC.Font.Bold = True


            Dim lblTotalCAP_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalCAP_TOTAL"), Label)
            lblTotalCAP_TOTAL.Text = grdCAP_TOTAL.ToString()
            lblTotalCAP_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalCAP_TOTAL.Font.Bold = True

            Dim lblTotalOCC_WS As Label = DirectCast(e.Row.FindControl("lblTotalOCC_WS"), Label)
            lblTotalOCC_WS.Text = grdOCC_WS.ToString()
            lblTotalOCC_WS.ForeColor = Drawing.Color.Black
            lblTotalOCC_WS.Font.Bold = True


            Dim lblTotalOCC_FC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_FC"), Label)
            lblTotalOCC_FC.Text = grdOCC_FC.ToString()
            lblTotalOCC_FC.ForeColor = Drawing.Color.Black
            lblTotalOCC_FC.Font.Bold = True

            Dim lblTotalOCC_HC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_HC"), Label)
            lblTotalOCC_HC.Text = grdOCC_HC.ToString()
            lblTotalOCC_HC.ForeColor = Drawing.Color.Black
            lblTotalOCC_HC.Font.Bold = True

            Dim lblTotalOCC_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalOCC_TOTAL"), Label)
            lblTotalOCC_TOTAL.Text = grdOCC_TOTAL.ToString()
            lblTotalOCC_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalOCC_TOTAL.Font.Bold = True

            Dim lblTotalVACANT_WS As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_WS"), Label)
            lblTotalVACANT_WS.Text = grdVACANT_WS.ToString()
            lblTotalVACANT_WS.ForeColor = Drawing.Color.Black
            lblTotalVACANT_WS.Font.Bold = True

            Dim lblTotalVACANT_FC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_FC"), Label)
            lblTotalVACANT_FC.Text = grdVACANT_FC.ToString()
            lblTotalVACANT_FC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_FC.Font.Bold = True


            Dim lblTotalVACANT_HC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_HC"), Label)
            lblTotalVACANT_HC.Text = grdVACANT_HC.ToString()
            lblTotalVACANT_HC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_HC.Font.Bold = True

            Dim lblTotalVACANT_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_TOTAL"), Label)
            lblTotalVACANT_TOTAL.Text = grdVACANT_TOTAL.ToString()
            lblTotalVACANT_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalVACANT_TOTAL.Font.Bold = True



            Dim lblTotalAll_Occ_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalAll_Occ_TOTAL"), Label)
            lblTotalAll_Occ_TOTAL.Text = grdAll_Occ_TOTAL

            lblTotalAll_Occ_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalAll_Occ_TOTAL.Font.Bold = True



            Dim lblTotalAllOccHC As Label = DirectCast(e.Row.FindControl("lblTotalAllOccHC"), Label)
            lblTotalAllOccHC.Text = grdTotalAllOccHC

            lblTotalAllOccHC.ForeColor = Drawing.Color.Black
            lblTotalAllOccHC.Font.Bold = True



            Dim lblTotalAllOccFC As Label = DirectCast(e.Row.FindControl("lblTotalAllOccFC"), Label)
            lblTotalAllOccFC.Text = grdTotalAllOccFC

            lblTotalAllOccFC.ForeColor = Drawing.Color.Black
            lblTotalAllOccFC.Font.Bold = True

            'lblTotalAllOccTOTAL


            Dim lblTotalAllOccTOTAL As Label = DirectCast(e.Row.FindControl("lblTotalAllOccTOTAL"), Label)
            lblTotalAllOccTOTAL.Text = grdTotalAllOccTOTAL

            lblTotalAllOccTOTAL.ForeColor = Drawing.Color.Black
            lblTotalAllOccTOTAL.Font.Bold = True


        End If
    End Sub

#End Region



#Region " Consolidate  RowDataBound SEZ "
    Private grdTotal_WS_SEZ As Decimal = 0
    Private grdCAP_WS_SEZ As Decimal = 0
    Private grdCAP_FC_SEZ As Decimal = 0
    Private grdCAP_HC_SEZ As Decimal = 0
    Private grdCAP_TOTAL_SEZ As Decimal = 0
    Private grdOCC_WS_SEZ As Decimal = 0
    Private grdOCC_FC_SEZ As Decimal = 0
    Private grdOCC_HC_SEZ As Decimal = 0
    Private grdOCC_TOTAL_SEZ As Decimal = 0
    Private grdVACANT_WS_SEZ As Decimal = 0
    Private grdVACANT_FC_SEZ As Decimal = 0
    Private grdVACANT_HC_SEZ As Decimal = 0
    Private grdVACANT_TOTAL_SEZ As Decimal = 0
    Private grdAll_Occ_TOTAL_SEZ As Decimal = 0
    Private grdTotalAllOccHC_SEZ As Decimal = 0
    Private grdTotalAllOccFC_SEZ As Decimal = 0
    Private grdTotalAllOccTOTAL_SEZ As Decimal = 0


    Protected Sub GV_ConsolidateRpt_SEZ_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ConsolidateRpt_SEZ.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Try

                Dim rowTOTAL_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL_WS"))
                grdTotal_WS_SEZ = grdTotal_WS_SEZ + rowTOTAL_WS


                Dim rowCAP_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_WS"))
                grdCAP_WS_SEZ = grdCAP_WS_SEZ + rowCAP_WS

                Dim rowCAP_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_FC"))
                grdCAP_FC_SEZ = grdCAP_FC_SEZ + rowCAP_FC

                Dim rowCAP_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_HC"))
                grdCAP_HC_SEZ = grdCAP_HC_SEZ + rowCAP_HC

                Dim rowCAP_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CAP_TOTAL"))
                grdCAP_TOTAL_SEZ = grdCAP_TOTAL_SEZ + rowCAP_TOTAL

                Dim rowOCC_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_WS"))
                grdOCC_WS_SEZ = grdOCC_WS_SEZ + rowOCC_WS

                Dim rowOCC_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_FC"))
                grdOCC_FC_SEZ = grdOCC_FC_SEZ + rowOCC_FC

                Dim rowOCC_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_HC"))
                grdOCC_HC_SEZ = grdOCC_HC_SEZ + rowOCC_HC

                Dim rowOCC_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OCC_TOTAL"))
                grdOCC_TOTAL_SEZ = grdOCC_TOTAL_SEZ + rowOCC_TOTAL




                Dim rowVACANT_WS As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_WS"))
                grdVACANT_WS_SEZ = grdVACANT_WS_SEZ + rowVACANT_WS

                Dim rowVACANT_FC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_FC"))
                grdVACANT_FC_SEZ = grdVACANT_FC_SEZ + rowVACANT_FC

                Dim rowVACANT_HC As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_HC"))
                grdVACANT_HC_SEZ = grdVACANT_HC_SEZ + rowVACANT_HC

                Dim rowVACANT_TOTAL As Decimal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VACANT_TOTAL"))
                grdVACANT_TOTAL_SEZ = grdVACANT_TOTAL_SEZ + rowVACANT_TOTAL

                grdAll_Occ_TOTAL_SEZ = grdAll_Occ_TOTAL_SEZ + (rowCAP_WS - rowOCC_WS)
                grdTotalAllOccHC_SEZ = grdTotalAllOccHC_SEZ + (rowCAP_HC - rowOCC_HC)
                grdTotalAllOccFC_SEZ = grdTotalAllOccFC_SEZ + (rowCAP_FC - rowOCC_FC)
                grdTotalAllOccTOTAL_SEZ = grdTotalAllOccTOTAL_SEZ + (rowCAP_TOTAL - rowOCC_TOTAL)






            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If


        If e.Row.RowType = DataControlRowType.Footer Then
            Dim grdTotalTOTAL_TOTAL As Integer = 0
            Dim lblTotalTOTAL_WS As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_WS"), Label)
            lblTotalTOTAL_WS.Text = grdTotal_WS_SEZ 'GetTotalWST_HCB_FCB("WST").ToString()
            lblTotalTOTAL_WS.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_WS.Font.Bold = True

            Dim lblTotalTOTAL_FC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_FC"), Label)
            lblTotalTOTAL_FC.Text = grdCAP_FC_SEZ 'GetTotalWST_HCB_FCB("FCB").ToString()
            lblTotalTOTAL_FC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_FC.Font.Bold = True

            Dim lblTotalTOTAL_HC As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_HC"), Label)
            lblTotalTOTAL_HC.Text = grdCAP_HC_SEZ 'GetTotalWST_HCB_FCB("HCB").ToString()
            lblTotalTOTAL_HC.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_HC.Font.Bold = True

            Dim lblTotalTOTAL_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalTOTAL_TOTAL"), Label)
            lblTotalTOTAL_TOTAL.Text = Integer.Parse(lblTotalTOTAL_HC.Text) + Integer.Parse(lblTotalTOTAL_FC.Text) + Integer.Parse(lblTotalTOTAL_WS.Text)
            lblTotalTOTAL_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalTOTAL_TOTAL.Font.Bold = True


            Dim lblTotalCAP_WS As Label = DirectCast(e.Row.FindControl("lblTotalCAP_WS"), Label)
            lblTotalCAP_WS.Text = grdCAP_WS_SEZ.ToString()
            lblTotalCAP_WS.ForeColor = Drawing.Color.Black
            lblTotalCAP_WS.Font.Bold = True

            Dim lblTotalCAP_FC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_FC"), Label)
            lblTotalCAP_FC.Text = grdCAP_FC_SEZ.ToString()
            lblTotalCAP_FC.ForeColor = Drawing.Color.Black
            lblTotalCAP_FC.Font.Bold = True

            Dim lblTotalCAP_HC As Label = DirectCast(e.Row.FindControl("lblTotalCAP_HC"), Label)
            lblTotalCAP_HC.Text = grdCAP_HC_SEZ.ToString()
            lblTotalCAP_HC.ForeColor = Drawing.Color.Black
            lblTotalCAP_HC.Font.Bold = True


            Dim lblTotalCAP_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalCAP_TOTAL"), Label)
            lblTotalCAP_TOTAL.Text = grdCAP_TOTAL_SEZ.ToString()
            lblTotalCAP_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalCAP_TOTAL.Font.Bold = True

            Dim lblTotalOCC_WS As Label = DirectCast(e.Row.FindControl("lblTotalOCC_WS"), Label)
            lblTotalOCC_WS.Text = grdOCC_WS_SEZ.ToString()
            lblTotalOCC_WS.ForeColor = Drawing.Color.Black
            lblTotalOCC_WS.Font.Bold = True


            Dim lblTotalOCC_FC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_FC"), Label)
            lblTotalOCC_FC.Text = grdOCC_FC_SEZ.ToString()
            lblTotalOCC_FC.ForeColor = Drawing.Color.Black
            lblTotalOCC_FC.Font.Bold = True

            Dim lblTotalOCC_HC As Label = DirectCast(e.Row.FindControl("lblTotalOCC_HC"), Label)
            lblTotalOCC_HC.Text = grdOCC_HC_SEZ.ToString()
            lblTotalOCC_HC.ForeColor = Drawing.Color.Black
            lblTotalOCC_HC.Font.Bold = True

            Dim lblTotalOCC_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalOCC_TOTAL"), Label)
            lblTotalOCC_TOTAL.Text = grdOCC_TOTAL_SEZ.ToString()
            lblTotalOCC_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalOCC_TOTAL.Font.Bold = True

            Dim lblTotalVACANT_WS As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_WS"), Label)
            lblTotalVACANT_WS.Text = grdVACANT_WS_SEZ.ToString()
            lblTotalVACANT_WS.ForeColor = Drawing.Color.Black
            lblTotalVACANT_WS.Font.Bold = True

            Dim lblTotalVACANT_FC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_FC"), Label)
            lblTotalVACANT_FC.Text = grdVACANT_FC_SEZ.ToString()
            lblTotalVACANT_FC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_FC.Font.Bold = True


            Dim lblTotalVACANT_HC As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_HC"), Label)
            lblTotalVACANT_HC.Text = grdVACANT_HC_SEZ.ToString()
            lblTotalVACANT_HC.ForeColor = Drawing.Color.Black
            lblTotalVACANT_HC.Font.Bold = True

            Dim lblTotalVACANT_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalVACANT_TOTAL"), Label)
            lblTotalVACANT_TOTAL.Text = grdVACANT_TOTAL_SEZ.ToString()
            lblTotalVACANT_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalVACANT_TOTAL.Font.Bold = True



            Dim lblTotalAll_Occ_TOTAL As Label = DirectCast(e.Row.FindControl("lblTotalAll_Occ_TOTAL"), Label)
            lblTotalAll_Occ_TOTAL.Text = grdAll_Occ_TOTAL_SEZ

            lblTotalAll_Occ_TOTAL.ForeColor = Drawing.Color.Black
            lblTotalAll_Occ_TOTAL.Font.Bold = True



            Dim lblTotalAllOccHC As Label = DirectCast(e.Row.FindControl("lblTotalAllOccHC"), Label)
            lblTotalAllOccHC.Text = grdTotalAllOccHC_SEZ

            lblTotalAllOccHC.ForeColor = Drawing.Color.Black
            lblTotalAllOccHC.Font.Bold = True



            Dim lblTotalAllOccFC As Label = DirectCast(e.Row.FindControl("lblTotalAllOccFC"), Label)
            lblTotalAllOccFC.Text = grdTotalAllOccFC_SEZ

            lblTotalAllOccFC.ForeColor = Drawing.Color.Black
            lblTotalAllOccFC.Font.Bold = True

            'lblTotalAllOccTOTAL


            Dim lblTotalAllOccTOTAL As Label = DirectCast(e.Row.FindControl("lblTotalAllOccTOTAL"), Label)
            lblTotalAllOccTOTAL.Text = grdTotalAllOccTOTAL_SEZ

            lblTotalAllOccTOTAL.ForeColor = Drawing.Color.Black
            lblTotalAllOccTOTAL.Font.Bold = True


        End If
    End Sub

#End Region



    Public Function GetTotalWST_HCB_FCB(ByVal SPC_LAYER As String) As Integer
        Dim spCount1 As New SqlParameter("@SPC_BDG_ID", SqlDbType.VarChar, 500)
        Dim spCount2 As New SqlParameter("@LCM_CTY_ID", SqlDbType.VarChar, 500)
        Dim spCount3 As New SqlParameter("@SPC_LAYER", SqlDbType.VarChar, 500)

        spCount1.Value = ddlLocation.SelectedItem.Value.Trim()
        spCount2.Value = ddlCity.SelectedItem.Value.Trim()
        spCount3.Value = SPC_LAYER
        Dim MCount As Integer = 0
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"GetTotalWST_HCB_FCB", spCount1, spCount2, spCount3)

        If ds.Tables(0).Rows.Count > 0 Then
            MCount = ds.Tables(0).Rows(0).Item("CNT")
        End If

        Return MCount
    End Function

    'Protected Sub GV_ConsolidateRpt_STPI_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GV_ConsolidateRpt_STPI.RowCreated

    '    If e.Row.RowType = DataControlRowType.Header Then

    '        AddSortDirectionImage(GV_ConsolidateRpt_STPI, e.Row)
    '    End If
    'End Sub

    'Protected Sub FreezeGridviewHeader(ByVal _gv1 As GridView, ByVal _tb1 As Table, ByVal _pc1 As Panel)
    '    Page.EnableViewState = False

    '    _tb1.Rows.Add(_gv1.HeaderRow)
    '    _tb1.Rows(0).ControlStyle.CopyFrom(_gv1.HeaderStyle)
    '    _tb1.CellPadding = _gv1.CellPadding
    '    _tb1.CellSpacing = _gv1.CellSpacing
    '    _tb1.BorderWidth = _gv1.BorderWidth

    '    Dim Count As Integer = 0
    '    _pc1.Width = Unit.Percentage(1.5)
    '    For Count = 0 To _gv1.HeaderRow.Cells.Count - 1
    '        _tb1.Rows(0).Cells(Count).Width = _gv1.Columns(Count).ItemStyle.Width
    '        _tb1.Rows(0).Cells(Count).BorderWidth = _gv1.Columns(Count).HeaderStyle.BorderWidth
    '        _tb1.Rows(0).Cells(Count).BorderStyle = _gv1.Columns(Count).HeaderStyle.BorderStyle
    '        _pc1.Width = Unit.Pixel(Convert.ToInt32(_tb1.Rows(0).Cells(Count).Width.Value) + Convert.ToInt32(_pc1.Width.Value) + 14)
    '    Next

    'End Sub
    'Public Sub AddSortDirectionImage(ByVal gridviewID As GridView, ByVal itemRow As GridViewRow)
    '    If gridviewID.AllowSorting = False Then
    '        Return
    '    End If

    '    Dim sortImage As New Image()
    '    Dim space As New Label()
    '    If gridviewID.SortDirection = SortDirection.Ascending Then

    '        sortImage.ImageUrl = "~/images/sort_asc.gif"
    '    Else

    '        sortImage.ImageUrl = "~/images/sort_desc.gif"
    '    End If

    '    sortImage.Visible = True
    '    space.Text = " "


    '    For i As Integer = 0 To gridviewID.Columns.Count - 1
    '        Dim colExpr As String = gridviewID.Columns(i).SortExpression
    '        If colExpr <> "" AndAlso colExpr = gridviewID.SortExpression Then
    '            itemRow.Cells(i).Controls.Add(space)
    '            itemRow.Cells(i).Controls.Add(sortImage)
    '        End If
    '    Next
    'End Sub
End Class

