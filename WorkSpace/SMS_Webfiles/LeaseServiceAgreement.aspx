﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaseServiceAgreement.aspx.cs" Inherits="WorkSpace_SMS_Webfiles_LeaseServiceAgreement" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="View Or Modify Maintenance Contract" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Lease Service Agreement</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />

                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-9 control-label">Filter By Property Name/Lease Id/Landlord Name<span style="color: red;">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                        Display="none" ErrorMessage="Filter By Property Name/Lease Id/Landlord Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Button ID="btnsearch" CssClass="btn btn-default btn-primary" runat="server" Text="Search" ValidationGroup="Val1"
                                                        CausesValidation="true" TabIndex="2" OnClick="btnsearch_Click" />
                                                    <asp:Button ID="txtreset" CssClass="btn btn-default btn-primary" runat="server" Text="Reset" ValidationGroup="Val1"
                                                        CausesValidation="true" TabIndex="2" OnClick="txtreset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br />
                                <div class="form-group">
                                    <asp:GridView ID="gvItems" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                        EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="5"
                                        OnPageIndexChanging="gvItems_PageIndexChanging" OnRowCommand="gvItems_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("LEASE_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease ID">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkLeaseId" runat="server" Text='<%#Eval("LEASE_NAME")%>' CommandName="ServiceAgreement"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="City">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLocation" Text='<%#Eval("CTY_NAME")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPropType" Text='<%#Eval("PN_PROPERTYTYPE")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaseStartDate" Text='<%#Eval("EFFECTIVE_AGREEMENT_DATE")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease End Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaseEndDate" Text='<%#Eval("EXPIRY_AGREEMENT_DATE") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Monthly Rent">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLeaseRent" Text='<%#Eval("LEASE_RENT") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Security Deposit">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSD" Text='<%#Eval("LEASE_SD")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Broker Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBrkrName" Text='<%#Eval("BROKER_NAME")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Landlord Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLLDname" Text='<%#Eval("LANDLORD_NAME")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Landlord Address">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLLDAdd" Text='<%#Eval("LANDLORD_ADDRESS")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Landlord Rent">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblrentamt" Text='<%#Eval("RENT_AMOUNT")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Recovery Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblrecamt" Text='<%#Eval("RECOVERY_AMOUNT")%>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="panel1" runat="Server">

                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />

                                    <div class="row" style="padding-top: 10px">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Lease Id</label>
                                                <asp:TextBox ID="txtleaseid" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Service Type<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvsertyp" runat="server" ControlToValidate="ddlServiceType"
                                                    Display="none" ErrorMessage="Please Select Service Type" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlServiceType" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    ToolTip="--Select--">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Service Provider<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvserprvdr" runat="server" ControlToValidate="ddlServiceProvider"
                                                    Display="none" ErrorMessage="Please Select Service Provider" ValidationGroup="Val2" InitialValue="--Select--">
                                                </asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlServiceProvider" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    ToolTip="--Select--">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Vendor<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvven" runat="server" ControlToValidate="ddlVendor"
                                                    Display="none" ErrorMessage="Please Select Vendor" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    ToolTip="--Select--">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Service Frequency<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvserfreq" runat="server" ControlToValidate="ddlServiceFreq"
                                                    Display="none" ErrorMessage="Please Select Service Frequency" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlServiceFreq" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    ToolTip="--Select--">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Agreement Title<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvagree" runat="server" ControlToValidate="txtAgreeTitle"
                                                    Display="none" ErrorMessage="Please Enter Agreement Title" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtAgreeTitle" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Agreement Start Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvstart" runat="server" ControlToValidate="AgreeStartDate"
                                                    Display="none" ErrorMessage="Please Select Agreement Start Date" ValidationGroup="Val2">
                                                </asp:RequiredFieldValidator>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="AgreeStartDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Agreement End Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvend" runat="server" ControlToValidate="AgreeEndDate"
                                                    Display="none" ErrorMessage="Please Select Agreement End Date" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <div class='input-group date' id='todate'>
                                                    <asp:TextBox ID="AgreeEndDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Notice Of Termination (Days)</label>
                                                <asp:TextBox ID="txtTerm" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Cost<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCost"
                                                    Display="none" ErrorMessage="Please Enter Cost" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtCost" TextMode="Number" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" OnTextChanged="txtCost_TextChanged">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Other Cost</label>
                                                <asp:TextBox ID="txtOtherCost" TextMode="Number" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" OnTextChanged="txtOtherCost_TextChanged">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Tax</label>
                                                <asp:TextBox ID="txtTax" TextMode="Number" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" OnTextChanged="txtTax_TextChanged">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Total</label>
                                                <asp:TextBox ID="txtTotal" TextMode="Number" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Terms<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvpayterm" runat="server" ControlToValidate="ddlPaymentTerm"
                                                    Display="none" ErrorMessage="Please Select Payment Terms" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlPaymentTerm" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    ToolTip="--Select--">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="PaymentDate"
                                                    Display="none" ErrorMessage="Please Select Payment Date" ValidationGroup="Val2">
                                                </asp:RequiredFieldValidator>
                                                <div class='input-group date' id='PayDate'>
                                                    <asp:TextBox ID="PaymentDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('PayDate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Mode<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvpaytype" runat="server" ControlToValidate="ddlPaymentType"
                                                    Display="none" ErrorMessage="Please Select Payment Type" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    ToolTip="--Select--" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Terms & Conditions</label>
                                                <div onmouseover="Tip('Enter Asset Description')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="panel2" runat="Server">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account / Serial Number <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                        ErrorMessage="Enter Valid Account Number" ValidationExpression="^[a-zA-Z0-9, ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Bank Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9, ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="panel3" runat="Server" visible="false">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account / Serial Number <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAccTwo"
                                                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                        ControlToValidate="txtAccTwo" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtAccTwo" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Bank Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtBankTwo"
                                                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtBankTwo"
                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtBankTwo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Branch Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvl3brnch" runat="server" ControlToValidate="txtbrnch"
                                                        Display="None" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revl3brnch" Display="None" runat="server" ControlToValidate="txtbrnch"
                                                        ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtbrnch" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>IFSC Code <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvIFsc" runat="server" ControlToValidate="txtIFSC"
                                                        Display="None" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="REVIFsc" Display="None" runat="server" ControlToValidate="txtIFSC"
                                                        ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtIFSC" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Upload Service Agreement</label>
                                                <asp:RegularExpressionValidator ID="rfvupserv" Display="None" ControlToValidate="BrowseServAgree"
                                                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                                    ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                                                </asp:RegularExpressionValidator>
                                                <div class="btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="BrowseServAgree" runat="Server" onchange="showselectedfiles(this)" Width="90%" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Upload Invoice Receipt</label>
                                                <asp:RegularExpressionValidator ID="rfvinvoice" Display="None" ControlToValidate="BrowseInvoice"
                                                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                                    ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                                                </asp:RegularExpressionValidator>
                                                <div class="btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="BrowseInvoice" runat="Server" onchange="showselectedfiles(this)" Width="90%" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Upload Tax Documents</label>
                                                <asp:RegularExpressionValidator ID="rfvtaxdoc" Display="None" ControlToValidate="BrowseTaxDoc"
                                                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                                    ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                                                </asp:RegularExpressionValidator>
                                                <div class="btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="BrowseTaxDoc" runat="Server" onchange="showselectedfiles(this)" Width="90%" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                            <div class="form-group">
                                                <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Submit" CausesValidation="true" ValidationGroup="Val2" OnClick="btnSubmit_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

