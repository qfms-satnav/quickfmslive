Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions


Partial Class frmSpaceverticalRequisition
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty




    Public Sub filterfalse()
        Carved.Visible = False
    End Sub

    Public Sub filtertrue()
        Carved.Visible = True
    End Sub

    Public Sub cleardata()
        ddlLocation.SelectedItem.Value = -1
        ddlVertical.SelectedItem.Value = -1
        txtRemarks.Text = String.Empty
    End Sub

    Public Sub LoadCountry()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlCountry, "GET_ACTIVECOUNTRIES_BY_USRID", "CNY_NAME", "CNY_CODE", param)
    End Sub

    Public Sub LoadCity()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE", param)
        loadlocation()
    End Sub

    Public Sub BindTowers()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlTower, "GET_TOWERS_BY_USRID", "TWR_NAME", "TWR_CODE", param)
    End Sub

    Public Sub BindFloors()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlFloor, "GET_FLOORS_BY_USRID", "FLR_NAME", "FLR_CODE", param)
    End Sub

    Public Sub loadlocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)

    End Sub

    Public Sub loadfloor()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@location", SqlDbType.NVarChar, 50)
        param(0).Value = ddlLocation.SelectedItem.Value
        param(1) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(1).Value = Session("Uid").ToString().Trim()
        ddlFloor.Items.Clear()
        ObjSubsonic.Binddropdown(ddlFloor, "usp_getActiveFloors_forshifts", "flr_name", "FLR_ID", param)

    End Sub

    Public Sub loadVertical()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlVertical, "GET_ALL_VERTICALS", "Ver_Name", "Ver_Code", param)
    End Sub

    Public Sub loadshifts()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@location", SqlDbType.NVarChar, 50)
        param(0).Value = ddlLocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlshift, "usp_getActiveshiftsbylocation", "sh_name", "sh_code", param)
        If (ddlshift.Items.Count < 1) Then
            lblMsg.Text = "You have No Shifts Alloted for this Location"
        End If
    End Sub

    Public Sub loadShifttimings()

        Dim dr As SqlDataReader
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@sh_code", SqlDbType.NVarChar, 200)
        param(0).Value = ddlshift.SelectedItem.Value
        param(1) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        dr = ObjSubsonic.GetSubSonicDataReader("usp_getActiveShifttiming_BYLOC", param)
        If dr.Read() Then
            starttimehr.SelectedValue = dr("SH_FRM_HRS")
            starttimemin.SelectedValue = dr("SH_FRM_MINS")
            endtimehr.SelectedValue = dr("SH_TO_HRS")
            endtimemin.SelectedValue = dr("SH_TO_MINS")
        End If

        'Dim cmd As New SqlCommand
        'Dim cn As New SqlConnection(ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        'Dim dr As SqlDataReader
        'cmd.Connection = cn
        'cmd.CommandType = CommandType.StoredProcedure
        'cmd.CommandText = Session("TENANT") & "." & "usp_getActiveShifttiming_BYLOC"
        'cmd.Parameters.AddWithValue("@sh_code", ddlshift.SelectedItem.Value)
        'cmd.Parameters.AddWithValue("@LOC", ddlSelectLocation.SelectedItem.Value)
        'cn.Open()
        'dr = cmd.ExecuteReader()
        'If dr.Read() Then

        '    starttimehr.SelectedValue = dr("SH_FRM_HRS").ToString()
        '    starttimemin.SelectedValue = dr("SH_FRM_MINS").ToString()
        '    endtimehr.SelectedValue = dr("SH_TO_HRS").ToString()
        '    endtimemin.SelectedValue = dr("SH_TO_MINS").ToString()
        'End If
        'cn.Close()
    End Sub

    Private Sub loadgrid()

        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If

        If ddlseattype.SelectedItem.Value = "D" Then
            ftime = "00:00"
            ttime = "23:30"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = ddlLocation.SelectedValue
        param(1) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50)
        param(1).Value = ddlTower.SelectedValue
        param(2) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50)
        param(2).Value = ddlFloor.SelectedValue
        param(3) = New SqlParameter("@FDATE", SqlDbType.DateTime)
        param(3).Value = Convert.ToDateTime(txtFrmDate.Text)
        param(4) = New SqlParameter("@TDATE", SqlDbType.DateTime)
        param(4).Value = Convert.ToDateTime(txtToDate.Text)
        param(5) = New SqlParameter("@FTIME", SqlDbType.DateTime)
        param(5).Value = ftime
        param(6) = New SqlParameter("@TTIME", SqlDbType.DateTime)
        param(6).Value = ttime

        ObjSubsonic.BindGridView(gdavail, "SMS_GET_VACANT_SPACES", param)
    End Sub

    Private Sub getvacantspace()
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If

        If ddlseattype.SelectedItem.Value = "D" Then
            ftime = "00:00"
            ttime = "23:30"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlLocation.SelectedValue
        param(1) = New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlTower.SelectedValue
        param(2) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 200)
        param(2).Value = ddlFloor.SelectedValue
        param(3) = New SqlParameter("@FDATE", SqlDbType.DateTime)
        param(3).Value = txtFrmDate.Text
        param(4) = New SqlParameter("@TDATE", SqlDbType.DateTime)
        param(4).Value = txtToDate.Text
        param(5) = New SqlParameter("@FTIME", SqlDbType.DateTime)
        param(5).Value = ftime
        param(6) = New SqlParameter("@TTIME", SqlDbType.DateTime)
        param(6).Value = ttime
        ObjSubsonic.BindGridView(gvEnter, "SMS_GET_AVAILABLE_SPACES_COUNT", param)
    End Sub

    Private Sub bindalldropdowns()
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If

        If ddlseattype.SelectedItem.Value = "D" Then
            ftime = "00:00"
            ttime = "23:30"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If

        Dim param(9) As SqlParameter
        param(0) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = ddlCity.SelectedValue
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = ddlLocation.SelectedValue
        param(2) = New SqlParameter("@frmdate", SqlDbType.DateTime)
        param(2).Value = Convert.ToDateTime(txtFrmDate.Text)
        param(3) = New SqlParameter("@toDate", SqlDbType.DateTime)
        param(3).Value = Convert.ToDateTime(txtToDate.Text)
        param(4) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        param(4).Value = ftime
        param(5) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        param(5).Value = ttime
        param(6) = New SqlParameter("@bcp", SqlDbType.Int)
        param(6).Value = ""
        param(7) = New SqlParameter("@shared", SqlDbType.NVarChar, 50)
        param(7).Value = ddlseattype.SelectedItem.Value
        param(8) = New SqlParameter("@vertical", SqlDbType.NVarChar, 50)
        param(8).Value = ddlVertical.SelectedItem.Value
        'param(9) = New SqlParameter("@numberofseatsreq", SqlDbType.Int)
        'param(9).Value = txtNumberofReq.Text
        param(9) = New SqlParameter("@flr_id", SqlDbType.NVarChar, 50)
        param(9).Value = ddlFloor.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlspacetype, "GET_SPACE_LAYERS", "SPC_TYPE_NAME", "space_layer")
        ddlspacetype.Items.Insert(0, New ListItem("--Space Type--", "--Select--"))
        ddlspacesubtype.Items.Insert(0, New ListItem("--Space Sub Type--", "--Select--"))
    End Sub

    Private Sub BindFilters()
        Try
            ObjSubsonic.Binddropdown(ddlspacetype, "USP_GET_ACTIVE_SPACE_TYPES", "SPC_TYPE_NAME", "SPC_TYPE_CODE")
            ObjSubsonic.Binddropdown(ddlspacesubtype, "GET_ACTIVE_SPACE_SUB_TYPES", "SST_NAME", "SST_CODE")
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""
        lblSelVertical.Text = Session("Parent")
        txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")

        If Not Page.IsPostBack Then
            txtFrmDate.Text = getoffsetdate(Date.Today)
            txtToDate.Text = getoffsetdate(Date.Today)
            filterfalse()
            btnSubmit.Visible = False
            DisplayData.Visible = False
            btnViewInMap.Visible = False
            txtFrmDate.Text = getoffsetdate(Date.Today)
            txtToDate.Text = getoffsetdate(Date.Today)
            Dim sp(0) As SqlParameter
            sp(0) = New SqlParameter("@VC_SESSION", SqlDbType.NVarChar, 50)
            sp(0).Value = Session("Uid").ToString().Trim()
            txtFrmDate.Text = getoffsetdate(Date.Today)
            txtToDate.Text = getoffsetdate(Date.Today)

            Dim intCount As Integer = ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_LA_ROLE_COUNT", sp)

            Dim myList As New System.Collections.ArrayList()
            myList.Add("Group C1")
            myList.Add("Group C2")
            myList.Add("Group D1")
            myList.Add("Group D2")
            myList.Add("Group E")

            If intCount = 0 Then
                Dim k As Integer = myList.IndexOf(ObjSubsonic.GetSubSonicExecuteScalar("USP_SPACE_GET_GRADE", sp))
                If k = -1 Then
                    PopUpMessage("You are not Authorized to Request for Vertical", Me)
                    btnSubmit.Enabled = False
                    btnViewInMap.Visible = False
                End If
            End If
            loadVertical()
            LoadCountry()
            LoadCity()
            BindTowers()
            BindFloors()
            ObjSubsonic.Binddropdown(ddlseattype, "GETSPACETYPE_NOTBCP", "SPACE_TYPE", "SPACE_TYPE_CODE")
            RIDDS = ObjSubsonic.RIDGENARATION("VerticalReq")

            Dim hours = Enumerable.Range(0, 24).[Select](Function(i) i.ToString("D2"))
            Dim minutes = Enumerable.Range(0, 60).[Select](Function(i) i.ToString("D2"))

            starttimehr.DataSource = hours
            starttimehr.DataBind()
            starttimehr.Items.Insert(0, "Hr")

            starttimemin.DataSource = minutes
            starttimemin.DataBind()
            starttimemin.Items.Insert(0, "Min")

            endtimehr.DataSource = hours
            endtimehr.DataBind()
            endtimehr.Items.Insert(0, "Hr")
            endtimemin.DataSource = minutes
            endtimemin.DataBind()
            endtimemin.Items.Insert(0, "Min")
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        REQID = ObjSubsonic.REQGENARATION_REQ(ddlVertical.SelectedItem.Value.Trim(), "SVR_ID", Session("TENANT") & ".", "SMS_VERTICAL_REQUISITION")

        Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If starttimemin.SelectedItem.Value <> "Min" Then
            StartMM = starttimemin.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            EndHr = endtimehr.SelectedItem.Text
        End If
        If endtimemin.SelectedItem.Value <> "Min" Then
            EndMM = endtimemin.SelectedItem.Text
        End If

        If ddlseattype.SelectedItem.Value = "D" Then
            ftime = "00:00"
            ttime = "23:59"
        Else
            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
        End If

        If CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "You cant request for the past month"
            Exit Sub
        ElseIf CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtToDate.Text).Year = getoffsetdatetime(DateTime.Now).Year Then
            lblMsg.Text = "You cant request for the past month"
            Exit Sub
        ElseIf CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now).Date Then
            lblMsg.Text = "Please Select Valid From Date Cannot be less than Current Date"
            Exit Sub
        ElseIf CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
            lblMsg.Text = "Selected To Date Cannot be less than From Date"
            Exit Sub
        End If
        If CDate(txtFrmDate.Text) < getoffsetdatetime(DateTime.Now).Date Or CDate(txtToDate.Text) < CDate(txtFrmDate.Text) Then
            lblMsg.Text = "Please Select Valid  From Date Cannot be less than Current Date/Todate Cannot be less than From Date"
            Exit Sub
        End If

        Dim sta As Integer = 5

        RIDDS = RIDGENARATION("VerticalReq")
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubsonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblMsg.Text = "Request is already raised "
            Exit Sub
        Else
            For i As Integer = 0 To gdavail.Rows.Count - 1
                Dim chkEmp As CheckBox = CType(gdavail.Rows(i).FindControl("chkSelect"), CheckBox)
                Dim lblspcid As Label = CType(gdavail.Rows(i).FindControl("lblspcid"), Label)
                Dim lblSpacelyr As Label = CType(gdavail.Rows(i).FindControl("lblSpaceType"), Label)
                If chkEmp.Checked = True Then
                    If lblSpacelyr.Text = "WORK STATION" Then
                        cntWst += 1
                    End If
                    If lblSpacelyr.Text = "HALF CABIN" Then
                        cntHCB += 1
                    End If
                    If lblSpacelyr.Text = "FULL CABIN" Then
                        cntFCB += 1
                    End If
                End If
            Next
            Dim chkcnt As Integer = 0
            For i As Integer = 0 To gdavail.Rows.Count - 1
                Dim chkSelect As CheckBox = DirectCast(gdavail.Rows(i).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    chkcnt = chkcnt + 1
                    Exit For
                End If
            Next
            If chkcnt = 0 Then
                lblMsg.Text = "Please select atleast one checkbox"
                Exit Sub
            End If
            Dim param2(11) As SqlParameter
            param2(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param2(0).Value = REQID
            param2(1) = New SqlParameter("@SPCTYP", SqlDbType.NVarChar, 200)
            param2(1).Value = ddlseattype.SelectedItem.Value
            param2(2) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
            param2(2).Value = Session("uid").ToString().Trim()
            param2(3) = New SqlParameter("@CNY", SqlDbType.NVarChar, 200)
            param2(3).Value = ddlCountry.SelectedValue
            param2(4) = New SqlParameter("@CTY", SqlDbType.NVarChar, 200)
            param2(4).Value = ddlCity.SelectedValue
            param2(5) = New SqlParameter("@LCN", SqlDbType.NVarChar, 200)
            param2(5).Value = ddlLocation.SelectedValue
            param2(6) = New SqlParameter("@FRMDT", SqlDbType.NVarChar, 200)
            param2(6).Value = txtFrmDate.Text
            param2(7) = New SqlParameter("@TODT", SqlDbType.NVarChar, 200)
            param2(7).Value = txtToDate.Text
            param2(8) = New SqlParameter("@FRMTM", SqlDbType.NVarChar, 200)
            param2(8).Value = ftime
            param2(9) = New SqlParameter("@TOTM", SqlDbType.NVarChar, 200)
            param2(9).Value = ttime
            param2(10) = New SqlParameter("@REM", SqlDbType.NVarChar, 200)
            param2(10).Value = txtRemarks.Text
            param2(11) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param2(11).Value = sta
            ObjSubsonic.GetSubSonicExecute("BLOCK_SEAT_REQUEST", param2)

            Dim cnt As Int32 = 0
            For Each row As GridViewRow In gdavail.Rows
                Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
                Dim lblspcsub As Label = DirectCast(row.FindControl("lblSpaceSubType"), Label)
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then
                    verticalreqid = ObjSubsonic.REQGENARATION_REQ(ddlVertical.SelectedItem.Value.Trim(), "ssa_id", Session("TENANT") & ".", "SMS_space_allocation")

                    Dim param3(6) As SqlParameter
                    param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
                    param3(0).Value = REQID
                    param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
                    param3(1).Value = verticalreqid
                    param3(2) = New SqlParameter("SPCID", SqlDbType.NVarChar, 200)
                    param3(2).Value = lblspcid.Text
                    param3(3) = New SqlParameter("@SPCTYP", SqlDbType.NVarChar, 200)
                    param3(3).Value = ddlseattype.SelectedItem.Value
                    param3(4) = New SqlParameter("@SPCSUBTYP", SqlDbType.NVarChar, 200)
                    param3(4).Value = lblspcsub.Text
                    param3(5) = New SqlParameter("@SEATTYP", SqlDbType.NVarChar, 200)
                    param3(5).Value = ddlseattype.SelectedItem.Value
                    param3(6) = New SqlParameter("@USERID", SqlDbType.NVarChar, 200)
                    param3(6).Value = ddlseattype.SelectedItem.Value
                    param3(7) = New SqlParameter("@FLRCODE", SqlDbType.NVarChar, 200)
                    param3(7).Value = ddlFloor.SelectedItem.Value
                    param3(8) = New SqlParameter("@FLRSTAID", SqlDbType.Int)
                    param3(8).Value = sta
                    param3(9) = New SqlParameter("@FLRCRT", SqlDbType.Int)
                    param3(9).Value = Session("uid").ToString().Trim()

                    ObjSubsonic.GetSubSonicExecute("BLOCK_SEAT_REQUEST_DETAILS", param3)
                End If
            Next
        End If

        strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("6B") & "&rid=" & clsSecurity.Encrypt(REQID)
GVColor:
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect, False)
        End If
        SendMail(REQID)
    End Sub

    Private Sub SendMail(ByVal REQ_ID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@strReqID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ_ID
        ObjSubsonic.GetSubSonicExecute("SEND_MAIL_VERTCALREQUISITION", param)
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCountry.SelectedIndexChanged
        ddlCity.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@CNYID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlCountry.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlCity, "GET_CITIES_BY_CNYID", "CTY_NAME", "CTY_CODE", param)
        If ddlCity.Items.Count = 0 Then
            lblMsg.Text = "No Cities Available"
        End If
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        ddlLocation.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlCity.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
        If ddlLocation.Items.Count = 0 Then
            lblMsg.Text = "No Locations Available"
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        ddlshift.Items.Clear()
        ddlTower.Items.Clear()
        loadshifts()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@LCMID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlTower, "GET_TOWERSBYLCMID", "TWR_NAME", "TWR_CODE", param)
        If ddlTower.Items.Count = 0 Then
            lblMsg.Text = "No Towers Available"
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ddlFloor.Items.Clear()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        param(1) = New SqlParameter("@TWRID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlTower.SelectedItem.Value
        param(2) = New SqlParameter("@LOCID", SqlDbType.NVarChar, 200)
        param(2).Value = ddlLocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlFloor, "GET_FLRS_BY_TWR_LOC", "FLR_NAME", "FLR_CODE", param)
        If ddlTower.Items.Count = 0 Then
            lblMsg.Text = "No Floors Available"
        End If
    End Sub

    Protected Sub btnavail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnavail.Click

        If ddlCity.SelectedIndex > 0 Then
            If ddlLocation.SelectedIndex > 0 Then
                If txtFrmDate.Text <> "" And txtToDate.Text <> "" Then
                    getvacantspace()
                    loadgrid()
                    If gdavail.Rows.Count > 0 Then
                        Carved.Visible = True
                        gdavail.Visible = True
                        btnSubmit.Visible = True
                        btnViewInMap.Visible = True
                        DisplayData.Visible = True
                        BindFilters()
                        filtertrue()
                    Else
                        Dim ic As New Integer
                        ic = Convert.ToInt32(gdavail.Rows.Count)
                        Carved.Visible = False
                        gdavail.Visible = False
                        btnSubmit.Visible = False
                        DisplayData.Visible = False
                        lblMsg.Text = "No of seats required should be less than or equal to total no of seats in selected location - " & ic
                        lblMsg.Visible = True
                        lblMsg.Text = "No spaces found for the selection"
                    End If
                    Session("FROMDATE") = txtFrmDate.Text
                    Session("TODATE") = txtToDate.Text
                Else
                    lblMsg.Text = "Please select From Date and ToDate"
                End If
            Else
                lblMsg.Text = "Please select Preferred Location"
            End If
        Else
            lblMsg.Text = "Please select City"
        End If
    End Sub

    Protected Sub ddlseattype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlseattype.SelectedIndexChanged
        If ddlseattype.SelectedItem.Value <> "S" Then
            trshift.Visible = False
            trTimeSlot.Visible = False
            starttimehr.SelectedIndex = 0
            starttimemin.SelectedIndex = 0
            endtimehr.SelectedIndex = 0
            endtimemin.SelectedIndex = 0
        Else
            trshift.Visible = True
            trTimeSlot.Visible = True
        End If
    End Sub

    Protected Sub gdavail_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gdavail.PageIndexChanging
        gdavail.PageIndex = e.NewPageIndex
        loadgrid()
    End Sub

    'Private Sub getfiltergrid()

    '    If ddlspacetype.SelectedItem.Text <> "--Space Type--" Then
    '        Dim ftime As String = ""
    '        Dim ttime As String = ""
    '        Dim StartHr As String = "00"
    '        Dim EndHr As String = "00"
    '        Dim StartMM As String = "00"
    '        Dim EndMM As String = "00"

    '        If starttimehr.SelectedItem.Value <> "Hr" Then
    '            StartHr = starttimehr.SelectedItem.Text
    '        End If
    '        If starttimemin.SelectedItem.Value <> "Min" Then
    '            StartMM = starttimemin.SelectedItem.Text
    '        End If
    '        If endtimehr.SelectedItem.Value <> "Hr" Then
    '            EndHr = endtimehr.SelectedItem.Text
    '        End If
    '        If endtimemin.SelectedItem.Value <> "Min" Then
    '            EndMM = endtimemin.SelectedItem.Text
    '        End If
    '        If ddlseattype.SelectedItem.Value = 1 Then
    '            ftime = "00:00"
    '            ttime = "23:30"
    '        Else
    '            ftime = StartHr + ":" + StartMM
    '            ttime = EndHr + ":" + EndMM
    '        End If

    '        Dim param(10) As SqlParameter
    '        param(0) = New SqlParameter("@CITY_CODE", SqlDbType.NVarChar, 50)
    '        param(0).Value = ddlCity.SelectedValue
    '        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 50)
    '        param(1).Value = ddlSelectLocation.SelectedValue
    '        param(2) = New SqlParameter("@frmdate", SqlDbType.DateTime)
    '        param(2).Value = Convert.ToDateTime(txtFrmDate.Text)
    '        param(3) = New SqlParameter("@toDate", SqlDbType.DateTime)
    '        param(3).Value = Convert.ToDateTime(txtToDate.Text)
    '        param(4) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
    '        param(4).Value = ftime
    '        param(5) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
    '        param(5).Value = ttime
    '        param(6) = New SqlParameter("@bcp", SqlDbType.Int)
    '        param(6).Value = ""
    '        param(7) = New SqlParameter("@shared", SqlDbType.NVarChar, 50)
    '        param(7).Value = ddlseattype.SelectedItem.Value
    '        param(8) = New SqlParameter("@vertical", SqlDbType.NVarChar, 50)
    '        param(8).Value = ddlVertical.SelectedItem.Value
    '        param(9) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 200)
    '        param(9).Value = ddlspacetype.SelectedItem.Text
    '        param(10) = New SqlParameter("@flr_id", SqlDbType.NVarChar, 200)
    '        param(10).Value = ddlFloor.SelectedItem.Value
    '        ObjSubsonic.BindGridView(gdavail, "USP_GET_LOCATION_SEATS_FILTER_NEW", param)
    '        If gdavail.Rows.Count > 0 Then
    '            btnViewInMap.Visible = True
    '            btnSubmit.Visible = True
    '        Else
    '            btnViewInMap.Visible = False
    '            btnSubmit.Visible = False
    '        End If
    '    Else
    '        lblMsg.Text = "Please select the Space Type"
    '    End If
    'End Sub

    Protected Sub btncheck_Click(sender As Object, e As EventArgs) Handles btncheck.Click
        BindFilters()
    End Sub

    Protected Sub btnViewInMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInMap.Click
        Dim spcid As String = ""
        Dim count1 As Integer = 0
        For Each row As GridViewRow In gdavail.Rows
            Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                count1 = count1 + 1
                spcid = spcid & lblspcid.Text & ";"
            End If
        Next

        If count1 = 0 Then
            lblMsg.Text = "Please select any of the checkbox to view the map"
        Else
            ScriptManager.RegisterStartupScript(Page, GetType(Page), "OpenWindow", "window.open('SpacesViewMap.aspx?spcid=" & spcid.ToString() & "');", True)
        End If
    End Sub

    Protected Sub ddlshift_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlshift.SelectedIndexChanged

        If ddlshift.SelectedIndex <> 0 Then
            loadShifttimings()
        Else
            starttimehr.SelectedIndex = 0
            starttimemin.SelectedIndex = 0
            endtimehr.SelectedIndex = 0
            endtimemin.SelectedIndex = 0
        End If
        gdavail.DataSource = Nothing
        gdavail.DataBind()
    End Sub

    Protected Sub gdavail_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gdavail.Sorting
        Dim ds As DataSet = TryCast(Session("dataset"), DataSet)
        Dim dt = ds.Tables(0)
        If dt IsNot Nothing Then
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            gdavail.DataSource = dt
            gdavail.DataBind()
        End If
    End Sub

    Private Function GetSortDirection(ByVal column As String) As String
        ' By default, set the sort direction to ascending.
        Dim sortDirection = "ASC"
        ' Retrieve the last column that was sorted.
        Dim sortExpression = TryCast(ViewState("SortExpression"), String)
        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then
                    sortDirection = "DESC"
                End If
            End If
        End If
        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column
        Return sortDirection
    End Function

    Protected Sub gvEnter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvEnter.SelectedIndexChanged
        getvacantspace()
    End Sub
End Class