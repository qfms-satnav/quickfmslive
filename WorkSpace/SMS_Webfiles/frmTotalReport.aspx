<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmTotalReport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmTotalReport" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                    <td width="100%" class="tableHEADER">
                        &nbsp;<strong> Consolidated Space Report</strong></td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                    </td>
                    <td align="left">
                        &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                    </td>
                    <td>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:Button ID="Button1" runat="server" Text="Export To Excel" CssClass="clsButton"
                            Width="136px" />
                        <br />
                        <br />
                        <br />
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:Label ID="Label1" Text="Select City" runat="server"></asp:Label>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                        <br />
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:Label ID="lblText" Text="Select Location" runat="server"></asp:Label>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                        <br />
                        <table>
                            <tr>
                                <td style="width: 25px; background-color: #008080">
                                </td>
                                <td align="left">
                                    Available Seats
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25px; background-color: #FF6600">
                                </td>
                                <td align="left">
                                    Allotted Seats
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25px; background-color: #0000FF">
                                </td>
                                <td align="left">
                                    Occupied Seats
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25px; background-color: #D7E4BC">
                                </td>
                                <td align="left">
                                    Allotted not Occupied
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25px; background-color: #FDE9D9">
                                </td>
                                <td align="left">
                                    Available for Allocation
                                </td>
                            </tr>
                        </table>
                        <br />
                        <cc1:ExportPanel ID="ExportPanel1" runat="server">
                            &nbsp;<asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            &nbsp;<asp:Label ID="lblReport" runat="server"></asp:Label>
                            <h2>
                                <asp:Label ID="lblSTPI" runat="server" Visible="False"></asp:Label>
                            </h2>
                            <asp:GridView ID="GV_ConsolidateRpt_STPI" Visible="false" runat="server" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" ShowFooter="true" GridLines="BOTH">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <HeaderStyle BackColor="#CCCCCC" ForeColor="Red" />
                                <Columns>
                                    <asp:BoundField DataField="TWR_Name" HeaderText="Tower" SortExpression="TWR_Name" />
                                    <asp:BoundField DataField="FLR_Name" HeaderText="Floor" SortExpression="FLR_Name" />
                                    <asp:BoundField DataField="WNG_NAME" HeaderText="Wing" SortExpression="WNG_NAME" />
                                    <asp:BoundField DataField="VER_NAME" HeaderText="Vertical" SortExpression="VER_NAME" />
                                    <asp:BoundField DataField="SSA_PROJECT" HeaderText="Projects / Horizontal" SortExpression="SSA_PROJECT" />
                                    <asp:TemplateField HeaderText="AVL WST" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_WS" runat="server" Text='<%# Eval("TOTAL_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AVL HCB" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_HC" runat="server" Text='<%# Eval("TOTAL_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AVL FCB" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_FC" runat="server" Text='<%# Eval("TOTAL_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AVL TOTAL" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_TOTAL" runat="server" Text='<%# Eval("TOTAL_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC WST" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_WS" runat="server" Text='<%# Eval("CAP_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC HCB" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_HC" runat="server" Text='<%# Eval("CAP_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC FCB" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_FC" runat="server" Text='<%# Eval("CAP_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC TOTAL" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_TOTAL" runat="server" Text='<%# Eval("CAP_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC WST" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_WS" runat="server" Text='<%# Eval("OCC_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC HCB" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_HC" runat="server" Text='<%# Eval("OCC_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC FCB" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_FC" runat="server" Text='<%# Eval("OCC_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC TOTAL" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_TOTAL" runat="server" Text='<%# Eval("OCC_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC WST" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAll_Occ_TOTAL" runat="server"><%#Eval("CAP_WS") - Eval("OCC_WS")%></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAll_Occ_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC HCB" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <%#Eval("CAP_HC") - Eval("OCC_HC")%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAllOccHC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC FCB" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <%#Eval("CAP_FC") - Eval("OCC_FC")%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAllOccFC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC TOTAL" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <%#Eval("CAP_TOTAL") - Eval("OCC_TOTAL")%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAllOccTOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC WST" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_WS" runat="server" Text='<%# Eval("VACANT_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC HCB" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_HC" runat="server" Text='<%# Eval("VACANT_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC FCB" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_FC" runat="server" Text='<%# Eval("VACANT_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC TOTAL" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_TOTAL" runat="server" Text='<%# Eval("VACANT_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left"
                                    VerticalAlign="Top" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                            <h2>
                                <asp:Label ID="lblSEZ" runat="server" Visible="False"></asp:Label>
                            </h2>
                            <asp:GridView ID="GV_ConsolidateRpt_SEZ" Visible="false" runat="server" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" ShowFooter="true" GridLines="BOTH">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <HeaderStyle BackColor="#CCCCCC" ForeColor="Red" />
                                <Columns>
                                    <asp:BoundField DataField="TWR_Name" HeaderText="Tower" SortExpression="TWR_Name" />
                                    <asp:BoundField DataField="FLR_Name" HeaderText="Floor" SortExpression="FLR_Name" />
                                    <asp:BoundField DataField="WNG_NAME" HeaderText="Wing" SortExpression="WNG_NAME" />
                                    <asp:BoundField DataField="VER_NAME" HeaderText="Vertical" SortExpression="VER_NAME" />
                                    <asp:BoundField DataField="SSA_PROJECT" HeaderText="Projects / Horizontal" SortExpression="SSA_PROJECT" />
                                    <asp:TemplateField HeaderText="AVL WST" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_WS" runat="server" Text='<%# Eval("TOTAL_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AVL HCB" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_HC" runat="server" Text='<%# Eval("TOTAL_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AVL FCB" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_FC" runat="server" Text='<%# Eval("TOTAL_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AVL TOTAL" HeaderStyle-BackColor="#008080">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTOTAL_TOTAL" runat="server" Text='<%# Eval("TOTAL_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalTOTAL_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC WST" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_WS" runat="server" Text='<%# Eval("CAP_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC HCB" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_HC" runat="server" Text='<%# Eval("CAP_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC FCB" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_FC" runat="server" Text='<%# Eval("CAP_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALC TOTAL" HeaderStyle-BackColor="#FF6600">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCAP_TOTAL" runat="server" Text='<%# Eval("CAP_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCAP_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC WST" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_WS" runat="server" Text='<%# Eval("OCC_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC HCB" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_HC" runat="server" Text='<%# Eval("OCC_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC FCB" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_FC" runat="server" Text='<%# Eval("OCC_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OCC TOTAL" HeaderStyle-BackColor="#0000FF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOCC_TOTAL" runat="server" Text='<%# Eval("OCC_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalOCC_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC WST" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAll_Occ_TOTAL" runat="server"><%#Eval("CAP_WS") - Eval("OCC_WS")%></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAll_Occ_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC HCB" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <%#Eval("CAP_HC") - Eval("OCC_HC")%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAllOccHC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC FCB" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <%#Eval("CAP_FC") - Eval("OCC_FC")%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAllOccFC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALL OCC TOTAL" HeaderStyle-BackColor="#D7E4BC">
                                        <ItemTemplate>
                                            <%#Eval("CAP_TOTAL") - Eval("OCC_TOTAL")%>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAllOccTOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC WST" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_WS" runat="server" Text='<%# Eval("VACANT_WS").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_WS" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC HCB" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_HC" runat="server" Text='<%# Eval("VACANT_HC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_HC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC FCB" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_FC" runat="server" Text='<%# Eval("VACANT_FC").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_FC" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VAC TOTAL" HeaderStyle-BackColor="#FDE9D9">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVACANT_TOTAL" runat="server" Text='<%# Eval("VACANT_TOTAL").ToString()%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalVACANT_TOTAL" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left"
                                    VerticalAlign="Top" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                        </cc1:ExportPanel>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%">
                    </td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
