﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TEST.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_TEST" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
   
            <div style="height: 10em; width: 400; overflow: scroll; border: solid gray 1px; padding: 0.2em 0.2em;">
               
                
                <asp:GridView ID="dlspacetype" runat="server" EmptyDataText="No Records Found" AllowPaging="true"
                                AutoGenerateColumns="false" Width="100%">
                               
                             
                               <Columns>
                                    <asp:TemplateField HeaderText="Space Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblspctype" runat="Server" Text='<%#Eval("space_type")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Space Count">
                                        <ItemTemplate>
                                            <asp:Label ID="lblspacename" runat="Server" Text='<%#Eval("space_Count")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                           
                 
                              </Columns>
                            </asp:GridView>

            </div>
                        
 
    </div>
    </form>
</body>
</html>
