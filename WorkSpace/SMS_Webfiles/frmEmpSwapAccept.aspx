﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmEmpSwapAccept.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmEmpSwapAccept" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function CheckDate() {
            var dtFrom = document.getElementById("txtFromdate").Value;
            var dtTo = document.getElementById("txtTodate").Value;
            if (dtFrom < dtTo) {
                alert("Invalid Dates");
            }
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)

            var theForm = document.forms['aspnetForm'];

            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }
        function CheckDataGrid() {
            document.getElementById("lblMsg").innerText = '';
            var k = 0;
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (elm.checked == true) {
                        k = k + 1;
                    }
                }
            }
            if (k == 0) {
                document.getElementById("lblMsg").innerText = 'Please check at least one checkbox to swap employee';
                return false;
            }
        }
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Employee Swapping Acceptance</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="EmpSwapPanel2" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" ForeColor="Red" CssClass="col-md-12 control-label" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="panel1" runat="Server">
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                            EmptyDataText="No Assets Found." CssClass="table table-condensed table-bordered table-hover table-striped">

                                            <Columns>
                                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                    </HeaderTemplate>
                                                    <ItemStyle Width="1px" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpName" runat="server" Text='<%#Eval("EMP_NAME")%>'></asp:Label>
                                                        <asp:Label ID="lblEmpId" runat="server" Visible="false" Text='<%#Eval("ES_EMP_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Department">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpdept" runat="server" Text='<%#Eval("ES_EMPDEPT_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Space Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpSpcId" runat="server" Text='<%#Eval("ES_EMP_SPACEID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee To Swap">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpSwapName" runat="server" Text='<%#Eval("EMP_SWAP_NAME")%>'></asp:Label>
                                                        <asp:Label ID="lblEmpSwapId" runat="server" Visible="false" Text='<%#Eval("ES_EMPSWAP_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Department">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSwapDept" runat="server" Text='<%#Eval("ES_SWAPDEPT_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Space Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSwapSpcId" runat="server" Text='<%#Eval("ES_EMPSWAP_SPACEID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>

                                </div>
                                <br />

                                <div class="row" id="txtremarks" runat="server">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnAccept" runat="server" CssClass="btn btn-primary custom-button-color" Text="Approve" OnClientClick="return CheckDataGrid()" />
                                            <asp:Button ID="btnReject" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reject" OnClientClick="return CheckDataGrid()" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
