﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_IntraMovementAssets
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim strLCM_CODE As String
    Dim strTWR_CODE As String
    Dim strfloor As String
    Dim StrEmp As String
    Dim Strast As String
    Dim MMR_AST_CODE, MMR_FROMBDG_ID, MMR_FROMFLR_ID, LCM_CODE, LCM_NAME, MMR_MVMT_DATE, MMR_RECVD_DATE As String
    Dim TOEMP_ID As Integer
    Dim SALVAL As String
   

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                ' BindInterGrid()
                BindIntraGrid()
                BindDisposeGrid()
            End If
        End If
    End Sub
    'Private Sub BindInterGrid()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSETS_MOVEMENT_DISPOSE")
    '    Dim ds As DataSet = sp.GetDataSet()
    '    gvgriditems.datasource = ds.Tables(0)
    '    gvgriditems.databind()
    'End Sub

    Private Sub BindIntraGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSETS_MOVEMENT_DISPOSE")
        Dim ds As DataSet = sp.GetDataSet()
        gvintra.DataSource = ds.Tables(0)
        gvintra.databind()
    End Sub
    Private Sub BindDisposeGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSETS_MOVEMENT_DISPOSE")
        Dim ds As DataSet = sp.GetDataSet()
        gvdispose.DataSource = ds.Tables(1)
        gvdispose.databind()
    End Sub
    'Protected Sub gvgriditems_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvgriditems.PageIndexChanging
    '    gvgriditems.pageindex = e.NewPageIndex()
    '    BindInterGrid()
    'End Sub
    Protected Sub gvdispose_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvdispose.PageIndexChanging
        gvdispose.pageindex = e.NewPageIndex()
        BindDisposeGrid()
    End Sub
    Protected Sub gvintra_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvintra.PageIndexChanging
        gvintra.pageindex = e.NewPageIndex()
        BindIntraGrid()
    End Sub
    'Protected Sub gvgriditems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvgriditems.RowCommand
    '    Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '    Dim lblasset As Label = DirectCast(row.FindControl("lblasset"), Label)
    '    Dim code As String = lblasset.Text
    '    If e.CommandName = "Transfer" Then
    '        'AssetTransferNote(lblasset.text, 1)
    '        AssetTransferNote(lblasset.Text, 2)
    '    End If
    'End Sub
    Protected Sub gvintra_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvintra.RowCommand
        Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
        Dim lblasset As Label = DirectCast(row.FindControl("lblasset"), Label)
        Dim code As String = lblasset.Text
        If e.CommandName = "Transfer" Then
            'AssetTransferNote(lblasset.text, 2)
            AssetTransferNote(lblasset.Text, 2)
        End If
    End Sub

    Protected Sub gvdispose_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvdispose.RowCommand
        If e.CommandName = "Dispose" Then
            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            Dim lblasset As Label = DirectCast(row.FindControl("lblasset"), Label)
            Dim code As String = lblasset.Text
            AssetDisposeNote(lblasset.Text)
        End If
    End Sub
    Private Sub AssetDisposeNote(ByVal reqid As String)
        Dim strFileName As String = "Asset_Dispose_Note_" & Session("uid") & "on" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".doc"
        Response.Clear()
        Response.Charset = ""
        Response.ContentType = "application/msword"
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + strFileName)
        Response.Charset = "utf-8"
        Dim spname As String = ""
        Dim param As String = ""
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ADMINASTDISPOSE_DETAILS")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        Dim dr As SqlDataReader
        dr = sp1.GetReader()

        While dr.Read()



            MMR_AST_CODE = dr.Item("AST_CODE")
            MMR_FROMBDG_ID = dr.Item("LCM_NAME")
            MMR_FROMFLR_ID = dr.Item("TWR_NAME")
            LCM_CODE = dr.Item("FLR_NAME")
            MMR_MVMT_DATE = dr.Item("DISPOSE_DATE")
            MMR_RECVD_DATE = dr.Item("DREQ_REQUESITION_DT")
            SALVAL = dr.Item("AST_SALAVAGE_VALUE")
        End While
        Response.Write("<br>".ToString())

        Response.Write("<h3><center>Asset Disposal Note</center></h3>")
        Response.Write("<h3><center><u>( In case of multiple assets, please attaché annexure giving details ) </u></center></h3>")

        Response.Write("<p>Description of the Asset:<u><b> " & MMR_AST_CODE & "</b></u> </p>")
        Response.Write("<p>Asset Code:<u><b>" & MMR_AST_CODE & "</b></u></p>")

        Response.Write("<p>Date of Purchase : <b><u>" & MMR_MVMT_DATE & "</u></b></p>")
        Response.Write("<p>WDV  at the time of sale:<u><b>" & SALVAL & "</b></u></p>")
        Response.Write("<p>Date of Disposal : <b><u>" & MMR_MVMT_DATE & "</u></b></p>")
        Response.Write("<p>Mode Of Shifting : Shifted by: </p>")
        Response.Write("<p>Location of asset :" & MMR_FROMBDG_ID & "/" & MMR_FROMFLR_ID & "/" & LCM_CODE & "")
        Response.Write("<p>Details of  purchaser : </p>")
        Response.Write("<p> _________________________________________________________</p>")
        Response.Write("<p>Disposal Authorized by : </p>")
        Response.Write("<p> _________________________________________________________</p>")


        Response.Write("<br>".ToString())
        Response.Write("<p><u>Name, Designation & Signature of the location head handing over asset to purchaser.</u> </p>")
        Response.Write("<p> _________________________________________________________</p>")
        Response.Write("<p><b>Intimation of asset disposal received  </b></p>")
        Response.Write("<p> _________________________________________________________</p>")


        Response.Write("<br>".ToString())
        Response.Write("<p>This is to confirm that the above mentioned Asset is delivered at " & LCM_CODE & "</p>")
        Response.Write("<p>On" & MMR_RECVD_DATE & "and the relevant entries have been made in our Asset register.</p>")
        Response.Write("<p>The Asset has been installed at above location and is in good working condition.Remarks: Shifted by Admin through local transportation </p>")

        Response.Write("<p> _________________________________________________________</p>")
        Response.Write("<p>Name & signature of person receiving intimation (From accounts department)</p>")
        Response.Write("<table align=left cellpadding=0 cellspacing=0>")
        Response.Write("<tr>")
        Response.Write("<td></td>")
        Response.Write("</tr>")
        Response.Write("</table>")
        Response.End()
        Response.Flush()
    End Sub
    Private Sub AssetTransferNote(ByVal asset As String, ByVal mode As Integer)

        Dim strFileName As String = "Asset_Transfer_Note_" & Session("uid") & "on" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".doc"
        Response.Clear()
        Response.Charset = ""
        Response.ContentType = "application/msword"
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + strFileName)
        Response.Charset = "utf-8"
        Dim spname As String = ""
        Dim param As String = ""
        If mode = 1 Then
            spname = "GET_REQID_InterMOVEMENTS_byREQ_ID"
            param = "@MMR_REQ_ID"
            'param = asset
        Else
            spname = "ASSET_GET_INTRAREQUISITIONS_DTLS"
            param = "@REQ_ID"
            'param = asset
        End If
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &spname)
        sp1.Command.AddParameter(param, asset, DbType.String)
        Dim dr As SqlDataReader
        dr = sp1.GetReader()
        Response.Write("<br>".ToString())
        Response.Write("<h3><center>SATNAV SERVICES LIMITED</center></h3>")
        Response.Write("<h3><center><u>Asset Transfer Note</u></center></h3>")
        Response.Write("<p>We are pleased to offer you the following premises available on lease on the terms &amp; conditions mentioned below:</p>")
        Response.Write("---------------------------------------------------------------------------------------------------------------------------")
        While dr.Read()
            MMR_AST_CODE = dr.Item("MMR_AST_CODE")
            MMR_FROMBDG_ID = dr.Item("MMR_FROMBDG_ID")
            MMR_FROMFLR_ID = dr.Item("MMR_FROMFLR_ID")
            LCM_CODE = dr.Item("FLOC_CODE")
            LCM_NAME = dr.Item("FLOC_NAME")
            TOEMP_ID = dr.Item("TOEMP_ID")
            MMR_MVMT_DATE = dr.Item("MMR_MVMT_DATE")
            MMR_RECVD_DATE = dr.Item("MMR_RECVD_DATE")
            Response.Write("<p>Description of the Asset:<u><b> " & MMR_AST_CODE & "</b></u> </p>")
            Response.Write("<p>Asset Code:<u><b>" & MMR_AST_CODE & "</b></u></p>")
            Response.Write("<p>Date of Purchase : <b><u>" & MMR_MVMT_DATE & "</u></b></p>")
            Response.Write("<p>Asset Shifted : From:<u><b>" & MMR_FROMBDG_ID & "</b></u></p>")
            Response.Write("<p>Date of Shift : <b><u>" & MMR_MVMT_DATE & "</u></b></p>")
            Response.Write("<p>Description of the Asset:<u><b> " & MMR_AST_CODE & "</b></u> </p>")
            Response.Write("---------------------------------------------------------------------------------------------------------------------------")

        End While
        Response.Write("<p>Mode Of Shifting : Shifted by: </p>")
        Response.Write("<p>Authorized by : <u><b><<>></b></u></p>")
        Response.Write("<p>Condition while shifting :")
        Response.Write("<br>".ToString())
        Response.Write("<p><u>Name, Designation & Signature of the location head shifting the Asset.</u> </p>")
        Response.Write("<p><b>To be completed & signed by the location receiving the Asset </b></p>")
        Response.Write("<br>".ToString())
        Response.Write("<p>This is to confirm that the above mentioned Asset is delivered at " & LCM_CODE & "</p>")
        Response.Write("<p>On" & MMR_RECVD_DATE & "and the relevant entries have been made in our Asset register.</p>")
        Response.Write("<p>The Asset has been installed at above location and is in good working condition.Remarks: Shifted by Admin through local transportation </p>")
        Response.Write("<p> _________________________________________________________</p>")
        Response.Write("<p>(AVP – Administration)   (AVP – Administration)</p>")
        Response.Write("<table align=left cellpadding=0 cellspacing=0>")
        Response.Write("<tr>")
        Response.Write("<td></td>")
        Response.Write("</tr>")
        Response.Write("</table>")
        Response.End()
        Response.Flush()
    End Sub
End Class
