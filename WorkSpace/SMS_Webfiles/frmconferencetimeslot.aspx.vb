﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Text
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports Google.Apis.Calendar.v3.Data

Partial Class WorkSpace_SMS_Webfiles_frmconferencetimeslot
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty
    Dim Conf_REQ_ID As String

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        If CDate(txtFdate.Text) >= getoffsetdate(Date.Today) Then
            txtTdate.Text = LastDayOfMonth(txtFdate.Text)
            If CDate(txtFdate.Text) <= CDate(txtTdate.Text) Then
                bindtimeslots()
                lblmsg.Visible = False
                'hpViewDetails.Visible = True
                Panel1.Visible = False
                btnView.Visible = True
                btnsubmit.Visible = False
            Else
                lblmsg.Visible = True
                lblmsg.Text = "To Date should be greater than From Date"
                'hpViewDetails.Visible = False

            End If
        Else
            lblmsg.Visible = True
            lblmsg.Text = "From Date should be greater than Today's Date"
            'hpViewDetails.Visible = False

        End If
    End Sub
    Protected Sub btnClear_click(sender As Object, e As EventArgs) Handles btnClear.Click
        cleardata()
        btnsubmit.Visible = True
    End Sub

    Private Sub cleardata()

        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        'ddlConference.Items.Clear()
        ddlCapacity.Items.Clear()
        ddlConf.Items.Clear()
        txtFdate.Text = String.Empty
        txtTdate.Text = String.Empty
        ddlstarttime.ClearSelection()
        ddlendtime.ClearSelection()
        btnsubmit.Visible = False
        ' gvDaily.Visible = False
        'gvassets.Visible = False
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function

    'Get the first day of the month
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        lblmsg.Text = ""
        lblconfalert.Text = ""
        txtTdate.Attributes.Add("readonly", "readonly")
        txtFdate.Attributes.Add("readonly", "readonly")

        If Not Page.IsPostBack Then
            'hpViewDetails.Visible = False
            gvspacereport.Visible = False
            gvPanel.Visible = False
            btnView.Visible = False
            btncheck.Visible = False
            txtFdate.Text = getoffsetdate(Date.Today)
            txtTdate.Text = getoffsetdate(Date.Today)
            lblmsg.Visible = False
            'BindMail()
            txtDate.Text = getoffsetdatetime(DateTime.Now).Date
            Dim sp2 As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GET_ROLE_OF_THE_USER")
            sp2.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            Dim ds2 As New DataSet()
            ds2 = sp2.GetDataSet()
            Session("STA") = ds2.Tables(0).Rows(0).Item("ROL_ID")

            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            'sp.Command.AddParameter("@TYPE ", 1, DbType.Int32)
            'Dim ChkOnbehalfSts = sp.ExecuteScalar()

            'If ChkOnbehalfSts = 1 Then
            '    divOnbehalfSts.Visible = True
            'Else
            '    divOnbehalfSts.Visible = False
            'End If
            If rbActionsself.Checked = True Then
                txtEmpId.Text = Session("Uid")
                txtEmpId.Enabled = False
                GetUserData(txtEmpId.Text)

            Else
                txtEmpId.Text = ""
                txtEmpId.Enabled = True
                ddlSelectLocation.ClearSelection()

            End If
            'LoadCity()
            LoadLocations()
            BindConfTimes()
            Dim Status As String = ""
            Dim sp1 As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GET_ROLE_OF_THE_USER")
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            Dim ds1 As New DataSet()
            ds1 = sp1.GetDataSet()
            STATUS = ds1.Tables(0).Rows(0).Item("ROL_ID")
            If STATUS = 1 Then
                rbActionsrecurring.Visible = True
            Else
                rbActionsrecurring.Visible = False

            End If

        End If
        lblmsg.Text = ""
    End Sub
    'Private Sub BindMail()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CM_GET_ALL_MAIL_IDS")
    '    sp.Command.AddParameter("@CITY", ddlCity.SelectedValue)
    '    sp.Command.AddParameter("@LOCATION", ddlSelectLocation.SelectedValue)
    '    sp.Command.AddParameter("@TOWER", ddlTower.SelectedValue)
    '    sp.Command.AddParameter("@FLOOR", ddlFloor.SelectedValue)
    '    sp.Command.AddParameter("@COMPANY", Session("COMPANYID"))
    '    sp.Command.AddParameter("@flag", 0)
    '    lstInternal.DataSource = sp.GetDataSet()
    '    lstInternal.DataTextField = "AUR_ID"
    '    lstInternal.DataValueField = "AUR_EMAIL"
    '    lstInternal.DataBind()
    'End Sub

    Public Sub LoadCity()
        Dim param As SqlParameter() = New SqlParameter(0) {}
        param(0) = New SqlParameter("@USER_ID", SqlDbType.VarChar)
        param(0).Value = txtEmpId.Text
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE", param)
    End Sub

    Public Sub LoadLocations()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@USER_ID", SqlDbType.VarChar)
        param(1).Value = Session("uid")
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub LoadTowers()
        If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
            ' objMsater.BindTowerLoc(ddlTower, ddlSelectLocation.SelectedItem.Value)
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCity.SelectedItem.Value
            param(1) = New SqlParameter("@usr_id", SqlDbType.VarChar)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@VC_LOC", SqlDbType.NVarChar, 200)
            param(2).Value = ddlSelectLocation.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)
        End If
    End Sub

    Public Sub LoadFloors()
        If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
            Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue.ToString().Trim())
            ddlFloor.DataSource = verDTO
            ddlFloor.DataTextField = "Floorname"
            ddlFloor.DataValueField = "Floorcode"
            ddlFloor.DataBind()
            ddlFloor.Items.Insert(0, "--Select--")
        End If
    End Sub

    Public Sub LoadCapacities()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")
    End Sub
    Public Sub bindtimeslots()
        Try
            Dim Status As String = ""
            Dim FTIME As Date
            Dim TTIME As Date
            Dim sp1 As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GET_ROLE_OF_THE_USER")
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            Dim ds1 As New DataSet()
            ds1 = sp1.GetDataSet()
            Status = ds1.Tables(0).Rows(0).Item("ROL_ID")
            If Status = 1 Then
                FTIME = txtFdate.Text
                TTIME = txtTdate.Text
            Else
                'FTIME = txtFdate.Text
                FTIME = DateTime.Today()
                TTIME = FTIME.AddDays(14)

            End If

            gvspacereport.Visible = True
            gvPanel.Visible = True
            btnView.Visible = True
            lblBuilding1.Text = ddlSelectLocation.SelectedItem.Text
            lblConfName1.Text = ddlConf.SelectedItem.Text
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CONFERENCE_TIME_SLOT")
            lblHeader.Text = "Availability Status for  " + CDate(txtFdate.Text).ToString("MMMM - yyyy")
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CONF_NAME", ddlConf.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@StartDate", FTIME, DbType.Date)
            sp.Command.AddParameter("@EndDate", TTIME, DbType.Date)
            Dim ds As DataSet
            ds = sp.GetDataSet
            Dim ka As Integer
            Dim l As String = 0
            For Each column As DataColumn In ds.Tables(0).Columns
                l = l + 1
                If l = 2 Then
                    ka = column.ColumnName
                End If

            Next
            gvspacereport.DataSource = ds
            gvspacereport.DataBind()
            'hpViewDetails.Visible = True

            Dim i As Integer = 0
            Dim j As Integer = 0


            For i = 0 To gvspacereport.Rows.Count - 1
                Dim k As String = ka - 1
                For j = 1 To gvspacereport.Rows(i).Cells.Count - 1
                    k = k + 1
                    'If gvspacereport.Rows(i).Cells(j).Text = "1" Then
                    If gvspacereport.Rows(i).Cells(j).Text.Contains("1;") Then

                        If gvspacereport.Rows(i).Cells(0).Text >= getoffsetdate(Date.Today) Then
                            If CInt(k) <= getoffsetdatetime(DateTime.Now).Hour And gvspacereport.Rows(i).Cells(0).Text = getoffsetdate(Date.Today) Then
                                If (Session("TENANT") = "AXA.dbo") Then
                                    gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                                    Dim EmpName As String = gvspacereport.Rows(i).Cells(j).Text
                                    gvspacereport.Rows(i).Cells(j).Text = "<img src='../../images/chair_red.gif' />"
                                    gvspacereport.Rows(i).Cells(j).ToolTip = "Already Booked by " & Replace(EmpName, "1;", "")

                                Else
                                    gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                                    gvspacereport.Rows(i).Cells(j).Text = ""
                                End If

                            Else
                                If (Session("TENANT") = "AXA.dbo") Then
                                    Dim EmpName As String = gvspacereport.Rows(i).Cells(j).Text
                                    gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.Red
                                    gvspacereport.Rows(i).Cells(j).ToolTip = "Booked by " & Replace(EmpName, "1;", "")
                                    gvspacereport.Rows(i).Cells(j).Text = ""
                                Else
                                    'gvspacereport.Rows(i).Cells(j).Text = ""
                                    gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.Red
                                    gvspacereport.Rows(i).Cells(j).ToolTip = "Booked by " & Replace(gvspacereport.Rows(i).Cells(j).Text, "1;", "")
                                    gvspacereport.Rows(i).Cells(j).Text = ""
                                End If

                            End If
                        Else
                            gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                            gvspacereport.Rows(i).Cells(j).Text = ""
                            'gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&FROM_TIME=" & j - 1 & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" & ddlSelectLocation.SelectedItem.Value & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CONF_CODE=" & ddlConf.SelectedValue & ">A </a>"
                        End If


                    ElseIf gvspacereport.Rows(i).Cells(j).Text = "0" Then
                        If gvspacereport.Rows(i).Cells(0).Text >= getoffsetdate(Date.Today) Then
                            If CInt(k) <= getoffsetdatetime(DateTime.Now).Hour And gvspacereport.Rows(i).Cells(0).Text = getoffsetdate(Date.Today) Then
                                If (Session("TENANT") = "AXA.dbo") Then
                                    gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.DarkGray
                                    gvspacereport.Rows(i).Cells(j).Text = " <img src='../../images/chair_green.gif' />"
                                    gvspacereport.Rows(i).Cells(j).ToolTip = "Not Used " & TimeSpan.FromHours(k).ToString() & " and " & TimeSpan.FromHours(k + 1).ToString()

                                Else
                                    gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                                    gvspacereport.Rows(i).Cells(j).Text = ""
                                End If

                            Else

                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.Green
                                gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & HttpUtility.UrlEncode(gvspacereport.Rows(i).Cells(0).Text) & "&FROM_TIME=" & k & "&CTY_CODE=" & HttpUtility.UrlEncode(ddlCity.SelectedItem.Value) & "&LCM_CODE=" + HttpUtility.UrlEncode(ddlSelectLocation.SelectedItem.Value) & "&TWR_CODE=" & HttpUtility.UrlEncode(ddlTower.SelectedItem.Value) & "&FLR_CODE=" & HttpUtility.UrlEncode(ddlFloor.SelectedItem.Value) & "&CAPACITY=" & ddlCapacity.SelectedValue & "&CONF_CODE=" & ddlConf.SelectedValue & "&EMP_ID=" & txtEmpId.Text & "&DESG_ID=" & "><img src='../../images/chair_yellow.gif' /> </a>"
                                gvspacereport.Rows(i).Cells(j).ToolTip = "Click here to book the room between " & TimeSpan.FromHours(k).ToString() & " and " & TimeSpan.FromHours(k + 1).ToString()
                            End If
                        Else
                            gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                            gvspacereport.Rows(i).Cells(j).Text = ""
                            'gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&FROM_TIME=" & j - 1 & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" & ddlSelectLocation.SelectedItem.Value & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CONF_CODE=" & ddlConf.SelectedValue & ">A </a>"
                        End If

                        'gvspacereport.Rows(i).Cells(j).Text = ""
                        'ElseIf gvspacereport.Rows(i).Cells(j).Text = "5" Then
                    ElseIf gvspacereport.Rows(i).Cells(j).Text.Contains("5;") Then
                        If gvspacereport.Rows(i).Cells(0).Text >= getoffsetdate(Date.Today) Then
                            If CInt(k) <= getoffsetdatetime(DateTime.Now).Hour And gvspacereport.Rows(i).Cells(0).Text = getoffsetdate(Date.Today) Then

                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                                gvspacereport.Rows(i).Cells(j).Text = ""
                            Else
                                gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.Black
                                'gvspacereport.Rows(i).Cells(j).Text = ""
                                gvspacereport.Rows(i).Cells(j).ToolTip = "Withhold by " & Replace(gvspacereport.Rows(i).Cells(j).Text, "5;", "")
                                gvspacereport.Rows(i).Cells(j).Text = ""

                            End If
                        Else
                            gvspacereport.Rows(i).Cells(j).BackColor = Drawing.Color.LightGray
                            gvspacereport.Rows(i).Cells(j).Text = ""
                            'gvspacereport.Rows(i).Cells(j).Text = "<a  href=conference_request.aspx?FROM_DATE=" & gvspacereport.Rows(i).Cells(0).Text & "&FROM_TIME=" & j - 1 & "&CTY_CODE=" & ddlCity.SelectedItem.Value & "&LCM_CODE=" & ddlSelectLocation.SelectedItem.Value & "&TWR_CODE=" & ddlTower.SelectedItem.Value & "&FLR_CODE=" & ddlFloor.SelectedItem.Value & "&CONF_CODE=" & ddlConf.SelectedValue & ">A </a>"
                        End If
                    End If
                Next
            Next
            For i = 0 To gvspacereport.Rows.Count - 1
                gvspacereport.Rows(i).Cells(0).Text = "<a  href=conference_request.aspx?FROM_DATE=" & HttpUtility.UrlEncode(gvspacereport.Rows(i).Cells(0).Text) & "&CTY_CODE=" & HttpUtility.UrlEncode(ddlCity.SelectedItem.Value) & "&LCM_CODE=" + HttpUtility.UrlEncode(ddlSelectLocation.SelectedItem.Value) & "&TWR_CODE=" & HttpUtility.UrlEncode(ddlTower.SelectedItem.Value) & "&FLR_CODE=" & HttpUtility.UrlEncode(ddlFloor.SelectedItem.Value) & "&CAPACITY=" & ddlCapacity.SelectedValue & "&CONF_CODE=" & ddlConf.SelectedValue & "&EMP_ID=" & txtEmpId.Text & "&DESG_ID=" & ">" + gvspacereport.Rows(i).Cells(0).Text + "</a>"

            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub gvspacereport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Cells(2).Attributes.Add("onclick", String.Format("document.getElementById('{0}').innerHTML='{1}';", e.Row.Cells(1).ClientID, "H"))
    '        e.Row.Cells(3).Attributes.Add("onclick", String.Format("document.getElementById('{0}').innerHTML='{1}';", e.Row.Cells(1).ClientID, "A"))
    '        e.Row.Cells(4).Attributes.Add("onclick", String.Format("document.getElementById('{0}').innerHTML='{1}';", e.Row.Cells(1).ClientID, "P"))
    '    End If
    'End Sub

    'Protected Sub gvspacereport_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowCreated
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Cells[0].Visible = false 
    '        e.Row.Cells(0).Visible = False
    '    End If
    'End Sub

    'Protected Sub gvspacereport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowDataBound


    'End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged

        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCapacity.Items.Clear()
        ddlConf.Items.Clear()
        gvItem.Visible = False
        LoadLocations()

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        Dim ds1 As DataSet
        ds1 = sp1.GetDataSet
        ddlCapacity.DataSource = sp1.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")


        'txtFdate.Text = String.Empty
    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSelectLocation.SelectedIndexChanged
        Try
            'usp_getActiveTower_LOC
            ddlTower.Items.Clear()
            ddlFloor.Items.Clear()
            ddlCapacity.Items.Clear()
            ddlConf.Items.Clear()
            gvItem.Visible = False

            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITYBYLOCATION")
            sp3.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            Dim ds3 As DataSet
            ds3 = sp3.GetDataSet
            ddlCity.Items.Clear()
            LoadCity()
            ddlCity.Items.FindByValue(ds3.Tables(0).Rows(0).Item("LCM_CTY_ID")).Selected = True

            LoadTowers()

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
            sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
            Dim ds1 As DataSet
            ds1 = sp1.GetDataSet
            ddlCapacity.DataSource = sp1.GetDataSet
            ddlCapacity.DataTextField = "capacity"
            ddlCapacity.DataValueField = "capacity"
            ddlCapacity.DataBind()
            ddlCapacity.Items.Insert(0, "--Select--")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")




        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            ddlFloor.Items.Clear()
            ddlCapacity.Items.Clear()
            ddlConf.Items.Clear()
            gvItem.Visible = False
            LoadFloors()

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
            sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

            Dim ds1 As DataSet
            ds1 = sp1.GetDataSet
            ddlCapacity.DataSource = sp1.GetDataSet
            ddlCapacity.DataTextField = "capacity"
            ddlCapacity.DataValueField = "capacity"
            ddlCapacity.DataBind()
            ddlCapacity.Items.Insert(0, "--Select--")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")


        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs)
        'Handles ddlFloor.SelectedIndexChanged

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        Dim ds1 As DataSet
        ds1 = sp1.GetDataSet
        ddlCapacity.DataSource = sp1.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = sp.GetDataSet
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlCapacity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCapacity.SelectedIndexChanged
        'GET_SPACE_CONFERENCE




        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")

        '

        If ddlSelectLocation.SelectedItem.Value <> "--Select--" Then
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        End If
        If ddlTower.Items.Count > 0 Then
            If ddlTower.SelectedItem.Value <> "--Select--" Then
                sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
            End If
        End If
        If ddlFloor.Items.Count > 0 Then
            If ddlFloor.SelectedItem.Value <> "--Select--" Then
                sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
            End If
        End If

        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)

        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        If ddlCapacity.SelectedItem.Value <> "--Select--" Then
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
            'txtFdate.Text = String.Empty
        Else
            ddlConf.Items.Clear()
        End If

    End Sub

    Protected Sub ddlConf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlConf.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SP_GET_CONF_DETAILS")
        sp.Command.AddParameter("@CONF_CODE", ddlConf.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCapacity.Items.Clear()
        LoadLocations()
        ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_LOCATION")).Selected = True
        LoadTowers()
        ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_TOWER")).Selected = True
        LoadFloors()
        ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_FLOOR")).Selected = True
        LoadCapacities()
        ddlCapacity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFERENCE_CAPACITY")).Selected = True
    End Sub
    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        Panel1.Visible = True
        gvspacereport.Visible = False
        btnView.Visible = False
        btnsubmit.Visible = True
        gvPanel.Visible = False

    End Sub

    Protected Sub gvspacereport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvspacereport.RowDataBound
        e.Row.Cells(e.Row.Cells.Count - 1).Visible = False
    End Sub

    Protected Sub rbActionsself_CheckedChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsrecurring.CheckedChanged
        If rbActionsrecurring.Checked = True Then

            btnsubmit.Visible = False
            btncheck.Visible = True
            Tr1.Visible = True
            rectimes.Visible = True
            AttendeesMail.Visible = True
            InternalAttendees.Visible = True
            gvItem.Visible = False
            txtFdate.Text = getoffsetdate(Date.Today)
            txtTdate.Text = getoffsetdate(Date.Today)
            ddlstarttime.ClearSelection()
            ddlendtime.ClearSelection()

        Else
            Tr1.Visible = False
            rectimes.Visible = False
            AttendeesMail.Visible = False
            InternalAttendees.Visible = False
            btnsubmit.Visible = True
            btncheck.Visible = False
            gvItem.Visible = False
            ddlstarttime.ClearSelection()
            ddlendtime.ClearSelection()
        End If
    End Sub

    Private Sub BindGrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONFERENCE_BOOKED_SLOTS")
        sp.Command.AddParameter("@REQ_ID", "", DbType.String)
        sp.Command.AddParameter("@SPC_ID", ddlConf.SelectedValue, DbType.String)
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", ddlstarttime.SelectedValue, DbType.String)
        sp.Command.AddParameter("@TO_TIME", ddlendtime.SelectedValue, DbType.String)

        'sp.Command.AddParameter("@S", 1, DbType.Int16)
        'sp.Command.AddParameter("@E", 10, DbType.Int16)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()

        If (ds.Tables(0).Rows.Count > 0) Then
            gvItem.Visible = True
            lblconfalert.Visible = True
            lblconfalert.Text = "Note: selected time interval is already booked, please select other available slots."

        ElseIf (txtFdate.Text >= getoffsetdate(Date.Today).Date) And (txtTdate.Text >= txtFdate.Text) Then
            'If ddlstarttime.SelectedValue < ddlendtime.SelectedValue Or (ddlstarttime.SelectedValue > ddlendtime.SelectedValue And ddlstarttime.SelectedValue = "23") Then

            Dim cntWst As Integer = 0
            Dim cntHCB As Integer = 0
            Dim cntFCB As Integer = 0
            Dim twr As String = ""
            Dim flr As String = ""
            Dim wng As String = ""
            Dim intCount As Int16 = 0
            Dim ftime As String = ""
            Dim ttime As String = ""
            Dim StartHr As String = "00"
            Dim EndHr As String = "00"
            Dim StartMM As String = "00"
            Dim EndMM As String = "00"

            If ddlstarttime.SelectedItem.Value <> "Hr" Then
                StartHr = ddlstarttime.SelectedItem.Text
            End If

            If ddlendtime.SelectedItem.Value <> "Hr" Then
                If ddlendtime.SelectedItem.Value = "24" Then
                    EndHr = "00"
                Else
                    EndHr = ddlendtime.SelectedItem.Text
                End If

            End If

            'If StartHr = "23" And EndHr <> "00" Then
            '    lblmsg.Text = "To date Should not be more than 00 hrs"
            '    Exit Sub
            'End If

            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM

            'Dim param(7) As SqlParameter
            'param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
            'param(0).Value = ddlSelectLocation.SelectedItem.Value
            'param(1) = New SqlParameter("@TOWER", SqlDbType.NVarChar, 200)
            'param(1).Value = ddlTower.SelectedItem.Value
            'param(2) = New SqlParameter("@FLOOR", SqlDbType.NVarChar, 200)
            'param(2).Value = ddlFloor.SelectedItem.Value
            'param(3) = New SqlParameter("@FROM_TIME", SqlDbType.DateTime)
            'param(3).Value = ftime
            'param(4) = New SqlParameter("@TO_TIME", SqlDbType.DateTime)
            'param(4).Value = ttime
            'param(5) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
            'param(5).Value = Convert.ToDateTime(txtFdate.Text)
            'param(6) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
            'param(6).Value = Convert.ToDateTime(txtTdate.Text)
            'param(7) = New SqlParameter("@CONF_CODE", SqlDbType.DateTime)
            'param(7).Value = ddlConf.SelectedValue

            'ObjSubSonic.Binddropdown(ddlConference, "usp_get_CONFERENCE_Seats_PART1", "spc_name", "spc_id", param)
            'confroom.Visible = True

            'If Request.QueryString("mode") = 2 Then
            '    emp.Visible = True
            'Else
            '    emp.Visible = False
            'End If
            If CDate(txtFdate.Text).Month < getoffsetdatetime(DateTime.Now).Month And CDate(txtFdate.Text).Year < getoffsetdatetime(DateTime.Now).Year Then
                lblmsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                Exit Sub
            End If

            SubmitAllocationOccupied("Conference", txtEmpId.Text, 2)
            'SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 2)
            lblmsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
            cleardata()



            'Else
            '    lblMsg.Text = "Please enter employee code."
            'End If
            'Else
            '    lblmsg.Visible = True
            '    lblmsg.Text = "Start Time Must Be Less Than End Time"
            'End If
        Else
            lblmsg.Visible = True
            lblmsg.Text = "To Date Must Be Greater than From date"
        End If

    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
    Public Sub BindConfTimes()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtFdate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlstarttime.DataSource = ds
        ddlstarttime.DataTextField = "TIMINGS"
        ddlstarttime.DataValueField = "TIMINGS"
        ddlstarttime.DataBind()
        ddlstarttime.Items.Insert(0, "HH")
        ddlendtime.DataSource = ds
        ddlendtime.DataTextField = "TIMINGS"
        ddlendtime.DataValueField = "TIMINGS"
        ddlendtime.DataBind()
        ddlendtime.Items.Insert(0, "HH")

    End Sub
    Protected Sub txtFrmDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtFdate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlstarttime.DataSource = ds
        ddlstarttime.DataTextField = "TIMINGS"
        ddlstarttime.DataValueField = "TIMINGS"
        ddlstarttime.DataBind()
        ddlstarttime.Items.Insert(0, "HH")
        ddlendtime.DataSource = ds
        ddlendtime.DataTextField = "TIMINGS"
        ddlendtime.DataValueField = "TIMINGS"
        ddlendtime.DataBind()
        ddlendtime.Items.Insert(0, "HH")
    End Sub
    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtTdate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlendtime.DataSource = ds
        ddlendtime.DataTextField = "TIMINGS"
        ddlendtime.DataValueField = "TIMINGS"
        ddlendtime.DataBind()
        ddlendtime.Items.Insert(0, "HH")
    End Sub
    Public Sub Textinternal_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim a As String = Textinternal.Text

        If ddlCapacity.SelectedItem Is Nothing Then
            lblmsg.Visible = True
            lblmsg.Text = "select Reservation Room"
            Exit Sub
        End If

        If ddlCapacity.SelectedItem.Value <= txtInternal.Items.Count Then
            lblmsg.Visible = True
            lblmsg.Text = "Maximum capacity reached"

        Else
            Dim item As ListItem = txtInternal.Items.FindByText(a)

            If item IsNot Nothing Then
                lblmsg.Visible = True
                lblmsg.Text = "The Selected mail is already present."
            Else
                txtInternal.Items.Add(a)
            End If

        End If
        Textinternal.Text = ""
    End Sub
    Public Sub Remove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim deletedItems As New List(Of ListItem)()

        If txtInternal.SelectedItem Is Nothing Then
            'Dim message As String = "select atleast one mail"
            'Dim sb As New System.Text.StringBuilder()
            'sb.Append("<script type = 'text/javascript'>")
            'sb.Append("window.onload=function(){")
            'sb.Append("alert('")
            'sb.Append(message)
            'sb.Append("')};")
            'sb.Append("</script>")
            'ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
            lblmsg.Text = "select atleast one mail"
        Else
            For Each item As ListItem In txtInternal.Items
                If item.Selected Then
                    deletedItems.Add(item)
                End If
            Next
            For Each item As ListItem In deletedItems
                txtInternal.Items.Remove(item)
            Next
        End If

    End Sub
    Protected Sub btncheck_Click(sender As Object, e As EventArgs) Handles btncheck.Click
        gvItem.Visible = False
        Try
            Dim intFromDate As Date = CType(txtFdate.Text, Date)
        Catch ex As Exception
            lblmsg.Text = "Enter valid From Date "
            lblmsg.Visible = True
            Exit Sub
        End Try

        Try
            Dim intToDate As Date = CType(txtTdate.Text, Date)
        Catch ex As Exception
            lblmsg.Text = "Enter valid to Date "
            lblmsg.Visible = True
            Exit Sub
        End Try
        If CDate(txtTdate.Text) < CDate(txtFdate.Text) Then
            lblmsg.Text = "To date should be greater than from date "
            lblmsg.Visible = True
            Exit Sub
        ElseIf CDate(txtFdate.Text) < CDate(txtDate.Text) Then
            lblmsg.Text = " Enter valid 'From Date' that is earlier than the 'Today's date' "
            lblmsg.Visible = True
            Exit Sub
        End If

        If ddlendtime.SelectedItem.Value < ddlstarttime.SelectedItem.Value Then
            lblmsg.Visible = True
            lblmsg.Text = "To Time should be grater than from Time"
            'btnsubmit.Visible = False
            Exit Sub
        End If

        Dim InternalCount As Integer = 0
        Dim InternalAttendees As String = ""
        Dim timeDifference As TimeSpan = CDate(txtTdate.Text) - CDate(txtFdate.Text)
        Dim Status1 As String = ""
        Dim sp1 As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GET_ROLE_OF_THE_USER")
        sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
        Dim ds1 As New DataSet()
        ds1 = sp1.GetDataSet()
        Status1 = ds1.Tables(0).Rows(0).Item("ROL_ID")
        If Status1 = 0 Then
            If timeDifference.Days > 14 Then
                lblmsg.Visible = True
                lblmsg.Text = "Cannot book a meeting room  more than 15 days"
                Exit Sub
            End If
        Else
        End If
        'For Each li As ListItem In lstInternal.Items
        '    If li.Selected = True Then
        '        InternalCount = 1
        '    End If
        'Next

        'If txtAttendees.Text = "" And txtInternal.Text = "" Then
        '    lblmsg.Visible = True
        '    lblmsg.Text = "Please enter or select at least one Attendees Email"

        '    Exit Sub
        'End If

        BindGrid()

    End Sub

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)

        REQID = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "SSA_ID", Session("TENANT") & ".", "CONFERENCE_BOOKING")
        Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If ddlstarttime.SelectedItem.Value <> "Hr" Then
            StartHr = ddlstarttime.SelectedItem.Text
        End If
        'If starttimemin.SelectedItem.Value <> "Min" Then
        '    StartMM = starttimemin.SelectedItem.Text
        'End If
        If ddlendtime.SelectedItem.Value <> "Hr" Then
            If ddlendtime.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = ddlendtime.SelectedItem.Text
            End If
        End If

        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM

        Dim sta As Integer = status

        RIDDS = RIDGENARATION("VerticalReq")
        Dim selectedValues As String = String.Empty
        For i As Integer = 0 To txtInternal.Items.Count - 1
            selectedValues = selectedValues & txtInternal.Items(i).Value + Convert.ToString(",")

        Next
        If selectedValues <> "" Then
            selectedValues = selectedValues.Remove(selectedValues.Length - 1)
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubSonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblmsg.Text = "A request has already been made "
            Exit Sub
        Else
            cntWst = 1

            Dim cnt As Int32 = 0
            ' For Each row As GridViewRow In gdavail.Rows
            Dim lblspcid As String   ' DirectCast(row.FindControl("lblspcid"), Label)
            lblspcid = ddlConf.SelectedItem.Value
            'Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblmsg.Text = ""
            ' If chkSelect.Checked = True Then
            verticalreqid = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "ssa_id", Session("TENANT") & ".", "SMS_space_allocation")
            Conf_REQ_ID = verticalreqid

            Dim param3(15) As SqlParameter

            param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param3(0).Value = REQID
            param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
            param3(1).Value = verticalreqid
            param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param3(2).Value = strVerticalCode

            param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
            param3(3).Value = lblspcid
            param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
            param3(4).Value = strAurId
            param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param3(5).Value = txtFdate.Text
            param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param3(6).Value = txtTdate.Text
            param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param3(7).Value = "2" ' GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param3(8).Value = ftime
            param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param3(9).Value = ttime
            param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param3(10).Value = sta
            param3(11) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
            param3(11).Value = Session("COMPANYID")
            param3(12) = New SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000)
            param3(12).Value = selectedValues
            param3(13) = New SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000)
            param3(13).Value = Regex.Replace(Regex.Replace(txtAttendees.Text.TrimEnd(), "\t|\n|\r", ","), ",,+", ",")
            param3(14) = New SqlParameter("@DESC", SqlDbType.NVarChar, 200)
            param3(14).Value = txtDescription.Text
            param3(15) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
            param3(15).Value = Session("Uid").ToString()
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART3", param3)
            'Addusers.Occupiedspace(Trim(strAurId), LTrim(RTrim(lblspcid)))
            ' UpdateRecord(LTrim(RTrim(lblspcid)), sta, Trim(strAurId) & "/" & lblDepartment.Text)
            'End If
            'Next
            '-------------------SEND MAIL-----------------

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_CONF_BOOKING_REQUEST")
            sp.Command.AddParameter("@REQID", REQID, DbType.String)
            sp.ExecuteScalar()
        End If

        strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("2") & "&rid=" & clsSecurity.Encrypt(REQID)
GVColor:
        'Try
        '    BookEventOnGoogleCalendar()
        'Catch ex As Exception
        '    Throw ex
        'End Try

        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect, False)
        End If


    End Sub

    Private Function BookEventOnGoogleCalendar() As Boolean
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        If ddlstarttime.SelectedItem.Value <> "Hr" Then
            StartHr = ddlstarttime.SelectedItem.Text
        End If
        'If starttimemin.SelectedItem.Value <> "Min" Then
        '    StartMM = starttimemin.SelectedItem.Text
        'End If
        If ddlendtime.SelectedItem.Value <> "Hr" Then
            If ddlendtime.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = ddlendtime.SelectedItem.Text
            End If
        End If

        If EndHr = "00" And StartHr = "23" Then
            EndHr = "23:59"
        End If

        ftime = txtFdate.Text + " " + StartHr + ":" + StartMM
        ttime = txtFdate.Text + " " + EndHr + ":" + EndMM

        Dim RecCount As Integer
        Dim FromDate As DateTime = txtFdate.Text
        Dim ToDate As DateTime = txtTdate.Text
        RecCount = (ToDate - FromDate).TotalDays
        RecCount = RecCount + 1

        Dim calendarservice As New clsCalendarService
        With calendarservice
            .Summary = txtDescription.Text
            .Location = ddlSelectLocation.SelectedItem.Text + "," + ddlTower.SelectedItem.Text + "," + ddlFloor.SelectedItem.Text + "," + ddlConf.SelectedItem.Text
            .Description = txtDescription.Text
            .Start = New EventDateTime
            With calendarservice.Start
                .DateTime = ftime
                '.TimeZone = "Asia/Kolkata"
                .TimeZone = Session("useroffset")
            End With

            .End = New EventDateTime
            With .End
                .DateTime = ttime
                .TimeZone = Session("useroffset")
            End With

            Dim ndx As Integer = 0
            Dim ndx1 As Integer = 0

            Dim attendees() = txtAttendees.Text.Split(",")
            .Attendees = New EventAttendee(attendees.Length - 1) {}
            For i = 0 To attendees.Length - 1
                .Attendees(ndx) = New EventAttendee
                With .Attendees(ndx)
                    .Email = (attendees(ndx)).Trim()
                End With
                ndx = ndx + 1
            Next
            Dim intattendees() = txtInternal.Text.Split(",")
            .Attendees = New EventAttendee(intattendees.Length - 1) {}
            For j = 0 To intattendees.Length - 1
                .Attendees(ndx1) = New EventAttendee
                With .Attendees(ndx1)
                    .Email = (intattendees(ndx1)).Trim()
                End With
                ndx1 = ndx1 + 1
            Next
            'For i = 0 To lstInternal.Items.Count - 1
            '    If lstInternal.Items(i).Selected Then
            '        ReDim Preserve .Attendees(ndx + 1)
            '        .Attendees(ndx) = New EventAttendee
            '        With .Attendees(ndx)
            '            .Email = lstInternal.Items(i).Value
            '        End With
            '        ndx = ndx + 1
            '    End If
            'Next
            .Recurrence = New String() {"RRULE:FREQ=DAILY;COUNT=" + RecCount.ToString()}
            .Reminders = New Google.Apis.Calendar.v3.Data.Event.RemindersData()
            With .Reminders
                .UseDefault = False
                .Overrides = New EventReminder() {
                    New EventReminder() With {.Method = "email", .Minutes = 24 * 60},
                    New EventReminder() With {.Method = "popup", .Minutes = 10}
                }
            End With
        End With
        'Return calendarservice.PushEvent()

        Dim Event_ID As String = calendarservice.PushEvent()

        Dim InternalAttendees As String = ""
        'For Each li As ListItem In lstInternal.Items
        '    If li.Selected = True Then
        '        InternalAttendees = InternalAttendees + li.Value + ","
        '    End If
        'Next

        InternalAttendees = InternalAttendees.TrimEnd(",")

        Dim param_Event(6) As SqlParameter
        param_Event(0) = New SqlParameter("@CONF_REQ_ID", SqlDbType.NVarChar, 1000)
        param_Event(0).Value = Conf_REQ_ID
        param_Event(1) = New SqlParameter("@CONF_EVENT_ID", SqlDbType.NVarChar, 1000)
        param_Event(1).Value = Event_ID
        param_Event(2) = New SqlParameter("@CONF_EVENT_STATUS", SqlDbType.Int)
        param_Event(2).Value = 1
        param_Event(3) = New SqlParameter("@CONF_DESCRIPTION", SqlDbType.NVarChar, 1000)
        param_Event(3).Value = txtDescription.Text
        param_Event(4) = New SqlParameter("@CREATED_BY", SqlDbType.NVarChar, 100)
        param_Event(4).Value = Session("UID")
        param_Event(5) = New SqlParameter("@EXTERNAL_ATTENDEES", txtAttendees.Text)
        param_Event(6) = New SqlParameter("@INTERNAL_ATTENDEES", txtInternal.Text)
        ObjSubSonic.GetSubSonicExecute("CONF_GOOGLE_NEW_EVENT_CREATE", param_Event)

    End Function


    ''''Jagadish 09july2019

    Private Sub ClearFileds_Rbt()
        txtEmpId.Text = ""
        txtEmpId.Enabled = True
        txtName.Text = ""
        txtDepartment.Text = ""
        txtSpaceID.Text = ""
        ddlSelectLocation.ClearSelection()
        ddlTower.ClearSelection()
        ddlFloor.ClearSelection()
        ddlCity.ClearSelection()
        ddlCapacity.ClearSelection()
        ddlConf.ClearSelection()

    End Sub

    Protected Sub rbActionsself_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActionsself.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActionsself.Checked = True Then
            txtEmpId.Text = Session("Uid")

            GetUserData(txtEmpId.Text)
            txtEmpId.Enabled = False
            lblmsg.Text = ""
            ''lnkShowEscaltion.Visible = False
        Else
            txtEmpId.Text = Session("Uid")

            ''lnkShowEscaltion.Visible = False
            ClearFileds_Rbt()
        End If

    End Sub

    Protected Sub txtEmpId_TextChanged(sender As Object, e As EventArgs) Handles txtEmpId.TextChanged
        GetUserData(txtEmpId.Text)
    End Sub


    Public Function validateempid(empid As String) As String
        Dim validatecode As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_AUR_ID")
        sp.Command.AddParameter("@AUR_ID", empid, DbType.String)
        validatecode = sp.ExecuteScalar
        Return validatecode
    End Function
    Public Sub GetUserData(aur_id As String)
        Dim validatecode As String = validateempid(txtEmpId.Text)
        If validatecode = "0" Then
            lblmsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HRDGET_USER_DETAILS")
            sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtName.Text = ds.Tables(0).Rows(0).Item("NAME")
                txtDepartment.Text = ds.Tables(0).Rows(0).Item("DEPARTMENT")
                txtSpaceID.Text = ds.Tables(0).Rows(0).Item("SPACEID")
                ''txtMobile.Text = ds.Tables(0).Rows(0).Item("PHONE_NUBER")
                ViewState("DSN_CODE") = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                'ddlCity.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
                'ddlSelectLocation.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
                'ddlTower.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
                'ddlFloor.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
                ddlCity.Items.Clear()
                LoadCity()
            Else
                lblmsg.Text = "Department Code not exist"
                txtName.Text = ""
                ' ClearAll()
            End If
        Else
            lblmsg.Text = "Employee ID not found. Enter valid employee ID."
            txtName.Text = ""
            'ClearAll()
        End If
    End Sub
    <System.Web.Script.Services.ScriptMethod(),
   System.Web.Services.WebMethod()>
    Public Shared Function SearchCustomers(ByVal prefixText As String) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
         .ConnectionStrings("CSAmantraFAM").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandType = Data.CommandType.StoredProcedure
        cmd.CommandText = HttpContext.Current.Session("TENANT") & "." & "AUTOCOMPLETE_SEARCH"
        'cmd.CommandText = "getsearchEmpName"
        cmd.Parameters.AddWithValue("@TERM", prefixText)
        cmd.Connection = conn
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("AUR_ID").ToString)
        End While

        conn.Close()
        Return customers
    End Function

    <System.Web.Script.Services.ScriptMethod(),
   System.Web.Services.WebMethod()>
    Public Shared Function GetUserDataClient(ByVal aur_id As String) As Object

        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "HRDGET_USER_DETAILS")
        sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            Return New With {.Name = ds.Tables(0).Rows(0).Item("NAME"), .DEPARTMENT = ds.Tables(0).Rows(0).Item("DEPARTMENT"), .SPACEID = ds.Tables(0).Rows(0).Item("SPACEID"), .PHONE_NUBER = ds.Tables(0).Rows(0).Item("PHONE_NUBER")}
        End If
        Return Nothing
    End Function

End Class
