﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Globalization
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.Data.OleDb
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions
Imports System.IO
Partial Class WorkSpace_SMS_Webfiles_frmEmpSwapAccept
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            BindGrid()

        End If

    End Sub
    
    Private Sub BindGrid()
        Try
            Dim param(0) As SqlParameter

            param(0) = New SqlParameter("@UID", SqlDbType.NVarChar, 200)
            param(0).Value = Session("uid")

            ObjSubSonic.BindGridView(gvItems, "SMS_EMPLOYEESWAP_ACCEPTANCE", param)
            If gvItems.Rows.Count = 0 Then
                btnAccept.Visible = False

            Else
                btnAccept.Visible = True

            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand

        Try
            For Each row As GridViewRow In gvItems.Rows
                Dim Emp_1 As String
                Dim emp1 As Array
                Dim Emp_2 As String
                Dim emp2 As Array
                Dim chkbox As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)

                If chkbox.Checked = True Then
                    Dim lbldata As Label = DirectCast(row.FindControl("lblEmpId"), Label)
                    emp1 = lbldata.Text.Split("/")
                    Emp_1 = Trim(emp1(0))

                    Dim lblswapdata As Label = DirectCast(row.FindControl("lblEmpSwapId"), Label)
                    emp2 = lblswapdata.Text.Split("/")
                    Emp_2 = Trim(emp2(0))

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SMS_SWAP_ACKNOWLEDGEMENT")
                    sp.Command.AddParameter("OEMPID", Emp_1, DbType.String)
                    sp.Command.AddParameter("@NEMPID", Emp_2, DbType.String)
                    sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
                    Dim flag As Integer = sp.ExecuteScalar()
                    If flag = 1 Then
                        lblMsg.Text = "Error Occcured while updating, Please Try Again."
                        Exit For
                    Else
                        lbldata.Text = lblswapdata.Text
                        lblswapdata.Text = Emp_1
                        lblMsg.Text = "Swapping Done Successfully"
                    End If
                End If
            Next

            BindGrid()
            panel1.Visible = True
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub


    Protected Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        Try
            For Each row As GridViewRow In gvItems.Rows
                Dim Emp_1 As String
                Dim emp1 As Array
                Dim Emp_2 As String
                Dim emp2 As Array
                Dim chkbox As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)

                If chkbox.Checked = True Then
                    Dim lbldata As Label = DirectCast(row.FindControl("lblEmpId"), Label)
                    emp1 = lbldata.Text.Split("/")
                    Emp_1 = Trim(emp1(0))

                    Dim lblswapdata As Label = DirectCast(row.FindControl("lblEmpSwapId"), Label)
                    emp2 = lblswapdata.Text.Split("/")
                    Emp_2 = Trim(emp2(0))

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SMS_SWAP_ACKNOWLEDGEMENT")
                    sp.Command.AddParameter("OEMPID", Emp_1, DbType.String)
                    sp.Command.AddParameter("@NEMPID", Emp_2, DbType.String)
                    sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
                        Dim flag As Integer = sp.ExecuteScalar()
                        If flag = 1 Then
                            lblMsg.Text = "Error Occcured while updating, Please Try Again."
                            Exit For
                        Else
                        lbldata.Text = lblswapdata.Text
                        lblswapdata.Text = Emp_1
                        lblMsg.Text = "Swapping Done Successfully"
                    End If
                End If
            Next
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            For Each row As GridViewRow In gvItems.Rows
                Dim Emp_1 As String
                Dim emp1 As Array
                Dim Emp_2 As String
                Dim emp2 As Array
                Dim chkbox As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)

                If chkbox.Checked = True Then
                    Dim lbldata As Label = DirectCast(row.FindControl("lblEmpId"), Label)
                    emp1 = lbldata.Text.Split("/")
                    Emp_1 = Trim(emp1(0))

                    Dim lblswapdata As Label = DirectCast(row.FindControl("lblEmpSwapId"), Label)
                    emp2 = lblswapdata.Text.Split("/")
                    Emp_2 = Trim(emp2(0))

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SMS_SWAP_ACKNOWLEDGEMENT_REJECT")
                    sp.Command.AddParameter("OEMPID", Emp_1, DbType.String)
                    sp.Command.AddParameter("@NEMPID", Emp_2, DbType.String)
                    sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
                    Dim flag As Integer = sp.ExecuteScalar()
                    If flag = 1 Then
                        lblMsg.Text = "Error Occcured while updating, Please Try Again."
                        Exit For
                    Else
                        lbldata.Text = lblswapdata.Text
                        lblswapdata.Text = Emp_1
                        lblMsg.Text = "Rejected Successfully"
                    End If
                End If
            Next
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "", "Load", ex)
        End Try
    End Sub
End Class
