<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmLocationAvailable.aspx.vb" Inherits="Masters_Mas_Webfiles_frmLocationAvailable"
    Title="Locations Available" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Available Locations  Master
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
     
            <table id="table3" cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 36px">
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Available Locations Master</strong>&nbsp;</td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                    </td>
                    <td align="left">
                      <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                       
                       <asp:HyperLink id="hyp1" runat="Server" Text="Delete Available and Occupied Locations" NavigateUrl="~/Masters/Mas_Webfiles/frmLocationAvailableEdit.aspx"></asp:HyperLink> 
                        <table id="tab" runat="Server" cellpadding="2" cellspacing="0" width="100%" border="1">
                       
                          <tr>
                          <td align="left" style="height:26px;width:25%">
                          Select City<font class="clsNote">*</font>
                          <asp:RequiredFieldValidator id="rfvcity" runat="server" controltoValidate="ddlcity" initialvalues="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select City">
                          </asp:RequiredFieldValidator>
                          </td>
                          <td align="left" style="height:26px;width:25%">
                          <asp:DropDownList id="ddlcity" runat="server" cssclass="clsComboBox" width="97%" AutoPostBack="True"></asp:DropDownList>
                          </td>
                                <td align="left" style="height: 26px; width: 25%" >
                                    Select Location <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqloc" runat="Server" controltovalidate="ddllocation"
                                        InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Location">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%" >
                                    <asp:DropDownList id="ddllocation" runat="server" cssclass="clsComboBox" width="97%" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Available WT<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqavailable_wt" runat="Server" controltovalidate="txtavailable_wt"
                                        ErrorMessage="Please Enter Available_WT" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revavailable_wt" runat="server" ControlToValidate="txtavailable_wt"
                                                ErrorMessage="Please enter valid Available_WT" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtavailable_wt" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                           
                                <td align="left" style="height: 26px; width: 25%">
                                    Occupied WT<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqOccupied_wt" runat="Server" controltovalidate="txtOccupied_wt"
                                        ErrorMessage="Please Enter Occupied_WT" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revOccupied_wt" runat="server" ControlToValidate="txtOccupied_wt"
                                                ErrorMessage="Please enter valid Occupied_WT" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtOccupied_wt" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Available WI<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqavailable_wi" runat="Server" controltovalidate="txtavailable_wi"
                                        ErrorMessage="Please Enter Available_WI" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revavailable_wi" runat="server" ControlToValidate="txtavailable_wi"
                                                ErrorMessage="Please enter valid Available_WI" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtavailable_wi" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                          
                                <td align="left" style="height: 26px; width: 25%">
                                    Occupied WI<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqOccupied_wi" runat="Server" controltovalidate="txtOccupied_wi"
                                        ErrorMessage="Please Enter Occupied_WI" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revOccupied_wi" runat="server" ControlToValidate="txtOccupied_wi"
                                                ErrorMessage="Please enter valid Occupied_WI" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtOccupied_wi" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Available WBPO<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqavailable_wb" runat="Server" controltovalidate="txtavailable_wb"
                                        ErrorMessage="Please Enter Available_WBPO" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revavailable_wb" runat="server" ControlToValidate="txtavailable_wb"
                                                ErrorMessage="Please enter valid Available_WBPO" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtavailable_wb" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                          
                                <td align="left" style="height: 26px; width: 25%">
                                    Occupied WBPO<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqOccupied_wb" runat="Server" controltovalidate="txtOccupied_wb"
                                        ErrorMessage="Please Enter Occupied_WBPO" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revOccupied_wb" runat="server" ControlToValidate="txtOccupied_wb"
                                                ErrorMessage="Please enter valid Occupied_WBPO" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtOccupied_wb" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Available WSTL<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqavailable_wstl" runat="Server" controltovalidate="txtavailable_wstl"
                                        ErrorMessage="Please Enter Available_WSTL" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revavailable_wstl" runat="server" ControlToValidate="txtavailable_wstl"
                                                ErrorMessage="Please enter valid Available_WSTL" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtavailable_wstl" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                           
                                <td align="left" style="height: 26px; width: 25%">
                                    Occupied WSTL<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator id="reqOccupied_wstl" runat="Server" controltovalidate="txtOccupied_wstl"
                                        ErrorMessage="Please Enter Occupied_WSTL" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revOccupied_wstl" runat="server" ControlToValidate="txtOccupied_wstl"
                                                ErrorMessage="Please enter valid Occupied_WSTL" Display="None" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox id="txtOccupied_wstl" runat="Server" cssclass="clsTextField" width="97%" maxlength="15">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td align="center" colspan="4">
                            <asp:Button id="btnsubmit" runat="Server" cssclass="button" Text="Submit" OnClick="btnsubmit_Click" ValidationGroup="Val1" />
                            </td>
                            </tr>
                        </table>
                        <asp:Panel id="pnlgrid" runat="Server" width="100%" VISIBLE="FALSE">
                         <asp:GridView id="gvitems" runat="server" Allowpaging="True" AllowSorting="True" AutoGenerateColumns="False" Width="100%">
                <columns>
                <asp:templatefield visible="false">
                <itemtemplate>
                <asp:label id="lblloc" runat="server" Text=<%#Eval("LOCATION_CODE") %>></asp:label> />
                </itemtemplate>
                </asp:templatefield>
                <asp:BoundField DataField="LOCATION_NAME" HeaderText="LOCATION" />
                <asp:BoundField DataField="AVAILABLE_WT" HeaderText="Available WT" />
                <asp:BoundField DataField="OCCUPIED_WT" HeaderText="Occupied WT" />
                <asp:BoundField DataField="AVAILABLE_WI" HeaderText="Available WI" />
                <asp:BoundField DataField="OCCUPIED_WI" HeaderText="Occupied WI" />
                <asp:BoundField DataField="AVAILABLE_WBPO" HeaderText="Available WBPO" />
                <asp:BoundField DataField="OCCUPIED_WBPO" HeaderText="Occupied WBPO" />
                <asp:BoundField DataField="AVAILABLE_WSTL" HeaderText="Available WSTL" />
                <asp:BoundField DataField="OCCUPIED_WSTL" HeaderText="Occupied WSTL" />
                 <asp:ButtonField Text="EDIT" CommandName="EDIT" />
                 <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                </columns>
                </asp:GridView>
                        </asp:Panel>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px; width: 861px;" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
    </div>
</asp:Content>
