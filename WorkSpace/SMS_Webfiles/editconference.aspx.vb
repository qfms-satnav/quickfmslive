﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports System.Threading
Partial Class WorkSpace_SMS_Webfiles_editconference
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Binddailygrid()
            BindLocations()
            BindReservationRooms()
        End If
    End Sub
    Private Sub Binddailygrid()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ""
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("Uid")
        param(2) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(2).Value = Session("COMPANYID")
        objsubsonic.BindGridView(gvDaily, "VIEW_CONFERENCE", param)
    End Sub

    Public Sub BindLocations()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        'objsubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
        objsubsonic.Binddropdown(ddlLocaton1, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Public Sub BindReservationRooms()

        'Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        'param(0).Value = Session("Uid").ToString().Trim()
        'objsubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
        objsubsonic.Binddropdown(ddlReser, "GET_RESERVATIONROOMS", "CONF_NAME", "CONF_CODE")
    End Sub
    Protected Sub ddlLocaton1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocaton1.SelectedIndexChanged
        Try
            'usp_getActiveTower_LOC





            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")

            sp.Command.AddParameter("@LCM_CODE", ddlLocaton1.SelectedItem.Value, DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlReser.DataSource = sp.GetDataSet
            ddlReser.DataTextField = "CONF_NAME"
            ddlReser.DataValueField = "CONF_CODE"
            ddlReser.DataBind()
            ddlReser.Items.Insert(0, "--Select--")



        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub
    Private Sub Bindsearchgrid()
        Dim DS As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_CONFERENCE_REQUESTS")
        sp.Command.AddParameter("@REQ_ID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@LOCATION", ddlLocaton1.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@CONFROOM", ddlReser.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String)
        'sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        'sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
        DS = sp.GetDataSet
        gvDaily.DataSource = DS
        gvDaily.DataBind()

    End Sub
    Protected Sub btnsearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click

        Bindsearchgrid()
        'Dim ds1 As New DataSet
        'Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        'spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        'ds1 = spSts.GetDataSet()
        'For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
        '    Dim ddlstat As DropDownList = CType(gvViewRequisitions.Rows(i).FindControl("ddlModifyStatus"), DropDownList)
        '    ddlstat.DataSource = ds1
        '    ddlstat.DataTextField = "STA_TITLE"
        '    ddlstat.DataValueField = "STA_ID"
        '    ddlstat.DataBind()
        '    ddlstat.ClearSelection()
        'Next
    End Sub

    Protected Sub gvDaily_PageIndexChanging(sender As Object, e As Web.UI.WebControls.GridViewPageEventArgs) Handles gvDaily.PageIndexChanging
        gvDaily.PageIndex = e.NewPageIndex()

        If (txtReqId.Text = "" And ddlLocaton1.SelectedValue = "--Select--" And ddlReser.SelectedValue = "--Select--" And txtfromDt.Text = "" And txtToDt.Text = "") Then
            Binddailygrid()
        Else
            Bindsearchgrid()

        End If


    End Sub


    Protected Sub gvDaily_RowCommand(sender As Object, e As Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDaily.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblreqid As Label = DirectCast(gvdaily.Rows(rowIndex).FindControl("lblreqid"), Label)
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CANCEL_CONFERENCE")
            SP1.Command.AddParameter("@REQ_ID", lblreqid.Text, DbType.String)

            SP1.ExecuteScalar()
        End If
        Binddailygrid()
    End Sub

    Protected Sub gvDaily_RowEditing(sender As Object, e As Web.UI.WebControls.GridViewEditEventArgs) Handles gvDaily.RowEditing

    End Sub

    Protected Sub gvDaily_RowDeleting(sender As Object, e As Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDaily.RowDeleting

    End Sub
End Class
