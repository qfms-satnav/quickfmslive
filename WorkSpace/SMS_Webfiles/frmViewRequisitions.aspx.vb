Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmViewRequisitions
    Inherits System.Web.UI.Page
    Dim strSQL As String
    Dim ObjDR As SqlDataReader
    Dim i As Integer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        txtFromDate.Attributes.Add("onClick", "displayDatePicker('" + txtFromDate.ClientID + "')")
        txtFromDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtDate.Attributes.Add("onClick", "displayDatePicker('" + txtDate.ClientID + "')")
        txtDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        Try
            If Not IsPostBack Then
                grdReqList.Visible = False
                grdReqList.Visible = False
                pnlBetweenDates.Visible = False
                pnlDatewise.Visible = False
                pnlMonth.Visible = False
                lblDisp.Visible = False
                lblExcel.Visible = False
                pnlButton.Visible = False
                btnExcel.Enabled = False
                btnPrint.Enabled = False
            End If
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            ' fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub cmbCriteria_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCriteria.SelectedIndexChanged
        Try
            grdReqList.Visible = False
            txtHiddenCriteria.Text = cmbCriteria.SelectedItem.Value
            If cmbCriteria.SelectedItem.Value = 0 Then
                pnlView.Visible = True
            End If
            If cmbCriteria.SelectedItem.Value <> 0 Then
                pnlButton.Visible = False
                pnlView.Visible = True
                If cmbCriteria.SelectedItem.Value = 1 Then
                    pnlBetweenDates.Visible = True
                    pnlDatewise.Visible = False
                    pnlMonth.Visible = False
                    txtFromDate.Text = ""
                    txtToDate.Text = ""
                    pnlButton.Visible = False
                    pnlView.Visible = True
                ElseIf cmbCriteria.SelectedItem.Value = 2 Then
                    pnlBetweenDates.Visible = False
                    pnlDatewise.Visible = True
                    pnlMonth.Visible = False
                    txtDate.Text = ""
                    pnlButton.Visible = False
                    pnlView.Visible = True
                ElseIf cmbCriteria.SelectedItem.Value = 3 Then
                    pnlBetweenDates.Visible = False
                    pnlDatewise.Visible = False
                    pnlMonth.Visible = True
                    pnlButton.Visible = False
                    pnlView.Visible = True
                    cmbYear.Items.Clear()
                    Dim currYear As Integer = Year(getoffsetdatetime(DateTime.Now)())
                    For i = currYear - 5 To currYear + 1
                        cmbYear.Items.Add(i)
                    Next
                    cmbYear.SelectedIndex = 5
                    cmbMonth.SelectedIndex = 0
                    cmbMonth.SelectedIndex = Month(getoffsetdatetime(DateTime.Now))
                End If
            Else
                btnExcel.Visible = False
                btnPrint.Visible = False
                pnlBetweenDates.Visible = False
                pnlDatewise.Visible = False
                pnlMonth.Visible = False
                pnlButton.Visible = False
                pnlView.Visible = True
            End If
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub bindgrid()
        'Try
        Dim strsql1, strsql2, strsql3, strsql, locs, userid, rol, Tstrsql As String
        Dim uid As String = Session("uid")
        Dim flag = 0
        Dim objdata As SqlDataReader
        strsql = "select url_usr_id,rol_acronym,URL_SCOPEMAP_ID " & _
                 "from  " & Session("TENANT") & "."  & "user_role, " & Session("TENANT") & "."  & "role " & _
                 "where url_rol_id=rol_id and rol_acronym in ('OSSI','OSSA','GAdmin','All','DEMO') and url_usr_id = '" & uid & "'"

        objdata = SqlHelper.ExecuteReader(CommandType.Text, strsql)
        strsql1 = "0"
        strsql2 = "0"
        strsql3 = "0"
        strsql = "0"
        locs = "0"
        While objdata.Read
            locs = locs & "," & objdata("URL_SCOPEMAP_ID").ToString
            userid = objdata("url_usr_id").ToString
            rol = objdata("rol_acronym").ToString
        End While
        If rol.ToUpper <> "GADMIN" Then

            If cmbCriteria.SelectedItem.Value = 1 Then
                strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE  CONVERT(NVARCHAR,SRN_REQ_DT,101) between '" & txtFromDate.Text & "' and '" & txtToDate.Text & "' and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
            ElseIf cmbCriteria.SelectedItem.Value = 2 Then
                strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE  CONVERT(NVARCHAR,SRN_REQ_DT,101) = '" & txtDate.Text & "' and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
            ElseIf cmbCriteria.SelectedItem.Value = 3 Then
                strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE month(SRN_REQ_DT)=" & cmbMonth.SelectedItem.Value & " and year(SRN_REQ_DT)= " & cmbYear.SelectedItem.Text & " and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
            End If
        Else
            If cmbReqStatus.SelectedItem.Text = "--All--" Then
                If cmbCriteria.SelectedItem.Value = 1 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE  CONVERT(NVARCHAR,SRN_REQ_DT,101) between '" & txtFromDate.Text & "' and '" & txtToDate.Text & "'"
                ElseIf cmbCriteria.SelectedItem.Value = 2 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE  CONVERT(NVARCHAR,SRN_REQ_DT,101) = '" & txtDate.Text & "' "
                ElseIf cmbCriteria.SelectedItem.Value = 3 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE month(SRN_REQ_DT)=" & cmbMonth.SelectedItem.Value & " and year(SRN_REQ_DT)= " & cmbYear.SelectedItem.Text
                End If

            End If
        End If
        If rol <> "OSSA" Then
            If cmbReqStatus.SelectedItem.Text = "--All--" Then
                If cmbCriteria.SelectedItem.Value = 1 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE SRN_AUR_ID ='" & Session("uid") & "'  AND CONVERT(NVARCHAR,SRN_REQ_DT,101) between '" & txtFromDate.Text & "' and '" & txtToDate.Text & "'"
                ElseIf cmbCriteria.SelectedItem.Value = 2 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE SRN_AUR_ID ='" & Session("uid") & "'  AND CONVERT(NVARCHAR,SRN_REQ_DT,101) = '" & txtDate.Text & "' "
                ElseIf cmbCriteria.SelectedItem.Value = 3 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE SRN_AUR_ID ='" & Session("uid") & "'  AND month(SRN_REQ_DT)=" & cmbMonth.SelectedItem.Value & " and year(SRN_REQ_DT)= " & cmbYear.SelectedItem.Text
                End If
            Else
                If cmbCriteria.SelectedItem.Value = 1 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE SRN_AUR_ID ='" & Session("uid") & "'  AND CONVERT(NVARCHAR,SRN_REQ_DT,101) between '" & txtFromDate.Text & "' and '" & txtToDate.Text & "' and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
                ElseIf cmbCriteria.SelectedItem.Value = 2 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE SRN_AUR_ID ='" & Session("uid") & "'  AND CONVERT(NVARCHAR,SRN_REQ_DT,101) = '" & txtDate.Text & "' and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
                ElseIf cmbCriteria.SelectedItem.Value = 3 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE ) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE SRN_AUR_ID ='" & Session("uid") & "'  AND month(SRN_REQ_DT)=" & cmbMonth.SelectedItem.Value & " and year(SRN_REQ_DT)= " & cmbYear.SelectedItem.Text & "  and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
                End If
            End If
        Else
            If cmbReqStatus.SelectedItem.Text = "--All--" Then
                If cmbCriteria.SelectedItem.Value = 1 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_ONE) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE (SRN_APP_BDG in (" & locs & ") or SRN_AUR_ID = '" & Session("uid") & "') and CONVERT(NVARCHAR,SRN_REQ_DT,101) between '" & txtFromDate.Text & "' and '" & txtToDate.Text & "'"
                ElseIf cmbCriteria.SelectedItem.Value = 2 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE (SRN_APP_BDG in (" & locs & ") or SRN_AUR_ID = '" & Session("uid") & "') and CONVERT(NVARCHAR,SRN_REQ_DT,101) = '" & txtDate.Text & "' "
                ElseIf cmbCriteria.SelectedItem.Value = 3 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE (SRN_APP_BDG in (" & locs & ") or SRN_AUR_ID = '" & Session("uid") & "') and month(SRN_REQ_DT)=" & cmbMonth.SelectedItem.Value & " and year(SRN_REQ_DT)= " & cmbYear.SelectedItem.Text
                End If
            Else
                If cmbCriteria.SelectedItem.Value = 1 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE  (SRN_APP_BDG in (" & locs & ") or SRN_AUR_ID = '" & Session("uid") & "') and CONVERT(NVARCHAR,SRN_REQ_DT,101) between '" & txtFromDate.Text & "' and '" & txtToDate.Text & "' and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
                ElseIf cmbCriteria.SelectedItem.Value = 2 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code = SRM.SRN_BDG_ONE) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE (SRN_APP_BDG in (" & locs & ") or SRN_AUR_ID = '" & Session("uid") & "') and CONVERT(NVARCHAR,SRN_REQ_DT,101) = '" & txtDate.Text & "' and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
                ElseIf cmbCriteria.SelectedItem.Value = 3 Then
                    strsql1 = "SELECT DISTINCT (select  distinct top 1  AUR_KNOWN_AS  from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID=SRN_AUR_ID) SRN_AUR_ID,SRN_STA_ID, (select  distinct top 1  STA_TITLE from  " & Session("TENANT") & "."  & "STATUS where STA_ID=SRN_STA_ID) Status, SRN_REQ_ID,SRN_REQ_DT,SRN_REM,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_ONE) PRM_DESC1,(select  distinct top 1  lcm_name FROM  " & Session("TENANT") & "."  & "location WHERE lcm_code= SRM.SRN_BDG_TWO ) PRM_DESC2,srn_type_id  FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "location PRM WHERE (SRN_APP_BDG in (" & locs & ") or SRN_AUR_ID = '" & Session("uid") & "') and month(SRN_REQ_DT)=" & cmbMonth.SelectedItem.Value & " and year(SRN_REQ_DT)= " & cmbYear.SelectedItem.Text & " and SRN_STA_ID=" & cmbReqStatus.SelectedItem.Value
                End If
            End If
        End If
        Tstrsql = strsql1 & "  order by SRN_STA_ID, SRN_REQ_ID, SRN_REQ_DT desc"

        BindGridSet(Tstrsql, grdReqList)
        BindGridSet(Tstrsql, Datagrid1)

        Dim i As Integer
        For i = 0 To grdReqList.Rows.Count - 1
            If grdReqList.Rows(i).Cells(6).Text.Trim = "5" Then
                grdReqList.Rows(i).Cells(5).ForeColor = System.Drawing.Color.Indigo
            ElseIf grdReqList.Rows(i).Cells(6).Text.Trim = "8" Then
                grdReqList.Rows(i).Cells(5).ForeColor = System.Drawing.Color.Red
            ElseIf grdReqList.Rows(i).Cells(6).Text.Trim = "10" Then
                grdReqList.Rows(i).Cells(5).ForeColor = System.Drawing.Color.Purple
            ElseIf grdReqList.Rows(i).Cells(6).Text.Trim = "7" Then
                grdReqList.Rows(i).Cells(5).ForeColor = System.Drawing.Color.Green
            ElseIf grdReqList.Rows(i).Cells(6).Text.Trim = "9" Then
                grdReqList.Rows(i).Cells(5).ForeColor = System.Drawing.Color.Blue
            ElseIf grdReqList.Rows(i).Cells(6).Text.Trim = "16" Then
                grdReqList.Rows(i).Cells(5).ForeColor = System.Drawing.Color.Gray
            ElseIf grdReqList.Rows(i).Cells(6).Text.Trim = "21" Then
                grdReqList.Rows(i).Cells(5).ForeColor = System.Drawing.Color.SeaGreen
            End If

            If grdReqList.Rows(i).Cells(8).Text = "1" Then
                grdReqList.Rows(i).Cells(7).Text = "Employee(s)"
            ElseIf grdReqList.Rows(i).Cells(8).Text = "2" Then
                grdReqList.Rows(i).Cells(7).Text = "Non Employee(s)"
            Else
                grdReqList.Rows(i).Cells(7).Text = "Na"
            End If
        Next
        For i = 0 To Datagrid1.Rows.Count - 1
            If Datagrid1.Rows(i).Cells(7).Text = "1" Then
                Datagrid1.Rows(i).Cells(6).Text = "Employee(s)"
            ElseIf Datagrid1.Rows(i).Cells(7).Text = "2" Then
                Datagrid1.Rows(i).Cells(6).Text = "Non Employee(s)"
            Else
                grdReqList.Rows(i).Cells(6).Text = "Na"
            End If
        Next

        If grdReqList.Rows.Count = 0 Then
            grdReqList.Visible = False
            lblDisp.Visible = True
            lblDisp.Text = "No Requisitions"
            pnlButton.Visible = False
            pnlView.Visible = False
        Else
            pnlButton.Visible = True
            pnlView.Visible = False
            btnExcel.Enabled = True
            btnPrint.Enabled = True
            grdReqList.Visible = True
            lblDisp.Visible = False
        End If
    End Sub
    Private Sub btnCriteria_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCriteria.Click
        Try
            pnlMain.Visible = True
            grdReqList.Visible = False
            lblDisp.Visible = False
            pnlButton.Visible = False
            pnlView.Visible = True
            btnView.Enabled = True
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        Try
            pnlMain.Visible = False
            visibleTF(1)
            bindgrid()
            visibleTF(0)
            btnView.Enabled = False
            btnCriteria.Enabled = True
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Private Sub btnExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        Try
            Datagrid1.Visible = True
            ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try

    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            PNLCONTAINER.Visible = False
            lblExcel.Visible = False
            If txtHiddenCriteria.Text = 1 Then
                lblExcel.Text = " Space Requisitions Status  between " & txtFromDate.Text & " and " & txtToDate.Text
            ElseIf txtHiddenCriteria.Text = 2 Then
                lblExcel.Text = " Space Requisitions Status as on  " & txtDate.Text
            ElseIf txtHiddenCriteria.Text = 3 Then
                lblExcel.Text = " Space Requisitions Status for " & cmbMonth.SelectedItem.Text & "    " & cmbYear.SelectedItem.Text
            End If
            lblHead.Visible = True
            lblHead.Text = lblExcel.Text
            pnlMain.Visible = False
            pnlButton.Visible = False
            grdReqList.Visible = False
            Datagrid1.Visible = True
            Response.Write("***a-mantra***")
            Response.Write("<script>javascript:window.print();</script> ")
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            ' fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Private Sub cmbReqStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            grdReqList.Visible = False
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Protected Sub grdReqList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdReqList.PageIndexChanging
        Try
            grdReqList.PageIndex = e.NewPageIndex
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            ' fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            ' fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub

    Protected Sub grdReqList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdReqList.RowCommand
        Try
            Dim btn As LinkButton = sender
            If btn.Text = "Show Details" Then
                Response.Write("<Script language=Javascript>window.open(""frmSpcViewDtlsl.aspx?RID=" & grdReqList.Rows(0).Cells(0).Text & " &status=" & grdReqList.Rows(0).Cells(5).Text & """)</Script>")
            End If
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub visibleTF(ByVal s As Integer)
        If s = 1 Then
            grdReqList.Columns(4).Visible = True
            grdReqList.Columns(6).Visible = True
            grdReqList.Columns(8).Visible = True
            Datagrid1.Columns(4).Visible = True
            Datagrid1.Columns(7).Visible = True
        Else
            grdReqList.Columns(4).Visible = False
            grdReqList.Columns(6).Visible = False
            grdReqList.Columns(8).Visible = False
            Datagrid1.Columns(4).Visible = False
            Datagrid1.Columns(7).Visible = False
        End If

    End Sub
End Class
