<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <%-- <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />--%>
    <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>

    <%-- <script defer src="../../BootStrapCSS/Scripts/highcharts.js"></script>--%>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Summary Report 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal" runat="server">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="bs-docs-example">
                                    <ul id="myTab" class="nav nav-tabs" role="tablist">
                                        <%--<li class="active"><a href="#consolidated" role="tab" data-toggle="tab"><i class="fa fa-home"></i>&nbsp;Consolidated Seat Report</a></li>--%>
                                        <li class="active"><a href="#All" role="tab" data-toggle="tab"><i class="fa fa-users"></i>&nbsp;Seat Summary</a></li>
                                        <li><a href="#vertical" role="tab" data-toggle="tab"><i class="fa fa-building-o"></i>&nbsp;<label runat="server" id="lblverticaltab"></label></a></li>
                                    </ul>
                                    <div>&nbsp;</div>
                                    <div id="myTabContent" class="tab-content">
                                        <%--<div class="tab-pane fade in active" id="consolidated">
                                        </div>--%>
                                        <div class="tab-pane fade in active" id="All">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">City</label>
                                                            <div class="col-md-6">
                                                                <select id="ddlCity" class="selectpicker" data-live-search="true">
                                                                    <option value='--All--'>--All--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">Location</label>
                                                            <div class="col-md-6">
                                                                <select id="ddlLocation" class="selectpicker" data-live-search="true">
                                                                    <option value='--All--'>--All--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">Tower </label>
                                                            <div class="col-md-6">
                                                                <select id="ddlTower" class="selectpicker" data-live-search="true">
                                                                    <option value='--All--'>--All--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">Floor</label>
                                                            <div class="col-md-6">
                                                                <select id="ddlFloor" class="selectpicker" data-live-search="true">
                                                                    <option value='--All--'>--All--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-1">
                                                </div>
                                                <div class="col-md-2">
                                                    <img src="../../images/chair_green.gif" />&nbsp;<b><label id="lblAvailable_Seats" style="font-weight: bold;"></label></b> Total
                                                </div>
                                                <div class="col-md-2">
                                                    <img src="../../images/chair_green.gif" />&nbsp;<label id="lblSEATS_AVAILABLE_FOR_ALLOCATION" style="font-weight: bold;"></label>
                                                    Vacant                          
                                                </div>
                                                <div class="col-md-2">
                                                    <img src="../../images/chair_red.gif" />&nbsp;<label id="lblOCCUPIED_SEATS" style="font-weight: bold;"></label>
                                                    Occupied 
                                                </div>
                                                <div class="col-md-3">
                                                    <img src="../../images/Chair_Blue.gif" />&nbsp;<label id="lblALLOTTED_BUT_NOT_OCCUPIED_SEATS" style="font-weight: bold;"></label>
                                                    Allocated but Vacant                           
                                                </div>
                                                <div class="row-md-1">
                                                    <img src="../../images/chair_red.gif" />&nbsp;<label id="lblALLOTED_SEATS" style="font-weight: bold;"></label>
                                                    Overall Allocated                          
                                                </div>
                                            </div>
                                            <br />
                                            <div>
                                                <div class="col-sm-12">
                                                    <label>View In : </label>
                                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                                        data-on-text="<span class='fa fa-table'></span>"
                                                        data-off-text="<span class='fa fa-bar-chart'></span>" />
                                                    <%-- data-label-text="<span class='fa fa-youtube fa-lg'></span>"--%>
                                                    <div class="col-md-12">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row tabularC" id="tabular">
                                                <div class="col-md-12">

                                                    <table id="tblsummary" style="width: 97%;" class="table table-condensed table-bordered table-hover table-striped">
                                                        <tr>
                                                            <th style="width: 150px;">City</th>
                                                            <th style="width: 200px;">Location</th>
                                                            <th style="width: 150px;">Tower</th>
                                                            <th style="width: 150px;">Floor</th>
                                                            <th style="width: 101px;">Total Seats</th>
                                                            <th style="width: 101px;">Vacant Seats</th>
                                                            <th style="width: 101px;">Occupied Seats</th>
                                                            <th style="width: 200px;">Allocated but Vacant Seats</th>
                                                            <th style="width: 101px;">Overall Allocated Seats</th>
                                                        </tr>
                                                    </table>
                                                    <div id="summarychartdiv"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="vertical">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <label class="col-md-5 control-label">
                                                                <label runat="server" id="lblSelVertical"></label>
                                                            </label>
                                                            <div class="col-md-7">
                                                                <select id="ddlVertical" runat="server" cssclass="selectpicker" data-live-search="true">
                                                                    <option value='--All--'>--All--</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="col-sm-12">
                                                    <label>View In : </label>
                                                    <input id="verticalviewswitch" type="checkbox" checked data-size="small"
                                                        data-on-text="<span class='fa fa-table'></span>"
                                                        data-off-text="<span class='fa fa-bar-chart'></span>" />
                                                    <%-- data-label-text="<span class='fa fa-youtube fa-lg'></span>"--%>
                                                    <div class="col-md-12">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row tabularC" id="vertabular">
                                                <div class="col-md-12">
                                                    <table id="tblvertical" style="width: 49%;" class="table table-condensed table-bordered table-hover table-striped">
                                                        <tr>
                                                            <th>Vertical</th>
                                                            <th>Allocated Seats</th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12 graphicalC" id="vergraphical">
                                                <div id="verticalchartdiv"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script defer src="../../Dashboard/C3/d3.v3.min.js"></script>
<script defer src="../../Dashboard/C3/c3.min.js"></script>
<link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
<script defer src="../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
<script defer src="../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
<script defer src="../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
<script defer type="text/javascript">
    var summarychart;
    var verticalchart;
    $(document).ready(function () {
        $("#summarychartdiv").fadeOut(function () {
            $("#tblsummary").fadeIn();
        });
        $("#vertabular").fadeIn();
        $("#vergraphical").fadeOut();

        //Summary Graph
        summarychart = c3.generate({
            data: {
                columns: [],
                type: 'bar',

                empty: { label: { text: "Sorry, No Data Found" } },
            },
            legend: {
                position: 'right'
            },
            axis: {
                x: {
                    label: {
                        text: 'Summary Report',
                        position: 'outer-center',
                    }
                },
                y: {
                    label: {
                        text: 'Seats count',
                        position: 'outer-middle',
                    }
                }
            },
            bar: {
                title: "Summary Report"
            }
        });


        //Vertical Graph
        verticalchart = c3.generate({
            data: {
                columns: [],
                type: 'bar',

                empty: { label: { text: "Sorry, No Data Found" } },
            },
            legend: {
                position: 'right'
            },
            axis: {
                x: {
                    label: {
                        text: 'Vertical Report',
                        position: 'outer-center',
                    }
                },
                y: {
                    label: {
                        text: 'Allocated Seats Count',
                        position: 'outer-middle',
                    }
                }
            },
        });


        $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                //$("#graphical").fadeOut(function () {
                //$("#tabular").fadeIn();
                //});
                $("#summarychartdiv").fadeOut(function () {
                    $("#tblsummary").fadeIn();
                });
            }
            else {
                $("#tblsummary").fadeOut(function () {
                    $("#summarychartdiv").fadeIn();
                });
            }
        });

        $('#verticalviewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                $("#vergraphical").fadeOut(function () {
                    $("#vertabular").fadeIn();
                });
            }
            else {
                $("#vertabular").fadeOut(function () {
                    $("#vergraphical").fadeIn(function () {
                        $("#vergraphical").load();
                        FunBindVerticalGrid();
                    });
                });
            }
        });

        //Dynamic Vertical Name bind
        $.ajax({
            type: "get",

            url: "../../api/SummaryReportAPI/GetlblVerticalCC",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#lblSelVertical").html("Select " + data);
                $("#lblverticaltab").html(data + " Wise");
                //lblparent = data[0];
            },
            failure: function (response) {
            }
        });

        funBindAll();
        funbindverticals();
        FunBindVerticalGrid();
        FunBindGrid();

        $("#ddlCity").change(function () {
            fnddlCitychange();

        });
        $("#ddlLocation").change(function () {
            fndddlLocationchange();

        });
        $("#ddlTower").change(function () {
            //FunBindGrid();
            fndddlTowerchange();

        });
        $("#ddlFloor").change(function () {
            FunBindGrid();
        })
        $("#ddlVertical").change(function () {
            FunBindVerticalGrid();
        });

        $("#summarychartdiv").append(summarychart.element);
        $("#verticalchartdiv").append(verticalchart.element);
    });

    function funBindAll() {
        $.getJSON('../../api/SummaryReportAPI/GetCityDetails', function (result) {
            $.each(result, function (key, value) {
                $("#ddlCity").append($("<option></option>").val(value.CTY_CODE).html(value.CTY_NAME));
            });
            $("#ddlCity").selectpicker('refresh');
        });
        var dataObject = { CityId: $("#ddlCity").val() };
        $.post('../../api/SummaryReportAPI/Getalllocations', dataObject, function (result) {
            $.each(result, function (key, value) {
                $("#ddlLocation").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
            $("#ddlLocation").selectpicker('refresh');
        });
        var dataObject = { CITY: $("#ddlCity").val(), LOCATION: $("#ddlLocation").val() };
        $.post('../../api/SummaryReportAPI/Getalltowers', dataObject, function (result) {
            $.each(result, function (key, value) {
                $("#ddlTower").append($("<option></option>").val(value.TWR_CODE).html(value.TWR_NAME));
            });
            $("#ddlTower").selectpicker('refresh');
        });

        var dataObject = { CITY: $("#ddlCity").val(), LOCATION: $("#ddlLocation").val(), TOWER: $("#ddlTower").val() };
        $.post('../../api/SummaryReportAPI/Getallfloors', dataObject, function (result) {
            $.each(result, function (key, value) {
                $("#ddlFloor").append($("<option></option>").val(value.FLR_CODE).html(value.FLR_NAME));
            });
            $("#ddlFloor").selectpicker('refresh');
        });

    }

    function ConsolidatedReport() {
        var dataObject = { CITY: $("#ddlCity").val(), LOCATION: $("#ddlLocation").val(), TOWER: $("#ddlTower").val(), FLOOR: $("#ddlFloor").val() };
        $.post('../../api/SummaryReportAPI/Getconsolidatedseatdetails', dataObject, function (result) {
            $("#lblAvailable_Seats").html(result[0].AVAILABLE_SEATS);
            $("#lblALLOTED_SEATS").html(result[0].ALLOTED_SEATS);
            $("#lblOCCUPIED_SEATS").html(result[0].OCCUPIED_SEATS);
            $("#lblALLOTTED_BUT_NOT_OCCUPIED_SEATS").html(result[0].ALLOTTED_BUT_NOT_OCCUPIED_SEATS);
            $("#lblSEATS_AVAILABLE_FOR_ALLOCATION").html(result[0].SEATS_AVAILABLE_FOR_ALLOCATION);

        });
    }

    function funbindverticals() {
        $.getJSON('../../api/SummaryReportAPI/Getverticals', function (result) {
            $.each(result, function (key, value) {
                $("#ddlVertical").append($("<option></option>").val(value.COST_CENTER_CODE).html(value.COST_CENTER_NAME));
            });
            $("#ddlVertical").selectpicker('refresh');
        });
    }

    function fnddlCitychange() {
        var dataObject = { CityId: $("#ddlCity").val() };
        $('#ddlLocation option').remove();
        $('#ddlTower option').remove();
        $('#ddlFloor option').remove();
        $.post('../../api/SummaryReportAPI/Getalllocations', dataObject, function (result) {

            $.each(result, function (key, value) {
                $("#ddlLocation").append($("<option></option>").val(value.LCM_CODE).html(value.LCM_NAME));
            });
            if ($('#ddlLocation option').length >= 1) {
                $("#ddlLocation option").eq(0).before($("<option></option>").val("--All--").html("--All--"));

            }
            fndddlLocationchange();
            $("#ddlLocation").selectpicker('refresh');
            $("#ddlTower").selectpicker('refresh');
            $("#ddlFloor").selectpicker('refresh');
        });
    }

    function fndddlLocationchange() {
        var dataObject = { CITY: $("#ddlCity").val(), LOCATION: $("#ddlLocation").val() };
        $('#ddlTower option').remove();
        $('#ddlFloor option').remove();
        $.post("../../api/SummaryReportAPI/Getalltowers", dataObject, function (result) {
            $.each(result, function (key, value) {
                $("#ddlTower").append($("<option></option>").val(value.TWR_CODE).html(value.TWR_NAME));
            });
            if ($('#ddlTower option').length >= 1) {
                $("#ddlTower option").eq(0).before($("<option></option>").val("--All--").html("--All--"));
            }
            fndddlTowerchange();
            $("#ddlTower").selectpicker('refresh');
            $("#ddlFloor").selectpicker('refresh');

        });
    }

    function fndddlTowerchange() {
        var dataObject = { CITY: $("#ddlCity").val(), LOCATION: $("#ddlLocation").val(), TOWER: $("#ddlTower").val() };
        $('#ddlFloor option').remove();
        $.post("../../api/SummaryReportAPI/Getallfloors", dataObject, function (result) {
            $.each(result, function (key, value) {
                $("#ddlFloor").append($("<option></option>").val(value.FLR_CODE).html(value.FLR_NAME));
            });
            if ($('#ddlFloor option').length >= 1) {
                $("#ddlFloor option").eq(0).before($("<option></option>").val("--All--").html("--All--"));
            }
            $("#ddlFloor").selectpicker('refresh');
            FunBindGrid();
        });
    }

    function FunBindGrid() {
        var table = $('#tblsummary');
        var dataObject = { CITY: $("#ddlCity").val(), LOCATION: $("#ddlLocation").val(), TOWER: $("#ddlTower").val(), FLOOR: $("#ddlFloor").val() };
        $.post("../../api/SummaryReportAPI/GetSummaryDetails", dataObject, function (summarydata) {
            $('#tblsummary td').remove();
            if (summarydata.Table.length > 0) {
                summarychart.load({
                    columns: [
                        ["Allocated Total", summarydata.ChartData[0].AllocatedTotal],
                        ["Occupied Total", summarydata.ChartData[1].OccupiedTotal],
                        ["Allocated Vacant", summarydata.ChartData[2].AllocatedVacant]
                    ],
                    ids: ['Allocated Total', 'Occupied Total', 'Allocated Vacant']
                });
                $("#summarychartdiv").append(summarychart.element);
                $.each(summarydata.Table, function (key, value) {
                    table.append("<tr>" +
                    "<td>" + value.CTY_NAME + "</td>" +
                    "<td>" + value.LCM_NAME + "</td>" +
                    "<td>" + value.TWR_NAME + "</td>" +
                    "<td>" + value.FLR_NAME + "</td>" +
                    "<td>" + value.AVAILABLE_SEATS + "</td>" +
                    "<td>" + value.SEATS_AVAILABLE_FOR_ALLOCATION + "</td>" +
                    "<td>" + value.OCCUPIED_SEATS + "</td>" +
                    "<td>" + value.ALLOCATED_BUT_NOT_OCCUPIED_SEATS + "</td>" +
                     "<td>" + value.ALLOCATED_SEATS + "</td>" +
                    "</tr>");
                });
            } else {
                $('#tblsummary td').remove()
                table.append("<tr>" + "<td colspan='9' align='center'> No Records </td>" + "</tr>");
                summarychart.unload({
                    ids: ['Allocated Total', 'Occupied Total', 'Allocated Vacant']
                });
            }
            ConsolidatedReport();
        });
    }
    function FunBindVerticalGrid() {
        var verttable = $('#tblvertical');
        $('#tblvertical td').remove()
        var dataObject = { VERTICAL: $("#ddlVertical").val() };
        $.post("../../api/SummaryReportAPI/GetVerticalDetails", dataObject, function (verticaldata) {
            $.each(verticaldata.Table, function (key, value) {
                verttable.append("<tr>" +
                "<td>" + value.COST_CENTER_NAME + "</td>" +
                "<td>" + value.ALLOCATED_SEATS + "</td>" +
                "</tr>");
            });
            if (verticaldata.Table.length > 0) {
                verticalchart.load({
                    columns: [
                        //["Vertical Name", verticaldata.VertChartData[0].verticalname],
                        //["Allocated Seats", verticaldata.VertChartData[1].allocatedseats]
                        ["Allocated Seats", verticaldata.VertChartData[0].allocatedseats]
                    ],
                    ids: ['Allocated Seats']
                    //ids: ["'" + $("#ddlVertical").val() + "'"]
                });

            } else {
                verttable.append("<tr>" + "<td colspan='2' align='center'> No Records </td>" + "</tr>");
                verticalchart.unload({
                    //ids: ['Vertical Name', 'Allocated Seats']
                    ids: ['Allocated Seats']
                });
            }
            $("#verticalchartdiv").append(verticalchart.element);
        });
    }
</script>
