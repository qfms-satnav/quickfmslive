﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Conference_UpdateWithhold.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Conference_UpdateWithhold" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
          <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

   
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
               <%-- <div ba-panel ba-panel-title="Conference Type Master" ba-panel-class="with-scroll" style="padding-right: 45px;">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">Edit Withheld Bookings</h3>
                        </div>
                        <div class="card">
                        <%--<div class="card-body" style="padding-right: 10px;">--%>
                            <form id="form1" runat="server">
                                <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="padding-left: 25px">
                                                            <div id="showDiv" runat="server">
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtid">Enter Request Id </label>
                                                                            <asp:TextBox ID="txtReqId" runat="server" Style="padding-right: 20px" CssClass="form-control" placeholder="Type request id here"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtloc">Location</label>
                                                                            <asp:DropDownList ID="ddlLocaton1" runat="server"  AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtcat">Reservation Room</label>
                                                                            <asp:DropDownList ID="ddlReser" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtcat">Select Status</label>
                                                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>--%>
                                                                </div>


                                                                <div class="row">
                                                                   <%--                                                                
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtid">Enter From Date</label>
                                                                            <div class='input-group date' id='fromdate'>
                                                                                <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>

                                    <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label>From Date<span class="text-danger">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                        ErrorMessage="Please Select From Date" ValidationGroup="Val1" ControlToValidate="txtfromDt"></asp:RequiredFieldValidator>
                                    <div class="input-group date" id='fromdate'>
                                        <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy"></asp:TextBox>
                                        <%--<div class="input-group-append">
                                            <div class="input-group-text bg-transparent">
                                                <span class="fa fa-calendar" onclick="setup('fromdate') "></span>
                                            </div>
                                        </div>--%>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>
                                    </div>

                                </div>
                            </div>
    <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <label> To Date<span class="text-danger">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none"
                                        ErrorMessage="Please Select To Date" ValidationGroup="Val1" ControlToValidate="txtToDt"></asp:RequiredFieldValidator>
                                    <div class="input-group date" id='toDt'>
                                        <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy"></asp:TextBox>
                                       <%-- <div class="input-group-append">
                                            <div class="input-group-text bg-transparent">
                                                <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                            </div>
                                        </div>--%>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>





                                                                  <%--  <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="txtloc">Enter To Date</label>
                                                                            <div class='input-group date' id='toDt'>
                                                                                <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                                                </span>
                                                                            </div>
                                                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="To date should be greater than From date" ControlToValidate="txtToDt" ControlToCompare="txtfromDt" Type="Date" Operator="GreaterThan" runat="server" />
                                                                        </div>
                                                                    </div>--%>
                                                                           <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <br />
                                                                <asp:Button ID="btnsearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                                                CausesValidation="true" TabIndex="2" />
                                                                        </div>
                                                                    </div>
                                                                       <%--<div class="col-md-12 ">
                                                                        <%--<div class="col-md-12 text-right">--%>
                                                                            <%--<br />--%>
                                                                            <%--<asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                                                CausesValidation="true" TabIndex="2" />--%>
                                                                        <%--</div>--%>
                                                                    <%--</div>--%>
                                                                </div>

                                                           <%--     <div class="row">
                                                                    <div class="col-md-12 ">
                                                                        <div class="col-md-12 text-right">
                                                                            <br />
                                                                            <asp:Button ID="txtReqIdFilter" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                                                CausesValidation="true" TabIndex="2" />
                                                                        </div>
                                                                    </div>
                                                                </div>--%>

                                                            </div>
                                                        </div>
                                <div id="pnlemp" runat="server">
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-md-12">
                                            <asp:GridView ID="gvDaily" runat="server" AutoGenerateColumns="False" EmptyDataText="No Reservation Withhold Found." AllowPaging="true"
                                                CssClass="table GridStyle" GridLines="None" OnRowDataBound="gvDaily_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Request ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQ_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("lcm_name")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Room Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("SPC_name") %>'></asp:Label>
                                                            <asp:Label ID="lblssa_spc_id" Visible="false" runat="server" Text='<%# Eval("ssa_spc_id") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Employee Code" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblssa_emp_map" runat="server" Text='<%# Eval("ssa_emp_map") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Employee Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("emp_name")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Department">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("Dep_name")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="From Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbldate" runat="server" Text='<%# Eval("FROM_date") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="From Time">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTime" runat="server" Text='<%# Eval("SSA_FROM_TIME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="To Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("TO_date") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="To Time">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTOTIME" runat="server" Text='<%# Eval("SSA_TO_TIME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblstat" runat="server" Text='<%# Eval("stat") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ShowHeader="False">
                                                        <ItemTemplate>
                                                            <a href='Conference_ModifyWithhold.aspx?id=<%#Eval("REQ_ID")%>'>Edit</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:ButtonField Text="Release" CommandName="DELETE" />

                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



