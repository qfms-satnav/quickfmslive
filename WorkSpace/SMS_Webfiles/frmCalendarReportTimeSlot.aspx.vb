Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions

Imports System.Threading

Partial Class WorkSpace_SMS_Webfiles_frmCalendarReportTimeSlot
    Inherits System.Web.UI.Page


    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("en-GB", False)


        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""

        txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        If Not Page.IsPostBack Then
            LoadCity()
        End If
    End Sub

    Public Sub LoadCity()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtFrmDate.Text = "" Then
            lblMsg.Text = "Select From date."
            Exit Sub
        End If

        If txtToDate.Text = "" Then
            lblMsg.Text = "Select to date."
            Exit Sub
        End If
        If ddlCity.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Select City."
            Exit Sub
        End If
        If ddlSelectLocation.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Select Location."
            Exit Sub
        End If

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        param(0).Value = txtFrmDate.Text
        param(1) = New SqlParameter("@TODATE", SqlDbType.NVarChar, 200)
        param(1).Value = txtToDate.Text
        param(2) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(2).Value = ddlCity.SelectedItem.Value
        param(3) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        param(3).Value = ddlSelectLocation.SelectedItem.Value
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GETCALENDARVERTICALTIME", param)
        If ds.Tables(0).Rows.Count > 0 Then
            CalendarReport.Visible = True
            lblMsg.Text = ""
        Else
            lblMsg.Text = "No records found."
            CalendarReport.Visible = False
        End If
    End Sub

    Dim dayofweek As String
    Dim subtract As Integer
    Dim cnt As Integer = 0

    Protected Sub CalendarReport_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles CalendarReport.DayRender
        'Try
        'CalendarReport.SelectedDate = Convert.ToDateTime(txtFrmDate.Text)
        Dim linkStr As String
        If txtFrmDate.Text = "" Then
            lblMsg.Text = "Select From date."
            Exit Sub
        End If

        If txtToDate.Text = "" Then
            lblMsg.Text = "Select to date."
            Exit Sub
        End If
        If ddlCity.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Select City."
            Exit Sub
        End If
        If ddlSelectLocation.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Select Location."
            Exit Sub
        End If

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@FROMDATE", SqlDbType.NVarChar, 200)
        param(0).Value = txtFrmDate.Text
        param(1) = New SqlParameter("@TODATE", SqlDbType.NVarChar, 200)
        param(1).Value = txtToDate.Text
        param(2) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(2).Value = ddlCity.SelectedItem.Value
        param(3) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        param(3).Value = ddlSelectLocation.SelectedItem.Value
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GETCALENDARVERTICALTIME", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblMsg.Text = ""
            Dim newdate As Date = Today.AddDays(subtract)
            Dim startweekdate As Date = CDate(ds.Tables(0).Rows((ds.Tables(0).Rows.Count - 1)).Item("SSA_FROM_DATE")).ToString("d")
            Dim endweekdate As Date = CDate(ds.Tables(0).Rows((ds.Tables(0).Rows.Count - 1)).Item("SSA_TO_DT")).ToString("d")
            'Dim TrainDate As String : TrainDate = e.Day.Date : e.Day.IsSelectable = False
            e.Cell.Enabled = True : e.Cell.HorizontalAlign = HorizontalAlign.Left : e.Cell.VerticalAlign = VerticalAlign.Top

            If e.Day.Date >= CDate(ds.Tables(1).Rows((ds.Tables(1).Rows.Count - 1)).Item("fromdate")) And e.Day.Date <= CDate(ds.Tables(1).Rows((ds.Tables(1).Rows.Count - 1)).Item("todate")) Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If e.Day.Date >= CDate(ds.Tables(0).Rows(i).Item("SSA_FROM_DATE")) And e.Day.Date <= CDate(ds.Tables(0).Rows(i).Item("SSA_TO_DT")) Then
                        e.Cell.BackColor = Drawing.Color.DarkKhaki '#FFCC66 "#FFCC66" 'Drawing.Color.Green
                        linkStr = "<span style=""font-size:8pt; color:black""><br>"
                        linkStr = linkStr & ds.Tables(0).Rows(i).Item("SSA_VERTICAL").ToString & "<br>From Time: " & ds.Tables(0).Rows(i).Item("FROM_TIME").ToString & "<br>To Time: " & ds.Tables(0).Rows(i).Item("TO_TIME").ToString & "<br>Total Allocated: (<a href='#' onclick=""showPopWin('ShowSpaceIdforVertical.aspx?req=" & ds.Tables(0).Rows(i).Item("SSA_SRNCC_ID") & "&fdt=" & ds.Tables(0).Rows(i).Item("SSA_FROM_DATE") & "&tdt=" & ds.Tables(0).Rows(i).Item("SSA_TO_DT") & "&vt=" & ds.Tables(0).Rows(i).Item("SSA_VERTICAL") & "&ct=" & ddlCity.SelectedItem.Value & "&loc=" & ddlSelectLocation.SelectedItem.Value & "',580,300,null)"">" & ds.Tables(0).Rows(i).Item("CNT").ToString & "</a>)<br>"


                        linkStr = linkStr & "</span>"
                        e.Cell.Controls.Add(New LiteralControl(linkStr))
                    End If
                Next
            End If
            'e.Cell.BackColor = Drawing.Color.Red
            'linkStr = "<span style=""font-size:8pt; font-Weight:bold; color:white""><br /></span>"
            'e.Cell.Controls.Add(New LiteralControl(linkStr))
        Else
            lblMsg.Text = "No records found."
        End If
        '     Catch ex2 As System.FormatException
        '         Dim linkStr As String
        '         e.Cell.BackColor = Drawing.Color.Red
        '         linkStr = "<span style=""font-size:11pt; font-Weight:bold; color:white""><br />Error Loading Date: " & e.Day.Date & _
        '"<br /><br />Check Date and Re-Submit!"
        '         e.Cell.Controls.Add(New LiteralControl(linkStr))
        '     Catch ex As Exception : MsgBox(ex.ToString)
        '     End Try
    End Sub



    Protected Sub CalendarReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendarReport.Load
        'CalendarReport.FirstDayOfWeek = FirstDayOfWeek.Sunday
        CalendarReport.NextPrevFormat = NextPrevFormat.ShortMonth
        CalendarReport.TitleFormat = TitleFormat.Month
        CalendarReport.ShowGridLines = True
        CalendarReport.DayStyle.Height = New Unit(40)
        CalendarReport.DayStyle.Width = New Unit(130)
        CalendarReport.DayStyle.HorizontalAlign = HorizontalAlign.Center
        CalendarReport.DayStyle.VerticalAlign = VerticalAlign.Middle
        CalendarReport.OtherMonthDayStyle.BackColor = System.Drawing.Color.AliceBlue
    End Sub

    Protected Sub CalendarReport_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalendarReport.SelectionChanged
        Dim Selecteddt As String
        Selecteddt = CalendarReport.SelectedDate.Month & "/" & CalendarReport.SelectedDate.Day & "/" & CalendarReport.SelectedDate.Year
        Response.Redirect("CalendarReportDetails.aspx?dt=" & Selecteddt & "&cty=" & ddlCity.SelectedItem.Value & "&loc=" & ddlSelectLocation.SelectedItem.Value)
    End Sub
End Class
