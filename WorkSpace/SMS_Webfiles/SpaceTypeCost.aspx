﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <%----%>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>

<body data-ng-controller="SpaceTypeCostController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">

                <h3 class="panel-title">Space Type Wise Cost</h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 15px;">--%>
                <form role="form" id="form1" name="frmSpaceTypeCost" data-valid-submit="getfloordetails()" novalidate style="padding: 20px;">
                    <div class="col-md-8 col-md-offset-4">
                        <input type="radio" name="formulatype" value="A" data-ng-model="SpaceTypeCost.areatype" ng-change="areatypeChanged()" />&nbsp Area's Formula Change
                                    <input type="radio" name="formulatype" value="S" data-ng-model="SpaceTypeCost.areatype" data-ng-checked="true" ng-init="SpaceTypeCost.areatype='S'" />&nbsp Space Type Cost Change
                    </div>
                    <div class="row">
                        <div class="col-md-12"></div>
                    </div>
                    <br />
                    <div class="clearfix row">
                        <div class="col-md-3 col-sm-3 col-xs-12" data-ng-hide="LocationStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceTypeCost.$submitted && frmSpaceTypeCost.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div id="Country" isteven-multi-select data-input-model="Country" data-output-model="SpaceTypeCost.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SpaceTypeCost.Country" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceTypeCost.$submitted && frmSpaceTypeCost.CNY_NAME.$invalid" style="color: red">Please select country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-hide="LocationStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceTypeCost.$submitted && frmSpaceTypeCost.CTY_NAME.$invalid}">
                                <label class="control-label">City <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="SpaceTypeCost.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SpaceTypeCost.City" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceTypeCost.$submitted && frmSpaceTypeCost.CTY_NAME.$invalid" style="color: red">Please select city </span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceTypeCost.$submitted && frmSpaceTypeCost.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="SpaceTypeCost.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SpaceTypeCost.Location" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceTypeCost.$submitted && frmSpaceTypeCost.LCM_NAME.$invalid" style="color: red">Please select location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" ng-show="SpaceTypeCost.areatype=='S'" data-ng-hide="LocationStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceTypeCost.$submitted && frmSpaceTypeCost.TWR_NAME.$invalid}">
                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Tower" data-output-model="SpaceTypeCost.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                    data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SpaceTypeCost.Tower" name="TWR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceTypeCost.$submitted && frmSpaceTypeCost.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix row" ng-show="SpaceTypeCost.areatype=='S'">
                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-hide="LocationStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceTypeCost.$submitted && frmSpaceTypeCost.FLR_NAME.$invalid}">
                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Floor" data-output-model="SpaceTypeCost.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                    data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SpaceTypeCost.Floor" name="FLR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceTypeCost.$submitted && frmSpaceTypeCost.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-hide="LocationStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': frmSpaceTypeCost.$submitted && frmSpaceTypeCost.FLR_NAME.$invalid}">
                                <label class="control-label">Child Entity <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Childlist" data-output-model="SpaceTypeCost.selectedChildEntity" data-button-label="icon CHE_NAME" data-item-label="icon CHE_NAME"
                                    data-on-select-all="ChildEntityChange()" data-on-select-none="ChildEntityChangeAll()" data-on-item-click="ChildEntitySelectNone()" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SpaceTypeCost.selectedChildEntity" name="CHE_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSpaceTypeCost.$submitted && frmSpaceTypeCost.CHE_NAME.$invalid" style="color: red">Please select child entity </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <br />
                                <input type="radio" name="areadetails" value="Y" data-ng-model="SpaceTypeCost.costinfo" data-ng-change="SpaceChange()" />&nbsp Per Sq. Ft. area Wise Cost
                                            <br />
                                <input type="radio" name="areadetails" value="N" data-ng-model="SpaceTypeCost.costinfo" data-ng-checked="true" data-ng-change="SpaceChange()" />&nbsp Space Type Wise Cost
                                            <br />
                                <input type="radio" name="areadetails" value="L" data-ng-model="SpaceTypeCost.costinfo" data-ng-change="LocationChange()" />&nbsp Location Wise Cost
                                            <br />
                            </div>
                        </div>
                    </div>
                    <div ng-show="SpaceTypeCost.areatype=='S'">
                        <div class="row">
                            <div class="row text-left" style="padding-left: 1000px; padding-top: 9px">
                                <div class="form-group">
                                    <input type="submit" value="Get" class='btn btn-primary custom-button-color' data-ng-click="getfloordetails()" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="col-md-12 col-sm-12 col-xs-12" data-ng-show="viewstatus == 1">
                            <div class="box">
                                <div class="box-body">
                                    <table data-ng-table="tableParams" class="table GridStyle" gridlines="None">
                                        <tr>
                                            <th>Floor Code</th>
                                            <th>Floor Name </th>
                                            <th>Fixed Cost</th>
                                            <th>Variable Cost</th>
                                        </tr>
                                        <tr data-ng-repeat="SPC in SPCDetails">
                                            <td style="vertical-align: central !important;">
                                                <label>{{SPC.FLR_CODE}}</label>
                                            </td>
                                            <td style="vertical-align: central !important;">
                                                <label>{{SPC.FLR_NAME}}</label>
                                            </td>
                                            <td style="vertical-align: central !important;">
                                                <input type="text" ng-model="SPC.AW_COST" />
                                            </td>
                                            <td style="vertical-align: central !important;">
                                                <input type="text" ng-model="SPC.VAR_COST" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" data-ng-show="viewstatus == 2">
                            <div class="box">
                                <div class="box-body">
                                    <table data-ng-table="tableParams" class="table GridStyle" gridlines="None">
                                        <tr>
                                            <th>Floor Code</th>
                                            <th>Floor Name </th>
                                            <th>Fixed Cost</th>
                                            <th>Variable Cost</th>
                                        </tr>
                                        <tr data-ng-repeat="SPC in SPCDetails">
                                            <td style="vertical-align: central !important;">
                                                <label>{{SPC.FLR_CODE}}</label>
                                            </td>
                                            <td style="vertical-align: central !important;">
                                                <label>{{SPC.FLR_NAME}}</label>
                                            </td>
                                            <%--<td style="vertical-align: central !important;">
                                                        <input type="text" ng-model="SPC.AW_COST" />
                                                    </td> --%>
                                            <td data-ng-form="innerForm">
                                                <div class="input-group margin">
                                                    <div data-ng-class="{'has-error': frmSpaceTypeCost.$submitted}">
                                                        <table data-ng-table="tableParams">
                                                            <tr data-ng-repeat="SPCcost in SPC.lstseattype">
                                                                <td>{{SPCcost.Name}}</td>
                                                                <td>
                                                                    <input class="input-sm" placeholder="" data-ng-model="SPCcost.Value" name="SPACE_COST" type="text"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <span class="error" data-ng-show="innerForm.SPACE_COST.$error.pattern">Enter Valid Number</span>
                                                </div>
                                            </td>
                                            <td data-ng-form="innerForm">
                                                <div class="input-group margin">
                                                    <div data-ng-class="{'has-error': frmSpaceTypeCost.$submitted}">
                                                        <table data-ng-table="tableParams">
                                                            <tr data-ng-repeat="Varcost in SPC.variableCost">
                                                                <td>{{Varcost.Name}}</td>
                                                                <td>
                                                                    <input class="input-sm" placeholder="" data-ng-model="Varcost.Value" name="VARIABLE_COST" type="text"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <span class="error" data-ng-show="innerForm.SPACE_COST.$error.pattern">Enter Valid Number</span>
                                                </div>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" data-ng-show="viewstatus == 3">
                            <div class="box">
                                <div class="box-body">
                                    <table data-ng-table="tableParams" class="table GridStyle" gridlines="None">
                                        <tr>
                                            <th>Location Code</th>
                                            <th>Location Name </th>
                                            <th>Seat Cost</th>
                                        </tr>
                                        <tr data-ng-repeat="LOC in LOCDetails">
                                            <td style="vertical-align: central !important;">
                                                <label>{{LOC.LCM_CODE}}</label>
                                            </td>
                                            <td style="vertical-align: central !important;">
                                                <label>{{LOC.LCM_NAME}}</label>
                                            </td>
                                            <td style="vertical-align: central !important;">
                                                <input type="text" ng-model="LOC.SEAT_COST" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <%-- <div class="row text-left" style="padding-left: 1000px; padding-top: 9px">--%>

                            <div class="col-md-12 text-right">
                                <%-- <div class="form-group">--%>
                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="save()" />
                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                <%--  </div>--%>
                            </div>
                        </div>
                        <%-- </div>--%>
                        <br />
                        <div class="row" style="padding-left: 15px;" ng-show="GridVisibility==1">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>

                        </div>
                        <div class="row" style="padding-left: 15px;" ng-show="GridLocation==1">
                            <input type="text" class="form-control" id="filtergrid" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridData" style="height: 250px; width: 100%;" class="ag-blue"></div>

                        </div>
                    </div>
                    <div ng-show="SpaceTypeCost.areatype=='A'">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="box">
                                <div class="box-body">
                                    <table data-ng-table="tableParams" class="table GridStyle" gridlines="None">
                                        <tr>
                                            <th>Area Type</th>
                                            <th>Option 1</th>
                                            <th>Operator</th>
                                            <th>Option 2</th>
                                            <th>Operator</th>
                                            <th>Option 3</th>
                                        </tr>
                                        <tr ng-repeat="td in TypeDetails">
                                            <td>{{ShowAreaType(td.SATD_SAT_ID)}}
                                                            <%--{{at.SAT_CODE}}--%>
                                            </td>
                                            <td>
                                                <select id="ddlopt1" ng-model="td.SATD_OPT1">
                                                    <option ng-selected="true">-- Select --</option>
                                                    <option ng-repeat="inat in AreaType |filter: {SAT_PARENT:1}" value="{{inat.SAT_ID}}">{{inat.SAT_CODE}}</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="ddlopratr1" ng-model="td.SATD_OPR1">
                                                    <option value="*">*</option>
                                                    <option value="/">/</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="ddlopt2" ng-model="td.SATD_OPT2">
                                                    <option ng-selected="true">-- Select --</option>
                                                    <option ng-repeat="inat in AreaType |filter: {SAT_PARENT:1}" value="{{inat.SAT_ID}}">{{inat.SAT_CODE}}</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="ddlopratr2" ng-model="td.SATD_OPR2">
                                                    <option value="/">/</option>
                                                    <option value="*">*</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="ddlopt3" ng-model="td.SATD_OPT3">
                                                    <option ng-selected="true">-- Select --</option>
                                                    <option ng-repeat="inat in AreaType |filter: {SAT_PARENT:1}" value="{{inat.SAT_ID}}">{{inat.SAT_CODE}}</option>
                                                </select>
                                            </td>
                                            <%--<td style="vertical-align: central !important;">
                                                            <label>{{SPC.FLR_CODE}}</label>
                                                        </td>
                                                        <td style="vertical-align: central !important;">
                                                            <label>{{SPC.FLR_NAME}}</label>
                                                        </td>
                                                        <td style="vertical-align: central !important;">
                                                            <input type="text" ng-model="SPC.AW_COST" />
                                                        </td>--%>
                                        </tr>
                                    </table>
                                </div>
                                <div class="box-footer">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="saveAreatypes()" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../../SMViews/Utility.min.js"></script>
    <script defer src="../../SMViews/Masters/SpaceTypeCost.min.js"></script>
</body>
</html>
