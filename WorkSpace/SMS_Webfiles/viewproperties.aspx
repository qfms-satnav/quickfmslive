﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="viewproperties.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_viewproperties" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- --%>
    <script defer src="http://maps.google.com/maps/api/js?sensor=false"
        type="text/javascript"></script>
    <script defer type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script defer type="text/javascript">
             
        var markers = [
        <asp:Repeater ID="rptMarkers" runat="server">
        <ItemTemplate>
                 {
                     "title": '<%# Eval("PN_NAME")%>',
                     "lat": '<%# Eval("LATITUDE")%>',
                     "lng": '<%# Eval("LONGITUDE")%>',
                     "description": '<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1 >  <%# Eval("PN_NAME")%>   <br> <b>Address:</b>  <%# Eval("PN_NAME")%> ,   <%# Eval("PROPERTY_DESCRIPTION")%>   ,   <%# Eval("CTY_NAME")%>   </font>'
                     

                 }
    </ItemTemplate>
    <SeparatorTemplate>
        ,
    </SeparatorTemplate>
    </asp:Repeater>
        ];      
    </script>
    <script defer type="text/javascript">

        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title
                });
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>
</head>
<body>
    <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View Properties
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class=" btn btn-default pull-right">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                        ToolTip="Please Select Add to add new Service Category and Select Modify to modify the existing Service Category" />
                                    View in List
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                        ToolTip="Please Select Add to add new Service Category and Select Modify to modify the existing Service Category" />
                                    View in Map
                                </label>
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div class="row" id="divcity" runat="server">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="Div1" runat="server">
                                        <label class="col-md-5 control-label">Filter by City  <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlproperty"
                                            Display="None" ErrorMessage="Please Select City" InitialValue="--Select--" ValidationGroup="Val1"> </asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlproperty" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="row">
                                                <div class="col-md-5 control-label">
                                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"></asp:Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--                        <div class="container">
                            <div class="row">
                                <div class="col-md-10">
                                    <div id="dvMap" style="height: 360px"></div>
                                </div>

                            </div>

                        </div>--%>
                        <div id="divmap" runat="server">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div id="dvMap" class="col-md-10" style="height: 460px">
                                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divlist" runat="server">
                            <div class="row table table table-condensed table-responsive">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


