﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms
Partial Class WorkSpace_SMS_Webfiles_FloorAllocReport
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ReportViewer1.Visible = False
            obj.bindTower(ddltower)
            ddltower.Items(0).Text = "--All--"
            ddlfloor.Items.Insert(0, "--All--")
            ddlfloor.SelectedIndex = 0
            trVertOcc.Visible = False
            Bindreport()

        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        Bindreport()
    End Sub
    Private Sub Bindreport()

        Dim strfloor As String = ""
        Dim strtower As String = ""

        If ddlfloor.SelectedItem.Value = "--All--" Then
            strfloor = ""
        Else
            strfloor = ddlfloor.SelectedItem.Value
        End If

        If ddlTower.SelectedItem.Value = "--All--" Then
            strtower = ""
        Else
            strtower = ddlTower.SelectedItem.Value
        End If

        Dim ds As New DataSet
        Dim grdTotal_WST As Integer = 0
        Dim grdTotal_HCB As Integer = 0
        Dim grdTotal_FCB As Integer = 0

        Dim grdTotal_Vacant_WST As Integer = 0
        Dim grdTotal_Vacant_HCB As Integer = 0
        Dim grdTotal_Vacant_FCB As Integer = 0


        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@SSA_TOWER", SqlDbType.NVarChar, 200)
        param(0).Value = strtower
        param(1) = New SqlParameter("@SSA_FLOOR", SqlDbType.NVarChar, 200)
        param(1).Value = strfloor
        ds = ObjSubSonic.GetSubSonicDataSet("USP_FLOOR_ALLOCATION_REPORT", param)

        Dim rds As New ReportDataSource()
        rds.Name = "FloorAllocationDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/FloorAllocationReport.rdlc")

        'Setting Header Column value dynamically
        Dim p1 As New ReportParameter("dynamicParam1", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        Dim param1(1) As SqlParameter
        param1(0) = New SqlParameter("@SSA_TOWER", SqlDbType.NVarChar, 200)
        param1(0).Value = strtower
        param1(1) = New SqlParameter("@SSA_FLOOR", SqlDbType.NVarChar, 200)
        param1(1).Value = strfloor
        Dim ds1 As DataSet
        ds1 = ObjSubSonic.GetSubSonicDataSet("USP_FLOOR_ALLOCATION_REPORT", param1)
        If ds1.Tables(0).Rows.Count > 0 Then
            trVertOcc.Visible = True
            lblTotalWST.Text = ds.Tables(1).Rows(0).Item("OCCUPIED_WORK_STATION")
            lblTotalHCB.Text = ds.Tables(1).Rows(0).Item("OCCUPIED_HALF_CABIN")
            lblTotalFCB.Text = ds.Tables(1).Rows(0).Item("OCCUPIED_full_cabin")
            'lblVacant_WST.Text = ds.Tables(1).Rows(0).Item("VACANT_WORK_STATION")
            'lblVacant_HCB.Text = ds.Tables(1).Rows(0).Item("VACANT_HALF_CABIN")
            'lblVacant_FCB.Text = ds.Tables(1).Rows(0).Item("VACANT_FULL_CABIN")
        Else
            trVertOcc.Visible = False
        End If
    End Sub

    Protected Sub ddltower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltower.SelectedIndexChanged
        ReportViewer1.Visible = False
        trVertOcc.Visible = False
        obj.bindfloor(ddlfloor, ddltower.SelectedValue)
        ddlfloor.Items(0).Text = "--All--"
        If ddltower.Items.Count = 2 Then
            ddltower.SelectedIndex = 0
        End If
    End Sub
End Class
