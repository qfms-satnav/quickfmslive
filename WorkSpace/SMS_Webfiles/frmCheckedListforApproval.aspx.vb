Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmCheckedListforApproval
    Inherits System.Web.UI.Page

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_LABELMASTER_RMAPPROVAL")
            sp.Command.AddParameter("@REQ_ID", ID, DbType.String)
            sp.Command.AddParameter("@REMARKS", txtComments.Text, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Text = "Report Succesfully Approved by RM"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_LABELMASTER_RMREJECT")
            sp.Command.AddParameter("@REQ_ID", ID, DbType.String)
            sp.Command.AddParameter("@REMARKS", txtComments.Text, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Text = "Report Rejected by RM"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim id As String = Request.QueryString("id")
            BindDetails(id)
            lblMsg.Text = ""
        End If
    End Sub
    Private Sub BindDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_LABELMASTER_GETDETAILS_SET1")
            sp.Command.AddParameter("@REQ_ID", id, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()

            gv.DataSource = ds.Tables(0)
            gv.DataBind()
            For i As Integer = 0 To gv.Rows.Count - 1
                Dim stat As Char = ds.Tables(0).Rows(i).Item("STAT")
                If stat = "Y" Then
                    CType(gv.Rows(i).FindControl("rdbtn"), RadioButtonList).SelectedValue = "Y"
                Else
                    CType(gv.Rows(i).FindControl("rdbtn"), RadioButtonList).SelectedValue = "N"
                End If
            Next
            gv1.DataSource = ds.Tables(1)
            gv1.DataBind()
            For i1 As Integer = 0 To gv1.Rows.Count - 1
                Dim stat1 As Char = ds.Tables(1).Rows(i1).Item("STAT")
                If stat1 = "Y" Then
                    CType(gv1.Rows(i1).FindControl("rdbtn1"), RadioButtonList).SelectedValue = "Y"
                Else
                    CType(gv1.Rows(i1).FindControl("rdbtn1"), RadioButtonList).SelectedValue = "N"
                End If
            Next
            gv2.DataSource = ds.Tables(2)
            gv2.DataBind()
            For i2 As Integer = 0 To gv2.Rows.Count - 1
                Dim stat2 As Char = ds.Tables(2).Rows(i2).Item("STAT")
                If stat2 = "Y" Then
                    CType(gv2.Rows(i2).FindControl("rdbtn2"), RadioButtonList).SelectedValue = "Y"
                Else
                    CType(gv2.Rows(i2).FindControl("rdbtn2"), RadioButtonList).SelectedValue = "N"
                End If
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
