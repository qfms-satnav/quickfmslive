<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SearchEmployee.ascx.vb"
    Inherits="WorkSpace_SMS_Webfiles_Controls_SearchEmployee" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding: 10px;">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
            <label for="txtcode">Select Search Type <span style="color: red;">*</span></label>
            <%--    <br />--%>
            <%--    <div class="col-md-7">--%>
            <asp:RadioButtonList ID="radSearch" AutoPostBack="true" runat="server">
                <asp:ListItem Text="Employee Id" Value="0" Enabled="true" Selected="True">
                </asp:ListItem>
                <asp:ListItem Text="Employee Name" Value="1" Enabled="true" Selected="false">
                </asp:ListItem>
            </asp:RadioButtonList>

        </div>
    </div>
    <div class="col-md-8 col-sm-6 col-xs-12" id="pnlempid" runat="server" visible="true">
        <div class="form-group">
            <%--<div class="row">--%>
            <div class="col-md-7">
                <label class="col-md-5 control-label">Employee Id<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="frvEmpSearch" runat="server" ControlToValidate="txtEmpId"
                    Display="None" ErrorMessage="Please Enter Employee Id to Search " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-6 col-xs-12" id="pnlEmailid" runat="server" visible="false">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Employee name<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmpName"
                        Display="None" ErrorMessage="Please Enter Employee Name to Search " ValidationGroup="Val1"></asp:RequiredFieldValidator>

                    <div class="col-md-7">
                        <asp:TextBox ID="txtEmpName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="clearfix">
            <div class="box-footer text-right">
                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
</div>
<br />

<%--<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">

            <div class="row">
                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" Style="padding-right: 45px" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
</div>--%>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="False" PageSize="1" EmptyDataText="No Employee Found." ShowHeaderWhenEmpty="True"
            CssClass="table GridStyle" GridLines="None">
            <Columns>
                <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblLoc" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_BDG_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Tower" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblTwr" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_TWR_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Floor" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <asp:Label ID="lblFlr" runat="server" CssClass="bodyText" Text='<%#Eval("SSA_FLR_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Space Id">
                    <ItemTemplate>
                        <%--<asp:Label ID="lblSpaceId" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_LAST_NAME")%>'></asp:Label>--%>
                        <asp:HyperLink
                            runat="server"
                            ID="hlDividents"
                            NavigateUrl='<%# String.Format("~/SMViews/Map/Maploader.aspx?lcm_code={0}&twr_code={1}&flr_code={2}&spc_id={3}&value={4}", Eval("FLR_BDG_ID"), Eval("FLR_TWR_ID"), Eval("FLR_CODE"), Eval("AUR_LAST_NAME"), ("1")) %>'
                            Text='<%# Eval("AUR_LAST_NAME") %>'>
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Entity" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <asp:Label ID="lblentity" runat="server" CssClass="bodyText" Text='<%#Eval("VER_CHE_CODE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vertical">
                    <ItemTemplate>
                        <asp:Label ID="lblSpaceId" runat="server" CssClass="bodyText" Text='<%#Eval("VER_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Costcenter">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" CssClass="bodyText" Text='<%#Eval("COST_CENTER_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Space Type">
                    <ItemTemplate>
                        <asp:Label ID="lblSpcType" runat="server" CssClass="bodyText" Text='<%#Eval("SPC_TYPE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee ID" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpName" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Name" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpName" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Extension Number" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblExtension" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_EXTENSION")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <label id="lblModalHead" class="control-label"></label>
                </h4>
            </div>
            <div class="modal-body" id="modelcontainer">
                <%-- Content loads here --%>
                <iframe id="modalcontentframe" src="#" width="100%" height="520px" style="border: none"></iframe>
            </div>
        </div>
    </div>
</div>

<script>
    function showPopWin(spcid, aurid) {
        //$("#modelcontainer").load("SpacesViewMapSearchEmployee.aspx?spcid=" + spcid + "&aur_id=" + aurid, function (responseTxt, statusTxt, xhr) {
        //    $("#myModal").modal('show');
        //});
        $("#modalcontentframe").attr("src", "SpacesViewMapSearchEmployee.aspx?spcid=" + spcid + "&aur_id=" + aurid);
        $("#myModal").modal('show');
        return false;
    }
</script>
