<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetViewDetails.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmAssetViewDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asset Details</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="center" width="100%">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                            ForeColor="Black">View Asset Details
             <hr align="center" width="60%" /></asp:Label></td>
                </tr>
            </table>
            <table id="table4" cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;View Asset Details</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <table id="tab" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Asset Code
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtasstcode" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Asset
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtasset" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Employee
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtemp" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Floor
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtasstflr" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Tower
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtassttower" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Location
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtasstloc" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    City
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtastcity" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
