Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_MAS_WebFiles_frmCountry
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters

    Private Sub Cleardata()
        txtCitycode.Text = String.Empty
        txtCityName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlCity.SelectedIndex = 0
        ddlCountry.SelectedIndex = 0
    End Sub
    Private Sub Modifydata()
        obj.getname = txtCityName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyCity(ddlCountry, ddlCity, Me) > 0) Then
            Cleardata()
            lblMsg.Text = "City Successfully updated  "
        End If
        obj.City_LoadGrid(gvItem)
        obj.BindCity(ddlCity)
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtCitycode.Text
        obj.getname = txtCityName.Text
        obj.getRemarks = txtRemarks.Text
        Dim iStatus As Integer = obj.InsertCity(ddlCountry, Me)
        If iStatus = 1 Then
            lblMsg.Text = "City Code is in use; try another "
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "City Successfully Inserted "
            lblMsg.Visible = True
            Cleardata()
        ElseIf iStatus = 0 Then
            lblMsg.Text = "City Successfully Inserted "
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.City_LoadGrid(gvItem)
        obj.BindCity(ddlCity)
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If Not Page.IsPostBack Then
                obj.City_LoadGrid(gvItem)
                trCName.Visible = False
                rbActions.Items(0).Selected = True
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "Page_Load", exp)
        End Try

    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        Try
            Dim i As Integer
            For i = 0 To rbActions.Items.Count - 1
                If rbActions.Items(i).Selected = True Then
                    If rbActions.Items(i).Text = "Add" Then
                        trCName.Visible = False
                        txtCitycode.ReadOnly = False
                        btnSubmit.Text = "Submit"
                        Cleardata()
                    ElseIf rbActions.Items(i).Text = "Modify" Then
                        trCName.Visible = True
                        txtCitycode.ReadOnly = True
                        btnSubmit.Text = "Modify"
                        Cleardata()
                        obj.BindCity(ddlCity)
                        obj.BindCountry(ddlCountry)
                    End If
                End If
            Next
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.SelectedItem.Text = "Add" Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtCitycode.Text = String.Empty Or txtCityName.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlCountry.SelectedItem.Text = "--Select--" Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 500 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me.Page)
                Else
                    Insertdata()
                End If
            ElseIf rbActions.SelectedItem.Text = "Modify" Then
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If ddlCity.SelectedItem.Value = "--Select--" Or txtCitycode.Text = String.Empty Or txtCityName.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlCountry.SelectedItem.Text = "--Select--" Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 500 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me.Page)
                Else
                    Modifydata()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmCountry", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            If ddlCity.SelectedItem.Value <> "--Select--" Then
                obj.BindCountry(ddlCountry)
                obj.City_SelectedIndex_Changed(ddlCity, ddlCountry)
                txtCitycode.Text = obj.getcode
                txtCityName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "ddlCity_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            'gvItem.Columns(2).Visible = True
            'gvItem.Columns(3).Visible = True
            obj.City_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmCountry", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.City_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(3).Visible = False
                    gvItem.HeaderRow.Cells(4).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(3).Visible = False
                        gvItem.Rows(i).Cells(4).Visible = False
                    Next
                    ' PopUpMessage("Inactivate all the Locations that are part of this City ", Me)
                    lblMsg.Text = "Inactivate all the Locations that are part of this City "
                    Exit Sub
                End If
                obj.City_LoadGrid(gvItem)
                Cleardata()
            End If
            obj.BindCity(ddlCity)
            obj.BindCountry(ddlCountry)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmCountry", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/Mas_Webfiles/frmMASMasters.aspx")
    End Sub
End Class