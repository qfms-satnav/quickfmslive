<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmRepMasters.aspx.vb" Inherits="SpaceManagement_frmRepMasters" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Reports</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <%--<div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnrefresh" runat="server" Text="Refresh Reports" CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>--%>

                        <%-- <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Button ID="btnrefresh" runat="server" Text="Refresh Reports" CssClass="btn btn-primary custom-button-color" />
                                    </div>
                                </div>--%>
                        <div id="divToggle4">

                            <h4>Requisition Reports</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton14" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repLocationwiseRequisition.aspx">Location Wise Requisition Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton13" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repTowerwiseRequisition.aspx">Tower Wise Requisition Report</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:LinkButton ID="LinkButtonver" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repVerticalwiseRequisition.aspx">
                                                <asp:Label ID="lblSelParent2" runat="server" Text=""></asp:Label>
                                                Wise Requisition Report
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div id="divToggle3">

                            <h4>Allocation Reports</h4>
                            <div class="row">
                                <%-- <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:LinkButton ID="LinkButton11" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repProjectwiseAllocation.aspx">
                                                <asp:Label ID="lblSelChild1" runat="server" Text=""></asp:Label>
                                                Wise Allocation Report
                                            </asp:LinkButton>
                                        </div>
                                    </div>--%>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton9" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/LocationAllocReport.aspx">Location Wise Allocation Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton10" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/TowerAllocReport.aspx">Tower Wise Allocation Report</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton11" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/FloorAllocReport.aspx">Floor Wise Allocation Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton12" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/frmVerticalAllocationReport.aspx">
                                            <asp:Label ID="lblSelParent1" runat="server" Text=""></asp:Label>
                                            Wise Allocation Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divToggle1">

                            <h4>Occupancy Reports</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton5" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repLocationwiseOccupancy.aspx">Location Wise Occupancy Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repTWROccupancy.aspx">Tower Wise Occupancy Report</asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <%--<div class="col-md-6">
                                        <div class="form-group">
                                            <asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repWingwiseOccupancy.aspx">Wing Wise Occupancy Report</asp:LinkButton>
                                        </div>
                                    </div>--%>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repFloorwiseOccupancy.aspx">Floor Wise Occupancy Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton19" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repshiftwiseoccupancy.aspx">Shift Wise Occupancy Report</asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton6" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repVerticalwiseOccupancy.aspx">
                                            <asp:Label ID="lblSelParent" runat="server" Text=""></asp:Label>
                                            Wise Occupancy Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton4" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repProjectwiseoccupancy.aspx">
                                            <asp:Label ID="lblselchild4" runat="server" Text=""></asp:Label>
                                            Wise Occupancy Report
                                        </asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <div id="divToggle">

                            <h4>Utilization Reports</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton16" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repLocationwiseutilization.aspx">Location Wise Utilization Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repTowerwiseUtilization.aspx">Tower Wise Utilization Report</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton18" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repFloorwiseutilization.aspx">Floor Wise Utilization Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton17" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/VerticalWiseutilization.aspx">
                                            <asp:Label ID="lblparent" runat="server" Text=""></asp:Label>
                                            Wise Utilization Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton15" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repProjectwiseUtilization.aspx">
                                            <asp:Label ID="lblSelChild" runat="server" Text=""></asp:Label>
                                            Wise Utilization Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div visible="false" runat="server">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="lnkGraphVertical" runat="server">
                                            <asp:Label ID="lblSelParent3" runat="server" Text=""></asp:Label>
                                            Wise Allocation Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="lnkGraphTower" runat="server">Tower Wise Allocation Report</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="lnkGraphProject" runat="server">
                                            <asp:Label ID="lblSelChild2" runat="server" Text=""></asp:Label>
                                            Wise Allocation Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="divToggle5">

                            <h4>Availability Reports</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton20" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/replocationavailability.aspx">Location Wise Availability Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton21" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/reptoweravailability.aspx">Tower Wise Availability Report</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton22" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repflooravailability.aspx">Floor Wise Availability Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton25" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repshiftavailability.aspx">Shift Wise Availability Report</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divtoggle6">

                            <h4>Cost Reports</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton23" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/BuildingCost.aspx">Location Cost Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton24" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/Verticalcostreport.aspx">
                                            <asp:Label ID="lblselparent6" runat="server" Text=""></asp:Label>
                                            Cost Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton26" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/Projectcostreport.aspx">
                                            <asp:Label ID="lblselchild6" runat="server" Text=""></asp:Label>
                                            Cost Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="divToggle2">


                            <h4>Extension Reports</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton7" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repTowerwiseExtension.aspx">Tower Wise Extension Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton8" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repFloorwiseExtension.aspx">Floor Wise Extension Report</asp:LinkButton>
                                    </div>
                                </div>
                            </div>


                        </div>


                         <div id="divRelease">


                            <h4>Release Reports</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton27" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/RepSearchRelease.aspx">Location Wise Release Report</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton28" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/repVerticalRelease.aspx">
                                            <asp:Label ID="lblParentVer" runat="server" Text=""></asp:Label>
                                            Wise Release Report
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                               <div class="row">
                              <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:LinkButton ID="LinkButton29" runat="server" PostBackUrl="../../WorkSpace/SMS_Webfiles/RepConsoleRelease.aspx">Consolidated Release Report</asp:LinkButton>
                                    </div>
                                </div>
                                   </div>

                        </div>






                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script defer type="text/javascript">
    function toggleDiv(divid) {
        if (document.getElementById(divid).style.display == 'none') {
            document.getElementById(divid).style.display = 'block';

        } else {
            document.getElementById(divid).style.display = 'none';
        }
    }

    function ToggleAll() {
        divToggle.style.display = 'none'
        divToggle1.style.display = 'none'
        divToggle2.style.display = 'none'
        divToggle3.style.display = 'none'
        divToggle4.style.display = 'none'
    }
    function ShowAll() {
        divToggle.style.display = 'block';
        divToggle1.style.display = 'block';
        divToggle2.style.display = 'block';
        divToggle3.style.display = 'block';
        divToggle4.style.display = 'block';
    }
</script>









