﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="conferenceRelease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_conferenceRelease" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">  
             <hr align="center" width="60%" />Booking Release</asp:Label></td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">&nbsp; <strong>Booking Release</strong>
            </td>
            <td style="width: 16px">
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">&nbsp;</td>
            <td align="left">
             
				    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
				
				<asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="clsMessage"
                    ForeColor="" />
                <br />
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                <br />
                <table id="table4" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                    border="1">
                    <tr>
                        <td align="left" class="clslabel" style="height: 26px; width: 50%" colspan="2">City<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                Display="None" ErrorMessage="Please Select City" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlCity" runat="server" Width="98%" CssClass="clsComboBox"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">Location<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlSelectLocation" AutoPostBack="true" runat="server" Width="98%"
                                CssClass="clsComboBox">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">
                            <asp:Label ID="Label1" runat="server" Width="92px" Height="18px">Tower<font class="clsNote">*</font></asp:Label>
												
                            <asp:CompareValidator ID="cmpflr" runat="server" ControlToValidate="ddlTower" Display="None"
                                ErrorMessage="Select Tower " ValueToCompare="--Select--" Operator="NotEqual">cmpflr</asp:CompareValidator></td>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="clsComboBox"
                                Width="98%" AutoPostBack="true">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">
                            <asp:Label ID="Label2" runat="server" Width="92px" Height="18px">Floor<font class="clsNote">*</font></asp:Label>
												
                            <asp:CompareValidator ID="cmptwr" runat="server" ControlToValidate="ddlFloor" Display="None"
                                ErrorMessage="Select Floor " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator></td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="clsComboBox"
                                Width="98%" AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    
                   
                    <tr runat="server" visible="false" >
                        <td align="left" style="height: 26px; width: 25%">From:<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr"
                                Display="None" ErrorMessage="Please Enter From Time " InitialValue="Hr">
                            </asp:RequiredFieldValidator>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimemin"
                                Display="None" ErrorMessage="Please Enter From Minutes" InitialValue="Min">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 25%">
                            <asp:DropDownList ID="starttimehr" runat="server" Width="60">
                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="starttimemin" runat="server" Width="60">
                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="height: 26px; width: 25%">To:<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr"
                                Display="None" ErrorMessage="Please Enter To Time " InitialValue="Hr">
                            </asp:RequiredFieldValidator>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimemin"
                                Display="None" ErrorMessage="Please Enter To Minutes" InitialValue="Min">
                            </asp:RequiredFieldValidator></td>
                        <td align="left" style="height: 26px; width: 25%">
                            <asp:DropDownList ID="endtimehr" runat="server" Width="60">
                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="endtimemin" runat="server" Width="60">
                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                  
                   
                    

                    <tr runat="server" >
                        <td align="left" style="height: 26px; width: 50%" colspan="2">
                            <asp:Label ID="Label3" runat="server" Width="92px" Height="18px">Conference<font class="clsNote">*</font></asp:Label>
												
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlConference"
                                Display="None" ErrorMessage="Please Select Conference Room" InitialValue="--Select--">
                            </asp:RequiredFieldValidator></td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlConference" TabIndex="5"  AutoPostBack="true" runat="server" CssClass="clsComboBox"
                                Width="98%">
                            </asp:DropDownList></td>
                    </tr>

                      <tr runat="server" id="emp">
                        <td align="left" style="height: 26px; width: 50%" colspan="2">
                            Enter Employee Code<font class="clsNote">*</font>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmployeeCode"
                                Display="None" ErrorMessage="Please Enter Employee ID ">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">
                            <asp:TextBox ID="txtEmployeeCode" runat="server" Width="75%"></asp:TextBox>
                        </td>

                        <td>&nbsp;</td>
                    </tr>

                    <tr runat="Server" id="empdetails" visible="false">
                        <td colspan="4">
                            <table cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                <tr>
                                    <td valign="top" align="left" class="bodytext">Employee Id
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblEmpSearchId" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">Employee Name
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblEmpName" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext">Email Id
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblEmpEmailId" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">Reporting To
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblReportingTo" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext" style="height: 20px">Designation
                                    </td>
                                    <td valign="top" align="left" style="height: 20px">
                                        <asp:Label ID="lblDesignation" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext" style="height: 20px">Department
                                    </td>
                                    <td valign="top" align="left" style="height: 20px">
                                        <asp:Label ID="lblDepartment" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext">Employee Extension
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblEmpExt" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">Contact Number
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblResNumber" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext">City
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblCity" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">State
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblState" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext">Project
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblPrjCode" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">Vertical
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblAUR_VERT_CODE" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext">Building
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblAUR_BDG_ID" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">Tower
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblEmpTower" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext">Floor
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblEmpFloor" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">Date of Joining
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:Label ID="lblDoj" runat="server" CssClass="bodytext"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" class="bodytext">Space
                                    </td>
                                    <td valign="top" align="left">
                                        <asp:HyperLink ID="hypSpaceId" runat="server" CssClass="BODYTEXT"></asp:HyperLink>
                                    </td>
                                    <td valign="top" align="left" class="bodytext">
                                        <asp:Label ID="lblSpaceAllocated" runat="server" CssClass="bodytext" Font-Bold="True"
                                            Font-Names="Arial" Font-Size="Medium" Font-Underline="True" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Center" colspan="4">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                   
                    <tr>
                        <td colspan="4" align="CENTER">
                            <asp:Button ID="btncheck" runat="server" Text="Submit" CssClass="button" />

                        </td>

                    </tr>
                  
                  

                      <tr>
                        <td colspan="4">
                            <asp.panel id="Asppanel1" runat="server" GroupingText="Mapped Assets">
                            <asp:GridView ID="gvassets" runat="server" AutoGenerateColumns="False" Width="100%" EmptyDatatext="No Assets Mapped">
                                <Columns>
                                 
                                     <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                     </ItemTemplate>
                                </asp:TemplateField>
                                    
                                     <asp:TemplateField HeaderText="Assets" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                        ItemStyle-Width="20">
                                        <ItemTemplate>
                                            <asp:Label ID="lblasset" runat="server" Text='<%# Eval("AAT_NAME")%>'></asp:Label>
                                            <asp:Label ID="lblassetcode" runat="server" Text='<%# Eval("AAT_CODE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      
                                    
                                        <asp:TemplateField HeaderText="Conference Room" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                        ItemStyle-Width="20">
                                        <ItemTemplate>
                                            <asp:Label ID="lblconfroom" runat="server" Text='<%# Eval("CONFERENCE_ROOM_NAME") %>'></asp:Label>
                                            <asp:Label ID="lblconfcode" runat="server" Text='<%# Eval("CONF_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Conference Room Type " ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                        ItemStyle-Width="20">
                                        <ItemTemplate>
                                            <asp:Label ID="lblconftype" runat="server" Text='<%# Eval("SPC_CR_TYPE_NAME") %>'></asp:Label>
                                           
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Capacity" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                        ItemStyle-Width="20">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcapacity" runat="server" Text='<%# Eval("CAPACITY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField HeaderText="Mapped Date" visible="false" ItemStyle-BackColor="#cccccc" HeaderStyle-Width="20"
                                        ItemStyle-Width="20">
                                        <ItemTemplate>
                                            <asp:Label ID="lblconfdate" runat="server" Text='<%# Eval("CONF_DATE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                  
                                </Columns>
                            </asp:GridView>
                                </asp.panel> 
                        </td>
                    </tr>

                      <tr>
                        <td colspan="4" align="center">
                          
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Release"  />

                        </td>
                    </tr>

                </table>

				 </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 16px; height: 100%;">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 16px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>

</asp:Content>

