Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web
Partial Class AssetManagement_SW_WebFiles_finalpage
    Inherits System.Web.UI.Page
    Dim rid As String = String.Empty
    Dim UID, reqid As String
    Dim FROMEmail, TOEmail, msg, subj, cc, mailFLAG, UID_NAME, location, loc_type, capacity, STE_CODE, STE_NAME As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        rid = clsSecurity.Decrypt(Request.QueryString("rid"))
        UID = Session("uid")
        reqid = clsSecurity.Decrypt(Request.QueryString("reqid"))
        lbl1.Text = lbl1.Text & "<body onLoad=""history.go(+1)"">"
        Dim strSta As String = clsSecurity.Decrypt(Request.QueryString("sta"))



        If strSta = "5" Then
            lbl1.Text = "<b><center><br><br> Requisition ID : </font>" & rid & "<br> has been requested.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "1111" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Space(s) has been submitted.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "1112" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Space(s) has been submitted for movement request.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "6" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Space(s) has been allocated.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "7" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Conference Room(s) has been booked successfully.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "9" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Conference Room(s) has been modified successfully.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "10" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br> has been cancelled successfully.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "6B" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Space(s) has been blocked.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "6A" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Space(s) has been allocated.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "8C" Then
            lbl1.Text = "<b><center><br><br>Requisition ID : </font>" & rid & " <br>Space(s) has been confirmed.<br><br>Thank you for using the system.</center>"
        ElseIf strSta = "4" Then
            lbl1.Text = "<b><center><br><br> </font> "
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse' ><tr> <th>S. No</th><th>Space ID</th></tr>"
            Dim dt As DataTable = CType(Session("ReleaseData"), DataTable)
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"

                'Dim param(0) As SqlParameter
                'param(0) = New SqlParameter("@SPC_ID", SqlDbType.VarChar, 50)
                'param(0).Value = dt.Rows(i)(1).ToString()

                'SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "VACANT_SEAT", param)


            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ReleaseData") = ""
            lbl1.Text = lbl1.Text & "<br> Has been Released"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "3" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            ' lbl1.Text = lbl1.Text &" Requisition ID : </font>")
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse'><tr> <th>S. No</th><th>Space ID</th></tr>"
            Dim dt As DataTable = CType(Session("ExtendData"), DataTable)
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"
            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ExtendData") = ""
            lbl1.Text = lbl1.Text & "<br> Has been send for Extension"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "11" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            ' lbl1.Text = lbl1.Text &" Requisition ID : </font>")
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse'><tr> <th>S. No</th><th>Project ID</th></tr>"
            Dim dt As DataTable = CType(Session("ExtendData"), DataTable)
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"
            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ExtendData") = ""
            lbl1.Text = lbl1.Text & "<br> Has been requested for Extension."
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "179" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been submitted"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "GRN" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> GRN has been generated"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "" Then
            write()

        ElseIf strSta = "178" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=small color=Black>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<br>Your request has been saved as draft.</font>"
            lbl1.Text = lbl1.Text & "<br>Thank You for using the system.</font></center>"

            'ElseIf strSta = "214" Then
            '   lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            ' lbl1.Text = lbl1.Text &" Requisition ID : </font>")
            '  lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            ' lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> has been Approved"
            'lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "214" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            ' lbl1.Text = lbl1.Text &" Requisition ID : </font>")
            lbl1.Text = lbl1.Text & rid
            lbl1.Text = lbl1.Text & "<br> Has been Approved"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "2" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been Submitted"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"

        ElseIf strSta = "8" Then
            'lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            'lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            'lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            'lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> has been Cancelled"
            'lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & rid
            lbl1.Text = lbl1.Text & "<br> Has been cancelled"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "182" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been approved"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "204" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been Finalized"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "186" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been approved"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "184" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been Approved"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "171" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br>PO has been generated."
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "185" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been Approved"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "192" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been Approved"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "205" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Payment Memo Generated"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "187" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been rejected"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "189" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been rejected"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "190" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been Rejected"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "191" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been rejected"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "193" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been rejected"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "212" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been sent to procurement"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "195" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been approved and request to collect the asset(s) from stores "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "194" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been approved"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "1001" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & rid
            lbl1.Text = lbl1.Text & "<br> Has been updated"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "147" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & rid
            lbl1.Text = lbl1.Text & "<br> Has been approved by level 1"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
            '		ElseIf strSta = "147" Then
            '           lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            '          lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            '         lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            '        lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> has been Approved by DH"
            '       lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "148" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "" & rid & " "
            lbl1.Text = lbl1.Text & "<br> Has been rejected by level 1"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "148-2" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been rejected by level 1 was failed"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "149" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been approved by level 1 and send for level 2 approval"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "163" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been apporved by level 2"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "164" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & " Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & rid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br> Has been rejetced by level 2 ! "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "167" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & "Vertical Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & reqid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br>Allocated spaces has been confirmed "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "170" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            lbl1.Text = lbl1.Text & "</font>"
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse' ><tr> <th>S. No</th><th>Space ID</th></tr>"
            Dim dt As DataTable = CType(Session("ReleaseData"), DataTable)
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"
            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ReleaseData") = ""
            lbl1.Text = lbl1.Text & reqid
            lbl1.Text = lbl1.Text & "<br>Space(s) Extended Successfully "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "172" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            lbl1.Text = lbl1.Text & "</font>"
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse' ><tr> <th>S. No</th><th>Project ID</th></tr>"
            Dim dt As DataTable = CType(Session("ReleaseData"), DataTable)
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"
            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ReleaseData") = ""
            lbl1.Text = lbl1.Text & reqid
            lbl1.Text = lbl1.Text & "<br>Space(s) Extended Successfully "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "202" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & "</font>"
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse' ><tr> <th>S. No</th><th>Space ID</th></tr>"
            Dim dt As DataTable = CType(Session("ReleaseData"), DataTable)
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"
            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ReleaseData") = ""
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & reqid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br>Space(s) Rejected Successfully "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        ElseIf strSta = "203" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & "</font>"
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse' ><tr> <th>S. No</th><th>Project ID</th></tr>"
            Dim dt As DataTable = CType(Session("ReleaseData"), DataTable)
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"
            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ReleaseData") = ""
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & reqid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br>Space(s) Rejected Successfully "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"

        ElseIf strSta = "199" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br>"
            lbl1.Text = lbl1.Text
            Dim dt As DataTable = CType(Session("ReleaseData"), DataTable)
            lbl1.Text = lbl1.Text & "<table border='1' cellpadding='0' cellspacing='2' style='border-collapse:collapse' ><tr> <th>S. No</th><th>Space ID</th></tr>"
            For i As Integer = 0 To dt.Rows.Count - 1
                lbl1.Text = lbl1.Text & "<tr><td>" & dt.Rows(i)(0).ToString() & "</td><td>" & dt.Rows(i)(1).ToString() & "</td></tr>"
                ' lbl1.Text = lbl1.Text &"<tr><td>" & dt.Rows(i)(1).ToString() & "</td></tr>")
            Next
            lbl1.Text = lbl1.Text & "</table>"
            Session("ReleaseData") = ""
            lbl1.Text = lbl1.Text & reqid
            lbl1.Text = lbl1.Text & "<br>Space Mapped To Employee Successfully"
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</center>"
        ElseIf strSta = "168" Then
            lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
            lbl1.Text = lbl1.Text & "Vertical Requisition ID : </font>"
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=medium color=MAROON>" & reqid & "</font> "
            lbl1.Text = lbl1.Text & "<font face=Zurich BT size=3 color=black><br>Allocated Spaces has been cancelled "
            lbl1.Text = lbl1.Text & ".<br><br>Thank You for using the system.</font></center>"
        End If

        lbl1.Text = lbl1.Text & "</body>"
    End Sub
    Private Sub write()
        lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
        lbl1.Text = lbl1.Text & " Requisition ID : </font>"
        lbl1.Text = lbl1.Text & "<font face=Zurich BT size=small color=Black>" & rid & "</font> "
        lbl1.Text = lbl1.Text & "<br>Your request has been submitted.</font>"
        lbl1.Text = lbl1.Text & "<br>Thank You for using the system.</font></center>"
    End Sub
    Private Sub write1()

        lbl1.Text = lbl1.Text & "<br>Thank You for using the system.</font></center>"
    End Sub
    Private Sub writeReject()
        lbl1.Text = lbl1.Text & "<b><center><br><br><font face=Zurich BT size=medium color=black>"
        lbl1.Text = lbl1.Text & "<br>You have completely Rejected the property request.</font>"
        lbl1.Text = lbl1.Text & "<br>Thank You for using the system.</font></center>"
    End Sub
End Class