Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports clsMasters
Imports Microsoft.Reporting.WebForms

Partial Class WorkSpace_SMS_Webfiles_repTWROccupancy
    Inherits System.Web.UI.Page

    Dim obj As New clsReports
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        lblMsg.Text = ""
        Label1.Text = Session("Child")

        If Not Page.IsPostBack Then

            obj.bindVertical(ddlverticals)
            lblSelParent.Text = Session("Parent")
            'RequiredFieldValidator2.ErrorMessage = "Please Select " + lblSelParent.Text
            lblSelChild1.Text = Session("Child")
            ddlverticals.Items(0).Text = "--All " & Session("Parent") & "(s)--"
            ddlTower.Items.Insert(0, "--All " & Session("Child") & "(s)--")
            ddlverticals.SelectedIndex = 0
            bindreport()
            lbltotal_util.ForeColor = Drawing.Color.Black
            lbltotal_util.Font.Bold = True
            'lbl1.Text = lbl1.Text + " " + Session("Parent")
        End If
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        Try
            If ddlverticals.SelectedIndex = 0 Then
                lbl1.Visible = False
                'Exit Sub
            End If

           
            bindreport()
           
        Catch ex As Exception

        End Try
    End Sub
    Public Sub bindreport()
        Dim rds As New ReportDataSource()
        rds.Name = "CostCenterUtilizationDS"

        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/CostCenterUtilizationReport.rdlc")

        'Setting Header Column value dynamically
        Dim p2 As New ReportParameter("dynamicParam2", Session("Parent").ToString())
        ReportViewer1.LocalReport.SetParameters(p2)

        Dim p1 As New ReportParameter("dynamicParam1", Session("Child").ToString())
        ReportViewer1.LocalReport.SetParameters(p1)



        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True


        Dim verticalid As String = ""
        Dim towerid As String = ""
        If ddlverticals.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            verticalid = ""
        Else
            verticalid = ddlverticals.SelectedValue
        End If

        If ddlTower.SelectedItem.Text = "--All " & Session("Child") & "(s)--" Then
            towerid = ""
        Else
            towerid = ddlTower.SelectedValue
        End If

        Dim param(3) As SqlParameter

        param(0) = New SqlParameter("@vc_Projname", SqlDbType.NVarChar, 200)
        param(0).Value = towerid
        param(1) = New SqlParameter("@vc_Vername", SqlDbType.NVarChar, 200)
        param(1).Value = verticalid
        param(2) = New SqlParameter("@SORTEXP", SqlDbType.NVarChar, 200)
        param(2).Value = "PROJECT"
        param(3) = New SqlParameter("@SORTDIR", SqlDbType.NVarChar, 200)
        param(3).Value = "asc"

        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("USP_GETTOTALWORKSTATIONS_PROJECT_REPORT", param)
        rds.Value = ds.Tables(0)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROJECT_UTI_COUNT")
        sp.Command.AddParameter("@VER_ID", verticalid, DbType.String)
        sp.Command.AddParameter("@PRJ_ID", towerid, DbType.String)
        ds = sp.GetDataSet()

        If ds.Tables(0).Rows.Count > 0 Then
            lbltotal_util.Text = ds.Tables(0).Rows(0).Item("util") & "%"
            tutility.Visible = True
        Else
            tutility.Visible = False
        End If
    End Sub
    Protected Sub ddlverticals_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlverticals.SelectedIndexChanged
        lbl1.Visible = False
        ReportViewer1.Visible = False
        tutility.Visible = False
        If ddlverticals.SelectedValue = "--All " & Session("Parent") & "(s)--" Then
            lbl1.Visible = False
            'Else
            '    lbl1.Visible = True
        End If


        Dim sp1 As New SqlParameter("@vc_VerticalName", SqlDbType.NVarChar, 50)
        If ddlverticals.SelectedIndex = 1 Then
            sp1.Value = ""
        Else
            sp1.Value = ddlverticals.SelectedValue
        End If
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_get_bu_Projects", sp1)
        BindCombo(ds, ddlTower, "PRJ_NAME", "PROJ_NAME")
        ddlTower.Items(0).Text = "--All " & Session("Child") & "(s)--"
        ddlTower.Items(0).Value = "1"
        ddlTower.SelectedIndex = 0
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        ReportViewer1.Visible = False
    End Sub
End Class