<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConferenceBookingFacilitiesView.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_ConferenceBookingFacilitiesView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conference Booking Facilities</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Conference Booking Facilities
                        </asp:Label>
                        <hr align="center" width="60%" />
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Conference Booking Facilities</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        <fieldset visible="false" id="confdetails" runat="server">
                            <legend>Conference Details </legend>
                            <table id="Table1" class="tbl" cellspacing="0" cellpadding="2" width="80%" border="0">
                                <tr>
                                    <td class="clsTbl" align="left">
                                        <b>Requested ID: </b>
                                    </td>
                                    <td class="clsTbl" align="left">
                                        <asp:Label ID="lblReqId" runat="server"></asp:Label>
                                    </td>
                                    <td class="clsTbl" align="left">
                                        <b>Conference Room: </b>
                                    </td>
                                    <td class="clsTbl" align="left">
                                        <asp:Label ID="lblConferenceRoom" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="clsTbl" align="left">
                                        <b>Booked By: </b>
                                    </td>
                                    <td class="clsTbl" align="left">
                                        <asp:Label ID="lblAurKnownAs" runat="server"></asp:Label>
                                    </td>
                                    <td class="clsTbl" align="left">
                                        <b>Booked Time Slot: </b>
                                    </td>
                                    <td class="clsTbl" align="left">
                                        <asp:Label ID="lblBDateTime" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <asp:Panel ID="pnlTop" runat="server" Width="95%">
                            <table id="tblmain1" class="tbl" cellspacing="0" cellpadding="0" width="100%" align="center">
                                <tr>
                                    <td class="clsTbl" width="40%">
                                        &nbsp;Select Participants And Facilities</td>
                                </tr>
                            </table>
                            <table id="tblmain" class="tbl" cellspacing="0" cellpadding="0" width="100%" align="center">
                                <tr>
                                    <td width="100%" align="center">
                                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center">
                                        <asp:Panel ID="pnlmain" Width="100%" runat="server">
                                            <br>
                                            <table border="1" width="100%">
                                                <tr>
                                                    <td class="label" width="75%" align="right">
                                                        Participants List:</td>
                                                    <td width="25%" align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" colspan="2">
                                                        <asp:DataGrid ID="dgParticipants" Width="100%" runat="server" PageSize="5" AllowPaging="True"
                                                            AutoGenerateColumns="False">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="SNo" Visible="false">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Label ID="Label2" runat="server" Text="<%# Container.ItemIndex+1 %>" Height="11px">
                                                                                    </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Name" HeaderText="Name">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="depname" HeaderText="Department">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Remarks" HeaderText="Remarks" Visible="false">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                            <PagerStyle VerticalAlign="Bottom" NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right">
                                                            </PagerStyle>
                                                        </asp:DataGrid></td>
                                                </tr>
                                            </table>
                                            <br />
                                            <table border="1" width="100%">
                                                <tr>
                                                    <td class="label" width="75%" align="right">
                                                        Visitors&nbsp;:</td>
                                                    <td align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" colspan="2">
                                                        <asp:DataGrid ID="dgVisitors" Width="100%" runat="server" PageSize="5" AllowPaging="True"
                                                            AutoGenerateColumns="False" Visible="False">
                                                            <HeaderStyle CssClass="label"></HeaderStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn Visible="False" HeaderText="SNo">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                    <ItemTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Label ID="Label6" runat="server" Text="<%# Container.ItemIndex+1 %>" Height="11px">
                                                                                    </asp:Label></td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="VIS_NAME" HeaderText="Name">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="VIS_DOMAIN" HeaderText="Domain">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="VIS_GROUP" HeaderText="Group">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="VIS_RELATION" HeaderText="Relation">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="VIS_REMARKS" HeaderText="Remarks">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                               
                                                            </Columns>
                                                            <PagerStyle VerticalAlign="Bottom" NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right">
                                                            </PagerStyle>
                                                        </asp:DataGrid></td>
                                                </tr>
                                            </table>
                                            <br>
                                            <table border="1" width="100%">
                                                <tr>
                                                    <td class="label" width="75%" align="right">
                                                        Asset Consumables &nbsp;:</td>
                                                    <td align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" colspan="2">
                                                        <asp:DataGrid ID="dgConSumableAssets" Width="100%" runat="server" PageSize="5" AllowPaging="True"
                                                            AutoGenerateColumns="False" Visible="False">
                                                            <HeaderStyle CssClass="label"></HeaderStyle>
                                                            <Columns>
                                                                <asp:BoundColumn DataField="SNO" HeaderText="S No" Visible="false">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="name" HeaderText="Name">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="qty" HeaderText="Quantity">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="remark" HeaderText="Remark">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="reqtype" HeaderText="RequestType">
                                                                    <HeaderStyle CssClass="label"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                
                                                            </Columns>
                                                            <PagerStyle VerticalAlign="Bottom" NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right">
                                                            </PagerStyle>
                                                        </asp:DataGrid></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                        height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                            width="9" /></td>
                    <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                            width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
