﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL

Partial Class WorkSpace_SMS_Webfiles_Conference_Withhold
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        txtToDate.Attributes.Add("readonly", "readonly")
        txtFrmDate.Attributes.Add("readonly", "readonly")
        lblMsg.Text = String.Empty
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            LoadCity()
            LoadLocations()
        End If
    End Sub

    Public Sub LoadCity()
        Dim param As SqlParameter() = New SqlParameter(0) {}
        param(0) = New SqlParameter("@USER_ID", SqlDbType.VarChar)
        param(0).Value = Session("uid")
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE", param)
    End Sub

    Public Sub LoadLocations()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@USER_ID", SqlDbType.VarChar)
        param(1).Value = Session("uid")
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub LoadTowers()
        If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
            ' objMsater.BindTowerLoc(ddlTower, ddlSelectLocation.SelectedItem.Value)
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCity.SelectedItem.Value
            param(1) = New SqlParameter("@usr_id", SqlDbType.VarChar)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@VC_LOC", SqlDbType.NVarChar, 200)
            param(2).Value = ddlSelectLocation.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)
        End If
    End Sub

    Public Sub LoadFloors()
        If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
            Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue.ToString().Trim())
            ddlFloor.DataSource = verDTO
            ddlFloor.DataTextField = "Floorname"
            ddlFloor.DataValueField = "Floorcode"
            ddlFloor.DataBind()
            ddlFloor.Items.Insert(0, "--Select--")
        End If
    End Sub

    Public Sub LoadCapacities()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged

        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCapacity.Items.Clear()
        ddlConf.Items.Clear()
        LoadLocations()

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        Dim ds1 As DataSet
        ds1 = sp1.GetDataSet
        ddlCapacity.DataSource = sp1.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = sp.GetDataSet
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
        'txtFdate.Text = String.Empty





    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSelectLocation.SelectedIndexChanged
        Try
            'usp_getActiveTower_LOC
            ddlTower.Items.Clear()
            ddlFloor.Items.Clear()
            ddlCapacity.Items.Clear()
            ddlConf.Items.Clear()

            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITYBYLOCATION")
            sp3.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            Dim ds3 As DataSet
            ds3 = sp3.GetDataSet
            ddlCity.Items.Clear()
            LoadCity()
            ddlCity.Items.FindByValue(ds3.Tables(0).Rows(0).Item("LCM_CTY_ID")).Selected = True

            LoadTowers()

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
            sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
            Dim ds1 As DataSet
            ds1 = sp1.GetDataSet
            ddlCapacity.DataSource = sp1.GetDataSet
            ddlCapacity.DataTextField = "capacity"
            ddlCapacity.DataValueField = "capacity"
            ddlCapacity.DataBind()
            ddlCapacity.Items.Insert(0, "--Select--")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)

            'ddlConf.Items.Clear()
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            ddlFloor.Items.Clear()
            ddlCapacity.Items.Clear()
            ddlConf.Items.Clear()
            LoadFloors()


            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
            sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

            Dim ds1 As DataSet
            ds1 = sp1.GetDataSet
            ddlCapacity.DataSource = sp1.GetDataSet
            ddlCapacity.DataTextField = "capacity"
            ddlCapacity.DataValueField = "capacity"
            ddlCapacity.DataBind()
            ddlCapacity.Items.Insert(0, "--Select--")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp1.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        Dim ds1 As DataSet
        ds1 = sp1.GetDataSet
        ddlCapacity.DataSource = sp1.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = sp.GetDataSet
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlCapacity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCapacity.SelectedIndexChanged
        'GET_SPACE_CONFERENCE

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        If ddlCapacity.SelectedItem.Value <> "--Select--" Then
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
            'txtFdate.Text = String.Empty
        Else
            ddlConf.Items.Clear()
        End If

    End Sub

    Protected Sub ddlConf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlConf.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SP_GET_CONF_DETAILS")
        sp.Command.AddParameter("@CONF_CODE", ddlConf.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCapacity.Items.Clear()
        LoadLocations()
        ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_LOCATION")).Selected = True
        LoadTowers()
        ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_TOWER")).Selected = True
        LoadFloors()
        ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_FLOOR")).Selected = True
        LoadCapacities()
        ddlCapacity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFERENCE_CAPACITY")).Selected = True
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If (txtFrmDate.Text >= getoffsetdate(Date.Today)) Then
            If txtToDate.Text >= txtFrmDate.Text Then
                Dim cntWst As Integer = 0
                Dim cntHCB As Integer = 0
                Dim cntFCB As Integer = 0
                Dim twr As String = ""
                Dim flr As String = ""
                Dim wng As String = ""
                Dim intCount As Int16 = 0
                Dim ftime As String = ""
                Dim ttime As String = ""
                Dim StartHr As String = "00"
                Dim EndHr As String = "00"
                Dim StartMM As String = "00"
                Dim EndMM As String = "00"

                If starttimehr.SelectedItem.Value <> "HH" Then
                    StartHr = starttimehr.SelectedItem.Text
                End If

                If endtimehr.SelectedItem.Value <> "HH" Then
                    If endtimehr.SelectedItem.Value = "24" Then
                        EndHr = "00"
                    Else
                        EndHr = endtimehr.SelectedItem.Text
                    End If
                End If

                ftime = StartHr + ":" + StartMM
                ttime = EndHr + ":" + EndMM



                If endtimehr.SelectedItem.Value <= starttimehr.SelectedItem.Value Then
                    lblMsg.Text = "To Time should be greater than from Time"
                    'btnsubmit.Visible = False
                    Exit Sub
                End If

                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONFERENCE_BOOKED_SLOTS")
                sp.Command.AddParameter("@REQ_ID", "", DbType.String)
                sp.Command.AddParameter("@SPC_ID", ddlConf.SelectedValue, DbType.String)
                sp.Command.AddParameter("@FROM_DATE", txtFrmDate.Text, DbType.Date)
                sp.Command.AddParameter("@TO_DATE", txtFrmDate.Text, DbType.Date)
                sp.Command.AddParameter("@FROM_TIME", starttimehr.SelectedValue, DbType.String)
                sp.Command.AddParameter("@TO_TIME", endtimehr.SelectedValue, DbType.String)


                Dim ds As New DataSet
                ds = sp.GetDataSet()

                If (ds.Tables(0).Rows.Count = 0) Then
                    SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 6)
                    lblMsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                    cleardata()
                    strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("6") & "&rid=" & clsSecurity.Encrypt(REQID)
                Else

                    lblMsg.Visible = True
                    lblMsg.Text = "Please choose an alternative time slot since the selected period is currently booked."
                    Exit Sub

                End If
GVColor:

                If strRedirect <> String.Empty Then
                    Response.Redirect(strRedirect, False)
                End If
            Else
                lblMsg.Text = "'To Date' must be greater than 'Today's date'"
            End If
        Else
            lblMsg.Text = "'From Date' must be greater than 'Today's date'"
        End If

    End Sub

    Private Sub cleardata()
        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlConf.Items.Clear()
        btnsubmit.Visible = False
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)

        REQID = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "SSA_ID", Session("TENANT") & ".", "CONFERENCE_BOOKING")
        Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "24"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If

        If endtimehr.SelectedItem.Value <> "Hr" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "23"
                EndMM = "59"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If

        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM

        If CDate(txtFrmDate.Text).Month < getoffsetdatetime(DateTime.Now).Month Then ' And CDate(txtToDate.Text).Year < getoffsetdatetime(DateTime.Now).Year 
            lblMsg.Text = "You can't request for the past month"
            Exit Sub

        End If

        Dim sta As Integer = 6

        RIDDS = RIDGENARATION("VerticalReq")

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubSonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblMsg.Text = "A request has already been made "
            Exit Sub
        Else
            cntWst = 1
            Dim cnt As Int32 = 0
            ' For Each row As GridViewRow In gdavail.Rows
            Dim lblspcid As String   ' DirectCast(row.FindControl("lblspcid"), Label)
            lblspcid = ddlConf.SelectedItem.Value
            'Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblMsg.Text = ""
            verticalreqid = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "ssa_id", Session("TENANT") & ".", "SMS_space_allocation")

            Dim param3(15) As SqlParameter
            param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param3(0).Value = REQID
            param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
            param3(1).Value = verticalreqid
            param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param3(2).Value = strVerticalCode

            param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
            param3(3).Value = lblspcid
            param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
            param3(4).Value = strAurId
            param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param3(5).Value = txtFrmDate.Text
            param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param3(6).Value = txtToDate.Text
            param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param3(7).Value = "2" ' GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param3(8).Value = ftime
            param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param3(9).Value = ttime
            param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param3(10).Value = sta
            param3(11) = New SqlParameter("@COMPANYID", SqlDbType.VarChar, 10)
            param3(11).Value = Session("COMPANYID")
            param3(12) = New SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000)
            param3(12).Value = ""
            param3(13) = New SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000)
            param3(13).Value = ""
            param3(14) = New SqlParameter("@DESC", SqlDbType.NVarChar, 200)
            param3(14).Value = ""
            param3(15) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
            param3(15).Value = Session("Uid").ToString()
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART3", param3)
            '-------------------SEND MAIL-----------------

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_CONF_BOOKING_WITHHOLD")
            sp.Command.AddParameter("@REQID", REQID, DbType.String)
            sp.ExecuteScalar()
        End If
    End Sub


    Protected Sub txtFrmDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtFrmDate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        starttimehr.DataSource = ds
        starttimehr.DataTextField = "TIMINGS"
        starttimehr.DataValueField = "TIMINGS"
        starttimehr.DataBind()
        starttimehr.Items.Insert(0, "HH")
        endtimehr.DataSource = ds
        endtimehr.DataTextField = "TIMINGS"
        endtimehr.DataValueField = "TIMINGS"
        endtimehr.DataBind()
        endtimehr.Items.Insert(0, "HH")
    End Sub

    Protected Sub txtToDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtToDate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        endtimehr.DataSource = ds
        endtimehr.DataTextField = "TIMINGS"
        endtimehr.DataValueField = "TIMINGS"
        endtimehr.DataBind()
        endtimehr.Items.Insert(0, "HH")
    End Sub


    Public Sub BindConference()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)

        If ddlCapacity.SelectedItem.Value <> "--Select--" Then
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
        Else
            ddlConf.Items.Clear()
        End If
    End Sub

End Class
