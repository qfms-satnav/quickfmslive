Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Data.SqlClient
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports System.Net
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL

Partial Class WorkSpace_SMS_Webfiles_ViewAllocatedSeats
    Inherits System.Web.UI.Page
    Dim objclsViewCancelChange As clsViewCancelChange
    Dim verBll As New VerticalBLL()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim obj As clsMasters
    Dim REQID As String
    Dim strStatus As String
    Dim RIDDS As String
    Dim dtVerReqDetails As DataTable
    Dim strRedirect As String = String.Empty
    Dim twrcode As String
    Dim flrcode As String
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        lblMsg.Text = ""
        If Not Page.IsPostBack Then
            Try
                BindCity()

            Catch ex As Exception

            End Try
        End If
    End Sub
    Public Sub BindCity()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@MODE", SqlDbType.NVarChar, 200)
        param(1).Value = 2
        ObjSubSonic.Binddropdown(ddlCity, "GET_CITY_ADM", "CTY_NAME", "CTY_CODE", param)
    End Sub
    
    Public Sub Bindlocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION_bycity", "LCM_NAME", "LCM_CODE", param)
        
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If ddlCity.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Please select city."
            Exit Sub
        End If
        If ddlLocation.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Please select Location."
            Exit Sub
        End If
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlLocation.SelectedItem.Value
        param(1) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(1).Value = ddlCity.SelectedItem.Value
        ObjSubSonic.BindGridView(gvitemsSameLocation, "GETALLOCATEDCOUNT", param)
        gvitemsSameLocation.Visible = True
        gvSpaceDetails.Visible = False
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        ddlLocation.Items.Clear()
        Bindlocation()
        If ddlLocation.Items.Count = 0 Then
            lblMsg.Text = "No Locations Available"
        End If

        gvitemsSameLocation.Visible = False


    End Sub

    Protected Sub gvitemsSameLocation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitemsSameLocation.RowCommand
        If e.CommandName = "ViewAllocated" Then
            trSpaceDetails.Visible = True

            Dim index As Integer = Convert.ToInt32(e.CommandArgument) - 1
            'Dim row As GridViewRow = gvitemsSameLocation.Rows(index)

            Dim lblTwrCode As Label = CType(gvitemsSameLocation.Rows(index).FindControl("lblTwrCode"), Label)
            Dim lblFlr_code As Label = CType(gvitemsSameLocation.Rows(index).FindControl("lblFlr_code"), Label)

 
            getspacedetails(lblTwrCode.Text, lblFlr_code.Text)
            Session("VerAllocTwrcode") = lblTwrCode.Text
            Session("VerAllocFlrcode") = lblFlr_code.Text
            'GET_SPACEDETAILSBYTWRFLR
        Else
            trSpaceDetails.Visible = False
        End If
    End Sub
    Public Sub getspacedetails(lblTwrCode As String, lblFlr_code As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = lblTwrCode
        param(1) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = lblFlr_code
        ObjSubSonic.BindGridView(gvSpaceDetails, "GET_SPACEDETAILSBYTWRFLR", param)
        gvSpaceDetails.Visible = True
    End Sub

    Protected Sub gvSpaceDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvSpaceDetails.PageIndexChanging
        gvSpaceDetails.PageIndex = e.NewPageIndex
        getspacedetails(Session("VerAllocTwrcode"), Session("VerAllocFlrcode"))
    End Sub

   
    Protected Sub gvSpaceDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSpaceDetails.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Text = Session("Parent")
        End If
      
    End Sub
End Class
