<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmLocationHistory.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmLocationHistory"
    Title="Locations History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <script defer language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

    <script defer language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>

    <div>
        <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Location Report<hr
                        align="center" width="60%" /> </asp:Label>
                </td>
            </tr>
            <tr>
                <td width="100%" align="center">
                </td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="85%" Height="100%">
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10px">
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp; Location Report </strong>
                    </td>
                    <td style="width: 17px">
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="tab" runat="server" cellpadding="2" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Date(MM/DD/YYYY)
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox ID="txtFdate" runat="server" CssClass="clsTextField" Width="97%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Select City
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:DropDownList ID="ddlcity" runat="server" CssClass="clsComboBox" Width="97%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnSubmit" runat="Server" CssClass="button" Text="Submit" />
                                </td>
                            </tr>
                        </table>
                        <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export to Excel" />
                        <asp:Panel ID="pnlgrid" runat="Server" Width="100%">
                            <asp:GridView ID="gvitems" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" Width="100%">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblloc" runat="server" Text='<%#Eval("LOCATION_CODE") %>'></asp:Label>
                                           
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="LOCATION_NAME" HeaderText="LOCATION"  />
                                    <asp:BoundField DataField="AVAILABLE_WT" HeaderText="Available WT"  />
                                    <asp:BoundField DataField="OCCUPIED_WT" HeaderText="Occupied WT"  />
                                    <asp:BoundField DataField="AVAILABLE_WI" HeaderText="Available WI"  />
                                    <asp:BoundField DataField="OCCUPIED_WI" HeaderText="Occupied WI"  />
                                    <asp:BoundField DataField="AVAILABLE_WBPO" HeaderText="Available WBPO"  />
                                    <asp:BoundField DataField="OCCUPIED_WBPO" HeaderText="Occupied WBPO"  />
                                    <asp:BoundField DataField="AVAILABLE_WSTL" HeaderText="Available WSTL"  />
                                    <asp:BoundField DataField="OCCUPIED_WSTL" HeaderText="Occupied WSTL"  />
                                    <asp:BoundField DataField="MODIFIED_DATE" HeaderText="Modified Date"  />
                                        <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
