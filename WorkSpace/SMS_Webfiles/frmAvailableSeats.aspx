<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAvailableSeats.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmAvailableSeats" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Available Seats
                        </asp:Label>
                        <hr align="center" width="60%" />
                    </td>
                </tr>
                <tr>
                    <td align="right" width="50%">
                        <asp:Button ID="btn3finish" runat="server" Text="Close" CssClass="button" TabIndex="96"
                            OnClientClick="parent.hidePopWin(true);" CausesValidation="false" /><br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Available Seats</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td style="width: 238px" class="clslabel" align="center" colspan="2">
                        <asp:Label ID="lblmsg" CssClass="error" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="width: 238px" class="clslabel" align="center" colspan="2">
                                    <asp:GridView ID="gvVerticalReport" CssClass="accordionContent" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" AllowPaging="true" PageSize="10" ForeColor="#333333" EmptyDataText="No Records Found."
                                        Width="100%" GridLines="BOTH">
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#CCCCCC" ForeColor="Red" />
                                        <Columns>
                                            <asp:BoundField DataField="rn" HeaderText="Sr. No." />
                                            <asp:BoundField DataField="LCM_NAME" HeaderText="Location" />
                                            <asp:BoundField DataField="TWR_NAME" HeaderText="Tower" />
                                            <asp:BoundField DataField="FLR_NAME" HeaderText="Floor" />
                                            <asp:TemplateField HeaderText="WORKSTATION (WST) ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblWST" runat="server" Text='<%# Eval("WST").ToString()%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="HALF CABIN (HCB) ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblHCB" runat="server" Text='<%# Eval("HCB").ToString()%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FULL CABIN (FCB) ">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFCB" runat="server" Text='<%# Eval("FCB").ToString()%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left"
                                            VerticalAlign="Top" />
                                        <EditRowStyle BackColor="#999999" />
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                        height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                            width="9" /></td>
                    <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                            width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
