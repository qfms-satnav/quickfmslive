﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Drawing
Imports System.Text


Partial Class WorkSpace_SMS_Webfiles_frmSeatTypeInformationReport
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim objmaster As New clsMasters

    Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        'This method will Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = String.Empty
        If Not IsPostBack Then
            'BindBEntity()
            BindCities()
            BindSeatTypes()
            ddlCity.Items(0).Text = "--All--"
            ddlLocation.Items.Insert(0, "--All--")
            BindVerticalLocation()
            btnExport.Visible = True
            ' gvVerticalReport.Columns(0).HeaderText = "Last Name"
        End If
    End Sub

    'Public Sub BindBEntity()
    '    objmaster.BindVertical(ddlVertical)
    '    ddlVertical.Items.Insert(1, "--All--")
    'End Sub

    Public Sub BindCities()
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindVerticalLocation()
        gvVerticalReport.Visible = True
    End Sub

    Private Sub BindVerticalLocation()


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_REPORT_DUP_SEATTYPE_AREA_ALL")
        'sp.Command.AddParameter("@VERT_CODE", ddlVertical.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Text, Data.DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@SEATTYPE", ddlseattype.SelectedValue, Data.DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        'gvVerticalReport.DataSource = ds
        'gvVerticalReport.DataBind()
        'btnExport.Visible = True
        If ds.Tables(0).Rows.Count > 0 Then
            gvVerticalReport.DataSource = ds
            gvVerticalReport.DataBind()
            btnExport.Visible = True
        Else
            gvVerticalReport.DataSource = Nothing
            gvVerticalReport.DataBind()
            btnExport.Visible = False
        End If


    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'Dim Mbdg As String = ""
        'Dim Mcity As String = ""
        'Dim Mseat As String = ""

        'If ddlCity.SelectedValue = "--All--" Then
        '    Mcity = ""
        'Else
        '    Mcity = ddlCity.SelectedValue
        'End If

        'If ddlLocation.SelectedValue = "--All--" Then
        '    Mbdg = ""
        'Else
        '    Mbdg = ddlLocation.SelectedValue
        'End If

        'If ddlseattype.SelectedValue = "--All--" Then
        '    Mseat = ""
        'Else
        '    Mseat = ddlseattype.SelectedValue
        'End If



        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_REPORT_DUP_SEATTYPE_AREA_ALL")
        ''sp.Command.AddParameter("@VERT_CODE", ddlVertical.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@CTY_CODE", Mcity, Data.DbType.String)
        'sp.Command.AddParameter("@LCM_CODE", Mbdg, Data.DbType.String)
        'sp.Command.AddParameter("@SEATTYPE", Mseat, Data.DbType.String)
        'Dim ds As New DataSet()
        'ds = sp.GetDataSet()
        'gvVerticalReport.DataSource = ds
        'gvVerticalReport.DataBind()
        ''Export("SeatTypeInfoReport" & DateTime.Now.ToString("yyyyMMddHHmmss") & ".xls", gvVerticalReport, ddlseattype.SelectedValue)

        ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        ExportPanel1.FileName = "SeatTypeInfoReport" & DateTime.Now.ToString("yyyyMMddHHmmss") & ""

    End Sub

    Public Function GridViewToHtml(grid As GridView) As String
        Dim sTringBuilder As New StringBuilder()
        Dim sTringWriter As New StringWriter(sTringBuilder)
        Dim hTmlTextWriter As New HtmlTextWriter(sTringWriter)
        grid.RenderControl(hTmlTextWriter)
        Return sTringBuilder.ToString()
    End Function

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView, seattype As String)

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create WS form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table

        If seattype = "--All--" Then

            Dim title As New TableRow()
            ' title.BackColor = Color.
            Dim titlecell As New TableCell()
            titlecell.ColumnSpan = 4
            Dim titlecell1 As New TableCell()
            titlecell1.ColumnSpan = 3
            Dim lbl As New Label()
            lbl.Text = "General Information"
            Dim lbl1 As New Label()
            lbl1.Text = "Per Unit Area"
            titlecell.Controls.Add(lbl)
            titlecell.HorizontalAlign = HorizontalAlign.Center

            titlecell1.Controls.Add(lbl1)
            title.Cells.Add(titlecell)
            title.Cells.Add(titlecell1)


            Dim titlecel2 As New TableCell()
            titlecel2.ColumnSpan = 4
            Dim titlecell3 As New TableCell()
            titlecell3.ColumnSpan = 4
            Dim lbl2 As New Label()
            lbl2.Text = "Seat Type WS Work Station"
            Dim lbl3 As New Label()
            lbl3.Text = "Seat Type WS Work Station Total Area"
            titlecel2.Controls.Add(lbl2)
            titlecell3.Controls.Add(lbl3)
            title.Cells.Add(titlecel2)
            title.Cells.Add(titlecell3)

            Dim titlecel4 As New TableCell()
            titlecel4.ColumnSpan = 4
            Dim titlecell5 As New TableCell()
            titlecell5.ColumnSpan = 4
            'titlecell3.BackColor = Color.Cyan
            'Should span across all columns
            Dim lbl4 As New Label()
            lbl4.Text = "Seat Type HC - Half Cabin"
            Dim lbl5 As New Label()
            lbl5.Text = "Seat Type HC - Half Cabin Total Area"
            titlecel4.Controls.Add(lbl4)
            titlecell5.Controls.Add(lbl5)
            title.Cells.Add(titlecel4)
            title.Cells.Add(titlecell5)


            Dim titlecel6 As New TableCell()
            titlecel6.ColumnSpan = 4
            Dim titlecel7 As New TableCell()
            titlecel7.ColumnSpan = 4
            Dim lbl6 As New Label()
            lbl6.Text = "Seat Type FC - Full Cabin"
            Dim lbl7 As New Label()
            lbl7.Text = "Seat Type FC - Full Cabin Total Area"
            titlecel6.Controls.Add(lbl6)
            titlecel7.Controls.Add(lbl7)
            title.Cells.Add(titlecel6)
            title.Cells.Add(titlecel7)

            Dim titlecel8 As New TableCell()
            titlecel8.ColumnSpan = 4
            Dim titlecel9 As New TableCell()
            titlecel9.ColumnSpan = 4
            Dim lbl8 As New Label()
            lbl8.Text = "Seat Type H Linear Seat"
            Dim lbl9 As New Label()
            lbl9.Text = "Seat Type H Linear Seat Total Area"
            titlecel8.Controls.Add(lbl8)
            titlecel9.Controls.Add(lbl9)
            title.Cells.Add(titlecel8)
            title.Cells.Add(titlecel9)

            Dim titlecel10 As New TableCell()
            titlecel10.ColumnSpan = 4
            Dim titlecel11 As New TableCell()
            titlecel11.ColumnSpan = 4
            Dim lbl10 As New Label()
            lbl10.Text = "Total Seat Summary"
            Dim lbl11 As New Label()
            lbl11.Text = "Total Seat Type Area"
            titlecel10.Controls.Add(lbl10)
            titlecel11.Controls.Add(lbl11)
            title.Cells.Add(titlecel10)
            title.Cells.Add(titlecel11)

            table.Rows.Add(title)

            'Start Align center for all columns
            titlecell.HorizontalAlign = HorizontalAlign.Center
            titlecell1.HorizontalAlign = HorizontalAlign.Center
            titlecel2.HorizontalAlign = HorizontalAlign.Center
            titlecell3.HorizontalAlign = HorizontalAlign.Center
            titlecel4.HorizontalAlign = HorizontalAlign.Center
            titlecell5.HorizontalAlign = HorizontalAlign.Center
            titlecel6.HorizontalAlign = HorizontalAlign.Center
            titlecel7.HorizontalAlign = HorizontalAlign.Center
            titlecel8.HorizontalAlign = HorizontalAlign.Center
            titlecel9.HorizontalAlign = HorizontalAlign.Center
            titlecel10.HorizontalAlign = HorizontalAlign.Center
            titlecel11.HorizontalAlign = HorizontalAlign.Center
            'End  Align center for all columns

        End If
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If





        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    'Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

   

    Public Sub BindLocation(ByVal strCity As String)
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)

    End Sub

    Public Sub BindSeatTypes()
        'Dim sp As New SubSonic.StoredProcedure("GET_SEAT_TYPE")
        'Dim ds As New DataSet()
        'ds = sp.GetDataSet()
        'ddlSeatType.DataSource = ds
        'ddlSeatType.DataTextField = "SEAT_CODE"
        'ddlSeatType.DataValueField = "SEAT_CODE"
        'ddlSeatType.DataBind()
        'ddlSeatType.Items.Insert(0, "--Select--")
        ObjSubSonic.Binddropdown(ddlseattype, "GET_SEAT_TYPE", "SEAT_NAME", "SEAT_CODE")
        ddlseattype.Items.Insert(0, "--All--")
        ddlseattype.Items.RemoveAt(1)
    End Sub

    Protected Sub gvVerticalReport_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvVerticalReport.RowCreated
        If ddlSeatType.SelectedItem.Text = "--All--" Then
            Dim gvRow__1 As GridView = DirectCast(sender, GridView)
            ' Dim  As GridViewRow = ;

            If e.Row.RowType = DataControlRowType.Header Then
                'gvrow_10 means cell numbers(cell0-cell9=10)
                Dim gvrow__10 As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim cell0 As New TableCell()
                cell0.Text = "General Information"
                'lbl1.Text = "General Information"
                cell0.ForeColor = Drawing.Color.Black
                'cell0.BackColor = Drawing.Color.Blue
                cell0.HorizontalAlign = HorizontalAlign.Center
                cell0.ColumnSpan = 4
                Dim cell1 As New TableCell()
                cell1.Text = "Per Unit Area"
                'lbl2.Text = "Per Unit Area"
                cell1.ForeColor = Drawing.Color.Black
                cell1.HorizontalAlign = HorizontalAlign.Center
                cell1.ColumnSpan = 3
                Dim cell2 As New TableCell()
                cell2.Text = "Seat Type WS - Work Station"
                'lbl3.Text = "Seat Type WS Work Station"
                cell2.ForeColor = Drawing.Color.Black
                'cell2.BackColor = Drawing.Color.LightGoldenrodYellow
                cell2.HorizontalAlign = HorizontalAlign.Center
                cell2.ColumnSpan = 4
                Dim cell3 As New TableCell()
                cell3.Text = "Seat Type WS - Work Station Total Area"
                ' lbl4.Text = "Seat Type WS Work Station Total Area"
                cell3.ForeColor = Drawing.Color.Black
                'cell3.BackColor = Drawing.Color.LightGoldenrodYellow
                cell3.HorizontalAlign = HorizontalAlign.Center
                cell3.ColumnSpan = 4
                Dim cell4 As New TableCell()
                cell4.Text = "Seat Type HC - Half Cabin "
                'lbl5.Text = "Seat Type HC - Half Cabin "
                cell4.ForeColor = Drawing.Color.Black
                'cell4.BackColor = Drawing.Color.DarkSeaGreen
                cell4.HorizontalAlign = HorizontalAlign.Center
                cell4.ColumnSpan = 4
                Dim cell5 As New TableCell()
                cell5.Text = "Seat Type HC - Half Cabin Total Area"
                'lbl6.Text = "Seat Type HC - Half Cabin Total Area"
                cell5.ForeColor = Drawing.Color.Black
                'cell5.BackColor = Drawing.Color.DarkSeaGreen
                cell5.HorizontalAlign = HorizontalAlign.Center
                cell5.ColumnSpan = 4
                Dim cell6 As New TableCell()
                cell6.Text = "Seat Type FC - Full Cabin "
                'lbl7.Text = "Seat Type FC - Full Cabin "
                cell6.ForeColor = Drawing.Color.Black
                'cell6.BackColor = Drawing.Color.LightBlue
                cell6.HorizontalAlign = HorizontalAlign.Center
                cell6.ColumnSpan = 4
                Dim cell7 As New TableCell()
                cell7.Text = "Seat Type FC - Full Cabin Total Area"
                'lbl8.Text = "Seat Type FC - Full Cabin Total Area"
                cell7.ForeColor = Drawing.Color.Black
                'cell7.BackColor = Drawing.Color.LightBlue
                cell7.HorizontalAlign = HorizontalAlign.Center
                cell7.ColumnSpan = 4

                'Dim cell8 As New TableCell()
                'cell8.Text = "Seat Type H Linear Seat"
                ''lbl9.Text = "Total Seat Summary"
                'cell8.ForeColor = Drawing.Color.Black
                ''cell8.BackColor = Drawing.Color.AntiqueWhite
                'cell8.HorizontalAlign = HorizontalAlign.Center
                'cell8.ColumnSpan = 4
                'Dim cell9 As New TableCell()
                'cell9.Text = "Seat Type H Linear Seat Total Area"
                ''lbl10.Text = "Total Seat Type Area"
                'cell9.ForeColor = Drawing.Color.Black
                ''cell9.BackColor = Drawing.Color.AntiqueWhite
                'cell9.HorizontalAlign = HorizontalAlign.Center
                'cell9.ColumnSpan = 4


                Dim cell10 As New TableCell()
                cell10.Text = "Total Seat Summary"
                'lbl10.Text = "Total Seat Summary"
                cell10.ForeColor = Drawing.Color.Black
                'cell10.BackColor = Drawing.Color.AntiqueWhite
                cell10.HorizontalAlign = HorizontalAlign.Center
                cell10.ColumnSpan = 4
                Dim cell11 As New TableCell()
                cell11.Text = "Total Seat Type Area"
                'lbl11.Text = "Total Seat Type Area"
                cell11.ForeColor = Drawing.Color.Black
                'cell11.BackColor = Drawing.Color.AntiqueWhite
                cell11.HorizontalAlign = HorizontalAlign.Center
                cell11.ColumnSpan = 4

                gvrow__10.Cells.Add(cell0)
                gvrow__10.Cells.Add(cell1)
                gvrow__10.Cells.Add(cell2)
                gvrow__10.Cells.Add(cell3)
                gvrow__10.Cells.Add(cell4)
                gvrow__10.Cells.Add(cell5)
                gvrow__10.Cells.Add(cell6)
                gvrow__10.Cells.Add(cell7)
                'gvrow__10.Cells.Add(cell8)
                'gvrow__10.Cells.Add(cell9)
                gvrow__10.Cells.Add(cell10)
                gvrow__10.Cells.Add(cell11)
                'gvrow__10.BackColor = Drawing.Color.LavenderBlush
                gvrow__10.BackColor = System.Drawing.ColorTranslator.FromHtml("#F1F1F1")
                gvrow__10.Font.Bold = True
                gvRow__1.Controls(0).Controls.AddAt(0, gvrow__10)

            End If


        End If
    End Sub
    Dim ASeat As Integer = 0
    Dim AVacant As Integer = 0
    Dim AOcc As Integer = 0
    Dim AREFM As Integer = 0
    Dim ASeatar As Integer = 0
    Dim AVacantar As Integer = 0
    Dim AOccar As Integer = 0
    Dim AREFMar As Integer = 0
    Dim BSeat As Integer = 0
    Dim BVacant As Integer = 0
    Dim BOcc As Integer = 0
    Dim BREFM As Integer = 0
    Dim BSeatar As Integer = 0
    Dim BVacantar As Integer = 0
    Dim BOccar As Integer = 0
    Dim BREFMar As Integer = 0
    Dim CSeat As Integer = 0
    Dim CVacant As Integer = 0
    Dim COcc As Integer = 0
    Dim CREFM As Integer = 0
    Dim CSeatar As Integer = 0
    Dim CVacantar As Integer = 0
    Dim COccar As Integer = 0
    Dim CREFMar As Integer = 0
    Dim TSeat As Integer = 0
    Dim TVacant As Integer = 0
    Dim TOcc As Integer = 0
    Dim TREFM As Integer = 0
    Dim TSeatar As Integer = 0
    Dim TVacantar As Integer = 0
    Dim TOccar As Integer = 0
    Dim TREFMar As Integer = 0

    Protected Sub gvVerticalReport_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvVerticalReport.RowDataBound
        If ddlSeatType.SelectedValue = "--All--" Then

            If e.Row.RowType = DataControlRowType.DataRow Then
                ASeat += CInt(e.Row.Cells(7).Text)
                AVacant += CInt(e.Row.Cells(8).Text)
                AOcc += CInt(e.Row.Cells(9).Text)
                AREFM += CInt(e.Row.Cells(10).Text)
                ASeatar += CInt(e.Row.Cells(11).Text)
                AVacantar += CInt(e.Row.Cells(12).Text)
                AOccar += CInt(e.Row.Cells(13).Text)
                AREFMar += CInt(e.Row.Cells(14).Text)
                BSeat += CInt(e.Row.Cells(15).Text)
                BVacant += CInt(e.Row.Cells(16).Text)
                BOcc += CInt(e.Row.Cells(17).Text)
                BREFM += CInt(e.Row.Cells(18).Text)
                BSeatar += CInt(e.Row.Cells(19).Text)
                BVacantar += CInt(e.Row.Cells(20).Text)
                BOccar += CInt(e.Row.Cells(21).Text)
                BREFMar += CInt(e.Row.Cells(22).Text)
                CSeat += CInt(e.Row.Cells(23).Text)
                CVacant += CInt(e.Row.Cells(24).Text)
                COcc += CInt(e.Row.Cells(25).Text)
                CREFM += CInt(e.Row.Cells(26).Text)
                CSeatar += CInt(e.Row.Cells(27).Text)
                CVacantar += CInt(e.Row.Cells(28).Text)
                COccar += CInt(e.Row.Cells(29).Text)
                CREFMar += CInt(e.Row.Cells(30).Text)
                TSeat += CInt(e.Row.Cells(31).Text)
                TVacant += CInt(e.Row.Cells(32).Text)
                TOcc += CInt(e.Row.Cells(33).Text)
                TREFM += CInt(e.Row.Cells(34).Text)
                TSeatar += CInt(e.Row.Cells(35).Text)
                TVacantar += CInt(e.Row.Cells(36).Text)
                TOccar += CInt(e.Row.Cells(37).Text)
                TREFMar += CInt(e.Row.Cells(38).Text)

            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Cells(6).Text = "Total"
                e.Row.Cells(7).Text = ASeat
                e.Row.Cells(8).Text = AVacant
                e.Row.Cells(9).Text = AOcc
                e.Row.Cells(10).Text = AREFM
                e.Row.Cells(11).Text = ASeatar
                e.Row.Cells(12).Text = AVacantar
                e.Row.Cells(13).Text = AOccar
                e.Row.Cells(14).Text = AREFMar
                e.Row.Cells(15).Text = BSeat
                e.Row.Cells(16).Text = BVacant
                e.Row.Cells(17).Text = BOcc
                e.Row.Cells(18).Text = BREFM
                e.Row.Cells(19).Text = BSeatar
                e.Row.Cells(20).Text = BVacantar
                e.Row.Cells(21).Text = BOccar
                e.Row.Cells(22).Text = BREFMar
                e.Row.Cells(23).Text = CSeat
                e.Row.Cells(24).Text = CVacant
                e.Row.Cells(25).Text = COcc
                e.Row.Cells(26).Text = CREFM
                e.Row.Cells(27).Text = CSeatar
                e.Row.Cells(28).Text = CVacantar
                e.Row.Cells(29).Text = COccar
                e.Row.Cells(30).Text = CREFMar
                e.Row.Cells(31).Text = TSeat
                e.Row.Cells(32).Text = TVacant
                e.Row.Cells(33).Text = TOcc
                e.Row.Cells(34).Text = TREFM
                e.Row.Cells(35).Text = TSeatar
                e.Row.Cells(36).Text = TVacantar
                e.Row.Cells(37).Text = TOccar
                e.Row.Cells(38).Text = TREFMar
                e.Row.Font.Bold = True
                e.Row.ForeColor = Color.Black
            End If
        Else
            If e.Row.RowType = DataControlRowType.DataRow Then

                ASeat += CInt(e.Row.Cells(5).Text)
                AVacant += CInt(e.Row.Cells(6).Text)
                AOcc += CInt(e.Row.Cells(7).Text)
                AREFM += CInt(e.Row.Cells(8).Text)
                ASeatar += CInt(e.Row.Cells(9).Text)
                AVacantar += CInt(e.Row.Cells(10).Text)
                AOccar += CInt(e.Row.Cells(11).Text)
                AREFMar += CInt(e.Row.Cells(12).Text)
            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Cells(4).Text = "Total"
                e.Row.Cells(5).Text = ASeat
                e.Row.Cells(6).Text = AVacant
                e.Row.Cells(7).Text = AOcc
                e.Row.Cells(8).Text = AREFM
                e.Row.Cells(9).Text = ASeatar
                e.Row.Cells(10).Text = AVacantar
                e.Row.Cells(11).Text = AOccar
                e.Row.Cells(12).Text = AREFMar
            End If
        End If
    End Sub

   
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged

        gvVerticalReport.Visible = False
        btnExport.Visible = False
        BindLocation(ddlCity.SelectedItem.Value)
        ddlLocation.Items(0).Text = "--All--"
        If ddlLocation.Items.Count = 2 Then
            ddlLocation.SelectedIndex = 0
        End If
        BindSeatTypes()

    End Sub
    
End Class
