Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_frmEditTenantDetails
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Public Sub Cleardata()
        ddlproptype.SelectedIndex = 0
        ddlBuilding.Items.Clear()
        ddlBuilding.Items.Insert(0, New ListItem("--Select --", "0"))
        ddlBuilding.SelectedIndex = 0
        txtTenantOccupiedArea.Text = ""
        txtRent.Text = ""
        txtDate.Text = getoffsetdate(Date.Today)
        txtSecurityDeposit.Text = ""
        txttcode.Text = ""
        txtRemarks.Text = ""
        txtPayableDate.Text = ""
        txttcode.Text = ""
        txtDate.Text = ""
        txtcar.Text = ""
        ddlPaymentTerms.SelectedIndex = -1
        txtPayableDate.Text = ""
        txtNoofparking.Text = ""
        txtfees.Text = ""
        txtamount.Text = ""
        ddluser.SelectedValue = 0
        txtSubject.Text = ""
        TntFrm.Text = ""
        txtPurpose.Text = ""
    End Sub
    Private Sub BindUser()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENUSER")
            'sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp1.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.Int32)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub
    Private Sub BindProperties()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_PROPERTIES_DETAILS")
        ddlBuilding.DataSource = sp.GetDataSet()
        ddlBuilding.DataTextField = "PM_PPT_NAME"
        ddlBuilding.DataValueField = "PM_PPT_SNO"
        ddlBuilding.DataBind()
        ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub
    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VIEW_TENANT_DETAILS")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()

            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Public Sub UpdateTenantDetails()
        Dim param1(23) As SqlParameter
        param1(0) = New SqlParameter("@BDG_PROP_TYPE", SqlDbType.NVarChar, 200)
        param1(0).Value = ddlproptype.SelectedItem.Value
        param1(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param1(1).Value = ddlLocation.SelectedItem.Value
        param1(2) = New SqlParameter("@TEN_CODE", SqlDbType.NVarChar, 200)
        param1(2).Value = txttcode.Text
        param1(3) = New SqlParameter("@PN_NAME", SqlDbType.NVarChar, 200)
        param1(3).Value = ddlBuilding.SelectedItem.Value
        param1(4) = New SqlParameter("@BDG_ADM_CODE", SqlDbType.NVarChar, 200)
        param1(4).Value = ddlCity.SelectedItem.Value
        param1(5) = New SqlParameter("@TEN_OCCUPIEDAREA", SqlDbType.VarChar, 100)
        param1(5).Value = txtTenantOccupiedArea.Text
        param1(6) = New SqlParameter("@TEN_RENT_PER_SFT", SqlDbType.Money, 100)
        param1(6).Value = txtRent.Text

        If txtDate.Text <> "" Then
            param1(7) = New SqlParameter("@TEN_COMMENCE_DATE", SqlDbType.DateTime, 100)
            param1(7).Value = txtDate.Text
        Else
            param1(7) = New SqlParameter("@TEN_COMMENCE_DATE", SqlDbType.VarChar, 100)
            param1(7).Value = ""
        End If
        'param1(7) = New SqlParameter("@TEN_COMMENCE_DATE", SqlDbType.DateTime, 100)
        'param1(7).Value = txtDate.Text
        param1(8) = New SqlParameter("@TEN_SECURITY_DEPOSIT", SqlDbType.Money, 100)
        param1(8).Value = txtSecurityDeposit.Text
        param1(9) = New SqlParameter("@TEN_PAYMENTTERMS", SqlDbType.NVarChar, 100)
        param1(9).Value = ddlPaymentTerms.SelectedItem.Value
        param1(10) = New SqlParameter("@TEN_NEXTPAYABLEDATE", SqlDbType.DateTime, 100)
        param1(10).Value = txtPayableDate.Text
        param1(11) = New SqlParameter("@TEN_NO_PARKING", SqlDbType.VarChar, 100)
        param1(11).Value = txtNoofparking.Text
        param1(12) = New SqlParameter("@TEN_MAINT_FEE", SqlDbType.Money, 100)
        param1(12).Value = txtfees.Text
        param1(13) = New SqlParameter("@TEN_OUTSTANDING_AMOUNT", SqlDbType.Money, 100)
        param1(13).Value = txtamount.Text
        param1(14) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param1(14).Value = ddluser.SelectedItem.Value
        param1(15) = New SqlParameter("@TEN_REMARKS", SqlDbType.NVarChar, 100)
        param1(15).Value = txtRemarks.Text
        param1(16) = New SqlParameter("@SUBAGREE", SqlDbType.NVarChar, 300)
        param1(16).Value = txtSubject.Text
        param1(17) = New SqlParameter("@PURAGREE", SqlDbType.NVarChar, 300)
        param1(17).Value = txtPurpose.Text
        param1(18) = New SqlParameter("@FRM_DT", SqlDbType.DateTime, 100)
        param1(18).Value = TntFrm.Text
        param1(19) = New SqlParameter("@TO_DT", SqlDbType.DateTime, 100)
        param1(19).Value = txtPayableDate.Text
        param1(20) = New SqlParameter("@ADD_PARKING", SqlDbType.Money, 100)
        param1(20).Value = txtcar.Text
        param1(21) = New SqlParameter("@SNO", SqlDbType.Int, 10)
        param1(21).Value = Request.QueryString("id")
        param1(22) = New SqlParameter("@UPDATEBY", SqlDbType.Int, 10)
        param1(22).Value = HttpContext.Current.Session("Uid").ToString
        Dim selectedItems As String = [String].Join(",", TntRem.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
        param1(23) = New SqlParameter("@TEN_REM", SqlDbType.NVarChar, 100)
        param1(23).Value = selectedItems
        ObjSubSonic.GetSubSonicExecute("PM_UPDATE_TENANTS", param1)
        lblmsg.Text = " Tenant Details Modified Succesfully"
        Cleardata()
    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPaymentTerms()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PAYMENT_TERMS")
            ddlPaymentTerms.DataSource = sp.GetDataSet()
            ddlPaymentTerms.DataTextField = "PM_PT_NAME"
            ddlPaymentTerms.DataValueField = "PM_PT_SNO"
            ddlPaymentTerms.DataBind()
            ddlPaymentTerms.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function Validateproperty()
        Dim validatecode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATEPROP_CODE")
        sp.Command.AddParameter("@propcode", ddlBuilding.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@user", ddluser.SelectedItem.Value, DbType.String)
        validatecode = sp.ExecuteScalar()
        Return validatecode
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If

        revNoofParking.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()

        If Not IsPostBack Then
            BindPropType()
            BindUser()
            BindCity()
            txtDate.Attributes.Add("readonly", "readonly")

            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENANT_DETAILS")
            sp4.Command.AddParameter("@SNO", Request.QueryString("id"), DbType.Int32)
            sp4.Command.AddParameter("@flag", 1, DbType.Int32)
            Dim ds As New DataSet
            ds = sp4.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                Dim li4 As ListItem = Nothing
                li4 = ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPERTY_TYPE"))
                If Not li4 Is Nothing Then
                    li4.Selected = True
                End If
                Dim li2 As ListItem = Nothing
                li2 = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_CITY_CODE"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                    'ddlCity.Enabled = False
                End If
                BindCityLoc()
                Dim li6 As ListItem = Nothing
                li6 = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_LOC_CODE"))
                If Not li6 Is Nothing Then
                    li6.Selected = True
                    'ddlCity.Enabled = False
                End If

                'BindProp()
                BindProperties()
                Dim li7 As ListItem = Nothing
                li7 = ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_PROPRTY"))
                If Not li7 Is Nothing Then
                    li7.Selected = True
                End If

                Dim li3 As ListItem = Nothing
                li3 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_NAME"))
                If Not li3 Is Nothing Then
                    li3.Selected = True
                End If
                BindPaymentTerms()
                Dim li8 As ListItem = Nothing
                li8 = ddlPaymentTerms.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TD_PAY_TERMS"))
                If Not li8 Is Nothing Then
                    li8.Selected = True
                End If
                txtTenantOccupiedArea.Text = ds.Tables(0).Rows(0).Item("PM_TEN_OCCUP_AREA")
                txtSubject.Text = ds.Tables(0).Rows(0).Item("PM_TEN_SUB_OF_AGREE")
                txtPurpose.Text = ds.Tables(0).Rows(0).Item("PM_TEN_PUR_OF_AGREE")
                txtRent.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_RENT"), 2, MidpointRounding.AwayFromZero)

                If ds.Tables(0).Rows(0).Item("PM_TD_JOIN_DT") = "01/01/1900 00:00:00" Then
                    txtDate.Text = ""
                Else
                    txtDate.Text = ds.Tables(0).Rows(0).Item("PM_TD_JOIN_DT")
                End If

                txtSecurityDeposit.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_SECURITY_DEPOSIT"), 2, MidpointRounding.AwayFromZero)

                ddlPaymentTerms.SelectedValue = ds.Tables(0).Rows(0).Item("PM_TD_PAY_TERMS")
                TntFrm.Text = ds.Tables(0).Rows(0).Item("PM_TEN_FRM_DT")
                txtPayableDate.Text = ds.Tables(0).Rows(0).Item("PM_TEN_TO_DT")
                txttcode.Text = ds.Tables(0).Rows(0).Item("PM_TEN_CODE")
                txtNoofparking.Text = ds.Tables(0).Rows(0).Item("PM_TEN_NO_OF_PARKING")
                txtfees.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_MAINT_FEES"), 2, MidpointRounding.AwayFromZero)
                txtamount.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT"), 2, MidpointRounding.AwayFromZero)
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("PM_TD_REMARKS")
                Dim s As String = ds.Tables(0).Rows(0).Item("PM_TEN_REMINDER_BEFORE").ToString()
                Dim values As String() = s.Split(","c).[Select](Function(sValue) sValue.Trim()).ToArray()
                For i As Integer = 0 To TntRem.Items.Count - 1
                    For j As Integer = 0 To values.Length - 1
                        If TntRem.Items(i).Value = values.GetValue(j) Then
                            TntRem.Items(i).Selected = True
                        End If
                    Next
                Next
            End If
        End If
    End Sub
    Protected Sub txtfees_TextChanged(sender As Object, e As EventArgs) Handles txtfees.TextChanged
        If txtRent.Text <> Nothing And txtfees.Text <> Nothing Then
            txtamount.ReadOnly = False
            txtamount.Text = Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtfees.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub
    Protected Sub txtRent_TextChanged(sender As Object, e As EventArgs) Handles txtRent.TextChanged
        If txtfees.Text <> Nothing And txtRent.Text <> Nothing Then
            txtamount.ReadOnly = False
            txtamount.Text = Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/PropertyManagement/Views/frmViewTenantDetails.aspx")
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim validatecode As Integer
            validatecode = Validateproperty()
            If validatecode = 0 Then
                lblmsg.Text = "Property already in use please select another property"
                lblmsg.Visible = True
            Else
                'UpdateTenant()
                UpdateTenantDetails()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            'BindProp()
            BindCityLoc()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlproptype.SelectedIndex > 0 Then
            BindProp()

        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0


        End If
    End Sub
End Class
