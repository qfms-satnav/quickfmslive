<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login_Popup.vb" ValidateRequest="true"
    Inherits="Masters_MAS_WebFiles_Login_Popup" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        body {    
    font: 15px/16px Roboto,sans-serif !important;
    color: #666 !important;    
}
    </style>
</head>

<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="SLA Details" ba-panel-class="with-scroll" style="padding-right: 100px;">
                    <div class="panel">
                        <div class="panel-body" style="padding-left: 450px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <div class="row form-inline" >
                                    <div class="form-group col-md-6">
                                        <b>You have Successfully Logged Out.<br /></b>
                                        <br />
                                    </div>
                                    <br />
                                </div>
     <%--                           <div class="form-group col-md-6" style="padding-left: 1px;">
                                    Click <a href="https://tatatechnologies.quickfms.com/" target="_self">https://tatatechnologies.quickfms.com/</a> to Login Again

                                </div>--%>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

     <script type="text/javascript">

         //TO AVOID FADE IN EFFECT IN IE
         var ms_ie = false;
         var ua = window.navigator.userAgent;
         var old_ie = ua.indexOf('MSIE ');
         var new_ie = ua.indexOf('Trident/');
         var mozilla = ua.indexOf('Firefox/');

        

         $(document).ready(function () {            
            
             $('.collapse-menu-link').on('click', function () {
                 $('main').toggleClass('menu-collapsed');
             });
            
         });       

    </script>
</body>

</html>
