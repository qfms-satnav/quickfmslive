Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.IO
Imports System.Threading
Imports System.Data.OleDb
Imports System.Globalization
Imports System.ComponentModel

Partial Class Controls_AddAsset
    Inherits System.Web.UI.UserControl
    Dim obj As clsMasters = New clsMasters
    Dim AATID As Integer

    Private Property lcmCode As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        RegularExpressionValidator6.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
        RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator3.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        RegularExpressionValidator5.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        RegularExpressionValidator7.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        txtIncDate.Attributes.Add("readonly", "readonly")
        txtAMCDate.Attributes.Add("readonly", "readonly")
        txtPDate.Attributes.Add("readonly", "readonly")
        txtMfgDate.Attributes.Add("readonly", "readonly")
        txtWarDate.Attributes.Add("readonly", "readonly")
        '  getlocation() 


        If Not IsPostBack Then
            getassetcategories()
            BindGridview()
            'getassetbrand()
            getlocation()
            'getassetcategory()
            getvendors()
            getAssetTypes()
            ddlAssetType.SelectedIndex = 1
            ddlCompany.SelectedIndex = -1
            gvBrand.Visible = True
            ''gvspareparts.Visible = True
            fillgrid()
            rbtamc()
            rbtIns()
            getLOB()
            BindCompany()
            ValidateCompany()
            ''txtIncDate.Attributes.Add("onClick", "displayDatePicker('" + txtIncDate.ClientID + "')")
            ''txtIncDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtAMCDate.Attributes.Add("onClick", "displayDatePicker('" + txtAMCDate.ClientID + "')")
            ''txtAMCDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtPDate.Attributes.Add("onClick", "displayDatePicker('" + txtPDate.ClientID + "')")
            ''txtPDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtMfgDate.Attributes.Add("onClick", "displayDatePicker('" + txtMfgDate.ClientID + "')")
            ''txtMfgDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ''txtWarDate.Attributes.Add("onClick", "displayDatePicker('" + txtWarDate.ClientID + "')")
            ''txtWarDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ddlAstBrand.Items.Insert(0, "--Select--")
            ddlAstSubCat.Items.Insert(0, "--Select--")
            ddlModel.Items.Insert(0, "--Select--")

            ddlTower.Items.Insert(0, "--Select--")
            ddlFloor.Items.Insert(0, "--Select--")
            lblProcess.Text = Session("Child")
            Div7.Visible = False
            'pnlAst.Visible = True
            ' getassettaxtype()
            Div7.Visible = False
            'btnExport.Visible = False



        End If
    End Sub



    Private Sub BindCompany()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COMPANYS")
        ddlCompany.DataSource = sp.GetDataSet()
        ddlCompany.DataTextField = "CNP_NAME"
        ddlCompany.DataValueField = "CNP_ID"
        ddlCompany.DataBind()
        ddlCompany.Items.Insert(0, New ListItem("--Select--"))
    End Sub
    Private Sub ValidateCompany()
        Dim var As String
        var = Session("COMPANYID")
        ddlCompany.ClearSelection()
        ddlCompany.Items.FindByValue(var).Selected = True
        ddlCompany.Enabled = True
        If var = 1 Then
            ddlCompany.Enabled = True
        Else
            ddlCompany.Enabled = False
        End If
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSET_DETAILS")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int16)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@PAGENUM", gvBrand.PageIndex + 1, DbType.String)
        sp.Command.AddParameter("@PAGESIZE", 10, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvBrand.VirtualItemCount = ds.Tables(1).Rows(0).ItemArray(0)
        gvBrand.DataSource = ds
        gvBrand.DataBind()

        'gvspareparts.DataSource = ds
        'gvspareparts.DataBind()
        'For i As Integer = 0 To gvBrand.Rows.Count - 1
        '    Dim lblstatus As Label = CType(gvBrand.Rows(i).FindControl("lblstatus"), Label)
        '    If lblstatus.Text = "1" Then
        '        lblstatus.Text = "Active"
        '    Else
        '        lblstatus.Text = "InActive"
        '    End If
        'Next

    End Sub
    Protected Sub gvBrand_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvBrand.PageIndexChanging
        gvBrand.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    'Private Sub getassetbrand()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETBRAND")
    '    sp.Command.AddParameter("@dummy", 1, DbType.Int16)
    '    ddlBrand.DataSource = sp.GetDataSet()
    '    ddlBrand.DataTextField = "manufacturer"
    '    ddlBrand.DataValueField = "manufacturerID"
    '    ddlBrand.DataBind()
    '    ddlBrand.Items.Insert(0, New ListItem("--Select--"))
    'End Sub
    Private Sub getassetcategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
    End Sub
    Private Sub getassetcategory(ven_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CATBYVENDORS_NOTCONS")
        sp.Command.AddParameter("@VT_CODE", ven_code, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
    End Sub
    Private Sub getLOB()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOB")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlDepartment.DataSource = sp.GetDataSet()
        ddlDepartment.DataTextField = "Cost_Center_Name"
        ddlDepartment.DataValueField = "Cost_Center_Code"
        ddlDepartment.DataBind()
        ddlDepartment.Items.Insert(0, "--Select--")
    End Sub
    'Private Sub getassettaxtype()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETTAXTYPE")
    '    sp.Command.AddParameter("@dummy", 1, DbType.Int16)
    '    ddlTaxTypeID.DataSource = sp.GetDataSet()
    '    ddlTaxTypeID.DataTextField = "taxtype"
    '    ddlTaxTypeID.DataValueField = "taxtypeid"
    '    ddlTaxTypeID.DataBind()
    '    ddlTaxTypeID.Items.Insert(0, New ListItem("--Select--"))
    'End Sub
    Private Function CheckAssetID() As Int16
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSET_ID_CHECKING")
        sp3.Command.AddParameter("@AssetID", txtAssetDescription.Text, DbType.String)
        Dim res As Int16
        res = sp3.ExecuteScalar()
        Return res
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'NewAssetCode()
        If Not Page.IsValid Then
            Exit Sub
        End If
        If btnSubmit.Text = "Add" Then
            NewAssetCode()
            If (txtMfgDate.Text) <> "" Then
                If (txtPDate.Text) <> "" Then
                    If CDate(txtMfgDate.Text) > CDate(txtPDate.Text) Then
                        lblMsg.Text = "The Manufactured Date must be earlier than the Purchased Date"
                        lblMsg.Visible = True
                        Exit Sub
                    End If
                    If (txtWarDate.Text) <> "" Then
                        If CDate(txtWarDate.Text) < CDate(txtMfgDate.Text) Then
                            lblMsg.Text = "The Manufactured Date must be earlier than the Expired Date"
                            lblMsg.Visible = True
                            fillgrid()
                            Exit Sub
                        End If
                    End If
                End If
            End If

            Dim isHouse As Int16
            isHouse = CheckAssetID()
            If isHouse = 1 Then
                lblMsg.Text = "Asset is in use; try another"
                lblMsg.Visible = True
                Exit Sub
            End If
            'If Session("DepMethod") <> 3 Then
            '    If (txtPDate.Text) <> "" Then
            '        lblMsg.Text = "To compute depreciation, choose Purchase Date."
            '        lblMsg.Visible = True
            '        Exit Sub
            '    End If
            'End If

            If Session("DepMethod") = 1 Then
                If (txtLifeSpan.Text) = "" Then
                    lblMsg.Text = "Enter the Life Span value; it is necessary to do so in order to compute depreciation."
                    lblMsg.Visible = True
                End If
            End If

            ltrAMC.Visible = False
            litINC.Visible = False
            Dim orgfilenameAMC As String = Replace(Replace(fpBrowseAMCDoc.FileName, " ", "_"), "&", "_")
            Dim orgfilenameINC As String = Replace(Replace(fpBrowseIncDoc.FileName, " ", "_"), "&", "_")
            Dim orgAssetImage As String = Replace(Replace(fpAssetImage.FileName, " ", "_"), "&", "_")
            Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
            Dim repdocdatetimeAMC As String = ""
            Dim repdocdatetimeINC As String = ""
            Dim repdocdatetimeImage As String = ""
            'fillgrid()

            Try
                If (fpBrowseAMCDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseAMCDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_AMC_" & orgfilenameAMC
                    fpBrowseAMCDoc.PostedFile.SaveAs(filePath)
                    repdocdatetimeAMC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "AMC_" & orgfilenameAMC

                Else
                    'lblMsg.Visible = True
                    'lblMsg.Text = "Please Select AMC Documents to Upload..."
                    'Exit Sub

                End If


                If (fpBrowseIncDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseIncDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_INC_" & orgfilenameINC
                    fpBrowseIncDoc.PostedFile.SaveAs(filePath)
                    repdocdatetimeINC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "INC_" & orgfilenameINC

                End If
                If (fpAssetImage.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpAssetImage.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_IMG_" & orgAssetImage
                    fpAssetImage.PostedFile.SaveAs(filePath)
                    repdocdatetimeImage = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "IMG_" & orgAssetImage
                End If

                'If (fpBrowseAMCDoc.HasFile) And (fpBrowseIncDoc.HasFile) Then
                ' getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
                Dim asttype As Integer = checkassettype()
                If asttype = 1 Then
                    'Consumable
                    pnlAst.Visible = False
                Else
                    'Not
                    insertnewrecord(repdocdatetimeAMC, repdocdatetimeINC, repdocdatetimeImage)
                    InsertAssetSpace(txtAssetCode.Text, ddlLocation.SelectedItem.Value, ddlTower.SelectedValue, ddlFloor.SelectedValue, 0, "MANUAL_UPLOAD_MASTER")

                End If
                fillgrid()
                lblMsg.Visible = True
                lblMsg.Text = "New Asset Successfully Added..."


                'cleardata()
                clearall()

                'Else
                '    lblMsg.Visible = True
                '    lblMsg.Text = "Select the file"
                'End If

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            'Dim ValidateCode As Integer
            'ValidateCode = ValidateAsset(txtAssetCode.Text)
            'If ValidateCode = 0 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Asset Code already exist please enter another Asset Code..."
            'ElseIf ValidateCode = 1 Then
            'insertnewrecord()
            'fillgrid()

            'End If
        Else
            Dim orgfilenameAMC As String = Replace(Replace(fpBrowseAMCDoc.FileName, " ", "_"), "&", "_")
            Dim orgfilenameINC As String = Replace(Replace(fpBrowseIncDoc.FileName, " ", "_"), "&", "_")
            Dim orgAssetImage As String = Replace(Replace(fpAssetImage.FileName, " ", "_"), "&", "_")
            Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
            Dim repdocdatetimeAMC As String = ""
            Dim repdocdatetimeINC As String = ""
            Dim repdocdatetimeImage As String = ""

            If ddlAsset.SelectedIndex <> 0 Then
                If (fpBrowseAMCDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseAMCDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_AMC_" & orgfilenameAMC
                    fpBrowseAMCDoc.PostedFile.SaveAs(filePath)
                    repdocdatetimeAMC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "AMC_" & orgfilenameAMC
                End If
                If (fpBrowseIncDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseIncDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_INC_" & orgfilenameINC
                    fpBrowseIncDoc.PostedFile.SaveAs(filePath)
                    repdocdatetimeINC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "INC_" & orgfilenameINC
                End If
                If (fpAssetImage.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpAssetImage.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_IMG_" & orgAssetImage
                    fpAssetImage.PostedFile.SaveAs(filePath)
                    repdocdatetimeImage = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "IMG_" & orgAssetImage
                End If

                modifydata(repdocdatetimeAMC, repdocdatetimeINC, repdocdatetimeImage)

            End If
            fillgrid()


            Exit Sub
        End If

    End Sub

    Private Sub modifydata(fpamcdocfilename As String, fpincdocfilename As String, fpAstImgFileName As String)
        If Not Page.IsValid Then
            Exit Sub
        End If
        Try
            Dim amctdt As DateTime
            Dim incdt As DateTime
            Dim mfgdt As DateTime
            Dim wrntdt As DateTime
            Dim purdt As DateTime

            If Session("DepMethod") <> 3 Then
                If (txtPDate.Text) = "" Then
                    lblMsg.Text = "To compute depreciation, choose Purchase Date."
                    lblMsg.Visible = True
                    Exit Sub
                End If
            End If

            If Session("DepMethod") = 1 Then
                If (txtLifeSpan.Text) = "" Then
                    lblMsg.Text = "Enter the Life Span value; it is necessary to do so in order to compute depreciation."
                    lblMsg.Visible = True
                End If
            End If

            If Not txtAMCDate.Text = "" Then
                amctdt = DateTime.ParseExact(txtAMCDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
            End If
            If Not txtIncDate.Text = "" Then
                incdt = DateTime.ParseExact(txtIncDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
            End If

            If Not txtMfgDate.Text = "" Then
                mfgdt = DateTime.ParseExact(txtMfgDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
            End If
            If Not txtWarDate.Text = "" Then
                wrntdt = DateTime.ParseExact(txtWarDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
            End If
            If Not txtPDate.Text = "" Then
                purdt = DateTime.ParseExact(txtPDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
            End If

            ''Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_AMG_ASSET_UPDATE_Master")
            Dim dt As DataTable = DirectCast(ViewState("Curtbl"), DataTable)

            Dim param As SqlParameter() = New SqlParameter(41) {}
            param(0) = New SqlParameter("@AAT_CODE", SqlDbType.VarChar)
            param(0).Value = txtAssetName.Text
            param(1) = New SqlParameter("@AAT_LOC_ID", SqlDbType.VarChar)
            param(1).Value = ddlLocation.SelectedValue
            param(2) = New SqlParameter("@AAT_MODEL_ID", SqlDbType.VarChar)
            param(2).Value = ddlModel.SelectedValue
            param(3) = New SqlParameter("@AAT_MODEL_NAME", SqlDbType.VarChar)
            param(3).Value = ddlModel.SelectedItem.Text
            param(4) = New SqlParameter("@AAT_SUB_CODE", SqlDbType.VarChar)
            param(4).Value = ddlAstSubCat.SelectedValue
            param(5) = New SqlParameter("@AAT_AVR_CODE", SqlDbType.VarChar)
            param(5).Value = ddlVendor.SelectedValue
            param(6) = New SqlParameter("@AAT_AAB_CODE", SqlDbType.VarChar)
            param(6).Value = ddlAstBrand.SelectedValue
            param(7) = New SqlParameter("@AAT_AMC_REQD", SqlDbType.Int)
            param(7).Value = rdAMC.SelectedValue
            param(8) = New SqlParameter("@AAT_AMC_DATE", SqlDbType.VarChar)
            param(8).Value = txtAMCDate.Text
            param(9) = New SqlParameter("@AAT_AMC_DOCPATH", SqlDbType.VarChar)
            param(9).Value = fpamcdocfilename
            param(10) = New SqlParameter("@AAT_INS_REQD", SqlDbType.VarChar)
            param(10).Value = rdIns.SelectedValue
            param(11) = New SqlParameter("@AAT_INS_DATE", SqlDbType.VarChar)
            param(11).Value = txtIncDate.Text
            param(12) = New SqlParameter("@AAT_AAG_CODE", SqlDbType.VarChar)
            param(12).Value = ddlAssetCategory.SelectedItem.Value
            param(13) = New SqlParameter("@AAT_INS_DOCPATH", SqlDbType.VarChar)
            param(13).Value = fpincdocfilename
            param(14) = New SqlParameter("@AAT_AST_COST", SqlDbType.VarChar)
            param(14).Value = Convert.ToDouble(txtAssetPrice.Text)

            param(15) = New SqlParameter("@AAT_AST_SERIALNO", SqlDbType.VarChar)
            param(15).Value = txtAssetSerialNo.Text
            param(16) = New SqlParameter("@AAT_UPT_DT", SqlDbType.DateTime)
            param(16).Value = getoffsetdatetime(DateTime.Now)
            param(17) = New SqlParameter("@AAT_UPT_BY", SqlDbType.VarChar)
            param(17).Value = Session("UID")
            param(18) = New SqlParameter("@AAT_STA_ID", SqlDbType.VarChar)
            param(18).Value = ddlStatus.SelectedItem.Value
            param(19) = New SqlParameter("@AAT_OWNED", SqlDbType.VarChar)
            param(19).Value = 1
            param(20) = New SqlParameter("@AAT_PURCHASED_STATUS", SqlDbType.VarChar)
            param(20).Value = 1
            param(21) = New SqlParameter("@AAT_SPC_FIXED", SqlDbType.VarChar)
            param(21).Value = 1
            param(22) = New SqlParameter("@AAT_USR_MOVABLE", SqlDbType.VarChar)
            param(22).Value = 0
            param(23) = New SqlParameter("@AAT_AST_CONS", SqlDbType.VarChar)
            param(23).Value = ddlStatus.SelectedItem.Value
            ''param(24) = New SqlParameter("@AAT_RATE_CONTRACT", SqlDbType.VarChar)
            ''param(24).Value = RdbRateContract.SelectedItem.Value
            param(24) = New SqlParameter("@AAT_DESC", SqlDbType.VarChar)
            param(24).Value = txtAssetDescription.Text
            param(25) = New SqlParameter("@CONF_TYPE", SqlDbType.VarChar)
            param(25).Value = ""
            param(26) = New SqlParameter("@AAT_MFG_DATE", SqlDbType.VarChar)
            param(26).Value = txtMfgDate.Text
            param(27) = New SqlParameter("@AAT_WRNTY_DATE", SqlDbType.VarChar)
            param(27).Value = txtWarDate.Text
            param(28) = New SqlParameter("@PURCHASEDDATE", SqlDbType.VarChar)
            param(28).Value = txtPDate.Text
            param(29) = New SqlParameter("@LIFESPAN", SqlDbType.VarChar)
            param(29).Value = txtLifeSpan.Text
            param(30) = New SqlParameter("@PONUMBER", SqlDbType.VarChar)
            param(30).Value = txtPONumber.Text
            param(31) = New SqlParameter("@DEPRECIATION", SqlDbType.Float)
            param(31).Value = IIf(txtDepriciation.Text = "", 0, txtDepriciation.Text)
            param(32) = New SqlParameter("@ASTYPE", SqlDbType.Int)
            param(32).Value = ddlAssetType.SelectedValue
            param(33) = New SqlParameter("@AAT_TOWER_CODE", SqlDbType.VarChar)
            param(33).Value = ddlTower.SelectedItem.Value
            param(34) = New SqlParameter("@AAT_ASSET_SpareParts", SqlDbType.Structured)
            param(34).Value = dt
            param(35) = New SqlParameter("@AAT_FLOOR_CODE", SqlDbType.VarChar)
            param(35).Value = ddlFloor.SelectedItem.Value
            param(36) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(36).Value = ddlCompany.SelectedValue
            param(37) = New SqlParameter("@LOB", SqlDbType.VarChar)
            param(37).Value = ddlDepartment.SelectedItem.Value
            param(38) = New SqlParameter("@AAT_ASSET_IMAGE", SqlDbType.VarChar)
            param(38).Value = fpAstImgFileName
            param(39) = New SqlParameter("@AAT_NAME", SqlDbType.VarChar)
            param(39).Value = txtAssetDescription.Text
            param(40) = New SqlParameter("@AAT_INVOICE_NUM", SqlDbType.VarChar)
            param(40).Value = TxtBox_INVOICE.Text
            param(41) = New SqlParameter("@AAT_DESCRIPTION", SqlDbType.VarChar)
            param(41).Value = txt_description.Text

            ''Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_AMG_ASSET_Insert_Master")
            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "AST_AMG_ASSET_UPDATE_Master", param)
            lblMsg.Visible = True
            lblMsg.Text = "Asset successfully modified"
            ddlTower.SelectedIndex = -1
            ddlFloor.SelectedIndex = -1
            fillgrid()
            cleardata()
            'ddlAsset.SelectedIndex = 0

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub InsertAssetSpace(ByVal AstCode As String, ByVal location As String, ByVal Tower As String, ByVal Floor As String, ByVal ProcType As Integer, ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMT_ASSET_SPACE_INSERT")
        sp.Command.AddParameter("@AAS_AAT_CODE", AstCode, DbType.String)
        sp.Command.AddParameter("@AAS_LOC_ID", location, DbType.String)
        sp.Command.AddParameter("@AAS_TWR_ID", Tower, DbType.String)
        sp.Command.AddParameter("@AAS_FLR_ID", Floor, DbType.String)
        sp.Command.AddParameter("@AAS_BDG_ID", "", DbType.String)
        sp.Command.AddParameter("@AAS_PROC_TYPE", ProcType, DbType.String)
        sp.Command.AddParameter("@AAS_REMARKS", "MANUAL_UPLOAD_MASTER", DbType.String)
        sp.Command.AddParameter("@AAS_REQ_ID", reqid, DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    'Private Sub modifydata()
    '    Try
    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MODIFY_ASSETDATA")
    '        sp1.Command.AddParameter("@productid", ddlAsset.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@sku", txtAssetCode.Text, DbType.String)
    '        sp1.Command.AddParameter("@productName", txtAssetName.Text, DbType.String)
    '        sp1.Command.AddParameter("@shortDescription", txtAssetCode.Text, DbType.String)
    '        sp1.Command.AddParameter("@ourprice", txtSalvgValue.Text, DbType.Currency)
    '        sp1.Command.AddParameter("@retailPrice", txtAssetPrice.Text, DbType.Currency)
    '        sp1.Command.AddParameter("@manufacturerID", ddlBrand.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@statusid", ddlStatus.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@productTypeID", ddlAssetCategory.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@taxTypeID", ddlTaxTypeID.SelectedItem.Value, DbType.Int32)
    '        sp1.Command.AddParameter("@stockLocation", txtStkLocation.Text, DbType.String)
    '        sp1.Command.AddParameter("@adminComments", txtAdmCmnts.Text, DbType.String)
    '        sp1.Command.AddParameter("@createdBy", Session("Uid"), DbType.String)
    '        sp1.ExecuteScalar()
    '        ' fillgrid()
    '        lblMsg.Visible = True
    '        lblMsg.Text = "Asset successfully modified"
    '        cleardata()
    '        ddlAsset.SelectedIndex = 0
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    Public Function ValidateAsset(ByVal assetcode As String)
        Dim ValidateCode As Integer
        'Dim PN_PROPERTYTYPE As String = brandcode
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_VALIDATE_ASSET")
        sp1.Command.AddParameter("@sku", assetcode, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function

    Public Sub insertnewrecord(fpamcdocfilename As String, fpincdocfilename As String, fpAstImgFileName As String)
        Try
            Dim dt As DataTable = DirectCast(ViewState("Curtbl"), DataTable)

            Dim param As SqlParameter() = New SqlParameter(43) {}
            param(0) = New SqlParameter("@AAT_CODE", SqlDbType.VarChar)
            param(0).Value = txtAssetCode.Text
            param(1) = New SqlParameter("@AAT_LOC_ID", SqlDbType.VarChar)
            param(1).Value = ddlLocation.SelectedValue
            param(2) = New SqlParameter("@AAT_MODEL_ID", SqlDbType.VarChar)
            param(2).Value = ddlModel.SelectedValue
            param(3) = New SqlParameter("@AAT_MODEL_NAME", SqlDbType.VarChar)
            param(3).Value = ddlModel.SelectedItem.Text
            param(4) = New SqlParameter("@AAT_SUB_CODE", SqlDbType.VarChar)
            param(4).Value = ddlAstSubCat.SelectedValue
            param(5) = New SqlParameter("@AAT_AVR_CODE", SqlDbType.VarChar)
            param(5).Value = ddlVendor.SelectedValue
            param(6) = New SqlParameter("@AAT_AAB_CODE", SqlDbType.VarChar)
            param(6).Value = ddlAstBrand.SelectedValue
            param(7) = New SqlParameter("@AAT_AMC_REQD", SqlDbType.Int)
            param(7).Value = rdAMC.SelectedValue
            param(8) = New SqlParameter("@AAT_AMC_DATE", SqlDbType.VarChar)
            param(8).Value = txtAMCDate.Text
            param(9) = New SqlParameter("@AAT_AMC_DOCPATH", SqlDbType.VarChar)
            param(9).Value = fpamcdocfilename
            param(10) = New SqlParameter("@AAT_INS_REQD", SqlDbType.VarChar)
            param(10).Value = rdIns.SelectedValue
            param(11) = New SqlParameter("@AAT_INS_DATE", SqlDbType.VarChar)
            param(11).Value = txtIncDate.Text
            param(12) = New SqlParameter("@AAT_AAG_CODE", SqlDbType.VarChar)
            param(12).Value = ddlAssetCategory.SelectedItem.Value
            param(13) = New SqlParameter("@AAT_INS_DOCPATH", SqlDbType.VarChar)
            param(13).Value = fpincdocfilename
            param(14) = New SqlParameter("@AAT_AST_COST", SqlDbType.VarChar)
            param(14).Value = Convert.ToDouble(txtAssetPrice.Text)

            param(15) = New SqlParameter("@AAT_AST_SERIALNO", SqlDbType.VarChar)
            param(15).Value = txtAssetSerialNo.Text
            param(16) = New SqlParameter("@AAT_UPT_DT", SqlDbType.DateTime)
            param(16).Value = getoffsetdatetime(DateTime.Now)
            param(17) = New SqlParameter("@AAT_UPT_BY", SqlDbType.VarChar)
            param(17).Value = Session("UID")
            param(18) = New SqlParameter("@AAT_STA_ID", SqlDbType.VarChar)
            param(18).Value = ddlStatus.SelectedItem.Value
            param(19) = New SqlParameter("@AAT_OWNED", SqlDbType.VarChar)
            param(19).Value = 1
            param(20) = New SqlParameter("@AAT_PURCHASED_STATUS", SqlDbType.VarChar)
            param(20).Value = 1
            param(21) = New SqlParameter("@AAT_SPC_FIXED", SqlDbType.VarChar)
            param(21).Value = 1
            param(22) = New SqlParameter("@AAT_USR_MOVABLE", SqlDbType.VarChar)
            param(22).Value = 0
            param(23) = New SqlParameter("@AAT_AST_CONS", SqlDbType.VarChar)
            param(23).Value = ddlStatus.SelectedItem.Value
            param(24) = New SqlParameter("@AAT_RATE_CONTRACT", SqlDbType.VarChar)
            param(24).Value = RdbRateContract.SelectedItem.Value
            param(25) = New SqlParameter("@AAT_DESC", SqlDbType.VarChar)
            param(25).Value = ddlModel.SelectedItem.Text
            param(26) = New SqlParameter("@CONF_TYPE", SqlDbType.VarChar)
            param(26).Value = ""
            param(27) = New SqlParameter("@AAT_MFG_DATE", SqlDbType.VarChar)
            param(27).Value = txtMfgDate.Text
            param(28) = New SqlParameter("@AAT_WRNTY_DATE", SqlDbType.VarChar)
            param(28).Value = txtWarDate.Text
            param(29) = New SqlParameter("@PURCHASEDDATE", SqlDbType.VarChar)
            param(29).Value = txtPDate.Text
            param(30) = New SqlParameter("@LIFESPAN", SqlDbType.VarChar)
            param(30).Value = txtLifeSpan.Text
            param(31) = New SqlParameter("@PONUMBER", SqlDbType.VarChar)
            param(31).Value = txtPONumber.Text
            param(32) = New SqlParameter("@DEPRECIATION", SqlDbType.Float)
            param(32).Value = IIf(txtDepriciation.Text = "", 0, txtDepriciation.Text)
            param(33) = New SqlParameter("@ASTYPE", SqlDbType.Int)
            param(33).Value = ddlAssetType.SelectedValue
            param(34) = New SqlParameter("@AAT_TOWER_CODE", SqlDbType.VarChar)
            param(34).Value = ddlTower.SelectedItem.Value
            param(35) = New SqlParameter("@AAT_ASSET_SpareParts", SqlDbType.Structured)
            param(35).Value = dt
            param(36) = New SqlParameter("@AAT_FLOOR_CODE", SqlDbType.VarChar)
            param(36).Value = ddlFloor.SelectedItem.Value
            param(37) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(37).Value = ddlCompany.SelectedValue
            param(38) = New SqlParameter("@LOB", SqlDbType.VarChar)
            param(38).Value = ddlDepartment.SelectedItem.Value
            param(39) = New SqlParameter("@AAT_ASSET_IMAGE", SqlDbType.VarChar)
            param(39).Value = fpAstImgFileName
            param(40) = New SqlParameter("@AAT_INVOICE_NUM", SqlDbType.VarChar)
            param(40).Value = TxtBox_INVOICE.Text
            param(41) = New SqlParameter("@AAT_VENDOR", SqlDbType.VarChar)
            param(41).Value = IIf(TextBox12.Text = "", "", TextBox12.Text)

            param(42) = New SqlParameter("@ASSET_NAME1", SqlDbType.VarChar)
            param(42).Value = txtAssetDescription.Text
            param(43) = New SqlParameter("@AAT_DESCRIPTION", SqlDbType.VarChar)
            param(43).Value = txt_description.Text
            ''Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_AMG_ASSET_Insert_Master")
            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "AST_AMG_ASSET_Insert_Master", param)
            If res = "1" Then
                lblMsg.Text = "Asset Successfully Added"
            Else
                lblMsg.Text = "Something went wrong; try again"
            End If

            fillgrid()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
    '    If rbActions.Checked = True Then
    '        ddlAsset.Visible = False
    '        lblAsset.Visible = False
    '        cleardata()
    '        btnSubmit.Text = "Add"
    '        getassetcategories()
    '        ddlAstBrand.Items.Insert(0, "--Select--")
    '        ddlAstSubCat.Items.Insert(0, "--Select--")
    '        ddlModel.Items.Insert(0, "--Select--")
    '        '  ddlLocation.Items.Insert(0, "--Select--")
    '        lblMsg.Visible = False
    '        txtAssetCode.Enabled = True
    '        ddlVendor.Enabled = True
    '        ddlAssetCategory.Enabled = True
    '        ddlAstSubCat.Enabled = True
    '        ddlAstBrand.Enabled = True
    '        ddlModel.Enabled = True
    '        ddlLocation.Enabled = True
    '        ddlTower.Enabled = True
    '        ddlFloor.Enabled = True
    '        txtAssetPrice.Enabled = True
    '        txtAssetName.Enabled = True
    '        ddlAssetType.Enabled = True
    '        ' fillgrid()
    '    Else
    '        ddlAsset.Visible = True
    '        lblAsset.Visible = True
    '        cleardata()
    '        btnSubmit.Text = "Modify"
    '        lblMsg.Visible = False
    '        txtAssetCode.Enabled = False
    '        getasset()
    '        ddlVendor.Enabled = False
    '        ddlAssetCategory.Enabled = False
    '        ddlAstSubCat.Enabled = False
    '        ddlAstBrand.Enabled = False
    '        ddlModel.Enabled = False
    '        ddlLocation.Enabled = False
    '        ddlTower.Enabled = False
    '        ddlFloor.Enabled = False
    '        txtAssetPrice.Enabled = False
    '        txtAssetName.Enabled = False
    '        ddlAssetType.Enabled = False
    '        'rbn1.Visible = True
    '        'rbn2.Visible = True

    '        ddlAssetType.SelectedIndex = 1

    '        'fillgrid()
    '    End If

    'End Sub

    Private Sub getasset()
        Dim var As String
        var = Session("COMPANYID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETALL_ASSET")
        sp.Command.AddParameter("@dummy", var, DbType.Int16)
        ddlAsset.DataSource = sp.GetDataSet()
        ddlAsset.DataTextField = "AAT_NAME"
        ddlAsset.DataValueField = "AAT_CODE"
        ddlAsset.DataBind()
        ddlAsset.Items.Insert(0, New ListItem("--Select--"))
    End Sub

    Protected Sub ddlAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAsset.SelectedIndexChanged
        Try
            If ddlAsset.SelectedIndex <> 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ALLASSETDATA")
                sp.Command.AddParameter("@AAT_ID", ddlAsset.SelectedItem.Value, DbType.String)
                Dim ds As New DataSet
                ds = sp.GetDataSet
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim cat_code As String = ds.Tables(0).Rows(0).Item("AAT_AAG_CODE")
                    Dim subcat_code As String = ds.Tables(0).Rows(0).Item("AAT_SUB_CODE")
                    Dim brand_code As String = ds.Tables(0).Rows(0).Item("AAT_AAB_CODE")
                    Dim model_code As String = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
                    Dim lcmCode As String = ds.Tables(0).Rows(0).Item("AAS_LOC_ID")
                    Dim twrCode As String = ds.Tables(0).Rows(0).Item("AAT_TOWER_CODE")
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_STA_ID")).Selected = True
                    ddlVendor.ClearSelection()
                    ddlVendor.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AVR_CODE")).Selected = True
                    ' getassetcategory(ds.Tables(0).Rows(0).Item("AAT_AVR_CODE"))
                    getassetcategories()
                    ddlAssetCategory.ClearSelection()
                    ddlAssetCategory.Items.FindByValue(cat_code).Selected = True
                    getsubcategorybycat(cat_code)
                    ddlCompany.ClearSelection()
                    ddlCompany.Items.FindByValue(ds.Tables(0).Rows(0).Item("COMPANYID")).Selected = True
                    ddlAstSubCat.ClearSelection()
                    ddlAstSubCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_SUB_CODE")).Selected = True
                    getbrandbycatsubcat(cat_code, subcat_code)
                    ddlAstBrand.ClearSelection()
                    ddlAstBrand.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AAB_CODE")).Selected = True
                    getmakebycatsubcat(cat_code, subcat_code, brand_code)
                    ddlModel.ClearSelection()
                    ddlModel.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")).Selected = True
                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAS_LOC_ID")).Selected = True
                    txtAssetName.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
                    txtAssetCode.Text = ds.Tables(0).Rows(0).Item("AAT_CODE")
                    txt_description.Text = ds.Tables(0).Rows(0).Item("AAT_DESCRIPTION")
                    txtAssetDescription.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
                    'txtSalvgValue.Text = ds.Tables(0).Rows(0).Item("AAT_AST_SLVGVAL")

                    txtAssetPrice.Text = ds.Tables(0).Rows(0).Item("AAT_AST_COST")
                    txtAssetSerialNo.Text = ds.Tables(0).Rows(0).Item("AAT_AST_SERIALNO")
                    txtAMCDate.Text = ds.Tables(0).Rows(0).Item("AAT_AMC_DATE")
                    txtIncDate.Text = ds.Tables(0).Rows(0).Item("AAT_INS_DATE")
                    txtMfgDate.Text = ds.Tables(0).Rows(0).Item("AAT_MFG_DATE")
                    txtPDate.Text = ds.Tables(0).Rows(0).Item("PURCHASEDDATE")
                    txtWarDate.Text = ds.Tables(0).Rows(0).Item("AAT_WRNTY_DATE")
                    txtLifeSpan.Text = ds.Tables(0).Rows(0).Item("AAT_LIFE_SPAN")
                    txtDepriciation.Text = ds.Tables(0).Rows(0).Item("AAT_DEPRICIATION")
                    txtPONumber.Text = ds.Tables(0).Rows(0).Item("AAT_PO_NUMBER")
                    ddlAssetType.ClearSelection()
                    ddlAssetType.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AST_STATUS")).Selected = True
                    getTowersByLocations(lcmCode)
                    ddlTower.ClearSelection()
                    ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_TOWER_CODE")).Selected = True
                    getFloorsByLocationTower(lcmCode, twrCode)
                    ddlFloor.ClearSelection()
                    ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_FLOOR_CODE")).Selected = True
                    getLOB()
                    ddlDepartment.ClearSelection()
                    ddlDepartment.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_LOB")).Selected = True
                    rdAMC.ClearSelection()
                    rdAMC.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AMC_REQD")).Selected = True
                    rbtamc()
                    rdIns.ClearSelection()
                    rdIns.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_INS_REQD")).Selected = True
                    rbtIns()
                    If rdIns.SelectedValue = 0 Then     'ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH") = "" Or 
                        hplINC.Visible = False
                        rbnInsDate.Visible = False
                        rbnInsUpload.Visible = False
                    Else
                        rbnAmcDate.Visible = False
                        rbnAmcUpload.Visible = False
                        hplINC.Visible = False
                        hplINC.Text = "Click to view document"
                        ' hplINC.NavigateUrl = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH")
                        hplINC.Visible = False
                        litINC.Visible = True
                        litINC.Text = "<a download href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH") & "' >Click to download document</a>"
                    End If
                    If rdAMC.SelectedValue = 0 Then     'ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") = "" Or
                        hplAMC.Visible = False
                        rbnAmcDate.Visible = False
                        rbnAmcUpload.Visible = False
                    Else
                        rbnAmcDate.Visible = True
                        rbnAmcUpload.Visible = True
                        hplAMC.Visible = False
                        hplAMC.Text = "Click to view document"
                        ' hplAMC.NavigateUrl = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH")
                        Dim filename As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH")
                        ' hplAMC.Target = "_blank"
                        ltrAMC.Visible = True
                        'ltrAMC.Text = "<a href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") & "' >Click to view document</a>"
                        ltrAMC.Text = "<a download href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") & "' >Click to download image</a>"
                        'fileDownload(ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH"), Server.MapPath("~/UploadFiles/"))
                    End If
                End If

                LnkAstImg.Visible = True
                'LnkAstImg.Text = "Click to view image"
                LtlAstImg.Visible = True
                LtlAstImg.Text = "<a download href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_ASSET_IMAGE") & "' >Click to download image</a>"
            Else
                cleardata()

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub fileDownload(fileName As String, fileUrl As String)
        Try
            Page.Response.Clear()
            Dim success As Boolean = ResponseFile(Page.Request, Page.Response, fileName, fileUrl, 1024000)
            If Not success Then
                Response.Write("Downloading Error!")
            End If
            Page.Response.[End]()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Shared Function ResponseFile(_Request As HttpRequest, _Response As HttpResponse, _fileName As String, _fullPath As String, _speed As Long) As Boolean
        Try
            Dim myFile As New FileStream(_fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            Dim br As New BinaryReader(myFile)
            Try
                _Response.AddHeader("Accept-Ranges", "bytes")
                _Response.Buffer = False
                Dim fileLength As Long = myFile.Length
                Dim startBytes As Long = 0

                Dim pack As Integer = 10240
                '10K bytes
                Dim sleep As Integer = CInt(Math.Floor(CDbl(1000 * pack / _speed))) + 1
                If _Request.Headers("Range") IsNot Nothing Then
                    _Response.StatusCode = 206
                    Dim range As String() = _Request.Headers("Range").Split(New Char() {"="c, "-"c})
                    startBytes = Convert.ToInt64(range(1))
                End If
                _Response.AddHeader("Content-Length", (fileLength - startBytes).ToString())
                If startBytes <> 0 Then
                    _Response.AddHeader("Content-Range", String.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength))
                End If
                _Response.AddHeader("Connection", "Keep-Alive")
                _Response.ContentType = "application/octet-stream"
                _Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(_fileName, System.Text.Encoding.UTF8))

                br.BaseStream.Seek(startBytes, SeekOrigin.Begin)
                Dim maxCount As Integer = CInt(Math.Floor(CDbl((fileLength - startBytes) / pack))) + 1

                For i As Integer = 0 To maxCount - 1
                    If _Response.IsClientConnected Then
                        _Response.BinaryWrite(br.ReadBytes(pack))
                        Thread.Sleep(sleep)
                    Else
                        i = maxCount
                    End If
                Next
            Catch
                Return False
            Finally
                br.Close()
                myFile.Close()
            End Try
        Catch
            Return False
        End Try
        Return True
    End Function

    Private Sub cleardata()
        ' txtAdmCmnts.Text = ""
        txtAssetCode.Text = ""
        txtAssetDescription.Text = ""
        txtAssetName.Text = ""
        txtAssetPrice.Text = ""
        txtMfgDate.Text = ""
        txtWarDate.Text = ""
        txtAMCDate.Text = ""
        txtPDate.Text = ""
        txtIncDate.Text = ""
        ddlVendor.SelectedIndex = 0
        ddlAssetCategory.Items.Clear()
        ddlAstSubCat.Items.Clear()
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        ddlLocation.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        ltrAMC.Visible = False
        litINC.Visible = False
        ltrAMC.Text = ""
        litINC.Text = ""
        txt_description.Text = ""
        txtAssetSerialNo.Text = ""
        txtLifeSpan.Text = ""
        txtDepriciation.Text = ""
        txtPONumber.Text = ""
        ddlAssetType.SelectedIndex = 0
        rdIns.SelectedValue = 1
        rdAMC.SelectedValue = 1
        rbtamc()
        rbtIns()

        'txtAMCDate.Visible = True
        'txtIncDate.Visible = True
        'fpBrowseAMCDoc.Visible = True
        'fpBrowseIncDoc.Visible = True       
        'ddlAsset.SelectedIndex = 0
        'ddlAssetCategory.SelectedIndex = 0
        'ddlBrand.SelectedIndex = 0
        'ddlStatus.SelectedIndex = 0
        'ddlTaxTypeID.SelectedIndex = 0


    End Sub

    Private Sub clearall()
        ' txtAdmCmnts.Text = ""
        txtAssetCode.Text = ""
        txtAssetDescription.Text = ""
        txtAssetName.Text = ""
        txtAssetPrice.Text = ""
        txtMfgDate.Text = ""
        txtWarDate.Text = ""
        txtAMCDate.Text = ""
        txtPDate.Text = ""
        txtIncDate.Text = ""
        ddlVendor.SelectedIndex = 0
        ddlAssetCategory.SelectedIndex = 0
        ddlAstSubCat.SelectedIndex = 0
        ddlAstBrand.SelectedIndex = 0
        ddlModel.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        ddlTower.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlDepartment.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        ltrAMC.Visible = False
        litINC.Visible = False
        ltrAMC.Text = ""
        litINC.Text = ""
        txt_description.Text = ""
        txtAssetSerialNo.Text = ""
        txtLifeSpan.Text = ""
        txtDepriciation.Text = ""
        txtPONumber.Text = ""
        ddlAssetType.SelectedIndex = 1
        rdIns.SelectedValue = 1
        rdAMC.SelectedValue = 1
        rbtamc()
        rbtIns()
        TextBox12.Text = ""
        TxtBox_INVOICE.Text = ""
        'txtAMCDate.Visible = True
        'txtIncDate.Visible = True
        'fpBrowseAMCDoc.Visible = True
        'fpBrowseIncDoc.Visible = True       
        'ddlAsset.SelectedIndex = 0
        'ddlAssetCategory.SelectedIndex = 0
        'ddlBrand.SelectedIndex = 0
        'ddlStatus.SelectedIndex = 0
        'ddlTaxTypeID.SelectedIndex = 0


    End Sub

    Private Sub getlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation")
        sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select--"))
    End Sub
    Private Sub getAssetTypes()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSET_TYPES")
        ddlAssetType.DataSource = sp.GetReader
        ddlAssetType.DataTextField = "TAG_NAME"
        ddlAssetType.DataValueField = "TAG_CODE"
        ddlAssetType.DataBind()
        ddlAssetType.Items.Insert(0, New ListItem("--Select--"))
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        lblMsg.Text = ""
        txtAssetName.Text = ""
        ddlAstSubCat.ClearSelection()
        ddlAstBrand.ClearSelection()
        ddlModel.ClearSelection()
        If ddlAssetCategory.SelectedItem.Value <> "--Select--" Then
            getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
            'Dim asttype As Integer = checkassettype()
            'If asttype = 1 Then
            '    'Consumable
            '    pnlAst.Visible = False
            'Else
            '    'Not
            '    pnlAst.Visible = True

            'End If
            pnlAst.Visible = False

        End If
        If Session("DepMethod") = 2 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_DEPRECIATION_VALUE")
            sp.Command.AddParameter("@CAT_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ''If ds.Tables(0).Rows(0).Item("VT_DEPR") > 0 Then
            ''    txtDepriciation.Text = ds.Tables(0).Rows(0).Item("VT_DEPR")
            ''    txtDepriciation.Enabled = False
            ''Else
            ''    txtDepriciation.Text = 0
            ''    txtDepriciation.Enabled = True
            ''End If

        End If

    End Sub
    'Checking the asset type if it is consumable or non consumable
    Public Function checkassettype() As Integer
        Dim astconstatus As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSETTYPE")
        sp.Command.AddParameter("@VT_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
        astconstatus = sp.ExecuteScalar()
        Return astconstatus
    End Function

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@companyid", Session("companyid"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        lblMsg.Visible = False
        If ddlAstSubCat.SelectedIndex <> 0 Then
            getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        End If
    End Sub

    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub

    Private Sub getTowersByLocations(ByVal lcmCode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_TOWER_BY_LOCATION")
        sp.Command.AddParameter("@LCM_CODE", lcmCode, DbType.String)
        'sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlTower.DataSource = sp.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub
    Private Sub getFloorsByLocationTower(ByVal lcmCode As String, ByVal twrCode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FLOOR_BY_LOC_TWR")
        sp.Command.AddParameter("@LCM_CODE", lcmCode, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrCode, DbType.String)
        ddlFloor.DataSource = sp.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex <> 0 Then
            getmakebycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value, ddlAstBrand.SelectedItem.Value)
            'ddlLocation.SelectedIndex = 1
        End If
    End Sub

    Private Sub getmakebycatsubcat(cat_code As String, subcat_code As String, brand_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", cat_code, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", subcat_code, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", brand_code, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()

        'ddlLocation.SelectedIndex = 1
        ddlModel.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        getTowersByLocations(ddlLocation.SelectedItem.Value)
        NewAssetCode()
    End Sub

    Public Sub NewAssetCode()
        lblMsg.Text = ""

        If ddlLocation.SelectedIndex <> 0 And ddlAssetCategory.SelectedIndex <> 0 And ddlAstSubCat.SelectedIndex <> 0 And ddlAstBrand.SelectedIndex <> 0 And ddlModel.SelectedIndex <> 0 Then
            Dim AstCode As String = ddlLocation.SelectedItem.Value + "/" + ddlAssetCategory.Text + "/" + ddlAstSubCat.Text + "/" + ddlAstBrand.Text + "/" + ddlModel.Text + "/" + CStr(GetMaxAssetId())
            txtAssetCode.Text = AstCode
            'txtAssetName.Text = AstCode
        End If
        GetAssetLable()


    End Sub


    Private Sub GetAssetLable()
        Try

            If ddlLocation.SelectedIndex <> 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_LABLE")
                sp.Command.AddParameter("@AAT_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@AAT_AAG_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@AAT_SUB_CODE", ddlAstSubCat.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@AAT_AAB_CODE", ddlAstBrand.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@AAT_MODEL_NAME", ddlModel.SelectedItem.Value, DbType.String)
                Dim ds As New DataSet
                ds = sp.GetDataSet
                If ds.Tables(0).Rows.Count > 0 Then
                    txtAssetName.Text = ds.Tables(0).Rows(0).Item("AAT_NAME").ToString
                End If
            Else
                cleardata()

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function GetMaxAssetId() As Integer
        Dim AstId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ASSET_GetMaxAAT_ID")
        If Integer.TryParse(sp.ExecuteScalar, AstId) Then
        Else
            AstId = 0
        End If
        Return AstId
    End Function

    Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendor.SelectedIndexChanged
        lblMsg.Visible = False
        If ddlVendor.SelectedItem.Value <> "--Select--" Then

            'getassetcategory(ddlVendor.SelectedItem.Value)
            getassetpricebyvendor()
        End If
    End Sub

    Private Sub getvendors()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VENDORS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
        lblMsg.Visible = False
        getassetprice()
        ddlLocation.ClearSelection()
        txtAssetName.Text = ""
        'ddlLocation.SelectedIndex = 1
    End Sub
    'Checking the asset type if it is consumable or non consumable
    Public Function getassetprice() As Decimal
        Dim getassetp As Decimal
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VENDERRATE")
        sp.Command.AddParameter("@AMG_VENCAN_VENID", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCOT_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_BRID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_MDLID", ddlModel.SelectedItem.Value, DbType.String)
        getassetp = sp.ExecuteScalar()
        'ddlLocation.SelectedIndex = 1
        'Dim ds As New DataSet
        'ds = sp.ExecuteScalar()
        'If ds.Tables(0).Rows.Count - 1 > 0 Then
        '    getassetp = ds.Tables(0).Rows(0).Item("AMG_VENCATN_RT")
        'End If
        If getassetp = 0.0 Then
            getassetp = 0.0
            lblMsg.Visible = False
            txtAssetPrice.Text = ""
            lblMsg.Text = "This model does not have a price listed; speak with the administrator...."
        Else
            txtAssetPrice.Text = getassetp
        End If

        Return getassetp
    End Function

    Public Function getassetpricebyvendor() As Decimal
        Dim getassetp As Decimal
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VENDERRATE")
        sp.Command.AddParameter("@AMG_VENCAN_VENID", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCOT_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_BRID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AMG_VENCON_MDLID", ddlModel.SelectedItem.Value, DbType.String)
        getassetp = sp.ExecuteScalar()
        'Dim ds As New DataSet
        'ds = sp.ExecuteScalar()
        'If ds.Tables(0).Rows.Count - 1 > 0 Then
        '    getassetp = ds.Tables(0).Rows(0).Item("AMG_VENCATN_RT")
        'End If
        ''If getassetp = 0.0 Then
        ''    getassetp = 0.0
        ''    lblMsg.Visible = True
        ''    txtAssetPrice.Text = ""
        ''    lblMsg.Text = "There is no price or rate of contract added for this selected criteria, Please contact administrator..."
        ''    btnSubmit.Enabled = False
        ''Else
        ''    txtAssetPrice.Text = getassetp
        ''    btnSubmit.Enabled = True
        ''End If

        Return getassetp
    End Function

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx")
    End Sub

    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)

                ' Check the number of rows in the Excel file
                Dim rowCount As Integer = GetExcelRowCount(filepath)

                If rowCount > 500 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "The uploaded Excel file contains more than 500 rows. Please upload a file with 500 rows or fewer."
                    Return
                End If

                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only"
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                Dim dt As New System.Data.DataTable()
                con.Open()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1
                Dim locationid_cnt As Integer = 1
                Dim category_codecnt As Integer = 1
                Dim category_namecnt As Integer = 1
                Dim status_cnt As Integer = 1
                Dim remarks_cnt As Integer = 1
                'Dim dmn_idcnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim ds2 As New DataSet
                Dim da As New OleDbDataAdapter
                Dim tab As New DataTable

                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset vendor" Then
                            locationid_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Vendor"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "asset category" Then
                            category_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Category"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "asset sub category" Then
                            category_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Subcategory"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "asset model" Then
                            status_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Model"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "asset brand" Then
                            remarks_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Brand"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "asset location" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Location"
                            Exit Sub

                        ElseIf LCase(ds.Tables(0).Columns(7).ToString) <> "asset cost" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Cost"

                            Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(8).ToString) <> "purchaseddate" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be PURCHASED DATE(MM/DD/YYYY)"
                        ElseIf LCase(ds.Tables(0).Columns(8).ToString) <> "tower" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Tower"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(9).ToString) <> "floor" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Floor"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(10).ToString) <> "asset description" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Description"

                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(11).ToString) <> "asset serial no" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset Serial No"

                            Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(10).ToString) <> "aat_name" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_NAME"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(10).ToString) <> "aat_desc" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_DESC"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(12).ToString) <> "aat_ast_slvgval" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_AST_SLVGVAL"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(13).ToString) <> "aat_ins_reqd" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_INS_REQD"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(14).ToString) <> "aat_amc_reqd" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_AMC_REQD"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(15).ToString) <> "aat_amc_date" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_AMC_DATE"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(16).ToString) <> "aat_ins_date" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_INS_DATE"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(17).ToString) <> "aat_wrnty_date" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_WRNTY_DATE"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(18).ToString) <> "aat_sta_id" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_STA_ID"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(19).ToString) <> "aat_ast_serialno" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_AST_SERIALNO"
                            '    Exit Sub

                            'ElseIf LCase(ds.Tables(0).Columns(20).ToString) <> "aat_life_span" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_LIFE_SPAN"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(21).ToString) <> "aat_po_number" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_PO_NUMBER"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(22).ToString) <> "aat_depriciation" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_DEPRICIATION"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(23).ToString) <> "aat_ast_status" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_AST_STATUS"
                            '    Exit Sub
                            '    'ElseIf LCase(ds.Tables(0).Columns(24).ToString) <> "Tower" Then
                            '    '    lblMsg.Visible = True
                            '    '    lblMsg.Text = "Column name should be AAT_TOWER_CODE"
                            '    '    '    Exit Sub
                            '    'ElseIf LCase(ds.Tables(0).Columns(25).ToString) <> "Floor" Then
                            '    '    lblMsg.Visible = True
                            '    '    lblMsg.Text = "Column name should be AAT_FLOOR_CODE"
                            '    '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(26).ToString) <> "aat_lob" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be AAT_LOB"
                            '    Exit Sub
                            '    'ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "domain_head_id" Then
                            '    '    dmn_idcnt = 0
                            '    '    lblMsg.Visible = True
                            '    '    lblMsg.Text = "Column name should be DOMAIN_HEAD_ID"
                            '    '    Exit Sub
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                    'Start inseting excel data
                    'Dim ValidateData As Integer
                    'ValidateData = ValidateCategory(ds.Tables(0).Rows(i).Item("CATEGORY_CODE").ToString, ds.Tables(0).Rows(i).Item("LOCATION_ID").ToString)
                    'If ValidateData = 1 Then
                    Dim AstCode As String = ds.Tables(0).Rows(i).Item("Asset Location").ToString + "/" + ds.Tables(0).Rows(i).Item("Asset Category").ToString + "/" + ds.Tables(0).Rows(i).Item("Asset Sub Category").ToString + "/" + ds.Tables(0).Rows(i).Item("Asset Brand").ToString + "/" + ds.Tables(0).Rows(i).Item("Asset Model").ToString + "/" + CStr(GetMaxAssetId())
                    'txtAssetCode.Text = AstCode
                    'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[AST_AMG_ASSET_Insert_Master]")
                    'Don't confuse Here we are passing the names(Location name,AssetName not like codes) to that parameters @AAT_AAG_CODE,@AAT_SUB_CODE'
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[AST_AMG_ASSET_Insert_Master_excel]")
                    sp.Command.AddParameter("@AAT_CODE", AstCode, DbType.String)
                    sp.Command.AddParameter("@AAT_NAME", ds.Tables(0).Rows(i).Item("SNO").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_MODEL_ID", ds.Tables(0).Rows(i).Item("Asset Model").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_MODEL_NAME", ds.Tables(0).Rows(i).Item("Asset Model").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_AAG_CODE", ds.Tables(0).Rows(i).Item("Asset Category").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_SUB_CODE", ds.Tables(0).Rows(i).Item("Asset Sub Category").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_VENDOR", ds.Tables(0).Rows(i).Item("Asset Vendor").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_AAB_CODE", ds.Tables(0).Rows(i).Item("Asset Brand").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_DESCRIPTION", ds.Tables(0).Rows(i).Item("Asset Description").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_AST_SERIALNO", ds.Tables(0).Rows(i).Item("Asset Serial No").ToString, DbType.String)
                    'sp.Command.AddParameter("@AAT_AMC_REQD", ds.Tables(0).Rows(i).Item("AAT_AMC_REQD").ToString, DbType.Int32)
                    'sp.Command.AddParameter("@AAT_AMC_DATE", ds.Tables(0).Rows(i).Item("AAT_AMC_DATE").ToString, DbType.Date)
                    'sp.Command.AddParameter("@AAT_AMC_DOCPATH", "", DbType.String)
                    'sp.Command.AddParameter("@AAT_INS_REQD", ds.Tables(0).Rows(i).Item("AAT_INS_REQD").ToString, DbType.Int32)
                    'sp.Command.AddParameter("@AAT_INS_DATE", ds.Tables(0).Rows(i).Item("AAT_INS_DATE").ToString, DbType.Date)
                    'sp.Command.AddParameter("@AAT_INS_DOCPATH", "", DbType.String)
                    sp.Command.AddParameter("@AAT_AST_COST", ds.Tables(0).Rows(i).Item("Asset Cost").ToString, DbType.Double)
                    'sp.Command.AddParameter("@AAT_AST_SLVGVAL", ds.Tables(0).Rows(i).Item("AAT_AST_SLVGVAL").ToString, DbType.Double)
                    ''sp.Command.AddParameter("@AAT_UPT_DT", getoffsetdatetime(DateTime.Now), DbType.DateTime)
                    sp.Command.AddParameter("@AAT_UPT_BY", Session("UID"), DbType.String)
                    'sp.Command.AddParameter("@AAT_STA_ID", ds.Tables(0).Rows(i).Item("AAT_STA_ID").ToString, DbType.Int32)
                    'sp.Command.AddParameter("@AAT_OWNED", 1, DbType.Int32)
                    'sp.Command.AddParameter("@AAT_PURCHASED_STATUS", 1, DbType.Int32)
                    'sp.Command.AddParameter("@AAT_SPC_FIXED", 1, DbType.Int32)
                    'sp.Command.AddParameter("@AAT_USR_MOVABLE", 0, DbType.Int32)
                    sp.Command.AddParameter("@AAT_AST_CONS", 0, DbType.Int32)
                    'sp.Command.AddParameter("@AAT_DESC", ds.Tables(0).Rows(i).Item("AAT_DESC").ToString, DbType.String)
                    'sp.Command.AddParameter("@CONF_TYPE", "", DbType.String)
                    'sp.Command.AddParameter("@AAT_MFG_DATE", ds.Tables(0).Rows(i).Item("AAT_MFG_DATE").ToString, DbType.String)
                    'sp.Command.AddParameter("@AAT_WRNTY_DATE", ds.Tables(0).Rows(i).Item("AAT_WRNTY_DATE").ToString, DbType.String)
                    'sp.Command.AddParameter("@PURCHASEDDATE", ds.Tables(0).Rows(i).Item("PURCHASEDDATE").ToString, DbType.String)
                    'sp.Command.AddParameter("@AAT_AST_SERIALNO", ds.Tables(0).Rows(i).Item("AAT_AST_SERIALNO").ToString, DbType.String)
                    ''sp.Command.AddParameter("@LIFESPAN", ds.Tables(0).Rows(i).Item("AAT_LIFE_SPAN").ToString, DbType.String)
                    'sp.Command.AddParameter("@PONUMBER", ds.Tables(0).Rows(i).Item("AAT_PO_NUMBER").ToString, DbType.String)
                    ''sp.Command.AddParameter("@DEPRECIATION", ds.Tables(0).Rows(i).Item("AAT_DEPRICIATION").ToString, DbType.Double)
                    'sp.Command.AddParameter("@ASTYPE", ds.Tables(0).Rows(i).Item("AAT_AST_STATUS").ToString, DbType.Int32)
                    sp.Command.AddParameter("@AAT_TOWER_CODE", ds.Tables(0).Rows(i).Item("Tower").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_FLOOR_CODE", ds.Tables(0).Rows(i).Item("Floor").ToString, DbType.String)
                    sp.Command.AddParameter("@AAT_LOC_ID", ds.Tables(0).Rows(i).Item("Asset Location").ToString, DbType.String)
                    'sp.Command.AddParameter("@AAT_INVOICE_DATE", ds.Tables(0).Rows(i).Item("AAT_INVOICE_DATE").ToString, DbType.String)
                    'sp.Command.AddParameter("@AAT_INVOICE_NUM", ds.Tables(0).Rows(i).Item("AAT_INVOICE_NUM").ToString, DbType.String)
                    'sp.Command.AddParameter("@LOB", ds.Tables(0).Rows(i).Item("AAT_LOB").ToString, DbType.String)
                    sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)

                    sp.ExecuteScalar()

                    'End If

                    'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMT_ASSET_SPACE_INSERT")
                    'sp1.Command.AddParameter("@AAS_AAT_CODE", AstCode, DbType.String)
                    'sp1.Command.AddParameter("@AAS_BDG_ID", "", DbType.String)
                    'sp1.Command.AddParameter("@AAS_FLR_ID", ds.Tables(0).Rows(i).Item("Floor"), DbType.String)
                    'sp1.Command.AddParameter("@AAS_PROC_TYPE", 0, DbType.String)
                    'sp1.Command.AddParameter("@AAS_REMARKS", "EXCEL_UPLOAD_MASTER", DbType.String)
                    'sp1.Command.AddParameter("@AAS_LOC_ID", ds.Tables(0).Rows(i).Item("Asset Location").ToString, DbType.String)
                    'sp1.Command.AddParameter("@AAS_REQ_ID", "EXCEL_UPLOAD_MASTER", DbType.String)
                    'sp1.Command.AddParameter("@AAS_TWR_ID", ds.Tables(0).Rows(i).Item("Tower").ToString, DbType.String)
                    'sp1.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)

                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Truncate_Asset_Temp_Table")
                Dim ds3 As New DataSet
                ds3 = sp1.GetDataSet()

                If ds3.Tables(0).Rows.Count > 0 Then
                    GridView1.DataSource = ds3.Tables(0)
                    GridView1.DataBind()
                    'btnbrowse.Visible = True
                End If
                'If ds1.Tables(0).Rows.Count > 100 Then
                '    lblMsg1.Visible = True
                '    lblMsg1.Text = "No.of users.. are more than 100. So, please wait for 10 mins to activate them."
                'End If





                lblMsg.Visible = True
                lblMsg.Text = "Data successfully uploaded, Check Remarks"
                fillgrid()
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Choose a file"
            End If

        Catch ex As System.IO.IOException
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Function GetExcelRowCount(filepath As String) As Integer
        Dim connectionstring As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
        Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
        Dim dt As New System.Data.DataTable()
        con.Open()
        dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        Dim listSheet As New List(Of String)
        Dim drSheet As DataRow
        For Each drSheet In dt.Rows
            listSheet.Add(drSheet("TABLE_NAME").ToString())
        Next
        Dim str As String = ""
        Dim sheetname As String = ""
        Dim msheet As String = ""
        Dim mfilename As String = ""
        msheet = listSheet(0).ToString()
        mfilename = msheet
        sheetname = mfilename
        str = "Select * from [" & sheetname & "]"
        Dim cmd As New OleDbCommand(str, con)
        Dim ds As New DataSet
        Dim da As New OleDbDataAdapter
        da.SelectCommand = cmd
        da.Fill(ds, sheetname.Replace("$", ""))
        con.Close()
        Return ds.Tables(0).Rows.Count
    End Function


    'Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
    '    'ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
    '    'GridView1.Visible = True
    '    'ExportPanel1.FileName = "UPLOAD_HRMS_DATA.xls"
    '    Export("Upload_Add_Capital_Asset.xls", gvDetails)
    'End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            row.Cells(0).Style.Add("mso-number-format", "\@")
            row.Cells(5).Style.Add("mso-number-format", "\@")


            PrepareControlForExport(row)
            table.Rows.Add(row)

        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)

        '  render the htmlwriter into the response
        'style to format numbers to string
        Dim style As String = "<style>.textmode{mso-number-format:\@;}</style>"
        'HttpContext.Current.Response.Write(style)
        HttpContext.Current.Response.Write(sw.ToString)
        'HttpContext.Current.Response.Write(style + sw.ToString())
        HttpContext.Current.Response.End()

        ' Response.Write(style)
        'Response.Output.Write(sw.ToString())


    End Sub
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Private Sub rbtamc()
        If Not rdAMC.SelectedValue = 1 Then
            rbnAmcDate.Visible = False
            rbnAmcUpload.Visible = False
        Else
            rbnAmcDate.Visible = True
            rbnAmcUpload.Visible = True
        End If
    End Sub

    Protected Sub rdAMC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdAMC.SelectedIndexChanged
        rbtamc()
    End Sub

    Private Sub rbtIns()
        If Not rdIns.SelectedValue = 1 Then
            rbnInsDate.Visible = False
            rbnInsUpload.Visible = False
        Else
            rbnInsDate.Visible = True
            rbnInsUpload.Visible = True
        End If
    End Sub
    Protected Sub rdIns_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdIns.SelectedIndexChanged
        rbtIns()
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlLocation.SelectedIndex <> 0 And ddlTower.SelectedIndex <> 0 Then
            getFloorsByLocationTower(ddlLocation.SelectedItem.Value, ddlTower.SelectedItem.Value)
        End If
    End Sub

    Protected Sub RdbRateContract_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RdbRateContract.SelectedIndexChanged
        If RdbRateContract.SelectedValue = 1 Then
            Div6.Visible = True
            ddlVendor.ClearSelection()
            txtAssetPrice.Visible = True
            Div7.Visible = False
        Else
            Div7.Visible = True
            Div6.Visible = False
            txtAssetPrice.Visible = True
            btnSubmit.Enabled = True
            lblMsg.Visible = False
            ddlVendor.SelectedItem.Value = "--Select--"

        End If
    End Sub

    Protected Sub txtcount_TextChanged(sender As Object, e As EventArgs) Handles txtcount.TextChanged
        If txtcount.Text = "" Then
            txtcount.Text = "1"
        End If
    End Sub

    Protected Sub gvBrand_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvBrand.RowEditing
        gvBrand.EditIndex = e.NewEditIndex
        fillgrid()
    End Sub

    Protected Sub gvBrand_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvBrand.RowCancelingEdit
        gvBrand.EditIndex = -1
        fillgrid()
    End Sub

    Protected Sub gvBrand_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvBrand.RowCommand
        Try

            If e.CommandName = "Edit" Then
                btnSubmit.Text = "Modify"
                ddlAsset.SelectedIndex = -1
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim lblAATID As Label = DirectCast(row.FindControl("lblAATID"), Label)
                AATID = Convert.ToInt32(lblAATID.Text)
                Session("AATID") = AATID
                hdnAATID.Value = AATID
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ALLASSETDATA")
                sp.Command.AddParameter("@AAT_ID", AATID, DbType.Int32)
                Dim ds As New DataSet
                ds = sp.GetDataSet
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim cat_code As String = ds.Tables(0).Rows(0).Item("AAT_AAG_CODE")
                    Dim subcat_code As String = ds.Tables(0).Rows(0).Item("AAT_SUB_CODE")
                    Dim brand_code As String = ds.Tables(0).Rows(0).Item("AAT_AAB_CODE")
                    Dim model_code As String = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
                    Dim asset_cost As String = ds.Tables(0).Rows(0).Item("AAT_AST_COST")
                    ''Dim location_id As String = ds.Tables(0).Rows(0).Item("AAT_LOC_ID")
                    Dim lcmCode As String = ds.Tables(0).Rows(0).Item("AAS_LOC_ID")
                    Dim twrCode As String = ds.Tables(0).Rows(0).Item("AAT_TOWER_CODE")
                    Dim rateContract As Integer = ds.Tables(0).Rows(0).Item("AAT_RATE_CONTRACT")

                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_STA_ID")).Selected = True
                    If rateContract = 1 Then
                        Div6.Visible = True
                        getvendors()
                        ddlVendor.ClearSelection()
                        ddlVendor.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AVR_CODE")).Selected = True
                        RdbRateContract.Text = rateContract
                    Else
                        Div6.Visible = False
                        RdbRateContract.Text = rateContract
                    End If

                    lblMsg.Text = ""
                    getassetcategories()
                    ddlAssetCategory.ClearSelection()
                    ddlAssetCategory.Items.FindByValue(cat_code).Selected = True
                    getsubcategorybycat(cat_code)
                    ddlAstSubCat.ClearSelection()
                    ddlAstSubCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_SUB_CODE")).Selected = True
                    getbrandbycatsubcat(cat_code, subcat_code)
                    ddlAstBrand.ClearSelection()
                    ddlAstBrand.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AAB_CODE")).Selected = True
                    getmakebycatsubcat(cat_code, subcat_code, brand_code)
                    ddlModel.ClearSelection()
                    ddlModel.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")).Selected = True

                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAS_LOC_ID")).Selected = True
                    txtAssetName.Text = ds.Tables(0).Rows(0).Item("AAT_CODE")
                    txtAssetCode.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
                    txtAssetDescription.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
                    txt_description.Text = ds.Tables(0).Rows(0).Item("AAT_DESCRIPTION")
                    'txtSalvgValue.Text = ds.Tables(0).Rows(0).Item("AAT_AST_SLVGVAL")
                    txtAssetPrice.Text = ds.Tables(0).Rows(0).Item("AAT_AST_COST")
                    txtAssetSerialNo.Text = ds.Tables(0).Rows(0).Item("AAT_AST_SERIALNO")
                    txtLifeSpan.Text = ds.Tables(0).Rows(0).Item("AAT_LIFE_SPAN")
                    txtDepriciation.Text = ds.Tables(0).Rows(0).Item("AAT_DEPRICIATION")
                    txtPONumber.Text = ds.Tables(0).Rows(0).Item("AAT_PO_NUMBER")
                    txtPDate.Text = ds.Tables(0).Rows(0).Item("PURCHASEDDATE")
                    TxtBox_INVOICE.Text = ds.Tables(0).Rows(0).Item("AAT_INVOICE_NUM")
                    If Convert.ToString(ds.Tables(0).Rows(0).Item("AAT_MFG_DATE")) <> "01/01/1900 00:00:00" Then
                        txtMfgDate.Text = ds.Tables(0).Rows(0).Item("AAT_MFG_DATE")
                    Else
                        txtMfgDate.Text = ""
                    End If
                    ' txtMfgDate.Text = ds.Tables(0).Rows(0).Item("AAT_MFG_DATE")
                    txtWarDate.Text = ds.Tables(0).Rows(0).Item("AAT_WRNTY_DATE")
                    ddlAssetType.ClearSelection()
                    If (ds.Tables(0).Rows(0).Item("AAT_AST_STATUS") <> "0") Then
                        ddlAssetType.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AST_STATUS")).Selected = True
                    End If

                    getTowersByLocations(lcmCode)
                    ddlTower.ClearSelection()
                    'ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_TOWER_CODE")).Selected = True
                    If ds.Tables(0).Rows(0)("AAT_TOWER_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("AAT_TOWER_CODE").ToString() IsNot Nothing Then
                        ddlTower.Items.FindByValue(ds.Tables(0).Rows(0)("AAT_TOWER_CODE").ToString()).Selected = True
                    End If

                    getFloorsByLocationTower(lcmCode, twrCode)
                    ddlFloor.ClearSelection()
                    'ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_FLOOR_CODE")).Selected = True
                    If ds.Tables(0).Rows(0)("AAT_FLOOR_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("AAT_FLOOR_CODE").ToString() IsNot Nothing Then
                        ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0)("AAT_FLOOR_CODE").ToString()).Selected = True
                    End If


                    rdAMC.ClearSelection()
                    rdAMC.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_AMC_REQD")).Selected = True
                    rdIns.ClearSelection()

                    rdIns.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAT_INS_REQD")).Selected = True
                    rbtIns()
                    If rdIns.SelectedValue = 0 Then     'ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH") = "" Or 
                        hplINC.Visible = False
                        rbnInsDate.Visible = False
                        rbnInsUpload.Visible = False
                    Else
                        'rbnAmcSDate.Visible = False
                        rbnAmcDate.Visible = False
                        rbnAmcUpload.Visible = False
                        hplINC.Visible = False
                        hplINC.Text = "Click to view document"
                        ' hplINC.NavigateUrl = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_INS_DOCPATH")
                        hplINC.Visible = False
                        litINC.Visible = True

                    End If
                    If rdAMC.SelectedValue = 0 Then     'ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") = "" Or
                        hplAMC.Visible = False
                        'rbnAmcSDate.Visible = False
                        rbnAmcDate.Visible = False
                        rbnAmcUpload.Visible = False
                    Else
                        rbnAmcDate.Visible = True
                        rbnAmcUpload.Visible = True
                        hplAMC.Visible = False
                        hplAMC.Text = "Click to view Image"

                        ' hplAMC.NavigateUrl = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH")
                        Dim filename As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH")
                        ' hplAMC.Target = "_blank"
                        ltrAMC.Visible = True
                        'ltrAMC.Text = "<a href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") & "' >Click to view document</a>"
                        ltrAMC.Text = "<a download href='../../../UploadFiles/" & ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH") & "' ></a>"
                        'fileDownload(ds.Tables(0).Rows(0).Item("AAT_AMC_DOCPATH"), Server.MapPath("~/UploadFiles/"))
                    End If
                    BindSpareparts()

                End If
            Else
                cleardata()

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Private Function AATID() As Object
    '    Throw New NotImplementedException
    'End Function

    Private Function rbnAmcSDate() As Object
        Throw New NotImplementedException
    End Function


    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSET_DETAILS_SEARCH")
        sp.Command.AddParameter("@SEARCH_CRITERIA", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvBrand.DataSource = sp.GetDataSet
        gvBrand.DataBind()
    End Sub

    Private Sub modifydata()
        Throw New NotImplementedException
    End Sub

    Private Sub BindSpareparts()
        Dim dt As New DataSet()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SPAREPARTS_DETAILS")
        sp.Command.AddParameter("@AAS_AAT_ID", Session("AATID"), DbType.Int32)
        dt = sp.GetDataSet()
        gvDetails.DataSource = dt.Tables(0)
        gvDetails.DataBind()
        ViewState("Curtbl") = dt.Tables(0)
    End Sub

    Protected Sub BindGridview()
        Dim dt As New DataTable()
        'dt.Columns.Add("rowid", GetType(Integer))
        dt.Columns.Add("SparePartID", GetType(String))
        dt.Columns.Add("SparePartName", GetType(String))
        dt.Columns.Add("Quantity", GetType(Integer))
        dt.Columns.Add("Cost", GetType(Double))
        dt.Columns.Add("TotalCost", GetType(Double))

        'Dim dr As DataRow = dt.NewRow()
        ''dr("rowid") = 1
        'dr("SparePartID") = String.Empty
        'dr("SparePartName") = String.Empty
        'dr("Quantity") = String.Empty
        'dt.Rows.Add(dr)
        ViewState("Curtbl") = dt
        gvDetails.DataSource = dt
        gvDetails.DataBind()

    End Sub
    Private Sub AddNewRow()
        Dim rowIndex As Integer = 0

        If ViewState("Curtbl") IsNot Nothing Then
            Dim dt As DataTable = DirectCast(ViewState("Curtbl"), DataTable)
            Dim drCurrentRow As DataRow = Nothing
            drCurrentRow = dt.NewRow()
            If txtSparePartId.Text = "" Then
                txtSparePartId.Text = ddlAssetCategory.SelectedItem.Value + "/" + txtSparePartName.Text
            End If

            drCurrentRow("SparePartID") = txtSparePartId.Text
            drCurrentRow("SparePartName") = txtSparePartName.Text
            drCurrentRow("Quantity") = If(txtQuantity.Text = "", 0, txtQuantity.Text)
            drCurrentRow("Cost") = If(txtspcost.Text = "", 0, txtspcost.Text)
            drCurrentRow("TotalCost") = If(txtsptcost.Text = "", 0, txtsptcost.Text)
            ''rowIndex += 1
            ''Next
            dt.Rows.Add(drCurrentRow)
            ViewState("Curtbl") = dt
            gvDetails.DataSource = dt
            gvDetails.DataBind()

            txtSparePartId.Text = ""
            txtSparePartName.Text = ""
            txtQuantity.Text = ""
            txtspcost.Text = ""
            txtsptcost.Text = ""
            ''BindGridview()
            ' End If
        Else
            Response.Write("ViewState Value is Null")
        End If
        SetOldData()
    End Sub
    Private Sub SetOldData()
        Dim rowIndex As Integer = 0
        If ViewState("Curtbl") IsNot Nothing Then
            Dim dt As DataTable = DirectCast(ViewState("Curtbl"), DataTable)
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    'Dim txtSparePartId As Label = DirectCast(gvDetails.Rows(rowIndex).Cells(1).FindControl("txtSparePartID"), Label)
                    'Dim txtSparePartName As Label = DirectCast(gvDetails.Rows(rowIndex).Cells(2).FindControl("txtSparePartName"), Label)
                    'Dim txtQuantity As Label = DirectCast(gvDetails.Rows(rowIndex).Cells(2).FindControl("txtQuantity"), Label)
                    txtSparePartId.Text = Convert.ToString(dt.Rows(i)("SparePartID"))
                    txtSparePartName.Text = Convert.ToString(dt.Rows(i)("SparePartName"))
                    txtQuantity.Text = Convert.ToInt32(dt.Rows(i)("Quantity"))
                    txtspcost.Text = Convert.ToDouble(dt.Rows(i)("Cost"))
                    txtsptcost.Text = Convert.ToDouble(dt.Rows(i)("TotalCost"))
                    'rowIndex += 1
                Next
            End If
        End If
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)
        AddNewRow()
    End Sub
    Protected Sub gvDetails_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
        If ViewState("Curtbl") IsNot Nothing Then
            Dim dt As DataTable = DirectCast(ViewState("Curtbl"), DataTable)
            Dim drCurrentRow As DataRow = Nothing
            Dim rowIndex As Integer = Convert.ToInt32(e.RowIndex)
            If dt.Rows.Count > 0 Then
                dt.Rows.Remove(dt.Rows(rowIndex))
                drCurrentRow = dt.NewRow()
                ViewState("Curtbl") = dt
                gvDetails.DataSource = dt
                gvDetails.DataBind()

                For i As Integer = 0 To gvDetails.Rows.Count - 2
                    gvDetails.Rows(i).Cells(0).Text = Convert.ToString(i + 1)
                Next
                'SetOldData()
            End If
            txtSparePartId.Text = ""
            txtSparePartName.Text = ""
            txtQuantity.Text = ""
            txtspcost.Text = ""
            txtsptcost.Text = ""
        End If
    End Sub

    Public Sub gvDetails_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvDetails.RowEditing
        gvDetails.EditIndex = e.NewEditIndex
        Dim dt As DataTable = DirectCast(ViewState("Curtbl"), DataTable)
        BindSpareparts()
    End Sub
    Private Function Ok2Delete(ByVal pRow As String) As Boolean
        If Session("RowIndex") Is Nothing OrElse
         (CInt(Session("Rowindex")) = pRow AndAlso DateTime.Now.Subtract(CDate(Session("RowIndexTimeStamp"))).Seconds < 2) Then

            Session("RowIndex") = pRow
            Session("RowIndexTimeStamp") = DateTime.Now
            Return True
        End If
        Return False
    End Function
    Public Sub gvDetails_RowUpdating(sender As Object, e As GridViewUpdateEventArgs)
        'If Not Ok2Delete(e.RowIndex) Then Return
        gvDetails.EditIndex = e.RowIndex
        Dim dt As DataTable = DirectCast(ViewState("Curtbl"), DataTable)
        Dim row As GridViewRow = CType(gvDetails.Rows(e.RowIndex), GridViewRow)

        Dim textsparepartid As TextBox = CType(row.Cells(1).Controls(0), TextBox)
        dt.Rows(e.RowIndex)("SparePartID") = textsparepartid.Text
        Dim textsparpartname As TextBox = CType(row.Cells(2).Controls(0), TextBox)
        dt.Rows(e.RowIndex)("SparePartName") = textsparpartname.Text
        Dim txtQuantity As TextBox = CType(row.Cells(3).Controls(0), TextBox)
        dt.Rows(e.RowIndex)("Quantity") = txtQuantity.Text
        Dim txtspcost As TextBox = CType(row.Cells(4).Controls(0), TextBox)
        dt.Rows(e.RowIndex)("Cost") = txtspcost.Text
        Dim txtsptcost As TextBox = CType(row.Cells(5).Controls(0), TextBox)
        dt.Rows(e.RowIndex)("TotalCost") = txtsptcost.Text
        ViewState("Curtbl") = dt
        gvDetails.EditIndex = -1
        gvDetails.DataSource = dt
        gvDetails.DataBind()


    End Sub

    Private Sub gvDetails_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles gvDetails.RowCancelingEdit
        gvDetails.EditIndex = -1
        BindSpareparts()
    End Sub

    'Private Sub txtspcost_TextChanged(sender As Object, e As EventArgs) Handles txtspcost.TextChanged


    'End Sub
End Class
