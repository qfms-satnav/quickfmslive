<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucAssetDisposalReport.ascx.vb"
    Inherits="Controls_ucAssetDisposalReport" %>
<table style="background-color: Silver;" width="100%">
    <tr>
        <td align="center">
            <table width="80%" border="0" style="background-color: White;">
                <tr>
                    <td align="center">
                        <table width="90%">
                            <tr>
                                <td align="center">
                                    <asp:Image ImageUrl="../images/welcome.gif" runat="server" ID="logo" Width="100px"
                                        Height="50px" />
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblHead" runat="server" CssClass="Heading" Width="95%" Font-Underline="False"
                                        ForeColor="Black"><H4>Asset Management Equipment Disposal Form</H4>
                                    </asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr align="center" width="100%" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="90%">
                            <tr>
                                <td align="center">
                                    <span class="text">Complete the form below if university-owned capital equipment is
                                        sold,transferred or disposed of. Capital equipment has a minimum cost of $5,000
                                        and a useful life of one year or more.Please note,<br />
                                        anytime you dispose of tagged equipment, complete the form.This form does not grant
                                        approval to dispose of equipment, it only serves to update the inventory database.
                                        You should obtain,and keep on file,written approval from your dean,chairperson or
                                        department administrator to remove capital equipment,however you don't need the
                                        approval of the university's Asset Management Accountant. </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <hr align="center" width="100%" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="90%" class="fornormaltext">
                            <tr>
                                <td>
                                    *Name:
                                    <asp:TextBox ID="txtName" runat="server" />
                                    <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="txtName"
                                        Display="None" ErrorMessage="Enter Name"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    Title:
                                    <asp:TextBox ID="txtTitle" runat="server" />
                                </td>
                                <td>
                                    *Today's Date:
                                    <asp:TextBox ID="txtTodaysdate" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTodaysdate"
                                        Display="None" ErrorMessage="Enter Date"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    *Department:
                                    <asp:TextBox ID="txtDepartment" runat="server" />
                                </td>
                                <td>
                                    *Email:
                                    <asp:TextBox ID="txtEmail" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                                        Display="None" ErrorMessage="Enter Emailid"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="re" runat="server" ControlToValidate="txtEmail"
                                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"
                                        ErrorMessage="Enter valid emailid">
                                    </asp:RegularExpressionValidator>
                                </td>
                                <td>
                                    Phone:
                                    <asp:TextBox ID="txtPhone" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <hr align="center" width="90%" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="80%" class="fornormaltext">
                            <tr>
                                <td>
                                    <span>*Type of Disposition:</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rbtntypesofdisposition" CssClass="fornormaltext" Width="700px"
                                        runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>Sold</asp:ListItem>
                                        <asp:ListItem>Scrapped</asp:ListItem>
                                        <asp:ListItem>Transfer w/in LUC</asp:ListItem>
                                        <asp:ListItem>Transfer outside LUC</asp:ListItem>
                                        <asp:ListItem>Traded-in</asp:ListItem>
                                        <asp:ListItem>Donated</asp:ListItem>
                                        <asp:ListItem>Missing</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="rbtntypesofdisposition"
                                        Display="None" ErrorMessage="Please select Type of Disposition"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    If transferred,New Location:
                                    <asp:TextBox ID="txttransferredNewLocation" runat="server" />
                                    *Equipment Disposal Date:
                                    <asp:TextBox ID="txtDisposalDate" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDisposalDate"
                                        Display="None" ErrorMessage="Enter Disposal date"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    *If Sold,Traded-in,or Donated,Recipient & Price Paid:
                                    <asp:TextBox ID="txtIfSold" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtIfSold"
                                        Display="None" ErrorMessage="Please enter If Sold,Traded-in,or Donated,Recipient & Price Paid"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Reason if Missing:
                                    <asp:TextBox ID="txtMissing" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="90%">
                                        <tr>
                                            <td align="center">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            #Tags
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag1" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag2" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag3" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag4" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            *Description(Manufacturer,Model & Serial #)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag5" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag6" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag7" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txttag8" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Following error occurs:"
                                                    ShowMessageBox="true" DisplayMode="BulletList" ShowSummary="false" />
                                                <asp:Button ID="btnsend" runat="server" Text="Send to Accounting" Width="150px" Height="30px"
                                                    BorderColor="Silver" ForeColor="black" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btnClear" runat="server" Text="Clear Form" CausesValidation="false"
                                                    Width="150px" Height="30px" BorderColor="Silver" ForeColor="black" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
