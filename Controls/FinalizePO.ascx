<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FinalizePO.ascx.vb" Inherits="Controls_FinalizePO" %>
<script type="text/javascript">
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID)
        for (i = 0; i < form1.elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'checkbox') {
                if (re.test(elm.name)) {
                    if (elm.disabled == false)
                        elm.checked = checkVal
                }
            }
        }
    }

    function ChildClick(CheckBox) {
        //Get target base & child control.
        var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
        var TargetChildControl = "chkSelect";
        //Get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        // check to see if all other checkboxes are checked
        for (var n = 0; n < Inputs.length; ++n)
            if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                // Whoops, there is an unchecked checkbox, make sure
                // that the header checkbox is unchecked
                if (!Inputs[n].checked) {
                    Inputs[0].checked = false;
                    return;
                }
            }
        // If we reach here, ALL GridView checkboxes are checked
        Inputs[0].checked = true;
    }

         <%--check box validation--%>
    function validateCheckBoxesMyReq() {
        var gridView = document.getElementById("<%=gvItems.ClientID%>");
        var checkBoxes = gridView.getElementsByTagName("input");
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                return true;
            }
        }
        alert("Please select atleast one checkbox");
        return false;
    }
</script>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>

<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />
<%--<div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">--%>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<div class="panel-heading">
    <h3 class="panel-title">Approved PO/Rejected PO</h3>
</div>
<%--<div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">--%>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width="90%" placeholder="Search By ID"></asp:TextBox>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
        <asp:Button ID="finalgridsearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
    </div>
</div>
<div class="row" style="width: 100%; min-height: 20px; max-height: 500px">
    <div class="col-md-12 col-sm-6 col-xs-12">
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <asp:GridView ID="FinalizeGrid" runat="server" AllowSorting="False" AutoGenerateColumns="false" AllowPaging="True" PageSize="6"
                    EmptyDataText="No Finalize PO(s) Found." CssClass="table GridStyle" GridLines="none">
                    <PagerSettings Mode="NumericFirstLast" />
                    <Columns>
                        <%--<asp:TemplateField HeaderText="PO ID" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="hLinkDetails" runat="server" 
                                                                Text='<%# Eval("AIP_PO_ID")%> '>
                                                            </asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                        <asp:BoundField DataField="AIP_PO_ID" HeaderText="PO ID" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="AIPD_VENDORNAME" HeaderText="Vender Name" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="AIP_PO_DATE" HeaderText="UpdatedPO Date" ItemStyle-HorizontalAlign="left" />
                        <%-- <asp:BoundField DataField="brand" HeaderText="Brand" ItemStyle-HorizontalAlign="left" />--%>
                        <%--<asp:BoundField DataField="model" HeaderText="Model" ItemStyle-HorizontalAlign="left" />--%>
                        <asp:BoundField DataField="AIPD_TOTALCOST" HeaderText="Cost" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="location" HeaderText="Location" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="category" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="Approved_by" HeaderText="Approved/Rejected By" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="AIP_APP_REM" HeaderText="Approver remarks" ItemStyle-HorizontalAlign="left" />
                        <%--<asp:BoundField DataField="subcategory" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />--%>
                        <asp:BoundField DataField="AIP_STA_ID" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="GRN_STATUS" HeaderText="GRN Status" ItemStyle-HorizontalAlign="left" />
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="finalgridsearch" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
<br />
<br />
<div class="panel-heading">
    <h3 class="panel-title">Finalize PO</h3>
</div>
<br />
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By ID"></asp:TextBox>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
        <asp:Button ID="btnsrch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
    </div>
</div>
<br />
<div class="row" style="min-height: 20px; max-height: 500px">
    <div class="col-md-12 col-sm-6 col-xs-12">
        <asp:UpdatePanel runat="server" ID="UpdatePanel7">
            <ContentTemplate>
                <asp:GridView ID="gvItems" runat="server" AllowSorting="False" AutoGenerateColumns="false" PageSize="10" AllowPaging="True"
                    EmptyDataText="No Finalize PO(s) Found." CssClass="table GridStyle" GridLines="none">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                    ToolTip="Click to check all" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="AIP_PO_ID" HeaderText="PO Id" ItemStyle-HorizontalAlign="left" />--%>
                        <asp:TemplateField HeaderText="PO ID" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIP_PO_ID", "~/FAM/FAM_WebFiles/frmPOFinalisationDetails.aspx?RID={0}")%>'
                                    Text='<%# Eval("AIP_PO_ID")%> '>
                                </asp:HyperLink>
                                <asp:Label ID="lblPoId" runat="server" Text='<%#Eval("AIP_PO_ID")%>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--    	<asp:BoundField DataField="Raised_NAME" HeaderText="Request Raised By" ItemStyle-HorizontalAlign="left" />--%>
                        <asp:BoundField DataField="Approved_by" HeaderText="L1 Approved By" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="AUR_NAME" HeaderText="PO Generated By" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="AIP_PO_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />

                        <asp:TemplateField HeaderText="Advance" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblAdvance" runat="server" Text='<%#Eval("AIPD_PLI")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AIP_STA_ID" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnsrch" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
</div>
<br />
<div class="row" id="divRemarks" runat="server">
    <div class="col-md-6">
        <div class="form-group">
            <label>Remarks<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtRemarks" Height="50px" Width="350px" runat="server" CssClass="form-control" TextMode="multiline" ValidationGroup="Val1"> </asp:TextBox>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnApproveAll" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
</div>
