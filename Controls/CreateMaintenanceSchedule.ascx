<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CreateMaintenanceSchedule.ascx.vb"
    Inherits="Controls_CreateMaintenanceSchedule" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Create Maintenance Schedule
                        </asp:Label>
                        <hr align="center" width="60%" />
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Create Maintenance Schedule</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        <table width="100%" cellpadding="2px">
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top" align="center">
                                    <asp:Panel ID="pnlContainer" runat="server" Width="100%">
                                        <asp:Panel ID="PanPremise" runat="server" Width="100%">
                                            <table border="1" width="100%" align="center">
                                                <tr>
                                                    <td align="left" width="50%">
                                                        <asp:Label ID="lblLocation" runat="server" CssClass="bodytext" Text="Select Location"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="cvLocation" runat="server" ControlToValidate="ddlLocation"
                                                            Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="0"
                                                            Enabled="true"></asp:RequiredFieldValidator>
                                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlLocation"
                                                            Display="None" ErrorMessage="Select Location" Operator="NotEqual" SetFocusOnError="True"
                                                            ValueToCompare="--Select--"></asp:CompareValidator></td>
                                                    <td align="left" width="50%">
                                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="clsComboBox" Width="99%"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="50%" style="height: 24px">
                                                        <asp:Label ID="lblTower" runat="server" CssClass="bodytext" Text="Select Tower"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfvTower" runat="server" ControlToValidate="ddlTower"
                                                            Display="None" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="0"
                                                            Enabled="true"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td align="left" width="50%" style="height: 24px">
                                                        <asp:DropDownList ID="ddlTower" runat="server" CssClass="clsComboBox" Width="99%"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="50%" style="height: 24px">
                                                        <asp:Label ID="lblFloor" runat="server" CssClass="bodytext" Text="Select Floor"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlfloor"
                                                            Display="None" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="0"
                                                            Enabled="true"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td align="left" width="50%" style="height: 24px">
                                                        <asp:DropDownList ID="ddlfloor" runat="server" CssClass="clsComboBox" Width="99%"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="bodytext" width="50%">
                                                        Assets<font class="clsNote">*</font></td>
                                                    <td width="50%">
                                                        <asp:DropDownList ID="ddlAssets" runat="server" CssClass="clsComboBox" Width="100%"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td class="bodytext" colspan="2">
                                                       
                                                        <asp:Panel ID="pnlAstList" runat="server" Visible="false"  >
                                                       
                                                       
                                                        <table border="1" width="100%" align="center">
                                                            <tr>
                                                                <td class="bodytext" colspan="2">
                                                                    Assets List <font class="clsNote">*</font></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" valign="top" align="left">
                                                                    <fieldset>
                                                                        <legend>Tagged Assets </legend>
                                                                        <asp:GridView ID="gv_astlist" runat="server" EmptyDataText="Tagged Assets not found."
                                                                            AllowPaging="true" PageSize="10" Width="100%" AutoGenerateColumns="false" OnPageIndexChanging="gv_astlist_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="left" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblChkAst" Visible="false" runat="server" Text='<%# Eval("AAT_ID") %>'></asp:Label>
                                                                                        <asp:CheckBox ID="chkAstList" runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Asset Code" ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="left"
                                                                                    ItemStyle-Width="30%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAstCode" Text='<%# Eval("AAT_CODE") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Asset Name" ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="left"
                                                                                    ItemStyle-Width="30%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAstName" Text='<%# Eval("AAT_MODEL_NAME") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Employee" ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="left"
                                                                                    ItemStyle-Width="30%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblKnownAs" Text='<%# Eval("AUR_KNOWN_AS") %>' runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" valign="top" align="left">
                                                                    <fieldset>
                                                                        <legend>Untagged Assets </legend>
                                                                        <asp:GridView ID="gvItems" runat="server" EmptyDataText="Untagged Assets not found."
                                                                            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Left" AllowPaging="True"
                                                                            AllowSorting="false" PageSize="10" AutoGenerateColumns="false" OnPageIndexChanging="gvItems_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="left" ItemStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblChkAst" Visible="false" runat="server" Text='<%# Eval("AAT_ID") %>'></asp:Label>
                                                                                        <asp:CheckBox ID="chkAstList" runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Assets Code" ItemStyle-HorizontalAlign="left" ItemStyle-Width="200px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAssetsCode" runat="server" Text='<%#Eval("AAT_Code") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Assets" ItemStyle-HorizontalAlign="left" ItemStyle-Width="200px">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAssets" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                   
                                                   
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="bodytext" width="50%">
                                                        Vendor<font class="clsNote">*
                                                            <asp:CompareValidator ID="Comparevalidator10" runat="server" Operator="NotEqual"
                                                                ControlToValidate="ddlVendors" ErrorMessage="Please Select Vendor!" ValueToCompare="--Select--"
                                                                Display="None"></asp:CompareValidator></font></td>
                                                    <td width="50%">
                                                        <asp:DropDownList ID="ddlVendors" runat="server" CssClass="clsComboBox" Width="100%"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="panAssetService" runat="server" Width="100%">
                                            <asp:Panel ID="pnlSub" runat="server" Width="100%">
                                                <asp:Panel ID="pnlCon" runat="server" Width="100%" Visible="false">
                                                    <table id="tblDate" border="1" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                        <tr width="100%">
                                                            <td class="label" width="25%">
                                                                <b>Contract Start Date</b></td>
                                                            <td width="25%">
                                                                <asp:TextBox ID="txtConStart" runat="server"></asp:TextBox>
                                                                <cc1:CalendarExtender TargetControlID="txtConStart" ID="CalExttxtConStart" runat="server">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                            <td classs="label" width="25%">
                                                                <b>Contract To Date</b></td>
                                                            <td width="25%">
                                                                <asp:TextBox ID="txtConTo" runat="server"></asp:TextBox>
                                                                <cc1:CalendarExtender TargetControlID="txtConTo" ID="CalExttxtConTo" runat="server">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="panPeriod" runat="server" Visible="false" Width="100%">
                                                    <table border="1" width="100%" align="center">
                                                        <tr>
                                                            <td width="17%">
                                                                <asp:RadioButton ID="radDaily" runat="server" CssClass="clsRadioButton" Text="Daily"
                                                                    AutoPostBack="True"></asp:RadioButton></td>
                                                            <td width="17%">
                                                                <asp:RadioButton ID="radWeekly" runat="server" CssClass="clsRadioButton" Text="Weekly"
                                                                    AutoPostBack="True"></asp:RadioButton></td>
                                                            <td width="17%">
                                                                <asp:RadioButton ID="radMonthly" runat="server" CssClass="clsRadioButton" Text="Monthly"
                                                                    AutoPostBack="True"></asp:RadioButton></td>
                                                            <td width="17%">
                                                                <asp:RadioButton ID="radBiMonthly" runat="server" CssClass="clsRadioButton" Text="Bimonthly"
                                                                    AutoPostBack="True"></asp:RadioButton></td>
                                                            <td width="17%">
                                                                <asp:RadioButton ID="radQuarterly" runat="server" CssClass="clsRadioButton" Text="Quarterly"
                                                                    AutoPostBack="True"></asp:RadioButton></td>
                                                            <td width="15%">
                                                                <asp:RadioButton ID="radYearly" runat="server" CssClass="clsRadioButton" Text="Yearly"
                                                                    AutoPostBack="True"></asp:RadioButton></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="panAllPeriods" runat="server" Visible="False">
                                                    <asp:Panel ID="panWeekly" runat="server" Width="100%">
                                                        <table border="1" width="100%" align="center">
                                                            <tr>
                                                                <td class="bodytext" width="50%">
                                                                    Day of the Week</td>
                                                                <td width="50%">
                                                                    <asp:DropDownList ID="cboWWeek" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        AutoPostBack="True">
                                                                        <asp:ListItem Value="2">Monday</asp:ListItem>
                                                                        <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                                                        <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                                                        <asp:ListItem Value="5">Thursday</asp:ListItem>
                                                                        <asp:ListItem Value="6">Friday</asp:ListItem>
                                                                        <asp:ListItem Value="7">Saturday</asp:ListItem>
                                                                        <asp:ListItem Value="1">Sunday</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="panMonthly" runat="server" Width="100%">
                                                        <table border="1" width="100%" align="center">
                                                            <tr>
                                                                <td class="bodytext" width="50%">
                                                                    Day of the Month</td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboMDate" runat="server" CssClass="clsComboBox" Width="100%">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="panBiMonthly" runat="server" Width="100%">
                                                        <table border="1" width="100%" align="center">
                                                            <tr>
                                                                <td class="bodytext" width="50%">
                                                                    Month</td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboBMonth" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        AutoPostBack="True">
                                                                        <asp:ListItem Value="1">January</asp:ListItem>
                                                                        <asp:ListItem Value="2">Febuary</asp:ListItem>
                                                                        <asp:ListItem Value="3">March</asp:ListItem>
                                                                        <asp:ListItem Value="4">April</asp:ListItem>
                                                                        <asp:ListItem Value="5">May</asp:ListItem>
                                                                        <asp:ListItem Value="6">June</asp:ListItem>
                                                                        <asp:ListItem Value="7">July</asp:ListItem>
                                                                        <asp:ListItem Value="8">August</asp:ListItem>
                                                                        <asp:ListItem Value="9">September</asp:ListItem>
                                                                        <asp:ListItem Value="10">October</asp:ListItem>
                                                                        <asp:ListItem Value="11">November</asp:ListItem>
                                                                        <asp:ListItem Value="12">December</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="bodytext" width="50%">
                                                                    Day</td>
                                                                <td>
                                                                    <asp:DropDownList ID="cboBday" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="panQuarterly" runat="server" Width="100%">
                                                        <table border="1" width="100%" align="center">
                                                            <tr>
                                                                <td class="clsTblHead" width="25%">
                                                                    First Quarter</td>
                                                                <td class="clsTblHead" width="25%">
                                                                    Second Quarter</td>
                                                                <td class="clsTblHead" width="25%">
                                                                    Third Quarter</td>
                                                                <td class="clsTblHead" width="25%">
                                                                    Fourth Quarter</td>
                                                            </tr>
                                                            <tr>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ1Month" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        Height="31px" AutoPostBack="True">
                                                                        <asp:ListItem Value="4">April</asp:ListItem>
                                                                        <asp:ListItem Value="5">May</asp:ListItem>
                                                                        <asp:ListItem Value="6">June</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ2Month" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        Height="31px" AutoPostBack="True">
                                                                        <asp:ListItem Value="7">July</asp:ListItem>
                                                                        <asp:ListItem Value="8">August</asp:ListItem>
                                                                        <asp:ListItem Value="9">September</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ3Month" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        Height="31px" AutoPostBack="True">
                                                                        <asp:ListItem Value="10">October</asp:ListItem>
                                                                        <asp:ListItem Value="11">November</asp:ListItem>
                                                                        <asp:ListItem Value="12">December</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ4Month" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        Height="31px" AutoPostBack="True">
                                                                        <asp:ListItem Value="1">January</asp:ListItem>
                                                                        <asp:ListItem Value="2">Febuary</asp:ListItem>
                                                                        <asp:ListItem Value="3">March</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ1Date" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        Height="31px" AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ2Date" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        Height="31px" AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ3Date" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        Height="31px" AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboQ4Date" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="panYearly" runat="server" Width="100%">
                                                        <table border="1" width="100%" align="center">
                                                            <tr>
                                                                <td class="bodytext" width="25%">
                                                                    Month</td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboYMonth" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        AutoPostBack="True">
                                                                        <asp:ListItem Value="1">January</asp:ListItem>
                                                                        <asp:ListItem Value="2">Febuary</asp:ListItem>
                                                                        <asp:ListItem Value="3">March</asp:ListItem>
                                                                        <asp:ListItem Value="4">April</asp:ListItem>
                                                                        <asp:ListItem Value="5">May</asp:ListItem>
                                                                        <asp:ListItem Value="6">June</asp:ListItem>
                                                                        <asp:ListItem Value="7">July</asp:ListItem>
                                                                        <asp:ListItem Value="8">August</asp:ListItem>
                                                                        <asp:ListItem Value="9">September</asp:ListItem>
                                                                        <asp:ListItem Value="10">October</asp:ListItem>
                                                                        <asp:ListItem Value="11">November</asp:ListItem>
                                                                        <asp:ListItem Value="12">December</asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                <td class="bodytext" width="25%">
                                                                    Day</td>
                                                                <td width="25%">
                                                                    <asp:DropDownList ID="cboYDate" runat="server" CssClass="clsComboBox" Width="100%"
                                                                        AutoPostBack="True">
                                                                    </asp:DropDownList></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="panDaily" runat="server" Width="100%">
                                                        <table border="1" width="100%" align="center">
                                                            <tr>
                                                                <td class="bodytext" width="50%">
                                                                    Time (HH:MM)</td>
                                                                <td width="50%" align="left">
                                                                    <asp:DropDownList ID="cboHour" runat="server" CssClass="clsComboBox" Width="35%">
                                                                    </asp:DropDownList>
                                                                    <asp:DropDownList ID="cboMin" runat="server" CssClass="clsComboBox" Width="35%">
                                                                        <asp:ListItem Value="00">00</asp:ListItem>
                                                                        <asp:ListItem Value="15">15</asp:ListItem>
                                                                        <asp:ListItem Value="30">30</asp:ListItem>
                                                                        <asp:ListItem Value="45">45</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <table border="1" width="100%" align="center">
                                                        <tr>
                                                            <td class="bodytext" width="20%">
                                                                From Date<font class="clsNote">* </font>
                                                            </td>
                                                            <td width="30%">
                                                                <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                                                <cc1:CalendarExtender TargetControlID="txtFromDate" ID="CalExttxtFromDate" runat="server">
                                                                </cc1:CalendarExtender>
                                                                <a onclick="return getCal('Form1','txtFromDate');" href="#"></a>
                                                            </td>
                                                            <td class="bodytext" width="20%">
                                                                To Date<font class="clsNote">* </font>
                                                            </td>
                                                            <td width="30%">
                                                                <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                                                                <cc1:CalendarExtender TargetControlID="txtToDate" ID="CalExttxtToDate" runat="server">
                                                                </cc1:CalendarExtender>
                                                                <a onclick="return getCal('Form1','txtToDate');" href="#"></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                    <br>
                                                    <table id="Table1" border="1" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit"></asp:Button></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                        height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                            width="9" /></td>
                    <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                            width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
        <asp:PostBackTrigger ControlID="txtConStart" />
    </Triggers>
</asp:UpdatePanel>
