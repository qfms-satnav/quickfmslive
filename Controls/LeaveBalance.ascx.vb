Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_LeaveBalance
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim Sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEE_LEAVE")
            Sp.Command.AddParameter("@EMP_ID", Session("UID"), DbType.String)
            ddlID.DataSource = Sp.GetDataSet()
            ddlID.DataTextField = "EMP_NAME"
            ddlID.DataValueField = "EMP_NO"
            ddlID.DataBind()
            ddlID.Items.Insert(0, New ListItem("--Select--", "--Select--"))
            'ddlID.Items.Add(Session("UID"))
            pnl1.Visible = False
            pnl2.Visible = False
            txtAssociateName.ReadOnly = True
            txtBalPaidLs.ReadOnly = True
            txtBalCom.ReadOnly = True
            txtBalPM.ReadOnly = True
            txtDesig.ReadOnly = True
            txtDept.ReadOnly = True
            txtRM.ReadOnly = True
            txtContactNo.ReadOnly = True
        End If
    End Sub

    Protected Sub ddlID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlID.SelectedIndexChanged
        If ddlID.SelectedIndex > 0 Then
            BindDetails()
            pnl1.Visible = True
            pnl2.Visible = True
        Else
            txtAssociateName.Text = ""
            txtDesig.Text = ""
            txtDept.Text = ""
            txtRM.Text = ""
            txtContactNo.Text = ""
            pnl1.Visible = False
            pnl2.Visible = False
        End If
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GETASSOCIATE_DETAILS1")
            sp.Command.AddParameter("@AUR_ID", ddlID.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                txtDept.Text = ds.Tables(0).Rows(0).Item("AUR_DEP_ID")
                txtRM.Text = ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")
                txtContactNo.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
                lblTodayDate.Text = ds.Tables(0).Rows(0).Item("TODAY_DATE")
                lblTdate.Text = ds.Tables(0).Rows(0).Item("TODAY_DATE")
                lblToDate.Text = ds.Tables(0).Rows(0).Item("TODAY_DATE")
                txtBalPaidLs.Text = ds.Tables(0).Rows(0).Item("BAL_PAID_LEAVES")
                txtBalCom.Text = ds.Tables(0).Rows(0).Item("BAL_COM_LEAVES")

                Dim gender As String = ds.Tables(0).Rows(0).Item("AUR_GENDER")
                If gender = "MALE" Then
                    lblbal.Text = "Balance Paternity Leaves As On "
                    txtBalPM.Text = ds.Tables(0).Rows(0).Item("BAL_PM")
                Else
                    lblbal.Text = "Balance Maternity Leaves As On"
                    txtBalPM.Text = ds.Tables(0).Rows(0).Item("BAL_MM")
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
