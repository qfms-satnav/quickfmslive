Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_CreateMaintenanceSchedule
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    'PVMCreate.aspx.vb
    Dim rid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            cboHour.Items.Clear()
            Dim i
            For i = 0 To 23
                cboHour.Items.Add(i)
            Next
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindLocation()
                'BindAssets()
                BindVendors()

            End If
            
        End If
    End Sub
    Private Sub BindLocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Public Sub BindAssets()
        ObjSubsonic.Binddropdown(ddlAssets, "USP_GET_ALLASTS", "PRODUCTNAME", "SKU")

        'Dim param(2) As SqlParameter
        'param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 100)
        'param(0).Value = ddlLocation.SelectedItem.Value
        'param(1) = New SqlParameter("@TOW", SqlDbType.NVarChar, 100)
        'param(1).Value = ddlTower.SelectedItem.Value
        'param(2) = New SqlParameter("@FLR", SqlDbType.NVarChar, 100)
        'param(2).Value = ddlfloor.SelectedItem.Value
        'ObjSubsonic.BindGridView(gv_astlist, "GET_ASTS", param)



    End Sub

    Public Sub BindVendors()
        ObjSubsonic.Binddropdown(ddlVendors, "USP_GETALLVENDORS", "AVR_NAME", "AVR_CODE")
    End Sub
    Private Sub BindTower()
        Try

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
            param(0).Value = ddlLocation.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlTower, "GET_LOCTWR", "TWR_NAME", "TWR_CODE", param)
 
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex > 0 Then
                BindTower()

            Else
                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlTower.SelectedValue = 0
                ddlfloor.Items.Clear()
                ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlfloor.SelectedValue = 0

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindFloor()
        Try


            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
            param(0).Value = ddlLocation.SelectedItem.Value
            param(1) = New SqlParameter("@dummy1", SqlDbType.NVarChar, 100)
            param(1).Value = ddlTower.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlfloor, "GET_TWRFLR", "FLR_NAME", "FLR_CODE", param)

 
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex > 0 Then
                BindFloor()


            Else
                ddlfloor.Items.Clear()
                ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlfloor.SelectedValue = 0


            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlfloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlfloor.SelectedIndexChanged
        Try
            If ddlfloor.SelectedIndex > 0 Then
                BindAssets()
                

            Else


            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub radDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDaily.CheckedChanged

        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = True

        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = False
        panDaily.Visible = True

        cboHour.Items.Clear()
        Dim i
        For i = 0 To 23
            cboHour.Items.Add(i)
        Next

    End Sub


    Private Sub radWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radWeekly.CheckedChanged

        radWeekly.Checked = True
        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = False

        panAllPeriods.Visible = True
        panWeekly.Visible = True
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = False
        'panDaily.Visible = False

    End Sub


    Private Sub radMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radMonthly.CheckedChanged

        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = True
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = False

        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = True
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = False
        'panDaily.Visible = False

        Dim i
        cboMDate.Items.Clear()
        For i = 1 To 31
            cboMDate.Items.Add(i)
        Next

    End Sub


    Private Sub radBiMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radBiMonthly.CheckedChanged

        radWeekly.Checked = False
        radBiMonthly.Checked = True
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = False

        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = True
        panQuarterly.Visible = False
        panYearly.Visible = False
        'panDaily.Visible = False

        Dim i
        cboBday.Items.Clear()
        For i = 1 To 31
            cboBday.Items.Add(i)
        Next

    End Sub


    Private Sub radQuarterly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radQuarterly.CheckedChanged

        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = True
        radYearly.Checked = False
        radDaily.Checked = False

        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = True
        panYearly.Visible = False
        'panDaily.Visible = False

        Dim i
        cboQ1Date.Items.Clear()
        For i = 1 To 30
            cboQ1Date.Items.Add(i)
        Next

        cboQ2Date.Items.Clear()
        For i = 1 To 31
            cboQ2Date.Items.Add(i)
        Next

        cboQ3Date.Items.Clear()
        For i = 1 To 31
            cboQ3Date.Items.Add(i)
        Next

        cboQ4Date.Items.Clear()
        For i = 1 To 31
            cboQ4Date.Items.Add(i)
        Next

    End Sub


    Private Sub radYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radYearly.CheckedChanged

        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = True
        radDaily.Checked = False

        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = True
        'panDaily.Visible = False

        Dim i
        cboYDate.Items.Clear()
        For i = 1 To 31
            cboYDate.Items.Add(i)
        Next


    End Sub

    Private Function GenerateRequestId(ByVal UserId As String) As String
        Dim ReqId As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_INTRA_MVMT_REQ_GetMaxMMR_ID")
        Dim SNO As String = CStr(sp.ExecuteScalar())

        Dim mvalue As String
        mvalue = getoffsetdatetime(DateTime.Now).ToString().Replace("/", "").Replace("AM", "").Replace("PM", "").Replace(":", "").Replace(" ", "").Replace("#", "")

        ReqId = "MMS/" + UserId + "/" + "REQ" + "/" + mvalue
        Return ReqId
    End Function


    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If Page.IsValid Then
            Dim ChkCount As Integer = 0
            Dim ChkTagged As Integer = 0
            Dim ChkUntagged As Integer = 0


            For Each row As GridViewRow In gv_astlist.Rows
                Dim chkAstList As CheckBox = DirectCast(row.FindControl("chkAstList"), CheckBox)
                Dim lblChkAst As Label = DirectCast(row.FindControl("lblChkAst"), Label)
                If chkAstList.Checked = True Then
                    ChkTagged += 1
                End If
            Next

            For Each row As GridViewRow In gvItems.Rows
                Dim chkAstList As CheckBox = DirectCast(row.FindControl("chkAstList"), CheckBox)
                Dim lblChkAst As Label = DirectCast(row.FindControl("lblChkAst"), Label)
                If chkAstList.Checked = True Then
                    ChkUntagged += 1
                End If
            Next

            ChkCount = ChkTagged + ChkUntagged

            If ChkCount = 0 Then
                PopUpMessageNormal("Select at list one Asset.", Me.Page)
                Exit Sub
            End If

            If ddlVendors.SelectedItem.Text = "--Select--" Then
                PopUpMessageNormal("Select a Vendor.", Me.Page)
                Exit Sub
            End If
            If txtConStart.Text = "" Then
                PopUpMessageNormal("Select Contract Start Date.", Me.Page)
                Exit Sub
            End If
            If txtConTo.Text = "" Then
                PopUpMessageNormal("Select Contract End Date.", Me.Page)
                Exit Sub
            End If
            If txtFromDate.Text = "" Then
                PopUpMessageNormal("Select Plan From Date.", Me.Page)
                Exit Sub
            End If
            If txtToDate.Text = "" Then
                PopUpMessageNormal("Select Plan End Date.", Me.Page)
                Exit Sub
            End If
            If CDate(txtFromDate.Text) < CDate(txtConStart.Text) Then
                PopUpMessageNormal("From Date Should Be within in the Contract Period", Me.Page)
                Exit Sub
            ElseIf CDate(txtFromDate.Text) > CDate(txtConTo.Text) Then
                PopUpMessageNormal("From Date Should Be within in the Contract Period ", Me.Page)
                Exit Sub
            ElseIf CDate(txtToDate.Text) > CDate(txtConTo.Text) Then
                PopUpMessageNormal("To Date Should Be within in the Contract Period ", Me.Page)
                Exit Sub
            ElseIf CDate(txtToDate.Text) < CDate(txtConStart.Text) Then
                PopUpMessageNormal("To Date Should Be within in the Contract Period ", Me.Page)
                Exit Sub
            End If


            Dim strUid As String = Session("Uid")
            rid = GenerateRequestId(strUid)
            If Page.IsValid = True Then
                Dim dateflag
                dateflag = Check_Dates()
                If dateflag = 1 Then
                    Dim Planflag
                    Planflag = Check_Plan_Id(rid)
                    If Planflag = 1 Then
                        Dim assetflag
                        assetflag = Check_Assets()
                        If assetflag = 1 Then
                            Dim insertflag As Boolean
                            Dim schdtime As String
                            Dim fd, td, maindate, tempdate As Date
                            Dim diffmm, i, j, diffyy, dd, mm, yy, dd1, mm1, yy1, dd2, mm2, dd3, mm3, dd4, mm4, flag, ast_Id
                            Dim selradfor, selradtype As String
                            Dim selradfreq As String = ""
                            Dim prmid, catid, ctr As Integer
                            Dim flrid, wngig As Integer
                            selradfor = "Office"
                            prmid = 0
                            flrid = 0
                            wngig = 0
                            selradtype = "Asset"
                            catid = 0



                            ctr = ChkCount



                            If radYearly.Checked = True Then
                                selradfreq = "Yearly"
                            ElseIf radQuarterly.Checked = True Then
                                selradfreq = "Quarterly"
                            ElseIf radBiMonthly.Checked = True Then
                                selradfreq = "BiMonthly"
                            ElseIf radMonthly.Checked = True Then
                                selradfreq = "Monthly"
                            ElseIf radWeekly.Checked = True Then
                                selradfreq = "Weekly"
                            ElseIf radDaily.Checked = True Then
                                selradfreq = "Daily"
                            End If

                            fd = CDate(txtFromDate.Text)
                            td = CDate(txtToDate.Text)
                            'schdtime = "00:00"
                            schdtime = cboHour.SelectedItem.Value & ":" & cboMin.SelectedItem.Value
                            'For j = 0 To ctr - 1




                            'Dim str As String = ddlAssets.SelectedItem.Text  ddlAssets.SelectedItem.Value


                            Dim paramM_Insert(17) As SqlParameter
                            paramM_Insert(0) = New SqlParameter("@PVM_BKG_TS", SqlDbType.NVarChar, 200)
                            paramM_Insert(0).Value = rid
                            paramM_Insert(1) = New SqlParameter("@PVM_PLAN_ID", SqlDbType.NVarChar, 200)
                            paramM_Insert(1).Value = rid
                            paramM_Insert(2) = New SqlParameter("@PVM_PLAN_FOR", SqlDbType.NVarChar, 200)
                            paramM_Insert(2).Value = selradfor
                            paramM_Insert(3) = New SqlParameter("@PVM_BDG_ID", SqlDbType.Int)
                            paramM_Insert(3).Value = prmid
                            paramM_Insert(4) = New SqlParameter("@PVM_TWR_ID", SqlDbType.Int)
                            paramM_Insert(4).Value = wngig
                            paramM_Insert(5) = New SqlParameter("@PVM_PLAN_TYPE", SqlDbType.NVarChar, 200)
                            paramM_Insert(5).Value = selradtype
                            paramM_Insert(6) = New SqlParameter("@PVM_PLANTYPE_ID", SqlDbType.Int)
                            paramM_Insert(6).Value = catid
                            paramM_Insert(7) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                            paramM_Insert(7).Value = selradfreq
                            paramM_Insert(8) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                            paramM_Insert(8).Value = fd
                            paramM_Insert(9) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                            paramM_Insert(9).Value = td
                            paramM_Insert(10) = New SqlParameter("@PVM_AVR_ID", SqlDbType.NVarChar, 200)
                            paramM_Insert(10).Value = ddlVendors.SelectedItem.Value
                            paramM_Insert(11) = New SqlParameter("@PVM_AVR_NAME", SqlDbType.NVarChar, 200)
                            paramM_Insert(11).Value = ddlVendors.SelectedItem.Text
                            paramM_Insert(12) = New SqlParameter("@PVM_PLANSTA_ID", SqlDbType.Int)
                            paramM_Insert(12).Value = 1
                            paramM_Insert(13) = New SqlParameter("@PVM_alterD_DT", SqlDbType.DateTime)
                            paramM_Insert(13).Value = getoffsetdatetime(DateTime.Now)
                            paramM_Insert(14) = New SqlParameter("@PVM_UPDT_DT", SqlDbType.DateTime)
                            paramM_Insert(14).Value = getoffsetdatetime(DateTime.Now)
                            paramM_Insert(15) = New SqlParameter("@PVM_UPDT_BY", SqlDbType.NVarChar, 200)
                            paramM_Insert(15).Value = strUid
                            paramM_Insert(16) = New SqlParameter("@PVM_FLR_ID", SqlDbType.Int)
                            paramM_Insert(16).Value = flrid
                            paramM_Insert(17) = New SqlParameter("@PVM_ASSET_CODE", SqlDbType.NVarChar, 200)
                            paramM_Insert(17).Value = "" 'str

                            ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_MAIN", paramM_Insert)

                            'ASSET CODE STATUS UPDATE

                            For Each row As GridViewRow In gv_astlist.Rows
                                Dim chkAstList As CheckBox = DirectCast(row.FindControl("chkAstList"), CheckBox)
                                Dim lblChkAst As Label = DirectCast(row.FindControl("lblChkAst"), Label)
                                Dim lblAstCode As Label = DirectCast(row.FindControl("lblAstCode"), Label)

                                If chkAstList.Checked = True Then
                                    ChkCount += 1

                                    Dim str As String = lblAstCode.Text
                                    If radWeekly.Checked = True Then
                                        flag = 1
                                        tempdate = fd
                                        While flag = 1
                                            If cboWWeek.SelectedItem.Value = DatePart(DateInterval.Weekday, tempdate) Then
                                                flag = 2
                                            Else
                                                tempdate = DateAdd(DateInterval.Day, 1, tempdate)
                                            End If
                                        End While
                                        fd = tempdate
                                        maindate = fd
                                        For i = 1 To DateDiff(DateInterval.Day, fd, td)
                                            If maindate >= fd And maindate <= td Then
                                                Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
                                            End If
                                            maindate = DateAdd(DateInterval.Day, 7, maindate)
                                        Next

                                    ElseIf radMonthly.Checked = True Then

                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm = Month(fd)
                                        yy = Year(fd)
                                        dd = cboMDate.SelectedItem.Value

                                        If mm = 12 Then
                                            mm1 = 1
                                            yy1 = yy + 1
                                        Else
                                            mm1 = mm + 1
                                            yy1 = yy
                                        End If

                                        For i = 0 To diffmm

                                            maindate = mm & "/1/" & yy
                                            tempdate = mm1 & "/1/" & yy1
                                            If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                                maindate = mm & "/" & dd & "/" & yy
                                                If maindate >= fd And maindate <= td Then
                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
 
                                                End If
                                            End If

                                            If mm = 12 Then
                                                mm = 1
                                                yy = yy + 1
                                            Else
                                                mm = mm + 1
                                            End If

                                            If mm1 = 12 Then
                                                mm1 = 1
                                                yy1 = yy1 + 1
                                            Else
                                                mm1 = mm1 + 1
                                            End If
                                        Next

                                    ElseIf radBiMonthly.Checked = True Then
                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm = cboBMonth.SelectedItem.Value
                                        yy = Year(fd)
                                        dd = cboBday.SelectedItem.Value

                                        If mm = 12 Then
                                            mm1 = 1
                                            yy1 = yy + 1
                                        Else
                                            mm1 = mm + 1
                                            yy1 = yy
                                        End If

                                        For i = 0 To diffmm

                                            maindate = mm & "/1/" & yy
                                            tempdate = mm1 & "/1/" & yy1
                                            If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                                maindate = mm & "/" & dd & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)

                                                     


                                                End If
                                            End If

                                            If mm = 12 Then
                                                mm = 2
                                                yy = yy + 1
                                            ElseIf mm = 11 Then
                                                mm = 1
                                                yy = yy + 1
                                            Else
                                                mm = mm + 2
                                            End If

                                            If mm = 12 Then
                                                mm1 = 1
                                                yy1 = yy + 1
                                            Else
                                                mm1 = mm + 1
                                                yy1 = yy
                                            End If
                                        Next

                                    ElseIf radQuarterly.Checked = True Then
                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm1 = cboQ1Month.SelectedItem.Value
                                        dd1 = cboQ1Date.SelectedItem.Value

                                        mm2 = cboQ2Month.SelectedItem.Value
                                        dd2 = cboQ2Date.SelectedItem.Value

                                        mm3 = cboQ3Month.SelectedItem.Value
                                        dd3 = cboQ3Date.SelectedItem.Value

                                        mm4 = cboQ4Month.SelectedItem.Value
                                        dd4 = cboQ4Date.SelectedItem.Value

                                        mm = Month(fd)
                                        yy = Year(fd)
                                        For i = 0 To diffmm

                                            If mm = mm1 Then
                                                maindate = mm1 & "/" & dd1 & "/" & yy
                                                If maindate >= fd And maindate <= td Then


                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
 
                                                End If
                                            End If
                                            If mm = mm2 Then
                                                maindate = mm2 & "/" & dd2 & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
 
                                                End If
                                            End If
                                            If mm = mm3 Then
                                                maindate = mm3 & "/" & dd3 & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
 

                                                End If
                                            End If
                                            If mm = mm4 Then
                                                maindate = mm4 & "/" & dd4 & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
 
                                                End If
                                            End If

                                            If mm = 12 Then
                                                mm = 1
                                                yy = yy + 1
                                            Else
                                                mm = mm + 1
                                            End If
                                        Next

                                    ElseIf radYearly.Checked = True Then
                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm = cboYMonth.SelectedItem.Value
                                        yy = Year(fd)
                                        dd = cboYDate.SelectedItem.Value

                                        mm1 = cboYMonth.SelectedItem.Value

                                        For i = 0 To diffmm

                                            If mm = mm1 Then
                                                maindate = mm & "/" & dd & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)

                                                  

                                                End If
                                            End If

                                            If mm1 = 12 Then
                                                mm1 = 1
                                                yy = yy + 1
                                            Else
                                                mm1 = mm1 + 1
                                            End If
                                        Next

                                    ElseIf radDaily.Checked = True Then
                                        maindate = fd
                                        schdtime = cboHour.SelectedItem.Value
                                        For i = 0 To DateDiff(DateInterval.Day, fd, td)
                                            If maindate >= fd And maindate <= td Then

                                                Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
 

                                            End If
                                            maindate = DateAdd(DateInterval.Day, 1, maindate)
                                        Next
                                    End If



                                End If
                            Next






                            '******************** Untagged Assets ********************************


                            For Each row As GridViewRow In gvItems.Rows
                                Dim chkAstList As CheckBox = DirectCast(row.FindControl("chkAstList"), CheckBox)
                                Dim lblChkAst As Label = DirectCast(row.FindControl("lblChkAst"), Label)
                                Dim lblAstCode As Label = DirectCast(row.FindControl("lblAssetsCode"), Label)

                                If chkAstList.Checked = True Then
                                    ChkCount += 1

                                    Dim str As String = lblAstCode.Text
                                    If radWeekly.Checked = True Then
                                        flag = 1
                                        tempdate = fd
                                        While flag = 1
                                            If cboWWeek.SelectedItem.Value = DatePart(DateInterval.Weekday, tempdate) Then
                                                flag = 2
                                            Else
                                                tempdate = DateAdd(DateInterval.Day, 1, tempdate)
                                            End If
                                        End While
                                        fd = tempdate
                                        maindate = fd
                                        For i = 1 To DateDiff(DateInterval.Day, fd, td)
                                            If maindate >= fd And maindate <= td Then
                                                Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)
                                            End If
                                            maindate = DateAdd(DateInterval.Day, 7, maindate)
                                        Next

                                    ElseIf radMonthly.Checked = True Then

                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm = Month(fd)
                                        yy = Year(fd)
                                        dd = cboMDate.SelectedItem.Value

                                        If mm = 12 Then
                                            mm1 = 1
                                            yy1 = yy + 1
                                        Else
                                            mm1 = mm + 1
                                            yy1 = yy
                                        End If

                                        For i = 0 To diffmm

                                            maindate = mm & "/1/" & yy
                                            tempdate = mm1 & "/1/" & yy1
                                            If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                                maindate = mm & "/" & dd & "/" & yy
                                                If maindate >= fd And maindate <= td Then
                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)

                                                End If
                                            End If

                                            If mm = 12 Then
                                                mm = 1
                                                yy = yy + 1
                                            Else
                                                mm = mm + 1
                                            End If

                                            If mm1 = 12 Then
                                                mm1 = 1
                                                yy1 = yy1 + 1
                                            Else
                                                mm1 = mm1 + 1
                                            End If
                                        Next

                                    ElseIf radBiMonthly.Checked = True Then
                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm = cboBMonth.SelectedItem.Value
                                        yy = Year(fd)
                                        dd = cboBday.SelectedItem.Value

                                        If mm = 12 Then
                                            mm1 = 1
                                            yy1 = yy + 1
                                        Else
                                            mm1 = mm + 1
                                            yy1 = yy
                                        End If

                                        For i = 0 To diffmm

                                            maindate = mm & "/1/" & yy
                                            tempdate = mm1 & "/1/" & yy1
                                            If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                                maindate = mm & "/" & dd & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)




                                                End If
                                            End If

                                            If mm = 12 Then
                                                mm = 2
                                                yy = yy + 1
                                            ElseIf mm = 11 Then
                                                mm = 1
                                                yy = yy + 1
                                            Else
                                                mm = mm + 2
                                            End If

                                            If mm = 12 Then
                                                mm1 = 1
                                                yy1 = yy + 1
                                            Else
                                                mm1 = mm + 1
                                                yy1 = yy
                                            End If
                                        Next

                                    ElseIf radQuarterly.Checked = True Then
                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm1 = cboQ1Month.SelectedItem.Value
                                        dd1 = cboQ1Date.SelectedItem.Value

                                        mm2 = cboQ2Month.SelectedItem.Value
                                        dd2 = cboQ2Date.SelectedItem.Value

                                        mm3 = cboQ3Month.SelectedItem.Value
                                        dd3 = cboQ3Date.SelectedItem.Value

                                        mm4 = cboQ4Month.SelectedItem.Value
                                        dd4 = cboQ4Date.SelectedItem.Value

                                        mm = Month(fd)
                                        yy = Year(fd)
                                        For i = 0 To diffmm

                                            If mm = mm1 Then
                                                maindate = mm1 & "/" & dd1 & "/" & yy
                                                If maindate >= fd And maindate <= td Then


                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)

                                                End If
                                            End If
                                            If mm = mm2 Then
                                                maindate = mm2 & "/" & dd2 & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)

                                                End If
                                            End If
                                            If mm = mm3 Then
                                                maindate = mm3 & "/" & dd3 & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)


                                                End If
                                            End If
                                            If mm = mm4 Then
                                                maindate = mm4 & "/" & dd4 & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)

                                                End If
                                            End If

                                            If mm = 12 Then
                                                mm = 1
                                                yy = yy + 1
                                            Else
                                                mm = mm + 1
                                            End If
                                        Next

                                    ElseIf radYearly.Checked = True Then
                                        If Year(td) <> Year(fd) Then
                                            diffmm = 12 - Month(fd)
                                            diffyy = Year(td) - Year(fd)
                                            If diffyy = 1 Then
                                                diffmm = diffmm + Month(td)
                                            Else
                                                diffmm = diffmm + (diffyy * 12) + Month(td)
                                            End If
                                        Else
                                            diffmm = Month(td) - Month(fd)
                                        End If

                                        mm = cboYMonth.SelectedItem.Value
                                        yy = Year(fd)
                                        dd = cboYDate.SelectedItem.Value

                                        mm1 = cboYMonth.SelectedItem.Value

                                        For i = 0 To diffmm

                                            If mm = mm1 Then
                                                maindate = mm & "/" & dd & "/" & yy
                                                If maindate >= fd And maindate <= td Then

                                                    Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)



                                                End If
                                            End If

                                            If mm1 = 12 Then
                                                mm1 = 1
                                                yy = yy + 1
                                            Else
                                                mm1 = mm1 + 1
                                            End If
                                        Next

                                    ElseIf radDaily.Checked = True Then
                                        maindate = fd
                                        schdtime = cboHour.SelectedItem.Value
                                        For i = 0 To DateDiff(DateInterval.Day, fd, td)
                                            If maindate >= fd And maindate <= td Then

                                                Insert_FAM_Maintenance_Details(rid, catid, str, maindate, schdtime, 1, getoffsetdatetime(DateTime.Now), strUid)


                                            End If
                                            maindate = DateAdd(DateInterval.Day, 1, maindate)
                                        Next
                                    End If



                                End If
                            Next

                            '*********************************************************************




















                            Dim group As String = Trim(ddlVendors.SelectedItem.Text) 'cboGP.SelectedItem.Text

                            'Response.Redirect("frmPVMFinalpage.aspx?staid=submitted&rid=" & rid & "&group=" & group)
                            Response.Redirect("frmAssetThanks.aspx?rid=MaintSc&MReqId=" & rid)
                        Else
                            Response.Write("<script language=javascript>alert(""Plan Already Created"")</script>")
                        End If
                    Else
                        Response.Write("<script language=javascript>alert(""Plan Id Already Submited"")</script>")
                    End If
                Else
                    Response.Write("<script language=javascript>alert(""Please Select The Valid Dates"")</script>")
                End If
            End If
        End If
    End Sub
    Public Sub Insert_FAM_Maintenance_Details(ByVal Mrid As String, ByVal Mcatid As String, ByVal Mstr As String, ByVal Mmaindate As String, ByVal Mschdtime As String, ByVal MSta_id As String, ByVal MTodayDate As String, ByVal MstrUid As String)

        Dim paramM_InsertDETAILS(7) As SqlParameter

        paramM_InsertDETAILS(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
        paramM_InsertDETAILS(0).Value = Mrid
        paramM_InsertDETAILS(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
        paramM_InsertDETAILS(1).Value = Mcatid

        paramM_InsertDETAILS(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
        paramM_InsertDETAILS(2).Value = Mstr

        paramM_InsertDETAILS(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
        paramM_InsertDETAILS(3).Value = Mmaindate

        paramM_InsertDETAILS(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
        paramM_InsertDETAILS(4).Value = Mschdtime

        paramM_InsertDETAILS(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
        paramM_InsertDETAILS(5).Value = 1

        paramM_InsertDETAILS(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
        paramM_InsertDETAILS(6).Value = getoffsetdatetime(DateTime.Now)

        paramM_InsertDETAILS(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
        paramM_InsertDETAILS(7).Value = MstrUid

        ObjSubsonic.GetSubSonicExecute("FAM_INSRT_PVM_DTLS", paramM_InsertDETAILS)

    End Sub
    Private Function Check_Assets()
        Dim assetflag
        assetflag = 1
        Dim fd, td As Date
        fd = CDate(txtFromDate.Text)
        td = CDate(txtToDate.Text)
        Dim str As String
        str = ddlAssets.SelectedItem.Text
        Dim ChkCount As Integer = 0

        For Each row As GridViewRow In gv_astlist.Rows
            Dim chkAstList As CheckBox = DirectCast(row.FindControl("chkAstList"), CheckBox)
            Dim lblChkAst As Label = DirectCast(row.FindControl("lblChkAst"), Label)
            Dim lblAstCode As Label = DirectCast(row.FindControl("lblAstCode"), Label)

            If chkAstList.Checked = True Then
                ChkCount += 1
                str = lblAstCode.Text
                Dim param_chkAst(6) As SqlParameter
                param_chkAst(0) = New SqlParameter("@PVM_BDG_ID", SqlDbType.Int)
                param_chkAst(0).Value = 0
                param_chkAst(1) = New SqlParameter("@PVM_FLR_ID", SqlDbType.Int)
                param_chkAst(1).Value = 0
                param_chkAst(2) = New SqlParameter("@PVM_TWR_ID", SqlDbType.Int)
                param_chkAst(2).Value = 0
                param_chkAst(3) = New SqlParameter("@pvm_asset_code", SqlDbType.NVarChar, 200)
                param_chkAst(3).Value = str
                param_chkAst(4) = New SqlParameter("@FDATE", SqlDbType.DateTime)
                param_chkAst(4).Value = fd
                param_chkAst(5) = New SqlParameter("@TDATE", SqlDbType.DateTime)
                param_chkAst(5).Value = td
                param_chkAst(6) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                param_chkAst(6).Value = str
                ObjDR = ObjSubsonic.GetSubSonicDataReader("FAM_GET_PVM_PLANID", param_chkAst)
                If ObjDR.Read() Then
                    assetflag = 0
                End If
                ObjDR.Close()
            End If
        Next
        Check_Assets = assetflag
    End Function

    Private Function Check_Plan_Id(ByVal planid As String)

        Dim insertflag
        insertflag = 1

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = planid

        Dim cnt As Integer

        ObjDR = ObjSubsonic.GetSubSonicDataReader("CHK_MAIN_REQ_ID", param)
        If ObjDR.Read() Then
            cnt = ObjDR.Item("CNT")
        End If
        If cnt > 0 Then
            insertflag = 0
        End If
        

        Check_Plan_Id = insertflag

    End Function


    Private Function Check_Dates()

        Dim fd, td, maindate, tempdate As Date
        Dim diffmm, i, j, diffyy, dd, mm, yy, dd1, mm1, yy1, dd2, mm2, dd3, mm3, dd4, mm4, flag, dateflag

        fd = CDate(txtFromDate.Text)
        td = CDate(txtToDate.Text)
        dateflag = 0

        If radWeekly.Checked = True Then
            flag = 1
            tempdate = fd
            While flag = 1
                If cboWWeek.SelectedItem.Value = DatePart(DateInterval.Weekday, tempdate) Then
                    flag = 2
                Else
                    tempdate = DateAdd(DateInterval.Day, 1, tempdate)
                End If
            End While
            fd = tempdate
            maindate = fd
            For i = 1 To DateDiff(DateInterval.Day, fd, td)
                If maindate >= fd And maindate <= td Then
                    dateflag = 1
                    Exit For
                End If
                maindate = DateAdd(DateInterval.Day, 7, maindate)
            Next

        ElseIf radMonthly.Checked = True Then

            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm = Month(fd)
            yy = Year(fd)
            dd = cboMDate.SelectedItem.Value

            If mm = 12 Then
                mm1 = 1
                yy1 = yy + 1
            Else
                mm1 = mm + 1
                yy1 = yy
            End If

            For i = 0 To diffmm

                maindate = mm & "/1/" & yy
                tempdate = mm1 & "/1/" & yy1
                If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                    maindate = mm & "/" & dd & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm = 12 Then
                    mm = 1
                    yy = yy + 1
                Else
                    mm = mm + 1
                End If

                If mm1 = 12 Then
                    mm1 = 1
                    yy1 = yy1 + 1
                Else
                    mm1 = mm1 + 1
                End If
            Next

        ElseIf radBiMonthly.Checked = True Then
            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm = cboBMonth.SelectedItem.Value
            yy = Year(fd)
            dd = cboBday.SelectedItem.Value

            If mm = 12 Then
                mm1 = 1
                yy1 = yy + 1
            Else
                mm1 = mm + 1
                yy1 = yy
            End If

            For i = 0 To diffmm

                maindate = mm & "/1/" & yy
                tempdate = mm1 & "/1/" & yy1
                If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                    maindate = mm & "/" & dd & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm = 12 Then
                    mm = 2
                    yy = yy + 1
                ElseIf mm = 11 Then
                    mm = 1
                    yy = yy + 1
                Else
                    mm = mm + 2
                End If

                If mm = 12 Then
                    mm1 = 1
                    yy1 = yy + 1
                Else
                    mm1 = mm + 1
                    yy1 = yy
                End If
            Next

        ElseIf radQuarterly.Checked = True Then
            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm1 = cboQ1Month.SelectedItem.Value
            dd1 = cboQ1Date.SelectedItem.Value

            mm2 = cboQ2Month.SelectedItem.Value
            dd2 = cboQ2Date.SelectedItem.Value

            mm3 = cboQ3Month.SelectedItem.Value
            dd3 = cboQ3Date.SelectedItem.Value

            mm4 = cboQ4Month.SelectedItem.Value
            dd4 = cboQ4Date.SelectedItem.Value

            mm = Month(fd)
            yy = Year(fd)
            For i = 0 To diffmm

                If mm = mm1 Then
                    maindate = mm1 & "/" & dd1 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If
                If mm = mm2 Then
                    maindate = mm2 & "/" & dd2 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If
                If mm = mm3 Then
                    maindate = mm3 & "/" & dd3 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If
                If mm = mm4 Then
                    maindate = mm4 & "/" & dd4 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm = 12 Then
                    mm = 1
                    yy = yy + 1
                Else
                    mm = mm + 1
                End If
            Next

        ElseIf radYearly.Checked = True Then
            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm = cboYMonth.SelectedItem.Value
            yy = Year(fd)
            dd = cboYDate.SelectedItem.Value

            mm1 = cboYMonth.SelectedItem.Value

            For i = 0 To diffmm

                If mm = mm1 Then
                    maindate = mm & "/" & dd & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm1 = 12 Then
                    mm1 = 1
                    yy = yy + 1
                Else
                    mm1 = mm1 + 1
                End If
            Next

        ElseIf radDaily.Checked = True Then
            maindate = fd
            For i = 0 To DateDiff(DateInterval.Day, fd, td)
                If maindate >= fd And maindate <= td Then
                    dateflag = 1
                    Exit For
                End If
                maindate = DateAdd(DateInterval.Day, 1, maindate)
            Next
        End If
        Check_Dates = dateflag

    End Function

    Private Sub cboYMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYMonth.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboYMonth.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If


        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)

        cboYDate.Items.Clear()

        For i = 1 To dd
            cboYDate.Items.Add(i)
        Next

    End Sub


    Private Sub cboBMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBMonth.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboBMonth.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If


        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)

        cboBday.Items.Clear()
        For i = 1 To dd
            cboBday.Items.Add(i)

        Next

    End Sub


    Private Sub cboQ1Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ1Month.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ1Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If


        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)

        cboQ1Date.Items.Clear()
        For i = 1 To dd
            cboQ1Date.Items.Add(i)
        Next

    End Sub


    Private Sub cboQ2Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ2Month.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ2Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If

        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)
        cboQ2Date.Items.Clear()
        For i = 1 To dd
            cboQ2Date.Items.Add(i)
        Next

    End Sub

    Private Sub cboQ3Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ3Month.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ3Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If


        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)

        cboQ3Date.Items.Clear()
        For i = 1 To dd
            cboQ3Date.Items.Add(i)
        Next

    End Sub

    Private Sub cboQ4Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ4Month.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ4Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then

            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If

        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)

        cboQ4Date.Items.Clear()
        For i = 1 To dd
            cboQ4Date.Items.Add(i)
        Next

    End Sub

    Protected Sub ddlVendors_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendors.SelectedIndexChanged
        If ddlVendors.SelectedIndex > 0 Then
            pnlCon.Visible = True
            panPeriod.Visible = True
        Else
            pnlCon.Visible = False
            panPeriod.Visible = False
        End If

    End Sub

    Protected Sub ddlAssets_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssets.SelectedIndexChanged
        FillAstList(ddlAssets.SelectedItem.Value)
    End Sub

    Private Sub FillAstList(ByVal AstCode As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@ASTCODE", SqlDbType.NVarChar, 200)
        param(0).Value = AstCode
        ObjSubsonic.BindGridView(gv_astlist, "GET_ASTLIST_By_ASTCODE", param)

        If gv_astlist.Rows.Count = 0 Then
            gv_astlist.DataSource = Nothing
            gv_astlist.DataBind()
            
        End If
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASTS")
        'sp.Command.AddParameter("@loc", ddlLocation.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@tow", ddlTower.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@flr", ddlfloor.SelectedItem.Value, DbType.String)
        'gvItems.DataSource = sp.GetDataSet()
        'gvItems.DataBind()

        Dim param1(3) As SqlParameter
        param1(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
        param1(0).Value = ddlLocation.SelectedItem.Value
        param1(1) = New SqlParameter("@TOW", SqlDbType.NVarChar, 200)
        param1(1).Value = ddlTower.SelectedItem.Value
        param1(2) = New SqlParameter("@FLR", SqlDbType.NVarChar, 200)
        param1(2).Value = ddlfloor.SelectedItem.Value
        param1(3) = New SqlParameter("@AST", SqlDbType.NVarChar, 200)
        param1(3).Value = ddlAssets.SelectedItem.Value
        ObjSubsonic.BindGridView(gvItems, "GET_UNTAGGED_AST", param1)
        If gvItems.Rows.Count = 0 Then
            gvItems.DataSource = Nothing
            gvItems.DataBind()

        End If
       
        pnlAstList.visible = True

    End Sub

    Protected Sub gv_astlist_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gv_astlist.PageIndex = e.NewPageIndex
        FillAstList(ddlAssets.SelectedItem.Value)
    End Sub

    
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    End Sub
End Class
