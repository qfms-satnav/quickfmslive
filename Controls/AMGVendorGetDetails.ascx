<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGVendorGetDetails.ascx.vb"
    Inherits="Controls_AMGVendorGetDetails" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row" style="padding-left:10px">
                <label class="col-md-2 control-label">Vendor Name<span style="color: red;">*</span></label>
                <label class="col-md-3 control-label">
                    <asp:TextBox ID="txtfindcode" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rffindcode" runat="server" ControlToValidate="txtfindcode"
                        ErrorMessage="Please enter Asset Vendor name!" ValidationGroup="Val1" SetFocusOnError="True"
                        Display="none"></asp:RequiredFieldValidator>
                </label>
                <div class="col-md-4">
                <%--<label class="col-md-4 control-label">--%>
                    <asp:Button ID="btnfincode" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" ValidationGroup="Val1" />
                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"  />&nbsp;&nbsp;
                   <%--</label>--%> </div>
                <div class="col-md-2"><asp:LinkButton ID="lbtn1" CssClass="btn btn-primary custom-button-color" runat="Server" Text="Get All Records"></asp:LinkButton></div>
                <div class="col-md-1 text-right">
                 
                    <asp:HyperLink ID="hyperlink1" runat="Server" NavigateUrl="~/FAM/FAM_Webfiles/frmAmgVendorNewRecord1.aspx" onmouseover="Tip('Add New Vendor')" onmouseout="UnTip()"><img id="Img1" src="../../images/glyphicons-7-user-add.png" /></asp:HyperLink>
                    
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-6 text-Center">
        <div class="form-group">
            <%--      <asp:Button ID="btnfincode" runat="server" CssClass="btn btn-primary custom-button-color" Text="GO" ValidationGroup="Val1" />

            <asp:HyperLink ID="hyperlink1" runat="Server" Text="Add New Vendor" NavigateUrl="~/FAM/FAM_Webfiles/frmAmgVendorNewRecord1.aspx"></asp:HyperLink>


            <asp:LinkButton ID="lbtn1" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Get All Records"></asp:LinkButton>--%>
        </div>
    </div>
</div>

<div class="row table table table-condensed table-responsive">
    <div class="form-group">
        <div class="col-md-12">
            <asp:GridView ID="gvDetails_AVR" runat="server" AutoGenerateColumns="False" AllowSorting="True" 
                AllowPaging="True" EmptyDataText="No Asset Vendor Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <PagerSettings Mode="NumericFirstLast"/>
                <Columns>
                   <%-- <asp:TemplateField HeaderText="Vendor Details">
                        <ItemTemplate>--%>
                            <%-- <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">ID</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="lblID" runat="server" CssClass="lblAssetID" Text='<%#Eval("AVR_ID")%>'></asp:Label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Code</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="lblCode_AVR" runat="server" CssClass="lblVendorCODE" Text='<%#Eval("AVR_CODE")%>'></asp:Label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Name</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblCodeName_AVR" runat="server" CssClass="lblVendorCODENAME" Text='<%#Eval("AVR_CO_NAME")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Phone No</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblPHNO_AVR" runat="server" CssClass="lblVendorPHNO" Text='<%#Eval("AVR_PHNO")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Mobile</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblMobile_AVR" runat="server" CssClass="lblVendorMobile" Text='<%#Eval("AVR_MOBILE_PHNO")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Email</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblEmail_AVR" runat="server" CssClass="lblVendorEmail" Text='<%#Eval("AVR_EMAIL")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">A/C No.</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="Label1" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_ACCNO")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Bank Name</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="Label2" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BANK_NAME")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Branch Name</label>

                                        <div class="col-md-6">
                                            <asp:Label ID="Label3" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BRN_NAME")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">IFSC Code</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="Label4" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_IFSC_CODE")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row-md-6">
                                <div class="form-group">
                                    <div class="col">
                                        <label class="col-md-6 control-label">Date</label>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblDate_AVR" runat="server" CssClass="lblVendorUpdateDate" Text='<%#Eval("AVR_UPT_DT")%>'></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <%--    <table width="100%" border="1" cellpadding="1" cellspacing="1">
                                <tr>
                                    <td align="left">ID
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblID" runat="server" CssClass="lblAssetID" Text='<%#Eval("AVR_ID")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Code</td>
                                    <td align="left">
                                        <asp:Label ID="lblCode_AVR" runat="server" CssClass="lblVendorCODE" Text='<%#Eval("AVR_CODE")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="lefT">Name</td>
                                    <td align="left">
                                        <asp:Label ID="lblCodeName_AVR" runat="server" CssClass="lblVendorCODENAME" Text='<%#Eval("AVR_CO_NAME")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="LEFT">Phone No</td>
                                    <td align="left">
                                        <asp:Label ID="lblPHNO_AVR" runat="server" CssClass="lblVendorPHNO" Text='<%#Eval("AVR_PHNO")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Mobile</td>
                                    <td align="left">
                                        <asp:Label ID="lblMobile_AVR" runat="server" CssClass="lblVendorMobile" Text='<%#Eval("AVR_MOBILE_PHNO")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Email</td>
                                    <td align="left">
                                        <asp:Label ID="lblEmail_AVR" runat="server" CssClass="lblVendorEmail" Text='<%#Eval("AVR_EMAIL")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">A/C No.</td>
                                    <td align="left">
                                        <asp:Label ID="Label1" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_ACCNO")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Bank Name</td>
                                    <td align="left">
                                        <asp:Label ID="Label2" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BANK_NAME")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Branch Name</td>
                                    <td align="left">
                                        <asp:Label ID="Label3" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_BRN_NAME")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">IFSC Code</td>
                                    <td align="left">
                                        <asp:Label ID="Label4" runat="server" CssClass="lblAccno" Text='<%#Eval("AVR_IFSC_CODE")%>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Date</td>
                                    <td align="left">
                                        <asp:Label ID="lblDate_AVR" runat="server" CssClass="lblVendorUpdateDate" Text='<%#Eval("AVR_UPT_DT")%>'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Vendor Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName_AVR" runat="server" CssClass="lblVendorName" Text='<%#Eval("AVR_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Grade">
                        <ItemTemplate>
                            <asp:Label ID="lblGrade_AVR" runat="server" CssClass="lblVendorGrade" Text='<%#Eval("AVR_GRADE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress_AVR" runat="server" CssClass="lblVendorAddress" Text='<%#Eval("AVR_ADDR")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor City">
                        <ItemTemplate>
                            <asp:Label ID="lblCity_AVR" runat="server" CssClass="lblVendorCity" Text='<%#Eval("AVR_CITY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Vendor Country">
                        <ItemTemplate>
                            <asp:Label ID="lblCountry_AVR" runat="server" CssClass="lblVendorCountry" Text='<%#Eval("AVR_COUNTRY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus_AVR" runat="server" CssClass="lblVendorStatus" Text='<%#Eval("AVR_STATUS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Updated By">
                        <ItemTemplate>
                            <asp:Label ID="lblUpdatedBy_AVR" runat="server" CssClass="lblVendorUpdatedBy" Text='<%#Eval("AVR_UPT_BY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks_AVR" runat="server" CssClass="lblRemarks" Text='<%#Eval("AVR_REMARKS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <a href='frmAMGVendorModify.aspx?code=<%#Eval("AVR_CODE")%>'>EDIT</a>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
