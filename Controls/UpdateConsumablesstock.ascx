<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateConsumablesstock.ascx.vb"
    Inherits="Controls_UpdateConsumablesstock" %>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Update Asset Stock
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Update Asset Stock</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table border="1" width="100%" align="center">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>&nbsp;<asp:ValidationSummary
                                ID="ValidationSummary1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" >
                            <asp:Label ID="Label1" runat="server" CssClass="bodytext"  Text="Select Location"></asp:Label></td>
                        <td align="left" >
                            <asp:Label ID="lblLocation" runat="server" CssClass="bodytext" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  >
                            <asp:Label ID="Label2" runat="server" CssClass="bodytext" Text="Select Tower"></asp:Label>
                        </td>
                        <td align="left"  >
                            <asp:Label ID="lblTower" runat="server" CssClass="bodytext" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left"  >
                            <asp:Label ID="Label3" runat="server" CssClass="bodytext" Text="Select Floor"></asp:Label>
                        </td>
                        <td align="left"  >
                            <asp:Label ID="lblFloor" runat="server" CssClass="bodytext" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="bodytext" >
                            Assets<font class="clsNote">*</font></td>
                          <td align="left"  >
                            <asp:Label ID="lblAsset" runat="server" CssClass="bodytext" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="bodytext" >
                            Asset Present Quantity<font class="clsNote">*</font> &nbsp;
                        </td>
                        <td >
                            <asp:Label ID="lblpreviousStock" runat="server" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="bodytext">
                            Add Quantity<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAstQty"
                                ErrorMessage="Invalid Integer" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)"
                                SetFocusOnError="True" Display="None"></asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAstQty"
                                Display="None" ErrorMessage="Required Asset Quantity" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                        <td>
                            <asp:TextBox ID="txtAstQty" runat="server" CssClass="clsTextField">0</asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="bodytext" >
                           Remarks<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                                Display="None" ErrorMessage="Remarks required" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                        <td >
                            <asp:TextBox ID="txtRemarks" TextMode="MultiLine"   runat="server" CssClass="clsTextField"></asp:TextBox>
                          </td>
                    </tr>
                    
                    <tr>
                        <td class="bodytext" >
                        </td>
                        <td >
                            <asp:Button ID="btnSubmit" runat="server" CssClass="clsButton" Text="Submit"></asp:Button>
                            <asp:Button ID="btnBack" runat="server" CssClass="clsButton" Text="Back" CausesValidation="False" /></td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</div>
