﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptPrintDetails.ascx.vb" Inherits="Controls_AptPrintDetails" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
    function printPartOfPage(elementId) {
        var printContent = document.getElementById(elementId);
        var windowUrl = 'about:blank';
        var uniqueName = new Date();
        var windowName = 'Print' + uniqueName.getTime();
        var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
        printWindow.close();

    }
    // -->
</script>
<div id="Div1">



    <%--<div class="row">
        <div class="col-md-12">
            <div class="row text-center">
                <strong>TRAVEL REQUEST FORM</strong>
            </div>
        </div>
    </div>--%>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        <asp:Image ID="Image1" ImageUrl="../images/a-mantralogo.gif" runat="server" />
                        <span style="color: red;"></span>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7 pull-right">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblheadingt" runat="server" Text="TRAVEL REQUEST FORM" CssClass="col-md-12 control-label"><strong>TRAVEL REQUEST FORM</strong></asp:Label>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <div class="row">
                    <asp:Button ID="btnprint" runat="server" Text="Print" CssClass="btn btn-primary custom-button-color" />
                </div>
            </div>
        </div>
    </div>


    <%--  <tr>
                        <td style="height: 20px">
                            <table width="100%" border="0">--%>

    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Travel Request No:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblreqno" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">From:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblfrmdate" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">To: </label>
                    <div class="col-md-7">
                        <asp:Label ID="lbltodate" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Name of the Traveller:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbltraveller" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Personnel No:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblpersonnelno" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Grade:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblgrade" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Age/Gender:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblage" runat="server"></asp:Label>
                        <asp:Label ID="lblgender" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Department:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbldepartment" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Location:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbllocation" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">
                        Requested Date:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblrequestdate" runat="server"></asp:Label>,travel form raised by,<asp:Label ID="lblaur_id" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Destination:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbldestination" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Purpose of travel:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblpurpose" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Status:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblstatus" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Activity:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblactivity" runat="server"></asp:Label>
                        </td>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Corporate Credit Card:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblcard" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label"><strong>Cost Assignment</strong></label>
                    <div class="col-md-7">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Distribution:</label>
                </div>
            </div>
        </div>
        <div class="col-md-4 pull-left">
            100%
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Assignment:</label>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Employee Cost Center:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblcostcenter" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Employee Company Code:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblempcompanycode" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Business Area:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblbusarea" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Trip Cost Center:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lbltripcc" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Company Code:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblcompanycode" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label"><strong>Request Transportation/Accommodation</strong></label>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">
                        1.</label>
                    <div class="col-md-7">

                        <asp:Label ID="lbldestination1" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">Beginning:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblbeginning" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <div class="row ">
                    <label class="col-md-5 control-label">End:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblend" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
       <div class="col-md-12">
            <asp:GridView ID="gvitems1" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Travel Details">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-md-2">
                                    <asp:Label ID="lblMODEOFTRAVEL" runat="Server" Text='<%#Eval("MODEOFTRAVEL")%>'></asp:Label>
                                </div>
                                <label class="col-md-2 control-label">From:</label>
                                <div class="col-md-1">
                                    <asp:Label ID="lblORIGIN" runat="Server" Text='<%#Eval("ORIGIN")%>'></asp:Label>
                                </div>
                                <label class="col-md-2 control-label">To:</label>
                                <div class="col-md-1">
                                    <asp:Label ID="lblDESTINATION" runat="Server" Text='<%#Eval("DESTINATION")%>'></asp:Label>
                                </div>
                            </div>
                            <div class="row">

                                <label class="col-md-2 control-label">Pickup:</label>
                                <div class="col-md-1">
                                    <asp:Label ID="lblpick" runat="Server" Text='<%#Eval("pick")%>'></asp:Label>
                                </div>

                                <label class="col-md-2 control-label">Drop Off:</label>
                                <div class="col-md-1">
                                    <asp:Label ID="lbldrop" runat="Server" Text='<%#Eval("droptime")%>'></asp:Label>
                                </div>
                                <label class="col-md-2 control-label">Comment:</label>
                                <div class="col-md-1">
                                    <asp:Label ID="lblcomment" runat="Server" Text='<%#Eval("COMMENT")%>'></asp:Label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>


</div>
