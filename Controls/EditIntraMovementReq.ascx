<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditIntraMovementReq.ascx.vb"
    Inherits="Controls_EditIntraMovementReq" %>
<script type="text/javascript">
<!--
    function printPartOfPage(elementId) {
        var printContent = document.getElementById(elementId);
        var windowUrl = 'about:blank';
        var uniqueName = new Date();
        var windowName = 'Print' + uniqueName.getTime();
        var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
        printWindow.close();
    }
    // -->
</script>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div id="Div1">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Request Id:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblReqId" runat="server" CssClass="bodytext"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">From Location:</label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlSLoc" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">From Tower:</label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlSTower" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">From Floor:</label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlSFloor" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Assets Found." AllowPaging="true"
                PageSize="10" CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">To Location:</label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlDLoc" runat="server" Width="275px" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">To Tower:</label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlDTower" runat="server" Width="275px" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">To Floor:</label>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlDFloor" runat="server" Width="275px">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Person To Receive Assets:</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtPersonName" runat="server" MaxLength="50" Width="275px"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <fieldset id="fldNewReq" runat="server" visible="false">
        <legend>New Request Summary</legend>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Request raised By:</label>
                        <div class="col-md-7">
                            <asp:Label ID="txtPendingApproved" CssClass="bodytext" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Remarks:</label>
                        <div class="col-md-7">
                            <asp:Label ID="txtPendingRemarks" CssClass="bodytext" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Date:</label>
                        <div class="col-md-7">
                            <asp:Label ID="txtPendingDate" CssClass="bodytext" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

    <fieldset id="fldITApproval" runat="server" visible="false">
        <legend>IT Approval/Reject Summary</legend>
        <div id="tblITSummary" runat="server" visible="false">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Request Approved by:</label>
                            <div class="col-md-7">
                                <asp:Label ID="lblITApproval" CssClass="bodytext" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Remarks:</label>
                            <div class="col-md-7">
                                <asp:Label ID="lblITRemarks" CssClass="bodytext" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Date:</label>
                            <div class="col-md-7">
                                <asp:Label ID="lblITDate" CssClass="bodytext" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </fieldset>

    <fieldset id="fldOutWard" runat="server" visible="false">
        <legend>Outward Approval/Reject Summary</legend>
        <div id="tblOutWardSummary" runat="server" visible="false">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Request Approved by:</label>
                            <div class="col-md-7">
                                <asp:Label ID="lblOutwardApproved" CssClass="bodytext" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Remarks:</label>
                            <div class="col-md-7">
                                <asp:Label ID="lblOutwardRemarks" CssClass="bodytext" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Date:</label>
                            <div class="col-md-7">
                                <asp:Label ID="lblOutWardDate" CssClass="bodytext" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <div class="row">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
                    <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="btn btn-primary custom-button-color"></asp:Button>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color"></asp:Button>
                </div>
            </div>
        </div>
    </div>

</div>

