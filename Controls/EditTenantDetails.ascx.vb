Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_EditTenantDetails
    Inherits System.Web.UI.UserControl
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim validatecode As Integer
            validatecode = validateproperty()
            If validatecode = 0 Then
                lblMsg.Text = "Property already in use please select another property"
                lblMsg.Visible = True
            Else
                UpdateTenant()
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Private Function Validateproperty()
        Dim validatecode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATEPROP_CODE")
        sp.Command.AddParameter("@propcode", ddlBuilding.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@user", ddluser.SelectedItem.Value, DbType.String)
        validatecode = sp.ExecuteScalar()
        Return validatecode
    End Function
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindPropType()
            BindUser()
            BindCity()

            'txtDate.Attributes.Add("onClick", "displayDatePicker('" + txtDate.ClientID + "')")
            'txtDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            'txtPayableDate.Attributes.Add("onClick", "displayDatePicker('" + txtPayableDate.ClientID + "')")
            'txtPayableDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtDate.Attributes.Add("readonly", "readonly")
            'txtPayableDate.Attributes.Add("readonly", "readonly")

            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETTENANT_DETAILS")
            sp4.Command.AddParameter("@SNO", Request.QueryString("id"), DbType.Int32)
            Dim ds As New DataSet
            ds = sp4.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then

                Dim li4 As ListItem = Nothing
                li4 = ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_PROP_TYPE"))
                If Not li4 Is Nothing Then
                    li4.Selected = True
                End If
                Dim li2 As ListItem = Nothing
                li2 = ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_ADM_CODE"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                    'ddlCity.Enabled = False
                End If
                BindCityLoc()
                Dim li6 As ListItem = Nothing
                li6 = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_ID"))
                If Not li6 Is Nothing Then
                    li6.Selected = True
                    'ddlCity.Enabled = False
                End If

                BindProp()
                Dim li As ListItem = Nothing
                li = ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PN_NAME"))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim li5 As Integer = ds.Tables(0).Rows(0).Item("TEN_STATUS")
                'If li5 <> 0 Then
                '    ddlstatus.SelectedValue = 1
                'Else
                '    ddlstatus.SelectedValue = 0

                'End If

                'Dim LI5 As ListItem = Nothing
                'LI5 = ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("TEN_STATUS"))
                'If Not LI5 Is Nothing Then
                '    LI5.Selected = True
                'End If

                Dim li3 As ListItem = Nothing
                li3 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("Aur_Id"))
                If Not li3 Is Nothing Then
                    li3.Selected = True
                End If


                'txtTenantOccupiedArea.Text = ds.Tables(0).Rows(0).Item("TEN_OCCUPIEDAREA")
                txtRent.Text = ds.Tables(0).Rows(0).Item("TEN_RENT_PER_SFT")
                txtDate.Text = ds.Tables(0).Rows(0).Item("TEN_COMMENCE_DATE")
                txtSecurityDeposit.Text = ds.Tables(0).Rows(0).Item("TEN_SECURITY_DEPOSIT")

                ddlPaymentTerms.SelectedValue = ds.Tables(0).Rows(0).Item("TEN_PAYMENTTERMS")
                'txtPayableDate.Text = ds.Tables(0).Rows(0).Item("TEN_NEXTPAYABLEDATE")

                txtNoofparking.Text = ds.Tables(0).Rows(0).Item("TEN_NO_PARKING")
                txtfees.Text = ds.Tables(0).Rows(0).Item("TEN_MAINT_FEE")
                txtamount.Text = ds.Tables(0).Rows(0).Item("TEN_OUTSTANDING_AMOUNT")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("TEN_REMARKS")
            End If
        End If
    End Sub
    Private Sub BindUser()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETUSER_BYTENANT")
        ddluser.DataSource = sp1.GetDataSet()
        ddluser.DataTextField = "AUR_FIRST_NAME"
        ddluser.DataValueField = "AUR_ID"
        ddluser.DataBind()
        ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub
    Public Sub UpdateTenant()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_TENANTS")
            sp2.Command.AddParameter("@SNO", Request.QueryString("id"), DbType.Int32)
            sp2.Command.AddParameter("@BDG_PROP_TYPE", ddlproptype.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp2.Command.AddParameter("@PN_NAME", ddlBuilding.SelectedItem.Value, DbType.String)
     
            'sp2.Command.AddParameter("@TEN_OCCUPIEDAREA", txtTenantOccupiedArea.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_RENT_PER_SFT", txtRent.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_COMMENCE_DATE", txtDate.Text, DbType.Date)
            sp2.Command.AddParameter("@TEN_SECURITY_DEPOSIT", txtSecurityDeposit.Text, DbType.Decimal)

            sp2.Command.AddParameter("@TEN_PAYMENTTERMS", ddlPaymentTerms.SelectedItem.Text, DbType.String)
            'sp2.Command.AddParameter("@TEN_NEXTPAYABLEDATE", txtPayableDate.Text, DbType.Date)
            'sp2.Command.AddParameter("@TEN_STATUS", ddlstatus.SelectedItem.Value, DbType.String)

            sp2.Command.AddParameter("@TEN_NO_PARKING", txtNoofparking.Text, DbType.Int32)
            sp2.Command.AddParameter("@TEN_MAINT_FEE", txtfees.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_OUTSTANDING_AMOUNT", txtamount.Text, DbType.Decimal)
            sp2.Command.AddParameter("@TEN_REMARKS", txtRemarks.Text, DbType.String)
            sp2.Command.AddParameter("@Aur_id", ddluser.SelectedItem.Value, DbType.String)
            sp2.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=8")
            'lblMsg.Text = " Tenant Details Modified Succesfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindProp()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTY_CITY")
            sp.Command.AddParameter("@PROPERTYTYPE", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            ddlBuilding.DataSource = sp.GetDataSet()
            ddlBuilding.DataTextField = "PN_NAME"
            ddlBuilding.DataValueField = "BDG_ID"
            ddlBuilding.DataBind()

            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Public Sub Cleardata()
        ddlproptype.SelectedIndex = 0

        ddlBuilding.Items.Clear()
        ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
        ddlBuilding.SelectedIndex = 0

        'txtTenantOccupiedArea.Text = ""
        txtRent.Text = ""
        txtDate.Text = getoffsetdate(Date.Today)
        txtSecurityDeposit.Text = ""

        ddlPaymentTerms.SelectedIndex = -1
        'txtPayableDate.Text = ""
        'ddlstatus.SelectedIndex = -1

        txtNoofparking.Text = ""
        txtfees.Text = ""
        txtamount.Text = ""
        txtRemarks.Text = ""
        ddluser.SelectedValue = 0
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmViewTenantDetails.aspx")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlproptype.SelectedIndex > 0 Then
            BindProp()

        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0


        End If
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            'BindProp()
            BindCityLoc()
        Else
            ddlBuilding.Items.Clear()
            ddlBuilding.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlBuilding.SelectedIndex = 0
        End If
    End Sub

    Protected Sub txtfees_TextChanged(sender As Object, e As EventArgs) Handles txtfees.TextChanged
        If txtRent.Text <> Nothing And txtfees.Text <> Nothing Then
            txtamount.ReadOnly = False
            txtamount.Text = Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtfees.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub

    Protected Sub txtRent_TextChanged(sender As Object, e As EventArgs) Handles txtRent.TextChanged
        If txtfees.Text <> Nothing And txtRent.Text <> Nothing Then
            txtamount.ReadOnly = False
            txtamount.Text = Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text)
            txtamount.ReadOnly = True
        Else
            txtamount.Text = Nothing
        End If
    End Sub
End Class