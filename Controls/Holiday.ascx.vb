Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.IO
Partial Class Controls_Holiday
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UID") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If
        'lblError.Visible = True
        'lblError.Text = ""
        lblMsg.Text = ""
        txtHYDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            Try
                'txtHYDate.Attributes.Add("onClick", "displayDatePicker('" + txtHYDate.ClientID + "')")
                'txtHYDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                gvHoliday.Visible = True
                Get_LOCATIONS()
                Fillgrid()
                btnModify.Visible = False
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Get_LOCATIONS()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select Location--")
    End Sub

    Private Sub Fillgrid()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_ALL_HOLIDAY_LIST_GRID")
        sp1.Command.AddParameter("@dummy", 1, DbType.Int32)
        gvHoliday.DataSource = sp1.GetDataSet()
        gvHoliday.DataBind()
        For i As Integer = 0 To gvHoliday.Rows.Count - 1
            Dim lblStatus As Label = CType(gvHoliday.Rows(i).FindControl("lblStatus"), Label)
            If lblStatus.Text = "1" Then
                lblStatus.Text = "Active"
            Else
                lblStatus.Text = "InActive"
            End If
        Next
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        InsertHoliday()
    End Sub

    Private Sub InsertHoliday()
        Try
           
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_ADD_HOLIDAY")
            sp.Command.AddParameter("@SHY_LCM_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@SHY_DATE", txtHYDate.Text, DbType.DateTime)
            sp.Command.AddParameter("@SHY_REASON", txtHYRsn.Text, DbType.String)
            sp.Command.AddParameter("@SHY_REM", txtRemarks.Text, DbType.String)
            sp.Command.AddParameter("@SHY_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@SHY_UP_BY", Session("Uid"), DbType.String)
            'sp.ExecuteScalar()
            Dim i As Integer = sp.ExecuteScalar()
            If i = 1 Then
                lblMsg.Visible = True
                lblMsg.Text = "Holiday Added Successfully..."
                Cleardata()
                Fillgrid()
            ElseIf i = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Holiday Already Exists..."
            End If
           
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub Cleardata()
        txtHYDate.Text = ""
        txtHYRsn.Text = ""
        txtRemarks.Text = ""
        ddlStatus.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
    End Sub

    Protected Sub gvHoliday_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvHoliday.PageIndexChanging
        gvHoliday.PageIndex = e.NewPageIndex
        Fillgrid()
    End Sub

    Protected Sub gvHoliday_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvHoliday.RowCommand
        If e.CommandName = "EDIT" Then
            btnSubmit.Visible = False
            lblMsg.Visible = False
            gvHoliday.Visible = True
            Fillgrid()
            btnModify.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvHoliday.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As String = Integer.Parse(lblID.Text)
            lblTemp.Text = id
            Dispdata()
            'ElseIf e.CommandName = "Status" Then
            '    Dim lnkStatus As LinkButton = DirectCast(e.CommandSource, LinkButton)
            '    Dim gvRow As GridViewRow = DirectCast(lnkStatus.NamingContainer, GridViewRow)
            '    Dim status As Integer = CInt(e.CommandArgument)
            '    Dim lblId As Label = DirectCast(gvRow.FindControl("lblId"), Label)
            '    Dim id1 As String = Integer.Parse(lblId.Text)
            '    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_HOLIDAY_STATUS")
            '    sp2.Command.AddParameter("@id1", id1, DbType.Int32)
            '    sp2.Command.AddParameter("@status", 1 - status, DbType.Int32)
            '    sp2.ExecuteScalar()
            '    gvHoliday.Visible = True
            '    fillgrid()
        End If
    End Sub

    Private Sub Dispdata()
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_ALL_HOLIDAY_EDIT")
            sp3.Command.AddParameter("@id", lblTemp.Text, DbType.Int32)
            Dim ds3 As New DataSet
            ds3 = sp3.GetDataSet()
            If ds3.Tables(0).Rows.Count > 0 Then
                txtHYDate.Text = ds3.Tables(0).Rows(0).Item("SHY_DATE")
                txtHYRsn.Text = ds3.Tables(0).Rows(0).Item("SHY_REASON")
                txtRemarks.Text = ds3.Tables(0).Rows(0).Item("SHY_REM")
                ddlLocation.ClearSelection()
                ddlLocation.Items.FindByValue(ds3.Tables(0).Rows(0).Item("SHY_LCM_ID")).Selected = True
                ddlLocation.Enabled = False
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds3.Tables(0).Rows(0).Item("SHY_STA_ID")).Selected = True
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvHoliday_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvHoliday.RowEditing

    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        btnModify.Visible = False
        btnSubmit.Visible = True
        Modifydata()
    End Sub

    Private Sub Modifydata()
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_UPDATE_HOLIDAY")
            sp3.Command.AddParameter("@id", lblTemp.Text, DbType.Int32)
            sp3.Command.AddParameter("@SHY_DATE", txtHYDate.Text, DbType.DateTime)
            sp3.Command.AddParameter("@SHY_REASON", txtHYRsn.Text, DbType.String)
            sp3.Command.AddParameter("@SHY_REM", txtRemarks.Text, DbType.String)
            sp3.Command.AddParameter("@SHY_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp3.Command.AddParameter("@SHY_LCM_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp3.Command.AddParameter("@SHY_UP_BY", Session("Uid"), DbType.String)
            sp3.ExecuteScalar()
            'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=34")
            lblMsg.Visible = True
            lblMsg.Text = "Holiday Modified Successfully..."
            Cleardata()
            Fillgrid()
            ddlLocation.Enabled = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If

                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If

                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                'Dim connectionstring As String = ""
                'If fpBrowseDoc.HasFile = True Then
                '    lblMsg.Visible = False
                '    Dim fs As System.IO.FileStream
                '    Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                '    Dim fname As String = fpBrowseDoc.PostedFile.FileName
                '    Dim s As String() = (fname.ToString()).Split(".")
                '    Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                '    Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                '    Try
                '        fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                '        fs.Close()
                '    Catch ex As System.IO.IOException
                '    End Try
                '    'fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                '    ''Connection String to Excel Workbook
                '    If strFileType.Trim() = ".xls" Then
                '        connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                '    ElseIf strFileType.Trim() = ".xlsx" Then
                '        connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""



                '    End If
                '    Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                '    con.Open()
                '    Dim dt As New System.Data.DataTable()
                '    dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                '    Dim listSheet As New List(Of String)
                '    Dim drSheet As DataRow
                '    For Each drSheet In dt.Rows
                '        listSheet.Add(drSheet("TABLE_NAME").ToString())
                '    Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1
                Dim location_cnt As Integer = 1
                Dim holidaydate_cnt As Integer = 1
                Dim holidayreason_cnt As Integer = 1
                Dim remarks_cnt As Integer = 1
                Dim status_cnt As Integer = 1
                'Dim bfm_idcnt As Integer = 1
                'Dim dmn_idcnt As Integer = 1
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "location" Then
                            location_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be LOCATION"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "holiday_date" Then
                            holidaydate_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be HOLIDAY_DATE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "holiday_reason" Then
                            holidayreason_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be HOLIDAY_REASON"
                            Exit Sub

                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "remarks" Then
                            remarks_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be REMARKS"
                            Exit Sub

                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "status" Then
                            status_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be STATUS"
                            Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "bfm_id" Then
                            '    bfm_idcnt = 0
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be BFM_ID"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "domain_head_id" Then
                            '    dmn_idcnt = 0
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be DOMAIN_HEAD_ID"
                            '    Exit Sub
                        End If
                    Next
                Next

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "location" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Location is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "holiday_date" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Holiday Date is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "holiday_reason" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Holiday reason is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "status" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Status is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'Start inseting excel data
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_ADD_HOLIDAY")
                    sp.Command.AddParameter("@SHY_LCM_ID", ds.Tables(0).Rows(i).Item("LOCATION").ToString, DbType.String)
                    sp.Command.AddParameter("@SHY_DATE", ds.Tables(0).Rows(i).Item("HOLIDAY_DATE").ToString, DbType.String)
                    sp.Command.AddParameter("@SHY_REASON", ds.Tables(0).Rows(i).Item("HOLIDAY_REASON").ToString, DbType.String)
                    sp.Command.AddParameter("@SHY_REM", ds.Tables(0).Rows(i).Item("REMARKS").ToString, DbType.String)
                    sp.Command.AddParameter("@SHY_STA_ID", ds.Tables(0).Rows(i).Item("STATUS").ToString, DbType.String)
                    sp.Command.AddParameter("@SHY_UP_BY", Session("Uid"), DbType.String)
                    sp.ExecuteScalar()
                    'End inseting excel data
                Next
                'Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_INSERT_SPACEASSETEXCELGETDATANEW")
                'sp3.Command.AddParameter("@USR_ID", Session("Uid"), DbType.String)
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                'obj.costCenter_LoadGrid(gvItem)
                Fillgrid()
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please Choose File..."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
