﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Data.OleDb
Imports System.Collections.Generic
Imports System.Globalization

Partial Class Controls_UploadHRMSData
    Inherits System.Web.UI.UserControl

    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                'Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filename As String = Path.GetFileNameWithoutExtension(fpBrowseDoc.FileName) & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                'Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename
                Dim filepath As String = Replace(Server.MapPath("~\UploadFiles\"), "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                'fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "~\UploadFiles\" + filename)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    fpBrowseDoc.SaveAs(filepath)
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only"
                End If
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only"
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                If listSheet(0).ToString() <> "_xlnm#_FilterDatabase" Then
                    msheet = listSheet(0).ToString()
                Else
                    msheet = listSheet(1).ToString()
                End If


                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If

                Dim numExcelRows As Integer = ds.Tables(0).Rows.Count
                If numExcelRows > 500 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Excel upload limit reached"
                    Return
                End If

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "employeeid" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Employee ID"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "first name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be First Name"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "middle name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Middle Name"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "last name" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Last Name"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "gender" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Gender"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "email id" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Email ID"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "supervisor id" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Supervisor ID"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(7).ToString) <> "designation" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Designation"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(8).ToString) <> "phone no" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Phone No"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(9).ToString) <> "department" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Department"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(10).ToString) <> "parentenitity" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Parent Enitity"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(11).ToString) <> "childentity" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Child Entity"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(12).ToString) <> "vertical" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Vertical"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(13).ToString) <> "costcenter/project" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Costcenter/Project"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(14).ToString) <> "location" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Location"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(15).ToString) <> "city" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be City"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(16).ToString) <> "country" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Country"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(17).ToString) <> "joining date(mm/dd/yyyy)" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Joining Date(mm/dd/yyyy)"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(18).ToString) <> "employee type" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Employee Type"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(19).ToString) <> "grade" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Grade"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(20).ToString) <> "status" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Status"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(21).ToString) <> "timezone" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be TimeZone"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(22).ToString) <> "employeeremarks" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be EmployeeRemarks"
                            Exit Sub
                        End If

                    Next
                Next

                Dim spcount As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_TOTAL_EMP_COUNT_HRMS")
                spcount.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
                Dim dsCount As New DataSet
                dsCount = spcount.GetDataSet

                Dim Tenant_Count As Integer
                Dim Emp_Count As Integer
                Tenant_Count = dsCount.Tables(0).Rows(0).Item("TENANT_EMP_COUNT")
                Emp_Count = 0 'dsCount.Tables(1).Rows(0).Item("EMP_COUNT") + ds.Tables(0).Rows.Count
                If Emp_Count <= Tenant_Count Then


                    Dim remarks As String = ""

                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                        For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                            If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "employee id" Then
                                If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                    remarks = remarks + "Employee Id is null or empty, "
                                End If
                            End If

                            If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "first name" Then
                                If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                    remarks = remarks + "First Name is null or empty, "
                                End If
                            End If

                            'If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "email id" Then
                            '    If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                            '        remarks = remarks + " Email Id is null or empty, "
                            '    End If
                            'End If
                            If chkRM.Checked = False Then
                                If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "vertical" Then
                                    If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                        remarks = remarks + " Vertical is null or empty, "
                                    End If
                                End If
                                If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "costcenter/project" Then
                                    If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                        remarks = remarks + " Costcenetr/Project is null or empty, "
                                    End If
                                End If
                            End If
                            If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "status" Then
                                If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                    remarks = remarks + " Status is null or empty, "
                                End If
                            End If

                            If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "timezone" Then
                                If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                    ds.Tables(0).Rows(i).Item(j) = "+05:30"
                                End If

                            End If
                            Dim thisDt As DateTime
                            Dim formats() As String = {"MM/dd/yyyy", "MM-dd-yyyy"}
                            If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "joining date(mm/dd/yyyy)" Then
                                If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = False Then
                                    If Not DateTime.TryParseExact(ds.Tables(0).Rows(i).Item(j), formats,
                       CultureInfo.InvariantCulture,
                       DateTimeStyles.None, thisDt) Then
                                        remarks = remarks + " Joining date should be in mm-dd-yyyy or mm/dd/yyyy format, "
                                    End If

                                End If

                            End If

                            'If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "EmployeeRemarks" Then
                            '    If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                            '        remarks = remarks + "EmployeeRemarks is null or empty,"
                            '    End If
                            'End If

                        Next

                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPLOAD_HRMS_DATA")
                        sp.Command.AddParameter("@EMP_ID", ds.Tables(0).Rows(i).Item(0).ToString, DbType.String)
                        sp.Command.AddParameter("@FNAME", ds.Tables(0).Rows(i).Item(1).ToString, DbType.String)
                        sp.Command.AddParameter("@MNAME", ds.Tables(0).Rows(i).Item(2).ToString, DbType.String)
                        sp.Command.AddParameter("@LNAME", ds.Tables(0).Rows(i).Item(3).ToString, DbType.String)
                        sp.Command.AddParameter("@GENDER", ds.Tables(0).Rows(i).Item(4).ToString, DbType.String)
                        sp.Command.AddParameter("@EMAIL_ID", ds.Tables(0).Rows(i).Item(5).ToString, DbType.String)
                        sp.Command.AddParameter("@SUPV_ID", ds.Tables(0).Rows(i).Item(6).ToString, DbType.String)
                        sp.Command.AddParameter("@DESIGNATION", ds.Tables(0).Rows(i).Item(7).ToString, DbType.String)
                        sp.Command.AddParameter("@PHONE_NO", ds.Tables(0).Rows(i).Item(8).ToString, DbType.String)
                        sp.Command.AddParameter("@DEPARTMENT", ds.Tables(0).Rows(i).Item(9).ToString, DbType.String)
                        sp.Command.AddParameter("@PENTITY", ds.Tables(0).Rows(i).Item(10).ToString, DbType.String)
                        sp.Command.AddParameter("@CENTITY", ds.Tables(0).Rows(i).Item(11).ToString, DbType.String)
                        sp.Command.AddParameter("@VERTICAL", ds.Tables(0).Rows(i).Item(12).ToString, DbType.String)
                        sp.Command.AddParameter("@COSTCENTER", ds.Tables(0).Rows(i).Item(13).ToString, DbType.String)
                        sp.Command.AddParameter("@LOCATION", ds.Tables(0).Rows(i).Item(14).ToString, DbType.String)
                        sp.Command.AddParameter("@CITY", ds.Tables(0).Rows(i).Item(15).ToString, DbType.String)
                        sp.Command.AddParameter("@COUNTRY", ds.Tables(0).Rows(i).Item(16).ToString, DbType.String)
                        sp.Command.AddParameter("@JOINING_DATE", ds.Tables(0).Rows(i).Item(17).ToString, DbType.String)
                        sp.Command.AddParameter("@EMP_TYPE", ds.Tables(0).Rows(i).Item(18).ToString, DbType.String)
                        sp.Command.AddParameter("@GRADE", ds.Tables(0).Rows(i).Item(19).ToString, DbType.String)
                        sp.Command.AddParameter("@STATUS", ds.Tables(0).Rows(i).Item(20).ToString, DbType.String)
                        sp.Command.AddParameter("@TIMEZONE", ds.Tables(0).Rows(i).Item(21).ToString, DbType.String)
                        sp.Command.AddParameter("@UID", Session("Uid"), DbType.String)
                        sp.Command.AddParameter("@REMARKS", remarks, DbType.String)
                        sp.Command.AddParameter("@EMP_REMARKS", ds.Tables(0).Rows(i).Item(22).ToString, DbType.String)
                        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
                        sp.ExecuteScalar()

                        remarks = ""
                    Next
                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_HRMS_DATA")
                    sp1.Command.AddParameter("@Checker", chkCCV.Checked, DbType.Boolean)
                    sp1.Command.AddParameter("@RM", chkRM.Checked, DbType.Boolean)
                    sp1.Command.AddParameter("@Allocation", chkChgAlloc.Checked, DbType.Boolean)
                    sp1.Command.AddParameter("@UPT_BY", Session("Uid"), DbType.String)
                    sp1.Command.AddParameter("@CNP_ID", Session("COMPANYID"), DbType.String)
                    Dim ds1 As New DataSet
                    ds1 = sp1.GetDataSet()
                    If ds1.Tables(0).Rows.Count > 0 Then
                        GridView1.DataSource = ds1.Tables(0)
                        GridView1.DataBind()
                        btnExport.Visible = True
                    End If
                    If ds1.Tables(0).Rows.Count > 100 Then
                        lblMsg1.Visible = True
                        lblMsg1.Text = "More than 100 users are present. The activation process will take time."
                    End If
                    'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_HRMS_DATA")
                    'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
                    'Dim ds1 As New DataSet
                    'ds1 = sp.GetDataSet()

                    lblMsg.Visible = True
                    lblMsg.Text = "Data uploaded, please review the comments..."

                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "You Should Not Upload More Than " + Convert.ToString(Tenant_Count) + " Employees,Total Employees are " + Convert.ToString(dsCount.Tables(1).Rows(0).Item("EMP_COUNT"))
                    btnbrowse.Enabled = False
                    Exit Sub
                End If

            Else
                lblMsg.Text = ""
                'lblMsg.Visible = True
                'lblMsg.Text = "Please select file..."
            End If



        Catch ex As Exception
            CommonModules.PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack Then
            lblMsg.Text = ""
            btnExport.Visible = False
        End If
        ' EmpCount()
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        'ExportPanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        'GridView1.Visible = True
        'ExportPanel1.FileName = "UPLOAD_HRMS_DATA.xls"
        Export("UPLOAD_HRMS_DATA.xls", GridView1)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            row.Cells(0).Style.Add("mso-number-format", "\@")
            row.Cells(5).Style.Add("mso-number-format", "\@")


            PrepareControlForExport(row)
            table.Rows.Add(row)

        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)

        '  render the htmlwriter into the response
        'style to format numbers to string
        Dim style As String = "<style>.textmode{mso-number-format:\@;}</style>"
        'HttpContext.Current.Response.Write(style)
        HttpContext.Current.Response.Write(sw.ToString)
        'HttpContext.Current.Response.Write(style + sw.ToString())
        HttpContext.Current.Response.End()

        ' Response.Write(style)
        'Response.Output.Write(sw.ToString())


    End Sub

    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Private Sub EmpCount()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_TOTAL_EMP_COUNT")
        sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
        Dim EmpCount As Integer
        EmpCount = sp.ExecuteScalar()
        'EmpCount = 1
        If EmpCount = 1 Then
            lblMsg.Text = "You have exceeded the maximum number of Employees."
            btnbrowse.Enabled = False
            Exit Sub
        End If

    End Sub

End Class
