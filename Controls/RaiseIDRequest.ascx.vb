Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_RaiseIDRequest
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtAssociateFirstName.ReadOnly = True
            txtAssociateMiddleName.ReadOnly = True
            txtAssociatesurName.ReadOnly = True
            txtAssociateNo.ReadOnly = True
            txtdep.ReadOnly = True
            txtdoj.ReadOnly = True
            txtBG.ReadOnly = True
            txtassname.ReadOnly = True
            txtdesig.ReadOnly = True
            txtphone.ReadOnly = True
            txtmobile.ReadOnly = True
            txtEmail.ReadOnly = True
            pnl1.Visible = False
            pnl2.Visible = False
        End If
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If ddlIdType.SelectedValue = "--Select--" Then
            lblMsg.Text = "Please Select Request ID Type"
        ElseIf ddlIdType.SelectedValue = "Associate" Then
            lblMsg.Text = ""
            pnl1.Visible = True
            pnl2.Visible = False
            BindAssociateDetails()
        ElseIf ddlIdType.SelectedValue = "Business" Then
            lblMsg.Text = ""
            pnl2.Visible = True
            pnl1.Visible = False
            BindBusinessDetails()
        End If
    End Sub
    Private Sub BindAssociateDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_ASSOCIATE_REQUEST")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateFirstName.Text = ds.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
                txtAssociateMiddleName.Text = ds.Tables(0).Rows(0).Item("AUR_MIDDLE_NAME")
                txtAssociatesurName.Text = ds.Tables(0).Rows(0).Item("AUR_LAST_NAME")
                txtAssociateNo.Text = ds.Tables(0).Rows(0).Item("AUR_NO")
                txtdep.Text = (ds.Tables(0).Rows(0).Item("AUR_DEP_ID"))
                txtdoj.Text = ds.Tables(0).Rows(0).Item("AUR_DOJ")
                txtBG.Text = ds.Tables(0).Rows(0).Item("AUR_BLOOD_GROUP")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindBusinessDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_BUSINESS_REQUEST")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtassname.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtdesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                txtphone.Text = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
                txtmobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
                txtEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnAssID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssID.Click
        Try
            If chkterms.Checked = False Then
                lblMsg.Text = "Please Accept Terms and conditions"
            Else
                lbtnbusterms.Enabled = False
                txtqty.Text = 0
                RaiseID()
            End If
           
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnBusSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBusSubmit.Click
        Try
            If chkbusterms.Checked = False Then
                lblMsg.Text = "Please Accept Terms and conditions"
            Else
                lbtnAssoc.Enabled = False
                RaiseID()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub RaiseID()
        Dim REQID As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("UID")
        Dim Req As Integer
        Req = ValidateReqAssociate(Session("UID"), ddlIdType.SelectedItem.Value)
        If Req = 0 Then
            lblMsg.Text = "You Already Raised Request for " & ddlIdType.SelectedItem.Text & " ID"
            lblMsg.Visible = True
        Else
            INSERT_ID(REQID)
            MAIL_ID(REQID)
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=66")
            lblMsg.Text = "Request for" & ddlIdType.SelectedItem.Text & "has been Raised Successfully."
        End If
    End Sub
    Private Sub INSERT_ID(ByVal REQID As String)
        Try
            Dim qty As Integer = 0
            If txtqty.Text = "" Then
                qty = 0
            Else
                qty = txtqty.Text
            End If
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_RAISE_ID")
            sp.Command.AddParameter("@REQ_ID", REQID, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@CARD_TYPE", ddlIdType.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@QUANTITY", qty, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateReqAssociate(ByVal user As String, ByVal idtype As String)
        Dim validatereq As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_VALIDATE_ID")
        sp.Command.AddParameter("@AUR_ID", user, DbType.String)
        sp.Command.AddParameter("@ID_TYPE", idtype, DbType.String)
        validatereq = sp.ExecuteScalar()
        Return validatereq
    End Function
    Protected Sub lbtnAssoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnAssoc.Click
        Response.Redirect("frmEditProfile.aspx")
    End Sub
    Protected Sub lbtnbusterms_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnbusterms.Click
        Response.Redirect("frmEditProfile.aspx")
    End Sub

    Protected Sub lbtnterms_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnterms.Click
        Response.Write("<script language=javascript>javascript:window.open('Files/IDcards.pdf','Window','toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450')</script>")
    End Sub


    Protected Sub lbtnbus1terms_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnbus1terms.Click
        Response.Write("<script language=javascript>javascript:window.open('Files/BusinessCards.pdf','Window','toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450')</script>")
    End Sub
    Private Sub MAIL_ID(ByVal REQID As String)
       

        Dim strSubj As String = ""
        Dim Associate_FirstName As String = ""
        Dim Associate_MiddleName As String = ""
        Dim Associate_SurName As String = ""
        Dim Associate_DOJ As String = ""
        Dim Associate_No As String = ""
        Dim Associate_Department As String = ""
        Dim Associate_BG As String = ""
        Dim Associate_Phone As String = ""
        Dim Associate_Mobile As String = ""
        Dim Associate_Email As String = ""
        Dim Associate_Quantity As Integer = 0
        Dim Associate_Designation As String = ""
        Dim strfrommail As String = ""
        Dim strfromname As String = ""
        Dim id_type As String = ""
        Dim strMsg As String = ""
        Dim strAppEmpName As String = ""
        Dim strAppEmpEmail As String = ""
        Dim strHrEmail As String = ""
        Dim card_type As String = ""
        Dim Associate_Name As String = ""
        Dim StrHrName As String = ""
        Dim strMsg1 As String = ""
      
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_GET_ID_REQUESTS")
        sp.Command.AddParameter("@REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()

        Associate_FirstName = ds.Tables(1).Rows(0).Item("AUR_FIRST_NAME")
        Associate_MiddleName = ds.Tables(1).Rows(0).Item("AUR_MIDDLE_NAME")
        Associate_SurName = ds.Tables(1).Rows(0).Item("AUR_LAST_NAME")
        Associate_DOJ = ds.Tables(1).Rows(0).Item("AUR_DOJ")
        Associate_No = ds.Tables(1).Rows(0).Item("AUR_NO")
        Associate_Department = ds.Tables(1).Rows(0).Item("AUR_DEP_ID")
        Associate_BG = ds.Tables(1).Rows(0).Item("AUR_BLOOD_GROUP")
        Associate_Phone = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
        Associate_Mobile = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
        Associate_Designation = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
        Associate_Email = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        Associate_Quantity = ds.Tables(2).Rows(0).Item("AUR_QTY")
        card_type = ds.Tables(2).Rows(0).Item("CARD_TYPE")
        Associate_Name = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")


        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "EmployeeServiceDesk"
        strSubj = "Request for" & ddlIdType.SelectedItem.Text & "<br>"

        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_IDCARD")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        strHrName = DS2.Tables(0).Rows(0).Item("HR_NAME")
        If card_type = "Associate" Then
            strMsg1 = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                                   "This is to inform you that the Details of Your" & ddlIdType.SelectedItem.Text & " Request  <br>" & _
                                "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor FirstName: </U></td>" & _
                                "<td width=50% align=left>" & Associate_FirstName & "</td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor Middle Name: </U></td>" & _
                                "<td width=50% align=left>" & Associate_MiddleName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor Sur Name </U></td>" & _
            "<td width=50% align=left>" & Associate_SurName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor ID: </U></td>" & _
            "<td width=50% align=left>" & Associate_No & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Date of Joining:</U></td>" & _
            "<td width=50% align=left> " & Associate_DOJ & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Blood Group </U></td>" & _
            "<td width=50% align=left>" & Associate_BG & "</td></tr>" & _
            "<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
             "Thanking You " & "<br><br>" & _
                    "Yours Sincerely, " & "<br>" & strfromname & "." & "<br>"

            strMsg = "Dear " & StrHrName & "," & "<br>" & "<br>" & _
                                   "This is to inform you that following request for" & ddlIdType.SelectedItem.Text & " by " & strAppEmpName & "<br>" & _
                                "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor FirstName: </U></td>" & _
                                "<td width=50% align=left>" & Associate_FirstName & "</td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor Middle Name: </U></td>" & _
                                "<td width=50% align=left>" & Associate_MiddleName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor Sur Name </U></td>" & _
            "<td width=50% align=left>" & Associate_SurName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor ID: </U></td>" & _
            "<td width=50% align=left>" & Associate_No & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Date of Joining:</U></td>" & _
            "<td width=50% align=left> " & Associate_DOJ & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Blood Group </U></td>" & _
            "<td width=50% align=left>" & Associate_BG & "</td></tr>" & _
            "<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
             "Thanking You " & "<br><br>" & _
                    "Yours Sincerely, " & "<br>" & strAppEmpName & "." & "<br>"
            
        Else
            strMsg1 = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that the Details of Your " & ddlIdType.SelectedItem.Text & " Request <br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Name & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Designation: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Designation & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Phone: </U></td>" & _
"<td width=50% align=left>" & Associate_Phone & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Mobile: </U></td>" & _
"<td width=50% align=left>" & Associate_Mobile & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Email: </U></td>" & _
"<td width=50% align=left>" & Associate_Email & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Quantity: </U></td>" & _
"<td width=50% align=left>" & Associate_Quantity & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strfromname & "." & "<br>"
            strMsg = "Dear " & StrHrName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request for" & ddlIdType.SelectedItem.Text & " by " & strAppEmpName & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Name & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Designation: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Designation & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Phone: </U></td>" & _
"<td width=50% align=left>" & Associate_Phone & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Mobile: </U></td>" & _
"<td width=50% align=left>" & Associate_Mobile & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Email: </U></td>" & _
"<td width=50% align=left>" & Associate_Email & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Quantity: </U></td>" & _
"<td width=50% align=left>" & Associate_Quantity & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strAppEmpName & "." & "<br>"
        End If
       
        '"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'If strAppEmpEmail <> "" Then
        '    Send_Mail(strAppEmpEmail, strHrEmail, REQID, strSubj & " - " & strAppEmpName, strMsg1)
        'End If
        'If strHrEmail <> "" Then
        '    Send_Mail(strHrEmail, strAppEmpEmail, REQID, strSubj & " - " & strAppEmpName, strMsg)
        'End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD_REQUEST")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
End Class
