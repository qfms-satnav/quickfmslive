Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_RMApproval
    Inherits System.Web.UI.UserControl
    Dim UID As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                UID = Session("uid")
                BindGrid(UID)
            End If
        End If
    End Sub
    Private Sub BindGrid(ByVal aur_id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GETREQBYSTATUSID1")
        sp.Command.AddParameter("@Cuser", Session("uid"), Data.DbType.String)
        'Dim ds As New dataset()
        'ds = sp.GetDataSet()
        'If ds.Tables(0).Rows.Count > 0 Then
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
        'End If
    End Sub

    Private Sub SearchBindGrid()
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_LEVEL1_APPROVAL_SEARCH")
        sp.Command.AddParameter("@search_criteria", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        divlevel.Visible = False
        btnsubmit.Visible = False
        btnCancel.Visible = False
    End Sub



    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        If (txtSearch.Text.Length > 0) Then
            SearchBindGrid()
        Else
            BindGrid(UID)
        End If
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim ReqIDsForMsg = ""
        For Each row As GridViewRow In gvItems.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
                Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text
                If bf.Checked Then
                    UpdateData(id, txtRM.Text)
                    ReqIDsForMsg = ReqIDsForMsg + ", " + id
                End If
            End If
        Next
        BindGrid(Session("UID"))
        txtRM.Text = ""
        lblMsg.Text = "Requisition(s) Successfully Approved  ( " + ReqIDsForMsg.Remove(0, 1) + " ) "

    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim ReqIDsForMsg = ""
        For Each row As GridViewRow In gvItems.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim bf As CheckBox = TryCast(row.Cells(0).FindControl("chkSelect"), CheckBox)
                Dim id As String = TryCast(row.Cells(1).FindControl("hLinkDetails"), HyperLink).Text
                If bf.Checked Then
                    CancelData(id, Trim(txtRM.Text))
                    ReqIDsForMsg = ReqIDsForMsg + ", " + id
                End If
            End If
        Next
        BindGrid(Session("UID"))
        txtRM.Text = ""
        lblMsg.Text = "Requisition(s) Successfully Rejected  ( " + ReqIDsForMsg.Remove(0, 1) + " ) "

    End Sub



    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAP_AssetRequisition_APP_REJ_DET")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub CancelData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAP_AssetRequisition_APP_REJ_BY_L2")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.Command.AddParameter("@STATUS", 1005, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_LEVEL1_APPROVAL_SEARCH")
        sp.Command.AddParameter("@search_criteria", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        divlevel.Visible = False
        btnsubmit.Visible = False
        btnCancel.Visible = False

    End Sub
End Class
