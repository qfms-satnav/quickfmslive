﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ItemIssuance.ascx.vb" Inherits="Controls_ItemIssuance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                    <asp:Label ID="Label2" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Employee Id</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlEmp" runat="server" Enabled="false" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                    Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Sub Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                    Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="pnlItems" runat="server" class="row">
    <div class="col-md-12">
        <h4>Item List</h4>
        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
            EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:BoundField DataField="AST_MD_CODE" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" />
                <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left" />
                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left" />
                <asp:TemplateField HeaderText="Item Name" ItemStyle-HorizontalAlign="left" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="AST_MD_CODE" runat="server" Text='<%#Eval("AST_MD_CODE") %>'> </asp:Label>
                        <asp:Label ID="AST_MD_NAME" runat="server" Text='<%#Eval("AST_MD_NAME") %>'> </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>
                        <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
                        <asp:Label ID="lblunits" runat="server" Text='<%#Eval("UNITS")%>'></asp:Label>
                        <asp:Label ID="lblITEMCODE" runat="server" Text='<%#Eval("AST_MD_ID") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblMinOrdQty" runat="server" Text='<%#Eval("AST_MD_MINQTY") %>' Visible="false"> </asp:Label>
                        <asp:Label ID="lblCat" Text='<%#Eval("AST_MD_CATID") %>' runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblSubCat" Text='<%#Eval("AST_MD_SUBCATID") %>' runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblBrand" Text='<%#Eval("AST_MD_BRDID") %>' runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lbltotal" Text='<%#Eval("AST_MD_TOTAVBL") %>' runat="server" Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRem"
                    Display="None" ErrorMessage="Please Enter the Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRem" runat="server" CssClass="form-control"
                        Rows="3" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
        </div>
    </div>
</div>

