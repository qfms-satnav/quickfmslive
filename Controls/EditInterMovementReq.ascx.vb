Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_EditInterMovementReq
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim strReqId As String
    Dim Req_status As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                strReqId = Request.QueryString("Req_id")
                getDetailsbyReqId(strReqId)
                Cache.Insert("ReqId", strReqId, Nothing, DateTime.Now.AddMinutes(21), System.TimeSpan.Zero)
            End If
        End If
    End Sub

    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)
        Dim ds As DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        ds = ObjSubsonic.GetSubSonicDataSet("GET_INTER_MVMR_REQ_DETAILS", param)

        lblfrmloc.Text = Convert.ToString(ds.Tables(0).Rows.Item(0)("LCM_NAME"))
        txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows.Item(0)("MMR_COMMENTS"))
        lblreqby.Text = Convert.ToString(ds.Tables(0).Rows.Item(0)("AUR_KNOWN_AS"))
        BindLocations(Convert.ToString(ds.Tables(0).Rows.Item(0)("MMR_FROMBDG_ID")), Convert.ToString(ds.Tables(0).Rows.Item(0)("MMR_TO_LOCID")))

        lblITApproval.Text = Convert.ToString(ds.Tables(0).Rows.Item(0)("MMR_APPROVED_BY"))
        lblITDate.Text = Convert.ToString(ds.Tables(0).Rows.Item(0)("MMR_Approved_date"))
        lblITRemarks.Text = Convert.ToString(ds.Tables(0).Rows.Item(0)("MMR_App_Rej_COMMENTS"))

        gvInterMVMT.DataSource = ds.Tables(1)
        gvInterMVMT.DataBind()

        chkstatus(Convert.ToString(ds.Tables(0).Rows.Item(0)("MMR_LRSTATUS_ID")))
    End Sub

    Public Sub chkstatus(ByVal Req_status As Integer)
        If Req_status = 1 Then
            tblITSummary.Visible = False
            btnSubmit.Visible = True
        Else
            tblITSummary.Visible = False
            fldITApproval.Visible = False
            btnSubmit.Visible = False
        End If

        If Req_status >= 1024 Then
            btnSubmit.Visible = False
            tblITSummary.Visible = True
            fldITApproval.Visible = True
            'lblITApproval.Text = strITAppBy
            'lblITDate.Text = strITAppDt
            'lblITRemarks.Text = strITAppComment
        Else
            fldITApproval.Visible = False
        End If
    End Sub

    Private Sub BindLocations(ByVal ddlloc As String, ByVal ddldestloc As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlToloc.DataSource = sp.GetDataSet()
            ddlToloc.DataTextField = "LCM_NAME"
            ddlToloc.DataValueField = "LCM_CODE"
            ddlToloc.DataBind()
            ddlToloc.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlToloc.Items.FindByValue(ddlloc).Attributes.Add("disabled", "disabled")
            ddlToloc.Items.FindByValue(ddldestloc).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = LocCode
        ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    End Sub

    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TwrCode", DbType.String)
        param(0).Value = TwrCode
        ObjSubsonic.Binddropdown(ddl, "USP_FLOOR_GETBYTOWER", "FLR_NAME", "FLR_CODE", param)
    End Sub


    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("Req_id")
        param(1) = New SqlParameter("@MMR_TOLOC", SqlDbType.NVarChar, 200)
        param(1).Value = ddlToloc.SelectedValue
        param(2) = New SqlParameter("@MMR_COMMENTS", SqlDbType.NVarChar, 500)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@UPDATEDBY", SqlDbType.NVarChar, 100)
        param(3).Value = Session("uid")
        ObjSubsonic.GetSubSonicExecute("AM_UPDATE_MMT_inter_MVMT_REQ_reqId", param)
        send_mail(Request.QueryString("Req_id"))
        Response.Redirect("frmAssetThanks.aspx?RID=EIntMVMT&reqid=" + Request.QueryString("Req_id") + "")
    End Sub

    Public Sub send_mail(ByVal reqid As String)
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_MOVEMENT_REQUEST_UPDATE")
        'sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        'sp.Execute()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_INTER_MVMT_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Command.AddParameter("@MODE", "UPDATE", DbType.String)
        sp.Execute()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmViewReq.aspx")
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If gvInterMVMT.Rows.Count > 0 Then
            For Each row As GridViewRow In gvInterMVMT.Rows
                Dim assetcode As Label = DirectCast(row.FindControl("lblastid"), Label)
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_MVMT_CANCEL")
                sp.Command.AddParameter("@ASSETCODE", assetcode.Text, DbType.String)
                sp.Command.AddParameter("@MMR_UPDATEDBY", Session("uid"), DbType.String)
                sp.Command.AddParameter("@REQ_ID", Convert.ToString(Cache("ReqId")), DbType.String)
                sp.ExecuteScalar()
            Next
        End If

        SendMail(Convert.ToString(Cache("ReqId")))

        Response.Redirect("frmAssetThanks.aspx?RID=MVMTCANCELLED&reqid=" + Convert.ToString(Cache("ReqId")) + "")
    End Sub

    Private Sub SendMail(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_INTER_MVMT_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", "CANCEL", DbType.String)
        sp.Execute()
    End Sub

End Class
