Imports System.Data
Partial Class Controls_ItemRequisitionDetails
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim id As String = Request.QueryString("id")
            lblTemp.Text = id
            dispdata()
        End If

    End Sub
    Private Sub dispdata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_VIEW_REQDETAILS")
        sp.Command.AddParameter("@id", lblTemp.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblReqId.Text = ds.Tables(0).Rows(0).Item("AIR_REQ_ID")

            lblLoc.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
            lblTwr.Text = ds.Tables(0).Rows(0).Item("TWR_NAME")
            lblFlr.Text = ds.Tables(0).Rows(0).Item("FLR_NAME")
            lblItemCode.Text = ds.Tables(0).Rows(0).Item("AIM_CODE")
            lblLocName.Text = ds.Tables(0).Rows(0).Item("LCM_CODE")
            lblTwrName.Text = ds.Tables(0).Rows(0).Item("TWR_CODE")
            lblFlrName.Text = ds.Tables(0).Rows(0).Item("FLR_CODE")


            gvItems.DataSource = ds
            gvItems.DataBind()
        End If


    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmRequesitionReport.aspx")
    End Sub

End Class
