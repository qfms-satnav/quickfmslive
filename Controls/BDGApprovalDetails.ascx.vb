Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Partial Class Controls_BDGApprovalDetails
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                BindUsers(UID)
                BindCategories()
                pnlItems.Visible = True
                BindRequisition()
            End If
        End If
    End Sub
    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function
    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)

                'li = ddlAstCat.Items.FindByValue(CStr(CatId))
                'If Not li Is Nothing Then
                '    li.Selected = True
                'End If
                Dim CatId As String

                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = False


                getassetsubcategory(CatId)
                ddlAstSubCat.Enabled = False
                Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")

                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)


                Dim asstbrand As String = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                ddlAstBrand.Enabled = False

                ddlAstCat.Enabled = False
                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")

                If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                    tr3.Visible = False
                Else
                    tr3.Visible = True

                End If

                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    tr1.Visible = False
                Else
                    tr1.Visible = True

                End If
              
                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                '    If StatusId = 1001 Or StatusId = 1002 Then
                '        btnApprove.Enabled = True
                '        btnCancel.Enabled = True
                '    Else
                '        btnApprove.Enabled = False
                '        btnCancel.Enabled = False
                '    End If
                'Else
                '    btnApprove.Enabled = False
                '    btnCancel.Enabled = False
                'End If

                BindGrid()
                

                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
                    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_GetDetailsByReqIdAndProductId")
                    sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
                    sp1.Command.AddParameter("@ProductId", CInt(lblProductId.Text), DbType.Int32)
                    Dim dr1 As SqlDataReader = sp1.GetReader
                    If dr1.Read() Then
                        chkSelect.Checked = True
                        txtQty.Text = dr1("AID_QTY")
                        If txtQty.Text = "NULL" Or txtQty.Text = "" Or txtQty.Text = 0 Then
                            row.Visible = False
                        Else
                            row.Visible = True
                        End If
                    Else
                        row.Visible = False
                    End If
                Next
            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub
    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub
    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")
    End Sub
    Private Sub BindGrid()
        'Dim CatId As Integer = 0
        'Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
        'gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
        'gvItems.DataBind()
        'pnlItems.Visible = True
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        'ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)


        ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)

        pnlItems.Visible = True
    End Sub
    Private Sub BindUsers(ByVal aur_id As String)


        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = aur_id
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP_Raisedby", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'Dim li As ListItem = ddlEmp.Items.FindByValue(Session("UID"))
        'If Not li Is Nothing Then
        '    li.Selected = True
        'End If
    End Sub
    Private Sub BindCategories()
        'GetChildRows("0")
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
    End Sub
    'Private Sub GetChildRows(ByVal i As String)
    '    Dim str As String = ""
    '    Dim id
    '    If i = "0" Then
    '        id = CType(i, Integer)
    '    Else
    '        Dim id1 As Array = Split(i, "~")
    '        str = id1(0).ToString & "  --"
    '        id = CType(id1(1), Integer)
    '    End If
    '    Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
    '    Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
    '    Dim ds As New DataSet
    '    da.Fill(ds)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each dr As DataRow In ds.Tables(0).Rows
    '            Dim j As Integer = CType(dr("CategoryId"), Integer)
    '            If id = 0 Then
    '                str = ""
    '            End If
    '            Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
    '            ddlAstCat.Items.Add(li)
    '            GetChildRows(str & "~" & j)
    '        Next
    '    End If
    'End Sub

    'Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
    '    If ddlAstCat.SelectedIndex > 0 Then
    '        Dim CatId As Integer = 0
    '        Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
    '        gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
    '        gvItems.DataBind()
    '        pnlItems.Visible = True
    '    End If
    'End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim ReqId As String = GetRequestId()
        Dim count As Integer
        UpdateData(ReqId, Trim(txtAdminRemarks.Text))
        ' DeleteRequistionItems(ReqId)
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            'If chkSelect.Checked Then
            '    InsertDetails(ReqId, CInt(lblProductId.Text), CInt(Trim(txtQty.Text)))
            'End If
            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                    Exit Sub
                ElseIf IsNumeric(txtQty.Text) > 0 Then
                    count = count + 1
                    If count = 0 Then
                        lblMsg.Text = "Sorry! Request Has not been Raised You haven't selected any Products and make Quantity more than Zero"
                        Exit Sub
                    End If
                    'Else
                    '    lblMsg.Text = "Please enter Quantity in Numerics Only"
                End If

                InsertDetails(ReqId, CInt(lblProductId.Text), CInt(Trim(txtQty.Text)))
            Else
                lblMsg.Text = "Sorry! Request Has not been Raised You haven't selected any Products and make Quantity more than Zero"
                Exit Sub
            End If

        Next
        Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
    End Sub
    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisition_UpdateByBudget")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", ddlEmp.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@StatusId", 1006, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As Integer, ByVal Qty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_updatedbybudjet")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.Int32)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        CancelData(GetRequestId, Trim(txtRemarks.Text))
        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub
    Private Sub CancelData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisition_UpdateByBudget")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", ddlEmp.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", CInt(ddlAstCat.SelectedItem.Value), DbType.Int32)
        sp.Command.AddParameter("@StatusId", 1007, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Sub DeleteRequistionItems(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_DeleteByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
