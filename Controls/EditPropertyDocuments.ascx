<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditPropertyDocuments.ascx.vb"
    Inherits="Controls_EditPropertyDocuments" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Edit Property Documents
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Edit Property Documents</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblCity" runat="server" CssClass="bodytext" Text="Select City"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" Display="dynamic" ErrorMessage="Please Select City"
                                ControlToValidate="ddlCity" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="dropDown" AutoPostBack="TRUE"
                                Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblPropIDName" runat="server" CssClass="bodytext" Text="Select Property"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvproperty" runat="server" Display="Dynamic" ErrorMessage="Please Select Property"
                                ControlToValidate="ddlPropIDName" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlPropIDName" runat="server" CssClass="dropDown" AutoPostBack="TRUE"
                                Width="99%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" ValidationGroup="Val1" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <asp:GridView ID="gvEditDoc" runat="server" EmptyDataText="Sorry! No Available Records..."
                                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("RPT_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Actual File Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblActualFilename" runat="server" Text='<%#Eval("RPT_ORG_FILENAME")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Uploaded File Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUploadedFilename" runat="server" Text='<%#Eval("RPT_DOC")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Uploaded Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%#Eval("RPT_DATE")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="View Document">
                                        <ItemTemplate>
                                            <asp:Label ID="lblViewFilename" Visible="false" runat="server" Text='<%#Eval("RPT_ORG_FILENAME")%>'>
                                            </asp:Label>
                                            <asp:HyperLink ID="hypbtnDocs" Visible="false" runat="server" Text='View Document'>
                                            </asp:HyperLink>
                                            <a target="_blank" href='<%=Request.ApplicationPath %>/UploadFiles/<%#Eval("RPT_DOC")%>'>
                                                <%#Eval("RPT_TITLE")%>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:HyperLinkField Text="View Document" Visible="false" Target="_blank" DataNavigateUrlFields="RPT_ORG_FILENAME"
                                        DataNavigateUrlFormatString='<%= Request.PhysicalApplicationPath %>/UploadFiles'>
                                    </asp:HyperLinkField>
                                    <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>

</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
        <asp:PostBackTrigger ControlID="gvEditDoc" />
    </Triggers>
</asp:UpdatePanel>
