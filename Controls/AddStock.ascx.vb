Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Imports System.Data.OleDb
Imports System.IO
Imports System.Collections.Generic

Partial Class Controls_AddStock
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim obj As clsMasters = New clsMasters
    Dim strpath As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        RegularExpressionValidator1.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()

        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                txtmnfdate.Attributes.Add("onClick", "displayDatePicker('" + txtmnfdate.ClientID + "')")
                txtmnfdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
                txtExpdate.Attributes.Add("onClick", "displayDatePicker('" + txtExpdate.ClientID + "')")
                txtExpdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

                getassetcategory()
                getvendors()
                BindLocation()

                'ddlAssetCategory.Items.Insert(0, "--Select--")

                ddlAstSubCat.Items.Insert(0, "--Select--")
                ddlAstBrand.Items.Insert(0, "--Select--")
                ddlModel.Items.Insert(0, "--Select--")
                ddlLocation.Items.Insert(0, "--Select--")
                ddlTower.Items.Insert(0, "--Select--")
                ddlFloor.Items.Insert(0, "--Select--")
                getLOB()
                lblProcess.Text = Session("Child")
            End If
        End If
    End Sub
    Private Sub getLOB()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOB")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlDepartment.DataSource = sp.GetDataSet()
        ddlDepartment.DataTextField = "Cost_Center_Name"
        ddlDepartment.DataValueField = "Cost_Center_Code"
        ddlDepartment.DataBind()
        ddlDepartment.Items.Insert(0, "--Select--")
    End Sub
    Private Sub BindLocation()
        'Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        'param(0).Value = 1
        'param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 100)
        'param(0).Value = Session("Uid").ToString
        'ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
        sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ' ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))


    End Sub
    Private Sub getTowersByLocations(ByVal lcmCode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_TOWER_BY_LOCATION")
        sp.Command.AddParameter("@LCM_CODE", lcmCode, DbType.String)
        'sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlTower.DataSource = sp.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub
    Private Sub getFloorsByLocationTower(ByVal lcmCode As String, ByVal twrCode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FLOOR_BY_LOC_TWR")
        sp.Command.AddParameter("@LCM_CODE", lcmCode, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrCode, DbType.String)
        ddlFloor.DataSource = sp.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GETCHECK_CONSUMABLES")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
    End Sub

    'Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
    '    Try
    '        If ddlLocation.SelectedIndex > 0 Then
    '            BindTower()
    '        Else
    '            ddlTower.Items.Clear()
    '            ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlTower.SelectedValue = 0
    '            ddlfloor.Items.Clear()
    '            ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlfloor.SelectedValue = 0
    '            ddlAssets.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlAssets.SelectedValue = 0
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Private Sub BindTower()
    '    Try
    '        Dim param(0) As SqlParameter
    '        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
    '        param(0).Value = ddlLocation.SelectedItem.Value
    '        ObjSubsonic.Binddropdown(ddlTower, "GET_LOCTWR", "TWR_NAME", "TWR_CODE", param)
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Private Sub BindFloor()
    '    Try
    '        Dim param(1) As SqlParameter
    '        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
    '        param(0).Value = ddlLocation.SelectedItem.Value
    '        param(1) = New SqlParameter("@dummy1", SqlDbType.NVarChar, 100)
    '        param(1).Value = ddlTower.SelectedItem.Value
    '        ObjSubsonic.Binddropdown(ddlfloor, "GET_TWRFLR", "FLR_NAME", "FLR_CODE", param)
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
    '    Try
    '        If ddlTower.SelectedIndex > 0 Then
    '            BindFloor()
    '        Else
    '            ddlfloor.Items.Clear()
    '            ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlfloor.SelectedValue = 0
    '            ddlAssets.Items.Insert(0, New ListItem("--Select--", "0"))
    '            ddlAssets.SelectedValue = 0
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Protected Sub ddlfloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlfloor.SelectedIndexChanged
    '    Try
    '        If ddlfloor.SelectedIndex > 0 Then
    '            BindAssets()
    '        Else
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Private Sub getlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_Location_GetAll")
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        If ddlAssetCategory.SelectedIndex <> 0 Then
            'ddlAstBrand.Items.Clear()
            'ddlModel.Items.Clear()
            ddlAstBrand.SelectedIndex = 0
            ddlModel.SelectedIndex = 0
            ddlLocation.SelectedIndex = 0
            ddlTower.SelectedIndex = 0
            ddlFloor.SelectedIndex = 0
            lblMinordqty.Text = ""
            lblstockqty.Text = ""
            txtAstQty.Text = ""
            txtmnfdate.Text = ""
            txtExpdate.Text = ""
            getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)

        End If
    End Sub

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@companyid", Session("companyid"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        If ddlAstSubCat.SelectedIndex <> 0 Then
            'ddlModel.Items.Clear()
            ddlModel.SelectedIndex = 0
            ddlLocation.SelectedIndex = 0
            lblMinordqty.Text = ""
            lblstockqty.Text = ""
            txtAstQty.Text = ""
            txtmnfdate.Text = ""
            txtExpdate.Text = ""
            getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        End If
    End Sub

    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")
        'getconsumbles()
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex <> 0 Then
            ddlLocation.SelectedIndex = 0
            lblMinordqty.Text = ""
            lblstockqty.Text = ""
            txtAstQty.Text = ""
            txtmnfdate.Text = ""
            txtExpdate.Text = ""
            getmakebycatsubcat()
        End If
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strpath As String = String.Empty
        If btnSubmit.Text = "Add" Then
            If CDate(txtmnfdate.Text) > CDate(txtExpdate.Text) Then
                Label1.Text = "Manufactured Date Must be earlier than Expired Date."
                Label1.Visible = True
                Exit Sub
            End If

            strpath = insertdata()
        Else
            strpath = modifydata()
        End If
        If strpath <> "" Then
            Response.Redirect(strpath, False)
        End If


    End Sub

    Public Function insertdata() As String
        Dim assetcode As String = String.Empty
        If IsNumeric(txtAstQty.Text) = True Then
            If Val(txtAstQty.Text) < Val(lblMinordqty.Text) Then
                Label1.Text = "Choose a file Quantity should be more than minimum order quantity"
                Return ""
                Exit Function
            End If
        Else
            Label1.Text = "The Quantity must be an integer."
            Return ""
            Exit Function
        End If

        'If Val(txtmnfdate.Text) > Val(txtExpdate.Text) Then
        If CDate(txtExpdate.Text) < CDate(txtmnfdate.Text) Then
            Label1.Text = "The Manufactured Date must be earlier than the Expired Date"
            Return ""


            Exit Function
        End If



        If ddlLocation.SelectedIndex <> 0 And ddlAssetCategory.SelectedIndex <> 0 And ddlAstSubCat.SelectedIndex <> 0 And ddlAstBrand.SelectedIndex <> 0 And ddlModel.SelectedIndex <> 0 Then
            Dim AstCode As String = ddlLocation.SelectedItem.Value + "/" + ddlAssetCategory.Text + "/" + ddlAstSubCat.Text + "/" + ddlAstBrand.Text + "/" + ddlModel.Text
            assetcode = AstCode
        End If

        Dim param1(19) As SqlParameter
        param1(0) = New SqlParameter("@AAC_CODE", SqlDbType.NVarChar, 200)
        param1(0).Value = assetcode
        param1(1) = New SqlParameter("@AAC_NAME", SqlDbType.NVarChar, 200)
        param1(1).Value = assetcode
        param1(2) = New SqlParameter("@AAC_CON_MINQTY", SqlDbType.Int)
        param1(2).Value = lblMinordqty.Text
        param1(3) = New SqlParameter("@AAC_CON_MINSTKQTY", SqlDbType.Int)
        param1(3).Value = lblstockqty.Text
        param1(4) = New SqlParameter("@AAC_CON_TOTAVBL", SqlDbType.Int)
        param1(4).Value = txtAstQty.Text
        param1(5) = New SqlParameter("@AAC_CON_LOCID", SqlDbType.NVarChar, 200)
        param1(5).Value = ddlLocation.SelectedValue
        param1(6) = New SqlParameter("@AAC_CON_AVR_ID", SqlDbType.NVarChar, 200)
        param1(6).Value = ddlVendor.SelectedValue
        param1(7) = New SqlParameter("@AAC_CON_MODEL", SqlDbType.NVarChar, 2000)
        param1(7).Value = ddlModel.SelectedValue
        param1(8) = New SqlParameter("@AAC_CON_BRDID", SqlDbType.NVarChar, 200)
        param1(8).Value = ddlAstBrand.SelectedValue
        param1(9) = New SqlParameter("@AAC_CON_SUBCATID", SqlDbType.NVarChar, 200)
        param1(9).Value = ddlAstSubCat.SelectedValue
        param1(10) = New SqlParameter("@AAC_CON_CATID", SqlDbType.NVarChar, 200)
        param1(10).Value = ddlAssetCategory.SelectedValue
        param1(11) = New SqlParameter("@AAC_CON_STAID", SqlDbType.Int)
        param1(11).Value = 1
        param1(12) = New SqlParameter("@AAC_CON_CREATEDBY", SqlDbType.NVarChar, 200)
        param1(12).Value = Session("Uid")
        param1(13) = New SqlParameter("@AAC_CON_MNFG_DATE", SqlDbType.DateTime, 200)
        param1(13).Value = txtmnfdate.Text
        param1(14) = New SqlParameter("@AAC_CON_EXP_DATE", SqlDbType.DateTime, 200)
        param1(14).Value = txtExpdate.Text
        param1(15) = New SqlParameter("@AAC_CON_REMARKS", SqlDbType.DateTime, 200)
        param1(15).Value = txtRemarks.Text
        param1(16) = New SqlParameter("@AAC_TOWER_CODE", SqlDbType.NVarChar, 200)
        param1(16).Value = ddlTower.SelectedValue
        param1(17) = New SqlParameter("@AAC_FLOOR_CODE", SqlDbType.NVarChar, 200)
        param1(17).Value = ddlFloor.SelectedValue
        param1(18) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param1(18).Value = HttpContext.Current.Session("COMPANYID")
        param1(19) = New SqlParameter("@LOB", SqlDbType.NVarChar, 200)
        param1(19).Value = ddlDepartment.SelectedValue

        Dim flag As Integer = CInt(ObjSubsonic.GetSubSonicExecuteScalar("INSERT_CONSUMABLE_ASSET_STOCK", param1))
        If flag = 1 Then
            'strpath = "frmAssetThanks.aspx?rid=addstock"
            Label1.Text = "Asset Successfully Added "
            Label1.Visible = True
            cleardata()
        Else
            Label1.Text = "Asset has already been added for this place."
        End If
        Return strpath
    End Function

    Public Function modifydata() As String


        Dim assetcode As String = String.Empty
        If IsNumeric(txtAstQty.Text) = True Then
            If Val(txtAstQty.Text) < Val(lblMinordqty.Text) Then
                Label1.Text = "Choose a file Quantity should be more than minimum order quantity"
                Return ""
                Exit Function
            End If
        Else
            Label1.Text = "The Quantity must be an integer."
            Return ""
            Exit Function
        End If

        If ddlLocation.SelectedIndex <> 0 And ddlAssetCategory.SelectedIndex <> 0 And ddlAstSubCat.SelectedIndex <> 0 And ddlAstBrand.SelectedIndex <> 0 And ddlModel.SelectedIndex <> 0 Then
            Dim AstCode As String = ddlLocation.SelectedItem.Value + "/" + ddlAssetCategory.Text + "/" + ddlAstSubCat.Text + "/" + ddlAstBrand.Text + "/" + ddlModel.Text
            assetcode = AstCode
        End If
        Dim param1(19) As SqlParameter
        param1(0) = New SqlParameter("@AAC_CODE", SqlDbType.NVarChar, 200)
        param1(0).Value = assetcode
        param1(1) = New SqlParameter("@AAC_NAME", SqlDbType.NVarChar, 200)
        param1(1).Value = assetcode
        param1(2) = New SqlParameter("@AAC_CON_MINQTY", SqlDbType.Int)
        param1(2).Value = lblMinordqty.Text
        param1(3) = New SqlParameter("@AAC_CON_MINSTKQTY", SqlDbType.Int)
        param1(3).Value = lblstockqty.Text
        param1(4) = New SqlParameter("@AAC_CON_TOTAVBL", SqlDbType.Int)
        param1(4).Value = txtAstQty.Text
        param1(5) = New SqlParameter("@AAC_CON_LOCID", SqlDbType.NVarChar, 200)
        param1(5).Value = ddlLocation.SelectedValue
        param1(6) = New SqlParameter("@AAC_CON_AVR_ID", SqlDbType.NVarChar, 200)
        param1(6).Value = ddlVendor.SelectedValue
        param1(7) = New SqlParameter("@AAC_CON_MODEL", SqlDbType.NVarChar, 2000)
        param1(7).Value = ddlModel.SelectedValue
        param1(8) = New SqlParameter("@AAC_CON_BRDID", SqlDbType.NVarChar, 200)
        param1(8).Value = ddlAstBrand.SelectedValue
        param1(9) = New SqlParameter("@AAC_CON_SUBCATID", SqlDbType.NVarChar, 200)
        param1(9).Value = ddlAstSubCat.SelectedValue
        param1(10) = New SqlParameter("@AAC_CON_CATID", SqlDbType.NVarChar, 200)
        param1(10).Value = ddlAssetCategory.SelectedValue
        param1(11) = New SqlParameter("@AAC_CON_STAID", SqlDbType.Int)
        param1(11).Value = 1
        param1(12) = New SqlParameter("@AAC_CON_CREATEDBY", SqlDbType.NVarChar, 200)
        param1(12).Value = Session("Uid")
        param1(13) = New SqlParameter("@AAC_CON_MNFG_DATE", SqlDbType.DateTime, 200)
        param1(13).Value = txtmnfdate.Text
        param1(14) = New SqlParameter("@AAC_CON_EXP_DATE", SqlDbType.DateTime, 200)
        param1(14).Value = txtExpdate.Text
        param1(15) = New SqlParameter("@AAC_CON_REMARKS", SqlDbType.DateTime, 200)
        param1(15).Value = txtRemarks.Text
        param1(16) = New SqlParameter("@AAC_TOWER_CODE", SqlDbType.NVarChar, 200)
        param1(16).Value = ddlTower.SelectedValue
        param1(17) = New SqlParameter("@AAC_FLOOR_CODE", SqlDbType.NVarChar, 200)
        param1(17).Value = ddlFloor.SelectedValue
        param1(18) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param1(18).Value = HttpContext.Current.Session("COMPANYID")
        param1(19) = New SqlParameter("@LOB", SqlDbType.NVarChar, 200)
        param1(19).Value = ddlDepartment.SelectedValue

        Dim flag As Integer = CInt(ObjSubsonic.GetSubSonicExecuteScalar("UPDATE_CONSUMABLE_ASSET_STOCK", param1))
        If flag = 1 Then
            'strpath = "frmAssetThanks.aspx?rid=modifystock"
            Label1.Text = "Asset successfully modified"
            Label1.Visible = True
            cleardata()
        End If
        Return strpath
    End Function

    'Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendor.SelectedIndexChanged
    '    If ddlVendor.SelectedIndex <> 0 Then
    '        ddlAstSubCat.Items.Clear()
    '        ddlAstBrand.Items.Clear()
    '        ddlModel.Items.Clear()
    '        ddlLocation.SelectedIndex = 0
    '        lblMinordqty.Text = ""
    '        lblstockqty.Text = ""
    '        txtAstQty.Text = ""
    '        txtmnfdate.Text = ""
    '        txtExpdate.Text = ""

    '    End If
    'End Sub

    Private Sub getvendors()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VENDORS")
        sp.Command.AddParameter("@dummy", 2, DbType.Int32)
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
        Dim ds As New DataSet
        If ddlModel.SelectedIndex <> 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMG_ITEMQTY")
            sp.Command.AddParameter("@AST_MD_CODE", ddlModel.SelectedValue, DbType.String)
            ds = sp.GetDataSet()

            lblMinordqty.Text = ds.Tables(0).Rows(0).Item("AST_MD_MINQTY")
            lblstockqty.Text = ds.Tables(0).Rows(0).Item("AST_MD_STKQTY")
            lblunit.Text = ds.Tables(0).Rows(0).Item("AST_MD_QTYUNITS")
        End If

    End Sub

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        cleardata()
        If rbActions.Checked = True Then
            lblMsg.Visible = False
            'ddlAsset.Visible = False
            'lblAsset.Visible = False
            trselVendor.Visible = False
            btnSubmit.Text = "Add"
            ddlAssetCategory.Items.Insert(0, "--Select--")
            getassetcategory()
            ddlLocation.ClearSelection()
            ' fillgrid()
        Else
            lblMsg.Visible = False
            'ddlAsset.Visible = True
            'lblAsset.Visible = True
            trselVendor.Visible = True
            btnSubmit.Text = "Modify"
            ddlAsset.Items.Clear()
            'ddlAssetCategory.Items.Clear()
            'ddlAstBrand.SelectedIndex = 0
            'ddlModel.SelectedIndex = 0
            getassets()
            'getasset()
            'fillgrid()
        End If
        ddlAssetCategory.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
    End Sub

    Private Sub cleardata()
        ddlAsset.ClearSelection()
        ddlLocation.ClearSelection()
        ddlVendor.Enabled = True
        ddlVendor.SelectedIndex = 0
        'ddlAssetCategory.Items.Clear()
        'ddlAstSubCat.Items.Clear()
        'ddlAstBrand.Items.Clear()
        'ddlModel.Items.Clear()
        ddlAssetCategory.SelectedIndex = 0
        ddlAstSubCat.SelectedIndex = 0
        ddlAstBrand.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        ddlModel.SelectedIndex = 0
        ddlTower.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        lblMinordqty.Text = String.Empty
        lblstockqty.Text = String.Empty
        lblunit.Text = String.Empty
        txtExpdate.Text = String.Empty
        txtmnfdate.Text = String.Empty
        txtRemarks.Text = String.Empty
        txtAstQty.Text = String.Empty

        ddlAssetCategory.Enabled = True
        ddlAstBrand.Enabled = True
        ddlAstSubCat.Enabled = True
        ddlLocation.Enabled = True
        ddlModel.Enabled = True
        ddlVendor.Enabled = True

    End Sub

    Public Sub getassets()
        ObjSubsonic.Binddropdown(ddlAsset, "GET_CONS_ASSETS", "AST_MD_NAME", "AAC_CODE")
    End Sub

    Protected Sub ddlAsset_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAsset.SelectedIndexChanged
        If ddlAsset.SelectedIndex <> 0 Then
            Try

                Dim dr As SqlDataReader
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@AAC_CODE", ddlAsset.SelectedValue)
                dr = ObjSubsonic.GetSubSonicDataReader("GET_CONS_ASSET_MODIFY", param)
                If dr.Read() Then
                    'Dim lcmCode As String = dr("AAC_CON_LOC_ID")

                    getassetcategory()
                    ddlAssetCategory.ClearSelection()
                    ddlAssetCategory.Items.FindByValue(dr("AAC_CON_CATID")).Selected = True
                    getsubcategorybycat(ddlAssetCategory.SelectedValue)
                    ddlAstSubCat.ClearSelection()
                    ddlAstSubCat.Items.FindByValue(dr("AAC_CON_SUBCATID")).Selected = True
                    getbrandbycatsubcat(ddlAssetCategory.SelectedValue, ddlAstSubCat.SelectedValue)
                    ddlAstBrand.ClearSelection()
                    ddlAstBrand.Items.FindByValue(dr("AAC_CON_BRDID")).Selected = True
                    getmakebycatsubcat()
                    ddlModel.ClearSelection()
                    ddlModel.Items.FindByValue(dr("AAC_CON_MODEL")).Selected = True
                    ddlModel_SelectedIndexChanged(sender, e)
                    'getlocation()
                    BindLocation()
                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(dr("AAC_CON_LOCID")).Selected = True
                    getTowersByLocations(ddlLocation.SelectedValue)
                    ddlTower.ClearSelection()
                    ddlTower.Items.FindByValue(dr("AAC_TOWER_CODE")).Selected = True
                    getFloorsByLocationTower(ddlLocation.SelectedValue, ddlTower.SelectedValue)
                    ddlFloor.ClearSelection()
                    ddlFloor.Items.FindByValue(dr("AAC_FLOOR_CODE")).Selected = True
                    getLOB()
                    ddlDepartment.ClearSelection()
                    ddlDepartment.Items.FindByValue(dr("AAC_LOB")).Selected = True
                    getvendors()
                    ddlVendor.ClearSelection()
                    ddlVendor.Items.FindByValue(dr("AAC_CON_AVR_ID")).Selected = True
                    txtAstQty.Text = dr("AAC_CON_TOTAVBL").ToString()
                    txtmnfdate.Text = dr("AAC_CON_MNFG_DATE").ToString()
                    txtExpdate.Text = dr("AAC_CON_EXP_DATE").ToString()
                    txtRemarks.Text = dr("AAC_CON_REMARKS").ToString()
                    lblunit.Text = dr("AAC_CON_UNIT_NAME").ToString()

                    '' disabling the controls
                    ddlAssetCategory.Enabled = False
                    ddlAstBrand.Enabled = False
                    ddlAstSubCat.Enabled = False
                    ddlLocation.Enabled = False
                    ddlModel.Enabled = False
                    ddlVendor.Enabled = False
                    ddlTower.Enabled = False
                    ddlFloor.Enabled = False

                Else
                    cleardata()
                End If

            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
        btnSubmit.Text = "Add"
        rbActions.Checked = True
        'ddlAsset.Visible = False
        'lblAsset.Visible = False
        trselVendor.Visible = False
        ddlAssetCategory.Enabled = True
        ddlAstBrand.Enabled = True
        ddlAstSubCat.Enabled = True
        ddlLocation.Enabled = True
        ddlModel.Enabled = True
        ddlVendor.Enabled = True
    End Sub

    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only"
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                'Dim vednor As Integer = 1
                'Dim asst_catgory As Integer = 1
                'Dim asst_subcatgory As Integer = 1
                'Dim asst_brand As Integer = 1
                'Dim asst_model As Integer = 1
                'Dim loc As Integer = 1
                'Dim asst_qty As Integer = 1
                'Dim asst_mfgdate As Integer = 1
                'Dim asst_expdate As Integer = 1
                'Dim asst_remark As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                'For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                '    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                '        If LCase(ds.Tables(0).Columns(0).ToString) <> "vendor" Then
                '            vednor = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be vendor"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset category" Then
                '            asst_catgory = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be Asset Category"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset sub category" Then
                '            asst_subcatgory = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be asset subcategory"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset brand/make" Then
                '            asst_brand = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be asset brand/make"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset model" Then
                '            asst_model = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be asset model"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "location" Then
                '            loc = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be location"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset qty" Then
                '            asst_qty = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be asset quantity"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset mfg date" Then
                '            asst_mfgdate = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be asset mfg date"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset exp date" Then
                '            asst_expdate = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Column name should be asset exp date"
                '            Exit Sub
                '        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "remarks" Then
                '            asst_remark = 0
                '            lblMsg.Visible = True
                '            lblMsg.Text = "Remark column is empty"
                '            Exit Sub
                '        End If
                '    Next
                'Next

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "vendor" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Upload excel files onlyvendor is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset category" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Category is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset subcategory" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Sub Category is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset brand/make" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Brand/Make is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset model" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Model is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "location" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Location is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If


                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset qty" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Quantity is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset mfg date" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Mfg Date is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset exp date" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Asset Exp Date is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "tower" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Tower is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "floor" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Floor is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                Next

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'Start inseting excel data
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_EXCEL_CONSUMABLE_ASSET_STOCK_AM")
                    sp.Command.AddParameter("@VENDOR", ds.Tables(0).Rows(i).Item("Vendor").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_CATEGORY", ds.Tables(0).Rows(i).Item("Asset Category").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_SUB_CATEGORY", ds.Tables(0).Rows(i).Item("Asset Sub Category").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_BRAND", ds.Tables(0).Rows(i).Item("Asset Brand/Make").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_MODEL", ds.Tables(0).Rows(i).Item("Asset Model").ToString, DbType.String)
                    sp.Command.AddParameter("@LOCATION", ds.Tables(0).Rows(i).Item("Location").ToString, DbType.String)
                    sp.Command.AddParameter("@TOWER", ds.Tables(0).Rows(i).Item("Tower").ToString, DbType.String)
                    sp.Command.AddParameter("@FLOOR", ds.Tables(0).Rows(i).Item("Floor").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_QTY", ds.Tables(0).Rows(i).Item("Asset Qty").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_MRF", ds.Tables(0).Rows(i).Item("Asset Mfg Date").ToString, DbType.DateTime)
                    sp.Command.AddParameter("@ASSET_EXP", ds.Tables(0).Rows(i).Item("Asset Exp Date").ToString, DbType.DateTime)
                    sp.ExecuteScalar()
                    'End inseting excel data
                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_EXCEL_CONSUMABLE_ASSET_STOCK_AM")
                Dim rst As New DataSet
                rst = sp1.GetDataSet()
                gvitems.DataSource = rst
                gvitems.DataBind()
                lblMsg.Visible = True
                lblMsg.Text = "Data upload successful"
            Else
                lblMsg.Text = ""
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex <> 0 Then
            getTowersByLocations(ddlLocation.SelectedItem.Value)
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlLocation.SelectedIndex <> 0 And ddlTower.SelectedIndex <> 0 Then
            getFloorsByLocationTower(ddlLocation.SelectedItem.Value, ddlTower.SelectedItem.Value)
        End If
    End Sub

 
End Class
