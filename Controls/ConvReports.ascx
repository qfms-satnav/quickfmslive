<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConvReports.ascx.vb" Inherits="Controls_ConvReports" %>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Approve Leaves Found.">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("LVE_REQ_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Requested By">
                    <ItemTemplate>
                        <asp:Label ID="lblReqBy" runat="server" CssClass="bodyText" Text='<%#Eval("LVE_EMP_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Encash Days">
                    <ItemTemplate>
                        <asp:Label ID="lblTotDays" runat="Server" CssClass="bodyText" Text='<%#Eval("LVE_ENCASH_DAYS") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested On">
                    <ItemTemplate>
                        <asp:Label ID="lblReqOn" runat="Server" CssClass="bodyText" Text='<%#Eval("LVE_REQUESTED_ON") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View">
                    <ItemTemplate>
                        <a href='frmLveRMApprovalEncash.aspx?ReqID=<%#Eval("LVE_REQ_ID") %>'>View Details</a>

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>



