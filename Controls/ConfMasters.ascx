<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfMasters.ascx.vb"
    Inherits="Controls_ConfMasters" %>
<script>
    function showPopWin(id) {
        $("#modelcontainer").load("frmLiveAtt.aspx", function (responseTxt, statusTxt, xhr) {
            $("#myModal").modal('show');
        });
    }
</script>
<asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
<div class="row">
    <div class="col-md-6">
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-8 control-label">
                    </div>
                    <div class="col-md-4">
                        <a href="#" onclick="showPopWin('mm')">Live Attendance</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select From Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvfrm" runat="server" ControlToValidate="txtFrmDate"
                    ErrorMessage="Please Select From Date" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtFrmDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select To Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="Please Select To Date" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='todate'>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"
                CausesValidation="True" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvItems" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                EmptyDataText="No Attendance Register Found.">
                <Columns>
                    <asp:TemplateField HeaderText="Associate">
                        <ItemTemplate>
                            <asp:Label ID="lblAssociate" runat="Server" Text='<%#Eval("EMP_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%#Eval("TODAYDATE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="In-Time">
                        <ItemTemplate>
                            <asp:Label ID="lblInTime" runat="server" Text='<%#Eval("INTIME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Out-Time">
                        <ItemTemplate>
                            <asp:Label ID="lblOutTime" runat="server" Text='<%#Eval("OUTTIME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
        </div>
    </div>
</div>
<div id="tablegrid1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvItems1" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                    EmptyDataText="No Attendance Register Found.">
                    <Columns>
                        <asp:TemplateField HeaderText="Associate">
                            <ItemTemplate>
                                <asp:Label ID="lblAssociate1" runat="Server" Text='<%#Eval("EMP_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDate1" runat="server" Text='<%#Eval("TODAYDATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="In-Time">
                            <ItemTemplate>
                                <asp:Label ID="lblInTime1" runat="server" Text='<%#Eval("INTIME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Out-Time">
                            <ItemTemplate>
                                <asp:Label ID="lblOutTime1" runat="server" Text='<%#Eval("OUTTIME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Live Attendance</h4>
            </div>
            <div class="modal-body" id="modelcontainer">
                <%-- Content loads here --%>
            </div>
        </div>
    </div>
</div>
