<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Mapped_unMapped_Summary.ascx.vb" Inherits="GenericHandler_Mapped_unMapped_Summary" %>
<table width="100%" cellpadding="2">
        <tr>
            <td align="left" valign="top">
            
                <fieldset>
                    <legend>Mapped Assets </legend>
                    <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                        EmptyDataText="No Asset(s) Found." Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="AssetCode">
                                <ItemTemplate>
                                    <asp:Label ID="lblcode" runat="server" Text='<%#Eval("AAS_AAT_CODE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AssetName">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("PRODUCTNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Employee">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("EMPLOYEE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <fieldset>
                    <legend>Unmapped Assets</legend>
                    <asp:GridView ID="gvUnmappedAst" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                        EmptyDataText="No Asset(s) Found." Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="AssetName">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("PRODUCTNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AssetCode">
                                <ItemTemplate>
                                    <asp:Label ID="lblcode" runat="server" Text='<%#Eval("AAS_AAT_CODE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </fieldset>
            </td>
        </tr>
    </table>