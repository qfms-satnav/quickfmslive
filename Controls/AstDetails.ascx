<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AstDetails.ascx.vb" Inherits="Controls_AstDetails" %>
<%@ Register Src="AstDescriptorDisplay.ascx" TagName="AstDescriptorDisplay" TagPrefix="uc2" %>
<%@ Register Src="AstAttributeSelection.ascx" TagName="AstAttributeSelection" TagPrefix="uc1" %>
<table border="0" width="100%" cellpadding="8">
    <tr>
        <td valign="top" align="left">
            <h1 style="color: blue; margin: 0">
                <%=product.productname%>
            </h1>
            <br />
            <%=product.ShortDescription%>
        </td>
    </tr>
    <tr>
        <td >
          
            <uc2:AstDescriptorDisplay ID="AstDescriptorDisplay1" runat="server" />
        </td>
    </tr>
     
</table>
