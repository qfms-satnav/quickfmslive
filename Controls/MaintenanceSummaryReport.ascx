<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MaintenanceSummaryReport.ascx.vb"
    Inherits="Controls_MaintenanceSummaryReport" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center" style="height: 54px">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Maintenance Summary Report
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Maintenance Summary Report</strong></td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        &nbsp; &nbsp; &nbsp;
                        <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                            <tr>
                                <td valign="top" width="50%" style="height: 50%">
                                    <asp:Button ID="btnexporttoexcel" runat="Server" Text="Export to Excel" CssClass="button" />
                                    <asp:GridView ID="gvTOCReqs" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="99%"
                                        AllowPaging="True" AllowSorting="false" PageSize="5" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Total No.of Open Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTOReqs" CommandName="TotOpenReqs" runat="server" Text='<%#BIND("OPEN_REQS")%>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total No.of Closed Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTCReqs" CommandName="TotClosedReqs" runat="server" Text='<%#BIND("CLOSED_REQS")%>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                    <br />
                                </td>
                                <td valign="top" width="50%" style="height: 50%">
                                    <asp:Button ID="btnExprtBldng" runat="Server" Text="Export to Excel" CssClass="button" />
                                    <asp:GridView ID="gvProp" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                        AllowPaging="True" AllowSorting="false" PageSize="5" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="PropertyCode" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpropcode" runat="server" Text='<%#Eval("BDG_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProp" runat="Server" Text='<%#Eval("PN_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No.of Open Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkOpen1" CommandArgument='<%#Eval("BDG_ID")%>' runat="server"
                                                        Text='<%#BIND("OPEN1")%>' CommandName="OpenBuilding"> </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No.of Closed Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkClosed" CommandArgument='<%#Eval("BDG_ID")%>' runat="server"
                                                        Text='<%#BIND("CLOSED")%>' CommandName="ClosedBuilding"> </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="50%" style="height: 50%">
                                    <asp:Button ID="btnExprtAll" runat="Server" Text="Export to Excel" CssClass="button" />
                                    <asp:GridView ID="gvAll" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="99%"
                                        AllowPaging="True" AllowSorting="false" PageSize="5" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Request Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRequestType" runat="Server" Text='<%#Eval("Request_Type")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No.of Open Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTOReqs" CommandArgument='<%#Eval("Request_Type")%>' runat="server"
                                                        Text='<%#BIND("OPEN")%>' CommandName="OpenReqType"> </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No.of Closed Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTCReqs" CommandArgument='<%#Eval("Request_Type")%>' runat="server"
                                                        Text='<%#BIND("Closed")%>' CommandName="ClosedReqType"> </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                </td>
                                <td valign="top" width="25%" style="height: 50%">
                                    <asp:Button ID="btnExprtTenant" runat="Server" Text="Export to Excel" CssClass="button" />
                                    <asp:GridView ID="gvTenant" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="99%"
                                        AllowPaging="True" AllowSorting="false" PageSize="5" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Tenant Code" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcode" runat="server" Text='<%Eval("TEN_CODE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTname" runat="Server" Text='<%#Eval("TEN_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTDate" runat="Server" Text='<%#Eval("TEN_COMMENCE_DATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No.of Open Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTOReqsT" CommandArgument='<%#Eval("TEN_CODE")%>' runat="server"
                                                        Text='<%#BIND("OPEN1")%>' CommandName="OpenTenant"> </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No.of Closed Requests">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTCReqsT" CommandArgument='<%#Eval("TEN_CODE")%>' runat="server"
                                                        Text='<%#BIND("CLOSED")%>' CommandName="ClosedTenant"> </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="50%" style="height: 50%">
                                    <asp:Button ID="btnEBLDNGRTYPEO" runat="Server" Text="Export to Excel" CssClass="button" />
                                    <asp:GridView ID="gvEBLDNGRTYPEO" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="99%"
                                        AllowPaging="True" AllowSorting="false" PageSize="1" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Request Deatils">
                                                <ItemTemplate>
                                                    <table width="100%" border="1" cellpadding="1" cellspacing="1">
                                                        <tr align="left">
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                ID
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblPN_MAINTENANCE_ID" runat="server" Text='<%#Eval("PN_MAINTENANCE_ID")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="Left">
                                                                PROPERTY NAME
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblprop" runat="server" Text='<%#Eval("PN_BLDG_CODE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td align="Left">
                                                                CITY
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCity" runat="server" Text='<%#Eval("PN_LOC_ID")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="Left">
                                                                TENANT NAME
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblTenant" runat="server" Text='<%#Eval("PN_TENANT_ID")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="Left">
                                                                REQUEST TYPE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_TYPE" runat="server" Text='<%#Eval("REQUEST_TYPE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST TITLE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_TITLE" runat="server" Text='<%#Eval("REQUEST_TITLE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST DESCRIPTION
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_DESCRIPTION" runat="server" Text='<%#Eval("REQUEST_DESCRIPTION")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                       <%-- <tr>
                                                            <td align="left">
                                                                REQUEST DOC
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_DOC" runat="server" Text='<%#Eval("REQUEST_DOC")%>'></asp:Label>
                                                            </td>
                                                        </tr>--%>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST STATUS
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_STATUS" runat="server" Text='<%#Eval("REQUEST_STATUS")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST RAISED BY
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_RAISED_BY" runat="server" Text='<%#Eval("REQUEST_RAISED_BY")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUESTED DATE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUESTED_DATE" runat="server" Text='<%#Eval("REQUESTED_DATE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                LAST UPDATED DATE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLAST_UPDATED_DATE" runat="server" Text='<%#Eval("LAST_UPDATED_DATE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST ASSIGNED TO
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_ASSIGNED_TO" runat="server" Text='<%#Eval("REQUEST_ASSIGNED_TO")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                    <br />
                                </td>
                                <td valign="top" width="50%" style="height: 50%">
                                    <asp:Button ID="btnEBLDNGRTYPEC" runat="Server" Text="Export to Excel" CssClass="button" />
                                    <asp:GridView ID="gvEBLDNGRTYPEC" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                        AllowPaging="True" AllowSorting="false" PageSize="1" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Request Deatils">
                                                <ItemTemplate>
                                                    <table width="100%" border="1" cellpadding="1" cellspacing="1">
                                                        <tr align="left">
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                ID
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblPN_MAINTENANCE_IDC" runat="server" Text='<%#Eval("PN_MAINTENANCE_ID")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                          <tr>
                                                            <td align="Left">
                                                                PROPERTY NAME
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblprop" runat="server" Text='<%#Eval("PN_BLDG_CODE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td align="Left">
                                                                CITY
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblCity" runat="server" Text='<%#Eval("PN_LOC_ID")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="Left">
                                                                TENANT NAME
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblTenant" runat="server" Text='<%#Eval("PN_TENANT_ID")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="Left">
                                                                REQUEST TYPE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_TYPEC" runat="server" Text='<%#Eval("REQUEST_TYPE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST TITLE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_TITLEC" runat="server" Text='<%#Eval("REQUEST_TITLE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST DESCRIPTION
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_DESCRIPTIONC" runat="server" Text='<%#Eval("REQUEST_DESCRIPTION")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <%--<tr>
                                                            <td align="left">
                                                                REQUEST DOC
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_DOCC" runat="server" Text='<%#Eval("REQUEST_DOC")%>'></asp:Label>
                                                            </td>
                                                        </tr>--%>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST STATUS
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_STATUSC" runat="server" Text='<%#Eval("REQUEST_STATUS")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST RAISED BY
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_RAISED_BYC" runat="server" Text='<%#Eval("REQUEST_RAISED_BY")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUESTED DATE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUESTED_DATEC" runat="server" Text='<%#Eval("REQUESTED_DATE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                LAST UPDATED DATE
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblLAST_UPDATED_DATEC" runat="server" Text='<%#Eval("LAST_UPDATED_DATE")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                REQUEST ASSIGNED TO
                                                            </td>
                                                            <td align="left">
                                                                <asp:Label ID="lblREQUEST_ASSIGNED_TOC" runat="server" Text='<%#Eval("REQUEST_ASSIGNED_TO")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" />
                    </td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnexporttoexcel" />
        <asp:PostBackTrigger ControlID="btnExprtAll" />
        <asp:PostBackTrigger ControlID="btnExprtBldng" />
        <asp:PostBackTrigger ControlID="btnExprtTenant" />
        <asp:PostBackTrigger ControlID="btnEBLDNGRTYPEC" />
        <asp:PostBackTrigger ControlID="btnEBLDNGRTYPEO" />
        <asp:PostBackTrigger ControlID="gvAll" />
        <asp:PostBackTrigger ControlID="gvProp" />
        <asp:PostBackTrigger ControlID="gvTenant" />
        <asp:PostBackTrigger ControlID="gvTOCReqs" />
        <asp:PostBackTrigger ControlID="gvEBLDNGRTYPEC" />
        <asp:PostBackTrigger ControlID="gvEBLDNGRTYPEO" />
    </Triggers>
</asp:UpdatePanel>
