Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AddCompensatory
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDep()
            Dept()
            BindRM()
            BindDetails()
            txtAssociateID.ReadOnly = True
            txtAssociateName.ReadOnly = True
            ddlRM.Enabled = False
            ddlDep.Enabled = False
            txtDesig.ReadOnly = True
            txtWorking.ReadOnly = True
            txtComp.ReadOnly = True

            btnSubmit.Enabled = False
            cboMin.Enabled = False
            ddlHH.Enabled = False
            ddlMM.Enabled = False

           
            txtFromDate.Text = getoffsetdate(Date.Today)
           
        End If
        txtFromDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GETEMPLOYEEDETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("AUR_ID")
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                ddlDep.ClearSelection()
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                ddlRM.ClearSelection()
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True
                txtContactNo.Text = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub cboHr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboHr.SelectedIndexChanged
        If cboHr.SelectedIndex > 0 Then
            cboMin.Enabled = True
        Else
            cboMin.SelectedIndex = 0
            ddlHH.SelectedIndex = 0
            ddlMM.SelectedIndex = 0
            cboMin.Enabled = False
            ddlHH.Enabled = False
            ddlMM.Enabled = False
            txtWorking.Text = ""
            txtComp.Text = ""
        End If
    End Sub

    Protected Sub cboMin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMin.SelectedIndexChanged
        If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 Then
            lblMsg.Visible = False
            ddlHH.Enabled = True
        Else
            ddlHH.SelectedIndex = 0
            ddlMM.SelectedIndex = 0
            ddlHH.Enabled = False
            ddlMM.Enabled = False
            txtWorking.Text = ""
            txtComp.Text = ""
        End If
    End Sub

    Protected Sub ddlHH_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlHH.SelectedIndexChanged
        If cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 Then
            ddlMM.Enabled = True
        Else
            ddlMM.SelectedIndex = 0
            ddlMM.Enabled = False
            txtWorking.Text = ""
            txtComp.Text = ""
        End If
    End Sub

    Protected Sub ddlMM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMM.SelectedIndexChanged
        If ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
            Bind_Compensatory()
        Else
            txtWorking.Text = ""
            txtComp.Text = ""
        End If
    End Sub
    Private Sub Bind_Compensatory()
        Try
            Dim hr As Decimal
            Dim min As Decimal
            Dim ddiff As Integer
            hr = CDbl(ddlHH.SelectedItem.Text) - CDbl(cboHr.SelectedItem.Text)
            If ddlMM.SelectedItem.Text > cboMin.SelectedItem.Text Then
                hr = (ddlHH.SelectedItem.Text - cboHr.SelectedItem.Text)
                min = (ddlMM.SelectedItem.Text - cboMin.SelectedItem.Text)
            ElseIf ddlMM.SelectedItem.Text < cboMin.SelectedItem.Text Then
                min = (ddlMM.SelectedItem.Text + 60) - cboMin.SelectedItem.Text
                hr = (hr - 1)
            ElseIf ddlMM.SelectedItem.Text = cboMin.SelectedItem.Text Then
                min = ddlMM.SelectedItem.Text - cboMin.SelectedItem.Text
                hr = hr
            End If

            Dim startDate As DateTime
            Dim endDate As DateTime
            If DateTime.TryParse(txtFromDate.Text, startDate) AndAlso DateTime.TryParse(txtToDate.Text, endDate) Then

            ElseIf txtFromDate.Text <> "" AndAlso txtToDate.Text <> "" Then
                lblMsg.Visible = True
                lblMsg.Text = "Please Select Proper Date"
                Exit Sub
            End If

            If txtFromDate.Text = "" Then
                lblMsg.Text = ""
                lblMsg.Visible = True
                lblMsg.Text = " Select From Date "
                ddlMM.Enabled = False
                cboHr.SelectedIndex = 0
            ElseIf txtToDate.Text = "" Then
                lblMsg.Text = ""
                lblMsg.Visible = True
                lblMsg.Text = " Select To Date "
                ddlMM.Enabled = False
                cboHr.SelectedIndex = 0
            ElseIf cboHr.SelectedIndex = 0 Then
                lblMsg.Text = ""
                lblMsg.Visible = True
                lblMsg.Text = " Select Hours "
            Else
                ddiff = DateDiff(DateInterval.Day, CDate(txtFromDate.Text), CDate(txtToDate.Text))
                Dim i As String
                i = (ddiff * 24) + Val(hr)
                If i >= 5 Then
                    lblMsg.Text = ""
                    btnSubmit.Enabled = True
                Else
                    lblMsg.Text = " Working Hours must be Minimum of 5 Hrs"
                    lblMsg.Visible = True
                    btnSubmit.Enabled = False
                End If
                txtWorking.Text = i & " : " & min
                Dim j As Integer
                j = Fix(i / 8)
                Dim n As Integer
                n = i Mod 8
                If n >= 5 Then
                    j = j + 1
                End If
                txtComp.Text = j
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim startDate As DateTime
        Dim endDate As DateTime

        If DateTime.TryParse(txtFromDate.Text, startDate) AndAlso DateTime.TryParse(txtToDate.Text, endDate) Then

        Else
            lblMsg.Text = "Please Check if the Dates are Proper"
            Exit Sub
        End If

        Try
            Dim ValidateComp As Integer
            ValidateComp = CheckComp()
            If ValidateComp = 0 Then
                lblMsg.Text = "You already applied CompOff on the selected dates."
                lblMsg.Visible = True
                txtWorking.Text = ""
                txtComp.Text = ""
                ddlMM.SelectedIndex = 0
                ddlHH.SelectedIndex = 0
                cboMin.SelectedIndex = 0
                cboHr.SelectedIndex = 0
            Else
                Dim REQID As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + txtAssociateID.Text
                Dim Req As Integer
                Req = ValidateReq(REQID)
                If Req = 0 Then
                    lblMsg.Text = "Your Request Has Been Submitted Already"
                    lblMsg.Visible = True
                Else
                    If Convert.ToInt32(txtComp.Text) <= 10 Then
                        ADD_COMPOFF(REQID)
                        MAIL_CompOff(REQID)
                        Response.Redirect("status.aspx?staid=2&ReqID=" + REQID)
                    Else
                        lblMsg.Text = "Compensatory Days should be less than or equal to 10"
                        lblMsg.Visible = True
                    End If


                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub MAIL_CompOff(ByVal REQID As String)

        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays, fromdt, todt, NoofHours As String
        strSubj = "Compensatory Leave Requisition."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_COMP_MAIL_REQUEST")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()

        strReqId = ds.Tables(0).Rows(0).Item("LMS_COMREQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LMS_COMREQ_DATE")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LMS_COM_DAYS")
        fromdt = ds.Tables(0).Rows(0).Item("LMS_FDATE")
        todt = ds.Tables(0).Rows(0).Item("LMS_TDATE")
        NoofHours = ds.Tables(0).Rows(0).Item("LMS_COM_HRS")


        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL_COMP")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "Employee Service Desk"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Compensatory Leave Request." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Hours: </U></td>" & _
"<td width=50% align=left>" & NoofHours & "</td></tr></table>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_COMP")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()



        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")

        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Compensatory Leave Request and is pending for Approval." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Hours: </U></td>" & _
"<td width=50% align=left>" & NoofHours & "</td></tr></table>" & _
"Click here to <a href=""http://projects.a-mantra.com/AmantraAMPS/EFM/EFM_Webfiles/frmLveRMApprovalCompOff.aspx?ReqID=" & REQID & """>Accept / Reject</a><br><br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"

        strSubj = " Compensatory Leave Request - by " & strEmpName & " from " & fromdt & " - " & todt & " for " & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Function CheckComp()
        Dim ValidateLeaves As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_COMP_REQ_VALIDATE")
        sp.Command.AddParameter("@LMS_AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@LMS_COM_FDATE", txtFromDate.Text, DbType.DateTime)
        sp.Command.AddParameter("@LMS_COM_TDATE", txtToDate.Text, DbType.DateTime)
        ValidateLeaves = sp.ExecuteScalar()
        Return ValidateLeaves
    End Function
    Private Sub ADD_COMPOFF(ByVal REQID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_ADD_COMP_OFF")
            sp.Command.AddParameter("@LMS_COMREQ_ID", REQID, DbType.String)
            sp.Command.AddParameter("@LMS_COMAUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LMS_COMAUR_NO", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LMS_COMAUR_RM", ddlRM.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LMS_COMAUR_DSG", txtDesig.Text, DbType.String)
            sp.Command.AddParameter("@LMS_COMAUR_CON", txtContactNo.Text, DbType.String)
            sp.Command.AddParameter("@LMS_COM_FDATE", txtFromDate.Text, DbType.DateTime)
            sp.Command.AddParameter("@LMS_COM_HRS", txtWorking.Text, DbType.String)
            sp.Command.AddParameter("@LMS_COMREQ_NO", REQID, DbType.String)
            sp.Command.AddParameter("@LMS_COM_DAYS", txtComp.Text, DbType.String)
            sp.Command.AddParameter("@LMS_COM_TDATE", txtToDate.Text, DbType.DateTime)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateReq(ByVal REQID As String)
        Dim Req As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_COM_VALIDATE_REQID")
        sp.Command.AddParameter("@REQID", REQID, DbType.String)
        Req = sp.ExecuteScalar()
        Return Req
    End Function
End Class
