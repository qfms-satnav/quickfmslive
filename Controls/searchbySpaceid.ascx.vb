Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient
Partial Class Controls_searchbySpaceid
    Inherits System.Web.UI.UserControl
    Dim Getspace_status As New AddUsers
    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If txtSpcName.Text <> "" Then
            ds = Getspace_status.SearchbySpaceid(txtSpcName.Text)
            If ds.Tables(0).Rows.Count > 0 Then
                lblMsg.Text = "Status of the Space : " & ds.Tables(0).Rows(0).Item("SPACE_STATUS")
            End If
        End If
    End Sub
End Class
