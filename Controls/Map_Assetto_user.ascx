<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Map_Assetto_user.ascx.vb"
    Inherits="Controls_Map_Assetto_user" %>

<script language="javascript" type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal)
        {
            re = new RegExp(aspCheckBoxID)
            for(i = 0; i < aspnetForm.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox')
                {
                    if (re.test(elm.name))
                    elm.checked = checkVal
                }
           }
        }
       function CheckDataGrid()
        {
                var k=0;
                for(i = 0; i < aspnetForm.elements.length; i++)
                {
                    elm = document.forms[0].elements[i]
                    if (elm.type == 'checkbox')
                    {
                        if (elm.checked == true)
                        {
                                k=k+1;
                        }
                    }
               }
               if (k==0)
               {
                   window.alert('Please Select atleast one');
                   return false;
               }
               else
               {    var input= confirm("Are you sure you want to Release?");
                    if(input==true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
               }
        }
</script>

<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">Manage Asset(s) with Users
            </asp:Label>
            <hr align="center" width="60%" />
            <br />
        </td>
    </tr>
</table>
<table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
    border="0">
    <tr>
        <td>
            <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                width="9" /></td>
        <td width="100%" class="tableHEADER" align="left">
            &nbsp;<strong>Manage Asset(s) with Users</strong>
        </td>
        <td>
            <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                width="16" /></td>
    </tr>
    <tr>
        <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
            &nbsp;</td>
        <td align="left">
            &nbsp;<br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
            <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                <tr>
                    <td align="left" width="50%">
                        Asset Category:
                        <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                            Display="Dynamic" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                            InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" width="50%">
                        <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" Width="99%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:GridView ID="gvAssetList" runat="server" AllowPaging="True" AllowSorting="False"
                            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Records Found">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkEmp" runat="server" />
                                        <asp:Label runat="server" ID="lblProductId" Visible="false" Text='<%#bind("ProductId") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <input id="Checkbox1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkEmp',this.checked)">
                                    </HeaderTemplate>
                                    <ItemStyle Width="1px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product Name" ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAstProductName" runat="server" CssClass="bodytext" Text='<%#Eval("productName")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="50%">
                        <asp:Label ID="lblrole" runat="server" CssClass="bodytext" Text="Employee Id"></asp:Label>
                    </td>
                    <td align="left" width="50%">
                        <asp:TextBox ID="txtEmpId" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="center" colspan="3" style="height: 39px">
                        <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Add" ValidationGroup="Val1"
                            CausesValidation="true" />
                        <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Back" />
                            <asp:Button ID="btnSearch" CssClass="button" runat="server" Text="Search" />
                            <asp:LinkButton ID="LinkButton1" runat="server">Show All Category</asp:LinkButton></td>
                </tr>
                <%--<tr>
                    <td colspan="3" align="left">
                        <fieldset>
                            <legend>Search Employee </legend>
                            <asp:Label ID="Label2" runat="server" CssClass="bodytext" Text="Employee Id"></asp:Label>
                            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp; &nbsp;
                        </fieldset>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table id="Table1" width="100%" runat="Server" cellpadding="0" cellspacing="0" border="1">
                <tr>
                    <td valign="top" align="left">
                        <asp:GridView ID="gvEmployeeProductList" runat="server" AllowPaging="True" AllowSorting="False"
                            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                            AutoGenerateColumns="false" EmptyDataText="No Records Found">
                            <PagerSettings Mode="NumericFirstLast" />
                            <Columns>
                                <asp:TemplateField HeaderText="Category Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAstCategory" runat="server" CssClass="bodytext" Text='<%#Eval("CategoryName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Product Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAstProductName" runat="server" CssClass="bodytext" Text='<%#Eval("productName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="bodytext" Text='Delete' CommandArgument='<%#Eval("ID")%>'
                                            CommandName="Delete"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
        <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
            height: 100%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width: 10px; height: 17px;">
            <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                width="9" /></td>
        <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
            <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                width="25" /></td>
        <td style="height: 17px; width: 17px;">
            <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                width="16" /></td>
    </tr>
</table>
