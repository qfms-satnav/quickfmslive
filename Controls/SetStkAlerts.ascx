<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SetStkAlerts.ascx.vb"
    Inherits="Controls_SetStkAlerts" %>
<script type="text/javascript">
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID)
        for (i = 0; i < form1.elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'checkbox') {
                if (re.test(elm.name)) {
                    if (elm.disabled == false)
                        elm.checked = checkVal
                }
            }
        }
    }
    function ChildClick(CheckBox) {
        //Get target base & child control.
        var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
        var TargetChildControl = "chkSelect";
        //Get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        // check to see if all other checkboxes are checked
        for (var n = 0; n < Inputs.length; ++n)
            if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                // Whoops, there is an unchecked checkbox, make sure
                // that the header checkbox is unchecked
                if (!Inputs[n].checked) {
                    Inputs[0].checked = false;
                    return;
                }
            }
        // If we reach here, ALL GridView checkboxes are checked
        Inputs[0].checked = true;
    }
</script>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" ForeColor="Red" CssClass="col-md-12 control-label" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div id="BindFilters" runat="server">
    <div class="row">
         <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val1" />
         <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger"
            ForeColor="Red" ValidationGroup="Val2" />


        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                
                <label class="col-md-12 control-label">Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="reqastcat" runat="server" ErrorMessage="Please Enter Asset Category" InitialValue="--Select--"
                    ControlToValidate="ddlastCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True" Style="width: 77px">
                    </asp:DropDownList>

                </div>
            </div>
        </div>


        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Asset Sub Category <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rersubastcat" runat="server" ErrorMessage="Please Enter Asset sub Category" InitialValue="--Select--"
                    ControlToValidate="ddlastsubCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastsubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>

            </div>
        </div>


        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>

            </div>
        </div>


        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Asset Model<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>

            </div>
        </div>

    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Location<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                    ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color" runat="server"  OnClientClick="LoadingPopUp()" Text="Search" ValidationGroup="Val1" CausesValidation="true" />
                <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" ValidationGroup="Val1" CausesValidation="False" />
            </div>
        </div>
    </div>
</div>

<div id="panel1" runat="Server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <%--   <div id="divSearch" runat="Server">
                    <label style="margin-left: 15px">Employee Name<span style="margin-left: 10px"></span></label>
                    <asp:TextBox ID="txtSearchEmpName" runat="server" Style="width: 25%"></asp:TextBox>
                    <label style="margin-left: 15px">Asset Name<span style="margin-left: 10px"></span></label>
                    <asp:TextBox ID="txtAssetName" runat="server" Style="width: 25%"></asp:TextBox>
                </div>--%>
            <div class="row">
                           <div class="col-md-12 text-left">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                 <br />
                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search By Any..."  CssClass="form-control"></asp:TextBox>

                            </div>
                            <div class="col-md-4">
                                <br />
                                <asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" OnClientClick="LoadingPopUp()"  runat="server" Text="Search"
                                    CausesValidation="true" TabIndex="2" />
                            </div>
                            <div class="col-md-3 move-right" style="width: 217px !important">
                                <div class="form-group">
                                    <label id="Label2" runat="server">Surrendered Date</label>
                                     
                                    <div class='input-group date' id='fromdate'>
                                        <asp:TextBox ID="txtSurrendered_TAG1" runat="server" CssClass="form-control"  MaxLength="10"  AutoPostBack="True"  > </asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtSurrendered_TAG"
                                            ErrorMessage="Please Enter Surrendered Date" ValidationGroup="Val1" SetFocusOnError="True"
                                           Display="None"></asp:RequiredFieldValidator>--%>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                </div>
            </div>
            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AllowCustomPaging="True" AutoGenerateColumns="false"
                EmptyDataText="No Assets Found." CssClass="table GridStyle" GridLines="none">
                 <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                ToolTip="Click to check all" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAstSno" runat="server" Text='<%#Eval("AST_SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" Text='<%#Eval("AAT_AST_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAur_id" runat="server" Text='<%#Eval("AUR_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mapped To">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("AUR_NAME") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Id">
                        <ItemTemplate>
                            <asp:Label ID="lblAstID" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Asset Serial No">
                        <ItemTemplate>
                            <asp:Label ID="lblAstSerialno" runat="server" Text='<%#Eval("AAT_AST_SERIALNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("AAT_DESC") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accepted By">
                        <ItemTemplate>
                            <asp:Label ID="lblAckBy" runat="server" Text='<%#Eval("AAT_ACK_BY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accepted Date">
                        <ItemTemplate>
                            <asp:Label ID="lblAckDt" runat="server" Text='<%#Eval("AAT_ACK_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Acknowledgement">
                        <ItemTemplate>
                            <asp:Label ID="ddlAck" runat="server" Text='<%#Eval("ACK_STATUS")%>'></asp:Label>
                           <%-- <asp:Label ID="ddlTempAck" runat="server" Text='<%#Eval("TEMP_ACK_STATUS")%>' Visible="false"></asp:Label>--%>
                            <%-- <asp:DropDownList ID="ddlAck" runat="server" CssClass="selectpicker" data-live-search="true"
                                AutoPostBack="false" SelectedValue='<%# Eval("AAT_ACK_STATUS")%>'>
                                <asp:ListItem Value="1">YES</asp:ListItem>
                                <asp:ListItem Value="0">NO</asp:ListItem>
                            </asp:DropDownList>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrendered Date">
                        <ItemStyle Width="14%" />
                        <ItemTemplate>
                            <div class='input-group date' id='fromdate'>
                                <asp:TextBox ID="txtSurrendered_TAG" runat="server" CssClass="form-control" ></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Surrender">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnksurrender" runat="server" CommandName="Surrender">Surrender</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>

    </div>
    <br />
    <div class="row" id="txtremarks" runat="server">
        <div class="col-md-6 col-sm-12 col-xs-12" id="remk" runat="server">
            <div class="form-group">

                <label class="col-md-12 control-label">Requestor Remarks <span style="color: red;">*</span></label>
                 <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="remarks"
                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val2"
                            Enabled="true"></asp:RequiredFieldValidator>
                <%--<span id="SpanApprRemarks" style="color: Red; display: none;"></span>--%>
                <div class="col-md-7">
                    <asp:TextBox ID="remarks" TextMode="multiline" Columns="20" Rows="3" runat="server" class="form-control" />
                </div>

            </div>
        </div>

        <%--  <div class="col-md-12 text-left">
            <label>Asset Category<span style="color: red;">*</span></label>
            <textarea name="remarks" rows="3" cols="50" id="remarks"></textarea>
        </div>--%>
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Surrender Asset" ValidationGroup="Val2"/>
            </div>
        </div>
    </div>
     <div class="modal fade" id="myModal" tabindex='-1'>
            <div class="modal-dialog modal-lg" style="height:200px;width:400px !important;">
                <div class="modal-content" style="margin-top:0px">
                    <div class="modal-body" id="modelcontainer4">
                        <div style="display:flex;align-items:center;justify-content:center;">
                        <img  src="../../../Bootstrapcss/images/loading_1.gif"  Style="width: 30px; height: 30px; margin:auto" />
                        </div>
                        <br />
                        <div style="display:flex;align-items:center;justify-content:center">Searching...</div>
                    </div>
                </div>
            </div>
        </div>
</div>
<script >
    function LoadingPopUp() {
        $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $("#myModal").modal('show');
        return true
    }
    function refreshSelectpicker() {
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%= ddlastCat.ClientID%>").selectpicker();
        $("#<%= ddlAstBrand.ClientID%>").selectpicker();
        $("#<%= ddlAstModel.ClientID%>").selectpicker();
        $("#<%= ddlastsubCat.ClientID%>").selectpicker();
        $("#<%=gvItems.ClientID %>").selectpicker();
        $('#<%=gvItems.ClientID %>').find('[id$="ddlAck"]').selectpicker();
        $('.modal-backdrop').removeClass('show').addClass('hide');
    };
    refreshSelectpicker();
</script>