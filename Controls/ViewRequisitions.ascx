<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewRequisitions.ascx.vb" Inherits="Controls_ViewRequisitions" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>

<asp:GridView ID="gvInterMVMT" runat="server" EmptyDataText="No Inter Movement Requisition Found." AllowPaging="true"
    PageSize="10" CssClass="table  GridStyle" GridLines="none" AutoGenerateColumns="false">
    <Columns>
        <asp:TemplateField HeaderText="Request Id" ItemStyle-HorizontalAlign="left">
            <ItemTemplate>
                <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("MMR_REQ_ID")%>'
                    CommandName="Edit"><%#Eval("MMR_REQ_ID") %></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="From Location" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
            <ItemTemplate>
                <asp:Label ID="lblfromloc" runat="server" Text='<%#Eval("MMR_FROMBDG_ID")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="To Location" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
            <ItemTemplate>
                <asp:Label ID="lbltoLoc" runat="server" Text='<%#Eval("MMR_TOBDG_ID")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Request Date" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
            <ItemTemplate>
                <asp:Label ID="lblDATE" runat="server" Text='<%#Eval("MMR_MVMT_DATE")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="View" ItemStyle-HorizontalAlign="left">
            <ItemTemplate>
                <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
    <PagerStyle CssClass="pagination-ys" />
</asp:GridView>
