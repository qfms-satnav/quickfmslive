<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewInterMovementRequisition.ascx.vb"
    Inherits="Controls_ViewInterMovementRequisition" %>

<%--<div class="row">
    <div class="col-md-12">
        <div class="form-group">--%>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <%--            <div class="row pull-right">--%>
            <a href="InterMovementRequisition.aspx">Raise Inter Movement Requisition </a>
        </div>
    </div>
    <%--   </div>--%>
</div>
<div class="row" style="margin-top: 10px">
  <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvgriditems" runat="server" AllowPaging="true" AllowSorting="true" PageSize="10"
                AutoGenerateColumns="false" EmptyDataText="No Movement Requisition Found" CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>                              
                            <asp:Label ID="lblreq" runat="server" Text='<%#Eval("MMR_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requisition">
                        <ItemTemplate>
                             <asp:LinkButton ID="lnkViewDetails" Text='<%#Eval("MMR_REQ_ID") %>' CommandArgument='<%#Eval("MMR_REQ_ID") %>'
                                CommandName="ViewDetails" runat="server"></asp:LinkButton>
                            <%--<asp:Label ID="lblasset" runat="server" Text='<%#Eval("MMR_REQ_ID") %>'></asp:Label>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Raised by">
                        <ItemTemplate>
                            <asp:Label ID="lblreqraised" runat="server" Text='<%#Eval("MMR_RAISEDBY") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Date">
                        <ItemTemplate>
                            <asp:Label ID="lbldate" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
