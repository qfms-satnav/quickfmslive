Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGGroupGetDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvDetails_AAG.PageIndex = 0
            fillgrid()
            lbtn1.Visible = False
        End If
      
    End Sub
    Public Sub fillgrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMG_GROUP")
        sp.Command.AddParameter("@AAG_CODE", txtfindcode.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvDetails_AAG.DataSource = ds
        gvDetails_AAG.DataBind()
        For i As Integer = 0 To gvDetails_AAG.Rows.Count - 1
            Dim lblStaID_AAG As Label = CType(gvDetails_AAG.Rows(i).FindControl("lblStaID_AAG"), Label)
            If lblStaID_AAG.Text = "0" Then
                lblStaID_AAG.Text = "Inactive"
            Else
                lblStaID_AAG.Text = "Active"
            End If
        Next


    End Sub

    

    

    Protected Sub gvDetails_AAG_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails_AAG.PageIndexChanging
        gvDetails_AAG.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvDetails_AAG_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails_AAG.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblCode_AAG As Label = DirectCast(gvDetails_AAG.Rows(rowIndex).FindControl("lblCode_AAG"), Label)
            Dim id As String = lblCode_AAG.Text
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMG_AAG_DEL")
            SP1.Command.AddParameter("@AAG_CODE", id, DbType.String)
            SP1.ExecuteScalar()
        End If
        fillgrid()
    End Sub

    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
       
        fillgrid()
        lbtn1.Visible = True

    End Sub

    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
        txtfindcode.Text = ""
        fillgrid()
        lbtn1.Visible = False
    End Sub

    Protected Sub gvDetails_AAG_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDetails_AAG.RowDeleting

    End Sub
End Class
