Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ConfRequisition
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtFrmDate.Text <> "" Then
            BindGrid()
            BindGrid1()
            BindOutGrid()
            gvIn.Visible = True
            gvItems.Visible = True
            gvOut.Visible = True
        Else
            gvIn.Visible = False
            gvItems.Visible = False
            gvOut.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvItems.Visible = False
            gvIn.Visible = True
            gvOut.Visible = True

            txtFrmDate.Text = getoffsetdate(Date.Today)
          
        End If
        txtFrmDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_HISTORY_IN_OUT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@FROMDATE", txtFrmDate.Text, DbType.DateTime)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Private Sub BindGrid1()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_HISTORY_IN_OUT_TIMINGS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtFrmDate.Text, DbType.DateTime)
        gvIn.DataSource = sp.GetDataSet()
        gvIn.DataBind()
    End Sub
    Private Sub BindOutGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_HISTORY_OUT_TIMINGS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtFrmDate.Text, DbType.DateTime)
        gvOut.DataSource = sp.GetDataSet()
        gvOut.DataBind()
    End Sub

   
    Protected Sub gvIn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvIn.PageIndexChanging
        gvIn.PageIndex = e.NewPageIndex()
        BindGrid1()
    End Sub

    Protected Sub gvOut_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOut.PageIndexChanging
        gvOut.PageIndex = e.NewPageIndex()
        BindOutGrid()
    End Sub
End Class
