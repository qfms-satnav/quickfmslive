Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_UpdateRentDetails
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try
            Dim ValidateCode As Integer
            ValidateCode = ValidateRentReceipt()
            If ValidateCode = 0 Then
                lblMsg.Text = "Rent Receipt Number already exist please enter another code"
            ElseIf ValidateCode = 1 Then
                UpdateRent()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        
    End Sub
    Public Function ValidateRentReceipt()

        Dim ValidateCode As Integer
        Dim Rent_Receipt As String = txtReceipt.Text
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_RENT_RECEIPT")
        sp3.Command.AddParameter("@ReceiptNumber", Rent_Receipt, DbType.String)
        ValidateCode = sp3.ExecuteScalar()
        Return ValidateCode

    End Function
    Public Sub UpdateRent()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Update_RentDetails")
        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_NPDTENANT")
        sp4.Command.AddParameter("@TenantID", ddlemp.SelectedItem.Value, DbType.String)
        sp4.Command.AddParameter("@NEXTDATE", txtFrmDate.Text, DbType.String)

        sp3.Command.AddParameter("@TenantID", ddlemp.SelectedItem.Value, DbType.String)
        sp3.Command.AddParameter("@ReceiptNumber", txtReceipt.Text, DbType.String)
        sp3.Command.AddParameter("@RentPaid", txtamount.Text, DbType.Decimal)
        sp3.Command.AddParameter("@OutStanding", txtOamount.Text, DbType.Decimal)
        sp3.Command.AddParameter("@PaidDate", txtPaiddate.Text, DbType.Date)
        sp3.Command.AddParameter("@FromDate", txtFrmDate.Text, DbType.Date)
        sp3.Command.AddParameter("@ToDate", txtToDate.Text, DbType.String)
        sp3.Command.AddParameter("@TDS", txtTDS.Text, DbType.Decimal)
        sp3.Command.AddParameter("@Payment_Mode", ddlpaymentmode.SelectedItem.Text, DbType.String)

        If ddlpaymentmode.SelectedItem.Value = "1" Then
            sp3.Command.AddParameter("@ChequeNo", txtCheque.Text, DbType.String)
            sp3.Command.AddParameter("@IssuingBank", txtBankName.Text, DbType.String)
            sp3.Command.AddParameter("@DepositedBank", "", DbType.String)
            sp3.Command.AddParameter("@AccountNumber", txtAccNo.Text, DbType.String)
            sp3.Command.AddParameter("@IFCB", "", DbType.String)

        ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
            sp3.Command.AddParameter("@ChequeNo", "", DbType.String)
            sp3.Command.AddParameter("@IssuingBank", txtIBankName.Text, DbType.String)
            sp3.Command.AddParameter("@DepositedBank", txtDeposited.Text, DbType.String)
            sp3.Command.AddParameter("@AccountNumber", "", DbType.String)
            sp3.Command.AddParameter("@IFCB", txtIFCB.Text, DbType.String)
        Else
            sp3.Command.AddParameter("@ChequeNo", "", DbType.String)
            sp3.Command.AddParameter("@IssuingBank", "", DbType.String)
            sp3.Command.AddParameter("@DepositedBank", "", DbType.String)
            sp3.Command.AddParameter("@AccountNumber", "", DbType.String)
            sp3.Command.AddParameter("@IFCB", "", DbType.String)
        End If

        sp3.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp4.ExecuteScalar()
        sp3.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=9")
        Cleardata()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            BindPropType()
            BindCity()

            ' Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_DETAILS_FOR_PI")
            'sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            'Dim ds As New DataSet()
            'ds = sp.GetDataSet()
            'ddlCity.SelectedValue = ds.Tables(0).Rows(0).Item("CITY_ID")
            'ddlCity.Enabled = False

            txtPaiddate.Text = getoffsetdate(Date.Today)
            'txtPaiddate.Attributes.Add("onClick", "displayDatePicker('" + txtPaiddate.ClientID + "')")
            'txtPaiddate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtFrmDate.Text = getoffsetdate(Date.Today)
            'txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
            'txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtToDate.Text = getoffsetdate(Date.Today)
            'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
            'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtFrmDate.Attributes.Add("readonly", "readonly")
            txtToDate.Attributes.Add("readonly", "readonly")
            txtPaiddate.Attributes.Add("readonly", "readonly")
          
           
            txtTDS.Text = 0
            panel1.Visible = False
            panel2.Visible = False

        End If
        txtPaiddate.Attributes.Add("readonly", "readonly")
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")

    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "1" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "2" Then
                panel1.Visible = False
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            End If
            'Else
            '   Response.Write("<script>alert('Please select payment mode!');location.href=frmUpdateRentDetails.aspx;</script>")
        End If
    End Sub
    Public Sub Cleardata()

        ddlTenant.SelectedValue = 0      
        txtReceipt.Text = ""
        txtamount.Text = ""
        txtOamount.Text = ""
        txtPaiddate.Text = ""
        txtFrmDate.Text = getoffsetdate(Date.Today)
        txtToDate.Text = getoffsetdatetime(DateTime.Now).AddYears(1).Date()
        txtTDS.Text = ""
        ddlpaymentmode.SelectedIndex = -1
        txtRemarks.Text = ""
        panel1.Visible = False
        panel2.Visible = False

    End Sub

    Protected Sub ddlTenant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTenant.SelectedIndexChanged   
        If ddlTenant.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETRENTAMT")
            sp.Command.AddParameter("@PROPCODE", ddlTenant.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtamount.Text = ds.Tables(0).Rows(0).Item("TEN_RENT_PER_SFT")
                Binduser()
                Dim li As ListItem = Nothing
                li = ddlemp.Items.FindByValue(ds.Tables(0).Rows(0).Item("TEN_NAME"))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

            End If

            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_FROMDATE")
            sp2.Command.AddParameter("@Tenant", ddlTenant.SelectedItem.Value, DbType.String)
            Dim ds2 As New DataSet()
            ds2 = sp2.GetDataSet()

            If ds2.Tables(0).Rows.Count > 0 Then
                txtFrmDate.Text = ds2.Tables(0).Rows(0).Item("TODATE")
            End If

            txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
            txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TODATE")
            sp1.Command.AddParameter("@Tenant", ddlTenant.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@date", txtFrmDate.Text, DbType.Date)
            Dim ds1 As New DataSet()
            ds1 = sp1.GetDataSet()

            If ds1.Tables(0).Rows.Count > 0 Then
                txtToDate.Text = ds1.Tables(0).Rows(0).Item("FDATE")
            End If

            txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
            txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        Else
            txtFrmDate.Text = getoffsetdate(Date.Today)
            txtToDate.Text = getoffsetdate(Date.Today)
        End If
    End Sub
    Private Sub Binduser()
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"gettenants")
        SP.Command.AddParameter("@dummy", ddlTenant.SelectedItem.Value, DbType.String)
        ddlemp.DataSource = SP.GetDataSet()
        ddlemp.DataTextField = "TEN_NAME"
        ddlemp.DataValueField = "TEN_CODE"
        ddlemp.DataBind()
        'ddlemp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))

        Catch ex As Exception

        End Try

    End Sub
    
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            'BindProp()
            BindCityLoc()
        Else
            ddlLocation.Items.Clear()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlLocation.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindProp()
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROPT")
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@dummy", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@user", Session("uid"), DbType.String)
            ddlTenant.DataSource = sp.GetDataSet()
            ddlTenant.DataTextField = "PN_NAME"
            ddlTenant.DataValueField = "BDG_ID"
            ddlTenant.DataBind()

            ddlTenant.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindProp()
        txtPaiddate.Text = String.Empty
        txtFrmDate.Text = String.Empty
        txtToDate.Text = String.Empty
        txtamount.Text = String.Empty
    End Sub
End Class
