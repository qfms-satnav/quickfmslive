Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_ITApprovalDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim FBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor, SBDG_ID As String
    Dim FromLoc, ToLoc, FromLocCode, ToLocCode As String
    Dim receiveAst, strremarks As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            getDetailsbyReqId(strReqId)
            BindLocations()
            BindRequestAssets(strReqId)
            TOFROMPERSONS(strReqId)
        End If
    End Sub

    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)

        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AM_GET_INTER_MVMT_DETLS_BY_REQID", param)
        If dr.Read Then
            'FBDG_ID = dr.Item("SLOC_CODE")
            'SBDG_ID = dr.Item("FLOC_CODE")
            'FTower = dr.Item("MMR_FROMBDG_ID")
            'FFloor = dr.Item("MMR_FROMFLR_ID")
            'TBDG_ID = dr.Item("FLOC_NAME")
            FromLoc = dr.Item("FromLoc")
            ToLoc = dr.Item("ToLoc")
            FromLocCode = dr.Item("FromLocCode")
            ToLocCode = dr.Item("ToLocCode")
            '  receiveAst = dr.Item("MMR_RECVD_BY_name")
            strremarks = dr.Item("MMR_COMMENTS")
            txtSendDate.Text = dr.Item("senddate")

        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If

        'txtPersonName.Text = receiveAst

    End Sub

    Private Sub TOFROMPERSONS(ByVal strREQ_id As String)
        'ddlModel.Items.Clear()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BRANCH_MANAGER_BYLOCATION")
        'sp.Command.AddParameter("@AUR_LOCATION", ddlDestLoc.SelectedItem.Value, DbType.String)
        ddlFromPerson.Text = ""
        ddlToPerson.Text = ""
        Dim raisedby As String
        Dim receivedby As String
        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.VarChar, 50)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AST_MMT_MVMT_PERSONS_TO_FROM", param)
        If dr.Read Then
            ddlFromPerson.Text = dr.Item("raisedby")
            ddlToPerson.Text = dr.Item("receivedby")
        End If
        If dr.IsClosed = False Then
            dr.Close()


        End If



    End Sub

    Private Sub BindLocations()
        '----------- Binding Location-----------------------
        '  ObjSubsonic.Binddropdown(ddlSLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")
        ' ObjSubsonic.Binddropdown(ddlDLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")
        BindLocation()
        BindDestLoactions()
        ddlSLoc.Items.FindByValue(FromLocCode).Selected = True
        ddlDLoc.Items.FindByValue(ToLocCode).Selected = True
        ''------------ From Building/Tower/Floor -----------------------------
        'If FBDG_ID = "" Or FBDG_ID Is Nothing Then

        'Else
        '    ddlSLoc.Items.FindByValue(FBDG_ID).Selected = True

        '    '----------- Binding Tower -----------------------
        '    If ddlSLoc.SelectedIndex > 0 Then
        '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
        '        BindTowersByLocation(LocCode, ddlSTower)
        '    End If
        '    ddlSTower.Items.FindByValue(FTower).Selected = True
        '    '-------------------------------------------------

        '    If ddlSTower.SelectedIndex > 0 Then
        '        Dim TwrCode As String = ddlSTower.SelectedItem.Value
        '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
        '        BindFloorsByTower(TwrCode, LocCode, ddlSFloor)
        '    End If
        '    ddlSFloor.Items.FindByValue(FFloor).Selected = True

        'End If
        ''------------ From Building/Tower/Floor -------------------------
        'If TBDG_ID = "" Or TBDG_ID Is Nothing Then

        'Else
        '    ddlDLoc.Items.FindByValue(TBDG_ID).Selected = True

        '    '----------- Binding Tower -----------------------
        '    If ddlDLoc.SelectedIndex > 0 Then
        '        Dim LocCode As String = ddlDLoc.SelectedItem.Value
        '        BindTowersByLocation(LocCode, ddlDTower)
        '    End If
        '    ddlDTower.Items.FindByValue(TTower).Selected = True
        '    '-------------------------------------------------

        '    If ddlDTower.SelectedIndex > 0 Then
        '        Dim TwrCode As String = ddlDTower.SelectedItem.Value
        '        Dim LocCode As String = ddlDLoc.SelectedItem.Value
        '        BindFloorsByTower(TwrCode, LocCode, ddlDFloor)
        '    End If
        '    ddlDFloor.Items.FindByValue(TFloor).Selected = True

        'End If
    End Sub
    Private Sub BindDestLoactions()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlDLoc.DataSource = sp.GetDataSet()
            ddlDLoc.DataTextField = "LCM_NAME"
            ddlDLoc.DataValueField = "LCM_CODE"
            ddlDLoc.DataBind()
            ddlDLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlSLoc.DataSource = sp.GetDataSet()
            ddlSLoc.DataTextField = "LCM_NAME"
            ddlSLoc.DataValueField = "LCM_CODE"
            ddlSLoc.DataBind()
            ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "AM_GET_MVMT_ASTS", param)
    End Sub


    Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            '   BindTowersByLocation(LocCode, ddlSTower)
            getDetailsbyReqId(Request.QueryString("Req_id"))
        End If
    End Sub

    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = LocCode
        ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    End Sub

    Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
        If ddlDLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlDLoc.SelectedItem.Value
            '  BindTowersByLocation(LocCode, ddlDTower)
        End If
    End Sub

    'Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
    '    If ddlSTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlSTower.SelectedItem.Value
    '        Dim Loccode As String = ddlSLoc.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, Loccode, ddlSFloor)
    '    End If
    'End Sub
    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByVal Loccode As String, ByRef ddl As DropDownList)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@FLR_LOC_ID", DbType.String)
        param(0).Value = Loccode
        param(1) = New SqlParameter("@FLR_TWR_ID", DbType.String)
        param(1).Value = TwrCode
        ObjSubsonic.Binddropdown(ddl, "GET_FLOOR_BYLOCTWR", "FLR_NAME", "FLR_CODE", param)
    End Sub

    'Protected Sub ddlDTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTower.SelectedIndexChanged
    '    If ddlDTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlDTower.SelectedItem.Value
    '        Dim Loccode As String = ddlDLoc.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, Loccode, ddlDFloor)
    '    End If
    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strASSET_LIST As New ArrayList
        For i As Integer = 0 To gvItems.Rows.Count - 1

            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)

            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1017
            param(4) = New SqlParameter("@ASSET_CODE", SqlDbType.Int)
            param(4).Value = lblAAS_AAT_CODE.Text
            param(5) = New SqlParameter("@DEST_LOC", SqlDbType.Int)
            param(5).Value = ddlDLoc.SelectedItem.Value

            ObjSubsonic.GetSubSonicExecute("UPDATE_MMT_MVMT_REQ_APPROVE_reqId", param)

            strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & txtRemarks.Text)
        Next
            SendMail(Request.QueryString("Req_id"), "Approve")
            Response.Redirect("frmAssetThanks.aspx?RID=InterMVMTITDetails")

    End Sub


    Private Sub SendMail(ByVal ReqId As String, ByVal Mode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_INTER_MVMT_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", Mode, DbType.String)
        sp.Execute()
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmITApproval.aspx")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim strASSET_LIST As New ArrayList
        For i As Integer = 0 To gvItems.Rows.Count - 1
            '    Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            '    Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            '    'strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text)
            '    strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & txtRemarks.Text)
            'Next

            'Dim param(3) As SqlParameter
            'param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            'param(0).Value = Request.QueryString("Req_id")
            'param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            'param(1).Value = Session("uid")
            'param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
            'param(2).Value = txtRemarks.Text
            'param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            'param(3).Value = 1018


            'ObjSubsonic.GetSubSonicExecute("UPDATE_MMT_MVMT_REQ_APPROVE_reqId", param)

            ''Dim MailTemplateId As Integer
            ''MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_it_reject"))
            ''    getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)

            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)

            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1018
            param(4) = New SqlParameter("@ASSET_CODE", SqlDbType.Int)
            param(4).Value = lblAAS_AAT_CODE.Text
            param(5) = New SqlParameter("@DEST_LOC", SqlDbType.Int)
            param(5).Value = ddlDLoc.SelectedItem.Value

            ObjSubsonic.GetSubSonicExecute("UPDATE_MMT_MVMT_REQ_APPROVE_reqId", param)

            strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & txtRemarks.Text)

        Next
        SendMail(Request.QueryString("Req_id"), "Reject")
        Response.Redirect("frmAssetThanks.aspx?RID=intermvmtitrej")

    End Sub
End Class
