<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ModifyStockItems.ascx.vb"
    Inherits="Controls_ModifyStockItems" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js">function TABLE1_onclick() {

}
</script>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Modify Item
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="TABLE1"
        onclick="return TABLE1_onclick()">
        <tr>
            <td align="left" colspan="3">
                <asp:Label align="left" ID="Label9" runat="server" CssClass="note" Text="(*) Mandatory fields"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>Modify Item</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                &nbsp;<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <%--<asp:Label ID="lblMnd" runat="server" CssClass="clsMessage" Text="(*) Mandatory fields"></asp:Label>--%>
                <table id="table4" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label13" runat="server" CssClass="clslabel" Text="Name"></asp:Label>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtName"
                                Display="None" ErrorMessage="Please enter Name!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtName" runat="server" CssClass="clsTextField" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label1" runat="server" CssClass="clslabel" Text="AIM Code"></asp:Label>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAIMC"
                                Display="None" ErrorMessage="Please enter AIM Code!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAIMC" runat="server" CssClass="clsTextField" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <asp:Label ID="lblTemp" runat="server" CssClass="clslabel" Visible="false"></asp:Label><tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label4" runat="server" CssClass="clslabel" Text="Unit Price"></asp:Label>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtUntPrc"
                                Display="None" ErrorMessage="Please Enter Unit Price!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtUntPrc"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Unit Price Value"
                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtUntPrc" runat="server" CssClass="clsTextField" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left;">
                                <asp:Label ID="lblsrsl1" runat="server" CssClass="clslabel" Text="ESC 1"></asp:Label>
                                <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvl1122" runat="server" ControlToValidate="txtESC1"
                                    Display="None" ErrorMessage="Please enter ESC 1" ValidationGroup="Val1">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="REV1" runat="server" ControlToValidate="txtESC1"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Esc1 Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            </div>
                            <div style="clear: both; float: left; width: 100%">
                                <asp:TextBox ID="txtESC1" runat="server"  Width="75%" MaxLength="3" CssClass="clsTextField">
                                </asp:TextBox>
                            </div>
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:Label ID="Label3" runat="server" CssClass="clslabel" Text="Email 1"></asp:Label>
                                <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvem1" runat="server" ControlToValidate="txtEmail1"
                                    Display="None" ErrorMessage="Please enter email 1" ValidationGroup="Val1" Width="51px"></asp:RequiredFieldValidator>&nbsp;
                            </div>
                            <div style="clear: both; float: left; width: 100%">
                                <asp:TextBox ID="txtEmail1" runat="server" Width="75%" CssClass="clsTextField">
                                </asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left;">
                                <asp:Label ID="Label5" runat="server" CssClass="clslabel" Text="ESC 2"></asp:Label>
                                <font class="clsNote">*</font>&nbsp;
                            <asp:RegularExpressionValidator ID="rfvv2" runat="server" ControlToValidate="txtESC2"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Esc2 Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>&nbsp;
                            </div>
                            <div style="clear: both; float: left; width: 100%">
                                <asp:TextBox ID="txtESC2" runat="server"  Width="75%" MaxLength="3" CssClass="clsTextField">
                                </asp:TextBox>
                            </div>
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:Label ID="Label6" runat="server" CssClass="clslabel" Text="Email 2"></asp:Label>
                                <font class="clsNote">*</font>
                            </div>
                            <div style="clear: both; float: left; width: 100%">
                                <asp:TextBox ID="txtEmail2" runat="server" Width="75%" CssClass="clsTextField">
                                </asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="float: left;">
                                <asp:Label ID="Label7" runat="server" CssClass="clslabel" Text="ESC 3"></asp:Label>
                                <font class="clsNote">*</font>&nbsp;
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtESC3"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Esc3 Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                            </div>
                            <div style="clear: both; float: left; width: 100%">
                                <asp:TextBox ID="txtESC3" runat="server"  Width="75%" MaxLength="3" CssClass="clsTextField">
                                </asp:TextBox>
                            </div>
                        </td>
                        <td>
                            <div style="float: left;">
                                <asp:Label ID="Label8" runat="server" CssClass="clslabel" Text="Email 3"></asp:Label>
                                <font class="clsNote">*</font>
                            </div>
                            <div style="clear: both; float: left; width: 100%">
                                <asp:TextBox ID="txtEmail3" runat="server"  Width="75%" CssClass="clsTextField">
                                </asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblRem" runat="server" CssClass="bodytext" Text="Remarks"></asp:Label>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRem"
                                Display="None" ErrorMessage="Please Enter the Remarks!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRem" runat="server" CssClass="textBox" Width="97%" MaxLength="100"
                                Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%" style="height: 26px">
                            <asp:Label ID="lblSatus" runat="server" CssClass="bodytext" Text="Status"></asp:Label>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                Display="None" ErrorMessage="Select Status" ValidationGroup="Val1" InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlStatus" runat="server" Width="97%" CssClass="clsComboBox">
                                <asp:ListItem>--Select Status--</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">InActive</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label12" runat="server" CssClass="clslabel" Text="Min Order Qty"></asp:Label>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtMinOrdr"
                                Display="None" ErrorMessage="Please enter Min Order Qty!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtMinOrdr"
                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Min Order Qty Value"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMinOrdr" runat="server" CssClass="clsTextField" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">&nbsp;
                            <asp:Button ID="btnModify" runat="server" Text="Modify" CssClass="button" ValidationGroup="Val1" />
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" />&nbsp;
                        </td>
                    </tr>
                </table>
                <br />
                &nbsp;<br />
                <br />
                <br />
            </td>
            <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
