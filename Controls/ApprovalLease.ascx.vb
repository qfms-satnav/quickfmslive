Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ApprovalLease
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            panel1.Visible = False
            txtStartDate.Attributes.Add("onClick", "displayDatePicker('" + txtStartDate.ClientID + "')")
            txtStartDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtExpiryDate.Attributes.Add("onClick", "displayDatePicker('" + txtExpiryDate.ClientID + "')")
            txtExpiryDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtEscalationDate.Attributes.Add("onClick", "displayDatePicker('" + txtEscalationDate.ClientID + "')")
            txtEscalationDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtDate2.Attributes.Add("onClick", "displayDatePicker('" + txtDate2.ClientID + "')")
            txtDate2.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtDate3.Attributes.Add("onClick", "displayDatePicker('" + txtDate3.ClientID + "')")
            txtDate3.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_LEASES_ALL_APPROVAL")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        BindPropType()
        BindCity()
        BindLeaseType()

        Dim lnkApprove As LinkButton = DirectCast(e.CommandSource, LinkButton)
        Dim gvRow As GridViewRow = DirectCast(lnkApprove.NamingContainer, GridViewRow)
        Dim lblLeasecode As Label = DirectCast(gvRow.FindControl("lblLeasecode"), Label)
        Dim lblLesseNum As Label = DirectCast(gvRow.FindControl("lblLesseNum"), Label)
        Dim code As String = lblLeasecode.Text
        Dim lesse As String = lblLesseNum.Text
        If e.CommandName = "Approve" Then
            panel1.Visible = True
            BindLesse(lesse)
            Binddetails(code)

        Else
            panel1.Visible = False
            RejectLease(code)
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=42")
        End If
    End Sub
    Private Sub Binddetails(ByVal code As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_LEASES_BINDAPPROVAL")
            sp.Command.AddParameter("@LEASE_CODE", code, DbType.String)

            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlproptype.Items.FindByValue(ds.Tables(0).Rows(0).Item("PN_PROP_TYPE")).Selected = True
                ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("LEASE_BDG_NAME")).Selected = True

                txtproperty.text = ds.Tables(0).Rows(0).Item("PROPERTY_NAME")
                ddlLeaseType.Items.FindByValue(ds.Tables(0).Rows(0).Item("LEASE_TYPE_CODE")).Selected = True
                ddlLesse.Items.FindByValue(ds.Tables(0).Rows(0).Item("LESSE_NAME")).Selected = True
                Dim STATU As Integer = ds.Tables(0).Rows(0).Item("LEASE_STATUS")
                If STATU = 0 Then
                    ddlStatus.SelectedValue = 0
                Else
                    ddlStatus.SelectedValue = 1
                End If

                Dim mode As Integer = ds.Tables(0).Rows(0).Item("LEASE_MODE")
                If mode = 1 Then
                    ddlMode.SelectedValue = 1
                ElseIf mode = 2 Then
                    ddlMode.SelectedValue = 2
                ElseIf mode = 3 Then
                    ddlMode.SelectedValue = 3
                Else
                    ddlMode.SelectedValue = 4
                End If
                txtLnumber.Text = ds.Tables(0).Rows(0).Item("LEASE_NUM")
                txtLname.Text = ds.Tables(0).Rows(0).Item("LEASE_NAME")
                txtpay.Text = ds.Tables(0).Rows(0).Item("Advance")
                txtLessorName.Text = ds.Tables(0).Rows(0).Item("LESSOR_NAME")
                txtStartDate.Text = ds.Tables(0).Rows(0).Item("LEASE_START_DATE")
                txtExpiryDate.Text = ds.Tables(0).Rows(0).Item("LEASE_EXPIRY_DATE")
                txtInvestedArea.Text = ds.Tables(0).Rows(0).Item("LEASE_INVESTED_AREA")
                txtOccupiedArea.Text = ds.Tables(0).Rows(0).Item("LEASE_OCCUPIED_AREA")
                txtComments.Text = ds.Tables(0).Rows(0).Item("COMMENTS")
                txtEmail1.Text = ds.Tables(0).Rows(0).Item("LEASE_ESC_EMAIL")
                txtEscalationDate.Text = ds.Tables(0).Rows(0).Item("LEASE_ESC_PERIOD")
                txtEmail2.Text = ds.Tables(0).Rows(0).Item("LEASE_ESC_EMAIL1")
                txtDate2.Text = ds.Tables(0).Rows(0).Item("LEASE_ESC_PERIOD1")
                txtEmail3.Text = ds.Tables(0).Rows(0).Item("LEASE_ESC_EMAIL2")
                txtDate3.Text = ds.Tables(0).Rows(0).Item("LEASE_ESC_PERIOD2")
                txtEscPer.Text = ds.Tables(0).Rows(0).Item("ESC_PER")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("UID"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            'ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    'Private Sub BindProp()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROP")
    '    sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@dummy", ddlCity.SelectedItem.Value, DbType.String)
    '    ddlBuilding.DataSource = sp.GetDataSet()
    '    ddlBuilding.DataTextField = "PN_NAME"
    '    ddlBuilding.DataValueField = "BDG_ID"
    '    ddlBuilding.DataBind()

    'End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ' ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindLeaseType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlLeaseType.DataSource = sp3.GetDataSet()
        ddlLeaseType.DataTextField = "PN_LEASE_TYPE"
        ddlLeaseType.DataValueField = "PN_LEASE_ID"
        ddlLeaseType.DataBind()

        ' ddlLeaseType.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindLesse(ByVal LESSE As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_LESSE2")
        sp.Command.AddParameter("@AUR_ID", LESSE, DbType.String)
        ddlLesse.DataSource = sp.GetDataSet()
        ddlLesse.DataTextField = "AUR_FIRST_NAME"
        ddlLesse.DataValueField = "AUR_ID"
        ddlLesse.DataBind()
        'ddlLesse.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_LEASES_aLL_APPROVE")
        sp.Command.AddParameter("@lease_code", txtLnumber.Text, DbType.String)
        sp.Command.AddParameter("@lesse_name", ddlLesse.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@lease_strt_date", txtStartDate.Text, DbType.Date)
        sp.Command.AddParameter("@lease_Expiry_date", txtExpiryDate.Text, DbType.Date)
        sp.Command.AddParameter("@lease_esc_Date1", txtEscalationDate.Text, DbType.Date)
        If (txtDate2.Text = "") Then
            sp.Command.AddParameter("@LEASE_ESC_PERIOD1", getoffsetdate(Date.Today), DbType.Date)
        Else
            sp.Command.AddParameter("@LEASE_ESC_PERIOD1", txtDate2.Text, DbType.Date)
        End If

        If (txtDate3.Text = "") Then
            sp.Command.AddParameter("@LEASE_ESC_PERIOD2", getoffsetdate(Date.Today), DbType.Date)
        Else
            sp.Command.AddParameter("@LEASE_ESC_PERIOD2", txtDate3.Text, DbType.Date)
        End If

        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=41")
    End Sub
    Private Sub RejectLease(ByVal code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_PN_LEASES_ALL_REJECT")
        sp.Command.AddParameter("@lease_code", code, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        panel1.Visible = False
    End Sub
End Class

