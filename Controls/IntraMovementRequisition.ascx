<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IntraMovementRequisition.ascx.vb"
    Inherits="Controls_IntraMovementRequisition" %>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">Intra Movement Requisition
             <hr align="center" width="60%" /></asp:Label>
            &nbsp;
            <br />
        </td>
    </tr>
</table>
<asp:Panel ID="PANELMAIN" runat="Server" Width="85%">
    <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Intra Movement Requisition</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <table id="Table2" width="100%" cellpadding="2" runat="server">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            From Location</td>
                        <td width="50%">
                            <asp:DropDownList ID="ddlSLoc" runat="server" Width="275px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            From Tower</td>
                        <td width="50%">
                            <asp:DropDownList ID="ddlSTower" runat="server" Width="275px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            From Floor</td>
                        <td width="50%">
                            <asp:DropDownList ID="ddlSFloor" runat="server" Width="275px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnGetAssets" runat="server" Text="View Assets" CssClass="button"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" width="100%">
                            <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Assets Found." AllowPaging="true"
                                PageSize="10" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 26px" width="50%">
                            To Location</td>
                        <td style="height: 26px" width="50%">
                            <asp:DropDownList ID="ddlDLoc" runat="server" Width="275px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            To Tower</td>
                        <td width="50%">
                            <asp:DropDownList ID="ddlDTower" runat="server" Width="275px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            To Floor</td>
                        <td width="50%">
                            <asp:DropDownList ID="ddlDFloor" runat="server" Width="275px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            Person To Receive Assets
                            <asp:RequiredFieldValidator ID="rfvemp" runat="server" ControlToValidate="ddlEmp"
                                ErrorMessage="Please Select Employee" Display="Dynamic" InitialValue="--Select--"></asp:RequiredFieldValidator>
                        </td>
                        <td width="50%">
                            <asp:DropDownList ID="ddlEmp" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            Remarks</td>
                        <td width="50%">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" Width="275px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="button"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</asp:Panel>
