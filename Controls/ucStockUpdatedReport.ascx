<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucStockUpdatedReport.ascx.vb"
    Inherits="Controls_StockUpdatedReport" %>
   <link href="../../StyleSheet.css" rel="stylesheet" type="text/css" />
    <%--gird--%>
    <link rel="stylesheet" href="../../style/print.css" type="text/css" media="print" charset="utf-8" />
    <link rel="stylesheet" href="../../themes/redmond/jquery-ui-1.8.14.custom.css" type="text/css"
        media="screen" />
    <!--<link rel="stylesheet" href="../../themes/ui.jqgrid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../../style/facebox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../../style/colorbox/colorbox.css" media="screen" />
    <link rel="stylesheet" href="../../style/tooltip.css" type="text/css" media="all" />
    <link rel="stylesheet" href="../../style/roi.css" type="text/css" media="all" />-->
    <%--end--%>

    <script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../../lib/jquery/jquery-ui-1.8.2.custom.min.js"></script>

    <script type="text/javascript" src="../../lib/jquery/facebox/facebox.js"></script>

    <script type="text/javascript" src="../../lib/jquery/colorbox/jquery.colorbox.js"></script>

    <script type="text/javascript" src="../../lib/page.js"></script>

    <script type="text/javascript" src="../../lib/roi_uam.js"></script>

    <script type="text/javascript" src="../../lib/utilities.js"></script>

  
    <%--gird--%>
    <link rel="stylesheet" href="../../themes/ui.jqgrid.css" type="text/css" media="screen" />

    <script type="text/javascript" src="../../lib/UserManager.js"></script>

    <script type="text/javascript" src="../../js/i18n/grid.locale-en.js"></script>

    <script type="text/javascript" src="../../js/jquery.jqGrid.src.js"></script>
    

    <script type="text/javascript" src="../../js/jquery.jqGrid.min.js"></script>

 


  <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Consumable Stock Report
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>

<%--<table id="table1" cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="fornormaltext" Width="95%" Font-Underline="False"
                ForeColor="Black">Stock Received Report   
             <hr align="center" width="60%" /></asp:Label></td>
    </tr>
</table>--%>
<table cellspacing="0" cellpadding="0" width="100%">
   <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>Consumable Stock Report</strong>
                    </td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
      <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                    </td>
        <td align="center">
        
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 100%" border="1">
                <tr>
                    <td align="left" colspan="2" style="height: 24px">
                        &nbsp;Select Location <strong><span style="font-size: 8pt; color: #ff0000">* </span>
                        </strong>
                    </td>
                    <td align="left" colspan="2" style="height: 24px">
                        <select name='Location' id="Location" style="width: 300px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" style="height: 24px">
                        &nbsp;Select Tower <strong><span style="font-size: 8pt; color: #ff0000">* </span></strong>
                    </td>
                    <td align="left" colspan="2" style="height: 24px">
                        <select name='Tower' id="Tower" style="width: 300px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" style="height: 24px">
                        &nbsp;Select Floor <strong><span style="font-size: 8pt; color: #ff0000">* </span></strong>
                    </td>
                    <td align="left" colspan="2" style="height: 24px">
                        <select name='Floor' id="Floor" style="width: 300px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" style="height: 24px">
                        &nbsp;Select Asset <strong><span style="font-size: 8pt; color: #ff0000">* </span></strong>
                    </td>
                    <td align="left" colspan="2" style="height: 24px">
                        <select name='Asset' id="Asset" style="width: 300px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <table id="StockAvailableReport_list">
                                    </table>
                                    <div id="StockAvailableReport_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table id="StockReceivableReport_list">
                                    </table>
                                    <div id="StockReceivableReport_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table id="StockHistory_list">
                                    </table>
                                    <div id="StockHistory_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
