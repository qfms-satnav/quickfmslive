Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ConfReservation
    Inherits System.Web.UI.UserControl

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvItems.Visible = False
            
        End If
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If CDate(txtFrmDate.Text) > CDate(txtToDate.Text) Then
                gvItems.Visible = False
                lblMsg.Text = "To Date Should be Greater than From Date"
            ElseIf txtFrmDate.Text = "" Or txtToDate.Text = "" Then
                lblMsg.Text = "Please Fill Mandatory Fields"
            Else

                BindGrid()
                gvItems.Visible = True
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_ATTENDANCE_DISCREPANCY")
            sp.Command.AddParameter("@FROM_DATE", txtFrmDate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtToDate.Text, DbType.Date)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
