﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GenerateIssuePrint.ascx.vb" Inherits="Controls_GenerateIssuePrint" %>

<script type="text/javascript">
<!--
    function printPartOfPage(elementId) {
        var printContent = document.getElementById(elementId);
        var windowUrl = 'about:blank';
        var uniqueName = new Date();
        var windowName = 'Print' + uniqueName.getTime();
        var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
        printWindow.close();
    }
    // -->
</script>

<div id="Div1">
    <table id="table2" cellspacing="0" cellpadding="0" width="98%" align="center" border="0">
        <tr>
            <td width="100%" align="center">
                <%--                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Print Journey Plan<hr align="center" width="60%" /> </asp:Label>
--%>
            </td>
        </tr>
        <tr>
            <td width="100%" align="center">
            </td>
        </tr>
    </table>
    <table id="Table3" cellspacing="0" cellpadding="0"   align="center" border="0">
        <tr>
            <td style="width: 10px">
                <%--    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" />--%>
            </td>
            <%--       <td class="tableHEADER" align="left" style="width: 100%">&nbsp;<strong>Print Journey Plan</strong></td>--%>
            <td>
                <%--  <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" />--%>
            </td>
        </tr>
        <tr>
            <%--      <td background="../../Images/table_left_mid_bg.gif" style="width: 10px"></td>--%>
            <td align="left">
                <table id="table4" cellspacing="0" cellpadding="0" width="100%" border="1">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="100%" border="1">
                                <tr>
                                    <td align="right" width="100px">
                                        <asp:Image ID="Image1" ImageUrl="~/images/a-mantralogo.gif"  runat="server" />
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblAddress" Text="SatNav Technologies Pvt. Ltd." runat="server" Font-Bold="True" /><br>
                                        <asp:Label ID="lblAddress1" Text="#203 , Lake Shore Towers, Rajabhavan Road" runat="server"
                                            Font-Bold="True" /><br>
                                        <asp:Label ID="Label1" Text="Somajiguda, Hyderabad, Andhra Pradesh 500082. India "
                                            runat="server" Font-Bold="True" />
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblheadingt" runat="server" Text="INVOICE" CssClass="clsHead" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnprint" runat="server" Text="Print" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <br />
                                    <td width="50%" valign="top">
                                        <asp:Label ID="lblLocation" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label><br />
                                        <asp:Label ID="lblDate" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label><br />
                                        <asp:Label ID="lblraised" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td width="50%" valign="top">
                                        <asp:Label ID="lblReqid" runat="server" CssClass="clsHead" Font-Bold="True" Text="Req ID   : "></asp:Label>
                                        <%--<asp:Label ID="lblPONo" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label><br />
                                        <asp:Label ID="lblPODate1" runat="server" CssClass="clsHead" Font-Bold="True" Text="PO DATE   : "></asp:Label>
                                        <asp:Label ID="lblPODate" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label><br />
                                        <asp:Label ID="lblInvoiceNo" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label>
                                        <asp:Label ID="lblInvoiceDate" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label>
                                        <asp:Label ID="lblPaymentTerms" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label>--%>
                                    </td>
                                </tr>
                               <%-- <tr>
                                    <td>
                                        CUSTOMER NAME :
                                        <asp:Label ID="lblVendorCustName" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td>
                                        PHONE :
                                        <asp:Label ID="lblVendorPhone" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        SALES CONTACT :
                                        <asp:Label ID="lblSalesCont" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td>
                                        PHONE :
                                        <asp:Label ID="lblPhone" runat="server" CssClass="clsHead" Font-Bold="True"></asp:Label></td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Asset(s) Found." Width="200%" ShowFooter="true">
                                            <Columns>
                                                 <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left" />
                                        <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_QTY") %>'></asp:TextBox>
                                                <asp:Label ID="lblProductid" runat="server" Text='<%#Eval("AST_MD_id") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lblMinOrdQty" runat="server" Text='<%#Eval("AST_MD_MINQTY") %>' Visible="false"> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="TOTAL ITEMS">
                                                    
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblTotalPrice" runat="server" /><br />
                                                      
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                  <td >
                              
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                     
                                    </td>
                                <td align="right">
                              
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Aurhorized Person
                                    </td>
                                    
                               
                               
                              </tr>
                              <hr>
                                <tr>
                                <td>
                                <b>Terms & Conditions </b><br />
                              1) Goods should be accompained by challen ub triplicate, and MUST bear our 

purchase order no. & Sr. No.<br />
 2) The quality of the supplies is to be approved by us and our apporval or 

rejection will be final.<br />
3) Payments should be made without prejudice to our rights
against partly executive order.</td><td></td></tr>
                            </table>
                            <%--  <table id="table9" runat="server" width="85%" border="0" cellspacing="0" cellpadding="0" >
                  
                  
                    <tr align="left">
                                    <td>
                                    <br />
                                    <br />
                                    <br />
                                       Payment details: amount transfer to our account #  <asp:Label ID="lblVnedorAccnt" runat="server" /> of  
                                       <asp:Label ID="lblVendorBank"  runat="server" />,
                                         <asp:Label ID="lblVendorBranch" runat="server" />,
                                           IFSC Code    <asp:Label ID="lblVendorIFSC" runat="server" />,

Please email details of payment to <asp:Label ID="Label2" runat="server" CssClass="clsHead" Font-Bold="True"> ,
                                        </asp:Label>
                                    </td>
                                 
                                </tr></table>-->
            </td>
            <%--     <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">&nbsp;</td>--%>
                            <%--        <tr>
            <td style="width: 10px; height: 17px;"><%#formatnumber(Eval("AIPD_TOTALCOST"),2) %>
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px; width: 524px;" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>--%>
        
        
                    </tr>
                </table>
                </td>
            </tr>
        </table>
</div>