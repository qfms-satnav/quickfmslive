<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddCompensatory.ascx.vb"
    Inherits="Controls_AddCompensatory" %>





<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDep" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlRM" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contact Number<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvCNo" runat="server" Display="none" ErrorMessage="Please Enter Contact Number"
                    ControlToValidate="txtContactNo" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revCNo" runat="Server" Display="none" ErrorMessage="Please Enter Numerics for Contact Number"
                    ControlToValidate="txtContactNo" ValidationExpression="^[0-9]+$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvstartdate" runat="server" ControlToValidate="txtFromDate"
                    Display="None" ErrorMessage="Please Pick From Date " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox of FromDate to Pick the date')" onmouseout="UnTip()">
                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtToDate"
                    Display="None" ErrorMessage="Please Pick To Date " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox of ToDate to Pick the date')" onmouseout="UnTip()">
                        <div class='input-group date' id='todate'>
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('todate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">In Time (hh:mm)<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cfvHr" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select No of Hours!"
                    Display="None" ControlToValidate="cboHr" InitialValue="-HH-"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="cfvMin" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select No of Minutes!"
                    Display="None" ControlToValidate="cboMin" InitialValue="-MM-"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="cboHr" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        <asp:ListItem Value="0">-HH--</asp:ListItem>
                        <asp:ListItem Value="1">0</asp:ListItem>
                        <asp:ListItem Value="2">1</asp:ListItem>
                        <asp:ListItem Value="3">2</asp:ListItem>
                        <asp:ListItem Value="4">3</asp:ListItem>
                        <asp:ListItem Value="5">4</asp:ListItem>
                        <asp:ListItem Value="6">5</asp:ListItem>
                        <asp:ListItem Value="7">6</asp:ListItem>
                        <asp:ListItem Value="8">7</asp:ListItem>
                        <asp:ListItem Value="9">8</asp:ListItem>
                        <asp:ListItem Value="10">9</asp:ListItem>
                        <asp:ListItem Value="11">10</asp:ListItem>
                        <asp:ListItem Value="12">11</asp:ListItem>
                        <asp:ListItem Value="13">12</asp:ListItem>
                        <asp:ListItem Value="14">13</asp:ListItem>
                        <asp:ListItem Value="15">14</asp:ListItem>
                        <asp:ListItem Value="16">15</asp:ListItem>
                        <asp:ListItem Value="17">16</asp:ListItem>
                        <asp:ListItem Value="18">17</asp:ListItem>
                        <asp:ListItem Value="19">18</asp:ListItem>
                        <asp:ListItem Value="20">19</asp:ListItem>
                        <asp:ListItem Value="21">20</asp:ListItem>
                        <asp:ListItem Value="22">21</asp:ListItem>
                        <asp:ListItem Value="23">22</asp:ListItem>
                        <asp:ListItem Value="24">23</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="cboMin" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        <asp:ListItem Value="0">-MM-</asp:ListItem>
                        <asp:ListItem Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Out Time (hh:mm)<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvTohh" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select No of Hours!"
                    Display="None" ControlToValidate="ddlHH" InitialValue="-HH-"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="rfvTomm" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select No of Minutes!"
                    Display="None" ControlToValidate="ddlMM" InitialValue="-MM-"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlHH" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        <asp:ListItem Value="-HH-">-HH--</asp:ListItem>
                        <asp:ListItem Value="1">0</asp:ListItem>
                        <asp:ListItem Value="2">1</asp:ListItem>
                        <asp:ListItem Value="3">2</asp:ListItem>
                        <asp:ListItem Value="4">3</asp:ListItem>
                        <asp:ListItem Value="5">4</asp:ListItem>
                        <asp:ListItem Value="6">5</asp:ListItem>
                        <asp:ListItem Value="7">6</asp:ListItem>
                        <asp:ListItem Value="8">7</asp:ListItem>
                        <asp:ListItem Value="9">8</asp:ListItem>
                        <asp:ListItem Value="10">9</asp:ListItem>
                        <asp:ListItem Value="11">10</asp:ListItem>
                        <asp:ListItem Value="12">11</asp:ListItem>
                        <asp:ListItem Value="13">12</asp:ListItem>
                        <asp:ListItem Value="14">13</asp:ListItem>
                        <asp:ListItem Value="15">14</asp:ListItem>
                        <asp:ListItem Value="16">15</asp:ListItem>
                        <asp:ListItem Value="17">16</asp:ListItem>
                        <asp:ListItem Value="18">17</asp:ListItem>
                        <asp:ListItem Value="19">18</asp:ListItem>
                        <asp:ListItem Value="20">19</asp:ListItem>
                        <asp:ListItem Value="21">20</asp:ListItem>
                        <asp:ListItem Value="22">21</asp:ListItem>
                        <asp:ListItem Value="23">22</asp:ListItem>
                        <asp:ListItem Value="24">23</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlMM" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        <asp:ListItem Value="-MM-">-MM--</asp:ListItem>
                        <asp:ListItem Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Working Hours<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtWorking" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Compensatory Days<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtComp" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" CausesValidation="true" />
        </div>
    </div>
</div>


