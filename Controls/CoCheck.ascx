<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CoCheck.ascx.vb" Inherits="Controls_CoCheck" %>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By Any.."></asp:TextBox>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
        <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" />
    </div>
</div>
<div class="col-md-12">
    <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false" EmptyDataText="No Requests Found."
        CssClass="table GridStyle" GridLines="none">
        <Columns>

            <asp:TemplateField HeaderText="Request ID" ItemStyle-HorizontalAlign="left">
                <ItemTemplate>
                    <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID", "~/FAM/FAM_WebFiles/frmAssetRequisitionDetails_CO.aspx?RID={0}") %>'
                        Text='<%# Eval("AIR_REQ_ID")%> '>
                    </asp:HyperLink>
                    <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
            <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
            <asp:BoundField DataField="LCM_NAME" HeaderText="Location" ItemStyle-HorizontalAlign="left" />
            <asp:BoundField DataField="VT_TYPE" HeaderText="Item" ItemStyle-HorizontalAlign="left" />
            <asp:BoundField DataField="AID_QTY" HeaderText="Quantity" ItemStyle-HorizontalAlign="left" />
            <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
            <%-- <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                        <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmAssetRequisitionDetails_CO.aspx?RID={0}") %>' Text="View Details"></asp:HyperLink>
                        <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
        </Columns>
        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
        <PagerStyle CssClass="pagination-ys" />
    </asp:GridView>
</div>
<%--</div>--%>


