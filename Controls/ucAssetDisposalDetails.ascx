<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucAssetDisposalDetails.ascx.vb"
    Inherits="Controls_ucAssetDisposalDetails" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<div class="row">
  <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Category</label>
            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                ToolTip="Select Asset Category" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Sub Category</label>
            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                ToolTip="Select Asset Sub Category" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Brand/Make</label>
            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>
                Asset Model</label>
            <asp:DropDownList ID="ddlModel" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                ToolTip="Select Asset Model">
            </asp:DropDownList>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Location</label>
            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                ToolTip="Select Asset Model">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Duration</label>
            <select id="ddlRange" class="form-control selectpicker with-search" onhange="getDate(this)">
                <option value="">Select Range</option>
                <option value="TODAY">Today</option>
                <option value="YESTERDAY">Yesterday</option>
                <option value="7">Last 7 Days</option>
                <option value="30">Last 30 Days</option>
                <option value="THISMONTH">This Month</option>
                <option value="LASTMONTH">Last Month</option>
            </select>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>From Date</label>
            <div class='input-group date' id='Div1'>
                <asp:TextBox ID="FromDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>To Date</label>
            <div class='input-group date' id='Div4'>
                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Company</label>
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select company">
            </asp:DropDownList>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <br />
            <asp:Button ID="btnSubmit" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
        </div>
    </div>
</div>


<div class="row table table table-condensed table-responsive">
    <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
        </div>
    </div>
</div>

<script>
    function refreshSelectpicker() {
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%=ddlModel.ClientID%>").selectpicker();
        $("#<%=ddlCompany.ClientID%>").selectpicker();

    }
    refreshSelectpicker();
</script>


