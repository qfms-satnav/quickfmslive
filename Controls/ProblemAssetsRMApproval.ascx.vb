Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data
Partial Class Controls_ProblemAssetsRMApproval
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CallFunction()
            
        End If
    End Sub
    Private Sub callfunction()
        BindRequestsGrid()
        pnlAssets.Visible = False
        tr1.Visible = False
        tr2.Visible = False
        btnsubmit.Visible = False
        txtremarks.ReadOnly = True
    End Sub
    Private Sub BindRequestsGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_FAM_SP_GETREQUESTS_RM")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvRequests.DataSource = sp.GetDataSet()
            gvRequests.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvRequests_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRequests.PageIndexChanging
        gvRequests.PageIndex = e.NewPageIndex()
        BindRequestsGrid()
    End Sub

    Protected Sub gvRequests_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRequests.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblreqid As Label = DirectCast(gvRequests.Rows(rowIndex).FindControl("lblreqid"), Label)
            txtstore.Text = lblreqid.Text
            BindDetailsforApproval()
            pnlAssets.Visible = True
            If gvAssetDetails.Rows.Count > 0 Then
                tr1.Visible = True
                tr2.Visible = True
                btnsubmit.Visible = True
                btnReject.Visible = True
            Else
                tr1.Visible = False
                tr2.Visible = False
                btnsubmit.Visible = False
                btnReject.Visible = False
            End If
        End If
    End Sub
    Private Sub BindDetailsforApproval()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_FAM_SP_GETDETAILS_RM")
            sp.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                gvAssetDetails.DataSource = ds
                gvAssetDetails.DataBind()
            End If
            txtremarks.Text = ds.Tables(1).Rows(0).Item("REMARKS")
            tr1.Visible = True

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvAssetDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAssetDetails.PageIndexChanging
        gvAssetDetails.PageIndex = e.NewPageIndex()
        BindDetailsforApproval()
    End Sub

    

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvAssetDetails.Rows.Count - 1
                Dim chkbxasset As CheckBox = DirectCast(gvAssetDetails.Rows(i).FindControl("chkbxasset"), CheckBox)
                If chkbxasset.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_FAM_SP_REQUEST_RM_APPROVE")
                    sp.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
                    sp.Command.AddParameter("@AAT_CODE", CType(gvAssetDetails.Rows(i).FindControl("lblastcode"), Label).Text, DbType.String)
                    sp.Command.AddParameter("@REMARKS", txtrmrks.Text, DbType.String)
                    sp.ExecuteScalar()
                    txtrmrks.Text = ""
                End If
                lblMsg.Text = "Request Approved Succesfully"
                callfunction()
            Next
            If flag = False Then
                lblMsg.Text = "Please Select any of the Asset to Approve Request"
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvAssetDetails.Rows.Count - 1
                Dim chkbxasset As CheckBox = DirectCast(gvAssetDetails.Rows(i).FindControl("chkbxasset"), CheckBox)
                If chkbxasset.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_FAM_SP_REQUEST_RM_REJECT")
                    sp.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
                    sp.Command.AddParameter("@AAT_CODE", CType(gvAssetDetails.Rows(i).FindControl("lblastcode"), Label).Text, DbType.String)
                    sp.Command.AddParameter("@REMARKS", txtrmrks.Text, DbType.String)
                    sp.ExecuteScalar()
                    txtrmrks.Text = ""
                End If
                lblMsg.Text = "Request Rejected by RM"
                callfunction()
            Next
            If flag = False Then
                lblMsg.Text = "Please Select any of the Asset to Approve Request"
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
