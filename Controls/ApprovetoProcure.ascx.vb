Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common

Partial Class Controls_ApprovetoProcure
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using

        If Not IsPostBack Then
            'BindGrid()
            rolebased()
        End If
    End Sub
    Dim user As String
    Dim user2 As String
    Private Sub rolebased()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        Dim ds As DataSet
        ds = sp.GetDataSet
        For Each dr As DataRow In ds.Tables(0).Rows
            user = dr("URL_USR_ID")
        Next
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument2]")
        Dim ds2 As DataSet
        ds2 = sp2.GetDataSet
        For Each dr As DataRow In ds2.Tables(0).Rows
            user2 = dr("URL_USR_ID")
        Next

        If user = Session("UID") Then
            BindGrid(1012)
        ElseIf user2 = Session("UID") Then
            BindGrid(1012)
        Else
            BindGrid(1008)
        End If
    End Sub
    Private Sub BindGrid(ByVal i As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetByStatusId3")
        sp.Command.AddParameter("@dummy", i, Data.DbType.Int32)
        sp.Command.AddParameter("@Cuser", Session("uid"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub

    'Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
    '    gvItems.PageIndex = e.NewPageIndex
    '    BindGrid()
    'End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Private Sub UpdateAssetRequisitionData(ByVal ReqId As String, ByVal ProductId As String, ByVal StockQty As Integer, ByVal PurchaseQty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_UpdateStockPurchaseQtyByReqIdAndProductIDApprove")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@USERID", Session("UID"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal StatusId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionStatus_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REM", txtRM.Text, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        'Dim ds As DataSet
        'ds = sp.GetDataSet
        'For Each dr As DataRow In ds.Tables(0).Rows
        '    user = dr("URL_USR_ID")
        'Next
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        Dim ds As DataSet
        ds = sp.GetDataSet
        For Each dr As DataRow In ds.Tables(0).Rows
            user = dr("URL_USR_ID")
        Next
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument2]")
        Dim ds2 As DataSet
        ds2 = sp2.GetDataSet
        For Each dr As DataRow In ds2.Tables(0).Rows
            user2 = dr("URL_USR_ID")
        Next

        If user = Session("UID") Then
            RoleData()
        ElseIf user2 = Session("UID") Then
            RoleData()

        Else
            RoleNormalData()
        End If

        'Dim ds As DataSet
        'Dim lblReqID As Label
        'Dim FlagStock As Boolean = False
        'Dim FlagPurchase As Boolean = False
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
        '    If chkSelect.Checked Then
        '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
        '        sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
        '        ds = sp1.GetDataSet
        '        For Each dr As DataRow In ds.Tables(0).Rows
        '            Dim lblProductId As String = dr("AST_MD_CODE")
        '            Dim txtQty As String = dr("AID_QTY")
        '            Dim txtStockQty As String = dr("AID_MVM_QTY")
        '            Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
        '            If CInt(Trim(txtPurchaseQty)) > 0 Then
        '                FlagPurchase = True
        '            End If
        '            If CInt(Trim(txtStockQty)) > 0 Then
        '                FlagStock = True
        '            End If
        '            UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
        '        Next
        '        Dim StatusId As Integer = 0
        '        If FlagPurchase = True And FlagStock = True Then
        '            StatusId = 1016
        '        ElseIf FlagPurchase = True Then
        '            StatusId = 1012
        '        ElseIf FlagStock = True Then
        '            StatusId = 1015
        '        End If
        '        If StatusId > 0 Then
        '            UpdateData(lblReqID.Text, StatusId)
        '        End If
        '        send_mail_PO(lblReqID.Text, 1)
        '    End If
        'Next
        'Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub
    Private Sub RoleData()
        Dim ds As DataSet
        Dim lblReqID As Label
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
            If chkSelect.Checked Then
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                ds = sp1.GetDataSet
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim lblProductId As String = dr("AST_MD_CODE")
                    Dim txtQty As String = dr("AID_QTY")
                    Dim txtStockQty As String = dr("AID_MVM_QTY")
                    Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
                    If CInt(Trim(txtPurchaseQty)) > 0 Then
                        FlagPurchase = True
                    End If
                    If CInt(Trim(txtStockQty)) > 0 Then
                        FlagStock = True
                    End If
                    UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
                Next
                Dim StatusId As Integer = 0
                If FlagPurchase = True And FlagStock = True Then
                    StatusId = 1016
                ElseIf FlagPurchase = True Then
                    StatusId = 3022
                ElseIf FlagStock = True Then
                    StatusId = 1015
                End If
                If StatusId > 0 Then
                    UpdateData(lblReqID.Text, StatusId)
                End If
                send_mail_PO(lblReqID.Text, 1)
            End If
        Next
        Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub

    Private Sub RoleNormalData()
        Dim ds As DataSet
        Dim lblReqID As Label
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
            If chkSelect.Checked Then
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                ds = sp1.GetDataSet
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim lblProductId As String = dr("AST_MD_CODE")
                    Dim txtQty As String = dr("AID_QTY")
                    Dim txtStockQty As String = dr("AID_MVM_QTY")
                    Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
                    If CInt(Trim(txtPurchaseQty)) > 0 Then
                        FlagPurchase = True
                    End If
                    If CInt(Trim(txtStockQty)) > 0 Then
                        FlagStock = True
                    End If
                    UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
                Next
                Dim StatusId As Integer = 0
                If FlagPurchase = True And FlagStock = True Then
                    StatusId = 1016
                ElseIf FlagPurchase = True Then
                    StatusId = 1012
                ElseIf FlagStock = True Then
                    StatusId = 1015
                End If
                If StatusId > 0 Then
                    UpdateData(lblReqID.Text, StatusId)
                End If
                send_mail_PO(lblReqID.Text, 1)
            End If
        Next
        Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub
    Public Sub send_mail_PO(ByVal ReqId As String, ByVal MODE As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_APPROVE")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", MODE, DbType.Int32)
        sp.Execute()
    End Sub

    'Public Sub send_mail_PO(ByVal ReqId As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_Reject")
    '    sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
    '    sp.Execute()
    'End Sub


    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        Dim ds As DataSet
        ds = sp.GetDataSet
        For Each dr As DataRow In ds.Tables(0).Rows
            user = dr("URL_USR_ID")
        Next
        If user = Session("UID") Then
            RoleRejData()
        Else
            RoleNormalRejData()
        End If
        'Dim ds As DataSet
        'Dim lblReqID As Label
        'Dim FlagStock As Boolean = False
        'Dim FlagPurchase As Boolean = False
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
        '    If chkSelect.Checked Then
        '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
        '        sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
        '        ds = sp1.GetDataSet
        '        For Each dr As DataRow In ds.Tables(0).Rows
        '            Dim lblProductId As String = dr("AST_MD_CODE")
        '            Dim txtQty As String = dr("AID_QTY")
        '            Dim txtStockQty As String = dr("AID_MVM_QTY")
        '            Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
        '            If CInt(Trim(txtPurchaseQty)) > 0 Then
        '                FlagPurchase = True
        '            End If
        '            If CInt(Trim(txtStockQty)) > 0 Then
        '                FlagStock = True
        '            End If
        '            UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
        '        Next
        '        Dim StatusId As Integer = 0
        '        If FlagPurchase = True And FlagStock = True Then
        '            StatusId = 1016
        '        ElseIf FlagPurchase = True Then
        '            StatusId = 1012
        '        ElseIf FlagStock = True Then
        '            StatusId = 1013
        '        End If
        '        If StatusId > 0 Then
        '            UpdateData(lblReqID.Text, StatusId)
        '        End If
        '        send_mail_PO(lblReqID.Text, 2)
        '    End If
        'Next
        'Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub

    Private Sub RoleNormalRejData()
        Dim ds As DataSet
        Dim lblReqID As Label
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
            If chkSelect.Checked Then
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                ds = sp1.GetDataSet
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim lblProductId As String = dr("AST_MD_CODE")
                    Dim txtQty As String = dr("AID_QTY")
                    Dim txtStockQty As String = dr("AID_MVM_QTY")
                    Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
                    If CInt(Trim(txtPurchaseQty)) > 0 Then
                        FlagPurchase = True
                    End If
                    If CInt(Trim(txtStockQty)) > 0 Then
                        FlagStock = True
                    End If
                    UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
                Next
                Dim StatusId As Integer = 0
                If FlagPurchase = True And FlagStock = True Then
                    StatusId = 1016
                ElseIf FlagPurchase = True Then
                    StatusId = 1012
                ElseIf FlagStock = True Then
                    StatusId = 1013
                End If
                If StatusId > 0 Then
                    UpdateData(lblReqID.Text, StatusId)
                End If
                send_mail_PO(lblReqID.Text, 2)
            End If
        Next
        Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub

    Private Sub RoleRejData()
        Dim ds As DataSet
        Dim lblReqID As Label
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
            If chkSelect.Checked Then
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                ds = sp1.GetDataSet
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim lblProductId As String = dr("AST_MD_CODE")
                    Dim txtQty As String = dr("AID_QTY")
                    Dim txtStockQty As String = dr("AID_MVM_QTY")
                    Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
                    If CInt(Trim(txtPurchaseQty)) > 0 Then
                        FlagPurchase = True
                    End If
                    If CInt(Trim(txtStockQty)) > 0 Then
                        FlagStock = True
                    End If
                    UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
                Next
                Dim StatusId As Integer = 0
                If FlagPurchase = True And FlagStock = True Then
                    StatusId = 1016
                ElseIf FlagPurchase = True Then
                    StatusId = 1012
                ElseIf FlagStock = True Then
                    StatusId = 3023
                End If
                If StatusId > 0 Then
                    UpdateData(lblReqID.Text, StatusId)
                End If
                send_mail_PO(lblReqID.Text, 2)
            End If
        Next
        Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        Dim ds As DataSet
        Dim str As String
        ds = sp.GetDataSet
        For Each dr As DataRow In ds.Tables(0).Rows
            user = dr("URL_USR_ID")
        Next
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument2]")
        Dim ds2 As DataSet
        ds2 = sp2.GetDataSet
        For Each dr As DataRow In ds2.Tables(0).Rows
            user2 = dr("URL_USR_ID")
        Next

        If user = Session("UID") Then
            str = 1012
        ElseIf user2 = Session("UID") Then
            str = 1012
        Else
            str = 1008
        End If
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_APPROVE_TO_PROCURE")
        sp3.Command.AddParameter("@SEARCH_CRITERIA", txtSearch.Text, Data.DbType.String)
        sp3.Command.AddParameter("@CUSER", Session("UID"), DbType.String)
        sp3.Command.AddParameter("@DUMMY", str, DbType.Int32)
        gvItems.DataSource = sp3.GetDataSet
        gvItems.DataBind()
        'btnsubmit.Visible = False
        'btnCancel.Visible = False
        'divapprove.Visible = False
    End Sub
End Class
