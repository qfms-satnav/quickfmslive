Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGVendorNewRecord
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim ValidateCode As Integer
        ValidateCode = ValidateVendorCode()
        If ValidateCode = 0 Then
            lblMsg.Text = "Code is in use, try another"
        ElseIf ValidateCode = 1 Then
            insertnewrecord()
        End If
        
    End Sub
    Public Function ValidateVendorCode()
        Dim ValidateCode As Integer
        Dim AVR_CODE As String = txtCode.Text
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_VENDOR_CODE")
        sp5.Command.AddParameter("@AVR_CODE", AVR_CODE, DbType.String)
        ValidateCode = sp5.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_AMG_VENDOR")
            sp1.Command.AddParameter("@AVR_CODE", txtCode.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_NAME", txtName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_GRADE", rdbtnGrade.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_ADDR", txtAddress.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_CITY", ddlCity.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_STATE", ddlState.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_COUNTRY", txtCountry.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_PHNO", txtPhone.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_MOBILE_PHNO", txtMobile.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_EMAIL", txtEmail.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STATUS", rdbtnStatus.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_STA_ID", rdbtnStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AVR_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AVR_REMARKS", txtRemarks.Text, DbType.String)


            sp1.ExecuteScalar()
            lblMsg.Text = "New Vendor Added"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub Cleardata()
        txtCode.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        txtPhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtRemarks.Text = ""
        ddlCity.SelectedIndex = 0
        'ddlState.SelectedItem.Value = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
            sp2.Command.AddParameter("@dummy", " ", DbType.String)
            ddlCity.DataSource = sp2.GetDataSet()
            ddlCity.DataTextField = "CTY_NAME"
            ddlCity.DataValueField = "CTY_CODE"
            ddlCity.DataBind()
            ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/MASTERS/MAS_Webfiles/frmMASMasters.aspx")
    End Sub

    'Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
    '    Dim AVR_STATE As String = ddlState.SelectedItem.Value
    '    Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
    '    sp4.Command.AddParameter("@AVR_STATE", AVR_STATE, DbType.String)
    '    Dim ds4 As New DataSet
    '    ds4 = sp4.GetDataSet()
    '    ddlCity.DataSource = ds4
    '    ddlCity.DataTextField = "CTY_NAME"
    '    ddlCity.DataValueField = "CTY_CODE"
    '    ddlCity.DataBind()
    '    ddlCity.Items.Insert(0, "--Select--")
    'End Sub
End Class
