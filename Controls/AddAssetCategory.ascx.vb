Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Controls_AddAssetCategory
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
        RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        RegExpNumber.ValidationExpression = User_Validation.GetValidationExpressionForPhone.VAL_EXPR()
        If Not IsPostBack Then
            'getassetbrand()
            btnSubmit.Text = "Add"
            fillgrid()
            If Session("DepMethod") = 2 Then
                divdepr.Visible = True
            Else
                divdepr.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text = "Add" Then
            Dim ValidateCode As Integer
            ValidateCode = ValidateBrand(txtBrand.Text)
            If ValidateCode = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Asset Category Code is in use; try another"
            ElseIf ValidateCode = 1 Then

                Dim orgfilenameINC As String = Replace(Replace(fpBrowseIncDoc.FileName, " ", "_"), "&", "_")
                Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
                Dim repdocdatetimeINC As String = ""

                If (fpBrowseIncDoc.HasFile) Then
                    Dim fileExt As String
                    fileExt = System.IO.Path.GetExtension(fpBrowseIncDoc.FileName)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_INC_" & orgfilenameINC
                    fpBrowseIncDoc.PostedFile.SaveAs(filePath)
                    repdocdatetimeINC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "INC_" & orgfilenameINC
                End If
                insertnewrecord(repdocdatetimeINC)
                ' fillgrid()

            End If
        Else
            txtBrand.Enabled = False
            If ddlBrand.SelectedIndex <> 0 Then
                modifydata()
            End If
            hplINC.Visible = False
            'getassetbrand()
        End If
        ' Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=1")
        ' insertnewrecord()
        ' cleardata()

    End Sub
    Public Function ValidateBrand(ByVal brandcode As String)
        Dim ValidateCode As Integer
        'Dim PN_PROPERTYTYPE As String = brandcode
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_VALIDATE_ASSETCATEGORY")
        sp1.Command.AddParameter("@VT_CODE", brandcode, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord(fpincdocfilename As String)
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_INSERT_ASSETCATEGORY")
            '@VT_CODE,@VT_TYPE,@VT_STATUS,@VT_CREATED_BY,@VT_CREATED_DT,@VT_REM
            sp1.Command.AddParameter("@VT_CODE", txtBrand.Text, DbType.String)
            sp1.Command.AddParameter("@VT_TYPE", txtBrandName.Text, DbType.String)
            sp1.Command.AddParameter("@VT_STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@VT_CONS_STATUS", ddlasttype.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@VT_CREATED_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@VT_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.Command.AddParameter("@VT_UPLOADED_DOC", fpincdocfilename, DbType.String)
            sp1.Command.AddParameter("@DEPR_VAL", txtDepr.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True

            lblMsg.Text = "New Asset Category Successfully Added"
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvCat.DataSource = ds
        gvCat.DataBind()
        For i As Integer = 0 To gvCat.Rows.Count - 1
            Dim lblstatus As Label = CType(gvCat.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "Inactive"
            End If
        Next
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            ddlBrand.Visible = False
            lblAssetBrand.Visible = False
            cleardata()
            btnSubmit.Text = "Add"
            lblMsg.Visible = False
            txtBrand.Enabled = True
            fillgrid()
        Else
            ddlBrand.Visible = True
            lblAssetBrand.Visible = True
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            txtBrand.Enabled = False
            getassetbrand()
            fillgrid()
        End If

    End Sub
    Private Sub cleardata()
        txtBrand.Text = ""
        txtBrandName.Text = ""
        'txtBrand.Enabled = True
        'ddlBrand.SelectedIndex = 0
        ddlStatus.ClearSelection()
        ddlasttype.ClearSelection()
        txtRemarks.Text = ""
        txtDepr.Text = ""
    End Sub
    Private Sub getassetbrand()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlBrand.DataSource = sp.GetDataSet()
        ddlBrand.DataTextField = "VT_TYPE"
        ddlBrand.DataValueField = "VT_CODE"
        ddlBrand.DataBind()
        ddlBrand.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        lblMsg.Text = " "
        If ddlBrand.SelectedIndex <> 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ALLASSETCATEGORY")
            sp.Command.AddParameter("@VT_CODE", ddlBrand.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then '
                txtBrand.Text = ds.Tables(0).Rows(0).Item("VT_CODE")
                txtBrandName.Text = ds.Tables(0).Rows(0).Item("VT_TYPE")
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("VT_STATUS")).Selected = True
                ddlasttype.ClearSelection()
                ddlasttype.Items.FindByValue(ds.Tables(0).Rows(0).Item("VT_CONS_STATUS")).Selected = True
                txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("VT_REM"))
                txtDepr.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("VT_DEPR"))
            End If

        Else
            txtBrand.Text = ""
            txtBrandName.Text = ""
            ddlStatus.ClearSelection()
            ddlasttype.ClearSelection()
            txtRemarks.Text = ""
            txtDepr.Text = ""
        End If
    End Sub
    Private Sub modifydata()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_MODIFY_ASSETCATEGORY")
        sp1.Command.AddParameter("@VT_CODE", txtBrand.Text, DbType.String)
        sp1.Command.AddParameter("@VT_TYPE", txtBrandName.Text, DbType.String)
        sp1.Command.AddParameter("@VT_CONS_STATUS", ddlasttype.SelectedItem.Value, DbType.Int32)
        sp1.Command.AddParameter("@VT_MODIFIED_BY", Session("Uid"), DbType.String)
        sp1.Command.AddParameter("@VT_STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp1.Command.AddParameter("@VT_REM", txtRemarks.Text, DbType.String)
        sp1.Command.AddParameter("@VT_DEPR", txtDepr.Text, DbType.String)
        sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        Dim orgfilenameINC As String = Replace(Replace(fpBrowseIncDoc.FileName, " ", "_"), "&", "_")
        Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss")
        Dim repdocdatetimeINC As String = ""

        If (fpBrowseIncDoc.HasFile) Then
            Dim fileExt As String
            fileExt = System.IO.Path.GetExtension(fpBrowseIncDoc.FileName)
            Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime & "_INC_" & orgfilenameINC
            fpBrowseIncDoc.PostedFile.SaveAs(filePath)
            repdocdatetimeINC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & "INC_" & orgfilenameINC
        End If

        If repdocdatetimeINC = "" Then
            repdocdatetimeINC = Replace(lblINC.Text, "~\UploadFiles", "")
        End If
        sp1.Command.AddParameter("@VT_UPLOADED_DOC", repdocdatetimeINC, DbType.String)

        sp1.ExecuteScalar()
        cleardata()

        fillgrid()
        lblMsg.Visible = True
        ddlBrand.SelectedIndex = 0
        lblMsg.Text = "Asset Category successfully modified"
    End Sub

    'Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Response.Redirect("~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx")
    'End Sub


End Class
