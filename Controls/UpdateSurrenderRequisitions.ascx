<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateSurrenderRequisitions.ascx.vb"
    Inherits="Controls_UpdateSurrenderRequisitions" %>


<%--<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Code:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstCode" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Name:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstName" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Allocated Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstAllocDt" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Surrender Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstSurDt" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Surrender Requesition Id:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblSurReq_id" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Surrender By:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblSurBy" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Requestor Remarks:</label>
                <div class="col-md-7">
                    <asp:TextBox CssClass="form-control" ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Admin Remarks:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>--%>




<div id="SurReqDet" runat="server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">

            <asp:GridView ID="SurReqDetItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                EmptyDataText="No Asset(s) Found." CssClass="table GridStyle" GridLines="none">
                <Columns>
                    <asp:TemplateField HeaderText="Asset code" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("AAT_AST_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset ID">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AssetName">
                        <ItemTemplate>
                            <asp:Label ID="lblModelName" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Serial No">
                        <ItemTemplate>
                            <asp:Label ID="lblserial" runat="server" Text='<%#Eval("AAT_AST_SERIALNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" Visible="false" runat="server" Text='<%#Eval("LCM_CODE")%>'></asp:Label>
                            <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Allocated Date">
                        <ItemTemplate>
                            <asp:Label ID="lblAstAllocDt" runat="server" Text='<%#Eval("AAT_ALLOCATED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Surrender Date">
                        <ItemTemplate>
                            <asp:Label ID="lblAstSurDt" runat="server" Text='<%#Eval("AAT_SURRENDERED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender Requesition Id" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblSurReq_id" runat="server" Text='<%#Eval("SREQ_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender By">
                        <ItemTemplate>
                            <asp:Label ID="lblSurBy" runat="server" Text='<%#Eval("AUR_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender Status" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("SREQ_STATUS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>

        </div>
    </div>
    <br />
    <br />
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-4 control-label">Requestor Remarks:<span style="color: red;">*</span></label>
                <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                    ErrorMessage="Please Enter Valid Remarks" Display="None"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox CssClass="form-control" ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-4 control-label">Approver Remarks:<span style="color: red;">*</span></label>
                <asp:RegularExpressionValidator ID="regrmremarks" runat="server" ControlToValidate="txtRMRemarks"
                    ErrorMessage="Please Enter Valid Remarks" Display="None"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnApprov" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
