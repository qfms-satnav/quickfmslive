Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptChanges
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocalGrid()
            BindDomesticGrid()
            gvlocaldetails.Visible = False
            tr2.Visible = False
            lblTamount.Visible = False
            lblTotalAmount.Text = ""
            gvdomesticdetails.Visible = False
            tr3.Visible = False
            btnApproveDomestic.Visible = False
            btnRejectDomestic.Visible = False
            btnApproveLocal.Visible = False
            btnRejectLocal.Visible = False
        End If
    End Sub
    Private Sub BindLocalGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONV_GETDETAILS_FINANCE")
            sp.Command.AddParameter("@ESP_AUR_ID", Session("UID"), DbType.String)
            gvLocalRequests.DataSource = sp.GetDataSet()
            gvLocalRequests.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDomesticGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_GETDETAILS_FINANCE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvDomesticRequests.DataSource = sp.GetDataSet()
            gvDomesticRequests.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvLocalRequests_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLocalRequests.PageIndexChanging
        gvLocalRequests.PageIndex = e.NewPageIndex()
        BindLocalGrid()
    End Sub

    Protected Sub gvDomesticRequests_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDomesticRequests.PageIndexChanging
        gvDomesticRequests.PageIndex = e.NewPageIndex()
        BindDomesticGrid()
    End Sub
    Protected Sub btnApproveLocal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveLocal.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvlocaldetails.Rows.Count - 1

                Dim lbllocalID1 As Label = DirectCast(gvlocaldetails.Rows(i).FindControl("lbllocalID1"), Label)
                Dim chkbxlocal As CheckBox = DirectCast(gvlocaldetails.Rows(i).FindControl("chkbxlocal"), CheckBox)
                If chkbxlocal.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_APPROVE_FINANCE")
                    sp.Command.AddParameter("@ESP_REQ_ID", txtStore.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lbllocalID1.Text, DbType.Int32)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkboxes to Approve or Reject"
            Else
                Mail_FinanceLocalApproved(txtStore.Text)
                BindDetails()
                BindLocalGrid()

                total_amount(txtStore.Text)
                If gvlocaldetails.Rows.Count > 0 Then
                    btnApproveLocal.Visible = True
                    btnRejectLocal.Visible = True
                    lblTamount.Visible = True
                    lblTotalAmount.Visible = True
                    tr2.Visible = True
                Else
                    btnApproveLocal.Visible = False
                    btnRejectLocal.Visible = False
                    lblTamount.Text = ""
                    lblTotalAmount.Visible = False
                    tr2.Visible = False
                End If
                lblMsg.Text = "Local Conveyance Requisition Approved Successfully by Finance"
                txtRemarks.Text = ""
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnRejectLocal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRejectLocal.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvlocaldetails.Rows.Count - 1

                Dim lbllocalID1 As Label = DirectCast(gvlocaldetails.Rows(i).FindControl("lbllocalID1"), Label)
                Dim chkbxlocal As CheckBox = DirectCast(gvlocaldetails.Rows(i).FindControl("chkbxlocal"), CheckBox)
                If chkbxlocal.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_MODIFY_FINANCE")
                    sp.Command.AddParameter("@ESP_REQ_ID", txtStore.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lbllocalID1.Text, DbType.Int32)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkboxes to Approve or Reject"
            Else
                Mail_FinanceLocalRejected(txtStore.Text)
                BindDetails()
                BindLocalGrid()
                total_amount(txtStore.Text)
                If gvlocaldetails.Rows.Count > 0 Then
                    btnApproveLocal.Visible = True
                    btnRejectLocal.Visible = True
                    lblTamount.Visible = True
                    lblTotalAmount.Visible = True
                    tr2.Visible = True
                Else
                    btnApproveLocal.Visible = False
                    btnRejectLocal.Visible = False
                    lblTamount.Text = ""
                    lblTotalAmount.Visible = False
                    tr2.Visible = False
                End If
                lblMsg.Text = "Local Conveyance Requisition Rejected  by Finance"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvLocalRequests_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLocalRequests.RowCommand
        If e.CommandName = "View" Then
            gvlocaldetails.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lbllocalID As Label = DirectCast(gvLocalRequests.Rows(rowIndex).FindControl("lbllocalID"), Label)
            Dim id As String = lbllocalID.Text
            txtStore.Text = id
            BindDetails()
            If gvlocaldetails.Rows.Count > 0 Then
                btnApproveLocal.Visible = True
                btnRejectLocal.Visible = True
                tr2.Visible = True
            Else
                btnApproveLocal.Visible = False
                btnRejectLocal.Visible = False
                lblTamount.Visible = False
                lblTotalAmount.Text = ""
                tr2.Visible = False
            End If
            total_amount(txtStore.Text)
        End If
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_BIND_FINANCE")
            sp.Command.AddParameter("@ESP_REQ_ID", txtStore.Text, DbType.String)
            gvlocaldetails.DataSource = sp.GetDataSet()
            gvlocaldetails.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub total_amount(ByVal id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_TOTAL_AMOUNT_FINANCE")
        sp.Command.AddParameter("@ESP_REQ_ID", id, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblTotalAmount.Visible = True
            lblTamount.Visible = True
            lblTotalAmount.Text = "Rs." & "" & ds.Tables(0).Rows(0).Item("Amount")
        End If
    End Sub

    Protected Sub gvlocaldetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvlocaldetails.PageIndexChanging
        gvlocaldetails.PageIndex = e.NewPageIndex()
        BindDetails()
    End Sub

    Protected Sub gvDomesticRequests_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDomesticRequests.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblDomesticID As Label = DirectCast(gvDomesticRequests.Rows(rowIndex).FindControl("lblDomesticID"), Label)
            Dim id As String = lblDomesticID.Text
            txtstore1.Text = id
            BindDetailsDomestic()
            If gvdomesticdetails.Rows.Count > 0 Then
                btnApproveDomestic.Visible = True
                btnRejectDomestic.Visible = True
            Else
                btnApproveDomestic.Visible = False
                btnRejectDomestic.Visible = False
            End If
            gvdomesticdetails.Visible = True
            tr3.Visible = True
        Else
            gvdomesticdetails.Visible = False
            tr3.Visible = False
        End If
    End Sub
    Private Sub BindDetailsDomestic()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONEVYANCE_BINDDETAILS_FINANCE")
            sp.Command.AddParameter("@CNV_REQ_ID", txtstore1.Text, DbType.String)
            gvdomesticdetails.DataSource = sp.GetDataSet()
            gvdomesticdetails.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnApproveDomestic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveDomestic.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvdomesticdetails.Rows.Count - 1

                Dim lbldomesticID1 As Label = DirectCast(gvdomesticdetails.Rows(i).FindControl("lbldomesticID1"), Label)
                Dim chkbxdomestic As CheckBox = DirectCast(gvdomesticdetails.Rows(i).FindControl("chkbxdomestic"), CheckBox)
                If chkbxdomestic.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_RMAPPROVAL_FINANCE")
                    sp.Command.AddParameter("@CNV_REQ_ID", txtstore1.Text, DbType.String)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lbldomesticID1.Text, DbType.Int32)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkboxes to Approve or Reject"
            Else
                Mail_FinanceDomesticApproved(txtstore1.Text)
                lblMsg.Text = "Your Request Has been Approved by Finance"
                Cleardata()
                BindDomesticGrid()
                BindDetails()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnRejectDomestic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRejectDomestic.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvdomesticdetails.Rows.Count - 1

                Dim lbldomesticID1 As Label = DirectCast(gvdomesticdetails.Rows(i).FindControl("lbldomesticID1"), Label)
                Dim chkbxdomestic As CheckBox = DirectCast(gvdomesticdetails.Rows(i).FindControl("chkbxdomestic"), CheckBox)
                If chkbxdomestic.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_RMREJECTED_FINANCE")
                    sp.Command.AddParameter("@CNV_REQ_ID", txtstore1.Text, DbType.String)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lbldomesticID1.Text, DbType.Int32)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkboxes to Approve or Reject"
            Else
                Mail_FinanceDomesticRejected(txtstore1.Text)
                BindDomesticGrid()
                BindDetails()
                Cleardata()
                lblMsg.Text = "Your Request Has been Rejected by Finance"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Cleardata()
        If gvdomesticdetails.Rows.Count > 0 Then
            tr3.Visible = True
            txtremarksdomestic.Text = ""

            btnApproveDomestic.Visible = True
            btnRejectDomestic.Visible = True
        Else
            tr3.Visible = False
            btnApproveDomestic.Visible = False
            btnRejectDomestic.Visible = False
        End If
    End Sub
    Private Sub Mail_FinanceLocalApproved(ByVal Reqid As String)

        Dim strEmpName As String = ""
        Dim strEmpEmail As String = ""
        Dim strSubj As String = ""
        Dim strRMEmail As String = ""
        Dim strfrommail As String = ""
        Dim strfromname As String = ""
        Dim strHrEmail As String = ""
        Dim strMsg As String = ""
        Dim strMsgAdm As String = ""
        Dim strFinEmpName As String = ""
        Dim strFinEmpEmail As String = ""
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_APPROVED_REQUISITIONS_MAIL_FINANCE")
        sp1.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        gvlocal1details.DataSource = sp1.GetDataSet()
        gvlocal1details.DataBind()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONEVYANCE_BIND_DETAILS_MAIL_FINANCE")
        sp.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        strRMEmail = ds.Tables(0).Rows(0).Item("RM_EMAIL")

        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"
        strSubj = " Local Conveyance Request of " & strEmpName & " has been Approved by  Finance."

        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
         "This is to inform your that these are the Details of Your Approved Local Conveyance Requisitions by Finance" & _
         "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
         "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
         "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
          "<td  align=left width=12.5%><U>From Place: </U></td>" & _
         "<td  align=left width=12.5%><U>To Place: </U></td>" & _
         "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
         "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
         "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
         "<td  align=left width=12.5%><U>Amount </U></td>" & _
         "<td  align=left width=12.5%><U>Remarks </U></td>" & _
         "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvlocal1details.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"

        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP_FINANCE")


        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strFinEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strFinEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")


        'Mail message for the Finance. 
        strMsgAdm = "Dear " & strFinEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of  " & strEmpName & "'s  Approved Local Conveyance Requisitions by Finance" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
       "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
       "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
        "<td  align=left width=12.5%><U>From Place: </U></td>" & _
       "<td  align=left width=12.5%><U>To Place: </U></td>" & _
       "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
       "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
       "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
       "<td  align=left width=12.5%><U>Amount </U></td>" & _
       "<td  align=left width=12.5%><U>Remarks </U></td>" & _
         "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvlocal1details.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()

       ' Response.Write(strMsg)
       ' Response.Write(strMsgAdm)
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, Reqid, strSubj, strMsg, strRMEmail)
        End If
        If strFinEmpEmail <> "" Then
            Send_Mail(strFinEmpEmail, strfrommail, Reqid, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Sub Mail_FinanceLocalRejected(ByVal ReqID As String)
        Dim strEmpName As String = ""
        Dim strEmpEmail As String = ""
        Dim strSubj As String = ""
        Dim strRMEmail As String = ""
        Dim strfrommail As String = ""
        Dim strfromname As String = ""
        Dim strHrEmail As String = ""
        Dim strMsg As String = ""
        Dim strMsgAdm As String = ""
        Dim strFinEmpName As String = ""
        Dim strFinEmpEmail As String = ""
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_REJECTED_REQUISITIONS_MAIL_FINANCE")
        sp1.Command.AddParameter("@ESP_REQ_ID", ReqID, DbType.String)
        gvlocal1details.DataSource = sp1.GetDataSet()
        gvlocal1details.DataBind()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONEVYANCE_BIND_DETAILS_MAIL_FINANCE")
        sp.Command.AddParameter("@ESP_REQ_ID", ReqID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        strRMEmail = ds.Tables(0).Rows(0).Item("RM_EMAIL")

        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"
        strSubj = " Local Conveyance Request of " & strEmpName & " has been Rejected by  Finance."

        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
         "This is to inform your that these are the Details of Your Rejected Local Conveyance Requisitions by Finance" & _
         "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
         "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
         "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
          "<td  align=left width=12.5%><U>From Place: </U></td>" & _
         "<td  align=left width=12.5%><U>To Place: </U></td>" & _
         "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
         "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
          "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
         "<td  align=left width=12.5%><U>Amount </U></td>" & _
         "<td  align=left width=12.5%><U>Remarks </U></td>" & _
         "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvlocal1details.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"

        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP_FINANCE")
        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strFinEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strFinEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")


        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strFinEmpName & "," & "<br>" & "<br>" & _
        "This is to inform your that these are the Details of  " & strEmpName & "'s  Rejected Local Conveyance Requisitions by Finance" & _
        "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
       "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
       "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
        "<td  align=left width=12.5%><U>From Place: </U></td>" & _
       "<td  align=left width=12.5%><U>To Place: </U></td>" & _
       "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
       "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
       "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
       "<td  align=left width=12.5%><U>Amount </U></td>" & _
       "<td  align=left width=12.5%><U>Remarks </U></td>" & _
        "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvlocal1details.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvlocal1details.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
       ' Response.Write(strMsg)
        'Response.Write(strMsgAdm)

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, ReqID, strSubj, strMsg, strRMEmail)
        End If
        If strFinEmpEmail <> "" Then
            Send_Mail(strFinEmpEmail, strfrommail, ReqID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Private Sub Mail_FinanceDomesticApproved(ByVal REQID As String)
        Dim strEmpName As String = ""
        Dim strEmpEmail As String = ""
        Dim strSubj As String = ""
        Dim strRMEmail As String = ""
        Dim strfrommail As String = ""
        Dim strfromname As String = ""
        Dim strHrEmail As String = ""
        Dim strMsg As String = ""
        Dim strMsgAdm As String = ""
        Dim strFinEmpName As String = ""
        Dim strFinEmpEmail As String = ""

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_APPROVED_DETAILS_FINANCE")
        sp1.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        gvDomestic1Details.DataSource = sp1.GetDataSet()
        gvDomestic1Details.DataBind()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONEVYANCE_BINDDETAILS_MAIL_FINANCE")
        sp.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        strRMEmail = ds.Tables(0).Rows(0).Item("RM_EMAIL")

        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"


        'Mail message for the Requisitioner. 
        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of Your Approved Domestic Conveyance Requisitions" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvDomestic1Details.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1jrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Mode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Tmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Appkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1purs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Amount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Remarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1rmRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP_FINANCE")

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strFinEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strFinEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")

        'Mail message for the Finance. 
        strMsgAdm = "Dear " & strFinEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of  " & strEmpName & "'s  Approved Domestic Conveyance Requisitions  " & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left  width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvDomestic1Details.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1> <tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1jrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Mode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Tmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Appkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1purs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Amount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Remarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1rmRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner.
        'Response.Write(strMsg)
       ' Response.Write(strMsgAdm)

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strRMEmail)
        End If
        If strFinEmpEmail <> "" Then
            Send_Mail(strFinEmpEmail, strfrommail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Private Sub Mail_FinanceDomesticRejected(ByVal REQID As String)
        Dim strEmpName As String = ""
        Dim strEmpEmail As String = ""
        Dim strSubj As String = ""
        Dim strRMEmail As String = ""
        Dim strfrommail As String = ""
        Dim strfromname As String = ""
        Dim strHrEmail As String = ""
        Dim strMsg As String = ""
        Dim strMsgAdm As String = ""
        Dim strFinEmpName As String = ""
        Dim strFinEmpEmail As String = ""

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_REJECTED_DETAILS_FINANCE")
        sp1.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        gvDomestic1Details.DataSource = sp1.GetDataSet()
        gvDomestic1Details.DataBind()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONEVYANCE_BINDDETAILS_MAIL_FINANCE")
        sp.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        strRMEmail = ds.Tables(0).Rows(0).Item("RM_EMAIL")

        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"


        'Mail message for the Requisitioner. 
        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of Your Rejected Domestic Conveyance Requisitions" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvDomestic1Details.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1jrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Mode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Tmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Appkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1purs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Amount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Remarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1rmRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP_FINANCE")

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strFinEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strFinEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")

        'Mail message for the Finance. 
        strMsgAdm = "Dear " & strFinEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of  " & strEmpName & "'s  Rejected Domestic Conveyance Requisitions  " & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left  width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvDomestic1Details.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1> <tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1jrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Mode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Tmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Appkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1purs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Amount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1Remarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvDomestic1Details.Rows(i).FindControl("lblDomestic1rmRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner.
        'Response.Write(strMsg)
       ' Response.Write(strMsgAdm)

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strRMEmail)
        End If
        If strFinEmpEmail <> "" Then
            Send_Mail(strFinEmpEmail, strfrommail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub

End Class
