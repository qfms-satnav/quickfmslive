<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddPropertyType.ascx.vb"
    Inherits="Controls_AddPropertyType" EnableViewState="True" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 ">
        <div class="form-group">
            <label class="col-md-2 btn pull-right">
                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                    ToolTip="Please Select Add to add new Property Type and Select Modify to modify the existing Property Type" />
                Add
            </label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-md-2 btn pull-left" style="margin-left: 25px">
                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                    ToolTip="Please Select Add to add new Property Type and Select Modify to modify the existing Property Type" />
                Modify
            </label>
        </div>
    </div>
</div>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="CityPanel1" runat="server">
    <ContentTemplate>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" Style="padding-bottom: 20px" />

        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div id="trPropertyType" runat="server">
                        <label>Property Type<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvProprtyType" runat="server" ControlToValidate="txtPropType"
                            Display="none" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmpPkeys" runat="server" ErrorMessage="Please Select Vendor!"
                            ControlToValidate="cmbPKeys" Display="none" ValidationGroup="Val1" Operator="NotEqual"
                            ValueToCompare="--Select--">cmpPkeys</asp:CompareValidator>
                        <asp:DropDownList ID="cmbPKeys" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Property Type" OnSelectedIndexChanged="cmbPKeys_SelectedIndexChanged">
                        </asp:DropDownList>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Property Type Name<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfPropertyName" runat="server" ControlToValidate="txtPropType"
                        Display="none" ErrorMessage="Please Enter Property Type Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPropertyName" runat="server" ControlToValidate="txtPropType"
                        ErrorMessage="Please Enter Valid Property Type" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                        ValidationGroup="Val1">
                    </asp:RegularExpressionValidator>
                    <div onmouseover="Tip('Enter code in alphabets,numbers and /-,')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtPropType" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Status<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                        Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <br />
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 16px">
                <div class="form-group">
                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" OnClientClick="Hide()"
                        CausesValidation="true" />
                    <asp:Button ID="btnclear" CssClass="btn btn-primary custom-button-color" runat="server" Text="Clear" />
                    <asp:Button ID="btnback" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />
                </div>
            </div>
        </div>
        <br />
        <br />
        <div id="pnl" runat="server">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-5 control-label">Search By Property Type</label>
                            <div class="col-md-1"></div>
                            <div class="col-md-6">
                                <asp:RequiredFieldValidator ID="rfvtype" runat="server" ControlToValidate="txtSearch"
                                    Display="Dynamic" ErrorMessage="Please Enter Property Type" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">

                            <div class="row">
                                <div class="col-md-7 control-label">
                                    <asp:LinkButton ID="lbtn1" runat="Server" Text="Get All Records"></asp:LinkButton>
                                    <div class="col-md-5">

                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:GridView ID="gvPropType" runat="server" AllowPaging="True" AllowSorting="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Property Type Found." CssClass="table GridStyle" GridLines="none">
                    <PagerSettings Mode="NumericFirstLast" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("PN_TYPEID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Property Type">
                            <ItemTemplate>
                                <asp:Label ID="PID2" runat="server" CssClass="lblASTCode" Text='<%#Eval("PN_PROPERTYTYPE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Property Status">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("PN_TYPESTATUS")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
        <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
        <script type="text/javascript">
            function Hide() {
                $('#<%= lblMsg.ClientID%>').hide();
                return false;
            }
        </script>
        <script type="text/ecmascript">
            $(document).ready(function () {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    refreshSelectpicker();
                });
                function refreshSelectpicker() {
                    $("#<%=ddlStatus.ClientID%>").selectpicker();
                    $("#<%=cmbPKeys.ClientID%>").selectpicker();
                }
            });
        </script>
    </ContentTemplate>
</asp:UpdatePanel>
