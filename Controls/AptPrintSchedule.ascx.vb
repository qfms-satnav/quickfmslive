﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptPrintSchedule
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()


        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_REQDETAILS_PRINT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvitems1.DataSource = sp.GetDataSet()
            gvitems1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvitems1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub gvitems1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvitems1.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvitems1.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As String = lblID.Text
            Response.Redirect("~/EFM/EFM_Webfiles/frmAptPrintDetails.aspx?rid=" + id)

        End If
    End Sub
End Class
