Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class Controls_LeaseAudit
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            lblMsg.Text = ""
            BindCurrentLeases()
            '            btnexport.Visible = False
        End If
    End Sub
    Private Sub BindCurrentLeases()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_LEASE_CURRENT")
        sp.Command.AddParameter("@LEASE", txtsearch.Text, DbType.String)
        'gvitems.DataSource = sp.GetDataSet()
        'gvitems.DataBind()
        'If gvitems.Rows.Count > 0 Then
        '    'btnexport.Visible = True
        'Else
        '    'btnexport.Visible = False
        'End If
        Dim ds As New DataSet

        ds = sp.GetDataSet()
        Dim rds As New ReportDataSource()
        rds.Name = "ViewLeaseReportDS"

        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/ViewLeaseReport.rdlc")
        'Dim cur As String = CultureInfo.GetCultureInfo(Session("userculture"))
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("Currencyparam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub

    'Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    '    gvitems.PageIndex = e.NewPageIndex()
    '    BindCurrentLeases()
    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        '  If txtsearch.Text = "" Then
        '     lblMsg.Text = "Please Enter Lease name to find particular lease"
        ' Else
        BindCurrentLeases()
        '  End If
    End Sub

    'Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LEASE_CURRENT")
    '     sp.Command.AddParameter("@LEASE", txtsearch.Text, DbType.String)
    '    Dim gv As New GridView
    '    gv.DataSource = sp.GetDataSet()
    '    gv.DataBind()
    '    Export("Current_Lease_Details_" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
    'End Sub
    '#Region "Export"
    '    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
    '        HttpContext.Current.Response.Clear()
    '        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
    '        HttpContext.Current.Response.ContentType = "application/ms-excel"
    '        Dim sw As StringWriter = New StringWriter
    '        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '        '  Create a form to contain the grid
    '        Dim table As Table = New Table
    '        table.GridLines = gv.GridLines
    '        '  add the header row to the table
    '        If (Not (gv.HeaderRow) Is Nothing) Then
    '            PrepareControlForExport(gv.HeaderRow)
    '            table.Rows.Add(gv.HeaderRow)
    '        End If
    '        '  add each of the data rows to the table
    '        For Each row As GridViewRow In gv.Rows
    '            PrepareControlForExport(row)
    '            table.Rows.Add(row)
    '        Next
    '        '  add the footer row to the table
    '        If (Not (gv.FooterRow) Is Nothing) Then
    '            PrepareControlForExport(gv.FooterRow)
    '            table.Rows.Add(gv.FooterRow)
    '        End If
    '        '  render the table into the htmlwriter
    '        table.RenderControl(htw)
    '        '  render the htmlwriter into the response
    '        HttpContext.Current.Response.Write(sw.ToString)
    '        HttpContext.Current.Response.End()
    '    End Sub
    '    ' Replace any of the contained controls with literals
    '    Private Shared Sub PrepareControlForExport(ByVal control As Control)
    '        Dim i As Integer = 0
    '        Do While (i < control.Controls.Count)
    '            Dim current As Control = control.Controls(i)
    '            If (TypeOf current Is LinkButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
    '            ElseIf (TypeOf current Is ImageButton) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
    '            ElseIf (TypeOf current Is HyperLink) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
    '            ElseIf (TypeOf current Is DropDownList) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
    '            ElseIf (TypeOf current Is TextBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
    '            ElseIf (TypeOf current Is CheckBox) Then
    '                control.Controls.Remove(current)
    '                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
    '                'TODO: Warning!!!, inline IF is not supported ?
    '            End If
    '            If current.HasControls Then
    '                PrepareControlForExport(current)
    '            End If
    '            i = (i + 1)
    '        Loop
    '    End Sub
    '#End Region


End Class
