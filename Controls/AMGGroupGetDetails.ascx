<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGGroupGetDetails.ascx.vb"
    Inherits="Controls_AMGGroupGetDetails" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<table width="98%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">View Asset Group Details
             <hr align="center" width="60%" /></asp:Label>
            &nbsp;
            <br />
        </td>
    </tr>
</table>
<asp:Panel ID="panel1" runat="server" Width="85%">
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Asset Group Details</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table width="80%" cellpadding="3" cellspacing="0" align="center" border="0">
                    <tr>
                        <td align="left" style="height: 26px" width="30%">
                            Enter Code to find Asset Group
                        </td>
                        <td align="left" style="height: 26px" width="25%">
                            <asp:TextBox ID="txtfindcode" runat="server" CssClass="clsTextField"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rffindcode" runat="server" ControlToValidate="txtfindcode"
                                ErrorMessage="Please enter Asset Group Code !" ValidationGroup="Val1" SetFocusOnError="True"
                                Display="none"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px" width="5%" valign="top">
                            <asp:Button ID="btnfincode" runat="server" CssClass="button" Text="GO" ValidationGroup="Val1" />
                        </td>
                        <td align="left" style="height: 26px" width="22%" valign="top" runat="server">
                            <asp:HyperLink ID="Hyperlink1" runat="Server" Text="Add New Group" NavigateUrl="~/WorkSpace/SMS_Webfiles/frmAMGGroupNewRecord.aspx"></asp:HyperLink>
                        </td>
                        <td align="LEFT" style="height: 26px" width="20%" valign="top">
                            <asp:LinkButton ID="lbtn1" runat="Server" Text="Get All Records"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td align="left" style="height: 20px">
                            <asp:GridView ID="gvDetails_AAG" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" PageSize="5" EmptyDataText="No Records Found">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" CssClass="lblGroupID" Text='<%#Eval("AAG_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode_AAG" runat="server" CssClass="lblGroupCODE" Text='<%#Eval("AAG_CODE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGrpName_AAG" runat="server" CssClass="lblGroupName" Text='<%#Eval("AAG_NAME")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SType">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStype_AAG" runat="server" CssClass="lblGroupStype" Text='<%#Eval("AAG_STYPE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStaID_AAG" runat="server" CssClass="lblStaID" Text='<%#Eval("AAG_STA_ID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUpdatedBy_AAG" runat="server" CssClass="lblGroupUpdatedBy" Text='<%#Eval("AAG_UPT_BY")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EXHCode">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXHCode_AAG" runat="server" CssClass="lblEXHCode" Text='<%#Eval("AAG_EXH_CODE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="MFType">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMFType_AAG" runat="server" CssClass="lblMFType" Text='<%#Eval("AAG_MFTYPE")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate_AAB" runat="server" CssClass="lblGroupUpdateDate" Text='<%#Eval("AAG_UPT_DT")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks_AAG" runat="server" CssClass="lblRemarks" Text='<%#Eval("AAG_REM")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <a href='frmAMGGroupModify.aspx?code=<%#Eval("AAG_CODE")%>'>EDIT</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</asp:Panel>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="gvDetails_AAG" />
    </Triggers>
</asp:UpdatePanel>
