<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AdminITApprovalDtls.ascx.vb" Inherits="Controls_AdminITApprovalDtls" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Admin/IT Approval Details
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Admin/IT Approval Details</strong></td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="center" valign="top" height="100%">
                    <table width="100%" cellpadding="2px">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Requisition Id:</td>
                            <td align="left">
                                <asp:Label ID="lblReqId" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Raised By:</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlEmp" runat="server" Width="275px" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Asset Category:</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" Width="275px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" valign="top">
                                <asp:Panel ID="pnlItems" runat="server" Width="100%">
                                    Assets List
                                    <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        EmptyDataText="No Asset(s) Found." Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="sku" HeaderText="sku" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="productname" HeaderText="Name" ItemStyle-HorizontalAlign="left" />
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Enabled="false"></asp:TextBox>
                                                    <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("ProductId") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" Enabled="False"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Status:</td>
                            <td align="left">
                                <asp:TextBox ID="txtStatus" runat="server" Width="275px" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="tr3" runat="Server">
                            <td align="left">
                                Requestor Remarks:</td>
                            <td align="left">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="400px" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="tr1" runat="server">
                            <td align="left">
                                RM Remarks:</td>
                            <td align="left">
                                <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" Width="400px" ReadOnly="True" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="tr2" runat="Server">
                            <td align="left">
                                Admin Remarks:</td>
                            <td align="left">
                                <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnApprove" Text="Approve" runat="server" CssClass="button" />&nbsp;
                                <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="button" />
                                <asp:Button ID="btnUpdate" Text="Update" runat="Server" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>