Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Imports clsSubSonicCommonFunctions
Partial Class Controls_DisposableAssetsRequisition
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMsg.Text = ""
            BindSurrenderRequisitions()


        End If
    End Sub






    Private Sub BindSurrenderRequisitions()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")

        param(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("COMPANYID")

        ObjSubsonic.GetSubSonicDataSet("GET_ALLDESPOSED_REQ", param)
        'If gvSurrenderAstReq.Rows.Count = 0 Then
        '    gvSurrenderAstReq.DataSource = Nothing
        '    gvSurrenderAstReq.DataBind()

        'End If
        Dim ds1 As DataSet
        ds1 = ObjSubsonic.GetSubSonicDataSet("GET_ALLDESPOSED_REQ", param)
        gvSurrenderAstReq.DataSource = ds1.Tables(0)
        gvSurrenderAstReq.DataBind()

        'Admin requisitions grid bind

        'Dim param1(1) As SqlParameter
        'param1(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        'param1(0).Value = Session("UID")

        'param1(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        'param1(1).Value = Session("COMPANYID")


        'Dim ds2 As DataSet
        'ds2 = ObjSubsonic.GetSubSonicDataSet("GET_ADMINDISPOSE_REQ", param)
        'gvAstDispoReq.DataSource = ds2.Tables(0)
        'gvAstDispoReq.DataBind()


        'If gvSurrenderAstReq.Rows.Count = 0 And gvAstDispoReq.Rows.Count = 0 Then
        '    divremarks.Visible = False
        'End If

        If gvSurrenderAstReq.Rows.Count = 0 Then
            divremarks.Visible = False
        End If

    End Sub

    Protected Sub gvSurrenderAstReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSurrenderAstReq.PageIndexChanging
        gvSurrenderAstReq.PageIndex = e.NewPageIndex
        BindSurrenderRequisitions()
    End Sub

    Protected Sub gvSurrenderAstReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSurrenderAstReq.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim txtLnkText As LinkButton = CType(e.Row.Cells(0).FindControl("lnkEdit"), LinkButton)
            If txtLnkText.Text = "DONE" Then
                txtLnkText.Enabled = False
            Else
                txtLnkText.Enabled = True
            End If

        End If
    End Sub
    Protected Sub gvSurrenderAstReq_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSurrenderAstReq.RowCommand
        If e.CommandName = "Edit2" Then
            ' Response.Redirect("frmDisposableAssetsRequisitionDTLS.aspx?Req_id=" & e.CommandArgument)
            Dim Update As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(Update.NamingContainer, GridViewRow)
            Dim lbl_Cur_val As TextBox = DirectCast(gvRow.FindControl("lbl_Cur_val"), TextBox)
            Dim lbl_cost As TextBox = DirectCast(gvRow.FindControl("lbl_cost"), TextBox)
            Dim lblpur_Date As TextBox = DirectCast(gvRow.FindControl("lblpur_Date"), TextBox)
            Dim lblAstSno As Label = DirectCast(gvRow.FindControl("lblSREQ_ID"), Label)
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Dispose_Data_Update]")
            sp2.Command.AddParameter("@AST_CURRENT_VALUE", lbl_Cur_val.Text, DbType.String)
            sp2.Command.AddParameter("@dreq_ts", lblAstSno.Text, DbType.String)
            sp2.Command.AddParameter("@lblpur_Date", lblpur_Date.Text, DbType.String)
            sp2.Command.AddParameter("@AST_ACTUAL_COST", lbl_cost.Text, DbType.String)
            sp2.ExecuteScalar()
            lblMsg.Text = "Successfully updated "
        End If
        BindSurrenderRequisitions()
    End Sub



    'Protected Sub gvAstDispoReq_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAstDispoReq.RowCommand
    '    If e.CommandName = "Edit" Then
    '        Response.Redirect("frmAdminAstDisposableDTLS.aspx?Req_id=" & e.CommandArgument)
    '    End If
    'End Sub

    'Protected Sub gvAstDispoReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAstDispoReq.PageIndexChanging
    '    gvAstDispoReq.PageIndex = e.NewPageIndex
    '    BindSurrenderRequisitions()
    'End Sub


    Private Sub UpdateData(ByVal Status As String)
        ''Surrender Disposable
        Dim count1 As Integer = 0

        For Each gvRow As GridViewRow In gvSurrenderAstReq.Rows
            Dim SurrchkSelect As CheckBox = DirectCast(gvRow.FindControl("SurrchkSelect"), CheckBox)
            If SurrchkSelect.Checked = True Then
                count1 = count1 + 1
                Exit For
            End If
        Next
        If count1 > 0 Then
            For Each gvRow As GridViewRow In gvSurrenderAstReq.Rows
                Dim SurrchkSelect As CheckBox = DirectCast(gvRow.FindControl("SurrchkSelect"), CheckBox)
                If SurrchkSelect.Checked = True Then
                    Dim lblAstCode As Label = DirectCast(gvRow.FindControl("lblAAT_AST_CODE"), Label)
                    Dim lbl_salvge As Label = DirectCast(gvRow.FindControl("lblSalvage"), Label)
                    Dim lblSREQ_ID As Label = DirectCast(gvRow.FindControl("lblSREQ_ID"), Label)
                    Session("lblPoId") = lblSREQ_ID.Text



                    Dim param(3) As SqlParameter
                    param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                    param(0).Value = lblSREQ_ID.Text
                    param(1) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.NVarChar, 200)
                    param(1).Value = Session("UID")
                    param(2) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.NVarChar, 2000)
                    param(2).Value = txtRemarks.Text
                    param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
                    param(3).Value = Status

                    ObjSubsonic.GetSubSonicDataSet("UPDATEDISPOSABLE_REQUISTION_byAdmin", param)

                    send_mail(lblSREQ_ID.Text)
                    'If (Status = 1041) Then
                    '    DownloadPO()
                    'Else

                    'End If


                    'Response.Redirect("frmDisposableAssetsRequisition.aspx")
                    ' Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))
                End If
            Next
            lblMsg.Visible = True
            If (Status = 1041) Then
                lblMsg.Text = "Dispose Requests successfully Approved "
            Else
                lblMsg.Text = "Dispose Requests successfully Rejected ..."
            End If


        End If
        ''Admin Disposable
        'Dim count2 As Integer = 0
        'For Each gvRow As GridViewRow In gvAstDispoReq.Rows
        '    Dim DispchkSelect As CheckBox = DirectCast(gvRow.FindControl("DispchkSelect"), CheckBox)
        '    If DispchkSelect.Checked = True Then
        '        count2 = count2 + 1
        '        Exit For
        '    End If
        'Next
        'If count2 > 0 Then
        '    For Each gvRow2 As GridViewRow In gvAstDispoReq.Rows
        '        Dim DispchkSelect As CheckBox = DirectCast(gvRow2.FindControl("DispchkSelect"), CheckBox)
        '        If DispchkSelect.Checked = True Then
        '            Dim lblAstCode As Label = DirectCast(gvRow2.FindControl("lblAAT_AST_CODE"), Label)
        '            Dim lbl_salvge As Label = DirectCast(gvRow2.FindControl("lblSalvage"), Label)
        '            Dim lblDREQ_ID As Label = DirectCast(gvRow2.FindControl("lblDREQ_ID"), Label)


        '            Dim param(3) As SqlParameter
        '            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        '            param(0).Value = lblDREQ_ID.Text
        '            param(1) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.NVarChar, 200)
        '            param(1).Value = Session("UID")
        '            param(2) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.NVarChar, 2000)
        '            param(2).Value = txtRemarks.Text
        '            param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
        '            param(3).Value = Status
        '            ObjSubsonic.GetSubSonicDataSet("UPDATEDISPOSABLE_REQUISTION_byAdmin", param)
        '            send_mail(lblDREQ_ID.Text)
        '        End If
        '    Next
        '    lblMsg.Visible = True
        '    If (Status = 1041) Then
        '        lblMsg.Text = "Dispose Requests successfully Approved ..."
        '    Else
        '        lblMsg.Text = "Dispose Requests successfully Rejected ..."
        '    End If
        '    BindSurrenderRequisitions()
        'End If
        'If count1 = 0 And count2 = 0 Then
        '    lblMsg.Visible = True
        '    lblMsg.Text = "Select the requisition to Approve/Reject"
        'End If

        If count1 = 0 Then
            lblMsg.Visible = True
            lblMsg.Text = "Select the requisition to Approve/Reject"
        End If

    End Sub
    Protected Sub btnApprovAll_Click(sender As Object, e As EventArgs) Handles btnApprovAll.Click
        UpdateData(1041)
        BindSurrenderRequisitions()
    End Sub
    Public Sub send_mail(ByVal reqid As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION_APPROVAL")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Execute()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        UpdateData(1042)
        BindSurrenderRequisitions()
    End Sub
    Private Sub DownloadPO()
        Try
            Dim viewer As ReportViewer = New ReportViewer()
            ''Dim param() As SqlParameter
            Dim ObjSubSonic As New clsSubSonicCommonFunctions
            'Dim reqid = Request.QueryString("REQ_ID")
            Dim reqid = Session("lblPoId")
            Dim reqid1 = Session("lblPoId")
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ALLDESPOSED_REPORT")
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session("UID").ToString(), DbType.String)
            sp.Command.AddParameter("@Reqid", reqid, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()

            Dim rds As ReportDataSource = New ReportDataSource()
            rds.Name = "AssetDisposeDT"
            rds.Value = ds.Tables(0)
            Dim rds1 As ReportDataSource = New ReportDataSource()
            rds1.Name = "DisposeApproved"
            rds1.Value = ds.Tables(1)
            viewer.Reset()
            viewer.LocalReport.DataSources.Add(rds)
            viewer.LocalReport.DataSources.Add(rds1)
            viewer.LocalReport.EnableExternalImages = True
            viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetDisposalApprvdReport.rdlc")

            Dim ci As New CultureInfo(Session("userculture").ToString())
            Dim nfi As NumberFormatInfo = ci.NumberFormat
            'Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
            'Dim p2 As New ReportParameter("ImageVal", BindLogo())
            viewer.LocalReport.EnableExternalImages = True
            Dim imagePath As String = BindLogo()
            Dim parameter As New ReportParameter("Imagepath", imagePath)
            viewer.LocalReport.SetParameters(parameter)
            viewer.LocalReport.Refresh()

            viewer.ProcessingMode = ProcessingMode.Local
            ''viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetDisposalApprvdReport.rdlc")

            reqid = reqid.Substring(reqid.Length - 1)
            Dim FileName As String = "Dispose " & reqid & ".pdf" 'DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            ''Dim FileName As String = "Finalize PO" & DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            Dim extension As String
            Dim FileFullpath As String
            Dim encoding As String
            Dim mimeType As String
            Dim streams As String()
            Dim warnings As Warning()
            Dim contentType As String = "application/pdf"
            Dim mybytes As Byte() = viewer.LocalReport.Render("PDF", Nothing, extension, encoding, mimeType, streams, warnings)
            FileFullpath = Server.MapPath("~/AssetDisposal_PDF/") & FileName
            Using fs As FileStream = File.Create(Server.MapPath("~/AssetDisposal_PDF/") & FileName)
                fs.Write(mybytes, 0, mybytes.Length)
            End Using

            'Response.ContentType = contentType
            'Response.AddHeader("Content-Disposition", "inline; filename=" + FileName + ".pdf")
            ''Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
            ''Response.WriteFile(Server.MapPath("~/AssetDisposal_PDF/" & FileName))
            ''Response.Flush()
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "sp_get_Disposal_mails")
            sp3.Command.AddParameter("@Reqid", reqid1, DbType.String)
            Dim ds3 As DataSet = sp3.GetDataSet()
            If (ds3.Tables(0).Rows.Count > 0) Then
                SendMail(FileFullpath, ds3.Tables(0).Rows(0)("TOMAIL").ToString(), ds3.Tables(0).Rows(0)("CCMAIL").ToString())
            End If
        Catch ex As Exception
            ex.ToString()

        End Try
    End Sub



    Public Function BindLogo() As String
        'Dim imagePath As String
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            Return "https://live.quickfms.com/BootStrapCSS/images/" & ds3.Tables(0).Rows(0).Item("IMAGENAME")

        Else
            Return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png"
        End If
        'Return New Uri(Server.MapPath(imagePath)).AbsoluteUri
    End Function

    Public Shared Sub SendMail(ByVal FileFullpath As String, ByVal TOMAIL As String, ByVal CCMAIL As String)

        Dim smtp As SmtpClient = New SmtpClient()
        Dim fromaddr As MailAddress = New MailAddress(ConfigurationManager.AppSettings("from"))
        smtp.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("mailid"), ConfigurationManager.AppSettings("password"))
        smtp.Host = ConfigurationManager.AppSettings("Host")
        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings("Port"))
        smtp.EnableSsl = True
        Dim mailMessage As MailMessage = New MailMessage()
        mailMessage.From = fromaddr
        mailMessage.Subject = "Dispose Approved Report"

        'Dim TOemailarr As String = TOMAIL
        mailMessage.[To].Add(TOMAIL)
        'Dim TOemailarr As String() = ConfigurationManager.AppSettings("GLOBALTO").ToString().Split(";"c)

        'For Each [to] As String In TOemailarr
        '    mailMessage.[To].Add([to])
        'Next
        'Dim Bccemailarr As String = CCMAIL
        'Dim Bccemailarr As String() = ConfigurationManager.AppSettings("GLOBALBCC").ToString().Split(";"c)

        'For Each bcc As String In Bccemailarr
        mailMessage.CC.Add(CCMAIL)
        'Next

        Using attachment As Attachment = New Attachment(FileFullpath, MediaTypeNames.Application.Octet)

            Try
                Dim disposition As Mime.ContentDisposition = attachment.ContentDisposition
                disposition.CreationDate = File.GetCreationTime(FileFullpath)
                disposition.ModificationDate = File.GetLastWriteTime(FileFullpath)
                disposition.ReadDate = File.GetLastAccessTime(FileFullpath)
                disposition.FileName = Path.GetFileName(FileFullpath)
                disposition.Size = New FileInfo(FileFullpath).Length
                disposition.DispositionType = DispositionTypeNames.Attachment
                mailMessage.Attachments.Add(attachment)
            Catch ex As Exception
            End Try

            If mailMessage.[To].Count() > 0 Then smtp.Send(mailMessage)
        End Using
    End Sub
    Protected Sub gvSurrenderAstReq_RowCommand1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSurrenderAstReq.RowCommand
        If e.CommandName = "Download" Then
            Try
                Dim fileName As String = e.CommandArgument.ToString()
                Dim filePath As String = Server.MapPath("~/UploadFiles/" & fileName)
                If Not System.IO.File.Exists(filePath) Then
                    Throw New FileNotFoundException("Document Not Uploaded.")

                End If
                Response.Clear()
                Response.ContentType = "application/octet-stream"
                Response.AddHeader("Content-Disposition", "attachment;filename=""" & System.IO.Path.GetFileName(filePath) & """")
                Response.WriteFile(filePath)
                Response.[End]()
            Catch ex As Exception
                'Response.Write(ex.Message)
                lblMsg.Text = "Document Not Uploaded."
            End Try
        End If
    End Sub
End Class
