Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_View_Maintanance_Schedule
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindMainSchedules()
        End If
    End Sub

    Public Sub BindMainSchedules()
        ObjSubsonic.BindGridView(gv_MainSch, "GET_ALL_MAINSCH")
    End Sub


    Protected Sub gv_MainSch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gv_MainSch.PageIndexChanging
        gv_MainSch.PageIndex = e.NewPageIndex
        BindMainSchedules()
    End Sub

    Protected Sub gv_MainSch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gv_MainSch.RowCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("Edit_Maintain_Schedule.aspx?Req_id=" & e.CommandArgument)
        ElseIf e.CommandName = "Update" Then
            Response.Redirect("Update_Maintain_Schedule.aspx?Req_id=" & e.CommandArgument)
        End If
 
    End Sub
End Class
