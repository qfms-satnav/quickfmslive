Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_WorkRequestReport
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            panel2.Visible = False
            btnBack.Visible = IIf(Request.QueryString("rurl") <> Nothing, True, False)
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim DBrecords As String
            DBrecords = IIf(Request.QueryString("rurl") <> Nothing, "1", "0")
            Session("dbr") = DBrecords
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETWORKREQVAL")
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@Curr", DBrecords, DbType.String)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
            'For i As Integer = 0 To gvitems.Rows.Count - 1
            '    Dim lnkstatus As LinkButton = CType(gvitems.Rows(i).FindControl("lnkstatus"), LinkButton)
            '    If lnkstatus.Text = 0 Then
            '        lnkstatus.Text = "Pending"
            '    ElseIf lnkstatus.Text = 1 Then
            '        lnkstatus.Text = "In-Progress"
            '    Else
            '        lnkstatus.Text = 2
            '        lnkstatus.Text = "Completed"

            '    End If
            'Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "Update" Then
            Dim lnkstatus As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkstatus.NamingContainer, GridViewRow)
            Dim lblreq As Label = DirectCast(gvRow.FindControl("lblreq"), Label)
            Session("Stat") = lblreq.Text
            BindGrid1()
            panel2.Visible = True
        End If

    End Sub
    Private Sub BindGrid1()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETREQSSTAT")
            sp.Command.AddParameter("@dummy", Session("Stat"), DbType.String)
            sp.Command.AddParameter("@Curr", Session("dbr").ToString(), DbType.String)
            gvitems1.DataSource = sp.GetDataSet()
            gvitems1.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex
        BindGrid1()
    End Sub

    Protected Sub gvitems_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvitems.RowUpdating

    End Sub

    'Protected Sub gvitems1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
    '    If e.CommandName = "GET" Then
    '        Dim lnkreqid As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '        Dim gvRow1 As GridViewRow = DirectCast(lnkreqid.NamingContainer, GridViewRow)
    '        Dim lblreq1 As Label = DirectCast(gvRow1.FindControl("lblreq1"), Label)
    '        Dim s As String = lblreq1.Text
    '        lnkreqid.Attributes.Add("onClick", "JavaScript:showPopWin(frmWorkRequestDetails.aspx?Req=s,550,480,null)")
    '    End If
    'End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If Request.QueryString("rurl") <> Nothing Then
                Response.Redirect(Request.QueryString("rurl") + "?back=property")
            Else

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    
End Class
