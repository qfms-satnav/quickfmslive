﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptScheduleJourneyPlan
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            GETREQ()
            BindDep()
            Dept()
            BindRM()
            BindDetails()
            txtstartdate.Text = getoffsetdate(Date.Today)
            txtenddate.Text = getoffsetdate(Date.Today)
           
        End If
        txtstartdate.Attributes.Add("readonly", "readonly")
        txtenddate.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub GETREQ()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CREATE_REQUEST_ID")
            sp.Command.AddParameter("@RAISEDBY", Session("Uid"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtreqid.Text = ds.Tables(0).Rows(0).Item("Column1")

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GETASSOCIATE_DETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("AUR_NO")
                ddlDep.ClearSelection()
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                ddlRM.ClearSelection()
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_ADD_SCHEDULE_JOURNEY")
            sp.Command.AddParameter("@REQUESTID", txtreqid.Text)
            sp.Command.AddParameter("@RAISEDBY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@ORIGIN", txtOrigin.Text, DbType.String)
            sp.Command.AddParameter("@DESTINATION", txtDestinantion.Text, DbType.String)
            sp.Command.AddParameter("@MODEOFTRAVEL", ddlmodeoftravel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@DATEOFJOURNEY", txtstartdate.Text, DbType.String)
            sp.Command.AddParameter("@EndDate", txtenddate.Text, DbType.String)
            sp.Command.AddParameter("@TIME", ddltime.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REQUESTNAME", txtreqname.Text)
            If (chktravelassistance.Checked = True) Then
                sp.Command.AddParameter("@TRAVELASSISTANCE", "Yes")

            Else
                sp.Command.AddParameter("@TRAVELASSISTANCE", "No")
            End If
            sp.ExecuteScalar()
            lblMsg.Text = "Journey Plan Added Successfully"
            BindGrid()
            clear()
            btnsubmit.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        'Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CHECK_JOURNEYPLAN")
            sp1.Command.AddParameter("@REQUESTID", txtreqid.Text)
            Dim ds As New DataSet
            ds = sp1.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)("CNT") <> "0" Then
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_UPDATE_SCHEDULE_JOURNEY")
                    sp.Command.AddParameter("@REQUESTID", txtreqid.Text)
                    sp.ExecuteScalar()
                    btnsubmit.Visible = False
                    btnAdd.Visible = True
                    lblMsg.Text = "Request submitted successfully"
                    '  BindGrid()
                    ' clear()
                    '  GETREQ()
                Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=62")
                Else
                    lblMsg.Text = "No journey details found to schedule trip."
                End If

            Else
                lblMsg.Text = "No journey details found to schedule trip."
            End If

       ' Catch ex As Exception
         '   Response.Write(ex.Message)
       ' End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_SCHEDULEDETAILS")
            sp.Command.AddParameter("@REQUESTID", txtreqid.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                gvItems.DataSource = sp.GetDataSet()
                gvItems.DataBind()
                For i As Integer = 0 To gvItems.Rows.Count - 1
                    Dim lblMode As Label = CType(gvItems.Rows(i).FindControl("lblMode"), Label)
                    If lblMode.Text = "FLT" Then
                        lblMode.Text = "Flight"
                    ElseIf lblMode.Text = "TRN" Then
                        lblMode.Text = "Train"
                    ElseIf lblMode.Text = "BS" Then
                        lblMode.Text = "Bus"
                    ElseIf lblMode.Text = "CR" Then
                        lblMode.Text = "Car"
                    ElseIf lblMode.Text = "ATO" Then
                        lblMode.Text = "Auto"
                    ElseIf lblMode.Text = "BKE" Then
                        lblMode.Text = "Bike"
                    End If
                Next
                btnsubmit.Visible = True


            Else
                ' btnsubmit.Visible = False
                gvItems.DataBind()

            End If


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "EDIT" Then
            btnAdd.Visible = False

            lblMsg.Text = ""
            btnModify.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As Integer = Integer.Parse(lblID.Text)
            Show_Data(id)
            txtsno.Text = id

        ElseIf e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As Integer = Integer.Parse(lblID.Text)
            txtsno.Text = id
            DELSCHEDULE(txtsno.Text)
            BindGrid()
        End If
    End Sub
    Private Sub Show_Data(ByVal id As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_SCHEDULEDETAILSBYID")
            sp.Command.AddParameter("@ID", id, DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtstartdate.Text = ds.Tables(0).Rows(0).Item("STARTDATE")
                txtenddate.Text = ds.Tables(0).Rows(0).Item("ENDDATE")
                txtOrigin.Text = ds.Tables(0).Rows(0).Item("ORIGIN")
                txtDestinantion.Text = ds.Tables(0).Rows(0).Item("DESTINATION")
                ddlmodeoftravel.ClearSelection()
                ddlmodeoftravel.Items.FindByValue(ds.Tables(0).Rows(0).Item("MODEOFTRAVEL")).Selected = True
                If (ds.Tables(0).Rows(0).Item("TRAVELASSISTANCE") = "Yes") Then
                    chktravelassistance.Checked = True
                Else
                    chktravelassistance.Checked = False
                End If
                ddltime.ClearSelection()
                ddltime.Items.FindByText(ds.Tables(0).Rows(0).Item("time")).Selected = True


            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_UPDATE_SCHEDULE")
            sp.Command.AddParameter("@ID", txtsno.Text, DbType.Int32)
            sp.Command.AddParameter("@DATEOFJOURNEY", Convert.ToDateTime(txtstartdate.Text), DbType.Date)
            sp.Command.AddParameter("@ENDDATE", Convert.ToDateTime(txtenddate.Text), DbType.Date)
            sp.Command.AddParameter("@ORIGIN", txtOrigin.Text, DbType.String)
            sp.Command.AddParameter("@DESTINATION", txtDestinantion.Text, DbType.String)
            sp.Command.AddParameter("@MODEOFTRAVEL", ddlmodeoftravel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@TIME", ddltime.SelectedItem.Text, DbType.String)
            If (chktravelassistance.Checked = True) Then
                sp.Command.AddParameter("@TRAVELASSISTANCE", "Yes")

            Else
                sp.Command.AddParameter("@TRAVELASSISTANCE", "No")
            End If
            sp.ExecuteScalar()
            BindGrid()
            lblMsg.Text = "Journey Plan Modified Successfully."
            clear()
            btnModify.Visible = False
            btnAdd.Visible = True

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub clear()
        Try
            txtOrigin.Text = ""
            txtDestinantion.Text = ""
            ddlmodeoftravel.SelectedValue = 0

            ddltime.SelectedValue = 0
            chktravelassistance.Checked = False
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvItems_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvItems.RowEditing

    End Sub
    Private Sub DELSCHEDULE(ByVal id1 As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_JOURNEYSCHEDULE_DELETE")
            sp.Command.AddParameter("@id", id1, DbType.Int32)
            sp.ExecuteScalar()

            lblMsg.Text = "journey details deleted successfully"

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvItems.RowDeleting

    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs) Handles btncancel.Click
        clear()
        btnModify.Visible = False
        btnAdd.Visible = True

    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub




End Class
