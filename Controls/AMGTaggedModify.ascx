<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGTaggedModify.ascx.vb" Inherits="Controls_AMGTaggedModify" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
 <div>
 <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">AssetTagged<hr align="center" width="60%" /> </asp:Label>
                </td>
            </tr>
            <tr>
                <td width="100%" align="center">
                </td>
            </tr>
        </table>
 <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table cellpadding="0" cellspacing="0" style="vertical-align: top;" width="95%"
                align="center" border="0">
                <tr>
                    <td style="width: 10px">
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" />
                        </td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp; Add New Record </strong>
                    </td>
                    <td style="width: 17px">
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" />
                        </td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        &nbsp;</td>
                        <td align="left">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <br />

       <asp:Panel ID="Panel1" runat="server" Width="100%">
       
                <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                    <tr>
                                        <td align="left">
                                            Code
                                            <asp:CompareValidator ID="cvcode" runat="server" ValueToCompare="--Select--" Operator="NotEqual"
                                                ControlToValidate="ddlcode" Display="Dynamic" ErrorMessage="Please Select ID !"
                                                ValidationGroup="Val1"></asp:CompareValidator> 
                                            </td>
                                    <td align="left">
                                    <asp:Label ID="lblerror" runat="server" Visible="false"></asp:Label>
                                    <asp:DropDownList ID="ddlcode" runat="server" CssClass="clsComboBox" Width="97%">
                                    <asp:ListItem Text="--Select--" Value="0" Selected="true"></asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                    </tr>
                                                 
                              
                            <tr>
                                <td align="left">
                                    EmployeeID
                                   <asp:CompareValidator ID="cvemp" runat="server" ValueToCompare="--Select--" Operator="NotEqual"
                                                ControlToValidate="ddlEmpID_AATTAG" Display="Dynamic" ErrorMessage="Please Select ID !"
                                                ValidationGroup="Val1"></asp:CompareValidator> 
                                    
                                    </td>
                                <td align="left">
                                    &nbsp;
                                    <asp:DropDownList ID="ddlEmpID_AATTAG" runat="server" CssClass="clsComboBox" Width="97%">
                                        <asp:ListItem Text="---SELECT---" Value="0" Selected="True"></asp:ListItem>
                                    </asp:DropDownList></td>
                               
                            </tr>
                            
                            <tr>
                                <td align="left">
                                    AssetTag Status
                                </td>
                                <td align="left">
                                    <asp:RadioButtonList ID="rdbtnStatus_AATTAG" runat="server" CssClass="clsRadioButton" Width="97%"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Value="0"> NOT TAGGED</asp:ListItem>
                                        <asp:ListItem Value="1" Selected="True"> TAGGED</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                               
                            </tr>
                            
                            
                            <tr>
                                
                                <td colspan="3" align="center">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" Width="76px" Text="Submit" />
                                <asp:Button ID="btnBack" runat="Server" CssClass="button" Width="70px" Text="Back" />
                                </td>
                                
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage" 
                                        ></asp:Label>
                                </td>
                            </tr>
                        </table>
                    <center>
                    &nbsp;
                    </center>
                    </asp:Panel>
                </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                            &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
