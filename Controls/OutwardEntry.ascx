<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OutwardEntry.ascx.vb"
    Inherits="Controls_OutwardEntry" %>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvReqIds" runat="server" EmptyDataText="No Outward Entry found." AllowPaging="true"
            PageSize="10" CssClass="table GridStyle" GridLines="none" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Request Id" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblReqId" runat="server" Text='<%#Eval("MMR_REQ_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Request Date" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblDATE" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                        <asp:Label ID="lblstat" runat="server" Visible="false" Text='<%#Eval("STAT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("MMR_REQ_ID") %>' CommandName="Details">Details/Approve</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
