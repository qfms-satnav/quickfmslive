Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_MaintenanceDesk
    Inherits System.Web.UI.UserControl
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim orgfilename As String = ""
        Dim filedatetime As String = ""
        If (fuBrowseFile.HasFile) Then
            orgfilename = fuBrowseFile.FileName
            filedatetime = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & orgfilename
            Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filedatetime
            fuBrowseFile.PostedFile.SaveAs(filePath)
        End If
        insertnewrecord(filedatetime)
    End Sub
    Public Sub insertnewrecord(ByVal filedatetime As String)
        Dim MaintenanceReq As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMdd") + "/" + Session("uid")
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MAINTENANCE_DESK")
        sp3.Command.AddParameter("@PN_MAINTENANCE_REQ", MaintenanceReq, DbType.String)
        sp3.Command.AddParameter("@PN_BLDG_CODE", ddlProperty.SelectedItem.Value, DbType.String)
        sp3.Command.AddParameter("@PN_LOC_ID", ddlCity.SelectedItem.Value, DbType.String)
        sp3.Command.AddParameter("@PN_TENANT_ID", ddlTenant.SelectedItem.Value, DbType.String)
        sp3.Command.AddParameter("@REQUEST_TYPE", ddlReq.SelectedItem.Value, DbType.String)
        sp3.Command.AddParameter("@REQUEST_TITLE", txtProblemTitle.Text, DbType.String)
        sp3.Command.AddParameter("@REQUEST_DESCRIPTION", txtProbDesc.Text, DbType.String)
        sp3.Command.AddParameter("@REQUEST_DOC", filedatetime, DbType.String)
        sp3.Command.AddParameter("@REQUEST_RAISED_BY", Session("UID"), DbType.String)
        sp3.Command.AddParameter("@REQUEST_REMARKS", txtRemarks.Text, DbType.String)
        sp3.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=18")
        Cleardata()
    End Sub
    Public Sub Cleardata()
        'ddlBuilding.SelectedValue = 0
        ddlProperty.SelectedValue = 0
        ddlTenant.SelectedValue = 0
        ddlReq.SelectedIndex = -1
        txtProblemTitle.Text = ""
        txtProbDesc.Text = ""
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            BindCity()
            BindRequestType()
        End If
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", "0"))
    End Sub
    Private Sub BindRequestType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MAINTENANCE_REQUEST_TYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlReq.DataSource = sp.GetDataSet()
        ddlReq.DataTextField = "REQUEST_TYPE"
        ddlReq.DataValueField = "SNO"
        ddlReq.DataBind()
        ddlReq.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Protected Sub ddlProperty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProperty.SelectedIndexChanged
        If ddlProperty.SelectedIndex > 0 Then
            BindTenant()     
        Else
            ddlTenant.Items.Clear()       
        End If
    End Sub
    Private Sub BindTenant()
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_TenantProperty")
        sp2.Command.AddParameter("@BDG_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp2.Command.AddParameter("@PN_NAME", ddlProperty.SelectedItem.Value, DbType.String)
        ddlTenant.DataSource = sp2.GetDataSet()
        ddlTenant.DataTextField = "TEN_NAME"
        ddlTenant.DataValueField = "TEN_CODE"
        ddlTenant.DataBind()
        'ddlTenant.Items.Insert(0, New ListItem("--Select Tenant--", "0"))
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            BindProp()
        Else
            ddlProperty.Items.Clear()
            ddlProperty.Items.Insert(0, New ListItem("--Select Property--", "0"))
            ddlProperty.SelectedIndex = 0

            ddlTenant.Items.Clear()
            'ddlTenant.Items.Insert(0, New ListItem("--Select Tenant--", "0"))
            'ddlTenant.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindProp()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETPROPCTY")
        sp.Command.AddParameter("@city", ddlCity.SelectedItem.Value, DbType.String)
        ddlProperty.DataSource = sp.GetDataSet()
        ddlProperty.DataTextField = "PN_NAME"
        ddlProperty.DataValueField = "BDG_ID"
        ddlProperty.DataBind()
        ddlProperty.Items.Insert(0, New ListItem("--Select Property--", "0"))
    End Sub
End Class
