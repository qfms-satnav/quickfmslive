Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common

Imports clsSubSonicCommonFunctions
Imports System.Text

Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports System.io


Partial Class Controls_AssetLocation_Details
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")

                BindCategories()
                'pnlItems.Visible = False
            End If
        End If
    End Sub

    Private Sub BindCategories()
        GetChildRows("0")
        ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub GetChildRows(ByVal i As String)
        Dim str As String = ""
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If


        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        'param(0).Value = Session("uid")
        'param(1) = New SqlParameter("@PARENTID", SqlDbType.NVarChar, 200)
        'param(1).Value = id
        'ds = objsubsonic.GetSubSonicDataSet("GETASTCATEGORIES", param)

        param(0) = New SqlParameter("@PARENTID", SqlDbType.NVarChar, 200)
        param(0).Value = id
        ds = ObjSubsonic.GetSubSonicDataSet("GETASTCATEGORIES_RPT", param)


        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim j As Integer = CType(dr("CategoryId"), Integer)
                If id = 0 Then
                    str = ""
                End If
                Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
                ddlAstCat.Items.Add(li)
                GetChildRows(str & "~" & j)
            Next
        End If
    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        If ddlAstCat.SelectedIndex > 0 Then
            Session("Flag") = 1
            BindAstList()
            pnlItems.Visible = True
        Else
            pnlItems.Visible = False
            'txtRemarks.Text = ""
            lblMsg.Visible = False
        End If
    End Sub


    Private Sub BindAstList()
        Dim CatId As Integer = 0
        Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@categoryId", SqlDbType.Int)
        param(0).Value = CatId
        ObjSubsonic.Binddropdown(ddlAsts, "GET_ASTCOUNT_BYCATEGORY", "PRODUCTNAME", "PRODUCTID", param)
    End Sub

    Protected Sub ddlAsts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        BindAstGrid()
    End Sub

    Private Sub BindAstGrid()

        Dim ProductId As Integer = 0
        Integer.TryParse(ddlAsts.SelectedItem.Value, ProductId)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = ProductId

        ObjSubsonic.BindGridView(gvItems, "GET_All_ASTSLIST_PRODUCT", param)

        If gvItems.Rows.Count > 0 Then
            btnExportToExcel.Visible = True
        Else
            btnExportToExcel.Visible = False
        End If

    End Sub
    '

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging

        gvItems.PageIndex = e.NewPageIndex
        BindAstGrid()

    End Sub


    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ExportToExcelConsildateReport()
    End Sub



    Private Sub ExportToExcelConsildateReport()

        Dim ProductId As Integer = 0
        Integer.TryParse(ddlAsts.SelectedItem.Value, ProductId)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = ProductId

      

        Dim dsConsolidateReport As New DataSet
        dsConsolidateReport = ObjSubsonic.GetSubSonicDataSet("GET_All_ASTSLIST_PRODUCT", param)

        dsConsolidateReport.Tables(0).Columns(0).ColumnName = "Sol Id"
        dsConsolidateReport.Tables(0).Columns(1).ColumnName = "Location"
        dsConsolidateReport.Tables(0).Columns(2).ColumnName = "Asset Name"
        dsConsolidateReport.Tables(0).Columns(4).ColumnName = "Asset Count"
        dsConsolidateReport.Tables(0).Columns.RemoveAt(3)
        Dim gv As New GridView
        gv.DataSource = dsConsolidateReport
        gv.DataBind()
        Export("AssetLocationWiseReport.xls", gv)

    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

End Class
