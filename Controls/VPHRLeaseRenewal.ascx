<%@ Control Language="VB" AutoEventWireup="false" CodeFile="VPHRLeaseRenewal.ascx.vb" Inherits="Controls_VPHRLeaseRenewal" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row" id="Tr1" runat="server" visible="false">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Lease Type<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="form-control" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <div class="row">
                <label class="col-md-8 control-label">Search by Tenant Code/Property Name/Tenant Name<span style="color: red;">*</span></label>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5">
                    <asp:TextBox ID="txtempid" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="rfvTxtEmpId" runat="server" ControlToValidate="txtempid" Display="None" ErrorMessage="Please Enter Tenant Code/Property Name/Tenant Name"
                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-5">
                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color"  ValidationGroup="Val1" Text="Submit" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
            AllowPaging="True" PageSize="5" EmptyDataText="No Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
            Style="font-size: 12px;">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>

                <asp:TemplateField HeaderText="Lease">
                    <ItemTemplate>
                        <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CTS Number">
                    <ItemTemplate>
                        <asp:Label ID="lblCode" runat="server" CssClass="clsLabel" Text='<%#Eval("CTS_NUMBER")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CITY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Employee No">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpNo" runat="server" CssClass="clsLabel" Text='<%#Eval("LESSE_ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Lessor">
                    <ItemTemplate>
                        <asp:Label ID="lblLesseName" runat="server" CssClass="clsLabel" Text='<%#Eval("LESSE_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Start Date">
                    <ItemTemplate>
                        <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_START_DATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Expiry Date">
                    <ItemTemplate>
                        <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_EXPIRY_DATE")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lease Status">
                    <ItemTemplate>
                        <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Created By">
                    <ItemTemplate>
                        <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <a href='VPHRApprovalLeaseDetails.aspx?id=<%# HttpUtility.UrlEncode(Eval("LEASE_NAME"))%>'>View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
