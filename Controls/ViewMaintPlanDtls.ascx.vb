Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_ViewMaintPlanDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim Req_ID As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Req_ID = Request.QueryString("REQID")
            FillGrid(Req_ID)
        End If
    End Sub

    Private Sub FillGrid(ByVal MReq_ID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = MReq_ID
        ' ObjSubsonic.BindGridView(gv_MainSch, "FAM_PMASSETDETAILS_SP", param)
        ObjSubsonic.BindGridViewGET_PLAN_DETAILS(gv_MainSch, "GET_PLAN_DETAILS", param)
        If gv_MainSch.Rows.Count = 0 Then
            gv_MainSch.DataSource = Nothing
            gv_MainSch.DataBind()
        End If
    End Sub

    Protected Sub gv_MainSch_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gv_MainSch.PageIndexChanging
        gv_MainSch.PageIndex = e.NewPageIndex
        Req_ID = Request.QueryString("REQID")
        FillGrid(Req_ID)
    End Sub
End Class
