<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucAstLocationSummary.ascx.vb" Inherits="Controls_ucAstLocationSummary" %>
<table id="table1" cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="fornormaltext" Width="95%" Font-Underline="False"
                ForeColor="Black">Asset Location  
             <hr align="center" width="60%" /></asp:Label></td>
    </tr>
</table>
<table cellspacing="0" cellpadding="0" width="100%" class="fornormaltext">
    <tr class="bcolor">
        <td align="left">
            <span><strong>&nbsp;Asset Location </strong></span>
        </td>
    </tr>
    <tr>
        <td align="center">
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 100%" border="1">
                <tr>
                    <td colspan="4" align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <table id="AssetLocationForInnerDetails1_list">
                                    </table>
                                    <div id="AssetLocationForInnerDetails1_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table id="AssetLocationForInnerDetails2_list">
                                    </table>
                                    <div id="AssetLocationForInnerDetails2_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>