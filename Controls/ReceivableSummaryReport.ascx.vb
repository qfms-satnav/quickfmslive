Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization


Partial Class Controls_ReceivableSummaryReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtfindcode.Attributes.Add("onClick", "displayDatePicker('" + txtfindcode.ClientID + "')")
        'txtfindcode.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txttoDate.Attributes.Add("onClick", "displayDatePicker('" + txttoDate.ClientID + "')")
        'txttoDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'lblMRentPaid.Visible = False
        'lblMRentPaid.Visible = False
        lblMsg.Visible = False
        btnexporttoexcel.Visible = False
        If Not IsPostBack Then
            txtfindcode.Text = getoffsetdatetime(DateTime.Now).AddYears(-1).Date
            txttoDate.Text = getoffsetdatetime(DateTime.Now).Date
            fillgrid()
        End If
        txtfindcode.Attributes.Add("readonly", "readonly")
        txttoDate.Attributes.Add("readonly", "readonly")
    End Sub
    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
        If CDate(txtfindcode.Text) > CDate(txttoDate.Text) Then
            lblMsg.Visible = True
            lblMsg.Text = "From Date is greater than To Date, Please Select another From Date"
            gvLDetails_PybleSmrRpt.Visible = False
        Else
            fillgrid()
        End If
    End Sub

    Protected Sub btnexporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexporttoexcel.Click
        Export("Receivable_Summary_Report" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".xls", gvLDetails_PybleSmrRpt)
    End Sub

    
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub gvLDetails_PybleSmrRpt_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvLDetails_PybleSmrRpt.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub
    Private Sub fillgrid()

        gvLDetails_PybleSmrRpt.Visible = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_Receivable_Summary_Report")
        sp.Command.AddParameter("@FromDate", txtfindcode.Text, DbType.String)
        sp.Command.AddParameter("@ToDate", txttoDate.Text, DbType.String)
        sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)

        Dim rds As New ReportDataSource()
        rds.Name = "RentReportDS"
        rds.Value = sp.GetDataSet().Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/RentReport.rdlc")
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        'gvLDetails_PybleSmrRpt.DataSource = sp.GetDataSet()
        'gvLDetails_PybleSmrRpt.DataBind()
        If gvLDetails_PybleSmrRpt.Rows.Count > 0 Then
            btnexporttoexcel.Visible = True
        Else
            btnexporttoexcel.Visible = False
        End If

        TOTALRO()
    End Sub
    Private Sub TOTALRO()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "gettotalro")
        sp.Command.AddParameter("@FromDate", txtfindcode.Text, DbType.String)
        sp.Command.AddParameter("@ToDate", txttoDate.Text, DbType.String)
        sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'lblMRentPaid.Visible = True
            'lblMOutStndng.Visible = True
            'lblMRentPaid.Text = "Total Rent Paid=    " & CDbl(ds.Tables(0).Rows(0).Item("RentPaid")).ToString("C2")
            propertyrentpaid = CDbl(ds.Tables(0).Rows(0).Item("RentPaid")).ToString("C2")
            totalrentpaid = CDbl(ds.Tables(0).Rows(0).Item("Outstanding")).ToString("C2")
        End If
    End Sub

    Private propertyrentpaid As String
    Public Property Rentpaid() As String
        Get
            Return propertyrentpaid
        End Get
        Set(ByVal value As String)
            propertyrentpaid = value
        End Set
    End Property
    Private totalrentpaid As String
    Public Property Outstandingamt() As String
        Get
            Return totalrentpaid
        End Get
        Set(ByVal value As String)
            totalrentpaid = value
        End Set
    End Property



End Class
