<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Query.ascx.vb" Inherits="Controls_Query" %>
<link href="../../Scripts/styWSP.css" type="text/css" rel="stylesheet"/>
		<link href="../../Scripts/styAMTamantra.css" type="text/css" rel="stylesheet"/>
		<table class="TABLE" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr align="left">
					<td bgcolor="#3e86cb">&nbsp;&nbsp;&nbsp;</td>
					<td width="700" bgcolor="#3e86cb" height="15"><b><span style="COLOR: white">Query Analyzer</span></b></td>
				</tr>
			</table>
			<table cellspacing="0" cellpadding="2" align="center" border="0">
				<tr>
					<td>Field</td>
					<td align="center"><asp:dropdownlist id="drpdwnCategory" runat="server" CssClass="clsComboBox" AutoPostBack="True" Height="20px"
							Width="150px"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td>Operator</td>
					<td align="center"><asp:dropdownlist id="drpdwnOpt" runat="server" CssClass="clsComboBox" AutoPostBack="True" Height="20px"
							Width="150px">
							<asp:ListItem Value="=" Selected="True">=</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td style="HEIGHT: 15px">Value</td>
					<td style="HEIGHT: 15px" align="center"><asp:dropdownlist id="drpdwnValue" runat="server" CssClass="clsComboBox" AutoPostBack="True" Height="20px"
							Width="150px"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><asp:imagebutton id="ImgFetch" runat="server" ImageUrl="~/WorkSpace/GIS/Images/Search.jpg"></asp:imagebutton>&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:imagebutton id="ImgZoom" runat="server" ImageUrl="~/WorkSpace/GIS/Images/ZOOM.JPG"></asp:imagebutton></td>
				</tr>
			</table>