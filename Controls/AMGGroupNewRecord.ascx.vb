Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGGroupNewRecord
    Inherits System.Web.UI.UserControl

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/MASTERS/MAS_Webfiles/frmMASMasters.aspx")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim ValidateCode As Integer
        ValidateCode = ValidateGroupCode()
        If ValidateCode = 0 Then
            lblMsg.Text = "Code already exists please select another code"
        ElseIf ValidateCode = 1 Then
            insertnewrecord()
        End If
        
    End Sub
    Public Function ValidateGroupCode()
        Dim ValidateCode As Integer
        Dim AAG_CODE As String = txtCode.Text
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_GROUP_CODE")
        sp5.Command.AddParameter("@AAG_CODE", AAG_CODE, DbType.String)
        ValidateCode = sp5.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_AMG_GROUP")

            sp1.Command.AddParameter("@AAG_CODE", txtCode.Text, DbType.String)
            sp1.Command.AddParameter("@AAG_NAME", txtName.Text, DbType.String)
            sp1.Command.AddParameter("@AAG_STA_ID", rdbtnGroupID.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AAG_UPT_BY", Session("Uid"), DbType.String)

            sp1.Command.AddParameter("@AAG_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@AAG_EXH_CODE", txtEXHCode.Text, DbType.String)
            sp1.Command.AddParameter("@AAG_MFType", txtMFType.Text, DbType.String)

            sp1.ExecuteScalar()
            lblMsg.Text = "New Group Added"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub Cleardata()
        txtCode.Text = ""
        txtName.Text = ""
        txtRemarks.Text = ""
        txtEXHCode.Text = ""
        txtMFType.Text = ""
    End Sub
End Class
