<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RMApprovalDetailsl.ascx.vb"
    Inherits="Controls_RMApprovalDetailsl" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">                  
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <%-- <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val1" />--%>
   <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>--%>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Requisition Id</label>
                    <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Raised By</label>
                    <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Category</label>
                    <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Sub Category</label>
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Asset Brand/Make</label>
                        <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Asset Model </label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                            Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                        <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Required Quantity</label>
                       <%-- <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>--%>
                         <asp:TextBox ID="txtQty" runat="server" cssclass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Approved Quantity</label>
                       <%-- <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtaprvqty"
                            Display="none" ErrorMessage="Please Enter Approved Quantity" ValidationGroup="Val1" ></asp:RequiredFieldValidator>
                         <asp:TextBox ID="txtaprvqty" runat="server" cssclass="form-control" ></asp:TextBox>
                    </div>
                </div>
                
            </div>
            <div id="pnlItems" runat="server">
                
               <%-- <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">

                            <div class="panel-heading" style="height: 41px;">
                                <h3 class="panel-title"><b>Assets List</b> </h3>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <div class="row" style="margin-top: 10px" id="grid1" runat="server">
                    <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                        <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                            EmptyDataText="No request(s) found." CssClass="table GridStyle" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="AST_MD_id" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" Visible="false" />
                                <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left" />

                                <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_QTY")%>'></asp:TextBox>
                                        <asp:Label ID="lblProductid" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Estimated Cost" ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEstCost" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_EST_COST")%>'></asp:TextBox>
                                        <asp:Label ID="srno" Text='<%#Eval("srno") %>' runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_vt_code" Text='<%#Eval("VT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_ast_subcat_code" Text='<%#Eval("AST_SUBCAT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_manufactuer_code" Text='<%#Eval("manufactuer_code") %>' runat="server" Visible="false"></asp:Label>
                                        <asp:Label ID="lbl_ast_md_code" Text='<%#Eval("AST_MD_CODE") %>' runat="server" Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReq_status" runat="server" Visible="false" Text='<%# Eval("AIR_STA_ID") %>'></asp:Label>
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%#Bind("ticked") %>'></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Approved Cost</label>
                        <asp:TextBox ID="txtaprvcost" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Status</label>
                        <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>

                <div id="tr3" runat="Server" class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Requestor Remarks</label>
                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                    </div>
                </div>
                <div id="tr1" runat="Server" class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Level 1 Approval Remarks<span style="color: red;">*</span></label>

                        <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRMRemarks"
                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"
                            Enabled="true"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group">
                        <%--<strong>Documents</strong>--%>
                        <label class="col-md-12 control-label"> Uploaded Documents</label>
                        <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                        </asp:Label>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <div id="tblGridDocs" runat="server">
                            <asp:DataGrid ID="grdDocs" runat="server" CssClass="table GridStyle" GridLines="None" DataKeyField="ID"
                                EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="AMG_FILEPATH" HeaderText="Document Name">
                                        <HeaderStyle></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AMG_CREATED_DT" HeaderText="Document Date">
                                        <HeaderStyle></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn Text="Download" CommandName="Download">
                                        <HeaderStyle></HeaderStyle>
                                    </asp:ButtonColumn>

                                </Columns>
                                <HeaderStyle ForeColor="white" BackColor="Black" />
                                <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <asp:Button ID="btnApprove" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                        <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"/>
                        <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
                    </div>
                </div>
            </div>
       <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
    <script type="text/ecmascript">
        function refreshSelectpicker() {
            $("#<%=ddlAstCat.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddlAstModel.ClientID%>").selectpicker();
        $("#<%=ddlEmp.ClientID%>").selectpicker();

        };
        refreshSelectpicker();
    </script>
