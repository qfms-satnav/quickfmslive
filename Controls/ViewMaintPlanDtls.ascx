<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewMaintPlanDtls.ascx.vb"
    Inherits="Controls_ViewMaintPlanDtls" %>

<script src="<%=Page.ResolveUrl("~/Scripts/wz_tooltip.js")%>" type="text/javascript"
    language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">View Maintenance Plan Details
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>View Maintenance Plan Details</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background='<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>'>
                &nbsp;</td>
            <td align="left">
                <table width="100%" cellpadding="2px">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:GridView ID="gv_MainSch" runat="server" EmptyDataText="No Requests Found." AllowPaging="true"
                                PageSize="5" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="PVD_ID" HeaderText="S.No."></asp:BoundField>
                                    <asp:BoundField DataField="AAT_CODE" HeaderText="Asset"></asp:BoundField>
                                    <asp:BoundField DataField="AVR_NAME" HeaderText="Vendor"></asp:BoundField>
                                    <asp:BoundField DataField="PVD_PLANSCHD_DT" HeaderText="Schedule Date" DataFormatString="{0:d}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="STA_TITLE" HeaderText="Status"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</div>
