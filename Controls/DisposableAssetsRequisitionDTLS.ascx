<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DisposableAssetsRequisitionDTLS.ascx.vb"
    Inherits="Controls_DisposableAssetsRequisitionDTLS" %>


<div id="SurReqDet" runat="server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />
            <asp:GridView ID="gvDispReqDetails" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>

                    <asp:TemplateField HeaderText="Asset code" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("AAT_AST_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset ID">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lblModel" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Model Name" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblModelName" runat="server" Text='<%#Eval("AAT_MODEL_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" Visible="false" runat="server" Text='<%#Eval("LCM_CODE")%>'></asp:Label>
                            <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset  Date" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAstDate" runat="server" Text='<%#Eval("AAT_UPT_DT")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Cost" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblActualcost" runat="server" Text='<%#Eval("AST_ACTUAL_COST")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Depreciated(%)" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDepr" runat="server" Text='<%#Eval("AST_DEPR_PERCENTAGE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Depreciation Cost(Till Date)" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblDeprcost" runat="server" Text='<%#Eval("AST_DEPR_COST")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current Value" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblCurval" runat="server" Text='<%#Eval("AST_CURRENT_VALUE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Allocated Date">
                        <ItemTemplate>
                            <asp:Label ID="lblAstAllocDt" runat="server" Text='<%#Eval("AAT_ALLOCATED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Surrender Date">
                        <ItemTemplate>
                            <asp:Label ID="lblAstSurDt" runat="server" Text='<%#Eval("AAT_SURRENDERED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Surrender Requesition Id">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblSurReq_id" runat="server" Text='<%#Eval("SREQ_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Admin Approval Date" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblSREQ_RMAPPROVAL_DATE" runat="server" Text='<%#Eval("SREQ_RMAPPROVAL_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approved By" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblRM_NAME" runat="server" Text='<%#Eval("ADMIN_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Satus" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("STA_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Disposal Document">
                        <%--<ItemTemplate>
                            <asp:ImageButton ID="lblDisposal" runat="server" Text='<%#Eval("AST_UPLOAD_DOC")%>' ImageUrl="~/Images/download.jpg" />
                        </ItemTemplate>--%>

                        <ItemTemplate>

                            <asp:LinkButton ID="lblDisposal" runat="server" Text='<%#Eval("AST_UPLOAD_DOC")%>' CommandName="Document"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>

        </div>
    </div>
    <br />
    <br />
</div>

<br />
<div class="row">

    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">

            <label class="col-md-12 control-label">Remarks:<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"
                            Enabled="true"></asp:RequiredFieldValidator>
            <div class="col-md-7">
                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnApprov" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
            <asp:Button ID="btnreject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
