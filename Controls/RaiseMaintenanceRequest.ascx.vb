Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_RaiseMaintenanceRequest
    Inherits System.Web.UI.UserControl

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim orgfilename As String = ""
        Dim filedatetime As String = ""
        If (fuBrowseFile.HasFile) Then
            orgfilename = fuBrowseFile.FileName
            filedatetime = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & orgfilename
            Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filedatetime
            fuBrowseFile.PostedFile.SaveAs(filePath)
        End If
        InsertRecord(filedatetime)
       
    End Sub
    Public Sub InsertRecord(ByVal filedatetime As String)

        Dim MaintenanceReq As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMdd") + "/" + Session("uid")
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MAINTENANCE_DESK1")
        sp3.Command.AddParameter("@PN_MAINTENANCE_REQ", MaintenanceReq, DbType.String)
        sp3.Command.AddParameter("@REQUEST_TYPE", ddlReq.SelectedItem.Value, DbType.String)
        sp3.Command.AddParameter("@REQUEST_TITLE", txtProblemTitle.Text, DbType.String)
        sp3.Command.AddParameter("@REQUEST_DESCRIPTION", txtProbDesc.Text, DbType.String)
        sp3.Command.AddParameter("@REQUEST_DOC", filedatetime, DbType.String)
        sp3.Command.AddParameter("REQUEST_RAISED_BY", Session("UID"), DbType.String)
        sp3.Command.AddParameter("@REQUEST_STATUS", "0", DbType.Int32)
        sp3.Command.AddParameter("@REQUEST_REMARKS", txtRemarks.Text, DbType.String)
        sp3.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=19")

        'lblMsg.Text = "Request Raised Succesfully"
        Cleardata()
        
    End Sub
    Public Sub Cleardata()
        ddlReq.SelectedIndex = -1
        txtProblemTitle.Text = ""
        txtProbDesc.Text = ""
        'ddlstatus.SelectedIndex = -1
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindRequestType()
        End If
    End Sub
    Private Sub BindRequestType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MAINTENANCE_REQUEST_TYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlReq.DataSource = sp.GetDataSet()
        ddlReq.DataTextField = "REQUEST_TYPE"
        ddlReq.DataValueField = "SNO"
        ddlReq.DataBind()
        ddlReq.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
End Class
