<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditInterMovementReq.ascx.vb"
    Inherits="Controls_EditInterMovementReq" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="red" CssClass="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />
<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="row">
                <label class="col-md-12 control-label">From Location:</label>
                <div class="col-md-12">
                    <asp:Label ID="lblfrmloc" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="row">
                <label class="col-md-12 control-label">Destination Location:</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlToloc" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div id="trremarks" runat="server">

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">Remarks:<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                        Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-12 control-label">Request Raised by:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblreqby" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="row" style="margin-top: 10px">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <fieldset>
            <legend>Inter Movement Requisitions </legend>
            <asp:GridView ID="gvInterMVMT" runat="server" EmptyDataText="No Inter Movement Requisition Found." AllowPaging="true"
                PageSize="10" CssClass="table GridStyle" GridLines="none" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Asset Id" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblastid" runat="server" Text='<%#Eval("AMVT_AST_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblastName" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("AMVT_REMARKS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </fieldset>
    </div>
</div>
<br />
<fieldset id="fldITApproval" runat="server" visible="false">
    <legend>IT Approval/Reject Summary</legend>
    <div id="tblITSummary" runat="server" visible="false">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">

                    <label class="col-md-12 control-label">Request Approved by:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblITApproval" runat="server"></asp:Label>

                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">

                    <label class="col-md-12 control-label">Remarks:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblITRemarks" runat="server"></asp:Label>


                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">

                    <label class="col-md-12 control-label">Date:</label>
                    <div class="col-md-7">
                        <asp:Label ID="lblITDate" runat="server"></asp:Label>

                    </div>

                </div>
            </div>
        </div>
    </div>
</fieldset>
<br />
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary custom-button-color"></asp:Button>
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
        </div>
    </div>
</div>
