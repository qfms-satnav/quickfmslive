<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TrvlChangeRequisiton.ascx.vb" Inherits="Controls_TrvlChangeRequisiton" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvCat" runat="server" ControlToValidate="ddlCategory"
                    Display="none" ErrorMessage="Please Select Category" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Document Type<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvDocType" runat="server" ControlToValidate="ddlDocType"
                    Display="none" ErrorMessage="Please Select Document Type" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDocType" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Documents Found."
            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
            AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="ID" Visible="FALSE">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Document Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("CATEGORY")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Document Type">
                    <ItemTemplate>
                        <asp:Label ID="lblDocumentType" runat="server" Text='<%#Eval("DOCTYPE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lbldoc" runat="server" Text='<%#Eval("DOC")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Document">
                    <ItemTemplate>
                        <asp:Label ID="lblViewFilename" Visible="false" runat="server" Text='<%#Eval("DOCTITLE")%>'>
                        </asp:Label>
                        <asp:HyperLink ID="hypbtnDocs" Visible="false" runat="server" Text='View Document'>
                        </asp:HyperLink>
                        <a target="_blank" href='<%=Request.ApplicationPath %>/UploadFiles/<%#Eval("DOC")%>'>
                            <%#Eval("DOCTITLE")%>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField Text="View Document" Visible="false" Target="_blank" DataNavigateUrlFields="RPT_ORG_FILENAME"
                    DataNavigateUrlFormatString='<%= Request.PhysicalApplicationPath %>/UploadFiles'></asp:HyperLinkField>
                <asp:ButtonField Text="DELETE" CommandName="DELETE" />
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>


