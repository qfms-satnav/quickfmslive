Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AssetNewRecord
    Inherits System.Web.UI.UserControl
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_GrpValues")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            drdGrp_Name.DataSource = ds
            drdGrp_Name.DataTextField = "AAG_NAME"
            drdGrp_Name.DataValueField = "AAG_cODE"
            drdGrp_Name.DataBind()
            drdGrp_Name.Items.Insert(0, New ListItem("--Select--", "0"))
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_VendorValues")
            sp1.Command.AddParameter("@dummy", 1, DbType.Int32)
            Dim ds1 As New DataSet
            ds1 = sp1.GetDataSet
            drdVendor_Name.DataSource = ds1
            drdVendor_Name.DataTextField = "AVR_NAME"
            drdVendor_Name.DataValueField = "AVR_CODE"
            drdVendor_Name.DataBind()
            drdVendor_Name.Items.Insert(0, New ListItem("--Select--", "0"))
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_Brandvalues")
            sp2.Command.AddParameter("@dummy", 1, DbType.Int32)
            Dim ds2 As New DataSet
            ds2 = sp2.GetDataSet
            drdBrand_Name.DataSource = ds2
            drdBrand_Name.DataTextField = "AAB_NAME"
            drdBrand_Name.DataValueField = "AAB_CODE"
            drdBrand_Name.DataBind()
            drdBrand_Name.Items.Insert(0, New ListItem("--Select--", "0"))

        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim ValidateCode As Integer
        ValidateCode = ValidateAssetCode()
        If ValidateCode = 0 Then
            lblMsg.Text = "Code already exists please select another code"
        ElseIf ValidateCode = 1 Then
            insertnewrecord()
        End If
    End Sub
    Public Function ValidateAssetCode()
        Dim ValidateCode As Integer
        Dim AAT_CODE As String = txtCode.Text
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_ASSET_CODE")
        sp5.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
        ValidateCode = sp5.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Dim AAT_CODE As String = txtCode.Text
        Dim AAT_NAME As String = txtName.Text
        Dim AAT_MODEL_NAME As String = txtModelName.Text
        Dim AAT_AAG_CODE As String = drdGrp_Name.SelectedItem.Value
        Dim AAT_AVR_CODE As String = drdVendor_Name.SelectedItem.Value
        Dim AAT_AAB_CODE As String = drdBrand_Name.SelectedItem.Value
        Dim AAT_AMC_REQD As Integer
        If (assetreq.SelectedItem.Value = True) Then
            AAT_AMC_REQD = 1
        Else
            AAT_AMC_REQD = 0
        End If
        Dim AAT_UPT_DT As Date = getoffsetdatetime(DateTime.Now).ToString()
        Dim AAT_UPT_BY As String = Session("Uid")
        Dim AAT_STA_ID As Integer
        If (assetid.SelectedItem.Value = True) Then
            AAT_STA_ID = 1
        Else
            AAT_STA_ID = 0

        End If

        Dim AAT_OWNED As Integer
        If (assetOwned.SelectedItem.Value = True) Then
            AAT_OWNED = 1
        Else
            AAT_OWNED = 0
        End If
        Dim AAT_PURCHASED_STATUS As Integer
        If (AssetPurchasedStatus.SelectedItem.Value = True) Then
            AAT_PURCHASED_STATUS = 1
        Else
            AAT_PURCHASED_STATUS = 0
        End If
        Dim AAT_SPC_FIXED As Integer
        If (rdbtnAssetSpace.SelectedItem.Value = True) Then
            AAT_SPC_FIXED = 1
        Else
            AAT_SPC_FIXED = 0
        End If

        Dim AAT_USR_MOVABLE As Integer


        If (rdbtnAssetUser.SelectedItem.Value = True) Then
            AAT_USR_MOVABLE = 1
        Else
            AAT_USR_MOVABLE = 0
        End If

        Dim AAT_AST_CONS As Integer
        If (rdbtnAssetCons.SelectedItem.Value = True) Then
            AAT_AST_CONS = 1
        Else
            AAT_AST_CONS = 0
        End If
        Dim AAT_DESC As String = txtRemarks.Text
        Try
            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Insert_AMG_ASSET")
            'sp4.Command.AddParameter("@AAT_ID", AAT_ID, Data.DbType.Int32)
            sp4.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_NAME", AAT_NAME, DbType.String)
            sp4.Command.AddParameter("@AAT_MODEL_NAME", AAT_MODEL_NAME, DbType.String)
            sp4.Command.AddParameter("@AAT_AAG_CODE", AAT_AAG_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_AVR_CODE", AAT_AVR_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_AAB_CODE", AAT_AAB_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_AMC_REQD", AAT_AMC_REQD, DbType.Int32)
            sp4.Command.AddParameter("@AAT_UPT_DT", AAT_UPT_DT, DbType.Date)
            sp4.Command.AddParameter("@AAT_UPT_BY", AAT_UPT_BY, DbType.String)
            sp4.Command.AddParameter("@AAT_STA_ID", AAT_STA_ID, DbType.Int32)
            sp4.Command.AddParameter("@AAT_OWNED", AAT_OWNED, DbType.Int32)
            sp4.Command.AddParameter("@AAT_PURCHASED_STATUS", AAT_PURCHASED_STATUS, DbType.Int32)
            sp4.Command.AddParameter("@AAT_SPC_FIXED", AAT_SPC_FIXED, DbType.Int32)
            sp4.Command.AddParameter("@AAT_USR_MOVABLE", AAT_USR_MOVABLE, DbType.Int32)
            sp4.Command.AddParameter("@AAT_AST_CONS", AAT_AST_CONS, DbType.Int32)
            sp4.Command.AddParameter("@AAT_DESC", AAT_DESC, DbType.String)
            sp4.ExecuteScalar()
            lblMsg.Text = "New Asset Added succesfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Public Sub Cleardata()
        txtCode.Text = ""
        txtName.Text = ""
        txtModelName.Text = ""
        drdGrp_Name.SelectedIndex = 0
        drdVendor_Name.SelectedIndex = 0
        drdBrand_Name.SelectedIndex = 0
        txtRemarks.Text = ""
    End Sub
End Class
