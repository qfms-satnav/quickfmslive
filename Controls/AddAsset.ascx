<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAsset.ascx.vb" Inherits="Controls_AddAsset" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Document(Only Excel)<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                    ControlToValidate="fpBrowseDoc" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" ControlToValidate="fpBrowseDoc"
                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                </asp:RegularExpressionValidator>
                <div class="col-md-6">
                    <div class="btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- <div class="col-md-1">
         <asp:Image ID="uploadExcel" runat="server" src="../../../Bootstrapcss/images/loading_1.gif" Style="width: 30px; height: 30px;display:none;" />
    </div>--%>
    <div class="col-md-5">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5  control-label">
                    <asp:Button ID="btnbrowse" runat="server" OnClientClick="LoadingPopUp()" CssClass="btn btn-primary custom-button-color" Text="Upload" Style="height: 32px" />
                </div>
                <div>
                    <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/CapitalAsset.xls"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 pull-right" style="overflow: auto;">
        <div class="row">
            <asp:GridView ID="GridView1" runat="server" CssClass="table GridStyle" GridLines="None">
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>

        </div>
    </div>
</div>
<hr />
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <script type="text/javascript" language="javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                if (args.get_error() != undefined) {
                    args.set_errorHandled(true);
                }
            }
        </script>
        <asp:ValidationSummary ID="VerticalValidations" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
       
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label id="lblAsset" runat="server" visible="False">Asset</label>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlAsset"
                        ErrorMessage="Please Select the Asset" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                    <asp:DropDownList ID="ddlAsset" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--" AutoPostBack="True" Visible="False">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Category<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                        Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                        ToolTip="--Select--" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Sub Category<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlAstSubCat"
                        Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--">
                    </asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                        ToolTip="--Select--" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Brand/Make<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlAstBrand"
                        Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                        ToolTip="--Select--" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Model<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                        Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlModel" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                        ToolTip="--Select--"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>


        <div class="row">
            <%--<asp:UpdatePanel ID="CityPanel1" runat="server">
        <ContentTemplate>--%>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Location<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlLocation"
                        Display="none" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Tower<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlTower"
                        Display="none" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="--Select--">
                    </asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlTower" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                        ToolTip="--Select--" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Floor<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="ddlFloor"
                        Display="none" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select Floor" Style="height: 22px">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>Auto Generated Asset Id <span style="color: red;"></span></label>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAssetName"
                Display="none" ErrorMessage="Please Enter Asset Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                    <asp:TextBox ID="txtAssetName" runat="server" placeholder="AutoGenerated..." CssClass="form-control" ReadOnly="true" Width="102%"></asp:TextBox>
                </div>
            </div>
            <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
        </div>

        <div class="row">
            <%--<div class="col-md-3 col-sm-3 col-xs-3">
        <div class="form-group">
            <label>Asset Name </label>
            <div onmouseover="Tip('Enter Asset Name')" onmouseout="UnTip()">
                <asp:TextBox ID="txtAssetDescription" runat="server" CssClass="form-control" Height="30%" MaxLength="500" Width="184px"></asp:TextBox>
            </div>

        </div>
    </div>--%>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>Asset Id<span style="color: red;">*</span> </label>
                    <div onmouseover="Tip('Enter Asset Name')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtAssetDescription" runat="server" CssClass="form-control" Height="30%" MaxLength="500" Width="184px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtAssetDescription"
                            Display="none" ErrorMessage="Please Enter  Asset ID" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    </div>

                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Rate Contract(Y/N)<span style="color: red;"></span></label>
                    <asp:RadioButtonList ID="RdbRateContract" runat="server" AutoPostBack="True" Font-Bold="True" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="col-md-9 control-label">
                        <asp:ListItem Value="1" Selected="True">Yes &nbsp</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div id="Div7" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Vendor</label>
                    <asp:TextBox ID="TextBox12" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
            <div id="Div6" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Vendor<span style="color: red;">*</span></label>
                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--" Style="height: 22px" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlVendor" ErrorMessage="Please Select Vendor" ValidationGroup="Val1" InitialValue="--Select--" Display="None"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Price (In Cost)<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAssetPrice"
                        Display="none" ErrorMessage="Please Enter  Asset Price(In Cost)" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtAssetPrice"
                        ErrorMessage="Please Enter Asset Price in numbers" Display="None" ValidationExpression="^[0-9.]+"
                        ValidationGroup="Val1">
                    </asp:RegularExpressionValidator>
                    <asp:TextBox ID="txtAssetPrice" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Count<span style="color: red;"></span></label>
                    <asp:TextBox ID="txtcount" runat="server" CssClass="form-control">1</asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Asset Serial No.<span style="color: red;"></span></label>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAssetSerialNo"
                Display="none" ErrorMessage="Please Enter Asset Serial No" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                    <asp:TextBox ID="txtAssetSerialNo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">

                <div class="form-group">
                    <label id="Label3" runat="server">Asset Type<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlStatus"
                        Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--">
                    </asp:DropDownList>
                </div>

                <%--<div class="form-group">
            <label id="Label3" runat="server">Asset Type<span style="color: red;"></span></label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlStatus"
                Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlAssetType" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--">
            </asp:DropDownList>
        </div>--%>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Life Span(In Years)</label>
                    <asp:TextBox ID="txtLifeSpan" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>PO Number</label>
                    <asp:TextBox ID="txtPONumber" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label id="Label1" runat="server">Warranty Date Up To<span style="color: red;"></span></label>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtWarDate"
                ErrorMessage="Please Enter Warranty. Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>
                    <%-- <asp:CompareValidator ID="CompareValidator1" runat="server"  ErrorMessage="Invalid Date Format" ControlToCompare ="txtMfgDate" ControlToValidate="txtWarDate" Type="Date" Operator="GreaterThanEqual" ></asp:CompareValidator>--%>
                    <div class='input-group date' id='Div4'>
                        <asp:TextBox ID="txtWarDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label id="Label2" runat="server">Purchase Date</label><br />
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtPDate"
                ErrorMessage="Please Enter Purchase Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtPDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label id="lblMfgDate" runat="server">Mfg. Date<span style="color: red;"></span></label>
                    <%--<asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="txtMfgDate"
                ErrorMessage="Please Enter Mfg. Date" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>
                    <%--<asp:CompareValidator ID="CV1" runat="server" ErrorMessage="Mfg Date must be greater than purchase Date" ControlToValidate="txtMfgDate"  ControlToCompare="txtPDate" Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>--%>

                    <div class='input-group date' id='Div1'>
                        <asp:TextBox ID="txtMfgDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                        </span>
                    </div>
                </div>

            </div>

        </div>
        <div class="row">

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>&nbsp;&nbsp;&nbsp;Depreciation(in %)</label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAssetPrice"
                        ErrorMessage="Please Enter Depriciation in numbers" Display="None" ValidationExpression="^[0-9.]+" ValidationGroup="Val1"> 
                    </asp:RegularExpressionValidator>
                    <asp:TextBox ID="txtDepriciation" runat="server" CssClass="form-control">0</asp:TextBox>
                </div>
            </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <%-- <label><span style="color: red;">*</span></label>--%>
                    <asp:Label ID="lblProcess" runat="server" Text=""><span style="color: red;"></span></asp:Label>
                    <%--<asp:RequiredFieldValidator ID="rfval" runat="server" ControlToValidate="ddlDepartment"
                Display="none" ErrorMessage="Please Select CostCenter" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label id="Label6" runat="server">Status</label>
                    <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                        Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select Status">
                        <%--<asp:ListItem>--Select--</asp:ListItem>--%>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label id="Label7" runat="server">Asset For</label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlCompany"
                        Display="none" ErrorMessage="Please Select Asset For" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select Company"></asp:DropDownList>
                </div>
            </div>
            <%-- <div id="divsalvag" runat="server" class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Salvage Value (In Cost)<span style="color: red;"></span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSalvgValue"
                Display="none" ErrorMessage="Please Enter  Salvage Value" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtSalvgValue"
                    ErrorMessage="Please Enter Salvage Value in numbers" Display="None" ValidationExpression="^[0-9.]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div onmouseover="Tip('Enter Salvage Value in numbers')" onmouseout="UnTip()">
                    <asp:TextBox ID="txtSalvgValue" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>--%>
        </div>



        <div class="row">

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>&nbsp;&nbsp;&nbsp;Invoice Number</label>
                    <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TxtBox_INVOICE"
                ErrorMessage="Please Enter Invoice Number" Display="None"  ValidationGroup="Val1"> 
            </asp:RegularExpressionValidator>--%>
                    <asp:TextBox ID="TxtBox_INVOICE" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group" id="Div5" runat="server">
                    <label>Upload Asset Image<span style="color: red;"></span></label>
                    <%-- <asp:RequiredFieldValidator ID="rfvimage" runat="server" ControlToValidate="fpAssetImage"
                Display="none" ErrorMessage="Please Choose Image" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" Display="None" ControlToValidate="fpAssetImage"
                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpeg,jpg,gif,tif files allowed"
                        ValidationExpression="^.+\.(([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                         
                    </asp:RegularExpressionValidator>
                    <div class="btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpAssetImage" runat="Server" Width="90%" />
                    </div>
                    <asp:LinkButton ID="LnkAstImg" runat="server" Visible="false"></asp:LinkButton>
                    <asp:Label ID="lblAstImg" runat="server" Text="" Visible="false" class="col-md-5 control-label"></asp:Label>
                    <asp:Literal ID="LtlAstImg" Text="" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Description <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvdes" runat="server" ControlToValidate="txt_description"
                        ErrorMessage="Please Enter Description" Display="None" ValidationGroup="Val1">
                    </asp:RequiredFieldValidator>
                    <asp:TextBox ID="txt_description" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
        <br />
        <div class="row clearfix">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>&nbsp;&nbsp;&nbsp; AMC Included(Y/N)<span style="color: red;"></span></label>
                    <asp:RadioButtonList ID="rdAMC" runat="server" AutoPostBack="True" Font-Bold="True" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="col-md-9 control-label">
                        <asp:ListItem Value="1" Selected="True">Yes &nbsp</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group" id="rbnAmcDate" runat="server">
                    <label>AMC Date Up To <span style="color: red;"></span></label>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtAMCDate"
                ErrorMessage="Please Enter AMC Date Up To" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>
                    <div class='input-group date' id='Div2'>
                        <asp:TextBox ID="txtAMCDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('Div2')"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group" id="rbnAmcUpload" runat="server">
                    <label>Upload AMC Document(s)<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseAMCDoc"
                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                        ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                    </asp:RegularExpressionValidator>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="fpBrowseAMCDoc" ValidationGroup="Val1" ErrorMessage="Please Upload AMC Documents..." SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                    <div class="btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseAMCDoc" runat="Server" onchange="showselectedfiles(this)" Width="90%" />
                    </div>
                    <asp:LinkButton ID="hplAMC" runat="server" Visible="false"></asp:LinkButton>
                    <asp:Label ID="lblhplAMC" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:Literal ID="ltrAMC" Text="" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group" id="rbn2" runat="server">
                    <label>&nbsp;&nbsp;&nbsp; Insurance Included(Y/N)<span style="color: red;"></span></label>
                    <asp:RadioButtonList ID="rdIns" runat="server" CssClass="col-md-9 control-label" AutoPostBack="True"
                        Font-Bold="True" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Value="1" Selected="True">Yes &nbsp</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group" id="rbnInsDate" runat="server">
                    <label id="Label4" runat="server">Insurance Date Up To<span style="color: red;"></span></label>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtIncDate"
                ErrorMessage="Please Enter Insurance Date Up To" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>--%>
                    <div class='input-group date' id='Div3'>
                        <asp:TextBox ID="txtIncDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('Div3')"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group" id="rbnInsUpload" runat="server">
                    <label>Upload Insurance Document(s)<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" ControlToValidate="fpBrowseIncDoc"
                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                        ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$"> 
                    </asp:RegularExpressionValidator>
                    <%-- <asp:RequiredFieldValidator ID="RFV1" runat="server" ControlToValidate="fpBrowseIncDoc"  ValidationGroup="Val1"  ErrorMessage="Please Upload Insurance Documents..." SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                    <div class="btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseIncDoc" runat="Server" onchange="show(this)" Width="90%" />
                    </div>
                    <div id="image-holder"></div>
                    <asp:LinkButton ID="hplINC" runat="server" Visible="false"></asp:LinkButton>
                    <asp:Label ID="lblINC" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:Literal ID="litINC" Text="" runat="server"></asp:Literal>
                </div>
            </div>

        </div>

        <%-- <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>&nbsp;&nbsp;&nbsp; Asset Name/Description </label>
                <div onmouseover="Tip('Enter Asset Name/Description')" onmouseout="UnTip()">
                    &nbsp;&nbsp;
                <asp:TextBox ID="txtAssetDescription" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="500" Width="184px"></asp:TextBox>
                </div>
            </div>
        </div>--%>
        <div class="panel panel-default " role="tab" id="div0">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#General">Add Spare Parts</a>
                </h4>
            </div>
            <div id="General" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Spare part(id)</label>
                                <asp:TextBox ID="txtSparePartId" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Spare part name</label>
                                <asp:TextBox ID="txtSparePartName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Quantity</label>
                                <asp:TextBox ID="txtQuantity" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Cost</label>
                                <asp:TextBox ID="txtspcost" runat="server" CssClass="form-control" ClientIDMode="Static" onchange="calc()"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Total Cost</label>
                                <asp:TextBox ID="txtsptcost" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="form-group" style="padding-top: 20px; width: 50px;">
                                <asp:Button ID="btnAdd" runat="server" Text="+" OnClick="btnAdd_Click" CssClass="btn btn-default btn-primary" />
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="GridviewDiv">
                            <%-- <asp:GridView runat="server" ID="gvDetails" ShowFooter="true" AllowPaging="true" PageSize="10" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDeleting="gvDetails_RowDeleting">
                        <HeaderStyle CssClass="headerstyle" />--%>
                            <asp:GridView ID="gvDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                                CssClass="table  GridStyle" GridLines="none" HorizontalAlign="Left" ShowHeaderWhenEmpty="true"
                                OnRowDeleting="gvDetails_RowDeleting" OnRowEditing="gvDetails_RowEditing" OnRowUpdating="gvDetails_RowUpdating" AutoGenerateEditButton="true" AutoGenerateDeleteButton="true">
                                <PagerSettings Mode="NumericFirstLast" />
                                <Columns>
                                    <asp:BoundField DataField="SparePartID" HeaderText="Spare Part ID" />
                                    <asp:BoundField DataField="SparePartName" HeaderText="Spare Part Name" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                                    <asp:BoundField DataField="Cost" HeaderText="Cost" />
                                    <asp:BoundField DataField="TotalCost" HeaderText="TotalCost" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
            <div class="form-group">
                <asp:Button ID="btnSubmit" OnClientClick="return ConfirmDelete();" CssClass="btn btn-default btn-primary" runat="server" Text="Add" ValidationGroup="Val1" />
                <%--<asp:Button ID="btnsparepart" CssClass="btn btn-default btn-primary" runat="server" Text="With Spare Parts" ValidationGroup="Val1" />--%>
                <asp:Button ID="btnBack" CssClass="btn btn-default btn-primary" runat="server" Text="Back" OnClick="btnBack_Click" CausesValidation="False" />

            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="pnlAst" runat="server" visible="false">Asset Code <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtAssetCode"
                    Display="none" ErrorMessage="Please Enter Asset Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtAssetCode" runat="server" CssClass="form-control" Enabled="false" Visible="false"></asp:TextBox>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 text-left">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3">
                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Search By Any..." CssClass="form-control"></asp:TextBox>

                        </div>
                        <div class="col-md-4">
                            <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search"
                                CausesValidation="true" TabIndex="2" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
            </div>
        </div>
        <div class="col-md-12">
            <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                CssClass="table GridStyle" GridLines="none" HorizontalAlign="Left">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField HeaderText="Asset UId" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAATID" runat="server" CssClass="lblAST ID" Text='<%#Eval("AAT_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Id">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("AST_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lbllocation" runat="server" Text='<%#Eval("LOCATION")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category">
                        <ItemTemplate>
                            <asp:Label ID="lblcategory" runat="server" Text='<%#Bind("AST_CATEGORY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sub Category">
                        <ItemTemplate>
                            <asp:Label ID="lblsubcat" runat="server" Text='<%#Bind("AST_SUBCAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Brand">
                        <ItemTemplate>
                            <asp:Label ID="lblbrand" runat="server" Text='<%#Bind("AST_BRAND")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Model Name">
                        <ItemTemplate>
                            <asp:Label ID="lblmodel" runat="server" Text='<%#Bind("MODEL_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Cost">
                        <ItemTemplate>
                            <%-- <asp:Label ID="lblcost" runat="server" Text='<%#Bind("COST")%>'></asp:Label>--%>
                            <asp:Label ID="lblcost" runat="server" Text='<%#Eval("COST", "{0:c2}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField HeaderText="Spare Parts Name">
                <ItemTemplate>
                    <asp:Label ID="lblSpname" runat="server" Text='<%#Bind("SPAREPART_NAME")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
                    <%--<asp:TemplateField HeaderText="Spare Parts Quantity">
                <ItemTemplate>
                    <%-- <asp:Label ID="lblcost" runat="server" Text='<%#Bind("COST")%>'></asp:Label>--%>
                    <%-- <asp:Label ID="lblSpQuantity" runat="server" Text='<%#Bind("SPAREPART_Quantity")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
                    <%-- <asp:TemplateField HeaderText="Asset Serial Number">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%#Bind("SERIAL_NO")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Purchase Date">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%#Bind("PURCHASEDATE")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Asset Type">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%#Bind("AST_TYPE")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vendor Name">
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%#Bind("AVR_NAME")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Added By">
                        <ItemTemplate>
                            <asp:Label ID="lblAddedBy" runat="server" Text='<%#Bind("AUR_KNOWN_AS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Edit"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
        <div class="modal fade" id="myModal" tabindex='-1'>
            <div class="modal-dialog modal-lg" style="height:200px;width:400px !important;">
                <div class="modal-content" style="margin-top:0px">
                    <div class="modal-body" id="modelcontainer4">
                        <div style="display:flex;align-items:center;justify-content:center;">
                        <img  src="../../../Bootstrapcss/images/loading_1.gif"  Style="width: 30px; height: 30px; margin:auto" />
                        </div>
                        <br />
                        <div style="display:flex;align-items:center;justify-content:center">Please wait while Excel is being uploaded.</div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnAATID" runat="server" />
        <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
        <script>

            function showselectedfiles(fu) {
                document.getElementById('placehere').innerHTML = "";
                //var filename=fu.files[0].name;
                //var extn = filename.split('.').pop();

                //if (extn == jpg) {


                var input = document.getElementById("<%= fpBrowseAMCDoc.ClientID%>");

                var fReader = new FileReader();
                fReader.readAsDataURL(input.files[0]);
                fReader.onloadend = function (event) {
                    var img = document.createElement("img");
                    img.id = "picture"
                    img.style.height = "50px";
                    img.style.width = "50px";
                    img.style.border = "1px solid yellow";
                    img.src = event.target.result;
                    document.getElementById("placehere").appendChild(img);
                }
            }

            function ConfirmDelete() {
                var ddlStatus1 = document.getElementById("<%=ddlStatus.ClientID%>");
                var getvalue = ddlStatus1.options[ddlStatus1.selectedIndex].value;

                if (getvalue == "0") {
                    return confirm("Please check if the asset have AMC and PPM plans");
                }
            }

            function LoadingPopUp() {
                $('#myModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#myModal").modal('show');
                return true
            }

            function calc() {
                console.log($('#txtQuantity').val());
                var textValue1 = $("#txtQuantity").val();
                var textValue2 = $("#txtspcost").val();
                var va = parseInt(textValue1) * parseInt(textValue2);
                $("#txtsptcost").val(va);
            }

    //$(document).ready(function () {
    //    $('#txtQuantity').keyup(calculate);
    //    $('#txtspcost').keyup(calculate);
    //});
    //function calculate(e) {
    //    $('#a3').val($('#a1').val() * $('#a2').val());
    //}



    //function show(fu) {
    //    document.getElementById('image-holder').innerHTML = "";

    // var input = document.getElementById("<%= fpBrowseIncDoc.ClientID%>");
            //    var fReader = new FileReader();
            //    fReader.readAsDataURL(input.files[0]);
            //    fReader.onloadend = function (event) {
            //        var img = document.createElement("img");
            //        img.id = "picture"
            //        img.style.height = "60px";
            //        img.style.width = "60px";
            //        img.style.border = "1px solid yellow";
            //        img.src = event.target.result;
            //        document.getElementById("image-holder").appendChild(img);        

            //    }

            //},,,,
            $(document).ready(function () {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    refreshSelectpicker();
                });

                function refreshSelectpicker() {
                    $("#<%=ddlLocation.ClientID%>").selectpicker();
                    $("#<%=ddlTower.ClientID%>").selectpicker();
                    $("#<%=ddlFloor.ClientID%>").selectpicker();
                    $("#<%=ddlStatus.ClientID%>").selectpicker();
                    $("#<%=ddlAssetType.ClientID%>").selectpicker();
                    $("#<%=ddlCompany.ClientID%>").selectpicker();
                    $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
                    $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
                    $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
                    $("#<%=ddlVendor.ClientID%>").selectpicker();
                    $("#<%=ddlModel.ClientID%>").selectpicker();
                    $("#<%=ddlDepartment.ClientID%>").selectpicker();
                    $("#<%=ddlAsset.ClientID%>").selectpicker();
                    $("#<%=ddlAstBrand.ClientID%>").selectpicker();
                    $("#<%=ddlStatus.ClientID%>").selectpicker();
                }
            });
        </script>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
    </Triggers>
</asp:UpdatePanel>
