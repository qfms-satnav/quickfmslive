Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptMasters
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            DEL_EXPENSE()
            txtJrDate.Text = getoffsetdate(Date.Today)
            btnsubmit.Visible = False
            btnModify.Visible = False
            lblTotalAmount.Visible = False
            lblTamount.Visible = False
            tr1.Visible = False
            tr2.visible = False
        End If
        txtJrDate.Attributes.Add("readonly", "readonly")
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            lblMsg.Text = ""
            Add_Temp_Domestic_Conveyance()
            BindGrid()
            If gvItems.Rows.Count > 0 Then
                btnsubmit.Visible = True
                lblTotalAmount.Visible = True
                lblTamount.Visible = True
            Else
                btnsubmit.Visible = False
                lblTotalAmount.Visible = False
                lblTamount.Visible = False
            End If
            ClearConveyance()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim validateexp As Integer
            'validateexp = ValidateExpn()
            'If validateexp = 0 Then
            '    lblMsg.Text = "You have already added this expense on this date"
            'Else

            ' End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Add_Temp_Domestic_Conveyance()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_EXPENSES_ADD")
        sp.Command.AddParameter("@JOURNEY_DATE", txtJrDate.Text, DbType.Date)
        sp.Command.AddParameter("@PURPOSE", txtPurpose.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@EXPENSE_MODE", ddlExpense.SelectedItem.Value, DbType.String)
        If ddlMode.SelectedIndex = 0 Then
            sp.Command.AddParameter("@TRAVEL_MODE", "", DbType.String)
        Else
            sp.Command.AddParameter("@TRAVEL_MODE", ddlMode.SelectedItem.Value, DbType.String)
        End If
        If txtKM.Text = "" Then
            txtKM.Text = 0
        End If
        sp.Command.AddParameter("@TRAVEL_KM", txtKM.Text, DbType.Decimal)
        sp.Command.AddParameter("@EXPENSE_AMOUNT", txtAmount.Text, DbType.Decimal)
        sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Function ValidateExpn()
        Dim validateexp As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_VALIDATE_CONVEYANCE_EXPENSE_MODE")
        sp.Command.AddParameter("@Expense_mode", ddlExpense.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@DATE", txtJrDate.Text, DbType.Date)
        validateexp = sp.ExecuteScalar()
        Return validateexp
    End Function
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_EXPENSES_GET")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            gvItems.DataSource = ds
            gvItems.DataBind()
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim lblTMode As Label = CType(gvItems.Rows(i).FindControl("lblTMode"), Label)
                If lblTMode.Text = "FLT" Then
                    lblTMode.Text = "Flight"
                ElseIf lblTMode.Text = "TRN" Then
                    lblTMode.Text = "Train"
                ElseIf lblTMode.Text = "BS" Then
                    lblTMode.Text = "Bus"
                ElseIf lblTMode.Text = "CR" Then
                    lblTMode.Text = "Car"
                ElseIf lblTMode.Text = "ATO" Then
                    lblTMode.Text = "AUTO"
                ElseIf lblTMode.Text = "BKE" Then
                    lblTMode.Text = "Bike"
                End If
                Dim lblMode As Label = CType(gvItems.Rows(i).FindControl("lblMode"), Label)
                If lblMode.Text = "TCKT" Then
                    lblMode.Text = "Tickets"
                ElseIf lblMode.Text = "CONV" Then
                    lblMode.Text = "Conveyance"
                ElseIf lblMode.Text = "DLAL" Then
                    lblMode.Text = "Daily Allowance"
                ElseIf lblMode.Text = "BL" Then
                    lblMode.Text = "Boarding & Lodging"
                ElseIf lblMode.Text = "PH" Then
                    lblMode.Text = "Phone"
                ElseIf lblMode.Text = "MSC" Then
                    lblMode.Text = "Miscellaneous"
                End If
            Next
            Total_Amount()
            If gvItems.Rows.Count > 0 Then
                lblTotalAmount.Visible = True
                lblTamount.Visible = True
            Else
                lblTotalAmount.Visible = False
                lblTamount.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Total_Amount()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_TOTAL_EXPENSE")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblTotalAmount.Visible = True
            lblTamount.Visible = True
            lblTotalAmount.Text = "Rs." & "" & ds.Tables(0).Rows(0).Item("Amount")
        End If

    End Sub
    Private Sub ClearConveyance()
        ddlExpense.SelectedIndex = 0
        tr2.Visible = False
        tr1.Visible = False
        txtAmount.Text = ""
        txtRemarks.Text = ""
    End Sub
   
    Private Sub DEL_EXPENSE()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_EXPENSES_DEL")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.ExecuteScalar()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "EDIT" Then
            btnAdd.Visible = False
            lblMsg.Text = ""
            btnModify.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As Integer = Integer.Parse(lblID.Text)
            Show_Data(id)
            txtStore.Text = id
        ElseIf e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As Integer = Integer.Parse(lblID.Text)
            DEL_DATA(id)
            If gvItems.Rows.Count > 0 Then
                btnsubmit.Visible = True
                lblTotalAmount.Visible = True
                lblTamount.Visible = True
            Else
                btnsubmit.Visible = False
                lblTotalAmount.Visible = False
                lblTamount.Visible = False
            End If

        End If
    End Sub
    Private Sub Show_Data(ByVal id As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_EXPENSES_GET_MODIFY")
            sp.Command.AddParameter("@SNO", id, DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlExpense.ClearSelection()
                ddlExpense.Items.FindByValue(ds.Tables(0).Rows(0).Item("EXPENSE_FOR")).Selected = True
                ddlMode.ClearSelection()
                Dim st1 As String = ds.Tables(0).Rows(0).Item("TRAVEL_MODE")
                If st1 = "" Then
                    tr2.Visible = False
                Else
                    tr2.Visible = True
                    ddlMode.Items.FindByValue(st1).Selected = True
                End If
                Dim st2 As Decimal = ds.Tables(0).Rows(0).Item("KM")
                If st2 = 0 Then
                    tr1.Visible = False
                Else
                    tr1.Visible = True
                    txtKM.Text = ds.Tables(0).Rows(0).Item("KM")
                End If
                txtAmount.Text = ds.Tables(0).Rows(0).Item("AMOUNT")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("REMARKS")
                txtJrDate.Text = ds.Tables(0).Rows(0).Item("JR_DATE")
                txtPurpose.Text = ds.Tables(0).Rows(0).Item("PURPOSE")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvItems.RowEditing

    End Sub

    Protected Sub gvItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItems.RowDeleting

    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        Try
            Dim validateexp As Integer
            validateexp = ValidateExpnModify()
            If validateexp = 0 Then
                lblMsg.Text = "You have already added this expense on this date"
            Else
                lblMsg.Text = ""
                Modify_Expense()
                ClearConveyance()
                BindGrid()
                btnAdd.Visible = True
                btnModify.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
   
    Private Sub Modify_Expense()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_EXPENSE_MODIFY")
            sp.Command.AddParameter("@SNO", txtStore.Text, DbType.Int32)
            sp.Command.AddParameter("@JOURNEY_DATE", txtJrDate.Text, DbType.Date)
            sp.Command.AddParameter("@PURPOSE", txtPurpose.Text, DbType.String)

            sp.Command.AddParameter("@EXPENSE_MODE", ddlExpense.SelectedItem.Value, DbType.String)
            If ddlMode.SelectedIndex = 0 Then
                sp.Command.AddParameter("@TRAVEL_MODE", "", DbType.String)
            Else
                sp.Command.AddParameter("@TRAVEL_MODE", ddlMode.SelectedItem.Value, DbType.String)
            End If
            If txtKM.Text = "" Then
                txtKM.Text = 0
            End If
            sp.Command.AddParameter("@TRAVEL_KM", txtKM.Text, DbType.Decimal)
            sp.Command.AddParameter("@EXPENSE_AMOUNT", txtAmount.Text, DbType.Decimal)
            sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
            sp.ExecuteScalar()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateExpnModify()
        Dim validateexp As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_EXPENSE_VALIDATE_MODIFY")
        sp.Command.AddParameter("@EXPENSE_MODE", ddlExpense.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SNO", txtStore.Text, DbType.Int32)
        sp.Command.AddParameter("@JR_DATE", txtJrDate.Text, DbType.Date)
        validateexp = sp.ExecuteScalar()
        Return validateexp

    End Function
    Private Sub DEL_DATA(ByVal id As Integer)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CONVEYANCE_EXPENSES_DELEXP")
        sp.Command.AddParameter("@SNO", id, DbType.Int32)
        sp.ExecuteScalar()
        BindGrid()

    End Sub

    
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If gvItems.Rows.Count > 0 Then
            Dim Reqid As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("UID")
            Add_Domestic_Conveyance(Reqid)
            'Add_Domestic_Conveyance_History(Reqid)
            lblMsg.Text = "Your Request Has Been Sent For Approval"
            mail_CNVReq(Reqid)
            Cleardata()
        End If
    End Sub
    Private Sub mail_CNVReq(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail

        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email

        Dim strHrEmail As String                              'To hold the HR Email
       


        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_REQUISITION_GETDETAILS")
        sp1.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        gvdetails.DataSource = sp1.GetDataSet()
        gvdetails.DataBind()

        strSubj = " Domestic Conveyance Requisition."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONEVYANCE_BINDDETAILS_MAIL")
        sp.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")

        Dim strfrommail, strfromname As String

        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "amantraadmin"

       
      

        strSubj = " Domestic Conveyance Request - by " & strEmpName & "."


        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
 "This is to inform your that these are the Details of Your  Domestic Conveyance Requisitions" & _
 "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
 "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
 "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
 "<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
 "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
 "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
 "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
 "<td  align=left width=12.5%><U>Amount </U></td>" & _
 "<td  align=left width=12.5%><U>Remarks </U></td></tr></table>"

        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblMode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAppkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblpurs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        'Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_CONVEYANCE")
        sp2.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")

        'Mail message for the Admin. 

       



        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of " & strEmpName & " Domestic Conveyance Requisitions" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
    "<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
    "<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
    "<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblMode"), Label).Text & "</td>" & _
    "<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTmode"), Label).Text & "</td>" & _
    "<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAppkm"), Label).Text & "</td>" & _
    "<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblpurs"), Label).Text & "</td>" & _
    "<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
    "<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strEmpName & "." & "<br>"
        'Response.Write(strMsgAdm)


        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Private Sub Cleardata()
        ddlMode.SelectedIndex = 0
        txtJrDate.Text = ""
        txtPurpose.Text = ""
        gvItems.Visible = False
        lblTamount.Visible = False
        lblTotalAmount.Visible = False
        btnsubmit.Visible = False

    End Sub
    Private Sub Add_Domestic_Conveyance(ByVal reqid As String)
        Try

            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_ADD")


                sp.Command.AddParameter("@CNV_REQ_ID", reqid, DbType.String)
                sp.Command.AddParameter("@JOURNEY_DATE", CType(gvItems.Rows(i).FindControl("lbljrdate"), Label).Text, DbType.Date)
                sp.Command.AddParameter("@PURPOSE", CType(gvItems.Rows(i).FindControl("lblpurs"), Label).Text, DbType.String)
                sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                sp.Command.AddParameter("@EXPENSE_MODE", CType(gvItems.Rows(i).FindControl("lblMode"), Label).Text, DbType.String)

            
                sp.Command.AddParameter("@TRAVEL_MODE", CType(gvItems.Rows(i).FindControl("lblTmode"), Label).Text, DbType.String)


               
                sp.Command.AddParameter("@TRAVEL_KM", CType(gvItems.Rows(i).FindControl("lblAppkm"), Label).Text, DbType.Decimal)


                sp.Command.AddParameter("@EXPENSE_AMOUNT", CType(gvItems.Rows(i).FindControl("lblAmount"), Label).Text, DbType.Decimal)
                sp.Command.AddParameter("@REMARKS", CType(gvItems.Rows(i).FindControl("lblRemarks"), Label).Text, DbType.String)
                sp.ExecuteScalar()
            Next
            'sp.Command.AddParameter("@CNV_AUR_ID", Session("UID"), DbType.String)
            'sp.Command.AddParameter("@CNV_JOURNEY_DATE", txtJrDate.Text, DbType.DateTime)
            'sp.Command.AddParameter("@CNV_TRVL_MODE", ddlMode.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@CNV_PURPOSE_TRVL", txtPurpose.Text, DbType.String)
            'sp.Command.AddParameter("@CNV_TOTAL_AMOUNT", lblTotalAmount.Text, DbType.Decimal)
            'sp.Command.AddParameter("@CNV_REQ_ID", reqid, DbType.String)

            'If txtKM.Text = "" Then
            '    txtKM.Text = 0
            'End If
            'sp.Command.AddParameter("@CNV_KM", txtKM.Text, DbType.Decimal)
            'sp.ExecuteScalar()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Private Sub Add_Domestic_Conveyance_History(ByVal reqid As String)
        Try
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_HISTORY_EXPENSE")
                sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                sp.Command.AddParameter("@EXPENSE_MODE", CType(gvItems.Rows(i).FindControl("lblMode"), Label).Text, DbType.String)
                sp.Command.AddParameter("@EXPENSE_AMOUNT", CType(gvItems.Rows(i).FindControl("lblAmount"), Label).Text, DbType.Decimal)
                sp.Command.AddParameter("@REMARKS", CType(gvItems.Rows(i).FindControl("lblRemarks"), Label).Text, DbType.String)
                sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
                sp.ExecuteScalar()
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMode.SelectedIndexChanged
        If ddlMode.SelectedValue = "CR" Then
            tr1.Visible = True
            txtKM.Text = ""
            If txtKM.Text = "" Then
                lblMsg.Text = "Please Enter Approximate Kilometers "
            Else
                lblMsg.Text = ""
                txtAmount.Text = 5.8 * (txtKM.Text)
                txtAmount.ReadOnly = True
              
            End If
        ElseIf ddlMode.SelectedValue = "BKE" Then
            tr1.Visible = True
            txtKM.Text = ""
            If txtKM.Text = "" Then
                lblMsg.Text = "Please Enter Approximate Kilometers "
            Else
                lblMsg.Text = ""
                txtAmount.Text = 2.3 * (txtKM.Text)
                txtAmount.ReadOnly = True
               
            End If
        Else
           
            tr1.Visible = False
            lblMsg.Text = ""
            txtAmount.Text = ""
            txtAmount.ReadOnly = False
        End If
    End Sub

    Protected Sub txtKM_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKM.TextChanged
        If ddlMode.SelectedValue = "CR" Then
            tr1.Visible = True
            If txtKM.Text = "" Then
                lblMsg.Text = "Please Enter Approximate Kilometers "

            Else
                lblMsg.Text = ""
                txtAmount.Text = 5.8 * (txtKM.Text)
                txtAmount.ReadOnly = True
            
            End If
        ElseIf ddlMode.SelectedValue = "BKE" Then
            tr1.Visible = True
            If txtKM.Text = "" Then
                lblMsg.Text = "Please Enter Approximate Kilometers "
            Else
                lblMsg.Text = ""
                txtAmount.Text = 2.3 * (txtKM.Text)
                txtAmount.ReadOnly = True
             
            End If
        Else
            
            tr1.Visible = False
            lblMsg.Text = ""
            txtAmount.Text = ""
            txtAmount.ReadOnly = False
        End If
    End Sub

    Protected Sub ddlExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlExpense.SelectedValue = "TCKT" Or ddlExpense.SelectedValue = "CONV" Then
            
            tr2.Visible = True
        Else
            lblMsg.Text = ""
            ddlMode.ClearSelection()
            ddlMode.SelectedIndex = 0
            tr2.Visible = False
            tr1.Visible = False
            txtKM.Text = ""
            txtAmount.Text = ""
            txtAmount.ReadOnly = False
        End If
       
    End Sub
End Class

