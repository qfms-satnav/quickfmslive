Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_PropertyDesk
    Inherits System.Web.UI.UserControl

    Dim TotalRent As Integer
    Dim TotalRentPaid As Integer
    Dim ClosingBalance As Integer
    Dim SecurityDeposit As Integer

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        gvItems.DataSource = Session("dataset")
        gvItems.DataBind()
        'gvItems.PageIndex = e.NewPageIndex()
        'Session("CurrentPageIndex") = e.NewPageIndex
        'BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "Surrender" Then
                'ddlcity.SelectedIndex = 0
                panel2.Visible = True
                'Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
                'Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
                'Dim lblpropcode As Label = DirectCast(gvRow.FindControl("lblpropcode"), Label)
                'UPDATE_PNSTATUS(lblpropcode.Text)
                'update_userstatus()
                'Dim currentpageindex As Integer = 0

                'If Session("CurrentPageIndex") <> Nothing Then
                '    currentpageindex = (Integer.Parse(Session("CurrentPageIndex")))
                'End If
                'Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString()) - (currentpageindex * 5)
                'Dim lblid As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblsno"), Label)
                'Dim lblPropertyCode As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblpropcode"), Label)
                'hfsno.Value = lblid.Text
                'txtpropertycode.Text = lblPropertyCode.Text

                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim lblPropertyCode As Label = DirectCast(row.FindControl("lblpropcode"), Label)
                Dim lblid As Label = DirectCast(row.FindControl("lblsno"), Label)
                Dim lblpropname As Label = DirectCast(row.FindControl("lblpropname"), Label)

                txtPropertyName.Text = lblpropname.Text

                hfsno.Value = lblid.Text
                txtpropertycode.Text = lblPropertyCode.Text

                GetTenantClosingBalance()


                txtRent.Text = TotalRent
                txtRentPaid.Text = TotalRentPaid

                txtTenClosingBal.Text = ClosingBalance
                txtSecurityDeposit.Text = SecurityDeposit
            Else
                panel2.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub GetTenantClosingBalance()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TENANT_CLOSING_BALANCE")
        sp.Command.AddParameter("@SNO", hfsno.Value, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            TotalRent = ds.Tables(0).Rows(0).Item("TotalRent")
            TotalRentPaid = ds.Tables(0).Rows(0).Item("TotalRentPaid")
            ClosingBalance = ds.Tables(0).Rows(0).Item("ClosingBalance")
            SecurityDeposit = ds.Tables(0).Rows(0).Item("TEN_SECURITY_DEPOSIT")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session("CurrentPageIndex") = 0
            panel1.Visible = True
            panel2.Visible = False
            BindGrid()
            txtSdate.Text = getoffsetdate(Date.Today)
            txtSdate.Attributes.Add("onClick", "displayDatePicker('" + txtSdate.ClientID + "')")
            txtSdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            ' BindCity()

        End If
        txtSdate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindCity()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            'ddlcity.DataSource = sp.GetDataSet()
            'ddlcity.DataTextField = "CTY_NAME"
            'ddlcity.DataValueField = "CTY_CODE"
            'ddlcity.DataBind()
            'ddlcity.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETPROPTEN")
            sp.Command.AddParameter("@user", txtEMPID.Text, DbType.String)
            sp.Command.AddParameter("@LoggedUser", Session("uid"), DbType.String)
            sp.Command.AddParameter("@FLAG", "Y", DbType.String)
            'gvItems.DataSource = sp.GetDataSet()
            Session("dataset") = sp.GetDataSet()
            gvItems.DataSource = Session("dataset")
            gvItems.DataBind()
            'gvItems.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
            gvItems.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        panel2.Visible = False
        BindGrid()
        panel1.Visible = True

        
    End Sub

    
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

        Dim validatedat As Integer
        validatedat = ValidateDate()
        If validatedat = 0 Then
            lblMsg.Text = "Surrendered Date Should be Less than Joining Date"
            lblMsg.Visible = True
        Else
            UpdateRecord()
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_TNTSTATUS1")
            'sp2.Command.AddParameter("@user", txtEMPID.Text, DbType.String)
            sp2.Command.AddParameter("@user", hfsno.Value, DbType.String)
            sp2.Command.AddParameter("@LOGGEDUSER", Session("UID"), DbType.String)
            'sp2.Command.AddParameter("@city", ddlcity.SelectedItem.Value, DbType.String)
            sp2.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=40")
        End If
    End Sub
    Private Sub UpdateRecord()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_SURRENDER_DATE")
        'sp.Command.AddParameter("@user", txtEMPID.Text, DbType.String)
        sp.Command.AddParameter("@sno", hfsno.Value, DbType.String)
        sp.Command.AddParameter("@sdate", txtSdate.Text, DbType.Date)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        'sp.Command.AddParameter("@City", ddlcity.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub UPDATE_PNSTATUS(ByVal propertycode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_PN_STATUS")
        sp.Command.AddParameter("@propcode", propertycode, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub update_userstatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_USR_STATUS")
        sp.Command.AddParameter("@user", txtEMPID.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Function ValidateDate()
        Dim validatedat As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Validate_Surrender_Date")
        sp.Command.AddParameter("@SDATE", txtSdate.Text, DbType.Date)
        sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
        validatedat = sp.ExecuteScalar()
        Return validatedat
    End Function
End Class
