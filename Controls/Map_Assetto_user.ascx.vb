Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_Map_Assetto_user
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindCategories()
        End If
    End Sub


    Private Sub BindCategories()
        GetChildRows("0")
        ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub GetChildRows(ByVal i As String)
        Dim str As String = ""
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If
        Dim ds As New DataSet
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        param(1) = New SqlParameter("@PARENTID", SqlDbType.NVarChar, 200)
        param(1).Value = id
        ds = ObjSubsonic.GetSubSonicDataSet("GETASTCATEGORIES_MAP", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim j As Integer = CType(dr("CategoryId"), Integer)
                If id = 0 Then
                    str = ""
                End If
                Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
                ddlAstCat.Items.Add(li)
                GetChildRows(str & "~" & j)
            Next
        End If
    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        BindAssetList(ddlAstCat.SelectedItem.Value)
    End Sub

    Private Sub BindAssetList(ByVal categoryid As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@categoryId", SqlDbType.Int)
        param(0).Value = categoryid
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASTLISTBYCATEGORY", param)
        gvAssetList.DataSource = ds
        gvAssetList.DataBind()

    End Sub

    Protected Sub gvAssetList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAssetList.PageIndexChanging

        Dim d As Integer = gvAssetList.PageCount
        Dim texts As String() = New String(gvAssetList.PageSize - 1) {}

        Dim count As Integer = 0
        For Each row As GridViewRow In gvAssetList.Rows

            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim chkEmp As CheckBox = DirectCast(row.FindControl("chkEmp"), CheckBox)


            If chkEmp.Checked = True Then
                texts(count) = lblProductId.Text & "," & chkEmp.Checked
            Else
                texts(count) = ""
            End If
            count += 1
        Next

        Session("page" + gvAssetList.PageIndex.ToString) = texts



        gvAssetList.PageIndex = e.NewPageIndex
        BindAssetList(ddlAstCat.SelectedItem.Value)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtEmpId.Text = "" Then
            lblMsg.Text = "Employee Id required."
            Exit Sub
        Else
            If CheckEmployee() = False Then
                gvEmployeeProductList.DataSource = Nothing
                gvEmployeeProductList.DataBind()
                Exit Sub
            End If
        End If
        GETGridValues()
        Dim count As Integer = 0
        Dim Message As String = String.Empty
        Dim TotalSelectedProduct As New ArrayList
        For i As Integer = 0 To gvAssetList.PageCount - 1
            If Session("page" + CStr(i)) IsNot Nothing Then
                TotalSelectedProduct.Add(Session("page" & i))
            End If
        Next

        Dim strPrdSelected As String = ""
        Dim cnt As Integer = 0
        For i As Integer = 0 To TotalSelectedProduct.Count - 1
            Dim strProduct As String()
            strProduct = TotalSelectedProduct.Item(i)
            For j As Integer = 0 To strProduct.Length - 1
                Dim a1
                If strProduct(j) Is Nothing Or strProduct(j) = "" Then
                Else
                    a1 = strProduct(j).Split(",")
                    If a1(1) = True Then
                        Dim prdid As Integer
                        prdid = a1(0)
                        If a1(1) = True Then
                            'strPrdSelected = strPrdSelected & prdid & ","
                            count += 1
                            Assign_Asset_to_Employee(prdid, Trim(txtEmpId.Text))
                        End If
                    End If
                End If
            Next
        Next
        If count = 0 Then
            lblMsg.Text = "Select at least one Asset to assign."
        Else
            lblMsg.Text = "Assets assign to Employee successufully."
            BindEmployeeProductList(Trim(txtEmpId.Text), False)
        End If
    End Sub

    Private Function CheckEmployee() As Boolean

        If txtEmpId.Text = "" Then
            lblMsg.Text = "Employee Id required."
            Return False
        Else
            lblMsg.Text = ""
        End If


        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Trim(txtEmpId.Text)
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("CHK_EMPLOYEEID", param)
        If CInt(ds.Tables(0).Rows(0).Item("EMPSTATUS")) = 0 Then
            lblMsg.Text = "Employee Id <b>" & Trim(txtEmpId.Text) & "</b> does not exists."
            Return False
        Else
            lblMsg.Text = ""
        End If
        Return True
    End Function

    Private Sub BindEmployeeProductList(ByVal strAurId As String, ByVal showAllcategory As Boolean)
        Dim CategoryId As Integer
        If ddlAstCat.SelectedItem.Value = "--Select--" Then
            CategoryId = 0
        Else
            CategoryId = ddlAstCat.SelectedItem.Value
        End If

        Session("ShowAllCategory") = showAllcategory

        If Session("ShowAllCategory") = True Then
            CategoryId = 0
        End If
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strAurId
        param(1) = New SqlParameter("@CATEGORYID", SqlDbType.Int)
        param(1).Value = CategoryId
        ObjSubsonic.BindGridView(gvEmployeeProductList, "GET_ASSIGN_AST_EMPLOYEE", param)

    End Sub


    Private Sub Assign_Asset_to_Employee(ByVal AstPrdId As Integer, ByVal strAurId As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strAurId
        param(1) = New SqlParameter("@AST_PRD_ID", SqlDbType.NVarChar, 200)
        param(1).Value = AstPrdId
        ObjSubsonic.GetSubSonicExecute("INSERT_AST_PRODUCTMAPPING", param)
    End Sub

    Private Sub GETGridValues()
        Dim d As Integer = gvAssetList.PageCount
        Dim texts As String() = New String(gvAssetList.PageSize - 1) {}

        Dim count As Integer = 0
        For Each row As GridViewRow In gvAssetList.Rows

            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim chkEmp As CheckBox = DirectCast(row.FindControl("chkEmp"), CheckBox)


            If chkEmp.Checked = True Then
                texts(count) = lblProductId.Text & "," & chkEmp.Checked
            Else
                texts(count) = ""
            End If
            count += 1
        Next

        Session("page" + gvAssetList.PageIndex.ToString) = texts
    End Sub

    Protected Sub gvAssetList_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAssetList.PreRender
        If Session("page" + gvAssetList.PageIndex.ToString) IsNot Nothing Then
            Dim texts As String() = DirectCast(Session("page" + CStr(gvAssetList.PageIndex)), String())
            For i As Integer = 0 To gvAssetList.Rows.Count - 1
                Dim chkEmp As CheckBox = DirectCast(gvAssetList.Rows(i).FindControl("chkEmp"), CheckBox)
                If texts(i) = "" Then
                Else
                    Dim prdValue As String() = texts(i).Split(",")

                    If prdValue(1) = True Then
                        chkEmp.Checked = True
                    Else
                        chkEmp.Checked = False
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If CheckEmployee() = False Then
            gvEmployeeProductList.DataSource = Nothing
            gvEmployeeProductList.DataBind()

            Exit Sub
        Else
            BindEmployeeProductList(Trim(txtEmpId.Text), False)
        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        If CheckEmployee() = False Then
            gvEmployeeProductList.DataSource = Nothing
            gvEmployeeProductList.DataBind()
            Exit Sub
        Else
            BindEmployeeProductList(Trim(txtEmpId.Text), True)
        End If
    End Sub

    Protected Sub gvEmployeeProductList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEmployeeProductList.PageIndexChanging
        
        gvEmployeeProductList.PageIndex = e.NewPageIndex
        BindEmployeeProductList(Trim(txtEmpId.Text), Session("ShowAllCategory"))

    End Sub

    Protected Sub gvEmployeeProductList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvEmployeeProductList.RowCommand
        If e.CommandName = "Delete" Then
            '*------------ Delete DELETE_ASSIGNEDPRODUCT **************
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@id", SqlDbType.Int)
            param(0).Value = CInt(e.CommandArgument)
            ObjSubsonic.GetSubSonicExecute("DELETE_ASSIGNEDPRODUCT", param)
            BindEmployeeProductList(Trim(txtEmpId.Text), Session("ShowAllCategory"))
            '**********************
        End If
    End Sub

    Protected Sub gvEmployeeProductList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvEmployeeProductList.RowDeleting

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx")
    End Sub
End Class
