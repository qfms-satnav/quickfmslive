<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LveEncashments.ascx.vb"
    Inherits="Controls_LveEncashments" %>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>




<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDep" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlRM" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Minimum Leave Balance for Encashment<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Balance Paid Leaves<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPleaves" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Balance Compensatory Leaves<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtCompLeaves" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Type of Encashment<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlEncash"
                    Display="None" ErrorMessage="Please Select Encashment Type" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlEncash" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                        <asp:ListItem Value="1">Cheque</asp:ListItem>
                        <asp:ListItem Value="2">Cash</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Type of Leave<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvleave" runat="server" ControlToValidate="ddlLeave"
                    Display="None" ErrorMessage="Please Select Leave Type" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLeave" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Applied Days for Encashment<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvencashdays" runat="server" ControlToValidate="txtEncash"
                    Display="None" ErrorMessage="Please Enter Applied Days to Encash" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revCNo" runat="Server" Display="none" ErrorMessage="Please Enter Numerics for Applied Days for Encashment"
                    ControlToValidate="txtEncash" ValidationExpression="^[0-9]+$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtEncash" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    <asp:Button ID="btnLeaves" runat="Server" Text="Leave(s) for Encashment" CssClass="btn btn-primary custom-button-color"
                        ValidationGroup="Val1" CausesValidation="true" /></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtNoLeaves" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" CausesValidation="true" />
        </div>
    </div>
</div>
