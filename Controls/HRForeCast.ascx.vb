Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_HRForeCast
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If ddlLocation.SelectedIndex = 0 Or ddlDept.SelectedIndex = 0 Or ddlDesig.SelectedIndex = 0 Or txtFnumber.Text = "" Or ddlFMonth.SelectedIndex = 0 Or ddlTMonth.SelectedIndex = 0 Or ddlFYear.SelectedIndex = 0 Or ddlFYear.SelectedIndex = 0 Then
            lblMsg.Text = "Please Fill All the Mandatory Fields"
            lblMsg.Visible = True
            'CHECK ISNUMERIC
        ElseIf IsNumeric(txtFnumber.Text) = 0 Then
            lblMsg.Text = "Please Enter Only Numerics for Forecast Number"
            lblMsg.Visible = True
        ElseIf txtFnumber.Text = 0 Then
            lblMsg.Text = "Please Enter Forecast Number more than Zero"
            lblMsg.Visible = True
        ElseIf CInt(ddlFYear.SelectedItem.Text) > CInt(ddlTYear.SelectedItem.Text) Then
            lblMsg.Text = "To Year Should be Greater than From Year"
            lblMsg.Visible = True
        Else
            lblMsg.Text = ""
            Insert_Data()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=40")
        End If
    End Sub
    Private Function IsNumeric(ByVal strVal As String) As Integer
        Try
            Convert.ToInt32(strVal)
            Return 1
        Catch
            Return 0
        End Try
    End Function

    Private Sub Insert_Data()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_HR_FORECAST_ADD")
            sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@DEPARTMENT", ddlDept.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@DESIGNATION", ddlDesig.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@FORECASTED_NO", txtFnumber.Text, DbType.Decimal)
            sp.Command.AddParameter("@F_MONTH", ddlFMonth.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@T_MONTH", ddlTMonth.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@F_YEAR", ddlFYear.SelectedItem.Text, DbType.Int32)
            sp.Command.AddParameter("@T_YEAR", ddlTYear.SelectedItem.Text, DbType.Int32)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocation()
            BindDept()
            BindDesig()
        End If
    End Sub
    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LOCATION_GET")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEPARTMENT")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDept.DataSource = sp.GetDataSet()
            ddlDept.DataTextField = "DEP_NAME"
            ddlDept.DataValueField = "DEP_CODE"
            ddlDept.DataBind()
            ddlDept.Items.Insert(0, New ListItem("--Select--", "0"))

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDesig()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DESIGNATION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDesig.DataSource = sp.GetDataSet()
            ddlDesig.DataTextField = "DSN_AMT_TITLE"
            ddlDesig.DataValueField = "DSN_CODE"
            ddlDesig.DataBind()
            ddlDesig.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
