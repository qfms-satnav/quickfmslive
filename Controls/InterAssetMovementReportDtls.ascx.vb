Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_InterAssetMovementReportDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim AAS_SNO, AAS_AAT_CODE, AAS_BDG_ID, AAS_FLR_ID, LCM_CODE, LCM_NAME, AAT_EMP_ID As String

    Dim strReqId As String
    Dim StrToEmp As Integer

    Dim FBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String
    Dim Req_status As Integer

    Dim strMMR_RAISEDBY, strRaisedDt As String


    Dim strITAppBy, strITAppDt, strITAppComment As String
    Dim strOutWardAppBy, strOutWardAppDt, strOutWardAppComment As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            BindEmp()
            getDetailsbyReqId(strReqId)
            BindLocations()

            'btnPrint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
        End If
    End Sub


    Public Sub BindEmp()
        ObjSubsonic.Binddropdown(ddlEmp, "GET_USERS", "AUR_FIRST_NAME", "AUR_ID")

    End Sub
    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)

        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("GET_INTER_MVMT_REQID", param)
        If dr.Read Then
            FBDG_ID = dr.Item("FLOC_CODE")
            FTower = dr.Item("MMR_FROMBDG_ID")
            FFloor = dr.Item("MMR_FROMFLR_ID")

            receiveAst = dr.Item("MMR_RECVD_BY")
            strremarks = dr.Item("MMR_COMMENTS")
            Req_status = dr.Item("MMR_LRSTATUS_ID")

            strMMR_RAISEDBY = dr.Item("MMR_RAISEDBY")
            strRaisedDt = dr.Item("MMR_MVMT_DATE")


            strITAppBy = dr.Item("MMR_APPROVED_BY")
            strITAppDt = dr.Item("MMR_Approved_date")
            strITAppComment = dr.Item("MMR_App_Rej_COMMENTS")

            strOutWardAppBy = dr.Item("MMR_OutWardAPPROVED_BY")
            strOutWardAppDt = dr.Item("MMR_OutWard_Date")
            strOutWardAppComment = dr.Item("MMR_OutWard_COMMENTS")
            StrToEmp = dr.Item("MMR_TOEMP_ID")
            Req_status = dr.Item("MMR_LRSTATUS_ID")
            BindEmp()
            ddlEmp.Items.FindByValue(StrToEmp).Selected = True

        End If

        If dr.IsClosed = False Then
            dr.Close()
        End If
        txtPendingApproved.Text = strMMR_RAISEDBY
        txtPendingRemarks.Text = strremarks
        txtPendingDate.Text = strRaisedDt
        chkstatus(Req_status)
    End Sub

    Public Sub chkstatus(ByVal Req_status As Integer)

        If Req_status = 1 Then
            ddlSLoc.Enabled = True
            ddlSTower.Enabled = True
            ddlSFloor.Enabled = True
            ddlEmp.Enabled = True
            fldNewReq.Visible = True
            tblITSummary.Visible = False

        Else
            ddlSLoc.Enabled = False
            ddlSTower.Enabled = False
            ddlSFloor.Enabled = False
            ddlEmp.Enabled = False
            tblITSummary.Visible = False
            fldITApproval.Visible = False

            fldNewReq.Visible = True
        End If

        If Req_status >= 1024 Then
            ddlSLoc.Enabled = False
            ddlSTower.Enabled = False
            ddlSFloor.Enabled = False
            ddlEmp.Enabled = False
            tblITSummary.Visible = True
            fldITApproval.Visible = True
            lblITApproval.Text = strITAppBy
            lblITDate.Text = strITAppDt
            lblITRemarks.Text = strITAppComment

        Else
            fldITApproval.Visible = False

        End If


    End Sub

    Private Sub BindLocations()
        '----------- Binding Location-----------------------
        ObjSubsonic.Binddropdown(ddlSLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")

        '------------ From Building/Tower/Floor -----------------------------
        If FBDG_ID = "" Or FBDG_ID Is Nothing Then

        Else
            ddlSLoc.Items.FindByValue(FBDG_ID).Selected = True

            '----------- Binding Tower -----------------------
            If ddlSLoc.SelectedIndex > 0 Then
                Dim LocCode As String = ddlSLoc.SelectedItem.Value
                BindTowersByLocation(LocCode, ddlSTower)
            End If
            ddlSTower.Items.FindByValue(FTower).Selected = True
            '-------------------------------------------------

            If ddlSTower.SelectedIndex > 0 Then
                Dim TwrCode As String = ddlSTower.SelectedItem.Value
                BindFloorsByTower(TwrCode, ddlSFloor)
            End If
            ddlSFloor.Items.FindByValue(FFloor).Selected = True

        End If

    End Sub


    Private Sub BindTower(ByVal AAS_BDG_ID As String)
        '----------- Binding Tower -----------------------
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlSTower)
        End If
        ddlSTower.Items.FindByValue(AAS_BDG_ID).Selected = True
        '-------------------------------------------------
    End Sub
    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = LocCode
        ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    End Sub

    Public Sub BindFloor(ByVal AAS_FLR_ID As String)
        If ddlSTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlSTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlSFloor)
        End If
        ddlSFloor.Items.FindByValue(AAS_FLR_ID).Selected = True
    End Sub

    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TwrCode", DbType.String)
        param(0).Value = TwrCode
        ObjSubsonic.Binddropdown(ddl, "USP_FLOOR_GETBYTOWER", "FLR_NAME", "FLR_CODE", param)
    End Sub

 
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAssetMovementReport.aspx")
    End Sub
End Class

