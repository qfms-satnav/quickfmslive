Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AssetModify
    Inherits System.Web.UI.UserControl
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim AAT_CODE As String = Request.QueryString("code")
        Dim AAT_NAME As String = txtName.Text
        Dim AAT_MODEL_NAME As String = txtModelName.Text
        Dim AAT_AAG_CODE As String = drdGrp_Name.SelectedItem.Value
        Dim AAT_AVR_CODE As String = drdVendor_Name.SelectedItem.Value
        Dim AAT_AAB_CODE As String = drdBrand_Name.SelectedItem.Value
        Dim AAT_AMC_REQD As Integer
        If (assetreq.SelectedItem.Value = True) Then
            AAT_AMC_REQD = 1
        Else
            AAT_AMC_REQD = 0
        End If

        Dim AAT_UPT_DT As Date = getoffsetdatetime(DateTime.Now).ToString()
        Dim AAT_UPT_BY As String = Session("uid")
        Dim AAT_STA_ID As Integer
        If (assetid.SelectedItem.Value = True) Then
            AAT_STA_ID = 1
        Else
            AAT_STA_ID = 0

        End If

        Dim AAT_OWNED As Integer
        If (assetOwned.SelectedItem.Value = True) Then
            AAT_OWNED = 1
        Else
            AAT_OWNED = 0
        End If
        Dim AAT_PURCHASED_STATUS As Integer
        If (AssetPurchasedStatus.SelectedItem.Value = True) Then
            AAT_PURCHASED_STATUS = 1
        Else
            AAT_PURCHASED_STATUS = 0
        End If
        Dim AAT_SPC_FIXED As Integer
        If (rdbtnAssetSpace.SelectedItem.Value = True) Then
            AAT_SPC_FIXED = 1
        Else
            AAT_SPC_FIXED = 0
        End If

        Dim AAT_USR_MOVABLE As Integer


        If (rdbtnAssetUser.SelectedItem.Value = True) Then
            AAT_USR_MOVABLE = 1
        Else
            AAT_USR_MOVABLE = 0
        End If

        Dim AAT_AST_CONS As Integer
        If (rdbtnAssetCons.SelectedItem.Value = True) Then
            AAT_AST_CONS = 1
        Else
            AAT_AST_CONS = 0
        End If
        Dim AAT_DESC As String = txtRemarks.Text
        Try
            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMG_AAT_UPDT")
            'sp4.Command.AddParameter("@AAT_ID", AAT_ID, Data.DbType.Int32)
            sp4.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_NAME", AAT_NAME, DbType.String)
            sp4.Command.AddParameter("@AAT_MODEL_NAME", AAT_MODEL_NAME, DbType.String)
            sp4.Command.AddParameter("@AAT_AAG_CODE", AAT_AAG_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_AVR_CODE", AAT_AVR_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_AAB_CODE", AAT_AAB_CODE, DbType.String)
            sp4.Command.AddParameter("@AAT_AMC_REQD", AAT_AMC_REQD, DbType.Int32)
            sp4.Command.AddParameter("@AAT_UPT_DT", AAT_UPT_DT, DbType.Date)
            sp4.Command.AddParameter("@AAT_UPT_BY", AAT_UPT_BY, DbType.String)
            sp4.Command.AddParameter("@AAT_STA_ID", AAT_STA_ID, DbType.Int32)
            sp4.Command.AddParameter("@AAT_OWNED", AAT_OWNED, DbType.Int32)
            sp4.Command.AddParameter("@AAT_PURCHASED_STATUS", AAT_PURCHASED_STATUS, DbType.Int32)
            sp4.Command.AddParameter("@AAT_SPC_FIXED", AAT_SPC_FIXED, DbType.Int32)
            sp4.Command.AddParameter("@AAT_USR_MOVABLE", AAT_USR_MOVABLE, DbType.Int32)
            sp4.Command.AddParameter("@AAT_AST_CONS", AAT_AST_CONS, DbType.Int32)
            sp4.Command.AddParameter("@AAT_DESC", AAT_DESC, DbType.String)
            sp4.ExecuteScalar()
            lblMsg.Text = "Data Modified Succesfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub Cleardata()
        txtName.Text = ""
        txtModelName.Text = ""
        drdGrp_Name.SelectedIndex = 0
        drdVendor_Name.SelectedIndex = 0
        drdBrand_Name.SelectedIndex = 0
        txtRemarks.Text = ""
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_GrpValues")
            sp3.Command.AddParameter("@dummy", 1, DbType.Int32)
            Dim ds3 As New DataSet
            ds3 = sp3.GetDataSet

            drdGrp_Name.DataSource = ds3
            drdGrp_Name.DataTextField = "AAG_NAME"
            drdGrp_Name.DataValueField = "AAG_CODE"
            drdGrp_Name.DataBind()
            drdGrp_Name.Items.Insert(0, New ListItem("--SELECT--", "0"))


            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_VendorValues")
            sp1.Command.AddParameter("@dummy", 1, DbType.Int32)
            Dim ds1 As New DataSet
            ds1 = sp1.GetDataSet

            drdVendor_Name.DataSource = ds1
            drdVendor_Name.DataTextField = "AVR_NAME"
            drdVendor_Name.DataValueField = "AVR_CODE"
            drdVendor_Name.DataBind()

            drdVendor_Name.Items.Insert(0, New ListItem("--SELECT--", "0"))
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Get_Brandvalues")
            sp2.Command.AddParameter("@dummy", 1, DbType.Int32)

            Dim ds2 As New DataSet
            ds2 = sp2.GetDataSet

            drdBrand_Name.DataSource = ds2
            drdBrand_Name.DataTextField = "AAB_NAME"
            drdBrand_Name.DataValueField = "AAB_CODE"
            drdBrand_Name.DataBind()

            drdBrand_Name.Items.Insert(0, New ListItem("--SELECT--", "0"))

            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMGASSET_DETAILS")
            SP.Command.AddParameter("@AAT_CODE", Request.QueryString("code"), DbType.String)
            Dim DS As New DataSet
            DS = SP.GetDataSet
            If DS.Tables(0).Rows.Count > 0 Then
                'txtCode.Text = Request.QueryString("code")
                txtName.Text = DS.Tables(0).Rows(0).Item("AAT_NAME")
                txtModelName.Text = DS.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
                Dim li As ListItem = Nothing
                li = drdGrp_Name.Items.FindByValue(DS.Tables(0).Rows(0).Item("AAT_AAG_CODE"))
                If Not li Is Nothing Then
                    li.selected = True
                End If
                'drdGrp_Name.SelectedItem.Value = DS.Tables(0).Rows(0).Item("AAT_AAG_CODE")
                Dim li1 As ListItem = Nothing
                li1 = drdVendor_Name.Items.FindByValue(DS.Tables(0).Rows(0).Item("AAT_AVR_CODE"))
                If Not li1 Is Nothing Then
                    li1.Selected = True
                End If
                'drdVendor_Name.SelectedItem.Value = DS.Tables(0).Rows(0).Item("AAT_AVR_CODE")
                Dim li2 As ListItem = Nothing
                li2 = drdBrand_Name.Items.FindByValue(DS.Tables(0).Rows(0).Item("AAT_AAB_CODE"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                End If
                'drdBrand_Name.SelectedItem.Value = DS.Tables(0).Rows(0).Item("AAT_AAB_CODE")
                Dim aat_req As Integer = DS.Tables(0).Rows(0).Item("AAT_AMC_REQD")
                If aat_req = 0 Then
                    assetreq.SelectedValue = "False"
                Else
                    assetreq.SelectedValue = "True"

                End If

                Dim aat_STAID As Integer = DS.Tables(0).Rows(0).Item("AAT_STA_ID")
                If aat_STAID = 0 Then

                    assetid.SelectedValue = "False"
                Else
                    assetid.SelectedValue = "True"

                End If
                Dim aat_Owned As Integer = DS.Tables(0).Rows(0).Item("AAT_OWNED")
                If aat_Owned = 0 Then

                    assetOwned.SelectedValue = "False"
                Else
                    assetOwned.SelectedValue = "True"

                End If
                Dim aat_purchasedstatus As Integer = DS.Tables(0).Rows(0).Item("AAT_PURCHASED_STATUS")
                If aat_purchasedstatus = 0 Then

                    AssetPurchasedStatus.SelectedValue = "False"
                Else
                    AssetPurchasedStatus.SelectedValue = "True"

                End If
                Dim aat_space As Integer = DS.Tables(0).Rows(0).Item("AAT_SPC_FIXED")
                If aat_space = 0 Then

                    rdbtnAssetSpace.SelectedValue = "False"
                Else
                    rdbtnAssetSpace.SelectedValue = "True"

                End If
                Dim aat_userfixed As Integer = DS.Tables(0).Rows(0).Item("AAT_USR_MOVABLE")
                If aat_userfixed = 0 Then

                    rdbtnAssetUser.SelectedValue = "False"
                Else
                    rdbtnAssetUser.SelectedValue = "True"

                End If
                Dim aat_cons As Integer = DS.Tables(0).Rows(0).Item("AAT_AST_CONS")
                If aat_cons = 0 Then

                    rdbtnAssetCons.SelectedValue = "False"
                Else
                    rdbtnAssetCons.SelectedValue = "True"

                End If
                txtRemarks.Text = DS.Tables(0).Rows(0).Item("AAT_DESC")

            Else
                'txtCode.Text = ""
                txtName.Text = ""
                txtModelName.Text = ""
                drdGrp_Name.SelectedIndex = 0
                drdVendor_Name.SelectedIndex = 0
                drdBrand_Name.SelectedIndex = 0
                txtRemarks.Text = ""

            End If
        End If
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAMGAssetGetDetails.aspx")
    End Sub
End Class
