<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApprovalLease.ascx.vb" Inherits="Controls_ApprovalLease" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>



<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Lease Approval
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="80%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
            <td colspan="3" align="left">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Lease Approval</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table class="table" style="height: 22px" width="100%" border="1">
                    <tr>
                        <td align="center" style="height: 20px">
                            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
                                AllowPaging="True" Width="100%" PageSize="5" EmptyDataText="Sorry ! No Records there to Approve">
                                <Columns>
                                    <asp:TemplateField visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeasecode" runat="server" Text='<%#Eval("LEASE_NUM") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLesseNum" runat="server" Text='<%#Eval("LESSE_NUM") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblleasename" runat="server" Text='<%#Eval("LEASE_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lesse">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllesse" runat="server" Text='<%#Eval("LESSE_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Start Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstdate" runat="server" Text='<%#Eval("LEASE_START_DATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Expiry Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblexpdate" runat="server" Text='<%#Eval("LEASE_EXPIRY_DATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Surrenderd Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsuddate" runat="server" Text='<%#Eval("SURRENDEREDDATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approve">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkApprove" runat="server" Text="Approve" CommandName="Approve"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reject">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkReject" runat="server" Text="Reject" CommandName="Reject"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="panel1" runat="server" Width="100%">
                    <table  cellspacing="0" cellpadding="1" width="100%" border="1">
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Select Property Type <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                    Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                                    InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:DropDownList ID="ddlproptype" runat="server" CssClass="clsComboBox" Width="99%" Enabled="false" >
                                   
                                </asp:DropDownList>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                Select City <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="clsComboBox" Width="99%" Enabled="false">
                                    
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                 Property <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlBuilding"
                                    Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtProperty" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="true"></asp:TextBox>
                            </td>
                          <td align="left" style="width: 25%; height: 26px;">
                                        Lease Code<font class="clsNote">*</font>
                                       
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                       
                                            <asp:TextBox ID="txtLnumber" runat="server" CssClass="clsTextField" Width="97%" Enabled="false" MaxLength="50"></asp:TextBox>
                                    </td>
                        </tr>
                        <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lease Name<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvLeaseName" runat="server" ControlToValidate="txtLname"
                                            Display="None" ErrorMessage="Please enter Lease Name" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>&nbsp;
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        
                                            <asp:TextBox ID="txtLname" runat="server" CssClass="clsTextField" Enabled="false" Width="97%" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lease Type<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvLeaseType" runat="server" ControlToValidate="ddlLeaseType"
                                            Display="None" ErrorMessage="Please enter Lease Type" InitialValue="0" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <asp:DropDownList ID="ddlLeaseType" runat="server" CssClass="clsComboBox" Enabled="false" 
                                            Width="99%">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                        <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lessor Name<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvLessorName" runat="server" ControlToValidate="txtLessorName"
                                            Display="None" ErrorMessage="Please enter Lessor Name " ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>&nbsp;
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                      
                                            <asp:TextBox ID="txtLessorName" Enabled="false" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Select Lesse<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvLesseName" runat="server" ControlToValidate="ddlLesse"
                                            Display="None" ErrorMessage="Please Select Lesse " ValidationGroup="Val1" InitialValue="0">
                                        </asp:RequiredFieldValidator>
                                   
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                       
                                            <asp:DropDownList ID="ddlLesse"  runat="server" CssClass="clsComboBox" Width="99%"></asp:DropDownList>
                                    </td>
                                </tr>
                       <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lesse Start Date<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvstartdate" runat="server" ControlToValidate="txtStartDate"
                                            Display="None" ErrorMessage="Please pick Date " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                       
                                            <asp:TextBox ID="txtStartDate" runat="server"  CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lesse Expiry Date<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtExpiryDate"
                                            Display="None" ErrorMessage="Please pick Date "  ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                      
                                            <asp:TextBox ID="txtExpiryDate" runat="server"  CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                </tr>
                        <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lesse Escalation Date1<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvdate1" runat="server" ControlToValidate="txtEscalationDate"
                                            Display="None" ErrorMessage="Please pick Date " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        
                                            <asp:TextBox ID="txtEscalationDate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Escalated to Email1<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail1"
                                            Display="None" ErrorMessage="Please enter Email " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail1"
                                            ErrorMessage="Please enter valid Email" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <asp:TextBox ID="txtEmail1" runat="server" CssClass="clsTextField" Enabled="false" Width="97%"></asp:TextBox>
                                    </td>
                                </tr>
                       <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lesse Escalation Date2<font class="clsNote"></font>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <asp:TextBox ID="txtDate2" runat="server"  CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Escalated to Email2<font class="clsNote"></font>&nbsp;
                                        <asp:RegularExpressionValidator ID="revEmail2" runat="server" ControlToValidate="txtEmail2"
                                            ValidationGroup="Val1" ErrorMessage="Please enter valid Email" Display="None"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <asp:TextBox ID="txtEmail2" runat="server" Enabled="false" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lesse Escalation Date3<font class="clsNote"></font>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <asp:TextBox ID="txtDate3" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Escalated to Email3<font class="clsNote"></font>&nbsp;
                                        <asp:RegularExpressionValidator ID="revEmail3" runat="server" ControlToValidate="txtEmail3"
                                            Display="None" ErrorMessage="Please enter valid Email" ValidationGroup="Val1"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        <asp:TextBox ID="txtEmail3" runat="server" Enabled="false" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lease Cost<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea"
                                            Display="None" ErrorMessage="Please enter Lease Amount " ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvinvestedArea" runat="server" ControlToValidate="txtInvestedArea"
                                            ErrorMessage="Please enter valid Lease Amount" ValidationGroup="Val1" Type="double"
                                            Operator="DataTypeCheck" Display="None"></asp:CompareValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                       
                                            <asp:TextBox ID="txtInvestedArea" Enabled="false" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="8"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Occupied Area<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvOccupiedArea" runat="server" ControlToValidate="txtOccupiedArea"
                                            Display="None" ErrorMessage="Please enter Lease Occupied Area " ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvoccupiedarea" runat="server" ControlToValidate="txtOccupiedArea"
                                            ErrorMessage="Please enter valid Lease Occupied Area" Display="None" ValidationGroup="Val1"
                                            Type="double" Operator="DataTypeCheck"></asp:CompareValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                     
                                            <asp:TextBox ID="txtOccupiedArea" Enabled="false"  runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Select Status<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="cvStatus" runat="server" ControlToValidate="ddlStatus"
                                            Display="None" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="center" style="width: 25%; height: 26px;">
                                        <asp:DropDownList ID="ddlStatus" runat="server" Width="99%" CssClass="clsComboBox" Enabled="false">
                                            <asp:ListItem>--Select Status--</asp:ListItem>
                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                            <asp:ListItem Value="0">Terminated</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Advance Payment<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="txtpay"
                                            Display="None" ErrorMessage="Please enter Advance Payment " ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        
                                            <asp:TextBox ID="txtpay" runat="server" Enabled="false" CssClass="clsTextField" Width="97%" MaxLength="8"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Lease Escalation Percentage<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvper" runat="server" ControlToValidate="txtEscPer"
                                            Display="None" ErrorMessage="Please enter Lease Escalation Percentage" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revper" runat="server" ControlToValidate="txtEscPer"
                                            ValidationGroup="Val1" ErrorMessage="Please enter valid Escalation Percentage"
                                            Display="None" ValidationExpression="^[0-9]*\.?[0-9]*$">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                       
                                            <asp:TextBox ID="txtEscPer" Enabled="false" runat="server" CssClass="clsTextField" Width="97%" MaxLength="3"></asp:TextBox>
                                    </td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                       Select Lease Cost Per<font class="clsNote">*</font>
                                       <asp:RequiredFieldValidator ID="rfvMode" runat="server" ControlToValidate="ddlMode" Display="None" ErrorMessage="Please Select Payment Term" ValidationGroup="Val1" InitialValue="--Select--">
                                       </asp:RequiredFieldValidator>
                                       </td>
                                       <td align="left" style="width:25%;height:26px">
                                       <asp:DropDownList ID="ddlMode" runat="server" CssClass="clsComboBox" Width="99%" Enabled="false">
                                       <asp:ListItem>--Select--</asp:ListItem>
                                       <asp:ListItem Value="1">Weekly</asp:ListItem>
                                       <asp:ListItem  Value="2">Monthly</asp:ListItem>
                                       <asp:ListItem Value="3">Half-Yearly</asp:ListItem>
                                       <asp:ListItem Value="4">Yearly</asp:ListItem>
                                       </asp:DropDownList>
                                       </td>
                                </tr>
                                <tr>
                                <td align="left" colspan="2" style="width: 25%; height: 26px;">
                                        Lease Comments<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfComments" runat="server" ControlToValidate="txtComments"
                                            Display="None" ErrorMessage="Please enter Comments" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" colspan="2" style="width: 25%; height: 26px;">
                                        
                                            <asp:TextBox ID="txtComments" runat="server" Enabled="false" CssClass="clsTextField" Width="97%"
                                                TextMode="MultiLine" Rows="3" MaxLength="250"></asp:TextBox>
                                    </td>
                                
                                </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Approve" ValidationGroup="Val1" />&nbsp;
                            <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>

</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="gvItems" />
<asp:PostBackTrigger ControlID="btnSubmit" />
    <asp:PostBackTrigger ControlID="btnCancel" />
</Triggers>
</asp:UpdatePanel>