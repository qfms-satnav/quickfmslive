Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Globalization
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.Data.OleDb
Imports System.Collections.Generic
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Text.RegularExpressions

Partial Class Controls_AssetReq
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions



    Public Class ImageClas
        Private _fn As String

        Public Property FILENAME() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String

        Public Property FILEPATH() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Page, Page.[GetType](), "anything", "refreshSelectpicker();", True)
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager1 As ScriptManager = ScriptManager.GetCurrent(Page)

        If scriptManager1 IsNot Nothing Then
            scriptManager1.RegisterPostBackControl(btnSubmit)
        End If

        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
            'RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                If "Aadhar.dbo" = Session("TENANT") Then
                    hide1.Visible = False

                End If
                If "Ujjivan.dbo" = Session("TENANT") Then
                    hide1.Visible = False

                End If
                If "Shubham_finance.dbo" = Session("TENANT") Then
                    hide1.Visible = False

                End If
                BindUsers()
                BindCategories()
                'getassetcategory()
                getassetsubcategory()
                BindLocation()
                ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
                ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
                'ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
                'ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
                ddlAstBrand.Items.Insert(0, "--Select--")
                ddlAstModel.Items.Insert(0, "--Select--")
                pnlItems.Visible = True
                lblMsg.Visible = False
                gvItems.Visible = True
                remarksAndSubmitBtn.Visible = True
                fillgrid()
                btnExport.Visible = False
            End If
        End If
    End Sub
    Private Sub BindUsers()

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")

        param(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("COMPANYID")

        ObjSubsonic.Binddropdown(ddlEmp, "AM_AMT_bindUsers_SP", "NAME", "AUR_ID", param)

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_bindUsers_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        Dim li As ListItem = ddlEmp.Items.FindByValue(Session("UID"))
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 0, 0)
    End Sub
    Private Sub BindCategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@aur_id", Session("uid"), DbType.String)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged

        pnlItems.Visible = False
        remarksAndSubmitBtn.Visible = False
        getassetsubcategory()
        getbrandbycatsubcat()
        getmakebycatsubcat()
    End Sub
    Private Sub getbrandbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, New ListItem("No Brand", "No Brand"))
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        getmakebycatsubcat()
        pnlItems.Visible = False
    End Sub
    Private Function GenerateRequestId() As String
        Dim ReqId As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEM_REQUISITION_GetMaxAIR_SNO")
        Dim Id As Integer = CInt(sp.ExecuteScalar())
        Return Session("UID") + "/AST/REQ/" + CStr(Id)
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
        'If fpBrowseDocc.PostedFiles IsNot Nothing Then
        '    For Each File In fpBrowseDocc.PostedFiles
        '        If File.ContentLength = 0 Then
        '            lblMsg.Text = "Upload the Required Attachment"
        '            lblMsg.Visible = True
        '            Exit Sub
        '        End If
        '    Next
        'End If
        Dim ReqId As String = GenerateRequestId()
        Validate(ReqId)
    End Sub
    Private Sub Validate(ByVal ReqId As String)
        Dim VT_Code As String
        Dim Ast_SubCat_Code As String
        Dim manufactuer_code As String
        Dim Ast_Md_code As String
        Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
        Dim Company_Id As Integer = Session("COMPANYID")

        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim count As Integer = 0
        Dim Message As String = String.Empty
        Dim mdcode As String = String.Empty
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim lblmdcode As Label = DirectCast(row.FindControl("lblmdcode"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        '    Dim txtEstCost As TextBox = DirectCast(row.FindControl("txtEstCost"), TextBox)
        '    mdcode = lblmdcode.Text
        '    Dim n As Integer

        '    If chkSelect.Checked Then
        '        If String.IsNullOrEmpty(txtQty.Text) Then
        '            lblMsg.Visible = True
        '            lblMsg.Text = "Enter Quantity for the Selected Checkbox"
        '            Exit Sub

        '        ElseIf IsNumeric(txtQty.Text) = False Then
        '            lblMsg.Visible = True
        '            lblMsg.Text = "Enter Quantity in numbers..."
        '            Exit Sub
        '        ElseIf CInt(txtQty.Text) > 0 Then
        '            count = count + 1
        '            If count = 0 Then
        '                lblMsg.Visible = True
        '                lblMsg.Text = "Enter Required Quantity And Select The Product"
        '                Exit Sub
        '            End If
        '        ElseIf CInt(txtQty.text) <= 10 Then
        '            lblMsg.Visible = True
        '            lblMsg.Text = "Enter Quantity less than 10"
        '            Exit Sub
        '        Else
        '            lblMsg.Text = "Enter more than Zero"
        '        End If

        '    End If

        '        If chkSelect.Checked Then
        '        If String.IsNullOrEmpty(txtEstCost.Text) Then
        '            lblMsg.Visible = True
        '            lblMsg.Text = "Estimated Cost checkbox is Selected, enter the estimated cost; otherwise, leave it at zero."
        '            Exit Sub
        '        ElseIf IsNumeric(txtEstCost.Text) = False Then
        '            lblMsg.Visible = True
        '            lblMsg.Text = "Enter Estimated Cost in numbers..."
        '            Exit Sub
        '        End If
        '    End If

        'Next
        'If count > 0 Then
        'InsertData(ReqId, txtRemarks.Text)
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtEstCost As TextBox = DirectCast(row.FindControl("txtEstCost"), TextBox)
            If chkSelect.Checked Then
                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Enter Quantity for the Selected Checkbox"
                        Exit Sub

                    ElseIf IsNumeric(txtQty.Text) = False Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Enter Quantity in numbers..."
                        Exit Sub
                    ElseIf CInt(txtQty.Text) > 0 Then
                        count = count + 1
                        If count = 0 Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Enter Required Quantity And Select The Product"
                            Exit Sub
                        End If
                    ElseIf CInt(txtQty.Text) <= 10 Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Enter Quantity less than 10"
                        Exit Sub
                    Else
                        lblMsg.Text = "Enter more than Zero"
                    End If

                End If

                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtEstCost.Text) Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Estimated Cost checkbox is Selected, enter the estimated cost; otherwise, leave it at zero."
                        Exit Sub
                    ElseIf IsNumeric(txtEstCost.Text) = False Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Enter Estimated Cost in numbers..."
                        Exit Sub
                    End If
                End If
                If count > 0 Then

                    Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
                    Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
                    Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
                    Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)

                    VT_Code = lbl_vt_code.Text
                    Ast_SubCat_Code = lbl_ast_subcat_code.Text
                    manufactuer_code = lbl_manufactuer_code.Text
                    Ast_Md_code = lbl_ast_md_code.Text

                    InsertDetails(ReqId, lblProductId.Text, CInt(Trim(txtQty.Text)), CInt(Trim(txtEstCost.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code, Company_Id)


                    send_mail(ReqId)
                ElseIf count = 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Sorry! There has been no request made. You didn't choose any products or set the quantity to be greater than zero."
                Else
                    lblMsg.Text = "Enter Quantity for the Selected Checkbox"
                End If
            End If



        Next
        Dim UploadFilesName As String
        'UploadFilesName = UploadFiles()
        Dim filePath As String = ""
        Dim FILE_PATH As String = ""
        Dim orgfilename As String = ""
        Dim repdocdatetime As String = ""


        Dim IC As ImageClas
        Dim i As Int32 = 0
        Dim fileSize As Int64 = 0


        If fpBrowseDocc.PostedFiles IsNot Nothing Then
            Dim countt = fpBrowseDocc.PostedFiles.Count
            While (i < countt)
                fileSize = fpBrowseDocc.PostedFiles(i).ContentLength + fileSize
                i = i + 1
            End While
            If (fileSize > 20971520) Then
                lblMsg.Text = "The maximum size for upload GSB Images was 20 MB."
                Exit Sub
            End If
            For Each File In fpBrowseDocc.PostedFiles
                If File.ContentLength > 0 Then
                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                    filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Upload_Time & "_" & File.FileName
                    File.SaveAs(filePath)
                    IC = New ImageClas With {
                            .FILENAME = File.FileName,
                            .FILEPATH = Upload_Time & "_" & File.FileName
                        }
                    Imgclass.Add(IC)
                End If
            Next
        End If
        InsertData(ReqId, Imgclass, txtRemarks.Text, ddlAstCat.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value, ddlAstBrand.SelectedItem.Value, ddlAstModel.SelectedItem.Value, ddlLocation.SelectedItem.Value, Company_Id)
        Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)


    End Sub




    'Private Function IsNumeric(ByVal txt As String)
    '    Dim value As String = Convert.ToInt32(txt.ToString())
    '    Return value
    'End Function
    Private Sub InsertData(ByVal ReqId As String, ByVal Imgclass As List(Of ImageClas), ByVal Remarks As String, ByVal cat1 As String, ByVal subcat1 As String, ByVal brand1 As String, ByVal Model1 As String, ByVal reqLocation As String, ByVal Company_Id As Integer)
        ''Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AssetRequisition_AddNew")
        ''sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
        ''sp1.Command.AddParameter("@AurId", Session("uid"), DbType.String)
        ''sp1.Command.AddParameter("@Remarks", Remarks, DbType.String)
        ''sp1.Command.AddParameter("@CatId", cat1, DbType.String)
        ''sp1.Command.AddParameter("@AIR_ITEM_SUBCAT", subcat1, DbType.String)
        ''sp1.Command.AddParameter("@AIR_ITEM_BRD", brand1, DbType.String)
        ''sp1.Command.AddParameter("@AIR_ITEM_MOD", Model1, DbType.String)
        ''sp1.Command.AddParameter("@AIR_REQ_LOCATION", reqLocation, DbType.String)
        ''sp1.Command.AddParameter("@COMPANYID", Company_Id, DbType.String)
        ''sp1.Command.AddParameter("@FilesName", UtilityService.ConvertToDataTable(Imgclass), SqlDbType.Structured)
        ''sp1.ExecuteScalar()




        Dim param(9) As SqlParameter

        param(0) = New SqlParameter("@ReqId", SqlDbType.VarChar)
        param(0).Value = ReqId
        param(1) = New SqlParameter("@AurId", SqlDbType.VarChar)
        param(1).Value = Session("uid")
        param(2) = New SqlParameter("@Remarks", SqlDbType.VarChar)
        param(2).Value = Remarks
        param(3) = New SqlParameter("@CatId", SqlDbType.VarChar)
        param(3).Value = cat1
        param(4) = New SqlParameter("@AIR_ITEM_SUBCAT", SqlDbType.VarChar)
        param(4).Value = subcat1
        param(5) = New SqlParameter("@AIR_ITEM_BRD", SqlDbType.VarChar)
        param(5).Value = brand1
        param(6) = New SqlParameter("@AIR_ITEM_MOD", SqlDbType.VarChar)
        param(6).Value = Model1
        param(7) = New SqlParameter("@AIR_REQ_LOCATION", SqlDbType.VarChar)
        param(7).Value = reqLocation
        param(8) = New SqlParameter("@COMPANYID", SqlDbType.VarChar)
        param(8).Value = Company_Id
        param(9) = New SqlParameter("@FilesName", SqlDbType.Structured)
        param(9).Value = UtilityService.ConvertToDataTable(Imgclass)
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AM_USP_AssetRequisition_AddNew", param)

        End Using
    End Sub
    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal EstCost As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String, ByVal Company_Id As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AssetRequisitionDetails_AddNew")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.Command.AddParameter("@EST_COST", EstCost, DbType.Int32)
        sp.Command.AddParameter("@ITEM_TYPE", VT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", AST_SUBCAT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", manufactuer_code, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", AST_MD_CODE, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Company_Id, DbType.String)
        sp.ExecuteScalar()
    End Sub



    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged

        getmakebycatsubcat()
        remarksAndSubmitBtn.Visible = False
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKE_FOR_ASSETGRID")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", IIf(ddlAstBrand.SelectedItem.Value = "No Brand", "ALL", ddlAstBrand.SelectedItem.Value), DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", IIf(ddlAstModel.SelectedItem.Value = "No Model", "ALL", ddlAstModel.SelectedItem.Value), DbType.String)
        sp.Command.AddParameter("@AST_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            remarksAndSubmitBtn.Visible = False
        End If

    End Sub

    Public Sub send_mail(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Execute()
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        'ddlAstModel.Items.Insert(0, New ListItem("No Model", "No Model"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, "--All--")
    End Sub
    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        '    If ddlAstCat.SelectedIndex > 0 Then
        getbrandbycatsubcat()
        remarksAndSubmitBtn.Visible = False
        '   End If
    End Sub
    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 0, 0)
    End Sub
    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        'ddlAstSubCat.Items.Insert(0, "--Select--")
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
        'pnlItems.Visible = False
        'remarksAndSubmitBtn.Visible = False
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        gvItems.Visible = True
        pnlItems.Visible = True
        remarksAndSubmitBtn.Visible = True
        fillgrid()
    End Sub
    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlAstCat.ClearSelection()
        ddlAstSubCat.ClearSelection()
        ddlLocation.ClearSelection()
        pnlItems.Visible = False
        remarksAndSubmitBtn.Visible = False
        getassetsubcategory()
        ddlAstBrand.Items.Clear()
        ddlAstModel.Items.Clear()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = Path.GetFileNameWithoutExtension(fpBrowseDoc.FileName) & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Server.MapPath("~\UploadFiles\"), "\", "\\") & filename
                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(filepath)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only"
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""

                msheet = listSheet(0).ToString()
                mfilename = msheet

                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"

                End If
                Dim snocnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If

                If Trim(LCase(ds.Tables(0).Columns(0).ToString)) <> "location name" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "At Column 1: Column name should be 'Location Name' "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(1).ToString)) <> "asset category" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "At Column 2: Column name should be 'Asset Category'"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(2).ToString)) <> "asset sub category" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "At Column 3: Column name should be 'Asset Subcategory'"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(3).ToString)) <> "asset brand" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "At Column 4: Column name should be 'Asset Brand'"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(4).ToString)) <> "asset model" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "At Column 5: Column name should be 'Asset Model'"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(5).ToString)) <> "quantity" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "At Column 6: Column name should be 'Quantity'"
                    Exit Sub

                End If

                Dim remarks As String = ""

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Location Name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Location Name is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Asset Category" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Category is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Asset Sub Category" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Sub Category is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Asset Brand" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Brand is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Asset Model" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Model is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "Quantity" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Quantity is null or empty, "
                            End If
                        End If


                    Next

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_UPLOAD_CAPITAL_REQ_DATA")
                    sp.Command.AddParameter("@LOC_NAME", ds.Tables(0).Rows(i).Item(0).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_CAT", ds.Tables(0).Rows(i).Item(1).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_SUB_CAT", ds.Tables(0).Rows(i).Item(2).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_BRAND", ds.Tables(0).Rows(i).Item(3).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_MDL", ds.Tables(0).Rows(i).Item(4).ToString, DbType.String)
                    sp.Command.AddParameter("@QUANTITY", ds.Tables(0).Rows(i).Item(5).ToString, DbType.String)
                    sp.Command.AddParameter("@UID", Session("Uid"), DbType.String)
                    sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
                    sp.Command.AddParameter("@REMARKS", remarks, DbType.String)
                    sp.ExecuteScalar()
                    remarks = ""
                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_INSERT_CAPITAL_REQ_DATA")
                sp1.Command.AddParameter("@dummy", 1, DbType.Int32)
                sp1.Command.AddParameter("@AURID", Session("Uid"), DbType.String)
                Dim ds1 As New DataSet
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    GvUpload.DataSource = ds1.Tables(0)
                    GvUpload.DataBind()
                    btnExport.Visible = True
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If ds1.Tables(0).Rows(i).Item("Remarks").ToString() <> "Success" Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Review the remarks and upload the correct data again."
                    Else
                        lblMsg.Visible = True
                        lblMsg.Text = "Data successfully uploaded ..."
                    End If
                Next
                'lblmsg.Visible = True
                'lblmsg.Text = "Data successfully uploaded ..."

            Else
                lblMsg.Text = "Select a file"
            End If
        Catch ex As Exception
            CommonModules.PopUpMessage(ex.Message, Page)
        End Try
    End Sub
End Class
