Imports System.Data
Partial Class Controls_EditSpace
    Inherits System.Web.UI.UserControl
    Dim lcmtemp As String
    Dim bdgtemp As String
    Dim twrtemp As String
    Dim spctemp As String
    Dim flrtemp As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvSpcDetails.Visible = True
            Get_Building()
        End If
    End Sub
    Private Sub Get_Building()
        'Retrieving Building name,id and binding into the Building dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_BUILDING")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlBDG.DataSource = sp.GetDataSet()
        ddlBDG.DataTextField = "BDG_NAME"
        ddlBDG.DataValueField = "BDG_ADM_CODE"
        ddlBDG.DataBind()
        ddlBDG.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub
    Private Sub fillgrid()
        gvSpcDetails.Visible = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SPACE_GRID")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int16)
        sp.Command.AddParameter("@SPC_BDG_ID", ddlBDG.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_TWR_ID", ddlTWR.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_FLR_ID", ddlFLR.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_WNG_ID", ddlWNG.SelectedItem.Value, DbType.String)
        gvSpcDetails.DataSource = sp.GetDataSet()
        gvSpcDetails.DataBind()

        For i As Integer = 0 To gvSpcDetails.Rows.Count - 1
            Dim lnkStatus As LinkButton = CType(gvSpcDetails.Rows(i).FindControl("lnkStatus"), LinkButton)
            If lnkStatus.Text = "1" Then
                lnkStatus.Text = "Active"
            Else
                lnkStatus.Text = "InActive"
            End If
           
        Next

        
    End Sub
    Private Sub fillgrid2()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TOTAL_SPACE1")
        sp1.Command.AddParameter("@SPC_BDG_ID", ddlBDG.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SPC_TWR_ID", ddlTWR.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SPC_FLR_ID", ddlFLR.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SPC_WNG_ID", ddlWNG.SelectedItem.Value, DbType.String)
        sp1.ExecuteScalar()
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        gvSpcLyrTot.DataSource = ds1
        gvSpcLyrTot.DataBind()

        Get_Total()
       
        'Dim indtot As Integer = 0
        'For j As Integer = 0 To gvSpcLyrTot.Rows.Count - 1
        '    Dim lblIndTotal As Label = CType(gvSpcLyrTot.Rows(j).FindControl("lblIndTotal"), Label)
        '    indtot = indtot + Val(lblIndTotal.Text)
        'Next
        'lblTotal.Visible = True
        'lblTotalM.Visible = True
        'lblTotalM.Text = indtot

        ''Dim indtot As Integer = 0
        ''For j As Integer = 0 To ds1.Tables(0).Rows.Count - 1

        ''    indtot = indtot + Val(ds1.Tables(0).Rows(0).Item("TOTAL"))
        ''Next
        'lblTotal.Visible = True
        'lblTotalM.Visible = True
        'lblTotalM.Text = indtot
    End Sub
    Private Sub Get_Total()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SPACE_GET_TOTAL")
        sp1.Command.AddParameter("@SPC_BDG_ID", ddlBDG.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SPC_TWR_ID", ddlTWR.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SPC_FLR_ID", ddlFLR.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@SPC_WNG_ID", ddlWNG.SelectedItem.Value, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        If ds1.Tables(0).Rows.Count > 0 Then
            lblTotal.Visible = True
            lblTotalM.Visible = True
            lblTotalM.Text = ds1.Tables(0).Rows(0).Item("TOTAL")
        End If
    End Sub

    Protected Sub gvSpcDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpcDetails.PageIndexChanging
        gvSpcDetails.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Protected Sub gvSpcDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSpcDetails.RowCommand
        If e.CommandName = "DELETE" Then
            If gvSpcDetails.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvSpcDetails.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As String = lblID.Text
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SPACE_DELETE")
                sp.Command.AddParameter("@id", id, DbType.String)
                sp.ExecuteScalar()
            End If
        ElseIf e.CommandName = "Status" Then
            'Changing the status from active to inactive and vice versa...
            Dim lnkStatus As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkStatus.NamingContainer, GridViewRow)
            Dim status As Integer = CInt(e.CommandArgument)
            Dim lblId As Label = DirectCast(gvRow.FindControl("lblId"), Label)
            Dim id1 As String = lblId.Text
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_UPDATE_SPACE_STATUS")
            sp2.Command.AddParameter("@id1", id1, DbType.String)
            sp2.Command.AddParameter("@status", 1 - status, DbType.Int32)
            sp2.ExecuteScalar()
            gvSpcDetails.Visible = True
            fillgrid()
        End If


        fillgrid()
    End Sub

    Protected Sub gvSpcDetails_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvSpcDetails.RowDeleting

    End Sub
    Protected Sub ddlBDG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBDG.SelectedIndexChanged
        lblTotal.Visible = False
        lblTotalM.Visible = False
        'If ddlBDG.SelectedIndex = 0 Then
        '    'ddlTWR.SelectedIndex = 0
        '    ddlFLR.SelectedIndex = 0
        '    ddlWNG.SelectedIndex = 0
        'End If
        ddlFLR.SelectedIndex = 0
        ddlWNG.SelectedIndex = 0
        'If ddlTWR.SelectedIndex = 0 Then

        '    ddlFLR.SelectedIndex = 0
        '    ddlWNG.SelectedIndex = 0
        'End If
        Get_Tower(ddlBDG.SelectedItem.Value)

        gvSpcDetails.Visible = False
        gvSpcLyrTot.Visible = False
    End Sub
    Private Sub Get_Tower(ByVal bdgtemp As String)
        'Retrieving Tower name,id and binding into the Tower dropdown based on selected building in building dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_TOWER")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        ddlTWR.DataSource = sp.GetDataSet()
        ddlTWR.DataTextField = "TWR_NAME"
        ddlTWR.DataValueField = "TWR_CODE"
        ddlTWR.DataBind()
        ddlTWR.Items.Insert(0, "--Select Tower--")
        'If twrtemp <> "" Then
        '    ddlTWR.Items.FindByValue(twrtemp).Selected = True
        'End If
    End Sub

    Protected Sub ddlTWR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTWR.SelectedIndexChanged
        lblTotal.Visible = False
        lblTotalM.Visible = False

        Get_Floor(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value)
        ddlWNG.SelectedIndex = 0
        ddlFLR.SelectedIndex = 0
        gvSpcDetails.Visible = False
        gvSpcLyrTot.Visible = False
    End Sub
    Private Sub Get_Floor(ByVal bdgtemp As String, ByVal twrtemp As String)
        'Retrieving Floor name,id and binding into the Floor dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_FLOOR")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        ddlFLR.DataSource = sp.GetDataSet()
        ddlFLR.DataTextField = "FLR_NAME"
        ddlFLR.DataValueField = "FLR_CODE"
        ddlFLR.DataBind()
        ddlFLR.Items.Insert(0, "--Select Floor--")
        'If flrtemp <> "" Then
        '    ddlFLR.Items.FindByValue(flrtemp).Selected = True
        'End If
    End Sub

    Protected Sub ddlFLR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFLR.SelectedIndexChanged
        Get_Wing(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value, ddlFLR.SelectedItem.Value)
        'fillgrid()
        ddlWNG.SelectedIndex = 0
        gvSpcDetails.Visible = False
        gvSpcLyrTot.Visible = False
        lblTotal.Visible = False
        lblTotalM.Visible = False
    End Sub
    Private Sub Get_Wing(ByVal bdgtemp As String, ByVal twrtemp As String, ByVal flrtemp As String)
        'Retrieving Space name,id and binding into the Space dropdown
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_WING")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", flrtemp, DbType.String)
        ddlWNG.DataSource = sp.GetDataSet()
        ddlWNG.DataTextField = "WNG_NAME"
        ddlWNG.DataValueField = "WNG_CODE"
        ddlWNG.DataBind()
        ddlWNG.Items.Insert(0, "--Select Wing--")
        'If spctemp <> "" Then
        '    ddlWNG.Items.FindByValue(spctemp).Selected = True
        'End If

    End Sub

    Protected Sub ddlWNG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWNG.SelectedIndexChanged
        gvSpcDetails.Visible = True
        gvSpcLyrTot.Visible = True
        fillgrid()
        fillgrid2()
    End Sub

    Protected Sub gvSpcLyrTot_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSpcLyrTot.PageIndexChanging
        gvSpcLyrTot.PageIndex = e.NewPageIndex
        fillgrid2()
    End Sub
End Class
