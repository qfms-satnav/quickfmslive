<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGVendorModify.ascx.vb" Inherits="Controls_AMGVendorModify" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-10" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
     <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor Code<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfCode" runat="server" ControlToValidate="txtCode" Display="nONE" ErrorMessage="Please Enter Vendor Code" ValidationGroup="Val1">
            </asp:RequiredFieldValidator>
            <asp:TextBox ID="txtCode" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor Name<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                Display="None" ErrorMessage="Please Enter Name !" ValidationGroup="Val1">
            </asp:RequiredFieldValidator>
            <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Address<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                Display="None" ErrorMessage="Please Enter Address !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine" MaxLength="500" Height="30%"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>City<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="cvcity" runat="server" InitialValue="0" ControlToValidate="ddlCity"
                Display="None" ErrorMessage="Please Select City !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="Select City">
            </asp:DropDownList>
        </div>
    </div>
    
<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor Type<span style="color: red;">*</span></label>
            <asp:ListBox ID="lstTypes" CssClass="form-control" SelectionMode="Multiple" runat="server"></asp:ListBox>
        </div>
    </div>
</div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor Category<span style="color: red;">*</span></label>
             <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please Select At Least One Vendor Category."
                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" Display="None" runat="server" ValidationGroup="Val1" />
            <asp:CheckBoxList ID="chkVendorCat" runat="server">
                <asp:ListItem Text="Supplier" Value="Supplier"></asp:ListItem>
                <asp:ListItem Text="Service" Value="Service"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Phone Number<span style="color: red;">*</span></label>
          <%--  <asp:RequiredFieldValidator ID="rfvPhoneNo" runat="server" ControlToValidate="txtPhone"
                Display="None" ErrorMessage="Please Enter Phone Number !" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPhone" ValidationGroup="Val1"
                Display="None" ErrorMessage="Please Enter A Valid Phone Number" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
             <cc1:FilteredTextBoxExtender ID="ftetxtPhone" runat="server" TargetControlID="txtPhone" FilterType="Numbers" ValidChars="0123456789." />
            <asp:TextBox ID="txtPhone" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Mobile<span style="color: red;">*</span></label>
            <%--<asp:RequiredFieldValidator ID="rfMobile" runat="server" ControlToValidate="txtMobile"
                Display="None" ErrorMessage="Please Enter Mobile Number !" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="none" runat="server" ControlToValidate="txtMobile"
                ValidationGroup="Val1" ErrorMessage="Please Enter Valid Mobile Number" ValidationExpression="^[7-9][0-9]{9}"></asp:RegularExpressionValidator>
             <cc1:FilteredTextBoxExtender ID="ftetxtMobile" runat="server" TargetControlID="txtMobile" FilterType="Numbers" ValidChars="0123456789" />
            <asp:TextBox ID="txtMobile" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Email<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfEmail" runat="server" ControlToValidate="txtEmail"
                Display="None" ErrorMessage="Please Enter Email !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <%--<asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="none"
                ValidationGroup="Val1" ErrorMessage="Please Enter Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
            </asp:RegularExpressionValidator>--%>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Bank Name<span style="color: red;">*</span></label>
           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtBankName"
                Display="None" ErrorMessage="Please Enter  Bank Name !" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
            <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>
                Bank A/C No<span style="color: red;">*</span>
            </label>
           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBankAcNo"
                Display="None" ErrorMessage="Please Enter Bank A/C No !" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
            <asp:TextBox ID="txtBankAcNo" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Branch Name<span style="color: red;">*</span></label>
           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBranchName"
                Display="None" ErrorMessage="Please Enter Branch Name !" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>

            <asp:TextBox ID="txtBranchName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    

</div>
<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>IFSC Code<span style="color: red;">*</span></label>
           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtIFSCCode"
                Display="None" ErrorMessage="Please Enter IFSC Code !" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
            <asp:TextBox ID="txtIFSCCode" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>PAN No.<span style="color: red;"></span></label>

            <asp:TextBox ID="txtPANno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>TAN No.<span style="color: red;"></span></label>

            <asp:TextBox ID="txtTANno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
     <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>State<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvmonth" runat="server" ControlToValidate="ddlState"
                Display="none" ErrorMessage="Please Select State" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlState" runat="server"  CssClass="form-control selectpicker" data-live-search="true"
               AutoPostBack="true" ></asp:DropDownList>
        </div>
    </div>
  
   <%-- <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>LST No.<span style="color: red;"></span></label>

            <asp:TextBox ID="txtlstno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>GIR No.<span style="color: red;"></span></label>

            <asp:TextBox ID="txtGIRno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>--%>
</div>
<div class="row">
   <%-- <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>WCT No.<span style="color: red;"></span></label>

            <asp:TextBox ID="txtwctno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>CST No.<span style="color: red;"></span></label>

            <asp:TextBox ID="txtcstno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>ST. Assessment Circle<span style="color: red;"></span></label>

            <asp:TextBox ID="txtstcircle" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>--%>
       <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>GST No.<span style="color: red;">*</span></label>
              <asp:RequiredFieldValidator ID="rggstno" runat="server" ControlToValidate="txtgsttno"
                Display="none" ErrorMessage="Please Enter GST No." ValidationGroup="Val1" ></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtgsttno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Status<span style="color: red;">*</span></label>
            <asp:RadioButtonList ID="rdbtnStatus" runat="server" CssClass="clsRadioButton"
                RepeatDirection="Horizontal">
                <asp:ListItem Value="1" Selected="True">Active &nbsp</asp:ListItem>
                <asp:ListItem Value="0">Inactive</asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" MaxLength="250" Height="30%"></asp:TextBox>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Update" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-default btn-primary" Text="Back" />
        </div>
    </div>
</div>

<%--<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" TextMode="MultiLine" MaxLength="250" Height="30%"></asp:TextBox>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="Button1" CssClass="btn btn-default btn-primary" runat="server" Text="Update" ValidationGroup="Val1" />
            <asp:Button ID="Button2" runat="server" CssClass="btn btn-default btn-primary" Text="Back" />
        </div>
    </div>
</div>--%>
<script type="text/ecmascript">
    function refreshSelectpicker() {
        $("#<%=ddlCity.ClientID%>").selectpicker();

    }
    refreshSelectpicker();
</script>


