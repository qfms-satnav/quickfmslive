<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAssetBrand.ascx.vb" Inherits="Controls_AddAssetBrand" %>

<style type="text/css">
   /* .auto-style1 {
        color: #000000;
    }
    select.form-control:not([size]):not([multiple]) {
        display: block !important;
    }*/
</style>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-10" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6">
        <label class="col-md-2 btn pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Asset Subcategory and Select Modify to modify the existing Asset Subcategory" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="col-md-2 btn pull-left">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Asset Subcategory and Select Modify to modify the existing Asset Subcategory" />
            Modify
        </label>
    </div>
</div>


<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label id="lblAssetBrand" runat="server" visible="False">Asset Brand<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvbr" runat="server" ControlToValidate="ddlBrand"
                Display="none" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="Select Asset Brand" AutoPostBack="True" Visible="False">
            </asp:DropDownList>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Brand/Make Code<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtBrand"
                Display="none" ErrorMessage="Please Enter Brand/Make Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegExpCode" runat="server" ControlToValidate="txtBrand"
                ErrorMessage="Please Enter Brand/Make Code in alphabets,numbers" Display="None" ValidationGroup="Val1">
            </asp:RegularExpressionValidator>

            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group" style="color: #FFFFFF">
            <label>Asset Brand/Make Name<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBrandName"
                Display="none" ErrorMessage="Please Enter Brand/Make Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegExpName" runat="server" ControlToValidate="txtBrandName"
                ErrorMessage="Please Enter Brand/Make Name in alphabets,numbers" Display="None" ValidationGroup="Val1">
            </asp:RegularExpressionValidator>

            <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtBrandName" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Category<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="Select Asset Category" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Sub Category<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                Display="none" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="Select Asset Sub Category" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
</div>

<div class="row" style="text-align: left">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Status</label>
            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="Active"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <%--<asp:ListItem>--Select--</asp:ListItem>--%>
                <asp:ListItem Value="1">Active</asp:ListItem>
                <asp:ListItem Value="0">InActive</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                ErrorMessage="Please Enter Remarks in alphabets,numbers" Display="None"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>

            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-default btn-primary" runat="server" Text="Back" OnClick="btnBack_Click" />
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowSorting="False" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left"
                PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Asset Brand Found." CssClass="table GridStyle" GridLines="none">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("manufacturerID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Brand Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" CssClass="lblCode" Text='<%#BIND("manufacturer")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Brand Code">
                        <ItemTemplate>
                            <asp:Label ID="lblASTCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("manufactuer_code")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Assets Sub Category">
                        <ItemTemplate>
                            <asp:Label ID="lblSubCat" runat="server" CssClass="lblCat" Text='<%#BIND("AST_SUBCAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Category">
                        <ItemTemplate>
                            <asp:Label ID="lblCat" runat="server" CssClass="lblCat" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("MANUFACTURER_STATUS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#d3d3d3" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<script type="text/ecmascript">
    $("#btnSubmit").click(function () {
        $('#lblMsg').text("")
    });
    function refreshSelectpicker() {
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlBrand.ClientID%>").selectpicker();
    }
    refreshSelectpicker();
</script>
