Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Partial Class Controls_ViewPopupPropertyDetails
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not IsPostBack Then
                gvDetails.PageIndex = 0
            End If
            fillgrid()
            gvDetails.Visible = True
        End If
    End Sub

    Private Sub fillgrid()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PRPTYPOPUP_DETAILS")
        sp3.Command.AddParameter("@sno", Request.QueryString("id"), DbType.String)
        Dim ds3 As New DataSet
        ds3 = sp3.GetDataSet()
        gvDetails.DataSource = ds3
        gvDetails.DataBind()
        Dim i As Integer = 0
        For i = 0 To gvDetails.Rows.Count - 1
            Dim lblStatus As Label = CType(gvDetails.Rows(i).FindControl("lblStatus"), Label)
            If lblStatus.Text = "1" Then
                lblStatus.Text = "Active"
            Else
                lblStatus.Text = "Inactive"
            End If
        Next

        For i = 0 To gvDetails.Rows.Count - 1
            Dim lblInsTypel As Label = CType(gvDetails.Rows(i).FindControl("lblInsType"), Label)
            If lblInsTypel.Text = "1" Then
                lblInsTypel.Text = "Building only"
            ElseIf lblInsTypel.Text = "2" Then
                lblInsTypel.Text = "Content only"
            ElseIf lblInsTypel.Text = "3" Then
                lblInsTypel.Text = "Building & Content"
            Else
                lblInsTypel.Text = ""
            End If

        Next

    End Sub

    Protected Sub gvDetails_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails.PageIndexChanging
        gvDetails.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub btnexporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexporttoexcel.Click
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel1.FileName = "Property_Deatails" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss")
    End Sub
End Class












