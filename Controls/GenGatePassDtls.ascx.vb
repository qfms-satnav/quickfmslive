Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_GenGatePassDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim strStat As String

    Dim FBDG_ID, SBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String
    Dim FromLoc, ToLoc, FromLocCode, ToLocCode, RequestedBy, ApprovedBy, ReceivingBy, SendingBy As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            strStat = Request.QueryString("stat")
            getDetailsbyReqId(strReqId)
            'BindRequestAssets(strReqId)
            BindInterRequestAssets(strReqId)
            btnPrint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
        End If
    End Sub


    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)

        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AM_GET_INTER_MVMT_DETLS_BY_REQID", param)
        If dr.Read Then
            FromLoc = dr.Item("FromLoc")
            ToLoc = dr.Item("ToLoc")
            FromLocCode = dr.Item("FromLocCode")
            ToLocCode = dr.Item("ToLocCode")
            RequestedBy = dr.Item("MMR_RAISEDBY")
            ApprovedBy = dr.Item("MMR_APPROVED_BY")
            strremarks = dr.Item("MMR_COMMENTS")
            ReceivingBy = dr.Item("ReceivingPerson")
            lblFromLoc.Text = FromLoc
            lblToLoc.Text = ToLoc

            lblRaisedBy.Text = RequestedBy
            lblapprovedby.Text = ApprovedBy
            lblreceivedby.Text = ReceivingBy


        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If

        'txtPersonName.Text = receiveAst

    End Sub


    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "GET_MVMT_ASTS", param)
    End Sub
    Public Sub BindInterRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "AM_GET_MVMT_ASTS", param)
    End Sub


    Protected Sub btnGetPass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetPass.Click
        Dim strASSET_LIST As New ArrayList
        If Request.QueryString("stat") = "INTRA" Then

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_GETPASS_approved_by", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_GETPASS_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = ""
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1021
            ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_GETPASS", param)
        Else
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_GETPASS_approved_by", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_GETPASS_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = ""
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1021
            'ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_MVMT_getpass", param)
            ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_GETPASS", param)
        End If
        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & lblAAT_NAME.Text)
        Next
        SendMail(Request.QueryString("Req_id"), "GATEPASS")
        Response.Redirect("frmAssetThanks.aspx?RID=gengatepass")
    End Sub

    Private Sub SendMail(ByVal ReqId As String, ByVal Mode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_INTER_MVMT_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", Mode, DbType.String)
        sp.Execute()
    End Sub

  



End Class
