<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RequesitionReport.ascx.vb"
    Inherits="Controls_RequesitionReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Requisition Report
                        </asp:Label>
                        <hr align="center" width="60%" />
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Requisition Report</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        <table width="100%" cellpadding="2px">
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label2" runat="server" CssClass="bodytext" Text="Select Date Range"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="Label1" runat="server" CssClass="bodytext" Text="From #"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtsdate" runat="server"></asp:TextBox><cc1:CalendarExtender ID="sdate"
                                        runat="server" TargetControlID="txtsdate">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label6" runat="server" CssClass="bodytext" Text="To #"></asp:Label></td>
                                <td align="left">
                                    <asp:TextBox ID="txtEdate" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="edate" runat="server" TargetControlID="txtedate">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnView" CssClass="button" runat="server" Text="Submit" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                </td>
                                <td align="left">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:Button ID="btnExpExcel" runat="Server" Text="Export to Excel" CssClass="button" />
                                    <cc1:ExportPanel ID="exportpanel1" runat="server">
                                        <asp:GridView ID="gvItem" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Asset(s) Found." Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Request Id">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkReq_id" runat="server" CommandArgument='<%# Eval("AIR_REQ_ID")%>' CausesValidation="false"  CommandName="ReqId" Text='<%# Eval("AIR_REQ_ID")%>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="AIR_REQ_ID" HeaderText="Request Id" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Date" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="LCM_NAME" HeaderText="Location" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="TWR_NAME" HeaderText="Tower" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="FLR_NAME" HeaderText="Floor" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="REQ_QTY" HeaderText="Requested Qty" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="REC_QTY" HeaderText="Received Qty" ItemStyle-HorizontalAlign="left" />
                                                <asp:TemplateField HeaderText="Pending">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPending" runat="server" Text='<%# Eval("REQ_QTY")-Eval("REC_QTY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </cc1:ExportPanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                        height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                            width="9" /></td>
                    <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                            width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExpExcel" />
    </Triggers>
</asp:UpdatePanel>
