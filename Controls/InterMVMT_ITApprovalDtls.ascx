<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InterMVMT_ITApprovalDtls.ascx.vb"
    Inherits="Controls_InterMVMT_ITApprovalDtls" %>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Location:</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlSLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Person To Receive Assets:</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks:</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Approve"></asp:Button>
            <asp:Button ID="btnReject" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reject" />
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />
        </div>
    </div>
</div>

