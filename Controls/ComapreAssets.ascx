<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ComapreAssets.ascx.vb" Inherits="ComapreAssets" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div style="min-height:425px;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Compare Asset Barcode Data
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="100%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Compare Asset Barcode Data</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
              <asp:Panel ID="panel2" runat="Server" GroupingText="Upload Excel">
                            <table id="tdetails" runat="Server" width="100%" cellpadding="2" cellspacing="0" align="center" border="1">

                              <tr>
    <td colspan="2">
         <asp:HyperLink ID="HyperLink1" runat="server" Text=" Click here to View the Template" NavigateUrl="~/ComapreAssets.xls"></asp:HyperLink>

    </td>
</tr>

        <tr>
          <td align="left" style="height: 26px; width: 50%">Upload excel for Compare Asset Barcode Data (Only Excel )<font class="clsNote">*</font>
       <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                ControlToValidate="fpBrowseDoc" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
         <asp:RegularExpressionValidator ID="revfubrowse" Display="Dynamic" ControlToValidate="fpBrowseDoc"
                   ValidationGroup="Val1" runat="Server" ErrorMessage="Only Excel file allowed"
                                            ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
         </asp:RegularExpressionValidator>
         
                                    </td>
                                    <td align="left" style="height: 26px; width: 50%">
                                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="97%" /></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                      
                                   &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                      <asp:Button ID="btnsubmit" runat="Server" CssClass="button" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                                    </td>
                                </tr>
                                 <tr>
                               
                            </tr>
                  
                            </table>
                        </asp:Panel>
                        
                             
                                    <br/>
                                    
                                   <h5>Asset Comparison Report with Barcode Data</h5>
                                  <br/>
                    
                   
                                    <asp:GridView ID="gvCompare" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" Width="100%"
                                        AllowPaging="True" pagesize="100" AllowSorting="false" AutoGenerateColumns="false">
                                     <Columns>
                                  
                                 <asp:TemplateField HeaderText="Building" ItemStyle-Width="10%">
                                        <ItemTemplate >
                                            
                                            <asp:Label ID="lblBuilding" runat="server" TEXT='<%#Eval("BDG_ID") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tower" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblTower" runat="server" TEXT='<%#Eval("TWR_ID") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Floor" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblFloor" runat="server" TEXT='<%#Eval("FLR_ID") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Space">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lclSpace" runat="server" TEXT='<%#Eval("ASSET_SPACE") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Barcode">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblBarcode" runat="server" TEXT='<%#Eval("BARCODE") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Asset Code">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblBarcode" runat="server" TEXT='<%#Eval("ASSET_CODE") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblStatus" runat="server" TEXT='<%#Eval("ASSET_STATUS") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                    </asp:GridView>
                                     <asp:Button ID="btnExport" runat="server" CssClass="button" Text="Export to Excel" Visible="false" /> 
                                    <br/>
                                    
                                 <%--  <h5> Not existed in the Uploaded Excel but mapped to space id</h5>
                                   <br/>--%>
                                  
<%--                                    <asp:GridView ID="GridView1" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Left" Width="100%"
                                        AllowPaging="True" AllowSorting="false" AutoGenerateColumns="false" Visible=FALSE>
                                     <Columns>
                                  
                                 <asp:TemplateField HeaderText="Building">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblBuilding" runat="server" TEXT='<%#Eval("BDG_ID") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tower">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblTower" runat="server" TEXT='<%#Eval("TWR_ID") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Floor">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblFloor" runat="server" TEXT='<%#Eval("FLR_ID") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Space">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblSpace" runat="server" TEXT='<%#Eval("ASSET_SPACE") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Asset Code">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblAssetCode" runat="server" TEXT='<%#Eval("ASSET_CODE") %>' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            
                                            <asp:Label ID="lblStatus" runat="server" TEXT='NOT EXISTED IN EXCEL' Visible="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                    </asp:GridView>
                    <asp:Button ID="btnExport1" runat="server" CssClass="button" Text="Export to Excel" Visible="false" /> 
                                    <br/>--%>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>