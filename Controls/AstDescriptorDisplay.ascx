<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AstDescriptorDisplay.ascx.vb"
    Inherits="Controls_AstDescriptorDisplay" %>
 
    <div id="flowertabs1" class="modernbricksmenu2">
        <ul>
            <% Dim i As Integer = 1%>
            <%
                For Each desc As Commerce.Common.ProductDescriptor In DescriptorList
            %>
            <% Dim temp As String = "tcontent" & i%>
            <li><a href="#" rel="<%=temp%>" class="selected">
                <%=desc.Title%>
            </a></li>
            <%i = i + 1%>
            <%
            Next desc
            %>
        </ul>
        <div style="clear: both; width: 100%;">
             </div>
    </div>
    <div class="tcontent1" style="background: #d1dcf6; border: 1px solid #7a91c7; padding: 6px; width: 100%;">
        <% Dim j As Integer = 1%>
        <%
            For Each desc As Commerce.Common.ProductDescriptor In DescriptorList
                'here desc.descriptor problem
        %>
        <% Dim againtemp As String = "tcontent" & j%>
        <div id="<%=againtemp%>" class="tabcontent_1">
            <%
                If desc.IsBulletedList Then
            %>
            <ul>
                <%
                    Dim sList As String() = desc.Descriptor.Split(ControlChars.Cr, ControlChars.Lf)
                    For Each listItem As String In sList
                        If (Not String.IsNullOrEmpty(listItem)) Then
                %>
                <li>
                    <%=listItem%>
                </li>
                <%
                End If
            Next listItem
                %>
            </ul>
            <%
            Else
            %>
            <%=desc.Descriptor%>
            <%
            End If
            %>
        </div>
        <%j = j + 1%>
        <%
        Next desc
        %>
    </div>

    <script type="text/javascript">
    var myflowers=new ddtabcontent("flowertabs1")
    myflowers.setpersist(true)
    myflowers.setselectedClassTarget("link") //"link" or "linkparent"
    myflowers.init()
    </script>
 
