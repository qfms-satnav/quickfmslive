Imports System.Data
Imports System.Data.SqlClient

Partial Class Controls_CoCheck
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEM_REQUISITION_GetByStatusId")
        sp.Command.AddParameter("@StatusId", 3015, Data.DbType.Int32)
        sp.Command.AddParameter("@Cuser", Session("UID"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        'lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "search_coordinator_check")
        sp.Command.AddParameter("@search_criteria", txtSearch.Text, Data.DbType.String)
        'sp.Command.AddParameter("@StatusId", 1004, Data.DbType.Int32)
        sp.Command.AddParameter("@StatusId", 3015, Data.DbType.Int32)
        sp.Command.AddParameter("@Cuser", Session("UID"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub
End Class
