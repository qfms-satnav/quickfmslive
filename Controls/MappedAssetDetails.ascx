<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MappedAssetDetails.ascx.vb"
    Inherits="Controls_MappedAssetDetails" %>
<%--<div id="tcontent3" class="tabcontent_1">--%>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvAssets" EmptyDataText="No Assets allocated." runat="server" AllowPaging="true"
            AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField HeaderText="Asset Code">
                    <ItemTemplate>
                        <asp:Label ID="lblAstCode" CssClass="bodytext" runat="server" Text='<%# Eval("AAT_CODE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Name"><%--ItemStyle-HorizontalAlign="Center"--%>
                    <ItemTemplate>
                        <asp:Label ID="lblAstCode" CssClass="bodytext" runat="server" Text='<%# Eval("AAT_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Space Asset Id">
                    <ItemTemplate>
                        <asp:Label ID="lblAstCode" CssClass="bodytext" runat="server" Text='<%# Eval("SPC_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>

