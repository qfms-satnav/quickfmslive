<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ITApproval.ascx.vb" Inherits="Controls_ITApproval" %>
<%--<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <fieldset>
            <legend> Intra Movement Requests  </legend>
            <asp:GridView ID="gvInterMVMT" runat="server" EmptyDataText="No Inter Movement Request Found." AllowPaging="true"
                PageSize="10" CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblMMR_AST_CODE" runat="server" Text='<%#Eval("MMR_AST_CODE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location Code" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSTWR_NAME" runat="server" Text='<%#Eval("FLOC_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location Name" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSFLR_NAME" runat="server" Text='<%#Eval("FLOC_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Employee" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblTEmp" runat="server" Text='<%#Eval("MMR_TOEMP_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Date" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblDATE" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="View" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkApproval" runat="server" CommandArgument='<%#Eval("MMR_REQ_ID") %>'
                                CommandName="Details">Details/Approve</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </fieldset>
    </div>
</div>--%>

<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <fieldset>
            <legend>Inter Movement Requests </legend>
            <asp:GridView ID="gvReqIds" runat="server" EmptyDataText="No Inter Movement Requests Found." AllowPaging="true"
                PageSize="10" CssClass="table GridStyle" GridLines="none" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Request ID" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblMMR_REQ_ID" runat="server" Text='<%#Eval("MMR_REQ_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From Location" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSTWR_NAME" runat="server" Text='<%#Eval("FROMLOC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To Location" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSFLR_NAME" runat="server" Text='<%#Eval("TOLOC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Date" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblDATE" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Details/Approve" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkApproval" runat="server" CommandArgument='<%#Eval("MMR_REQ_ID") %>'
                                CommandName="Details">Details/Approve</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </fieldset>
    </div>
</div>

