﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PaymentMemo.ascx.vb"
    Inherits="Controls_PaymentMemo" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvitems1" runat="server" EmptyDataText="No Payment Memo Found."
            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
            AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Request Id">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="Server" Text='<%#Eval("REQUESTID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested Date">
                    <ItemTemplate>
                        <asp:Label ID="lblAur_id" runat="server" Text='<%#Eval("REQUESTED_ON")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Request Name">
                    <ItemTemplate>
                        <asp:Label ID="lblreqname" runat="server" Text='<%#Eval("REQUESTNAME")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STA_TITLE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField Text="Select" CommandName="View" />
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div id="pnluploaddoc" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row text-center">
                    <strong>DOWNLOAD DOC.</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Category<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvCat" runat="server" ControlToValidate="ddlCategory"
                        Display="none" ErrorMessage="Please Select Category" ValidationGroup="Val3"
                        InitialValue="0"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Document Type<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvDocType" runat="server" ControlToValidate="ddlDocType"
                        Display="none" ErrorMessage="Please Select Document Type" ValidationGroup="Val3"
                        InitialValue="0"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlDocType" runat="server" CssClass="selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val3"
                    CausesValidation="true" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvdoc" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document Category">
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server" Text='<%#Eval("CATEGORY")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document Type">
                        <ItemTemplate>
                            <asp:Label ID="lblDocumentType" runat="server" Text='<%#Eval("DOCTYPE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document">
                        <ItemTemplate>
                            <asp:Label ID="lblViewFilename" Visible="false" runat="server" Text='<%#Eval("DOCTITLE")%>'>
                            </asp:Label>
                            <asp:HyperLink ID="hypbtnDocs" Visible="false" runat="server" Text='View Document'>
                            </asp:HyperLink>
                            <a target="_blank" href='<%=Request.ApplicationPath %>/UploadFiles/<%#Eval("DOC")%>'>
                                <%#Eval("DOCTITLE")%>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField Text="View Document" Visible="false" Target="_blank" DataNavigateUrlFields="RPT_ORG_FILENAME"
                        DataNavigateUrlFormatString='<%= Request.PhysicalApplicationPath %>/UploadFiles'></asp:HyperLinkField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div id="pnljourneydetails" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row text-center">
                    <strong>JOURNEY DETAILS</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvItems" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date">
                        <ItemTemplate>
                            <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("STARTDATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <ItemTemplate>
                            <asp:Label ID="lblenddate" runat="server" Text='<%#Eval("ENDDATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From">
                        <ItemTemplate>
                            <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ORIGIN")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To">
                        <ItemTemplate>
                            <asp:Label ID="lblTo" runat="server" Text='<%#Eval("DESTINATION")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("MODEOFTRAVEL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Travel  Assistance">
                        <ItemTemplate>
                            <asp:Label ID="lblTRAVELASSISTANCE" runat="server" Text='<%#Eval("TRAVELASSISTANCE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time">
                        <ItemTemplate>
                            <asp:Label ID="lblTIME" runat="server" Text='<%#Eval("TIME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <div class="row">
                <asp:TextBox ID="txtStore" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>
            </div>
        </div>
    </div>
</div>
<div id="pnlpaydetails" runat="server" visible="false">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row text-center">
                    <strong>PAYMENT DETAILS</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvpaydetails" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID2" runat="Server" Text='<%#Eval("DETAILS_ID", "{0:c2}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stay">
                        <ItemTemplate>
                            <asp:Label ID="lblStay" runat="server" Text='<%#Eval("STAY", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtstay" runat="server" CssClass="form-control" Text='<%# Eval("STAY")%>'></asp:TextBox>
                            <%--   Text='<%# Eval("INFRASTRUCTURE_COST","{0:c2}") %>'--%>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Food">
                        <ItemTemplate>
                            <asp:Label ID="lblfood" runat="server" Text='<%#Eval("FOOD", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtfood" runat="server" Text='<%#Eval("FOOD")%>' CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Entertainment">
                        <ItemTemplate>
                            <asp:Label ID="lblEntertainment" runat="server" Text='<%#Eval("ENTERTAINMENT", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtentertainment" runat="server" Text='<%#Eval("ENTERTAINMENT")%>'
                                CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Family Expenses">
                        <ItemTemplate>
                            <asp:Label ID="lblexpfamily" runat="server" Text='<%#Eval("EXPOFFAMILY", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtexpfamily" runat="server" Text='<%#Eval("EXPOFFAMILY")%>' CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Conveyance">
                        <ItemTemplate>
                            <asp:Label ID="lblConveyance" runat="server" Text='<%#Eval("CONVEYANCE", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtconveyance" runat="server" Text='<%#Eval("CONVEYANCE")%>' CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Telephone">
                        <ItemTemplate>
                            <asp:Label ID="lbltelephone" runat="server" Text='<%#Eval("TELEPHONE", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txttelephone" runat="server" Text='<%#Eval("TELEPHONE")%>' CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Laundry">
                        <ItemTemplate>
                            <asp:Label ID="lblLaundry" runat="server" Text='<%#Eval("LAUNDRYEXP", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtlaundry" runat="server" Text='<%#Eval("LAUNDRYEXP")%>' CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reimbursement Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblremamount" runat="server" Text='<%#Eval("REMAMOUNT", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtreamount" runat="server" Text='<%#Eval("REMAMOUNT")%>' CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="KMS">
                        <ItemTemplate>
                            <asp:Label ID="lblkms" runat="server" Text='<%#Eval("NOOFKMS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtkms" runat="server" Text='<%#Eval("NOOFKMS")%>' CssClass="form-control"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Update">
                        <ItemTemplate>
                            <asp:Button ID="btnedit" runat="server" Text="Edit" CssClass="btn btn-primary custom-button-color" CommandName="Edit" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnupdate" runat="server" Text="Update" CssClass="btn btn-primary custom-button-color" CommandName="update" />
                            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="btn btn-primary custom-button-color" CommandName="cancel" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div id="pnlpayment" runat="server" visible="false">

    <div class="row">
        <div class="col-md-12">
            <div class="row text-center">
                <div class="form-group">
                    <strong>PAYMENT</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 pull-right">
            <div class="form-group">
                <asp:RadioButtonList ID="rbActions" runat="server" RepeatDirection="Horizontal" CssClass="col-md-3 control-label"
                    AutoPostBack="True" ToolTip="Please Select Check or Cash" OnSelectedIndexChanged="rbActions_SelectedIndexChanged">
                    <asp:ListItem Value="0" Selected="True">Cash</asp:ListItem>
                    <asp:ListItem Value="1">Check</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>

    <div id="pnlcash" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row text-center">
                        <strong>CASH</strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Amount<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtamount"
                            Display="None" ErrorMessage="Please Enter Amount " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegAmt" runat="server" Display="None" ControlToValidate="txtamount" ErrorMessage="Invalid Amount" ValidationGroup="Val1" ValidationExpression="([0-9]+\.[0-9]*)|([0-9]*\.[0-9]+)|([0-9]+)"></asp:RegularExpressionValidator>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtamount" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Comment<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcomment"
                            Display="None" ErrorMessage="Please Enter Comment " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtcomment" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <div class="form-group">
                    <asp:Button ID="btnpay" runat="server" Text="Pay" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                </div>
            </div>
        </div>

    </div>

    <div id="pnlcheck" runat="server" visible="false">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row text-center">
                        <strong>CHECK</strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Bank Name<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtbankname"
                            Display="None" ErrorMessage="Please Enter Bank Name " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtbankname" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Comment<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcommentcheck"
                            Display="None" ErrorMessage="Please Enter Comment " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtcommentcheck" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <asp:Button ID="btnpaycheck" runat="server" Text="Pay" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" />
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


