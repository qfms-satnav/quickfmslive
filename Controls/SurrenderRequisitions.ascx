<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SurrenderRequisitions.ascx.vb"
    Inherits="Controls_SurrenderRequisitions" %>
<script type="text/javascript">
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID)
        for (i = 0; i < form1.elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'checkbox') {
                if (re.test(elm.name)) {
                    if (elm.disabled == false)
                        elm.checked = checkVal
                }
            }
        }
    }
    function ChildClick(CheckBox) {
        //Get target base & child control.
        var TargetBaseControl = document.getElementById('<%= Me.gvSurrenderAstReq.ClientID%>');
        var TargetChildControl = "chkSelect";
        //Get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        // check to see if all other checkboxes are checked
        for (var n = 0; n < Inputs.length; ++n)
            if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                // Whoops, there is an unchecked checkbox, make sure
                // that the header checkbox is unchecked
                if (!Inputs[n].checked) {
                    Inputs[0].checked = false;
                    return;
                }
            }
        // If we reach here, ALL GridView checkboxes are checked
        Inputs[0].checked = true;
    }
    function validateCheckBoxesMyReq() {
        var gridView = document.getElementById("<%=gvSurrenderAstReq.ClientID%>");
        var checkBoxes = gridView.getElementsByTagName("input");
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                return true;
            }
        }
        alert("Please select atleast one checkbox");
        return false;
    }

</script>
<style>
    .form-horizontal .control-label {
        padding-top: 7px;
        margin-bottom: 0;
        /* text-align: right; */
    }

    .alert-danger {
        background-color: white;
        border-color: white;
    }

    .alert {
        position: relative;
        box-sizing: border-box;
        /*padding: 15px;*/
        margin: 10px 10px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label text-start" ForeColor="Red">                  
            </asp:Label>
        </div>
    </div>
</div>
<%--<br />--%>
<div class="row">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                    CssClass="alert alert-danger"
                    ForeColor="Red" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
    <div class="col-md-12 text-left">
        <%--<div class="form-group">--%>
        <div class="row">
            <div class="col-md-3">
                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search By Any..." CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-4">
                <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search"
                    CausesValidation="true" TabIndex="2" />
            </div>
        </div>

        <%-- </div>--%>
        <br />
        <%--<div class="col-md-12 col-sm-12 col-xs-12">--%>
        <asp:GridView ID="gvSurrenderAstReq" runat="server" EmptyDataText="No View Surrender Requisition Found."
            AllowPaging="true" PageSize="10" CssClass="table GridStyle" GridLines="none" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                            ToolTip="Click to check all" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                    </ItemTemplate>
                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("SREQ_ID", "~/FAM/FAM_WebFiles/UpdateSurrenderRequisitions.aspx?Req_id={0}")%>'
                            Text='<%# Eval("SREQ_ID")%> '></asp:HyperLink>
                        <asp:Label ID="lblSREQ_ID" runat="server" Text='<%#Eval("SREQ_ID") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Asset ID" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblAAT_AST_CODE" runat="server" Text='<%#Eval("AAT_AST_CODE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--  <asp:TemplateField HeaderText="Asset Name" Visible="false" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                 <asp:TemplateField HeaderText="Asset Serial No" Visible="true">
                    <ItemTemplate>
                        <asp:Label ID="lblserialno" runat="server" Text='<%#Eval("AAT_AST_SERIALNO")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Location" Visible="false" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Requested Date">
                    <ItemTemplate>
                        <asp:Label ID="lblSurrenderDate" runat="server" Text='<%#Eval("AAT_SURRENDERED_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested By">
                    <ItemTemplate>
                        <asp:Label ID="lblSurBy" runat="server" Text='<%#Eval("AUR_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Surrender Status" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblSurStatus" runat="server" Text='<%#Eval("STA_TITLE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <%-- <asp:TemplateField HeaderText="View" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("SREQ_ID") %>'
                                CommandName="Edit"> View</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<div class="row" id="divlevel" runat="server" visible="False">
    <div class="col-md-3">
        <label id="remarks">Remarks<span style="color: red;">*</span></label>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRM"
            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1" Enabled="true"></asp:RequiredFieldValidator>
        <asp:TextBox ID="txtRM" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <%-- <div class="form-group">--%>
        <asp:Button ID="Approvbtn" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color"
            OnClientClick="if (validateCheckBoxesMyReq() && Page_ClientValidate('Val1')) { this.value='Approving..'; this.disabled=true;}"
            ValidationGroup="Val1" Visible="false" UseSubmitBehavior="false" />
        <asp:Button ID="Rejectbtn" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color"
            OnClientClick="if (validateCheckBoxesMyReq() && Page_ClientValidate('Val1')) { this.value='Rejecting..'; this.disabled=true;}"
            ValidationGroup="Val1" Visible="false" UseSubmitBehavior="false" />
        <%--</div>--%>
    </div>
</div>