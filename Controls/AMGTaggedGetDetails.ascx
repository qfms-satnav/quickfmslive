<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGTaggedGetDetails.ascx.vb"
    Inherits="Controls_AMGTaggedGetDetails" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <script language="javascript" type="text/javascript">
     function maxLength(s,args)
     { 
     if(args.Value.length >= 500)
     args.IsValid=false;
     }
        function CheckAllGridLinkButtons(aspLinkButtonID, checkVal)
        {
            re = new RegExp(aspLinkButtonID)
            for(i = 0; i < form1.elements.length; i++)
            {
                elm = document.forms[0].elements[i]
                if (elm.type == 'linkbutton')
                {
                    if (re.test(elm.name))
                    elm.checked = checkVal
                }
           }
        }
        function CheckDataGrid()
        {
                var k=0;
                for(i = 0; i < form1.elements.length; i++)
                {
                    elm = document.forms[0].elements[i]
                    if (elm.type == 'linkbutton')
                    {
                        if (elm.checked == true)
                        {
                                k=k+1;
                        }
                    }
               }
               if (k==0)
               {
                   window.alert('Please Select atleast one');
                   return false;
               }
               else
               {    var input= confirm("Are you sure you want to fix surrendered date");
                    if(input==true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
               }
        }
        </script>
        
 <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<table width="98%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">View Asset Tagged Details
             <hr align="center" width="60%" /></asp:Label>
            &nbsp;
            <br />
        </td>
    </tr>
</table>
<asp:Panel ID="panel1" runat="server" Width="90%">
<table id="paneltable" cellspacing="0" cellpadding="0" style="vertical-align: top;"
    width="90%" align="center" border="0">
    <tr>
        <td>
            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
        <td width="100%" class="tableHEADER" align="left">
            &nbsp;<strong>Asset Tag Details</strong></td>
        <td>
            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
    </tr>
    <tr>
        <td background="../../Images/table_left_mid_bg.gif">
            &nbsp;</td>
        <td align="left">
               <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
            <table width="80%" cellpadding="3" cellspacing="0" align="center" border="0">
                <tr>
                    <td align="left" style="height: 26px" width="25%">
                        Enter Code to find Asset Tag 
                    </td>
                    <td align="left" style="height: 30px" width="25%">
                        <asp:TextBox ID="txtfindcode" runat="server" CssClass="clsTextField" Width="90%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rffindcode" runat="server" ControlToValidate="txtfindcode"
                            ErrorMessage="Please enter Asset Tag Code !" ValidationGroup="Val1" SetFocusOnError="True"
                            Display="none"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" style="height: 30px" width="5%" valign="top">
                        <asp:Button ID="btnfincode" runat="server" CssClass="button" Text="GO" ValidationGroup="Val1" />
                    </td>
                <td align="LEFT" style="height: 30px" width="25%" valign="top">
                            <asp:LinkButton ID="lbtn1" runat="Server" Text="Get All Records"></asp:LinkButton>
                        </td>
                
                </tr>
                
            </table>
            <table class="table" cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td align="left" style="height:20px">
                        <asp:GridView ID="gvDetails_TAG" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                            AllowPaging="True" PageSize="5" EmptyDataText="No Records Found">
                            <Columns>
                                <asp:TemplateField HeaderText="AST ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblID_TAG" runat="server" CssClass="lblAST ID" Text='<%#Eval("AST_SNO")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCode_TAG" runat="server" CssClass="lblASTCode" Text='<%#Eval("AAT_AST_CODE")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Employee">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpID_TAG" runat="server" CssClass="lblEMPID" Text='<%#Eval("AAT_EMP_ID")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Co-Owner">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCoonrID_TAG" runat="server" CssClass="lblCoonrID" Text='<%#Eval("AAT_COONR_ID")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Allocated Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAllocated_TAG" runat="server" CssClass="lblAllocated" Text='<%#Eval("AAT_ALLOCATED_DATE")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Surrendered Date">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtSurrendered_TAG" runat="server" CssClass="clsTextField" Width="85%" Text='<%#Eval("AAT_SURRENDERED_DATE")%>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="lnkStatus" Text='<% #bind("AAT_TAG_STATUS") %>' CommandArgument='<%#BIND("AAT_TAG_STATUS") %>'
                                                         CausesValidation="False" CommandName="UpdateStatus"></asp:LinkButton>
                                                    <asp:Label runat="server" ID="Label2" Text='<% #bind("AAT_TAG_STATUS") %>' Visible="FALSE"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
            &nbsp;</td>
    </tr>
    <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
</table>
</asp:Panel>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="gvDetails_TAG" />
    </Triggers>
</asp:UpdatePanel>