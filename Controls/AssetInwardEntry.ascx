<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetInwardEntry.ascx.vb"
    Inherits="Controls_AssetInwardEntry" %>
<script type="text/javascript">
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID)
        for (i = 0; i < form1.elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'checkbox') {
                if (re.test(elm.name)) {
                    if (elm.disabled == false)
                        elm.checked = checkVal
                }
            }
        }
    }
    function ChildClick(CheckBox) {
        //Get target base & child control.
        var TargetBaseControl = document.getElementById('<%= Me.GridView1.ClientID%>');
        var TargetChildControl = "chkSelect";
        //Get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        // check to see if all other checkboxes are checked
        for (var n = 0; n < Inputs.length; ++n)
            if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                // Whoops, there is an unchecked checkbox, make sure
                // that the header checkbox is unchecked
                if (!Inputs[n].checked) {
                    Inputs[0].checked = false;
                    return;
                }
            }
        // If we reach here, ALL GridView checkboxes are checked
        Inputs[0].checked = true;
    }
    function setup() {
        $("[ID$=fromdate]").datepicker({
            //startDate: new Date(),
            format: 'mm/dd/yyyy',
            autoclose: true,
            showOn: 'focus'
        });
    };

</script>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <label class="col-md-12 control-label">
                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                </label>
            </div>
        </div>
    </div>
</div>
<%--<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Vendor<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVendor"
                    Display="none" ErrorMessage="Please Select Vendor !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>--%>

<%--    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">PO<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlPO"
                    Display="none" ErrorMessage="Please Select PO !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlPO" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>--%>
<%--<div id="pnlItems" runat="server" visible="false">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <h4>Assets List</h4>
            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>--%>

<%--  <asp:TemplateField HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblReqId" runat="server" Text='<%#Eval("AIPD_ITMREQ_ID") %>' Visible="TRUE"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAst_code" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="TRUE"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
<%-- <asp:BoundField DataField="AST_MD_CODE" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" />
                    <asp:BoundField DataField="AST_MD_NAME" HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" />
                    <asp:BoundField DataField="LOCATION" HeaderText="Asset Requested Location" ItemStyle-HorizontalAlign="left" />
                    <asp:TemplateField HeaderText="PO Qty." ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPOQty" runat="server" CssClass="form-control" ReadOnly="true" MaxLength="5" Text='<%#Eval("AIPD_ACT_QTY") %>'></asp:TextBox>
                            <asp:Label ID="lblProductId1" runat="server" Text='<%#Eval("AST_MD_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Received Qty." ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:CompareValidator ID="valQtyNumeric" runat="server" ControlToValidate="txtQty"
                                Display="none" SetFocusOnError="true" Text="" ErrorMessage="Qty must be a number!"
                                Operator="DataTypeCheck" Type="Integer" ValidationGroup="Val">              
                            </asp:CompareValidator>
                            <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="Recieved Quantity must be a number! " Display="None"
                                ControlToValidate="txtQty" ValidationExpression="^[0-9]+" ValidationGroup="Val1" />
                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" MaxLength="5" Text=""></asp:TextBox>
                            <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
<%--   <asp:TemplateField HeaderText="Invoice Date" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <div class='input-group date' id='fromdate'>
                                <%--<asp:RequiredFieldValidator ID="rfinvDate" runat="server" ControlToValidate="txtInvoiceDate"
                                Display="None" ErrorMessage="Please Select Invoice Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
<%-- <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>--%>
<%--   </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Invoice Number" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtInvoiceNumber" runat="server" CssClass="form-control" Text="" ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Invoice Value" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtInvoiceValue" runat="server" CssClass="form-control" Text="" MaxLength="10"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                ToolTip="Click to check all" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                    </asp:TemplateField>--%>
<%--          </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>--%>
<%--  <div class="row" style="padding-top: 20px">
        <div class="col-md-12 text-right">
            <div class="form-group">
                 <asp:Button ID="btnedit" runat="server" Text="Edit PO" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClick="btnedit_Click"  />
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClick="btnSubmit_Click" />
            </div>
        </div>
    </div>
</div>--%>
<div id="pnlItems2" runat="server">

    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <h4>Assets List</h4>
            <div class="row">
                <div class="col-md-12 text-left">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search By Any..." CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search"
                                    CausesValidation="true" TabIndex="2" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                EmptyDataText="No Asset(s) Found." CssClass="table GridStyle" GridLines="none">

                <Columns>

                    <asp:TemplateField HeaderText="PO Id" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblPOID" runat="server" Text='<%#Eval("POID") %>' Visible="TRUE"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblReqId" runat="server" Text='<%#Eval("AIPD_ITMREQ_ID") %>' Visible="TRUE"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAst_code" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="TRUE"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AST_MD_CODE" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" />
                    <asp:BoundField DataField="AST_MD_NAME" HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" />
                    <asp:BoundField DataField="LOCATION" HeaderText="Asset Requested Location" ItemStyle-HorizontalAlign="left" />
                    <asp:TemplateField HeaderText="PO Qty." ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPOQty" runat="server" CssClass="form-control" ReadOnly="true" MaxLength="5" Text='<%#Eval("AIPD_ACT_QTY") %>'></asp:TextBox>
                            <asp:Label ID="lblProductId1" runat="server" Text='<%#Eval("AST_MD_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Received Qty." ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:CompareValidator ID="valQtyNumeric" runat="server" ControlToValidate="txtQty"
                                Display="none" SetFocusOnError="true" Text="" ErrorMessage="Qty must be a number!"
                                Operator="DataTypeCheck" Type="Integer" ValidationGroup="Val">              
                            </asp:CompareValidator>
                            <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="Recieved Quantity must be a number! " Display="None"
                                ControlToValidate="txtQty" ValidationExpression="^[0-9]+" ValidationGroup="Val1" />
                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" MaxLength="5" Text=""></asp:TextBox>
                            <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Date" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <div class='input-group date' id='fromdate'>
                                <%--<asp:RequiredFieldValidator ID="rfinvDate" runat="server" ControlToValidate="txtInvoiceDate"
                                Display="None" ErrorMessage="Please Select Invoice Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Number" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtInvoiceNumber" runat="server" CssClass="form-control" Text=""></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Invoice Value" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:TextBox ID="txtInvoiceValue" runat="server" CssClass="form-control" Text="" MaxLength="10"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                ToolTip="Click to check all" />

                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                    </asp:TemplateField>

                    <%--                       <asp:TemplateField  ShowHeader="False" HeaderText="PO Id" ItemStyle-HorizontalAlign="left" >
                        <ItemTemplate>
                            <asp:Label ID="lblPOID" runat="server" Text='<%#Eval("POID") %>' Visible="TRUE"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
    <div>
    </div>


    <div class="row" style="padding-top: 20px">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <%--      <asp:Button ID="btnedit" runat="server" Text="Edit PO" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClick="btnedit_Click"  />--%>
                <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClick="Button1_Click" />
                <%--                 <asp:Button ID="btnSubmit2" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" OnClick="btnSubmit2_Click" />--%>
            </div>
        </div>
    </div>
</div>
<%--<script>
    function refreshSelectpicker() {
        $("#<%=ddlPO.ClientID%>").selectpicker();
        $("#<%= ddlVendor.ClientID%>").selectpicker();
   
    }
    refreshSelectpicker();
</script>--%>