Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_UpdateConsumablesstock
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                FillDetails()
            End If
        End If
    End Sub

    Private Sub FillDetails()
        Dim dr As SqlDataReader
        Dim StockId As Integer
        StockId = Request.QueryString("sid")
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@STOCKID", SqlDbType.NVarChar, 200)
        param(0).Value = StockId
        dr = ObjSubsonic.GetSubSonicDataReader("GET_CONSUMABLEASTDETAILS", param)
        If dr.Read Then
            lblLocation.Text = dr.Item("LCM_NAME")
            lblTower.Text = dr.Item("TWR_NAME")
            lblFloor.Text = dr.Item("FLR_NAME")
            lblAsset.Text = dr.Item("AIM_CODE") & " (" & dr.Item("AIM_NAME") & ")"
            'txtAstQty.Text = dr.Item("AID_AVLBL_QTY")
            lblpreviousStock.Text = dr.Item("AID_AVLBL_QTY")
            'txtStockAlert.Text = dr.Item("CAS_STOCKALERT")

        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim StockId As Integer
        Dim param(3) As SqlParameter

        If IsNumeric(txtAstQty.Text) = False Then
            txtAstQty.Text = "0"
        End If
        
        Dim AstQty As Integer


        AstQty = Val(txtAstQty.Text) + Val(lblpreviousStock.Text)
        




        StockId = Request.QueryString("sid")
        param(0) = New SqlParameter("@STOCK_ID", SqlDbType.Int)
        param(0).Value = StockId
        param(1) = New SqlParameter("@QTY", SqlDbType.Int)
        param(1).Value = AstQty
        param(2) = New SqlParameter("@NEW_QTY", SqlDbType.Int)
        param(2).Value = Val(txtAstQty.Text)
        param(3) = New SqlParameter("@REMARKS", SqlDbType.NVarChar, 2000)
        param(3).Value = txtRemarks.Text

        ObjSubsonic.GetSubSonicExecute("UPDATE_STOCK", param)

        Response.Redirect("ViewConsumablesstock.aspx")

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("ViewConsumablesstock.aspx")
    End Sub
End Class
