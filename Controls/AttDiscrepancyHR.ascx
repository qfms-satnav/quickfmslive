<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AttDiscrepancyHR.ascx.vb"
    Inherits="Controls_AttDiscrepancyHR" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Employee<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlEmployee"
                    Display="None" ErrorMessage="Please Select Employee" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select From Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvfrm" runat="server" ControlToValidate="txtFrmDate"
                    ErrorMessage="Please select From Date" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtFrmDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select To Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                    ErrorMessage="Please select To Date" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='todate'>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"
                CausesValidation="True" />

        </div>
    </div>
</div>


<div id="tablegrid1" runat="server">

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnExportExcel" runat="server" Text="Export to Excel" CssClass="btn btn-primary custom-button-color" OnClick="btnExportExcel_Click" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <cc1:ExportPanel ID="ExportPanel" runat="server">
                    <asp:GridView ID="gvItems" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                        EmptyDataText="No Attendance Histroy Found." OnPageIndexChanging="gvItems_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Associate">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssociate" runat="Server" Text='<%#Eval("ASSOCIATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%#Eval("TODAYDATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="In-Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblInTime" runat="server" Text='<%#Eval("INTIME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Out-Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblOutTime" runat="server" Text='<%#Eval("OUTTIME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ExtraTime">
                                <ItemTemplate>
                                    <asp:Label ID="lblExtraTime" runat="server" Text='<%#Eval("EXTRA") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Shortfall">
                                <ItemTemplate>
                                    <asp:Label ID="lblshortfall" runat="server" Text='<%#Eval("SHORTFALL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Late">
                                <ItemTemplate>
                                    <asp:Label ID="lblLate" runat="server" Text='<%#Eval("LATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </cc1:ExportPanel>
            </div>
        </div>
    </div>
</div>



