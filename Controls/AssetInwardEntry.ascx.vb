Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.IO
Imports System.Threading
Imports System.Data.OleDb
Imports System.Globalization
Partial Class Controls_AssetInwardEntry
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            'bindvendor()
            bindgrid1()
            pnlItems2.Visible = True
            lblMsg.Visible = False
        End If
    End Sub
    Private Sub bindgrid1()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSETSFORFINALIZEDPO1")

        Dim ds As DataSet
        ds = sp.GetDataSet()
        GridView1.DataSource = ds
        GridView1.DataBind()
    End Sub

    'Private Sub bindpos()

    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FINILIZEDPOS")
    '    sp.Command.AddParameter("@AIP_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
    '    ddlPO.DataSource = sp.GetDataSet()
    '    ddlPO.DataTextField = "CODE1"
    '    ddlPO.DataValueField = "CODE2"
    '    ddlPO.DataBind()
    '    ddlPO.Items.Insert(0, "--Select--")
    'End Sub

    'Private Sub bindvendor()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FINILIZEDPOSVENDOR")
    '    sp.Command.AddParameter("@dummy", 1, DbType.Int32)
    '    sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '    ddlVendor.DataSource = sp.GetDataSet()
    '    ddlVendor.DataTextField = "AVR_NAME"
    '    ddlVendor.DataValueField = "AVR_CODE"
    '    ddlVendor.DataBind()
    '    ddlVendor.Items.Insert(0, "--Select--")

    'End Sub

    'Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
    '    lblMsg.Text = " "
    '    If ddlVendor.SelectedIndex > 0 Then
    '        bindpos()
    '        gvItems.Visible = False
    '        pnlItems.Visible = False
    '    Else
    '        pnlItems.Visible = False
    '        ddlPO.SelectedIndex = 0
    '        lblMsg.Visible = False
    '        gvItems.Visible = False
    '        pnlItems.Visible = False

    '    End If
    'End Sub

    'Protected Sub ddlPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPO.SelectedIndexChanged
    '    lblMsg.Text = " "
    '    If ddlPO.SelectedIndex > 0 Then
    '        gvItems.Visible = True
    '        pnlItems.Visible = True
    '        bindgrid()
    '    Else
    '        pnlItems.Visible = False
    '        lblMsg.Visible = False
    '        gvItems.Visible = False
    '        pnlItems.Visible = False
    '    End If

    'End Sub

    'Private Sub bindgrid()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASSETSFORFINALIZEDPO")
    '    sp.Command.AddParameter("@AIP_AVR_CODE", ddlVendor.SelectedItem.Value, DbType.String)
    '    sp.Command.AddParameter("@AIPD_PO_ID", ddlPO.SelectedItem.Value, DbType.String)
    '    Dim ds As DataSet
    '    ds = sp.GetDataSet()
    '    gvItems.DataSource = ds
    '    gvItems.DataBind()
    'End Sub

    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim count As Integer = 0
    '    'Dim invoicdat As DateTime
    '    For Each row As GridViewRow In gvItems.Rows
    '        Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
    '        Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
    '        Dim txtPOQty As TextBox = DirectCast(row.FindControl("txtPOQty"), TextBox)
    '        Dim lblProductId1 As Label = DirectCast(row.FindControl("lblAst_code"), Label)
    '        Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
    '        Dim txtInvoiceDate As TextBox = DirectCast(row.FindControl("txtInvoiceDate"), TextBox)
    '        Dim txtInvoiceValue As TextBox = DirectCast(row.FindControl("txtInvoiceValue"), TextBox)
    '        Dim txtInvoiceNumber As TextBox = DirectCast(row.FindControl("txtInvoiceNumber"), TextBox)
    '        'If Not txtInvoiceDate.Text = "" Then
    '        '    invoicdat = DateTime.ParseExact(txtInvoiceDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
    '        'End If
    '        If chkSelect.Checked Then
    '            If String.IsNullOrEmpty(txtQty.Text) Then
    '                lblMsg.Visible = True
    '                lblMsg.Text = "Enter Quantity for the Selected Checkbox"
    '                Exit Sub
    '            ElseIf Integer.Parse(txtQty.Text) > Integer.Parse(txtPOQty.Text) Then
    '                lblMsg.Visible = True
    '                lblMsg.Text = "Enter Correct Received Quantity for selected checkbox"
    '                Exit Sub
    '            ElseIf Integer.Parse(txtQty.Text) = 0 Then
    '                lblMsg.Visible = True
    '                lblMsg.Text = "Quantity should be greater than zero"
    '                Exit Sub
    '            ElseIf (txtInvoiceDate.Text) = "" Then
    '                lblMsg.Visible = True
    '                lblMsg.Text = "Enter Invoice Date"
    '                Exit Sub
    '            Else
    '                count = count + 1
    '                UpdatePODetails(lblProductId1.Text, ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text), lblReqId.Text, txtInvoiceDate.Text, txtInvoiceValue.Text, txtInvoiceNumber.Text)
    '                'UpdatePODetails(Integer.Parse(lblProductId1.Text), ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text), lblReqId.Text)
    '            End If

    '        End If

    '    Next

    '    If count = 0 Then
    '        lblMsg.Visible = True
    '        lblMsg.Text = "Select the Asset(s)"
    '        Exit Sub
    '    End If
    'End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim count As Integer = 0
        'Dim invoicdat As DateTime
        For Each row As GridViewRow In GridView1.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPOQty As TextBox = DirectCast(row.FindControl("txtPOQty"), TextBox)
            Dim lblProductId1 As Label = DirectCast(row.FindControl("lblAst_code"), Label)
            Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
            Dim txtInvoiceDate As TextBox = DirectCast(row.FindControl("txtInvoiceDate"), TextBox)
            Dim txtInvoiceValue As TextBox = DirectCast(row.FindControl("txtInvoiceValue"), TextBox)
            Dim txtInvoiceNumber As TextBox = DirectCast(row.FindControl("txtInvoiceNumber"), TextBox)
            Dim lblPOID As Label = DirectCast(row.FindControl("lblPOID"), Label)
            'If Not txtInvoiceDate.Text = "" Then
            '    invoicdat = DateTime.ParseExact(txtInvoiceDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
            'End If
            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Enter Quantity for the Selected Checkbox"
                    Exit Sub
                ElseIf Integer.Parse(txtQty.Text) > Integer.Parse(txtPOQty.Text) Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Enter Correct Received Quantity for selected checkbox"
                    Exit Sub
                ElseIf Integer.Parse(txtQty.Text) = 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Quantity should be greater than zero"
                    Exit Sub
                ElseIf (txtInvoiceDate.Text) = "" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Enter Invoice Date"
                    Exit Sub
                Else
                    count = count + 1
                    '
                    '        

                    'Dim POValue As String = If(ddlPO.SelectedIndex.ToString() = "0", ddlPO.SelectedItem.Value, lblPOID.Text)

                    UpdatePODetails(lblProductId1.Text, lblPOID.Text, (txtQty.Text), lblReqId.Text, txtInvoiceDate.Text, txtInvoiceValue.Text, txtInvoiceNumber.Text)



                    'UpdatePODetails(Integer.Parse(lblProductId1.Text), ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text), lblReqId.Text)


                End If

            End If
            'UpdatePODetails(lblProductId1.Text, lblPOID.Text, Integer.Parse(txtQty.Text), lblReqId.Text, txtInvoiceDate.Text, txtInvoiceValue.Text, txtInvoiceNumber.Text)

        Next

        If count = 0 Then
            lblMsg.Visible = True
            lblMsg.Text = "Select the Asset(s)"
            Exit Sub
        End If
    End Sub
    'Protected Sub btnedit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Response.Redirect("~/FAM/FAM_WebFiles/frmPOFinalisationDetails.aspx?RID=" + ddlPO.SelectedValue + "")
    'End Sub

    Private Sub UpdatePODetails(ByVal productid As String, ByVal po As String, ByVal rcvdqty As Integer, ByVal reqid As String, ByVal txtInvoiceDate As DateTime, ByVal InvoiceAmt As String, ByVal InvoiceNum As String)
        'Dim invoicdat As DateTime
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_UPDATE_PODETAILSRECEIVED_NP")
        sp.Command.AddParameter("@AIPD_AST_CODE", productid, DbType.String)
        sp.Command.AddParameter("@AIPD_PO_ID", po, DbType.String)
        sp.Command.AddParameter("@AIPD_RCD_QTY", rcvdqty, DbType.Int32)
        sp.Command.AddParameter("@AIPD_ITMREQ_ID", reqid, DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@AIPD_INVOICE_DATE", txtInvoiceDate, DbType.DateTime)
        sp.Command.AddParameter("@AIPD_INVOICE_AMOUNT", InvoiceAmt, DbType.String)
        sp.Command.AddParameter("@AIPD_INVOICE_NO", InvoiceNum, DbType.String)
        Dim flag As Integer = sp.ExecuteScalar()
        lblMsg.Visible = True
        lblMsg.Text = "Asset Inward process completed..."
        'ddlPO.ClearSelection()
        'ddlVendor.ClearSelection()
        pnlItems2.Visible = False
        If flag = 0 Then
            send_mail_Inward_Entry(po, "Completed")
        Else
            send_mail_Inward_Entry(po, "Pending")
        End If
    End Sub

    Public Sub send_mail_Inward_Entry(ByVal POnum As String, ByVal sta As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_INWARD_ENTRY")
        sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
        sp.Command.AddParameter("@STATUS", sta, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Execute()
    End Sub

    Protected Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        bindgrid1()
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "searchByLoc")
        sp.Command.AddParameter("@search_criteria", txtSearch.Text, Data.DbType.String)
        'sp.Command.AddParameter("@AURID", Session("UID"), DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        GridView1.DataSource = ds
        GridView1.DataBind()
    End Sub

End Class
