Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_TrvlChangeRequisiton
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindCategory()
        End If
    End Sub
    Private Sub BindCategory()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CATEGORY_GET")
            ddlCategory.DataSource = sp.GetDataSet()
            ddlCategory.DataTextField = "CATEGORY"
            ddlCategory.DataValueField = "SNO"
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If ddlCategory.SelectedIndex > 0 And ddlDocType.SelectedIndex > 0 Then
                BindGrid()
                gvItems.Visible = True
            Else
                gvItems.Visible = False
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        If ddlCategory.SelectedIndex > 0 Then
            BindDocType()
        Else
            ddlDocType.Items.Clear()
            ddlDocType.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlDocType.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindDocType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_CATEGORY")
            sp.Command.AddParameter("@CATEGORY", ddlCategory.SelectedItem.Value, DbType.Int32)
            ddlDocType.DataSource = sp.GetDataSet()
            ddlDocType.DataTextField = "DOC_TYPE"
            ddlDocType.DataValueField = "SNO"
            ddlDocType.DataBind()
            ddlDocType.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOCUMENTS_VIEW")
            sp.Command.AddParameter("@CATEGORY", ddlCategory.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@DOC_TYPE", ddlDocType.SelectedItem.Value, DbType.Int32)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvItems.DataSource = ds
            gvItems.DataBind()
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim hypbtnDocs As HyperLink = CType(gvItems.Rows(i).FindControl("hypbtnDocs"), HyperLink)
                Dim lblViewFilename As Label = CType(gvItems.Rows(i).FindControl("lblViewFilename"), Label)
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & lblViewFilename.Text
                hypbtnDocs.NavigateUrl = filePath
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
                Dim lbldoc As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lbldoc"), Label)
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & lbldoc.Text

                If System.IO.File.Exists(filePath) = True Then
                    System.IO.File.Delete(filePath)
                End If

                Dim id As String = Integer.Parse(lblID.Text)
                Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOCUMENTS_DELETE")
                SP.Command.AddParameter("@SNO", id, DbType.Int32)
                SP.ExecuteScalar()
            Next
        End If
        BindGrid()
        gvItems.Visible = True
    End Sub

    Protected Sub gvItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItems.RowDeleting

    End Sub
End Class
