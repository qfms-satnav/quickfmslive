<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LveChangeRequisitionDetails.ascx.vb" Inherits="Controls_LveChangeRequisitionDetails" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDep" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateID" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlRM" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contact Number<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvCNo" runat="server" Display="none" ErrorMessage="Please Enter Contact Number"
                    ControlToValidate="txtContactNo" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revCNo" runat="Server" Display="none" ErrorMessage="Please Enter Numerics for Contact Number"
                    ControlToValidate="txtContactNo" ValidationExpression="^[0-9]+$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvstartdate" runat="server" ControlToValidate="txtFromDate"
                    Display="None" ErrorMessage="Please Pick FromDate " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox of FromDate to Pick the date')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control"></asp:TextBox>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ControlToValidate="txtToDate"
                    Display="None" ErrorMessage="Please Pick To Date " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox of To Date to Pick the date')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="tr1" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">From Time(hh:mm)<span style="color: red;">*</span></label>


                    <div class="col-md-7">
                        <asp:DropDownList ID="cboHr" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="false">
                            <asp:ListItem Value="0">-HH-</asp:ListItem>
                            <asp:ListItem Value="1">0</asp:ListItem>
                            <asp:ListItem Value="2">1</asp:ListItem>
                            <asp:ListItem Value="3">2</asp:ListItem>
                            <asp:ListItem Value="4">3</asp:ListItem>
                            <asp:ListItem Value="5">4</asp:ListItem>
                            <asp:ListItem Value="6">5</asp:ListItem>
                            <asp:ListItem Value="7">6</asp:ListItem>
                            <asp:ListItem Value="8">7</asp:ListItem>
                            <asp:ListItem Value="9">8</asp:ListItem>
                            <asp:ListItem Value="10">9</asp:ListItem>
                            <asp:ListItem Value="11">10</asp:ListItem>
                            <asp:ListItem Value="12">11</asp:ListItem>
                            <asp:ListItem Value="13">12</asp:ListItem>
                            <asp:ListItem Value="14">13</asp:ListItem>
                            <asp:ListItem Value="15">14</asp:ListItem>
                            <asp:ListItem Value="16">15</asp:ListItem>
                            <asp:ListItem Value="17">16</asp:ListItem>
                            <asp:ListItem Value="18">17</asp:ListItem>
                            <asp:ListItem Value="19">18</asp:ListItem>
                            <asp:ListItem Value="20">19</asp:ListItem>
                            <asp:ListItem Value="21">20</asp:ListItem>
                            <asp:ListItem Value="22">21</asp:ListItem>
                            <asp:ListItem Value="23">22</asp:ListItem>
                            <asp:ListItem Value="24">23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="cboMin" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="false">
                            <asp:ListItem Value="0">-MM-</asp:ListItem>
                            <asp:ListItem Value="00">00</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="45">45</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">To Time(hh:mm)<span style="color: red;">*</span></label>


                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlHH" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="false">
                            <asp:ListItem Value="0">-HH-</asp:ListItem>
                            <asp:ListItem Value="1">0</asp:ListItem>
                            <asp:ListItem Value="2">1</asp:ListItem>
                            <asp:ListItem Value="3">2</asp:ListItem>
                            <asp:ListItem Value="4">3</asp:ListItem>
                            <asp:ListItem Value="5">4</asp:ListItem>
                            <asp:ListItem Value="6">5</asp:ListItem>
                            <asp:ListItem Value="7">6</asp:ListItem>
                            <asp:ListItem Value="8">7</asp:ListItem>
                            <asp:ListItem Value="9">8</asp:ListItem>
                            <asp:ListItem Value="10">9</asp:ListItem>
                            <asp:ListItem Value="11">10</asp:ListItem>
                            <asp:ListItem Value="12">11</asp:ListItem>
                            <asp:ListItem Value="13">12</asp:ListItem>
                            <asp:ListItem Value="14">13</asp:ListItem>
                            <asp:ListItem Value="15">14</asp:ListItem>
                            <asp:ListItem Value="16">15</asp:ListItem>
                            <asp:ListItem Value="17">16</asp:ListItem>
                            <asp:ListItem Value="18">17</asp:ListItem>
                            <asp:ListItem Value="19">18</asp:ListItem>
                            <asp:ListItem Value="20">19</asp:ListItem>
                            <asp:ListItem Value="21">20</asp:ListItem>
                            <asp:ListItem Value="22">21</asp:ListItem>
                            <asp:ListItem Value="23">22</asp:ListItem>
                            <asp:ListItem Value="24">23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlMM" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="false">
                            <asp:ListItem Value="0">-MM--</asp:ListItem>
                            <asp:ListItem Value="00">00</asp:ListItem>
                            <asp:ListItem Value="15">15</asp:ListItem>
                            <asp:ListItem Value="30">30</asp:ListItem>
                            <asp:ListItem Value="45">45</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contact Address<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvaddress" runat="server" ControlToValidate="txtAddress"
                    Display="None" ErrorMessage="Please Enter Contact Address " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Address with maximum Limit of 500 characters)" onmouseout="UnTip()">
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Rows="3" MaxLength="500"></asp:TextBox>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reason for Leave<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtReason"
                    Display="None" ErrorMessage="Please Enter Reason for Leave " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Reason with maximum Limit of 250 characters)" onmouseout="UnTip()">
                        <asp:TextBox ID="txtReason" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Rows="3" MaxLength="250"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Leave Status<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtLveStatus" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<%-- <tr >
                        <td align="left" style="width: 25%; height: 26px;">
                            Type of Leave<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlLeaveType"
                                Display="None" ErrorMessage="Please Select Leave Type" ValidationGroup="Val1"
                                InitialValue="--Select--"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 25%; height: 26px;">
                            <asp:DropDownList ID="ddlLeaveType" runat="server" CssClass="clsComboBox" Width="99%"
                                AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="width: 25%; height: 26px;">
                            <asp:Button ID="btnLeaves" runat="Server" Text="Leave(s) Applied" CssClass="button"
                                ValidationGroup="Val1" CausesValidation="true" />
                        </td>
                        <td align="left" style="width: 25%; height: 26px;">
                            <asp:TextBox ID="txtNoLeaves" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="tr2" runat="Server">
                        <td align="left" style="height: 26px; width: 25%">
                            Total Available Leaves As On Today
                        </td>
                        <td align="left" style="height: 26px; width: 25%">
                            <asp:TextBox ID="txtTotalLeave" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                        </td>
                        <td align="left" style="height: 26px; width: 25%">
                            Balance Leaves
                        </td>
                        <td align="left" style="height: 26px; width: 25%">
                            <asp:TextBox ID="txtBalLeaves" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
--%>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">
            <asp:Button ID="btnSubmit" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
