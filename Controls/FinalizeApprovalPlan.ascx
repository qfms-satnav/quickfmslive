﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FinalizeApprovalPlan.ascx.vb"
    Inherits="Controls_FinalizeApprovalPlan" %>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvitems1" runat="server" EmptyDataText="No Finalize Journey Plan Found."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Request Id">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("REQUESTID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Date">
                        <ItemTemplate>
                            <asp:Label ID="lblreqdate" runat="server" Text='<%#Eval("REQUESTED_ON")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Name">
                        <ItemTemplate>
                            <asp:Label ID="lblreqname" runat="server" Text='<%#Eval("REQUESTNAME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STA_TITLE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField Text="Add Details" CommandName="View" />
                    <asp:ButtonField Text="Add Travel Advance" CommandName="AddTravelAdv" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div id="pnltraveladvance" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row text-center">
                    <strong>TRAVEL ADVANCE</strong>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Travel Advance Amount<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txttraveladvamt"
                        Display="None" ErrorMessage="Please Enter Travel Advance Amount " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid Travel Advance Amount"
                        ControlToValidate="txttraveladvamt" ValidationGroup="Val2" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txttraveladvamt" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">

                <asp:Button ID="btnAddtravelamt" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" />
                <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="btn btn-primary custom-button-color" CausesValidation="false" />

            </div>
        </div>
    </div>
</div>
<div id="pnlreqdetails" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row text-center">
                    <strong>REQUEST DETAILS</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblsno" runat="server" Visible="false"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvItems" runat="server" EmptyDataText="Sorry! No Available Records..."
                    RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="FALSE">
                            <ItemTemplate>
                                <asp:Label ID="lblID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Start Date">
                            <ItemTemplate>
                                <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("STARTDATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Date">
                            <ItemTemplate>
                                <asp:Label ID="lblenddate" runat="server" Text='<%#Eval("ENDDATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="From">
                            <ItemTemplate>
                                <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ORIGIN")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To">
                            <ItemTemplate>
                                <asp:Label ID="lblTo" runat="server" Text='<%#Eval("DESTINATION")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblMode" runat="server" Text='<%#Eval("MODEOFTRAVEL")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Travel Assistance">
                            <ItemTemplate>
                                <asp:Label ID="lblTRAVELASSISTANCE" runat="server" Text='<%#Eval("TRAVELASSISTANCE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time">
                            <ItemTemplate>
                                <asp:Label ID="lblTIME" runat="server" Text='<%#Eval("TIME")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField CommandName="Select" Text="Select" HeaderText="Select" />
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
</div>
<div id="pnladd" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row text-center">
                    <strong>ADD DETAILS</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Stay<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Stay Details Please Enter Numericals"
                        ControlToValidate="txtstay" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtstay" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Conveyance<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid Conveyance Details Please Enter Numericals"
                        ControlToValidate="txtConveyance" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtConveyance" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Food<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid Food Details Please Enter Numericals"
                        ControlToValidate="txtfood" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtfood" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Telephone<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Invalid Telephone Please Enter Numericals"
                        ControlToValidate="txttelephone" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txttelephone" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Entertainment<span style="color: red;"></span></label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtentertainment" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Laundry Expenses<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="RegLau" runat="server" ErrorMessage="Invalid Expenses Please Enter Numericals"
                        ControlToValidate="txtlaundry" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtlaundry" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Expenses Of Family<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="RegExp" runat="server" ErrorMessage="Invalid Expenses of family Please Enter Numericals"
                        ControlToValidate="txtexpensesfamily" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtexpensesfamily" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">No of Kms<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="Regnoofkm" runat="server" ErrorMessage="Invalid No of Kms Please Enter Numericals"
                        ControlToValidate="txtkms" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtkms" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Reimbursement Amount<span style="color: red;"></span></label>
                    <asp:RegularExpressionValidator ID="Regramt" runat="server" ErrorMessage="Invalid Reimbursement amount Please Enter Numericals"
                        ControlToValidate="txtremamount" ValidationGroup="Val1" Display="None" ValidationExpression="^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtremamount" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">               
                    <asp:Button ID="btnAddpay" runat="server" Text="Update" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                        CausesValidation="true" />
               
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">           
                <asp:Button ID="btnAdd" runat="server" Text="Finalize" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                    CausesValidation="false" Visible="false" />
                <asp:Button ID="btnModify" runat="server" Text="Cancel" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                    CausesValidation="false" Visible="false" />
                <asp:TextBox ID="txtStore" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>          
        </div>
    </div>
</div>




