﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewConsumableIssuane.ascx.vb" Inherits="Controls_ConsumableIssuane" %>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Consumable Item Issuance
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="TABLE1"
        onclick="return TABLE1_onclick()">
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Consumable Item Issuance</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <br />
                <br />
                <asp:GridView ID="gvItemStock" TabIndex="5" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" EmptyDataText="No Records Found">
                    <Columns>
                                    <asp:BoundField DataField="AIR_REQ_ID" HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Date" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmConsumableItemIssuance.aspx?RID={0}") %>'
                                                Text="View Details"></asp:HyperLink>
                                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                   
                </asp:GridView>
                <br />
                <br />
            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>