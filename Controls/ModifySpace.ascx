<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ModifySpace.ascx.vb" Inherits="Controls_ModifySpace" %>
<%--<asp:ScriptManager ID="scriptmanager1" runat="server">
</asp:ScriptManager>--%>





<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js">function TABLE1_onclick() {

}

</script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Modify Space
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="TABLE1" onclick="return TABLE1_onclick()">
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>Modify Space</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label2" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="table4" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <%-- <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblLCM" runat="server" CssClass="bodytext" Text="Select Location">
                
                            </asp:Label><font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlLCM"
                                Display="Dynamic" ErrorMessage="Please Select Location" ValidationGroup="Val1"
                                InitialValue="0">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlLCM" runat="server" Width="97%" CssClass="dropDown" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    --%>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblTemp" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblBDG" runat="server" CssClass="bodytext">Select Building                           
                            </asp:Label>
                            <asp:RequiredFieldValidator ID="rfvBDG" runat="server" ControlToValidate="ddlBDG"
                                Display="None" ErrorMessage="Please Select Building" ValidationGroup="Val1"
                                InitialValue="--Select Building--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlBDG" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" Enabled="false">
                                <asp:ListItem>--Select Building--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblTwr" runat="server" CssClass="bodytext">Select Tower</asp:Label>
                            <%-- <asp:Label ID="Label6" runat="server" CssClass="bodytext">Select Location<font class="clsNote">*</font></asp:Label>--%>
                            <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ControlToValidate="ddlTWR"
                                Display="None" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="--Select Tower--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlTWR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" Enabled="false">
                                <asp:ListItem>--Select Tower--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblFLR" runat="server" CssClass="bodytext" >Select Floor</asp:Label>
                
                            
                           <asp:RequiredFieldValidator ID="rfvFLR" runat="server" ControlToValidate="ddlFLR"
                                Display="None" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select Floor--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlFLR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" Enabled="false">
                                <asp:ListItem>--Select Floor--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblWNG" runat="server" CssClass="bodytext">Select Wing</asp:Label>
                            <asp:RequiredFieldValidator ID="frvWNG" runat="server" ControlToValidate="ddlWNG"
                                Display="None" ErrorMessage="Please Select Wing" ValidationGroup="Val1" InitialValue="--Select Wing--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlWNG" runat="server" Width="97%" CssClass="clsComboBox" Enabled="false">
                                <asp:ListItem>--Select Wing--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%" style="height: 26px">
                            <asp:Label ID="Label1" runat="server" CssClass="bodytext">Select Space Type</asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlSPCType"
                                Display="None" ErrorMessage="Please Select Space Type" ValidationGroup="Val1" InitialValue="--Select Space Type--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%; height: 26px;">
                            <asp:DropDownList ID="ddlSPCType" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label5" runat="server" CssClass="bodytext">Space Name </asp:Label>
                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtSpcName"
                                Display="None" ErrorMessage="Please Enter the Space Name!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:TextBox ID="txtSpcName" runat="server" CssClass="textBox" Width="96%" MaxLength="50" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <%-- <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label2" runat="server" CssClass="bodytext" Text="Space X Value"></asp:Label>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtX"
                                Display="Dynamic" ErrorMessage="Please Enter the Space X Value!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                 <asp:RegularExpressionValidator ID="REV1" runat="server" ControlToValidate="txtX"
                            Display="Dynamic" ErrorMessage="Invalid X Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:TextBox ID="txtX" runat="server" CssClass="textBox" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label3" runat="server" CssClass="bodytext">Space Cost Value </asp:Label>
                            
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCost"
                                Display="None" ErrorMessage="Please Enter the Space Cost Value!" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCost"
                                Display="None" ErrorMessage="Invalid Cost Value" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:TextBox ID="txtCost" runat="server" CssClass="textBox" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label4" runat="server" CssClass="bodytext"> Space Area</asp:Label>
                            
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtArea"
                                Display="None" ErrorMessage="Please Enter the Space Area Value!" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtArea"
                                Display="None" ErrorMessage="Invalid Area Value" ValidationGroup="Val1" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:TextBox ID="txtArea" runat="server" CssClass="textBox" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>


                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblDesc" runat="server" CssClass="bodytex">Description <font class="clsNote">* </font></asp:Label>
                        
                            <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDesc"
                                Display="None" ErrorMessage="Please Enter the Description!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:TextBox ID="txtDesc" runat="server" CssClass="textBox" Width="96%" MaxLength="50"
                                Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblRem" runat="server" CssClass="bodytext">Remarks<font class="clsNote">* </font></asp:Label>
                          
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRem"
                                Display="None" ErrorMessage="Please Enter the Remarks!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:TextBox ID="txtRem" runat="server" CssClass="textBox" Width="96%" MaxLength="50"
                                Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>







                    <tr>
                        <td align="left" width="50%" style="height: 26px">
                            <asp:Label ID="lblSatus" runat="server" CssClass="bodytext">Status<font class="clsNote">* </font></asp:Label>
                          
                            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                Display="None" ErrorMessage="Select Status" ValidationGroup="Val1" InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlStatus" runat="server" Width="97%" CssClass="clsComboBox">
                                <asp:ListItem>--Select Status--</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">InActive</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>


                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" />
                            &nbsp;<asp:Button ID="btnModify" runat="server" Text="Modify" CssClass="button" ValidationGroup="Val1" />&nbsp;
   
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                <br />
                <br />

            </td>
            <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
