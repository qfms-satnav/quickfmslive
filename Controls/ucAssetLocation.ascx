<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucAssetLocation.ascx.vb"
    Inherits="Controls_ucAssetLocation" %>
 <link href="../../StyleSheet.css" rel="stylesheet" type="text/css" />
    <%--gird--%>
    <link rel="stylesheet" href="../../style/print.css" type="text/css" media="print" charset="utf-8" />
    <link rel="stylesheet" href="../../themes/redmond/jquery-ui-1.8.14.custom.css" type="text/css"
        media="screen" />
  <!--  <link rel="stylesheet" href="../../themes/ui.jqgrid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../../style/facebox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../../style/colorbox/colorbox.css" media="screen" />
    <link rel="stylesheet" href="../../style/tooltip.css" type="text/css" media="all" />
    <link rel="stylesheet" href="../../style/roi.css" type="text/css" media="all" />-->
    <%--end--%>

    <script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../../lib/jquery/jquery-ui-1.8.2.custom.min.js"></script>

    <script type="text/javascript" src="../../lib/jquery/facebox/facebox.js"></script>

    <script type="text/javascript" src="../../lib/jquery/colorbox/jquery.colorbox.js"></script>

    <script type="text/javascript" src="../../lib/page.js"></script>

    <script type="text/javascript" src="../../lib/roi_uam.js"></script>

    <script type="text/javascript" src="../../lib/utilities.js"></script>

  
    <%--gird--%>
    <link rel="stylesheet" href="../../themes/ui.jqgrid.css" type="text/css" media="screen" />

    <script type="text/javascript" src="../../lib/UserManager.js"></script>

    <script type="text/javascript" src="../../js/i18n/grid.locale-en.js"></script>

    <script type="text/javascript" src="../../js/jquery.jqGrid.src.js"></script>
    

    <script type="text/javascript" src="../../js/jquery.jqGrid.min.js"></script>

  <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Asset Location Summary Report
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
<table cellspacing="0" cellpadding="0" width="100%" class="fornormaltext">
     <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>View Item Requisition Report</strong>
                    </td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
      <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                    </td>
                    <td align="center">
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
            <table id="tblDetails" cellspacing="0" cellpadding="0" style="width: 100%" border="1">
                <tr>
                    <td align="left" colspan="2" style="height: 24px">
                        &nbsp;Asset Category <strong><span style="font-size: 8pt; color: #ff0000">* </span></strong>
                    </td>
                    <td align="left" colspan="2" style="height: 24px">
                        <select name='AssetCategory' id="AssetCategory" style="width: 300px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <%-- <a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAstLocationSummary.aspx")%>?id=2691,850,338,'')">
                                        Link to popup</a>
                                   --%>
                                   <%-- <a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAstLocationSummary.aspx")%>?id=<%# Eval("ProductId") %>',850,338,'')">--%>
                                        <table id="AssetCategory_list">
                                        </table>
                                    <%--</a>--%>
                                    <div id="AssetCategory_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
               <%-- <tr>
                    <td colspan="4" align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <table id="AssetLocationForInnerDetails1_list">
                                    </table>
                                    <div id="AssetLocationForInnerDetails1_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table id="AssetLocationForInnerDetails2_list">
                                    </table>
                                    <div id="AssetLocationForInnerDetails2_pager" class="scroll" style="text-align: center;">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
            </table>
        </td>
    </tr>
</table>
