Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptReservation
    Inherits System.Web.UI.UserControl

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            pnl.Visible = False
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_GETDETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As String = lblID.Text
            BindDetails(id)

            txtstore.Text = id
            If gvrm.Rows.Count > 0 Then
                btnsubmit.Visible = True
                btnReject.Visible = True
            Else
                btnsubmit.Visible = False
                btnReject.Visible = False
            End If
            pnl.Visible = True
        Else
            pnl.Visible = False
        End If
    End Sub
    Private Sub BindDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONEVYANCE_BINDDETAILS")
            sp.Command.AddParameter("@CNV_REQ_ID", id, DbType.String)
            gvrm.DataSource = sp.GetDataSet()
            gvrm.DataBind()
            
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvrm.Rows.Count - 1

                Dim lblID1 As Label = DirectCast(gvrm.Rows(i).FindControl("lblID1"), Label)
                Dim chbx As CheckBox = DirectCast(gvrm.Rows(i).FindControl("chkbx"), CheckBox)
                If chbx.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_RMAPPROVAL")
                    sp.Command.AddParameter("@CNV_REQ_ID", txtstore.Text, DbType.String)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lblID1.Text, DbType.Int32)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkboxes to Approve or Reject"
            Else
                Mail_RmApproved(txtstore.Text)
                lblMsg.Text = "Your Request Has been Approved by RM"

                BindGrid()
                BindDetails(txtstore.Text)
                Cleardata()
            End If


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvrm.Rows.Count - 1

                Dim lblID1 As Label = DirectCast(gvrm.Rows(i).FindControl("lblID1"), Label)
                Dim chbx As CheckBox = DirectCast(gvrm.Rows(i).FindControl("chkbx"), CheckBox)
                If chbx.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_RMREJECTED")
                    sp.Command.AddParameter("@CNV_REQ_ID", txtstore.Text, DbType.String)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lblID1.Text, DbType.Int32)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkboxes to Approve or Reject"
            Else
                Mail_RmRejected(txtstore.Text)
                BindGrid()
                BindDetails(txtstore.Text)
                Cleardata()

                lblMsg.Text = "Your Request Has been Rejected by RM"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Cleardata()
        If gvrm.Rows.Count > 0 Then
            tr1.Visible = True
            txtRemarks.Text = ""

            btnsubmit.Visible = True
            btnReject.Visible = True
        Else
            tr1.Visible = False
            btnsubmit.Visible = False
            btnReject.Visible = False
        End If
    End Sub
    Private Sub Mail_RmApproved(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strHrEmail As String = ""                              'To hold the HR Email
        Dim strFinEmail = ""
        Dim StrFinName = ""
       

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_APPROVED_DETAILS")
        sp1.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        gvdetails.DataSource = sp1.GetDataSet()
        gvdetails.DataBind()


        strSubj = " Approval of Domestic Conveyance Request"
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONEVYANCE_BINDDETAILS_MAIL")
        sp.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")

        Dim strfrommail, strfromname As String

        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"


        'Mail message for the Requisitioner. 
        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of Your Approved Domestic Conveyance Requisitions by Reporting Manager" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblMode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAppkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblpurs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRemarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrmRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
            ' Response.Write(strMsg)
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_CONVEYANCE")
            sp2.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
            Dim DS2 As New DataSet()
            DS2 = sp2.GetDataSet()
            strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")

        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP_FINANCE")

        Dim DS3 As New DataSet()
        DS3 = sp3.GetDataSet()
        If DS3.Tables(0).Rows.Count > 0 Then
            StrFinName = DS3.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
            strFinEmail = DS3.Tables(0).Rows(0).Item("AUR_EMAIL")
            'strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        End If
            'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of  " & strEmpName & "'s  Approved Domestic Conveyance Requisitions by Reporting Manager" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left  width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1> <tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblMode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAppkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblpurs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRemarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrmRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strEmpName & "." & "<br>"
            ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

            'objCom.Dispose()
            'Mail  for the Requisitioner. 

            If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strHrEmail)
            End If
            If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strfrommail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
        If strFinEmail <> "" Then
            Send_Mail(strFinEmail, strfrommail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)

        End If

    End Sub
    Private Sub Mail_RmRejected(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strHrEmail As String = ""                             'To hold the HR Email
     




        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONVEYANCE_REJECTED_DETAILS")
        sp1.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        gvdetails.DataSource = sp1.GetDataSet()
        gvdetails.DataBind()

        strSubj = " Rejection of Domestic Conveyance Request"
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOMESTIC_CONEVYANCE_BINDDETAILS_MAIL")
        sp.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()


        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        Dim strfrommail, strfromname As String


        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of Your Rejected Domestic Conveyance Requisitions by Reporting Manager" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left ><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblMode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAppkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblpurs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRemarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrmRemarks"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_CONVEYANCE")
        sp2.Command.AddParameter("@CNV_REQ_ID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        'strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")

        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of  " & strEmpName & "'s  Rejected Domestic Conveyance Requisitions by Reporting Manager  " & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
"<td align=left width=12.5%><U>Journey Date: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Expense: </U></td>" & _
"<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
"<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
"<td  align=left width=12.5%><U>Amount </U></td>" & _
"<td  align=left width=12.5%><U>Remarks </U></td>" & _
"<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"

        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblMode"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAppkm"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblpurs"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRemarks"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrmRemarks"), Label).Text & "</td></tr></table>"
        Next

        strMsgAdm = strMsgAdm + "<br><br>  Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strfrommail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub gvrm_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvrm.PageIndexChanging
        gvrm.PageIndex = e.NewPageIndex()
        BindDetails(txtstore.Text)
    End Sub

    Protected Sub gvItems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItems.SelectedIndexChanged

    End Sub
End Class
