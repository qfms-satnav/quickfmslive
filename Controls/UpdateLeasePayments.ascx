<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateLeasePayments.ascx.vb"
    Inherits="Controls_UpdateLeasePayments" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <label class="col-md-3 control-label">Click on the Payment Report</label>
                <asp:LinkButton ID="btnexport" runat="server" Text="Download the Payment details for Updation" />
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Document (Only Excel)<span style="color: red;">*</span></label>               
                <div class="col-md-7">
                    <div class="btn btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Choose File"
                                            ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5  control-label">
                    <asp:Button ID="btnsubmit" runat="Server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" Text="Submit" />
                </div>
                <div class="col-md-7">
                    <asp:HyperLink ID="hyp" runat="server" Text=" Click here to view the template" NavigateUrl="~/WorkSpace/SMS_Webfiles/Payment_Template.xls"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</div>
