Imports SubSonic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class Controls_TopMenu
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                Dim UID As String = Session("uid")
            End If

            Dim aurid As String = Session("uid")
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MENU_SCRIPT1")
            sp.Command.AddParameter("@AURID", aurid, Data.DbType.String)
            litMenu.Text = sp.ExecuteScalar

            'Dim strQuery As String = ""
            'Dim objdata As SqlDataReader
            'strQuery = "select aur_known_as  from " & Session("TENANT") & "."  & "amantra_user where aur_id=  '" & aurid & "'"
            'objdata = SqlHelper.ExecuteReader(CommandType.Text, strQuery)
            'While (objdata.Read())
            '    lbl1.Text = "Welcome " & objdata("aur_known_as").ToString & "!"
            'End While

        End If

    End Sub

    Protected Sub lbtnLogOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLogOut.Click
       
        Response.Redirect("~/logout.aspx")

    End Sub
End Class
