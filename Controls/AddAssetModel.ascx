<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAssetModel.ascx.vb" Inherits="Controls_AddAssetModel" %>

<style>
  /*  select.form-control:not([size]):not([multiple]) {
        display: block !important;
    }*/
</style>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-10" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6 text-right">
        <label class="col-md-2 btn pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Asset Subcategory and Select Modify to modify the existing Asset Subcategory" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="col-md-2 btn pull-left">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Asset Subcategory and Select Modify to modify the existing Asset Subcategory" />
            Modify
        </label>
    </div>
</div>

<div class="row" id="pnlmodelcode" runat="server" visible="false">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label id="lblAssetModel" runat="server">Asset Model<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlModel"
                ErrorMessage="Please Select Asset Model" ValueToCompare="--Select--"></asp:CompareValidator>
            <asp:RequiredFieldValidator ID="rfmodel" runat="server" ControlToValidate="ddlModel"
                Display="none" InitialValue="--Select--" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlModel" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--"
                AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Model Code<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtModel"
                Display="none" ErrorMessage="Please Enter Model Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtModel"
                ErrorMessage="Please Enter code in alphabets,numbers" Display="None" ValidationExpression="^[0-9a-zA-Z]+"
                ValidationGroup="Val1"></asp:RegularExpressionValidator>
            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Model Name<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtModelName"
                Display="none" ErrorMessage="Please Enter Model Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtModelName"
                ErrorMessage="Please Enter Model Name in alphabets,numbers" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>--%>
            <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtModelName" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Category<span style="color: red;">*</span></label>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="--Select--" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Sub Category<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="--Select--" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Brand/Make<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="--Select--" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Status</label>
            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                <%--<asp:ListItem>--Select--</asp:ListItem>--%>
                <asp:ListItem Value="1">Active</asp:ListItem>
                <asp:ListItem Value="0">InActive</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div id="pnlMinQtyCon" runat="server" visible="false">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Quantity Unit<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlunit"
                    Display="none" ErrorMessage="Please Select Quantity Unit" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlunit" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                    ToolTip="Select Quantity Unit">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Min order Qty.<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMinOrdQty"
                    Display="none" ErrorMessage="Please Enter Min Order Qty." InitialValue="" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMinOrdQty"
                    ErrorMessage="Please Enter Quantity In Numbers" Display="None" ValidationExpression="^[0-9]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div onmouseover="Tip('Enter Quantity in numbers')" onmouseout="UnTip()">
                    <asp:TextBox ID="txtMinOrdQty" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Min stock Qty.<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMinEscQty"
                    Display="none" ErrorMessage="Please Enter Min Stock Qty." ValidationGroup="Val1" InitialValue=""></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtMinEscQty"
                    ErrorMessage="Please Enter Minimum Stock In Numbers" Display="None" ValidationExpression="^[0-9]+"
                    ValidationGroup="Val1">
                </asp:RegularExpressionValidator>
                <div onmouseover="Tip('Enter Minimum Stock In Numbers')" onmouseout="UnTip()">
                    <asp:TextBox ID="txtMinEscQty" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-default btn-primary" runat="server" Text="Back" CausesValidation="False" PostBackUrl="~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowSorting="False" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left"
                PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Asset Model Found." CssClass="table GridStyle" GridLines="none">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("AST_MD_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Model Name">
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" CssClass="lblCode" Text='<%#BIND("AST_MD_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Model Code">
                        <ItemTemplate>
                            <asp:Label ID="lblMdlCode" runat="server" Text='<%#Eval("AST_MD_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Brand/Make">
                        <ItemTemplate>
                            <asp:Label ID="lblAstBrnd" runat="server" CssClass="lblCat" Text='<%#BIND("manufacturer")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Sub Category">
                        <ItemTemplate>
                            <asp:Label ID="lblSubCat" runat="server" CssClass="lblCat" Text='<%#BIND("AST_SUBCAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Category">
                        <ItemTemplate>
                            <asp:Label ID="lblCat" runat="server" CssClass="lblCat" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("AST_MD_STAID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#d3d3d3" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<script type="text/ecmascript">
    //$("#btnSubmit").click(function () {
    //    $('#lblMsg').text("")
    //});
    function refreshSelectpicker() {
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlunit.ClientID%>").selectpicker();
        $("#<%=ddlModel.ClientID%>").selectpicker();
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddlStatus.ClientID%>").selectpicker();
    }
    refreshSelectpicker();
</script>
