<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LveChangeRequisition.ascx.vb"
    Inherits="Controls_LveChangeRequisition" %>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText=" No Leave Requisition Found.">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_REQ_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField DataTextField="ASSOCIATENAME" DataNavigateUrlFields="LMS_REQ_ID" DataNavigateUrlFormatString="~/EFM/EFM_Webfiles/frmLveChangeRequisitionDetails.aspx?ReqID={0}" HeaderText="Requested By" />

                <asp:TemplateField HeaderText="From Date">
                    <ItemTemplate>
                        <asp:Label ID="lblFromDate" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_FDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Date">
                    <ItemTemplate>
                        <asp:Label ID="lblToDate" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_TDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Number Of Days">
                    <ItemTemplate>
                        <asp:Label ID="lblTotDays" runat="Server" CssClass="bodyText" Text='<%#Eval("LMS_NO_DAYS") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested On">
                    <ItemTemplate>
                        <asp:Label ID="lblReqOn" runat="Server" CssClass="bodyText" Text='<%#Eval("LMS_REQ_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Leave Status">
                    <ItemTemplate>
                        <asp:Label ID="lblsta" runat="Server" CssClass="bodyText" Text='<%#Eval("LMS_STATUS_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>




