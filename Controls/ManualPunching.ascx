<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ManualPunching.ascx.vb" Inherits="Controls_ManualPunching" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>







<asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>


<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <div class="row">
                <asp:Button ID="btnPunchIn" CssClass="btn btn-primary custom-button-color" runat="server" Text="Punch-In" ValidationGroup="Val1" />

                <asp:Button ID="btnPunchOut" CssClass="btn btn-primary custom-button-color" runat="server" Text="Punch-Out" ValidationGroup="Val1" />
            </div>
        </div>
    </div>
</div>


