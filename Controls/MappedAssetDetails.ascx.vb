Imports clsSubSonicCommonFunctions
Imports System.Data
Imports System.Data.SqlClient

Partial Class Controls_MappedAssetDetails
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                GetEmployeeDetails()
            End If
        End If


    End Sub
    Private Sub GetEmployeeDetails()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SPC_ID", SqlDbType.VarChar, 50)
        param(0).Value = Request.QueryString("id")
        Dim dsEmployeeDetails As New DataSet
        dsEmployeeDetails = ObjSubSonic.GetSubSonicDataSet("GET_SPACEDETAILS", param)
        'Start Newly addded
        GetAssetDetails(Request.QueryString("id"))
        'End Newly addded

        'If dsEmployeeDetails.Tables(0).Rows.Count > 0 Then
        '    'lblEmpId.Text = dsEmployeeDetails.Tables(0).Rows(0).Item("aur_id")
        '    'lblName.Text = dsEmployeeDetails.Tables(0).Rows(0).Item("aur_first_name")
        '    'lblEmailId.Text = dsEmployeeDetails.Tables(0).Rows(0).Item("aur_Email")
        '    'lblDept.Text = dsEmployeeDetails.Tables(0).Rows(0).Item("dep_name")
        '    'lblStatus.Text = dsEmployeeDetails.Tables(0).Rows(0).Item("sta_title")
        '    GetAssetDetails(dsEmployeeDetails.Tables(0).Rows(0).Item("aur_id"))
        'Else
        '    gvAssets.DataSource = Nothing
        '    gvAssets.DataBind()
        'End If


    End Sub


    Private Sub GetAssetDetails(ByVal EmpId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@EMP_ID", SqlDbType.VarChar, 50)
        param(0).Value = EmpId

        Dim dsASSET_DETAILS As New DataSet
        dsASSET_DETAILS = ObjSubSonic.GetSubSonicDataSet("GET_ASSET_DETAILS", param)
        If dsASSET_DETAILS.Tables(0).Rows.Count > 0 Then
            gvAssets.DataSource = dsASSET_DETAILS
            gvAssets.DataBind()
        Else
            gvAssets.DataSource = Nothing
            gvAssets.DataBind()
        End If
    End Sub
End Class
