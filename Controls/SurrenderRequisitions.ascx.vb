Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_SurrenderRequisitions
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        lblMsg.Text = ""
        If Not IsPostBack Then
            BindSurrenderRequisitions()
        End If
    End Sub

    Private Sub BindSurrenderRequisitions()
        txtRM.Text = ""
        Dim ds As New DataSet()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@RM_AUR_ID", SqlDbType.Int)
        param(0).Value = Session("uid")
        ObjSubsonic.BindGridView(gvSurrenderAstReq, "GET_ALLSURRENDERREQ", param)
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ALLSURRENDERREQ", param)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            gvSurrenderAstReq.DataSource = ds
            gvSurrenderAstReq.DataBind()
            divlevel.Visible = True
            Approvbtn.Visible = True
            Rejectbtn.Visible = True
            txtSearch.Visible = True
            btnSearch.Visible = True
        Else
            gvSurrenderAstReq.DataSource = Nothing
            gvSurrenderAstReq.DataBind()
            divlevel.Visible = False
            Approvbtn.Visible = False
            Rejectbtn.Visible = False
            txtSearch.Visible = False
            btnSearch.Visible = False
        End If
    End Sub

    Protected Sub gvSurrenderAstReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSurrenderAstReq.PageIndexChanging
        gvSurrenderAstReq.PageIndex = e.NewPageIndex
        BindSurrenderRequisitions()
    End Sub

    Protected Sub gvSurrenderAstReq_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSurrenderAstReq.RowCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("UpdateSurrenderRequisitions.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[SEARCH_SURRENDERREQ]")
        sp.Command.AddParameter("@RM_AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@SearchValue", txtSearch.Text, Data.DbType.String)
        gvSurrenderAstReq.DataSource = sp.GetDataSet
        gvSurrenderAstReq.DataBind()
    End Sub
    Public Sub send_mail(ByVal reqid As String, ByVal astcode As String, ByVal proc As String)
        Dim sp1 As New SubSonic.StoredProcedure(proc)
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@AAT_CODE", astcode, DbType.String)
        sp1.Execute()
    End Sub

    Protected Sub Approvbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Approvbtn.Click
        Dim selectedRows As New List(Of String)()
        Dim param(3) As SqlParameter
        For Each row As GridViewRow In gvSurrenderAstReq.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblMsg.Text = ""
            If chkSelect.Checked Then
                Dim lblAAT_AST_CODE As Label = DirectCast(row.FindControl("lblAAT_AST_CODE"), Label)
                Dim lblSREQ_ID As Label = DirectCast(row.FindControl("lblSREQ_ID"), Label)
                param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param(0).Value = lblSREQ_ID.Text
                param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
                param(1).Value = Session("UID")
                param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
                param(2).Value = txtRM.Text
                param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
                param(3).Value = 1037
                ObjSubsonic.GetSubSonicDataSet("[BULK_UPDATESUR_REQ_byRM]", param)
                selectedRows.Add(lblAAT_AST_CODE.Text)
                send_mail(lblSREQ_ID.Text, lblAAT_AST_CODE.Text, Session("TENANT") & "." & "SEND_MAIL_ASSET_SURRENDER_REQ_ITAPPROVAL_BULK")
            End If
        Next
        lblMsg.Visible = True
        lblMsg.Text = "Surrender request(s) successfully Approved "
        BindSurrenderRequisitions()
        'Response.Redirect("frmSurrenderRequisitions.aspx")
    End Sub
    Protected Sub Rejectbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rejectbtn.Click
        Dim assetCodes As New List(Of String)()
        Dim param(3) As SqlParameter
        For Each row As GridViewRow In gvSurrenderAstReq.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblMsg.Text = ""
            If chkSelect.Checked Then
                Dim lblAAT_AST_CODE As Label = DirectCast(row.FindControl("lblAAT_AST_CODE"), Label)
                Dim lblSREQ_ID As Label = DirectCast(row.FindControl("lblSREQ_ID"), Label)
                param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
                param(0).Value = lblSREQ_ID.Text
                param(1) = New SqlParameter("@RM_AUR_ID", SqlDbType.NVarChar, 200)
                param(1).Value = Session("UID")
                param(2) = New SqlParameter("@RM_REMARKS", SqlDbType.NVarChar, 2000)
                param(2).Value = txtRM.Text
                param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
                param(3).Value = 1038
                ObjSubsonic.GetSubSonicDataSet("[BULK_UPDATESUR_REQ_byRM]", param)
                assetCodes.Add(lblAAT_AST_CODE.Text)
                send_mail(lblSREQ_ID.Text, lblAAT_AST_CODE.Text, Session("TENANT") & "." & "SEND_MAIL_ASSET_SURRENDER_REQ_REJECT_BULK")
            End If
        Next
        lblMsg.Visible = True
        lblMsg.Text = "Surrender request(s) successfully Rejected "
        BindSurrenderRequisitions()
        'Response.Redirect("frmSurrenderRequisitions.aspx")
    End Sub
End Class
