<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RaiseIDRequest.ascx.vb"
    Inherits="Controls_RaiseIDRequest" %>




<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="form-group">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select ID Card Type<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvidtype" runat="server" Display="none" ErrorMessage="Please Select ID Type"
                    ControlToValidate="ddlIdType" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlIdType" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                        <asp:ListItem Value="Associate">Associate ID-Card</asp:ListItem>
                        <asp:ListItem Value="Business">Business Card</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="row">
            <asp:Button ID="btnsubmit" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Submit" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
            </div>
        </div>
    </div>
</div>

<div id="pnl1" runat="server">


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Associate First Name</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAssociateFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Associate Middle Name</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAssociateMiddleName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Associate SurName</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAssociatesurName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Associate No</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAssociateNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Department</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtdep" runat="Server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">D.O.J</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtdoj" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Blood Group</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtBG" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                <span style="color: red;">*</span>
                <asp:CheckBox ID="chkterms" runat="Server" />
                <asp:LinkButton ID="lbtnterms" runat="server" Text="I Accept to the Terms and Conditions"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:LinkButton ID="lbtnAssoc" runat="Server" Text="Edit"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="row">
                <asp:Button ID="btnAssID" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" />
            </div>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
            </div>
        </div>
    </div>
</div>

<div id="pnl2" runat="server">


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Associate Name</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtassname" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Designation</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtdesig" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Phone</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtphone" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Mobile</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtmobile" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Email</label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Quantity <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvqty" runat="server" Display="none" ErrorMessage="Please Enter Quantity"
                        ControlToValidate="txtqty" ValidationGroup="Val3"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revqty" runat="Server" Display="none" ErrorMessage="Please Enter Valid Quantity"
                        ControlToValidate="txtqty" ValidationExpression="^[0-9]+$" ValidationGroup="Val3"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtqty" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                <span style="color: red;">*</span>
                <%--<asp:RequiredFieldValidator ID="rfvbusterms" runat="Server" ControlToValidate="chkbusterms" ErrorMessage="Please Accept Terms and conditions" Display="None" ValidationGroup="Val3"></asp:RequiredFieldValidator>--%>
                <asp:CheckBox ID="chkbusterms" runat="Server" />
                <asp:LinkButton ID="lbtnbus1terms" runat="server" Text="I Accepts to the Terms and Conditions"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11 text-right">
            <div class="form-group">
                <asp:LinkButton ID="lbtnbusterms" runat="Server" Text="Edit"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="row">
                <asp:Button ID="btnBusSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val3" />
            </div>
        </div>
    </div>

</div>
