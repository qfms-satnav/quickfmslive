Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Controls_AddAssetSubCategory
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
        RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        lblMsg.Text = String.Empty
        If Not IsPostBack Then
            getassetbrand()
            getsubasset()
            fillgrid()
            btnSubmit.Text = "Add"
        End If
    End Sub
    Private Sub getsubasset()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_subcatASSETDRP")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlBrand.DataSource = sp.GetDataSet()
        ddlBrand.DataTextField = "AST_CAT_CODE"
        ddlBrand.DataValueField = "AST_SUBCAT_ID"
        ddlBrand.DataBind()
        ddlBrand.Items.Insert(0, "--Select--")
    End Sub
    Private Sub getassetbrand()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAssetCat.DataSource = sp.GetDataSet()
        ddlAssetCat.DataTextField = "VT_TYPE"
        ddlAssetCat.DataValueField = "VT_CODE"
        ddlAssetCat.DataBind()
        ddlAssetCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            ddlBrand.Visible = False
            lblAssetBrand.Visible = False
            cleardata()
            btnSubmit.Text = "Add"
            lblMsg.Visible = False
            txtBrand.Enabled = True
            ddlAssetCat.Enabled = True
            fillgrid()
        Else
            ddlBrand.Visible = True
            lblAssetBrand.Visible = True
            getsubasset()
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            txtBrand.Enabled = False
            ddlAssetCat.Enabled = False
            ' getassetbrand()
            fillgrid()
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text = "Add" Then
            Dim ValidateCode As Integer
            ValidateCode = ValidateBrand(txtBrand.Text)
            If ValidateCode = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Asset Subcategory Code is in use; try another"
            ElseIf ValidateCode = 1 Then
                insertnewrecord()
                ' fillgrid()
            End If
        Else
            If ddlBrand.SelectedIndex <> 0 Then
                modifydata()
                'If ddlBrand.SelectedIndex <> 0 Then
                '    txtBrand.Enabled = False
                '    Dim ValidateCode As Integer
                '    ValidateCode = ValidateBrand(txtBrand.Text)
                '    If ValidateCode = 0 Then
                '        modifydata()
                '        lblMsg.Visible = True
                '        lblMsg.Text = "Asset Subcategory Code is in use; try another"
                '    ElseIf ValidateCode = 1 Then
                '        modifydata()
                '        fillgrid()
                '    End If
                'End If
            End If
            End If
        'getassetbrand()
        'getsubasset()
    End Sub
    Private Sub modifydata()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MODIFY_ASSETSUBCATEGORY")
        sp1.Command.AddParameter("@AST_SUBCAT_ID", ddlBrand.SelectedItem.Value, DbType.Int32)
        sp1.Command.AddParameter("@AST_SUBCAT_CODE", txtBrand.Text, DbType.String)
        sp1.Command.AddParameter("@AST_SUBCAT_NAME", txtBrandName.Text, DbType.String)
        sp1.Command.AddParameter("@AST_CAT_CODE", ddlAssetCat.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AST_SUBCATUPDATED_BY", Session("Uid"), DbType.String)
        sp1.Command.AddParameter("@AST_SUBCATSTA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp1.Command.AddParameter("@AST_REM", txtRemarks.Text, DbType.String)
        sp1.Command.AddParameter("@AST_VALIDITY", txtLife.Text, DbType.Int32)
        sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp1.ExecuteScalar()
        fillgrid()

        cleardata()
        ddlBrand.SelectedIndex = 0
        'ddlAssetCat.SelectedIndex = 0
        lblMsg.Visible = True
        lblMsg.Text = "Asset Subcategory successfully modified"
    End Sub
    Public Function ValidateBrand(ByVal brandcode As String)
        Dim ValidateCode As Integer
        'Dim PN_PROPERTYTYPE As String = brandcode
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_VALIDATE_ASSETSUBCATEGORY")
        sp1.Command.AddParameter("@AST_SUBCAT_CODE", brandcode, DbType.String)
        sp1.Command.AddParameter("@AST_CAT_CODE", ddlAssetCat.SelectedItem.Value, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_INSERT_ASSETSUBCATEGORY")
            '@VT_CODE,@VT_TYPE,@VT_STATUS,@VT_CREATED_BY,@VT_CREATED_DT,@VT_REM
            sp1.Command.AddParameter("@AST_SUBCAT_CODE", txtBrand.Text, DbType.String)
            sp1.Command.AddParameter("@AST_SUBCAT_NAME", txtBrandName.Text, DbType.String)
            sp1.Command.AddParameter("@AST_CAT_CODE", ddlAssetCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AST_SUBCATSTA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AST_SUBCATCREATED_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AST_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@AST_VALIDITY", txtLife.Text, DbType.Int32)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "New Asset Subcategory Successfully Added"
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETSUBCATEGORIES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvCat.DataSource = ds
        gvCat.DataBind()
        For i As Integer = 0 To gvCat.Rows.Count - 1
            Dim lblstatus As Label = CType(gvCat.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "Inactive"
            End If
        Next
    End Sub

    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        lblMsg.Text = " "
        If ddlBrand.SelectedIndex <> 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_SUBCATASSEGET")
            sp.Command.AddParameter("@AST_SUBCAT_ID", ddlBrand.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then '
                txtBrand.Text = ds.Tables(0).Rows(0).Item("AST_SUBCAT_CODE")
                txtBrandName.Text = ds.Tables(0).Rows(0).Item("AST_SUBCAT_NAME")
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AST_SUBCATSTA_ID")).Selected = True
                getassetbrand()
                ddlAssetCat.ClearSelection()
                ddlAssetCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("AST_CAT_CODE")).Selected = True
                txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("AST_REM"))
                txtLife.Text = ds.Tables(0).Rows(0).Item("AST_LIFE_TIME")
            Else
     
                cleardata()
                lblMsg.Visible = True
                lblMsg.Text = "Verify the information you typed.."
            End If
        Else
            cleardata()
        End If
    End Sub
    Private Sub cleardata()
        txtBrand.Text = ""
        txtBrandName.Text = ""
        txtBrand.Enabled = True
        ddlBrand.SelectedIndex = 0
        ddlAssetCat.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        txtRemarks.Text = ""
        txtLife.Text = ""


    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx")
    End Sub

    Protected Sub gvCat_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCat.PageIndexChanging
        gvCat.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
End Class
'Private Sub Insert()
'    obj.getcode = txtCitycode.Text
'    obj.getname = txtCityName.Text
'    obj.getRemarks = txtRemarks.Text
'    Dim iStatus As Integer = obj.InsertCity(ddlCountry, Me)
'    If iStatus = 1 Then
'        lblMsg.Text = "City Code Already Exists "
'        lblMsg.Visible = True
'    ElseIf iStatus = 2 Then
'        lblMsg.Text = "City Inserted Successfully "
'        lblMsg.Visible = True
'        Cleardata()
'    ElseIf iStatus = 0 Then
'        lblMsg.Text = "City Inserted Successfully "
'        lblMsg.Visible = True
'        Cleardata()
'    End If
'    obj.City_LoadGrid(gvItem)
'    obj.BindCity(ddlCity)
'End Sub
