Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Controls_AddAssetBrand
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
        RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()

        If Not IsPostBack Then
            getassetbrand()
            getassetcategory()
            getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
            fillgrid()
            btnSubmit.Text = "Add"
            ddlAstSubCat.Enabled = True
        End If
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
        ddlAssetCategory.SelectedIndex = 0
    End Sub
    

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If btnSubmit.Text = "Add" Then

            Dim ValidateCode As Integer
            ValidateCode = ValidateBrand(txtBrand.Text)
            If ValidateCode = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Brand/Make is in use; try another"
            ElseIf ValidateCode = 1 Then
                insertnewrecord()
                'fillgrid()

            End If
        Else
            txtBrand.Enabled = False
            If ddlBrand.SelectedIndex <> 0 Then
                '    Dim ValidateCode As Integer
                '    ValidateCode = ValidateBrand(txtBrand.Text)
                '    If ValidateCode = 0 Then
                '        lblMsg.Visible = True
                '        lblMsg.Text = "Brand/Make is in use; try another"
                '    ElseIf ValidateCode = 1 Then

                modifydata()
                '        fillgrid()

                '    End If

            End If






            End If
            ' Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=1")
            ' insertnewrecord()
            cleardata()
    End Sub
    Private Sub modifydata()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MODIFY_ASSETBRAND")
        sp1.Command.AddParameter("@manufactuer_code", ddlBrand.SelectedItem.Value, DbType.Int32)
        sp1.Command.AddParameter("@manufacturer", txtBrandName.Text, DbType.String)
        sp1.Command.AddParameter("@modifiedBy", Session("Uid"), DbType.String)
        sp1.Command.AddParameter("@MANUFACTUER_TYPE_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@MANUFACTUER_STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp1.Command.AddParameter("@MANU_REM", txtRemarks.Text, DbType.String)
        sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp1.ExecuteScalar()
        fillgrid()
        lblMsg.Visible = True
        lblMsg.Text = "Brand/Make successfully modified"
    End Sub
    Public Function ValidateBrand(ByVal brandcode As String)
        Dim ValidateCode As Integer
        'Dim PN_PROPERTYTYPE As String = brandcode
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_VALIDATE_BRAND")
        sp1.Command.AddParameter("@MANUFACTUER_CODE", brandcode, DbType.String)
        sp1.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_INSERT_ASSETBRAND")
            sp1.Command.AddParameter("@manufactuer_code", txtBrand.Text, DbType.String)
            sp1.Command.AddParameter("@manufacturer", txtBrandName.Text, DbType.String)
            sp1.Command.AddParameter("@createdBy", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@MANUFACTUER_TYPE_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@MANUFACTUER_STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@MANU_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "New Brand/Make Successfully Added"
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASSETBRAND")
        sp.Command.AddParameter("@dummy", 1, DbType.Int16)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvBrand.DataSource = ds
        gvBrand.DataBind()


        For i As Integer = 0 To gvBrand.Rows.Count - 1
            Dim lblstatus As Label = CType(gvBrand.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "Inactive"
            End If
        Next
        getassetbrand()
        getassetcategory()
    End Sub
    


    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            cleardata()
            ddlBrand.Visible = False
            lblAssetBrand.Visible = False
            'cleardata()
            btnSubmit.Text = "Add"
            lblMsg.Visible = False
            txtBrand.Enabled = True
            ddlAssetCategory.Enabled = True
            ddlAstSubCat.Enabled = True
            fillgrid()
        Else
            cleardata()
            ddlBrand.Visible = True
            lblAssetBrand.Visible = True
            ddlAssetCategory.SelectedIndex = 0
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            ddlAssetCategory.Enabled = False
            ddlAstSubCat.Enabled = False
            txtBrand.Enabled = False
            getassetbrand()
            fillgrid()
        End If

    End Sub
    Private Sub cleardata()
        txtBrand.Text = ""
        txtBrandName.Text = ""
        txtBrand.Enabled = True
        ddlBrand.SelectedIndex = 0
        ddlAssetCategory.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        ddlAstSubCat.SelectedIndex = 0
        txtRemarks.Text = ""
    End Sub
    Protected Sub ddlBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBrand.SelectedIndexChanged
        lblMsg.Text = " "
        If ddlBrand.SelectedIndex <> 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ALLASSETBRANDS")
            sp.Command.AddParameter("@manufactuer_code", ddlBrand.SelectedItem.Value, DbType.Int32)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            Dim categorycode As String
            If ds.Tables(0).Rows.Count > 0 Then
                txtBrand.Text = ds.Tables(0).Rows(0).Item("manufactuer_code")
                txtBrandName.Text = ds.Tables(0).Rows(0).Item("manufacturer")
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("MANUFACTURER_STATUS")).Selected = True
                ddlAssetCategory.ClearSelection()
                ddlAssetCategory.Items.FindByValue(ds.Tables(0).Rows(0).Item("VT_CODE")).Selected = True
                categorycode = ds.Tables(0).Rows(0).Item("VT_CODE")
                getsubcategorybycat(categorycode)
                ddlAstSubCat.ClearSelection()
                ddlAstSubCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("AST_SUBCAT_CODE")).Selected = True
                txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("MANU_REM"))
            End If
        Else
            cleardata()

        End If
    End Sub

    Protected Sub gvBrand_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBrand.PageIndexChanging
        gvBrand.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Private Sub getassetbrand()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETBRANDDRP")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlBrand.DataSource = sp.GetDataSet()
        ddlBrand.DataTextField = "MANUFACTURER"
        ddlBrand.DataValueField = "MANUFACTUER_cODE"
        ddlBrand.DataBind()
        ddlBrand.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub ddlAssetCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        If ddlAssetCategory.SelectedIndex <> 0 Then
            getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        End If
    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        'ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx")
    End Sub
End Class
