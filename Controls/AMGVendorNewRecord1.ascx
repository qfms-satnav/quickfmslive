<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGVendorNewRecord1.ascx.vb" Inherits="Controls_AMGVendorNewRecord1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<script type="text/javascript">

    function ValidateCheckBoxList(sender, args) {
        var checkBoxList = document.getElementById("<%=chkVendorCat.ClientID %>");
        var checkboxes = checkBoxList.getElementsByTagName("input");
        var isValid = false;
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                isValid = true;
                break;
            }
        }
        args.IsValid = isValid;
    }
</script>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-10" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor Code<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfCode" runat="server" ControlToValidate="txtCode" Display="nONE" ErrorMessage="Please Enter Vendor Code" ValidationGroup="Val1">
            </asp:RequiredFieldValidator>
            <asp:TextBox ID="txtCode" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor Name<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="None" ErrorMessage="Please Enter Vendor Name" ValidationGroup="Val1">
            </asp:RequiredFieldValidator>
            <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor Type<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ErrorMessage="Please Select At Least One Vendor Type" ControlToValidate="lstTypes" runat="server" Display="None" ValidationGroup="Val1" />
            <asp:ListBox ID="lstTypes" CssClass="form-control" SelectionMode="Multiple" runat="server"></asp:ListBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label>Vendor Category<span style="color: red;">*</span></label>
            <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please Select At Least One Vendor Category."
                ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" Display="None" runat="server" ValidationGroup="Val1" />
            <asp:CheckBoxList ID="chkVendorCat" runat="server">
                <asp:ListItem Text="Supplier" Value="Supplier"></asp:ListItem>
                <asp:ListItem Text="Service" Value="Service"></asp:ListItem>
            </asp:CheckBoxList>


        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Address<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" Display="None" ErrorMessage="Please Enter Address" ValidationGroup="Val1">
            </asp:RequiredFieldValidator>
            <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" CssClass="form-control" Height="30%"></asp:TextBox>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>City<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="cvcity" runat="server" InitialValue="--Select--" ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City"
                ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select City">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Phone Number</label>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPhone" ValidationGroup="Val1"
                Display="None" ErrorMessage="Please Enter A Valid Phone Number" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
            <cc1:FilteredTextBoxExtender ID="ftetxtPhone" runat="server" TargetControlID="txtPhone" FilterType="Numbers" ValidChars="0123456789." />
            <asp:TextBox ID="txtPhone" MaxLength="13" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Mobile Number</label>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMobile" ValidationGroup="Val1"
                Display="None" ErrorMessage="Please Enter a Valid Mobile Number" ValidationExpression="^[7-9][0-9]{9}"></asp:RegularExpressionValidator>
            <cc1:FilteredTextBoxExtender ID="ftetxtMobile" runat="server" TargetControlID="txtMobile" FilterType="Numbers" ValidChars="0123456789." />
            <asp:TextBox ID="txtMobile" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Email Address</label>
            <%--<asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="Val1"
                Display="None" ErrorMessage="Please Enter Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
            </asp:RegularExpressionValidator>--%>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Bank Name</label>
            <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Bank A/C No</label>
            <asp:TextBox ID="txtBankAcNo" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Branch Name</label>
            <asp:TextBox ID="txtBranchName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>IFSC Code</label>
            <asp:TextBox ID="txtIFSCCode" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>


    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>PAN No.</label>
            <asp:TextBox ID="txtPANno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>TAN No.<span style="color: red;"></span></label>

            <asp:TextBox ID="txtTANno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>State<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvmonth" runat="server" ControlToValidate="ddlState"
                Display="none" ErrorMessage="Please Select State" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlState" runat="server"  CssClass="form-control selectpicker" data-live-search="true"
               AutoPostBack="true" ></asp:DropDownList>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>GST No.<span style="color: red;">*</span></label>
              <asp:RequiredFieldValidator ID="rggstno" runat="server" ControlToValidate="txtgsttno"
                Display="none" ErrorMessage="Please Enter GST No." ValidationGroup="Val1" ></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtgsttno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <%--<div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>WCT No.<span style="color: red;"></span></label>
            <asp:TextBox ID="txtwctno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>CST No.<span style="color: red;"></span></label>
            <asp:TextBox ID="txtcstno" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>ST. Assessment Circle<span style="color: red;"></span></label>
            <asp:TextBox ID="txtstcircle" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
    </div>--%>
     <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Status<span style="color: red;">*</span></label>
            <asp:RadioButtonList ID="rdbtnStatus" runat="server" CssClass="clsRadioButton"
                RepeatDirection="Horizontal">
                <asp:ListItem Value="1" Selected="True">Active &nbsp</asp:ListItem>
                <asp:ListItem Value="0">InActive</asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" Height="30%"></asp:TextBox>
        </div>
    </div>
     <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-default btn-primary" Text="Back" />
        </div>
    </div>
</div>
<%--<div class="row">

   
   
</div>--%>
<div class="row">
    <div class="col-md-3 text-left">
        <div class="form-group">
            <asp:TextBox ID="txtfindcode" runat="server" placeholder="Search By Any..." CssClass="form-control"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-2" style="padding-top: 22px">
        <div class="form-group">
            <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search"
                CausesValidation="true" TabIndex="2" Style="margin-top: -25px" />
        </div>
    </div>
    <div class="col-md-11 text-right">
        <div class="form-group">
            <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export To Excel" CausesValidation="False" Style="margin-top: -40px" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <asp:GridView ID="gvDetails_AVR" runat="server" AutoGenerateColumns="False" AllowSorting="True" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left"
                AllowPaging="True" EmptyDataText="No Asset Vendor Found." CssClass="table GridStyle" GridLines="none">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField HeaderText="Vendor Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName_AVR" runat="server" CssClass="lblVendorName" Text='<%#Eval("AVR_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress_AVR" runat="server" CssClass="lblVendorName" Text='<%#Eval("AVR_ADDR")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor City">
                        <ItemTemplate>
                            <asp:Label ID="lblCity_AVR" runat="server" CssClass="lblVendorCity" Text='<%#Eval("AVR_CITY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Country">
                        <ItemTemplate>
                            <asp:Label ID="lblCountry_AVR" runat="server" CssClass="lblVendorCountry" Text='<%#Eval("AVR_COUNTRY")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus_AVR" runat="server" CssClass="lblVendorStatus" Text='<%#Eval("AVR_STATUS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks_AVR" runat="server" CssClass="lblRemarks" Text='<%#Eval("AVR_REMARKS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <%--<a href='frmAMGVendorModify.aspx?code=<%#Eval("AVR_CODE")%>'>EDIT</a>--%>
                          <a href='frmAMGVendorModify.aspx?ID=<%# Eval("AVR_ID") %>&code=<%# Eval("AVR_CODE") %>'>EDIT</a>

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#d3d3d3" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<%--<cc1:ExportPanel ID="exportpanel1" runat="server">

    <div class="row" id="expDiv" Visible="false">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="form-group">
               
                <asp:GridView ID="gvExportToExcel" runat="server" AutoGenerateColumns="False" AllowSorting="True" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left"
                    AllowPaging="false" Visible="false" EmptyDataText="No Asset Vendor Found." CssClass="table table-bordered table-hover table-striped">
                    <PagerSettings Mode="NumericFirstLast" />
                    <Columns>
                        <asp:TemplateField HeaderText="Vendor Name">
                            <ItemTemplate>
                                <asp:Label ID="lblName_AVR" runat="server" CssClass="lblVendorName" Text='<%#Eval("AVR_NAME")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Address">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress_AVR" runat="server" CssClass="lblVendorName" Text='<%#Eval("AVR_ADDR")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor City">
                            <ItemTemplate>
                                <asp:Label ID="lblCity_AVR" runat="server" CssClass="lblVendorCity" Text='<%#Eval("AVR_CITY")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Country">
                            <ItemTemplate>
                                <asp:Label ID="lblCountry_AVR" runat="server" CssClass="lblVendorCountry" Text='<%#Eval("AVR_COUNTRY")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus_AVR" runat="server" CssClass="lblVendorStatus" Text='<%#Eval("AVR_STATUS")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks_AVR" runat="server" CssClass="lblRemarks" Text='<%#Eval("AVR_REMARKS")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                      <%--  <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <a href='frmAMGVendorModify.aspx?code=<%#Eval("AVR_CODE")%>'>EDIT</a>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
<%--  </Columns>
                    <HeaderStyle BackColor="#d3d3d3" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            
            </div>
        </div>
    </div>

</cc1:ExportPanel>--%>


