﻿
Partial Class Controls_HDMAdminViewRequisitions
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_ADMIN_REQUISITION_GetByAdminUserId")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        gvViewRequisitions.DataSource = sp.GetDataSet
        gvViewRequisitions.DataBind()
    End Sub

    Protected Sub gvViewRequisitions_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_ADMIN_REQUISITION_GetByAdminUserId_REQID")
        sp.Command.AddParameter("@REQ_ID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        gvViewRequisitions.DataSource = sp.GetDataSet
        gvViewRequisitions.DataBind()
    End Sub
End Class
