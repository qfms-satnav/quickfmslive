<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddVendorRateContract.ascx.vb" Inherits="Controls_AddVendorRateContract" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style>
   /* select#AddVendorRateContract1_ddlAstBrand {
        display: block !important;
        width: 100%;
        padding: 7px;
        background: #f8f9fa;
        border: 1px solid #ced4da;
    }*/
</style>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-10" ForeColor="Red"></asp:Label>

            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 20px">
    <div class="col-md-6 text-right">
        <label class="col-md-2 btn pull-right">
            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                ToolTip="Please Select Add to add new Asset Subcategory and Select Modify to modify the existing Asset Subcategory" />
            Add
        </label>
    </div>
    <div class="col-md-6">
        <label class="col-md-2 btn pull-left">
            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                ToolTip="Please Select Add to add new Asset Subcategory and Select Modify to modify the existing Asset Subcategory" />
            Modify
        </label>
    </div>
</div>

<div id="mainpanel" runat="server">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label visible="false">Vendor<span style="color: red;">*</span></label>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVendor"
                    Display="none" ErrorMessage="Please Select Vendor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Vendor" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                    Display="none" ErrorMessage="Please Select Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                    ToolTip="--Select--" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Sub Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                    Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                    ToolTip="--Select--" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                    ToolTip="--Select--" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblAssetModel" runat="server">Asset Model<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlModel"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"
                    ToolTip="--Select--" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblHYDate" runat="server">Contract From Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="txtFDate"
                    ErrorMessage="Please Select From Date" ValidationGroup="Val1" SetFocusOnError="True"
                    Display="None"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtFDate" ControlToCompare="txtTDate" Type="Date" Operator="LessThan" SetFocusOnError="true" ErrorMessage="From Date must be Lesser Than ToDate...."></asp:CompareValidator>
                <div class='input-group date' id='fromdate'>
                    <asp:TextBox ID="txtFDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="Label1" runat="server">Contract To Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtTDate"
                    ErrorMessage="Please Select To Date" ValidationGroup="Val1" SetFocusOnError="True"
                    Display="None"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CV1" runat="server" ControlToValidate="txtTDate" ControlToCompare="txtFDate" Type="Date" Operator="GreaterThan" SetFocusOnError="true" ErrorMessage="InValid Date Format...."></asp:CompareValidator>
                <div class='input-group date' id='Div1'>
                    <asp:TextBox ID="txtTDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="subpanel" runat="server" visible="false">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblVenRate" runat="server">Vendor Rate Contract <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlVenRate"
                    Display="none" ErrorMessage="Please Select Vendor Rate Contract " ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlVenRate" runat="server" CssClass="selectpicker" data-live-search="true"
                    ToolTip="Select Asset Model"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblVendorName" runat="server">Vendor Name</label>
                <asp:TextBox ID="lblVendorNameData" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblCat" runat="server">Category Name</label>
                <asp:TextBox ID="lblCatData" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblSubCat" runat="server">Sub Category Name</label>
                <asp:TextBox ID="lblSubCatData" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblBrand" runat="server">Brand/Make</label>
                <asp:TextBox ID="lblBrandData" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="lblModel" runat="server">Model</label>
                <asp:TextBox ID="lblModelData" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="Label2" runat="server">From Date</label>
                <div class='input-group date' id='Div2'>
                    <asp:TextBox ID="lblFDate" runat="server" CssClass="form-control"></asp:TextBox><span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('lblFDate')"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label id="Label4" runat="server">To Date</label>
                <div class='input-group date' id='Div3'>
                    <asp:TextBox ID="lblTDate" runat="server" CssClass="form-control"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('lblTDate')"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Actual Cost. <span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtActualCost"
                Display="none" ErrorMessage="Please Enter Asset Actual Cost" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPrice"
                ErrorMessage="Please enter price in numbers" Display="None" ValidationExpression="^[0-9]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtActualCost"
                FilterMode="ValidChars" ValidChars="0123456789">
            </cc1:FilteredTextBoxExtender>

            <div onmouseover="Tip('Enter price in numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtActualCost" runat="server" CssClass="form-control" MaxLength="10" AutoPostBack="true"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Discount(%) <span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtDiscount"
                Display="none" ErrorMessage="Please Enter Discount(%)" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPrice"
                ErrorMessage="Please enter price in numbers" Display="None" ValidationExpression="^[0-9]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtDiscount"
                FilterMode="ValidChars" ValidChars="0123456789">
            </cc1:FilteredTextBoxExtender>

            <div onmouseover="Tip('Enter price in numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="true"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Cost per unit Qty<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtPrice"
                Display="none" ErrorMessage="Please Enter Price" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtPrice"
                ErrorMessage="Please enter price in numbers" Display="None" ValidationExpression="^[0-9]+"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>

            <div onmouseover="Tip('Enter price in numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtRemarks" runat="server" Height="30%" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back" />
            <%--BackColor="#209e91"--%>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="horizontal-scroll">
                <asp:GridView ID="gvBrand" runat="server" AllowPaging="True" AllowSorting="False" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left"
                    PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Vendor Rate Contract Found." CssClass="table GridStyle" GridLines="none">
                    <PagerSettings Mode="NumericFirstLast" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("AMG_VENCON_ID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Category">
                            <ItemTemplate>
                                <asp:Label ID="lblCat" runat="server" CssClass="lblCat" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Sub Category">
                            <ItemTemplate>
                                <asp:Label ID="lblSubCat" runat="server" CssClass="lblCat" Text='<%#BIND("AST_SUBCAT_NAME")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Brand/Make">
                            <ItemTemplate>
                                <asp:Label ID="lblAstBrnd" runat="server" CssClass="lblCat" Text='<%#BIND("manufacturer")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Model">
                            <ItemTemplate>
                                <asp:Label ID="lblCode" runat="server" CssClass="lblCode" Text='<%#BIND("AST_MD_NAME")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor Name">
                            <ItemTemplate>
                                <asp:Label ID="lblvENnAME" runat="server" Text='<%#Eval("AVR_NAME")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Actual Cost">
                            <ItemTemplate>
                                <asp:Label ID="lblAstActCost" runat="server" CssClass="lblCode" Text='<%# Eval("AMG_ACT_AMT", "{0:c2}")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Discount (%)">
                            <ItemTemplate>
                                <asp:Label ID="lblAstDiscCost" runat="server" CssClass="lblCode" Text='<%# Eval("AMG_DISC_AMT")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Cost">
                            <ItemTemplate>
                                <asp:Label ID="lblAstCost" runat="server" CssClass="lblCode" Text='<%# Eval("AMG_VENCAN_RT","{0:c2}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="From Date">
                            <ItemTemplate>
                                <asp:Label ID="lblFromDate" runat="server" CssClass="lblFDate" Text='<%#Bind("AMG_VEN_FDATE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To Date">
                            <ItemTemplate>
                                <asp:Label ID="lblToDate" runat="server" CssClass="lblToDate" Text='<%#Bind("AMG_VEN_TDATE")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%--<HeaderStyle BackColor="#d3d3d3" />--%>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
</div>
<script type="text/ecmascript">
    //$("#btnSubmit").click(function () {
    //    $('#lblMsg').text("")
    //});
    function refreshSelectpicker() {
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlVendor.ClientID%>").selectpicker();
        $("#<%=ddlModel.ClientID%>").selectpicker();
        $("#<%=ddlVenRate.ClientID%>").selectpicker();
    }
    refreshSelectpicker();
</script>
