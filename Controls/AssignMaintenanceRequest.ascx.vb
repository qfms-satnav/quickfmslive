Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AssignMaintenanceRequest
    Inherits System.Web.UI.UserControl

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_MAINTENANCEREQ")
            sp.Command.AddParameter("@PN_MAINTENANCE_REQ", ddlid.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@REQUEST_ASSIGNED_TO", ddlUser.SelectedItem.Value, DbType.String)
            sp.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=20")

            'lblMsg.Text = "Request Assigned Succesfully"
            fillgrid()
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub Cleardata()
        ddlid.SelectedValue = 0
        ddlUser.SelectedValue = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_OPENIDS")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlid.DataSource = sp.GetDataSet()
            ddlid.DataTextField = "PN_MAINTENANCE_REQ"
            ddlid.DataBind()

            ddlid.Items.Insert(0, New ListItem("--Select ID--", "0"))

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETUSER_BYLOCATION")
            sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddlUser.DataSource = sp1.GetDataSet()
            ddlUser.DataTextField = "AUR_FIRST_NAME"
            ddlUser.DataValueField = "AUR_ID"
            ddlUser.DataBind()

            ddlUser.Items.Insert(0, New ListItem("--Select User--", "0"))
        End If

    End Sub
    Public Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSIGNED")
        sp.Command.AddParameter("@PN_MAINTENANCE_REQ", ddlid.SelectedItem.Text, DbType.String)
        gvmainreq.datasource = sp.GetDataSet()
        gvmainreq.DataBind()
        For i As Integer = 0 To gvmainreq.Rows.Count - 1
            Dim lblstatus As Label = CType(gvmainreq.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = 0 Then
                lblstatus.Text = "Pending"
            ElseIf lblstatus.Text = 1 Then
                lblstatus.Text = "In Progress"
            Else
                lblstatus.Text = 2
                lblstatus.Text = "Completed"

            End If
        Next
        'lbtn1.Visible = False

    End Sub

    'Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSIGNEDRECORD")
    '    sp.Command.AddParameter("PN_MAINTENANCE_REQ", txtfindcode.Text, DbType.String)
    '    gvmainreq.DataSource = sp.GetDataSet()
    '    gvmainreq.DataBind()
    '    For i As Integer = 0 To gvmainreq.Rows.Count - 1
    '        Dim lblstatus As Label = CType(gvmainreq.Rows(i).FindControl("lblstatus"), Label)
    '        If lblstatus.Text = "0" Then
    '            lblstatus.Text = "Pending"
    '        ElseIf lblstatus.Text = "1" Then
    '            lblstatus.Text = "In Progress"
    '        Else
    '            lblstatus.Text = "2"
    '            lblstatus.Text = "Completed"

    '        End If
    '    Next

    '    lbtn1.Visible = True
    'End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/Workspace/SMS_webfiles/frmMaintenanceDesk.aspx")
    End Sub
End Class
