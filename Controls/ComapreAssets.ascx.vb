Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Threading
Imports System.Configuration
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Collections.Generic

Partial Class ComapreAssets
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMsg.Visible = False
            'GridView1.Visible = False
            gvCompare.Visible = False
        End If
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            Dim connectionstring As String = ""

            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False

                Dim fs As System.IO.FileStream

                
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim filename As String = fpBrowseDoc.PostedFile.FileName
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "ComapreAssets\", "\", "\\") + filename
                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException

                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "ComapreAssets\" + fpBrowseDoc.FileName)



                'Connection String to Excel Workbook
                If strFileType.Trim() = ".xls" Then
                    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                ElseIf strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1
                Dim asset_codecnt As Integer = 1
                Dim space_idcnt As Integer = 1
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "space_id" Then
                            space_idcnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SPACE_ID"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "asset_code" Then
                            asset_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be ASSET_CODE"
                            Exit Sub
                        End If

                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_INSERT_SPACEASSETEXCEL")
                    sp.Command.AddParameter("@SNO", ds.Tables(0).Rows(i).Item("SNO").ToString, DbType.String)
                    sp.Command.AddParameter("@SPACE_ID", ds.Tables(0).Rows(i).Item("SPACE_ID").ToString, DbType.String)
                    sp.Command.AddParameter("@ASSET_CODE", ds.Tables(0).Rows(i).Item("ASSET_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@USR_ID", Session("Uid"), DbType.String)
                    sp.ExecuteScalar()
                Next
                Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_INSERT_SPACEASSETEXCELGETDATANEW")
                sp3.Command.AddParameter("@USR_ID", Session("Uid"), DbType.String)
                gvCompare.Visible = True
                gvCompare.DataSource = sp3.GetDataSet
                gvCompare.DataBind()
                btnExport.Visible = True
                'GridView1.Visible = False
                'GridView1.DataSource = sp3.GetDataSet
                'GridView1.DataBind()
                'btnExport1.Visible = True
                'Dim dt1 As New DataTable()
                'dt1.Columns.Add("BDG_ID", GetType(String))
                'dt1.Columns.Add("TWR_ID", GetType(String))
                'dt1.Columns.Add("FLR_ID", GetType(String))
                'dt1.Columns.Add("ASSET_CODE", GetType(String))
                'dt1.Columns.Add("ASSET_SPACE", GetType(String))
                'dt1.Columns.Add("ASSET_STATUS", GetType(String))
                'If snocnt = 1 And asset_codecnt = 1 And space_idcnt = 1 Then

                '    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                '        Dim spacecode As String = ds.Tables(0).Rows(i).Item(1).ToString
                '        Dim assetcode As String = ds.Tables(0).Rows(i).Item(2).ToString
                '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CHK_SPACE_ID_WITHASSETCODE")
                '        sp.Command.AddParameter("@AAT_AST_SPACEID", spacecode, DbType.String)
                '        sp.Command.AddParameter("@AAT_CODE", assetcode, DbType.String)
                '        Dim ds1 As New DataSet

                '        ds1 = sp.GetDataSet()
                '        dt1 = ds1.Tables(0)
                '        Dim bdg_id As String = ds1.Tables(0).Rows(0).Item("BDG_ID").ToString
                '        Dim twr_id As String = ds1.Tables(0).Rows(0).Item("TWR_ID").ToString
                '        Dim flr_id As String = ds1.Tables(0).Rows(0).Item("FLR_ID").ToString
                '        Dim asst_code As String = ds1.Tables(0).Rows(0).Item("ASSET_CODE").ToString
                '        Dim spc_id As String = ds1.Tables(0).Rows(0).Item("ASSET_SPACE").ToString
                '        Dim asst_status As String = ds1.Tables(0).Rows(0).Item("ASSET_STATUS").ToString
                '        Dim NewRow As DataRow = dt1.NewRow()

                '        NewRow(0) = bdg_id
                '        NewRow(1) = twr_id
                '        NewRow(2) = flr_id
                '        NewRow(3) = asst_code
                '        NewRow(4) = spc_id
                '        NewRow(5) = asst_status
                '        dt1.Rows.Add(bdg_id, twr_id, flr_id, asst_code, spc_id, asst_status)

                '        gvCompare.DataSource = sp.GetDataSet()
                '        gvCompare.DataBind()
                '    Next
               
                'Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CREATE_TEMPTTABLE")
                'sp2.Command.AddParameter("@dummy", 1, DbType.Int32)
                'sp2.ExecuteScalar()


                'Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_INSERT_SPACEASSETEXCELGETDATA")
                'sp3.Command.AddParameter("@USR_ID", Session("Uid"), DbType.String)
                'GridView1.Visible = True
                'GridView1.DataSource = sp3.GetDataSet
                'GridView1.DataBind()
                'btnExport1.Visible = True
            Else
                gvCompare.Visible = False
                'GridView1.Visible = False
                btnExport.Visible = False
                'btnExport1.Visible = False
                lblMsg.Visible = True
                lblMsg.Text = "Please select file..."

            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Private Function checkassetcode(ByVal assetcode As String) As String
        Dim assetcode1 As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CHECK_ASSETCODE")
        sp.Command.AddParameter("@ASSET_CODE", assetcode, DbType.String)
        assetcode1 = sp.ExecuteScalar
        Return assetcode1
    End Function
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Export("Compare_Asset_Data.xls", gvCompare)
    End Sub

   
    'Protected Sub btnExport1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport1.Click
    '    Export("Compare_Asset_Data1.xls", GridView1)
    'End Sub

  
End Class
