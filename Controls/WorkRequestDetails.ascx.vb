Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_WorkRequestDetails
    Inherits System.Web.UI.UserControl

    ' Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    'Meta tags
    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '' Styles
    'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))

    '' Styles
    'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    '************************

    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindReq()
            ddlpay.Visible = False
            lblpay.Visible = False
            panel1.Visible = False
            panel2.Visible = False
            BindDetails(Request.QueryString("REQID"))
            btnBack.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")

        End If
    End Sub
    Private Sub BindReq()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_REQWORK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
    End Sub
    Private Sub BindDetails(ByVal req As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETWORKREQDLS")
            sp.Command.AddParameter("@dummy", req, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            'Dim rds As New ReportDataSource()
            'rds.Name = "WorkRequestReport"
            'rds.Value = ds.Tables(0)
            ''This refers to the dataset name in the RDLC file
            'ReportViewer1.Reset()
            'ReportViewer1.LocalReport.DataSources.Add(rds)
            'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/WorkRequestReport.rdlc")
            'ReportViewer1.LocalReport.Refresh()
            'ReportViewer1.SizeToReportContent = True
            'ReportViewer1.Visible = True

            If ds.Tables(0).Rows.Count > 0 Then
                Dim li As ListItem = Nothing
                li = ddlWorkRequest.Items.FindByText(ds.Tables(0).Rows(0).Item("PN_WORKREQUEST_REQ"))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                txtBuilding.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
                txtproperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT")
                txtVendorName.Text = ds.Tables(0).Rows(0).Item("VENDOR_NAME")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("Remarks")
                Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                If status = 0 Then
                    ddlwstatus.SelectedValue = 0
                ElseIf status = 1 Then
                    ddlwstatus.SelectedValue = 1
                Else
                    ddlwstatus.SelectedValue = 2
                End If
                Dim mode As String = Nothing
                mode = ds.Tables(0).Rows(0).Item("PAYMENT_MODE")
                If mode = "0" Then
                    ddlpay.Visible = False
                    lblpay.Visible = False
                    panel1.Visible = False
                    panel2.Visible = False
                ElseIf mode = "1" Then
                    ddlpay.Visible = True
                    lblpay.Visible = True
                    ddlpay.SelectedValue = 1
                    panel1.Visible = True
                    panel2.Visible = False
                    txtCheque.Text = ds.Tables(0).Rows(0).Item("ChequeNo")
                    txtBankName.Text = ds.Tables(0).Rows(0).Item("IssuingBank")
                    txtAccNo.Text = ds.Tables(0).Rows(0).Item("AccountNumber")
                ElseIf mode = "2" Then
                    ddlpay.SelectedValue = 2
                    ddlpay.Visible = True
                    lblpay.Visible = True
                    panel1.Visible = False
                    panel2.Visible = False
                ElseIf mode = "3" Then
                    ddlpay.SelectedValue = 3
                    ddlpay.Visible = True
                    lblpay.Visible = True
                    panel1.Visible = False
                    panel2.Visible = True
                    txtIBankName.Text = ds.Tables(0).Rows(0).Item("IssuingBank")
                    txtDeposited.Text = ds.Tables(0).Rows(0).Item("DepositedBank")
                    txtIFCB.Text = ds.Tables(0).Rows(0).Item("IFSC")
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/Workspace/SMS_Webfiles/frmWorkRequestReport.aspx")
    End Sub
End Class
