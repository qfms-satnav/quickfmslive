<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LveApproval.ascx.vb" Inherits="Controls_LveApproval" %>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Leave Approval Found.">
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_REQ_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested By">
                    <ItemTemplate>
                        <asp:Label ID="lblReqBy" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_REQ_BY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From Date">
                    <ItemTemplate>
                        <asp:Label ID="lblFromDate" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_FDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Date">
                    <ItemTemplate>
                        <asp:Label ID="lblToDate" runat="server" CssClass="bodyText" Text='<%#Eval("LMS_TDATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="No Of Days">
                    <ItemTemplate>
                        <asp:Label ID="lblTotDays" runat="Server" CssClass="bodyText" Text='<%#Eval("LMS_NO_DAYS") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reason for Leave">
                    <ItemTemplate>
                        <asp:Label ID="lblreason" runat="Server" CssClass="bodyText" Text='<%#Eval("REASON") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested On">
                    <ItemTemplate>
                        <asp:Label ID="lblReqOn" runat="Server" CssClass="bodyText" Text='<%#Eval("LMS_REQ_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View">
                    <ItemTemplate>
                        <a href='frmLveRMApproval.aspx?ReqID=<%#Eval("LMS_REQ_ID") %>'>View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>






