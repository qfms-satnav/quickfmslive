Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_EditLease
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        If Not IsPostBack Then
            table2.Visible = False
            BindLType()
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            gvLDetails_Lease.PageIndex = 0
            fillgrid()
            grid()
            table2.Visible = True

        End If
    End Sub
    Public Sub grid()
        Try
            If gvLDetails_Lease.Rows.Count > 0 Then
                '    t1.Visible = True
                table2.Visible = True
            Else
                't1.Visible = False
                table2.Visible = True
                'table2.Visible = False
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLType()
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
            sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlLtype.DataSource = sp3.GetDataSet()
            ddlLtype.DataTextField = "PN_LEASE_TYPE"
            ddlLtype.DataValueField = "PN_LEASE_ID"
            ddlLtype.DataBind()

            ddlLtype.Items.Insert(0, New ListItem("--Select LeaseType--", "0"))


        Catch ex As Exception

        End Try
    End Sub
    Public Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LEASE_DETAILS")
        sp.Command.AddParameter("@LEASE_TYPE", ddlLtype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LEASE_CODE", txtfindcode.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvLDetails_Lease.DataSource = ds
        gvLDetails_Lease.DataBind()
        For i As Integer = 0 To gvLDetails_Lease.Rows.Count - 1
            Dim lblLstatus As Label = CType(gvLDetails_Lease.Rows(i).FindControl("lblLstatus"), Label)
            If lblLstatus.Text = 1 Then
                lblLstatus.Text = "Active"
            Else
                lblLstatus.Text = "Terminated"
            End If
        Next
        lbtn1.Visible = False
    End Sub
    Protected Sub gvLDetails_Lease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetails_Lease.PageIndexChanging
        gvLDetails_Lease.PageIndex = e.NewPageIndex
        fillgrid()
        table2.Visible = True
        grid()
    End Sub
    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
        fillgrid()
        grid()
        table2.Visible = True
        lbtn1.Visible = True
    End Sub
    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
       
            gvLDetails_Lease.PageIndex = 0
            fillgrid()
            table2.Visible = True
            txtfindcode.Text = ""
            grid()
			lbtn1.Visible=False
        
    End Sub

    Protected Sub gvLDetails_Lease_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLDetails_Lease.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lbllname As Label = DirectCast(gvLDetails_Lease.Rows(rowIndex).FindControl("lbllname"), Label)
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DEL_LEASE")
            SP1.Command.AddParameter("@LEASE_ID", lbllname.Text, DbType.String)

            SP1.ExecuteScalar()
        End If
        fillgrid()
        grid()
        table2.Visible = True
    End Sub

    Protected Sub gvLDetails_Lease_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvLDetails_Lease.RowDeleting

    End Sub

    Protected Sub ddlLtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLtype.SelectedIndexChanged
        Try
            fillgrid()
            grid()
            table2.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
