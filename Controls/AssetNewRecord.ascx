<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetNewRecord.ascx.vb"
    Inherits="Controls_AssetNewRecord" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Asset<hr align="center" width="60%" /> </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                    </td>
                </tr>
            </table>
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="85%" Height="100%">
                <table id="Table3" cellspacing="0" cellpadding="0" style="vertical-align: top; width: 95%;"
                    align="center" border="0">
                      <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td style="width: 10px">
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER" align="left">
                            <strong>&nbsp; Add New Record </strong>
                        </td>
                        <td style="width: 17px">
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                            &nbsp;</td>
                        <td align="left">
                         <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            Code <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                                                Display="None" ErrorMessage="Please enter code !" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:TextBox ID="txtCode" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            Name <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                                Display="None" ErrorMessage="Please enter Name !" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            ModelName <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvModelName" runat="server" ControlToValidate="txtModelName"
                                                Display="None" ErrorMessage="Please enter ModelName !" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:TextBox ID="txtModelName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetGroupName <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="cvAssetName" runat="server" InitialValue="0" ControlToValidate="drdGrp_Name"
                                                Display="None" ErrorMessage="Please Select AssetName !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:DropDownList ID="drdGrp_Name" runat="server" CssClass="clsComboBox" Width="97%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetVendorName <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="cvVendorName" runat="server" InitialValue="0" ControlToValidate="drdVendor_Name"
                                                Display="None" ErrorMessage="Please Select VendorName !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:DropDownList ID="drdVendor_Name" runat="server" CssClass="clsComboBox" Width="97%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetBrandName <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="cvBrandName" runat="server" InitialValue="0" ControlToValidate="drdBrand_Name"
                                                Display="None" ErrorMessage="Please Select BrandName !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:DropDownList ID="drdBrand_Name" runat="server" CssClass="clsComboBox" Width="97%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetRequired <font class="clsNote">*</font>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:RadioButtonList ID="assetreq" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                                Width="97%" CellPadding="0" CellSpacing="0" RepeatLayout="Flow">
                                                <asp:ListItem Value="True" Selected="True">Yes</asp:ListItem>
                                                <asp:ListItem Value="False">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                  
               
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            Asset Status<font class="clsNote">*</font>
                                            </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:RadioButtonList ID="assetid" runat="server" CssClass="clsRadioButton" Width="97%"
                                                RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0" RepeatLayout="Flow">
                                                <asp:ListItem Value="True" Selected="True">Active</asp:ListItem>
                                                <asp:ListItem Value="False">InActive</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            Asset Owned<font class="clsNote">*</font>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:RadioButtonList ID="assetOwned" runat="server" CssClass="clsRadioButton" Width="97%"
                                                RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0" RepeatLayout="Flow">
                                                <asp:ListItem Value="True" Selected="True">Owned</asp:ListItem>
                                                <asp:ListItem Value="False">Rented</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetPurchasedStatus<font class="clsNote">*</font>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:RadioButtonList ID="AssetPurchasedStatus" CssClass="clsRadioButton" runat="server"
                                                Width="97%" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem Value="True" Selected="True">Purchased</asp:ListItem>
                                                <asp:ListItem Value="False">Rebilished</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetSpaceFixed<font class="clsNote">*</font>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:RadioButtonList ID="rdbtnAssetSpace" runat="server" CssClass="clsRadioButton"
                                                Width="97%" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem Value="True" Selected="True">Fixed</asp:ListItem>
                                                <asp:ListItem Value="False">Movable</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetUser<font class="clsNote">*</font>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:RadioButtonList ID="rdbtnAssetUser" runat="server" Width="97%" RepeatDirection="horizontal"
                                                RepeatLayout="Flow">
                                                <asp:ListItem Value="True">Fixed</asp:ListItem>
                                                <asp:ListItem Value="False" Selected="True">Movable</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            AssetCondition<font class="clsNote">*</font>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:RadioButtonList ID="rdbtnAssetCons" CssClass="clsRadioButton" runat="server"
                                                Width="97%" RepeatDirection="Horizontal" CellPadding="1" RepeatLayout="Flow">
                                                <asp:ListItem Value="True" Selected="True">Workable</asp:ListItem>
                                                <asp:ListItem Value="False">Disposable</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left"  style="height: 26px;width:50%">
                                            Remarks<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfRemarks" runat="server" ControlToValidate="txtRemarks"
                                                Display="None" ErrorMessage="Please enter Remarks" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left"  style="height: 26px;width:50%">
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" Width="97%" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Width="76px" Text="Submit"
                                                ValidationGroup="Val1" />
                                        </td>
                                    </tr>
                                    
                                </table>
                            </asp:Panel>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px; width: 17px;">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
    </Triggers>
</asp:UpdatePanel>
