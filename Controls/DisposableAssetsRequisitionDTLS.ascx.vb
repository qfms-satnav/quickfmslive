Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class Controls_DisposableAssetsRequisitionDTLS
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim Req_id As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Req_id = Request.QueryString("Req_id")
            'GetDetailsByRequistion(Req_id)
            GetSurDetailsByRequistion(Req_id)
        End If
    End Sub

    Private Sub GetSurDetailsByRequistion(ByVal Req_id As String)
        ' Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Req_id
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("GET_DISPOSABLE_ASSETREQ_BYREQ", param)
        gvDispReqDetails.DataSource = ds
        gvDispReqDetails.DataBind()
        Dim Status_Id As Integer
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Status_Id = Integer.Parse(ds.Tables(0).Rows(i)("STA_ID").ToString())
        Next
        If Status_Id = "1042" Or Status_Id = "1041" Then
            btnApprov.Enabled = False
        Else
            btnApprov.Enabled = True

        End If
        'ObjSubsonic.BindGridView(gvDispReqDetails, "GET_DISPOSABLE_ASSETREQ_BYREQ", param)
    End Sub

    'Private Sub GetDetailsByRequistion(ByVal Req_id As String)
    '    Dim ds As New DataSet
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
    '    param(0).Value = Req_id
    '    ds = ObjSubsonic.GetSubSonicDataSet("GET_ALLAST_DISPOSABLEREQ_BYREQ", param)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        lblAstId.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
    '        lblAstName.Text = ds.Tables(0).Rows(0).Item("AAT_DESC")
    '        lblModelName.Text = ds.Tables(0).Rows(0).Item("AAT_MODEL_NAME")
    '        lblLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
    '        lblAstAllocDt.Text = ds.Tables(0).Rows(0).Item("AAT_ALLOCATED_DATE")
    '        If ds.Tables(0).Rows(0).Item("AAT_SURRENDERED_DATE") = "Jan  1 1900" Then
    '            lblAstSurDt.Text = (DateTime.Now).ToString("dd/MM/yyyy")
    '        Else
    '            lblAstSurDt.Text = ds.Tables(0).Rows(0).Item("AAT_SURRENDERED_DATE")
    '        End If

    '        lblSurReq_id.Text = Req_id
    '        If ds.Tables(0).Rows(0).Item("AAT_UPT_DT") = "Jan  1 1900" Then
    '            lblAstDate.Text = (DateTime.Now).ToString("dd/MM/yyyy")
    '        Else
    '            lblAstDate.Text = ds.Tables(0).Rows(0).Item("AAT_UPT_DT")
    '        End If
    '        lblAdminDate.Text = ds.Tables(0).Rows(0).Item("SREQ_ADMINAPPROVAL_DATE")
    '        lblAdminName.Text = ds.Tables(0).Rows(0).Item("ADMIN_NAME")
    '        lblAdminRemarks.Text = ds.Tables(0).Rows(0).Item("SREQ_ADMIN_REMARKS")
    '        lbl_cost.Text = ds.Tables(0).Rows(0).Item("AST_ACTUAL_COST")
    '        lbl_Dep_per.Text = ds.Tables(0).Rows(0).Item("AST_DEPR_PERCENTAGE")
    '        lbl_Dep_val.Text = ds.Tables(0).Rows(0).Item("AST_DEPR_COST")
    '        lbl_Cur_val.Text = ds.Tables(0).Rows(0).Item("AST_CURRENT_VALUE")
    '    End If
    'End Sub

    Protected Sub btnApprov_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprov.Click
        Approve_Reject(1041)
        Response.Redirect("frmAssetThanks.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))
    End Sub
    Protected Sub btnreject_Click(sender As Object, e As EventArgs) Handles btnreject.Click
        Approve_Reject(1042)
        Response.Redirect("frmAssetThanks.aspx?RID=outwardrej&MReqId=" + Request.QueryString("REQ_ID"))
    End Sub
    Public Sub Approve_Reject(ByVal Staid As String)
        Dim REQ = Request.QueryString("REQ_ID")
        ' ------- Asset Dispose -----------------
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = REQ
        param(1) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Session("UID")
        param(2) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.NVarChar, 2000)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@REQ_STATUS", SqlDbType.NVarChar, 2000)
        param(3).Value = Staid
        ObjSubsonic.GetSubSonicDataSet("UPDATEDISPOSABLE_REQUISTION_byAdmin", param)
        send_mail(REQ)
        If (Staid = 1041) Then
            DownloadDisposal()
        End If
    End Sub

    Public Sub send_mail(ByVal reqid As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION_APPROVAL")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Execute()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmDisposableAssetsRequisition.aspx")
    End Sub
    Protected Sub gvDispReqDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDispReqDetails.RowCommand
        If e.CommandName = "Document" Then

            Dim lnkDisposal As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvDispReqDetails As GridViewRow = DirectCast(lnkDisposal.NamingContainer, GridViewRow)

            Dim orgfilename As String = lnkDisposal.Text
            Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
            If filePath <> "" Then
                ''Dim path As String = Server.MapPath(filePath)
                Dim file As System.IO.FileInfo = New System.IO.FileInfo(filePath)

                If file.Exists Then
                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                    Response.AddHeader("Content-Length", file.Length.ToString())
                    Response.ContentType = "application/octet-stream"
                    Response.WriteFile(file.FullName)
                    Response.[End]()
                Else
                    Response.Write("This file does not exist.")
                End If
            End If
        End If

    End Sub
    Private Sub DownloadDisposal()
        Try
            Dim viewer As ReportViewer = New ReportViewer()
            ''Dim param() As SqlParameter
            Dim ObjSubSonic As New clsSubSonicCommonFunctions

            Dim reqid = Request.QueryString("REQ_ID")
            Dim reqid1 = Request.QueryString("REQ_ID")
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ALLDESPOSED_REPORT")
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session("UID").ToString(), DbType.String)
            sp.Command.AddParameter("@Reqid", reqid, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()

            Dim rds As ReportDataSource = New ReportDataSource()
            rds.Name = "AssetDisposeDT"
            rds.Value = ds.Tables(0)
            Dim rds1 As ReportDataSource = New ReportDataSource()
            rds1.Name = "DisposeApproved"
            rds1.Value = ds.Tables(1)
            viewer.Reset()
            viewer.LocalReport.DataSources.Add(rds)
            viewer.LocalReport.DataSources.Add(rds1)
            viewer.LocalReport.EnableExternalImages = True
            viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetDisposalApprvdReport.rdlc")

            Dim ci As New CultureInfo(Session("userculture").ToString())
            Dim nfi As NumberFormatInfo = ci.NumberFormat
            'Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
            'Dim p2 As New ReportParameter("ImageVal", BindLogo())
            viewer.LocalReport.EnableExternalImages = True
            Dim imagePath As String = BindLogo()
            Dim parameter As New ReportParameter("Imagepath", imagePath)
            viewer.LocalReport.SetParameters(parameter)
            viewer.LocalReport.Refresh()

            viewer.ProcessingMode = ProcessingMode.Local
            ''viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetDisposalApprvdReport.rdlc")

            reqid = reqid.Substring(reqid.Length - 1)
            Dim FileName As String = "Dispose Report" & reqid & ".pdf" 'DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            ''Dim FileName As String = "Finalize PO" & DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            Dim extension As String
            Dim FileFullpath As String
            Dim encoding As String
            Dim mimeType As String
            Dim streams As String()
            Dim warnings As Warning()
            Dim contentType As String = "application/pdf"
            Dim mybytes As Byte() = viewer.LocalReport.Render("PDF", Nothing, extension, encoding, mimeType, streams, warnings)
            FileFullpath = Server.MapPath("~/AssetDisposal_PDF/") & FileName
            Using fs As FileStream = File.Create(Server.MapPath("~/AssetDisposal_PDF/") & FileName)
                fs.Write(mybytes, 0, mybytes.Length)
            End Using

            Response.ContentType = contentType
            Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
            ''Response.WriteFile(Server.MapPath("~/FinalizePO_pdfs/" & FileName))
            ''Response.Flush()
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "sp_get_Disposal_mails")
            sp3.Command.AddParameter("@Reqid", reqid1, DbType.String)
            Dim ds3 As DataSet = sp3.GetDataSet()
            If (ds3.Tables(0).Rows.Count > 0) Then
                SendMail(FileFullpath, ds3.Tables(0).Rows(0)("TOMAIL").ToString(), ds3.Tables(0).Rows(0)("CCMAIL").ToString())
            End If
        Catch ex As Exception
            ex.ToString()

        End Try
    End Sub



    Public Function BindLogo() As String
        'Dim imagePath As String
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            Return "https://live.quickfms.com/BootStrapCSS/images/" & ds3.Tables(0).Rows(0).Item("IMAGENAME")

        Else
            Return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png"
        End If
        'Return New Uri(Server.MapPath(imagePath)).AbsoluteUri
    End Function

    Public Shared Sub SendMail(ByVal FileFullpath As String, ByVal TOMAIL As String, ByVal CCMAIL As String)

        Dim smtp As SmtpClient = New SmtpClient()
        Dim fromaddr As MailAddress = New MailAddress(ConfigurationManager.AppSettings("from"))
        smtp.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("mailid"), ConfigurationManager.AppSettings("password"))
        smtp.Host = ConfigurationManager.AppSettings("Host")
        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings("Port"))
        smtp.EnableSsl = True
        Dim mailMessage As MailMessage = New MailMessage()
        mailMessage.From = fromaddr
        mailMessage.Subject = "Dispose Report"

        'Dim TOemailarr As String = TOMAIL
        mailMessage.[To].Add(TOMAIL)
        'Dim TOemailarr As String() = ConfigurationManager.AppSettings("GLOBALTO").ToString().Split(";"c)

        'For Each [to] As String In TOemailarr
        '    mailMessage.[To].Add([to])
        'Next
        'Dim Bccemailarr As String = CCMAIL
        'Dim Bccemailarr As String() = ConfigurationManager.AppSettings("GLOBALBCC").ToString().Split(";"c)

        'For Each bcc As String In Bccemailarr
        mailMessage.CC.Add(CCMAIL)
        'Next

        Using attachment As Attachment = New Attachment(FileFullpath, MediaTypeNames.Application.Octet)

            Try
                Dim disposition As Mime.ContentDisposition = attachment.ContentDisposition
                disposition.CreationDate = File.GetCreationTime(FileFullpath)
                disposition.ModificationDate = File.GetLastWriteTime(FileFullpath)
                disposition.ReadDate = File.GetLastAccessTime(FileFullpath)
                disposition.FileName = Path.GetFileName(FileFullpath)
                disposition.Size = New FileInfo(FileFullpath).Length
                disposition.DispositionType = DispositionTypeNames.Attachment
                mailMessage.Attachments.Add(attachment)
            Catch ex As Exception
            End Try

            If mailMessage.[To].Count() > 0 Then smtp.Send(mailMessage)
        End Using
    End Sub



End Class
