<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditBuilding.ascx.vb" Inherits="Controls_EditBuilding" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Edit Property
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Edit Building</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
               <table width="80%" cellpadding="3" cellspacing="0" align="center" border="0">
                    <tr>
                        <td align="Center" style="height: 30px" width="20%">
                            Enter Code to find Building
                        </td>
                        <td align="left" style="height: 30px" width="30%">
                            <asp:TextBox ID="txtfindcode" runat="server" CssClass="textbox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rffindcode" runat="server" ControlToValidate="txtfindcode"
                                ErrorMessage="Please enter Building Code !" ValidationGroup="Val2" SetFocusOnError="True"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 30px" width="10%" valign="top">
                            <asp:Button ID="btnfincode" runat="server" CssClass="button" Text="GO" ValidationGroup="Val2" CausesValidation="False" />
                        </td>
                        <td align="LEFT" style="height: 30px" width="30%">
                            <asp:LinkButton ID="lbtn1" runat="Server" Text="Get All Records"></asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <table id="table1" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvBuildingList" runat="server" EmptyDataText="There are no data records to display."
                                Width="100%" AutoGenerateColumns="false" AllowPaging="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" runat="Server" Text='<%# Eval("BDG_ID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Building Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcode" runat="server" Text='<%# Eval("BDG_ADM_CODE") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Building Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%# Eval("BDG_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBDGSTAID" Text="" runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <a href='frmModifyBuilding.aspx?ID=<%# Eval("BDG_ID") %>'>EDIT</a>
                                            <%--<a href="#" title="Edit" onclick="showPopWin('<%=Page.ResolveUrl("~/WorkSpace/SMS_Webfiles/frmEditBuilding.aspx")%>?_id=<%# Eval("BDG_ID") %>',850,338,null">
                                                        <img src="<%=Page.ResolveUrl("~/images/edit.gif")%>" alt="Edit" />
                                                    </a>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:ButtonField Text="DELETE" CommandName="Delete" />
                                    <%--<asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgDelete" ImageUrl="~/images/delete.gif" CommandArgument='<%# Eval("BDG_ID") %>'
                                                CommandName="Delete" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
        &nbsp;</td>
    </tr>
    <tr>
        <td style="width: 10px; height: 17px;">
            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
        <td style="height: 17px; width: 17px;">
            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
    </tr>
    </table>
</div>
