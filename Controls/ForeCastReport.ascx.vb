Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ForeCastReport
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocation()
            tab1.Visible = False
            txtFdate.Text = getoffsetdate(Date.Today)
            txtFdate.Attributes.Add("onClick", "displayDatePicker('" + txtFdate.ClientID + "')")
            txtFdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtToDate.Text = getoffsetdatetime(DateTime.Now).AddMonths(11).Date()
            txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
            txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        End If
    End Sub
    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LOCATION_GETBYHR")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            tab1.Visible = True
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_FORECASTED_NO")
            sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FROMDATE", txtFdate.Text, DbType.Date)
            sp.Command.AddParameter("@TODATE", txtToDate.Text, DbType.Date)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        tab1.Visible = False
        ddlLocation.SelectedIndex = 0
        txtFdate.Text = getoffsetdate(Date.Today)
        txtToDate.Text = getoffsetdatetime(DateTime.Now).AddMonths(11).Date()
    End Sub
End Class
