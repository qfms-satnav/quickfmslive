<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetUtilizationReport.ascx.vb"
    Inherits="Controls_ViewLeaseAgreements" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Asset Utilization Report
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Asset Utilization Report</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
            
            <table width="100%" cellpadding="2">
                   <tr>
                   
                   <td align="right">
                   <asp:Button ID="btnexcel" runat="Server" Text="Export to Excel" CssClass="button" />
                   </td>
                   </tr>
                    <tr>
                        <td>
                         <cc1:ExportPanel ID="exportpanel1" runat="server">
                            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false" EmptyDataText="No Asset(s) Found." Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="PRODUCTNAME" HeaderText="Product Name" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="STAT" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="cnt" HeaderText="Count" ItemStyle-HorizontalAlign="left" />
                                    
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmAssetRequisitionDetails.aspx?RID={0}") %>' Text="View Details"></asp:HyperLink>
                                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                            </cc1:ExportPanel> 
                        </td>
                    </tr>
                    <tr>
                   
                   <td align="right">
                   <asp:Button ID="btnexport1" runat="Server" Text="Export to Excel" CssClass="button" />
                   </td>
                   </tr>
                    <tr>
                    
                    
                    <td>
                     <cc1:ExportPanel ID="exportpanel2" runat="server">
                     <asp:GridView ID="gvitems1" runat="server" AllowPaging="true" AutoGenerateColumns="false" EmptyDataText="No Asset(s) Found." Width="100%">
                                <Columns>
                    <asp:BoundField DataField="TOTAL" HeaderText="Total" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="ALLOCATED" HeaderText="Allocated" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="AVAILABLE" HeaderText="Available" ItemStyle-HorizontalAlign="left" />        
                     </Columns>
                            </asp:GridView>
                            </cc1:ExportPanel> 
                    </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnexcel" />
        <asp:PostBackTrigger ControlID="btnexport1" />
    </Triggers>
</asp:UpdatePanel>

