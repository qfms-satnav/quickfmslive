<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Holiday.ascx.vb" Inherits="Controls_Holiday" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Document  (Only Excel )<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                    ControlToValidate="fpBrowseDoc" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div class="btn btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5  control-label">
                    <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" />
                </div>
                <div class="col-md-7">
                    <asp:HyperLink ID="hyp" runat="server" Text=" Click here to view the template" NavigateUrl="~/Masters/Mas_Webfiles/HolidayMaster.xls"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>
</div>
<div>&nbsp</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblLocation" runat="server" CssClass="col-md-5 control-label">Select Location<span style="color: red;">*</span></asp:Label>

                <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlLocation"
                    Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="--Select Location--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" AutoPostBack="true" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Location">
                        <%-- <asp:ListItem>--Select Status--</asp:ListItem>
                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>--%>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Holiday Date<span style="color: red;">*</span></label>
                <%-- <asp:Label ID="lblHYDate" runat="server" CssClass="col-md-5 control-label">Holiday Date<span style="color: red;">*</span></asp:Label>--%>

                <asp:Label ID="lblTemp" runat="server" Text="" Visible="false"></asp:Label>

                <asp:RequiredFieldValidator ID="rfvHYDate" runat="server" ControlToValidate="txtHYDate"
                    ErrorMessage="Please Enter Holiday Date" ValidationGroup="Val1" SetFocusOnError="True"
                    Display="None"></asp:RequiredFieldValidator>
                <div class="col-md-7">

                    <div class='input-group date' id='todate'>
                        <asp:TextBox ID="txtHYDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblHYRsn" runat="server" CssClass="col-md-5 control-label">Holiday Reason<span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="rfvHYRsn" runat="server" ControlToValidate="txtHYRsn" Display="None"
                    ErrorMessage="Please Enter Holiday Reason" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtHYRsn" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblRemarks" runat="server" CssClass="col-md-5 control-label">Remarks<span style="color: red;">*</span></asp:Label>

                <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks" Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <div class="col-md-7">

                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" MaxLength="500"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblStatus" runat="server" CssClass="col-md-5 control-label">Select Status<span style="color: red;">*</span></asp:Label>
                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                    Display="None" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                        ToolTip="Select Status">
                        <asp:ListItem>--Select Status--</asp:ListItem>
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">InActive</asp:ListItem>
                    </asp:DropDownList>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
            <asp:Button ID="btnModify" runat="Server" Text="Modify" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/Workspace/SMS_WEBfiles/helpmasters.aspx" CausesValidation="False" />

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="gvHoliday" TabIndex="9" runat="server" AutoGenerateColumns="False"
            AllowPaging="True" EmptyDataText="No Holidays Found." CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                <asp:TemplateField HeaderText="ID" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SHY_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Location Name">
                    <ItemTemplate>
                        <asp:Label ID="lblLoc" runat="Server" Text='<%#Eval("LOCNAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Holiday Date">
                    <ItemTemplate>
                        <asp:Label ID="lbllist" runat="Server" Text='<%#Eval("SHY_DATE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Holiday Reason">
                    <ItemTemplate>
                        <asp:Label ID="lbllist122" runat="Server" Text='<%#Eval("SHY_REASON") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="Server" Text='<%#Eval("SHY_STA_ID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField Text="EDIT" CommandName="EDIT" />
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>

