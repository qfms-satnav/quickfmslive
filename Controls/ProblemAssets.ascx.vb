Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class Controls_ClaimAssets
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            If gvAssetDetails.Rows.Count > 0 Then
                tr1.Visible = True
                btnsubmit.Visible = True
            Else
                tr1.Visible = False
                btnsubmit.Visible = False
            End If

        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_FAM_SP_GETEMPLOYEE_ASSETS")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvAssetDetails.DataSource = sp.GetDataSet()
            gvAssetDetails.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvAssetDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAssetDetails.PageIndexChanging
        gvAssetDetails.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            Dim flag As Boolean
            flag = False
            Dim Req_Id As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("UID")
            txtstore.text = Req_Id
            For i As Integer = 0 To gvAssetDetails.Rows.Count - 1
                Dim chkbxasset As CheckBox = DirectCast(gvAssetDetails.Rows(i).FindControl("chkbxasset"), CheckBox)
                If chkbxasset.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_FAM_SP_REQUEST_RAISED")
                    sp.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
                    sp.Command.AddParameter("@AAT_CODE", CType(gvAssetDetails.Rows(i).FindControl("lblastcode"), Label).Text, DbType.String)
                    sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                    sp.Command.AddParameter("@REMARKS", txtrmrks.Text, DbType.String)
                    sp.ExecuteScalar()
                    txtrmrks.Text = ""
                End If
                lblMsg.Text = "Request Raised Succesfully"
            Next
            If flag = False Then
                lblMsg.Text = "Please Select any of the Asset to Raise Request"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
  
End Class
