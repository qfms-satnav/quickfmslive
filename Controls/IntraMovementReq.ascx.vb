Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_IntraMovementReq
    Inherits System.Web.UI.UserControl

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            tab1.Visible = False
            tabIntra.Visible = False
            tabdetails.visible = False
            BindLocations()
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ASSET_GET_INTRAREQUISITIONS")
            'sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvgriditems.DataSource = sp.GetDataSet()
            gvgriditems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub BindEmp()
          
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        ObjSubsonic.Binddropdown(ddlEmp, "GET_ALL_AMANTRA_USER", "AUR_FIRST_NAME", "AUR_ID", param)
        ' ddlEmp.Enabled = False


    End Sub
    Private Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        Dim dr As SqlDataReader = sp.GetReader
        ddlSLoc.DataSource = dr
        ddlSLoc.DataTextField = "LCM_NAME"
        ddlSLoc.DataValueField = "LCM_CODE"
        ddlSLoc.DataBind()
        ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))

        For Each li As ListItem In ddlSLoc.Items
            ddlDLoc.Items.Add(li)
        Next
    End Sub

    Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlSTower)
        End If
    End Sub
    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Tower_GetByLocation")
        sp.Command.AddParameter("@LocId", Trim(LocCode), DbType.String)
        ddl.DataSource = sp.GetReader
        ddl.DataTextField = "TWR_NAME"
        ddl.DataValueField = "TWR_Code"
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
        If ddlSTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlSTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlSFloor)
        End If
    End Sub
    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Floor_GetByTower")
        sp.Command.AddParameter("@TwrCode", Trim(TwrCode), DbType.String)
        ddl.DataSource = sp.GetReader
        ddl.DataTextField = "FLR_NAME"
        ddl.DataValueField = "FLR_Code"
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub btnGetAssets_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetAssets.Click
        Session("Flag") = 1
        BindAssetGrid()
    End Sub


    Private Sub BindAssetGrid()
        Dim Location As String = ""
        Dim Tower As String = ""
        Dim Floor As String = ""

        If ddlSLoc.SelectedIndex > 0 Then
            Location = ddlSLoc.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a location."
            Exit Sub
        End If

        If ddlSTower.SelectedIndex > 0 Then
            Tower = ddlSTower.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a tower."
            Exit Sub
        End If

        If ddlSFloor.SelectedIndex > 0 Then
            Floor = ddlSFloor.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a floor."
            Exit Sub
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASTS1")
        sp.Command.AddParameter("@loc", Location, DbType.String)
        sp.Command.AddParameter("@tow", Tower, DbType.String)
        sp.Command.AddParameter("@flr", Floor, DbType.String)
        sp.Command.AddParameter("@ASSET_CODE", txtstore1.Text, DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_ASTS")
        'sp.Command.AddParameter("@loc", Location, DbType.String)
        'sp.Command.AddParameter("@tow", Tower, DbType.String)
        'sp.Command.AddParameter("@flr", Floor, DbType.String)
        'gvItems.DataSource = sp.GetDataSet
        'gvItems.DataBind()
    End Sub
    Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlDTower)
        End If
    End Sub

    Protected Sub ddlDTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTower.SelectedIndexChanged
        If ddlDTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlDTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlDFloor)
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim Location As String = ""
        Dim Tower As String = ""
        Dim Floor As String = ""
        Dim DLocation As String = ""
        Dim DTower As String = ""
        Dim DFloor As String = ""

        If ddlSLoc.SelectedIndex > 0 Then
            Location = ddlSLoc.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a location."
            Exit Sub
        End If

        If ddlSTower.SelectedIndex > 0 Then
            Tower = ddlSTower.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a tower."
            Exit Sub
        End If

        If ddlSFloor.SelectedIndex > 0 Then
            Floor = ddlSFloor.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a floor."
            Exit Sub
        End If

        If ddlDLoc.SelectedIndex > 0 Then
            DLocation = ddlDLoc.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a location."
            Exit Sub
        End If

        If ddlDTower.SelectedIndex > 0 Then
            DTower = ddlDTower.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a tower."
            Exit Sub
        End If

        If ddlDFloor.SelectedIndex > 0 Then
            DFloor = ddlDFloor.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a floor."
            Exit Sub
        End If

        Dim chkCount As Integer = 0

        For Each gvRow As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                chkCount += 1
            End If
        Next
        If chkCount = 0 Then
            lblMsg.Text = "Please select at least one asset."
            Exit Sub
        End If


        '-------------- Source Building and Tower should not be equal to  Destination Building and Tower -----------------

        If ddlSLoc.SelectedItem.Value = ddlDLoc.SelectedItem.Value And ddlSTower.SelectedItem.Value = ddlDTower.SelectedItem.Value And ddlSFloor.SelectedItem.Value = ddlDFloor.SelectedItem.Value Then
            lblMsg.Text = "Source Building, Tower and Floor should not be equal to Destination Building, Tower and Floor."
            Exit Sub
        End If

        '-----------------------------------------------------------------------------------------------------------------
        Dim strASSET_LIST As New ArrayList
        Dim PersonName As String = ddlEmp.SelectedItem.Value
        Dim Remarks As String = Trim(txtRemarks.Text)
        Dim ReqId As String = ""
        Dim count As Integer = 0
        Dim Message As String = String.Empty
        Dim TotalSelectedProduct As New ArrayList
        Dim cnt As Integer = 0
        ReqId = GenerateRequestId(Tower, Floor, DTower, DFloor, "")
        For i As Integer = 0 To gvItems.Rows.Count - 1

            Dim lblAAS_AAT_CODE As Label = DirectCast(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim chbx As CheckBox = DirectCast(gvItems.Rows(i).FindControl("chkSelect"), CheckBox)
            If chbx.Checked = True Then
                cnt = 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_MVMT_REQ_Insert")
                sp.Command.AddParameter("@MMR_REQ_ID", ReqId, DbType.String)
                sp.Command.AddParameter("@MMR_AST_CODE", lblAAS_AAT_CODE.Text, DbType.String)
                sp.Command.AddParameter("@MMR_FROMBDG_ID", Tower, DbType.String)
                sp.Command.AddParameter("@MMR_FROMFLR_ID", Floor, DbType.String)
                sp.Command.AddParameter("@MMR_TOBDG_ID", DTower, DbType.String)
                sp.Command.AddParameter("@MMR_TOFLR_ID", DFloor, DbType.String)
                sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
                sp.Command.AddParameter("@MMR_RECVD_BY", PersonName, DbType.String)
                sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
                sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
                sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
                sp.Command.AddParameter("@MMR_COMMENTS", Remarks, DbType.String)
                sp.Command.AddParameter("@MMR_ITEM_REQUISITION", txtstore.Text, DbType.String)
                sp.ExecuteScalar()
                strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text & "," & ddlEmp.SelectedItem.Text)
            End If

        Next
        


        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_employee"))
        'getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, False)


        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_it_approval_reject"))
        'getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, True)




        ''For i As Integer = 0 To gvItems.PageCount - 1
        ''    If Session("pageIntra" + CStr(i)) IsNot Nothing Then
        ''        TotalSelectedProduct.Add(Session("pageIntra" & i))
        ''    End If
        ''Next

        ''Dim cnt As Integer = 0
        ''ReqId = GenerateRequestId(Tower, Floor, DTower, DFloor, "")
        ''For i As Integer = 0 To TotalSelectedProduct.Count - 1
        ''    Dim strProduct As String()
        ''    strProduct = TotalSelectedProduct.Item(i)
        ''    For j As Integer = 0 To strProduct.Length - 1
        ''        Dim a1
        ''        If strProduct(j) Is Nothing Then
        ''        Else
        ''            If strProduct(j) = "" Then
        ''            Else
        ''                a1 = strProduct(j).Split(",")
        ''                If a1(2) = True Then
        ''                    Dim strAAS_AAT_CODE As String
        ''                    Dim strAAT_NAME As String

        ''                    strAAS_AAT_CODE = a1(0)
        ''                    strAAT_NAME = a1(1)
        ''                    cnt = 1
        ''                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_MVMT_REQ_Insert")
        ''                    sp.Command.AddParameter("@MMR_REQ_ID", ReqId, DbType.String)
        ''                    sp.Command.AddParameter("@MMR_AST_CODE", strAAS_AAT_CODE, DbType.String)
        ''                    sp.Command.AddParameter("@MMR_FROMBDG_ID", Tower, DbType.String)
        ''                    sp.Command.AddParameter("@MMR_FROMFLR_ID", Floor, DbType.String)
        ''                    sp.Command.AddParameter("@MMR_TOBDG_ID", DTower, DbType.String)
        ''                    sp.Command.AddParameter("@MMR_TOFLR_ID", DFloor, DbType.String)
        ''                    sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
        ''                    sp.Command.AddParameter("@MMR_RECVD_BY", PersonName, DbType.String)
        ''                    sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
        ''                    sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
        ''                    sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
        ''                    sp.Command.AddParameter("@MMR_COMMENTS", Remarks, DbType.String)
        ''                    sp.ExecuteScalar()
        ''                End If


        ''            End If
        ''        End If


        ''    Next
        ''Next

        If cnt = 1 Then
            Response.Redirect("frmAssetThanks.aspx?RID=intramovement")
        End If
        'For Each gvRow As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
        '    Dim lblAAS_AAT_CODE As Label = DirectCast(gvRow.FindControl("lblAAS_AAT_CODE"), Label)
        '    Dim lblAAT_NAME As Label = DirectCast(gvRow.FindControl("lblAAT_NAME"), Label)

        '    If chkSelect.Checked Then

        '        ReqId = GenerateRequestId(Tower, Floor, DTower, DFloor, lblAAS_AAT_CODE.Text)

        '        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_MVMT_REQ_Insert")
        '        'sp.Command.AddParameter("@MMR_REQ_ID", ReqId, DbType.String)
        '        'sp.Command.AddParameter("@MMR_AST_CODE", lblAAS_AAT_CODE.Text, DbType.String)
        '        'sp.Command.AddParameter("@MMR_FROMBDG_ID", Tower, DbType.String)
        '        'sp.Command.AddParameter("@MMR_FROMFLR_ID", Floor, DbType.String)
        '        'sp.Command.AddParameter("@MMR_TOBDG_ID", DTower, DbType.String)
        '        'sp.Command.AddParameter("@MMR_TOFLR_ID", DFloor, DbType.String)
        '        'sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
        '        'sp.Command.AddParameter("@MMR_RECVD_BY", PersonName, DbType.String)
        '        'sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
        '        'sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
        '        'sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
        '        'sp.Command.AddParameter("@MMR_COMMENTS", Remarks, DbType.String)
        '        'sp.ExecuteScalar()

        '    End If
        'Next

    End Sub



    Private Sub getRequestDetails(ByVal ReqId As String, ByVal strAst As ArrayList, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        'Dim ReqId As String = GetRequestId()
        'Dim strASSET_LIST As New ArrayList
        'Dim i As Integer = 0
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        '    If chkSelect.Checked Then
        '        strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
        '    End If
        '    i += 1
        'Next
        SendMail(ReqId, strAst, Session("uid"), txtRemarks.Text, MailStatus, App_Rej_status)
    End Sub


    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = ObjSubsonic.GetSubSonicDataSet("GET_ASSET_INTRA_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty


            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = ObjSubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If

            'strAstList = "<table width='200' border='1'><tr><td><strong>Asset Name </strong></td><td><strong>Quantity</strong></td></tr>"
            'For i As Integer = 0 To AstList.Count - 1
            '    Dim p1
            '    p1 = AstList.Item(i).Split(",")

            '    strAstList = strAstList & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            'Next
            'strAstList = strAstList & "</table> "


            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(3) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(5) & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&rl=rm&ty=imr> Click to Approve </a></td></tr></table>"

            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&rl=rm&ty=imr>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)



            '1. IT Request Approved copy goes to request raised person.
            '2. One copy for IT ADMIN to approve.

            If App_Rej_status = False Then
                Insert_AmtMail(body, strRR, strSubject, strRM)
            Else
                Insert_AmtMail(body, strRM, strSubject, "")
            End If






        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "IntraMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub




    Private Function GenerateRequestId(ByVal TowerId As String, ByVal FloorId As String, ByVal DTowerId As String, ByVal DFloorId As String, ByVal AssetCode As String) As String
        Dim ReqId As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_MVMT_REQ_GetMaxMMR_ID")
        Dim SNO As String = CStr(sp.ExecuteScalar())
        ' ReqId = "MMR/" + TowerId + "/" + FloorId + "/" + AssetCode + "/" + DTowerId + "/" + DFloorId + "/" + SNO
        ReqId = "MMR/" + TowerId + "/" + FloorId + "/" + DTowerId + "/" + DFloorId + "/" + SNO
        Return ReqId
    End Function
    'Private Function GetTowerCode(ByVal TowerId As Integer) As String
    '    Dim TowerCode As String = ""
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Tower_GetById")
    '    sp.Command.AddParameter("@TWR_ID", TowerId, DbType.Int32)
    '    Dim dr As SqlDataReader = sp.GetReader
    '    If dr.Read Then
    '        TowerCode = dr("TWR_CODE")
    '    End If
    '    Return TowerCode
    'End Function
    'Private Function GetFloorCode(ByVal FloorId As Integer) As String
    '    Dim FloorCode As String = ""
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Floor_GetById")
    '    sp.Command.AddParameter("@FLR_ID", FloorId, DbType.Int32)
    '    Dim dr As SqlDataReader = sp.GetReader
    '    If dr.Read Then
    '        FloorCode = dr("FLR_CODE")
    '    End If
    '    Return FloorCode
    'End Function

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        Dim d As Integer = gvItems.PageCount
        Dim texts As String() = New String(gvItems.PageSize - 1) {}

        Dim count As Integer = 0
        For Each row As GridViewRow In gvItems.Rows

            Dim lblAAS_AAT_CODE As Label = DirectCast(row.FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = DirectCast(row.FindControl("lblAAT_NAME"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)

            If chkSelect.Checked = True Then
                texts(count) = lblAAS_AAT_CODE.Text & "," & lblAAT_NAME.Text & "," & chkSelect.Checked
            Else
                texts(count) = ""
            End If
            count += 1
        Next

        Session("pageIntra" + gvItems.PageIndex.ToString) = texts
        gvItems.PageIndex = e.NewPageIndex
        BindAssetGrid()
    End Sub

    Protected Sub gvItems_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItems.PreRender
        If Session("Flag") = 1 Then
            For i As Integer = 0 To gvItems.PageCount - 1
                Session("pageIntra" + i.ToString) = Nothing
            Next
            Session("Flag") = 0
        Else

            If Session("pageIntra" + gvItems.PageIndex.ToString) IsNot Nothing Then
                Dim texts As String() = DirectCast(Session("pageIntra" + CStr(gvItems.PageIndex)), String())
                For i As Integer = 0 To gvItems.Rows.Count - 1
                    Dim lblAAS_AAT_CODE As Label = DirectCast(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
                    Dim lblAAT_NAME As Label = DirectCast(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
                    Dim chkSelect As CheckBox = DirectCast(gvItems.Rows(i).FindControl("chkSelect"), CheckBox)

                    If texts(i).ToString = "" Then
                    Else
                        Dim prdValue As String() = texts(i).Split(",")
                        chkSelect.Checked = prdValue(2)
                    End If


                    'If textBox.Text <> "" Then
                    '    chkSelect.Checked = True
                    'Else
                    '    chkSelect.Checked = False
                    'End If
                Next
            End If
        End If

    End Sub

    Protected Sub gvgriditems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvgriditems.PageIndexChanging
        gvgriditems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvgriditems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvgriditems.RowCommand
        If e.CommandName = "ViewDetails" Then
            tab1.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblasset As Label = DirectCast(gvgriditems.Rows(rowIndex).FindControl("lblasset"), Label)
            txtstore.Text = lblasset.Text
            BindGrid1()
            tabIntra.Visible = False
            tabdetails.Visible = True
            BindCategories()

            BindRequisition()
            'BindEmp()
            'BindFromDetails()
            'BindToDetails()
            'Session("Flag") = 1
            'BindAssetGrid()
        End If
    End Sub
    Private Sub BindRequisition()
        Dim ReqId As String = txtstore.Text
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", txtstore.Text, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                ddlEmp1.Items.Insert(0, New ListItem(CStr(RaisedBy), "0"))


                Dim CatId As Integer = 0
                Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)

                Dim li As ListItem
                li = ddlAstCat.Items.FindByValue(CStr(CatId))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = False

                txtRemarks1.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")

                txtRemarks1.ReadOnly = True
                txtRMRemarks.ReadOnly = True
                txtAdminRemarks.ReadOnly = True
                txtStatus.ReadOnly = True

                If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                    trRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    trRMRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    trAdminRemarks.Visible = False
                End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                '    If StatusId = 1001 Or StatusId = 1002 Then
                '        btnSubmit.Enabled = True
                '        btnCancel.Enabled = True
                '    Else
                '        btnSubmit.Enabled = False
                '        btnCancel.Enabled = False
                '    End If
                'Else
                '    btnSubmit.Enabled = False
                '    btnCancel.Enabled = False
                'End If


            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub
    Private Sub BindCategories()
        GetChildRows("0")
        'ddlAstCat.DataSource = CategoryController.CategoryList
        'ddlAstCat.DataTextField = "CategoryName"
        'ddlAstCat.DataValueField = "CategoryID"
        'ddlAstCat.DataBind()
        'For Each li As ListItem In ddlAstCat.Items

        'Next
        ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub GetChildRows(ByVal i As String)
        Dim str As String = ""
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If


        Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
        Dim ds As New DataSet
        da.Fill(ds)



        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim j As Integer = CType(dr("CategoryId"), Integer)
                If id = 0 Then
                    str = ""
                End If
                Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
                ddlAstCat.Items.Add(li)
                GetChildRows(str & "~" & j)
            Next
        End If
    End Sub
    Private Sub BindGrid1()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ASSET_GET_ASSETS_INTRA_ITEMCODE")
            sp.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
            gvassets.DataSource = sp.GetDataSet()
            gvassets.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindFromDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_BIND_INTRA_FROMDETAILS")
            sp.Command.AddParameter("@REQ", txtstore1.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSLoc.ClearSelection()
                ddlSLoc.Items.FindByValue(ds.Tables(0).Rows(0).Item("Location")).Selected = True
                ddlSLoc.Enabled = False
                ddlSTower.ClearSelection()
                BindTowersByLocation(ddlSLoc.SelectedItem.Value, ddlSTower)
                ddlSTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("Tower")).Selected = True
                ddlSTower.Enabled = False
                ddlSFloor.ClearSelection()
                BindFloorsByTower(ddlSTower.SelectedItem.Value, ddlSFloor)
                ddlSFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("Floor")).Selected = True
                ddlSFloor.Enabled = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub
    Private Sub BindToDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_BIND_INTRA_TODETAILS")
            sp.Command.AddParameter("@REQ_ID", txtstore.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlDLoc.ClearSelection()
                ddlDLoc.Items.FindByValue(ds.Tables(0).Rows(0).Item("Location")).Selected = True
                ddlDTower.ClearSelection()
                BindTowersByLocation(ddlDLoc.SelectedItem.Value, ddlDTower)
                ddlDTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("Tower")).Selected = True
                ddlDFloor.ClearSelection()
                BindFloorsByTower(ddlDTower.SelectedItem.Value, ddlDFloor)
                ddlDFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("Floor")).Selected = True
                ddlDLoc.Enabled = False
                ddlDTower.Enabled = False
                ddlDFloor.Enabled = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvassets_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvassets.PageIndexChanging
        gvassets.PageIndex = e.NewPageIndex()
        BindGrid1()
    End Sub

    Protected Sub gvassets_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvassets.RowCommand
        If e.CommandName = "ViewDetails" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lbtnaas_aat_code As Label = DirectCast(gvassets.Rows(rowIndex).FindControl("lbtnaas_aat_code"), Label)
            txtstore1.Text = lbtnaas_aat_code.Text
            tabIntra.Visible = True
            BindEmp()
            BindFromDetails()
            BindToDetails()
            BindAssetGrid()
        End If
    End Sub
End Class
