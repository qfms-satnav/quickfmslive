﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptPrintSchedule.ascx.vb" Inherits="Controls_AptPrintSchedule" %>
<asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
<div class="row">
  <div class="col-md-12">
        <asp:GridView ID="gvitems1" runat="server" EmptyDataText="Sorry! No Available Records..."
            RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
            AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Request Id">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="Server" Text='<%#Eval("REQUESTID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested date">
                    <ItemTemplate>
                        <asp:Label ID="lblreqdate" runat="server" Text='<%#Eval("REQUESTED_ON")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Request Name">
                    <ItemTemplate>
                        <asp:Label ID="lblreqname" runat="server" Text='<%#Eval("REQUESTNAME")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STA_TITLE")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField Text="View Details" CommandName="View" />
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>


