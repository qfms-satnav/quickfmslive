Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_TrvlMasters
    Inherits System.Web.UI.UserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindCategory()
            BindGrid()
            btnModify.Visible = False
            btnSubmit.Visible = True
        End If
    End Sub
    Private Sub BindCategory()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CATEGORY_GET")
            ddlCategory.DataSource = sp.GetDataSet()
            ddlCategory.DataTextField = "CATEGORY"
            ddlCategory.DataValueField = "SNO"
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim validateDocType As Integer
            validateDocType = ValidateDoc()
            If validateDocType = 0 Then
                lblMsg.Text = "Document Type Already Exists Please Enter Another Document Type"
            Else
                AddDocType()
                Cleardata()
                BindGrid()
                lblMsg.Text = "Document Type Added Successfully"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateDoc()
        Dim validateDocType As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_VALIEDATE_ADD")
        sp.Command.AddParameter("@CAT", ddlCategory.SelectedItem.Value, DbType.Int32)
        sp.Command.AddParameter("@DOC_TYPE", txtDocType.Text, DbType.String)
        validateDocType = sp.ExecuteScalar()
        Return validateDocType

    End Function
    Private Sub AddDocType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_ADD")
        sp.Command.AddParameter("@DOC_TYPE", txtDocType.Text, DbType.String)
        sp.Command.AddParameter("@STAT", ddlStatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAT", ddlCategory.SelectedItem.Value, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "EDIT" Then
            btnSubmit.Visible = False
            lblMsg.Text = ""
            btnModify.Visible = True
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim lblDoc As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblDoc"), Label)

            Dim id As Integer = Integer.Parse(lblID.Text)
            dispdata(id)
            txtstore.Text = lblDoc.Text
        ElseIf e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As Integer = Integer.Parse(lblID.Text)
            DeleteDocType(id)
        End If
    End Sub
    Private Sub dispdata(ByVal id As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_GETDETAILS")
            sp.Command.AddParameter("@ID", id, DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCategory.ClearSelection()
                ddlCategory.Items.FindByText(ds.Tables(0).Rows(0).Item("CATEGORY")).Selected = True
                txtDocType.Text = ds.Tables(0).Rows(0).Item("DOC_TYPE")
                Dim STAT As Integer = ds.Tables(0).Rows(0).Item("STAT")
                If STAT = 0 Then
                    ddlStatus.SelectedValue = 0
                Else
                    ddlStatus.SelectedValue = 1
                End If
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()

        Try
            Dim id As Integer = 0
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_GETDETAILS")
            sp.Command.AddParameter("@ID", ID, DbType.Int32)
            Dim ds As New DataSet()
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim lblstat As Label = CType(gvItems.Rows(i).FindControl("lblstat"), Label)
                If lblstat.Text = 1 Then
                    lblstat.Text = "Active"
                Else
                    lblstat.Text = "Inactive"
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub
    Private Sub DeleteDocType(ByVal id As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_DEL")
            sp.Command.AddParameter("@ID", id, DbType.Int32)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        Try
           
            Dim validate As Integer
            Dim sno As Integer
            sno = Getsno(txtDocType.Text)
            validate = VALIDATEDOCMODIFY(sno)
            If validate = 0 Then
                lblMsg.Text = "Document Type Already Exists Please Enter Another Document Type"
            End If
            Dim sno1 As Integer
            sno1 = Getsno(txtStore.Text)
            ModifyDocType(sno1)
            Cleardata()
            BindGrid()
            lblMsg.Text = "Document Type Details Modified Successfully"
            btnModify.Visible = False
            btnSubmit.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function VALIDATEDOCMODIFY(ByVal sno As Integer)
        Dim validate As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOCTYPE_VALIDATE_MODIFY")
        sp.Command.AddParameter("@CAT", ddlCategory.SelectedItem.Value, DbType.Int32)
        sp.Command.AddParameter("@SNO", sno, DbType.Int32)
        sp.Command.AddParameter("@DOC_TYPE", txtDocType.Text, DbType.String)
        validate = sp.ExecuteScalar()
        Return validate
    End Function
    Private Function Getsno(ByVal doc As String)
        Dim sno As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_GET_SNO_DOCTYPE")
        sp.Command.AddParameter("@DOCTYPE", doc, DbType.String)
        sno = sp.ExecuteScalar()
        Return sno
    End Function
    Private Sub Cleardata()
        ddlCategory.SelectedIndex = 0
        txtDocType.Text = ""
        ddlStatus.SelectedIndex = 0
    End Sub
    Private Sub ModifyDocType(ByVal sno As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_MODIFY")
        sp.Command.AddParameter("@SNO", sno, DbType.Int32)
        sp.Command.AddParameter("@DOC_TYPE", txtDocType.Text, DbType.String)
        sp.Command.AddParameter("@STAT", ddlStatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAT", ddlCategory.SelectedItem.Value, DbType.Int32)
        sp.ExecuteScalar()
        Cleardata()
    End Sub

    Protected Sub gvItems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvItems.RowEditing

    End Sub

    Protected Sub gvItems_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItems.RowDeleting

    End Sub
End Class
