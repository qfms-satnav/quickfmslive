<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetLocation_Details.ascx.vb"
    Inherits="Controls_AssetLocation_Details" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black"> Asset Location
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong> Asset Location</strong></td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="center" valign="top" height="100%">
                        <table width="100%" cellpadding="1" runat="Server" id="tr1" border="1">
                            <tr>
                                <td align="left" width="50%">
                                    Asset Category:
                                    <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                                        Display="Dynamic" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                                        InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="50%">
                                    <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" Width="99%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    Asset:
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAsts"
                                        Display="Dynamic" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                                        InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="50%">
                                    <asp:DropDownList ID="ddlAsts" runat="server" AutoPostBack="True" Width="99%" OnSelectedIndexChanged="ddlAsts_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 238px" class="clslabel">
                                </td>
                                <td>
                                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" Width="136px"
                                        CssClass="clsButton" Visible="false" OnClick="btnExportToExcel_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left" valign="top">
                                    <asp:Panel ID="pnlItems" runat="server" Width="100%">
                                        Assets List
                                        <cc1:ExportPanel ID="exp_AssetList" runat="server">
                                            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                EmptyDataText="No Asset(s) Found." Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Asset Count">
                                                        <ItemTemplate>
                                                            <a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAssetLocationMapped_Unmapped_details.aspx")%>?Prd_id=<%# Eval("ProductId") %>&LCM_CODE=<%# Eval("LCM_CODE") %>',850,338,'')">
                                                                <asp:Label ID="lblAstCount" runat="server" Text='<%#Eval("ASTCOUNT") %>'></asp:Label>
                                                            </a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AssetName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("PRODUCTNAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </cc1:ExportPanel>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
