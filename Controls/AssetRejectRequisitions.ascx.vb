Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class Controls_AssetRejectRequisitions
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindAssetRejectedRequisitions()
        End If
    End Sub

    Private Sub BindAssetRejectedRequisitions()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@RM_AUR_ID", SqlDbType.Int)
        param(0).Value = Session("uid")
        ObjSubsonic.BindGridView(gvAstRejectedReq, "GET_ALLREJECTED_ASSETS_REQ", param)
        If gvAstRejectedReq.Rows.Count = 0 Then
            gvAstRejectedReq.DataSource = Nothing
            gvAstRejectedReq.DataBind()
        End If
    End Sub

    Protected Sub gvAstRejectedReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAstRejectedReq.PageIndexChanging
        gvAstRejectedReq.PageIndex = e.NewPageIndex
        BindAssetRejectedRequisitions()
    End Sub

    Protected Sub gvAstRejectedReq_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAstRejectedReq.RowCommand
        If e.CommandName = "View" Then
            Response.Redirect("frmAssetRejectRequisitions.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub
End Class
