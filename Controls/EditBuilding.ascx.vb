Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_EditBuilding
    Inherits System.Web.UI.UserControl

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvBuildingList.PageIndex = 0
        End If
        fillgrid()
    End Sub
    Public Sub fillgrid()
        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BDG_DETAILS")
        sp4.Command.AddParameter("@dummy", 1, DbType.Int32)
        gvBuildingList.DataSource = sp4.GetDataSet()
        gvBuildingList.DataBind()
        lbtn1.Visible = False
    End Sub

    Protected Sub gvBuildingList_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBuildingList.PageIndexChanged

    End Sub

    Protected Sub gvBuildingList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBuildingList.RowCommand
        Try
            If e.CommandName = "Delete" Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblid As Label = DirectCast(gvBuildingList.Rows(rowIndex).FindControl("lblid"), Label)
                Dim SP5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BDG_DEL")
                SP5.Command.AddParameter("@BDG_ID", lblid.Text, DbType.Int32)
                SP5.ExecuteScalar()
            End If
            fillgrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
        Dim sp6 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BUILDING_RECORD")
        sp6.Command.AddParameter("@BDG_ADM_CODE", txtfindcode.Text, DbType.String)
        gvBuildingList.DataSource = sp6.GetDataSet()
        gvBuildingList.DataBind()
        lbtn1.Visible = True
    End Sub

    Protected Sub gvBuildingList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBuildingList.PageIndexChanging
        gvBuildingList.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvBuildingList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvBuildingList.RowDeleting

    End Sub
End Class
