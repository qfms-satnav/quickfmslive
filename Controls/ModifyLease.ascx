<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ModifyLease.ascx.vb"
    Inherits="Controls_ModifyLease" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%--<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>
--%>
<style type="text/css">
    #UpdatePanel1,  #UpdateProgress1 { 
      border-right: gray 1px solid; border-top: gray 1px solid; 
      border-left: gray 1px solid; border-bottom: gray 1px solid;
    }
    #UpdatePanel1,  { 
      width:200px; height:200px; position: relative;
      float: left; margin-left: 10px; margin-top: 10px;
     }
     #UpdateProgress1 {
      width: 400px; background-color: #FFC080; 
      bottom: 0%; left: 0px; position: absolute;
     }
    </style>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div>
    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">
             <hr align="center" width="60%" /></asp:Label></td>
        </tr>
    </table>
    <table id="table3" cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                <strong>&nbsp;Lease Details</strong>
            </td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:TextBox ID="txtstore1" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtstore" runat="server" Visible="false"></asp:TextBox>
                 <asp:TextBox ID="txtstore2" runat="server" Visible="false"></asp:TextBox>
                 <asp:TextBox ID="txtstore3" runat="server" Visible="false"></asp:TextBox>
                 <asp:TextBox ID="txtstore4" runat="server" Visible="false"></asp:TextBox>
                 <asp:TextBox ID="txtstore5" runat="server" Visible="false"></asp:TextBox>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor=""  ValidationGroup="Val1"/>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="clsMessage"
                    ForeColor=""  ValidationGroup="Val2"/>
                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" CssClass="clsMessage"
                    ForeColor=""  ValidationGroup="Val3"/>
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="tab" runat="server" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <asp:Panel ID="panwiz1" runat="server" Width="100%" GroupingText="Lease Details">
                                <table id="TABWIZ1" cellspacing="0" cellpadding="1" width="100%" border="1">
                                    <tr id="Tr1" runat="server" visible="false">
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Select Property Type<%--<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                                            Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Property Type" InitialValue="0"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:DropDownList ID="ddlproptype" runat="server" CssClass="clsComboBox" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Select Lesse
                                            <%--<font class="clsNote">*</font>
                                                               <asp:RequiredFieldValidator ID="rfvLesseName" runat="server" ControlToValidate="ddlLesse"
                                                            Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Lesse " InitialValue="0"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:DropDownList ID="ddlLesse" runat="server" CssClass="clsComboBox" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr runat="server" visible="false" > 
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Select City <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="clsComboBox" Width="99%"
                                                AutoPostBack="True" TabIndex="1" Enabled="false">
                                            </asp:DropDownList>
                                        </td>
                                       
                                        <td align="left" style="width: 25%; height: 26px;">
                                            PinCode<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvPincode" runat="server" ControlToValidate="txtpincode"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter PinCode"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revpincode" runat="server" ControlToValidate="txtpincode"
                                                ErrorMessage="Please enter valid PinCode" Display="None"   ValidationGroup="Val1"   ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <div onmouseover="Tip('Enter Numerics only')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtpincode" runat="server" CssClass="clsTextField" TabIndex="5" Width="97%" MaxLength="10">0</asp:TextBox>
                                        </td>
                                       
                                       
                                       
                                       
                                    </tr>
                                    <tr runat="Server" visible="false">
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Property Address 2 <%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvpropaddr2" runat="server" ControlToValidate="txtBuilding"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter Property Address2"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtBuilding" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="500" TabIndex="3" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Property Address 3 <%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvpropaddr3" runat="server" ControlToValidate="txtprop3"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter Property Address3"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtprop3" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                TextMode="MultiLine" Rows="5" TabIndex="4"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                         <td align="left" style="width: 25%; height: 26px;">
                                                Select Property <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvpropaddr1" runat="server" ControlToValidate="ddlproperty"
                                                    Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Property "
                                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;">
                                                <asp:DropDownList ID="ddlproperty" runat="server" CssClass="clsComboBox" Width="97%">
                                                </asp:DropDownList>
                                                <%-- <asp:TextBox ID="txtProperty" runat="server" CssClass="clsTextField" TabIndex="2"
                                                Width="97%" MaxLength="500" TextMode="MultiLine" Rows="5"></asp:TextBox>--%>
                                            </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            CTS Number<%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvLNumber" runat="server" ControlToValidate="txtLnumber"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please enter CTS Number"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revLNumber" runat="server" ControlToValidate="txtLnumber"
                                                ErrorMessage="Please enter valid CTS Number" Display="None"   ValidationGroup="Val1"   ValidationExpression="^[A-Za-z0-9\\\/\- ]*$"></asp:RegularExpressionValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <div onmouseover="Tip('Enter Alphabets,Numbers and some special characters like /-\ with maximum length 50')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtLnumber" runat="server" CssClass="clsTextField" TabIndex="6" Width="97%" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td id="Td1" align="left" style="width: 25%; height: 26px;" visible="false" runat="server">
                                            Select Status<%--<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="cvStatus" runat="server" ControlToValidate="ddlStatus"
                                                            Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Status" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td id="Td2" align="center" style="width: 25%; height: 26px;" runat="server" visible="false">
                                            <asp:DropDownList ID="ddlStatus" runat="server" Width="99%" CssClass="clsComboBox">
                                                <%--  <asp:ListItem Value="--Select--">--Select--</asp:ListItem>--%>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">Terminated</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Entitled Lease Amount<%--<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="rfventitle" runat="server" ControlToValidate="txtentitle"
                                                            Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter Entitled Lease Amount"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtentitle" runat="server" CssClass="clsTextField" TabIndex="7" Width="97%"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Rent Amount With out Escalation(MAX
                                            <asp:Label ID="lblmaxrent" runat="server" CssClass="bodytext"></asp:Label>) <font
                                                class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter Rent Amount with out Escalation "></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtInvestedArea"
                                                ErrorMessage="Please Enter Valid Rent Amount with out Escalation" Display="None"   ValidationGroup="Val1"   ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtInvestedArea" runat="server" CssClass="clsTextField" Width="97%"
                                                    MaxLength="15" TabIndex="8" AutoPostBack="true" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Security Deposit (MAX
                                            <asp:Label ID="lblmaxsd" runat="server" CssClass="bodytext"></asp:Label>) <font class="clsNote">
                                                *</font><asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="txtpay"
                                                    Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter Security Deposit "></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revsec" runat="server" ControlToValidate="txtpay"
                                                ErrorMessage="Please Enter valid Security Deposit" Display="None"   ValidationGroup="Val1"   ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtpay" runat="server" CssClass="clsTextField" TabIndex="9" Width="97%" MaxLength="15" AutoPostBack="true" ></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Builtup Area (SQ. FT.)<%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvOccupiedArea" runat="server" ControlToValidate="txtOccupiedArea"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter Lease BuiltUp Area "></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="revbuilt" runat="server" ControlToValidate="txtOccupiedArea"
                                                ErrorMessage="Please Enter Valid Lease BuiltUp Area" Display="None"   ValidationGroup="Val1"   ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtOccupiedArea" runat="server" CssClass="clsTextField" Width="97%"
                                                    MaxLength="15" TabIndex="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr  runat="server" visible="false">
                                        <td align="left" style="width: 25%; height: 26px;" visible="false">
                                            Lease Type<%--<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvLeaseType" runat="server" ControlToValidate="ddlLeaseType"
                                                            Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Lease Type" InitialValue="0"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" visible="false">
                                            <asp:DropDownList ID="ddlLeaseType" runat="server" CssClass="clsComboBox" AutoPostBack="True"
                                                Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Lease Cost Per<%--<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvMode" runat="server" ControlToValidate="ddlMode"
                                                            Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select  Lease Cost Per" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px">
                                            <asp:DropDownList ID="ddlMode" runat="server" CssClass="clsComboBox" Width="99%">
                                                <asp:ListItem>--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Weekly</asp:ListItem>
                                                <asp:ListItem Value="2">Monthly</asp:ListItem>
                                                <asp:ListItem Value="3">Half-Yearly</asp:ListItem>
                                                <asp:ListItem Value="4">Yearly</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td id="Td3" align="left" style="width: 25%; height: 26px;" runat="server">
                                            Effective Date of Agreement <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvsdate" runat="server" ControlToValidate="txtsdate"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Effective Date of Agreement!"></asp:RequiredFieldValidator>
                                        </td>
                                        <td id="Td4" align="left" style="width: 25%; height: 26px;" runat="server">
                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtsdate" runat="server" CssClass="clsTextField" Width="97%" TabIndex="11"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtsdate"
                                                    Format="dd-MMM-yyyy">
                                                </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            Expiry Date of Agreement <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvedate" runat="server" ControlToValidate="txtedate"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Expiry Date of Agreement!"></asp:RequiredFieldValidator>
                                            <%--<asp:CompareValidator ID="cvexpagree" runat="server" Type="date"  Operator="GreaterThanEqual"  
                                                ErrorMessage="Expiry Date of Agreement Should be More than Effective Date of Agreement!"
                                                ControlToValidate="txtsdate" ControlToCompare="txtedate" Display="None"   ValidationGroup="Val1"    ></asp:CompareValidator>--%>
                                        </td>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtedate" runat="server" CssClass="clsTextField" Width="97%" TabIndex="12"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtedate"
                                                    Format="dd-MMM-yyyy">
                                                </cc1:CalendarExtender>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Agreement to be signed by POA<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvpoa" runat="server" ControlToValidate="ddlpoa"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Agreement to be signed by POA" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="center" style="width: 25%; height: 26px;">
                                            <asp:DropDownList ID="ddlpoa" runat="server" Width="99%" CssClass="clsComboBox"  TabIndex="13">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Value="No">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            No. of Landlords<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvlandlord" runat="server" ControlToValidate="ddlleaseld"
                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select No. of Landlords" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="center" style="width: 25%; height: 26px;">
                                            <asp:DropDownList ID="ddlleaseld" runat="server" Width="99%" CssClass="clsComboBox"
                                                 TabIndex="14" AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Do You wish to Enter Lease Escalation <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvesc" runat="server" ControlToValidate="ddlesc"
                                                Display="None"   ValidationGroup="Val1"   InitialValue="--Select--" ErrorMessage="Please Select Lease Escalation"></asp:RequiredFieldValidator>
                                        </td>
                                        <td id="tdlseesc1" runat="server" align="left" style="width: 25%; height: 26px;">
                                            <asp:DropDownList ID="ddlesc" runat="server" CssClass="clsComboBox" TabIndex="15" Width="99%" AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Value="No">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    
                                    <tr  runat="server" visible="false">
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            Lease Escalation Type<font class="clsNote">*</font>
                                            <%-- <asp:RequiredFieldValidator ID="rfvesctype" runat="server" ControlToValidate="ddlesctype"
                                                                    Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Select Lease Escalation Type" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td id="Td9" align="center" style="width: 25%; height: 26px;" runat="server">
                                            <asp:DropDownList ID="ddlesctype" runat="server" Width="99%" CssClass="clsComboBox"
                                                AutoPostBack="True">
                                                <%--<asp:ListItem Value="--Select--">--Select--</asp:ListItem>--%>
                                                <asp:ListItem Value="PER">Percentage</asp:ListItem>
                                                <asp:ListItem Value="FLT">Flat Amount</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2" width="100%">
                                            <asp:Panel ID="pnlesc1" runat="server" Width="100%" GroupingText="Escalation1">
                                                <table id="tbesc1" runat="Server" cellpadding="1" width="100%">
                                                    <tr>
                                                        <td  align="left" style="width: 50%; height: 26px;" runat="server">
                                                            From Date<font class="clsNote">*</font>
                                                            <asp:RequiredFieldValidator ID="rfvedcdate" runat="server" ControlToValidate="txtEscalationDate"
                                                                ErrorMessage="Please Enter Escalation Date" Display="None"   ValidationGroup="Val1"  ></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td id="Td11" align="left" style="width: 50%; height: 26px;" runat="server">
                                                            <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtEscalationDate" runat="server" TabIndex="16" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtEscalationDate"
                                                                    Format="dd-MMM-yyyy">
                                                                </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td12" align="left" style="width: 50%; height: 26px;" runat="server">
                                                            To Date<font class="clsNote">*</font>
                                                            <asp:RequiredFieldValidator ID="rfcesctodate" runat="server" ControlToValidate="txtesctodate1"
                                                                ErrorMessage="Please Enter Escalation Date" Display="None"   ValidationGroup="Val1"  ></asp:RequiredFieldValidator>
                                                            <%-- <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" Operator="GreaterThanEqual"
                                                                ErrorMessage="To Date Should be More than From Date!" Display="None"   ValidationGroup="Val1"   ControlToValidate="txtesctodate1"
                                                                ControlToCompare="txtEscalationDate" ></asp:CompareValidator>--%>
                                                        </td>
                                                        <td  align="left" style="width: 50%; height: 26px;" runat="server">
                                                            <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtesctodate1" runat="server" TabIndex="17" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtesctodate1"
                                                                    Format="dd-MMM-yyyy">
                                                                </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="width: 50%; height: 26px;">
                                                            Total Escalated Amt of Actual Rent Excluding Rent Amount<font class="clsNote">*</font>
                                                            <asp:RequiredFieldValidator ID="rfvfirstesc" runat="server" ControlToValidate="txtfirstesc"
                                                                Display="None"   ValidationGroup="Val1"   ErrorMessage="Please Enter Amount for First Escalation"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revfirstescalation" runat="server" ControlToValidate="txtfirstesc"
                                                                ErrorMessage="Please Enter valid Amount for First Escalation" Display="None"   ValidationGroup="Val1"  
                                                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td id="tdesc2" runat="server" align="left" style="width: 50%; height: 26px;">
                                                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtfirstesc" runat="server" TabIndex="18" CssClass="clsTextField" Width="97%"
                                                                    MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                        <td align="left" colspan="2" width="100%">
                                            <asp:Panel ID="pnlesc2" runat="server" Width="100%" GroupingText="Escalation2">
                                                <table id="tbesc2" runat="Server" width="100%">
                                                    <tr>
                                                        <td  align="left" style="width: 50%; height: 26px;" runat="server">
                                                            From Date<%--<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtescfromdate2"
                                                            ErrorMessage="Please Enter Escalation Date" Display="None"   ValidationGroup="Val1"  ></asp:RequiredFieldValidator>--%>
                                                            <%--  <asp:CompareValidator ID="CompareValidator3" Type="Date" runat="server" Operator="GreaterThanEqual"
                                                                ErrorMessage="Escalated From Date2 Should be More than Escalated To Date1!" Display="None"   ValidationGroup="Val1"  
                                                                ControlToValidate="txtescfromdate2" ControlToCompare="txtesctodate1"></asp:CompareValidator>--%>
                                                        </td>
                                                        <td  align="left" style="width: 50%; height: 26px;" runat="server">
                                                            <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtescfromdate2" runat="server" TabIndex="19" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtescfromdate2"
                                                                    Format="dd-MMM-yyyy">
                                                                </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td16" align="left" style="width: 50%; height: 26px;" runat="server">
                                                            To Date<%--<font class="clsNote">*</font>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtesctodate2"
                                                            ErrorMessage="Please Enter Escalation Date" Display="None"   ValidationGroup="Val1"  ></asp:RequiredFieldValidator>--%>
                                                            <%--<asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" Operator="GreaterThanEqual"
                                                                ErrorMessage="To Date Should be More than From Date!" Display="None"   ValidationGroup="Val1"   ControlToValidate="txtesctodate2"
                                                                ControlToCompare="txtescfromdate2"  ></asp:CompareValidator>--%>
                                                        </td>
                                                        <td id="Td45" align="left" style="width: 50%; height: 26px;" runat="server">
                                                            <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtesctodate2" runat="server" TabIndex="20" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="txtesctodate2"
                                                                    Format="dd-MMM-yyyy">
                                                                </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="width: 50%; height: 26px;">
                                                            Total Escalated Amt of Actual Rent Excluding Rent Amount
                                                            <asp:RegularExpressionValidator ID="revsecondesc" runat="server" ControlToValidate="txtsecondesc"
                                                                ErrorMessage="Please Enter valid Amount for Second Escalation" Display="None"   ValidationGroup="Val1"  
                                                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                        </td>
                                                        <td id="tdesc4" runat="server" align="left" style="width: 50%; height: 26px;">
                                                            <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtsecondesc" runat="server" TabIndex="21" CssClass="clsTextField" Width="97%"
                                                                    MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                     <tr>
                                    <td align="left" style="width: 25%; height: 26px;" >
                                            Total Rent payable<font class="clsNote">*</font>
                                            
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" >
                                           
                                                <asp:TextBox ID="txttotalrent" runat="server" CssClass="clsTextField" Width="52%" TabIndex="22" ReadOnly="True"></asp:TextBox>
            
            <asp:Button ID="btntotal" runat="server" CssClass="button" Width="43%" Text="Calculate" OnClick="btntotal_Click"  />
                                                    
                                        </td>
                                    
                                    
                                        <td align="left" style="width: 25%; height: 26px;" >
                                            Lease Comments<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfComments" runat="server" ControlToValidate="txtComments"
                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Comments"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" >
                                            <div onmouseover="Tip('Enter Comments with maximum 750 Characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtComments" runat="server" CssClass="clsTextField" Width="97%"
                                                    TextMode="MultiLine" Rows="5" MaxLength="1000" TabIndex="22"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btn1Next" runat="server" CssClass="button" CausesValidation="true"
                                Text="Next" ValidationGroup="Val1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Panel ID="panwiz3" runat="server" Width="100%" GroupingText="Agreement Details"
                                Visible="false">
                                <table id="tabwiz3" runat="server" cellpadding="1" cellspacing="0" width="100%">
                                    <tr runat="server">
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            Tentative Execution Date <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvagreedate" runat="server" ControlToValidate="txtAgreedate"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Agreement Execution Date"></asp:RequiredFieldValidator>
                                        </td>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox ID="txtAgreedate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="txtAgreedate"
                                                Format="dd-MMM-yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            Amount of stampDuty paid <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvagreeamt" runat="server" ControlToValidate="txtagreeamt"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter  Amount of stampDuty paid"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revagreeamt" runat="server" ControlToValidate="txtagreeamt"
                                                ErrorMessage="Please Enter valid Amount of stampDuty paid" Display="None"   ValidationGroup="Val2"   ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox ID="txtagreeamt" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="15"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="Td5" align="left" style="width: 25%; height: 26px;" runat="server">
                                            Amount paid towards Registraion<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvregamt" runat="server" ControlToValidate="txtregamt"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter  Amount paid towards Registraion"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revregamt" runat="server" ControlToValidate="txtregamt"
                                                ErrorMessage="Please Enter valid Amount paid towards Registraion" Display="None"   ValidationGroup="Val2"  
                                                ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td id="Td6" align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox ID="txtregamt" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"></asp:TextBox>
                                        </td>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            Agreement Registered<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvagreeregis" runat="server" ControlToValidate="ddlagreeres"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Select Agreement Registered!" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        </td>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:DropDownList ID="ddlagreeres" runat="server" CssClass="clsComboBox" Width="99%"
                                                AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Value="No" Selected="True">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trregagree" runat="server">
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            Date of Registration <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvAgreeregdate" runat="server" ControlToValidate="txtagreeregdate"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Select Date of Registration!"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            <div onmouseover="Tip('Please click on the Textbox to select Date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtagreeregdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender8" runat="server" TargetControlID="txtagreeregdate"
                                                    Format="dd-MMM-yyyy">
                                                </cc1:CalendarExtender>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            Sub-Regristrarís office name <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvagreesub" runat="server" ControlToValidate="txtagreesub"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Sub-Regristrarís office name!"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox ID="txtagreesub" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            Termination Notice (in days)<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvnotice" runat="server" ControlToValidate="txtnotice"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Termination Notice!"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revnotice" runat="server" ControlToValidate="txtnotice"
                                                ErrorMessage="Please Enter Termination Notice in No of Days" Display="None"   ValidationGroup="Val2"   ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            <div onmouseover="Tip('Please Enter No of days')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtnotice" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            Lock-In Period of Agreement(in months)<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvlock" runat="server" ControlToValidate="txtlock"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Lock-In Period of Agreement !"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revlock" runat="server" ControlToValidate="txtlock"
                                                ErrorMessage="Please Enter Lock-In Period of Agreement in No of Months" Display="None"   ValidationGroup="Val2"  
                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox ID="txtlock" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="panbrk" runat="server" Width="100%" GroupingText="Brokerage Details"
                               >
                                <table cellpadding="1" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Amount of Brokerage Paid<%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvBrkamount" runat="server" ControlToValidate="txtbrkamount"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Amount of Brokerage Paid"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="revbrkamount" runat="server" ControlToValidate="txtbrkamount"
                                                ErrorMessage="Please enter valid Amount of Brokerage Paid" Display="None"   ValidationGroup="Val2"   ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtbrkamount" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="12" TabIndex="24"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Broker Name <%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvBrkName" runat="server" ControlToValidate="txtbrkname"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter  BrokerName"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtbrkname" runat="server" CssClass="clsTextField" TabIndex="25" Width="97%" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Broker Address<%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvbrkaddr" runat="server" ControlToValidate="txtbrkaddr"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter  Broker Address"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtbrkaddr" runat="server" CssClass="clsTextField" Width="97%" MaxLength="1000"
                                                TextMode="MultiLine" TabIndex="26" Rows="5"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Broker Pan Number <%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvpanbrk" runat="server" ControlToValidate="txtbrkpan"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Broker Pan Number!"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="regpanbrk" runat="server" ControlToValidate="txtbrkpan"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Broker Pan number in Alphanumerics only. !"
                                                ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                                
                                                <asp:RegularExpressionValidator ID="regExTextBox1" runat="server" 
   ControlToValidate="txtbrkpan" Display="None"  ValidationGroup="Val2"
   ErrorMessage="Broker Pan card Minimum length is 10"
   ValidationExpression=".{10}.*" />
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtbrkpan" runat="server" TabIndex="27" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="Tr5" runat="server">
                                        <td id="Td24" align="left" style="width: 25%; height: 26px;" runat="server">
                                            Broker Email <%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvbrkremail" runat="server" ControlToValidate="txtbrkremail"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Broker Email!"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="revbrkremail" runat="server" ControlToValidate="txtbrkremail"
                                                ErrorMessage="Please Enter valid Email" Display="None"   ValidationGroup="Val2"   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        </td>
                                        <td id="Td25" align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox ID="txtbrkremail" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="50" TabIndex="28"></asp:TextBox>
                                        </td>
                                        <td id="Td26" align="left" style="width: 25%; height: 26px;" runat="server">
                                            Contact Details <%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvbrkmob" runat="server" ControlToValidate="txtbrkmob"
                                                Display="None"   ValidationGroup="Val2"   ErrorMessage="Please Enter Mobile Number!"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="revbrkmob" runat="server" ControlToValidate="txtbrkmob"
                                                ErrorMessage="Please Enter valid Contact Details" Display="None"   ValidationGroup="Val2"   ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td id="Td27" align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox ID="txtbrkmob" runat="server" CssClass="clsTextField" TabIndex="29" Width="97%" MaxLength="12"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                         
                            <asp:Panel ID="panrecwiz" runat="server" Width="100%" GroupingText="Recovery Details Before Escalation ">
                                <table id="tabrecwiz" runat="Server" width="100%" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Employee's Account Number
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtEmpAccNo" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Employee's Bank Name
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtEmpBankName" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Employee's Bank Branch Name
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtEmpBranch" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Recovery Amount (Per Month)
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtEmpRcryAmt" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="LEFT" style="height: 26px; width: 25%">
                                            Recovery From Date
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcryfromdate" runat="Server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Recovery To Date
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcrytodate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="panafteresc1" runat="server" Width="100%" GroupingText="First Escalation ">
                                <table id="tabafteresc1" runat="Server" width="100%" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Recovery Amount (Per Month)
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcramt1" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="LEFT" style="height: 26px; width: 25%">
                                            Recovery From Date
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcrfrmdate1" runat="Server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Recovery To Date
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcrtodate1" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="panafteresc2" runat="server" Width="100%" GroupingText="Second Escalation ">
                                <table id="tabafteresc2" runat="Server" width="100%" cellpadding="1" cellspacing="0">
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Recovery Amount (Per Month)
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcramt2" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="LEFT" style="height: 26px; width: 25%">
                                            Recovery From Date
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcrfrmdate2" runat="Server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Recovery To Date
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtrcrtodate2" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btn2prev" runat="server" Text="Previous" CssClass="button" CausesValidation="false" />
                            <asp:Button ID="btn2Next" runat="server" Text="Next" CausesValidation="true" CssClass="button" ValidationGroup="Val2" />
                        </td>
                    </tr>
                    <tr>
                       <td align="left">
                            <asp:Panel ID="landlord" runat="server" Width="100%">
                               <asp:Panel ID="panPOA" runat="Server" Width="100%" GroupingText="Power of Attorney Details">
                                <table id="tabpoa" runat="Server" width="100%" cellpadding="1" cellspacing="0" border="1">
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Name <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvPOAName" runat="server" ControlToValidate="txtPOAName"
                                                Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Name of Power of Attorney"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtPOAName" runat="server" CssClass="clsTextField" TabIndex="32" Width="97%" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Address <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvPOAAddress" runat="server" ControlToValidate="txtPOAAddress"
                                                Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Address of Power of Attorney"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtPOAAddress" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="1000" TextMode="MultiLine" Rows="4" TabIndex="33"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Contact Details <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                                Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Contact Details of Power of Attorney!"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                                ErrorMessage="Please enter valid Contact Details of Power of Attorney" Display="None"   ValidationGroup="Val3"  
                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtPOAMobile" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="12" TabIndex="34"></asp:TextBox>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            Email-ID
                                            <%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvPOAEmail" runat="server" ControlToValidate="txtPOAEmail"
                                                Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Email of POA!"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="revPOAEmail" runat="server" ControlToValidate="txtPOAEmail"
                                                ErrorMessage="Please Enter valid Email of Power of Attorney" Display="None"   ValidationGroup="Val3"   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="width: 25%; height: 26px;">
                                            <asp:TextBox ID="txtPOAEmail" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="1000" TextMode="MultiLine" TabIndex="35" Rows="4"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                                <asp:Label ID="lbl1" runat="server" CssClass="clsMessage"></asp:Label>
                                <asp:Panel ID="panld1" runat="server" Width="100%" GroupingText="Landlord1 Details">
                                    <table id="tabld1" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                        <tr id="Tr2" runat="server">
                                            <td id="Td7" align="left" style="width: 25%; height: 26px;" runat="server">
                                                Name<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtldname"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 name"></asp:RequiredFieldValidator>
                                            </td>
                                            <td id="Td8" align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtldname" runat="server" TabIndex="36" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 1 <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvldaddr" runat="server" ControlToValidate="txtldaddr"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 Address1"></asp:RequiredFieldValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtldaddr" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" TabIndex="37" Rows="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 2 <%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtld1addr2"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 Address2"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld1addr2" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" TabIndex="38" Rows="5"></asp:TextBox>
                                            </td>
                                             <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 3 <%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtld1addr3"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 Address3"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld1addr3" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" TabIndex="38" Rows="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                        
                                             <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                State <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlstate"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Select Landlord1 State" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:DropDownList ID="ddlstate" runat="server" CssClass="clsTextField" AutoPostBack="true" Width="99%" TabIndex="39"></asp:DropDownList>
                                            </td>
                                            <td    align="left" style="width: 25%; height: 26px;" runat="server">
                                                City <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlld1city"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Select Landlord1 City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td    align="left" style="width: 25%; height: 26px;" runat="server">
                                               <asp:TextBox id="ddlld1city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="40"></asp:TextBox>
                                               <%-- <asp:DropDownList ID="ddlld1city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="40"></asp:DropDownList>--%>
                                            </td>
                                        </tr>
                                        <tr >
                                        
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                            
                                                PIN CODE <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld1pin" runat="server" ControlToValidate="txtld1Pin"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 PAN No!"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtld1Pin"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 Pin number in numerics only. !"
                                                    ValidationExpression="^[0-9 ]*"></asp:RegularExpressionValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <div onmouseover="Tip('Enter PIN No with maximum length 10')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtld1Pin" runat="server" CssClass="clsTextField" Width="97%" TabIndex="41" MaxLength="10"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                            
                                                PAN No <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="revpan" runat="server" ControlToValidate="txtPAN"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 PAN No!"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="REVl1pan" runat="server" ControlToValidate="txtPAN"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 Pan number in Alphanumerics only. !"
                                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" 
   ControlToValidate="txtPAN" Display="None" ValidationGroup="Val3"
   ErrorMessage="Landlord1 Pan card Minimum length should be 10"
   ValidationExpression=".{10}.*" />
                                            
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtPAN" runat="server" CssClass="clsTextField" Width="97%" TabIndex="42" MaxLength="10"></asp:TextBox>
                                            </td>
                                            
                                        </tr>
                                        <tr runat="server">
                                        <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Email<%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvldemail" runat="server" ControlToValidate="txtldemail"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 Email!"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="revldemail" runat="server" ControlToValidate="txtldemail"
                                                    ErrorMessage="Please Enter valid Email of Landlord1" Display="None"   ValidationGroup="Val3"   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtldemail" runat="server" CssClass="clsTextField" Width="97%" TabIndex="43" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Contact Details <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvmob" runat="server" ControlToValidate="txtmob"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord1 Contact Details!"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revmob" runat="server" ControlToValidate="txtmob"
                                                    ErrorMessage="Please Enter valid Contact Details of Landlord1" Display="None"   ValidationGroup="Val3"  
                                                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtmob" runat="server" CssClass="clsTextField" Width="97%" TabIndex="44" MaxLength="12"></asp:TextBox>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Monthly Rent Payable <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvMrent" runat="server" ControlToValidate="txtpmonthrent"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter  Monthly Rent Payable"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revmrent" runat="server" ControlToValidate="txtpmonthrent"
                                                    ErrorMessage="Please Enter Valid Monthly Rent Payable" Display="None"   ValidationGroup="Val3"   ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td id="Td34" align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtpmonthrent" runat="server" CssClass="clsTextField" Width="97%"
                                                    MaxLength="15" TabIndex="45"></asp:TextBox>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Security Deposit <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvpsec" runat="server" ControlToValidate="txtpsecdep"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Security Deposit!"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revpsec" runat="server" ControlToValidate="txtpsecdep"
                                                    ErrorMessage="Please enter Valid Security Deposit" Display="None"   ValidationGroup="Val3"   ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtpsecdep" runat="server" TabIndex="46" CssClass="clsTextField" Width="97%" MaxLength="15"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                        <td align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                From Date <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvPFromdate" runat="server" ControlToValidate="txtpfromdate"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter From Date"></asp:RequiredFieldValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                <asp:TextBox ID="txtpfromdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server" visible="False">
                                                To Date <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvptodate" runat="server" ControlToValidate="txtptodate"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter To Date"></asp:RequiredFieldValidator>
                                                <%--<asp:CompareValidator ID="cvptodate" Type="Date" runat="server" Operator="LessThanEqual"
                                                    ErrorMessage="To Date Should be More than From Date!" Display="None"   ValidationGroup="Val3"   ControlToValidate="txtpfromdate"
                                                    ControlToCompare="txtptodate" ></asp:CompareValidator>--%>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                <asp:TextBox ID="txtptodate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                            </td>
                                            </tr>
                                            <tr>
                                            
                                            <td align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                Select Payment Mode <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="cvmode" runat="server" ControlToValidate="ddlpaymentmode"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Select PaymentMode" InitialValue="--Select PaymentMode--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                <asp:DropDownList ID="ddlpaymentmode" runat="server" CssClass="clsComboBox" Width="99%"
                                                    AutoPostBack="True" TabIndex="47">
                                                    <asp:ListItem Value="--Select PaymentMode--">--Select PaymentMode--</asp:ListItem>
                                                    <asp:ListItem Value="1">DD</asp:ListItem>
                                                    <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                                    <asp:ListItem Value="3">NEFT / RTGS</asp:ListItem>
                                                </asp:DropDownList>&nbsp;
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td colspan="4" width="100%" runat="server">
                                                <asp:Panel ID="panel1" runat="server" Width="100%">
                                                    <table id="table4" runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <%--<tr runat="server">
                                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                                Cheque No <font class="clsNote">*</font>
                                                                                <asp:RequiredFieldValidator ID="rfvCheque" runat="server" ControlToValidate="txtCheque"
                                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Cheque Number Required"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="rvcheque" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtCheque"
                                                                                    ErrorMessage="Invalid Cheque Number" ValidationExpression="^[A-Za-z0-9]*$"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                                <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtCheque" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                                            </td>--%>
                                                        <tr>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Bank Name <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Bank Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revBankName" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtBankName"
                                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtBankName" runat="server" TabIndex="48" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Account Number <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revAccno" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtAccNo"
                                                                    ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtAccNo" runat="server" TabIndex="49" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td colspan="4" width="100%" runat="server">
                                                <asp:Panel ID="panelL12" runat="server" Width="100%">
                                                    <table runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Account Number <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtL12Accno"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtL12Accno" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtL12Accno" runat="server" TabIndex="50" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td colspan="4" width="100%" runat="server">
                                                <asp:Panel ID="panel2" runat="server" Width="100%">
                                                    <table id="table5" runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                LandLords Account Number <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Account Name" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtIBankName" runat="server" TabIndex="51" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Bank Name <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Bank Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revDeposited" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtDeposited"
                                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtDeposited" runat="server" TabIndex="52" CssClass="clsTextField" Width="97%"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td id="Td51" align="left" style="height: 26px; width: 25%" runat="server">
                                                                Branch Name <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvBrnch" runat="server" ControlToValidate="txtbrnch"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Branch Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revbrnch" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtbrnch"
                                                                    ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtbrnch" runat="server" TabIndex="53" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                IFSC Code <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvIFsc" runat="server" ControlToValidate="txtIFSC"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="IFSC Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="REVIFsc" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtIFSC"
                                                                    ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtIFSC" runat="server" TabIndex="54" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Button ID="btnAddlandlord1" runat="server" CssClass="button" Text="Add Landlord1"
                                                    Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlld2" runat="server" Width="100%" GroupingText="Landlord2 Details">
                                    <table id="tabld2" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                        <tr runat="server">
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Name<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2name" runat="server" ControlToValidate="txtld1name"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 name"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld1name" runat="server" TabIndex="55" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 1<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2address" runat="server" ControlToValidate="txtld2addr"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Address"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2addr" runat="server" TabIndex="56" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" Rows="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                         <tr>
                                        <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 2 <%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtld2addr2"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Address2"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2addr2" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" TabIndex="57" Rows="5"></asp:TextBox>
                                            </td>
                                             <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 3 <%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtld2addr3"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Address3"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2addr3" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" TabIndex="58" Rows="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                        
                                             <td    align="left" style="width: 25%; height: 26px;" runat="server">
                                                State <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlld2state"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Select Landlord2 State" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:DropDownList ID="ddlld2state" runat="server" CssClass="clsTextField" AutoPostBack="true" Width="99%" TabIndex="59"></asp:DropDownList>
                                            </td>
                                            <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                City <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ddlld2city"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Select Landlord2 City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                               <asp:TextBox ID="ddlld2city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="60"></asp:TextBox>
                                               
                                                <%--<asp:DropDownList ID="ddlld2city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="60"></asp:DropDownList>--%>
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                        <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                            
                                                PIN CODE <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtld2Pin"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 PIN No!"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtld2Pin"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Pin number in numerics only. !"
                                                    ValidationExpression="^[0-9 ]*"></asp:RegularExpressionValidator>
                                            </td>
                                            <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtld2Pin" runat="server" CssClass="clsTextField" Width="97%" TabIndex="61" MaxLength="10"></asp:TextBox>
                                            </td>
                                        
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                PAN No<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2pan" runat="server" ControlToValidate="txtld2pan"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 PAN"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revld2pan" runat="server" ControlToValidate="txtld2pan"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Pan number in Alphanumerics only. !"
                                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" 
   ControlToValidate="txtld2pan" Display="None" ValidationGroup="Val3"
   ErrorMessage="Landlord2 Pan card Minimum length should be 10"
   ValidationExpression=".{10}.*" />
                                            
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2pan" runat="server" TabIndex="62" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Email<%--<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RFVLD2EMAIL" runat="server" ControlToValidate="txtld2email"
                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Email"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="revld2email" runat="server" ControlToValidate="txtld2email"
                                                    ErrorMessage="Please Enter valid  Email of Landlord2" Display="None"   ValidationGroup="Val3"   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2email"  runat="server"  TabIndex="63" CssClass="clsTextField" Width="97%"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Contact Details<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2mobile" runat="server" ControlToValidate="txtld2mob"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Contact Details"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revld2mobile" runat="server" ControlToValidate="txtld2mob"
                                                    ErrorMessage="Please Enter valid Landlord2 Contact Details" Display="None"   ValidationGroup="Val3"   ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2mob" runat="server" TabIndex="64" CssClass="clsTextField" Width="97%" MaxLength="12"></asp:TextBox>
                                            </td>
                                            </tr>
                                            
                                        
                                        <tr runat="server">
                                            
                                            <td align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                From Date<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2fdate" runat="server" ControlToValidate="txtld2frmdate"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 From Date"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                <asp:TextBox ID="txtld2frmdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                To Date<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2todate" runat="server" ControlToValidate="txtld2todate"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 To Date"></asp:RequiredFieldValidator>
                                                <%--  <asp:CompareValidator ID="cvld2todate" runat="server" Operator="LessThanEqual" ErrorMessage="To Date Should be more Than From Date!"
                                    Type="Date" ControlToValidate="txtld2frmdate" ControlToCompare="txtld2todate"
                                    Display="None"   ValidationGroup="Val3"   ></asp:CompareValidator>--%>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                <asp:TextBox ID="txtld2todate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                            </td>
                                            </tr>
                                                    <tr>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Monthly Rent Payable<%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2rent" runat="server" ControlToValidate="txtld2rent"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Monthly Rent"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="revld2rent" runat="server" ControlToValidate="txtld2rent"
                                                    ErrorMessage="Please Enter Valid Monthly Rent Payable of Landlord2" Display="None"   ValidationGroup="Val3"  
                                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2rent" runat="server" CssClass="clsTextField" TabIndex="65" Width="97%" MaxLength="15"></asp:TextBox>
                                            </td>
                                        
                                            
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Security Deposit<%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2sd" runat="server" ControlToValidate="txtld2sd"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord2 Security Deposit"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="revld2sd" runat="server" ControlToValidate="txtld2sd"
                                                    ErrorMessage="Please enter Valid Security Deposit of landlord2" Display="None"   ValidationGroup="Val3"  
                                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld2sd" runat="server" TabIndex="66" CssClass="clsTextField" Width="97%" MaxLength="15"></asp:TextBox>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td  align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                Select Payment Mode<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld2paymode" runat="server" ControlToValidate="ddlld2mode"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Select Payment Mode of Landlord2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="height: 26px; width:50%" colspan="2" runat="server">
                                                <asp:DropDownList ID="ddlld2mode" runat="server" CssClass="clsComboBox" Width="99%"
                                                    AutoPostBack="True" TabIndex="67">
                                                    <asp:ListItem Value="--Select--">--Select PaymentMode--</asp:ListItem>
                                                    <asp:ListItem Value="1">DD</asp:ListItem>
                                                    <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                                    <asp:ListItem Value="3">NEFT / RTGS</asp:ListItem>
                                                </asp:DropDownList>&nbsp;
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td colspan="4" width="100%" runat="server">
                                                <asp:Panel ID="panel3" runat="server" Width="100%">
                                                    <table id="table1" runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <%--<tr runat="server">
                                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                                Cheque No
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtld2cheque"
                                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Cheque Number Required"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                                    ControlToValidate="txtld2cheque" ErrorMessage="Invalid Cheque Number of landlord2"
                                                                                    ValidationExpression="^[A-Za-z0-9]*$"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                                <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtld2cheque" runat="server" CssClass="clsTextField" Width="97%"
                                                                                        MaxLength="50"></asp:TextBox>
                                                                            </td>--%>
                                                        <tr>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Bank Name<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld2bankname" runat="server" ControlToValidate="txtld2bankname"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Bank Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld2bankname" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtld2bankname" ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtld2bankname" runat="server" TabIndex="68" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Account Number<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld2accno" runat="server" ControlToValidate="txtld2accno"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld2accno" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtld2accno"
                                                                    ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtld2accno" runat="server" TabIndex="69" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" width="100%" runat="Server">
                                                <asp:Panel ID="pnll22" runat="server" Width="100%">
                                                    <table runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <tr id="Tr4" runat="server">
                                                            <td  align="left" style="height: 26px; width: 25%" runat="server">
                                                                Account Number <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtl22accno"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtl22accno" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td  align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtl22accno" runat="server" TabIndex="70" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td colspan="4" width="100%" runat="server">
                                                <asp:Panel ID="panel4" runat="server" Width="100%">
                                                    <table id="table6" runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                LandLords Account Number<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvl2accnumber" runat="server" ControlToValidate="txtld2IBankName"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revl2accnumber" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtld2IBankName" ErrorMessage="Enter Valid Account Number"
                                                                    ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtld2IBankName" runat="server" TabIndex="71" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Bank Name<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld2Deposited" runat="server" ControlToValidate="txtld2Deposited"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Deposited Bank Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld2Deposited" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtld2Deposited" ErrorMessage="Enter Valid Deposited Bank "
                                                                    ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtld2Deposited" runat="server" TabIndex="72" CssClass="clsTextField" Width="97%"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Branch Name<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvl2brnch" runat="server" ControlToValidate="txtl2brnchname"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Branch Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revl2brnch" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtl2brnchname"
                                                                    ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td id="Td31" align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtl2brnchname" runat="server" TabIndex="73" CssClass="clsTextField" Width="97%"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                IFSC Code<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld2IFsc" runat="server" ControlToValidate="txtld2IFSC"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="IFSC Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="REVld2IFsc" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtld2IFSC"
                                                                    ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtld2IFSC" runat="server" TabIndex="74" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Button ID="btnAddLandLord2" runat="server" CssClass="button" Text="Add LandLord2"
                                                    Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlld3" runat="server" Width="100%" GroupingText="Landlord3 Details">
                                    <table id="tabld3" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                        <tr runat="server">
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Name<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3name" runat="server" ControlToValidate="txtld3name"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Name"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3name" runat="server" TabIndex="75" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 1 <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3address" runat="server" ControlToValidate="txtld3addr"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Address"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3addr" runat="server" TabIndex="76" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" Rows="5" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 2 <%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtld3addr2"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Address2"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td    align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3addr2" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" TabIndex="57" Rows="5" ></asp:TextBox>
                                            </td>
                                             <td    align="left" style="width: 25%; height: 26px;" runat="server">
                                                Address 3 <%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtld3addr3"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Address3"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3addr3" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                    TextMode="MultiLine" TabIndex="78" Rows="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                       
                                             <td    align="left" style="width: 25%; height: 26px;" runat="server">
                                                State <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="ddlld3state"
                                                    Display="None"    ValidationGroup="Val3"   ErrorMessage="Please Select Landlord3 State" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:DropDownList ID="ddlld3state" runat="server" CssClass="clsTextField" AutoPostBack="true" Width="99%" TabIndex="79"></asp:DropDownList>
                                            </td>
                                             <td     align="left" style="width: 25%; height: 26px;" runat="server">
                                                City <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="ddlld3city"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 City" ></asp:RequiredFieldValidator>
                                            </td>
                                            <td     align="left" style="width: 25%; height: 26px;" runat="server">
                                            <asp:TextBox id="ddlld3city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="80"></asp:TextBox>
                                               <%-- <asp:DropDownList ID="ddlld3city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="80"></asp:DropDownList>--%>
                                            </td>
                                        </tr>
                                       
                                        
                                        
                                        <tr runat="server">
                                        
                                        <td    align="left" style="width: 25%; height: 26px;" runat="server">
                                            
                                                PIN CODE <font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtld3Pin"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 PIN No!"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtld3Pin"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Pin number in numerics only. !"
                                                    ValidationExpression="^[0-9 ]*"></asp:RegularExpressionValidator>
                                            </td>
                                            <td   align="left" style="width: 25%; height: 26px;" runat="server">
                                                <div onmouseover="Tip('Enter PIN No with maximum length 10')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtld3Pin" runat="server" CssClass="clsTextField" Width="97%" TabIndex="81" MaxLength="10"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                PAN No<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3pan" runat="server" ControlToValidate="txtld3pan"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 PAN"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revld3pan" runat="server" ControlToValidate="txtld3pan"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Pan number in Alphanumerics only. !"
                                                    ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" 
   ControlToValidate="txtld3pan" Display="None" ValidationGroup="Val3"
   ErrorMessage="Landlord3 Pan card Minimum length should be 10"
   ValidationExpression=".{10}.*" />
                                            
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3pan" runat="server" CssClass="clsTextField" TabIndex="82" Width="97%" MaxLength="10" ></asp:TextBox>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Email<%--<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvld3email" runat="server" ControlToValidate="txtld3email"
                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Email"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="revld3email" runat="server" ControlToValidate="txtld3email"
                                                    ErrorMessage="Please Enter valid  Email of Landlord3" Display="None"   ValidationGroup="Val3"   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3email" runat="server" CssClass="clsTextField" Width="97%"
                                                    MaxLength="50" TabIndex="83"></asp:TextBox>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                Contact Details<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3mobile" runat="server" ControlToValidate="txtld3mob"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Contact Details"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revld3mob" runat="server" ControlToValidate="txtld3mob"
                                                    ErrorMessage="Please Enter valid Contact Details of Landlord3" Display="None"   ValidationGroup="Val3"  
                                                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3mob" runat="server" CssClass="clsTextField" TabIndex="84" Width="97%" MaxLength="12"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr runat="server" visible="False">
                                            
                                            <td align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                From Date<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3fdate" runat="server" ControlToValidate="txtld3fromdate"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 From Date"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server" visible="False">
                                                <asp:TextBox ID="txtld3fromdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                To Date<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvtdate" runat="server" ControlToValidate="txtld3todate"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 To Date"></asp:RequiredFieldValidator>
                                                <%-- <asp:CompareValidator ID="cvld3tdate" runat="server" Operator="LessThanEqual" ErrorMessage="To Date Should be more Than From Date!"
                                    Type="Date" ControlToValidate="txtld3fromdate" ControlToCompare="txtld3todate"
                                    Display="None"   ValidationGroup="Val3"   ></asp:CompareValidator>--%>
                                            </td>
                                            <td  align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                <asp:TextBox ID="txtld3todate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                            </td>
                                            </tr>
                                                    <tr>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Monthly Rent Payable<%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3monthrent" runat="server" ControlToValidate="txtld3rent"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Month Rent"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="revld3rent" runat="server" ControlToValidate="txtld3rent"
                                                    ErrorMessage="Please Enter Valid Monthly Rent Payable of Landlord3" Display="None"   ValidationGroup="Val3"  
                                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3rent" runat="server" TabIndex="85" CssClass="clsTextField" Width="97%" MaxLength="15"></asp:TextBox>
                                            </td>
                                        
                                            
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                Security Deposit<%--<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3sd" runat="server" ControlToValidate="txtld3sd"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Enter Landlord3 Security Deposit"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="revld3sd" runat="server" ControlToValidate="txtld3sd"
                                                    ErrorMessage="Please enter Valid Security Deposit of landlord3" Display="None"   ValidationGroup="Val3"  
                                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="width: 25%; height: 26px;" runat="server">
                                                <asp:TextBox ID="txtld3sd" runat="server" CssClass="clsTextField" TabIndex="86" Width="97%" MaxLength="15"></asp:TextBox>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                Select Payment Mode<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvld3paymode" runat="server" ControlToValidate="ddlld3mode"
                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Please Select Payment Mode for Landlord3" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                <asp:DropDownList ID="ddlld3mode" runat="server" TabIndex="87" CssClass="clsComboBox" Width="99%"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="--Select--">--Select PaymentMode--</asp:ListItem>
                                                    <asp:ListItem Value="1">DD</asp:ListItem>
                                                    <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                                    <asp:ListItem Value="3"> NEFT / RTGS </asp:ListItem>
                                                </asp:DropDownList>&nbsp;
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td colspan="4" width="100%" runat="server">
                                                <asp:Panel ID="panel5" runat="server" Width="100%">
                                                    <table id="table7" runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <%-- <tr runat="server">
                                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                                Cheque No
                                                                                <asp:RequiredFieldValidator ID="rfvld3chq" runat="server" ControlToValidate="txtld3cheque"
                                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Cheque Number Required"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="revld3cheque" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtld3cheque"
                                                                                    ErrorMessage="Invalid Cheque Number of landlord2" ValidationExpression="^[A-Za-z0-9]*$"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                                <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtld3cheque" runat="server" CssClass="clsTextField" Width="97%"
                                                                                        MaxLength="50"></asp:TextBox>
                                                                            </td>--%>
                                                        <tr>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Bank Name<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld3bank" runat="server" ControlToValidate="txtld3bankname"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Bank Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld3bank" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtld3bankname"
                                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtld3bankname" runat="server" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50" TabIndex="88"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Account Number<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld3acc" runat="server" ControlToValidate="txtLd3acc"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld3acc" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtLd3acc"
                                                                    ErrorMessage="Enter Valid Account Number" ValidationExpression="^[a-zA-Z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtLd3acc" runat="server" TabIndex="89" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" width="100%" runat="Server">
                                                <asp:Panel ID="pnll32" runat="server" Width="100%">
                                                    <table runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Account Number <font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtl32accno"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtl32accno" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtl32accno" runat="server" TabIndex="90" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td colspan="4" width="100%" runat="server">
                                                <asp:Panel ID="panel6" runat="server" Width="100%">
                                                    <table id="table8" runat="server" width="100%" cellpadding="0" cellspacing="0" >
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                LandLords Account Number<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld3ibankname" runat="server" ControlToValidate="txtld3IBankName"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Account Number Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld3ibankname" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtld3IBankName" ErrorMessage="Enter Valid Account Number"
                                                                    ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtld3IBankName" runat="server" TabIndex="91" CssClass="clsTextField" Width="97%"
                                                                        MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Bank Name<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld3deposited" runat="server" ControlToValidate="txtld3Deposited"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Bank Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld3deposited" Display="None"   ValidationGroup="Val3"   runat="server"
                                                                    ControlToValidate="txtld3Deposited" ErrorMessage="Enter Valid Bank Name " ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtld3Deposited" runat="server" TabIndex="92" CssClass="clsTextField" Width="97%"
                                                                    MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr runat="server">
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                Branch Name<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvl3brnch" runat="server" ControlToValidate="txtl3brnch"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="Branch Name Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revl3brnch" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtl3brnch"
                                                                    ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" /> 
                                                                <asp:TextBox ID="txtl3brnch" runat="server" TabIndex="93" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                IFSC Code<font class="clsNote">*</font>
                                                                <asp:RequiredFieldValidator ID="rfvld3ifsc" runat="server" ControlToValidate="txtld3ifsc"
                                                                    Display="None"   ValidationGroup="Val3"   ErrorMessage="IFSC Required"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revld3ifsc" Display="None"   ValidationGroup="Val3"   runat="server" ControlToValidate="txtld3ifsc"
                                                                    ErrorMessage="Enter Valid IFSC" ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td align="left" style="height: 26px; width: 25%" runat="server">
                                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                    onmouseout="UnTip()" />
                                                                <asp:TextBox ID="txtld3ifsc" runat="server" TabIndex="94" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Button ID="btnAddlandlord3" runat="server" CssClass="button" Text="Add LandLord3"
                                                    Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btn3Prev" runat="server" CssClass="button" Text="Previous" CausesValidation="False" />
                            <asp:Button ID="btn3Finish" runat="server" CssClass="button" Text="Finish" CausesValidation="true" ValidationGroup="Val3" />
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0">
    <ProgressTemplate>
    <img src="../../images/ajax-loader.gif" />
        
    </ProgressTemplate>
    </asp:UpdateProgress>
