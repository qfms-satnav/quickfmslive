Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_AssetMovementReportDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim Req_status As Integer
    Dim strMMR_RAISEDBY, strRaisedDt As String

    Dim FBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String
    Dim strOutWardAppBy, strOutWardAppDt, strOutWardAppComment As String
    Dim strITAppBy, strITAppDt, strITAppComment As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            getDetailsbyReqId(strReqId)
            BindLocations()
            BindRequestAssets(strReqId)
        End If
    End Sub

    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)

        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("GET_INTRA_MVMT_REQID", param)
        If dr.Read Then
            FBDG_ID = dr.Item("FLOC_CODE")
            FTower = dr.Item("MMR_FROMBDG_ID")
            FFloor = dr.Item("MMR_FROMFLR_ID")
            TBDG_ID = dr.Item("FLOC_CODE")
            TTower = dr.Item("MMR_TOBDG_ID")
            TFloor = dr.Item("MMR_TOFLR_ID")
            receiveAst = dr.Item("MMR_RECVD_BY")
            strremarks = dr.Item("MMR_COMMENTS")
            Req_status = dr.Item("MMR_LRSTATUS_ID")

            strMMR_RAISEDBY = dr.Item("MMR_RAISEDBY")
            strRaisedDt = dr.Item("MMR_MVMT_DATE")


            strITAppBy = dr.Item("MMR_APPROVED_BY")
            strITAppDt = dr.Item("MMR_Approved_date")
            strITAppComment = dr.Item("MMR_App_Rej_COMMENTS")

            strOutWardAppBy = dr.Item("MMR_OutWardAPPROVED_BY")
            strOutWardAppDt = dr.Item("MMR_OutWard_Date")
            strOutWardAppComment = dr.Item("MMR_OutWard_COMMENTS")
            'lblReqId.Text = Request.QueryString("Req_id")
        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If
        txtPendingRemarks.Text = strremarks
        txtPendingApproved.Text = strMMR_RAISEDBY
        txtPersonName.Text = receiveAst
        txtPendingDate.Text = strRaisedDt
        chkstatus(Req_status)
        'If dr.Read Then
        '    FBDG_ID = dr.Item("FLOC_CODE")
        '    FTower = dr.Item("MMR_FROMBDG_ID")
        '    FFloor = dr.Item("MMR_FROMFLR_ID")
        '    TBDG_ID = dr.Item("FLOC_CODE")
        '    TTower = dr.Item("MMR_TOBDG_ID")
        '    TFloor = dr.Item("MMR_TOFLR_ID")
        '    receiveAst = dr.Item("MMR_RECVD_BY")
        '    strremarks = dr.Item("MMR_COMMENTS")
        '    Req_status = dr.Item("MMR_LRSTATUS_ID")
        '    chkstatus(Req_status)
        'End If
        'If dr.IsClosed = False Then
        '    dr.Close()
        'End If

        'txtPersonName.Text = receiveAst

    End Sub

    Public Sub chkstatus(ByVal Req_status As Integer)

        If Req_status = 1 Then
            ddlDLoc.Enabled = True
            ddlDTower.Enabled = True
            ddlDFloor.Enabled = True
            txtPersonName.Enabled = True
            fldNewReq.Visible = True
            tblITSummary.Visible = False
            
        Else
            ddlDLoc.Enabled = False
            ddlDTower.Enabled = False
            ddlDFloor.Enabled = False
            txtPersonName.Enabled = False
            tblITSummary.Visible = False
            fldITApproval.Visible = False
            fldOutWard.Visible = False
            fldNewReq.Visible = True

        End If

        If Req_status >= 1017 Then
            ddlDLoc.Enabled = False
            ddlDTower.Enabled = False
            ddlDFloor.Enabled = False
            txtPersonName.Enabled = False
            tblITSummary.Visible = True
            fldITApproval.Visible = True

            lblITApproval.Text = strITAppBy
            lblITDate.Text = strITAppDt
            lblITRemarks.Text = strITAppComment
        Else
            fldITApproval.Visible = False
        End If
        If Req_status >= 1019 Then
            ddlDLoc.Enabled = False
            ddlDTower.Enabled = False
            ddlDFloor.Enabled = False
            txtPersonName.Enabled = False
            tblITSummary.Visible = True
            tblOutWardSummary.Visible = True
            fldOutWard.Visible = True

            lblOutwardApproved.Text = strOutWardAppBy
            lblOutWardDate.Text = strOutWardAppDt
            lblOutwardRemarks.Text = strOutWardAppComment
        Else
            fldOutWard.Visible = False
        End If

    End Sub

    'Public Sub chkstatus(ByVal Req_status As Integer)

    '    If Req_status = 1 Then

    '        fldNewReq.Visible = True

    '    End If

    '    If Req_status >= 1017 Then

    '        tblITSummary.Visible = True
    '        fldITApproval.Visible = True
    '        fldNewReq.Visible = True
    '        lblITApproval.Text = strITAppBy
    '        lblITDate.Text = strITAppDt
    '        lblITRemarks.Text = strITAppComment
    '    Else
    '        fldITApproval.Visible = False
    '    End If
    '    If Req_status >= 1019 Then

    '        tblITSummary.Visible = True
    '        tblOutWardSummary.Visible = True
    '        fldOutWard.Visible = True
    '        fldNewReq.Visible = True
    '        lblOutwardApproved.Text = strOutWardAppBy
    '        lblOutWardDate.Text = strOutWardAppDt
    '        lblOutwardRemarks.Text = strOutWardAppComment
    '    Else
    '        fldOutWard.Visible = False
    '    End If

    'End Sub

    Private Sub BindLocations()
        '----------- Binding Location-----------------------
        ObjSubsonic.Binddropdown(ddlSLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")
        ObjSubsonic.Binddropdown(ddlDLoc, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")

        '------------ From Building/Tower/Floor -----------------------------
        If FBDG_ID = "" Or FBDG_ID Is Nothing Then

        Else
            ddlSLoc.Items.FindByValue(FBDG_ID).Selected = True

            '----------- Binding Tower -----------------------
            If ddlSLoc.SelectedIndex > 0 Then
                Dim LocCode As String = ddlSLoc.SelectedItem.Value
                BindTowersByLocation(LocCode, ddlSTower)
            End If
            ddlSTower.Items.FindByValue(FTower).Selected = True
            '-------------------------------------------------

            If ddlSTower.SelectedIndex > 0 Then
                Dim TwrCode As String = ddlSTower.SelectedItem.Value
                BindFloorsByTower(TwrCode, ddlSFloor)
            End If
            ddlSFloor.Items.FindByValue(FFloor).Selected = True

        End If
        '------------ From Building/Tower/Floor -------------------------
        If TBDG_ID = "" Or TBDG_ID Is Nothing Then

        Else
            ddlDLoc.Items.FindByValue(TBDG_ID).Selected = True

            '----------- Binding Tower -----------------------
            If ddlDLoc.SelectedIndex > 0 Then
                Dim LocCode As String = ddlDLoc.SelectedItem.Value
                BindTowersByLocation(LocCode, ddlDTower)
            End If
            ddlDTower.Items.FindByValue(TTower).Selected = True
            '-------------------------------------------------

            If ddlDTower.SelectedIndex > 0 Then
                Dim TwrCode As String = ddlDTower.SelectedItem.Value
                BindFloorsByTower(TwrCode, ddlDFloor)
            End If
            ddlDFloor.Items.FindByValue(TFloor).Selected = True

        End If
    End Sub


    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "GET_MVMT_ASTS", param)
    End Sub


    Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlSTower)
            getDetailsbyReqId(Request.QueryString("Req_id"))
        End If
    End Sub

    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = LocCode
        ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    End Sub

    Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
        If ddlDLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlDLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlDTower)
        End If
    End Sub

    Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
        If ddlSTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlSTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlSFloor)
        End If
    End Sub
    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TwrCode", DbType.String)
        param(0).Value = TwrCode
        ObjSubsonic.Binddropdown(ddl, "USP_FLOOR_GETBYTOWER", "FLR_NAME", "FLR_CODE", param)
    End Sub

    Protected Sub ddlDTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTower.SelectedIndexChanged
        If ddlDTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlDTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlDFloor)
        End If
    End Sub
 
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAssetMovementReport.aspx")
    End Sub


End Class
