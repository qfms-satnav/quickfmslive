Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Partial Class Controls_AssetLocationReport
    Inherits System.Web.UI.UserControl

    Protected Sub ddlLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLoc.SelectedIndexChanged
        Try
            If ddlLoc.SelectedIndex > 0 Then
                BindGrid()
                gvitems.Visible = True
            Else
                gvitems.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Bind_ddlLoc()
        End If
        gvitems.Visible = False
    End Sub
    Private Sub Bind_ddlLoc()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCDDL")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlLoc.DataSource = sp.GetDataSet()
            ddlLoc.DataTextField = "LCM_NAME"
            ddlLoc.DataValueField = "LCM_CODE"
            ddlLoc.DataBind()
            ddlLoc.Items.Insert(0, New ListItem("--Select Location--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSETS_BY_lOCATION")
            sp.Command.AddParameter("@LOCCODE", ddlLoc.SelectedItem.Value, DbType.String)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
End Class
