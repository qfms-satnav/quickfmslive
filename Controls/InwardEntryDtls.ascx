<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InwardEntryDtls.ascx.vb" Inherits="Controls_InwardEntryDtls" %>
<script>
    function setup(id) {
        $('#' + id).datepicker({
            startDate: new Date(),
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    };
</script>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From Location</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlSLoc" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top: 10px">
    <div class="col-md-12 pull-right">
        <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Assets Found." AllowPaging="true"
            PageSize="10" CssClass="table GridStyle" GridLines="None" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    </div>
</div>
<br />
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To Location</label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="False">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="col-md-5 control-label">Sending Date</label>
            <div class="input-group date" style="width: 150px" id='frmdate'>
                <asp:TextBox runat="server" type="text" class="form-control" placeholder="mm/dd/yyyy" ID="txtSendingDate" Enabled="false"></asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Person To Receive Assets</label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtPersonName" runat="server" MaxLength="50" CssClass="form-control" Enabled="False"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="col-md-5 control-label">Receiving Date<span style="color: red;">*</span></label>
            <div class="input-group date" style="width: 150px" id='todate'>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReceiveDate"
                    Display="None" ErrorMessage="Please Select Receiving Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:TextBox runat="server" type="text" class="form-control" placeholder="mm/dd/yyyy" ID="txtReceiveDate"></asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                </span>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="txtRemarks"
                        Display="none" ErrorMessage="Please enter Remarks!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnSubmit" runat="server" Text="Received" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"></asp:Button>
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />

        </div>
    </div>
</div>
<script>
    function refreshSelectpicker() {
        $("#<%=ddlDLoc.ClientID%>").selectpicker();
            $("#<%= ddlSLoc.ClientID%>").selectpicker();


    }
    refreshSelectpicker();
</script>
