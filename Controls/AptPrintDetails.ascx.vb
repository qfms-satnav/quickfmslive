﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptPrintDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            btnprint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
            BINDDETAILS()


        End If
    End Sub
    Private Sub BINDDETAILS()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SCHEDULE_PRINTDETAILS_BYREQID")
            sp.Command.AddParameter("@REQUEST_ID", Request.QueryString("rid"), DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblreqno.Text = ds.Tables(0).Rows(0)("REQUESTID")
                lblfrmdate.Text = ds.Tables(0).Rows(0)("STARTDATE")
                lbltodate.Text = ds.Tables(0).Rows(0)("ENDDATE")
                lbltraveller.Text = ds.Tables(0).Rows(0)("AUR_FIRST_NAME")
                lblpersonnelno.Text = ds.Tables(0).Rows(0)("RAISEDBY")
                lblgrade.Text = ds.Tables(0).Rows(0)("AUR_GRADE")
                lblgender.Text = ds.Tables(0).Rows(0)("AUR_GENDER")
                lbldepartment.Text = ds.Tables(0).Rows(0)("DEP_NAME")
                lbllocation.Text = ds.Tables(0).Rows(0)("LCM_NAME")
                lblrequestdate.Text = ds.Tables(0).Rows(0)("REQUESTED_ON")
                lblaur_id.Text = ds.Tables(0).Rows(0)("RAISEDBY")
                lblstatus.Text = ds.Tables(0).Rows(0)("STA_TITLE")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    lbldestination.Text = ds.Tables(0).Rows(0)("DESTINATION") + " AND " + ds.Tables(0).Rows(i)("DESTINATION")

                Next

                lblpurpose.Text = ds.Tables(0).Rows(0)("REQUESTNAME")
                lblactivity.Text = ds.Tables(0).Rows(0)("REQUESTNAME")
                lblcard.Text = ds.Tables(0).Rows(0)("PAYTYPE")
                lblcostcenter.Text = ds.Tables(0).Rows(0)("Cost_Center_name")
                lblempcompanycode.Text = ds.Tables(0).Rows(0)("ver_code")
                lblbusarea.Text = ds.Tables(0).Rows(0)("DEP_NAME")
                lbltripcc.Text = ds.Tables(0).Rows(0)("Cost_Center_name")
                lblcompanycode.Text = ds.Tables(0).Rows(0)("ver_code")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    lbldestination1.Text = ds.Tables(0).Rows(0)("DESTINATION") + " AND " + ds.Tables(0).Rows(i)("DESTINATION")

                Next

                lblbeginning.Text = ds.Tables(0).Rows(0)("STARTDATE")
                lblend.Text = ds.Tables(0).Rows(0)("ENDDATE")


            End If
            If ds.Tables(1).Rows.Count > 0 Then
                gvitems1.DataSource = ds.Tables(1)
                gvitems1.DataBind()

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
