<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WorkRequestDetails.ascx.vb"
    Inherits="Controls_WorkRequestDetails" %>


<script type="text/javascript">
<!--
    function printPartOfPage(elementId) {
        var printContent = document.getElementById(elementId);
        var windowUrl = 'about:blank';
        var uniqueName = new Date();
        var windowName = 'Print' + uniqueName.getTime();
        var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
        printWindow.close();
    }
    // -->
</script>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Work Request Details
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                        <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <%--<tr>

            <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>Work Request Details</strong></td>

        </tr>--%>
        <tr>
            <td align="left">
                <div id="Div1">
                    <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblworkreq" runat="server" CssClass="bodytext" Text="Select WorkRequest"></asp:Label>
                                <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                                    Display="Dynamic" ErrorMessage="Please Select WorkRequest" ValidationGroup="Val1"
                                    InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" width="50%">
                                <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="dropDown" AutoPostBack="true"
                                    Width="99%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblbdg" runat="server" CssClass="bodytext" Text="City"></asp:Label>
                            </td>
                            <td align="left" width="50%">
                                <asp:TextBox ID="txtBuilding" runat="server" CssClass="textBox" Width="97%" MaxLength="50"
                                    Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblProperty" runat="server" CssClass="bodytext" Text="Property"></asp:Label>
                            </td>
                            <td align="left" width="50%">
                                <asp:TextBox ID="txtproperty" runat="server" CssClass="textBox" Width="97%" MaxLength="50"
                                    Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblTitle" runat="server" CssClass="bodytext" Text="Work Title"></asp:Label>
                            </td>
                            <td align="left" width="50%">
                                <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="textBox" Width="97%" MaxLength="50"
                                    Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblworkspec" runat="server" CssClass="bodytext" Text="Work Specifications"></asp:Label>
                            </td>
                            <td align="left" width="50%">
                                <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="textBox" Width="97%" Rows="3"
                                    TextMode="MultiLine" MaxLength="1000" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblamount" runat="server" CssClass="bodytext" Text="Estimated Amount"></asp:Label>
                            </td>
                            <td align="left" width="50%">
                                <asp:TextBox ID="txtamount" runat="server" CssClass="textBox" Width="97%" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="25%">Vendor Name<font class="clsNote"></font>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:TextBox ID="txtVendorName" runat="server" CssClass="textBox" Width="97%" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblworkstatus" runat="server" CssClass="bodytext" Text="Select Work Status"></asp:Label>
                            </td>
                            <td align="left" width="50%">
                                <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="dropDown" AutoPostBack="true"
                                    Width="99%" Enabled="False">
                                    <asp:ListItem>--Select Status--</asp:ListItem>
                                    <asp:ListItem Value="0">Pending</asp:ListItem>
                                    <asp:ListItem Value="1">InProgress</asp:ListItem>
                                    <asp:ListItem Value="2">Completed</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblpay" runat="server" CssClass="bodytext" Text="Select Payment Terms"></asp:Label>&nbsp;
                            </td>
                            <td align="left" width="50%">
                                <asp:DropDownList ID="ddlpay" runat="server" CssClass="dropDown" AutoPostBack="true"
                                    Width="99%" Enabled="false">
                                    <asp:ListItem>--Select PaymentTerms--</asp:ListItem>
                                    <asp:ListItem Value="1">Cheque</asp:ListItem>
                                    <asp:ListItem Value="2">Cash</asp:ListItem>
                                    <asp:ListItem Value="3">Bank Transfer</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="lblRemarks" runat="server" CssClass="bodytext" Text="Remarks"></asp:Label>&nbsp;
                            </td>
                            <td align="left" width="50%">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="textBox" Width="97%" TextMode="MultiLine"
                                    Rows="3" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="panel1" runat="server" Width="100%">
                        <table id="table3" runat="server" width="100%" cellpadding="2" cellspacing="0" border="1">
                            <tr>
                                <td align="left" style="width: 50%">
                                    <asp:Label ID="lblCheque" runat="Server" CssClass="bodytext" Text="Cheque No"></asp:Label>
                                    &nbsp;
                                </td>
                                <td align="left" style="width: 50%">

                                    <asp:TextBox ID="txtCheque" runat="Server" CssClass="textBox" Width="97%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%">
                                    <asp:Label ID="lblbankname" runat="Server" CssClass="bodytext" Text="Bank Name"></asp:Label>
                                    &nbsp;
                                </td>
                                <td align="left" style="width: 50%">

                                    <asp:TextBox ID="txtBankName" runat="Server" CssClass="textBox" Width="97%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%">
                                    <asp:Label ID="lblAccountNumber" runat="Server" CssClass="bodytext" Text="Account Number"></asp:Label>
                                    &nbsp;
                                </td>
                                <td align="left" style="width: 50%">

                                    <asp:TextBox ID="txtAccNo" runat="Server" CssClass="textBox" Width="97%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="panel2" runat="Server" Width="100%">
                        <table id="table4" runat="server" width="100%" cellpadding="2" cellspacing="0" border="1">
                            <tr>
                                <td align="left" width="50%">
                                    <asp:Label ID="lblIsuuingBank" runat="Server" CssClass="bodytext" Text="Issuing Bank Name"></asp:Label>
                                    &nbsp;
                                </td>
                                <td align="left" style="width: 50%">

                                    <asp:TextBox ID="txtIBankName" runat="Server" CssClass="textBox" Width="97%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%">
                                    <asp:Label ID="lblDeposited" runat="Server" CssClass="bodytext" Text="Deposited Bank"></asp:Label>
                                    &nbsp;
                                </td>
                                <td align="left" style="width: 50%">

                                    <asp:TextBox ID="txtDeposited" runat="Server" CssClass="textBox" Width="97%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblifcb" runat="Server" CssClass="bodytext" Text="IFSC Code"></asp:Label>
                                    &nbsp;
                                </td>
                                <td align="left" style="width: 25%">

                                    <asp:TextBox ID="txtIFCB" runat="Server" CssClass="textBox" Width="97%" Enabled="False"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <table cellspacing="0" cellpadding="0" width="100%" border="1">
                    <tr>
                        <td colspan="6" align="center">&nbsp;&nbsp;
                                    <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Print" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
