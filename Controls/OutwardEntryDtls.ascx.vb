Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_OutwardEntryDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim strstat As String


    Dim FBDG_ID, SBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String
    Dim FromLoc, ToLoc, FromLocCode, ToLocCode, ReceivingPerson As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            strstat = Request.QueryString("stat")
            getDetailsbyReqId(strReqId)
            BindLocations()

        End If
    End Sub

    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)
        If strstat = "INTRA" Then

            Dim dr As SqlDataReader
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strREQ_id
            dr = ObjSubsonic.GetSubSonicDataReader("AM_GET_INTER_MVMT_DETLS_BY_REQID", param)
            If dr.Read Then
                FromLoc = dr.Item("FromLoc")
                ToLoc = dr.Item("ToLoc")
                FromLocCode = dr.Item("FromLocCode")
                ToLocCode = dr.Item("ToLocCode")
                '  receiveAst = dr.Item("MMR_RECVD_BY_name")
                strremarks = dr.Item("MMR_COMMENTS")
            End If
            If dr.IsClosed = False Then
                dr.Close()
            End If
            BindRequestAssets(strReqId)
            txtPersonName.Text = receiveAst
        Else
            Dim dr As SqlDataReader
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strREQ_id
            dr = ObjSubsonic.GetSubSonicDataReader("AM_GET_INTER_MVMT_DETLS_BY_REQID", param)
            If dr.Read Then
                FromLoc = dr.Item("FromLoc")
                ToLoc = dr.Item("ToLoc")
                FromLocCode = dr.Item("FromLocCode")
                ToLocCode = dr.Item("ToLocCode")
                '  receiveAst = dr.Item("MMR_RECVD_BY_name")
                strremarks = dr.Item("MMR_COMMENTS")
                ' receiveAst = dr.Item("MMR_RECVD_BY")
                ReceivingPerson = dr.Item("ReceivingPerson")

            End If
            If dr.IsClosed = False Then
                dr.Close()
            End If
            BindInterRequestAssets(strReqId)
            txtPersonName.Text = ReceivingPerson
        End If
    End Sub



    Private Sub BindDestLoactions()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlDLoc.DataSource = sp.GetDataSet()
            ddlDLoc.DataTextField = "LCM_NAME"
            ddlDLoc.DataValueField = "LCM_CODE"
            ddlDLoc.DataBind()
            ddlDLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlSLoc.DataSource = sp.GetDataSet()
            ddlSLoc.DataTextField = "LCM_NAME"
            ddlSLoc.DataValueField = "LCM_CODE"
            ddlSLoc.DataBind()
            ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Private Sub BindLocations()
        BindLocation()
        BindDestLoactions()
        ddlSLoc.Items.FindByValue(FromLocCode).Selected = True
        ddlDLoc.Items.FindByValue(ToLocCode).Selected = True
    End Sub

    Public Sub BindInterRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "AM_GET_MVMT_ASTS", param)
    End Sub

    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "AM_GET_MVMT_ASTS", param)
    End Sub


    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strASSET_LIST As New ArrayList
        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            'strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text)
        Next

        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_Outwardentrydetails_approve"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)
        If Request.QueryString("stat") = "INTRA" Then

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1019
            ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_OUTWARDSTATUS", param)
            'lblMsg.Text = "Request succesfully approved."
        Else
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1019
            ObjSubsonic.GetSubSonicExecute("AM_UPDATE_INTER_MVMT_OUTWARDSTATUS", param)
        End If
        SendMail(Request.QueryString("Req_id"), "OUTWARDENTRY_APPROVED")
        Response.Redirect("frmAssetThanks.aspx?RID=outwardapp")
    End Sub

    Private Sub SendMail(ByVal ReqId As String, ByVal Mode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_INTER_MVMT_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", Mode, DbType.String)
        sp.Execute()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmOutwardEntry.aspx")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim strASSET_LIST As New ArrayList
        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            'strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text)
        Next

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("Req_id")
        param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Session("uid")
        param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
        param(3).Value = 1020
        ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_OUTWARDSTATUS", param)
        Dim MailTemplateId As Integer
        MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_Outwardentrydetails_Reject"))
        'lblMsg.Text = "Succesfully Updated information for request id " & Request.QueryString("Req_id") & "."
        'lblMsg.Text = "Rejected successfully."
        Response.Redirect("frmAssetThanks.aspx?RID=outwardrej")
    End Sub


End Class
