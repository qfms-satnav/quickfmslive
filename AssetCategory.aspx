﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

   <link href="../../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
<link href="../../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
<link href="../../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
<%--<link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
<link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
<link href="Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />




<!-- Include Bootstrap Select CSS -->
<%--<link href="path/to/bootstrap-select.min.css" rel="stylesheet" />--%>
 <%--<style>
     .grid-align {
         text-align: center;
     }

     a:hover {
         cursor: pointer;
     }

     .ag-header-cell {
         background-color: #1c2b36;
     }
 </style>--%>
 <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
 <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>


 <%--<style>
     .grid-align {
         text-align: center;
     }

     a:hover {
         cursor: pointer;
     }

     .ag-header-cell-filtered {
         background-color: #4682B4;
     }

     .modal-header-primary {
         color: #1D1C1C;
         padding: 9px 15px;
     }

   

     .ag-header-cell-filtered {
         background-color: #4682B4;
     }


     .ag-header-cell-menu-button {
         opacity: 1 !important;
         transition: opacity 0.5s, border 0.2s;
     }
 </style>--%>
</head>
<body data-ng-controller="AssetCategoryController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Asset Category Master</h3>
            </div>
            <div class="card">
                <form id="form1" name="assetcategory" data-valid-submit="Save()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetcategory.$submitted && assetcategory.VT_CODE.$invalid}">
                                <label>Asset Category Code<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                    <input name="VT_CODE" type="text" id="AddAssetCat" class="form-control" data-ng-model="cat.VT_CODE" ng-disabled="isEditMode"  required=" "/>
                                    <span class="error" data-ng-show="assetcategory.$submitted && assetcategory.VT_CODE.$invalid">Asset Category Code is required.</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetcategory.$submitted && assetcategory.VT_TYPE.$invalid}">
                                <label>Asset Category Name<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                                    <input name="VT_TYPE" type="text" id="AddAssetCatName" class="form-control" data-ng-model="cat.VT_TYPE" required=" "/>
                                    <span class="error"  data-ng-show="assetcategory.$submitted && assetcategory.VT_TYPE.$invalid" >Asset Category Name is required.</span>

                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetcategory.$submitted && assetcategory.ASSET_TYPE.$invalid}">
                                <label>Asset Type<span style="color: red;">*</span></label>
                                <select name="ASSET_TYPE" id="ASSET_TYPE" class="form-control" data-ng-model="cat.ASSET_TYPE" required=" ">
                                    <option value="">--Select--</option>
                                    <option data-ng-repeat="type in AssetTypes" value="{{type.Id}}">{{type.Name}}</option>
                                </select>
                                <span class="error" data-ng-show="assetcategory.$submitted && assetcategory.ASSET_TYPE.$invalid">Asset Type is required.</span>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetcategory.$submitted && assetcategory.VT_STATUS.$invalid}">
                                <label>Status<span style="color: red;">*</span></label>
                                <select name="VT_STATUS" id="VT_STATUS"  class="form-control " data-ng-model="cat.VT_STATUS" required=" ">
                                     <option value="">--Select--</option>
                                     <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                </select>
                                <span class="error" data-ng-show="assetcategory.$submitted && assetcategory.VT_STATUS.$invalid">Status is required.</span>
                               
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                
                                <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                    <textarea name="VT_REM" rows="2" cols="20" id="AddAssetCatRemarks" class="form-control" style="height: 30%;" data-ng-model="cat.VT_REM"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="AddAssetCategory1_divdepr" class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Depreciation(%)</label>
                                
                                <input name="VT_DEPR" type="text" id="AddAssetCatDepr" class="form-control" data-ng-model="cat.VT_DEPR" />
                            </div>
                        </div>
                        <div id="AssetCatUpload" class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">

                                <label>Upload Checklist Document</label>

                               


                                <div class="btn-default">
                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                    <input type="file" name="file" id="file" onchange="angular.element(this).scope().fileUpload(this,'VT_UPLOADED_DOC')" multiple="multiple" data-ng-model="cat.VT_UPLOADED_DOC" accept=".png,.jpg,.xlsx,.pdf,.docx" style ="width: 90%;" />

                                </div>
                              
                            </div>
                        </div>
                               

                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                            <div class="form-group">
                               
                                <input type="submit" name="AddAssetCatSubmit" value="{{buttonText}}" class="btn btn-default btn-primary" />
                                <input type="submit" value="Modify" class="btn btn-default btn-primary" data-ng-show="ActionStatus==1" />
                                <a class="btn btn-default btn-primary" href="javascript:history.back()">Back</a>
                               
                            </div>
                        </div>

                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 510px; width: auto;"></div>
                          
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <%--<script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>--%>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <!-- AngularJS and other JS libraries -->
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    <%--var CompanySessionId = '<%= Session["COMPANYID"]%>';--%>
    <%--var UID = '<%= Session["UID"] %>';--%>;

    </script>
    <%--<script src="path/to/bootstrap-select.min.js"></script>--%>


    <script src="../../SMViews/Utility.js"></script>
    <script src="AssetCategory.js"></script>
</body>
</html>
