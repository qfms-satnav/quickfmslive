<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="google-signin-client_id" content="817305281174-8k1t7lombpfpo0gij169s90fcqo6imsq.apps.googleusercontent.com">

    <title>QuickFMS :: Total Infrastructure Control</title>
    <%=ScriptCombiner.GetScriptTags("login_scripts", "css", 1)%>



    <!--[if lt IE 9]>
        <script defer src="BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <style>
        h1 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .form-group {
            margin-bottom: 0.3rem;
        }
    </style>
</head>
<body>
    <div class="login__wrapper">
        <%--<div class="company__logo z-index-tooltip">
            <img src="BootStrapCSS/assets/img/quickfms 1.png" alt="">
        </div>--%>
        <div class="login__content">
            <div class="company__logo z-index-tooltip" style="display: flex; justify-content: center;">
                <img src="images/quickfms.png" style="width: 70%;" alt="">
            </div>

            <div class="login__form">
                <%-- <div class="login__heading mb-1">
                    <h2>Login</h2>
                </div>--%>
                <form id="loginform" class="form-horizontal" role="form" runat="server" defaultfocus="tenantID">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>

                    <asp:HiddenField ID="hidForModel" runat="server" />
                    <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="hidForModel"
                        CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none; color: black">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <h5>You have already Logged in. Do you want to clear that Session?</h5>
                                </div>
                                <div class="modal-footer">
                                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary " OnClick="btnYes_Click" />
                                    <asp:Button ID="btnClose" runat="server" Text="No" CssClass="btn btn-danger" OnClick="btnClose_Click" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSignIn">
                        <div class="row">
                            <div class="col-md-12">
                                <asp:ValidationSummary ID="ValidationSummary2" CssClass="alert alert-danger" runat="server" ValidationGroup="Val1" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Label ID="lbl1" runat="server" CssClass="text-danger"></asp:Label>
                            </div>
                        </div>
                        <div class="login__form-content">
                            <div class="form-group">
                                <label for="tenantID">Company ID</label>
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-icon"><i class="ri-briefcase-line"></i></span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvtnt" runat="server" CssClass="text-danger" ControlToValidate="tenantID"
                                        Display="None" ErrorMessage="Please Enter Company Id" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                    <asp:TextBox ID="tenantID" runat="server" type="text" class="form-control border-left-0" placeholder="Company ID" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="txtUsrId">User Name</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-icon"><i class="ri-user-line"></i></span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvusr" runat="server" ControlToValidate="txtUsrId"
                                        Display="None" ErrorMessage="Please Enter Username" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtUsrId" runat="server" type="text" class="form-control border-left-0" placeholder="User Name" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="txtUsrPwd">Password</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-icon"><i class="ri-lock-line"></i></span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="rfvpwd" runat="server" ControlToValidate="txtUsrPwd"
                                        Display="None" ErrorMessage="Please Enter Password" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtUsrPwd" runat="server" TextMode="Password" MaxLength="16" class="form-control border-left-0" placeholder="Password" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="javascript:void(0)" onclick="showPopWin()" class="text-right">Forgot password?</a>
                            </div>
                            <asp:Button ID="btnSignIn" runat="server" class="btn btn-primary btn-block btn-login-sign-in mt-3" Text="Log In" ValidationGroup="Val1" />
                        </div>
                    </asp:Panel>
                    <!--Start Licence key entering    -->
                    <asp:Panel ID="pnlLic" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val3" />
                        <div class="form-group text-center">
                            <%--  <asp:Label ID="lblSystemKey" runat="server" CssClass="control-label"></asp:Label>--%>
                        </div>
                        <div class="form-group text-center">
                            <b>
                                <asp:Label ID="lblKeyDesc" runat="server" CssClass="control-label" Text="License Code: "></asp:Label></b>
                            <asp:Label ID="lblKey" runat="server" CssClass="control-label" Text="Key"></asp:Label>
                        </div>
                        <div style="margin-bottom: 25px" class="input-group">

                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <asp:TextBox ID="txtSerialKey" runat="server" type="text" class="form-control" placeholder="Enter Activation Code"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSerialKey"
                                Display="None" ErrorMessage="Please Enter Valid Activation Code" ValidationGroup="Val3"> </asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 controls">
                            </div>
                            <div class="col-sm-6 controls">
                                <asp:Button ID="btnNext" runat="server" class="btn login-btn pull-right" Text="Next" ValidationGroup="Val3" />
                            </div>
                        </div>
                    </asp:Panel>
                    <!--End License key entering-->

                    <div class="row" style="margin-top: 60px;">
                        <div style="width:50%" id="gConnect">
                            <div class="g-signin2" data-onsuccess="onSignIn">
                            </div>
                        </div>
                        <div style="width:50%;display: flex; justify-content: right;">
                            <img src="images/iso_gdpr.png" alt="">
                        </div>
                        <%--<div class="col-sm-6" >
                        <a class="btn-microsoft"  onclick="MicrosoftPopup()"><img src="images/ms-symbollockup_signin_light.svg" style=" height: 36px;" /></a>
                    </div>--%>
                    </div>

                    <input type="hidden" runat="server" id="ni" />
                </form>
            </div>
        </div>
    </div>

    <script src="BootStrapCSS/Scripts/jquery.min.js"></script>
    <script defer type="text/javascript">
        $(document).keydown(function (event) {
            if (event.keyCode == 123) {
                return false;
            }
            else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                return false;
            }
        });
        $(document).ready(function () {
            $('#content').height($(window).height());
        });
        if (self != top) {
            window.top.location.href = "login.aspx"
        }
    </script>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 400px;">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title">Forgot Password</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="250" frameborder="0" style="border: none"></iframe>

                </div>
            </div>
        </div>
    </div>
    <%=ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <%--<script src="https://secure.aadcdn.microsoftonline-p.com/lib/1.0.0/js/msal.min.js"></script>--%>
    <script src="https://apis.google.com/js/api.js"></script>
    <%--<script src="https://apis.google.com/js/platform.js" async defer></script>--%>

    <%--<script defer src="MicrosoftAuth.js"></script>--%>
    <script type="text/javascript">
        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript'; po.async = true;
            po.src = 'https://plus.google.com/js/client:plusone.js';
            //po.src = 'BlurScripts/BlurJs/clientplusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();

    </script>
    <script type="text/javascript">
        function showPopWin() {

            $("#modalcontentframe").attr("src", "frmForgetPwd.aspx");
            $("#myModal").modal('show');
            return false;
            //$("#modelcontainer").load("frmForgetPwd.aspx", function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal('show');
            //});
        }



        gapi.load('auth2', function () {
            gapi.auth2.init();
        });
        //function onSignIn(googleUser) {

        //    var profile = googleUser.getBasicProfile();

        //    window.location.href = "GoogleAuth.aspx?email=" + profile.getEmail();
        //}
        function onSignIn(googleUser) {
            console.log(googleUser);
            var profile = googleUser.getBasicProfile();
            var id_token = googleUser.getAuthResponse().id_token;
            console.log(profile);

            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
            var urlString = window.location.href;
            var urlParams = new URLSearchParams(urlString.split('?')[1]);
            var GRedirect = '';
            GRedirect = urlParams.get('g');
            if (GRedirect != "0")
                window.location.href = "GoogleAuth2.aspx?email=" + profile.getEmail() + "&token=" + id_token;
            //window.location.href = "GoogleAuth.aspx?email=" + profile.getEmail();

        }
    </script>

</body>
</html>
