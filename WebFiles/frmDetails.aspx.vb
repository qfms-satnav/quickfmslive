
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO

Partial Class WebFiles_frmDetails
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If

        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[GET_MENU_SCRIPT_BOOTSTRAP_2023]")
            sp.Command.AddParameter("@AURID", Session("uid").ToString(), Data.DbType.String)
            litMenu.Text = sp.ExecuteScalar
        End If

    End Sub
End Class