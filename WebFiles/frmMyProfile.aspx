﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMyProfile.aspx.vb" Inherits="WebFiles_frmMyProfile" %>

<html>
<head>
    <title>My Profile</title>
</head>
<body>
    <div class="card-body">
       <%-- <div class="row mb-3">
            <div class="col-md-12">
                <div class="form-group">
                    <iframe width="600" height="374" src="https://app.powerbi.com/view?r=eyJrIjoiMDdmMzkzN2EtMzhjMC00M2IwLTllM2QtMzU4NzMwNDQ2M2FlIiwidCI6IjA2YWI3Njc5LTU4ZmYtNDAwZi1iOWM1LWRiZTk3OTgzN2ZhZCJ9&pageName=ReportSectionee701454f1ce193b04e8" frameborder="0" allowfullscreen="true"></iframe>
                </div>
            </div>
        </div>--%>
        <form id="form1" runat="server">
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-default" runat="server" id="divspace" visible="false">
                        <div class="card-header d-flex align-items-center">
                            <h6 class="mb-0">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('space')">My Space Booking </a>
                            </h6>
                            <h6 class="mb-0 ml-auto text-info">
                                <i class="fa fa-bell" data-content="<%=popupVal%>" id="spacePopOver" data-container="body" data-placement="right" data-trigger="hover"></i>
                            </h6>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" style="display: block">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>Allocated Space ID:</span>
                                    <asp:HyperLink runat="server" ID="hlDividents"></asp:HyperLink>
                                </div>

                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>Location:</span>
                                    <asp:Label runat="server" ID="lblLocation"></asp:Label>
                                </div>

                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>Floor:</span>
                                    <asp:Label runat="server" ID="lblfloor"></asp:Label>
                                </div>

                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>Vertical:</span>
                                    <asp:Label runat="server" ID="lblvertical"></asp:Label>
                                </div>
                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>Cost Center:</span>
                                    <asp:Label runat="server" ID="lblcostcenter"></asp:Label>
                                </div>
                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>From Date:</span>
                                    <asp:Label runat="server" ID="lblFromDate"></asp:Label>
                                </div>
                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>To Date:</span>
                                    <asp:Label runat="server" ID="lblToDate"></asp:Label>
                                </div>

                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>Shift Timings:</span>
                                    <asp:Label runat="server" ID="lblshifttime"></asp:Label>
                                </div>
                                <div class="d-flex align-items-center justify-content-between flex-wrap">
                                    <span>Extension Number:</span>
                                    <asp:Label runat="server" ID="lblextension"></asp:Label>
                                </div>
                                <div class="d-flex align-items-center justify-content-between flex-wrap" hidden>
                                    <span>Asset Details:</span>
                                    <asp:Label runat="server" ID="lblAsset"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                   <div class="panel panel-default" runat="server" id="propertydiv" visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('property')">My Property Management<i class="fa fa-bell pull-right" id="propertyPopOver" data-container="body" data-placement="right" data-trigger="hover" data-content="<%=propertypopup%>"></i></a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" style="display: none">
                            <div class="panel-body color">
                                <div class="row">
                                    <div class="col-md-7">
                                        Property Types:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblpt"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7">
                                        No of Properties:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblpptscount"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7">
                                        Lease Properties:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblleasecount"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7">
                                        Tenant Properties:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lbltenantcount"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7">
                                        Properties Under Lease & Tenant:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblLseTnt"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default" runat="server" id="assetdiv" visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('asset')">My Asset Management<i class="fa fa-bell pull-right" id="assetPopOver" data-container="body" data-placement="right" data-trigger="hover"
                                    data-content="<%=Assetpopup%>"></i></a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" style="display: none">
                            <div class="panel-body color">
                                <div class="row">
                                    <div class="col-md-7">
                                        No of Assets Mapped (Capital):
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblastcount"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="panel panel-default" runat="server" id="maintenancediv" visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('maintenance')">My Maintenance Management<i class="fa fa-bell pull-right" id="maintenancePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                    data-content="<%=maintenancepopup%>"></i></a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" style="display: none">
                            <div class="panel-body color">
                                <div class="row">
                                    <div class="col-md-5">
                                        Assets under AMC:
                                    </div>
                                    <div class="col-md-7">
                                        <asp:Label runat="server" ID="lblamcassets"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="panel panel-default" runat="server" id="helpdeskdiv" visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('helpdesk')">My Helpdesk Management<i class="fa fa-bell pull-right" id="helpDeskPopOver" data-container="body" data-placement="right" data-trigger="hover"
                                    data-content="<%=HelpDeskpopup%>"></i></a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" style="display: none">
                            <div class="panel-body color">
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests Raised: 
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblreqraised"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests In Progress:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblinprogreq"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests On Hold:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblonhold"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests Closed:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblclosed"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests Rejected:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="lblrejected"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                         <div class="panel panel-default" runat="server" id="gh" visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('helpdesk2')">My Helpdesk Management<i class="fa fa-bell pull-right" id="I1" data-container="body" data-placement="right" data-trigger="hover"
                                    data-content="<%=HelpDeskpopup%>"></i></a>
                            </h4>
                        </div>
                        <div id="Div2" class="panel-collapse collapse" style="display: none">
                            <div class="panel-body color">
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests Raised: 
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label3"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests In progress:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label4"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests On Hold:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label5"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests Closed:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label6"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Requests Rejected:
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label7"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default" runat="server" id="conferencediv" visible="false">
                        <div class="panel-heading">
                         <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('conference')">My Reservation Management<i class="fa fa-bell pull-right" id="conferencePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                    data-content="<%=Conferencepopup%>"></i></a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body color">
                                <div class="row">
                                    <div class="col-md-5">
                                        Reservation Name:
                                    </div>
                                    <div class="col-md-7">
                                        <asp:Label runat="server" ID="lblconfname"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        Location:
                                    </div>
                                    <div class="col-md-7">
                                        <asp:Label runat="server" ID="lblconfloc"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        Floor:
                                    </div>
                                    <div class="col-md-7">
                                        <asp:Label runat="server" ID="lblconsfloor"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        Booked Date:
                                    </div>
                                    <div class="col-md-7">
                                        <asp:Label runat="server" ID="lblbkdate"></asp:Label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-5">
                                        Booked Time:
                                    </div>
                                    <div class="col-md-7">
                                        <asp:Label runat="server" ID="lblbktime"></asp:Label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                  <div class="panel panel-default" runat="server" id="Businessid" visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" onclick="ShowHideModuleDetails('businesscard')">My Business Card Management<i class="fa fa-bell pull-right" id="BusinessPop" data-container="body" data-placement="right" data-trigger="hover"
                                    data-content="<%=BusinessPop%>"></i></a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" style="display: none">
                            <div class="panel-body color">
                                <div class="row">
                                    <div class="col-md-7">
                                        Total No. of Requests
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label1"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        Total Closed Requests
                                    </div>
                                    <div class="col-md-5">
                                        <asp:Label runat="server" ID="Label2"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-7">
                    <div class="profile card bg-white">
                        <div class="card-header">
                            <div class="d-flex align-items-center column-gap-10">
                                <h6 class="mb-0 text-info"><i class="fa fa-user" aria-hidden="true"></i></h6>
                                <h6 class="mb-0">
                                    <a href="javascript:void(0)">
                                        <asp:Label ID="lblempname" runat="server"></asp:Label>
                                    </a>
                                </h6>
                            </div>
                        </div>
                        <div class="card-body" runat="server">
                            <div class="form-group" runat="server">
                                <a href="#">
                                    <asp:Image ID="img" runat="server" ToolTip="Click to change profile picture" ImageUrl="../Userprofiles/default-user-icon-profile.jpg"
                                        Height="120px" Width="120px" AlternateText="" CssClass="rounded-circle mx-auto d-block img-responsive" />
                                    <input type="file" id="fup" name="fileUp" class="d-none" accept="image/*">
                                </a>
                            </div>
                            <ul>
                                <li class="d-flex align-items-center column-gap-10 py-2">
                                    <h6 class="mb-0"><i class="fa fa-credit-card" aria-hidden="true"></i></h6>
                                    <span class="mb-0">User ID:<asp:Label ID="lblemployeeID" runat="server" CssClass="font-weight-bold"></asp:Label></span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2" style="display: none !important">
                                    <h6 class="mb-0"><i class="fa fa-envelope" aria-hidden="true"></i></h6>
                                    <span class="mb-0" id="lblVaccineSta1" runat="server">Vaccine Status:
                                        <asp:Label ID="lblVaccineSta" runat="server" CssClass="font-weight-bold"></asp:Label>
                                    </span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2">
                                    <h6 class="mb-0"><i class="fa fa-envelope" aria-hidden="true"></i></h6>
                                    <span class="mb-0">Email:
                                        <asp:Label ID="lblemail" runat="server" CssClass="font-weight-bold"></asp:Label>
                                    </span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2 d-none" style="display: none !important">
                                    <h6 class="mb-0"><i class="fa fa-envelope" aria-hidden="true"></i></h6>
                                    <span class="mb-0" id="lblClusterl1" runat="server">Cluster:
                                        <asp:Label ID="lblClusterl" runat="server" CssClass="font-weight-bold"></asp:Label>
                                    </span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2" style="display: none !important">
                                    <h6 class="mb-0"><i class="fa fa-envelope" aria-hidden="true"></i></h6>
                                    <span class="mb-0" id="lblGroupl1" runat="server">Group:
                                        <asp:Label ID="lblGroupl" runat="server" CssClass="font-weight-bold"></asp:Label>
                                    </span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2">
                                    <h6 class="mb-0"><i class="fa fa-tags" aria-hidden="true"></i></h6>
                                    <span class="mb-0">Designation: 
                                       <asp:Label ID="lbldesig" runat="server" CssClass="font-weight-bold"></asp:Label>
                                    </span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2">
                                    <h6 class="mb-0"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></h6>
                                    <span class="mb-0" id="lbldept2" runat="server">Department:
                                       <asp:Label ID="lbldept" runat="server" CssClass="font-weight-bold" Visible="false"></asp:Label>
                                    </span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2">
                                    <h6 class="mb-0"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></h6>
                                    <span class="mb-0">Reporting To:
                                       <asp:Label ID="lblreporting" runat="server" CssClass="font-weight-bold"></asp:Label>
                                    </span>
                                </li>
                                <li class="d-flex align-items-center column-gap-10 py-2">
                                    <h6 class="mb-0"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></h6>
                                    <span class="mb-0" id="lblskiplevel2" runat="server">Skip Level Reporting To:
                                       <asp:Label ID="lblskiplevel" runat="server" Visible="false" CssClass="font-weight-bold"></asp:Label>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script defer>
        $(function () {
            //$("#spacePopOver").popover({ title: 'Twitter Bootstrap Popover', content: "It's so simple to create a tooltop for my website!" });
            $("#spacePopOver").popover();
            $("#propertyPopOver").popover();
            $("#assetPopOver").popover();
            $("#conferencePopOver").popover();
            $("#maintenancePopOver").popover();
            $("#helpDeskPopOver").popover();
            $("#BusinessPop").popover();
        });

        $("#img").click(function () {
            var ofd = document.getElementById("fup");
            ofd.click();
            return false;
        });
        $(":file").change(function (e) {
            var fileUpload = $("#fup").get(0);
            var files = fileUpload.files;
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
            }
            $.ajax({
                url: "../api/MyprofileAPI/SaveImagetoDB",
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function (result) { $.notify(result, { color: '#fff', background: '#20D67B', close: true, icon: "check" }); },
                error: function (err) {
                    $.notify('Failed to upload please check again !', { color: "#fff", background: "#D44950", close: true });
                }
            });
            e.preventDefault();
            $("#img").attr("src", "../Userprofiles/" + files[0].name);
        });

        $(document).ready(function () {
            getModulesByUserID();
        });

        function getModulesByUserID() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetModulesByUserID',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {

                    for (var i = 0; i < data.length; i++) {

                        if (data[i].CLS_ID == "108") {
                            $("#assetdiv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "195") {
                            $("#conferencediv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "234") {
                            $("#helpdeskdiv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "218") {
                            $("#maintenancediv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "62" || data[i].CLS_ID == "83") {
                            $("#propertydiv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "248") {
                            //console.log(data[i].CLS_ID);
                            $("#divspace").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "1403") {
                            $("#Businessid").removeAttr("style");
                        }


                    }



                }
            });
        }

        function OpenHelpdesk() {
            alert(1);
        }

        function getHelpdeskDetails() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetDashboardHelpdeskDetails',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    $("#lblreqraised").html(data.Table[0]["REQUESTS RAISED"]);
                    $("#HelpDeskpopup").html(data.Table1[0]["PENDING"]);
                    $("#lblinprogreq").html(data.Table2[0]["INPROGRESS"]);
                    $("#lblclosed").html(data.Table3[0]["CLOSED"]);
                    $("#lblonhold").html(data.Table4[0]["ONHOLD"]);
                    $("#lblrejected").html(data.Table5[0]["REJECTED"]);
                }
            });
        }
        GetEmployeeSpaceDetails();

        function GetEmployeeSpaceDetails() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetEmployeeSpaceDetails',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    if (data.Table == undefined || data.Table.length < 1) {
                        $("#hlDividents").html("No Records Found");
                        return;
                    }
                    $("#lblLocation").html(data.Table[0]["LOCNAME"]);
                    $("#lblfloor").html(data.Table[0]["FLRNAME"]);
                    $("#lblshifttime").html(data.Table[0]["SHITTIME"]);
                    $("#lblvertical").html(data.Table[0]["SSA_VERTICAL"]);
                    var costCenter = data.Table[0]["SSA_COST_CENTER"];
                    $("#lblcostcenter").html((costCenter != undefined || costCenter != null) ? costCenter : "NA");
                    $("#lblextension").html(data.Table[0]["EXTN"]);
                    $("#lblAsset").html(data.Table[0]["AAT_NAME"]);
                    $("#lblFromDate").html(data.Table[0]["SSA_FROM_DT"]);
                    $("#lblToDate").html(data.Table[0]["SSA_TO_DT"]);
                    if (data.Table[0]["SPACEID"] != undefined || data.Table[0]["SPACEID"] != null) {
                        $("#hlDividents").html(data.Table[0]["SPACEID"]);
                        $("#popup_msg").html("Seat Expiry Date: " + data.Table[0]["SSA_VERTICAL"]);
                        $("#hlDividents").attr('href', "/SMViews/Map/Maploader.aspx?lcm_code=" + data.Table1[0]["SPC_BDG_ID"] + "&twr_code=" + data.Table1[0]["SPC_TWR_ID"] + "&flr_code=" + data.Table1[0]["SPC_FLR_ID"] + "&spc_id=" + data.Table1[0]["SPC_ID"] + "&value=1&passVal=Y")
                    }
                    else {
                        $("#hlDividents").html("No Records Found");
                        $("#popup_msg").html("Seat Expiry Date: NA");
                    }
                }
            });
        }

        function GetPropertyDetails() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetPropertyDetails',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    $("#lblpt").html(data.Table.length);
                    $("#lblLseTnt").html(data.Table1[0]["TOTAL"]);
                    $("#lblpptscount").html(data.Table1[3]["TOTAL"]);
                    $("#lblleasecount").html(data.Table1[2]["TOTAL"]);
                    $("#lbltenantcount").html(data.Table1[1]["TOTAL"]);
                    GetExpiryLeases();
                }
            });
        }

        function GetAssetDetails() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetAssetDetails',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    $("#lblastcount").html(data.Table[0]["CNT"]);
                    GetAssetsExpCount();
                }
            });
        }

        function GetTotalClosedReq() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetTotalClosedReq',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    $("#Label2").html(data.Table[0]["CLOSED"] ?? "0");
                    $("#Label1").html(data.Table1[0]["total"]);
                }
            });
        }

        function GetConfBookingDtls() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetConfBookingDtls',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    if (data.Table[0] == undefined || data.Table[0].length < 1) {
                        $("#lblconfname").html("No Records Found");
                    }
                    else {
                        $("#lblconfname").html(data.Table[0]["CONFERENCE_ROOM_NAME"]);
                        $("#lblconfloc").html(data.Table[0]["lcm_name"]);
                        $("#lblconsfloor").html(data.Table[0]["flr_name"]);
                        $("#lblbktime").html(data.Table[0]["SHITTIME"]);
                        $("#lblbkdate").html(data.Table[0]["SSA_FROM_DATE"]);
                    }
                }
            });
        }

        function GetAMCDetails() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetAMCDetails',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    if (data.Table[0] == undefined || data.Table[0].length < 1) {
                        $("#lblamcassets").html("No Records Found");
                    }
                    else {
                        $("#lblamcassets").html(data.Table[0]["TOTAL"]);
                    }
                }
            });
        }

        function GetExpiryLeases() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetExpiryLeases',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    if (data.Table[0] == undefined || data.Table[0].length < 1) {
                        $("#propertyPopOver").attr("data-content", "Leases Expiring this month: 0");
                    }
                    else {
                        $("#propertyPopOver").attr("data-content", "Leases Expiring this month: " + data.Table[0].length);
                    }
                }
            });
        }

        function GetAssetsExpCount() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetAssetsExpCount',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    if (data.Table[0] == undefined || data.Table[0].length < 1) {
                        $("#assetPopOver").attr("data-content", "Assets Around Min. Stock Qty: 0");
                        $("lblreqraised").html("No Records Found");
                    }
                    else {
                        $("#assetPopOver").attr("data-content", "Assets Around Min. Stock Qty: " + data.Table[0]["TOTAL"]);
                    }
                }
            });
        }

        function GetTotalrequests() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetTotalrequests',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {
                    if (data.Table[0] == undefined || data.Table[0].length < 1) {
                        $("#BusinessPop").attr("data-content", "Total No.of Closed Requests: 0");
                        $("lblreqraised").html("No Records Found");
                    }
                    else {
                        $("#BusinessPop").attr("data-content", "Total No.of Closed Requests: " + data.Table[0]["TOTAL"]);
                    }
                }
            });
        }

        function ShowHideModuleDetails(type) {
            debugger;
            switch (type) {
                case "space":
                    if ($('#collapseOne').css('display') == 'none') {
                        GetEmployeeSpaceDetails();
                        $('#collapseOne').show();
                    } else {
                        $('#collapseOne').hide();
                    }
                    break;
                case "asset":
                    if ($('#collapseTwo').css('display') == 'none') {
                        GetAssetDetails();
                        $('#collapseTwo').show();
                    } else {
                        $('#collapseTwo').hide();
                    }
                    break;
                case "property":
                    if ($('#collapseThree').css('display') == 'none') {
                        GetPropertyDetails();
                        $('#collapseThree').show();
                    } else {
                        $('#collapseThree').hide();
                    }
                    break;
                case "helpdesk":
                    if ($('#collapseFour').css('display') == 'none') {
                        getHelpdeskDetails();
                        $('#collapseFour').show();
                    } else {
                        $('#collapseFour').hide();
                    }
                    break;
                case "conference":
                    if ($('#collapseFive').css('display') == 'none') {
                        GetConfBookingDtls();
                        $('#collapseFive').show();
                    } else {
                        $('#collapseFive').hide();
                    }
                    break;
                case "maintenance":
                    if ($('#collapseSix').css('display') == 'none') {
                        GetAMCDetails();
                        $('#collapseSix').show();
                    } else {
                        $('#collapseSix').hide();
                    }
                    break;
                case "businesscard":
                    if ($('#collapseEight').css('display') == 'none') {
                        GetTotalClosedReq();
                        $('#collapseEight').show();
                    } else {
                        $('#collapseEight').hide();
                    }
                    break;
                default:
                    break;
            }

        }

    </script>
</body>
</html>
