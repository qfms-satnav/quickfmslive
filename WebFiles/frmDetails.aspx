<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmDetails.aspx.vb" Inherits="WebFiles_frmDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        iframe {
            height: 86vh !important;
        }

        .sidebar__menu {
            max-height: 90vh;
            min-height: 90vh;
            overflow: scroll;
        }

        .active1 {
            background-color: #209e91;
            border-right: 4px solid #209e91;
            color: #FFFFFF !important;
        }

        .section {
            height: 87vh;
        }

        .sidebar__menu ul a:focus, sidebar__menu ul a:active {
            background-color: var(--bg-info);
            text-decoration: none;
        }

        a:visited {
            color: #209e91;
            background-color: #209e91;
        }

        .ScrollStyle::-webkit-scrollbar {
            width: 12px;
        }

        Track
        .ScrollStyle::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(99,187,178,0.3);
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }

        Handle
        .ScrollStyle::-webkit-scrollbar-thumb {
            -webkit-border-radius: 10px;
            border-radius: 10px;
            background: rgba(32, 158, 145, 1);
            -webkit-box-shadow: inset 0 0 6px rgba(99,187,178,0.5);
        }

        .loader {
            color: black;
            font-family: "Poppins",sans-serif;
            font-weight: 500;
            font-size: 25px;
            -webkit-box-sizing: content-box;
            box-sizing: content-box;
            height: 40px;
            padding: 10px 10px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            border-radius: 8px;
            /*margin-left: 45%;*/
            justify-content: center;
            margin-top: -40px;
        }

        .words {
            overflow: hidden;
        }

        .word {
            display: flex;
            height: 100%;
            padding-left: 6px;
            color: blue;
            animation: spin_4991 4s infinite;
            justify-content: center;
        }

        @keyframes spin_4991 {
            10% {
                -webkit-transform: translateY(-105%);
                transform: translateY(-105%);
            }

            25% {
                -webkit-transform: translateY(-100%);
                transform: translateY(-100%);
            }

            35% {
                -webkit-transform: translateY(-205%);
                transform: translateY(-205%);
            }

            50% {
                -webkit-transform: translateY(-200%);
                transform: translateY(-200%);
            }

            60% {
                -webkit-transform: translateY(-305%);
                transform: translateY(-305%);
            }

            75% {
                -webkit-transform: translateY(-300%);
                transform: translateY(-300%);
            }

            85% {
                -webkit-transform: translateY(-405%);
                transform: translateY(-405%);
            }

            100% {
                -webkit-transform: translateY(-400%);
                transform: translateY(-400%);
            }
        }

        .svgbox {
            --blue: rgb(148, 66, 63);
            stroke: var(--blue);
            stroke-width: 5;
            fill: none;
            stroke-dasharray: 50, 14;
            stroke-dashoffset: 192;
            animation: dash_682 1.4s linear infinite;
        }

        @keyframes dash_682 {
            72.5% {
                opacity: 1;
            }

            to {
                stroke-dashoffset: 1;
            }
        }

        /*.active1 {
            background-color: #209e91;
            border-right: 4px solid #209e91;
            color: #FFFFFF !important;
        }*/

        /*a:visited {
            color: #209e91;
            background-color: #209e91;
        }*/
        a {
            font-size: 1rem;
        }

        #menu li {
            margin-top: 1px;
        }

        .overlay {
            background-color: rgba(255, 255, 255, 0.7);
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 1000;
            top: 0px;
            left: 0px;
            backdrop-filter: blur(3px);
        }

        .loader1 {
            /* margin-left: 44%; */
            /* margin-top: 10%; */
            /* margin-bottom: -4%; */
            display: flex;
            justify-content: center;
            align-items: center;
            margin-top: 17%;
        }

        .copilotShow {
            position: fixed;
            bottom: 10px;
            right: 10px;
            /*width: 400px;*/
            /*height: 60vh;*/
        }
        .aside {
            position: fixed;
            bottom: 7%;
            right: 10px;
            /*width: 400px;*/
        }
    </style>

    <div class="page__wrapper">
        <ba-sidebar id="mysidebar" class="sidebar__menu">
            <div id="loader" class="overlay">
                <div class="loader1 row">
                    <div style="display: flex; justify-content: center;">
                            <svg viewBox="0 0 187.3 93.7" height="200px" width="300px" class="svgbox">
                                <defs>
                                    <linearGradient y2="0%" x2="100%" y1="0%" x1="0%" id="gradient">
                                        <stop stop-color="pink" offset="0%"></stop>

                                        <stop stop-color="blue" offset="100%"></stop>
                                    </linearGradient>
                                </defs>

                                <path stroke="url(#gradient)" d="M93.9,46.4c9.3,9.5,13.8,17.9,23.5,17.9s17.5-7.8,17.5-17.5s-7.8-17.6-17.5-17.5c-9.7,0.1-13.3,7.2-22.1,17.1c-8.9,8.8-15.7,17.9-25.4,17.9s-17.5-7.8-17.5-17.5s7.8-17.5,17.5-17.5S86.2,38.6,93.9,46.4z"></path>
                            </svg>
                        </div>
                        <br />
                        <div class="loader">
                            <%-- <p>loading</p>--%>

                            <div id="AdminFunction" class="words" style="display: none">
                                <span class="word">Loading Screens</span>
                                <span class="word">Loading Master Data</span>
                            </div>
                            <div id="PropertyMgmt" class="words" style="display: none">
                                <span class="word">Loading Screens</span>
                                <span class="word">Fetching Properties </span>
                            </div>

                            <div id="SpaceDiv" class="words" style="display: none">
                                <span class="word">Loading Maps</span>
                                <span class="word">Loading Seats</span>
                                <span class="word">Loading Triggers</span>
                                <span class="word">Fetching Bookings</span>
                            </div>

                            <div id="AssetMgmt" class="words" style="display: none">
                                <span class="word">Loading Assets</span>
                                <span class="word">Fetching Asset Summary</span>
                            </div>

                            <div id="MaintenanceMgmt" class="words" style="display: none">
                                <span class="word">Loading Contracts</span>
                                <span class="word">Fetching Plans</span>
                            </div>

                            <div id="HelpDeskDiv" class="words" style="display: none">
                                <span class="word">Fetching Categories</span>
                                <span class="word">Loading Requests</span>
                                <span class="word">Fetching Escalations & TAT</span>
                            </div>

                            <div id="SharedServices" class="words" style="display: none">
                                <span class="word">Loading Meeting Rooms</span>
                                <span class="word">Fetching Bookings</span>
                            </div>

                            <div id="CheckList" class="words" style="display: none">
                                <span class="word">Fetching Categories</span>
                                <span class="word">Loading Checklist</span>
                            </div>

                            <div id="BudgMgmt" class="words" style="display: none">
                                <span class="word">Fetching Data Utility</span>
                                <span class="word">Fetching Usage Details</span>
                            </div>

                            <div id="ContractMgmt" class="words" style="display: none">
                                <span class="word">Loading Assets</span>
                                <span class="word">Fetching Contracts</span>
                            </div>

                            <div id="DashboardDiv" class="words" style="display: none">
                                <span class="word">Loading Screens</span>
                                <span class="word">Fetching Details</span>
                                <span class="word">Making The Final Setup</span>
                            </div>

                            <div id="Reports" class="words" style="display: none">
                                <span class="word">Loading Data</span>
                                <span class="word">Fetching Summary</span>
                                <span class="word">Fetching Final Data To The Reports</span>

                            </div>

                            <div id="Manuals" class="words" style="display: none">
                                <span class="word">Loading Manuals</span>
                                <span class="word">Converting to PDF's</span>
                            </div>

                        </div>
                </div>
                </div>
            <aside class="al-sidebar">
                <%-- <div id="loader" class="loader">
                        <div class="loader-inner"></div>
                    </div>--%>
                <nav class="sidebar-nav">
                    <ul id="menu" class="sidebar__menu-container">
                        <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                    </ul>
                </nav>
            </aside>
        </ba-sidebar>
        <div class="page__content">
            <div class="container-fluid">
                <div class="section">
                    <iframe id="container" src="../Dashboard/DashboardV2.aspx" allowtransparency="true" style="background: #fff;" src="about:blank" allowfullscreen="true" marginwidth="0" marginheight="0" frameborder="0"></iframe>
                </div>
            </div>


<%--            <a id="main">--%>
                <%--  <div class="al-main">
            <div class="al-content">
                <content-top></content-top>
                <div class="holds-the-iframe">                   
                <iframe id="container" src="../Dashboard/DashboardV2.aspx" width="100%" frameborder="0"></iframe>
                </div>                
            </div>
        </div>--%>

                 <div class="aside">
                <a onclick="coPilotClick()">
                    <img style="height: 50px; border-radius: 10px;" src="../images/copilotLogo.png" /></a>
            </div>

            <div id="copilot" class="copilotShow hide">
                <div style="display: flex; justify-content: right">
                    <span style="margin-right:10px;">
                        <a class=" mb-1" onclick="refreshIframe()" style="height: 20px;"> <img style="height: 20px; border-radius: 3px;" src="../images/refresh.png" /></a>
                    </span>
                    <span>
                        <a class="mb-1" onclick="coPilotClick()">
                            <img style="height: 20px; border-radius: 3px;" src="../images/CloseImg.jpg" /></a>
                    </span>
                </div>
                <iframe id="copilotframe" src="../WebFiles/Copilot.aspx" frameborder="0" style="width: 400px; height: 70vh !important; border-radius: 10px; border:1px solid hsl(218deg 65.87% 23.32%)"></iframe>
            </div>

                <div class="al-footer">
                    <div class="quick-footer pull-left">
                        <a href="http://www.quickfms.com/" target="_blank">www.quickfms.com</a>
                    </div>
                    <div class="quick-footer pull-right">
                        <%Response.Write("&copy;" & Year(Now))%>&nbsp;<%Response.Write("QuickFMS v4.0")%>
                    </div>
                </div>
        </div>

        <%--</a>--%>
        <%--<back-top></back-top>--%>
    </div>
    <script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../Scripts/Intercom/Intercom.js"></script>
    <noscript>Your browser does not support JavaScript!</noscript>
    <script defer type="text/javascript">

        //$(document).ready(function () {
        //    $('.collapse').append('in');
        //});

        function coPilotClick() {
            $('#copilot').toggleClass('hide');
            //$('.aside').toggleClass('show');
        }


        //TO AVOID FADE IN EFFECT IN IE
        var ms_ie = false;
        var ua = window.navigator.userAgent;
        var old_ie = ua.indexOf('MSIE ');
        var new_ie = ua.indexOf('Trident/');
        var mozilla = ua.indexOf('Firefox/');

        if ((old_ie > -1) || (new_ie > -1)) {
            ms_ie = true;
        } else if (mozilla > -1) {
            mozilla = true;
        }

        function ShowHideLoader() {
            document.getElementById('AdminFunction').style.display = "none";
            document.getElementById('PropertyMgmt').style.display = "none";
            document.getElementById('SpaceDiv').style.display = "none";
            document.getElementById('AssetMgmt').style.display = "none";
            document.getElementById('MaintenanceMgmt').style.display = "none";
            document.getElementById('HelpDeskDiv').style.display = "none";
            document.getElementById('SharedServices').style.display = "none";
            document.getElementById('CheckList').style.display = "none";
            document.getElementById('BudgMgmt').style.display = "none";
            document.getElementById('ContractMgmt').style.display = "none";
            document.getElementById('DashboardDiv').style.display = "none";
            document.getElementById('Reports').style.display = "none";
            document.getElementById('Manuals').style.display = "none";
        }

        function setFrameSource(src) {

            //if (src.includes("HelpdeskManagement")) {
            //    document.getElementById('spaceDiv').style.display = "none";
            //    document.getElementById('DashboardDiv').style.display = "none";
            //    document.getElementById('helpDeskDiv').style.display = "block";
            //}
            //else {
            //    document.getElementById('spaceDiv').style.display = "none";
            //    document.getElementById('helpDeskDiv').style.display = "none";
            //    document.getElementById('DashboardDiv').style.display = "block";
            //}
            ShowHideLoader();
            switch (true) {
                case src.includes("AdminFunctions"):
                    document.getElementById('AdminFunction').style.display = "block";
                    break;
                case src.includes("PropertyManagement"):
                    document.getElementById('PropertyMgmt').style.display = "block";
                    break;
                case src.includes("SMViews/SMReports"):
                case src.includes("PropertyManagement/Views"):
                case src.includes("MaintenanceManagement/Reports"):
                case src.includes("HDM/HDM_Webfiles/Reports"):
                case src.includes("DrillDownReports"):
                case src.includes("ConferenceViews/Reports"):
                case src.includes("GHB/Reports"):
                case src.includes("AssetManagement/Reports"):
                case src.includes("BusinessCard/Reports"):
                case src.includes("BranchCheckListManagement/View"):
                case src.includes("EM/Reports"):
                case src.includes("ContractManagement/Masters"):
                    document.getElementById('Reports').style.display = "block";
                    break;
                case src.includes("SMViews"):
                    document.getElementById('SpaceDiv').style.display = "block";
                    break;
                case src.includes("FAM"):
                    document.getElementById('AssetMgmt').style.display = "block";
                    break;
                case src.includes("MaintenanceManagement"):
                    document.getElementById('MaintenanceMgmt').style.display = "block";
                    break;
                case src.includes("HelpdeskManagement"):
                    document.getElementById('HelpDeskDiv').style.display = "block";
                    break;
                case src.includes("WorkSpace"):
                    document.getElementById('SharedServices').style.display = "block";
                    break;
                case src.includes("BranchCheckListManagement"):
                    document.getElementById('CheckList').style.display = "block";
                    break;
                case src.includes("EM/Views"):
                    document.getElementById('BudgMgmt').style.display = "block";
                    break;
                case src.includes("ContractManagement"):
                    document.getElementById('ContractMgmt').style.display = "block";
                    break;
                //case src.includes("Dashboard"):
                //    document.getElementById('DashboardDiv').style.display = "none";
                //    break;
                case src.includes("Reports"):
                    document.getElementById('Reports').style.display = "block";
                    break;
                default:
                    document.getElementById('DashboardDiv').style.display = "block";
                    break;
            }

            var loader = document.getElementById('loader');
            loader.style.display = "block";
            if (!(ms_ie || mozilla)) {
                $("#container").hide();
            }
            $("#container").attr("src", src);
            /*Intercom('update', { "Link": src });*/
            return false;
        }

        //$(document).ready(function () {


        //});

        $(document).ready(function () {
            //var loader = document.getElementById('loader');
            //loader.style.display = "block";
            $("#container").on('load', function (e) {
                $("#container").fadeIn("slow");
                var loader = document.getElementById('loader');
                loader.style.display = "none";

            });
            $(document).on('click', '.sidebar__toggler', function () {
                $('.sidebar__menu').toggleClass('active');
            })
            //$(window).on('blur', function () { $('.dropdown-toggle').parent().removeClass('open'); });            
            $('#container').css('height', $(window).height() - 100 + 'px');
            $('.sidebar').css('height', $(window).height() - 100 + 'px');
            $("#page-wrapper").css('min-height', $(window).height() + 'px');
            var viewportWidth = $(window).width();
            if (viewportWidth < 800) {
                $('main').addClass('menu-collapsed');
                $('#time').css('display', 'none');
                $('.ie8-pngcss').css({ 'height': 'auto', 'max-width': '115px' });
                $('.ie9-pngcss').css({ 'height': 'auto', 'width': '30px' });

            } else {
                $("main").removeClass("menu-collapsed")
                $('#time').css("display", "block");
                $('.ie8-pngcss').css({ "height": "50px", "max-width": "none" });
                $('.ie9-pngcss').css({ "height": "40px", "width": "40px" });

            }
            //if (!ms_ie) {

            //}

            //$('.collapse-menu-link').on('click', function () {
            //    $('main').toggleClass('menu-collapsed');
            //});


            //$(window).resize(function () {
            //    var viewportWidth = $(window).width();
            //    if (viewportWidth < 800) {
            //        $('main').addClass('menu-collapsed');                   
            //        $('#time').css("display", "none");                    
            //        $('.ie8-pngcss').css({ 'height': 'auto', 'max-width': '115px' });
            //        $('.ie9-pngcss').css({ 'height': 'auto', 'width': '30px' });

            //    }
            //    else {
            //        $("main").removeClass("menu-collapsed")                   
            //        $('#time').css("display", "block");
            //        $('.ie8-pngcss').css({ "height": "50px", "max-width": "none" });
            //        $('.ie9-pngcss').css({ "height": "40px", "width": "40px" });                  
            //    }
            //});
        });
        //$('#main').on('click', function () {
        //    $("main").removeClass("menu-collapsed")
        //});

        window.intercomSettings = {
            app_id: "w7eauak2",
            name: '<%=Session("uid")%>', // Full name
            email: '<%=Session("uemail")%>', // Email address
            created_at: '<%=Session("LoginTime")%>',// Signup date as a Unix timestamp
            "Link": 'frmDetails.aspx',
            tenant_id: '<%=Session("TENANT")%>'
        };
        $(document).ready(function () {
            //$('a').click(function () {
            //    $('a').removeClass("active1");
            //    $(this).addClass("active1");
            //});

            //$('ul').click(function () {
            //    $('ul').removeClass("show");
            //    $(this).addClass("show");
            //});
        });
        $(document).ready(function () {
            $('#menu').children('li').on('click', function () {
                $(this).siblings().children().collapse('hide');
            });
            $('#menu').children('li').children('ul').children('li').on('click', function () {
                $(this).siblings().children().collapse('hide');
            });
        });
    </script>
    <%-- <script type="text/javascript">
        var rumMOKey = 'c006b44a81a882a219b0b33dd470ce03';
        (function () {
            if (window.performance && window.performance.timing && window.performance.navigation) {
                var site24x7_rum_beacon = document.createElement('script');
                site24x7_rum_beacon.async = true;
                site24x7_rum_beacon.setAttribute('src', '//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey=' + rumMOKey);
                document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);
            }
        })(window)
    </script>--%>
      <script>
          function close() {
              document.getElementById("mysidebar").style.display = "none";
          }

          function refreshIframe() {
              var iframe = document.getElementById('copilotframe');
              iframe.src = iframe.src;
          }
      </script>
</asp:Content>
