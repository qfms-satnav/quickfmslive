﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO
Partial Class WebFiles_frmMyProfile
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim mode As Integer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If

        If Not Page.IsPostBack Then
            Try

                BindEmpDetails()
                GetTenantModules()
                'BindTotalClosedReq()
                If Session("TENANT") = "[LIBERTY].dbo" Then
                    lblskiplevel.Visible = False
                    lblskiplevel2.Visible = False
                    lbldept.Visible = False
                    lbldept2.Visible = False

                Else
                    lblskiplevel.Visible = True
                    lblskiplevel2.Visible = True
                    lbldept.Visible = True
                    lbldept2.Visible = True
                End If

            Catch ex As Exception
            Finally
            End Try
        End If
    End Sub

    Private Sub GetTenantModules()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Tenant_Module")
        sp.Command.AddParameter("@TID", Session("TENANT"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            Dim exts() As String = {"Space", "Property", "Asset", "Conference", "Maintenance", "Helpdesk", "BusinessCard", "GuestHouse"}
            For Each dr1 As DataRow In ds.Tables(0).Rows
                For i As Integer = 0 To exts.Length - 1
                    If dr1("T_MODULE") = exts(i) Then
                        bindreqmodule(exts(i))
                    End If
                    '    CStr(dr1("T_MODULE")) = exts
                Next
            Next
        End If

    End Sub

    Private Sub bindreqmodule(ByVal req_module As String)
        If req_module = "Space" Then
            divspace.Visible = True
            'BindEmpSpaceDetails()
        ElseIf req_module = "Property" Then
            propertydiv.Visible = True
            'BindPropertyDetails()
            'GetExpiryLeases()
        ElseIf req_module = "Asset" Then
            assetdiv.Visible = True
            'BindAssetMapping()
            'BindAssetsExpCount()
        ElseIf req_module = "Maintenance" Then
            maintenancediv.Visible = True
            'BindAMCDetails()
        ElseIf req_module = "Helpdesk" Then
            helpdeskdiv.Visible = True
            'BindHelpDeskDtls()
        ElseIf req_module = "Conference" Then
            conferencediv.Visible = True
            'BindConfBookingDtls()
        ElseIf req_module = "BusinessCard" Then
            Businessid.Visible = True
            'BindTotalClosedReq()
        End If

    End Sub

    Private Sub BindEmpDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMP_DETAILS_NEW")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As DataSet = sp.GetDataSet()
            lblempname.Text = ds.Tables(0).Rows(0).Item("EMP_NAME")
            lbldesig.Text = ds.Tables(0).Rows(0).Item("EMP_DESIG")
            lbldept.Text = ds.Tables(0).Rows(0).Item("EMP_DEPT")
            lblemail.Text = ds.Tables(0).Rows(0).Item("EMP_EMAIL")
            lblemployeeID.Text = ds.Tables(0).Rows(0).Item("EMP_ID")
            lblreporting.Text = ds.Tables(0).Rows(0).Item("EMP_REPNAME")
            lblskiplevel.Text = ds.Tables(0).Rows(0).Item("SKIPEMP_REPNAME")
            If ds.Tables(0).Rows(0).Item("EMP_IMG") = "" Then
                img.ImageUrl = "../Userprofiles/default-user-icon-profile.jpg"
            Else
                img.ImageUrl = "~/userprofiles/" + ds.Tables(0).Rows(0).Item("EMP_IMG")
            End If
        Catch ex As Exception
        End Try
    End Sub

    'My Space Management
    Private Sub BindEmpSpaceDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USERDETAILS_DASHBOARD")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As DataSet = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                'lblSpaceId.Text = ds.Tables(0).Rows(0).Item("SPACEID")
                lblLocation.Text = ds.Tables(0).Rows(0).Item("LOCNAME")
                lblfloor.Text = ds.Tables(0).Rows(0).Item("FLRNAME")
                lblshifttime.Text = ds.Tables(0).Rows(0).Item("SHITTIME")
                lblvertical.Text = ds.Tables(0).Rows(0).Item("SSA_VERTICAL")
                lblcostcenter.Text = IIf(ds.Tables(0).Rows(0).Item("SSA_COST_CENTER").ToString() <> Nothing, ds.Tables(0).Rows(0).Item("SSA_COST_CENTER").ToString, "NA")
                lblextension.Text = ds.Tables(0).Rows(0).Item("EXTN")
                lblAsset.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
                lblFromDate.Text = ds.Tables(0).Rows(0).Item("SSA_FROM_DT")
                lblToDate.Text = ds.Tables(0).Rows(0).Item("SSA_TO_DT")
                popup_msg = "Seat Expiry Date: " + ds.Tables(0).Rows(0).Item("SSA_TO_DT")
                hlDividents.Text = ds.Tables(0).Rows(0).Item("SPACEID")
                hlDividents.NavigateUrl = "~/SMViews/Map/Maploader.aspx?lcm_code=" + ds.Tables(1).Rows(0).Item("SPC_BDG_ID") + "&twr_code=" + ds.Tables(1).Rows(0).Item("SPC_TWR_ID") + "&flr_code=" + ds.Tables(1).Rows(0).Item("SPC_FLR_ID") + "&spc_id=" + ds.Tables(1).Rows(0).Item("SPC_ID") + "&value=1&passVal=Y"
            Else
                hlDividents.Text = "No Records Found"
                popup_msg = "Seat Expiry Date: NA"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'My Property Management
    Private Sub BindPropertyDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETPROPERTY_DTLS_DASHBOARD")
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
            Dim ds As DataSet = sp.GetDataSet()
            Dim sum As Integer
            If ds.Tables(0).Rows.Count > 0 Then
                lblpt.Text = ds.Tables(0).Rows.Count
                'For Each dr1 As DataRow In ds.Tables(0).Rows
                '    sum = sum + CInt(dr1("TOTAL"))
                'Next
                lblLseTnt.Text = ds.Tables(1).Rows(0).Item(0)
                lblpptscount.Text = ds.Tables(1).Rows(3).Item(0)
                lblleasecount.Text = ds.Tables(1).Rows(2).Item(0)
                lbltenantcount.Text = ds.Tables(1).Rows(1).Item(0)
            Else
                lblpt.Text = "No Records Found"
            End If
        Catch ex As Exception

        End Try
    End Sub

    'My Asset Management
    Private Sub BindAssetMapping()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_EMPLOYEEASSETMAPPED_DB")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANY", Session("CompanyId"), DbType.String)
            Dim ds As DataSet = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblastcount.Text = ds.Tables(0).Rows(0).Item(0)
            Else
                lblastcount.Text = "No Records Found"
            End If
        Catch ex As Exception

        End Try
    End Sub
    'My Business Card
    Private Sub BindTotalClosedReq()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BC_TOTAL_CLOSEDREQ")
            Dim ds As DataSet = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                Label2.Text = ds.Tables(0).Rows(0)("closed").ToString()
            Else
                Label2.Text = "No Records Found"
            End If
            If ds.Tables(1).Rows.Count > 0 Then
                Label1.Text = ds.Tables(1).Rows(0)("total").ToString()
                BusinessPop = "Total No.of Requests:" + ds.Tables(1).Rows(0)("total").ToString()
            Else
                Label1.Text = "No Records Found"
            End If
        Catch ex As Exception

        End Try
    End Sub

    'My Conference Management
    Private Sub BindConfBookingDtls()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMP_CONFBKD_DASHBOARD")
            sp.Command.AddParameter("@USER_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
            Dim ds As DataSet = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblconfname.Text = ds.Tables(0).Rows(0).Item("CONFERENCE_ROOM_NAME")
                lblconfloc.Text = ds.Tables(0).Rows(0).Item("lcm_name")
                lblconsfloor.Text = ds.Tables(0).Rows(0).Item("flr_name")
                lblbktime.Text = ds.Tables(0).Rows(0).Item("SHITTIME")
                lblbkdate.Text = ds.Tables(0).Rows(0).Item("SSA_FROM_DATE")
            Else
                lblconfname.Text = "No Records Found"
            End If

            'COUNT OF CONF BOOKED IN ADVANCE FROM TODAY
            If ds.Tables(1).Rows.Count > 0 Then
                Conferencepopup = "No of Conferences Booked/Withheld In Advance From Today: " + Convert.ToString(ds.Tables(1).Rows(0).Item("CONFBOOKED"))
            Else
                Conferencepopup = "No of Conferences Booked/Withheld In Advance From Today: NA"
            End If

        Catch ex As Exception

        End Try
    End Sub

    'My Maintenance Management
    Private Sub BindAMCDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GETAMCTYPES_DASHBOARD")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            Dim ds As DataSet = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblamcassets.Text = ds.Tables(0).Rows(1).Item("TOTAL")
                maintenancepopup = "Assets Expiring This Month: " + Convert.ToString(ds.Tables(1).Rows(0).Item("TOTAL"))
            Else
                lblamcassets.Text = "No Records Found"
                maintenancepopup = "Assets Expiring This Month: NA"
            End If
        Catch ex As Exception

        End Try
    End Sub

    'My HelpDesk Management
    Private Sub BindHelpDeskDtls()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_HELPDESK_STS_DASHBOARD")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
            Dim ds As DataSet = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblreqraised.Text = ds.Tables(0).Rows(0).Item("Requests Raised")
                'lblpendingreq.Text = ds.Tables(1).Rows(0).Item("PENDING")
                HelpDeskpopup = "Total Pending Requests:" + Convert.ToString(ds.Tables(1).Rows(0).Item("PENDING"))
                lblinprogreq.Text = ds.Tables(2).Rows(0).Item("INPROGRESS")
                lblclosed.Text = ds.Tables(3).Rows(0).Item("CLOSED")
                lblonhold.Text = ds.Tables(4).Rows(0).Item("ONHOLD")
                lblrejected.Text = ds.Tables(5).Rows(0).Item("REJECTED")
            Else
                lblreqraised.Text = "No Records Found"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetExpiryLeases()
        Dim ds As DataTable = New DataTable()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_Expiry_Leases_Dashboard")
            sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString(), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            ds = sp.GetDataSet().Tables(0)
            If ds.Rows.Count > 0 Then
                propertypopup = "Leases Expiring this month: " + Convert.ToString(ds.Rows(0).Item(0))
            Else
                propertypopup = "Leases Expiring this month: 0"
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindAssetsExpCount()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETS_REQUIRED_STOCK_DASHBOARD")
            Dim ds As DataSet = sp.GetDataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                Assetpopup = "Assets Around Min. Stock Qty: " + Convert.ToString(ds.Tables(0).Rows(0).Item("TOTAL"))
            Else
                lblreqraised.Text = "No Records Found"
                Assetpopup = "Assets Around Min. Stock Qty: NA"
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindTotalrequests()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BC_TOTAL_REQUESTS")
            Dim ds As DataSet = sp.GetDataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                BusinessPop = "Total No.of Closed Requests: " + Convert.ToString(ds.Tables(0).Rows(0).Item("TOTAL"))
            Else
                lblreqraised.Text = "No Records Found"
                BusinessPop = "Total No.of Closed Requests: NA"
            End If
        Catch ex As Exception
        End Try
    End Sub

    'Space hover binding property
    Private popup_msg As String
    Public Property popupVal() As String
        Get
            Return popup_msg
        End Get
        Set(ByVal value As String)
            popup_msg = value
        End Set
    End Property

    'maintenance hover binding property
    Private maintenance_msg As String
    Public Property maintenancepopup() As String
        Get
            Return maintenance_msg
        End Get
        Set(ByVal value As String)
            maintenance_msg = value
        End Set
    End Property

    'Property hover binding property
    Private property_msg As String
    Public Property propertypopup() As String
        Get
            Return property_msg
        End Get
        Set(ByVal value As String)
            property_msg = value
        End Set
    End Property

    'Asset hover binding property
    Private Asset_msg As String
    Public Property Assetpopup() As String
        Get
            Return Asset_msg
        End Get
        Set(ByVal value As String)
            Asset_msg = value
        End Set
    End Property
    'Business Card
    Private Business_msg As String
    Public Property BusinessPop() As String
        Get
            Return Business_msg
        End Get
        Set(ByVal value As String)
            Business_msg = value
        End Set
    End Property
    'Conference hover binding property
    Private Conference_msg As String
    Public Property Conferencepopup() As String
        Get
            Return Conference_msg
        End Get
        Set(ByVal value As String)
            Conference_msg = value
        End Set
    End Property

    'HelpDesk hover binding property
    Private HelpDesk_msg As String
    Public Property HelpDeskpopup() As String
        Get
            Return HelpDesk_msg
        End Get
        Set(ByVal value As String)
            HelpDesk_msg = value
        End Set
    End Property

End Class

