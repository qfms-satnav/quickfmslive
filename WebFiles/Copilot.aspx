﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Copilot.aspx.cs" Inherits="WebFiles_Copilot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Contoso Sample Web Chat</title>
    <!--
       This styling is for the Web Chat demonstration purposes.
       It is recommended that style is moved to a separate file for organization in larger projects.

       Please visit https://github.com/microsoft/BotFramework-WebChat for details about Web Chat.
     -->
    <link href="../BootStrapCSS/assets/css/Bootstrap-V4/bootstrap.min.css" rel="stylesheet" />
    <style>
        html,
        body {
            height: 100%;
            background-color: white;
            color: #3f8acb !important;
        }

        body {
            margin: 0;
        }

        .react-film__main {
            overflow: scroll !important;
            height: 30vh;
        }
        /*  .react-film__main{
            display:none;
        }*/
        .react-film__filmstrip__list {
            display: inline !important;
        }

        h1 {
            color: whitesmoke;
            font-family: 'Open Sans', sans-serif;
            font-size: 1rem;
            line-height: 20px;
            margin: 0;
            padding: 0 20px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 0.8rem;
            line-height: 1rem;
        }

        ul {
            margin-bottom: 2px !important;
        }

        #banner {
            align-items: center;
            background-color: hsl(218deg 65.87% 23.32%);
            display: flex;
            height: 50px;
        }

        #webchat {
            height: calc(100% - 50px);
            overflow: hidden;
            position: fixed;
            top: 50px;
            width: 100%;
        }

        ::-webkit-scrollbar {
            width: 10px;
            height: 8px;
            border-radius: 5px;
        }

        ::-webkit-scrollbar-thumb {
            background: rgb(188 193 200);
            cursor: pointer;
            border-radius: 5px;
        }

        ::-webkit-scrollbar-track {
            background: 0 0;
        }

        .webchat__upload-button {
            display: none !important;
        }
        /* .react-film__filmstrip__item{
            height:100px;
            width:50%
        }
        .webchat__suggested-actions__button{
             height:100px;
        }
        .webchat__suggested-actions__button-text{
            text-wrap:balance;
        }*/
        .webchat__suggested-action {
            border-color: rgb(150 184 217) !important;
            border-radius: 10px !important;
            border-width: 1px !important;
            background-color: rgb(150 184 217);
            color: white;
            font-size: 0.8rem;
        }

        .menuIcon {
            position: fixed;
            bottom: 41px;
            right: 12px;
            z-index: 1001;
        }

        .suggested_actions_button:hover {
            background-color: #f7f5f5;
        }

        .suggested_actions_button1:hover {
            background-color: #f7f5f5;
        }

        .suggested_actions_button {
            border: none;
            border-radius: 10px !important;
            border-width: 1px !important;
            margin-bottom: 2px;
            background-color: rgb(231 243 255);
            font-size: 0.9rem;
        }

        .suggested_actions_button1 {
            border: none;
            border-radius: 10px !important;
            border-width: 1px !important;
            margin-bottom: 2px;
            background-color: rgb(231 243 255);
            font-size: 0.9rem;
        }

        .cpmenu {
            padding: 0px 2rem;
        }

        .webchat__bubble__content {
            border-color: rgb(150 184 217) !important;
            border-radius: 10px !important;
        }

        /*.suggested_actions_button1 {
            display: none;
        }*/

        .suggested_actions {
            font-size: 0.8rem;
            display:flex;
            justify-content:center;
        }

        .suggested_actions1 {
            display:flex;
            font-size: 0.8rem;
            justify-content:center;
        }
    </style>
</head>
<body>
    <div>
        <div id="banner">
            <h1>QuickFMS Copilot</h1>
        </div>
        <div id="webchat" role="main">
        </div>
        <div class="menuIcon">
            <button class="btn btn-secondary" onclick="ShowMenu()">Menu</button>
        </div>
    </div>

    <!--
       In this sample, the latest version of Web Chat is being used.
       In production environment, the version number should be pinned and version bump should be done frequently.

       Please visit https://github.com/microsoft/BotFramework-WebChat/tree/main/CHANGELOG.md for changelog.
     -->
    <script crossorigin="anonymous" src="https://cdn.botframework.com/botframework-webchat/latest/webchat.js"></script>
    <script>
        (async function () {

            const styleOptions = {
                accent: '#00809d',
                botAvatarBackgroundColor: '#FFFFFF',
                botAvatarImage: "../images/copilotLogo.png",
                botAvatarInitials: 'BT',
                userAvatarImage: "../images/copilotLogo.png"
            };
            // Specifies style options to customize the Web Chat canvas.
            // Please visit https://microsoft.github.io/BotFramework-WebChat for customization samples.
            //const styleOptions = {
            //    // Hide upload button.
            //    hideUploadButton: true
            //};

            // Specifies the token endpoint URL.
            // To get this value, visit Copilot Studio > Settings > Channels > Mobile app page.
            const tokenEndpointURL = new URL('https://default06ab767958ff400fb9c5dbe979837f.ad.environment.api.powerplatform.com/powervirtualagents/botsbyschema/cr414_quickFmsCopilotLive/directline/token?api-version=2022-03-01-preview');

            // Specifies the language the copilot and Web Chat should display in:
            // - (Recommended) To match the page language, set it to document.documentElement.lang
            // - To use current user language, set it to navigator.language with a fallback language
            // - To use another language, set it to supported Unicode locale

            // Setting page language is highly recommended.
            // When page language is set, browsers will use native font for the respective language.

            const locale = document.documentElement.lang || 'en'; // Uses language specified in <html> element and fallback to English (United States).
            // const locale = navigator.language || 'ja-JP'; // Uses user preferred language and fallback to Japanese.
            // const locale = 'zh-HAnt'; // Always use Chinese (Traditional).

            const apiVersion = tokenEndpointURL.searchParams.get('api-version');

            const [directLineURL, token] = await Promise.all([
                fetch(new URL(`/powervirtualagents/regionalchannelsettings?api-version=${apiVersion}`, tokenEndpointURL))
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Failed to retrieve regional channel settings.');
                        }
                        return response.json();
                    })
                    .then(({ channelUrlsById: { directline } }) => directline),
                fetch(tokenEndpointURL)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error('Failed to retrieve Direct Line token.');
                        }
                        return response.json();
                    })
                    .then(({ token }) => token)
            ]);

            // The "token" variable is the credentials for accessing the current conversation.
            // To maintain conversation across page navigation, save and reuse the token.

            // The token could have access to sensitive information about the user.
            // It must be treated like user password.

            const directLine = WebChat.createDirectLine({ domain: new URL('v3/directline', directLineURL), token });

            // Sends "startConversation" event when the connection is established.

            const subscription = directLine.connectionStatus$.subscribe({
                next(value) {
                    if (value === 2) {
                        directLine
                            .postActivity({
                                localTimezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                                locale,
                                name: 'startConversation',
                                type: 'event',
                                value: {
                                    uid: '<%=Session["UID"]%>',
                                    company: '<%= Session["CPTENANT"]%>',
                                    url_live:'https://live.quickfms.com'
                                }
                            })
                            .subscribe();
                        //$('.webchat__basic-transcript__activity-body').append(item);
                        // Only send the event once, unsubscribe after the event is sent.
                        subscription.unsubscribe();
                    }
                }
            });

            WebChat.renderWebChat({ directLine, locale, styleOptions }, document.getElementById('webchat'));

            window.menuClick = function (message) {
                directLine.postActivity({
                    //from: { id: 'user1' }, // Replace 'user1' with the actual user ID
                    type: 'message',
                    text: message
                }).subscribe();
                $('.menuIcon').show();
            }

        })();
    </script>
    <script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../BootStrapCSS/assets/js/Bootstrap-V4/bootstrap.js"></script>
    <script defer type="text/javascript">
        var space = '<%=Session["space"]%>';
        var conference = '<%=Session["conference"]%>';
        var businesscard = '<%=Session["businesscard"]%>';
        var GuestHouse = '<%=Session["GuestHouse"]%>';
        var Energy = '<%=Session["Energy"]%>';
        var maintenance = '<%=Session["maintenance"]%>';
        var property = '<%=Session["property"]%>';
        var helpdesk = '<%=Session["helpdesk"]%>';
        var assets = '<%=Session["Assets"]%>';
        var checklist = '<%=Session["Branchchecklist"]%>';
        var companyid = '<%= Session["TENANT"]%>';
        var content = ``
        var contentEx = ``
        var Fitem = ``
        var cnt = 0
        function MenuRefresh() {
            cnt = 0
            var Fitem = ``
            content = ``
            contentEx = ``
            if (space == 1) {
                //$('.space').show();
                if (cnt < 2) {
                    content += eSpace;
                }
                else {
                    contentEx += eSpace;
                }
                cnt++;
            }
            //else
            //    $('.space').hide();
            if (checklist == 1) {
                //$('.checklist').show();
                if (cnt < 2) {
                    content += eChecklist;
                }
                else {
                    contentEx += eChecklist;
                }
                cnt++;
            }
            //else
            //    $('.checklist').hide();
            if (helpdesk == 1) {
                //$('.HelpDesk').show();
                if (cnt < 2) {
                    content += eHelpDesk;
                }
                else {
                    contentEx += eHelpDesk;
                }
                cnt++;
            }
            //else
            //    $('.HelpDesk').hide();
            if (assets == 1) {
                //$('.Asset').show();
                if (cnt < 2) {
                    content += eAsset;
                }
                else {
                    contentEx += eAsset;
                }
                cnt++;
            }
            if (maintenance == 1) {
                //$('.Asset').show();
                if (cnt < 2) {
                    content += eMaintenance;
                }
                else {
                    contentEx += eMaintenance;
                }
                cnt++;
            }
            //else
            //    $('.Asset').hide();
            if (GuestHouse == 1) {
                //$('.Reservation').show();
                if (cnt < 2) {
                    content += eGuestHouse;
                }
                else {
                    contentEx += eGuestHouse;
                }
                cnt++;
            }
            if (conference == 1) {
                //$('.Reservation').show();
                if (cnt < 2) {
                    content += eReservation;
                }
                else {
                    contentEx += eReservation;
                }
                cnt++;
            }
            if (property == 1) {
                //$('.Reservation').show();
                if (cnt < 2) {
                    content += eProperty;
                }
                else {
                    contentEx += eProperty;
                }
                cnt++;
            }
            //else
            //    $('.Reservation').hide();
           

        }

        function ShowMenu() {
            MenuRefresh();
            if (cnt > 2) {
                Fitem = itemF + content + Button + `</div>`
            }
            else {
                Fitem = itemF + content + `</div>`
            }
            $('.webchat__basic-transcript__transcript').append(Fitem);
            //$('.webchat__basic-transcript__activity-body').append(Fitem);
            //$('.suggested_actions_button1').hide();
            //$('.suggested_actions1').hide();
            //$('.showButton').show();
            $('.menuIcon').hide();
        }
        function ShowMenu1() {
            MenuRefresh();
            //Fitem = itemF + content + contentEx + `</div>`
            $('.webchat__basic-transcript__transcript').append(`<div class="cpmenu" role="toolbar">` + contentEx + `</div>`);
            //$('.suggested_actions_button1').show();
            //$('.suggested_actions1').show();
            $('.showButton').hide();
        }
        var itemF = `<div class="cpmenu" role="toolbar">`
        var eSpace = `
        <div class="row mb-1 space">
        <div class="col-md-12 suggested_actions">Space</div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Quick Seat Booking')"                          type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Quick seat booking</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Book Any Available Seat in XXX Floor')"        type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Book any available seat in XXX floor</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Please release the current seat')"                                type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Release Seat</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Please map xxx location to me')"                                type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Please map xxx location to me</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Download Space Report')"                                type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Download Space Report</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`
        var eHelpDesk = `
        <div class="row mt-2 mb-1 HelpDesk">
        <div class="col-md-12 suggested_actions">Help Desk</div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Raise a Request')"                            type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Raise Request</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Who is the incharge of ticket numer xxxxxx')"                            type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Who is the incharge of ticket numer xxxxxx</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('What is the current status of ticket xxxxx and what is the SLA')"                            type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">What is the current status of ticket xxxxx and what is the SLA</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Download consolidated report')"                            type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Download consolidated report</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('What is the total number of open tickets?')"                            type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">What is the total number of open tickets?</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`
        var eChecklist = `
        <div class="row mt-2 mb-1 Checklist">
        <div class="col-md-12 suggested_actions">Branch Checklist</div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('List of Tickets for Validations')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">List of tickets for validations</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('List of Tickets for Approval')"                type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">List of tickets for Approval</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Show me the submitted vs not submitted checklist branches')"                type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Show me the submitted vs not submitted checklist branches</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Last one month not ok cases')"                type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Last one month not ok cases</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`
        var eAsset = `
        <div class="row mt-2 mb-1 Asset">
        <div class="col-md-12 suggested_actions1">Asset</div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Total Value of All Assets Currently Managed')" type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Total value of all assets currently managed</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('POs Generated Today')"                         type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">PO's generated today</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Low Stock Assets')"                            type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Low stock assets</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Pending Asset Requests for Approval')"         type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Pending Asset requests for Approval</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`
        var eMaintenance = `
        <div class="row mt-2 mb-1 Maintenance">
        <div class="col-md-12 suggested_actions1">Maintenance</div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Upcoming Scheduled Maintenance Activities')"   type="button" tabindex= "0"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Upcoming scheduled maintenance activities</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('AMCs Expired this Month or Upcoming')"        type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">AMC's expired this month or upcoming</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Current Month Maintenance Cost')"              type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Current month maintenance cost</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Todays PPM Activites')"                       type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Today's PPM activites</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`
        var eReservation = `
        <div class="row mt-2 mb-1 Reservation">
        <div class="col-md-12 suggested_actions1">Reservation</div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Book Meeting Room')"                           type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Book meeting room</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Cancel My Active Bookings Today')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Cancel my active bookings today</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Meeting Rooms scheduled for today')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Meeting Rooms scheduled for today</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Edit my active booking')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Edit my active booking</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Show me available meeting rooms')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Available meeting rooms from xyz am/pm to abc am/pm</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Which rooms are currently available for booking?')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Available meeting rooms</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`
        var eGuestHouse = `
        <div class="row mt-2 mb-1 GuestHouse">
        <div class="col-md-12 suggested_actions1">GuestHouse</div><div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Available Guest House for Today')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Available guest house for today</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`
        var eProperty = `
        <div class="row mt-2 mb-1 Property">
        <div class="col-md-12 suggested_actions1">Property</div>
        <div class="col-auto"><button class="suggested_actions_button" onclick="menuClick('Future Property Renewals')"             type="button" tabindex="-1"><span class="webchat__suggested-action__text webchat__suggested-actions__button-text">Future Property Renewals</span><div class="webchat__suggested-action__keyboard-focus-indicator"></div></button></div>
        </div>`

        var Button =
            `<div class="row showButton"><div class="col-md-12" style="display:flex;justify-content:center;"><button class="suggested_actions_button" onclick="ShowMenu1()">... Show more</button></div>`


        //var Fitem = itemF + content + `</div>`
    </script>
</body>
</html>
