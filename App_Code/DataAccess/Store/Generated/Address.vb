Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the Address class.
    ''' </summary>

    <Serializable()> _
    Partial Public Class AddressCollection
        Inherits ActiveList(Of Address)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As AddressCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As AddressCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As AddressCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As AddressCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As AddressCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As AddressCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As AddressCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As AddressCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Store_Address")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub

    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the CSK_Store_Address table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class Address
        Inherits ActiveRecord(Of Address)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Store_Address")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As Address = New Address()
            Return Address.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Store_Address")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

        Public Sub New(ByVal columnName As String, ByVal columnValue As Object)
            SetSQLProps()
            MyBase.LoadByParam(columnName, columnValue)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("AddressID")> _
        Public Property AddressID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("AddressID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("AddressID", Value)
            End Set
        End Property
        <XmlAttribute("UserName")> _
        Public Property UserName() As String
            Get
                Dim result As Object = Me.GetColumnValue("UserName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("UserName", Value)
            End Set
        End Property
        <XmlAttribute("FirstName")> _
        Public Property FirstName() As String
            Get
                Dim result As Object = Me.GetColumnValue("FirstName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("FirstName", Value)
            End Set
        End Property
        <XmlAttribute("LastName")> _
        Public Property LastName() As String
            Get
                Dim result As Object = Me.GetColumnValue("LastName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("LastName", Value)
            End Set
        End Property
        <XmlAttribute("Phone")> _
        Public Property Phone() As String
            Get
                Dim result As Object = Me.GetColumnValue("Phone")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Phone", Value)
            End Set
        End Property
        <XmlAttribute("Email")> _
        Public Property Email() As String
            Get
                Dim result As Object = Me.GetColumnValue("Email")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Email", Value)
            End Set
        End Property
        <XmlAttribute("Address1")> _
        Public Property Address1() As String
            Get
                Dim result As Object = Me.GetColumnValue("Address1")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Address1", Value)
            End Set
        End Property
        <XmlAttribute("Address2")> _
        Public Property Address2() As String
            Get
                Dim result As Object = Me.GetColumnValue("Address2")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Address2", Value)
            End Set
        End Property
        <XmlAttribute("City")> _
        Public Property City() As String
            Get
                Dim result As Object = Me.GetColumnValue("City")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("City", Value)
            End Set
        End Property
        <XmlAttribute("StateOrRegion")> _
        Public Property StateOrRegion() As String
            Get
                Dim result As Object = Me.GetColumnValue("StateOrRegion")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("StateOrRegion", Value)
            End Set
        End Property
        <XmlAttribute("Zip")> _
        Public Property Zip() As String
            Get
                Dim result As Object = Me.GetColumnValue("Zip")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Zip", Value)
            End Set
        End Property
        <XmlAttribute("Country")> _
        Public Property Country() As String
            Get
                Dim result As Object = Me.GetColumnValue("Country")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Country", Value)
            End Set
        End Property
        <XmlAttribute("CreatedOn")> _
        Public Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedOn", Value)
            End Set
        End Property
        <XmlAttribute("CreatedBy")> _
        Public Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedBy", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedOn", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedBy", Value)
            End Set
        End Property

#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal userName As String, ByVal firstName As String, ByVal lastName As String, ByVal phone As String, ByVal email As String, ByVal address1 As String, ByVal address2 As String, ByVal city As String, ByVal stateOrRegion As String, ByVal zip As String, ByVal country As String)
            Dim item As Address = New Address()
            item.UserName = userName
            item.FirstName = firstName
            item.LastName = lastName
            item.Phone = phone
            item.Email = email
            item.Address1 = address1
            item.Address2 = address2
            item.City = city
            item.StateOrRegion = stateOrRegion
            item.Zip = zip
            item.Country = country

            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal addressID As Integer, ByVal userName As String, ByVal firstName As String, ByVal lastName As String, ByVal phone As String, ByVal email As String, ByVal address1 As String, ByVal address2 As String, ByVal city As String, ByVal stateOrRegion As String, ByVal zip As String, ByVal country As String)
            Dim item As Address = New Address()
            item.AddressID = addressID
            item.UserName = userName
            item.FirstName = firstName
            item.LastName = lastName
            item.Phone = phone
            item.Email = email
            item.Address1 = address1
            item.Address2 = address2
            item.City = city
            item.StateOrRegion = stateOrRegion
            item.Zip = zip
            item.Country = country

            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared AddressID As String
            Public Shared UserName As String
            Public Shared FirstName As String
            Public Shared LastName As String
            Public Shared Phone As String
            Public Shared Email As String
            Public Shared Address1 As String
            Public Shared Address2 As String
            Public Shared City As String
            Public Shared StateOrRegion As String
            Public Shared Zip As String
            Public Shared Country As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                AddressID = "addressID"
                UserName = "userName"
                FirstName = "firstName"
                LastName = "lastName"
                Phone = "phone"
                Email = "email"
                Address1 = "address1"
                Address2 = "address2"
                City = "city"
                StateOrRegion = "stateOrRegion"
                Zip = "zip"
                Country = "country"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
            End Sub
        End Structure
#End Region

    End Class
End Namespace
