Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the OrderItem class.
    ''' </summary>

    <Serializable()> _
    Partial Public Class OrderItemCollection
        Inherits ActiveList(Of OrderItem)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As OrderItemCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As OrderItemCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As OrderItemCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As OrderItemCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As OrderItemCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As OrderItemCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As OrderItemCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As OrderItemCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Store_OrderItem")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub

    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the CSK_Store_OrderItem table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class OrderItem
        Inherits ActiveRecord(Of OrderItem)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Store_OrderItem")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As OrderItem = New OrderItem()
            Return OrderItem.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Store_OrderItem")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

        Public Sub New(ByVal columnName As String, ByVal columnValue As Object)
            SetSQLProps()
            MyBase.LoadByParam(columnName, columnValue)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("OrderItemID")> _
        Public Property OrderItemID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("OrderItemID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("OrderItemID", Value)
            End Set
        End Property
        <XmlAttribute("OrderID")> _
        Public Property OrderID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("OrderID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("OrderID", Value)
            End Set
        End Property
        <XmlAttribute("ProductID")> _
        Public Property ProductID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ProductID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ProductID", Value)
            End Set
        End Property
        <XmlAttribute("Sku")> _
        Public Property Sku() As String
            Get
                Dim result As Object = Me.GetColumnValue("Sku")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Sku", Value)
            End Set
        End Property
        <XmlAttribute("ProductName")> _
        Public Property ProductName() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductName", Value)
            End Set
        End Property
        <XmlAttribute("ImageFile")> _
        Public Property ImageFile() As String
            Get
                Dim result As Object = Me.GetColumnValue("ImageFile")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ImageFile", Value)
            End Set
        End Property
        <XmlAttribute("ProductDescription")> _
        Public Property ProductDescription() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductDescription")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductDescription", Value)
            End Set
        End Property
        <XmlAttribute("PromoCode")> _
        Public Property PromoCode() As String
            Get
                Dim result As Object = Me.GetColumnValue("PromoCode")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("PromoCode", Value)
            End Set
        End Property
        <XmlAttribute("Weight")> _
        Public Property Weight() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Weight")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Weight", Value)
            End Set
        End Property

        <XmlAttribute("Length")> _
        Public Property Length() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Length")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Length", Value)
            End Set
        End Property
        <XmlAttribute("Height")> _
        Public Property Height() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Height")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Height", Value)
            End Set
        End Property
        <XmlAttribute("Width")> _
        Public Property Width() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Width")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Width", Value)
            End Set
        End Property
        <XmlAttribute("DimensionUnit")> _
        Public Property DimensionUnit() As String
            Get
                Dim result As Object = Me.GetColumnValue("DimensionUnit")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("DimensionUnit", Value)
            End Set
        End Property

        <XmlAttribute("Dimensions")> _
        Public Property Dimensions() As String
            Get
                Dim result As Object = Me.GetColumnValue("Dimensions")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Dimensions", Value)
            End Set
        End Property
        <XmlAttribute("Quantity")> _
        Public Property Quantity() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("Quantity")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("Quantity", Value)
            End Set
        End Property
        <XmlAttribute("OriginalPrice")> _
        Public Property OriginalPrice() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("OriginalPrice")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("OriginalPrice", Value)
            End Set
        End Property
        <XmlAttribute("PricePaid")> _
        Public Property PricePaid() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("PricePaid")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("PricePaid", Value)
            End Set
        End Property
        <XmlAttribute("Attributes")> _
        Public Property Attributes() As String
            Get
                Dim result As Object = Me.GetColumnValue("Attributes")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Attributes", Value)
            End Set
        End Property
        <XmlAttribute("DownloadURL")> _
        Public Property DownloadURL() As String
            Get
                Dim result As Object = Me.GetColumnValue("DownloadURL")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("DownloadURL", Value)
            End Set
        End Property
        <XmlAttribute("IsShipped")> _
        Public Property IsShipped() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsShipped")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
            Set(ByVal value As Boolean)
                Me.MarkDirty()
                Me.SetColumnValue("IsShipped", Value)
            End Set
        End Property
        <XmlAttribute("ShipDate")> _
        Public Property ShipDate() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ShipDate")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ShipDate", Value)
            End Set
        End Property
        <XmlAttribute("ShippingEstimate")> _
        Public Property ShippingEstimate() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShippingEstimate")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShippingEstimate", Value)
            End Set
        End Property
        <XmlAttribute("ShipmentReference")> _
        Public Property ShipmentReference() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipmentReference")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipmentReference", Value)
            End Set
        End Property
        <XmlAttribute("Rating")> _
        Public Property Rating() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Rating")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Rating", Value)
            End Set
        End Property
        <XmlAttribute("CreatedOn")> _
        Public Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedOn", Value)
            End Set
        End Property
        <XmlAttribute("CreatedBy")> _
        Public Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedBy", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedOn", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedBy", Value)
            End Set
        End Property
        <XmlAttribute("IsLastAdded")> _
        Public Property IsLastAdded() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsLastAdded")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
            Set(ByVal value As Boolean)
                Me.MarkDirty()
                Me.SetColumnValue("IsLastAdded", Value)
            End Set
        End Property
        <XmlAttribute("DiscountAmt")> _
                Public Property DiscountAmt() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("DiscountAmt")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("DiscountAmt", value)
            End Set
        End Property
#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal orderID As Integer, ByVal productID As Integer, ByVal sku As String, ByVal productName As String, ByVal imageFile As String, ByVal productDescription As String, ByVal promoCode As String, ByVal weight As Decimal, ByVal dimensions As String, ByVal quantity As Integer, ByVal originalPrice As Decimal, ByVal pricePaid As Decimal, ByVal attributes As String, ByVal downloadURL As String, ByVal isShipped As Boolean, ByVal shipDate As DateTime, ByVal shippingEstimate As String, ByVal shipmentReference As String, ByVal rating As Decimal, ByVal isLastAdded As Boolean)
            Dim item As OrderItem = New OrderItem()
            item.OrderID = orderID
            item.ProductID = productID
            item.Sku = sku
            item.ProductName = productName
            item.ImageFile = imageFile
            item.ProductDescription = productDescription
            item.PromoCode = promoCode
            item.Weight = weight
            item.Dimensions = dimensions
            item.Quantity = quantity
            item.OriginalPrice = originalPrice
            item.PricePaid = pricePaid
            item.Attributes = attributes
            item.DownloadURL = downloadURL
            item.IsShipped = isShipped
            item.ShipDate = shipDate
            item.ShippingEstimate = shippingEstimate
            item.ShipmentReference = shipmentReference
            item.Rating = rating
            item.IsLastAdded = isLastAdded

            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal orderItemID As Integer, ByVal orderID As Integer, ByVal productID As Integer, ByVal sku As String, ByVal productName As String, ByVal imageFile As String, ByVal productDescription As String, ByVal promoCode As String, ByVal weight As Decimal, ByVal dimensions As String, ByVal quantity As Integer, ByVal originalPrice As Decimal, ByVal pricePaid As Decimal, ByVal attributes As String, ByVal downloadURL As String, ByVal isShipped As Boolean, ByVal shipDate As DateTime, ByVal shippingEstimate As String, ByVal shipmentReference As String, ByVal rating As Decimal, ByVal isLastAdded As Boolean)
            Dim item As OrderItem = New OrderItem()
            item.OrderItemID = orderItemID
            item.OrderID = orderID
            item.ProductID = productID
            item.Sku = sku
            item.ProductName = productName
            item.ImageFile = imageFile
            item.ProductDescription = productDescription
            item.PromoCode = promoCode
            item.Weight = weight
            item.Dimensions = dimensions
            item.Quantity = quantity
            item.OriginalPrice = originalPrice
            item.PricePaid = pricePaid
            item.Attributes = attributes
            item.DownloadURL = downloadURL
            item.IsShipped = isShipped
            item.ShipDate = shipDate
            item.ShippingEstimate = shippingEstimate
            item.ShipmentReference = shipmentReference
            item.Rating = rating
            item.IsLastAdded = isLastAdded

            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared OrderItemID As String
            Public Shared OrderID As String
            Public Shared ProductID As String
            Public Shared Sku As String
            Public Shared ProductName As String
            Public Shared ImageFile As String
            Public Shared ProductDescription As String
            Public Shared PromoCode As String
            Public Shared Weight As String
            Public Shared Dimensions As String
            Public Shared Quantity As String
            Public Shared OriginalPrice As String
            Public Shared PricePaid As String
            Public Shared Attributes As String
            Public Shared DownloadURL As String
            Public Shared IsShipped As String
            Public Shared ShipDate As String
            Public Shared ShippingEstimate As String
            Public Shared ShipmentReference As String
            Public Shared Rating As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            Public Shared IsLastAdded As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                OrderItemID = "orderItemID"
                OrderID = "orderID"
                ProductID = "productID"
                Sku = "sku"
                ProductName = "productName"
                ImageFile = "imageFile"
                ProductDescription = "productDescription"
                PromoCode = "promoCode"
                Weight = "weight"
                Dimensions = "dimensions"
                Quantity = "quantity"
                OriginalPrice = "originalPrice"
                PricePaid = "pricePaid"
                Attributes = "attributes"
                DownloadURL = "downloadURL"
                IsShipped = "isShipped"
                ShipDate = "shipDate"
                ShippingEstimate = "shippingEstimate"
                ShipmentReference = "shipmentReference"
                Rating = "rating"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
                IsLastAdded = "isLastAdded"
            End Sub
        End Structure
#End Region

    End Class
End Namespace
