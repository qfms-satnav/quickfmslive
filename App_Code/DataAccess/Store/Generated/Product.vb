Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the Product class.
    ''' </summary>

    <Serializable()> _
    Partial Public Class ProductCollection
        Inherits ActiveList(Of Product)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As ProductCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As ProductCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As ProductCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As ProductCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As ProductCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As ProductCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As ProductCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As ProductCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Store_Product")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub

    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the CSK_Store_Product table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class Product
        Inherits ActiveRecord(Of Product)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Store_Product")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As Product = New Product()
            Return Product.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Store_Product")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

        Public Sub New(ByVal columnName As String, ByVal columnValue As Object)
            SetSQLProps()
            MyBase.LoadByParam(columnName, columnValue)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("ProductID")> _
        Public Property ProductID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ProductID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ProductID", Value)
            End Set
        End Property
        <XmlAttribute("Sku")> _
        Public Property Sku() As String
            Get
                Dim result As Object = Me.GetColumnValue("Sku")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Sku", Value)
            End Set
        End Property
        <XmlAttribute("ProductGUID")> _
        Public Property ProductGUID() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductGUID")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductGUID", Value)
            End Set
        End Property
        <XmlAttribute("ProductName")> _
        Public Property ProductName() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductName", Value)
            End Set
        End Property
        <XmlAttribute("ShortDescription")> _
        Public Property ShortDescription() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShortDescription")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShortDescription", Value)
            End Set
        End Property
        <XmlAttribute("ManufacturerID")> _
        Public Property ManufacturerID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ManufacturerID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ManufacturerID", Value)
            End Set
        End Property
        <XmlAttribute("AttributeXML")> _
        Public Property AttributeXML() As String
            Get
                Dim result As Object = Me.GetColumnValue("AttributeXML")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("AttributeXML", Value)
            End Set
        End Property
        <XmlAttribute("StatusID")> _
        Public Property StatusID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("StatusID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("StatusID", Value)
            End Set
        End Property
        <XmlAttribute("ProductTypeID")> _
        Public Property ProductTypeID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ProductTypeID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ProductTypeID", Value)
            End Set
        End Property
        <XmlAttribute("ShippingTypeID")> _
        Public Property ShippingTypeID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ShippingTypeID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ShippingTypeID", Value)
            End Set
        End Property
        <XmlAttribute("ShipEstimateID")> _
        Public Property ShipEstimateID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ShipEstimateID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ShipEstimateID", Value)
            End Set
        End Property
        <XmlAttribute("TaxTypeID")> _
        Public Property TaxTypeID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("TaxTypeID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("TaxTypeID", Value)
            End Set
        End Property
        <XmlAttribute("StockLocation")> _
        Public Property StockLocation() As String
            Get
                Dim result As Object = Me.GetColumnValue("StockLocation")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("StockLocation", Value)
            End Set
        End Property
        <XmlAttribute("OurPrice")> _
        Public Property OurPrice() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("OurPrice")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("OurPrice", Value)
            End Set
        End Property
        <XmlAttribute("RetailPrice")> _
        Public Property RetailPrice() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("RetailPrice")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("RetailPrice", Value)
            End Set
        End Property
        <XmlAttribute("Weight")> _
        Public Property Weight() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Weight")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Weight", Value)
            End Set
        End Property
        <XmlAttribute("CurrencyCode")> _
        Public Property CurrencyCode() As String
            Get
                Dim result As Object = Me.GetColumnValue("CurrencyCode")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CurrencyCode", Value)
            End Set
        End Property
        <XmlAttribute("UnitOfMeasure")> _
        Public Property UnitOfMeasure() As String
            Get
                Dim result As Object = Me.GetColumnValue("UnitOfMeasure")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("UnitOfMeasure", Value)
            End Set
        End Property
        <XmlAttribute("AdminComments")> _
        Public Property AdminComments() As String
            Get
                Dim result As Object = Me.GetColumnValue("AdminComments")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("AdminComments", Value)
            End Set
        End Property
        <XmlAttribute("Length")> _
        Public Property Length() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Length")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Length", Value)
            End Set
        End Property
        <XmlAttribute("Height")> _
        Public Property Height() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Height")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Height", Value)
            End Set
        End Property
        <XmlAttribute("Width")> _
        Public Property Width() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("Width")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("Width", Value)
            End Set
        End Property
        <XmlAttribute("DimensionUnit")> _
        Public Property DimensionUnit() As String
            Get
                Dim result As Object = Me.GetColumnValue("DimensionUnit")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("DimensionUnit", Value)
            End Set
        End Property
        <XmlAttribute("IsDeleted")> _
        Public Property IsDeleted() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsDeleted")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
            Set(ByVal value As Boolean)
                Me.MarkDirty()
                Me.SetColumnValue("IsDeleted", Value)
            End Set
        End Property
        <XmlAttribute("ListOrder")> _
        Public Property ListOrder() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ListOrder")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ListOrder", Value)
            End Set
        End Property
        <XmlAttribute("RatingSum")> _
        Public Property RatingSum() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("RatingSum")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("RatingSum", Value)
            End Set
        End Property
        <XmlAttribute("TotalRatingVotes")> _
        Public Property TotalRatingVotes() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("TotalRatingVotes")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("TotalRatingVotes", Value)
            End Set
        End Property
        <XmlAttribute("DefaultImage")> _
        Public Property DefaultImage() As String
            Get
                Dim result As Object = Me.GetColumnValue("DefaultImage")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("DefaultImage", Value)
            End Set
        End Property
        <XmlAttribute("CreatedOn")> _
        Public Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedOn", Value)
            End Set
        End Property
        <XmlAttribute("CreatedBy")> _
        Public Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedBy", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedOn", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedBy", Value)
            End Set
        End Property
        <XmlAttribute("thumbimage")> _
        Public Property thumbimage() As String
            Get
                Dim result As Object = Me.GetColumnValue("thumbimage")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("thumbimage", value)
            End Set
        End Property
        <XmlAttribute("bigimage")> _
        Public Property bigimage() As String
            Get
                Dim result As Object = Me.GetColumnValue("bigimage")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("bigimage", value)
            End Set
        End Property

        <XmlAttribute("ProductPageTitle")> _
       Public Property ProductPageTitle() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductPageTitle")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductPageTitle", value)
            End Set
        End Property

        <XmlAttribute("ProductPageKeywords")> _
        Public Property ProductPageKeywords() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductPageKeywords")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductPageKeywords", value)
            End Set
        End Property

        <XmlAttribute("ProductPageMetaDescription")> _
       Public Property ProductPageMetaDescription() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductPageMetaDescription")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductPageMetaDescription", value)
            End Set
        End Property
        <XmlAttribute("OldPrice")> _
                Public Property OldPrice() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("OldPrice")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("OldPrice", value)
            End Set
        End Property

        <XmlAttribute("ServiceCharge")> _
        Public Property ServiceCharge() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("ServiceCharge")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("ServiceCharge", value)
            End Set
        End Property
        <XmlAttribute("ServiceTaxinPercentage")> _
       Public Property ServiceTaxinPercentage() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("ServiceTaxinPercentage")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("ServiceTaxinPercentage", value)
            End Set
        End Property

#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal sku As String, ByVal productGUID As String, ByVal productName As String, ByVal shortDescription As String, ByVal manufacturerID As Integer, ByVal attributeXML As String, ByVal statusID As Integer, ByVal productTypeID As Integer, ByVal shippingTypeID As Integer, ByVal shipEstimateID As Integer, ByVal taxTypeID As Integer, ByVal stockLocation As String, ByVal ourPrice As Decimal, ByVal retailPrice As Decimal, ByVal weight As Decimal, ByVal currencyCode As String, ByVal unitOfMeasure As String, ByVal adminComments As String, ByVal length As Decimal, ByVal height As Decimal, ByVal width As Decimal, ByVal dimensionUnit As String, ByVal isDeleted As Boolean, ByVal listOrder As Integer, ByVal ratingSum As Integer, ByVal totalRatingVotes As Integer, ByVal defaultImage As String, Optional ByVal thumbImage As String = "", Optional ByVal bigImage As String = "")
            Dim item As Product = New Product()
            item.Sku = sku
            item.ProductGUID = productGUID
            item.ProductName = productName
            item.ShortDescription = shortDescription
            item.ManufacturerID = manufacturerID
            item.AttributeXML = attributeXML
            item.StatusID = statusID
            item.ProductTypeID = productTypeID
            item.ShippingTypeID = shippingTypeID
            item.ShipEstimateID = shipEstimateID
            item.TaxTypeID = taxTypeID
            item.StockLocation = stockLocation
            item.OurPrice = ourPrice
            item.RetailPrice = retailPrice
            item.Weight = weight
            item.CurrencyCode = currencyCode
            item.UnitOfMeasure = unitOfMeasure
            item.AdminComments = adminComments
            item.Length = length
            item.Height = height
            item.Width = width
            item.DimensionUnit = dimensionUnit
            item.IsDeleted = isDeleted
            item.ListOrder = listOrder
            item.RatingSum = ratingSum
            item.TotalRatingVotes = totalRatingVotes
            item.DefaultImage = defaultImage
            item.thumbimage = thumbImage
            item.bigimage = bigImage
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal productID As Integer, ByVal sku As String, ByVal productGUID As String, ByVal productName As String, ByVal shortDescription As String, ByVal manufacturerID As Integer, ByVal attributeXML As String, ByVal statusID As Integer, ByVal productTypeID As Integer, ByVal shippingTypeID As Integer, ByVal shipEstimateID As Integer, ByVal taxTypeID As Integer, ByVal stockLocation As String, ByVal ourPrice As Decimal, ByVal retailPrice As Decimal, ByVal weight As Decimal, ByVal currencyCode As String, ByVal unitOfMeasure As String, ByVal adminComments As String, ByVal length As Decimal, ByVal height As Decimal, ByVal width As Decimal, ByVal dimensionUnit As String, ByVal isDeleted As Boolean, ByVal listOrder As Integer, ByVal ratingSum As Integer, ByVal totalRatingVotes As Integer, ByVal defaultImage As String, Optional ByVal thumbimage As String = "", Optional ByVal bigimage As String = "")
            Dim item As Product = New Product()
            item.ProductID = productID
            item.Sku = sku
            item.ProductGUID = productGUID
            item.ProductName = productName
            item.ShortDescription = shortDescription
            item.ManufacturerID = manufacturerID
            item.AttributeXML = attributeXML
            item.StatusID = statusID
            item.ProductTypeID = productTypeID
            item.ShippingTypeID = shippingTypeID
            item.ShipEstimateID = shipEstimateID
            item.TaxTypeID = taxTypeID
            item.StockLocation = stockLocation
            item.OurPrice = ourPrice
            item.RetailPrice = retailPrice
            item.Weight = weight
            item.CurrencyCode = currencyCode
            item.UnitOfMeasure = unitOfMeasure
            item.AdminComments = adminComments
            item.Length = length
            item.Height = height
            item.Width = width
            item.DimensionUnit = dimensionUnit
            item.IsDeleted = isDeleted
            item.ListOrder = listOrder
            item.RatingSum = ratingSum
            item.TotalRatingVotes = totalRatingVotes
            item.DefaultImage = defaultImage
            item.thumbimage = thumbimage
            item.bigimage = bigimage
            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared ProductID As String
            Public Shared Sku As String
            Public Shared ProductGUID As String
            Public Shared ProductName As String
            Public Shared ShortDescription As String
            Public Shared ManufacturerID As String
            Public Shared AttributeXML As String
            Public Shared StatusID As String
            Public Shared ProductTypeID As String
            Public Shared ShippingTypeID As String
            Public Shared ShipEstimateID As String
            Public Shared TaxTypeID As String
            Public Shared StockLocation As String
            Public Shared OurPrice As String
            Public Shared RetailPrice As String
            Public Shared Weight As String
            Public Shared CurrencyCode As String
            Public Shared UnitOfMeasure As String
            Public Shared AdminComments As String
            Public Shared Length As String
            Public Shared Height As String
            Public Shared Width As String
            Public Shared DimensionUnit As String
            Public Shared IsDeleted As String
            Public Shared ListOrder As String
            Public Shared RatingSum As String
            Public Shared TotalRatingVotes As String
            Public Shared DefaultImage As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            Public Shared thumbimage As String
            Public Shared bigimage As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                ProductID = "productID"
                Sku = "sku"
                ProductGUID = "productGUID"
                ProductName = "productName"
                ShortDescription = "shortDescription"
                ManufacturerID = "manufacturerID"
                AttributeXML = "attributeXML"
                StatusID = "statusID"
                ProductTypeID = "productTypeID"
                ShippingTypeID = "shippingTypeID"
                ShipEstimateID = "shipEstimateID"
                TaxTypeID = "taxTypeID"
                StockLocation = "stockLocation"
                OurPrice = "ourPrice"
                RetailPrice = "retailPrice"
                Weight = "weight"
                CurrencyCode = "currencyCode"
                UnitOfMeasure = "unitOfMeasure"
                AdminComments = "adminComments"
                Length = "length"
                Height = "height"
                Width = "width"
                DimensionUnit = "dimensionUnit"
                IsDeleted = "isDeleted"
                ListOrder = "listOrder"
                RatingSum = "ratingSum"
                TotalRatingVotes = "totalRatingVotes"
                DefaultImage = "defaultImage"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
                thumbimage = "thumbimage"
                bigimage = "bigimage"
            End Sub
        End Structure
#End Region

    End Class
End Namespace
