Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the Order class.
    ''' </summary>

    <Serializable()> _
    Partial Public Class OrderCollection
        Inherits ActiveList(Of Order)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As OrderCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As OrderCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As OrderCollection
            Return Me
        End Function
        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As OrderCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As OrderCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As OrderCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As OrderCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As OrderCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Store_Order")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()
        End Sub
    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the CSK_Store_Order table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class Order
        Inherits ActiveRecord(Of Order)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Store_Order")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As Order = New Order()
            Return Order.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Store_Order")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

        Public Sub New(ByVal columnName As String, ByVal columnValue As Object)
            SetSQLProps()
            MyBase.LoadByParam(columnName, columnValue)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("OrderID")> _
        Public Property OrderID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("OrderID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("OrderID", Value)
            End Set
        End Property
        <XmlAttribute("OrderGUID")> _
        Public Property OrderGUID() As String
            Get
                Dim result As Object = Me.GetColumnValue("OrderGUID")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("OrderGUID", Value)
            End Set
        End Property
        <XmlAttribute("OrderNumber")> _
        Public Property OrderNumber() As String
            Get
                Dim result As Object = Me.GetColumnValue("OrderNumber")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("OrderNumber", Value)
            End Set
        End Property
        <XmlAttribute("OrderDate")> _
        Public Property OrderDate() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("OrderDate")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("OrderDate", Value)
            End Set
        End Property
        <XmlAttribute("OrderStatusID")> _
        Public Property OrderStatusID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("OrderStatusID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("OrderStatusID", Value)
            End Set
        End Property
        <XmlAttribute("UserName")> _
        Public Property UserName() As String
            Get
                Dim result As Object = Me.GetColumnValue("UserName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("UserName", Value)
            End Set
        End Property
        <XmlAttribute("Email")> _
        Public Property Email() As String
            Get
                Dim result As Object = Me.GetColumnValue("Email")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Email", Value)
            End Set
        End Property
        <XmlAttribute("ShipEmail")> _
        Public Property ShipEmail() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipEmail")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipEmail", value)
            End Set
        End Property
        <XmlAttribute("City")> _
        Public Property City() As String
            Get
                Dim result As Object = Me.GetColumnValue("City")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("City", value)
            End Set
        End Property
        <XmlAttribute("State")> _
        Public Property State() As String
            Get
                Dim result As Object = Me.GetColumnValue("State")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("State", value)
            End Set
        End Property
        <XmlAttribute("Country")> _
        Public Property Country() As String
            Get
                Dim result As Object = Me.GetColumnValue("Country")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Country", value)
            End Set
        End Property
        <XmlAttribute("Zip")> _
        Public Property Zip() As String
            Get
                Dim result As Object = Me.GetColumnValue("Zip")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Zip", value)
            End Set
        End Property
        <XmlAttribute("ShipCity")> _
        Public Property ShipCity() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipCity")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipCity", value)
            End Set
        End Property
        <XmlAttribute("ShipState")> _
        Public Property ShipState() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipState")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipState", value)
            End Set
        End Property
        <XmlAttribute("ShipCountry")> _
        Public Property ShipCountry() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipCountry")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipCountry", value)
            End Set
        End Property
        <XmlAttribute("ShipZip")> _
        Public Property ShipZip() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipZip")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipZip", value)
            End Set
        End Property
        <XmlAttribute("Phone")> _
        Public Property Phone() As String
            Get
                Dim result As Object = Me.GetColumnValue("Phone")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Phone", value)
            End Set
        End Property
        <XmlAttribute("FirstName")> _
        Public Property FirstName() As String
            Get
                Dim result As Object = Me.GetColumnValue("FirstName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("FirstName", Value)
            End Set
        End Property
        <XmlAttribute("LastName")> _
        Public Property LastName() As String
            Get
                Dim result As Object = Me.GetColumnValue("LastName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("LastName", Value)
            End Set
        End Property
        <XmlAttribute("ShipFirstName")> _
        Public Property ShipFirstName() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipFirstName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipFirstName", value)
            End Set
        End Property
        <XmlAttribute("ShipLastName")> _
        Public Property ShipLastName() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipLastName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipLastName", value)
            End Set
        End Property
        <XmlAttribute("Address1")> _
        Public Property Address1() As String
            Get
                Dim result As Object = Me.GetColumnValue("Address1")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Address1", value)
            End Set
        End Property
        <XmlAttribute("Address2")> _
        Public Property Address2() As String
            Get
                Dim result As Object = Me.GetColumnValue("Address2")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Address2", value)
            End Set
        End Property
        <XmlAttribute("ShipAddress1")> _
        Public Property ShipAddress1() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipAddress1")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipAddress1", value)
            End Set
        End Property
        <XmlAttribute("ShipAddress2")> _
        Public Property ShipAddress2() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipAddress2")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipAddress2", value)
            End Set
        End Property
        <XmlAttribute("ShipPhone")> _
        Public Property ShipPhone() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipPhone")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipPhone", Value)
            End Set
        End Property
        <XmlAttribute("ShippingMethod")> _
        Public Property ShippingMethod() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShippingMethod")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShippingMethod", Value)
            End Set
        End Property
        <XmlAttribute("SubTotalAmount")> _
        Public Property SubTotalAmount() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("SubTotalAmount")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("SubTotalAmount", Value)
            End Set
        End Property
        <XmlAttribute("ShippingAmount")> _
        Public Property ShippingAmount() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("ShippingAmount")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("ShippingAmount", Value)
            End Set
        End Property
        <XmlAttribute("HandlingAmount")> _
        Public Property HandlingAmount() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("HandlingAmount")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("HandlingAmount", Value)
            End Set
        End Property
        <XmlAttribute("TaxAmount")> _
        Public Property TaxAmount() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("TaxAmount")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("TaxAmount", Value)
            End Set
        End Property
        <XmlAttribute("TaxRate")> _
        Public Property TaxRate() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("TaxRate")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("TaxRate", Value)
            End Set
        End Property
        <XmlAttribute("CouponCodes")> _
        Public Property CouponCodes() As String
            Get
                Dim result As Object = Me.GetColumnValue("CouponCodes")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CouponCodes", Value)
            End Set
        End Property
        <XmlAttribute("DiscountAmount")> _
        Public Property DiscountAmount() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("DiscountAmount")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut
            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("DiscountAmount", Value)
            End Set
        End Property
        <XmlAttribute("SpecialInstructions")> _
        Public Property SpecialInstructions() As String
            Get
                Dim result As Object = Me.GetColumnValue("SpecialInstructions")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("SpecialInstructions", Value)
            End Set
        End Property
        <XmlAttribute("ShipToAddress")> _
        Public Property ShipToAddress() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShipToAddress")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShipToAddress", Value)
            End Set
        End Property
        <XmlAttribute("BillToAddress")> _
        Public Property BillToAddress() As String
            Get
                Dim result As Object = Me.GetColumnValue("BillToAddress")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("BillToAddress", Value)
            End Set
        End Property
        <XmlAttribute("UserIP")> _
        Public Property UserIP() As String
            Get
                Dim result As Object = Me.GetColumnValue("UserIP")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("UserIP", Value)
            End Set
        End Property
        <XmlAttribute("ShippingTrackingNumber")> _
        Public Property ShippingTrackingNumber() As String
            Get
                Dim result As Object = Me.GetColumnValue("ShippingTrackingNumber")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ShippingTrackingNumber", Value)
            End Set
        End Property
        <XmlAttribute("NumberOfPackages")> _
        Public Property NumberOfPackages() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("NumberOfPackages")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("NumberOfPackages", Value)
            End Set
        End Property
        <XmlAttribute("PackagingNotes")> _
        Public Property PackagingNotes() As String
            Get
                Dim result As Object = Me.GetColumnValue("PackagingNotes")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("PackagingNotes", Value)
            End Set
        End Property
        <XmlAttribute("CreatedOn")> _
        Public Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedOn", Value)
            End Set
        End Property
        <XmlAttribute("CreatedBy")> _
        Public Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedBy", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedOn", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedBy", value)
            End Set
        End Property
        <XmlAttribute("ServiceChargeAmount")> _
         Public Property ServiceChargeAmount() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("ServiceChargeAmount")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut
            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("ServiceChargeAmount", value)
            End Set
        End Property
        <XmlAttribute("ServiceTaxAmount")> _
                 Public Property ServiceTaxAmount() As Decimal
            Get
                Dim result As Object = Me.GetColumnValue("ServiceTaxAmount")
                Dim oOut As Decimal = 0
                Try
                    oOut = Decimal.Parse(result.ToString())
                Catch
                End Try
                Return oOut
            End Get
            Set(ByVal value As Decimal)
                Me.MarkDirty()
                Me.SetColumnValue("ServiceTaxAmount", value)
            End Set
        End Property
#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal orderGUID As String, ByVal orderNumber As String, ByVal orderDate As DateTime, ByVal orderStatusID As Integer, ByVal userName As String, ByVal email As String, ByVal firstName As String, ByVal lastName As String, ByVal shipPhone As String, ByVal shippingMethod As String, ByVal subTotalAmount As Decimal, ByVal shippingAmount As Decimal, ByVal handlingAmount As Decimal, ByVal taxAmount As Decimal, ByVal taxRate As Decimal, ByVal couponCodes As String, ByVal discountAmount As Decimal, ByVal specialInstructions As String, ByVal shipToAddress As String, ByVal billToAddress As String, ByVal userIP As String, ByVal shippingTrackingNumber As String, ByVal numberOfPackages As Integer, ByVal packagingNotes As String)
            Dim item As Order = New Order()
            item.OrderGUID = orderGUID
            item.OrderNumber = orderNumber
            item.OrderDate = orderDate
            item.OrderStatusID = orderStatusID
            item.UserName = userName
            item.Email = email
            item.FirstName = firstName
            item.LastName = lastName
            item.ShipPhone = shipPhone
            item.ShippingMethod = shippingMethod
            item.SubTotalAmount = subTotalAmount
            item.ShippingAmount = shippingAmount
            item.HandlingAmount = handlingAmount
            item.TaxAmount = taxAmount
            item.TaxRate = taxRate
            item.CouponCodes = couponCodes
            item.DiscountAmount = discountAmount
            item.SpecialInstructions = specialInstructions
            item.ShipToAddress = shipToAddress
            item.BillToAddress = billToAddress
            item.UserIP = userIP
            item.ShippingTrackingNumber = shippingTrackingNumber
            item.NumberOfPackages = numberOfPackages
            item.PackagingNotes = packagingNotes

            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal orderID As Integer, ByVal orderGUID As String, ByVal orderNumber As String, ByVal orderDate As DateTime, ByVal orderStatusID As Integer, ByVal userName As String, ByVal email As String, ByVal firstName As String, ByVal lastName As String, ByVal shipPhone As String, ByVal shippingMethod As String, ByVal subTotalAmount As Decimal, ByVal shippingAmount As Decimal, ByVal handlingAmount As Decimal, ByVal taxAmount As Decimal, ByVal taxRate As Decimal, ByVal couponCodes As String, ByVal discountAmount As Decimal, ByVal specialInstructions As String, ByVal shipToAddress As String, ByVal billToAddress As String, ByVal userIP As String, ByVal shippingTrackingNumber As String, ByVal numberOfPackages As Integer, ByVal packagingNotes As String)
            Dim item As Order = New Order()
            item.OrderID = orderID
            item.OrderGUID = orderGUID
            item.OrderNumber = orderNumber
            item.OrderDate = orderDate
            item.OrderStatusID = orderStatusID
            item.UserName = userName
            item.Email = email
            item.FirstName = firstName
            item.LastName = lastName
            item.ShipPhone = shipPhone
            item.ShippingMethod = shippingMethod
            item.SubTotalAmount = subTotalAmount
            item.ShippingAmount = shippingAmount
            item.HandlingAmount = handlingAmount
            item.TaxAmount = taxAmount
            item.TaxRate = taxRate
            item.CouponCodes = couponCodes
            item.DiscountAmount = discountAmount
            item.SpecialInstructions = specialInstructions
            item.ShipToAddress = shipToAddress
            item.BillToAddress = billToAddress
            item.UserIP = userIP
            item.ShippingTrackingNumber = shippingTrackingNumber
            item.NumberOfPackages = numberOfPackages
            item.PackagingNotes = packagingNotes

            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared OrderID As String
            Public Shared OrderGUID As String
            Public Shared OrderNumber As String
            Public Shared OrderDate As String
            Public Shared OrderStatusID As String
            Public Shared UserName As String
            Public Shared Email As String
            Public Shared FirstName As String
            Public Shared LastName As String
            Public Shared ShipPhone As String
            Public Shared ShippingMethod As String
            Public Shared SubTotalAmount As String
            Public Shared ShippingAmount As String
            Public Shared HandlingAmount As String
            Public Shared TaxAmount As String
            Public Shared TaxRate As String
            Public Shared CouponCodes As String
            Public Shared DiscountAmount As String
            Public Shared SpecialInstructions As String
            Public Shared ShipToAddress As String
            Public Shared BillToAddress As String
            Public Shared UserIP As String
            Public Shared ShippingTrackingNumber As String
            Public Shared NumberOfPackages As String
            Public Shared PackagingNotes As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                OrderID = "orderID"
                OrderGUID = "orderGUID"
                OrderNumber = "orderNumber"
                OrderDate = "orderDate"
                OrderStatusID = "orderStatusID"
                UserName = "userName"
                Email = "email"
                FirstName = "firstName"
                LastName = "lastName"
                ShipPhone = "shipPhone"
                ShippingMethod = "shippingMethod"
                SubTotalAmount = "subTotalAmount"
                ShippingAmount = "shippingAmount"
                HandlingAmount = "handlingAmount"
                TaxAmount = "taxAmount"
                TaxRate = "taxRate"
                CouponCodes = "couponCodes"
                DiscountAmount = "discountAmount"
                SpecialInstructions = "specialInstructions"
                ShipToAddress = "shipToAddress"
                BillToAddress = "billToAddress"
                UserIP = "userIP"
                ShippingTrackingNumber = "shippingTrackingNumber"
                NumberOfPackages = "numberOfPackages"
                PackagingNotes = "packagingNotes"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
            End Sub
        End Structure
#End Region

    End Class
End Namespace
