Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic
Imports System.Web

Namespace Commerce.Common
    Public Class SPs
        ''' <summary>
        ''' Creates an object wrapper for the CSK_Coupons_GetCoupon Stored Procedure
        ''' </summary>
        Public Shared Function CouponsGetCoupon(ByVal couponCode As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Coupons_GetCoupon")
            sp.Command.AddParameter("@couponCode", couponCode, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Order_Query Stored Procedure
        ''' </summary>
        Public Shared Function StoreOrderQuery(ByVal dateStart As DateTime, ByVal dateEnd As DateTime, ByVal userName As String, ByVal orderNumber As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Order_Query")
            sp.Command.AddParameter("@dateStart", dateStart, DbType.DateTime)
            sp.Command.AddParameter("@dateEnd", dateEnd, DbType.DateTime)
            sp.Command.AddParameter("@userName", userName, DbType.String)
            sp.Command.AddParameter("@orderNumber", orderNumber, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Category_GetPageByNameMulti Stored Procedure
        ''' </summary>
        Public Shared Function StoreCategoryGetPageByNameMulti(ByVal categoryName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetPageByNameMulti")
            sp.Command.AddParameter("@categoryName", categoryName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Coupons_SaveCoupon Stored Procedure
        ''' </summary>
        Public Shared Function CouponsSaveCoupon(ByVal CouponCode As String, ByVal CouponTypeId As Integer, ByVal IsSingleUse As Boolean, ByVal NumberUses As Integer, ByVal ExpirationDate As DateTime, ByVal XmlData As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Coupons_SaveCoupon")
            sp.Command.AddParameter("@CouponCode", CouponCode, DbType.String)
            sp.Command.AddParameter("@CouponTypeId", CouponTypeId, DbType.Int32)
            sp.Command.AddParameter("@IsSingleUse", IsSingleUse, DbType.Boolean)
            sp.Command.AddParameter("@NumberUses", NumberUses, DbType.Int32)
            sp.Command.AddParameter("@ExpirationDate", ExpirationDate, DbType.DateTime)
            sp.Command.AddParameter("@XmlData", XmlData, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_CategoryGetActive Stored Procedure
        ''' </summary>
        Public Shared Function StoreCategoryGetActive() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_CategoryGetActive")

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_AddRating Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductAddRating(ByVal productID As Integer, ByVal rating As Integer, ByVal userName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_AddRating")
            sp.Command.AddParameter("@productID", productID, DbType.Int32)
            sp.Command.AddParameter("@rating", rating, DbType.Int32)
            sp.Command.AddParameter("@userName", userName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Stats_FavoriteProducts Stored Procedure
        ''' </summary>
        Public Shared Function StatsFavoriteProducts(ByVal userName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Stats_FavoriteProducts")
            sp.Command.AddParameter("@userName", userName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Stats_FavoriteCategories Stored Procedure
        ''' </summary>
        Public Shared Function StatsFavoriteCategories(ByVal userName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Stats_FavoriteCategories")
            sp.Command.AddParameter("@userName", userName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_AddItemToCart Stored Procedure
        ''' </summary>
        Public Shared Function StoreAddItemToCart(ByVal userName As String, ByVal productID As Integer, ByVal attributes As String, ByVal pricePaid As Decimal, ByVal promoCode As String, ByVal quantity As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_AddItemToCart")
            sp.Command.AddParameter("@userName", userName, DbType.String)
            sp.Command.AddParameter("@productID", productID, DbType.Int32)
            sp.Command.AddParameter("@attributes", attributes, DbType.String)
            sp.Command.AddParameter("@pricePaid", pricePaid, DbType.Currency)
            sp.Command.AddParameter("@promoCode", promoCode, DbType.String)
            sp.Command.AddParameter("@quantity", quantity, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_EnsureOrderCoupon Stored Procedure
        ''' </summary>
        Public Shared Function PromoEnsureOrderCoupon(ByVal orderID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_EnsureOrderCoupon")
            sp.Command.AddParameter("@orderID", orderID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetPostAddMulti Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetPostAddMulti(ByVal userName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetPostAddMulti")
            sp.Command.AddParameter("@userName", userName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_DeletePermanent Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductDeletePermanent(ByVal productID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_DeletePermanent")
            sp.Command.AddParameter("@productID", productID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_ProductMatrix Stored Procedure
        ''' </summary>
        Public Shared Function PromoProductMatrix() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_ProductMatrix")

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Category_GetByProductID Stored Procedure
        ''' </summary>
        Public Shared Function StoreCategoryGetByProductID(ByVal productID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetByProductID")
            sp.Command.AddParameter("@productID", productID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_Bundle_GetSelectedProducts Stored Procedure
        ''' </summary>
        Public Shared Function PromoBundleGetSelectedProducts(ByVal bundleID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_Bundle_GetSelectedProducts")
            sp.Command.AddParameter("@bundleID", bundleID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_Bundle_GetByProductID Stored Procedure
        ''' </summary>
        Public Shared Function PromoBundleGetByProductID(ByVal productID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_Bundle_GetByProductID")
            sp.Command.AddParameter("@productID", productID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_GetProductList Stored Procedure
        ''' </summary>
        Public Shared Function PromoGetProductList(ByVal promoID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_GetProductList")
            sp.Command.AddParameter("@promoID", promoID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Coupons_GetCouponType Stored Procedure
        ''' </summary>
        Public Shared Function CouponsGetCouponType(ByVal couponTypeId As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Coupons_GetCouponType")
            sp.Command.AddParameter("@couponTypeId", couponTypeId, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Coupons_GetAllCouponTypes Stored Procedure
        ''' </summary>
        Public Shared Function CouponsGetAllCouponTypes() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Coupons_GetAllCouponTypes")

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Coupons_SaveCouponType Stored Procedure
        ''' </summary>
        Public Shared Function CouponsSaveCouponType(ByVal CouponTypeId As Integer, ByVal Description As String, ByVal ProcessingClassName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Coupons_SaveCouponType")
            sp.Command.AddParameter("@CouponTypeId", CouponTypeId, DbType.Int32)
            sp.Command.AddParameter("@Description", Description, DbType.String)
            sp.Command.AddParameter("@ProcessingClassName", ProcessingClassName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Tax_GetTaxRate Stored Procedure
        ''' </summary>
        Public Shared Function TaxGetTaxRate(ByVal zip As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Tax_GetTaxRate")
            sp.Command.AddParameter("@zip", zip, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Tax_SaveZipRate Stored Procedure
        ''' </summary>
        Public Shared Function TaxSaveZipRate(ByVal state As String, ByVal zipCode As String, ByVal rate As Decimal, ByVal userName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Tax_SaveZipRate")
            sp.Command.AddParameter("@state", state, DbType.String)
            sp.Command.AddParameter("@zipCode", zipCode, DbType.String)
            sp.Command.AddParameter("@rate", rate, DbType.Currency)
            sp.Command.AddParameter("@userName", userName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Tax_CalculateAmountByZIP Stored Procedure
        ''' </summary>
        Public Shared Function TaxCalculateAmountByZIP(ByVal zipCode As String, ByVal subTotal As Decimal) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Tax_CalculateAmountByZIP")
            sp.Command.AddParameter("@zipCode", zipCode, DbType.String)
            sp.Command.AddParameter("@subTotal", subTotal, DbType.Currency)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Tax_CalculateAmountByState Stored Procedure
        ''' </summary>
        Public Shared Function TaxCalculateAmountByState(ByVal stateAbbreviation As String, ByVal subTotal As Decimal) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Tax_CalculateAmountByState")
            sp.Command.AddParameter("@stateAbbreviation", stateAbbreviation, DbType.String)
            sp.Command.AddParameter("@subTotal", subTotal, DbType.Currency)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Stats_Tracker_SynchTrackingCookie Stored Procedure
        ''' </summary>
        Public Shared Function StatsTrackerSynchTrackingCookie(ByVal userName As String, ByVal trackerCookie As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Stats_Tracker_SynchTrackingCookie")
            sp.Command.AddParameter("@userName", userName, DbType.String)
            sp.Command.AddParameter("@trackerCookie", trackerCookie, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Stats_Tracker_GetByBehaviorID Stored Procedure
        ''' </summary>
        Public Shared Function StatsTrackerGetByBehaviorID(ByVal behaviorID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Stats_Tracker_GetByBehaviorID")
            sp.Command.AddParameter("@behaviorID", behaviorID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Stats_Tracker_GetByProductAndBehavior Stored Procedure
        ''' </summary>
        Public Shared Function StatsTrackerGetByProductAndBehavior(ByVal behaviorID As Integer, ByVal sku As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Stats_Tracker_GetByProductAndBehavior")
            sp.Command.AddParameter("@behaviorID", behaviorID, DbType.Int32)
            sp.Command.AddParameter("@sku", sku, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_RemoveProducts Stored Procedure
        ''' </summary>
        Public Shared Function PromoRemoveProducts(ByVal promoID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_RemoveProducts")
            sp.Command.AddParameter("@promoID", promoID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_AddProduct Stored Procedure
        ''' </summary>
        Public Shared Function PromoAddProduct(ByVal promoID As Integer, ByVal productID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_AddProduct")
            sp.Command.AddParameter("@promoID", promoID, DbType.Int32)
            sp.Command.AddParameter("@productID", productID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_Bundle_AddProduct Stored Procedure
        ''' </summary>
        Public Shared Function PromoBundleAddProduct(ByVal bundleID As Integer, ByVal productID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_Bundle_AddProduct")
            sp.Command.AddParameter("@bundleID", bundleID, DbType.Int32)
            sp.Command.AddParameter("@productID", productID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Shipping_GetRates_Ground Stored Procedure
        ''' </summary>
        Public Shared Function ShippingGetRatesGround(ByVal weight As Decimal) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Shipping_GetRates_Ground")
            sp.Command.AddParameter("@weight", weight, DbType.Currency)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Shipping_GetRates_Air Stored Procedure
        ''' </summary>
        Public Shared Function ShippingGetRatesAir(ByVal weight As Decimal) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Shipping_GetRates_Air")
            sp.Command.AddParameter("@weight", weight, DbType.Currency)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Shipping_GetRates Stored Procedure
        ''' </summary>
        Public Shared Function ShippingGetRates(ByVal weight As Decimal) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Shipping_GetRates")
            sp.Command.AddParameter("@weight", weight, DbType.Currency)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Content_Update Stored Procedure
        ''' </summary>
        Public Shared Function ContentUpdate(ByVal contentID As Integer, ByVal contentGUID As String, ByVal title As String, ByVal contentName As String, ByVal content As String, ByVal iconPath As String, ByVal dateExpires As DateTime, ByVal contentGroupID As Integer, ByVal lastEditedBy As String, ByVal externalLink As String, ByVal status As String, ByVal listOrder As Integer, ByVal callOut As String, ByVal createdOn As DateTime, ByVal createdBy As String, ByVal modifiedOn As DateTime, ByVal modifiedBy As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Content_Update")
            sp.Command.AddParameter("@contentID", contentID, DbType.Int32)
            sp.Command.AddParameter("@contentGUID", contentGUID, DbType.Guid)
            sp.Command.AddParameter("@title", title, DbType.String)
            sp.Command.AddParameter("@contentName", contentName, DbType.String)
            sp.Command.AddParameter("@content", content, DbType.String)
            sp.Command.AddParameter("@iconPath", iconPath, DbType.String)
            sp.Command.AddParameter("@dateExpires", dateExpires, DbType.DateTime)
            sp.Command.AddParameter("@contentGroupID", contentGroupID, DbType.Int32)
            sp.Command.AddParameter("@lastEditedBy", lastEditedBy, DbType.String)
            sp.Command.AddParameter("@externalLink", externalLink, DbType.String)
            sp.Command.AddParameter("@status", status, DbType.String)
            sp.Command.AddParameter("@listOrder", listOrder, DbType.Int32)
            sp.Command.AddParameter("@callOut", callOut, DbType.String)
            sp.Command.AddParameter("@createdOn", createdOn, DbType.DateTime)
            sp.Command.AddParameter("@createdBy", createdBy, DbType.String)
            sp.Command.AddParameter("@modifiedOn", modifiedOn, DbType.DateTime)
            sp.Command.AddParameter("@modifiedBy", modifiedBy, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Content_Get Stored Procedure
        ''' </summary>
        Public Shared Function ContentGet(ByVal contentName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Content_Get")
            sp.Command.AddParameter("@contentName", contentName, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Content_Insert Stored Procedure
        ''' </summary>
        Public Shared Function ContentInsert(ByVal contentGUID As String, ByVal title As String, ByVal contentName As String, ByVal content As String, ByVal iconPath As String, ByVal dateExpires As DateTime, ByVal contentGroupID As Integer, ByVal lastEditedBy As String, ByVal externalLink As String, ByVal status As String, ByVal listOrder As Integer, ByVal callOut As String, ByVal createdOn As DateTime, ByVal createdBy As String, ByVal modifiedOn As DateTime, ByVal modifiedBy As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Content_Insert")
            sp.Command.AddParameter("@contentGUID", contentGUID, DbType.Guid)
            sp.Command.AddParameter("@title", title, DbType.String)
            sp.Command.AddParameter("@contentName", contentName, DbType.String)
            sp.Command.AddParameter("@content", content, DbType.String)
            sp.Command.AddParameter("@iconPath", iconPath, DbType.String)
            sp.Command.AddParameter("@dateExpires", dateExpires, DbType.DateTime)
            sp.Command.AddParameter("@contentGroupID", contentGroupID, DbType.Int32)
            sp.Command.AddParameter("@lastEditedBy", lastEditedBy, DbType.String)
            sp.Command.AddParameter("@externalLink", externalLink, DbType.String)
            sp.Command.AddParameter("@status", status, DbType.String)
            sp.Command.AddParameter("@listOrder", listOrder, DbType.Int32)
            sp.Command.AddParameter("@callOut", callOut, DbType.String)
            sp.Command.AddParameter("@createdOn", createdOn, DbType.DateTime)
            sp.Command.AddParameter("@createdBy", createdBy, DbType.String)
            sp.Command.AddParameter("@modifiedOn", modifiedOn, DbType.DateTime)
            sp.Command.AddParameter("@modifiedBy", modifiedBy, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Content_Save Stored Procedure
        ''' </summary>
        Public Shared Function ContentSave(ByVal contentName As String, ByVal title As String, ByVal content As String, ByVal groupID As Integer, ByVal iconPath As String, ByVal status As String, ByVal userName As String, ByVal callOut As String, ByVal externalLink As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Content_Save")
            sp.Command.AddParameter("@contentName", contentName, DbType.String)
            sp.Command.AddParameter("@title", title, DbType.String)
            sp.Command.AddParameter("@content", content, DbType.String)
            sp.Command.AddParameter("@groupID", groupID, DbType.Int32)
            sp.Command.AddParameter("@iconPath", iconPath, DbType.String)
            sp.Command.AddParameter("@status", status, DbType.String)
            sp.Command.AddParameter("@userName", userName, DbType.String)
            sp.Command.AddParameter("@callOut", callOut, DbType.String)
            sp.Command.AddParameter("@externalLink", externalLink, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the ClearLogs Stored Procedure
        ''' </summary>
        Public Shared Function ClearLogs() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "ClearLogs")

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the InsertCategoryLog Stored Procedure
        ''' </summary>
        Public Shared Function InsertCategoryLog(ByVal CategoryID As Integer, ByVal LogID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "InsertCategoryLog")
            sp.Command.AddParameter("@CategoryID", CategoryID, DbType.Int32)
            sp.Command.AddParameter("@LogID", LogID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the WriteLog Stored Procedure
        ''' </summary>
        Public Shared Function WriteLog(ByVal EventID As Integer, ByVal Priority As Integer, ByVal Severity As String, ByVal Title As String, ByVal Timestamp As DateTime, ByVal MachineName As String, ByVal AppDomainName As String, ByVal ProcessID As String, ByVal ProcessName As String, ByVal ThreadName As String, ByVal Win32ThreadId As String, ByVal Message As String, ByVal FormattedMessage As String, ByVal LogId As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "WriteLog")
            sp.Command.AddParameter("@EventID", EventID, DbType.Int32)
            sp.Command.AddParameter("@Priority", Priority, DbType.Int32)
            sp.Command.AddParameter("@Severity", Severity, DbType.String)
            sp.Command.AddParameter("@Title", Title, DbType.String)
            sp.Command.AddParameter("@Timestamp", Timestamp, DbType.DateTime)
            sp.Command.AddParameter("@MachineName", MachineName, DbType.String)
            sp.Command.AddParameter("@AppDomainName", AppDomainName, DbType.String)
            sp.Command.AddParameter("@ProcessID", ProcessID, DbType.String)
            sp.Command.AddParameter("@ProcessName", ProcessName, DbType.String)
            sp.Command.AddParameter("@ThreadName", ThreadName, DbType.String)
            sp.Command.AddParameter("@Win32ThreadId", Win32ThreadId, DbType.String)
            sp.Command.AddParameter("@Message", Message, DbType.String)
            sp.Command.AddParameter("@FormattedMessage", FormattedMessage, DbType.String)
            sp.Command.AddParameter("@LogId", LogId, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Config_GetList Stored Procedure
        ''' </summary>
        Public Shared Function StoreConfigGetList() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Config_GetList")

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Category_GetCrumbs Stored Procedure
        ''' </summary>
        Public Shared Function StoreCategoryGetCrumbs(ByVal categoryID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetCrumbs")
            sp.Command.AddParameter("@categoryID", categoryID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetByPriceRange Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetByPriceRange(ByVal categoryID As Integer, ByVal priceStart As Decimal, ByVal priceEnd As Decimal) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetByPriceRange")
            sp.Command.AddParameter("@categoryID", categoryID, DbType.Int32)
            sp.Command.AddParameter("@priceStart", priceStart, DbType.Currency)
            sp.Command.AddParameter("@priceEnd", priceEnd, DbType.Currency)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetByManufacturerID Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetByManufacturerID(ByVal manufacturerID As Integer, ByVal categoryID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetByManufacturerID")
            sp.Command.AddParameter("@manufacturerID", manufacturerID, DbType.Int32)
            sp.Command.AddParameter("@categoryID", categoryID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetByCategoryID Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetByCategoryID(ByVal categoryID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetByCategoryID")
            sp.Command.AddParameter("@categoryID", categoryID, DbType.Int32)

            Return sp
        End Function
        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetTop8ByCategoryID Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetTop8ByCategoryID(ByVal categoryID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetTop8ByCategoryID")
            sp.Command.AddParameter("@categoryID", categoryID, DbType.Int32)
            Return sp
        End Function
        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetByTypeID Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetByTypeID(ByVal typeID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetByTypeID")
            sp.Command.AddParameter("@TypeID", typeID, DbType.Int32)
            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetMostPopular Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetMostPopular() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetMostPopular")
            Return sp
        End Function




        ''' <summary>
        ''' Creates an object wrapper for the CSK_Promo_Bundle_GetAvailableProducts Stored Procedure
        ''' </summary>
        Public Shared Function PromoBundleGetAvailableProducts(ByVal bundleID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Promo_Bundle_GetAvailableProducts")
            sp.Command.AddParameter("@bundleID", bundleID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the AddCategory Stored Procedure
        ''' </summary>
        Public Shared Function AddCategory(ByVal CategoryName As String, ByVal LogID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "AddCategory")
            sp.Command.AddParameter("@CategoryName", CategoryName, DbType.String)
            sp.Command.AddParameter("@LogID", LogID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_SmartSearch Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductSmartSearch(ByVal DescriptionLength As Integer, ByVal AllWords As Boolean, ByVal Word1 As String, ByVal Word2 As String, ByVal Word3 As String, ByVal Word4 As String, ByVal Word5 As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_SmartSearch")
            sp.Command.AddParameter("@DescriptionLength", DescriptionLength, DbType.Int32)
            sp.Command.AddParameter("@AllWords", AllWords, DbType.Boolean)
            sp.Command.AddParameter("@Word1", Word1, DbType.String)
            sp.Command.AddParameter("@Word2", Word2, DbType.String)
            sp.Command.AddParameter("@Word3", Word3, DbType.String)
            sp.Command.AddParameter("@Word4", Word4, DbType.String)
            sp.Command.AddParameter("@Word5", Word5, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Category_GetAllSubs Stored Procedure
        ''' </summary>
        Public Shared Function StoreCategoryGetAllSubs(ByVal parentID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetAllSubs")
            sp.Command.AddParameter("@parentID", parentID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Category_GetPageMulti Stored Procedure
        ''' </summary>
        Public Shared Function StoreCategoryGetPageMulti(ByVal categoryID As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetPageMulti")
            sp.Command.AddParameter("@categoryID", categoryID, DbType.Int32)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Category_GetPageByGUIDMulti Stored Procedure
        ''' </summary>
        Public Shared Function StoreCategoryGetPageByGUIDMulti(ByVal categoryGUID As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetPageByGUIDMulti")
            sp.Command.AddParameter("@categoryGUID", categoryGUID, DbType.String)

            Return sp
        End Function

        ''' <summary>
        ''' Creates an object wrapper for the CSK_Store_Product_GetByCategoryName Stored Procedure
        ''' </summary>
        Public Shared Function StoreProductGetByCategoryName(ByVal categoryName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetByCategoryName")
            sp.Command.AddParameter("@categoryName", categoryName, DbType.String)
            Return sp
        End Function

        Public Shared Function GetWebsiteLogo() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_GetWebsiteLogo")
            Return sp
        End Function

        Public Shared Function UpdateLogo(ByVal ImageName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_UpdateLogo")
            sp.Command.AddParameter("@ImageLogo", ImageName, DbType.String)
            Return sp
        End Function

        Public Shared Function StoreCategoryGetAllParent() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetAllParent")
            Return sp
        End Function

        Public Shared Function GetSubCategoriesByCategoryId(ByVal CategoryId As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GetSubCategoriesByCategoryId")
            sp.Command.AddParameter("@IdCategory", CategoryId, DbType.Int32)
            Return sp
        End Function

        'Public Shared Function GetCategoryByCategoryId(ByVal CategoryId As Integer) As StoredProcedure
        '    Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." &"CSK_Store_Category_GetCategorybyId")
        '    sp.Command.AddParameter("@IdCategory", CategoryId, DbType.Int32)
        '    Return sp
        'End Function

        Public Shared Function GetCategoryByCategoryId(ByVal CategoryId As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Category_GetCategorybyId")
            sp.Command.AddParameter("@IdCategory", CategoryId, DbType.String)
            Return sp
        End Function

        'UpdateMostPopularProducts

        Public Shared Function UpdateMostPopularProducts(ByVal ProductIdList As String, ByVal ProductOfTheDay As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_UpdateMostPopularProducts")
            sp.Command.AddParameter("@ProductIdList", ProductIdList, DbType.String)
            sp.Command.AddParameter("@ProductOfTheDay", ProductOfTheDay, DbType.String)
            Return sp
        End Function

        Public Shared Function GetMostPopularProducts() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetMostPopularProducts")
            Return sp
        End Function
        'Public Shared Function StoreGetAllUsersSignedForNewsLetter() As StoredProcedure
        '    Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." &"GMR_GetAllUsers_SignedForNewLetter")
        '    Return sp
        'End Function
        ''' <summary>
        ''' Added By Sowmitri
        ''' </summary>
        ''' 
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function StoreDeleteUsersSignedForNewsLetter(ByVal IdNewsLetter As Int64) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_DeleteUser_SignedForNewLetter")
            sp.Command.AddParameter("@IdNewsLetter", IdNewsLetter, DbType.Int64)
            Return sp
        End Function

        Public Shared Function AddEmailToNewsLetter(ByVal Name As String, ByVal Email As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_AddEmailToNewsLetter")
            sp.Command.AddParameter("@Name", Name, DbType.String)
            sp.Command.AddParameter("@EmailId", Email, DbType.String)
            Return sp
        End Function

        Public Shared Function GetAllNewsLetterDetails() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_GetNewsLetterDetails")
            Return sp
        End Function

        Public Shared Function CheckEmailForNewsLetter(ByVal Email As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_CheckEmailForNewsLetter")
            sp.Command.AddParameter("@EmailId", Email, DbType.String)
            Return sp
        End Function

        Public Shared Function StoreGetAllUsersSignedForNewsLetter() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetAllUsers_SignedForNewLetter")
            Return sp
        End Function


        Public Shared Function GetProductsByAdvancedSearch(ByVal sku As String, ByVal productname As String, ByVal keywords As String, ByVal LowerPrice As Decimal, ByVal UpperPrice As Decimal) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "SP_ProductAdvancedSearch")
            sp.Command.AddParameter("@sku", sku, DbType.String)
            sp.Command.AddParameter("@ProductName", productname, DbType.String)
            sp.Command.AddParameter("@Keywords", keywords, DbType.String)
            sp.Command.AddParameter("@LowerPrice", LowerPrice, DbType.Decimal)
            sp.Command.AddParameter("@UpperPrice", UpperPrice, DbType.Decimal)
            Return sp
        End Function

        Public Shared Function GetProductsByManufacturer(ByVal manufacturerid As Integer) As StoredProcedure

            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetProductsByManufacturer")
            sp.Command.AddParameter("@manufacturerid", manufacturerid, DbType.Int64)
            Return sp
        End Function

        Public Shared Function GetAllProducts() As StoredProcedure

            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetAllProducts")
            'sp.Command.AddParameter("@manufacturerid", manufacturerid, DbType.Int64)
            Return sp
        End Function
        Public Shared Function GetAllProductTypes() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetAllProductTypes")
            Return sp
        End Function
        Public Shared Function GerDealer_LocatorByCity() As StoredProcedure

            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GerDealer_LocatorByCity")
            'sp.Command.AddParameter("@manufacturerid", manufacturerid, DbType.Int64)
            Return sp
        End Function

        '
        Public Shared Function GerStore_LocatorByCity() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetStore_LocatorByCity")
            'sp.Command.AddParameter("@manufacturerid", manufacturerid, DbType.Int64)
            Return sp
        End Function
        Public Shared Function GerStore_LocatorCitiesByState(ByVal StateName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetStore_LocatorCitiesByState")
            sp.Command.AddParameter("@StateName", StateName, DbType.String)
            Return sp
        End Function
        Public Shared Function GMR_GetDealer_LocatorByState(ByVal State_Name As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetDealer_LocatorByState")
            sp.Command.AddParameter("@State", State_Name, DbType.String)
            Return sp
        End Function
        Public Shared Function GMR_GetAllDealers() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetAllDealers")
            Return sp
        End Function
        Public Shared Function GMR_GetDealer_LocatorByCity(ByVal CityName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetDealer_LocatorByCity")
            sp.Command.AddParameter("@City", CityName, DbType.String)
            Return sp
        End Function
        Public Shared Function GMR_GetDealer_LocatorByStateCity(ByVal StateName As String, ByVal CityName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetDealer_LocatorByStateCity")
            sp.Command.AddParameter("@State", StateName, DbType.String)
            sp.Command.AddParameter("@City", CityName, DbType.String)
            Return sp
        End Function

        Public Shared Function GMR_GetStore_LocatorByState(ByVal State_Name As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_Getstore_LocatorByState")
            sp.Command.AddParameter("@State", State_Name, DbType.String)
            Return sp
        End Function
        Public Shared Function GMR_GetStore_LocatorByCity(ByVal CityName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_Getstore_LocatorByCityV1")
            sp.Command.AddParameter("@City", CityName, DbType.String)
            Return sp
        End Function
        Public Shared Function GMR_GetStore_LocatorByStateCity(ByVal StateName As String, ByVal CityName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_Getstore_LocatorByStateCity")
            sp.Command.AddParameter("@State", StateName, DbType.String)
            sp.Command.AddParameter("@City", CityName, DbType.String)
            Return sp
        End Function

        Public Shared Function spGetGuidCategoryName(ByVal catName As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetGuidCategoryName")
            sp.Command.AddParameter("@catName", catName)
            Return sp
        End Function
        Public Shared Function spGetshortDescription(ByVal shortDescription As String) As StoredProcedure

            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetshortDescription")
            sp.Command.AddParameter("@shortDescription", shortDescription)
            Return sp
        End Function

        Public Shared Function spGetProductDeepProductName(ByVal ProductName As String) As StoredProcedure

            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetProductByProductName")
            sp.Command.AddParameter("@ProductName", ProductName)
            Return sp
        End Function
        Public Shared Function spGetProductDeepProductId(ByVal ProductId As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetProductByProductId")
            sp.Command.AddParameter("@productId", ProductId)
            Return sp
        End Function
        Public Shared Function StoreProductGetRelatedProductsByProductId(ByVal ProductId As Int64) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "StoreProductGetRelatedProductsByProductId")
            sp.Command.AddParameter("@ProductId", ProductId, DbType.Int64)
            Return sp
        End Function


        Public Shared Function spGetPartners() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetPartners")
            Return sp
        End Function

        Public Shared Function spGetStores() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetStores")
            Return sp
        End Function
        Public Shared Function spGetAllStoreLocators() As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetAllStore_Locator")
            Return sp
        End Function

        Public Shared Function News_GetNewsArticleByNewsId(ByVal NewsId As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_News_GetNewsListByNewsId")
            sp.Command.AddParameter("@NewsId", NewsId, DbType.Int64)
            Return sp
        End Function
        Public Shared Function News_GetActiveNewsArticles(ByVal GetAll As Boolean, ByVal DisplayCount As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_News_GetActiveNewsList")
            sp.Command.AddParameter("@GetAll", GetAll, DbType.Boolean)
            sp.Command.AddParameter("@Count", DisplayCount, DbType.Int64)
            Return sp
        End Function

        Public Shared Function News_GetActiveAds(ByVal GetAll As Boolean, ByVal DisplayCount As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_advert_GetActiveAdverts")
            sp.Command.AddParameter("@GetAll", GetAll, DbType.Boolean)
            sp.Command.AddParameter("@Count", DisplayCount, DbType.Int64)
            Return sp
        End Function


        Public Shared Function News_GetAllNewsArticles(ByVal GetAll As Boolean, ByVal DisplayCount As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_News_GetNewsList")
            sp.Command.AddParameter("@GetAll", GetAll, DbType.Boolean)
            sp.Command.AddParameter("@Count", DisplayCount, DbType.Int64)
            Return sp
        End Function


        Public Shared Function News_InsertNewsArticles(ByVal NewsTitle As String, ByVal NewsSubject As String, ByVal NewsDescription As String, ByVal NewsImage As String, ByVal NewsAttachment As String, ByVal Active As Boolean) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_News_InsertNews")
            sp.Command.AddParameter("@NewsTitle", NewsTitle, DbType.String)
            sp.Command.AddParameter("@NewsSubject", NewsSubject, DbType.String)
            sp.Command.AddParameter("@NewsDescription", NewsDescription, DbType.String)
            sp.Command.AddParameter("@NewsImage", NewsImage, DbType.String)
            sp.Command.AddParameter("@NewsAttachment", NewsAttachment, DbType.String)
            sp.Command.AddParameter("@Active", Active, DbType.Boolean)
            Return sp
        End Function

        Public Shared Function News_UpdateNewsArticles(ByVal NewsId As Integer, ByVal NewsTitle As String, ByVal NewsSubject As String, ByVal NewsDescription As String, ByVal NewsImage As String, ByVal NewsAttachment As String, ByVal Active As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_News_UpdateNews")
            sp.Command.AddParameter("@NewsId", NewsId, DbType.Int64)
            sp.Command.AddParameter("@NewsTitle", NewsTitle, DbType.String)
            sp.Command.AddParameter("@NewsSubject", NewsSubject, DbType.String)
            sp.Command.AddParameter("@NewsDescription", NewsDescription, DbType.String)
            sp.Command.AddParameter("@NewsImage", NewsImage, DbType.String)
            sp.Command.AddParameter("@NewsAttachment", NewsAttachment, DbType.String)
            sp.Command.AddParameter("@Active", Active, DbType.Int64)
            Return sp
        End Function

        Public Shared Function News_DeleteNewsArticles(ByVal NewsId As Integer) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_News_DeleteNews")
            sp.Command.AddParameter("@NewsId", NewsId, DbType.Int64)
            Return sp
        End Function
        Public Shared Function GetProductSEOTags(ByVal ProductId As String) As StoredProcedure
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "CSK_Store_Product_GetProductSEOTagsByProductId")
            sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
            Return sp
        End Function

    End Class
End Namespace