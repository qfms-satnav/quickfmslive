Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the ProductReviewFeedback class.
    ''' </summary>

    <Serializable()> _
    Partial Public Class ProductReviewFeedbackCollection
        Inherits ActiveList(Of ProductReviewFeedback)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As ProductReviewFeedbackCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As ProductReviewFeedbackCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As ProductReviewFeedbackCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As ProductReviewFeedbackCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As ProductReviewFeedbackCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As ProductReviewFeedbackCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As ProductReviewFeedbackCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As ProductReviewFeedbackCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Store_ProductReviewFeedback")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub

    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the CSK_Store_ProductReviewFeedback table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class ProductReviewFeedback
        Inherits ActiveRecord(Of ProductReviewFeedback)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Store_ProductReviewFeedback")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As ProductReviewFeedback = New ProductReviewFeedback()
            Return ProductReviewFeedback.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Store_ProductReviewFeedback")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

        Public Sub New(ByVal columnName As String, ByVal columnValue As Object)
            SetSQLProps()
            MyBase.LoadByParam(columnName, columnValue)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("FeedbackID")> _
        Public Property FeedbackID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("FeedbackID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("FeedbackID", Value)
            End Set
        End Property
        <XmlAttribute("ReviewID")> _
        Public Property ReviewID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ReviewID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ReviewID", Value)
            End Set
        End Property
        <XmlAttribute("UserName")> _
        Public Property UserName() As String
            Get
                Dim result As Object = Me.GetColumnValue("UserName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("UserName", Value)
            End Set
        End Property
        <XmlAttribute("IsHelpful")> _
        Public Property IsHelpful() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsHelpful")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
            Set(ByVal value As Boolean)
                Me.MarkDirty()
                Me.SetColumnValue("IsHelpful", Value)
            End Set
        End Property
        <XmlAttribute("CreatedOn")> _
        Public Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedOn", Value)
            End Set
        End Property
        <XmlAttribute("CreatedBy")> _
        Public Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedBy", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedOn", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedBy", Value)
            End Set
        End Property

#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal ReviewID As Integer, ByVal UserName As String, ByVal IsHelpful As Boolean)
            Dim item As ProductReviewFeedback = New ProductReviewFeedback()
            item.ReviewID = ReviewID
            item.UserName = UserName
            item.IsHelpful = IsHelpful

            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal feedbackID As Integer, ByVal ReviewID As Integer, ByVal UserName As String, ByVal IsHelpful As Boolean)
            Dim item As ProductReviewFeedback = New ProductReviewFeedback()
            item.FeedbackID = feedbackID
            item.ReviewID = ReviewID
            item.UserName = UserName
            item.IsHelpful = IsHelpful

            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared FeedbackID As String
            Public Shared ReviewID As String
            Public Shared UserName As String
            Public Shared IsHelpful As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()

            Shared Sub New()
                FeedbackID = "feedbackID"
                ReviewID = "ReviewID"
                UserName = "UserName"
                IsHelpful = "IsHelpful"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
            End Sub
        End Structure
#End Region

    End Class
End Namespace
