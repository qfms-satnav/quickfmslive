Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports SubSonic

Namespace Commerce.Common
    ''' <summary>
    ''' Strongly-typed collection for the Ad class.
    ''' </summary>

    <Serializable()> _
    Partial Public Class AdCollection
        Inherits ActiveList(Of Ad)

        Private wheres As List(Of Where) = New List(Of Where)()
        Private betweens As List(Of BetweenAnd) = New List(Of BetweenAnd)()
        Private orderBy As SubSonic.OrderBy
        Public Function OrderByAsc(ByVal columnName As String) As AdCollection
            Me.orderBy = SubSonic.OrderBy.Asc(columnName)
            Return Me
        End Function
        Public Function OrderByDesc(ByVal columnName As String) As AdCollection
            Me.orderBy = SubSonic.OrderBy.Desc(columnName)
            Return Me
        End Function
        Public Function WhereDatesBetween(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As AdCollection
            Return Me

        End Function

        'INSTANT VB NOTE: The parameter where was renamed since Visual Basic will not allow parameters with the same name as their method:
        Public Function Where(ByVal where_Renamed As Where) As AdCollection
            wheres.Add(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal value As Object) As AdCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me
        End Function
        Public Function Where(ByVal columnName As String, ByVal comp As Comparison, ByVal value As Object) As AdCollection
            'INSTANT VB NOTE: The local variable where was renamed since Visual Basic will not allow local variables with the same name as their method:
            Dim where_Renamed As Where = New Where()
            where_Renamed.ColumnName = columnName
            where_Renamed.Comparison = comp
            where_Renamed.ParameterValue = value
            Me.Where(where_Renamed)
            Return Me

        End Function
        Public Function BetweenAnd(ByVal columnName As String, ByVal dateStart As DateTime, ByVal dateEnd As DateTime) As AdCollection
            Dim between As BetweenAnd = New BetweenAnd()
            between.ColumnName = columnName
            between.StartDate = dateStart
            between.EndDate = dateEnd
            betweens.Add(between)
            Return Me
        End Function
        Public Overridable Overloads Function Load() As AdCollection
            ' LJD 10/17/2007 Added Overridable Overloads
            Dim qry As Query = New Query("CSK_Store_Ad")

            For Each where As Where In wheres
                qry.AddWhere(where)
            Next where
            For Each between As BetweenAnd In betweens
                qry.AddBetweenAnd(between)
            Next between

            If Not orderBy Is Nothing Then
                qry.OrderBy = orderBy
            End If



            Dim rdr As IDataReader = qry.ExecuteReader()
            Me.Load(rdr)
            rdr.Close()
            Return Me
        End Function
        Public Sub New()


        End Sub

    End Class

    ''' <summary>
    ''' This is an ActiveRecord class which wraps the CSK_Store_Ad table.
    ''' </summary>
    <Serializable()> _
    Partial Public Class Ad
        Inherits ActiveRecord(Of Ad)

#Region "Default Settings"
        Private Sub SetSQLProps()
            If Schema Is Nothing Then
                Schema = Query.BuildTableSchema("CSK_Store_Ad")
            End If
        End Sub
#End Region

#Region "Schema Accessor"
        Public Shared Function GetTableSchema() As TableSchema.Table
            Dim item As Ad = New Ad()
            Return Ad.Schema
        End Function
#End Region

#Region "Query Accessor"
        Public Shared Function CreateQuery() As Query
            Return New Query("CSK_Store_Ad")
        End Function
#End Region

#Region ".ctors"
        Public Sub New()
            SetSQLProps()
            SetDefaults()
            Me.MarkNew()
        End Sub

        Public Sub New(ByVal keyID As Object)
            SetSQLProps()
            MyBase.LoadByKey(keyID)
        End Sub

        Public Sub New(ByVal columnName As String, ByVal columnValue As Object)
            SetSQLProps()
            MyBase.LoadByParam(columnName, columnValue)
        End Sub

#End Region

#Region "Public Properties"
        <XmlAttribute("AdID")> _
        Public Property AdID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("AdID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("AdID", Value)
            End Set
        End Property
        <XmlAttribute("PageName")> _
        Public Property PageName() As String
            Get
                Dim result As Object = Me.GetColumnValue("PageName")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("PageName", Value)
            End Set
        End Property
        <XmlAttribute("ListOrder")> _
        Public Property ListOrder() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("ListOrder")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("ListOrder", Value)
            End Set
        End Property
        <XmlAttribute("Placement")> _
        Public Property Placement() As String
            Get
                Dim result As Object = Me.GetColumnValue("Placement")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("Placement", Value)
            End Set
        End Property
        <XmlAttribute("AdText")> _
        Public Property AdText() As String
            Get
                Dim result As Object = Me.GetColumnValue("AdText")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("AdText", Value)
            End Set
        End Property
        <XmlAttribute("ProductSku")> _
        Public Property ProductSku() As String
            Get
                Dim result As Object = Me.GetColumnValue("ProductSku")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ProductSku", Value)
            End Set
        End Property
        <XmlAttribute("PromoID")> _
        Public Property PromoID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("PromoID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("PromoID", Value)
            End Set
        End Property
        <XmlAttribute("CategoryID")> _
        Public Property CategoryID() As Integer
            Get
                Dim result As Object = Me.GetColumnValue("CategoryID")
                Dim oOut As Integer = 0
                Try
                    oOut = Integer.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As Integer)
                Me.MarkDirty()
                Me.SetColumnValue("CategoryID", Value)
            End Set
        End Property
        <XmlAttribute("DateExpires")> _
        Public Property DateExpires() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("DateExpires")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("DateExpires", Value)
            End Set
        End Property
        <XmlAttribute("IsActive")> _
        Public Property IsActive() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsActive")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
            Set(ByVal value As Boolean)
                Me.MarkDirty()
                Me.SetColumnValue("IsActive", Value)
            End Set
        End Property
        <XmlAttribute("IsDeleted")> _
        Public Property IsDeleted() As Boolean
            Get
                Dim result As Object = Me.GetColumnValue("IsDeleted")
                Dim bOut As Boolean
                If result Is Nothing Then
                    bOut = False
                Else
                    bOut = Convert.ToBoolean(result)
                End If
                Return bOut

            End Get
            Set(ByVal value As Boolean)
                Me.MarkDirty()
                Me.SetColumnValue("IsDeleted", Value)
            End Set
        End Property
        <XmlAttribute("CreatedOn")> _
        Public Property CreatedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("CreatedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedOn", Value)
            End Set
        End Property
        <XmlAttribute("CreatedBy")> _
        Public Property CreatedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("CreatedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("CreatedBy", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedOn")> _
        Public Property ModifiedOn() As DateTime
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedOn")
                Dim oOut As DateTime = New DateTime(1900, 1, 1)
                Try
                    oOut = DateTime.Parse(result.ToString())
                Catch
                End Try
                Return oOut

            End Get
            Set(ByVal value As DateTime)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedOn", Value)
            End Set
        End Property
        <XmlAttribute("ModifiedBy")> _
        Public Property ModifiedBy() As String
            Get
                Dim result As Object = Me.GetColumnValue("ModifiedBy")
                Dim sOut As String
                If result Is Nothing Then
                    sOut = String.Empty
                Else
                    sOut = result.ToString()
                End If
                Return sOut

            End Get
            Set(ByVal value As String)
                Me.MarkDirty()
                Me.SetColumnValue("ModifiedBy", Value)
            End Set
        End Property

#End Region

#Region "ObjectDataSource support"

        ''' <summary>
        ''' Inserts a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Insert(ByVal pageName As String, ByVal listOrder As Integer, ByVal placement As String, ByVal adText As String, ByVal productSku As String, ByVal promoID As Integer, ByVal categoryID As Integer, ByVal dateExpires As DateTime, ByVal isActive As Boolean, ByVal isDeleted As Boolean)
            Dim item As Ad = New Ad()
            item.PageName = pageName
            item.ListOrder = listOrder
            item.Placement = placement
            item.AdText = adText
            item.ProductSku = productSku
            item.PromoID = promoID
            item.CategoryID = categoryID
            item.DateExpires = dateExpires
            item.IsActive = isActive
            item.IsDeleted = isDeleted

            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub


        ''' <summary>
        ''' Updates a record, can be used with the Object Data Source
        ''' </summary>
        Public Shared Sub Update(ByVal adID As Integer, ByVal pageName As String, ByVal listOrder As Integer, ByVal placement As String, ByVal adText As String, ByVal productSku As String, ByVal promoID As Integer, ByVal categoryID As Integer, ByVal dateExpires As DateTime, ByVal isActive As Boolean, ByVal isDeleted As Boolean)
            Dim item As Ad = New Ad()
            item.AdID = adID
            item.PageName = pageName
            item.ListOrder = listOrder
            item.Placement = placement
            item.AdText = adText
            item.ProductSku = productSku
            item.PromoID = promoID
            item.CategoryID = categoryID
            item.DateExpires = dateExpires
            item.IsActive = isActive
            item.IsDeleted = isDeleted

            item.IsNew = False
            item.Save(System.Web.HttpContext.Current.User.Identity.Name)
        End Sub

#End Region

#Region "Columns Struct"
        Public Structure Columns
            Public Shared AdID As String
            Public Shared PageName As String
            Public Shared ListOrder As String
            Public Shared Placement As String
            Public Shared AdText As String
            Public Shared ProductSku As String
            Public Shared PromoID As String
            Public Shared CategoryID As String
            Public Shared DateExpires As String
            Public Shared IsActive As String
            Public Shared IsDeleted As String
            Public Shared CreatedOn As String
            Public Shared CreatedBy As String
            Public Shared ModifiedOn As String
            Public Shared ModifiedBy As String
            'LJD 10/17/07 Added Public Event dummy()
            Public Event dummy()


            Shared Sub New()
                AdID = "adID"
                PageName = "pageName"
                ListOrder = "listOrder"
                Placement = "placement"
                AdText = "adText"
                ProductSku = "productSku"
                PromoID = "promoID"
                CategoryID = "categoryID"
                DateExpires = "dateExpires"
                IsActive = "isActive"
                IsDeleted = "isDeleted"
                CreatedOn = "createdOn"
                CreatedBy = "createdBy"
                ModifiedOn = "modifiedOn"
                ModifiedBy = "modifiedBy"
            End Sub
        End Structure
#End Region

    End Class
End Namespace
