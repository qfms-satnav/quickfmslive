#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic
Imports System.Web

Namespace Commerce.Common
    ''' <summary>
    ''' A Persistable class that uses Generics to store it's state
    ''' in the database. This class maps to the CSK_Store_Order table.
    ''' </summary>
    Partial Public Class Order
        Inherits ActiveRecord(Of Order)

#Region "custom .ctors"
        ''' <summary>
        ''' Sets the static Table property from our Base class. This property tells
        ''' the base class how to create the CRUD queries, etc.
        ''' </summary>

        Public Sub New(ByVal orderID As String)
            SetSQLProps()
            'override the default implementation since orderID is a GUID
            Dim q As Query = New Query(Schema)
            q.AddWhere("orderID", orderID)
            Dim rdr As IDataReader = q.ExecuteReader()
            Load(rdr)
        End Sub

#End Region

#Region "Custom - not in DB"

        Private _lastAdded As OrderItem

        Public Property LastAdded() As OrderItem
            Get
                Return _lastAdded
            End Get
            Set(value As OrderItem)
                _lastAdded = value
            End Set
        End Property

        Private currencyDecimals As Integer = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits
        ''' <summary>
        ''' This is a calculated field that's also stored in the DB for
        ''' Reporting purposes.
        ''' </summary>
        Public Function CalculateSubTotal() As Decimal
            Dim dOut As Decimal = 0
            If Not Me.Items Is Nothing Then

                For Each item As OrderItem In Me.Items
                    dOut += item.LineTotal
                Next item

                dOut = Math.Round(dOut, currencyDecimals)

                'apply any discounts
                If dOut >= Me.DiscountAmount Then
                    dOut -= Me.DiscountAmount
                Else
                    dOut = 0
                End If
            Else
                'Added By Rakesh on 11th March
                Try
                    Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GMR_GetSubTotalAmountByOrderId")
                    sp.Command.AddParameter("@OrderId", Me.OrderID, DbType.Int64)
                    dOut = sp.ExecuteScalar()
                Catch ex As Exception
                    dOut = 0
                End Try
            End If
            'using this for the save method
            Return dOut
        End Function
        ''' <summary>
        ''' This is a Added By Rakesh on 15Jan 2010 To show total in Basket Page
        ''' Reporting purposes.
        ''' </summary>
        Public Function CalculateSubTotal_Custom() As Decimal

            Dim dOut As Decimal = 0
            If Not Me.Items Is Nothing Then

                For Each item As OrderItem In Me.Items
                    'dOut += item.LineTotal
                    dOut += item.LineTotal_Custom
                Next item

                dOut = Math.Round(dOut, currencyDecimals)

                'apply any discounts
                dOut -= Me.DiscountAmount
            Else
                dOut = 0
            End If



            'using this for the save method
            Return dOut


        End Function
        ''' <summary>
        ''' Delete's a pending order from the system. This will not work
        ''' for orders that have already been paid/cancelled/refunded
        ''' </summary>
        Public Sub DeletePermanent()
            Dim coll As QueryCommandCollection = New QueryCommandCollection()
            If Me.OrderStatus = OrderStatus.NotProcessed Then
                'in order - notes, transactions, items, order
                'notes
                Dim q As Query = New Query(OrderNote.GetTableSchema())
                q.AddWhere("orderID", OrderID)
                coll.Add(q.BuildDeleteCommand())

                'transactions
                q = New Query(Transaction.GetTableSchema())
                q.AddWhere("orderID", OrderID)
                coll.Add(q.BuildDeleteCommand())

                'items
                q = New Query(OrderItem.GetTableSchema())
                q.AddWhere("orderID", OrderID)
                coll.Add(q.BuildDeleteCommand())

                q = New Query(Schema)
                q.AddWhere("orderID", OrderID)
                coll.Add(q.BuildDeleteCommand())

                DataService.ExecuteTransaction(coll)

            End If
        End Sub
        Public Sub SaveItems()

            'queue up a SQL batch of the existing items
            'and save them all at once
            Dim coll As QueryCommandCollection = New QueryCommandCollection()

            'first, delete out all the existing items
            Dim qry As Query = New Query(OrderItem.GetTableSchema())
            qry.AddWhere("orderID", Me.OrderID)
            qry.QueryType = QueryType.Delete
            coll.Add(qry.BuildCommand())
            Dim insertItemCommand As QueryCommand = Nothing

            For Each item As OrderItem In Me.Items
                insertItemCommand = item.GetInsertCommand(Utility.GetUserName())
                coll.Add(insertItemCommand)

            Next item

            DataService.ExecuteTransaction(coll)
        End Sub
        Public Function GetItemsWeight() As Decimal
            Dim dOut As Decimal = 0
            Dim dQty As Decimal = 0
            Dim dWeight As Decimal = 0

            For Each item As OrderItem In Me.Items
                dQty = Convert.ToDecimal(item.Quantity)
                dWeight = item.Weight
                dOut += dQty * dWeight
            Next item
            Return dOut
        End Function
        Public Function GetTotalWidth() As Decimal
            Dim dOut As Decimal = 0
            Dim dQty As Decimal = 0
            Dim dWidth As Decimal = 0

            For Each item As OrderItem In Me.Items
                dQty = Convert.ToDecimal(item.Quantity)
                dWidth = item.Width
                dOut += dQty * dWidth
            Next item
            Return dOut
        End Function
        Public Function GetTotalHeight() As Decimal
            Dim dOut As Decimal = 0
            Dim dQty As Decimal = 0
            Dim dHeight As Decimal = 0

            For Each item As OrderItem In Me.Items
                dQty = Convert.ToDecimal(item.Quantity)
                dHeight = item.Height
                dOut += dQty * dHeight
            Next item
            Return dOut
        End Function
        Public Function GetMaxLength() As Decimal
            Dim dOut As Decimal = 0
            Dim dQty As Decimal = 0
            Dim dLength As Decimal = 0

            For Each item As OrderItem In Me.Items
                dQty = Convert.ToDecimal(item.Quantity)
                dLength = item.Length
                dOut += dQty * dLength
            Next item
            Return dOut
        End Function

        Public ReadOnly Property OrderTotal() As Decimal
            Get
                Dim dSub As Decimal = Me.CalculateSubTotal()
                'If dSub <= 0 Then
                '    dSub = Me.SubTotalAmount
                'End If

                Return dSub + Me.TaxAmount + Me.ShippingAmount + Me.ServiceChargeAmount + Me.ServiceTaxAmount + Me.HandlingAmount
                'Return Math.Round((Me.SubTotalAmount + Me.TaxAmount + Me.ShippingAmount + Me.ServiceChargeAmount + Me.ServiceTaxAmount), 0) '& Me.ShippingAmount
            End Get
        End Property

#Region "Credit Card Information"
        <NonSerialized(), System.Xml.Serialization.XmlIgnore()> _
        Private _CreditCardNumber As String
        <NonSerialized(), System.Xml.Serialization.XmlIgnore()> _
        Private _CrediCartExpYear As Integer
        <NonSerialized(), System.Xml.Serialization.XmlIgnore()> _
        Private _CreditCardExpireMonth As Integer
        <NonSerialized(), System.Xml.Serialization.XmlIgnore()> _
        Private _CreditCardSecurityNumber As String
        <NonSerialized(), System.Xml.Serialization.XmlIgnore()> _
        Private _CreditCardType As CreditCardType

        '[NonSerialized()]
        <System.Xml.Serialization.XmlIgnore()> _
        Public Property CreditCardNumber() As String
            Get
                Return _CreditCardNumber
            End Get
            Set(value As String)
                _CreditCardNumber = value
            End Set
        End Property

        '[NonSerialized()]
        <System.Xml.Serialization.XmlIgnore()> _
        Public Property CreditCardExpireYear() As Integer
            Get
                Return _CrediCartExpYear
            End Get
            Set(value As Integer)
                _CrediCartExpYear = value
            End Set
        End Property

        '[NonSerialized()]
        <System.Xml.Serialization.XmlIgnore()> _
        Public Property CreditCardExpireMonth() As Integer
            Get
                Return _CreditCardExpireMonth
            End Get
            Set(value As Integer)
                _CreditCardExpireMonth = value
            End Set
        End Property

        '[NonSerialized()]
        <System.Xml.Serialization.XmlIgnore()> _
        Public Property CreditCardSecurityNumber() As String
            Get
                Return _CreditCardSecurityNumber
            End Get
            Set(value As String)
                _CreditCardSecurityNumber = value
            End Set
        End Property


        '[NonSerialized()]
        <System.Xml.Serialization.XmlIgnore()> _
        Public Property CreditCardType() As CreditCardType
            Get
                Return _CreditCardType
            End Get
            Set(value As CreditCardType)
                _CreditCardType = value
            End Set
        End Property


#End Region


        Public Property PONumber() As String
            Get
                Return _PONumber
            End Get
            Set(value As String)
                _PONumber = value
            End Set
        End Property

        'Not currently implemented. Stubbed 
        'for future use.
        Public Property TaxExemptionCode() As String
            Get
                Return _TaxExemptionCode
            End Get
            Set(value As String)
                _TaxExemptionCode = value
            End Set
        End Property
        <XmlIgnore> _
        Public Items As OrderItemCollection
        Public ShippingAddress As Address
        Public BillingAddress As Address
        <XmlIgnore> _
        Public Transactions As TransactionCollection
        <XmlIgnore> _
        Public Notes As OrderNoteCollection


        Private _PONumber As String
        Private _TaxExemptionCode As String
#End Region

#Region "ToString() Override"
        Public Overrides Function ToString() As String
            Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder()

            'override the base class to output a nice text rendering
            sb.Append("********* ORDER " & Me.OrderNumber & " ************************")
            sb.Append("Bill To:" & Constants.vbCrLf & Me.BillToAddress & Constants.vbCrLf & Constants.vbCrLf)
            sb.Append("Ship To:" & Constants.vbCrLf & Me.ShipToAddress & Constants.vbCrLf & Constants.vbCrLf)

            If Me.Transactions.Count > 0 Then
                'sb.Append("Payment Method: " & Me.Transactions(0).TransactionType.ToString() & Constants.vbCrLf & Constants.vbCrLf)
                Dim TranType As String = String.Empty
                If Me.Transactions(0).TransactionType = TransactionType.CashOnDelivery Then
                    TranType = "COD"
                Else
                    TranType = "Online Payment"
                End If
                sb.Append("Payment Method: " & TranType & Constants.vbCrLf & Constants.vbCrLf)
            End If
            sb.Append(ItemsToString(False) & Constants.vbCrLf & Constants.vbCrLf)
            sb.Append("More Info: " & Utility.GetSiteRoot() & "/admin/admin_orders_details.aspx?id=" & Me.OrderID)
            Return sb.ToString()
        End Function
        Public Function ToHtml() As String
            Dim sb As System.Text.StringBuilder = New StringBuilder()
            sb.Append("<table width=600 cellpadding=0 cellspacing=0 >")
            sb.Append("<tr><td colspan=2 style='height:20;background-color:whitesmoke;border:1px solid #cccccc;font-size:14px;padding:10px'><b>Order Number " & Me.OrderNumber & "</b></td></tr>")
            sb.Append("<tr><td colspan=2 style='padding:10px'><font color=#990000><b>Status:</b> " & Utility.ParseCamelToProper(System.Enum.GetName(GetType(OrderStatus), Me.OrderStatus)) & "</font><br>")
            sb.Append("<b>Date:</b> " & Me.OrderDate.ToShortDateString() & "<br><br></td></tr>")
            If Me.Transactions.Count > 0 Then
                sb.Append("<tr><td colspan=2><font size=2>&nbsp;&nbsp;<b>Payment Information</b></font></td></tr>")
                sb.Append("<tr><td colspan=2 style='border-top:1px solid #cccccc'>&nbsp;</td></tr>")
                sb.Append("<tr><td colspan=2 style='padding:10px'><b>Transaction Number:</b> " & Me.Transactions(0).AuthorizationCode & "<br>")
                'sb.Append("<b>Payment Method:</b> " & Me.Transactions(0).TransactionType.ToString() & "<br><br><br><br></td></tr>")
                Dim TranType As String = String.Empty
                If Me.Transactions(0).TransactionType = TransactionType.CashOnDelivery Then
                    TranType = "COD"
                Else
                    TranType = "Online Payment"
                End If
                sb.Append("<b>Payment Method:</b> " & TranType & "<br><br><br><br></td></tr>")
            End If
            sb.Append("<tr><td colspan=2>&nbsp;&nbsp;<font size=2><b>Shipping And Billing</b></font></td></tr>")
            sb.Append("<tr><td colspan=2 style='border-top:1px solid #cccccc'>&nbsp;</td></tr>")
            sb.Append("<tr><td width=50% style='padding:10px' valign=top><b>Bill To:</b><br>" & Me.BillToAddress & "</td><td style='padding:10px' valign=top><b>Ship To:</b><br>" & Me.ShipToAddress.ToString() & "<br><br><br><br></td></tr>")
            sb.Append("<tr><td colspan=2>&nbsp;&nbsp;<font size=2><b>Order Items</b></font></td></tr>")
            sb.Append("<tr><td colspan=2 style='border-top:1px solid #cccccc'>&nbsp;</td></tr>")
            sb.Append("<tr><td colspan=2 style='padding:10px'>" & Me.ItemsToString(True) & "</td></tr>")
            sb.Append("</table>")

            Return sb.ToString()


        End Function
        'Public Function ItemsToString(ByVal UseHtml As Boolean) As String

        '	Dim sOut As String = ""
        '	If UseHtml Then
        '		sOut = ""
        '		Dim sb As StringBuilder = New StringBuilder()

        '		sb.Append("<table width=100% cellpadding=4 cellspacing=0>")
        '		sb.Append("<tr style='border:1px solid #cccccc; background-color:whitesmoke'><td><b>Item Number</b></td><td><b>Item</b></td><td><b>Quantity</b></td><td><b>Price</b></td><td><b>Total<b></td></tr>")

        '		Dim appendFormat As String = "<tr bgcolor=#ffffff><Td>{0}</td><td>{1}</td><td>{2}</td><td align=right>{3}</td><td align=right>{4}</td></tr>"
        '		Dim appendAltFormat As String = "<tr bgcolor=#f5f5f5><Td>{0}</td><td>{1}</td><td>{2}</td><td align=right>{3}</td><Td align=right>{4}</td></tr>"
        '		Dim formatToUse As String = appendFormat

        '		Dim isEven As Boolean = True

        '		For Each item As OrderItem In Me.Items

        '			If isEven Then
        '				formatToUse = appendFormat
        '			Else
        '				formatToUse = appendAltFormat
        '			End If
        '			Dim attSelections As String = ""
        '			If Not item.Attributes Is String.Empty Then
        '				attSelections = "<br>" & item.Attributes
        '			End If

        '                  sb.AppendFormat(formatToUse, item.Sku, item.ProductName & " " & attSelections, item.Quantity.ToString(), "Rs." & FormatNumber(item.PricePaid, 2), "Rs." & FormatNumber(item.LineTotal, 2))
        '			isEven = Not isEven


        '		Next item
        '		'append the totals
        '		Dim subTotal As Decimal = Me.CalculateSubTotal()

        '		If Me.DiscountAmount > 0 Then
        '			Dim originalAmount As Decimal = subTotal+Me.DiscountAmount
        '                  sb.Append("<tr><td colspan=4 align=right><b>Item Total:</td><td align=right>" & "Rs." & FormatNumber(originalAmount, 2) & "</td></tr>")
        '                  sb.Append("<tr><td colspan=4 align=right><b>Discount:</td><td align=right style='color:#990000'>(" & "Rs." & FormatNumber(Me.DiscountAmount, 2) & ")</td></tr>")
        '                  sb.Append("<tr><td colspan=4 align=right><b>Subtotal:</td><td align=right>" & "Rs." & FormatNumber(subTotal, 2) & "</td></tr>")

        '		Else
        '                  sb.Append("<tr><td colspan=4 align=right><b>Subtotal:</td><td align=right>" & "Rs." & FormatNumber(subTotal, 2) & "</td></tr>")

        '		End If


        '              sb.Append("<tr><td colspan=4 align=right><b>Tax:</td><td align=right>" & "Rs." & FormatNumber(Me.TaxAmount, 2) & "</td></tr>")
        '              sb.Append("<tr><td colspan=4 align=right><b>Shipping:</td><td align=right>" & "Rs." & FormatNumber(Me.ShippingAmount, 2) & "</td></tr>")
        '              sb.Append("<tr><td colspan=4 align=right><b><font color:#990000>Total:</font></td><td align=right><font color:#990000>" & "Rs." & FormatNumber(Math.Round(Me.OrderTotal, 0), 2) & "</font></td></tr>")

        '		sb.Append("</table>")
        '		sOut = sb.ToString()
        '	Else
        '		sOut = GetASCII()
        '	End If
        '	Return sOut
        '      End Function
        Public Function ItemsToString(ByVal UseHtml As Boolean) As String

            Dim sOut As String = ""
            If UseHtml Then
                sOut = ""
                Dim sb As StringBuilder = New StringBuilder()

                sb.Append("<table width=100% cellpadding=4 cellspacing=0>")
                sb.Append("<tr style='border:1px solid #cccccc; background-color:whitesmoke'><td><b>Item Number</b></td><td><b>Item</b></td><td><b>Quantity</b></td><td><b>Price</b></td><td><b>Total<b></td></tr>")

                Dim appendFormat As String = "<tr bgcolor=#ffffff><Td>{0}</td><td>{1}</td><td>{2}</td><td align=right>{3}</td><td align=right>{4}</td></tr>"
                Dim appendAltFormat As String = "<tr bgcolor=#f5f5f5><Td>{0}</td><td>{1}</td><td>{2}</td><td align=right>{3}</td><td align=right>{4}</td></tr>"
                Dim formatToUse As String = appendFormat

                Dim isEven As Boolean = True

                For Each item As OrderItem In Me.Items

                    If isEven Then
                        formatToUse = appendFormat
                    Else
                        formatToUse = appendAltFormat
                    End If
                    Dim attSelections As String = ""
                    If Not item.Attributes Is String.Empty Then
                        attSelections = "<br>" & item.Attributes
                    End If
                    If item.DiscountAmt > 0 Then
                        sb.AppendFormat(formatToUse, item.Sku, item.ProductDescription & " " & attSelections, item.Quantity.ToString(), "Rs." & FormatNumber(item.PricePaid, 2), "Rs." & FormatNumber(item.LineTotal, 2))
                    Else
                        sb.AppendFormat(formatToUse, item.Sku, item.ProductDescription & " " & attSelections, item.Quantity.ToString(), "Rs." & FormatNumber(item.PricePaid, 2), "Rs." & FormatNumber(item.LineTotal, 2))
                    End If

                    isEven = Not isEven


                Next item
                'append the totals
                Dim subTotal As Decimal = Me.CalculateSubTotal()
                If Me.DiscountAmount > 0 Then
                    Dim originalAmount As Decimal = subTotal + Me.DiscountAmount
                    sb.Append("<tr><td colspan=4 align=right><b>Item Total:</td><td align=right>" & "Rs." & FormatNumber(originalAmount, 2) & "</td></tr>")
                    sb.Append("<tr><td colspan=4 align=right><b>Discount:</td><td align=right style='color:#990000'>(" & "- Rs." & FormatNumber(Me.DiscountAmount, 2) & ")</td></tr>")
                    sb.Append("<tr><td colspan=4 align=right><b>Subtotal:</td><td align=right>" & "Rs." & FormatNumber(subTotal, 2) & "</td></tr>")
                Else
                    sb.Append("<tr><td colspan=4 align=right><b>Subtotal:</td><td align=right>" & "Rs." & FormatNumber(subTotal, 2) & "</td></tr>")
                End If
                If Me.ShippingAmount > 0 Then
                    sb.Append("<tr><td colspan=4 align=right><b>Shipping:</td><td align=right>" & "Rs." & FormatNumber(Me.ShippingAmount, 2) & "</td></tr>")
                End If
                If Me.HandlingAmount > 0 Then
                    'sb.Append("<tr><td colspan=4 align=right><b>COD:</td><td align=right>" & "Rs." & FormatNumber(Me.HandlingAmount, 2) & "</td></tr>")
                End If
                If Me.TaxAmount > 0 Then
                    sb.Append("<tr><td colspan=4 align=right><b>VAT/CST:</td><td align=right>" & "Rs." & FormatNumber(Me.TaxAmount, 2) & "</td></tr>")
                End If
                If Me.ServiceChargeAmount > 0 Then
                    sb.Append("<tr><td colspan=4 align=right><b>Service Charge:</td><td align=right>" & "Rs." & FormatNumber(Me.ServiceChargeAmount, 2) & "</td></tr>")
                End If
                If Me.ServiceTaxAmount > 0 Then
                    sb.Append("<tr><td colspan=4 align=right><b>Service Tax:</td><td align=right>" & "Rs." & FormatNumber(Me.ServiceTaxAmount, 2) & "</td></tr>")
                End If
                If Me.OrderTotal > 0 Then
                    sb.Append("<tr><td colspan=4 align=right><b><font color:#990000>Total:</font></td><td align=right><font color:#990000>" & "Rs. " & FormatNumber(Math.Round(Me.OrderTotal, 0), 2) & "</font></td></tr>")
                Else
                    sb.Append("<tr><td colspan=4 align=right><b><font color:#990000>Total:</font></td><td align=right><font color:#990000>" & "Rs. 0.0" & "</font></td></tr>")
                End If
                sb.Append("</table>")
                sOut = sb.ToString()
            Else
                sOut = GetASCII()
            End If
            Return sOut
        End Function
        Public Function InvoiceItemsToString(ByVal UseHtml As Boolean) As String

            Dim sOut As String = ""
            If UseHtml Then
                sOut = ""
                Dim sb As StringBuilder = New StringBuilder()

                sb.Append("<table width=100% cellpadding=4 cellspacing=0>")
                sb.Append("<tr style='border:1px solid #cccccc; background-color:whitesmoke'><td><b>Item Number</b></td><td><b>Item</b></td><td><b>Quantity</b></td><td><b>Price</b></td><td><b>Total<b></td></tr>")

                Dim appendFormat As String = "<tr bgcolor=#ffffff><Td>{0}</td><td>{1}</td><td>{2}</td><td align=right>{3}</td><td align=right>{4}</td></tr>"
                Dim appendAltFormat As String = "<tr bgcolor=#f5f5f5><Td>{0}</td><td>{1}</td><td>{2}</td><td align=right>{3}</td><Td align=right>{4}</td></tr>"
                Dim formatToUse As String = appendFormat

                Dim isEven As Boolean = True

                For Each item As OrderItem In Me.Items

                    If isEven Then
                        formatToUse = appendFormat
                    Else
                        formatToUse = appendAltFormat
                    End If
                    Dim attSelections As String = ""
                    If Not item.Attributes Is String.Empty Then
                        attSelections = "<br>" & item.Attributes
                    End If

                    sb.AppendFormat(formatToUse, item.Sku, item.ProductDescription & " " & attSelections, item.Quantity.ToString(), "Rs." & FormatNumber(item.PricePaid, 2), "Rs." & FormatNumber(item.LineTotal, 2))
                    isEven = Not isEven


                Next item
                'append the totals
                Dim subTotal As Decimal = Me.CalculateSubTotal()

                'If Me.DiscountAmount >= 0 Then

                '    'Else
                '    '    sb.Append("<tr><td colspan=4 align=right><b>Subtotal:</td><td align=right>" & "Rs." & FormatNumber(subTotal, 2) & "</td></tr>")

                'End If
                Dim originalAmount As Decimal = subTotal + Me.DiscountAmount
                sb.Append("<tr><td colspan=4 align=right><b>Item Total:</td><td align=right>" & "Rs." & FormatNumber(originalAmount, 2) & "</td></tr>")
                sb.Append("<tr><td colspan=4 align=right><b>Subtotal:</td><td align=right>" & "Rs." & FormatNumber(subTotal, 2) & "</td></tr>")
                sb.Append("<tr><td colspan=4 align=right><b>Discount:</td><td align=right style='color:#990000'>(" & "Rs." & FormatNumber(Me.DiscountAmount, 2) & ")</td></tr>")
                sb.Append("<tr><td colspan=4 align=right><b>Shipping:</td><td align=right>" & "Rs." & FormatNumber(Me.ShippingAmount, 2) & "</td></tr>")
                'sb.Append("<tr><td colspan=4 align=right><b>COD:</td><td align=right>" & "Rs." & FormatNumber(Me.HandlingAmount, 2) & "</td></tr>")
                sb.Append("<tr><td colspan=4 align=right><b>VAT/CST:</td><td align=right>" & "Rs." & FormatNumber(Me.TaxAmount, 2) & "</td></tr>")
                sb.Append("<tr><td colspan=4 align=right><b>Service Charge:</td><td align=right>" & "Rs." & FormatNumber(Me.ServiceChargeAmount, 2) & "</td></tr>")
                sb.Append("<tr><td colspan=4 align=right><b>Service Tax:</td><td align=right>" & "Rs." & FormatNumber(Me.ServiceTaxAmount, 2) & "</td></tr>")
                sb.Append("<tr><td colspan=4 align=right><b><font color:#990000>Total:</font></td><td align=right><font color:#990000>" & "Rs." & FormatNumber(Math.Round(Me.OrderTotal, 0), 2) & "</font></td></tr>")

                sb.Append("</table>")
                sOut = sb.ToString()
            Else
                sOut = GetASCII()
            End If
            Return sOut
        End Function
        ''' <summary>
        ''' Helper for the output of the string
        ''' </summary>
        ''' <returns></returns>
        Private Function GetASCII() As String
            Dim sb As StringBuilder = New StringBuilder()
            Dim runningTotal As Decimal = 0
            For Each item As OrderItem In Me.Items
                runningTotal += item.LineTotal
                sb.AppendLine("Item        :" & item.ProductName)
                sb.AppendLine("Quantity    :" & item.Quantity.ToString())
                sb.AppendLine("Price Paid  :" & item.PricePaid.ToString("C"))
                sb.AppendLine("")
                sb.AppendLine("")
            Next item
            sb.AppendLine("")
            sb.AppendLine("========================================")
            sb.AppendLine("Grand Total: " & runningTotal.ToString("C"))
            Return sb.ToString()
        End Function
#End Region

#Region "Public Props"
        'enum
        Public Property OrderStatus() As OrderStatus
            Get
                Return CType(Me.GetColumnValue("orderStatusID"), OrderStatus)
            End Get
            Set(value As OrderStatus)
                Me.MarkDirty()
                Me.SetColumnValue("orderStatusID", value)
            End Set
        End Property
#End Region

    End Class
End Namespace
