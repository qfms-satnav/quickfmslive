#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic

Namespace Commerce.Common
	''' <summary>
	''' A Persistable class that uses Generics to store it's state
	''' in the database. This class maps to the CSK_Store_Transaction table.
	''' </summary>
	Public Partial Class Transaction
		Inherits ActiveRecord(Of Transaction)

		#Region "Custom - not in DB"
		Private _CVV2Code As String

		Public Property CVV2Code() As String
			Get
				Return _CVV2Code
			End Get
			Set
				_CVV2Code = Value
			End Set
		End Property

		Private _GatewayResponse As String

		Public Property GatewayResponse() As String
			Get
				Return _GatewayResponse
			End Get
			Set
				_GatewayResponse = Value
			End Set
		End Property
		'enum
		Public Property TransactionType() As TransactionType
			Get
				Return CType(Me.GetColumnValue("transactionTypeID"), TransactionType)
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("transactionTypeID", Value)
			End Set
		End Property
		#End Region

	End Class
End Namespace
