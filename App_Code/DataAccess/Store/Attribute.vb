#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Xml
Imports System.Xml.Serialization

Namespace Commerce.Common

	''' <summary>
	''' A collection of attributes. 
	''' </summary>
	<Serializable> _
	Public Partial Class Attributes
		Inherits System.Collections.CollectionBase

		Public Sub Add(ByVal att As Attribute)
			List.Add(att)
		End Sub

		Public Sub Remove(ByVal itemIndex As Integer)
			List.RemoveAt(itemIndex)
		End Sub
		''' <summary>
		''' Indexer for this collection. Returns the CSK_Store_Attribute object in the specified position.
		''' </summary>
		Public ReadOnly Default Property Item(ByVal index As Integer) As Attribute
			Get
				Return CType(List(index), Attribute)
			End Get
		End Property

		Public Overrides Function ToString() As String
			Dim attSelections As String = ""
			For Each att As Attribute In Me
				attSelections &= "<b>" & att.Name & "</b>: " & att.SelectionList & "<br>"
			Next att
			Return attSelections

		End Function
		Public Function ToXML() As String
			Return Utility.ObjectToXML(GetType(Attributes), Me)
		End Function

	End Class

	''' <summary>
	''' A class that describes a product attribute, such as "Size"
	''' </summary>
	<Serializable> _
	Public Partial Class Attribute
		Private name_Renamed As String

		Public Property Name() As String
			Get
				Return name_Renamed
			End Get
			Set
				name_Renamed = Value
			End Set
		End Property
		Private description_Renamed As String

		Public Property Description() As String
			Get
				Return description_Renamed
			End Get
			Set
				description_Renamed = Value
			End Set
		End Property
		Public Function GetPriceAdjustment(ByVal selection As String) As Decimal
			Dim dOut As Decimal=0
			For Each sel As AttributeSelection In Me.selections_Renamed
				If sel.Value.ToLower().Equals(selection.ToLower()) Then
					dOut=sel.PriceAdjustment
					Exit For
				End If
			Next sel
			Return dOut
		End Function
		Public ReadOnly Property SelectionList() As String
			Get
				Dim sOut As String = ""
				If Not Me.selections_Renamed Is Nothing Then
					For Each sel As AttributeSelection In Me.selections_Renamed
						sOut &= sel.Value & ", "
					Next sel
					If sOut.Length > 1 Then
						sOut = sOut.Remove(sOut.Length - 2, 2)
					End If
				End If
				Return sOut
			End Get
		End Property
		Private selections_Renamed As List(Of AttributeSelection)

		Public Property Selections() As List(Of AttributeSelection)
			Get
				Return selections_Renamed
			End Get
			Set
				selections_Renamed = Value
			End Set
		End Property


		Private selectionType_Renamed As AttributeType

		Public Property SelectionType() As AttributeType
			Get
				Return selectionType_Renamed
			End Get
			Set
				selectionType_Renamed = Value
			End Set
		End Property

	End Class

	''' <summary>
	''' A class representing the selections for an attribute. Example: "Large", "Small", etc
	''' </summary>
	<Serializable> _
	Public Partial Class AttributeSelection
		Private selectionValue As String
		Public Property FormattedValue() As String
			Get
				'if there is a price adjustment we want to show it
				'you can alter this however needed.
				Dim sOut As String = selectionValue
				If priceAdjustment_Renamed <> 0 Then
					sOut &= " "
					If priceAdjustment_Renamed > 0 Then
                        sOut &= ""
					End If
                    sOut &= "Rs. " & FormatNumber(priceAdjustment_Renamed, 2)

				End If
				Return sOut
            End Get
			Set
				selectionValue = Value
			End Set
		End Property
		Public Property Value() As String
			Get
				Return selectionValue

			End Get
			Set
				selectionValue = Value
			End Set
		End Property

		Private imageFile_Renamed As String

		Public Property ImageFile() As String
			Get
				Return imageFile_Renamed
			End Get
			Set
				imageFile_Renamed = Value
			End Set
		End Property


		Private priceAdjustment_Renamed As Decimal

		Public Property PriceAdjustment() As Decimal
			Get
				Return priceAdjustment_Renamed
			End Get
			Set
				priceAdjustment_Renamed = Value

			End Set
		End Property

	End Class
End Namespace
