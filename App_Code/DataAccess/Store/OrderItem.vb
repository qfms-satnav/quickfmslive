#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic

Namespace Commerce.Common
	Public Partial Class OrderItemCollection
		Inherits ActiveList(Of OrderItem)
		Public Function FindItem(ByVal item As OrderItem) As OrderItem
			Dim itemOut As OrderItem = Nothing
			For Each child As OrderItem In Me
				If child.ProductID = item.ProductID Then
					itemOut = child
					Exit For
				End If
			Next child
			Return itemOut
		End Function

		Public Shadows Function Contains(ByVal item As OrderItem) As Boolean
			Dim bOut As Boolean = Not Find(item.ProductID) Is Nothing
			Return bOut
		End Function

        Public Overridable Overloads Function Find(ByVal productID As Integer) As OrderItem
            'LJD 10/17/07 Added Overridable Overloads before function
            Dim result As OrderItem = Nothing
            For Each child As OrderItem In Me
                If child.ProductID = productID Then
                    result = child
                    Exit For
                End If
            Next child
            Return result
        End Function
    End Class

	''' <summary>
	''' A Persistable class that uses Generics to store it's state
	''' in the database. This class maps to the CSK_Store_OrderItem table.
	''' </summary>
	Public Partial Class OrderItem
		Inherits ActiveRecord(Of OrderItem)

		#Region "Custom - not in DB"
		Private currencyDecimals As Integer = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits

		Public ReadOnly Property LineTotal() As Decimal
			Get
				Dim dOut As Decimal = Convert.ToDecimal(Me.Quantity) * Me.PricePaid
				Return Math.Round(dOut, currencyDecimals)
			End Get
        End Property

        'Added By Rakesh
        Public ReadOnly Property LineTotal_Custom() As Decimal
            Get
                Dim dOut As Decimal = Convert.ToDecimal(Me.Quantity) * Me.OriginalPrice
                Return Math.Round(dOut, currencyDecimals)
            End Get
        End Property
        'End
		Private _discountAmount As Decimal

		Public Property DiscountAmount() As Decimal
			Get
				Return _discountAmount
			End Get
			Set
				_discountAmount = Value
			End Set
		End Property

		Private _dateAdded As DateTime

		Public Property DateAdded() As DateTime
			Get
				Return _dateAdded
			End Get
			Set
				_dateAdded = Value
			End Set
		End Property
		#End Region


	End Class
End Namespace
