#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports System.Data
Imports Commerce.Common
Imports SubSonic


Namespace Commerce.Promotions
	Public Class CouponType
		Private Shared _couponTypes As System.Collections.Generic.Dictionary(Of Integer, CouponType)

		''' <summary>
		''' Gets the CouponType by the coupon type id
		''' </summary>
		''' <param name="CouponTypeId">ID for the coupon type</param>
		''' <returns>The CouponType corresponding to the type id supplied</returns>
		''' <exception cref="System.ArgumentException">Thrown if the supplied type is not found.</exception>
		Public Shared Function GetCouponType(ByVal CouponTypeId As Integer) As CouponType
			If _couponTypes Is Nothing Then
				GetAllCouponTypes()
			End If
			If _couponTypes.ContainsKey(CouponTypeId) Then
				Return _couponTypes(CouponTypeId)
			Else
				'Not found in cache.  Retrieve from the database.  
				'TODO: Integrate with standard DataAccess 
				Using rdr As IDataReader = GetCouponTypeReader(CouponTypeId)
					If rdr.Read() Then
						Return CreateCouponTypeFromReader(rdr)
					Else
						'Coupon Type was not found.  
						Throw New ArgumentException("Specified coupon type was not found", "CouponTypeId")
					End If
				End Using
			End If
		End Function

		Private Shared Function CreateCouponTypeFromReader(ByVal rdr As IDataReader) As CouponType
			Dim newType As CouponType = New CouponType(rdr.GetInt32(rdr.GetOrdinal("CouponTypeId")), rdr.GetString(rdr.GetOrdinal("Description")), rdr.GetString(rdr.GetOrdinal("ProcessingClassName")))
			'Add to the cache.  

			_couponTypes.Add(newType.CouponTypeID, newType)
			Return newType
		End Function

		Public Shared Function GetAllCouponTypes() As System.Collections.Generic.Dictionary(Of Integer, CouponType)
			Return GetAllCouponTypes(False)
		End Function
		Public Shared Function GetAllCouponTypes(ByVal refreshCache As Boolean) As System.Collections.Generic.Dictionary(Of Integer, CouponType)
			If refreshCache OrElse _couponTypes Is Nothing Then
				'force refill of the cache
				_couponTypes = New Dictionary(Of Integer, CouponType)()
				Using rdr As IDataReader = GetCouponTypeReader()
					Do While rdr.Read()
						CreateCouponTypeFromReader(rdr)
					Loop
				End Using
			End If
			Return _couponTypes

		End Function
		Private Shared Function GetCouponTypeReader(ByVal CouponTypeId As Integer) As IDataReader

			Return SPs.CouponsGetCouponType(CouponTypeId).GetReader()

		End Function
		Private Shared Function GetCouponTypeReader() As IDataReader
			Return SPs.CouponsGetCouponType(0).GetReader()

		End Function


		Public Sub New()
			_couponTypeId = -1
		End Sub

		Public Sub New(ByVal couponTypeId As Integer, ByVal description As String, ByVal classTypeName As String)
			_couponTypeId = couponTypeId
			_description = description
			_couponClassType = System.Web.Compilation.BuildManager.GetType(classTypeName, True)

		End Sub
		Private _couponTypeId As Integer
		Public ReadOnly Property CouponTypeID() As Integer
			Get
				Return _couponTypeId
			End Get
		End Property

		Private _description As String
		Public Property Description() As String
			Get
				Return _description
			End Get
			Set
				_description = Value
			End Set
		End Property

		Private _couponClassType As Type
		Public Property CouponClassType() As Type
			Get
				Return _couponClassType
			End Get
			Set
				_couponClassType = Value
			End Set
		End Property

		Public Sub Save()

			SPs.CouponsSaveCouponType(Me.CouponTypeID, Me.Description, Me.CouponClassType.FullName).Execute()

		End Sub
	End Class

	Public Class CouponValidationResponse
		Public Sub New(ByVal isValid As Boolean, ByVal validationMessage As String)
			_isValid = isValid
			_validationMessage = validationMessage
		End Sub

		Private _isValid As Boolean
		Public ReadOnly Property IsValid() As Boolean
			Get
				Return _isValid
			End Get
		End Property

		Private _validationMessage As String
		Public ReadOnly Property ValidationMessage() As String
			Get
				Return _validationMessage
			End Get
		End Property

	End Class
	Public MustInherit Class Coupon

		Protected Sub New()

		End Sub
		Protected Sub New(ByVal couponCode As String, ByVal couponType As CouponType)
			_couponCode = couponCode
			_couponType = couponType
		End Sub

		Private _couponCode As String
		<XmlIgnore()> _
		Public Overridable Property CouponCode() As String
			Get
				Return _couponCode
			End Get
			Protected Set
				_couponCode = Value
			End Set
		End Property

		Private _couponType As CouponType
		<XmlIgnore()> _
		Public ReadOnly Property CouponType() As CouponType
			Get
				Return _couponType
			End Get

		End Property

		Private _isSingleUse As Boolean
		<XmlIgnore()> _
		Public Overridable Property IsSingleUse() As Boolean
			Get
				Return _isSingleUse
			End Get
			Set
				_isSingleUse = Value
			End Set
		End Property

		Private _userId As Nullable(Of Guid)
		<XmlIgnore()> _
		Public Overridable Property UserId() As Nullable(Of Guid)
			Get
				Return _userId
			End Get
			Set
				_userId = Value
			End Set
		End Property

		Private _numUses As Integer
		<XmlIgnore()> _
		Public Overridable Property NumberOfUses() As Integer
			Get
				Return _numUses
			End Get
			Protected Set
				_numUses = Value
			End Set
		End Property

		Private _expirationDate As Nullable(Of DateTime)
		<XmlIgnore()> _
		Public Overridable Property ExpirationDate() As Nullable(Of DateTime)
			Get
				Return _expirationDate
			End Get
			Set
				_expirationDate = Value
			End Set
		End Property

		''' <summary>
		''' Returns the Xml representation of coupon-specific data as a string
		''' </summary>
		''' <returns></returns>
		Public Function GetXmlData() As String
			'Create the serializer
			Dim ser As XmlSerializer = New XmlSerializer(Me.CouponType.CouponClassType)
			Using stm As System.IO.MemoryStream = New System.IO.MemoryStream()

				'serialize to a memory stream
				ser.Serialize(stm, Me)
				'reset to beginning so we can read it.  
				stm.Position = 0
				'Convert a string. 
				Using stmReader As System.IO.StreamReader = New System.IO.StreamReader(stm)
					Dim xmlData As String = stmReader.ReadToEnd()
					Return xmlData
				End Using
			End Using

		End Function

		''' <summary>
		''' Applies a coupon to an order
		''' </summary>
		''' <param name="order">Order to apply the coupon to</param>
		Public MustOverride Sub ApplyCouponToOrder(ByVal order As Commerce.Common.Order)

		''' <summary>
		''' Performs basic validation based on common coupon properties
		''' </summary>
		''' <param name="order">Order to validate</param>
		''' <returns></returns>
		Public Overridable Function ValidateCouponForOrder(ByVal order As Commerce.Common.Order) As CouponValidationResponse
			If IsSingleUse AndAlso NumberOfUses > 0 Then
				Return New CouponValidationResponse(False, "This coupon has already been used")
			End If

			If ExpirationDate.HasValue Then
				If ExpirationDate < DateTime.UtcNow Then
					Return New CouponValidationResponse(False, "This coupon has expired")
				End If
			End If

			'TODO: how to check the current user id?  

			Return New CouponValidationResponse(True, "")
		End Function

		''' <summary>
		''' Returns a coupon by the coupon code.  
		''' </summary>
		''' <param name="couponCode"></param>
		''' <returns></returns>
		''' <exception cref="System.ArgumentException">Thrown if the coupon cannot be found</exception>
		Public Shared Function GetCoupon(ByVal couponCode As String) As Coupon
			Return GetCoupon(couponCode, True)
		End Function
		Public Shared Function GetCoupon(ByVal couponCode As String, ByVal throwOnError As Boolean) As Coupon
			Using reader As IDataReader = GetCouponReader(couponCode)
				If reader.Read() Then
					Return CreateCoupon(reader)
				Else
					If throwOnError Then
						Throw New ArgumentException("Coupon specified was not found", "couponCode")
					Else
						Return Nothing
					End If
				End If
			End Using
		End Function
		Private Shared Function CreateCoupon(ByVal reader As IDataReader) As Coupon
			'Get the coupon type id.  
			Dim couponTypeId As Integer = reader.GetInt32(reader.GetOrdinal("CouponTypeId"))
			Dim couponType As CouponType = CouponType.GetCouponType(couponTypeId)

			'Get an XmlReader for the XmlData 
			Dim xmlString As String = reader.GetString(reader.GetOrdinal("XmlData"))
			Dim rdr As System.IO.StringReader = New System.IO.StringReader(xmlString)
			Dim xmlData As XmlReader = XmlReader.Create(rdr)

			'Deserialize the Xml to the object.  
			Dim serializer As XmlSerializer = New XmlSerializer(couponType.CouponClassType)
			Dim newCoupon As Coupon = CType(serializer.Deserialize(xmlData), Coupon)

			'Load the common properties. 
			newCoupon._couponCode = reader.GetString(reader.GetOrdinal("CouponCode"))
			newCoupon._couponType = couponType
			newCoupon._isSingleUse = reader.GetBoolean(reader.GetOrdinal("IsSingleUse"))
			newCoupon._numUses = reader.GetInt32(reader.GetOrdinal("NumberUses"))
			'the following is a stupid hack. 
			'it seems to me that a nullable type should just convert from DbNull ... 
			If Not reader("ExpirationDate") Is DBNull.Value Then
				newCoupon._expirationDate = CType(reader("ExpirationDate"), Nullable(Of DateTime))
			End If
			If Not reader("UserId") Is DBNull.Value Then
				newCoupon._userId = CType(reader("UserId"), Nullable(Of Guid))
			End If
			Return newCoupon
		End Function

		''' <summary>
		''' Saves a coupon to the database. 
		''' </summary>
		Public Overridable Sub SaveCoupon()
			Dim expDate As DateTime = CDate(Me.ExpirationDate)

			SPs.CouponsSaveCoupon(Me.CouponCode, Me.CouponType.CouponTypeID, Me.IsSingleUse, Me.NumberOfUses, expDate, Me.GetXmlData()).Execute()


		End Sub

		Public Shared Function GetAllCoupons() As IDataReader
			Return New Query("CSK_Coupons").ExecuteReader()
		End Function
		Private Shared Function GetCouponReader(ByVal couponCode As String) As IDataReader

			Return SPs.CouponsGetCoupon(couponCode).GetReader()


		End Function


		Private Shared Function CreateListFromReader(ByVal couponReader As IDataReader) As List(Of Coupon)
			Dim couponList As List(Of Coupon) = New List(Of Coupon)()
			Do While couponReader.Read()
				couponList.Add(Coupon.CreateCoupon(couponReader))
			Loop
			Return couponList
		End Function

		Public Shared Function GenerateCouponCode(ByVal minCharacters As Integer, ByVal maxCharacters As Integer) As String
			Dim newCode As String
			Dim testCoupon As Coupon
			Do
				'This is not cryptographically random, but should be good enough.  
				Dim rnd As System.Random = New Random()
				Dim numCharacters As Integer = rnd.Next(minCharacters, maxCharacters)
				Dim sb As StringBuilder = New StringBuilder(numCharacters)
				Dim i As Integer = 0
				Do While i < numCharacters
					sb.Append(Char.ConvertFromUtf32(rnd.Next(65, 90)))
					i += 1
				Loop
				newCode = sb.ToString()
				testCoupon = GetCoupon(newCode, False)
			Loop While Not testCoupon Is Nothing
			Return newCode

		End Function
		Public Shared Function GenerateCouponCode() As String
			Return GenerateCouponCode(5, 10)
		End Function
	End Class
End Namespace
