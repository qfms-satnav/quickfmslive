#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports Commerce.Common

Namespace Commerce.Promotions
	Public Class PercentOffCoupon
		Inherits Coupon
		Public Sub New()
			MyBase.New()

		End Sub

		Public Sub New(ByVal couponCode As String, ByVal couponType As CouponType)
			MyBase.New(couponCode, couponType)
		End Sub
		Private _percentOff As Integer

		Public Property PercentOff() As Integer
			Get
				Return _percentOff
			End Get
			Set
				_percentOff = Value
			End Set
		End Property



		Private Function CalculateDiscountAmount(ByVal itemAmount As Decimal) As Decimal

			Dim result As Decimal = 0
			Dim dPercent As Decimal = Convert.ToDecimal(PercentOff)
			If PercentOff > 0 Then
				Dim dRate As Decimal = dPercent / 100.00D
				result = dRate * itemAmount
				result = Math.Round(result, 2)
			End If

			Return result
		End Function
		Public Overrides Sub ApplyCouponToOrder(ByVal order As Commerce.Common.Order)
			'Validate the order to make sure it's good.  
			Dim validationResponse As CouponValidationResponse = ValidateCouponForOrder(order)
            If (Not validationResponse.IsValid) Then
                'LJD 11/17/07 Replaced validationResponse to validationResponse.ToString
                Throw New ArgumentException("Coupon is not valid for order: " & validationResponse.ToString, "order")
            End If

            'updated by Spook, 11-5-2006


			'The logic for this coupon is to apply a Percent off to the order items
			'this does not include shipping/tax since those items are not 

			'first, make sure this coupon hasn't been used
			If (Not order.CouponCodes.Contains(CouponCode)) Then

				'calculate the discount
				order.DiscountAmount = CalculateDiscountAmount(order.CalculateSubTotal())

				'add a comment to the order
				order.CouponCodes &= CouponCode

				'save the order
				order.Save("Coupon System")
			End If

		End Sub

	End Class
End Namespace
