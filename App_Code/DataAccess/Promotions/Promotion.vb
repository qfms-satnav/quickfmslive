#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic
Imports Commerce.Common

Namespace Commerce.Promotions

	''' <summary>
	''' A simple "smart" class. Use "Save" to put the item to the DB
	''' </summary>
	Public Class Promo
		Inherits ActiveRecord(Of Promo)

		#Region ".ctors"
		''' <summary>
		''' Sets the static Table property from our Base class. This property tells
		''' the base class how to create the CRUD queries, etc.
		''' </summary>
		Private Sub SetSQLProps()

			If Schema Is Nothing Then
				Schema = Query.BuildTableSchema("CSK_Promo")

			End If

			'set the default values
			Me.SetColumnValue("promoID", 0)
			Me.SetColumnValue("campaignID", 0)
			Me.SetColumnValue("promoCode", String.Empty)
			Me.SetColumnValue("title", String.Empty)
			Me.SetColumnValue("description", String.Empty)
			Me.SetColumnValue("discount", 0)
			Me.SetColumnValue("qtyThreshold", 0)
			Me.SetColumnValue("inventoryGoal", 0)
			Me.SetColumnValue("revenueGoal", 0)
			Me.SetColumnValue("dateEnd", New DateTime(1900, 01, 01))
			Me.SetColumnValue("isActive", False)

			'state properties - these are set automagically 
			'during save
			Me.SetColumnValue("createdOn", DateTime.UtcNow)
			Me.SetColumnValue("createdBy", String.Empty)
			Me.SetColumnValue("modifiedOn", DateTime.UtcNow)
			Me.SetColumnValue("modifiedBy", String.Empty)

		End Sub

		Public Shared Function GetTableSchema() As TableSchema.Table
			'instance an object to make sure
			'the table schema has been created
			Dim item As Promo = New Promo()
			Return Promo.Schema
		End Function

		Public Sub New()
			SetSQLProps()
			Me.MarkNew()
		End Sub
		Public Sub New(ByVal promoID As Integer)
			SetSQLProps()
			MyBase.LoadByKey(promoID)

		End Sub

		#End Region

		#Region "Public Props"
		<XmlAttribute("CreatedOn")> _
		Public Property CreatedOn() As DateTime
			Get
				Dim result As Object = Me.GetColumnValue("CreatedOn")
				Dim oOut As DateTime = New DateTime(1900, 01, 01)
				Try
					oOut = DateTime.Parse(result.ToString())
				Catch
				End Try
				Return oOut

			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("CreatedOn", Value)
			End Set
		End Property
		<XmlAttribute("CreatedBy")> _
		Public Property CreatedBy() As String
			Get
				Dim result As Object = Me.GetColumnValue("CreatedBy")
				Dim sOut As String
				If result Is Nothing Then
					sOut = String.Empty
				Else
					sOut = result.ToString()
				End If
				Return sOut

			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("CreatedBy", Value)
			End Set
		End Property
		<XmlAttribute("ModifiedOn")> _
		Public Property ModifiedOn() As DateTime
			Get
				Dim result As Object = Me.GetColumnValue("ModifiedOn")
				Dim oOut As DateTime = New DateTime(1900, 01, 01)
				Try
					oOut = DateTime.Parse(result.ToString())
				Catch
				End Try
				Return oOut

			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("ModifiedOn", Value)
			End Set
		End Property
		<XmlAttribute("ModifiedBy")> _
		Public Property ModifiedBy() As String
			Get
				Dim result As Object = Me.GetColumnValue("ModifiedBy")
				Dim sOut As String
				If result Is Nothing Then
					sOut = String.Empty
				Else
					sOut = result.ToString()
				End If
				Return sOut

			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("ModifiedBy", Value)
			End Set
		End Property
		<XmlAttribute("PromoID")> _
		Public Property PromoID() As Integer
			Get
				Return Integer.Parse(Me.GetColumnValue("promoID").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("promoID", Value)

			End Set
		End Property
		<XmlAttribute("CampaignID")> _
		Public Property CampaignID() As Integer
			Get
				Return Integer.Parse(Me.GetColumnValue("campaignID").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("campaignID", Value)

			End Set
		End Property
		<XmlAttribute("PromoCode")> _
		Public Property PromoCode() As String
			Get
				Return Me.GetColumnValue("promoCode").ToString()
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("promoCode", Value)

			End Set
		End Property
		<XmlAttribute("Title")> _
		Public Property Title() As String
			Get
				Return Me.GetColumnValue("title").ToString()
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("title", Value)

			End Set
		End Property
		<XmlAttribute("Description")> _
		Public Property Description() As String
			Get
				Return Me.GetColumnValue("description").ToString()
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("description", Value)

			End Set
		End Property
		<XmlAttribute("Discount")> _
		Public Property Discount() As Decimal
			Get
				Return Decimal.Parse(Me.GetColumnValue("discount").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("discount", Value)

			End Set
		End Property
		<XmlAttribute("QtyThreshold")> _
		Public Property QtyThreshold() As Integer
			Get
				Return Integer.Parse(Me.GetColumnValue("qtyThreshold").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("qtyThreshold", Value)

			End Set
		End Property
		<XmlAttribute("InventoryGoal")> _
		Public Property InventoryGoal() As Integer
			Get
				Return Integer.Parse(Me.GetColumnValue("inventoryGoal").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("inventoryGoal", Value)

			End Set
		End Property
		<XmlAttribute("RevenueGoal")> _
		Public Property RevenueGoal() As Decimal
			Get
				Return Decimal.Parse(Me.GetColumnValue("revenueGoal").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("revenueGoal", Value)

			End Set
		End Property
		<XmlAttribute("DateEnd")> _
		Public Property DateEnd() As DateTime
			Get
				Return DateTime.Parse(Me.GetColumnValue("dateEnd").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("dateEnd", Value)

			End Set
		End Property
		<XmlAttribute("IsActive")> _
		Public Property IsActive() As Boolean
			Get
				Return Boolean.Parse(Me.GetColumnValue("isActive").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("isActive", Value)

			End Set
		End Property

		#End Region

		#Region "Extended Data Access"


		''' <summary>
		''' Executes "CSK_Promo_ProductMatrix" and returns the results in an IDataReader 
		''' </summary>
		''' <returns>System.Data.IDataReader </returns>	
		Public Shared Function ProductMatrix() As IDataReader

			Return SPs.PromoProductMatrix().GetReader()

		End Function
		''' <summary>
		''' Executes "CSK_Promo_GetProductList" and returns the results in an IDataReader 
		''' </summary>
		''' <returns>System.Data.IDataReader </returns>	
		Public Shared Function GetProductList(ByVal promoID As Integer) As DataSet

			Return SPs.PromoGetProductList(promoID).GetDataSet()

		End Function

		''' <summary>
		''' Executes "CSK_Promo_RemoveProducts" and returns the results in an IDataReader 
		''' </summary>
		''' <returns>System.Data.IDataReader </returns>	
		Public Shared Function RemoveProducts(ByVal promoID As Integer) As IDataReader

			Return SPs.PromoRemoveProducts(promoID).GetReader()

		End Function

		#End Region

	End Class
End Namespace
