#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic

Namespace Commerce.Promotions

	''' <summary>
	''' A Persistable class that uses Generics to store it's state
	''' in the database. This class maps to the CSK_Promo_Campaign table.
	''' </summary>
	Public Class Campaign
		Inherits ActiveRecord(Of Campaign)

		#Region ".ctors"
		''' <summary>
		''' Sets the static Table property from our Base class. This property tells
		''' the base class how to create the CRUD queries, etc.
		''' </summary>
		Private Sub SetSQLProps()

			If Schema Is Nothing Then
				Schema = Query.BuildTableSchema("CSK_Promo_Campaign")
			End If

			'set the default values
			Me.SetColumnValue("campaignID", 0)
			Me.SetColumnValue("campaignName", String.Empty)
			Me.SetColumnValue("description", String.Empty)
			Me.SetColumnValue("objective", String.Empty)
			Me.SetColumnValue("revenueGoal", 0)
			Me.SetColumnValue("inventoryGoal", 0)
			Me.SetColumnValue("dateEnd", New DateTime(1900, 01, 01))
			Me.SetColumnValue("isActive", False)

			'state properties - these are set automagically 
			'during save
			Me.SetColumnValue("createdOn", DateTime.UtcNow)
			Me.SetColumnValue("createdBy", String.Empty)
			Me.SetColumnValue("modifiedOn", DateTime.UtcNow)
			Me.SetColumnValue("modifiedBy", String.Empty)

		End Sub

		Public Shared Function GetTableSchema() As TableSchema.Table
			'instance an object to make sure
			'the table schema has been created
			Dim item As Campaign = New Campaign()
			Return Campaign.Schema
		End Function

		Public Sub New()
			SetSQLProps()
			Me.MarkNew()
		End Sub
		Public Sub New(ByVal campaignID As Integer)
			SetSQLProps()
			MyBase.LoadByKey(campaignID)

		End Sub

		#End Region

		#Region "Public Props"
		<XmlAttribute("CampaignID")> _
		Public Property CampaignID() As Integer
			Get
				Return Integer.Parse(Me.GetColumnValue("campaignID").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("campaignID", Value)

			End Set
		End Property
		<XmlAttribute("CampaignName")> _
		Public Property CampaignName() As String
			Get
				Return Me.GetColumnValue("campaignName").ToString()
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("campaignName", Value)

			End Set
		End Property
		<XmlAttribute("Description")> _
		Public Property Description() As String
			Get
				Return Me.GetColumnValue("description").ToString()
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("description", Value)

			End Set
		End Property
		<XmlAttribute("Objective")> _
		Public Property Objective() As String
			Get
				Return Me.GetColumnValue("objective").ToString()
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("objective", Value)

			End Set
		End Property
		<XmlAttribute("RevenueGoal")> _
		Public Property RevenueGoal() As Decimal
			Get
				Return Decimal.Parse(Me.GetColumnValue("revenueGoal").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("revenueGoal", Value)

			End Set
		End Property
		<XmlAttribute("InventoryGoal")> _
		Public Property InventoryGoal() As Integer
			Get
				Return Integer.Parse(Me.GetColumnValue("inventoryGoal").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("inventoryGoal", Value)

			End Set
		End Property
		<XmlAttribute("DateEnd")> _
		Public Property DateEnd() As DateTime
			Get
				Return DateTime.Parse(Me.GetColumnValue("dateEnd").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("dateEnd", Value)

			End Set
		End Property
		<XmlAttribute("IsActive")> _
		Public Property IsActive() As Boolean
			Get
				Return Boolean.Parse(Me.GetColumnValue("isActive").ToString())
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("isActive", Value)

			End Set
		End Property

		#End Region

	End Class
End Namespace
