Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Collections.Generic
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Microsoft.VisualBasic
Imports Amantra.RequisitionDTON
Imports Amantra.SmartReader
Imports System.Configuration.ConfigurationManager

Namespace Amantra.RequisitionDALN

    Public Class RequsitionDAL

#Region "BLL and DTO Instance declarations"

        Private m_Classname As String = "RequisitionDAL"
        Private rDTO As New RequisitionDTO()


#End Region

#Region "Private Variable Declaration"
        Private insertdata As Integer = 0
        Private countryId As Integer = 0

#End Region


#Region "[ SELECT ALL Requisition DETAILS FOR Display in Page ]"

        '' ----------------------------------------------------------------------------------------------- 
        '@req_id as nvarchar(50),  
        '@sta_id as int,  
        '@usr_id as int  
        ' <summary> 
        ' Retrieve all Requisition Details details 
        ' </summary> 
        ' <Author>Srinivasulu M</Author> 
        ' Created date 05/07/2008Created date 
        Public Function GetRequisitionData(ByVal Req_ID As String, ByVal Sta_id As Integer, ByVal usr_id As String) As IList(Of RequisitionDTO)

            Dim m_Methodname As String = "In GetRequisitionData Method"
            Dim RequisitionDetails As IList(Of RequisitionDTO) = New List(Of RequisitionDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar), New SqlParameter("@sta_id", SqlDbType.NVarChar, 50), New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)}

            parms(0).Value = Req_ID
            parms(1).Value = Sta_id
            parms(2).Value = usr_id

            'parms(3).Value = rDTO.getUserName
            Try
                'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(SqlHelper.ConnectionString, CommandType.StoredProcedure, "usp_AmantraAxis_getRequisitionData", parms)
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_Wipro_getRequisitionData", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim requisition As RequisitionDTO = Nothing
                While smartReader.Read()
                    requisition = New RequisitionDTO(smartReader.GetString("srn_req_id"), smartReader.GetInt32("cabins"), smartReader.GetInt32("cubicals"), smartReader.GetString("Work_Stations"), smartReader.GetString("srn_bdg_one"), smartReader.GetString("srn_bdg_two"), smartReader.GetString("srn_frm_dt"), smartReader.GetString("srn_to_dt"), smartReader.GetString("usr_name"), smartReader.GetString("RptMgr_Name"), smartReader.GetString("srn_sta_id"), smartReader.GetString("dep_name"), smartReader.GetString("srn_rem"), smartReader.GetString("SRN_BRANCH_ID"))
                    RequisitionDetails.Add(requisition)
                End While
                dr.Close()
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return RequisitionDetails
        End Function

        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region "Get Requistion Ids"
        Public Function GetRequisitionIDs(ByVal Sta_id As Integer) As IList(Of RequisitionDTO)

            Dim m_Methodname As String = "In GetRequisitionData Method"
            Dim RequisitionDetails As IList(Of RequisitionDTO) = New List(Of RequisitionDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@sta_id", SqlDbType.NVarChar, 50)}


            parms(0).Value = Sta_id


            'parms(3).Value = rDTO.getUserName
            Try
                'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(SqlHelper.ConnectionString, CommandType.StoredProcedure, "usp_AmantraAxis_getRequisitionData", parms)
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_AmantraAxis_getReqIDs", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim requisition As RequisitionDTO = Nothing
                While smartReader.Read()
                    requisition = New RequisitionDTO(smartReader.GetString("srn_req_id1"), smartReader.GetString("srn_req_id"))
                    RequisitionDetails.Add(requisition)
                End While
                dr.Close()
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return RequisitionDetails
        End Function
#End Region
#Region " Update Requisition Status"
        Public Function updateRequestStatus(ByVal rdto As RequisitionDTO) As Integer

            Dim m_Methodname As String = "updateRMRequestStatus"
            Dim CountryList As IList(Of RequisitionDTO) = New List(Of RequisitionDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar, 50), New SqlParameter("@rm_remarks", SqlDbType.NVarChar, 500), New SqlParameter("@sta_id", SqlDbType.Int)}


            parms(0).Value = rdto.RequestID
            parms(1).Value = rdto.getRMRemarks
            parms(2).Value = rdto.getStatus


            Try
                insertdata = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_UpdateRequestStatus", parms)
                'insertdata = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionString, CommandType.StoredProcedure, " usp_AmantraAxis_Country_InsertCountry", parms)

            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in DAL", m_Classname, m_Methodname, ex)
            End Try
            Return insertdata
        End Function

        Public Function updateBFMRequestStatus(ByVal rdto As RequisitionDTO) As Integer

            Dim m_Methodname As String = "updateRMRequestStatus"
            Dim CountryList As IList(Of RequisitionDTO) = New List(Of RequisitionDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar, 50), New SqlParameter("@bfm_remarks", SqlDbType.NVarChar, 500), New SqlParameter("@sta_id", SqlDbType.Int)}


            parms(0).Value = rdto.RequestID
            parms(1).Value = rdto.getBFMRemarks
            parms(2).Value = rdto.getStatus


            Try
                insertdata = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_AmantraAxis_UpdateBFMRequestStatus", parms)
                'insertdata = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionString, CommandType.StoredProcedure, " usp_AmantraAxis_Country_InsertCountry", parms)

            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in DAL", m_Classname, m_Methodname, ex)
            End Try
            Return insertdata
        End Function
        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region "[ SELECT ALL BFM Requisition DETAILS FOR Display in Page ]"

        '' ----------------------------------------------------------------------------------------------- 
        ''' <summary> 
        ''' Retrieve all BFM Requisition Details details 
        ''' </summary> 
        ''' <Author>Srinivasulu M</Author> 
        ''' <Createddate>07/07/2008</Createddate> 
        Public Function GetBFMRequisitionData(ByVal Req_ID As String, ByVal Sta_id As Integer, ByVal usr_id As String) As IList(Of RequisitionDTO)

            Dim m_Methodname As String = "In GetRequisitionData Method"
            Dim RequisitionDetails As IList(Of RequisitionDTO) = New List(Of RequisitionDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar), New SqlParameter("@sta_id", SqlDbType.NVarChar, 50), New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)}

            parms(0).Value = Req_ID
            parms(1).Value = Sta_id
            parms(2).Value = usr_id


            'parms(3).Value = rDTO.getUserName
            Try
                'Dim dr As SqlDataReader = SqlHelper.ExecuteReader(SqlHelper.ConnectionString, CommandType.StoredProcedure, "usp_AmantraAxis_getRequisitionData", parms)
                strSQL = "select distinct srn_req_id,ssr.SRN_BRANCH_ID 'SRN_BRANCH_ID', " & _
                         "(select twr_name from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_code=srn_bdg_one) as 'srn_bdg_one',  " & _
                        "(select twr_name from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_code=srn_bdg_two) as 'srn_bdg_two', " & _
                        "convert(nvarchar,srn_frm_dt,101) as 'srn_frm_dt', convert(nvarchar,srn_to_dt,101) as 'srn_to_dt', " & _
                        "(select usr_name from " & HttpContext.Current.Session("TENANT") & "." & "[user] where usr_id='" & usr_id & "') as 'usr_name', (select dep_name from " & HttpContext.Current.Session("TENANT") & "." & "department where dep_code=srn_dep_id) as 'dep_name', " & _
                        "srn_rem, (select usr_name from " & HttpContext.Current.Session("TENANT") & "." & "[user] where usr_id = (select aur_reporting_to from " & HttpContext.Current.Session("TENANT") & "." & "amantra_user where aur_id='" & usr_id & "')) as 'RptMgr_Name', " & _
                        "srn_sta_id,isnull(srn_rm_rem,'NA') as 'srn_rm_rem',SSAD.SMS_WST_ALLOC as 'Work_Stations', SSAD.SMS_CAB_ALLOC as 'Cabins', SSAD.SMS_CUB_ALLOC as 'Cubicals' " & _
                        "from " & HttpContext.Current.Session("TENANT") & "." & "sms_spacerequisition ssr INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACE_APPROVAL_DTLS SSAD on  " & _
                        "ssr.SRN_REQ_ID = ssad.SMS_ALLOC_REQ  where srn_req_id='" & Req_ID & "' and ssad.SMS_APPROVED_BY='RM'and SRN_STA_ID=149 "

                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                Dim smartReader As New SmartDataReader(dr)
                Dim requisition As RequisitionDTO = Nothing
                While smartReader.Read()
                    requisition = New RequisitionDTO(smartReader.GetString("srn_req_id"), smartReader.GetInt32("cabins"), smartReader.GetInt32("cubicals"), smartReader.GetString("Work_Stations"), smartReader.GetString("srn_bdg_one"), smartReader.GetString("srn_bdg_two"), smartReader.GetString("srn_frm_dt"), smartReader.GetString("srn_to_dt"), smartReader.GetString("usr_name"), smartReader.GetString("RptMgr_Name"), smartReader.GetString("srn_sta_id"), smartReader.GetString("dep_name"), smartReader.GetString("srn_rem"), smartReader.GetString("srn_rm_rem"), smartReader.GetString("SRN_BRANCH_ID"))
                    RequisitionDetails.Add(requisition)
                End While
                dr.Close()
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return RequisitionDetails
        End Function

        '' ----------------------------------------------------------------------------------------------- 
#End Region
    End Class

End Namespace
