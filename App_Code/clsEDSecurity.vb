Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports System
Imports System.Text

Public Class clsEDSecurity
    Public Shared Function Encrypt(ByVal strToEncrypt As String) As String
        Try
            '********** MD5 Encryption
            'Return Encrypt(strToEncrypt, "A-mantraMD5ForAmantraAxis")
            'Return strToEncrypt
            '********** MD5 Encryption


            '********** Unicode Encoding
            Dim b() As Byte = Encoding.Unicode.GetBytes(strToEncrypt)
            Return Convert.ToBase64String(b)
            '********** Unicode Encoding
        Catch ex As Exception
            Return "Wrong Input. " + ex.Message
        End Try
    End Function
    Public Shared Function Decrypt(ByVal strEncrypted As String) As String
        Try
            '********** MD5 Decryption
            'Return Decrypt(strEncrypted, "A-mantraMD5ForAmantraAxis")
            'Return strEncrypted
            '********** MD5 Decryption


            '********** Unicode Decoding
            Dim b() As Byte = Convert.FromBase64String(strEncrypted)
            Return Encoding.Unicode.GetString(b)
            '********** Unicode Decoding

        Catch ex As Exception
            Return "Wrong Input. " + ex.Message
        End Try
    End Function
    Public Shared Function Encrypt(ByVal strToEncrypt As String, ByVal strKey As String) As String
        Try
            Dim objDESCrypto As New TripleDESCryptoServiceProvider()
            Dim objHashMD5 As New MD5CryptoServiceProvider()
            Dim byteHash As Byte(), byteBuff As Byte()
            Dim strTempKey As String = strKey
            byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey))
            objHashMD5 = Nothing
            objDESCrypto.Key = byteHash
            objDESCrypto.Mode = CipherMode.ECB
            'CBC, CFB 
            byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt)
            Return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length))
        Catch ex As Exception
            Return "Wrong Input. " + ex.Message
        End Try
    End Function
    Public Shared Function Decrypt(ByVal strEncrypted As String, ByVal strKey As String) As String
        Try
            Dim objDESCrypto As New TripleDESCryptoServiceProvider()
            Dim objHashMD5 As New MD5CryptoServiceProvider()
            Dim byteHash As Byte(), byteBuff As Byte()
            Dim strTempKey As String = strKey
            byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey))
            objHashMD5 = Nothing
            objDESCrypto.Key = byteHash
            objDESCrypto.Mode = CipherMode.ECB
            'CBC, CFB 
            byteBuff = Convert.FromBase64String(strEncrypted)
            Dim strDecrypted As String = ASCIIEncoding.ASCII.GetString(objDESCrypto.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length))
            objDESCrypto = Nothing
            Return strDecrypted
        Catch ex As Exception
            Return "Wrong Input. " + ex.Message
        End Try
    End Function

End Class
