Imports Microsoft.VisualBasic
Imports System.Web.UI.HtmlControls

Public Module MetaTagsConfig


    Public Function addStyle(ByVal path2css As String) As System.Web.UI.Control
        Dim css As New HtmlLink
        css.Href = path2css
        css.Attributes.Add("rel", "stylesheet")
        css.Attributes.Add("type", "text/css")
        css.Attributes.Add("media", "all")
        Return css
    End Function


    Public Function addScript(ByVal path2js As String) As System.Web.UI.Control
        Dim si As New HtmlGenericControl
        si.TagName = "script"
        si.Attributes.Add("type", "text/javascript")
        si.Attributes.Add("src", path2js)
        Return si
    End Function

    Public Function addScript_inline(ByVal js As String) As System.Web.UI.Control
        Dim si As New HtmlGenericControl
        si.TagName = "script"
        si.Attributes.Add("type", "text/javascript")
        si.InnerHtml = js
        Return si
    End Function

End Module
