Imports System
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Web
Imports System.Web.UI.HtmlControls


Public Class clsProperty
    Implements IDisposable

    Public Country, Region, State, Centre, Location, Category, Subcategory, GSC As Integer
    Public PremiseID, LPeriod, LPeriodExt, LLockPeriod, RentFree, CarParking, FloorRebate As Integer

    Public LandArea, LandCost, BuiltUp, CarpetArea, SiteCost, MonthlyRent, StampDuty, StampDutyPercent As Double
    Public ElecAmount, Registration, Retention, AdjustAmount, Brokerage, OtherCharges1, OtherCharges2, OtherCharges3, MunicipalTax, HouseTax As Double

    Public ReqID, PropType, PremiseName, PremiseAddress, Road, Landmark, Pincode, CTS, TownPlan, LandCon, Comment As String
    Public StrongRoom, ToiletBlock, StructuralChanges, StairCase, ElecDeposit, LDeedNo, occupiedby As String
    Public TaxBy, OutflowRemark, OtherChargesRemark, PaymentType, LeaseTermRemarks As String

    Public LDeedDate, LCommencement, LExpiry, LockDate As Date

    Private dtSites, dtLessors, dtNatureOfPayments, dtVendorDistributions As DataTable

    Private blnLessorAmended, blnNOPaymentAmended, blnDistributionAmended As Boolean


    ' Newly Added Fields by Phani
    Public areapersqft, costpersqft As Double
    Public EscTerm, EscPercentage, RentableCarParks, CarparkPerSlot, TotalCarParks, CamChages, RenewalTermination, IntFreeSecDep As Double



#Region " Constructor "

    Public Sub New()

    End Sub
#End Region

#Region " Class Properties "
    Public Property Sites() As DataTable
        Get
            Return dtSites
        End Get
        Set(ByVal Value As DataTable)
            dtSites = Value
        End Set

    End Property

    Public Property NatureOfPayments() As DataTable
        Get
            Return dtNatureOfPayments
        End Get
        Set(ByVal Value As DataTable)
            dtNatureOfPayments = Value
        End Set

    End Property

    Public Property Lessors() As DataTable
        Get
            Return dtLessors
        End Get
        Set(ByVal Value As DataTable)
            dtLessors = Value
        End Set

    End Property

    Public Property LessorsAmended() As Boolean
        Get
            Return blnLessorAmended
        End Get
        Set(ByVal Value As Boolean)
            blnLessorAmended = Value
        End Set

    End Property

    Public Property NatureofPaymentsAmended() As Boolean
        Get
            Return blnNOPaymentAmended
        End Get
        Set(ByVal Value As Boolean)
            blnNOPaymentAmended = Value
        End Set

    End Property

    Public Property DistributionsAmended() As Boolean
        Get
            Return blnDistributionAmended
        End Get
        Set(ByVal Value As Boolean)
            blnDistributionAmended = Value
        End Set

    End Property


    Public Property LessorDistributions() As DataTable
        Get
            Return dtVendorDistributions
        End Get
        Set(ByVal Value As DataTable)
            dtVendorDistributions = Value
        End Set

    End Property
#End Region

#Region " Save Details "
    Public Function SaveDetails(ByVal uid As String) As String
        Try
            Dim objPB As New clsPMSBusiness

            Dim intReqID, indx As Integer
            Dim strReqID, strAgrNo As String
            Dim dtNewPremise, dtRequest, dtReqSites, dtAgrLessors, dtNOPayments, dtLeaseTerms, dtLessorDist As DataTable
            Dim drNewRow As DataRow

            ' Making entry of the new premise
            ' If PropType = "New" Then
            dtNewPremise = New DataTable
            dtNewPremise = objPB.fnTableSchema("PMS_NEWPREMISE")

            drNewRow = dtNewPremise.NewRow()

            drNewRow("PNP_NAME") = PremiseName
            drNewRow("PNP_Address") = PremiseAddress
            drNewRow("PNP_ROAD") = Road
            drNewRow("PNP_LANDMARK") = Landmark
            drNewRow("PNP_CTSNO") = CTS
            drNewRow("PNP_TOWNPLANNo") = TownPlan
            drNewRow("PNP_PINCODE") = Pincode
            drNewRow("PNP_UPT_BY") = uid
            drNewRow("PNP_UPT_DT") = getoffsetdatetime(DateTime.Now)

            dtNewPremise.Rows.Add(drNewRow)

            PremiseID = objPB.fnInsertData(dtNewPremise)
            ' End If

            'Call business class method for getting the table schema 
            dtRequest = New DataTable
            dtRequest = objPB.fnTableSchema("PMS_BUILDING_REQUEST")

            drNewRow = dtRequest.NewRow()

            drNewRow("PBR_PREQ_ID") = uid
            drNewRow("PBR_PNP_ID") = PremiseID
            drNewRow("PBR_ACQ_TYPE") = "LEASE"
            drNewRow("PBR_PURPOSE") = ""
            drNewRow("PBR_CNY_ID") = Country
            drNewRow("PBR_RGN_ID") = Region
            drNewRow("PBR_STE_ID") = State
            drNewRow("PBR_CTY_ID") = Centre
            drNewRow("PBR_LCM_ID") = Location
            drNewRow("PBR_GSC_ID") = GSC
            drNewRow("PBR_CAT_ID") = Category
            drNewRow("PBR_SCAT_ID") = Subcategory
            drNewRow("PBR_PROP_TYPE") = PropType
            drNewRow("PBR_LANDAVAIL") = LandCon
            drNewRow("PBR_LANDAREA") = LandArea
            drNewRow("PBR_LANDCOST") = LandCost
            drNewRow("PBR_UPT_BY") = uid
            drNewRow("PBR_UPT_DT") = getoffsetdatetime(DateTime.Now)
            drNewRow("PBR_COMMENTS") = Comment
            drNewRow("PBR_STA_ID") = 5

            dtRequest.Rows.Add(drNewRow)

            intReqID = objPB.fnInsertData(dtRequest)
            strReqID = uid & "/" & "LEASE" & "/" & CStr(intReqID).PadLeft(6, "0")


            '  Updating formatted Requisition ID in the table
            objPB.subUpdateRow("PMS_BUILDING_REQUEST", "PBR_PREQ_ID", "'" & strReqID & "'", "PBR_ID = " & intReqID)


            ' Inserting Premise Site details           

            If (dtSites Is Nothing) Or (dtSites.Rows.Count = 0) Then Throw New PMSException("No Sites Available")

            dtReqSites = New DataTable
            dtReqSites = objPB.fnTableSchema("PMS_BUILDINGSITE_DETAILS")

            For indx = 0 To dtSites.Rows.Count - 1
                drNewRow = dtReqSites.NewRow()

                drNewRow("PBD_PREQ_ID") = strReqID
                drNewRow("PBD_BNAME") = dtSites.Rows(indx).Item(1)
                ' drNewRow("PBD_TOWER") = dtSites.Rows(indx).Item(3)
                drNewRow("PBD_FLOOR") = dtSites.Rows(indx).Item(3)
                drNewRow("PBD_WING") = dtSites.Rows(indx).Item(5)
                drNewRow("PBD_FLAT_NO") = dtSites.Rows(indx).Item(6)
                drNewRow("PBD_BP_AREA") = dtSites.Rows(indx).Item(7)
                drNewRow("PBD_CP_AREA") = dtSites.Rows(indx).Item(8)
                drNewRow("PBD_COST_SQFT") = dtSites.Rows(indx).Item(9)
                drNewRow("PBD_STA_ID") = 1

                dtReqSites.Rows.Add(drNewRow)
            Next

            objPB.subInsertData(dtReqSites)


            ' Inserting Agreement Lessors

            If (dtLessors Is Nothing) Or (dtLessors.Rows.Count = 0) Then Throw New PMSException("No Lessors Selected")

            dtAgrLessors = New DataTable
            dtAgrLessors = objPB.fnTableSchema("PMS_AGREEMENT_LESSORS")

            For indx = 0 To dtLessors.Rows.Count - 1
                drNewRow = dtAgrLessors.NewRow()

                drNewRow("PAL_PREQ_ID") = strReqID
                drNewRow("PLR_ID") = dtLessors.Rows(indx).Item(0)
                drNewRow("PAL_UPD_BY") = uid
                drNewRow("PAL_UPD_DT") = getoffsetdatetime(DateTime.Now)

                dtAgrLessors.Rows.Add(drNewRow)
            Next

            objPB.subInsertData(dtAgrLessors)


            '  Inserting Nature of Payments and Vendor Distributions
            If (dtNatureOfPayments Is Nothing) Or (dtNatureOfPayments.Rows.Count = 0) Then Throw New PMSException("No Nature Of Payments Details Available ")

            Dim newNOPID As Integer, iVend As Integer

            dtNOPayments = New DataTable
            dtNOPayments = objPB.fnTableSchema("PMS_LEASERENTAL_DETAILS")

            For indx = 0 To dtNatureOfPayments.Rows.Count - 1
                drNewRow = dtNOPayments.NewRow()

                drNewRow("PLD_PREQ_ID") = strReqID
                drNewRow("PLD_FROM_DT") = dtNatureOfPayments.Rows(indx).Item(1)
                drNewRow("PLD_TO_DT") = dtNatureOfPayments.Rows(indx).Item(2)
                drNewRow("PLD_NOP_ID") = dtNatureOfPayments.Rows(indx).Item(3)
                drNewRow("PLD_MONTHLY_OFLOW") = dtNatureOfPayments.Rows(indx).Item(5)
                drNewRow("PLD_UPD_BY") = uid
                drNewRow("PLD_UPD_DT") = getoffsetdatetime(DateTime.Now)
                drNewRow("PLD_STA_ID") = 1

                dtNOPayments.Rows.Add(drNewRow)

                '  Inserting into PMS_LEASERENTAL_DETAILS table
                newNOPID = objPB.fnInsertData(dtNOPayments)


                dtLessorDist = New DataTable
                dtLessorDist = objPB.fnTableSchema("PMS_LESSOR_DISTRIBUTIONS")

                For iVend = 0 To dtVendorDistributions.Rows.Count - 1
                    If dtNatureOfPayments.Rows(indx).Item("Sno") = dtVendorDistributions.Rows(iVend).Item("Sno") Then

                        drNewRow = dtLessorDist.NewRow

                        drNewRow("PVD_PLD_ID") = newNOPID
                        drNewRow("PVD_VEN_ID") = dtVendorDistributions.Rows(iVend).Item("VendorID")
                        drNewRow("PVD_AMOUNT") = dtVendorDistributions.Rows(iVend).Item("Amount")
                        drNewRow("PVD_UPD_BY") = uid
                        drNewRow("PVD_UPD_DT") = getoffsetdatetime(DateTime.Now)
                        drNewRow("PVD_STA_ID") = 1

                        dtLessorDist.Rows.Add(drNewRow)

                    End If
                Next


                '  Insering vendor distributions
                objPB.subInsertData(dtLessorDist)

                dtNOPayments.Rows(0).Delete()
            Next



            '  Inserting Lease Terms

            dtLeaseTerms = New DataTable
            dtLeaseTerms = objPB.fnTableSchema("PMS_LEASE_TERMS")

            drNewRow = dtLeaseTerms.NewRow()

            drNewRow("LAG_PREQ_ID") = strReqID
            drNewRow("LAG_LDEED_DT") = LDeedDate
            drNewRow("LAG_LDEED_NO") = LDeedNo
            drNewRow("LAG_LCOMM_DT") = LCommencement
            drNewRow("LAG_CLOSING_DT") = LExpiry
            drNewRow("LAG_LEASE_PERIOD") = LPeriod
            drNewRow("LAG_LPER_EXTN") = LPeriodExt
            drNewRow("LAG_LOCK_PER") = LLockPeriod
            drNewRow("LAG_LOCK_DT") = LockDate
            drNewRow("LAG_MRENT_AMT") = MonthlyRent
            drNewRow("LAG_RENT_FP") = RentFree
            drNewRow("LAG_STAMPDUTY") = StampDuty
            drNewRow("LAG_STDUTY_LESS") = StampDutyPercent
            drNewRow("LAG_STCHGS_BY") = StructuralChanges
            drNewRow("LAG_TBLOCKS_BY") = ToiletBlock
            drNewRow("LAG_STROOM_BY") = StrongRoom
            drNewRow("LAG_ELECDEP_BY") = ElecDeposit
            drNewRow("LAG_ELECDEP") = ElecAmount
            drNewRow("LAG_STCASE_BY") = StairCase
            drNewRow("LAG_REGISTRATION") = Registration
            drNewRow("LAG_RTN") = Retention
            drNewRow("LAG_ADJ_ADVANCE") = AdjustAmount
            drNewRow("LAG_CPARK_NOS") = CarParking
            drNewRow("LAG_TAX_BORNE") = TaxBy
            drNewRow("LAG_BROKERAGE") = Brokerage
            drNewRow("LAG_OTHCHRGS_1") = OtherCharges1
            drNewRow("LAG_OTHCHRGS_2") = OtherCharges2
            drNewRow("LAG_OTHCHRGS_3") = OtherCharges3
            drNewRow("LAG_FLR_REBATE") = FloorRebate
            drNewRow("LAG_OTHCHRGS_REM") = OtherChargesRemark
            ' drNewRow("LAG_OUTFLOW_REM") = OutflowRemark
            drNewRow("LAG_Occupied_by") = occupiedby
            drNewRow("LAG_PTYPE") = PaymentType
            drNewRow("LAG_MNCPL_TAX") = MunicipalTax
            drNewRow("LAG_HouseTAX") = HouseTax
            drNewRow("LAG_LTERM_REM") = LeaseTermRemarks
            drNewRow("LAG_UPD_BY") = uid
            drNewRow("LAG_UPD_DT") = getoffsetdatetime(DateTime.Now)
            drNewRow("LAG_STA_ID") = 1
            'Newly Added Fields
            drNewRow("LAG_ESC_TERM") = EscTerm
            drNewRow("LAG_ESC_PERCENT") = EscPercentage
            drNewRow("LAG_RENTABLE_CARPARKS") = RentableCarParks
            drNewRow("LAG_CARPARK_PERSLOT") = CarparkPerSlot
            drNewRow("LAG_TOTAL_CARPARKS") = TotalCarParks
            drNewRow("LAG_CAM_CHARGES") = CamChages
            drNewRow("LAG_RENEWAL_TERM") = RenewalTermination
            drNewRow("LAG_INTFREE_SECDEPOSIT") = IntFreeSecDep
            drNewRow("LAG_AREA_SQFT") = areapersqft
            drNewRow("LAG_COST_SQFT") = costpersqft


            dtLeaseTerms.Rows.Add(drNewRow)
            objPB.subInsertData(dtLeaseTerms)
            SaveDetails = strReqID

        Catch err As PMSException
            Throw err

        Catch err As Exception
            SaveDetails = ""

        End Try

    End Function
#End Region

#Region " Agreement Details "

    Public Sub OpenAgreement(ByVal AgNo As String)

        Dim obj As New clsPMSBusiness
        dtSites = obj.fnSelectSites(clsPMSBusiness.QueryType.Agreement, AgNo)

    End Sub
#End Region

#Region " Update Details "

    Public Function UpdatePremise(ByVal uid As String) As Boolean
        Try
            Dim objData As New clsData
            Dim strColumns As String, strValues As String
            Dim status As Boolean = True

            If PropType = "New" Then
                strColumns = "PNP_NAME,PNP_ADDRESS,PNP_ROAD,PNP_LANDMARK,PNP_CTSNo,PNP_TOWNPLANNo,PNP_PINCODE,PNP_UPT_BY,PNP_UPT_DT"
                strValues = "'" & PremiseName & "','" & PremiseAddress & "','" & Road & "','" & Landmark & "','" & CTS & "','" & TownPlan & "','" & _
                            Pincode & "','" & uid & "','" & Now & "'"

                status = objData.fnUpdateRow("PMS_NEWPREMISE", strColumns, strValues, "PNP_ID=" & PremiseID)
            End If

            If status = True Then
                strColumns = "PBR_CNY_ID,PBR_RGN_ID,PBR_STE_ID,PBR_CTY_ID,PBR_LCM_ID,PBR_GSC_ID,PBR_CAT_ID,PBR_SCAT_ID,PBR_LANDAVAIL,PBR_LANDAREA,PBR_LANDCOST,PBR_UPT_BY,PBR_UPT_DT"
                strValues = Country & "," & Region & "," & State & "," & Centre & "," & Location & "," & GSC & "," & Category & "," & Subcategory & _
                            ",'" & LandCon & "'," & LandArea & "," & LandCost & ",'" & uid & "','" & Now & "' "


                status = objData.fnUpdateRow("PMS_BUILDING_REQUEST", strColumns, strValues, "PBR_PREQ_ID='" & ReqID & "'")
            End If

            Return status

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function UpdateLeaseTerms(ByVal uid As String) As Boolean
        Try
            Dim objData As New clsData
            Dim strColumns As String, strValues As String

            strColumns = "LAG_LDEED_DT,LAG_LDEED_NO,LAG_LCOMM_DT,LAG_CLOSING_DT,LAG_LEASE_PERIOD,LAG_LOCK_DT, " & _
                         "LAG_MRENT_AMT,LAG_MNCPL_TAX,LAG_HouseTAX,LAG_STAMPDUTY,LAG_STDUTY_LESS,LAG_REGISTRATION," & _
                         "LAG_BROKERAGE,LAG_RTN,LAG_ADJ_ADVANCE,LAG_Occupied_by,LAG_LTERM_REM,LAG_OTHCHRGS_REM," & _
                         "LAG_PTYPE,LAG_STROOM_BY,LAG_STCHGS_BY,LAG_ELECDEP_BY,LAG_ELECDEP,LAG_TBLOCKS_BY,LAG_STCASE_BY," & _
                         "LAG_OTHCHRGS_1,LAG_OTHCHRGS_2,LAG_OTHCHRGS_3,LAG_FLR_REBATE,LAG_LPER_EXTN,LAG_LOCK_PER,LAG_RENT_FP, " & _
                         "LAG_CPARK_NOS,LAG_TAX_BORNE,LAG_Upd_By,LAG_Upd_DT,LAG_ESC_TERM,LAG_ESC_PERCENT,LAG_RENTABLE_CARPARKS,LAG_CARPARK_PERSLOT,LAG_TOTAL_CARPARKS,LAG_CAM_CHARGES,LAG_RENEWAL_TERM,LAG_INTFREE_SECDEPOSIT,LAG_AREA_SQFT,LAG_COST_SQFT"

            strValues = "'" & LDeedDate & "','" & LDeedNo & "','" & LCommencement & "','" & LExpiry & "'," & LPeriod & ",'" & _
                        LockDate & "'," & MonthlyRent & "," & MunicipalTax & "," & HouseTax & "," & StampDuty & "," & _
                        StampDutyPercent & "," & Registration & "," & Brokerage & "," & Retention & "," & AdjustAmount & ",'" & _
                        occupiedby & "','" & LeaseTermRemarks & "','" & OtherChargesRemark & "','" & PaymentType & "','" & _
                        StrongRoom & "','" & StructuralChanges & "','" & ElecDeposit & "'," & ElecAmount & ",'" & ToiletBlock & "','" & _
                        StairCase & "'," & OtherCharges1 & "," & OtherCharges2 & "," & OtherCharges3 & "," & FloorRebate & "," & _
                        LPeriodExt & "," & LLockPeriod & "," & RentFree & ",'" & CarParking & "','" & TaxBy & "','" & uid & "','" & Now & "','" & EscTerm & "','" & EscPercentage & "','" & RentableCarParks & "','" & CarparkPerSlot & "','" & TotalCarParks & "','" & CamChages & "','" & RenewalTermination & "','" & IntFreeSecDep & "','" & areapersqft & "','" & costpersqft & "'"


            Return objData.fnUpdateRow("PMS_LEASE_TERMS", strColumns, strValues, "LAG_PREQ_ID='" & ReqID & "'")

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function UpdateDistributions(ByVal uid As String) As Boolean
        Try
            Dim objData As New clsData
            Dim drNewRow As DataRow
            Dim strSQL As String
            Dim iNdx As Integer
            Dim dtAgrLessors, dtNOPayments, dtLessorDist As DataTable


            If LessorsAmended = True Or NatureofPaymentsAmended = True Then
                ' Deleting existing records from PMS_LESSOR_DISTRIBUTIONS, PMS_AGREEMENT_LESSORS and PMS_LEASERENTAL_DETAILS tables
                strSQL = "BEGIN Insert into " & HttpContext.Current.Session("TENANT") & "." & "PMS_AGREEMENT_LESSORS_Audit select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_AGREEMENT_LESSORS where PAL_PREQ_ID='" & ReqID & "'  " & _
                         "      Insert into " & HttpContext.Current.Session("TENANT") & "." & "PMS_LESSOR_DISTRIBUTIONS_Audit select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_LESSOR_DISTRIBUTIONS where PVD_PLD_ID in (Select PLD_ID from " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASERENTAL_DETAILS where PLD_PREQ_ID='" & ReqID & "')  " & _
                         "      Insert into " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASERENTAL_DETAILS_Audit select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASERENTAL_DETAILS where PLD_PREQ_ID='" & ReqID & "'  " & _
                         " Delete from " & HttpContext.Current.Session("TENANT") & "." & "PMS_AGREEMENT_LESSORS where PAL_PREQ_ID='" & ReqID & "'  " & _
                         " Delete from " & HttpContext.Current.Session("TENANT") & "." & "PMS_LESSOR_DISTRIBUTIONS where PVD_PLD_ID in (Select PLD_ID from " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASERENTAL_DETAILS where PLD_PREQ_ID='" & ReqID & "')  " & _
                         " Delete from " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASERENTAL_DETAILS where PLD_PREQ_ID='" & ReqID & "'  " & _
                         " END "
                SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)


                dtAgrLessors = New DataTable
                dtAgrLessors = objData.fnSelectSchema("PMS_AGREEMENT_LESSORS")

                For iNdx = 0 To dtLessors.Rows.Count - 1
                    drNewRow = dtAgrLessors.NewRow()

                    drNewRow("PAL_PREQ_ID") = ReqID
                    drNewRow("PLR_ID") = dtLessors.Rows(iNdx).Item(0)
                    drNewRow("PAL_UPD_BY") = uid
                    drNewRow("PAL_UPD_DT") = getoffsetdatetime(DateTime.Now)

                    dtAgrLessors.Rows.Add(drNewRow)
                Next

                objData.fnInsertData(dtAgrLessors)


                '  Inserting Nature of Payments and Vendor Distributions
                Dim newNOPID As Integer, iVend As Integer

                dtNOPayments = New DataTable
                dtNOPayments = objData.fnSelectSchema("PMS_LEASERENTAL_DETAILS")

                For iNdx = 0 To dtNatureOfPayments.Rows.Count - 1
                    drNewRow = dtNOPayments.NewRow()

                    drNewRow("PLD_PREQ_ID") = ReqID
                    drNewRow("PLD_FROM_DT") = dtNatureOfPayments.Rows(iNdx).Item(1)
                    drNewRow("PLD_TO_DT") = dtNatureOfPayments.Rows(iNdx).Item(2)
                    drNewRow("PLD_NOP_ID") = dtNatureOfPayments.Rows(iNdx).Item(3)
                    drNewRow("PLD_MONTHLY_OFLOW") = dtNatureOfPayments.Rows(iNdx).Item(5)
                    drNewRow("PLD_UPD_BY") = uid
                    drNewRow("PLD_UPD_DT") = getoffsetdatetime(DateTime.Now)
                    drNewRow("PLD_STA_ID") = 1

                    dtNOPayments.Rows.Add(drNewRow)

                    '  Inserting into PMS_LEASERENTAL_DETAILS table
                    newNOPID = objData.fnInsertIdentityData(dtNOPayments)


                    dtLessorDist = New DataTable
                    dtLessorDist = objData.fnSelectSchema("PMS_LESSOR_DISTRIBUTIONS")

                    For iVend = 0 To dtVendorDistributions.Rows.Count - 1
                        If dtNatureOfPayments.Rows(iNdx).Item("Sno") = dtVendorDistributions.Rows(iVend).Item("Sno") Then

                            drNewRow = dtLessorDist.NewRow

                            drNewRow("PVD_PLD_ID") = newNOPID
                            drNewRow("PVD_VEN_ID") = dtVendorDistributions.Rows(iVend).Item("VendorID")
                            drNewRow("PVD_AMOUNT") = dtVendorDistributions.Rows(iVend).Item("Amount")
                            drNewRow("PVD_UPD_BY") = uid
                            drNewRow("PVD_UPD_DT") = getoffsetdatetime(DateTime.Now)
                            drNewRow("PVD_STA_ID") = 1

                            dtLessorDist.Rows.Add(drNewRow)

                        End If
                    Next

                    '  Insering vendor distributions
                    objData.fnInsertData(dtLessorDist)
                    dtLessorDist = Nothing

                    dtNOPayments.Rows(0).Delete()
                Next


            ElseIf DistributionsAmended = True Then
                Dim iVend, existNOPID As Integer
                Dim strColumns, strValues, strWhere As String

                For iNdx = 0 To dtNatureOfPayments.Rows.Count - 1
                    For iVend = 0 To dtVendorDistributions.Rows.Count - 1
                        If dtNatureOfPayments.Rows(iNdx).Item("Sno") = dtVendorDistributions.Rows(iVend).Item("Sno") Then

                            strColumns = "PVD_AMOUNT,PVD_UPD_BY,PVD_UPD_DT,PVD_STA_ID"
                            strValues = dtVendorDistributions.Rows(iVend).Item("Amount") & ",'" & uid & "','" & Now & "',1"
                            strWhere = "PVD_VEN_ID=" & dtVendorDistributions.Rows(iVend).Item("VendorID") & " and PVD_PLD_ID=" & dtNatureOfPayments.Rows(iNdx).Item("ExistingID") & " and PVD_STA_ID=1"

                            objData.fnUpdateRow("PMS_LESSOR_DISTRIBUTIONS", strColumns, strValues, strWhere)

                        End If
                    Next
                Next

            End If

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

#End Region

#Region " Requisition Details "

    Public Sub OpenRequisition(ByVal ReqID As String)
        Dim dt As DataTable

        Dim obj As New clsPMSBusiness


        ' Requisition Details
        dt = obj.fnRequestDtls("PBR_PREQ_ID='" & ReqID & "'")
        If dt.Rows.Count > 0 Then
            Me.ReqID = ReqID
            Country = dt.Rows(0).Item("PBR_CNY_ID")
            Region = dt.Rows(0).Item("PBR_RGN_ID")
            State = dt.Rows(0).Item("PBR_STE_ID")
            Centre = dt.Rows(0).Item("PBR_CTY_ID")
            Location = dt.Rows(0).Item("PBR_LCM_ID")
            GSC = dt.Rows(0).Item("PBR_GSC_ID")
            Category = dt.Rows(0).Item("PBR_CAT_ID")
            Subcategory = dt.Rows(0).Item("PBR_SCAT_ID")
            PropType = dt.Rows(0).Item("PBR_PROP_TYPE")
            LandCon = Trim(dt.Rows(0).Item("PBR_LANDAVAIL"))
            LandArea = dt.Rows(0).Item("PBR_LANDAREA")
            LandCost = dt.Rows(0).Item("PBR_LANDCOST")
            PremiseID = dt.Rows(0).Item("PBR_PNP_ID")
            PremiseName = dt.Rows(0).Item("PNP_NAME")
            PremiseAddress = dt.Rows(0).Item("PNP_ADDRESS")
            Road = dt.Rows(0).Item("PNP_ROAD")
            Landmark = dt.Rows(0).Item("PNP_LANDMARK")
            CTS = dt.Rows(0).Item("PNP_CTSNO")
            TownPlan = dt.Rows(0).Item("PNP_TOWNPLANNO")
            Pincode = dt.Rows(0).Item("PNP_PINCODE")
        End If
        dt = Nothing


        ' Site details
        dtSites = obj.fnSelectSites(clsPMSBusiness.QueryType.Requisition, ReqID)


        '  Lease Terms
        dt = obj.fnSelect("*", "PMS_LEASE_TERMS", "LAG_PREQ_ID='" & ReqID & "'")
        If dt.Rows.Count > 0 Then
            LDeedDate = dt.Rows(0).Item("LAG_LDEED_DT")
            LDeedNo = dt.Rows(0).Item("LAG_LDEED_NO")
            LCommencement = dt.Rows(0).Item("LAG_LCOMM_DT")
            LExpiry = dt.Rows(0).Item("LAG_CLOSING_DT")
            LLockPeriod = dt.Rows(0).Item("LAG_LOCK_PER")
            LockDate = dt.Rows(0).Item("LAG_LOCK_DT")
            MonthlyRent = dt.Rows(0).Item("LAG_MRENT_AMT")
            LPeriod = dt.Rows(0).Item("LAG_LEASE_PERIOD")
            MunicipalTax = dt.Rows(0).Item("LAG_MNCPL_TAX")
            HouseTax = dt.Rows(0).Item("LAG_HOUSETAX")
            StampDuty = dt.Rows(0).Item("LAG_STAMPDUTY")
            StampDutyPercent = dt.Rows(0).Item("LAG_STDUTY_LESS")
            Registration = dt.Rows(0).Item("LAG_REGISTRATION")
            Brokerage = dt.Rows(0).Item("LAG_BROKERAGE")
            Retention = dt.Rows(0).Item("LAG_RTN")
            AdjustAmount = dt.Rows(0).Item("LAG_ADJ_ADVANCE")
            'OutflowRemark = dt.Rows(0).Item("LAG_OUTFLOW_REM")
            If IsDBNull(dt.Rows(0).Item("LAG_Occupied_by")) = True Then
                occupiedby = "NA"
            Else
                occupiedby = dt.Rows(0).Item("LAG_Occupied_by")
            End If

            OtherChargesRemark = dt.Rows(0).Item("LAG_OTHCHRGS_REM")
            PaymentType = dt.Rows(0).Item("LAG_PTYPE")
            StrongRoom = dt.Rows(0).Item("LAG_STROOM_BY")
            StructuralChanges = dt.Rows(0).Item("LAG_STCHGS_BY")
            ElecDeposit = dt.Rows(0).Item("LAG_ELECDEP_BY")
            ElecAmount = dt.Rows(0).Item("LAG_ELECDEP")
            ToiletBlock = dt.Rows(0).Item("LAG_TBLOCKS_BY")
            StairCase = dt.Rows(0).Item("LAG_STCASE_BY")
            OtherCharges1 = dt.Rows(0).Item("LAG_OTHCHRGS_1")
            OtherCharges2 = dt.Rows(0).Item("LAG_OTHCHRGS_2")
            OtherCharges3 = dt.Rows(0).Item("LAG_OTHCHRGS_3")
            FloorRebate = dt.Rows(0).Item("LAG_FLR_REBATE")
            LPeriodExt = dt.Rows(0).Item("LAG_LPER_EXTN")
            RentFree = dt.Rows(0).Item("LAG_RENT_FP")
            CarParking = dt.Rows(0).Item("LAG_CPARK_NOS")
            TaxBy = dt.Rows(0).Item("LAG_TAX_BORNE")
            EscTerm = dt.Rows(0).Item("LAG_ESC_TERM")
            EscPercentage = dt.Rows(0).Item("LAG_ESC_PERCENT")
            RentableCarParks = dt.Rows(0).Item("LAG_RENTABLE_CARPARKS")
            CarparkPerSlot = dt.Rows(0).Item("LAG_CARPARK_PERSLOT")
            TotalCarParks = dt.Rows(0).Item("LAG_TOTAL_CARPARKS")
            CamChages = dt.Rows(0).Item("LAG_CAM_CHARGES")
            RentableCarParks = dt.Rows(0).Item("LAG_RENEWAL_TERM")
            IntFreeSecDep = dt.Rows(0).Item("LAG_INTFREE_SECDEPOSIT")
            areapersqft = dt.Rows(0).Item("LAG_AREA_SQFT")
            costpersqft = dt.Rows(0).Item("LAG_COST_SQFT")
        End If
        dt = Nothing

        dtNatureOfPayments = GetEditableNOPayments(ReqID)

        ' Lessor Distributions
        dtLessors = obj.fnSelect("PLR_ID,(select PLR_NAME +' (' + cast(PLR_ID as nvarchar(10)) +')' from PMS_LESSOR where PLR_ID=PMS_AGREEMENT_LESSORS.PLR_ID) as PLR_NAME", "PMS_AGREEMENT_LESSORS", "PAL_PREQ_ID='" & ReqID & "'")

        dtVendorDistributions = obj.fnSelectLessorDistibutions(clsPMSBusiness.QueryType.Requisition, ReqID)

    End Sub
#End Region

#Region " Generate Agreement "
    Public Function PrepareAgreement(ByVal UserID As String, ByVal ApprovedReqID As String) As String
        Try
            Dim objData As New clsData
            Dim dtAgreement As New DataTable("Agreement")

            Dim drNewRow As DataRow

            dtAgreement = objData.fnSelectSchema("PMS_PROPERTY_AGREEMENT")

            drNewRow = dtAgreement.NewRow

            drNewRow("LAG_AGR_NO") = UserID
            drNewRow("LAG_PREQ_ID") = ApprovedReqID
            drNewRow("LAG_LDEED_DT") = LDeedDate
            drNewRow("LAG_LDEED_NO") = LDeedNo
            drNewRow("LAG_LCOMM_DT") = LCommencement
            drNewRow("LAG_CLOSING_DT") = LExpiry
            drNewRow("LAG_LEASE_PERIOD") = LPeriod
            drNewRow("LAG_LPER_EXTN") = LPeriodExt
            drNewRow("LAG_LOCK_PER") = LLockPeriod
            drNewRow("LAG_LOCK_DT") = LockDate
            drNewRow("LAG_MRENT_AMT") = MonthlyRent
            drNewRow("LAG_RENT_FP") = RentFree
            drNewRow("LAG_STAMPDUTY") = StampDuty
            drNewRow("LAG_STDUTY_LESS") = StampDutyPercent
            drNewRow("LAG_STCHGS_BY") = StructuralChanges
            drNewRow("LAG_TBLOCKS_BY") = ToiletBlock
            drNewRow("LAG_STROOM_BY") = StrongRoom
            drNewRow("LAG_ELECDEP_BY") = ElecDeposit
            drNewRow("LAG_ELECDEP") = ElecAmount
            drNewRow("LAG_STCASE_BY") = StairCase
            drNewRow("LAG_REGISTRATION") = Registration
            drNewRow("LAG_RTN") = Retention
            drNewRow("LAG_ADJ_ADVANCE") = AdjustAmount
            drNewRow("LAG_CPARK_NOS") = CarParking
            drNewRow("LAG_TAX_BORNE") = TaxBy
            drNewRow("LAG_BROKERAGE") = Brokerage
            drNewRow("LAG_OTHCHRGS_1") = OtherCharges1
            drNewRow("LAG_OTHCHRGS_2") = OtherCharges2
            drNewRow("LAG_OTHCHRGS_3") = OtherCharges3
            drNewRow("LAG_FLR_REBATE") = FloorRebate
            drNewRow("LAG_OTHCHRGS_REM") = OtherChargesRemark
            'drNewRow("LAG_OUTFLOW_REM") = OutflowRemark
            drNewRow("LAG_Occupied_by") = occupiedby
            drNewRow("LAG_PTYPE") = PaymentType
            drNewRow("LAG_MNCPL_TAX") = MunicipalTax
            drNewRow("LAG_HouseTAX") = HouseTax
            drNewRow("LAG_App_BY") = UserID
            drNewRow("LAG_App_DT") = getoffsetdatetime(DateTime.Now)
            drNewRow("LAG_STA_ID") = 45
            'Newly Added Fields
            drNewRow("LAG_ESC_TERM") = EscTerm
            drNewRow("LAG_ESC_PERCENT") = EscPercentage
            drNewRow("LAG_RENTABLE_CARPARKS") = RentableCarParks
            drNewRow("LAG_CARPARK_PERSLOT") = CarparkPerSlot
            drNewRow("LAG_TOTAL_CARPARKS") = TotalCarParks
            drNewRow("LAG_CAM_CHARGES") = CamChages
            drNewRow("LAG_RENEWAL_TERM") = RenewalTermination
            drNewRow("LAG_INTFREE_SECDEPOSIT") = IntFreeSecDep


            dtAgreement.Rows.Add(drNewRow)

            Dim runAgrNo As Integer = objData.fnInsertIdentityData(dtAgreement)

            dtAgreement = Nothing

            Dim strAgrNo As String = "AgNo_" & CStr(runAgrNo).PadLeft(6, "0")

            objData.fnUpdateRow("PMS_PROPERTY_AGREEMENT", "LAG_AGR_NO", "'" & strAgrNo & "'", "LAG_SNO=" & runAgrNo)

            'If PropType = "New" Then
            Dim dtPremise As New DataTable("Premise")
            dtPremise = objData.fnSelectSchema("PMS_PREMISE")

            drNewRow = dtPremise.NewRow()

            drNewRow("PRD_ID") = PremiseID
            drNewRow("PRM_NAME") = PremiseName
            drNewRow("PRM_ADDRESS") = PremiseAddress
            drNewRow("PRM_PREQ_ID") = ApprovedReqID
            drNewRow("PRM_TYPE") = PropType

            dtPremise.Rows.Add(drNewRow)

            objData.fnInsertData(dtPremise)

            Return strAgrNo

        Catch ex As Exception
            Return ""
        End Try

    End Function
#End Region

#Region " Dispose Class "

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        dtSites = Nothing
        dtLessors = Nothing
        dtNatureOfPayments = Nothing
        dtVendorDistributions = Nothing
    End Sub

    Protected Overrides Sub Finalize()
        Dispose()
    End Sub
#End Region

    Private Function GetEditableNOPayments(ByVal RequisitionID As String) As DataTable
        Try
            Dim objData As New clsData
            Dim dtSource As New DataTable
            Dim dtFormatted As New DataTable("NOPayments")
            Dim drrow As DataRow
            Dim strQuery As String
            Dim iNdx As Integer

            strQuery = "SELECT PLD.PLD_FROM_DT, PLD.PLD_TO_DT, PLD.PLD_NOP_ID, PNP.NOP_NAME, PLD.PLD_MONTHLY_OFLOW,PLD.PLD_ID " & _
                       "FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_NATURE_OF_PAYMENTS PNP INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASERENTAL_DETAILS PLD ON PNP.NOP_ID = PLD.PLD_NOP_ID " & _
                       "WHERE PLD.PLD_STA_ID = 1 and PLD.PLD_PREQ_ID='" & RequisitionID & "'"

            dtSource = objData.fnSelectData(strQuery)

            dtFormatted.Columns.Add("Sno", Type.GetType("System.Int64"))
            dtFormatted.Columns.Add("FromDate", Type.GetType("System.String"))
            dtFormatted.Columns.Add("ToDate", Type.GetType("System.String"))
            dtFormatted.Columns.Add("NOPaymentN", Type.GetType("System.Int64"))
            dtFormatted.Columns.Add("NOPaymentS", Type.GetType("System.String"))
            dtFormatted.Columns.Add("LeaseAmt", Type.GetType("System.Single"))
            dtFormatted.Columns.Add("ExistingID", Type.GetType("System.Int64"))

            For iNdx = 0 To dtSource.Rows.Count - 1
                drrow = dtFormatted.NewRow

                drrow("Sno") = iNdx + 1
                drrow("FromDate") = CDate(dtSource.Rows(iNdx).Item("PLD_FROM_DT")).ToShortDateString
                drrow("ToDate") = CDate(dtSource.Rows(iNdx).Item("PLD_TO_DT")).ToShortDateString
                drrow("NOPaymentN") = dtSource.Rows(iNdx).Item("PLD_NOP_ID")
                drrow("NOPaymentS") = dtSource.Rows(iNdx).Item("NOP_NAME")
                drrow("LeaseAmt") = dtSource.Rows(iNdx).Item("PLD_MONTHLY_OFLOW")
                drrow("ExistingID") = dtSource.Rows(iNdx).Item("PLD_ID")

                dtFormatted.Rows.Add(drrow)
            Next

            Return dtFormatted

        Catch er As Exception

            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function
End Class


Public Class clsPMSBusiness

#Region " Local Variable Declaration "

    Private Shared objDataClass As New clsData

#End Region

#Region " Enumerations "

    Public Enum LocalityType As Integer
        Country
        Region
        State
        Centre
        Location
        GSC
    End Enum

    Public Enum QueryType As Integer
        Requisition
        Agreement
    End Enum

#End Region

#Region " Select Functions "

    Public Function fnSelect(ByVal strColumn As String, ByVal strTable As String, Optional ByVal strCondition As String = "") As DataTable
        Try
            Dim dtGroup As New DataTable(strTable)
            Dim strQuery As String

            strQuery = "SELECT " & strColumn & " FROM " & HttpContext.Current.Session("TENANT") & "." & strTable

            If strCondition.Trim <> "" Then strQuery = strQuery & " Where " & strCondition

            dtGroup = objDataClass.fnSelectData(strQuery)


            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable

            Return dtTemp
        End Try
    End Function

    Public Function fnSelect(ByVal strColumns As String, ByVal strTable As String, ByVal strWhereClause As String, ByVal strOrderClause As String) As DataTable
        Try
            Dim dtGroup As New DataTable(strTable)
            Dim strQuery As String

            strQuery = "SELECT distinct " & strColumns & " FROM " & HttpContext.Current.Session("TENANT") & "." & strTable & " Where " & strWhereClause & " Order By " & strOrderClause
            dtGroup = objDataClass.fnSelectData(strQuery)

            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable

            Return dtTemp
        End Try
    End Function

#Region " Request Details "
    Public Function fnRequestDtls(Optional ByVal strWhereClause As String = "") As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PBR_ID,PBR_PNP_ID,PNP_NAME,PNP_ADDRESS,PBR_CNY_ID,PBR_RGN_ID,PBR_STE_ID,PBR_CTY_ID,")
            strQuery.Append("PBR_LCM_ID,PBR_GSC_ID,PBR_CAT_ID,PBR_SCAT_ID,PBR_PROP_TYPE, PBR_STA_ID,PBR_LANDAVAIL, ")
            strQuery.Append("PBR_LANDAREA,PBR_LANDCOST,PNP_ROAD,PNP_LANDMARK,PNP_CTSNO,PNP_TOWNPLANNO,PNP_PINCODE ")
            strQuery.Append("FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "PMS_NEWPREMISE ")
            strQuery.Append("ON PMS_BUILDING_REQUEST.PBR_PNP_ID = PMS_NEWPREMISE.PNP_ID Where 1=1 ")

            If strWhereClause <> "" Then
                strQuery.Append(" AND " & strWhereClause)
            End If

            strQuery.Append(" ORDER BY PBR_PREQ_ID ")

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function

#End Region

#Region " Category & Subcategory "
    Public Shared Function fnGetCategories() As DataSet
        Try
            Dim ds As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Category order by PCY_NAME"
            ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return ds

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetCategories(ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Category "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by PCY_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetCategories(ByVal strColumns As String, ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select " & strColumns & " from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Category "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by PCY_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetCategoryName(ByVal CategoryID As Integer) As String
        Dim result As String = ""
        Try
            strSQL = "select PCY_NAME from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Category Where PCY_ID=" & CategoryID

            result = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)
            Return result

        Catch ex As Exception
            Return result
        End Try

    End Function


    Public Shared Function fnGetSubCategories() As DataSet
        Try
            Dim ds As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_SubCategory order by PSC_NAME"
            ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return ds

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetSubCategories(ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_SubCategory "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by PSC_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetSubCategories(ByVal strColumns As String, ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select " & strColumns & " from " & HttpContext.Current.Session("TENANT") & "." & "PMS_SubCategory "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by PSC_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try

    End Function

    Public Shared Function fnGetSubcategoryName(ByVal SubcategoryID As Integer) As String
        Dim result As String = ""
        Try
            strSQL = "select PSC_NAME from " & HttpContext.Current.Session("TENANT") & "." & "PMS_SubCategory Where PSC_ID=" & SubcategoryID

            result = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)
            Return result

        Catch ex As Exception
            Return result
        End Try
    End Function

#End Region

#Region " Agreements &  Documents "
    Public Function fnGetDocuments(ByVal AgreementNo As String) As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strWhere As String

            strWhere = "PBD_AGR_NO = '" & AgreementNo & "' AND PBD_STA_ID=1"

            dtGroup = fnSelect("PBD_ID,PBD_DOC_TYPE,PBD_DOC_RECDT,PBD_DOC_STATUS,PBD_DOC_LINK", "PMS_BUILDING_DOCS", strWhere, "PBD_DOC_TYPE")
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function

    Public Function fnGetAgreements(Optional ByVal strCondition As String = "") As DataTable
        Try
            Dim dt As New DataTable("Report")
            Dim strQry As String

            strQry = "Select * from PMS_LOCATION_AGREEMENT_VW Where 1=1 "

            If strCondition.Trim <> "" Then strQry = strQry & strCondition

            strQry = strQry & " Order By PRM_NAME"

            dt = objDataClass.fnSelectData(strQry)
            Return dt

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function

#End Region

#Region " Lease Deeds "

    Public Function fnSelectLeaseDeeds(ByVal PremiseID As Integer) As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strWhere As String

            strWhere = "PRD_ID = " & PremiseID & " and LAG_STA_ID=45"

            dtGroup = fnSelect("LAG_LDEED_NO", "PMS_PREMISE_AGREEMENTS_VW", strWhere, "LAG_LDEED_NO")
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function

    Public Function fnSelectLeaseDeeds(Optional ByVal strCondition As String = "") As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strQuery As String = "Select LAG_LDEED_NO from PMS_PREMISE_AGREEMENTS_VW "

            If strCondition.Trim <> "" Then strQuery = strQuery & " Where " & strCondition

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function

#End Region

    Public Function fnGetRequisitions(Optional ByVal strCondition As String = "") As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST.PBR_PREQ_ID, convert(nvarchar," & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASE_TERMS.LAG_LCOMM_DT,101) LAG_LCOMM_DT,")
            strQuery.Append("convert(nvarchar," & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASE_TERMS.LAG_CLOSING_DT,101) LAG_CLOSING_DT," & HttpContext.Current.Session("TENANT") & "." & "PMS_NEWPREMISE.PNP_NAME,")
            strQuery.Append("(SELECT PCY_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_CATEGORY WHERE pcy_id = PMS_BUILDING_REQUEST.PBR_CAT_ID) AS CATEGORY ")
            strQuery.Append("FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "PMS_NEWPREMISE ON " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST.PBR_PNP_ID = " & HttpContext.Current.Session("TENANT") & "." & "PMS_NEWPREMISE.PNP_ID ")
            strQuery.Append("INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASE_TERMS ON " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST.PBR_PREQ_ID = " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASE_TERMS.LAG_PREQ_ID ")
            strQuery.Append("WHERE 1=1 and pbr_sta_id in ('21','5')")

            If strCondition <> "" Then
                strQuery.Append(" AND " & strCondition)
            End If

            strQuery.Append(" ORDER BY PBR_PREQ_ID DESC")

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function
    Public Function fnGetRequisitions_new(Optional ByVal strCondition As String = "") As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PMS_BUILDING_REQUEST.PBR_PREQ_ID, convert(nvarchar,PMS_LEASE_TERMS.LAG_LCOMM_DT,101) LAG_LCOMM_DT,")
            strQuery.Append("convert(nvarchar,PMS_LEASE_TERMS.LAG_CLOSING_DT,101) LAG_CLOSING_DT,PMS_NEWPREMISE.PNP_NAME,")
            strQuery.Append("(SELECT PCY_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_CATEGORY WHERE pcy_id = PMS_BUILDING_REQUEST.PBR_CAT_ID) AS CATEGORY ")
            strQuery.Append("FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "PMS_NEWPREMISE ON PMS_BUILDING_REQUEST.PBR_PNP_ID = PMS_NEWPREMISE.PNP_ID ")
            strQuery.Append("INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "PMS_LEASE_TERMS ON PMS_BUILDING_REQUEST.PBR_PREQ_ID = PMS_LEASE_TERMS.LAG_PREQ_ID ")
            strQuery.Append("WHERE 1=1 and pbr_sta_id='21'")

            If strCondition <> "" Then
                strQuery.Append(" or " & strCondition)
            End If

            strQuery.Append(" ORDER BY PBR_PREQ_ID DESC")

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function
#Region " Mail IDs "

    Public Shared Function GetRequestor_MailID(ByVal RequisitionID As String) As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT AUR_FULL_NAME,AUR_EMAIL from AMT_USER_ROLE_VW ")
            strQuery.Append("Where AUR_ID in (Select PBR_UPT_BY from " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST where PBR_PREQ_ID='" & RequisitionID & "')")

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function

    Public Shared Function GetRH_HOD_MailID(ByVal RequisitionID As String) As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT AUR_FULL_NAME,AUR_EMAIL from " & HttpContext.Current.Session("TENANT") & "." & "AMT_USER_ROLE_VW ")
            strQuery.Append("Where AUR_ID in (Select PBR_FAPP_BY from " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST where PBR_PREQ_ID='" & RequisitionID & "')")

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function

    Public Shared Function GetAdmin_MailID(ByVal RequisitionID As String) As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT AUR_FULL_NAME,AUR_EMAIL from " & HttpContext.Current.Session("TENANT") & "." & "AMT_USER_ROLE_VW ")
            strQuery.Append("Where AUR_ID in (Select PBR_SAPP_BY from " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDING_REQUEST where PBR_PREQ_ID='" & RequisitionID & "')")

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception

            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function

#End Region

#Region " Nature of Payments "

    Public Shared Function fnGetNatureOfPayments() As DataSet
        Try
            Dim ds As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Nature_of_Payments order by NOP_NAME"
            ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return ds

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetNatureOfPayments(ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Nature_of_Payments "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by NOP_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetNatureOfPayments(ByVal strColumns As String, ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select " & strColumns & " from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Nature_of_Payments "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by NOP_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

#End Region

#Region " Site Details "

    Public Function fnSelectSites(ByVal Query As QueryType, ByVal Value As String) As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PBD_SNO, PBD_BNAME,(SELECT TWR_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "Tower WHERE twr_id = PBD_TOWER) AS Tower, ")
            strQuery.Append("(SELECT FLR_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "[Floor] WHERE FLR_ID = PBD_FLOOR) AS [Floor],")
            strQuery.Append("(SELECT WNG_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "Wing WHERE WNG_ID = PBD_WING) AS Wing, ")
            strQuery.Append("PBD_FLAT_NO, PBD_BP_AREA, PBD_CP_AREA, PBD_COST_SQFT, PBD_STA_ID ")
            strQuery.Append("FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_BUILDINGSITE_DETAILS WHERE PBD_STA_ID=1 ")

            If Query = QueryType.Agreement Then
                strQuery.Append(" and PBD_PREQ_ID in (Select LAG_PREQ_ID FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_PROPERTY_AGREEMENT where LAG_AGR_NO='" & Value & "') ")
            ElseIf Query = QueryType.Requisition Then
                strQuery.Append(" and PBD_PREQ_ID = '" & Value & "' ")
            End If

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function
    Public Function fnGetRent(ByVal strReq As String) As Int32
        Try
            Dim dtToday As Date
            Dim strQuery As String
            Dim iResult As Int32
            dtToday = getoffsetdate(Date.Today)
            strQuery = "select isnull(" & HttpContext.Current.Session("TENANT") & "." & "GetRent('" & dtToday & "','" & strReq & "'),convert(int,lag_mrent_amt)) from " & HttpContext.Current.Session("TENANT") & "." & "pms_property_agreement where lag_preq_id='" & strReq & "' "

            iResult = objDataClass.fnSelectScalar(strQuery)
            Return iResult
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Function fnGetTotalArea(ByVal strReq As String) As DataTable

        Try
            Dim dtArea As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("select sum(convert(int,pbd_bp_area)),sum(convert(int,pbd_cp_area)) from " & HttpContext.Current.Session("TENANT") & "." & "pms_buildingsite_details where pbd_preq_id='" & strReq & "'")


            dtArea = objDataClass.fnSelectData(strQuery)
            Return dtArea

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function
    Public Function fnSelectSiteFloors(Optional ByVal strCondition As String = "") As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT [FLOOR],WING,PBD_BP_AREA ,PBD_CP_AREA from " & HttpContext.Current.Session("TENANT") & "." & "PMS_PREMISE_SITES_VW  ")

            If strCondition.Trim <> "" Then
                strQuery.Append("WHERE " & strCondition)
            End If

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function
    Public Function fnSelectLessors(Optional ByVal strCondition As String = "") As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("select plr_name from " & HttpContext.Current.Session("TENANT") & "." & "pms_agreement_lessors al join  pms_lessor pl on al.plr_id=pl.plr_id ")

            If strCondition.Trim <> "" Then
                strQuery.Append("WHERE " & strCondition)
            End If

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function


    Public Function fnSelectSitePremises(Optional ByVal strCondition As String = "") As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT distinct COUNTRY,REGION,STATE,CENTRE,LOCATION,GSC,PRM_NAME from " & HttpContext.Current.Session("TENANT") & "." & "PMS_PREMISE_SITES_VW  ")

            If strCondition.Trim <> "" Then
                strQuery.Append("WHERE " & strCondition)
            End If

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function

    Public Function fnSelectSiteBuildings(Optional ByVal strCondition As String = "") As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_PREMISE_BUILDINGS_VW Where 1=1 ")

            If strCondition.Trim <> "" Then
                strQuery.Append(strCondition)
            End If

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As DataTable
            Return dtTemp
        End Try
    End Function

#End Region

#Region " Lessor Details "
    Public Shared Function fnGetLessors() As DataSet
        Try
            Dim ds As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Lessor order by PLR_NAME"
            ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return ds

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetLessors(ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Lessor "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by PLR_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetLessors(ByVal strColumns As String, ByVal strWhere As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select " & strColumns & " from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Lessor "
            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere
            strSQL = strSQL & " order by PLR_NAME"

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Shared Function fnGetLessors(ByVal strColumns As String, ByVal strWhere As String, ByVal sortColumns As String) As DataSet
        Try
            Dim dtresult As DataSet
            strSQL = "select " & strColumns & " from " & HttpContext.Current.Session("TENANT") & "." & "PMS_Lessor "

            If strWhere <> "" Then strSQL = strSQL & " Where " & strWhere

            If sortColumns <> "" Then strSQL = strSQL & " order by " & sortColumns

            dtresult = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return dtresult

        Catch ex As Exception
            Dim dt As DataSet
            Return dt
        End Try
    End Function

    Public Function fnSelectLessors(ByVal Query As QueryType, ByVal Value As String) As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PMS_LESSOR.PLR_ID, PLR_CODE, PLR_NAME , PLR_ADDRESS,  ")
            strQuery.Append("PLR_OFFICE_PHNO, PLR_PINCODE, PLR_MOBILE_PHNO, PLR_RESIDENCE_PHNO, ")
            strQuery.Append("PLR_EMAIL, PLR_PAN_NO, PLR_GIR_NO,PLR_TAN_NO, PLR_BANKAC_NO, PLR_STA_ID ")
            strQuery.Append("FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_LESSOR INNER JOIN " & HttpContext.Current.Session("TENANT") & "." & "PMS_AGREEMENT_LESSORS ON " & HttpContext.Current.Session("TENANT") & "." & "PMS_LESSOR.PLR_ID = " & HttpContext.Current.Session("TENANT") & "." & "PMS_AGREEMENT_LESSORS.PLR_ID ")
            strQuery.Append("WHERE 1=1 ")

            If Query = QueryType.Agreement Then
                strQuery.Append(" and PAL_PREQ_ID in (Select LAG_PREQ_ID FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_PROPERTY_AGREEMENT where LAG_AGR_NO='" & Value & "') ")

            ElseIf Query = QueryType.Requisition Then
                strQuery.Append(" and PAL_PREQ_ID = '" & Value & "' ")

            End If

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function

    Public Function fnLessorPremises(ByVal LessorID As String, Optional ByVal strCondition As String = "") As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PPA.PRD_ID,PPA.PRM_NAME, PPA.PRM_Address ")
            strQuery.Append("FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_AGREEMENT_LESSORS AL INNER JOIN  " & HttpContext.Current.Session("TENANT") & "." & "PMS_PREMISE_AGREEMENTS_VW PPA ")
            strQuery.Append(" ON AL.PAL_PREQ_ID = PPA.PBR_PREQ_ID WHERE AL.PLR_ID =" & LessorID)

            If strCondition.Trim <> "" Then strQuery.Append(" and " & strCondition)

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As DataTable
            Return dtTemp
        End Try
    End Function

#Region " Lessor Distributions "

    Public Function fnSelectLessorDistibutions(ByVal Query As QueryType, ByVal Value As String) As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT * FROM PMS_PROPERTY_NOPAYMENTS_VW WHERE 1=1 AND PLD_STA_ID=1 ")

            If Query = QueryType.Agreement Then
                strQuery.Append(" and PBR_PREQ_ID in (Select LAG_PREQ_ID FROM " & HttpContext.Current.Session("TENANT") & "." & "PMS_PROPERTY_AGREEMENT where LAG_AGR_NO='" & Value & "') ")
            ElseIf Query = QueryType.Requisition Then
                strQuery.Append(" and PBR_PREQ_ID = '" & Value & "' ")
            End If

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function
#End Region

#End Region

#Region " Premise Details "

    Public Function fnGetPremiseLocality(ByVal PremiseID As Integer) As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PBR_CNY_ID,PBR_RGN_ID, PBR_STE_ID, PBR_CTY_ID, PBR_LCM_ID,PBR_GSC_ID ")
            strQuery.Append("From " & HttpContext.Current.Session("TENANT") & "." & "PMS_Premise_Location_VW Where PRD_ID=" & PremiseID)

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try
    End Function

    Public Function fnSelectPremisePhotos(ByVal strWhereClause As String) As DataTable

        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PPP_Sno, PPP_PHOTO, PPP_Info from " & HttpContext.Current.Session("TENANT") & "." & "PMS_PREMISE_PHOTOS ")

            If strWhereClause <> "" Then
                strQuery.Append("Where " & strWhereClause & " and PPP_STATUS=1")
            End If

            strQuery.Append(" ORDER BY PPP_SNO")

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable

            Return dtTemp
        End Try

    End Function

    Public Function fnGetPremiseName(ByVal PremiseID As Integer) As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strQuery As New StringBuilder

            strQuery.Append("SELECT PNP_NAME, PNP_ADDRESS from " & HttpContext.Current.Session("TENANT") & "." & "PMS_NEWPREMISE ")
            strQuery.Append("Where PNP_ID=" & PremiseID)

            dtGroup = objDataClass.fnSelectData(strQuery)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp

        End Try

    End Function

#End Region

#End Region

#Region " Select Schema "
    Public Function fnTableSchema(ByVal strTable As String) As DataTable
        Try
            Dim dtTable As New DataTable

            dtTable = objDataClass.fnSelectSchema(strTable)
            Return dtTable

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp

        End Try

    End Function
#End Region

#Region " Upload Photo "

    Public Function fnUploadPremisePhoto(ByVal Premise As Integer, ByVal UpFile As HtmlInputFile, ByVal PhysicalPath As String) As Boolean
        Dim d As Integer
        Dim sPath, sfile, sFullPath, sPathFriendly, sextn, sfilenm As String    'holds Path values for Document upload
        Dim sSplit(), sSplitz() As String                                       'holds path values for Document upload
        Dim strSql As String
        Dim str() As String

        sPath = PhysicalPath & "PMS\PHOTOS\"
        If Right(sPath, 1) <> "\" Then
            sPathFriendly = sPath 'Friendly path name for display
            sPath = sPath & "\"
        Else
            sPathFriendly = Left(sPath, Len(sPath) - 1)
        End If

        sfile = "Bldg" & Premise & ".jpg"
        sFullPath = sPath & sfile

        UpFile.PostedFile.SaveAs(sFullPath)


    End Function

#End Region

#Region " Insert Data "

    Public Function fnInsertData(ByVal dtTable As DataTable) As Int64

        Try
            fnInsertData = objDataClass.fnInsertIdentityData(dtTable)

            Return fnInsertData
        Catch er As Exception

            Console.Write(er.Message)
        End Try

    End Function


    Public Sub subInsertData(ByVal dtTable As DataTable)

        Try
            objDataClass.fnInsertData(dtTable)

        Catch er As Exception

            Console.Write(er.Message)
        End Try

    End Sub

    Public Sub subInsertData(ByVal dtSet As DataSet, ByVal strTable As String)

        Try
            objDataClass.fnInsertData(dtSet, strTable)

        Catch er As Exception

            Console.Write(er.Message)
        End Try

    End Sub

#End Region

#Region " Update Data "

    Public Sub subUpdateData(ByVal dtTable As DataTable)

        Try
            objDataClass.fnUpdateData(dtTable)

        Catch er As Exception

        End Try

    End Sub

    Public Sub subUpdateData(ByVal dtSet As DataSet, ByVal strTable As String)

        Try
            objDataClass.fnUpdateData(dtSet, strTable)

        Catch er As Exception

        End Try

    End Sub

    Public Sub subUpdateRow(ByVal strTable As String, ByVal strColumns As String, ByVal strValues As String, ByVal strWhereClause As String)

        Try
            objDataClass.fnUpdateRow(strTable, strColumns, strValues, strWhereClause)

        Catch er As Exception

        End Try

    End Sub

#End Region

#Region " Delete Data "
    Public Sub subDeleteRow(ByVal strTable As String, ByVal strWhereClause As String)
        Try
            objDataClass.fnDeleteRow(strTable, strWhereClause)
        Catch er As Exception
            Console.Write(er.Message)
        End Try
    End Sub

#End Region

#Region " Terminate Agreement "
    Public Function fnSelectTerminablePremises() As DataTable
        Try
            Dim dtGroup As New DataTable
            Dim strWhere As String

            strWhere = "( cast('" & Now & "' as smalldatetime) between LAG_LCOMM_DT and LAG_CLOSING_DT) " & _
                       " and (cast('" & Now & "' as smalldatetime) >= LAG_LOCK_DT) and LAG_STA_ID=45"

            dtGroup = fnSelect("distinct PRD_ID,PRM_NAME", "PMS_PREMISE_AGREEMENTS_VW", strWhere)
            Return dtGroup

        Catch er As Exception
            Dim dtTemp As New DataTable
            Return dtTemp
        End Try

    End Function

    Public Function fnTerminate(ByVal uid As String, ByVal AgrNo As String, ByVal Remarks As String) As Boolean
        Try
            Dim dtStatus As New DataTable("Status")
            dtStatus = clsMasters.GetStatus("STA_CODE='TERM'")

            If Not (dtStatus Is Nothing) Then
                Dim strColList As String = "LAG_STA_ID,LAG_Terminated_By,LAG_Terminated_DT,LAG_Terminated_Rem"
                Dim strValueList As String = dtStatus.Rows(0).Item("STA_ID") & ",'" & uid & "','" & Now & "','" & Remarks & "'"

                subUpdateRow("PMS_PROPERTY_AGREEMENT", strColList, strValueList, "LAG_AGR_NO='" & AgrNo & "'")

                dtStatus.Dispose()
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

#Region " Reports "
    Public Function repLeaseDetails(ByVal strCondition As String) As DataTable
        Try
            Dim dt As New DataTable("Report")
            Dim strQry As String

            strQry = "Select * from " & HttpContext.Current.Session("TENANT") & "." & "PMS_LOCATION_AGREEMENT_VW Where 1=1 "

            If strCondition.Trim <> "" Then strQry = strQry & strCondition

            strQry = strQry & " Order By LAG_AGR_NO"

            dt = objDataClass.fnSelectData(strQry)
            Return dt

        Catch ex As Exception
            Dim dt As DataTable
            Return dt

        End Try

    End Function

    Public Function repExpiryDetails(ByVal StartDate As Date, ByVal EndDate As Date, Optional ByVal strCondition As String = "") As DataTable
        Try
            Dim strCond As String
            Dim dt As New DataTable("ExpiryReport")

            If strCondition.Trim <> "" Then strCond = strCond & strCondition

            strCond = strCond & "  and (LAG_CLOSING_DT between cast('" & StartDate & "' as smalldatetime) and cast('" & EndDate & "' as smalldatetime)) "

            dt = repLeaseDetails(strCond)
            Return dt

        Catch ex As Exception

            Dim dt As DataTable
            Return dt

        End Try

    End Function


#End Region

End Class


#Region " Exception "

Public Class PMSException
    Inherits Exception

    Private ErrorMessage As String

    Sub New()

    End Sub

    Sub New(ByVal ErrMessage As String)
        ErrorMessage = ErrMessage
    End Sub

    Public Overrides ReadOnly Property Message() As String
        Get
            Return ErrorMessage
        End Get

    End Property

End Class

#End Region