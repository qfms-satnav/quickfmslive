Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Collections.Generic
Imports System.Xml
Imports System

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class AddUsers
    Inherits System.Web.Services.WebService
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function

    <WebMethod()> _
    Public Function AddUser(ByVal Title As String, ByVal Firstname As String, ByVal Middlename As String, ByVal lastname As String, ByVal Emailid As String, ByVal Userid As String, ByVal Country As String, ByVal City As String, ByVal Location As String, ByVal Password As String, ByVal Grade As String, ByVal Department As String, ByVal Designation As String, ByVal Reporting_Manager As String, ByVal Address As String, ByVal Mobileno As String, ByVal Doj As Date)
        Try
            Dim status As Integer
            Dim knownas As String
            knownas = Firstname & " " & Middlename & " " & lastname
            Dim valmax, aurno, gender As String
            valmax = Userid
            aurno = valmax
            If Title = "Mr." Then
                gender = "Male"
            Else
                gender = "Female"
            End If
            Dim sp1 As New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 50)
            sp1.Value = Userid
            Dim sp2 As New SqlParameter("@AUR_NO", SqlDbType.NVarChar, 50)
            sp2.Value = aurno
            Dim sp3 As New SqlParameter("@AUR_TITLE", SqlDbType.NVarChar, 50)
            sp3.Value = Title
            Dim sp4 As New SqlParameter("@AUR_FIRST_NAME", SqlDbType.NVarChar, 50)
            sp4.Value = Firstname
            Dim sp5 As New SqlParameter("@AUR_MIDDLE_NAME", SqlDbType.NVarChar, 50)
            sp5.Value = Middlename
            Dim sp6 As New SqlParameter("@AUR_LAST_NAME", SqlDbType.NVarChar, 50)
            sp6.Value = lastname
            Dim sp7 As New SqlParameter("@AUR_KNOWN_AS", SqlDbType.NVarChar, 150)
            sp7.Value = knownas
            Dim sp8 As New SqlParameter("@AUR_EMAIL", SqlDbType.NVarChar, 250)
            sp8.Value = Emailid
            Dim sp9 As New SqlParameter("@AUR_REPORTING_TO", SqlDbType.NVarChar, 50)
            sp9.Value = Reporting_Manager
            Dim sp10 As New SqlParameter("@AUR_BDG_ID", SqlDbType.NVarChar, 50)
            sp10.Value = Location
            Dim sp11 As New SqlParameter("@AUR_GENDER", SqlDbType.NVarChar, 50)
            sp11.Value = gender
            Dim sp12 As New SqlParameter("@AUR_CITY", SqlDbType.NVarChar, 50)
            sp12.Value = City
            Dim sp13 As New SqlParameter("@AUR_GRADE", SqlDbType.NVarChar, 50)
            sp13.Value = Grade
            Dim sp14 As New SqlParameter("@USR_LOGIN_PASSWORD", SqlDbType.NVarChar, 180)
            sp14.Value = Password
            Dim sp15 As New SqlParameter("@USERID", SqlDbType.NVarChar, 50)
            sp15.Value = Userid
            Dim sp16 As New SqlParameter("@AUR_RES_NUMBER", SqlDbType.NVarChar, 50)
            sp16.Value = Mobileno
            Dim sp17 As New SqlParameter("@AUR_DESGN_ID", SqlDbType.NVarChar, 50)
            sp17.Value = Designation
            Dim sp18 As New SqlParameter("@AUR_DEP_ID", SqlDbType.NVarChar, 50)
            sp18.Value = Department
            Dim sp19 As New SqlParameter("@AUR_COUNTRY", SqlDbType.NVarChar, 50)
            sp19.Value = Country
            Dim sp20 As New SqlParameter("AUR_DOJ", SqlDbType.SmallDateTime)
            sp20.Value = Doj
            Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12, sp13, sp14, sp15, sp16, sp17, sp18, sp19, sp20}
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_USER", parms)
            Return status
        Catch ex As Exception
            Return 1
        End Try
    End Function

    <WebMethod()> _
    Public Function AddRole(ByVal Role_Acronym As String, ByVal Role_Description As String, ByVal Role_Remarks As String)
        Try
            Dim status As Integer
            Dim sp1 As New SqlParameter("@ROL_ACRONYM", SqlDbType.NVarChar, 50)
            sp1.Value = Role_Acronym
            Dim sp2 As New SqlParameter("@ROL_DESCRIPTION", SqlDbType.NVarChar, 50)
            sp2.Value = Role_Description
            Dim sp3 As New SqlParameter("@ROL_REMARKS", SqlDbType.NVarChar, 50)
            sp3.Value = Role_Remarks
            Dim sp4 As New SqlParameter("@ROL_UPDATED_BY", SqlDbType.NVarChar, 50)
            sp4.Value = "10054_fs"
            Dim sp5 As New SqlParameter("@i_Status", SqlDbType.Int)
            sp5.Value = 1
            Dim sp6 As New SqlParameter("@i_Op", SqlDbType.Int)
            sp6.Value = ParameterDirection.Output
            Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6}
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_MODIFY_ROLE", parms)
            Return status
        Catch ex As Exception
            Return 1
        End Try
    End Function
    '<WebMethod()> _
    ' Public Function Allocatespace(ByVal Employee_id As String, ByVal Space_id As String)
    '    Try
    '        Dim status As Integer
    '        Dim sp1 As New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
    '        sp1.Value = Employee_id
    '        Dim sp2 As New SqlParameter("@SPCID", SqlDbType.NVarChar, 50)
    '        sp2.Value = Space_id
    '        Dim parms As SqlParameter() = {sp1, sp2}
    '        status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "ASSIGN_EMPLOYEE", parms)
    '        Return status
    '    Catch ex As Exception
    '        Return 1
    '    End Try
    'End Function

    <WebMethod()> _
    Public Function Occupiedspace(ByVal Employee_id As String, ByVal Space_id As String)
        Try
            Dim status As Integer
            Dim sp1 As New SqlParameter("@spcid", SqlDbType.NVarChar, 50)
            sp1.Value = Space_id
            Dim sp2 As New SqlParameter("@empid", SqlDbType.NVarChar, 50)
            sp2.Value = Employee_id
            Dim parms As SqlParameter() = {sp1, sp2}
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "OccupiedEmployee", parms)
            Return status
        Catch ex As Exception
            Return 1
        End Try
    End Function

    <WebMethod()> _
    Public Function Allocatespace(ByVal Employee_id As String, ByVal Space_id As String)
        Try
            Dim status As Integer
            Dim sp1 As New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
            sp1.Value = Employee_id
            Dim sp2 As New SqlParameter("@SPCID", SqlDbType.NVarChar, 50)
            sp2.Value = Space_id
            Dim parms As SqlParameter() = {sp1, sp2}
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "ASSIGN_EMPLOYEE", parms)
            Return status
        Catch ex As Exception
            Return 1
        End Try
    End Function

    <WebMethod()> _
    Public Function Allocatespace(ByVal Employee_id As String, ByVal Space_id As String, ByVal Fromdate As String, ByVal todate As String, ByVal fromtime As String, ByVal totime As String)
        Try
            Dim status As Integer
            Dim sp1 As New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
            sp1.Value = Employee_id
            Dim sp2 As New SqlParameter("@SPCID", SqlDbType.NVarChar, 50)
            sp2.Value = Space_id
            Dim sp3 As New SqlParameter("@FROM_DATE", SqlDbType.NVarChar, 50)
            sp3.Value = Fromdate
            Dim sp4 As New SqlParameter("@TO_DATE", SqlDbType.NVarChar, 50)
            sp4.Value = todate
            Dim sp5 As New SqlParameter("@FROMTIME", SqlDbType.NVarChar, 50)
            sp5.Value = fromtime
            Dim sp6 As New SqlParameter("@TOTIME", SqlDbType.NVarChar, 50)
            sp6.Value = totime
            Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6}
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "ASSIGN_EMPLOYEE_DATE_FROMMAP", parms)
            Return status
        Catch ex As Exception
            Return 1
        End Try
    End Function
    <WebMethod()> _
    Public Function Allocatespace(ByVal Employee_id As String, ByVal Space_id As String, ByVal SHIFTID As Integer)
        Try
            Dim status As Integer
            Dim sp1 As New SqlParameter("@EMPID", SqlDbType.NVarChar, 50)
            sp1.Value = Employee_id
            Dim sp2 As New SqlParameter("@SPCID", SqlDbType.NVarChar, 50)
            sp2.Value = Space_id
            Dim sp3 As New SqlParameter("@SHIFTID", SqlDbType.NVarChar, 50)
            sp3.Value = SHIFTID
            Dim parms As SqlParameter() = {sp1, sp2, sp3}
            status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "ASSIGN_EMPLOYEE_Shifts", parms)
            Return status
        Catch ex As Exception
            Return 1
        End Try
    End Function
    <WebMethod()> _
    Public Function SearchbyEmployeeid(ByVal Employee_id As String) As DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Employee_id
        Dim dsEmpDetails As New DataSet
        dsEmpDetails = ObjSubSonic.GetSubSonicDataSet("GETEMPLOYEEDETAILS_SPACE", param)
        Return dsEmpDetails
    End Function
    <WebMethod()> _
    Public Function SearchbySpaceid(ByVal Space_id As String) As DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@SPACE_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Space_id
        Dim dsspacedetails As New DataSet
        dsspacedetails = ObjSubSonic.GetSubSonicDataSet("GET_SPACE_STATUS", param)
        Return dsspacedetails
    End Function
End Class
