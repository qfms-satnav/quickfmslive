Imports Microsoft.VisualBasic

Public Class clsTower
    Private mTWR_NAME As String

    Public Property TWR_NAME() As String
        Get
            Return mTWR_NAME
        End Get
        Set(ByVal value As String)
            mTWR_NAME = value
        End Set
    End Property
    Private mTWR_CODE As String

    Public Property TWR_CODE() As String
        Get
            Return mTWR_CODE
        End Get
        Set(ByVal value As String)
            mTWR_CODE = value
        End Set
    End Property
End Class
