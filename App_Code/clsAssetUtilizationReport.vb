Imports Microsoft.VisualBasic

Public Class clsAssetUtilizationReport
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
    Private mSTAT As String

    Public Property STAT() As String
        Get
            Return mSTAT
        End Get
        Set(ByVal value As String)
            mSTAT = value
        End Set
    End Property
    Private mcnt As String

    Public Property cnt() As String
        Get
            Return mcnt
        End Get
        Set(ByVal value As String)
            mcnt = value
        End Set
    End Property

End Class
