Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace Amantra.Exception
    Public Class DataException
        Inherits BaseException
        Public Sub New(ByVal message As String, ByVal className As String, ByVal methodName As String, ByVal objException As System.Exception)
            MyBase.New(message, className, methodName, objException)
        End Sub

        Public Sub New(ByVal message As String, ByVal className As String, ByVal formName As String, ByVal methodName As String, ByVal objException As System.Exception)
            MyBase.New(message, className, formName, methodName, objException)
        End Sub

        Public Sub New(ByVal message As String, ByVal className As String, ByVal formName As String, ByVal methodName As String, ByVal UserId As String)
            MyBase.New(message, className, formName, methodName, UserId)
        End Sub
    End Class
End Namespace