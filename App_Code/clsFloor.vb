Imports Microsoft.VisualBasic

Public Class clsFloor
    Private mFLR_NAME As String

    Public Property FLR_NAME() As String
        Get
            Return mFLR_NAME
        End Get
        Set(ByVal value As String)
            mFLR_NAME = value
        End Set
    End Property
    Private mFLR_CODE As String

    Public Property FLR_CODE() As String
        Get
            Return mFLR_CODE
        End Get
        Set(ByVal value As String)
            mFLR_CODE = value
        End Set
    End Property
End Class
