Imports Microsoft.VisualBasic

Public Class clsAssetLocationForGrid
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
    Private mASTCOUNT As String

    Public Property ASTCOUNT() As String
        Get
            Return mASTCOUNT
        End Get
        Set(ByVal value As String)
            mASTCOUNT = value
        End Set
    End Property
    Private mPRODUCTID As String

    Public Property PRODUCTID() As String
        Get
            Return mPRODUCTID
        End Get
        Set(ByVal value As String)
            mPRODUCTID = value
        End Set
    End Property
End Class
