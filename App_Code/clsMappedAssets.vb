Imports Microsoft.VisualBasic

Public Class clsMappedAssets
    Private mCNT As String

    Public Property CNT() As String
        Get
            Return mCNT
        End Get
        Set(ByVal value As String)
            mCNT = value
        End Set
    End Property
    Private mLCM_NAME As String

    Public Property LCM_NAME() As String
        Get
            Return mLCM_NAME
        End Get
        Set(ByVal value As String)
            mLCM_NAME = value
        End Set
    End Property
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
End Class
