Imports Microsoft.VisualBasic

Public Class clsStockReport
    Private mLCM_NAME As String

    Public Property LCM_NAME() As String
        Get
            Return mLCM_NAME
        End Get
        Set(ByVal value As String)
            mLCM_NAME = value
        End Set
    End Property
    Private mTWR_NAME As String

    Public Property TWR_NAME() As String
        Get
            Return mTWR_NAME
        End Get
        Set(ByVal value As String)
            mTWR_NAME = value
        End Set
    End Property
    Private mFLR_NAME As String

    Public Property FLR_NAME() As String
        Get
            Return mFLR_NAME
        End Get
        Set(ByVal value As String)
            mFLR_NAME = value
        End Set
    End Property
    Private mAIM_NAME As String

    Public Property AIM_NAME() As String
        Get
            Return mAIM_NAME
        End Get
        Set(ByVal value As String)
            mAIM_NAME = value
        End Set
    End Property
    Private mAID_AVLBL_QTY As String

    Public Property AID_AVLBL_QTY() As String
        Get
            Return mAID_AVLBL_QTY
        End Get
        Set(ByVal value As String)
            mAID_AVLBL_QTY = value
        End Set
    End Property
    Private mREQ_QTY As String

    Public Property REQ_QTY() As String
        Get
            Return mREQ_QTY
        End Get
        Set(ByVal value As String)
            mREQ_QTY = value
        End Set
    End Property

End Class
