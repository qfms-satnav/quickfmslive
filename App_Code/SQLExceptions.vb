Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections.Generic
Imports System.Exception

Namespace Amantra.CustomExceptions
    Public Class SQLExceptions
        Inherits ApplicationException
        'Exception Message String 
        Private m_customMessage As String = ""

        'Exception 
        Private innerException As System.Exception



        ' Public accessor for the Custom Message 
        Public Property CustomMessage() As String
            Get
                Return Me.m_customMessage
            End Get
            Set(ByVal value As String)
                Me.m_customMessage = value
            End Set
        End Property

        ' Default Constructor 
        Public Sub New()
            ' 
            ' TODO: Add constructor logic here 
            ' 
        End Sub

        ' Constructor with parameters 
        Public Sub New(ByVal customMessage As String, ByVal innerException As System.Exception)
            MyBase.New(customMessage, innerException)
            Me.m_customMessage = customMessage
            Me.innerException = innerException
        End Sub

        Public Sub New(ByVal customMessage As String)
            MyBase.New(customMessage)
            Me.m_customMessage = customMessage
        End Sub
    End Class
End Namespace