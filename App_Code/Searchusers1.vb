﻿Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Searchusers1
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function SearchUser1(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSETS")
        sp.Command.AddParameter("@asset", prefixText, DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        Dim dt As DataTable = ds.Tables(0)
        Dim txtItems As New List(Of String)
        Dim dbValues As String
        For Each row As DataRow In dt.Rows
            ''String From DataBase(dbValues)
            dbValues = row("ASSET_CODE").ToString()
            dbValues = dbValues.ToLower()
            txtItems.Add(dbValues)
        Next
        Return txtItems.ToArray()
    End Function

End Class