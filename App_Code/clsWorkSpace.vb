Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports System.Web.UI.WebControls
Imports System
Imports System.Web.UI
Imports System.Configuration
Imports System.Web


Public Class clsWorkSpace
    Dim REQID As String
    Dim RIDDS As String


#Region " REQUEST ID GENARATION(RID)"
    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function
    Public Function REQGENARATION_REQ(ByVal Strfield As String, ByVal countfield As String, ByVal DataBase As String, ByVal Table As String, Optional ByVal Wherecondiction As String = "1=1", Optional ByVal field1 As String = "0", Optional ByVal field2 As String = "0") As String
        Dim TmpReqseqid As String
        Dim Reqseqid As Integer

        strSQL = "SELECT isnull(MAX(" & countfield & "),0)+1 FROM " & DataBase + Table & " where " & Wherecondiction
        Reqseqid = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)

        If Reqseqid < 10 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/000000" & Reqseqid
        ElseIf Reqseqid < 100 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/00000" & Reqseqid
        ElseIf Reqseqid < 1000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/0000" & Reqseqid
        ElseIf Reqseqid < 10000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/000" & Reqseqid
        ElseIf Reqseqid < 100000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/00" & Reqseqid
        ElseIf Reqseqid < 1000000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/0" & Reqseqid
        Else
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/" & Reqseqid
        End If
        Return TmpReqseqid
    End Function
#End Region


    Public Sub loadlocation(ByVal ddl As DropDownList)
        strSQL = "select lcm_code,lcm_name from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1 order by lcm_name"
        BindCombo(strSQL, ddl, "LCM_NAME", "LCM_CODE")
    End Sub

    Public Sub loadlocation_City(ByVal ddl As DropDownList, ByVal strCity As String)
        'strSQL = "select lcm_code,lcm_name from " & HttpContext.Current.Session("TENANT") & "."  & "location where lcm_sta_id=1 and lcm_cty_id='" & strCity & "' order by lcm_name"
        strSQL = "usp_getLocationsforCity '" & strCity & "'"
        BindCombo(strSQL, ddl, "LCM_NAME", "LCM_CODE")
    End Sub
    Public Sub loaddepartment(ByVal ddl As DropDownList)
        strSQL = "select dep_code,DEP_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "DEPARTMENT WHERE DEP_STA_ID=1 ORDER BY DEP_NAME"
        BindCombo(strSQL, ddl, "dep_name", "dep_code")
    End Sub
    Public Sub loadrm(ByVal ddlRM As DropDownList)
        strSQL = "SELECT AUR_REPORTING_TO,USR_NAME FROM " & _
                " " & HttpContext.Current.Session("TENANT") & "." & "AMT_AMANTRAUSER_VW," & HttpContext.Current.Session("TENANT") & "." & "[User] WHERE AUR_ID= '1' and usr_id=AUR_REPORTING_TO"
        BindCombo(strSQL, ddlRM, "usr_name", "aur_reporting_to")
    End Sub
    Public Sub loadgrid(ByVal gdavail As GridView, ByVal ddlSelectLocation As DropDownList)
        gdavail.Columns(4).Visible = True
        strSQL = "Select lcm_code,lcm_name,cwst=0,ccub=0,ccab=0 from " & HttpContext.Current.Session("TENANT") & "." & "location order by lcm_name"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
        gdavail.DataSource = ds
        gdavail.DataBind()
        For i As Integer = 0 To gdavail.Rows.Count - 1
            strSQL = "Select isnull(sum(svr_wst_req),0) cwst,isnull(sum(svr_cab_req),0) ccab,isnull(sum(svr_cub_req),0) ccub from " & HttpContext.Current.Session("TENANT") & "." & "sms_vertical_requisition where svr_loc_id='" & ddlSelectLocation.SelectedItem.Value & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            While ObjDR.Read()
                gdavail.Rows(i).Cells(1).Text = ObjDR("cwst")
                gdavail.Rows(i).Cells(2).Text = ObjDR("ccub")
                gdavail.Rows(i).Cells(3).Text = ObjDR("ccab")
            End While
            ObjDR.Close()
        Next
        For i As Integer = 0 To gdavail.Rows.Count - 1
            ' strSQL = "Select isnull(lcm_wstnum,0) cwst,isnull(lcm_fcnum,0) ccab,isnull(lcm_hcnum,0) ccub from  " & HttpContext.Current.Session("TENANT") & "."  & "LOCATION where lcm_code='" & gdavail.Rows(i).Cells(4).Text & "'"
            strSQL = "Select isnull(lcm_wstnum,0) cwst,isnull(lcm_fcnum,0) ccab,isnull(lcm_hcnum,0) ccub from  " & HttpContext.Current.Session("TENANT") & "." & "LOCATION where lcm_code='" & ddlSelectLocation.SelectedItem.Value & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            While ObjDR.Read()
                gdavail.Rows(i).Cells(1).Text = ObjDR("cwst") - CInt(gdavail.Rows(i).Cells(1).Text)
                gdavail.Rows(i).Cells(2).Text = ObjDR("ccub") - CInt(gdavail.Rows(i).Cells(2).Text)
                gdavail.Rows(i).Cells(3).Text = ObjDR("ccab") - CInt(gdavail.Rows(i).Cells(3).Text)
            End While
            ObjDR.Close()
        Next
        gdavail.Columns(4).Visible = False
    End Sub
    Public Sub insertdata(ByVal trLabel As Label, ByVal gvEnter As GridView, ByVal txtRemarks As TextBox, ByVal ddlRM As DropDownList, ByVal ddlSelectLocation As DropDownList, ByVal ddlVertical As DropDownList, ByVal page As Page)
        Dim userid As String = page.Session("uid")
        Dim intwst, intcab, intcub As Integer
        Dim i As Integer
        Dim txtworkst, txtcubicalsReq, txtCabinsReq As TextBox
        Dim cmbmon, cmbyear As DropDownList
        trLabel.Visible = True
        For i = 0 To gvEnter.Rows.Count - 1
            Dim intw, inth, intf As Integer
            txtworkst = CType(gvEnter.Rows(i).Cells(2).FindControl("txtWorkstations"), TextBox)
            txtcubicalsReq = CType(gvEnter.Rows(i).Cells(3).FindControl("txtHalfcabinsrequired"), TextBox)
            txtCabinsReq = CType(gvEnter.Rows(i).Cells(4).FindControl("txtFullCabinsRequired"), TextBox)
            cmbmon = CType(gvEnter.Rows(i).Cells(0).FindControl("ddlMonth"), DropDownList)
            cmbyear = CType(gvEnter.Rows(i).Cells(1).FindControl("ddlYear"), DropDownList)
            intw = Val(txtworkst.Text)
            inth = Val(txtcubicalsReq.Text)
            intf = Val(txtCabinsReq.Text)
            intwst = intwst + Val(txtworkst.Text)
            intcab = intcab + Val(txtCabinsReq.Text)
            intcub = intcub + Val(txtcubicalsReq.Text)
            If Val(txtCabinsReq.Text) = 0 And Val(txtcubicalsReq.Text) = 0 And Val(txtworkst.Text) = 0 Then
                PopUpMessage("Please enter quantities required", page)
                Exit Sub
            ElseIf txtRemarks.Text.Length > 500 Then
                PopUpMessage("Please enter remarks in less than or equal to 500 characters", page)
            End If
            Dim sta As Integer = 5
            RIDDS = RIDGENARATION("VerticalReq")
            REQID = REQGENARATION_REQ("VerticalReq", "SVR_ID", HttpContext.Current.Session("TENANT") & ".", "SMS_VERTICAL_REQUISITION")
            strSQL = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & "SMS_VERTICAL_REQUISITIONDTLS(SVD_REQID, SVD_WSTNO,SVD_HCNO,SVD_FCNO,SVD_MON,SVD_YEAR) VALUES ('" & REQID & "'," & intw & "," & inth & "," & intf & ",'" & cmbmon.SelectedItem.Text & "','" & cmbyear.SelectedItem.Text & "')"
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            strSQL1 = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & "SMS_VERTICAL_REQUISITION(SVR_REQ_ID,SVR_REQ_DS,SVR_REQ_DATE,SVR_STA_ID,SVR_REQ_BY,SVR_WST_REQ,SVR_CAB_REQ,SVR_CUB_REQ,SVR_REM,SVR_REP_MGR,SVR_DEP_ID,SVR_LOC_ID) VALUES ('" & REQID & "','" & RIDDS & "','" & getoffsetdatetime(DateTime.Now) & "'," & sta & ",'" & userid & "'," & intwst & "," & intcab & "," & intcub & ",'" & Replace(Trim(txtRemarks.Text), "'", "''") & "','" & ddlRM.SelectedItem.Value & "','" & ddlVertical.SelectedItem.Value & "','" & ddlSelectLocation.SelectedItem.Value & "')"
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL1)
            'Response.Redirect("finalpage.aspx?sta=" & sta & "&rid=" & REQID)
        Next
    End Sub
    Public Sub lnkmore(ByVal gvEnter As GridView)
        Dim i As Integer
        Dim dt As New DataTable
        dt.Columns.Add("sno")
        For i = 0 To gvEnter.Rows.Count
            Dim dr As DataRow = dt.NewRow
            dr(0) = i + 1
            dt.Rows.Add(dr)
        Next
        gvEnter.DataSource = dt
        gvEnter.DataBind()
        gvEnter.Visible = True
    End Sub


    '**************************************  SPACE REQUISITION EVENTS *****************************************
    Public Sub loadvertical(ByVal ddl As DropDownList)
        'strSQL = "select distinct ver_code,ver_name from " & HttpContext.Current.Session("TENANT") & "."  & "vertical where ver_sta_id=1 order by ver_name"
        strSQL = "usp_getVerticalDetails"
        BindCombo(strSQL, ddl, "ver_name", "ver_code")
    End Sub
    Public Sub loadtower1(ByVal ddl As DropDownList, ByVal strCityCode As String)
        strSQL = " select distinct twr_code,(twr_name +'/'+((select LCM_NAME from location where lcm_code=twr_bdg_id))) as twr_name from " & HttpContext.Current.Session("TENANT") & "." & "tower where TWR_STA_ID=1 and TWR_CTY_ID = '" & strCityCode & "'  order by twr_name"
        BindCombo(strSQL, ddl, "twr_name", "twr_code")
    End Sub


    Public Sub loadtower1(ByVal ddl As DropDownList, ByVal strCityCode As String, ByVal strLoc As String)
        'strSQL = " select distinct twr_code,(twr_name +'/'+((select LCM_NAME from location where lcm_code=twr_bdg_id))) as twr_name from " & HttpContext.Current.Session("TENANT") & "."  & "tower where TWR_STA_ID=1 and TWR_CTY_ID = '" & strCityCode & "' and twr_bdg_id='" & strLoc & "' order by twr_name"

        Dim sp1 As New SqlParameter("@vc_ctyCode", SqlDbType.NVarChar, 50)
        Dim sp2 As New SqlParameter("@vc_bdg_Code", SqlDbType.NVarChar, 50)
        sp1.Value = strCityCode
        sp2.Value = strLoc
        strSQL = "usp_getTowerforBuilding"
        BindCombo(strSQL, ddl, "twr_name", "twr_code", sp1, sp2)
    End Sub


    Public Sub loadtower2(ByVal ddl As DropDownList)
        strSQL = " select distinct twr_code,twr_name from " & HttpContext.Current.Session("TENANT") & "." & "tower where TWR_STA_ID=1 order by twr_name"
        BindCombo(strSQL, ddl, "twr_name", "twr_code")
    End Sub

    Public Function tower1_selectedindexchanged(ByVal ddl As DropDownList, ByVal txtcny As TextBox, ByVal txtcty As TextBox, ByVal txtloc As TextBox) As String
        Try
            Dim strLocCode As String = String.Empty
            'strSQL = "select (select cny_name from " & HttpContext.Current.Session("TENANT") & "."  & "country where cny_code=twr_cny_id) twr_cny_id," & _
            '        "(select cty_name from " & HttpContext.Current.Session("TENANT") & "."  & "city where cty_code=twr_cty_id) twr_cty_id," & _
            '        "(select lcm_name from " & HttpContext.Current.Session("TENANT") & "."  & "location where lcm_code=twr_loc_id) twr_loc_id,twr_loc_id as 'twr_loc_code' from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code='" & ddl.SelectedItem.Value & "'"
            Dim sp1 As New SqlParameter("@vc_Code", SqlDbType.NVarChar, 50)
            sp1.Value = ddl.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getTowerDetailsforTowerCode", sp1)
            'ObjDR = DataReader(strSQL)
            Do While ObjDR.Read
                txtcny.Text = ObjDR("twr_cny_id")
                txtcty.Text = ObjDR("twr_cty_id")
                txtloc.Text = ObjDR("twr_loc_id")
                strLocCode = ObjDR("twr_loc_code").ToString().Trim()
            Loop
            Return strLocCode
        Catch ex As Exception
            Return String.Empty
        Finally
            'ObjDR.Close()
        End Try

    End Function

    Public Sub tower2_selectedindexchanged(ByVal ddl As DropDownList, ByVal txtcny As TextBox, ByVal txtcty As TextBox, ByVal txtloc As TextBox)
        Try

            strSQL = "select (select cny_name from " & HttpContext.Current.Session("TENANT") & "." & "country where cny_code=twr_cny_id) twr_cny_id," & _
                    "(select cty_name from " & HttpContext.Current.Session("TENANT") & "." & "city where cty_code=twr_cty_id) twr_cty_id," & _
                    "(select lcm_name from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_code=twr_loc_id) twr_loc_id from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_code='" & ddl.SelectedItem.Value & "'"
            ObjDR = DataReader(strSQL)
            Do While ObjDR.Read
                txtcny.Text = ObjDR("twr_cny_id")
                txtcty.Text = ObjDR("twr_cty_id")
                txtloc.Text = ObjDR("twr_loc_id")
            Loop
        Catch ex As Exception
        Finally
            ObjDR.Close()
        End Try

    End Sub

    Public Function INSERTSPACEREQUISITION(ByVal ddldept As DropDownList, ByVal cubical As TextBox, ByVal cabins As TextBox, ByVal workstations As TextBox, ByVal proj As TextBox, ByVal lbl As Label, ByVal page As Page, ByVal txtfromdate As TextBox, ByVal txttodate As TextBox, ByVal ddltower1 As DropDownList, ByVal txtrem As TextBox, ByVal ddlrm As DropDownList, ByVal ddlvertical As DropDownList) As String
        Dim sta As Integer = 5
        Dim userid As String = page.Session("uid")
        Dim REQID As String = getNumber(page)
        If txtfromdate.Text = String.Empty Or txttodate.Text = String.Empty Or ddlrm.SelectedItem.Text = String.Empty Or txtrem.Text = String.Empty Then
            PopUpMessage(" Please Enter the Mandatory Fields", page)
        Else
            strSQL = "insert into " & HttpContext.Current.Session("TENANT") & "." & "sms_spacerequisition(srn_req_id,srn_req_dt,srn_frm_dt,srn_to_dt,srn_sta_id,srn_aur_id,srn_bdg_one," & _
            "srn_rem,srn_rm_id,srn_branch_id,srn_proj_id,srn_dep_id) values ('" & REQID & "','" & getoffsetdatetime(DateTime.Now) & "','" & txtfromdate.Text & "','" & txttodate.Text & "'," & _
            "'" & sta & "','" & userid & "','" & ddltower1.SelectedItem.Value & "','" & txtrem.Text & "','" & ddlrm.SelectedItem.Value & "'," & _
            "'" & ddlvertical.SelectedItem.Value & "','" & proj.Text & "','" & ddldept.SelectedItem.Value & "')"
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            If cubical.Text = String.Empty Then
                cubical.Text = 0
            End If
            strSQL = "insert into " & HttpContext.Current.Session("TENANT") & "." & "sms_spacerequisition_details(srd_srnreq_id,srd_type,srd_qty) values ('" & REQID & "','Half Cabin','" & cubical.Text & "')"
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            If cabins.Text = String.Empty Then
                cabins.Text = 0
            End If
            strSQL = "insert into " & HttpContext.Current.Session("TENANT") & "." & "sms_spacerequisition_details(srd_srnreq_id,srd_type,srd_qty) values ('" & REQID & "','Full Cabin','" & cabins.Text & "')"
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            If workstations.Text = String.Empty Then
                workstations.Text = 0
            End If
            strSQL = "insert into " & HttpContext.Current.Session("TENANT") & "." & "sms_spacerequisition_details(srd_srnreq_id,srd_type,srd_qty) values ('" & REQID & "','Work Station','" & workstations.Text & "')"
            SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
            PopUpMessage(" Record inserted Successfully ", page)
            'Dim to_mail As String
            'Dim cc_mail As String
            'Dim body As String
            'to_mail = "venugopal@satnavtech.com"
            'cc_mail = "phanindra_vakalapudi@satnavtech.com "
            'body = "Dear <b> USER </b> <br /> <br />   The request has been submitted successfully. The details are as follows <br /> <br /> Space Requisition Id : <b> " & REQID & "</b><br />Cubicals(Half Cabins) :<b> " & cubical.Text & " </b><br />Full Cabins : <b> " & cabins.Text & " </b> <br />Work Stations : <b> " & workstations.Text & " </b> <br /> From Date :<b> " & txtfromdate.Text & " </b><br /> To Date : <b> " & txttodate.Text & "</b><br /> <br /> <br /><br /> Thanks & Regards <br /><b>myAmantraAxis Team</b>"
            'Mail_to_user(body, to_mail, cc_mail)
        End If
        Return REQID
    End Function

    Public Function INSERTSPACEREQUISITION_Values(ByVal ddldept As String, ByVal cubical As Integer, ByVal cabins As Integer, ByVal workstations As Integer, ByVal proj As String, ByVal page As Page, ByVal txtfromdate As Date, ByVal txttodate As Date, ByVal ddltower1 As String, ByVal txtrem As String, ByVal ddlvertical As String, ByVal strBdg As String, ByVal ddlCost As String) As String
        Dim sta As Integer = 5
        Dim userid As String = page.Session("uid")
        Dim REQID As String = getNumber(page)
        Dim sp1 As New SqlParameter("@vc_reqid", SqlDbType.NVarChar, 50)
        sp1.Value = REQID
        Dim sp2 As New SqlParameter("@vc_fromdate", SqlDbType.DateTime)
        sp2.Value = txtfromdate
        Dim sp3 As New SqlParameter("@vc_todate", SqlDbType.DateTime)
        sp3.Value = txttodate
        Dim sp4 As New SqlParameter("@vc_status", SqlDbType.NVarChar, 10)
        sp4.Value = sta
        Dim sp5 As New SqlParameter("@vc_user", SqlDbType.NVarChar, 50)
        sp5.Value = userid
        Dim sp6 As New SqlParameter("@vc_tower", SqlDbType.NVarChar, 50)
        sp6.Value = ddltower1
        Dim sp7 As New SqlParameter("@vc_remarks", SqlDbType.NVarChar, 250)
        sp7.Value = txtrem
        Dim sp8 As New SqlParameter("@vc_vertical", SqlDbType.NVarChar, 50)
        sp8.Value = ddlvertical
        Dim sp9 As New SqlParameter("@vc_proj", SqlDbType.NVarChar, 50)
        sp9.Value = proj
        Dim sp10 As New SqlParameter("@vc_dept", SqlDbType.NVarChar, 50)
        sp10.Value = ddldept
        Dim sp11 As New SqlParameter("@vc_bdg", SqlDbType.NVarChar, 50)
        sp11.Value = strBdg
        Dim sp12 As New SqlParameter("@cubicals", SqlDbType.Int)
        sp12.Value = cubical
        Dim sp13 As New SqlParameter("@cabins", SqlDbType.Int)
        sp13.Value = cabins
        Dim sp14 As New SqlParameter("@WorkStations", SqlDbType.Int)
        sp14.Value = workstations
        Dim sp15 As New SqlParameter("@COSTCENTER", SqlDbType.NVarChar, 150)
        sp15.Value = ddlCost
        Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12, sp13, sp14, sp15}
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertSpaceDetails", parms)
        Return REQID
    End Function
    Function getNumber(ByVal page As Page) As String
        Dim iBKgType, strTmpReqSeqId As String
        strTmpReqSeqId = ""
        strSQL = "SELECT MAX(ISNULL(SRN_SNO,0))+1 SNO FROM " & HttpContext.Current.Session("TENANT") & "." & " SMS_SPACEREQUISITION"
        Try
            iBKgType = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)
        Catch ex As Exception
            iBKgType = 1
        End Try

        If iBKgType < 10 Then
            strTmpReqSeqId = page.Session("uid") & "/SPCREQ/000000" & iBKgType
        ElseIf iBKgType < 100 Then
            strTmpReqSeqId = page.Session("uid") & "/SPCREQ/00000" & iBKgType
        ElseIf iBKgType < 1000 Then
            strTmpReqSeqId = page.Session("uid") & "/SPCREQ/0000" & iBKgType
        ElseIf iBKgType < 10000 Then
            strTmpReqSeqId = page.Session("uid") & "/SPCREQ/000" & iBKgType
        ElseIf iBKgType < 100000 Then
            strTmpReqSeqId = page.Session("uid") & "/SPCREQ/00" & iBKgType
        ElseIf iBKgType < 1000000 Then
            strTmpReqSeqId = page.Session("uid") & "/SPCREQ/0" & iBKgType
        ElseIf iBKgType < 10000000 Then
            strTmpReqSeqId = page.Session("uid") & "/SPCREQ/" & iBKgType
        End If
        Return strTmpReqSeqId
    End Function

    Public Sub PopUpMessage(ByVal strText As String, ByVal Page As Page)
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ajaxscript", "alert('" & strText & "');", True)
        Return
    End Sub
    Sub Mail_to_user(ByVal msg As String, ByVal mail_to As String, ByVal cc_mail As String)
        Dim mail As New MailMessage
        'mail.From = New MailAddress(ConfigurationManager.AppSettings("AmantraEmailId").ToString)
        mail.From = New MailAddress(ConfigurationManager.AppSettings("AmantraEmailId").ToString)
        mail.To.Add(mail_to)
        mail.CC.Add(cc_mail)
        mail.Subject = "Requisition Details"
        mail.IsBodyHtml = True
        mail.Body = msg
        'Create the SMTP Client
        Dim client As New SmtpClient()
        client.Host = "mail.satnavtech.com"
        Try
            client.Send(mail)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub loadverdepartment(ByVal ddl As DropDownList, ByVal ddlvertical As DropDownList)
        'strSQL = "select distinct dep_code,dep_name from " & HttpContext.Current.Session("TENANT") & "."  & "department where dep_vertical='" & ddlvertical.SelectedItem.Value & "' "
        strSQL = "usp_getDeptsforVertical '" & ddlvertical.SelectedItem.Value & "'"
        BindCombo(strSQL, ddl, "dep_name", "dep_code")
    End Sub
    Public Sub loadProjects(ByVal ddl As DropDownList, ByVal strVertical As String)
        'strSQL = "SELECT PRJ_CODE, PRJ_NAME FROM  " & HttpContext.Current.Session("TENANT") & "."  & " PROJECT where PRJ_vertical='" & strVertical & "' "
        strSQL = "usp_getProjectsforVertical '" & strVertical & "'"
        BindCombo(strSQL, ddl, "PRJ_NAME", "PRJ_CODE")
    End Sub
    '*************************************** RM Approval Events ******************************

    Public Sub loadreqids(ByVal ddl As DropDownList)
        strSQL = "select distinct srn_req_id from " & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACEREQUISITION where srn_sta_id ='5'"
        BindCombo(strSQL, ddl, "srn_req_id", "srn_req_id")
    End Sub

    '******************** View map events ********************************
    Public Sub BindTower(ByVal ddl As DropDownList, ByVal loc As String)
        strSQL = "select distinct twr_code,twr_name from " & HttpContext.Current.Session("TENANT") & "." & "TOWER where twr_loc_id='" & loc & "' order by twr_name"
        BindCombo(strSQL, ddl, "twr_name", "twr_code")
    End Sub
    Public Sub BindFloor(ByVal ddl As DropDownList, ByVal twr As String)
        strSQL = "select distinct flr_code,flr_name from " & HttpContext.Current.Session("TENANT") & "." & "FLOOR where flr_twr_id='" & twr & "' order by flr_code"
        BindCombo(strSQL, ddl, "flr_name", "flr_code")
    End Sub




End Class
