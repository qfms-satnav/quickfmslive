Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System

Namespace Amantra.SmartReader
    ''' <summary> 
    ''' Summary description for SmartDataReader. 
    ''' </summary> 
    Public NotInheritable Class SmartDataReader
        Private defaultDate As Date
        Public Sub New(ByVal reader As SqlDataReader)
            Me.defaultDate = Date.MinValue
            Me.reader = reader
        End Sub

        Public Function GetInt64(ByVal column As String) As Long
            Dim data As Long = IIf((reader.IsDBNull(reader.GetOrdinal(column))), CLng(0), CLng(reader(column)))
            Return data
        End Function

        Public Function GetInt32(ByVal column As String) As Integer
            Dim data As Integer = IIf((reader.IsDBNull(reader.GetOrdinal(column))), CInt(0), CInt(reader(column)))
            Return data
        End Function


        Public Function GetInt16(ByVal column As String) As Short
            Dim data As Short = IIf((reader.IsDBNull(reader.GetOrdinal(column))), CShort(0), CShort(reader(column)))
            Return data
        End Function
        Public Function GetTinyInt(ByVal column As String) As Integer

            'int data =(reader.IsDBNull(reader.GetOrdinal(column))) ? 0 : (int)reader[column]; 
            Dim data As Integer

            If Not (reader(column).ToString() Is Nothing) Then

                If Integer.Parse(reader(column).ToString()) = 1 Then
                    data = 1
                Else
                    data = 0

                End If
            Else
                data = 0
            End If


            Return data
        End Function
        Public Function GetFloat(ByVal column As String) As Single
            Dim data As Single = IIf((reader.IsDBNull(reader.GetOrdinal(column))), 0, Single.Parse(reader(column).ToString()))
            Return data
        End Function

        '''
        ''' Since float has the limitation of 7 digit parsing ,Application need more than 7 digit 
        ''' parsing for which 
        ''' the below getdecimal is added 


        Public Function GetDecimal(ByVal column As String) As Decimal
            Dim data As Decimal = IIf((reader.IsDBNull(reader.GetOrdinal(column))), 0, Decimal.Parse(reader(column).ToString()))
            Return data
        End Function

        Public Function GetBoolean(ByVal column As String) As Boolean
            Dim data As Boolean = IIf((reader.IsDBNull(reader.GetOrdinal(column))), False, CBool(reader(column)))
            Return data
        End Function

        Public Function GetString(ByVal column As String) As String
            Dim data As String = IIf((reader.IsDBNull(reader.GetOrdinal(column))), Nothing, reader(column).ToString())
            Return data
        End Function

        Public Function GetDateTime(ByVal column As String) As DateTime
            Dim data As DateTime = IIf((reader.IsDBNull(reader.GetOrdinal(column))), defaultDate, DirectCast(reader(column), DateTime))
            Return data
        End Function

        Public Function GetBinary(ByVal column As String) As Byte()
            Dim data As Byte() = IIf((reader.IsDBNull(reader.GetOrdinal(column))), Nothing, CByte(reader(column)))
            Return data
        End Function

        Public Function Read() As Boolean
            Return Me.reader.Read()
        End Function
        Private reader As SqlDataReader
    End Class
End Namespace