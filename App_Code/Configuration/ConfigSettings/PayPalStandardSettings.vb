#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

''' <summary>
''' Summary description for PayPalStandardSettings
''' </summary>
Public Class PayPalStandardSettings
	Inherits ConfigurationSection
	<ConfigurationProperty("isActive", DefaultValue := False)> _
	Public Property IsActive() As Boolean
		Get
			Return CBool(MyBase.Item("isActive"))
		End Get
		Set
			MyBase.Item("isActive") = Value
		End Set
	End Property
	<ConfigurationProperty("useSandbox", DefaultValue := True)> _
	Public Property UseSandbox() As Boolean
		Get
			Return CBool(MyBase.Item("useSandbox"))
		End Get
		Set
			MyBase.Item("useSandbox") = Value
		End Set
	End Property
	<StringValidator(MinLength := 5), ConfigurationProperty("businessEmail", DefaultValue := "me@business.com")> _
	Public Property BusinessEmail() As String
		Get
			Return CStr(MyBase.Item("businessEmail"))
		End Get
		Set
			MyBase.Item("businessEmail") = Value
		End Set
	End Property
	<ConfigurationProperty("PDTID", DefaultValue := "")> _
	Public Property PDTID() As String
		Get
			Return CStr(MyBase.Item("PDTID"))
		End Get
		Set
			MyBase.Item("PDTID") = Value
		End Set
	End Property

End Class
