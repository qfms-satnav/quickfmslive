﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ConsumeSpareController : ApiController

{
    ConsumeSpareService services = new ConsumeSpareService();


    [HttpPost]

    public HttpResponseMessage GetAssetDetails(CustomizableRptVM CustRpt)
    {
        var obj = services.GetAssetDetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetTicketDetails(CustomizableRptVM CustRpt)
    {
        var obj = services.GetTicketDetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]

    public HttpResponseMessage GetDescrption(CustomizableRptVM CustRpt)
    {
        var obj = services.GetDescrption(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetCode(CustomizableRptVM CustRpt)
    {
        var obj = services.GetCode(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetCost(CustomizableRptVM CustRpt)
    {
        var obj = services.GetCost(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetQuantity(CustomizableRptVM CustRpt)
    {
        var obj = services.GetQuantity(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(CustomizableRptVM CustRpt)
    {
        var obj = services.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}