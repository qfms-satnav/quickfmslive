﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for Validatesparesrvices
/// </summary>
public class ConsumeSpareService
{
    SubSonic.StoredProcedure sp;

    public object GetAssetDetails(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS_LCM");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetTicketDetails(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ATTACHTICKETSLISTS");
            sp.Command.AddParameter("@LCM_CODE", CustRpt.LCM_CODE, DbType.String);
            sp.Command.AddParameter("@SUBC_CODE", CustRpt.AST_SUBCAT_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetDescrption(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ITEMDESCRPTION");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_AAT_CODE", CustRpt.AAS_AAT_CODE, DbType.String);
            sp.Command.AddParameter("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
            sp.Command.AddParameter("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetCode(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ITEMCODE");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetCost(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ITEMCOST");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetQuantity(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ITEMAVALIBLE");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object SaveData(CustomizableRptVM CustRpt)
    {
        try
        {
            //DataSet ds = new DataSet();
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INS_UPD_DET_ADDSPARES");
            //sp.Command.AddParameter("@ASP_REQ_LOC", CustRpt.ASP_REQ_LOC, DbType.String);
            //sp.Command.AddParameter("@ASP_REMARKS", CustRpt.ASP_REMARKS, DbType.String);
            //sp.Command.AddParameter("@ASP_AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            //sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            //sp.Command.AddParameter("@FILENAME", CustRpt.FILENAME, DbType.String);
            //sp.Command.AddParameter("@ASP_MAINT_ID", CustRpt.ASP_MAINT_ID, DbType.String);
            //sp.Command.AddParameter("@COST_OF_INVENTORY", CustRpt.COST_OF_INVENTORY, DbType.String);
            //sp.Command.AddParameter("@TYPE", "Consume", DbType.String);
            //sp.Command.AddParameter("@SPAREPARTDETAILS", UtilityService.ConvertToDataTable(CustRpt.SPAREPARTDETAILS), DbType.String);
            //ds=sp.GetDataSet();



            /////////////////////////////////////////////
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@ASP_REQ_LOC", SqlDbType.VarChar);
            param[0].Value = CustRpt.ASP_REQ_LOC;
            param[1] = new SqlParameter("@ASP_REMARKS", SqlDbType.VarChar);
            param[1].Value = CustRpt.ASP_REMARKS;
            param[2] = new SqlParameter("@ASP_AUR_ID", SqlDbType.VarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@ASP_MAINT_ID", SqlDbType.VarChar);
            param[4].Value = CustRpt.ASP_MAINT_ID;
            param[5] = new SqlParameter("@COST_OF_INVENTORY", SqlDbType.VarChar);
            param[5].Value = CustRpt.COST_OF_INVENTORY;
            param[6] = new SqlParameter("@TYPE", SqlDbType.VarChar);
            param[6].Value = "Consume";
            param[7] = new SqlParameter("@SPAREPARTDETAILS", SqlDbType.Structured);
            param[7].Value = UtilityService.ConvertToDataTable(CustRpt.SPAREPARTDETAILS);
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "INS_UPD_DET_ADDSPARES", param);
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }


}