﻿using QuickFMS.API.Filters;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

public class Asset_ReconciliationController : ApiController
{

    Asset_ReconciliationService asset_Reconciliation = new Asset_ReconciliationService();


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetBarcodeAsset(AstBarCode astBar)
    {
        var obj = asset_Reconciliation.GetBarcodeAsset(astBar);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UploadAsset_Rec()
    {
        var httpFile = HttpContext.Current.Request;
        var obj= asset_Reconciliation.ReadExcel_Data(httpFile);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
