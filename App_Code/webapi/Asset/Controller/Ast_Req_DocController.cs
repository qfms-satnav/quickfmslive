﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class AssetReportController : ApiController
{

    AssetService ECRS = new AssetService();
    [GzipCompression]
    
    [HttpPost]
    public HttpResponseMessage GetData(AssetReport data)
    {
        var obj = ECRS.GetData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}