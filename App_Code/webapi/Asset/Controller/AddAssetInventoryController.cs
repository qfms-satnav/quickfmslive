﻿using QuickFMS.API.Filters;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using System;

public class AddAssetInventoryController : ApiController
{

    public string FilePath;
    AddAssetInventoryService AAIS = new AddAssetInventoryService();

    [HttpGet]
    public HttpResponseMessage AssetMainCategory()
    {
        IEnumerable<AstMainCategory> AssetMainCategoryList = AAIS.AssetMainCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetMainCategoryList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetSubCategory()
    {
        IEnumerable<AstSubCategory> AssetSUbCategoryList = AAIS.AssetSubCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetSUbCategoryList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetBrand()
    {
        IEnumerable<AstBrand> AssetBrandList = AAIS.AssetBrand();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetBrandList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetModel()
    {
        IEnumerable<AstModel> AssetModelList = AAIS.AssetModel();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetModelList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetCostcenter()
    {
        IEnumerable<Costcenter> CCList = AAIS.GetCostcenter();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CCList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetLOCATION()
    {
        IEnumerable<AstlOCATION> AssetLOCList = AAIS.AssetLOCATION();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetLOCList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage Assettower()
    {
        IEnumerable<Astltower> AssettowList = AAIS.Assettower();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssettowList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage Assetfloor()
    {
        IEnumerable<AstlFloor> AssetFloorList = AAIS.Assetfloor();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetFloorList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetVendor()
    {
        IEnumerable<AstVendor> AssetVendorList = AAIS.AssetVendor();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetVendorList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage Assettype()
    {
        IEnumerable<Asttype> AssettypeList = AAIS.Assettype();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssettypeList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetCompany()
    {
        IEnumerable<Astcompany> AssetcompanyList = AAIS.GetCompany();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetcompanyList);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetData([FromBody] ASTDETAILS data)
    {
        var obj = AAIS.GetData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetDetailsById([FromBody] ASTDETAILS data)
    {
        var obj = AAIS.GetDetailsById(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetAssetLable([FromBody] ASTDETAILS data)
    {
        var obj = AAIS.GetAssetLable(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveRecord([FromBody] ASTDETAILS data)
    {
        var obj = AAIS.Submit(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ModifyAssetDetails([FromBody] ASTDETAILS data)
    {
        var obj = AAIS.ModifyAssetDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;
        string sPath = "";
        string DB = HttpContext.Current.Session["TENANT"].ToString();

        var httpRequest = HttpContext.Current.Request;
        //var validateFile = FileValidator.ValidateUploadedFile(httpRequest.Files[0], ".xlsx,.xls,.pdf", 11000);
        //sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/");
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + DB + "/ASSET_IMG/" + "/");

        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {

            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                //hpf.SaveAs(sPath + Path.GetFileName( hpf.FileName.Replace(" ","")));
                hpf.SaveAs(sPath + System.IO.Path.GetFileName(hfc.Keys[iCnt].ToString()));
                iUploadedCnt = iUploadedCnt + 1;
            }

        }
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }


    [HttpPost]
    public HttpResponseMessage UploadExcel()
    {
        var httpRequest = HttpContext.Current.Request;

        //var validateFile = FileValidator.ValidateUploadedFile(httpRequest.Files[0], ".xlsx,.xls", 11000);

        //if (validateFile.IsValid)
        //{
        var obj = AAIS.UploadExcel(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    //public HttpResponseMessage Bulkinventoryexcelupload()
    //{
    //    var httpFile = HttpContext.Current.Request;
    //    var obj = AAIS.ReadExcel_Data(httpFile);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
}
