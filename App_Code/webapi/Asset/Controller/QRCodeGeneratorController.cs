﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;


public class QRCodeGeneratorController:ApiController
{
    QRCodeGeneratorService QRCG = new QRCodeGeneratorService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetGriddata(QRCodeGeneratorModel dat)
    {
        var obj = QRCG.GetQRCodeDetails(dat);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage AssetName(AstBarCodedata astBar)
    {
        IEnumerable<assetname> AssetName = QRCG.AssetName(astBar);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetName);
        return response;
    }
}