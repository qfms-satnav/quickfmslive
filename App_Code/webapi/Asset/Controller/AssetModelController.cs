﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Http;

public class AssetModelController : ApiController
{
    AssetModelService AMS = new AssetModelService();

    [HttpGet]
    public HttpResponseMessage AssetMainCategoryModel()
    {
        IEnumerable<assetmainCategory> AssetMainCategoryList = AMS.AssetMainCategoryModel();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetMainCategoryList);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage AssetSubCategoryModel()
    {
        IEnumerable<assetSubCategorybycat> AssetSUbCategoryList = AMS.AssetSubCategoryModel();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetSUbCategoryList);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage AssetBrandModel()
    {
        IEnumerable<AstBrandbysubcat> AssetBrandList = AMS.AssetBrandModel();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetBrandList);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveAssetModel([FromBody] AssetModelmodel data)
    {
        var obj = AMS.SaveAssetModel(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    //[InputValidation]
    public HttpResponseMessage ModifyAssetModel(AssetModelmodel updatedata)
    {
        var obj = AMS.ModifyAssetModel(updatedata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpGet]
    // [Route("GetGridData")]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<AssetModelmodel> sev = AMS.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, sev);
        return response;
    }
}
