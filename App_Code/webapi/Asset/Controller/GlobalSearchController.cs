﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

/// <summary>
/// Summary description for GlobalSearchController
/// </summary>
public class GlobalSearchController : ApiController
{
    GlobalSearchService GSS = new GlobalSearchService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(GlobalSearchModel Loc)
    {
        var obj = GSS.GetData(Loc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}