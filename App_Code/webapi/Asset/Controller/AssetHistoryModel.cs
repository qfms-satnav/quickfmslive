﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssetHistoryModel
/// </summary>
public class AssetHistoryModel
{
    
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string STA_ID { get; set; }
    public string STA_TITLE { get; set; }

    public string STlist { get; set; }

    //public List<Statuslist> STlist1 { get; set; }
}

//public class Statuslist
//{
//    public string STA_ID { get; set; }
//    public string STA_TITLE { get; set; }
//    public bool ticked { get; set; }
//}

