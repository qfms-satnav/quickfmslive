﻿using QuickFMS.API.Filters;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

public class AssetReconsilationReportController : ApiController
{
    AssetReconsilationReportService ast_rpt = new AssetReconsilationReportService();


    [HttpPost]
    public HttpResponseMessage GetBarcodeAsset(AstBarCodedata astBar)
    {
        var obj = ast_rpt.GetBarcodeAsset(astBar);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage AssetNameRecon(AstBarCodedata astBar)
    {
        IEnumerable<AssetNameRecon> AssetName = ast_rpt.AssetNameRecon(astBar);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetName);
        return response;
    }

}
