﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Http;

public class AssetBrandController : ApiController
{

    AssetBrandService ABS = new AssetBrandService();

    [HttpGet]
    public HttpResponseMessage AssetMainCategory()
    {
        IEnumerable<assetCategory> AssetMainCategoryList = ABS.AssetMainCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetMainCategoryList);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage AssetSubCategory()
    {
        IEnumerable<assetSubCategory> AssetSUbCategoryList = ABS.AssetSubCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetSUbCategoryList);
        return response;
    }

    [HttpPost]
    //[InputValidation]
    public HttpResponseMessage SaveAssetBrand([FromBody] AssetBrandModel data)
    {
        ////int val = ABS.SaveAssetBrand(data);
        ////if (val == 0)
        ////{
        ////    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, data);
        ////    return response;
        ////}
        //////else if (val == 1)
        //////    return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already Exists");

        ////HttpResponseMessage response1 = Request.CreateResponse(HttpStatusCode.OK, data);
        ////return response1;
        var obj = ABS.SaveAssetBrand(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    //[InputValidation]
    public HttpResponseMessage ModifyAssetBrand(AssetBrandModel updatedata)
    {
        var obj = ABS.ModifyAssetBrand(updatedata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpGet]
    // [Route("GetGridData")]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<AssetBrandModel> sev = ABS.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, sev);
        return response;
    }
}
