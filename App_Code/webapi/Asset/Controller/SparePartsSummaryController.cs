﻿using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SparePartsSummaryController : ApiController
{
    SparePartsSummaryService SAS = new SparePartsSummaryService();
    [HttpPost]
    public HttpResponseMessage GetData()
    {
        var obj = SAS.GetData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetAssetDetails(CustomizableRptVM CustRpt)
    {
        var obj = SAS.GetAssetDetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage SearchData(CustomizableRpt CustRpt)
    {
        var obj = SAS.SearchData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage SearchToTInventoryToTCost(CustomizableRpt CustRpt)
    {
        var obj = SAS.SearchToTInventoryToTCost(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    public HttpResponseMessage SearchToTInventoryToTCostReport(CustomizableRpt CustRpt)
    {
        var obj = SAS.SearchToTInventoryToTCostReport(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
