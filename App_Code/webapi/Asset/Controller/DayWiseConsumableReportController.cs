﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class DayWiseConsumableReportController : ApiController
{
    public string FilePath;
    DayWiseConsumableReportService DCRS = new DayWiseConsumableReportService();

    [HttpGet]
    public HttpResponseMessage AssetMainCategory()
    {
        IEnumerable<Categoriess> AssetMainCategoryList = DCRS.AssetMainCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetMainCategoryList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetSubCategory()
    {
        IEnumerable<SubCategory> AssetSUbCategoryList = DCRS.AssetSubCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetSUbCategoryList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetBrand()
    {
        IEnumerable<Brand> AssetBrandList = DCRS.AssetBrand();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetBrandList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetModel()
    {
        IEnumerable<Model> AssetModelList = DCRS.AssetModel();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetModelList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetLOCATION()
    {
        IEnumerable<lOCATION> AssetLOCList = DCRS.AssetLOCATION();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, AssetLOCList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage AssetCITY()
    {
        IEnumerable<cityLists> cityLists = DCRS.AssetCITY();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, cityLists);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage DayWiseConsReport(DayWiseConsReport data)
    {
        var obj = DCRS.DayWiseConsReport(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
