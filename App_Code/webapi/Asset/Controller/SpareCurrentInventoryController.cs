﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SpareCurrentInventoryController : ApiController
{
    SpareCurrentInventoryService scis = new SpareCurrentInventoryService();
    [HttpPost]
    public HttpResponseMessage GetData()
    {
        var obj = scis.GetData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetAssetDetails(CustomizableRptVM CustRpt)
    {
        var obj = scis.GetAssetDetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage SearchData(CustomizableRpt CustRpt)
    {
        var obj = scis.SearchData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
