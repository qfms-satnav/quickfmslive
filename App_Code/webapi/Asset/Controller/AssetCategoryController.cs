﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Http;
//using static System.Windows.Forms.VisualStyles.VisualStyleElement.TreeView;
//using static System.Windows.Forms.VisualStyles.VisualStyleElement.TreeView;

//[RoutePrefix("api/AssetCategory")]
public class AssetCategoryController : ApiController
{
    AssetCategoryService ACS = new AssetCategoryService();


    [HttpPost]
    //[InputValidation]
    public HttpResponseMessage SaveAssetCategory([FromBody] AssetCategoryModel data)
    {
        var obj = ACS.SaveAssetCategory(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    //[InputValidation]
    public HttpResponseMessage ModifyAssetCategories(AssetCategoryModel updatedata)
    {
        var obj = ACS.ModifyAssetCategories(updatedata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    // [Route("GetGridData")]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<AssetCategoryModel> sev = ACS.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, sev);
        return response;
    }
    [HttpGet]
    
    public HttpResponseMessage AssetTypes()
    {
        IEnumerable<AssetType> sev = ACS.AssetTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, sev);
        return response;
    }

    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;
        string sPath = "";
        string DB = HttpContext.Current.Session["TENANT"].ToString();

        var httpRequest = HttpContext.Current.Request;
        //var validateFile = FileValidator.ValidateUploadedFile(httpRequest.Files[0], ".xlsx,.xls,.pdf", 11000);
        //sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/");
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/");

        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {

            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                //hpf.SaveAs(sPath + Path.GetFileName( hpf.FileName.Replace(" ","")));
                hpf.SaveAs(sPath + System.IO.Path.GetFileName(hfc.Keys[iCnt].ToString()));
                iUploadedCnt = iUploadedCnt + 1;
            }

        }
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }

}
