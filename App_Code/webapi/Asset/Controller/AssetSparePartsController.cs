﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http.Headers;
using ClosedXML.Excel;


public class AssetSparePartsController : ApiController
{
    AssetSparePartsService ASP = new AssetSparePartsService();
    [HttpPost]
    public HttpResponseMessage GetAssetDetails(CustomizableRptVM CustRpt)
    {
        var obj = ASP.GetAssetDetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetVendor(CustomizableRptVM CustRpt)
    {
        var obj = ASP.GetVendor(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(CustomizableRptVM CustRpt)
    {
        var obj = ASP.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    public HttpResponseMessage updateData(CustomizableRptVM CustRpt)
    {
        var obj = ASP.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage GetSpareSub(string q)
    {
        var obj = ASP.GetSpareSub(q);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage GetSpareBrand(string q)
    {
        var obj = ASP.GetSpareBrand(q);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    public HttpResponseMessage GetSpareModel(string q)
    {
        var obj = ASP.GetSpareModel(q);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetAccestData(CustomizableRptVM CustRpt)
    {
        var obj = ASP.GetAccestData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage UploadExcel()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = ASP.UploadExcel(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object DownloadTemplate(string loc)
    {
        var httpRequest = HttpContext.Current.Request;
        return ASP.GetSpareDataDump(loc);

    }
    [HttpPost]
    public HttpResponseMessage GetZone()
    {
        var obj = ASP.GetZone();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}




