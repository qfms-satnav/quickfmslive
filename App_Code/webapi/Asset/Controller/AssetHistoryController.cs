﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class AssetHistoryController : ApiController
{
    AssetHistoryService ECRS = new AssetHistoryService();
    //[GzipCompression]
    //[HttpPost]
    //public HttpResponseMessage AssetHistoryData()
    //{
    //    var obj = ECRS.GetAssetHistory();
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    [GzipCompression]
    //[HttpPost]
    //public object AssetHistoryData()
    //{
    //    return ECRS.GetAssetHistory();
    //}

    [HttpPost]
    public HttpResponseMessage GetStatus(AssetHistoryModel ASHM)
    {
        var obj = ECRS.GetStatus(ASHM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SearchData(AssetHistoryModel ASHM)
    {
        var obj = ECRS.SearchData(ASHM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
