﻿using System;
using SubSonic;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Runtime.InteropServices;
using UtiltiyVM;
using QuickFMS.API.Filters;

public class AssetMappingController : ApiController
{
    AssetMappingService astm = new AssetMappingService();

    [HttpPost]
    public HttpResponseMessage GetGriddata(AssetMappinglist Equipment)
    {
        var obj = astm.GetGriddata(Equipment);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveData(AssetMappinglist data)
    {
        var obj = astm.SaveData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetEmployees(AssetMappinglist data)
    {
        var obj = astm.GetEmployees(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetReqIds([FromBody] AssetMappinglist data)
    {
        var obj = astm.GetReqIds(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetTowers([FromBody] AssetMappinglist data)
    {
        var obj = astm.GetTowers(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetFloor([FromBody] AssetMappinglist data)
    {
        var obj = astm.GetFloor(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetSpaceIds([FromBody] AssetMappinglist data)
    {
        var obj = astm.GetSpaceIds(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}



