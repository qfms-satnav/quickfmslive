﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for AssetMappingService
/// </summary>
public class AssetMappingService
{
    SubSonic.StoredProcedure sp;
    List<AssetEquipmentMappedData> Cust;
    AssetEquipmentMappedData Ecustm;
    DataSet ds;





    public object GetEmployees(AssetMappinglist data)
    {
        try
        {

            List<Emplst> Emplist = new List<Emplst>();
            Emplst Emplst;

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(data.loclst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AM_GET_AEMP_LOCLIST", param))
            {
                while (sdr.Read())
                {
                    Emplst = new Emplst();
                    Emplst.AUR_FIRST_NAME = sdr["AUR_FIRST_NAME"].ToString();
                    Emplst.AUR_ID = sdr["AUR_ID"].ToString();

                    Emplst.ticked = false;
                    Emplist.Add(Emplst);
                }
            }
            if (Emplist.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Emplist };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }

    public object GetReqIds(AssetMappinglist data)
    {
        try
        {

            List<Reqlst> Reqlist = new List<Reqlst>();
            Reqlst Reqlst;

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(data.loclst);
            param[1] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(data.Categorylst);
            param[2] = new SqlParameter("@BRND_LIST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(data.Brandlst);
            param[3] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(data.Modellst);
            param[4] = new SqlParameter("@SUB_CAT_LIST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(data.Subcatlst);

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AST_GETALLREQID_ON_CATEGORY_FILTER", param))
            {
                while (sdr.Read())
                {
                    Reqlst = new Reqlst();
                    Reqlst.AIR_REQ_TS = sdr["AIR_REQ_TS"].ToString();


                    Reqlst.ticked = false;
                    Reqlist.Add(Reqlst);
                }
            }
            if (Reqlist.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Reqlist };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }

    public object GetTowers(AssetMappinglist data)
    {
        try
        {

            List<Twrlst> Twrlist = new List<Twrlst>();
            Twrlst Twrlst;

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[0].Value = data.LCM_CODE;
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[AM_GET_TOWERLIST]", param))
            {
                while (sdr.Read())
                {
                    Twrlst = new Twrlst();
                    Twrlst.TWR_CODE = sdr["TWR_CODE"].ToString();
                    Twrlst.TWR_NAME = sdr["TWR_NAME"].ToString();
                    Twrlst.LCM_CODE = sdr["TWR_LOC_ID"].ToString();
                    Twrlst.CTY_CODE = sdr["TWR_CTY_ID"].ToString();
                    Twrlst.CNY_CODE = sdr["TWR_CNY_ID"].ToString();
                    Twrlst.isChecked = false;
                    Twrlist.Add(Twrlst);
                }
            }
            if (Twrlist.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Twrlist };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }
    public object GetFloor(AssetMappinglist data)
    {
        try
        {

            List<Flrlst> FLrlist = new List<Flrlst>();
            Flrlst Flrlst;

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@TWR_CODE", SqlDbType.NVarChar);
            param[0].Value = data.TWR_CODE;
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[AM_GET_FLOORLIST]", param))
            {
                while (sdr.Read())
                {
                    Flrlst = new Flrlst();
                    Flrlst.FLR_CODE = sdr["FLR_CODE"].ToString();
                    Flrlst.FLR_NAME = sdr["FLR_NAME"].ToString();
                    Flrlst.TWR_CODE = sdr["FLR_TWR_ID"].ToString();
                    Flrlst.LCM_CODE = sdr["FLR_LOC_ID"].ToString();
                    Flrlst.CTY_CODE = sdr["FLR_CTY_ID"].ToString();
                    Flrlst.CNY_CODE = sdr["FLR_CNY_ID"].ToString();
                    Flrlst.isChecked = false;
                    FLrlist.Add(Flrlst);
                }
            }
            if (FLrlist.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = FLrlist };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }
    public object GetSpaceIds(AssetMappinglist data)
    {
        try
        {

            List<SPC_LIST> SPCIDS_LIST = new List<SPC_LIST>();
            SPC_LIST SPC_LIST;

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[0].Value = data.LCM_CODE;
            param[1] = new SqlParameter("@TWR_CODE", SqlDbType.NVarChar);
            param[1].Value = data.TWR_CODE;
            param[2] = new SqlParameter("@FLR_CODE", SqlDbType.NVarChar);
            param[2].Value = data.FLR_CODE;
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[GET_SPACE_ASSETMAPPING]", param))
            {
                while (sdr.Read())
                {
                    SPC_LIST = new SPC_LIST();
                    SPC_LIST.SPC_ID = sdr["SPC_ID"].ToString();
                    SPC_LIST.SPC_NAME = sdr["SPC_NAME"].ToString();

                    SPC_LIST.isChecked = false;
                    SPCIDS_LIST.Add(SPC_LIST);
                }
            }
            if (SPCIDS_LIST.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = SPCIDS_LIST };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }

    public object GetGriddata(AssetMappinglist Equipment)
    {
        try
        {
            DataTable dt = SearchAllData(Equipment);
            if (dt.Rows.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = dt };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }




    public DataTable SearchAllData(AssetMappinglist Equipment)
    {
        List<AssetMappinglist> ECData = new List<AssetMappinglist>();
        DataTable DT = new DataTable();
        //DataSet ds;
        try
        {
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AM_GET_ASSET_DATA_GRIDBIND");

            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Equipment.Categorylst);
            //sp.Command.AddParameter("@CAT_LIST", Equipment.Categorylst, DbType.Structured);
            param[1] = new SqlParameter("@BRND_LIST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Equipment.Brandlst);

            param[2] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(Equipment.Modellst);

            param[3] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(Equipment.loclst);
            param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            param[5] = new SqlParameter("@SUB_CAT_LIST", SqlDbType.Structured);
            param[5].Value = UtilityService.ConvertToDataTable(Equipment.Subcatlst);
            param[6] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[6].Value = Equipment.Request_Type;
            param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            param[8] = new SqlParameter("@PAGENUM", SqlDbType.Int);
            param[8].Value = Equipment.PageNumber;
            param[9] = new SqlParameter("@PAGESIZE", SqlDbType.Int);
            param[9].Value = Equipment.PageSize;
            param[10] = new SqlParameter("@SEARCH", SqlDbType.NVarChar);
            param[10].Value = Equipment.Search;

            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[AM_GET_ASSET_DATA_GRIDBIND]", param);

            return DT;


        }
        catch (Exception ex)
        {
            return DT;
        }
    }
    public object SaveData(AssetMappinglist data)
    {
        if (data.Emplst.Count == 0)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@MAPPING_DETAILS", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(data.Mappedlst);
                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[UPDATE_AMT_ASSET_SPACE_STATUS]", param);

                return new { Message = MessagesVM.SPC_REQ_UPDATED };
            }
            catch (Exception ex)
            {
                return new { Message = MessagesVM.SPC_REQ_CANCELED + '/' + ex.Message.ToString() };
            }
        }
        else
        {
            try
            {

                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@MAPPING_DETAILS", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(data.Mappedlst);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = HttpContext.Current.Session["UID"];
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.Int);
                param[2].Value = HttpContext.Current.Session["COMPANYID"];
                param[3] = new SqlParameter("@Remarks", SqlDbType.NVarChar);
                param[3].Value = data.Remarks;
                param[4] = new SqlParameter("@Emp_lst", SqlDbType.Structured);
                param[4].Value = UtilityService.ConvertToDataTable(data.Emplst);
                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[ALLOCATE_ASSET_EMPLOYEE]", param);

                return new { Message = MessagesVM.SPC_REQ_UPDATED };
            }
            catch (Exception ex)
            {
                return new { Message = MessagesVM.SPC_REQ_CANCELED + '/' + ex.Message.ToString() };
            }
        }

    }
}