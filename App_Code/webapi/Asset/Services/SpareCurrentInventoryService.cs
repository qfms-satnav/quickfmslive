﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


/// <summary>
/// Summary description for SpareCurrentInventoryService
/// </summary>
public class SpareCurrentInventoryService
{
    SubSonic.StoredProcedure sp;
    public object GetData()
    {

        try
        {
            DataSet ds = new DataSet();
            //sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AMG_AST_SPAREPARTSS");
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ADDSPARESDATA");
            sp.Command.AddParameter("@USERID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object GetAssetDetails(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS_LCM_VENDOR");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            sp.Command.AddParameter("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
            sp.Command.AddParameter("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object SearchData(CustomizableRpt CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[Search_SpareParts2]");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            sp.Command.AddParameter("@USERID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@SUBC_NAME", CustRpt.SUBC_NAME, DbType.String);
            sp.Command.AddParameter("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
            sp.Command.AddParameter("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
            sp.Command.AddParameter("@SUBTYPE_NAME", "All", DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}