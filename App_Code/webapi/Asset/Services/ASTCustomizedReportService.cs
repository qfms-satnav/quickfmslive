﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for EquipmentCusomizedReportService
/// </summary>
public class EquipmentCustomizedReportService
{
    SubSonic.StoredProcedure sp;
    List<EquipmentCustomizedData> Cust;
    EquipmentCustomizedData Ecustm;
    DataSet ds;
    public object GetCustomizedObject(EquipmentVM Equipment)
    {
        try
        {
            DataTable dt = GetCustomizedDetails(Equipment);
            if (dt.Rows.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = dt };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetCustomizedDetails(EquipmentVM Equipment)
    {

        List<EquipmentCustomizedData> ECData = new List<EquipmentCustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            

                SqlParameter[] param = new SqlParameter[8];
                param[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
                if (Equipment.Categorylst == null)
                {
                    param[0].Value = null;
                }
                else
                {
                    param[0].Value = UtilityService.ConvertToDataTable(Equipment.Categorylst);
                }

                param[1] = new SqlParameter("@BRND_LIST", SqlDbType.Structured);
                if (Equipment.Brandlst == null)
                {
                    param[1].Value = null;
                }
                else
                {
                    param[1].Value = UtilityService.ConvertToDataTable(Equipment.Brandlst);
                }

                param[2] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
                if (Equipment.Modellst == null)
                {
                    param[2].Value = null;
                }
                else
                {
                    param[2].Value = UtilityService.ConvertToDataTable(Equipment.Modellst);
                }

                param[3] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
                if (Equipment.loclst == null)
                {
                    param[3].Value = null;
                }
                else
                {
                    param[3].Value = UtilityService.ConvertToDataTable(Equipment.loclst);
                }

                param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[4].Value = HttpContext.Current.Session["UID"];
                param[5] = new SqlParameter("@SUB_CAT_LIST", SqlDbType.Structured);
                if (Equipment.Subcatlst == null)
                {
                    param[5].Value = null;
                }
                else
                {
                    param[5].Value = UtilityService.ConvertToDataTable(Equipment.Subcatlst);
                }
                param[6] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
                param[6].Value = Equipment.Request_Type;
                if (Equipment.CNP_NAME == null)
                {
                    param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
                    param[7].Value = HttpContext.Current.Session["COMPANYID"];
                }
                else
                {
                    param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
                    param[7].Value = Equipment.CNP_NAME;
                }

                //param[8] = new SqlParameter("@PageNumber", SqlDbType.Int);
                //param[8].Value = Equipment.PageNumber;
                //param[9] = new SqlParameter("@PageSize", SqlDbType.Int);
                //param[9].Value = Equipment.PageSize;
                //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "GET_EQUIPMENT_CUSTOMIZED_DATA", param);

                //DataSet DS = new DataSet();
                DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "AM_GET_ASSET_CUSTOMIZED_DATA_GRIDBIND", param);
                HttpContext.Current.Session["AssetCustomizedGrid"] = DT;
                return DT;
           
           
          
            //DS = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "AM_GET_ASSET_CUSTOMIZED_DATA_GRIDBIND", param);
            //using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AM_GET_ASSET_CUSTOMIZED_DATA_GRIDBIND", param))
            //{
            //    while (reader.Read())
            //    {
            //        Ecustm = new EquipmentCustomizedData();

            //        Ecustm.AAT_NAME = reader["AAT_NAME"].ToString();
            //        Ecustm.VT_TYPE = reader["VT_TYPE"].ToString();
            //        Ecustm.MANUFACTURER = reader["MANUFACTURER"].ToString();
            //        Ecustm.AST_MD_NAME = reader["AAT_MODEL_NAME"].ToString();
            //        Ecustm.AST_SUBCAT_NAME = reader["AST_SUBCAT_NAME"].ToString();
            //        DateTime? dt = null;
            //        Ecustm.PURCHASEDDATE = reader["PURCHASEDDATE"] is DBNull ? dt : Convert.ToDateTime(reader["PURCHASEDDATE"]);
            //        Ecustm.AAT_WRNTY_DATE = reader["AAT_WRNTY_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AAT_WRNTY_DATE"]);
            //        Ecustm.TOTALMONTHS = reader["TOTALMONTHS"].ToString();
            //        Ecustm.AAT_AST_COST = reader["AAT_AST_COST"] == null ? (float)reader["AAT_AST_COST"] : 0;
            //        Ecustm.STATUS = reader["STATUS"].ToString();
            //        Ecustm.MAPPEDTO = reader["MAPPEDTO"].ToString();
            //        Ecustm.MOVEDFROMLOCATION = reader["MOVEDFROMLOCATION"].ToString();
            //        Ecustm.MOVEDTOLOCATION = reader["MOVEDTOLOCATION"].ToString();
            //        Ecustm.RAISEDBY = reader["RAISEDBY"].ToString();
            //        Ecustm.INTERRECEIVEDBY = reader["INTERRECEIVEDBY"].ToString();
            //        Ecustm.AVR_NAME = reader["AVR_NAME"].ToString();
            //        Ecustm.AAT_LOC_ID = reader["AAT_LOC_ID"].ToString();
            //        //Ecustm.AAT_CODE = reader["AAT_CODE"].ToString();
            //        Ecustm.Asset_count = (int)reader["Asset_count"];
            //        Ecustm.LCM_NAME = reader["LCM_NAME"].ToString();
            //        Ecustm.AAT_AST_SERIALNO = reader["AAT_AST_SERIALNO"].ToString();
            //        Ecustm.Asset_Age = reader["Asset_Age"].ToString();
            //        Ecustm.DISPOSAL_STATUS = reader["DISPOSAL_STATUS"].ToString();
            //        Ecustm.AAT_MFG_DATE = reader["AAT_MFG_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AAT_MFG_DATE"]);
            //        Ecustm.AAT_DESC = reader["AAT_DESC"].ToString();
            //        Ecustm.TAG_NAME = reader["TAG_NAME"].ToString();
            //        Ecustm.AAT_STA_ID = reader["AAT_STA_ID"].ToString();
            //        Ecustm.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
            //        Ecustm.AAT_INVOICE_DATE = reader["AAT_INVOICE_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AAT_INVOICE_DATE"]);

            //        //Ecustm.PURCHASEDDATE = reader["PURCHASEDDATE"] == null ? (DateTime)reader["PURCHASEDDATE"] : DateTime.Now;
            //        //Ecustm.AAT_AMC_DATE = reader["AAT_AMC_DATE"] == null ? (DateTime)reader["AAT_AMC_DATE"] : DateTime.Now;
            //        ECData.Add(Ecustm);

            //    }
            //    reader.Close();
            //}
            //return ECData;
        }
        catch (Exception ex)
        {
            return DT;
        }
    }

    //public object GetSearchAllData(EquipmentVM param)
    //{
    //    try
    //    {
    //        DataTable dt =  SearchAllData(param);

    //        if (dt.Rows.Count != 0)

    //            return new { Message = MessagesVM.UM_NO_REC, data = dt };
    //        else
    //            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
    //    }
    //}
    public List<EquipmentCustomizedData> SearchAllData(EquipmentVM Equipment)
    {
        List<EquipmentCustomizedData> ECData = new List<EquipmentCustomizedData>();
        DataTable DT1 = new DataTable();
        try
        {
            DataTable DT = new DataTable();
            DT = (DataTable)HttpContext.Current.Session["AssetCustomizedGrid"];
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(DT);
            ECData = JsonConvert.DeserializeObject<List<EquipmentCustomizedData>>(JSONString);
            return ECData;
        }
        catch(Exception ex)
        {
            return ECData;
        }
    }
    public object GetCustomizedObject2(EquipmentVM Equipment)
    {
        try
        {
            DataTable dt = GetCustomizedDetailsMapped(Equipment);
            if (dt.Rows.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = dt };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetCustomizedDetailsMapped(EquipmentVM Equipment)
    {

        List<EquipmentCustomizedData> ECData = new List<EquipmentCustomizedData>();
        DataTable DT = new DataTable();
        try
        {


            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
            if (Equipment.Categorylst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Equipment.Categorylst);
            }

            param[1] = new SqlParameter("@BRND_LIST", SqlDbType.Structured);
            if (Equipment.Brandlst == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(Equipment.Brandlst);
            }

            param[2] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
            if (Equipment.Modellst == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(Equipment.Modellst);
            }

            param[3] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
            if (Equipment.loclst == null)
            {
                param[3].Value = null;
            }
            else
            {
                param[3].Value = UtilityService.ConvertToDataTable(Equipment.loclst);
            }

            param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            param[5] = new SqlParameter("@SUB_CAT_LIST", SqlDbType.Structured);
            if (Equipment.Subcatlst == null)
            {
                param[5].Value = null;
            }
            else
            {
                param[5].Value = UtilityService.ConvertToDataTable(Equipment.Subcatlst);
            }
            param[6] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[6].Value = Equipment.Request_Type;
            if (Equipment.CNP_NAME == null)
            {
                param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
                param[7].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
                param[7].Value = Equipment.CNP_NAME;
            }

            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "AM_GET_ASSET_CUSTOMIZED_DATA_GRIDBIND_AVAILABLE_STOCK", param);
            HttpContext.Current.Session["AssetCustomizedGrid"] = DT;
            return DT;



        }
        catch (Exception ex)
        {
            return DT;
        }
    }

    //public DataTable ExporttoExcel(EquipmentVM Equipment)
    //{

    //    try
    //    {
    //        List<EquipmentCustomizedData> ECData = new List<EquipmentCustomizedData>();
    //        SqlParameter[] param = new SqlParameter[11];
    //        param[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
    //        if (Equipment.Categorylst == null)
    //        {
    //            param[0].Value = null;
    //        }
    //        else
    //        {
    //            param[0].Value = UtilityService.ConvertToDataTable(Equipment.Categorylst);
    //        }

    //        param[1] = new SqlParameter("@BRND_LIST", SqlDbType.Structured);
    //        if (Equipment.Brandlst == null)
    //        {
    //            param[1].Value = null;
    //        }
    //        else
    //        {
    //            param[1].Value = UtilityService.ConvertToDataTable(Equipment.Brandlst);
    //        }

    //        param[2] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
    //        if (Equipment.Modellst == null)
    //        {
    //            param[2].Value = null;
    //        }
    //        else
    //        {
    //            param[2].Value = UtilityService.ConvertToDataTable(Equipment.Modellst);
    //        }

    //        param[3] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
    //        if (Equipment.loclst == null)
    //        {
    //            param[3].Value = null;
    //        }
    //        else
    //        {
    //            param[3].Value = UtilityService.ConvertToDataTable(Equipment.loclst);
    //        }

    //        param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
    //        param[4].Value = HttpContext.Current.Session["UID"];
    //        param[5] = new SqlParameter("@SUB_CAT_LIST", SqlDbType.Structured);
    //        if (Equipment.Subcatlst == null)
    //        {
    //            param[5].Value = null;
    //        }
    //        else
    //        {
    //            param[5].Value = UtilityService.ConvertToDataTable(Equipment.Subcatlst);
    //        }
    //        param[6] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
    //        param[6].Value = Equipment.Request_Type;
    //        if (Equipment.CNP_NAME == null)
    //        {
    //            param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
    //            param[7].Value = HttpContext.Current.Session["COMPANYID"];
    //        }
    //        else
    //        {
    //            param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
    //            param[7].Value = Equipment.CNP_NAME;
    //        }

    //        param[8] = new SqlParameter("@PageNumber", SqlDbType.Int);
    //        param[8].Value = Equipment.PageNumber;
    //        param[9] = new SqlParameter("@PageSize", SqlDbType.Int);
    //        param[9].Value = Equipment.PageSize;
    //        param[10] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
    //        param[10].Value = Equipment.SearchValue;
    //        //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "GET_EQUIPMENT_CUSTOMIZED_DATA", param);

    //        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "AM_GET_ASSET_CUSTOMIZED_DATA", param);
    //        return dt;       
    //    }
    //    catch (Exception ex)
    //    {
    //        throw;
    //    }
    //}
}