﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for DayWiseConsumableReportService
/// </summary>
public class DayWiseConsumableReportService
{
    SubSonic.StoredProcedure sp;
    List<DayWiseConsumableReportModel> dayWiseConsumableReportModels;
    DayWiseConsumableReportModel Daywise;
    DataSet DT;
    public IEnumerable<Categoriess> AssetMainCategory()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[ASSET_CONS_CATEGORIES]").GetReader();
        List<Categoriess> AssetMainCategoryList = new List<Categoriess>();
        while (reader.Read())
        {
            AssetMainCategoryList.Add(new Categoriess()
            {
                VT_CODE = reader.GetString(0),
                VT_TYPE = reader.GetString(1),
                VT_CONS_STATUS = reader.GetInt32(2).ToString(),
                ticked = false

            });
        }
        reader.Close();
        return AssetMainCategoryList;
    }
    public IEnumerable<SubCategory> AssetSubCategory()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[ASSET_CONS_SUBCATEGORIES]").GetReader();
        List<SubCategory> AssetSUbCategoryList = new List<SubCategory>();
        while (reader.Read())
        {
            AssetSUbCategoryList.Add(new SubCategory()
            {
                AST_SUBCAT_CODE = reader.GetString(0),
                AST_SUBCAT_NAME = reader.GetString(1),
                VT_CODE = reader.GetString(2),
                AST_CAT_CODE = reader.GetString(3),
                ticked = false

            });
        }
        reader.Close();
        return AssetSUbCategoryList;
    }
    public IEnumerable<Brand> AssetBrand()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[ASSET_CONS_BRAND]").GetReader();
        List<Brand> AssetBrandList = new List<Brand>();
        while (reader.Read())
        {
            AssetBrandList.Add(new Brand()
            {
                manufactuer_code = reader.GetString(0),
                manufacturer = reader.GetString(1),
                AST_SUBCAT_NAME = reader.GetString(2),
                manufacturer_type_code = reader.GetString(3),
                AST_SUBCAT_CODE = reader.GetString(4),
                VT_CODE = reader.GetString(5),
                manufacturer_type_subcode = reader.GetString(6),
                ticked = false

            });
        }
        reader.Close();
        return AssetBrandList;
    }
    public IEnumerable<Model> AssetModel()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[ASSET_CONS_MODEL]").GetReader();
        List<Model> AssetModelList = new List<Model>();
        while (reader.Read())
        {
            AssetModelList.Add(new Model()
            {
                AST_MD_CODE = reader.GetString(0),
                AST_MD_NAME = reader.GetString(1),
                AST_MD_BRDID = reader.GetString(2),
                AST_MD_SUBCATID = reader.GetString(3),
                AST_MD_CATID = reader.GetString(4),
                VT_CODE = reader.GetString(5),
                AST_SUBCAT_CODE = reader.GetString(6),
                manufactuer_code = reader.GetString(7),
                ticked = false

            });
        }
        reader.Close();
        return AssetModelList;
    }
    public IEnumerable<lOCATION> AssetLOCATION()
    {
        List<lOCATION> AssetLOCList = new List<lOCATION>();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        IDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[ASSET_LOCATIONS]", param);
        while (reader.Read())
        {
            AssetLOCList.Add(new lOCATION()
            {
                LCM_CODE = reader.GetString(0),
                LCM_NAME = reader.GetString(1),
                CTY_CODE = reader.GetString(2),
                CNY_CODE = reader.GetString(3),
                ticked = false
            });
        }
        reader.Close();
        return AssetLOCList;
    }
    public IEnumerable<cityLists> AssetCITY()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[ASSET_CONS_CITY]").GetReader();
        List<cityLists> cityLists = new List<cityLists>();
        while (reader.Read())
        {
            cityLists.Add(new cityLists()
            {
                CTY_CODE = reader.GetString(0),
                CTY_NAME = reader.GetString(1),
                CNY_CODE = reader.GetString(2),
                ticked = false

            });
        }
        reader.Close();
        return cityLists;
    }

    //public object DayWiseConsReport(DayWiseConsReport data)
    //{
    //    try
    //    {
    //        DataTable dt = GetConsumedReportDetails(data);
    //        if (dt.Rows.Count != 0)
    //        {
    //            return new { Message = MessagesVM.SER_OK, data = dt };
    //        }
    //        else
    //        {
    //            return new { Message = MessagesVM.SER_OK, data = (object)null };
    //        }

    //    }

    //    catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    //}
    public object DayWiseConsReport(DayWiseConsReport data)
    {
        //List<DayWiseConsReport> dayWiseConsReports = new List<DayWiseConsReport>();
        DataTable DT = new DataTable();
        List<ConsumableGridCols> gridcols = new List<ConsumableGridCols>();
        ConsumableGridCols gridcolscls;
        try
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];
            param[1] = new SqlParameter("@CATE_LIST", SqlDbType.Structured);
            if (data.AssetMainCategoryList == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(data.AssetMainCategoryList);
            }
            param[2] = new SqlParameter("@SUBCAT_LIST", SqlDbType.Structured);
            if (data.AssetSUbCategoryList == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(data.AssetSUbCategoryList);
            }
            param[3] = new SqlParameter("@BRAND_LIST", SqlDbType.Structured);
            if (data.AssetBrandList == null)
            {
                param[3].Value = null;
            }
            else
            {
                param[3].Value = UtilityService.ConvertToDataTable(data.AssetBrandList);
            }

            param[4] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
            if (data.AssetModelList == null)
            {
                param[4].Value = null;
            }
            else
            {
                param[4].Value = UtilityService.ConvertToDataTable(data.AssetModelList);
            }
            param[5] = new SqlParameter("@LOACTIONLST", SqlDbType.Structured);
            if (data.AssetLOCList == null)
            {
                param[5].Value = null;
            }
            else
            {
                param[5].Value = UtilityService.ConvertToDataTable(data.AssetLOCList);
            }
            param[6] = new SqlParameter("@CITYLST", SqlDbType.Structured);
            if (data.cityLists == null)
            {
                param[6].Value = null;
            }
            else
            {
                param[6].Value = UtilityService.ConvertToDataTable(data.cityLists);
            }
            param[7] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[7].Value = data.Fromdate;
            param[8] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[8].Value = data.Todate;
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "DAY_WISE_CONSUMABLE_STOCK_REPORT", param);
            //HttpContext.Current.Session["DayWiseConsumableReport"] = DT;
            //return DT;
            if (DT != null && DT.Rows.Count > 0)
            {
                List<string> Colstr = (from dc in DT.Columns.Cast<DataColumn>()
                                       select dc.ColumnName).ToList();

                foreach (string col in Colstr)
                {
                    gridcolscls = new ConsumableGridCols
                    {
                        cellClass = "grid-align",
                        field = col,
                        headerName = col
                    };
                    gridcols.Add(gridcolscls);
                }
                return new { griddata = DT, Coldef = gridcols };
            }

            return new { griddata = (object)null, Coldef = (object)null };
        }
        catch (Exception ex)
        {
            return DT;
        }
    }
}