﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for AssetModelService
/// </summary>
public class AssetModelService
{
    SubSonic.StoredProcedure sp;
    //DataSet ds;

    public IEnumerable<assetmainCategory> AssetMainCategoryModel()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[AM_USP_GET_ASSETCATEGORIES_MODEL]").GetReader();
        List<assetmainCategory> AssetMainCategoryList = new List<assetmainCategory>();
        while (reader.Read())
        {
            AssetMainCategoryList.Add(new assetmainCategory()
            {
                VT_CODE = reader.GetValue(0).ToString(),
                VT_TYPE = reader.GetValue(1).ToString()

                //ticked = false

            });
        }
        reader.Close();
        return AssetMainCategoryList;
    }
    public IEnumerable<assetSubCategorybycat> AssetSubCategoryModel()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[AM_USP_GET_ASSETSUBCATBYASSET_MODEL]").GetReader();
        List<assetSubCategorybycat> AssetSUbCategoryList = new List<assetSubCategorybycat>();
        while (reader.Read())
        {
            AssetSUbCategoryList.Add(new assetSubCategorybycat()
            {
                AST_SUBCAT_CODE = reader.GetValue(0).ToString(),
                AST_SUBCAT_NAME = reader.GetValue(1).ToString(),
                //AST_CAT_CODE = reader.GetValue(2).ToString(),
                //VT_TYPE = reader.GetValue(3).ToString(),

                VT_CODE = reader.GetValue(2).ToString()
                //Assetmaincatcode = reader.GetValue(3).ToString(),
                //ticked = false


            });
        }
        reader.Close();
        return AssetSUbCategoryList;
    }
    public IEnumerable<AstBrandbysubcat> AssetBrandModel()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[AM_AST_GET_MAKEBYCATSUBCAT_MODEL]").GetReader();
        List<AstBrandbysubcat> AssetBrandList = new List<AstBrandbysubcat>();
        while (reader.Read())
        {
            AssetBrandList.Add(new AstBrandbysubcat()
            {
                manufactuer_code = reader.GetValue(0).ToString(),
                manufacturer = reader.GetValue(1).ToString(),
                manufacturer_type_subcode = reader.GetValue(2).ToString(),
                AST_SUBCAT_NAME = reader.GetValue(3).ToString(),
                manufacturer_type_code = reader.GetValue(4).ToString(),
                VT_CODE = reader.GetValue(5).ToString(),
                AST_SUBCAT_CODE = reader.GetValue(6).ToString()
                // ticked = false


            });
        }
        reader.Close();
        return AssetBrandList;
    }

    public object SaveAssetModel(AssetModelmodel data)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[12];

            param[0] = new SqlParameter("@AST_MD_CODE", SqlDbType.NVarChar);
            param[0].Value = data.AST_MD_CODE;

            param[1] = new SqlParameter("@AST_MD_NAME", SqlDbType.NVarChar);
            param[1].Value = data.AST_MD_NAME;

            param[2] = new SqlParameter("@AST_MD_BRDID", SqlDbType.NVarChar);
            param[2].Value = data.AST_MD_BRDID;

            param[3] = new SqlParameter("@AST_MD_SUBCATID", SqlDbType.NVarChar);
            param[3].Value = data.AST_MD_SUBCATID;

            param[4] = new SqlParameter("@AST_MD_CATID", SqlDbType.NVarChar);
            param[4].Value = data.AST_MD_CATID;

            param[5] = new SqlParameter("@AST_MD_STAID", SqlDbType.Int);
            param[5].Value = data.AST_MD_STAID;

            param[6] = new SqlParameter("@AST_MD_CREATEDBY", SqlDbType.NVarChar);
            param[6].Value = HttpContext.Current.Session["UID"];

            param[7] = new SqlParameter("@AST_MD_MINQTY", SqlDbType.Int);
            param[7].Value = 0;

            param[8] = new SqlParameter("@AST_MD_STKQTY", SqlDbType.Int);
            param[8].Value = 0;

            param[9] = new SqlParameter("@AST_MD_QTYUNIT", SqlDbType.VarChar);
            param[9].Value = 0;

            param[10] = new SqlParameter("@AST_MD_REM", SqlDbType.NVarChar);
            param[10].Value = data.AST_MD_REM;

            param[11] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[11].Value = HttpContext.Current.Session["COMPANYID"];


            object result = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "MAS_INSERT_MODEL", param);
            int resultCode = Convert.ToInt32(result);
            string msg;


            if (resultCode == 1)
                msg = "New Asset Model Successfully Added";
            else if (resultCode == 0)
                msg = "Asset Model already exists.";
            else
                msg = "Something Went Wrong";


            return new { data = data, Message = msg };
        }
        catch (Exception ex)
        {

            return new { data = (object)null, Message = ex.Message };
        }
    }

    public object ModifyAssetModel(AssetModelmodel updatedata)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[13];
            param[0] = new SqlParameter("@AST_MD_CODE", SqlDbType.NVarChar);
            param[0].Value = updatedata.AST_MD_CODE;
            param[1] = new SqlParameter("@AST_MD_NAME", SqlDbType.NVarChar);
            param[1].Value = updatedata.AST_MD_NAME;
            param[2] = new SqlParameter("@AST_MD_BRDID", SqlDbType.NVarChar);
            param[2].Value = updatedata.AST_MD_BRDID;
            param[3] = new SqlParameter("@AST_MD_SUBCATID", SqlDbType.NVarChar);
            param[3].Value = updatedata.AST_MD_SUBCATID;
            param[4] = new SqlParameter("@AST_MD_QTYUNIT", SqlDbType.NVarChar);
            param[4].Value = 0;
            param[5] = new SqlParameter("@AST_MD_CATID", SqlDbType.NVarChar);
            param[5].Value = updatedata.AST_MD_CATID;
            param[6] = new SqlParameter("@AST_MD_STAID", SqlDbType.NVarChar);
            param[6].Value = updatedata.AST_MD_STAID;
            param[7] = new SqlParameter("@AST_MD_UPDATEDBY", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["UID"];
            param[8] = new SqlParameter("@AST_MD_UPDATEDDT", SqlDbType.DateTime);
            param[8].Value = (DateTime.Now);
            param[9] = new SqlParameter("@AST_MD_MINQTY", SqlDbType.NVarChar);
            param[9].Value = 0;
            param[10] = new SqlParameter("@AST_MD_STKQTY", SqlDbType.NVarChar);
            param[10].Value = 0;
            param[11] = new SqlParameter("@AST_MD_REM", SqlDbType.NVarChar);
            param[11].Value = updatedata.AST_MD_REM;
            param[12] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[12].Value = HttpContext.Current.Session["COMPANYID"];
            //SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MAS_MODIFY_MODEL", param);
            //return new { Message = MessagesVM.Facility_Modify, data = (object)null };
            object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[MAS_MODIFY_MODEL]", param);
            int sem_id = (int)o;
            string msg;
            if (sem_id == 1)
                msg = "Details are Modified Successfully";
            else
                msg = "Something Went Wrong";
            return new { data = msg, Message = msg };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message };
        }
    }
    public IEnumerable<AssetModelmodel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ASSETMODEL_GRID").GetReader())
        {
            try
            {

                List<AssetModelmodel> sev = new List<AssetModelmodel>();


                while (reader.Read())
                {
                    sev.Add(new AssetModelmodel()
                    {

                        AST_MD_ID = reader.GetValue(0).ToString(),
                        AST_MD_CODE = reader.GetValue(1).ToString(),
                        AST_MD_NAME = reader.GetValue(2).ToString(),
                        VT_CODE = reader.GetValue(3).ToString(),
                        VT_TYPE = reader.GetValue(4).ToString(),
                        AST_SUBCAT_CODE = reader.GetValue(5).ToString(),
                        AST_SUBCAT_NAME = reader.GetValue(6).ToString(),
                        manufactuer_code = reader.GetValue(7).ToString(),
                        manufacturer = reader.GetValue(8).ToString(),
                        AST_MD_STAID = reader.GetValue(9).ToString(),
                        AST_MD_REM = reader.GetValue(10).ToString()

                    });
                }
                reader.Close();
                return sev;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}