﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for AssetReconsilationReportService
/// </summary>
public class AssetReconsilationReportService
{
    public Object GetBarcodeAsset(AstBarCodedata astBar)
    {
        try
        {
            IEnumerable<AssetReconsilationReportModel> asset_Reconciliations = GetAssetDetails(astBar);
            if (asset_Reconciliations.Any())
            {
                return new { Message = MessagesVM.UM_OK, data = asset_Reconciliations };
            }
            else
            {
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }



    public IEnumerable<AssetReconsilationReportModel> GetAssetDetails(AstBarCodedata astBar)
    {
        try
        {
            List<AssetReconsilationReportModel> asset_Reconciliations = new List<AssetReconsilationReportModel>();
            SqlParameter[] parameters = new SqlParameter[5];
            //parameters[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
            //if (astBar.Types == null)
            //{
            //    parameters[0].Value = null;
            //}
            //else
            //{
            //    parameters[0].Value = UtilityService.ConvertToDataTable(astBar.Types);
            //}
            parameters[0] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
            if (astBar.LocationLstAsts == null)
            {
                parameters[0].Value = null;
            }
            else
            {
                parameters[0].Value = UtilityService.ConvertToDataTable(astBar.LocationLstAsts);
            }
            parameters[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            parameters[1].Value = HttpContext.Current.Session["UID"];
            parameters[2] = new SqlParameter("@REQTYPE", SqlDbType.VarChar);
            parameters[2].Value = astBar.ReqOpt;
            ////parameters[4] = new SqlParameter("@RECONDATE", SqlDbType.DateTime);
            ////parameters[4].Value = astBar.ReconDate;
            parameters[3] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            parameters[3].Value = astBar.FromDate;
            parameters[4] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            parameters[4].Value = astBar.ToDate;
            //parameters[6] = new SqlParameter("@ASSET", SqlDbType.Structured);
            //parameters[6].Value = UtilityService.ConvertToDataTable(astBar.AssetData);
            using (SqlDataReader dataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[GET_SCANNED_ASSETS_LINQ]", parameters))
            {
                while (dataReader.Read())
                {
                    AssetReconsilationReportModel _ReconciliationModel = new AssetReconsilationReportModel
                    {
                        Asset_Code = dataReader["Asset_Code"].ToString(),
                        Asset_Name = dataReader["Asset_Name"].ToString(),
                        Location_Name = dataReader["Location_Name"].ToString(),
                        AAT_AAG_CODE = dataReader["AAT_AAG_CODE"].ToString(),
                        SCANNED_BY = dataReader["SCANNED_BY"].ToString(),
                        SCANNED_DT = dataReader["SCANNED_DT"].ToString(),
                        AST_TYPE = dataReader["AST_TYPE"].ToString(),
                        Status = dataReader["Status"].ToString(),
                        DESCRIPTION = dataReader["DESCRIPTION"].ToString()
                    };

                    asset_Reconciliations.Add(_ReconciliationModel);
                }
            }

            var result = asset_Reconciliations.Join(
                         astBar.Types,
                         emp => emp.AAT_AAG_CODE,
                         dept => dept.CAT_CODE,
                         (emp, dept) => emp
                         ).Join(
                         astBar.AssetData,
                         x => x.Asset_Name,
                         y => y.AAT_NAME,
                         (emp, dept) => emp
                         ).ToList();

            return result;
        }
        catch (Exception ex)
        {
            return new List<AssetReconsilationReportModel> { null };
        }
    }
    public IEnumerable<AssetNameRecon> AssetNameRecon(AstBarCodedata astBar)
    {
        List<AssetNameRecon> AssetName = new List<AssetNameRecon>();
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(astBar.Types);
        param[1] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(astBar.LocationLstAsts);
        IDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_ASSETRECON_NAME_LOC", param);
        while (reader.Read())
        {
            AssetName.Add(new AssetNameRecon()
            {
                AAT_NAME = reader.GetString(0),
                AAT_LOC_ID = reader.GetString(1)
            });
        }
        reader.Close();
        return AssetName;
    }
}