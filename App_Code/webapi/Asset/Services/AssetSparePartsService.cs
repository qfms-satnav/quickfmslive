﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using UtiltiyVM;

public class AssetSparePartsService
{

    SubSonic.StoredProcedure sp;
    //DataSet ds;

    public object GetAssetDetails(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS");
            //sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS_LCM");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetVendor(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_GET_SpareVENDORSiteVise");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            sp.Command.AddParameter("@LOC_CODE", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetSpareSub(string name)
    {
        try
        {

            List<EmployeeNameID> emplst = new List<EmployeeNameID>();
            EmployeeNameID emp;
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@NAME", SqlDbType.NVarChar);
            param[0].Value = name;

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AM_USP_GET_ASSETSUBSPAREPARTS", param))
            {
                while (sdr.Read())
                {
                    emp = new EmployeeNameID();
                    emp.NAME = sdr["AAS_SUB_NAME"].ToString();
                    emplst.Add(emp);
                }
            }
            return new { items = emplst, total_count = emplst.Count, incomplete_results = false };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetSpareBrand(string name)
    {
        try
        {

            List<EmployeeNameID> emplst = new List<EmployeeNameID>();
            EmployeeNameID emp;
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@NAME", SqlDbType.NVarChar);
            param[0].Value = name;

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AM_USP_GET_SPAREPARTBRAND", param))
            {
                while (sdr.Read())
                {
                    emp = new EmployeeNameID();
                    emp.NAME = sdr["AAS_AAB_NAME"].ToString();
                    emplst.Add(emp);
                }
            }
            return new { items = emplst, total_count = emplst.Count, incomplete_results = false };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetSpareModel(string name)
    {
        try
        {

            List<EmployeeNameID> emplst = new List<EmployeeNameID>();
            EmployeeNameID emp;
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@NAME", SqlDbType.NVarChar);
            param[0].Value = name;

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AM_USP_GET_SPAREPARTMODEL", param))
            {
                while (sdr.Read())
                {
                    emp = new EmployeeNameID();
                    emp.NAME = sdr["AAS_MODEL_NAME"].ToString();
                    emplst.Add(emp);
                }
            }
            return new { items = emplst, total_count = emplst.Count, incomplete_results = false };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetSpareDataDump(string Location)
    {
        List<SpareDumpData> spareDumpDatas = new List<SpareDumpData>();
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "Get_Spare_Dump_Data");
        sp.Command.Parameters.Add("@Location", Location, DbType.String);
        ds = sp.GetDataSet();
        string filepath = "";
        // filepath = Path.Combine(HttpRuntime.AppDomainAppPath, "SpareExcel\\Sample.xlsx");
        filepath = HostingEnvironment.MapPath("~\\Sample.xlsx");
        bool data = CreateExcelFile.CreateExcelDocument(ds, filepath);
        byte[] bytes = File.ReadAllBytes(filepath);
        string JSONString = string.Empty;
        JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
        return new { Message = data ? "Success" : "Something went wrong.", path = filepath, data = bytes };

    }

    public object GetAccestData(CustomizableRptVM CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_GET_SPAREPARTS");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            //sp.Command.AddParameter("@AAS_AAT_CODE", CustRpt.AAS_AAT_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_AVR_CODE", CustRpt.AAS_AVR_CODE, DbType.String);
            //sp.Command.AddParameter("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
            sp.Command.AddParameter("@ZONE", "South", DbType.String);
            //sp.Command.AddParameter("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
            sp.Command.AddParameter("@LCM_DIVISION", "", DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(CustomizableRptVM CustRpt)
    {
        try
        {
            List<CustomizableRptVM> CustRp = new List<CustomizableRptVM>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_ASSET_SPAREPARTS");
            sp.Command.AddParameter("@AAS_SPAREPART_NAME", CustRpt.AAS_SPAREPART_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_DES", CustRpt.AAS_SPAREPART_DES, DbType.String);
            sp.Command.AddParameter("@AAS_MODEL_NAME", CustRpt.AAS_MODEL_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SUB_NAME", CustRpt.AAS_SUB_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_AVBQUANTITY", CustRpt.AAS_SPAREPART_AVBQUANTITY, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_MINQUANTITY", CustRpt.AAS_SPAREPART_MINQUANTITY, DbType.String);
            sp.Command.AddParameter("@AAS_CTY_CODE", CustRpt.AAS_CTY_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_LOC_ID", CustRpt.AAS_LOC_ID, DbType.String);
            sp.Command.AddParameter("@AAS_AVR_CODE", CustRpt.AAS_AVR_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_TOWER_CODE", CustRpt.AAS_TOWER_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_AAB_NAME", CustRpt.AAS_AAB_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_COST", CustRpt.AAS_SPAREPART_COST, DbType.String);
            sp.Command.AddParameter("@AAS_RACK_NO", CustRpt.AAS_RACK_NO, DbType.String);
            sp.Command.AddParameter("@AAS_VED", CustRpt.AAS_VED, DbType.String);
            sp.Command.AddParameter("@AAS_FSN", CustRpt.AAS_FSN, DbType.String);
            sp.Command.AddParameter("@AAS_AAT_CODE", CustRpt.AAS_AAT_CODE, DbType.String);

            sp.Command.AddParameter("@ASS_LEAD_TIME", CustRpt.ASS_LEAD_TIME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_QUANTITY", CustRpt.AAS_SPAREPART_QUANTITY, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@AAS_REMARKS", CustRpt.AAS_REMARKS, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_NUMBER", CustRpt.AAS_SPAREPART_NUMBER, DbType.String);
            //sp.Command.AddParameter("@AAS_SPAREPART_ID", CustRpt.AAS_SPAREPART_ID, DbType.String);
            //sp.Command.AddParameter("@AAS_ASP_CODE", CustRpt.AAS_ASP_CODE, DbType.String);

            sp.Execute();
            return new { data = CustRpt };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object updateData(CustomizableRptVM CustRpt)
    {
        try
        {
            List<CustomizableRptVM> CustRp = new List<CustomizableRptVM>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_ASSET_SPAREPARTS");
            sp.Command.AddParameter("@AAS_SPAREPART_NAME", CustRpt.AAS_SPAREPART_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_DES", CustRpt.AAS_SPAREPART_DES, DbType.String);
            sp.Command.AddParameter("@AAS_MODEL_NAME", CustRpt.AAS_MODEL_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SUB_NAME", CustRpt.AAS_SUB_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_AVBQUANTITY", CustRpt.AAS_SPAREPART_AVBQUANTITY, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_MINQUANTITY", CustRpt.AAS_SPAREPART_MINQUANTITY, DbType.String);
            sp.Command.AddParameter("@AAS_CTY_CODE", CustRpt.AAS_CTY_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_LOC_ID", CustRpt.AAS_LOC_ID, DbType.String);
            sp.Command.AddParameter("@AAS_AVR_CODE", CustRpt.AAS_AVR_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_TOWER_CODE", CustRpt.AAS_TOWER_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_AAB_NAME", CustRpt.AAS_AAB_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_COST", CustRpt.AAS_SPAREPART_COST, DbType.String);
            sp.Command.AddParameter("@AAS_RACK_NO", CustRpt.AAS_RACK_NO, DbType.String);
            sp.Command.AddParameter("@AAS_VED", CustRpt.AAS_VED, DbType.String);
            sp.Command.AddParameter("@AAS_FSN", CustRpt.AAS_FSN, DbType.String);
            sp.Command.AddParameter("@AAS_AAT_CODE", CustRpt.AAS_AAT_CODE, DbType.String);
            sp.Command.AddParameter("@AAS_ID", CustRpt.AAS_ID, DbType.Int32);
            sp.Command.AddParameter("@ASS_LEAD_TIME", CustRpt.ASS_LEAD_TIME, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_QUANTITY", CustRpt.AAS_SPAREPART_QUANTITY, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@AAS_REMARKS", CustRpt.AAS_REMARKS, DbType.String);
            sp.Command.AddParameter("@AAS_SPAREPART_NUMBER", CustRpt.AAS_SPAREPART_NUMBER, DbType.String);
            sp.Execute();
            return new { data = CustRpt };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }
    public object UploadExcel(HttpRequest httpRequest)
    {
        try
        {
            List<AddSpare> uadmlst = GetDataTableFrmReq(httpRequest);

            String jsonstr = httpRequest.Params["CurrObj"];
            String jsonLoc = httpRequest.Params["CurrLoc"].Split('"')[1];
            var locationWise = uadmlst.Where(a => a.LocationID != jsonLoc.ToString() && a.LocationID != "").ToList();
            if (locationWise.Count > 0)
                return new { Message = "Selected location not match with uploaded Excel", data = (object)null };
            //UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);

            string str1 = "";
            string strFinal = "";
            string strinsert = "";
            string strupdate = "";
            int retval, cnt = 1;
            string str = "";

            var strSql = "Select max(AAS_ID) from " + HttpContext.Current.Session["TENANT"] + ".AMG_AST_SPAREPARTS ";

            var maxId = SqlHelper.ExecuteScalar(CommandType.Text, strSql);

            foreach (AddSpare uadm in uadmlst)
            {
                if (uadm.ID == "0")
                {
                    // ass_id +1 
                    str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + ".AMG_AST_SPAREPARTS " +
                        "(AAS_CTY_CODE " +
                        ",AAS_LOC_ID " +
                        ",AAS_RACK_NO" +
                        ",AAS_SUB_CODE" +
                        ",AAS_SUB_NAME" +
                        ",AAS_AAB_CODE" +
                        ",AAS_AAB_NAME" +
                        ",AAS_AAT_CODE " +
                        ",AAS_AVR_CODE" +
                        ",AAS_SPAREPART_NAME " +
                        ",AAS_SPAREPART_DES" +
                        ",AAS_REMARKS " +
                        ",AAS_SPAREPART_NUMBER " +
                        ",AAS_VED" +
                        ",AAS_SPAREPART_QUANTITY" +
                        ",AAS_SPAREPART_AVBQUANTITY" +
                        ",AAS_SPAREPART_COST" +
                        ",AAS_SPAREPART_MINQUANTITY" +
                        ",ASS_LEAD_TIME" +
                        ",AAS_SPAREPART_ID" +
                        ",AAS_ASP_CODE" +
                        ",AAS_SPAREPART_TOTCOST" +
                        ",AAS_SPAREPART_CREATEDBY" +
                        ",AAS_SPAREPART_CREATEDDATE" +
                        ",COMPANYID" +
                        ",AAS_STA_ID" +
                        ") VALUES ";
                    if ((cnt % 1000) == 0)
                    {
                        str1 = str1.Remove(str1.Length - 1, 1);
                        retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                        if (retval == -1)
                        {
                            return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                        }
                        else
                        {
                            str1 = "";
                            cnt = 1;
                        }
                    }

                    str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}')",
                              uadm.City, uadm.LocationID, uadm.Storage, uadm.SpareType, uadm.SpareType, uadm.Brand, uadm.Brand, uadm.Equipment,
                              uadm.Vendor, uadm.SparePartName, uadm.SparePart_Description, uadm.Remarks, uadm.Part_number, uadm.Priority, uadm.Measure,
                              uadm.Available_Qty, uadm.Item_Cost, uadm.Minimum_Quantity, uadm.Lead_Time,
                              "Spare/" + uadm.SparePartName.Substring(0, Math.Min(4, uadm.SparePartName.Length)) + "/" + maxId + cnt,
                              "Spare/" + uadm.SparePartName.Substring(0, Math.Min(4, uadm.SparePartName.Length)) + "/" + maxId + cnt,
                               (Convert.ToDouble(uadm.Available_Qty) * Convert.ToDouble(uadm.Item_Cost)),
                                HttpContext.Current.Session["Uid"],
                                DateTime.Now, 1, 1

                            );
                    cnt = cnt + 1;
                    strinsert = strinsert + str + str1;
                    str1 = "";
                }
                else if (uadm.ID != "")
                {
                    strupdate = strupdate + " Update " + HttpContext.Current.Session["TENANT"] + ".AMG_AST_SPAREPARTS  Set AAS_CTY_CODE ='" + uadm.City + "', AAS_LOC_ID ='" + uadm.LocationID + "', AAS_RACK_NO ='" + uadm.Storage
                        + "', AAS_SUB_CODE ='" + uadm.SpareType + "', AAS_SUB_NAME ='" + uadm.SpareType + "', AAS_AAB_CODE ='" + uadm.Brand + "', AAS_AAB_NAME ='" + uadm.Brand + "', AAS_AAT_CODE ='" + uadm.Equipment
                        + "', AAS_AVR_CODE ='" + uadm.Vendor + "', AAS_SPAREPART_NAME ='" + uadm.SparePartName + "', AAS_SPAREPART_DES ='" + uadm.SparePart_Description
                        + "', AAS_REMARKS ='" + uadm.Remarks + "', AAS_SPAREPART_NUMBER ='" + uadm.Part_number + "', AAS_VED ='" + uadm.Priority
                        + "', AAS_SPAREPART_QUANTITY ='" + uadm.Measure + "', AAS_SPAREPART_AVBQUANTITY ='" + uadm.Available_Qty + "', AAS_SPAREPART_COST ='" + uadm.Item_Cost
                        + "', AAS_SPAREPART_MINQUANTITY ='" + uadm.Minimum_Quantity + "', ASS_LEAD_TIME ='" + uadm.Lead_Time
                        + "', AAS_SPAREPART_TOTCOST ='" + (Convert.ToDouble(uadm.Available_Qty) * Convert.ToDouble(uadm.Item_Cost))
                        + "', AAS_SPAREPART_CREATEDBY ='" + HttpContext.Current.Session["Uid"] + "', AAS_SPAREPART_CREATEDDATE ='" + DateTime.Now
                        + "' Where AAS_ID = " + uadm.ID;
                }
            }
            //str1 = str1.Remove(str1.Length - 1, 1);
            retval = SqlHelper.ExecuteNonQuery(CommandType.Text, (strinsert + strupdate));
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[0].Value = HttpContext.Current.Session["UID"];
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "ADD_SPARE_DATA", param);
                return new { Message = MessagesVM.UAD_UPLOK, data = dt };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
    public List<AddSpare> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();
        List<AddSpare> spareList = new List<AddSpare>();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);
        // var upls = CreateExcelFile.ReadList(filePath);
        var uplsrr = CreateExcelFile.GetExceldata(filePath);
        //AddSpare addSpare = new AddSpare();
        if (uplsrr.Rows.Count != 0)
        {

            spareList = (from DataRow uplsrr1 in uplsrr.Rows
                         select new AddSpare()
                         {
                             ID = uplsrr1["ID"].ToString(),
                             City = uplsrr1["City"].ToString(),
                             LocationID = uplsrr1["LocationID"].ToString(),
                             Location = uplsrr1["Location"].ToString(),
                             Part_number = uplsrr1["Part_number"].ToString(),
                             SparePartName = uplsrr1["SparePartName"].ToString(),
                             SparePart_Description = uplsrr1["SparePart_Description"].ToString(),
                             Storage = uplsrr1["Storage"].ToString(),
                             SpareType = uplsrr1["SpareType"].ToString(),
                             Brand = uplsrr1["Brand"].ToString(),
                             Equipment = uplsrr1["Equipment"].ToString(),
                             Vendor = uplsrr1["Vendor"].ToString(),
                             Remarks = uplsrr1["Remarks"].ToString(),
                             Priority = uplsrr1["Priority"].ToString(),
                             Available_Qty = uplsrr1["Available_Qty"].ToString(),
                             Measure = uplsrr1["Measure"].ToString(),
                             Item_Cost = uplsrr1["Item_Cost"].ToString(),
                             Minimum_Quantity = uplsrr1["Minimum_Quantity"].ToString(),
                             Lead_Time = uplsrr1["Minimum_Quantity"].ToString()

                         }).ToList();
            return spareList;
        }
        return null;

    }

    public object GetZone()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ZONE");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
}



