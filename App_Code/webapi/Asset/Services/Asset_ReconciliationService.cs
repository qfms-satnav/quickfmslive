﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for Asset_ReconciliationService
/// </summary>
public class Asset_ReconciliationService
{

    public Object GetBarcodeAsset(AstBarCode astBar)
    {
        try
        {
            IEnumerable<Asset_ReconciliationModel> asset_Reconciliations = GetAssetDetails(astBar);
            if (asset_Reconciliations.Any())
            {
                return new { Message = MessagesVM.UM_OK, data = asset_Reconciliations };
            }
            else
            {
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }



    public IEnumerable<Asset_ReconciliationModel> GetAssetDetails(AstBarCode astBar)
    {
        List<Asset_ReconciliationModel> asset_Reconciliations = new List<Asset_ReconciliationModel>();
        SqlParameter[] parameters = new SqlParameter[5];
        parameters[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
        if (astBar.Types == null)
        {
            parameters[0].Value = null;
        }
        else
        {
            parameters[0].Value = UtilityService.ConvertToDataTable(astBar.Types);
        }
        parameters[1] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        if (astBar.LocationLstAsts == null)
        {
            parameters[1].Value = null;
        }
        else
        {
            parameters[1].Value = UtilityService.ConvertToDataTable(astBar.LocationLstAsts);
        }
        parameters[2] = new SqlParameter("@REQTYPE", SqlDbType.VarChar);
        parameters[2].Value = astBar.ReqOpt;
        parameters[3] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        parameters[3].Value = HttpContext.Current.Session["UID"];
        parameters[4] = new SqlParameter("@RECONDATE", SqlDbType.DateTime);
        parameters[4].Value = astBar.ReconDate;
        using (SqlDataReader dataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, astBar.ReqOpt != "2" ? "[BIND_BARCODE_ASSETS]" : "[GET_ASSETS_NOTIN_BARCODE]", parameters))
        {
            while (dataReader.Read())
            {
                Asset_ReconciliationModel _ReconciliationModel = new Asset_ReconciliationModel
                {
                    Asset_Code = dataReader["Asset_Code"].ToString(),
                    Asset_Name = dataReader["Asset_Name"].ToString(),
                    Location_Name = dataReader["Location_Name"].ToString(),
                    Status = dataReader["Status"].ToString(),
                    TAG_NAME = dataReader["ASSET_TYPE"].ToString(),
                };

                asset_Reconciliations.Add(_ReconciliationModel);
            }
        }

        return asset_Reconciliations;
    }

    internal object ReadExcel_Data(HttpRequest httpFile)
    {
        try
        {
            if (httpFile.Files.Count > 0)
            {
                HttpPostedFile Inputfile = null;
                Inputfile = httpFile.Files[0];
                List<ExcelFields> excelFields = ImportExcel<ExcelFields>(Inputfile);
                if (excelFields.Count > 0)
                {
                    return Save_Asset_Reconsolation_Data(excelFields);
                }
                else
                {
                    return new { Message = MessagesVM.PM_NO_REC, data = (object)null };
                }
                
            }
            else
            {
                return new { Message = MessagesVM.UAD_UPLNO, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.Err, data = (object)null };
        }

    }

    private object Save_Asset_Reconsolation_Data(List<ExcelFields> excelFields)
    {
        try
        {
            string str1 = "insert into " + HttpContext.Current.Session["TENANT"] + ".ASSET_BARCODE_DATA (ABD_AST_CODE,ABD_AST_NAME,ABD_LOC_CODE,ABD_AST_STATUS,ABD_CREATED_BY,ABD_CREATED_DATE) values ";
            string str2 = "";
            int retval, cnt = 1;
            foreach (ExcelFields excel in excelFields)
            {
                if ((cnt % 1000) == 0)
                {
                    str2 = str2.Remove(str2.Length - 1, 1);
                    retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str1 + str2);
                    if (retval == -1)
                    {
                        return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                    }
                    else
                    {
                        str2 = "";
                        cnt = 1;
                    }

                }
                str2 += string.Format("('{0}','{1}','{2}','{3}','{4}','{5}'),", excel.Asset_Code, excel.Asset_Name, excel.Location, 1, HttpContext.Current.Session["UID"], DateTime.Now);
                cnt++;
            }
            str2 = str2.Remove(str2.Length - 1, 1);
            retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str1 + str2);
            //if (retval == -1)
                return new { Message = MessagesVM.UAD_UPLOK, data = (object)null };
            
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        
    }

    public List<T> ImportExcel<T>(HttpPostedFile httpPostedFile)
    {
        List<T> list = new List<T>();
        Type typeOfObject = typeof(T);
        using (IXLWorkbook workbook = new XLWorkbook(httpPostedFile.InputStream))
        {
            IXLWorksheet worksheet = workbook.Worksheet(1);
            var properties = typeOfObject.GetProperties();
            var columns = worksheet.FirstRow().Cells().Select((v, i) => new { Value = v.Value, Index = i + 1 }); //indexing foreach(IXLRow row in worksheet. RowsUsed (). Skip (1))//Skip first row which is used for column header texts
            if (!properties.Select(p => p.Name).SequenceEqual(columns.Select(c => c.Value.ToString())))
            {
                // Handle the mismatch (throw exception, log, or take appropriate action)
                throw new Exception("Header mismatch:");
            }
            foreach (IXLRow row in worksheet.RowsUsed().Skip(1))
            {
                T obj = (T)Activator.CreateInstance(typeOfObject);
                foreach (var prop in properties)
                {
                    int colIndex = columns.SingleOrDefault(c => c.Value.ToString() == prop.Name.ToString()).Index; 
                    var val = row.Cell(colIndex).Value; 
                    var type = prop.PropertyType;
                    prop.SetValue(obj, Convert.ChangeType(val, type));
                }
                list.Add(obj);
            }
        }
        return list;
    }
}
