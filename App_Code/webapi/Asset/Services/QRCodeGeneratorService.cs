﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using System.Drawing;
using System.IO;
using QRCoder;

public class QRCodeGeneratorService
{
    SubSonic.StoredProcedure sp;
    List<QRCodeGeneratorModel> QRCodeLst;
    QRCodeGeneratorModel QRCode;
    DataSet ds;
    static byte[] byteImage;
    public object GetQRCodeDetails(QRCodeGeneratorModel data)
    {

        QRCodeLst = new List<QRCodeGeneratorModel>();
        try
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];
            //param[1] = new SqlParameter("@Location", SqlDbType.NVarChar);
            //param[1].Value = data.LCM_CODE;
            param[1] = new SqlParameter("@Location", SqlDbType.Structured);
            if (data.loclst == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(data.loclst);
            }
            param[2] = new SqlParameter("@CompanyId", SqlDbType.NVarChar);
            param[2].Value =data.CompanyId;
            param[3] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            param[3].Value = data.FromDate;
            param[4] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            param[4].Value = data.ToDate;
            param[5] = new SqlParameter("@ASSET", SqlDbType.Structured);
            param[5].Value = ConvertToDataTable(data.AssetData);
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "ASSET_QR_CODE_READER", param);
            return new { griddata = ds.Tables[0]};
          
        }
        catch(Exception ex)
        {
            return QRCodeLst;
        }
    }
    public IEnumerable<assetname> AssetName(AstBarCodedata astBar)
    {
        List<assetname> AssetName = new List<assetname>();
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(astBar.LocationLstAsts);
        IDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_ASSET_NAME_LOC", param);
        while (reader.Read())
        {
            AssetName.Add(new assetname()
            {
                AAT_NAME = reader.GetString(0),
                AAT_LOC_ID = reader.GetString(1)
            });
        }
        reader.Close();
        return AssetName;
    }

    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }
}