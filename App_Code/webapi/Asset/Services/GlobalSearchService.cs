﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for GlobalSearchService
/// </summary>
public class GlobalSearchService
{

    List<GlobalSearchModel> GSLst;
    public object GetData(GlobalSearchModel data)
    {

        GSLst = new List<GlobalSearchModel>();
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            //param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            //param[0].Value = HttpContext.Current.Session["UID"];
            param[0] = new SqlParameter("@Location", SqlDbType.NVarChar);
            param[0].Value = data.LCM_CODE;
            param[1] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[1].Value = data.PageNumber;
            param[2] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[2].Value = data.PageSize;
            param[3] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[3].Value = data.SearchVal;
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GLOBAL_SEARCH", param);
            return new { griddata = ds.Tables[0] };
        }
        catch (Exception ex)
        {
            return GSLst;
        }
    }
}