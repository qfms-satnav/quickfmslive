﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for AssetHistoryService
/// </summary>
public class AssetHistoryService
{
    SubSonic.StoredProcedure sp;
    //public object GetAssetHistory()
    //{
        
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
    //        CustomizedGridCols gridcolscls;
    //        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "AssetHistoryReport");
    //        List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
    //                               select dc.ColumnName).ToList();
    //        foreach (string col in Colstr)
    //        {
    //            gridcolscls = new CustomizedGridCols();
    //            gridcolscls.cellClass = "grid-align";
    //            gridcolscls.field = col;
    //            gridcolscls.headerName = col;
    //            gridcols.Add(gridcolscls);
    //        }
    //        return new { griddata = ds.Tables[0], Coldef = gridcols };
    //    }
    //    catch (SqlException) { throw; }

    //}

    private DataTable GetAssetHistoryDetails()
    {
        DataTable DT = new DataTable();
        try
        {
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "AssetHistoryReport");
            HttpContext.Current.Session["AssetHistory"] = DT;
            return DT;
        }
        catch (Exception ex)
        {
            return DT;
        }
    }

    public object GetStatus(AssetHistoryModel ASHM)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ASSET_HISTORY_STATUS_VALUES");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object SearchData(AssetHistoryModel ASHM)
    {
        try
        {

            DataSet ds = new DataSet();
            //DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@Statuslist", SqlDbType.NVarChar);
            param[0].Value = ASHM.STlist;
            param[1] = new SqlParameter("@FromDate", SqlDbType.NVarChar);
            param[1].Value = ASHM.FromDate;
            param[2] = new SqlParameter("@ToDate", SqlDbType.NVarChar);
            param[2].Value = ASHM.ToDate;

            // sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[ASSET_HISTORY_REPORT_BASED_ON_STATUS]");

            //ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "AssetHistoryReport");

            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "ASSET_HISTORY_REPORT_BASED_ON_STATUS", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (SqlException) { throw; }
    }



}