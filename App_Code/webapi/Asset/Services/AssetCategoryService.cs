﻿using DocumentFormat.OpenXml.Drawing.Charts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
//using static System.Windows.Forms.VisualStyles.VisualStyleElement.TreeView;


/// <summary>
/// Summary description for AssetCategoryService
/// </summary>
public class AssetCategoryService
{
    SubSonic.StoredProcedure sp;

    public object SaveAssetCategory(AssetCategoryModel data)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[10];

            param[0] = new SqlParameter("@VT_CODE", SqlDbType.NVarChar);
            param[0].Value = data.VT_CODE;

            param[1] = new SqlParameter("@VT_TYPE", SqlDbType.NVarChar);
            param[1].Value = data.VT_TYPE;

            param[2] = new SqlParameter("@VT_STATUS", SqlDbType.Int);
            param[2].Value = data.VT_STATUS;

            param[3] = new SqlParameter("@VT_CREATED_BY", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];

            param[4] = new SqlParameter("@VT_CONS_STATUS", SqlDbType.Int);
            param[4].Value = data.VT_CONS_STATUS;

            param[5] = new SqlParameter("@VT_REM", SqlDbType.NVarChar);
            param[5].Value = data.VT_REM;

            param[6] = new SqlParameter("@VT_UPLOADED_DOC", SqlDbType.NVarChar);
            param[6].Value = data.VT_UPLOADED_DOC;

            param[7] = new SqlParameter("@VT_DEPR", SqlDbType.VarChar);
            param[7].Value = data.VT_DEPR;

            param[8] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[8].Value = HttpContext.Current.Session["COMPANYID"];
            param[9] = new SqlParameter("@TYPE_ID", SqlDbType.VarChar);
            param[9].Value = data.AST_TYPE_ID;


            object result = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[INSERT_ASSETCATEGORY]", param);
            int resultCode = Convert.ToInt32(result);
            string msg;


            if (resultCode == 1)
                msg = "New Asset Category Successfully Added.";
            else if (resultCode == 0)
                msg = "Asset Category already exists.";
            else
                msg = "Something Went Wrong";


            return new { data = data, Message = msg };
        }
        catch (Exception ex)
        {

            return new { data = (object)null, Message = ex.Message };
        }
    }

    public object ModifyAssetCategories(AssetCategoryModel updatedata)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@VT_CODE", SqlDbType.NVarChar);
            param[0].Value = updatedata.VT_CODE;
            param[1] = new SqlParameter("@VT_TYPE", SqlDbType.NVarChar);
            param[1].Value = updatedata.VT_TYPE;
            param[2] = new SqlParameter("@VT_CONS_STATUS", SqlDbType.NVarChar);
            param[2].Value = updatedata.VT_CONS_STATUS;
            param[3] = new SqlParameter("@VT_STATUS", SqlDbType.NVarChar);
            param[3].Value = updatedata.VT_STATUS;
            param[4] = new SqlParameter("@VT_REM", SqlDbType.NVarChar);
            param[4].Value = updatedata.VT_REM;
            param[5] = new SqlParameter("@VT_DEPR", SqlDbType.NVarChar);
            param[5].Value = updatedata.VT_DEPR;
            param[6] = new SqlParameter("@VT_MODIFIED_BY", SqlDbType.NVarChar);
            param[6].Value = HttpContext.Current.Session["UID"].ToString();
            param[7] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MODIFY_ASSETCATEGORY", param);
            return new { Message = MessagesVM.Facility_Modify, data = (object)null };
        }
        catch (Exception x)
        {
            string y = x.Message;
        }
        return new { Message = MessagesVM.Facility_Modify, data = (object)null };
    }
    public IEnumerable<AssetCategoryModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_ASSETCATEGORIES(NEW)").GetReader())
        {
            try
            {

                List<AssetCategoryModel> sev = new List<AssetCategoryModel>();


                while (reader.Read())
                {
                    sev.Add(new AssetCategoryModel()
                    {

                        VT_CODE = reader.GetValue(0).ToString(),
                        VT_TYPE = reader.GetValue(1).ToString(),
                        ASSET_TYPE = reader.GetValue(2).ToString(),
                        VT_STATUS = reader.GetValue(3).ToString(),
                        VT_DEPR = reader.GetValue(4).ToString(),
                        //VT_CONS_STATUS = reader.GetValue(5).ToString(),
                        VT_REM = reader.GetValue(5).ToString()
                    });
                }
                reader.Close();
                return sev;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

    public IEnumerable<AssetType> AssetTypes()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[AM_USP_GET_ASSETTYPES]").GetReader();
        List<AssetType> AssetTypes = new List<AssetType>();
        while (reader.Read())
        {
            AssetTypes.Add(new AssetType()
            {
                TYPE_ID = reader.GetValue(0).ToString(),
                ASSET_TYPE_NAME = reader.GetValue(1).ToString(),
            });
        }
        reader.Close();
        return AssetTypes;
    }
}