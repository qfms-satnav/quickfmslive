﻿using DocumentFormat.OpenXml.Drawing.Charts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for AssetBrandService
/// </summary>
public class AssetBrandService
{
    SubSonic.StoredProcedure sp;
    //DataSet ds;

    public IEnumerable<assetCategory> AssetMainCategory()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[AM_USP_GET_ASSETCATEGORIES_BRAND]").GetReader();
        List<assetCategory> AssetMainCategoryList = new List<assetCategory>();
        while (reader.Read())
        {
            AssetMainCategoryList.Add(new assetCategory()
            {
                VT_CODE = reader.GetValue(0).ToString(),
                VT_TYPE = reader.GetValue(1).ToString()

                //ticked = false

            });
        }
        reader.Close();
        return AssetMainCategoryList;
    }
    public IEnumerable<assetSubCategory> AssetSubCategory()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_ASSETSUBCATBYASSET]").GetReader();
        List<assetSubCategory> AssetSUbCategoryList = new List<assetSubCategory>();
        while (reader.Read())
        {
            AssetSUbCategoryList.Add(new assetSubCategory()
            {
                AST_SUBCAT_CODE = reader.GetValue(0).ToString(),
                AST_SUBCAT_NAME = reader.GetValue(1).ToString(),
                VT_CODE = reader.GetValue(2).ToString()
                //Assetmaincatcode = reader.GetValue(3).ToString(),
                //ticked = false


            });
        }
        reader.Close();
        return AssetSUbCategoryList;
    }
    public object SaveAssetBrand(AssetBrandModel data)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[8];

            param[0] = new SqlParameter("@manufactuer_code", SqlDbType.NVarChar);
            param[0].Value = data.manufactuer_code;

            param[1] = new SqlParameter("@manufacturer", SqlDbType.NVarChar);
            param[1].Value = data.manufacturer;

            param[2] = new SqlParameter("@MANUFACTUER_TYPE_CODE", SqlDbType.NVarChar);
            param[2].Value = data.MANUFACTUER_TYPE_CODE;

            param[3] = new SqlParameter("@createdBy", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];

            param[4] = new SqlParameter("@MANUFACTUER_STATUS", SqlDbType.Int);
            param[4].Value = data.MANUFACTUER_STATUS;

            param[5] = new SqlParameter("@manufacturer_type_subcode", SqlDbType.NVarChar);
            param[5].Value = data.manufacturer_type_subcode;

            param[6] = new SqlParameter("@MANU_REM", SqlDbType.NVarChar);
            param[6].Value = data.MANU_REM;

            param[7] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            object result = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[INSERT_ASSETBRAND]", param);
            int resultCode = Convert.ToInt32(result);
            string msg;


            if (resultCode == 1)
                msg = "New Asset Brand Successfully Added.";
            else if (resultCode == 0)
                msg = "Asset Brand already exists.";
            else
                msg = "Something Went Wrong";


            return new { data = data, Message = msg };
        }
        catch (Exception ex)
        {

            return new { data = (object)null, Message = ex.Message };
        }
    }

    public object ModifyAssetBrand(AssetBrandModel updatedata)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@manufactuer_code", SqlDbType.NVarChar);
            param[0].Value = updatedata.manufactuer_code;
            param[1] = new SqlParameter("@manufacturer", SqlDbType.NVarChar);
            param[1].Value = updatedata.manufacturer;
            param[2] = new SqlParameter("@MANUFACTUER_TYPE_CODE", SqlDbType.NVarChar);
            param[2].Value = updatedata.MANUFACTUER_TYPE_CODE;
            param[3] = new SqlParameter("@manufacturer_type_subcode", SqlDbType.NVarChar);
            param[3].Value = updatedata.manufacturer_type_subcode;
            param[4] = new SqlParameter("@MANUFACTUER_STATUS", SqlDbType.NVarChar);
            param[4].Value = updatedata.MANUFACTUER_STATUS;
            param[5] = new SqlParameter("@MANU_REM", SqlDbType.NVarChar);
            param[5].Value = updatedata.MANU_REM;
            param[6] = new SqlParameter("@modifiedBy", SqlDbType.NVarChar);
            param[6].Value = HttpContext.Current.Session["UID"].ToString();
            param[7] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MODIFY_ASSETBRAND", param);
            return new { Message = MessagesVM.Facility_Modify, data = (object)null };
        }
        catch (Exception x)
        {
            string y = x.Message;
        }
        return new { Message = MessagesVM.Facility_Modify, data = (object)null };
    }
    public IEnumerable<AssetBrandModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_ASSETBRAND_NEW").GetReader())
        {
            try
            {

                List<AssetBrandModel> sev = new List<AssetBrandModel>();


                while (reader.Read())
                {
                    sev.Add(new AssetBrandModel()
                    {

                        manufactuer_code = reader.GetValue(0).ToString(),
                        manufacturerID = reader.GetValue(1).ToString(),
                        manufacturer = reader.GetValue(2).ToString(),
                        MANUFACTURER_STATUS = reader.GetValue(3).ToString(),
                        MANUFACTURER_TYPE_CODE = reader.GetValue(4).ToString(),
                        AST_SUBCAT_NAME = reader.GetValue(5).ToString(),
                        VT_TYPE = reader.GetValue(6).ToString(),
                        AST_SUBCAT_CODE = reader.GetValue(7).ToString(),
                        MANU_REM = reader.GetValue(8).ToString()
                    });
                }
                reader.Close();
                return sev;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}