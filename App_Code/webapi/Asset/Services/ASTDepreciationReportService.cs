﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for ASTDepreciationReportService
/// </summary>
public class ASTDepreciationReportService
{
    SubSonic.StoredProcedure sp;
    List<EquipmentCustomizedData> Cust;
    EquipmentCustomizedData Ecustm;
    DataSet ds;

    public object GetDepreciationObject(EquipmentVM Equipment)
    {
        try
        {
            Cust = GetDepreciationDetails(Equipment);
            if (Cust.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Cust };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<EquipmentCustomizedData> GetDepreciationDetails(EquipmentVM Equipment)
    {
        try
        {
            List<EquipmentCustomizedData> ECData = new List<EquipmentCustomizedData>();

            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
            if (Equipment.Categorylst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Equipment.Categorylst);
            }

            param[1] = new SqlParameter("@BRND_LIST", SqlDbType.Structured);
            if (Equipment.Brandlst == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(Equipment.Brandlst);
            }

            param[2] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
            if (Equipment.Modellst == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(Equipment.Modellst);
            }

            param[3] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
            if (Equipment.loclst == null)
            {
                param[3].Value = null;
            }
            else
            {
                param[3].Value = UtilityService.ConvertToDataTable(Equipment.loclst);
            }

            param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            param[5] = new SqlParameter("@SUB_CAT_LIST", SqlDbType.Structured);
            if (Equipment.Subcatlst == null)
            {
                param[5].Value = null;
            }
            else
            {
                param[5].Value = UtilityService.ConvertToDataTable(Equipment.Subcatlst);
            }
            param[6] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[6].Value = Equipment.Request_Type;
            if (Equipment.CNP_NAME == null)
            {
                param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
                param[7].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[7] = new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar);
                param[7].Value = Equipment.CNP_NAME;
            }
            //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "GET_EQUIPMENT_CUSTOMIZED_DATA", param);


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AM_GET_ASSET_CUSTOMIZED_DATA", param))
            {
                while (reader.Read())
                {
                    Ecustm = new EquipmentCustomizedData();

                    Ecustm.AAT_NAME = reader["AAT_NAME"].ToString();
                    Ecustm.VT_TYPE = reader["VT_TYPE"].ToString();
                    Ecustm.MANUFACTURER = reader["MANUFACTURER"].ToString();
                    Ecustm.AST_MD_NAME = reader["AAT_MODEL_NAME"].ToString();
                    Ecustm.AST_SUBCAT_NAME = reader["AST_SUBCAT_NAME"].ToString();
                    DateTime? dt = null;
                    Ecustm.PURCHASEDDATE = reader["PURCHASEDDATE"] is DBNull ? dt : Convert.ToDateTime(reader["PURCHASEDDATE"]);
                    //Ecustm.AAT_AMC_DATE = reader["AAT_AMC_DATE"] == null ? (DateTime)reader["AAT_AMC_DATE"] : DateTime.Now;
                    Ecustm.AAT_WRNTY_DATE = reader["AAT_WRNTY_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AAT_WRNTY_DATE"]);
                    //Ecustm.AAT_DEPRICIATION = reader["AAT_DEPRICIATION"].ToString();
                    //Ecustm.AAT_PO_NUMBER = reader["AAT_PO_NUMBER"].ToString();
                    Ecustm.TOTALMONTHS = reader["TOTALMONTHS"].ToString();
                    //Ecustm.AAT_AST_SLVGVAL = reader["AAT_AST_SLVGVAL"] == null ? (float)reader["AAT_AST_SLVGVAL"] :0;
                    Ecustm.AAT_AST_COST = reader["AAT_AST_COST"] == null ? (float)reader["AAT_AST_COST"] : 0;
                    Ecustm.STATUS = reader["STATUS"].ToString();
                    Ecustm.MAPPEDTO = reader["MAPPEDTO"].ToString();
                    Ecustm.MOVEDFROMLOCATION = reader["MOVEDFROMLOCATION"].ToString();
                    Ecustm.MOVEDTOLOCATION = reader["MOVEDTOLOCATION"].ToString();
                    Ecustm.RAISEDBY = reader["RAISEDBY"].ToString();
                    Ecustm.INTERRECEIVEDBY = reader["INTERRECEIVEDBY"].ToString();
                    //Ecustm.AAT_AST_MOT_HP = reader["AAT_AST_MOT_HP"].ToString();
                    Ecustm.AVR_NAME = reader["AVR_NAME"].ToString();
                    Ecustm.AAT_LOC_ID = reader["AAT_LOC_ID"].ToString();
                    Ecustm.AAT_CODE = reader["AAT_CODE"].ToString();
                    Ecustm.Asset_count = (int)reader["Asset_count"];
                    Ecustm.LCM_NAME = reader["LCM_NAME"].ToString();
                    //Ecustm.AAT_AST_SERIALNO = reader["AAT_AST_SERIALNO"].ToString();
                    Ecustm.Asset_Age = reader["Asset_Age"].ToString();
                    Ecustm.DISPOSAL_STATUS = reader["DISPOSAL_STATUS"].ToString();
                    Ecustm.AAT_MFG_DATE = reader["AAT_MFG_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AAT_MFG_DATE"]);


                    //Ecustm.PURCHASEDDATE = reader["PURCHASEDDATE"] == null ? (DateTime)reader["PURCHASEDDATE"] : DateTime.Now;
                    //Ecustm.AAT_AMC_DATE = reader["AAT_AMC_DATE"] == null ? (DateTime)reader["AAT_AMC_DATE"] : DateTime.Now;
                    ECData.Add(Ecustm);

                }
                reader.Close();
            }
            return ECData;
        }
        catch
        {
            throw;
        }
    }
}