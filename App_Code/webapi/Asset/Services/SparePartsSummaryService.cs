﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SparePartsSummaryService
/// </summary>
public class SparePartsSummaryService
{
    SubSonic.StoredProcedure sp;
    public object GetData()
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AMG_AST_SPAREPARTS");
            //sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    //public object GetAssetDetails(CustomizableRptVM CustRpt)
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS");
    //        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
    //        ds = sp.GetDataSet();
    //        if (ds.Tables.Count != 0)
    //            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
    //        else
    //            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    //    }
    //    catch (Exception e)
    //    {
    //        throw e;
    //    }

    //}
    public object GetAssetDetails(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS_LCM");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object SearchData(CustomizableRpt CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[Search_SpareParts]");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            sp.Command.AddParameter("@AAS_AAT_CODE", CustRpt.SUBC_NAME, DbType.String);
            sp.Command.AddParameter("@FromDate", CustRpt.FromDate, DbType.String);
            sp.Command.AddParameter("@ToDate", CustRpt.ToDate, DbType.String);
            sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@LCM_ZONE", HttpContext.Current.Session["Zone"], DbType.String);
            sp.Command.Parameters.Add("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data2 = ds.Tables[1] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public object SearchToTInventoryToTCost(CustomizableRpt CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[Search_ToTInventoryToTCost]");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            sp.Command.Parameters.Add("@USERID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
            sp.Command.Parameters.Add("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public object SearchToTInventoryToTCostReport(CustomizableRpt CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[Search_ToTInventoryToTCostReport]");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            sp.Command.Parameters.Add("@USERID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
            sp.Command.Parameters.Add("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}