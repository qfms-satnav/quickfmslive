﻿
using ClosedXML.Excel;
using Microsoft.VisualStudio.Shell.Interop;
using Newtonsoft.Json;
using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Web;
using System.Web.Http.Routing;
using UtiltiyVM;




/// <summary>
/// <summary>
/// Summary description for AddAssetInventoryService
/// </summary>
public class AddAssetInventoryService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public IEnumerable<AstMainCategory> AssetMainCategory()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSET_CATEGORY]").GetReader();
        List<AstMainCategory> AssetMainCategoryList = new List<AstMainCategory>();
        while (reader.Read())
        {
            AssetMainCategoryList.Add(new AstMainCategory()
            {
                Assetmaincatname = reader.GetValue(0).ToString(),
                Assetmaincatcode = reader.GetValue(1).ToString(),
                ticked = false

            });
        }
        reader.Close();
        return AssetMainCategoryList;
    }
    public IEnumerable<AstSubCategory> AssetSubCategory()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSETSUB_CATEGORY]").GetReader();
        List<AstSubCategory> AssetSUbCategoryList = new List<AstSubCategory>();
        while (reader.Read())
        {
            AssetSUbCategoryList.Add(new AstSubCategory()
            {
                Assetsubncatcode = reader.GetValue(0).ToString(),
                Assetsubncatname = reader.GetValue(1).ToString(),
                Assetmaincatname = reader.GetValue(2).ToString(),
                Assetmaincatcode = reader.GetValue(3).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssetSUbCategoryList;
    }
    public IEnumerable<AstBrand> AssetBrand()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSET_BRAND]").GetReader();
        List<AstBrand> AssetBrandList = new List<AstBrand>();
        while (reader.Read())
        {
            AssetBrandList.Add(new AstBrand()
            {
                BrandCode = reader.GetValue(0).ToString(),
                BrandName = reader.GetValue(1).ToString(),
                Assetsubncatcode = reader.GetValue(2).ToString(),
                Assetsubncatname = reader.GetValue(3).ToString(),
                Assetcatcode = reader.GetValue(4).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssetBrandList;
    }
    public IEnumerable<AstModel> AssetModel()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSET_Model]").GetReader();
        List<AstModel> AssetModelList = new List<AstModel>();
        while (reader.Read())
        {
            AssetModelList.Add(new AstModel()
            {
                MdCode = reader.GetValue(0).ToString(),
                MdName = reader.GetValue(1).ToString(),
                MdBrandCode = reader.GetValue(2).ToString(),
                Mdsubncatcode = reader.GetValue(3).ToString(),
                Mdcatcode = reader.GetValue(4).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssetModelList;
    }
    public IEnumerable<Costcenter> GetCostcenter()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[Bind_costcenter]").GetReader();
        List<Costcenter> CCList = new List<Costcenter>();
        while (reader.Read())
        {
            CCList.Add(new Costcenter()
            {
                Cost_Center_Code = reader.GetValue(0).ToString(),
                Cost_Center_Name = reader.GetValue(1).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return CCList;
    }


    public IEnumerable<AstlOCATION> AssetLOCATION()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSET_LOCATION]").GetReader();
        List<AstlOCATION> AssetLOCList = new List<AstlOCATION>();
        while (reader.Read())
        {
            AssetLOCList.Add(new AstlOCATION()
            {
                lCMCODE = reader.GetValue(0).ToString(),
                LCMNAME = reader.GetValue(1).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssetLOCList;
    }
    public IEnumerable<Astltower> Assettower()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSET_TOWER]").GetReader();
        List<Astltower> AssettowList = new List<Astltower>();
        while (reader.Read())
        {
            AssettowList.Add(new Astltower()
            {
                twr_CODE = reader.GetValue(0).ToString(),
                twr_name = reader.GetValue(1).ToString(),
                lCMCODE = reader.GetValue(2).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssettowList;
    }
    public IEnumerable<AstlFloor> Assetfloor()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSET_FLOOR]").GetReader();
        List<AstlFloor> AssetFloorList = new List<AstlFloor>();
        while (reader.Read())
        {
            AssetFloorList.Add(new AstlFloor()
            {
                flr_code = reader.GetValue(0).ToString(),
                flr_name = reader.GetValue(1).ToString(),
                twr_CODE = reader.GetValue(2).ToString(),
                lCM_CODE = reader.GetValue(3).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssetFloorList;
    }
    public IEnumerable<AstVendor> AssetVendor()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_ASSET_VENDOR]").GetReader();
        List<AstVendor> AssetVendorList = new List<AstVendor>();
        while (reader.Read())
        {
            AssetVendorList.Add(new AstVendor()
            {
                vendorCode = reader.GetValue(0).ToString(),
                vendorname = reader.GetValue(1).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssetVendorList;
    }
    public IEnumerable<Asttype> Assettype()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[USP_GET_ASSET_TYPES]").GetReader();
        List<Asttype> AssettypeList = new List<Asttype>();
        while (reader.Read())
        {
            AssettypeList.Add(new Asttype()
            {
                Assettype = reader.GetValue(0).ToString(),
                Assetname = reader.GetValue(1).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssettypeList;
    }
    public IEnumerable<Astcompany> GetCompany()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_COMPANYS]").GetReader();
        List<Astcompany> AssetcompanyList = new List<Astcompany>();
        while (reader.Read())
        {
            AssetcompanyList.Add(new Astcompany()
            {
                AssettCompany = reader.GetValue(0).ToString(),
                ticked = false


            });
        }
        reader.Close();
        return AssetcompanyList;
    }
    public List<T> ImportExcel<T>(HttpPostedFile httpPostedFile)
    {
        List<T> list = new List<T>();
        Type typeOfObject = typeof(T);
        using (IXLWorkbook workbook = new XLWorkbook(httpPostedFile.InputStream))
        {
            IXLWorksheet worksheet = workbook.Worksheet(1);
            var properties = typeOfObject.GetProperties();
            var columns = worksheet.FirstRow().Cells().Select((v, i) => new { Value = v.Value, Index = i + 1 }); //indexing foreach(IXLRow row in worksheet. RowsUsed (). Skip (1))//Skip first row which is used for column header texts
            if (!properties.Select(p => p.Name).SequenceEqual(columns.Select(c => c.Value.ToString())))
            {
                // Handle the mismatch (throw exception, log, or take appropriate action)
                throw new Exception("Header mismatch:");
            }
            foreach (IXLRow row in worksheet.RowsUsed().Skip(1))
            {
                T obj = (T)Activator.CreateInstance(typeOfObject);
                foreach (var prop in properties)
                {
                    int colIndex = columns.SingleOrDefault(c => c.Value.ToString() == prop.Name.ToString()).Index;
                    var val = row.Cell(colIndex).Value;
                    var type = prop.PropertyType;
                    prop.SetValue(obj, Convert.ChangeType(val, type));
                }
                list.Add(obj);
            }
        }
        return list;
    }





    //internal object ReadExcel_Data(HttpRequest httpFile)
    //{
    //    try
    //    {
    //        if (httpFile.Files.Count > 0)
    //        {
    //            HttpPostedFile Inputfile = null;
    //            Inputfile = httpFile.Files[0];
    //            List<excelbulkupload> excelbulkupload = ImportExcel<excelbulkupload>(Inputfile);
    //            if (excelbulkupload.Count > 0)
    //            {
    //                return Save_Asset_Data(excelbulkupload);
    //            }
    //            else
    //            {
    //                return new { Message = MessagesVM.PM_NO_REC, data = (object)null };
    //            }

    //        }
    //        else
    //        {
    //            return new { Message = MessagesVM.UAD_UPLNO, data = (object)null };
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Message = MessagesVM.Err, data = (object)null };
    //    }

    //}



    public object Submit(ASTDETAILS data)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[43];


            param[0] = new SqlParameter("@AAT_CODE", SqlDbType.NVarChar);
            param[0].Value = data.AAT_CODE;
            param[1] = new SqlParameter("@AAT_LOC_ID", SqlDbType.NVarChar);
            param[1].Value = data.AST_LOC;
            param[2] = new SqlParameter("@AAT_MODEL_ID", SqlDbType.NVarChar);
            param[2].Value = data.AST_MODEL_NAME;
            param[3] = new SqlParameter("@AAT_MODEL_NAME", SqlDbType.NVarChar);
            param[3].Value = data.AST_MODEL;
            param[4] = new SqlParameter("@AAT_SUB_CODE", SqlDbType.NVarChar);
            param[4].Value = data.AST_SUB_CAT;
            param[5] = new SqlParameter("@AAT_AVR_CODE", SqlDbType.NVarChar);
            param[5].Value = data.AST_VENDOR;
            param[6] = new SqlParameter("@AAT_AAB_CODE", SqlDbType.NVarChar);
            param[6].Value = data.AST_BRD;
            param[7] = new SqlParameter("@AAT_AMC_REQD", SqlDbType.Int);
            param[7].Value = data.AAT_AMT_INC;
            param[8] = new SqlParameter("@AAT_AMC_DATE", SqlDbType.DateTime);
            param[8].Value = data.AAT_AMT_DT;
            param[9] = new SqlParameter("@AAT_AMC_DOCPATH", SqlDbType.NVarChar);
            param[9].Value = data.AAT_AMC_DOC;
            param[10] = new SqlParameter("@AAT_INS_REQD", SqlDbType.Int);
            param[10].Value = data.AAT_INS_INC;
            param[11] = new SqlParameter("@AAT_INS_DATE", SqlDbType.DateTime);
            param[11].Value = data.AAT_INS_DT;
            param[12] = new SqlParameter("@AAT_AAG_CODE", SqlDbType.NVarChar);
            param[12].Value = data.AST_CAT;
            param[13] = new SqlParameter("@AAT_INS_DOCPATH", SqlDbType.NVarChar);
            param[13].Value = data.AAT_INC_DOC;
            param[14] = new SqlParameter("@AAT_AST_COST", SqlDbType.Decimal);
            param[14].Value = Convert.ToDouble(data.AAT_COST);

            param[15] = new SqlParameter("@AAT_AST_SERIALNO", SqlDbType.NVarChar);
            param[15].Value = data.AAT_SNO;
            param[16] = new SqlParameter("@AAT_UPT_DT", SqlDbType.DateTime);
            param[16].Value = (DateTime.Now);
            param[17] = new SqlParameter("@AAT_UPT_BY", SqlDbType.NVarChar);
            param[17].Value = HttpContext.Current.Session["UID"];
            param[18] = new SqlParameter("@AAT_STA_ID", SqlDbType.NVarChar);
            param[18].Value = data.AAT_STATUS;
            param[19] = new SqlParameter("@AAT_OWNED", SqlDbType.NVarChar);
            param[19].Value = 1;
            param[20] = new SqlParameter("@AAT_PURCHASED_STATUS", SqlDbType.NVarChar);
            param[20].Value = 1;
            param[21] = new SqlParameter("@AAT_SPC_FIXED", SqlDbType.NVarChar);
            param[21].Value = 1;
            param[22] = new SqlParameter("@AAT_USR_MOVABLE", SqlDbType.NVarChar);
            param[22].Value = 0;
            param[23] = new SqlParameter("@AAT_AST_CONS", SqlDbType.NVarChar);
            param[23].Value = 0;
            param[24] = new SqlParameter("@AAT_RATE_CONTRACT", SqlDbType.NVarChar);
            param[24].Value = data.AST_RT_CNT;
            param[25] = new SqlParameter("@AAT_DESC", SqlDbType.NVarChar);
            param[25].Value = data.AAT_NAME;
            param[26] = new SqlParameter("@CONF_TYPE", SqlDbType.NVarChar);
            param[26].Value = "";
            param[27] = new SqlParameter("@AAT_MFG_DATE", SqlDbType.DateTime);
            param[27].Value = data.AAT_MFG_DT;
            param[28] = new SqlParameter("@AAT_WRNTY_DATE", SqlDbType.DateTime);
            param[28].Value = data.AAT_WAR_DT;
            param[29] = new SqlParameter("@PURCHASEDDATE", SqlDbType.DateTime);
            param[29].Value = data.AAT_PUR_DT;
            param[30] = new SqlParameter("@LIFESPAN", SqlDbType.NVarChar);
            param[30].Value = data.AAT_LF_SP;
            param[31] = new SqlParameter("@PONUMBER", SqlDbType.NVarChar);
            param[31].Value = data.AAT_PO_NO;
            param[32] = new SqlParameter("@DEPRECIATION", SqlDbType.Float);
            param[32].Value = Convert.ToDouble(data.AAT_DEP);
            param[33] = new SqlParameter("@ASTYPE", SqlDbType.Int);
            param[33].Value = Convert.ToInt32(data.AAT_TYPE);
            param[34] = new SqlParameter("@AAT_TOWER_CODE", SqlDbType.NVarChar);
            param[34].Value = data.AST_TOWER;
            param[35] = new SqlParameter("@AAT_FLOOR_CODE", SqlDbType.NVarChar);
            param[35].Value = data.AST_FLR;
            param[36] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[36].Value = 1;
            param[37] = new SqlParameter("@LOB", SqlDbType.NVarChar);
            param[37].Value = data.AAT_LOB;
            param[38] = new SqlParameter("@AAT_ASSET_IMAGE", SqlDbType.NVarChar);
            param[38].Value = data.AAT_AST_IMG;
            param[39] = new SqlParameter("@AAT_INVOICE_NUM", SqlDbType.NVarChar);
            param[39].Value = data.AAT_INV;
            param[40] = new SqlParameter("@AAT_VENDOR", SqlDbType.NVarChar);
            param[40].Value = "";

            param[41] = new SqlParameter("@ASSET_NAME1", SqlDbType.NVarChar);
            param[41].Value = data.AAT_NAME;
            param[42] = new SqlParameter("@AAT_DESCRIPTION", SqlDbType.NVarChar);
            param[42].Value = data.AAT_DES;


            //object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[AST_AMG_ASSET_Insert_Master_NEW]", param);
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[AST_AMG_ASSET_Insert_Master_NEW]", param);

            //int sem_id = (int)o;
            int sem_id = (int)dt.Rows[0][0];
            string msg;
            if (sem_id == 1)
                return new { Message = "Details are Inserted Successfully", data = dt };
            //msg = "Details are Inserted Successfully";
            else if (sem_id == 0)
                msg = "Asset Already Exist";
            else
                msg = "Something Went Wrong";
            return new { data = msg, Message = msg };
        }

        catch (Exception ex)
        {
            return new { Message = ex.Message };
        }
    }

    public object UploadExcel(HttpRequest httpRequest)
    {
        try
        {
            List<UploadExcelList> astlst;
            DataTable dt = new DataTable();
            /*      var baseUrl = "http://localhost:59227\\";*/ // Base URL or server address
            bool isSuccess;
            var postedFile = httpRequest.Files[0];
            var filePath = HttpContext.Current.Server.MapPath("~/UploadFiles/" + Path.GetFileName(postedFile.FileName));

            postedFile.SaveAs(filePath);

            var uplist = CreateExcelFile.ReadAsListExp6(filePath);

            isSuccess = true;
            isSuccess = uplist[0].AssetSNO == "SNO" ? isSuccess : false;
            isSuccess = uplist[0].AssetVendor == "Asset Vendor" ? isSuccess : false;
            isSuccess = uplist[0].AssetCategory == "Asset Category" ? isSuccess : false;
            isSuccess = uplist[0].AssetSubCategory == "Asset Sub Category" ? isSuccess : false;
            isSuccess = uplist[0].AssetModel == "Asset Model" ? isSuccess : false;
            isSuccess = uplist[0].AssetBrand == "Asset Brand" ? isSuccess : false;
            isSuccess = uplist[0].AssetLocation == "Asset Location" ? isSuccess : false;
            isSuccess = uplist[0].AssetCost == "Asset Cost" ? isSuccess : false;
            isSuccess = uplist[0].Tower == "Tower" ? isSuccess : false;
            isSuccess = uplist[0].Floor == "Floor" ? isSuccess : false;
            isSuccess = uplist[0].AssetDescription == "Asset Description" ? isSuccess : false;
            isSuccess = uplist[0].AAT_Serial_No == "Asset Serial No" ? isSuccess : false;





            if (isSuccess == false)
            {
                return new { Message = "Column Names are not matching Please check Uploaded Excel", data = (object)null };
            }

            DateTime dateNow = DateTime.Now;

            if (uplist.Count != 0)
            {

                uplist.RemoveAt(0);
            }
            ///return uplist;
            astlst = uplist;

            string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "[TEMP_AMG_ASSET_REMARKS] " +
        "(AssetSNO,AssetVendor,AssetCategory,AssetSubCategory,AssetModel,AssetBrand,AssetLocation,AssetCost,AssetDescription,AAT_Serial_No,Tower,Floor,Remarks) VALUES ";
            string str1 = "";
            int retval, cnt = 1;

            foreach (UploadExcelList uadm in astlst)
            {
                //DateTime dt = DateTime.ParseExact(uadm.BILL_DATE,"dd/MM/yyyy", CultureInfo.InvariantCulture);                 

                //string newString = dt.ToString("MM/dd/yyyy");


                if ((cnt % 1000) == 0)
                {
                    str1 = str1.Remove(str1.Length - 1, 1);
                    retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                    if (retval == -1)
                    {
                        return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                    }
                    else
                    {
                        str1 = "";
                        cnt = 1;
                    }

                }

                str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','1'),",

                      uadm.AssetSNO == null ? "" : uadm.AssetSNO,
                     uadm.AssetVendor == null ? "" : uadm.AssetVendor,
                    uadm.AssetCategory == null ? "" : uadm.AssetCategory,
                   uadm.AssetSubCategory == null ? "" : uadm.AssetSubCategory,
                  uadm.AssetModel == null ? "" : uadm.AssetModel,
                uadm.AssetBrand == null ? "" : uadm.AssetBrand,
                 uadm.AssetLocation == null ? "" : uadm.AssetLocation,
                  uadm.AssetCost == null ? "" : uadm.AssetCost,
                  uadm.AssetDescription == null ? "" : uadm.AssetDescription,
                  uadm.AAT_Serial_No == null ? "" : uadm.AAT_Serial_No,
                  uadm.Tower == null ? "" : uadm.Tower,
                  uadm.Floor == null ? "" : uadm.Floor

                        );
                cnt = cnt + 1;
            }
            int j = str1.Length;
            str1 = str1.Remove(str1.Length - 1, 1);
            retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[0].Value = HttpContext.Current.Session["UID"];
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
                param[1].Value = 1;

                DataTable dt1 = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[AMG_ASSET_Insert_Master_excel]", param);

                return new { Message = MessagesVM.UAD_UPLOK, data1 = dt1 };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
    public object GetData(ASTDETAILS data)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_ASSET_DETAILS_NEW]");
            sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@PAGENUM", data.PageNumber, DbType.String);
            sp.Command.AddParameter("@PAGESIZE", data.PageSize, DbType.String);
            sp.Command.AddParameter("@SEARCH", data.Search, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            return ds;
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }
    public object GetDetailsById(ASTDETAILS data)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_ASSET_DETAILS_BY_REQID]");
            sp.Command.Parameters.Add("@AAT_ID", data.AAT_ID, DbType.Int32);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            return ds;
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }

    }
    public object GetAssetLable(ASTDETAILS data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ASSET_LABLE");
            sp.Command.Parameters.Add("@AAT_LOC_ID", data.AST_LOC, DbType.String);
            sp.Command.Parameters.Add("@AAT_AAG_CODE", data.AST_CAT, DbType.String);
            sp.Command.Parameters.Add("@AAT_SUB_CODE", data.AST_SUB_CAT, DbType.String);
            sp.Command.Parameters.Add("@AAT_AAB_CODE", data.AST_BRD, DbType.String);
            sp.Command.Parameters.Add("@AAT_MODEL_NAME", data.AST_MODEL, DbType.String);
            ds = sp.GetDataSet();
            return ds;
            // return new { Message = "Contract Rejected Successfully", data = ds };
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }
    public object ModifyAssetDetails(ASTDETAILS data)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[44];


            param[0] = new SqlParameter("@AAT_CODE", SqlDbType.NVarChar);
            param[0].Value = data.AAT_CODE;
            param[1] = new SqlParameter("@AAT_LOC_ID", SqlDbType.NVarChar);
            param[1].Value = data.AST_LOC;
            param[2] = new SqlParameter("@AAT_MODEL_ID", SqlDbType.NVarChar);
            param[2].Value = data.AST_MODEL_NAME;
            param[3] = new SqlParameter("@AAT_MODEL_NAME", SqlDbType.NVarChar);
            param[3].Value = data.AST_MODEL;
            param[4] = new SqlParameter("@AAT_SUB_CODE", SqlDbType.NVarChar);
            param[4].Value = data.AST_SUB_CAT;
            param[5] = new SqlParameter("@AAT_AVR_CODE", SqlDbType.NVarChar);
            param[5].Value = data.AST_VENDOR;
            param[6] = new SqlParameter("@AAT_AAB_CODE", SqlDbType.NVarChar);
            param[6].Value = data.AST_BRD;
            param[7] = new SqlParameter("@AAT_AMC_REQD", SqlDbType.Int);
            param[7].Value = data.AAT_AMT_INC;
            param[8] = new SqlParameter("@AAT_AMC_DATE", SqlDbType.DateTime);
            param[8].Value = data.AAT_AMT_DT;
            param[9] = new SqlParameter("@AAT_AMC_DOCPATH", SqlDbType.NVarChar);
            param[9].Value = data.AAT_AMC_DOC;
            param[10] = new SqlParameter("@AAT_INS_REQD", SqlDbType.Int);
            param[10].Value = data.AAT_INS_INC;
            param[11] = new SqlParameter("@AAT_INS_DATE", SqlDbType.DateTime);
            param[11].Value = data.AAT_INS_DT;
            param[12] = new SqlParameter("@AAT_AAG_CODE", SqlDbType.NVarChar);
            param[12].Value = data.AST_CAT;
            param[13] = new SqlParameter("@AAT_INS_DOCPATH", SqlDbType.NVarChar);
            param[13].Value = data.AAT_INC_DOC;
            param[14] = new SqlParameter("@AAT_AST_COST", SqlDbType.Decimal);
            param[14].Value = Convert.ToDouble(data.AAT_COST);

            param[15] = new SqlParameter("@AAT_AST_SERIALNO", SqlDbType.NVarChar);
            param[15].Value = data.AAT_SNO;
            param[16] = new SqlParameter("@AAT_UPT_DT", SqlDbType.DateTime);
            param[16].Value = (DateTime.Now);
            param[17] = new SqlParameter("@AAT_UPT_BY", SqlDbType.NVarChar);
            param[17].Value = HttpContext.Current.Session["UID"];
            param[18] = new SqlParameter("@AAT_STA_ID", SqlDbType.NVarChar);
            param[18].Value = data.AAT_STATUS;
            param[19] = new SqlParameter("@AAT_OWNED", SqlDbType.NVarChar);
            param[19].Value = 1;
            param[20] = new SqlParameter("@AAT_PURCHASED_STATUS", SqlDbType.NVarChar);
            param[20].Value = 1;
            param[21] = new SqlParameter("@AAT_SPC_FIXED", SqlDbType.NVarChar);
            param[21].Value = 1;
            param[22] = new SqlParameter("@AAT_USR_MOVABLE", SqlDbType.NVarChar);
            param[22].Value = 0;
            param[23] = new SqlParameter("@AAT_AST_CONS", SqlDbType.NVarChar);
            param[23].Value = 0;
            param[24] = new SqlParameter("@AAT_RATE_CONTRACT", SqlDbType.NVarChar);
            param[24].Value = data.AST_RT_CNT;
            param[25] = new SqlParameter("@AAT_DESC", SqlDbType.NVarChar);
            param[25].Value = data.AAT_NAME;
            param[26] = new SqlParameter("@CONF_TYPE", SqlDbType.NVarChar);
            param[26].Value = "";
            param[27] = new SqlParameter("@AAT_MFG_DATE", SqlDbType.DateTime);
            param[27].Value = data.AAT_MFG_DT;
            param[28] = new SqlParameter("@AAT_WRNTY_DATE", SqlDbType.DateTime);
            param[28].Value = data.AAT_WAR_DT;
            param[29] = new SqlParameter("@PURCHASEDDATE", SqlDbType.DateTime);
            param[29].Value = data.AAT_PUR_DT;
            param[30] = new SqlParameter("@LIFESPAN", SqlDbType.NVarChar);
            param[30].Value = data.AAT_LF_SP;
            param[31] = new SqlParameter("@PONUMBER", SqlDbType.NVarChar);
            param[31].Value = data.AAT_PO_NO;
            param[32] = new SqlParameter("@DEPRECIATION", SqlDbType.Float);
            param[32].Value = Convert.ToDouble(data.AAT_DEP);
            param[33] = new SqlParameter("@ASTYPE", SqlDbType.Int);
            param[33].Value = Convert.ToInt32(data.AAT_TYPE);
            param[34] = new SqlParameter("@AAT_TOWER_CODE", SqlDbType.NVarChar);
            param[34].Value = data.AST_TOWER;
            param[35] = new SqlParameter("@AAT_FLOOR_CODE", SqlDbType.NVarChar);
            param[35].Value = data.AST_FLR;
            param[36] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[36].Value = 1;
            param[37] = new SqlParameter("@LOB", SqlDbType.NVarChar);
            param[37].Value = data.AAT_LOB;
            param[38] = new SqlParameter("@AAT_ASSET_IMAGE", SqlDbType.NVarChar);
            param[38].Value = data.AAT_AST_IMG;
            param[39] = new SqlParameter("@AAT_INVOICE_NUM", SqlDbType.NVarChar);
            param[39].Value = data.AAT_INV;
            param[40] = new SqlParameter("@AAT_VENDOR", SqlDbType.NVarChar);
            param[40].Value = "";

            param[41] = new SqlParameter("@AAT_NAME", SqlDbType.NVarChar);
            param[41].Value = data.AAT_NAME;
            param[42] = new SqlParameter("@AAT_DESCRIPTION", SqlDbType.NVarChar);
            param[42].Value = data.AAT_DES;
            param[43] = new SqlParameter("@AAT_ID", SqlDbType.Int);
            param[43].Value = data.AAT_ID;

            //object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[ASSET_UPDATE_Master]", param);
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[ASSET_UPDATE_Master]", param);
            //int sem_id = (int)o;
            int sem_id = (int)dt.Rows[0][0];
            string msg;
            if (sem_id == 1)
                return new { Message = "Details are Modified Successfully", data = dt };
            //string msg;
            //if (sem_id == 1)
            //    msg = "Details are Modified Successfully";
            else
                msg = "Something Went Wrong";
            return new { data = msg, Message = msg };
        }

        catch (Exception ex)
        {
            return new { Message = ex.Message };
        }
    }

}