﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for AddAssetInventoryModel
/// </summary>
/// 
//namespace AddAssetInventoryModel
//{

public class AstMainCategory
{
    public string Assetmaincatname { get; set; }
    public string Assetmaincatcode { get; set; }

    public bool ticked { get; set; }
}

public class AstSubCategory
{
    public string Assetsubncatcode { get; set; }
    public string Assetsubncatname { get; set; }

    public string Assetmaincatname { get; set; }
    public string Assetmaincatcode { get; set; }
    public bool ticked { get; set; }
}
public class AstModel
{
    public string MdCode { get; set; }
    public string MdName { get; set; }
    public string MdBrandCode { get; set; }
    public string Mdsubncatcode { get; set; }
    public string Mdcatcode { get; set; }
    public bool ticked { get; set; }
}

public class UploadExcelList
{
    public string AssetSNO { get; set; }
    public string AssetVendor { get; set; }
    public string AssetCategory { get; set; }
    public string AssetSubCategory { get; set; }
    public string AssetModel { get; set; }
    public string AssetBrand { get; set; }
    public string AssetLocation { get; set; }
    public string AssetCost { get; set; }

    public string AssetDescription { get; set; }
    public string AAT_Serial_No { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    //public string Remarks { get; set; }
}
public class Costcenter
{
    public string Cost_Center_Code { get; set; }
    public string Cost_Center_Name { get; set; }
    public bool ticked { get; set; }
}
public class AstBrand
{
    public string BrandCode { get; set; }
    public string BrandName { get; set; }
    public string Assetsubncatcode { get; set; }
    public string Assetsubncatname { get; set; }
    public string Assetcatcode { get; set; }
    public bool ticked { get; set; }
}
public class AstlOCATION
{
    public string lCMCODE { get; set; }
    public string LCMNAME { get; set; }
    public bool ticked { get; set; }
}
public class Astltower
{
    public string lCMCODE { get; set; }
    public string twr_CODE { get; set; }
    public string twr_name { get; set; }

    public bool ticked { get; set; }
}
public class AstlFloor
{
    public string flr_code { get; set; }
    public string flr_name { get; set; }
    public string twr_CODE { get; set; }
    public string lCM_CODE { get; set; }
    public bool ticked { get; set; }
}
public class AstVendor
{
    public string vendorCode { get; set; }
    public string vendorname { get; set; }

    public bool ticked { get; set; }
}
public class ASTDETAILS

{
    public int AAT_ID { get; set; }
    public string AST_CAT { get; set; }
    public string AST_SUB_CAT { get; set; }
    public string AST_BRD { get; set; }
    public string AST_MODEL { get; set; }
    public string AST_LOC { get; set; }
    public string AST_TOWER { get; set; }
    public string AST_FLR { get; set; }
    public string AAT_CODE { get; set; }
    public string AAT_NAME { get; set; }
    public string AST_VENDOR { get; set; }
    public string AST_RT_CNT { get; set; }
    public string AAT_COST { get; set; }
    public string AAT_CNT { get; set; }
    public string AAT_SNO { get; set; }
    public string AAT_TYPE { get; set; }
    public string AAT_LF_SP { get; set; }
    public string AAT_PO_NO { get; set; }
    public Nullable<DateTime> AAT_WAR_DT { get; set; }
    public Nullable<DateTime> AAT_PUR_DT { get; set; }
    public Nullable<DateTime> AAT_MFG_DT { get; set; }
    public string AAT_DEP { get; set; }
    public string AAT_LOB { get; set; }
    public string AAT_COM { get; set; }
    public string AAT_INV { get; set; }
    public string AAT_DES { get; set; }
    public Nullable<DateTime> AAT_AMT_DT { get; set; }
    public Nullable<DateTime> AAT_INS_DT { get; set; }
    public string AAT_STATUS { get; set; }
    public string AAT_AMT_INC { get; set; }
    public string AAT_INS_INC { get; set; }
    public string AAT_INC_DOC { get; set; }
    public string AAT_AMC_DOC { get; set; }
    public string AAT_AST_IMG { get; set; }
    public string AST_MODEL_NAME { get; set; }
    public List<AstModel> AstModel { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string Search { get; set; }
}
public class Asttype
{
    public string Assettype { get; set; }
    public string Assetname { get; set; }

    public bool ticked { get; set; }
}
public class Astcompany
{
    public string AssettCompany { get; set; }

    public bool ticked { get; set; }
}

public class excelbulkupload
{
    [JsonProperty("Asset Category")]
    public string AssetCategory { get; set; }

    [JsonProperty("Asset Sub Category")]
    public string AssetSubCategory { get; set; }


    [JsonProperty("Asset Model")]
    public string AssetModel { get; set; }

    [JsonProperty("AssetBrand")]
    public string AssetBrand { get; set; }

    [JsonProperty("Asset Location")]
    public string AssetLocation { get; set; }


    [JsonProperty("Asset Cost")]
    public string AssetCost { get; set; }

    public string Tower { get; set; }


    public string Floor { get; set; }

    [JsonProperty("Asset Description")]
    public string AssetDescription { get; set; }

    [JsonProperty("Asset Serial No")]
    public string AssetSerialNo { get; set; }



}
//}