﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssetBrandModel
/// </summary>
public class AssetBrandModel
{
    public string manufactuer_code { get; set; }
    public string manufacturer { get; set; }
    public string createdBy { get; set; }
    public string MANUFACTUER_TYPE_CODE { get; set; }
    public string MANUFACTUER_STATUS { get; set; }
    public string manufacturer_type_subcode { get; set; }
    public string MANU_REM { get; set; }
    public string manufacturerID { get; set; }

    public string MANUFACTURER_STATUS { get; set; }

    public string MANUFACTURER_TYPE_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string VT_TYPE { get; set; }


    public string AST_SUBCAT_CODE { get; set; }
}
public class assetCategory
{
    public string VT_TYPE { get; set; }
    public string VT_CODE { get; set; }
}
public class assetSubCategory
{
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string VT_CODE { get; set; }

}