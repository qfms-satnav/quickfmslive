﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Asset_ReconciliationModel
/// </summary>
public class Asset_ReconciliationModel
{
	public string Asset_Code { get; set; }

	public string Asset_Name { get; set; }

	public string Status { get; set; }

	public string Location_Name { get; set; }
	public string TAG_NAME { get; set; }

}

public class TypeLst
{
	public string CAT_CODE { get; set; }
	public string CAT_NAME { get; set; }
	public bool isChecked { get; set; }
}

public class LocationLstAst
{
	public string LCM_CODE { get; set; }
	public string LCM_NAME { get; set; }
	public string CTY_CODE { get; set; }
	public string CNY_CODE { get; set; }
	public bool ticked { get; set; }
}

public class AstBarCode
{
	public List<LocationLstAst> LocationLstAsts { get; set; }

	public List<TypeLst> Types { get; set; }

	public string ReqOpt { get; set; }
	public DateTime ReconDate { get; set; }
}

public class ExcelFields
{
	public int SNO { get; set; }
	public string Asset_Code { get; set; }
	public string Asset_Name { get; set; }
	public string Location { get; set; }
}