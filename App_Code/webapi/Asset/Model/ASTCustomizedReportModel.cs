﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;



public class EquipmentVM
{
    public EquipmentCustomizedData EquipmentCustomizedData { get; set; }
    public List<Categorylst> Categorylst { get; set; }
    public List<SubCategorylst> Subcatlst { get; set; }

    public List<Modellst> Modellst { get; set; }
    public List<Brandlst> Brandlst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public string Type { get; set; }
    public string Request_Type { get; set; }
    public string CNP_NAME { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public string SearchValue { get; set; }
}


    public class Categorylst
    {
        public string CAT_CODE { get; set; }
        public string CAT_NAME { get; set; }
        public string CAT_STATUS { get; set; }
        public bool isChecked { get; set; }
    }

    public class AssetTypes
    {
        public string ASSET_TYPE_CODE { get; set; }
        public string ASSET_TYPE_NAME { get; set; }
        public string TYPE_ID { get; set; }
        public bool isChecked { get; set; }
    }

    public class EmployeeList
{
    public string AUR_KNOWN_AS { get; set; }
    public string AUR_ID { get; set; }
}
public class SubCategorylst
{
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string CAT_CODE { get; set; }
    public bool isChecked { get; set; }
}
public class Brandlst
{
    public string BRND_CODE { get; set; }
    public string BRND_NAME { get; set; }
    public string CAT_CODE { get; set; }
    public string BRND_SUBCODE { get; set; }
    public bool isChecked { get; set; }
}
public class ContractStatuslst
{
    public string DRS_CODE { get; set; }
    public string DRS_NAME { get; set; }
    public bool isChecked { get; set; }
}
public class ContractCategorylst
{
    public string DMC_CODE { get; set; }
    public string DMC_NAME { get; set; }
    public bool isChecked { get; set; }
}
public class ContractSubcategorylst
{
    public string DBT_CODE { get; set; }
    public string DBT_NAME { get; set; }
    public string DMC_CODE { get; set; }
    public bool isChecked { get; set; }
}
public class Modellst
{
    public string MD_CODE { get; set; }
    public string MD_NAME { get; set; }
    public string BRND_CODE { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public string CAT_CODE { get; set; }
    public bool isChecked { get; set; }
}

public class ModelAsset
{
    public string AAT_MODEL_NAME { get; set; }
    public bool isChecked { get; set; }
}


public class EquipmentCustomizedData
{
    public string AAT_CODE { get; set; }
    public string AAT_LOC_ID { get; set; }
    public string LCM_NAME { get; set; }
    public string AST_MD_NAME { get; set; }
    public string VT_TYPE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string AAT_NAME { get; set; }
    public string MANUFACTURER { get; set; }
    public DateTime? PURCHASEDDATE { get; set; }
    public DateTime? AAT_AMC_DATE { get; set; }
    public string AAT_AST_SERIALNO { get; set; }
    public DateTime? AAT_WRNTY_DATE { get; set; }
    public string AAT_DEPRICIATION { get; set; }
    public string AAT_PO_NUMBER { get; set; }
    public string TOTALMONTHS { get; set; }
    public float? AAT_AST_SLVGVAL { get; set; }
    public float? AAT_AST_COST { get; set; }
    public string MAPPEDTO { get; set; }
    public string MOVEDFROMLOCATION { get; set; }
    public string MOVEDTOLOCATION { get; set; }
    public string RAISEDBY { get; set; }
    public string AATINTERRECEIVEDBY_AST_SEALING_TYPE { get; set; }
    public string STATUS { get; set; }
    public string AAT_AST_MOT_HP { get; set; }
    public string AVR_NAME { get; set; }
    public int Asset_count { get; set; }
    public string Asset_Age { get; set; }
    public string INTERRECEIVEDBY { get; set; }
    public string DISPOSAL_STATUS { get; set; }
    public DateTime? AAT_MFG_DATE { get; set; }
    public string AAT_DESC { get; set; }
    public string TAG_NAME { get; set; }
    public string AAT_STA_ID { get; set; }
    public string   OVERALL_COUNT { get; set; }
public DateTime? AAT_INVOICE_DATE { get; set; }

}