﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssetReconsilationReportModel
/// </summary>
public class AssetReconsilationReportModel
{


	public string Asset_Code { get; set; }

	public string Asset_Name { get; set; }
	public string Location_Name { get; set; }
	public string AAT_AAG_CODE { get; set; }
	public string SCANNED_BY { get; set; }
	public string SCANNED_DT { get; set; }
	public string AST_TYPE { get; set; }
	public string Status { get; set; }
    public string DESCRIPTION { get; set; }
}

public class TypeList
{
	public string CAT_CODE { get; set; }
	public string CAT_NAME { get; set; }
	public bool isChecked { get; set; }
}

public class LocationListAst
{
	public string LCM_CODE { get; set; }
	public string LCM_NAME { get; set; }
	public string CTY_CODE { get; set; }
	public string CNY_CODE { get; set; }
	public bool ticked { get; set; }
}

public class AstBarCodedata
{
	public List<LocationListAst> LocationLstAsts { get; set; }

	public List<TypeList> Types { get; set; }

	public string ReqOpt { get; set; }
	//public DateTime ReconDate { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public List<AssetDataRecon> AssetData { get; set; }
}
public class AssetNameRecon
{
    public string AAT_NAME { get; set; }
    public string AAT_LOC_ID { get; set; }
}
public class AssetDataRecon
{
    public string AAT_NAME { get; set; }
}
