﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DayWiseConsumableReportModel
/// </summary>
public class Categoriess
{
    public string VT_CODE { get; set; }
    public string VT_TYPE { get; set; }
    public string VT_CONS_STATUS { get; set; }
    public bool ticked { get; set; }
}

public class SubCategory
{
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string VT_CODE { get; set; }
    public string AST_CAT_CODE { get; set; }
    public bool ticked { get; set; }
}
public class Model
{
    public string AST_MD_CODE { get; set; }
    public string AST_MD_NAME { get; set; }
    public string AST_MD_BRDID { get; set; }
    public string AST_MD_SUBCATID { get; set; }
    public string AST_MD_CATID { get; set; }
    public string VT_CODE { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public string manufactuer_code { get; set; }
    public bool ticked { get; set; }
}
public class Brand
{
    public string manufactuer_code { get; set; }
    public string manufacturer { get; set; }
    public string manufacturer_type_subcode { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string manufacturer_type_code { get; set; }
    public string VT_CODE { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public bool ticked { get; set; }
}
public class lOCATION
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}
public class cityLists
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}
public class DayWiseConsReport
{
    public List<Categoriess> AssetMainCategoryList { get; set; }

    public List<SubCategory> AssetSUbCategoryList { get; set; }
    public List<Brand> AssetBrandList { get; set; }
    public List<Model> AssetModelList { get; set; }

    public List<lOCATION> AssetLOCList { get; set; }
    public List<cityLists> cityLists { get; set; }
    public string VT_TYPE { get; set; }
    public string AAC_CON_TOTASET { get; set; }
    public string AAC_CON_TOTAVBL { get; set; }
    public DateTime Fromdate { get; set; }
    public DateTime Todate { get; set; }

}
public class DayWiseConsumableReportModel
{
    public string VT_TYPE { get; set; }
    public string AAC_CON_TOTASET { get; set; }
    public string AAC_CON_TOTAVBL { get; set; }
    public DateTime Fromdate { get; set; }
    public DateTime Todate { get; set; }
}
public class ConsumableGridCols
{
    public string headerName { get; set; }
    public string field { get; set; }
    public int width { get; set; }
    public string cellClass { get; set; }
    public bool suppressMenu { get; set; }
}