﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GlobalSearchModel
/// </summary>
public class GlobalSearchModel
{
    public string LCM_CODE { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string SearchVal { get; set; }
}

public class GlobalSearchData
{
    public string AAT_ID { get; set; }
    public string AUR_STA_ID { get; set; }
    public string AUR_LOCATION { get; set; }
    public string AAT_NAME { get; set; }
    public string AAT_PO_NUMBER { get; set; }
    public string AAT_AVR_CODE { get; set; }
    public string AAT_AST_STATUS { get; set; }
    public string AMN_FROM_DATE { get; set; }
    public string AMN_TO_DATE { get; set; }
    public string PVM_PLAN_FDATE { get; set; }
    public string PVM_PLAN_TDATE { get; set; }
    public string STA_DESC { get; set; }
    public string AAT_CODE { get; set; }
    public string AAT_ASSET_IMAGE { get; set; }
}