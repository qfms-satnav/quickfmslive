﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssetCategoryModel
/// </summary>
public class AssetCategoryModel
{
    public string VT_CODE { get; set; }

    public string VT_TYPE { get; set; }
    public string ASSET_TYPE { get; set; }

    public string VT_STATUS { get; set; }
    public string VT_REM { get; set; }
    public string DEPR_VAL { get; set; }
    public string VT_DEPR { get; set; }
    public string VT_CONS_STATUS { get; set; }
    public DateTime VT_CREATED_BY { get; set; }
    public string VT_UPLOADED_DOC { get; set; }
    public string AST_TYPE_ID { get; set; }

}

public class AssetType
{
    public string ASSET_TYPE_NAME { get; set; }
    public string TYPE_ID { get; set; }
}