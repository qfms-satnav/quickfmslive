﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssetModelmodel
/// </summary>
public class AssetModelmodel
{
    public string AST_MD_CODE { get; set; }
    public string AST_MD_NAME { get; set; }
    public string AST_MD_BRDID { get; set; }
    public string AST_MD_SUBCATID { get; set; }
    public string AST_MD_CATID { get; set; }
    public string AST_MD_STAID { get; set; }
    public string AST_MD_CREATEDBY { get; set; }
    public int AST_MD_MINQTY { get; set; }
    public int AST_MD_STKQTY { get; set; }
    public string AST_MD_QTYUNIT { get; set; }
    public string AST_MD_REM { get; set; }

    public string AST_MD_QTYUNITS { get; set; }
    public string AST_MD_UPDATEDBY { get; set; }

    public string AST_MD_ID { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }

    public string VT_TYPE { get; set; }
    public string VT_CODE { get; set; }
    public string manufactuer_code { get; set; }
    public string manufacturer { get; set; }

}
public class assetSubCategorybycat
{
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }

    public string VT_TYPE { get; set; }
    public string VT_CODE { get; set; }
    public string AST_CAT_CODE { get; set; }
}
public class assetmainCategory
{
    public string VT_TYPE { get; set; }
    public string VT_CODE { get; set; }

    // public bool ticked { get; set; }
}
public class AstBrandbysubcat
{
    public string manufactuer_code { get; set; }
    public string MANUFACTURER_TYPE_CODE { get; set; }
    public string manufacturer { get; set; }
    public string manufacturer_type_subcode { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string VT_CODE { get; set; }
    //public bool ticked { get; set; }
    public string manufacturer_type_code { get; set; }
}