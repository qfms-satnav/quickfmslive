﻿using DocumentFormat.OpenXml.Bibliography;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>

public class AssetMappinglist
{
    public AssetEquipmentMappedData EquipmentMappedData { get; set; }
    public List<Categorylst> Categorylst { get; set; }
    public List<SubCategorylst> Subcatlst { get; set; }

    public List<Modellst> Modellst { get; set; }
    public List<Brandlst> Brandlst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<Mappedlst> Mappedlst { get; set; }
    public List<Emplst> Emplst { get; set; }
    public List<Reqlst> Reqlst { get; set; }
    public string Request_Type { get; set; }

    public string SearchValue { get; set; }
    public string LCM_CODE { get; set; }
    public string TWR_CODE { get; set; }
    public string FLR_CODE { get; set; }
    public string Remarks { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string Search { get; set; }
}
public class Flrlst
{
    public string FLR_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public string TWR_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool isChecked { get; set; }
}
public class Mappedlst
{
    public string Asset_Code { get; set; }


    public string LCM_CODE { get; set; }
    public string TWR_CODE { get; set; }
    public string FLR_CODE { get; set; }
    public string SPC_ID { get; set; }

    public bool ticked { get; set; }
}

public class SPC_LIST
{
    public string SPC_ID { get; set; }
    public string SPC_NAME { get; set; }
    public bool isChecked { get; set; }
}
public class Emplst
{
    public string AUR_ID { get; set; }
    public string AUR_FIRST_NAME { get; set; }

    public bool ticked { get; set; }
}
public class Reqlst
{
    public string AIR_REQ_TS { get; set; }

    public bool ticked { get; set; }
}
public class Twrlst
{
    public string TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool isChecked { get; set; }
}
public class AssetCategorylst
{
    public string CAT_CODE { get; set; }
    public string CAT_NAME { get; set; }
    public bool isChecked { get; set; }
}
public class AssetSubCategorylst
{
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string CAT_CODE { get; set; }
    public bool isChecked { get; set; }
}
public class AssetBrandlst
{
    public string BRND_CODE { get; set; }
    public string BRND_NAME { get; set; }
    public string CAT_CODE { get; set; }
    public string BRND_SUBCODE { get; set; }
    public bool isChecked { get; set; }
}
public class AssetModellst
{
    public string MD_CODE { get; set; }
    public string MD_NAME { get; set; }
    public string BRND_CODE { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public string CAT_CODE { get; set; }
    public bool isChecked { get; set; }
}
public class AssetEquipmentMappedData
{
    public string AAT_CODE { get; set; }
    public string AAT_LOC_ID { get; set; }
    public string LCM_NAME { get; set; }
    public string AST_MD_NAME { get; set; }
    public string VT_TYPE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string AAT_NAME { get; set; }
    public string MANUFACTURER { get; set; }
    public DateTime? PURCHASEDDATE { get; set; }
    public DateTime? AAT_AMC_DATE { get; set; }
    public string AAT_AST_SERIALNO { get; set; }
    public DateTime? AAT_WRNTY_DATE { get; set; }
    public string AAT_DEPRICIATION { get; set; }
    public string AAT_PO_NUMBER { get; set; }
    public string TOTALMONTHS { get; set; }
    public float? AAT_AST_SLVGVAL { get; set; }
    public float? AAT_AST_COST { get; set; }
    public string MAPPEDTO { get; set; }
    public string MOVEDFROMLOCATION { get; set; }
    public string MOVEDTOLOCATION { get; set; }
    public string RAISEDBY { get; set; }
    public string AATINTERRECEIVEDBY_AST_SEALING_TYPE { get; set; }
    public string STATUS { get; set; }
    public string AAT_AST_MOT_HP { get; set; }
    public string AVR_NAME { get; set; }
    public int Asset_count { get; set; }
    public string Asset_Age { get; set; }
    public string INTERRECEIVEDBY { get; set; }
    public string DISPOSAL_STATUS { get; set; }
    public DateTime? AAT_MFG_DATE { get; set; }
    public string AAT_DESC { get; set; }
    public string TAG_NAME { get; set; }
    public string AAT_STA_ID { get; set; }
    public string OVERALL_COUNT { get; set; }
    public DateTime? AAT_INVOICE_DATE { get; set; }

}