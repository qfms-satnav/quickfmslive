﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for UserGuestHouseUltReportService
/// </summary>
public class DailyOccupancyService
{
    SubSonic.StoredProcedure sp;
    DailyOccupancyDetailsGrid UsrHosdt;
    List<DailyOccupancyDetailsGrid> dolst;
    DataSet ds;


    public object GetDailyOccupancyDetailsObj(DailyOccupancyVM Det)
    {
        try
        {
            dolst = GetDailyOccupancyDetails(Det);
            if (dolst.Count != 0) { return new { Message = MessagesVM.SER_OK, data = dolst }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<DailyOccupancyDetailsGrid> GetDailyOccupancyDetails(DailyOccupancyVM UGHU)
    {
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(UGHU.lcmlst);
        param[1] = new SqlParameter("@SELECTEDDATE", SqlDbType.DateTime);
        param[1].Value = UGHU.Date;
        //param[3] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
        //param[3].Value = UGHU.Status;
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);

        if (UGHU.CompanyID == null)
        {
            param[2].Value = HttpContext.Current.Session["companyid"];
        }
        else
        {
            param[2].Value = UGHU.CompanyID;
        }

        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["uid"];
        param[4] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
        param[4].Value = UGHU.SearchValue;
        param[5] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
        param[5].Value = UGHU.PageNumber;
        param[6] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
        param[6].Value = UGHU.PageSize;

        List<DailyOccupancyDetailsGrid> UserGustDt = new List<DailyOccupancyDetailsGrid>();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_DAILY_OCCUPANCY_REPORT", param))
        {
            while (sdr.Read())
            {
                UsrHosdt = new DailyOccupancyDetailsGrid();
                UsrHosdt.RB_REQ_ID = sdr["RB_REQ_ID"].ToString();
                UsrHosdt.RT_NAME = sdr["RT_NAME"].ToString();
                UsrHosdt.RF_NAME = sdr["RF_NAME"].ToString();
                UsrHosdt.RR_NAME = sdr["RR_NAME"].ToString();
                UsrHosdt.RBS_CTY_CODE = sdr["RBS_CTY_CODE"].ToString();
                UsrHosdt.RBS_LOC_CODE = sdr["RBS_LOC_CODE"].ToString();
                UsrHosdt.RB_FROM_DATE = Convert.ToDateTime(sdr["RB_FROM_DATE"]);
                UsrHosdt.RB_TO_DATE = Convert.ToDateTime(sdr["RB_TO_DATE"].ToString());
                UsrHosdt.DEP_NAME = sdr["DEP_NAME"].ToString();
                UsrHosdt.RB_CREATEDBY = sdr["RB_CREATEDBY"].ToString();
                UsrHosdt.RB_CREATEDON = Convert.ToDateTime(sdr["RB_CREATEDON"]);
                UsrHosdt.RB_RESERVED_FOR = sdr["RB_RESERVED_FOR"].ToString();
                UsrHosdt.RESVERED_FOR_EMAIL = sdr["RESVERED_FOR_EMAIL"].ToString();
                UsrHosdt.RESVERED_BY_EMAIL = sdr["RESVERED_BY_EMAIL"].ToString();
                //UsrHosdt.RB_CHK_IN_DATE = Convert.ToDateTime(sdr["RB_CHK_IN_DATE"]);
                
                //UsrHosdt.RB_CHK_OUT_DATE = Convert.ToDateTime(sdr["RB_CHK_OUT_DATE"]);

                UsrHosdt.RB_CHK_IN_DATE = sdr["RB_CHK_IN_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(sdr["RB_CHK_IN_DATE"]);
                UsrHosdt.RB_CHK_OUT_DATE = sdr["RB_CHK_OUT_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(sdr["RB_CHK_OUT_DATE"]);

                UsrHosdt.RB_CHK_IN_TIME = sdr["RB_CHK_IN_TIME"].ToString();
                UsrHosdt.RB_CHK_OUT_TIME = sdr["RB_CHK_OUT_TIME"].ToString();
                UsrHosdt.STA_TITLE = sdr["STA_TITLE"].ToString();
                UsrHosdt.RB_REFERENCE_ID = sdr["RB_REFERENCE_ID"].ToString();
                UsrHosdt.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                UsrHosdt.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                UserGustDt.Add(UsrHosdt);
            }
        }
        return UserGustDt;
    }
    
  }