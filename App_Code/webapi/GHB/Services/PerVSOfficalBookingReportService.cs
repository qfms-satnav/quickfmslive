﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;


/// <summary>
/// Summary description for PerVSOfficalBookingReportService
/// </summary>
public class PerVSOfficalBookingReportService
{
    SubSonic.StoredProcedure sp;
    PervsoffDetails peroffdt;
    DataSet ds;

    public List<PervsoffDetails> Getdetails(PerVSOfficalBookingVM PVSOB)
    {
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(PVSOB.lcmlst);
        param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[1].Value = PVSOB.FromDate;
        param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[2].Value = PVSOB.ToDate;
        param[3] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
        param[3].Value = PVSOB.Status;
        
        param[4] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
       
        if (PVSOB.CompanyID == null)
        {
            param[4].Value = HttpContext.Current.Session["companyid"];
        }
        else
        {
            param[4].Value = PVSOB.CompanyID;
        }

        param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["uid"];
        param[6] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
        param[6].Value = PVSOB.SearchValue;
        param[7] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
        param[7].Value = PVSOB.PageNumber;
        param[8] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
        param[8].Value = PVSOB.PageSize;
        List<PervsoffDetails> peroffdtDt = new List<PervsoffDetails>();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_PERSONAL_VS_OFFICAL_BOOKING_DT", param))
        {
            while (sdr.Read())
            {
                peroffdt = new PervsoffDetails();
                peroffdt.RT_NAME = sdr["RT_NAME"].ToString();
                peroffdt.RF_NAME = sdr["RF_NAME"].ToString();
                peroffdt.RR_NAME = sdr["RR_NAME"].ToString();
                peroffdt.RR_CAPCITY = sdr["RR_CAPCITY"].ToString();
                peroffdt.RF_CTY_CODE = sdr["RF_CTY_CODE"].ToString();
                peroffdt.RF_LOC_CODE = sdr["RF_LOC_CODE"].ToString();
                peroffdt.PERSONAL = sdr["PERSONAL"].ToString();
                peroffdt.OFFICIAL = sdr["OFFICIAL"].ToString();
                peroffdt.RF_COST = Convert.ToInt32(sdr["RF_COST"].ToString());
                peroffdt.TOTALCOST = Convert.ToInt32(sdr["TOTALCOST"].ToString());
                peroffdt.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                peroffdtDt.Add(peroffdt);
            }
        }
        return peroffdtDt;
    }
    public object GetPersonalVSOfficialChart(PerVSOfficalBookingVM Avail)
    {
        try
        {
            List<SpaceAvailData> Spcdata = new List<SpaceAvailData>();
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Avail.lcmlst);
            param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
            param[1].Value = Avail.FromDate;
            param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
            param[2].Value = Avail.ToDate;
            param[3] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
            param[3].Value = Avail.Status;
            param[4] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["Uid"].ToString();
            param[5] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            if (Avail.CompanyID == null)
            {
                param[5].Value = HttpContext.Current.Session["companyid"];
            }
            else
            {
                param[5].Value = Avail.CompanyID;
            }

            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GH_GET_PERSONAL_VS_OFFICAL_CHART", param);
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}