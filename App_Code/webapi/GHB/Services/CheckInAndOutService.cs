﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;


/// <summary>
/// Summary description for CheckInAndOutService
/// </summary>
public class CheckInAndOutService
{
    SubSonic.StoredProcedure sp;
    UserGuestHouseDetails UsrHosdt;
    DataSet ds;

    public object GetDetails()
    {

        ChkInOutDetails chkdetails;
        List<ChkInOutDetails> chkdetailslist = new List<ChkInOutDetails>();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_DETAILS_TO_CHECK_IN_OUT");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            using (IDataReader reader = sp.GetReader())
            {                
                while (reader.Read())
                {
                    chkdetails = new ChkInOutDetails();
                    chkdetails.RB_REQ_ID = reader["RB_REQ_ID"].ToString();
                    chkdetails.RT_NAME = reader["RT_NAME"].ToString();
                    chkdetails.RF_NAME = reader["RF_NAME"].ToString();
                    chkdetails.RR_NAME = reader["RR_NAME"].ToString();


                    chkdetails.CNY_NAME = reader["CNY_NAME"].ToString();
                    chkdetails.CTY_NAME = reader["CTY_NAME"].ToString();
                    chkdetails.LCM_NAME = reader["LCM_NAME"].ToString();

                    chkdetails.RF_CNY_CODE = reader["RF_CNY_CODE"].ToString();
                    chkdetails.RF_CTY_CODE = reader["RF_CTY_CODE"].ToString();
                    chkdetails.RF_LOC_CODE = reader["RF_LOC_CODE"].ToString();

                    chkdetails.RB_FROM_DATE = Convert.ToDateTime(reader["RB_FROM_DATE"]);
                    chkdetails.RB_TO_DATE = Convert.ToDateTime(reader["RB_TO_DATE"].ToString());
                    chkdetails.RB_FRM_TIME = reader["RB_FRM_TIME"].ToString();
                    chkdetails.RB_TO_TIME = reader["RB_TO_TIME"].ToString();



                    chkdetails.RESERVED_FOR_EMAIL = reader["RESERVED_FOR_EMAIL"].ToString();
                    chkdetails.RESERVED_FOR = reader["RESERVED_FOR"].ToString();
                    chkdetails.RESERVED_BY_EMAIL = reader["RESERVED_BY_EMAIL"].ToString();
                    chkdetails.RESERVED_BY = reader["RESERVED_BY"].ToString();
                    chkdetails.RESERVED_DT = Convert.ToDateTime(reader["RESERVED_DT"]);

                    chkdetails.RB_REFERENCE_ID = reader["RB_REFERENCE_ID"].ToString();

                    chkdetails.CHKSTATUS = reader["CHKSTATUS"].ToString();

                    chkdetailslist.Add(chkdetails);
                }
                reader.Close();
                if (chkdetailslist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = chkdetailslist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }


    public object GetDetails(QuickSelect qs)
    {

        ChkInOutDetails chkdetails;
        List<ChkInOutDetails> chkdetailslist = new List<ChkInOutDetails>();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_DETAILS_TO_CHECK_IN_OUT");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@FROMDATE", qs.FromDate, DbType.String);
            sp.Command.AddParameter("@TODATE", qs.ToDate, DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    chkdetails = new ChkInOutDetails();
                    chkdetails.RB_REQ_ID = reader["RB_REQ_ID"].ToString();
                    chkdetails.RT_NAME = reader["RT_NAME"].ToString();
                    chkdetails.RF_NAME = reader["RF_NAME"].ToString();
                    chkdetails.RR_NAME = reader["RR_NAME"].ToString();


                    chkdetails.CNY_NAME = reader["CNY_NAME"].ToString();
                    chkdetails.CTY_NAME = reader["CTY_NAME"].ToString();
                    chkdetails.LCM_NAME = reader["LCM_NAME"].ToString();

                    chkdetails.RF_CNY_CODE = reader["RF_CNY_CODE"].ToString();
                    chkdetails.RF_CTY_CODE = reader["RF_CTY_CODE"].ToString();
                    chkdetails.RF_LOC_CODE = reader["RF_LOC_CODE"].ToString();

                    chkdetails.RB_FROM_DATE = Convert.ToDateTime(reader["RB_FROM_DATE"]);
                    chkdetails.RB_TO_DATE = Convert.ToDateTime(reader["RB_TO_DATE"].ToString());
                    chkdetails.RB_FRM_TIME = reader["RB_FRM_TIME"].ToString();
                    chkdetails.RB_TO_TIME = reader["RB_TO_TIME"].ToString();



                    chkdetails.RESERVED_FOR_EMAIL = reader["RESERVED_FOR_EMAIL"].ToString();
                    chkdetails.RESERVED_FOR = reader["RESERVED_FOR"].ToString();
                    chkdetails.RESERVED_BY_EMAIL = reader["RESERVED_BY_EMAIL"].ToString();
                    chkdetails.RESERVED_BY = reader["RESERVED_BY"].ToString();
                    chkdetails.RESERVED_DT = Convert.ToDateTime(reader["RESERVED_DT"]);

                    chkdetails.RB_REFERENCE_ID = reader["RB_REFERENCE_ID"].ToString();

                    chkdetails.CHKSTATUS = reader["CHKSTATUS"].ToString();
                    chkdetails.RB_SNO = reader["RB_SNO"].ToString();

                    chkdetailslist.Add(chkdetails);
                }
                reader.Close();
                if (chkdetailslist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = chkdetailslist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }


    public object GetDetailsByReqId(string reqid)
    {
        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_CHKINOUT_DETAILS_BY_REQID");
        //sp.Command.AddParameter("@REQID", reqid, DbType.String);
        //sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        //DataSet ds = sp.GetDataSet();
        //return ds;
        CheckInOutGrid chkdetails;
        List<CheckInOutGrid> chkdetailslist = new List<CheckInOutGrid>();

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_CHKINOUT_DETAILS_BY_REQID");
        sp.Command.AddParameter("@REQID", reqid, DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        using (IDataReader reader = sp.GetReader())
        {
            while (reader.Read())
            {
                chkdetails = new CheckInOutGrid();
                chkdetails.RB_REQ_ID = reader["RB_REQ_ID"].ToString();
                chkdetails.RB_SNO = reader["RB_SNO"].ToString();
                chkdetails.RR_NAME = reader["RR_NAME"].ToString();


                chkdetails.RB_EMP_TYPE = reader["RB_EMP_TYPE"].ToString();
                chkdetails.RB_EMP_NAME = reader["RB_EMP_NAME"].ToString();
                chkdetails.RB_EMP_EMAIL = reader["RB_EMP_EMAIL"].ToString();

                chkdetails.RB_REQ_ID_CC = reader["RB_REQ_ID_CC"].ToString();
                chkdetails.RB_STA_ID = reader["RB_STA_ID"].ToString();

                chkdetails.RB_FROM_DATE = Convert.ToDateTime(reader["RB_FROM_DATE"].ToString());

                chkdetails.RB_TO_DATE = Convert.ToDateTime(reader["RB_TO_DATE"].ToString());

                chkdetails.RB_CHK_IN_TIME = reader["RB_CHK_IN_TIME"].ToString();
                chkdetails.RB_CHK_OUT_TIME = reader["RB_CHK_OUT_TIME"].ToString();
                chkdetails.RB_CHK_IN_OUT_STA_ID = reader["RB_CHK_IN_OUT_STA_ID"].ToString();
                chkdetails.RB_RESERVED_FOR = reader["RB_RESERVED_FOR"].ToString();
                chkdetails.RB_REMARKS = "";
                chkdetails.RB_MOBILE_NUMBER = reader["RB_MOBILE_NUMBER"].ToString();

                chkdetailslist.Add(chkdetails);
            }
            reader.Close();
            if (chkdetailslist.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = chkdetailslist };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }
    public object SubmitCheckInOut(CheckInOutGrid obj)
    {
        SqlParameter[] param = new SqlParameter[12];

        param[0] = new SqlParameter("@RB_SNO", SqlDbType.Int);
        param[0].Value = obj.RB_SNO;

        param[1] = new SqlParameter("@RB_REQ_ID", SqlDbType.NVarChar);
        param[1].Value = obj.RB_REQ_ID;

        param[2] = new SqlParameter("@RB_RESERVED_FOR", SqlDbType.VarChar);
        param[2].Value = obj.RB_RESERVED_FOR;

        param[3] = new SqlParameter("@RB_EMP_NAME", SqlDbType.NVarChar);
        param[3].Value = obj.RB_EMP_NAME;

        param[4] = new SqlParameter("@RB_EMP_TYPE", SqlDbType.NVarChar);
        param[4].Value = obj.RB_EMP_TYPE;

        param[5] = new SqlParameter("@RB_EMP_EMAIL", SqlDbType.NVarChar);
        param[5].Value = obj.RB_EMP_EMAIL;

        param[6] = new SqlParameter("@RB_CHK_IN_TIME", SqlDbType.NVarChar);
        param[6].Value = obj.RB_CHK_IN_TIME;

        param[7] = new SqlParameter("@RB_CHK_OUT_TIME", SqlDbType.NVarChar);
        param[7].Value = obj.RB_CHK_OUT_TIME;

        param[8] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[8].Value = HttpContext.Current.Session["UID"];

        param[9] = new SqlParameter("@TYPE", SqlDbType.NVarChar);
        param[9].Value = obj.Type;

        param[10] = new SqlParameter("@RB_REMARKS", SqlDbType.NVarChar);
        param[10].Value = obj.RB_REMARKS;

        param[11] = new SqlParameter("@RB_CHK_OUT_DATE", SqlDbType.NVarChar);
        param[11].Value = obj.RB_CHK_OUT_DATE;

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_INSERT_CHECK_IN_OUT_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    SendMailCheckInOut(dr["REQID"].ToString(), 1, obj.RB_SNO);
                    return new { Message = dr["MSG"] + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    SendMailCheckInOut(dr["REQID"].ToString(), 2, obj.RB_SNO);
                    return new { Message = dr["MSG"] + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else
                    return new { Message = dr["REQID"].ToString(), data = (object)null };
            }
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public void SendMailCheckInOut(string REQUEST_ID, int FLAG, string RB_SNO)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_SEND_MAIL_CHECK_IN_OUT");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.Command.AddParameter("@RB_SNO", RB_SNO, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }
}