﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;


/// <summary>
/// Summary description for CheckInAndOutService
/// </summary>
public class ViewAndModifyService
{
    SubSonic.StoredProcedure sp;
    UserGuestHouseDetails UsrHosdt;
    DataSet ds;

    public object GetBookedRequests(QuickSelect qs)
    {

        ViewAndModifyVM vmdetails;
        List<ViewAndModifyVM> vmdetailslist = new List<ViewAndModifyVM>();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_BOOKED_REQUEST_VIEW_MODIFY");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@FROMDATE", qs.FromDate, DbType.String);
            sp.Command.AddParameter("@TODATE", qs.ToDate, DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    vmdetails = new ViewAndModifyVM();
                    vmdetails.BM_REQ_ID = reader["BM_REQ_ID"].ToString();
                    vmdetails.BM_FROM_DATE = Convert.ToDateTime(reader["BM_FROM_DATE"]);
                    vmdetails.BM_TO_DATE = Convert.ToDateTime(reader["BM_TO_DATE"].ToString());
                    vmdetails.BM_FRM_TIME = reader["BM_FRM_TIME"].ToString();
                    vmdetails.BM_TO_TIME = reader["BM_TO_TIME"].ToString();
                    vmdetails.BM_REFERENCE_ID = reader["BM_REFERENCE_ID"].ToString();
                    vmdetails.RESERVED_FOR_EMAIL = reader["RESERVED_FOR_EMAIL"].ToString();
                    vmdetails.RESERVED_FOR = reader["BD_EMP_NAME"].ToString();
                    vmdetails.RESERVED_BY_EMAIL = reader["RESERVED_BY_EMAIL"].ToString();
                    vmdetails.RESERVED_BY = reader["RESERVED_BY"].ToString();
                    vmdetails.RESERVED_DT = Convert.ToDateTime(reader["RESERVED_DT"]);
                    vmdetails.RR_NAME = reader["RR_NAME"].ToString();
                    vmdetails.LCM_NAME = reader["LCM_NAME"].ToString();
                    vmdetails.BM_LCM_CODE = reader["BM_LCM_CODE"].ToString();
                    vmdetails.RF_NAME = reader["RF_NAME"].ToString();
                    vmdetails.RF_COST = Convert.ToInt32(reader["RF_COST"].ToString());
                    vmdetails.BM_CHK_IN_OUT_STA_ID = reader["BM_CHK_IN_OUT_STA_ID"].ToString();
                    vmdetailslist.Add(vmdetails);

                }
                reader.Close();
                if (vmdetailslist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = vmdetailslist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public List<Events> GetBookedEvent(string screenType, string LocationCode, string RB_REQ_ID, string startdate, string enddate)
    {
        List<Events> CData = new List<Events>();
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        param[0].Value = startdate;
        param[1] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        param[1].Value = enddate;
        param[2] = new SqlParameter("@RB_REQ_ID", SqlDbType.NVarChar);
        param[2].Value = RB_REQ_ID;
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];

        param[4] = new SqlParameter("@ScreenType", SqlDbType.NVarChar);
        param[4].Value = screenType;

        param[5] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
        param[5].Value = LocationCode;

        param[6] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[6].Value = HttpContext.Current.Session["COMPANYID"];

        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_CALENDAR_EVENTS", param))
        {
            while (reader.Read())
            {
                Events e = new Events();
                e.title = Convert.ToString(reader["Title"]);
                e.start = Convert.ToString(reader["StartDateString"]);
                e.end = Convert.ToString(reader["EndDateString"]);
                e.ReferenceID = Convert.ToString(reader["BM_REFERENCE_ID"]);
                e.ReservedFor = Convert.ToString(reader["BM_RESERVED_FOR"]);
                e.AUR_NAME = Convert.ToString(reader["AUR_NAME"]);
                e.Remarks = Convert.ToString(reader["BM_REMARKS"]);
                e.RoomNames = Convert.ToString(reader["RR_NAME"]);
                e.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                e.className = "#FF0000";
                e.RF_NAME = Convert.ToString(reader["RF_NAME"]);
                e.RB_TYPE = Convert.ToString(reader["BM_TYPE"]);
                CData.Add(e);
            }
            reader.Close();
        }
        return CData;
    }

    public object UpdateBookedRequest(AdminBookingVM wobj)
    {
        SqlParameter[] param = new SqlParameter[17];

        param[0] = new SqlParameter("@BM_TYPE", SqlDbType.Int);
        param[0].Value = wobj.obj.BM_TYPE;

        param[1] = new SqlParameter("@BM_TITLE", SqlDbType.NVarChar);
        param[1].Value = wobj.obj.BM_TITLE;

        param[2] = new SqlParameter("@BM_FROM_DATE", SqlDbType.DateTime);
        param[2].Value = wobj.obj.BM_FROM_DATE;

        param[3] = new SqlParameter("@BM_TO_DATE", SqlDbType.DateTime);
        param[3].Value = wobj.obj.BM_TO_DATE;

        param[4] = new SqlParameter("@BM_FROM_TIME", SqlDbType.VarChar);
        param[4].Value = wobj.obj.BM_FROM_TIME;

        param[5] = new SqlParameter("@BM_TO_TIME", SqlDbType.VarChar);
        param[5].Value = wobj.obj.BM_TO_TIME;

        param[6] = new SqlParameter("@BM_RESERVED_FOR", SqlDbType.NVarChar);
        param[6].Value = wobj.obj.BM_RESERVED_FOR;

        param[7] = new SqlParameter("@BM_REFERENCE_ID", SqlDbType.NVarChar);
        param[7].Value = wobj.obj.BM_REFRERENCE_ID;

        param[8] = new SqlParameter("@BM_STA_ID", SqlDbType.Int);
        param[8].Value = wobj.obj.BM_STA_ID;

        param[9] = new SqlParameter("@BM_REMARKS", SqlDbType.NVarChar);
        param[9].Value = wobj.obj.BM_REMARKS;

        param[10] = new SqlParameter("@BM_CREATEDBY", SqlDbType.NVarChar);
        param[10].Value = HttpContext.Current.Session["UID"];

        param[11] = new SqlParameter("@BD_LIST", SqlDbType.Structured);
        param[11].Value = UtilityService.ConvertToDataTable(wobj.objList);

        param[12] = new SqlParameter("@BM_REQ_ID", SqlDbType.NVarChar);
        param[12].Value = wobj.obj.BM_REQ_ID;

        param[13] = new SqlParameter("@BM_LCM_CODE", SqlDbType.NVarChar);
        param[13].Value = wobj.obj.BM_LCM_CODE;

        param[14] = new SqlParameter("@BM_CTY_CODE", SqlDbType.NVarChar);
        param[14].Value = wobj.obj.BM_CTY_CODE;

        param[15] = new SqlParameter("@BM_CNY_CODE", SqlDbType.NVarChar);
        param[15].Value = wobj.obj.BM_CNY_CODE;


        param[16] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[16].Value = HttpContext.Current.Session["COMPANYID"];

        string sp_name = "";
        if (wobj.obj.BM_STA_ID == 3)
            sp_name = "GH_ADMIN_DIRECT_CANCEL_BOOKING";
        else
            sp_name = "GH_ADMIN_DIRECT_BOOKING_UPDATE";

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, sp_name, param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 2)
                {
                    string RetMessage = string.Empty;
                    SendMailAdminBooking(dr["REQID"].ToString(), wobj.obj.BM_STA_ID);
                    if (wobj.obj.BM_STA_ID == 4)
                        wobj.obj.BM_STA_ID = 16;
                    else
                        wobj.obj.BM_STA_ID = 1;
                    RequestState sta = (RequestState)wobj.obj.BM_STA_ID;
                    switch (sta)
                    {
                        case RequestState.Canceled: RetMessage = MessagesVM.RB_CANCELED;
                            break;
                        case RequestState.Modified: RetMessage = MessagesVM.RB_UPDATED;
                            break;

                    }
                    return new { Message = RetMessage + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.GH_FAIL, data = dr["REQID"].ToString() };
                }
                else
                    return new { Message = dr["REQID"].ToString(), data = (object)null };
            }
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public void SendMailAdminBooking(string REQUEST_ID, int FLAG)
    {
        try
        {
            
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_SEND_MAIL_ADMIN_BOOKING");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }

    public object GetRequestDetails(string reqID)
    {

        ModifyABVM vmdetails;
        List<ModifyABVM> vmdetailslist = new List<ModifyABVM>();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_REQ_DETAILS_WITHHOLD");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@REQID", reqID, DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    vmdetails = new ModifyABVM();
                    vmdetails.RT_NAME = reader["RT_NAME"].ToString();
                    vmdetails.RF_NAME = reader["RF_NAME"].ToString();
                    vmdetails.RR_NAME = reader["RR_NAME"].ToString();

                    vmdetails.COUNTRY = reader["CNY_NAME"].ToString();
                    vmdetails.CITY = reader["CTY_NAME"].ToString();
                    vmdetails.LOCATION = reader["LCM_NAME"].ToString();

                    vmdetails.RF_CNY_CODE = reader["RF_CNY_CODE"].ToString();
                    vmdetails.RF_CTY_CODE = reader["RF_CTY_CODE"].ToString();
                    vmdetails.RF_LOC_CODE = reader["RF_LOC_CODE"].ToString();

                    vmdetails.ticked = Convert.ToBoolean(reader["ticked"]);

                    vmdetails.RR_RT_SNO = reader["RT_SNO"].ToString();
                    vmdetails.RR_RF_SNO = reader["RF_SNO"].ToString();
                    vmdetails.RR_SNO = reader["RR_SNO"].ToString();

                    vmdetails.WD_REQ_ID = reader["RB_REQ_ID_CC"].ToString();

                    vmdetailslist.Add(vmdetails);
                }
                reader.Close();
                if (vmdetailslist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = vmdetailslist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
}