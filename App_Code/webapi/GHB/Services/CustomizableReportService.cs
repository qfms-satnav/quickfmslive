﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
/// <summary>
/// Summary description for CustomizableReportService
/// </summary>
public class CustomizableReportService
{
    SubSonic.StoredProcedure sp;
    CustomizableRpteDetails Customizabledt;
    DataSet ds;

    public List<CustomizableRpteDetails> Getdetails(CustomizableRptVM CustRpt)
    {
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(CustRpt.lcmlst);
        param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[1].Value = CustRpt.FromDate;
        param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[2].Value = CustRpt.ToDate;
        param[3] = new SqlParameter("@STA_TYPE", SqlDbType.NVarChar);
        param[3].Value = CustRpt.Status;


        param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[4].Value = HttpContext.Current.Session["UID"];

        param[5] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);

        if (CustRpt.CompanyID == null)
        {
            param[5].Value = HttpContext.Current.Session["companyid"];
        }
        else
        {
            param[5].Value = CustRpt.CompanyID;
        }
        param[6] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
        param[6].Value = CustRpt.SearchValue;
        param[7] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
        param[7].Value = CustRpt.PageNumber;
        param[8] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
        param[8].Value = CustRpt.PageSize;

        List<CustomizableRpteDetails> Custlstdt = new List<CustomizableRpteDetails>();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CUSTOMIZABLE_RPT_GH", param))
        {
            while (sdr.Read())
            {
                Customizabledt = new CustomizableRpteDetails();
                Customizabledt.RB_REQ_ID = sdr["RB_REQ_ID"].ToString();
                Customizabledt.RT_NAME = sdr["RT_NAME"].ToString();
                Customizabledt.RF_NAME = sdr["RF_NAME"].ToString();
                Customizabledt.RR_NAME = sdr["RR_NAME"].ToString();
                Customizabledt.RR_CAPCITY = sdr["RR_CAPCITY"].ToString();
                Customizabledt.RBS_CNY_CODE = sdr["RBS_CNY_CODE"].ToString();
                Customizabledt.RBS_CTY_CODE = sdr["RBS_CTY_CODE"].ToString();
                Customizabledt.RBS_LOC_CODE = sdr["RBS_LOC_CODE"].ToString();
                Customizabledt.RB_FROM_DATE = Convert.ToDateTime(sdr["RB_FROM_DATE"]);
                Customizabledt.RB_TO_DATE = Convert.ToDateTime(sdr["RB_TO_DATE"].ToString());
                Customizabledt.DEP_NAME = sdr["DEP_NAME"].ToString();
                Customizabledt.RB_CREATEDBY = sdr["RB_CREATEDBY"].ToString();
                Customizabledt.RB_CREATEDON = sdr["RB_CREATEDON"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(sdr["RB_CREATEDON"]);
                Customizabledt.RB_RESERVED_FOR = sdr["RB_RESERVED_FOR"].ToString();
                Customizabledt.RESVERED_BY_EMAIL = sdr["RESVERED_BY_EMAIL"].ToString();
                Customizabledt.RESVERED_FOR_EMAIL = sdr["RESVERED_FOR_EMAIL"].ToString();
                Customizabledt.RB_CHK_IN_TIME = sdr["RB_CHK_IN_TIME"].ToString();
                Customizabledt.RB_CHK_OUT_TIME = sdr["RB_CHK_OUT_TIME"].ToString();
                Customizabledt.STA_TITLE = sdr["STA_TITLE"].ToString();

                Customizabledt.RB_FRM_TIME = sdr["RB_FRM_TIME"].ToString();
                Customizabledt.RB_TO_TIME = sdr["RB_TO_TIME"].ToString();

                Customizabledt.RB_REFERENCE_ID = sdr["RB_REFERENCE_ID"].ToString();

                Customizabledt.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                Customizabledt.band = sdr["band"].ToString();
                Customizabledt.rf_cost = sdr["rf_cost"].ToString();
                Customizabledt.rb_remarks = sdr["rb_remarks"].ToString();
                Customizabledt.noofnights = sdr["noofnights"].ToString();
                Customizabledt.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                Custlstdt.Add(Customizabledt);
            }
        }
        return Custlstdt;
    }
}