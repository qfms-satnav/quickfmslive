﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ViewAndModifyController : ApiController
{
    ViewAndModifyService vmservice = new ViewAndModifyService();

    [HttpPost]
    public HttpResponseMessage GetBookedRequests([FromBody] QuickSelect qs)
    {
        var obj = vmservice.GetBookedRequests(qs);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetBookedEvent(string screenType, string LocationCode, string RB_REQ_ID, string start, string end)
    {
        var resp = vmservice.GetBookedEvent(screenType, LocationCode,  RB_REQ_ID, start, end);
        var eventList = from e in resp
                        select new
                        {

                            title = e.title,
                            start = e.start,
                            end = e.end,
                            allDay = true,
                            className = e.className,
                            referenceID = e.ReferenceID,
                            reservedFor = e.ReservedFor,
                            remarks = e.Remarks,
                            AUR_NAME = e.AUR_NAME,
                            //CNY_NAME = e.CNY_NAME,
                            //CTY_NAME = e.CTY_NAME,
                            //LCM_NAME = e.LCM_NAME,
                          //  BOOKED_RR_NAME = e.BOOKED_RR_NAME,
                            RB_TYPE = e.RB_TYPE
                        };
        var rows = eventList.ToArray();

        var response = Request.CreateResponse(HttpStatusCode.OK, resp);
        return response;
    }

  

    [HttpPost]
    public HttpResponseMessage UpdateBookedRequest(AdminBookingVM obj)
    {
        var retMsg = vmservice.UpdateBookedRequest(obj);
        if (retMsg != null)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retMsg);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Guest House is already booked between your selected dates.");
        }

    }


    [HttpPost]
    public HttpResponseMessage GetRequestDetails(string reqid)
    {
        var obj = vmservice.GetRequestDetails(reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
