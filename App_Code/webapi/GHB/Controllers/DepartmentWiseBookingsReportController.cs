﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;


public class DepartmentWiseBookingsReportController : ApiController
{
    DepartmentWiseBookingsReportService deptwsBookingService = new DepartmentWiseBookingsReportService();


    [HttpPost]
    public async Task<HttpResponseMessage> GetPerVsofficalReportdata([FromBody]DeptWiseBookingVM deptwdata)
    {
        ReportGenerator<DeptWiseBookingDetails> reportgen = new ReportGenerator<DeptWiseBookingDetails>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/GuestHouse_Mgmt/DepartmentWiseBookingsReport.rdlc"),
            DataSetName = "DepartmentWiseBookingsDS",
            ReportType = "Department Wise Bookings Report"
            
        };

        List<DeptWiseBookingDetails> reportdata = deptwsBookingService.Getdetails(deptwdata);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/DepartmentWiseBookingsReport." + deptwdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, deptwdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "DepartmentWiseBookingsReport." + deptwdata.Type;
        return result;
    }
    [HttpPost]
    public HttpResponseMessage GetGuesthousedetails(DeptWiseBookingVM deptbk)
    {
        var obj = deptwsBookingService.Getdetails(deptbk);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


  

}

