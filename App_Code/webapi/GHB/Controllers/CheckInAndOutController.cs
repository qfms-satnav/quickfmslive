﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class CheckInAndOutController : ApiController
{
    CheckInAndOutService chkinoutservice = new CheckInAndOutService();

    [HttpPost]
    public HttpResponseMessage GetChkInOutDetails([FromBody] QuickSelect qs)
    {
        var obj = chkinoutservice.GetDetails(qs);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetDetailsByReqId(string reqId)
    {
        var obj = chkinoutservice.GetDetailsByReqId(reqId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SubmitCheckInOut(CheckInOutGrid obj)
    {
        var retMsg = chkinoutservice.SubmitCheckInOut(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
