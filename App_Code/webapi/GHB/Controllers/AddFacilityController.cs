﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for AddReservationController
/// </summary>
public class AddFacilityController : ApiController
{

    AddFacilityService service = new AddFacilityService();


    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<AddFacilityVM> griddata = service.GetGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetRoomDetails(AddFacilityVM obj)
    {
        IEnumerable<AddRoomVM> griddata = service.GetRoomDetails(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Create(AddFacilityVM obj)
    {
        var retobj = service.Create(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retobj);
        return response;

    }
   

    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = service.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpGet]
    //public HttpResponseMessage DownloadFile([FromUri] string fileName)
    //{
    //    HttpResponseMessage result = null;
    //    var localFilePath = Path.Combine(HttpRuntime.AppDomainAppPath, "GHB\\UploadImages\\" + fileName);

    //    if (!File.Exists(localFilePath))
    //    {
    //        result = Request.CreateResponse(HttpStatusCode.Gone);
    //    }
    //    else
    //    {
    //        // Serve the file to the client
    //        //result = Request.CreateResponse(HttpStatusCode.OK);
    //        //result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
    //        //result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
    //        //result.Content.Headers.ContentDisposition.FileName = fileName;
    //        result = new HttpResponseMessage(HttpStatusCode.OK);
    //        var stream = new FileStream(localFilePath, FileMode.Open);
    //        result.Content = new StreamContent(stream);
    //        result.Content.Headers.ContentType =
    //        new MediaTypeHeaderValue("application/octet-stream");
    //    }

    //    return result;
    //}
   
}