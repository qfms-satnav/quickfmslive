﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;


public class PerVSOfficalBookingReportController : ApiController
{
    PerVSOfficalBookingReportService peroffservice = new PerVSOfficalBookingReportService();


    [HttpPost]
    public async Task<HttpResponseMessage> GetPerVsofficalReportdata([FromBody]PerVSOfficalBookingVM Peroffdata)
    {
        ReportGenerator<PervsoffDetails> reportgen = new ReportGenerator<PervsoffDetails>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/GuestHouse_Mgmt/PervsofficalReport.rdlc"),
            DataSetName = "PervsofficalReportDS",
            ReportType = "Personal VS Offical Booking Report"
        };

        List<PervsoffDetails> reportdata = peroffservice.Getdetails(Peroffdata);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/PervsofficalReport." + Peroffdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, Peroffdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "PervsofficalReport." + Peroffdata.Type;
        return result;
    }

    [HttpPost]
    public HttpResponseMessage GetGuesthousedetails(PerVSOfficalBookingVM PVSOB)
    {
        var obj = peroffservice.Getdetails(PVSOB);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetPersonalVSOfficialChart(PerVSOfficalBookingVM deptbk)
    {
        var obj = peroffservice.GetPersonalVSOfficialChart(deptbk);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}

