﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class ViewAvailabilityController : ApiController
{
    ViewAvailabilityService vabService = new ViewAvailabilityService();
    ViewAvailabilityReportView report = new ViewAvailabilityReportView();

    [HttpPost]
    public HttpResponseMessage GetViewAvailabilityDetails(ViewAvailabilityDetails vabData)
    {
        var obj = vabService.GetViewAvailableObject(vabData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpGet]
    //public HttpResponseMessage GetReservationTypes([FromUri] int id)
    //{
    //    var obj = vabService.GetReservationTypes(id);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    [HttpPost]
    public async Task<HttpResponseMessage> GetViewAvailabilityData([FromBody]ViewAvailabilityDetails data)
    {
        ReportGenerator<ViewAvailabilityData> reportgen = new ReportGenerator<ViewAvailabilityData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/GuestHouse_Mgmt/ViewAvailability.rdlc"),
            DataSetName = "ViewAvailabilityDS",
            ReportType = "View Availability"
        };

        vabService = new ViewAvailabilityService();
        List<ViewAvailabilityData> reportdata = vabService.GetViewAvailableDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ViewAvailability." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ViewAvailability." + data.Type;
        return result;
    }

    [HttpPost]
    public HttpResponseMessage GetAvailableNBookedRoomsCount(ViewAvailabilityDetails vabData)
    {
        var obj = vabService.GetAvailableNBookedRoomsCount(vabData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetAvailableNBookedRoomsCountWithoutSession(ViewAvailabilityDetails vabData)
    {
        var obj = vabService.GetAvailableNBookedRoomsCountWithoutSession(vabData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage BookRoomsWithoutSession([FromBody] ViewAvailabilityDetails vabData)
    {
        var obj = vabService.BookRoomsWithoutSession(vabData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
