﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ViewAndModifyWithholdController : ApiController
{
    ViewAndModifyWithholdService vmservice = new ViewAndModifyWithholdService();

    [HttpGet]
    public HttpResponseMessage GetBookedRequests([FromUri] string ScreenType)
    {
        var obj = vmservice.GetBookedRequests(ScreenType);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetBookedEvent(string RB_REQ_ID, string LocationCode, string screenType, string start, string end)
    {
        // AdminBookingDetails ad = new AdminBookingDetails();
        var resp = vmservice.GetBookedEvent(RB_REQ_ID, LocationCode, screenType, start, end);

        var eventList = from e in resp
                        select new
                        {

                            title = e.title,
                            start = e.start,
                            end = e.end,
                            allDay = true,
                            className = e.className,
                            referenceID = e.ReferenceID,
                            reservedFor = e.ReservedFor,
                            remarks = e.Remarks,
                            AUR_NAME = e.AUR_NAME,
                            RB_TYPE = e.RB_TYPE
                        };
        var rows = eventList.ToArray();

        var response = Request.CreateResponse(HttpStatusCode.OK, resp);
        return response;
    }

   
    [HttpPost]
    public HttpResponseMessage UpdateBookedRequest(WithholdVM obj)
    {
        var retMsg = vmservice.UpdateBookedRequest(obj);
        if (retMsg != null)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, retMsg);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Guest House is already booked between your selected dates.");
        }

    }


    [HttpPost]
    public HttpResponseMessage GetRequestDetails(string reqid)
    {
        var obj = vmservice.GetRequestDetails(reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
