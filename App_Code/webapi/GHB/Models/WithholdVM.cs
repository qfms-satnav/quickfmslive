﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class WithholdDetails
{
    public List<Locationlst> loclst { get; set; }
    public List<FacilityNamelst> rflist { get; set; }
    public string Request_Type { get; set; }
    public int STAT { get; set; }
}

public class WithholdVM
{
    public WithholdMain obj { get; set; }
    public List<WithholdDetailslst> objList { get; set; }
    
}


public class WithholdMain
{
    public string WM_REQ_ID { get; set; }
    public int WM_TYPE { get; set; }
    public string WM_TITLE { get; set; }
    public string WM_RESERVED_FOR { get; set; }
    public string WM_REFRERENCE_ID { get; set; }
    public string WM_FROM_DATE { get; set; }
    public string WM_TO_DATE { get; set; }
    public string WM_FROM_TIME { get; set; }
    public string WM_TO_TIME { get; set; }
    public string WM_REMARKS { get; set; }
    public int WM_STA_ID { get; set; }
    public string WM_LCM_CODE { get; set; }
    public string WM_CTY_CODE { get; set; }
    public string WM_CNY_CODE { get; set; }

}

public class WithholdDetailslst
{
    public string WD_WM_SNO { get; set; }
    public string WD_REQ_ID { get; set; }
    public string WD_RT_SNO { get; set; }
    public string WD_RF_SNO { get; set; }
    public string WD_RR_SNO { get; set; }
    public string WD_STA_ID { get; set; }

}


public class WithholdGrid
{
    public string COUNTRY { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string RT_NAME { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RRDT_FILE_NAME { get; set; }
    public string RR_RT_SNO { get; set; }
    public string RR_RF_SNO { get; set; }
    public string RR_SNO { get; set; }

}




