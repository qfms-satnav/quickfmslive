﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserGuestHouseUltVM
/// </summary>
public class DailyOccupancyVM
{
    public string Date { get; set; }
    public List<Locationlst> lcmlst { get; set; }
    public string Status { get; set; }
    public string Type { get; set; }
    public string CompanyID { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }

}

public class DailyOccupancyDetailsGrid
{
    public string RB_REQ_ID { get; set; }
    public string RT_NAME { get; set; }
    public DateTime RB_FROM_DATE { get; set; }
    public DateTime RB_TO_DATE { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    
    public string RBS_CTY_CODE { get; set; }
    public string RBS_LOC_CODE { get; set; }
    public string DEP_NAME { get; set; }
    public string RB_CREATEDBY { get; set; }
    public DateTime RB_CREATEDON { get; set; }
    public string RB_RESERVED_FOR { get; set; }
  
    public string STA_TITLE { get; set; }
    public string RESVERED_FOR_EMAIL { get; set; }
    public string RESVERED_BY_EMAIL { get; set; }


    public Nullable<DateTime> RB_CHK_IN_DATE { get; set; }
    public Nullable<DateTime> RB_CHK_OUT_DATE { get; set; }


    public string RB_CHK_IN_TIME { get; set; }
        
    public string RB_CHK_OUT_TIME { get; set; }
    
    public string RB_REFERENCE_ID { get; set; }
    public string Cost_Center_Name { get; set; }

    public string OVERALL_COUNT { get; set; }

}
