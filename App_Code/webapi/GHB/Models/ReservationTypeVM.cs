﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReservationTypeVM
/// </summary>
public class ReservationTypeVM
{
    public int RT_SNO { get; set; }
    public string RT_NAME { get; set; }
    public string RT_STATUS { get; set; }
    public string RT_REMARKS { get; set; }
    public string RT_CREATEDON { get; set; }
    public string RT_MODIFIEDON { get; set; }
}