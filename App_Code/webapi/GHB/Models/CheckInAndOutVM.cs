﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CheckInAndOutVM
/// </summary>

public class ChkInOutDetails
{
    public string RB_REQ_ID { get; set; }

    public string RT_NAME { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }

    public DateTime RB_FROM_DATE { get; set; }
    public DateTime RB_TO_DATE { get; set; }
    public string RR_CODE { get; set; }

    public string RB_FRM_TIME { get; set; }
    public string RB_TO_TIME { get; set; }

    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }

    public string RF_CNY_CODE { get; set; }
    public string RF_CTY_CODE { get; set; }
    public string RF_LOC_CODE { get; set; }


    public string RESERVED_FOR_EMAIL { get; set; }
    public string RESERVED_FOR { get; set; }
    public string RESERVED_BY_EMAIL { get; set; }
    public string RESERVED_BY { get; set; }
    public DateTime RESERVED_DT { get; set; }
    public string RB_REFERENCE_ID { get; set; }
    public string CHKSTATUS { get; set; }

    public string RB_SNO { get; set; }



}

public class CheckInOutGrid
{
    public string BD_STA_ID { get; set; }
    public string RB_REQ_ID { get; set; }
    public string RB_SNO { get; set; }
    public string RR_NAME { get; set; }
    public string RB_RESERVED_FOR { get; set; }
    public string RB_EMP_TYPE { get; set; }
    public string RB_EMP_NAME { get; set; }
    public string RB_EMP_EMAIL { get; set; }
    public string RB_REQ_ID_CC { get; set; }
    public string RB_STA_ID { get; set; }
    public string RB_CHK_IN_TIME { get; set; }
    public string RB_CHK_OUT_TIME { get; set; }
    public string RB_CHK_IN_OUT_STA_ID { get; set; }
    public string Type { get; set; }
    public string RB_REMARKS { get; set; }
    public DateTime RB_FROM_DATE { get; set; }
    public DateTime RB_TO_DATE { get; set; }
    public string RB_CHK_IN_DATE { get; set; }
    public string RB_CHK_OUT_DATE { get; set; }
    public string RB_MOBILE_NUMBER { get; set; }

}

public class SaveCheckInOutData
{
    public string RB_REQ_ID { get; set; }
    public string checkinout { get; set; }
    public string Time { get; set; }
    public string Remarks { get; set; }
    public string Type { get; set; }

}
