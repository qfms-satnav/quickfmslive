﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DeptWiseBookingVM
/// </summary>
public class DeptWiseBookingVM
{
//    public Nullable<System.DateTime> FromDate { get; set; }
//    public Nullable<System.DateTime> ToDate { get; set; }
    public List<DeptLoclst> lcmlst { get; set; }
    public string Status { get; set; }
    public string Booking_Type { get; set; }
    public string Type { get; set; }
    public string Month { get; set; }
    public string Year { get; set; }
    public string CompanyID { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}

public class DeptLoclst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class DeptWiseBookingDetails
{
    public string RT_NAME { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RR_CAPCITY { get; set; }
    public string Cost_Center_Name { get; set; }
    public string DEP_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string WEEK_1 { get; set; }
    public string WEEK_2 { get; set; }
    public string WEEK_3 { get; set; }
    public string WEEK_4 { get; set; }
    public string WEEK_5 { get; set; }
    public string WEEK_6 { get; set; }
    public string OVERALL_COUNT { get; set; }

}