﻿using System;
using System.Collections.Generic;


/// <summary>
/// Summary description for CustomizableRptVM
/// </summary>
public class CustomizableRptVM
{	
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public List<Locationlst> lcmlst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Cnylst> cnylst { get; set; }
    public List< SPAREPARTDETAILS> SPAREPARTDETAILS { get; set; }
    public string Status { get; set; }
    public string Type { get; set; }
    public string CompanyID { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string AAS_SPAREPART_NAME { get; set; }
    public string AAS_SPAREPART_DES { get; set; }
    public string AAS_MODEL_NAME { get; set; }
    public string AAS_SUB_NAME { get; set; }
    public string AAS_SPAREPART_AVBQUANTITY { get; set; }
    public string AAS_SPAREPART_MINQUANTITY { get; set; }
    public string AAS_CTY_CODE { get; set; }
    public string AAS_LOC_ID { get; set; }
    public string AAS_TOWER_CODE { get; set; }
    public string AVR_NAME { get; set; }
    public string AAS_AAB_NAME { get; set; }
    public string AAS_SPAREPART_COST { get; set; }
    public string AAS_AAT_CODE { get; set; }
    public string AAS_ID { get; set; }
    public string VT_TYPE { get; set; }
    public string VER_NAME { get; set; }
    public string Cost_Center_Name { get; set; }
    public string LCM_NAME { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string UGC_NAME { get; set; }
    public string UGC_CODE { get; set; }
    public string AAS_RACK_NO { get; set; }
    public string AAS_VED { get; set; }
    public string AAS_FSN { get; set; }
    public string AAS_AVR_CODE { get; set; }
    public string AAS_SPAREPART_ID { get; set; }
    public string AAS_ASP_CODE { get; set; }
    public string UDM_AUR_ID { get; set; }
    public string ASP_REQ_ID_DTLS { get; set; }
    public string Aur_ID { get; set; }
    public string BDMP_PLAN_ID { get; set; }
    public string Ticket { get; set; }
    public string LCM_CODE { get; set; }
    public string ASP_REQ_LOC { get; set; }
    public string ASP_REMARKS { get; set; }
    public string ASP_AUR_ID { get; set; }
    public string COMPANYID { get; set; }
    public string FILENAME { get; set; }
    public string ASP_MAINT_ID { get; set; }
    public string COST_OF_INVENTORY { get; set; }
    public string TYPE { get; set; }
    public string ASS_LEAD_TIME { get; set; }
    public string AAS_SPAREPART_QUANTITY { get; set; }
    public string AAS_REMARKS { get; set; }
    public string AAS_SPAREPART_NUMBER { get; set; }
}

public class CustomizableRpt
{
    public string AST_SUBCAT_NAME { get; set; }
    public string VT_TYPE { get; set; }
    public string LCM_NAME { get; set; }
    public string manufacturer { get; set; }
    public string AST_MD_NAME { get; set; }
    public string AVR_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string UGC_NAME { get; set; }

    public string FromDate { get; set; }
    public string ToDate { get; set; }

}
public class clsBreakDownReport
{
    public string SER_REQ_ID { get; set; }
    public string LCM_NAME { get; set; }
    public string EUIPEMENT { get; set; }
    public string IMPCT_NAME { get; set; }
    public string OtherErrorDesc { get; set; }
    public string ProblemDescOwn { get; set; }
    public string OtherProblemOwner { get; set; }
    public string Status { get; set; }
    public string SER_CREATED_DT { get; set; }
    public string SER_CREATED_BY { get; set; }
    public string SER_UPDATED_DT { get; set; }
    public string SER_UPDATED_BY { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
    public string DOWNTIME { get; set; }
    public string DOWNTIMEFACTOR { get; set; }
    public string NoOfFreq { get; set; }
    public string TicketAge { get; set; }
    public string RootCause { get; set; }
    public string RCADESC { get; set; }
    public string RCAAge { get; set; }
    public string BDMP_SHIFT { get; set; }
    public string PERMNT_FIX { get; set; }
    public string PreventiveAction { get; set; }
    public string ProductionImp { get; set; }
    public string BREACHED { get; set; }
    public string AssignTo { get; set; }
    public string ProbCat { get; set; }
    public string AdditionalBreach { get; set; }
    public string Remarks { get; set; }
    /// /////////////RCA//////////////  
    public string RCA_DESCRIPTION { get; set; }
    public string RCA_STATUS { get; set; }
    public string RCAPATH { get; set; }
    /////////////////Downtime////////////////
    public string BDMP_PLAN_FDATE { get; set; }
    public string FromTimeR { get; set; }
    public string BDMP_PLAN_TDATE { get; set; }
    public string ToTimeR { get; set; }
    public string BDMP_DOWNTIME { get; set; }
}

public class Lcmlst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class Custcnylst
{
    public string CNY_CODE { get; set; }
    public string CNY_NAME { get; set; }
    public bool ticked { get; set; }
}

public class Ctylst
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class CustomizableRpteDetails
{
    public string RB_REQ_ID { get; set; }
    public string RT_NAME { get; set; }
    public DateTime RB_FROM_DATE { get; set; }
    public DateTime RB_TO_DATE { get; set; }

    public string RB_FRM_TIME { get; set; }
    public string RB_TO_TIME { get; set; }

    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RR_CAPCITY { get; set; }
    public string RBS_CNY_CODE { get; set; }
    public string RBS_CTY_CODE { get; set; }
    public string RBS_LOC_CODE { get; set; }
    public string DEP_NAME { get; set; }
    public string RB_CREATEDBY { get; set; }
    public Nullable<DateTime> RB_CREATEDON { get; set; }
    public string RB_RESERVED_FOR { get; set; }

    public string STA_TITLE { get; set; }
    public string RESVERED_FOR_EMAIL { get; set; }
    public string RESVERED_BY_EMAIL { get; set; }

    public string RB_CHK_IN_TIME { get; set; }
    public string RB_CHK_OUT_TIME { get; set; }

    public string RB_REFERENCE_ID { get; set; }
    public string Cost_Center_Name { get; set; }
    public string band { get; set; }
    public string rf_cost { get; set; }
    public string bt_name { get; set; }
    public string rb_remarks { get; set; }
    public string noofnights { get; set; }
    public string OVERALL_COUNT { get; set; }
}

public class ValidateSSpare
{
    public string AUR_ID { get; set; }
    public string REMARKS { get; set; }
    public string TYPE { get; set; }
    public string REQ_ID { get; set; }
}

//public class AddSpare
//{
//    public string AAS_SPAREPART_NAME { get; set; }
//    public string AAS_SPAREPART_DES { get; set; }
//    public string AAS_SUB_NAME { get; set; }
//    public string AAS_SPAREPART_AVBQUANTITY { get; set; }
//    public string AAS_SPAREPART_QUANTITY { get; set; }
//    public string AAS_SPAREPART_MINQUANTITY { get; set; }
//    public string AAS_REMARKS { get; set; }
//    public string AAS_VED { get; set; }
//    public string AAS_FSN { get; set; }
//    public string AAS_RACK_NO { get; set; }
//    public string AAS_IMAGE_PATH { get; set; }
//    public string AAS_LOC_ID { get; set; }
//    public string AAS_CTY_CODE { get; set; }
//    public string AAS_AAB_NAME { get; set; }
//    public string AAS_AVR_CODE { get; set; }
//    public string AAS_SPAREPART_COST { get; set; }

//}
public class AddSpare
{
    public string ID { get; set; }
    public string City { get; set; }
    public string LocationID { get; set; }
    public string Part_number { get; set; }
    public string SparePartName { get; set; }
    public string SparePart_Description { get; set; }
    public string Storage { get; set; }
    public string SpareType { get; set; }
    public string Brand { get; set; }
    public string Equipment { get; set; }
    public string Vendor { get; set; }
    public string Remarks { get; set; }
    public string Priority { get; set; }
    public string Available_Qty { get; set; }
    public string Measure { get; set; }
    public string Item_Cost { get; set; }
    public string Minimum_Quantity { get; set; }
    public string Lead_Time { get; set; }
    public string AAS_AAT_CODE { get; set; }
    public string AVR_NAME { get; set; }
    public string AAS_SPAREPART_NUMBER { get; set; }
    public string ASS_LEAD_TIME { get; set; }
    public string AAS_SPAREPART_ID { get; set; }
    public string AAS_ASP_CODE { get; set; }
    public string Location { get; set; }
}