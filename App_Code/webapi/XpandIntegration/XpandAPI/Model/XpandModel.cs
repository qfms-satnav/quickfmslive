﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class Emplist
{
    public List<EMP_ID_LIST> emp_list;

}
public class EMP_ID_LIST
{
    public string EMP_ID { get; set; }
    public string FIRST_NAME { get; set; }
    public string MIDDLE_NAME { get; set; }
    public string LAST_NAME { get; set; }
    public string Email_ID { get; set; }
    public string RM_ID { get; set; }
    public string ROLL_TYPE { get; set; }
    public string LCM_NAME { get; set; }
    public string DSN { get; set; }
    public string DEPT { get; set; }
    public string DOB { get; set; }
    public string DOJ { get; set; }
    public string CNY { get; set; }
    public string CTY { get; set; }
    public string GRADE { get; set; }
    public string PARENT_ENTITY { get; set; }
    public string CHILD_ENTITY { get; set; }
    public string BU { get; set; }
    public string LOB { get; set; }
    public string EOD { get; set; }
}