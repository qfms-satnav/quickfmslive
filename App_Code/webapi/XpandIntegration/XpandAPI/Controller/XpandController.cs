﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using System.Data;

public class XpandController : ApiController
{
    XpandService XpandSvc = new XpandService();

    //Employee List
    [HttpPost]
    public HttpResponseMessage EmployeeList(EMP_ID_LIST list)
    {
        var obj = XpandSvc._EmployeeList(list);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
