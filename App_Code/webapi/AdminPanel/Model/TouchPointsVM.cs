﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TouchPointsVM
/// </summary>
public class TouchPointsVM
{
    public String Category { get; set; }
    public String[] Catarray { get; set; }
}

public class SortingPagingInfo
{
    public string SortField { get; set; }
    public string SortDirection { get; set; }
    public int PageSize { get; set; }
    public int PageCount { get; set; }
    public int CurrentPageIndex { get; set; }
    public int TotalCount { get; set; }
}