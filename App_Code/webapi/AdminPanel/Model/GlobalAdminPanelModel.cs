﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GlobalAdminPanelModel
/// </summary>
public class GlobalAdminPanelModel
{
	public GlobalAdminPanelModel()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public class ExtensionViewModel
    {
        public string TenName { get; set; }
        public DateTime ExtDate { get; set; }
    }

    public class ExtensionEmpCountModel
    {
        public string TenName { get; set; }
        public int ExtCount { get; set; }
    }
}