﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AdminfunctionsService
/// </summary>
public class AdminfunctionsService
{
    SubSonic.StoredProcedure sp;
    DataSet ds = new DataSet();
    public DataTable GetRoledata()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AMT_GetRoleDtls_SP");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetModulesByUserId()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MODULES_BY_USER_ID");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public string InsertUserRole()
    {
        return "Success";

    }


    public DataSet GetDashboardHelpdeskDetails()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_STS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@CMP_ID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        return sp.GetDataSet();
    }
    public DataSet GetEmployeeSpaceDetails()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_USERDETAILS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        return sp.GetDataSet();
    }
    public DataSet GetPropertyDetails()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GETPROPERTY_DTLS_DASHBOARD");
        sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
        return sp.GetDataSet();
    }
    public DataSet GetAssetDetails()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AM_GET_EMPLOYEEASSETMAPPED_DB");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
        return sp.GetDataSet();
    }
    public DataSet GetTotalClosedReq()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_TOTAL_CLOSEDREQ");
        return sp.GetDataSet();
    }
    public DataSet GetConfBookingDtls()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_EMP_CONFBKD_DASHBOARD");
        sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
        return sp.GetDataSet();
    }
    public DataSet GetAMCDetails()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GETAMCTYPES_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        return sp.GetDataSet();
    }
    public DataSet GetExpiryLeases()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_Expiry_Leases_Dashboard");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        return sp.GetDataSet();
    }
    public DataSet GetAssetsExpCount()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ASSETS_REQUIRED_STOCK_DASHBOARD");
        return sp.GetDataSet();
    }
    public DataSet GetTotalrequests()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_TOTAL_REQUESTS");
        return sp.GetDataSet();
    }
}