﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GlobalAdminPanelService
/// </summary>
public class GlobalAdminPanelService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
	public GlobalAdminPanelService()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public object[][] GetAllCountryCustomers()
    {
        sp = new SubSonic.StoredProcedure("GET_TENANTS_ADMINPANEL");
        ds = sp.GetDataSet();
        object[][] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }

    public object[][] GetCustomerwiseempcount()
    {
        sp = new SubSonic.StoredProcedure("GET_CUSTOMERWISE_EMPLOYEE_COUNT_ADMINPANEL");
        ds = sp.GetDataSet();
        object[][] arr1 = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr1;
    }

    public DataTable GetCustomertranscationcount()
    {
        sp = new SubSonic.StoredProcedure("GET_TRANSCATIONS_COUNT_BYTENANT_ADMINPANEL");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
        
    public DataTable GettotalCustDetails()
    {
        sp = new SubSonic.StoredProcedure("GET_ACTIVE_INACTIVE_CUSTOMERS_INFO_ADMINPANEL");
        sp.Command.Parameters.Add("@TYPE", "TOTAL", DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable Get14DaysTrialCustDetails()
    {
        sp = new SubSonic.StoredProcedure("GET_ACTIVE_INACTIVE_CUSTOMERS_INFO_ADMINPANEL");
        sp.Command.Parameters.Add("@TYPE", "14DAYS", DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable Get14daystrialexpcustDetails()
    {
        sp = new SubSonic.StoredProcedure("GET_ACTIVE_INACTIVE_CUSTOMERS_INFO_ADMINPANEL");
        sp.Command.Parameters.Add("@TYPE", "14DAYSTREXP", DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetSubscribedCustDetails()
    {
        sp = new SubSonic.StoredProcedure("GET_ACTIVE_INACTIVE_CUSTOMERS_INFO_ADMINPANEL");
        sp.Command.Parameters.Add("@TYPE", "SUBSCRIBED", DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetSubscribedInactCustDetails()
    {
        sp = new SubSonic.StoredProcedure("GET_ACTIVE_INACTIVE_CUSTOMERS_INFO_ADMINPANEL");
        sp.Command.Parameters.Add("@TYPE", "SUBSCRIBEDINACTIVE", DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetCustomeremplimit()
    {
        sp = new SubSonic.StoredProcedure("GET_CUSTOMERWISE_EMP_COUNT_EXT_ADMINPANEL");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public void UpdatetrialversExpCustDetails(GlobalAdminPanelModel.ExtensionViewModel obj)
    {
        try
        {
            SubSonic.StoredProcedure sp;
            sp = new SubSonic.StoredProcedure("UPDATE_EXTDATE_CUSTOMERS_ADMINPANEL");
            sp.Command.Parameters.Add("@TenName", obj.TenName, DbType.String);
            sp.Command.Parameters.Add("@ExtDate", obj.ExtDate, DbType.Date);
            sp.Command.Parameters.Add("@Type", "ExtDate", DbType.String);
            sp.ExecuteScalar();
        }
        catch (SqlException ex)
        {
            Console.WriteLine("SQL Error" + ex.Message.ToString());
        }
    }

    public void UpdateCustEmployeeCount(GlobalAdminPanelModel.ExtensionEmpCountModel obj)
    {
        try
        {
            SubSonic.StoredProcedure sp;
            sp = new SubSonic.StoredProcedure("UPDATE_EXTDATE_CUSTOMERS_ADMINPANEL");
            sp.Command.Parameters.Add("@TenName", obj.TenName, DbType.String);
            sp.Command.Parameters.Add("@EmpCount", obj.ExtCount, DbType.Int32);
            sp.Command.Parameters.Add("@Type", "EmpCount", DbType.String);
            sp.ExecuteScalar();
        }
        catch (SqlException ex)
        {
            Console.WriteLine("SQL Error" + ex.Message.ToString());
        }
    }

    public DataTable GetTotalCustomersModuleWise()
    {
        sp = new SubSonic.StoredProcedure("GET_CUSTOMERS_MODULE_COUNT_ADMINPANEL");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetCustomersByModules()
    {
        sp = new SubSonic.StoredProcedure("GET_CUSTOMERS_BY_MODULES_ADMINPANEL");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

}