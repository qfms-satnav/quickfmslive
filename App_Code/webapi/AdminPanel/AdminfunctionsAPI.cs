﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class AdminfunctionsAPIController : ApiController
{
    AdminfunctionsService afs = new AdminfunctionsService();
    public DataTable GetRoledata()
    {
        return afs.GetRoledata();
    }
    public DataTable GetModulesByUserID()
    {
        return afs.GetModulesByUserId();
    }

    [HttpPost]
    public string InsertUserRole()
    {
        return afs.InsertUserRole();
    }
    [HttpGet]
    public DataSet GetDashboardHelpdeskDetails()
    {
        return afs.GetDashboardHelpdeskDetails();
    }
    [HttpGet]
    public DataSet GetEmployeeSpaceDetails()
    {
        return afs.GetEmployeeSpaceDetails();
    }
    [HttpGet]
    public DataSet GetPropertyDetails()
    {
        return afs.GetPropertyDetails();
    }
    [HttpGet]
    public DataSet GetAssetDetails()
    {
        return afs.GetAssetDetails();
    }
    [HttpGet]
    public DataSet GetTotalClosedReq()
    {
        return afs.GetTotalClosedReq();
    }
    [HttpGet]
    public DataSet GetConfBookingDtls()
    {
        return afs.GetConfBookingDtls();
    }
    [HttpGet]
    public DataSet GetAMCDetails()
    {
        return afs.GetAMCDetails();
    }
    [HttpGet]
    public DataSet GetExpiryLeases()
    {
        return afs.GetExpiryLeases();
    }
    [HttpGet]
    public DataSet GetAssetsExpCount()
    {
        return afs.GetAssetsExpCount();
    }
    [HttpGet]
    public DataSet GetTotalrequests()
    {
        return afs.GetTotalrequests();
    }
}
