﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class AMCWorkOrderVM
{
    //public List<Locationlst> selectedLoc { get; set; }
    //public string FromDate { get; set; }
    public string CNP_NAME {get;set;}
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}

public class AMCWorkOrderVMGridList
{

    public string PLANID { get; set; }
    public string PLANFOR { get; set; }
    public string VENDOR { get; set; }
    public string COST { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }

    //public string CATEGORY { get; set; }
    //public string SUB_CATEGORY { get; set; }
    //public string BRAND { get; set; }
    //public string MODEL { get; set; }
    public string LOCATION { get; set; }
    public string OVERALL_COUNT { get; set; }
    //public string Asset_Code { get; set; }
    //public string Asset_Name { get; set; }
    //public string AVR_NAME { get; set; }
    //public string AMN_PLAN_ID { get; set; }
}