﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class MaintenanceData
{
    public string CNP_NAME { get; set; }
    public string PLAN_ID { get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}
public class AssetUnderMaint
{
    public string PLANID { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string AST_MD_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string AVR_NAME { get; set; }
    public string AAT_NAME { get; set; }
    public string AMN_PLAN_ID { get; set; }
    public DateTime? AMN_FROM_DATE { get; set; }
    public DateTime? AMN_TO_DATE { get; set; }
    public string OVERALL_COUNT { get; set; }
}