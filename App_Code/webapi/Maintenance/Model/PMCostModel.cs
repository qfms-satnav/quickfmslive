﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class PMCostVM
{
    public List<Locationlst> selectedLoc { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string CNP_NAME { get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}

public class PMCostModel
{
    public string PVM_PLAN_TYPE { get; set; }
    public string PVM_ASSET_NAME { get; set; }
    public Nullable<System.DateTime> PVD_PLANEXEC_DT { get; set; }
    public string PVD_PLANSPARES_COST { get; set; }
    public string PVD_PLANLABOUR_COST { get; set; }
    public string STA_TITLE { get; set; }
    public string OVERALL_COUNT { get; set; }
}