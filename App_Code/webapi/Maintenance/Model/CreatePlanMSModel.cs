﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CreatePlanMSModel
/// </summary>
public class CreatePlanMSModel
{

}

public class CreatePlanSearch
{
    public List<Locationlst> selectedLoc { get; set; }
    public List<SubTypes> Equipments { get; set; }
    public List<Vendors1> Vendors { get; set; }
    public string SearchValue { get; set; }
}

public class SubTypes
{
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string CAT_CODE { get; set; }
    public bool ticked { get; set; }
}
public class Vendors
{
    public string AVR_CODE { get; set; }
    public string AVR_NAME { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public bool ticked { get; set; }
}

public class Vendors1
{
    public string AVR_CODE { get; set; }
    public string AVR_NAME { get; set; }

    public bool ticked { get; set; }
}

public class CreatePModel
{
    public string AAT_NAME { get; set; }
    public string AAT_CODE { get; set; }
    public string AVR_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string OVERALL_COUNT { get; set; }
    public string AVR_CODE { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public string AST_SUBCAT_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string AMN_FROM_DATE { get; set; }
    public string AMN_TO_DATE { get; set; }
    public string ScheduledDate { get; set; }
    public string PLAN_TYPE { get; set; }
    public string PLAN_TYPE_ID { get; set; }
    public string PVD_PLANSTA_ID { get; set; }
    public string PVD_ID { get; set; }
    public string PVD_PLAN_ID { get; set; }


}