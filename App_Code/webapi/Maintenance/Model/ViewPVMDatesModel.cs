﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



public class PVMDates
{
    public string CNP_NAME { get; set; }
    public string PLAN_ID { get; set; }
    public string Type { get; set; }
}

public class ViewPVMDatesModel
{
    public string Category { get; set; }
    public string Location { get; set; }
    public string Sub_Category { get; set; }
    public string Vendor { get; set; }
    public string PVD_PLANAAT_ID { get; set; }
    public string PVM_PLAN_FREQ { get; set; }    
    public string STA_TITLE { get; set; }
    public DateTime? PVD_PLANSCHD_DT { get; set; }
    
}