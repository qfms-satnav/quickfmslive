﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class MaintenanceCustomizedModel
{
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string Request_Type { get; set; }
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string tag_name { get; set; }
}

public class MainCustReport
{
    public string AAG_MFTYPE { get; set; }
    public string AGT_NAME { get; set; }
    public string AAB_NAME { get; set; }
    public string AAM_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string CNY_NAME { get; set; }
    public string ASSET_CODE { get; set; }
    public string TAG_NAME { get; set; }
    public Nullable<System.DateTime> AAP_WRNT_DATE { get; set; }
    public Nullable<System.DateTime> AMN_TO_DATE { get; set; }
    public Nullable<System.DateTime> AMN_FROM_DATE { get; set; }
    public string AVR_NAME { get; set; }
    public string AMC_STATUS { get; set; }
    public string PVM_STATUS { get; set; }
    public string BRK_STATUS { get; set; }
    public string PLAN_FREQUENCY { get; set; }
    public double PVD_PLANSPARES_COST { get; set; }
    public double PVD_PLANLABOUR_COST { get; set; }
    public String CNP_NAME { get; set; }
    public Nullable<System.DateTime> PVD_PLANSCHD_DT { get; set; }
    public Nullable<System.DateTime> PVD_PLANEXEC_DT { get; set; }
    public String STA_TITLE { get; set; }
    public String PVD_PLAN_REMARKS { get; set; }
    public string OVERALL_COUNT { get; set; }
}
public class MaintReportView
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public MaintReportView()
    {
        SpaceDatas = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> SpaceDatas { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case MaintReportView.ReportFormat.Word: return ".doc";
                case MaintReportView.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.SpaceDatas)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}