﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class CompanyData
{
    public string CNP_NAME { get; set; }
    public string PLAN_ID { get; set; }
    public string Type { get; set; }
}
public class AMCPaymentAdviceModel
{
    public string AMN_PLAN_ID { get; set; }
    public string APM_PAYMEMO_ID { get; set; }
    public string AWO_AMC_COST { get; set; }
    public string LCM_NAME { get; set; }
    public string AVR_NAME { get; set; }
    public string AAT_NAME { get; set; }
    public string APM_BILL_NO { get; set; }
    public DateTime? AMN_FROM_DATE { get; set; }
    public DateTime? AMN_TO_DATE { get; set; }
    public string APM_BILL_DATE { get; set; }
    public string APM_NET_PAYABLE { get; set; }
    public string APM_MPAPAYADVICE_NO { get; set; }
    public string APM_REMARKS { get; set; }
    public string APM_PAY_MODE { get; set; }
    public string CHEQUE { get; set; }

}