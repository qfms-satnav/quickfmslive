﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class Company
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public List<Floorlst> flrlst { get; set; }
    public List<LCMlst> lcmlst { get; set; }
    public List<VER_LST> verlst { get; set; }
    public string CNP_NAME { get; set; }
    public string PLAN_ID { get; set; }
    public string Type { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
   
}

public class AMCPaymentMemoModel
{
    public string AMN_PLAN_ID { get; set; }
    public string APM_PAYMEMO_ID { get; set; }
    public string AWO_AMC_COST { get; set; }
    public string LCM_NAME { get; set; }
    public string AVR_NAME { get; set; }
    public string AAT_NAME { get; set; }
    public string APM_BILL_NO { get; set; }
    public DateTime? AMN_FROM_DATE { get; set; }
    public DateTime? AMN_TO_DATE { get; set; }
    public string APM_BILL_DATE { get; set; }
    public string APM_NET_PAYABLE { get; set; }
    public string APM_MPAPAYADVICE_NO { get; set; }
    public string APM_REMARKS { get; set; }
    public string APM_PAY_MODE { get; set; }

}