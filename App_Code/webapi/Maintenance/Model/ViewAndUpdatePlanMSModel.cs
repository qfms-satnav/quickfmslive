﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ViewAndUpdatePlanMSModel
/// </summary>
public class ViewAndUpdatePlanMSModel
{
    
}

public class ViewUpdateModel
{
    public string AST_ID { get; set; }
    public string AAT_DESC { get; set; }
    public string AVR_NAME { get; set; }
    public string LOCATION { get; set; }
    public string PLAN_TYPE { get; set; }
    public DateTime ScheduledDate { get; set; }
    public string AUR_ID { get; set; }
    public string STA_TITLE { get; set; }
    public string Update { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string LCM_CODE { get; set; }
    public string VT_CODE { get; set; }
    public string AST_SUBCAT_CODE { get; set; }
    public string manufactuer_code { get; set; }
    public string AST_MD_CODE { get; set; }
    public string REMARKS { get; set; }
    public string PVD_ID { get; set; }
    public string FILES { get; set; }

    


}


public class ViewUpdateModel1
{
    public List<ViewUpdateModel> sad { get; set; }
}