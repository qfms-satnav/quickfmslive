﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for CreatePlanMSService
/// </summary>
public class CreatePlanMSService
{
    SubSonic.StoredProcedure sp;
    List<CreatePModel> CRTPlst;
    CreatePModel CRPT;
    DataSet ds;


    public object getdata(CreatePlanSearch CRP)
    {
        try
        {
            CRTPlst = GetCreatePlanDetails(CRP);
            if (CRTPlst.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = CRTPlst };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<CreatePModel> GetCreatePlanDetails(CreatePlanSearch CRP)
    {
        try
        {
            List<CreatePModel> WData = new List<CreatePModel>();
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
            if (CRP.selectedLoc == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(CRP.selectedLoc);
            }
            param[1] = new SqlParameter("@AST_SUBCAT_CODE", SqlDbType.Structured);
            if (CRP.Equipments == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(CRP.Equipments);
            }

            param[2] = new SqlParameter("@VEN_CODE", SqlDbType.Structured);
            if (CRP.Vendors == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(CRP.Vendors);
            }
            param[3] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[3].Value = HttpContext.Current.Session["Uid"].ToString();
            param[4] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[4].Value = CRP.SearchValue;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ALL_ASSETS_WITH_AND_WITHOUT_CONTRACTS", param))
            {
                while (reader.Read())
                {
                    CRPT = new CreatePModel();
                    CRPT.LCM_NAME = reader["LCM_NAME"].ToString();
                    CRPT.AST_SUBCAT_NAME = reader["AST_SUBCAT_NAME"].ToString();
                    CRPT.AVR_NAME = reader["AVR_NAME"].ToString();
                    CRPT.LCM_CODE = reader["LCM_CODE"].ToString();
                    CRPT.AST_SUBCAT_CODE = reader["AST_SUBCAT_CODE"].ToString();
                    CRPT.AVR_CODE = reader["AVR_CODE"].ToString();
                    CRPT.AAT_NAME = reader["AAT_NAME"].ToString();
                    CRPT.AAT_CODE = reader["AAT_CODE"].ToString();                   
                    CRPT.PVD_PLANSTA_ID= reader["PVD_PLANSTA_ID"].ToString();
                    CRPT.AMN_FROM_DATE = reader["AMN_FROM_DATE"].ToString();  //Convert.ToDateTime(reader["AMN_FROM_DATE"].ToString());
                    CRPT.AMN_TO_DATE = reader["AMN_TO_DATE"].ToString();      //Convert.ToDateTime(reader["AMN_TO_DATE"].ToString());
                    CRPT.ScheduledDate = reader["ScheduledDate"].ToString();  //Convert.ToDateTime(reader["ScheduledDate"].ToString());
                    CRPT.PLAN_TYPE = reader["PLAN_TYPE"].ToString();
                    CRPT.PLAN_TYPE_ID = reader["PLAN_TYPE_ID"].ToString();
                    CRPT.PVD_PLAN_ID = reader["PVD_PLAN_ID"].ToString();
                    CRPT.PVD_ID = reader["PVD_ID"].ToString();
                    WData.Add(CRPT);
                }
                reader.Close();
            }
            return WData;
        }
        catch (Exception e) 
        { throw e; }    
    }

    public object CreatePPMPlane(CreatePModel CPM)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@ASSET_ID", SqlDbType.NVarChar);
            param[0].Value = CPM.AAT_CODE;
            param[1] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[1].Value = CPM.LCM_CODE;
            param[2] = new SqlParameter("@AST_SUBCAT_CODE", SqlDbType.NVarChar);
            param[2].Value = CPM.AST_SUBCAT_CODE;
            param[3] = new SqlParameter("@AVR_CODE", SqlDbType.NVarChar);
            param[3].Value = CPM.AVR_CODE;
            param[4] = new SqlParameter("@PLAN_TYPE", SqlDbType.NVarChar);
            param[4].Value = CPM.PLAN_TYPE;
            param[5] = new SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.NVarChar);
            param[5].Value = CPM.ScheduledDate;
            param[6] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[6].Value = HttpContext.Current.Session["UID"];
            param[7] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            param[8] = new SqlParameter("@AMN_FROM_DATE", SqlDbType.NVarChar);
            param[8].Value = CPM.AMN_FROM_DATE;
            param[9] = new SqlParameter("@AMN_TO_DATE", SqlDbType.NVarChar);
            param[9].Value = CPM.AMN_TO_DATE;

            int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CREATE_PALNS_FOR_WITH_AND_WITHOUT_CONTRACTS", param);
            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK, data = CPM.PLAN_TYPE };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    //Update Planes
    public object UpdatePPMPlane(CreatePModel sad)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar);
            param[0].Value = sad.AAT_CODE;
            param[1] = new SqlParameter("@PVD_ID", SqlDbType.NVarChar);
            param[1].Value = sad.PVD_ID;
            param[2] = new SqlParameter("@AST_SUBCAT_CODE", SqlDbType.NVarChar);
            param[2].Value = sad.AST_SUBCAT_CODE;
            param[3] = new SqlParameter("@AVR_CODE", SqlDbType.NVarChar);
            param[3].Value = sad.AVR_NAME;
            param[4] = new SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.DateTime);
            param[4].Value = sad.ScheduledDate;
            param[5] = new SqlParameter("@PLAN_TYPE", SqlDbType.NVarChar);
            param[5].Value = sad.PLAN_TYPE;
            param[6] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[6].Value = HttpContext.Current.Session["UID"];
            param[7] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            param[8] = new SqlParameter("@PVD_PLANSTA_ID", SqlDbType.NVarChar);
            param[8].Value = sad.PVD_PLANSTA_ID;
            param[9] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[9].Value = sad.LCM_CODE;
            int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UPDATE_PPM_PLAN_DATE_CHANGES", param);
            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK, data = sad.PLAN_TYPE };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
        catch (Exception e)
        {
            throw e;
        }
    }


    //Delete Planes
    public object DeletePPMPlane(CreatePModel sad)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar);
            param[0].Value = sad.AAT_CODE;
            param[1] = new SqlParameter("@PVD_ID", SqlDbType.NVarChar);
            param[1].Value = sad.PVD_ID;
            param[2] = new SqlParameter("@AST_SUBCAT_CODE", SqlDbType.NVarChar);
            param[2].Value = sad.AST_SUBCAT_CODE;
            param[3] = new SqlParameter("@AVR_CODE", SqlDbType.NVarChar);
            param[3].Value = sad.AVR_NAME;
            param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            param[5] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["COMPANYID"];
            param[6] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[6].Value = sad.LCM_CODE;
            int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DELETE_PPM_PLAN_DATE_CHANGES", param);
            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK, data = sad.PLAN_TYPE };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
        catch (Exception e)
        {
            throw e;
        }
    }
}