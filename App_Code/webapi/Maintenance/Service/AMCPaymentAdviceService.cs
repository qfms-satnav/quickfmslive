﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;



public class AMCPaymentAdviceService
{
    SubSonic.StoredProcedure sp;
    List<AMCPaymentAdviceModel> MaintList;
    AMCPaymentAdviceModel Maint;
    DataSet ds;
    public object GetMaintDetails(CompanyData AMaint)
    {
        try
        {
            MaintList = PaymentAdviceDetails(AMaint);
            if (MaintList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = MaintList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    public List<AMCPaymentAdviceModel> PaymentAdviceDetails(CompanyData AMaint)
    {
        try
        {
            List<AMCPaymentAdviceModel> MaintData = new List<AMCPaymentAdviceModel>();

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];

            if (AMaint.CNP_NAME == null)
            {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = AMaint.CNP_NAME;
            }

            //param[2] = new SqlParameter("@PLANID", SqlDbType.NVarChar);
            //param[2].Value = AMaint.PLAN_ID;

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GETPAYMENT_ADVICE", param))
            {
                while (reader.Read())
                {
                    DateTime? dt = null;
                    Maint = new AMCPaymentAdviceModel();
                    Maint.AMN_PLAN_ID = reader["AMN_PLAN_ID"].ToString();
                    Maint.APM_PAYMEMO_ID = reader["APM_PAYMEMO_ID"].ToString();
                    Maint.APM_MPAPAYADVICE_NO = reader["APM_MPAPAYADVICE_NO"].ToString();
                    Maint.LCM_NAME = reader["LCM_NAME"].ToString();
                    Maint.AVR_NAME = reader["AVR_NAME"].ToString();
                    Maint.AMN_FROM_DATE = reader["AMN_FROM_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AMN_FROM_DATE"]);
                    Maint.AMN_TO_DATE = reader["AMN_TO_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AMN_TO_DATE"]);
                    Maint.APM_NET_PAYABLE = reader["APM_NET_PAYABLE"].ToString();
                    Maint.APM_BILL_NO = reader["APM_BILL_NO"].ToString();
                    Maint.APM_BILL_DATE = reader["APM_BILL_DATE"].ToString();                    
                    Maint.APM_PAY_MODE = reader["APM_PAY_MODE"].ToString();
                    Maint.AWO_AMC_COST = reader["AWO_AMC_COST"].ToString();
                    Maint.CHEQUE = reader["CHEQUE"].ToString();
                    MaintData.Add(Maint);
                }
                reader.Close();
            }
            return MaintData;
        }
        catch
        {
            throw;
        }
    }
}