﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ViewAndUpdatePlanMSService
/// </summary>
public class ViewAndUpdatePlanMSService
{
    SubSonic.StoredProcedure sp;
    //Grid Data
    public Object getdata(ViewUpdateModel VUM)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@To_Date", SqlDbType.NVarChar);
            param[0].Value = VUM.ToDate;
            param[1] = new SqlParameter("@From_Date", SqlDbType.NVarChar);
            param[1].Value = VUM.FromDate;
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "[AST_GET_FOR_VIEW_AND_UPDATE]", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (Exception e)
        {
            throw e;
        }
    }
    //Update Planes
    public object UpdatePPMPlane(ViewUpdateModel sad)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar);
            param[0].Value = sad.AST_ID;
            param[0] = new SqlParameter("@PVD_ID", SqlDbType.NVarChar);
            param[0].Value = sad.PVD_ID;
            param[1] = new SqlParameter("@VT_CODE", SqlDbType.NVarChar);
            param[1].Value = sad.VT_CODE;
            param[2] = new SqlParameter("@AST_SUBCAT_CODE", SqlDbType.NVarChar);
            param[2].Value = sad.AST_SUBCAT_CODE;
            param[3] = new SqlParameter("@AVR_CODE", SqlDbType.NVarChar);
            param[3].Value = sad.AVR_NAME;
            param[4] = new SqlParameter("@PLAN_TYPE", SqlDbType.NVarChar);
            param[4].Value = sad.PLAN_TYPE;
            param[5] = new SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.DateTime);
            //param[5].Value = sad.FromDate;
            DateTime fromDate = sad.FromDate; // Assuming sad.FromDate is a DateTime variable

            if (fromDate >= SqlDateTime.MinValue.Value && fromDate <= SqlDateTime.MaxValue.Value)
            {
                param[5].Value = fromDate;
            }
            else
            {
                param[5].Value = SqlDateTime.MinValue.Value; // Use the minimum valid DateTime value for SQL Server
            }
            param[6] = new SqlParameter("@To_Date", SqlDbType.DateTime);
            param[6].Value = sad.ToDate;
            param[7] = new SqlParameter("@PVD_PLAN_REMARKS", SqlDbType.NVarChar);
            param[7].Value = sad.REMARKS;
            param[8] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[8].Value = HttpContext.Current.Session["UID"];
            param[9] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[9].Value = HttpContext.Current.Session["COMPANYID"];
            param[10] = new SqlParameter("@PVD_UPLOADED_DOC", SqlDbType.NVarChar);
            param[10].Value = DateTime.Now.ToString("ddMMyyyy").ToString()+sad.FILES;

            int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UPDATE_PPM_PLANS_WITH_DOCUMENTS", param);
            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK, data = sad.REMARKS };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
        catch (Exception e)
        {
            throw e;
        }
    }
}