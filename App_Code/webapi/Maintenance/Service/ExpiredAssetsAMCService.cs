﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class ExpiredAssetsAMCService
{
    SubSonic.StoredProcedure sp;
    List<ExpiredAssetsAMCGridList> gridList;
    ExpiredAssetsAMCGridList gridlstObj;
    DataSet ds;
    public object BindDataToGrid(ExpiredAssetsAMCModel VM)
    {
        try
        {
            gridList = BindDataToGridList(VM);
            if (gridList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = gridList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex) { return new { Message = ex.Message, Info = ex.Message, data = (object)null }; }
    }
    public List<ExpiredAssetsAMCGridList> BindDataToGridList(ExpiredAssetsAMCModel VM) 
    {
        try {
            List<ExpiredAssetsAMCGridList> WData = new List<ExpiredAssetsAMCGridList>();
            SqlParameter[] param = new SqlParameter[7];

            param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            if (VM.selectedLoc == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(VM.selectedLoc);
            }
            param[1] = new SqlParameter("@USER_ID", SqlDbType.VarChar);
            param[1].Value = HttpContext.Current.Session["Uid"].ToString();

            param[2] = new SqlParameter("@COMPANY_ID", SqlDbType.VarChar);
            param[2].Value = VM.CNP_NAME;

            param[3] = new SqlParameter("@AMN_TO_DATE", SqlDbType.VarChar);
            param[3].Value = VM.FromDate;
            param[4] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[4].Value = VM.SearchValue;
            param[5] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[5].Value = VM.PageNumber;
            param[6] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[6].Value = VM.PageSize;



            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MN_AMC_RPT_EXPIRY_DTLS_BY_BDG", param))
            {
                while (reader.Read())
                {
                    gridlstObj = new ExpiredAssetsAMCGridList();
                    gridlstObj.CATEGORY = reader["CATEGORY"].ToString();
                    gridlstObj.MODEL = reader["MODEL"].ToString();
                    gridlstObj.LOCATION = reader["LOCATION"].ToString();
                    gridlstObj.Asset_Name = reader["Asset_Name"].ToString();
                    gridlstObj.VENDOR = reader["VENDOR"].ToString();
                    gridlstObj.FROM_DATE = reader["FROM_DATE"].ToString();
                    gridlstObj.TO_DATE = reader["TO_DATE"].ToString();
                    gridlstObj.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    WData.Add(gridlstObj);
                }
                reader.Close();
            }
            return WData;
        }
        catch(Exception ex)
        {
            throw;
        }
    }
}