﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;



public class ViewPVMDatesService
{
    SubSonic.StoredProcedure sp;
    List<ViewPVMDatesModel> MaintList;
    ViewPVMDatesModel Maint;
    DataSet ds;
    public object GetMaintDetails(PVMDates AMaint)
    {
        try
        {
            MaintList = PPMStatusDetails(AMaint);
            if (MaintList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = MaintList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    public List<ViewPVMDatesModel> PPMStatusDetails(PVMDates AMaint)
    {
        try
        {
            List<ViewPVMDatesModel> MaintData = new List<ViewPVMDatesModel>();

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@USR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];

            if (AMaint.CNP_NAME == null)
            {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = AMaint.CNP_NAME;
            }

            param[2] = new SqlParameter("@FRMDT", SqlDbType.NVarChar);
            param[2].Value = Maint.PVD_PLANSCHD_DT;

            param[3] = new SqlParameter("@TODT", SqlDbType.NVarChar);
            param[3].Value = Maint.PVD_PLANSCHD_DT;


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BIND_GROLE_PVM_STATUS_DETAILS_ALL", param))
            {
                while (reader.Read())
                {
                    DateTime? dt = null;
                    Maint = new ViewPVMDatesModel();
                    Maint.Category = reader["Category"].ToString();
                    Maint.Sub_Category = reader["Sub_Category"].ToString();
                    Maint.Vendor = reader["Vendor"].ToString();
                    Maint.PVD_PLANAAT_ID = reader["PVD_PLANAAT_ID"].ToString();
                    Maint.PVM_PLAN_FREQ = reader["PVM_PLAN_FREQ"].ToString();
                    Maint.PVD_PLANSCHD_DT = reader["PVD_PLANSCHD_DT"] is DBNull ? dt : Convert.ToDateTime(reader["PVD_PLANSCHD_DT"]);                   
                    Maint.STA_TITLE = reader["STA_TITLE"].ToString();
                    Maint.Location = reader["Location"].ToString();
                    MaintData.Add(Maint);
                }
                reader.Close();
            }
            return MaintData;
        }
        catch
        {
            throw;
        }
    }
}