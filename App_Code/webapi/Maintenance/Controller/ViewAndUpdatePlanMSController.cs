﻿using QuickFMS.API.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using UtiltiyVM;

public class ViewAndUpdatePlanMSController : ApiController
{
    ViewAndUpdatePlanMSService VUPM = new ViewAndUpdatePlanMSService();
    SubSonic.StoredProcedure sp;
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getdata([FromBody] ViewUpdateModel VUM)
    {
        var obj = VUPM.getdata(VUM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Update PPM Plans Method
    [GzipCompression]
    [HttpPost]
    public object UpdatePPMPlane(ViewUpdateModel sad)
    {
        return VUPM.UpdatePPMPlane(sad);
    }

    //Image Upload Method
    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;
        string sPath = HttpContext.Current.Session["TENANT"].ToString();
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/Maintenance/");
        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
        if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"].ToString())))
        {
            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"].ToString()));
        }
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                hpf.SaveAs(sPath + Path.GetFileName(DateTime.Now.ToString("ddMMyyyy").ToString() + hpf.FileName));
                iUploadedCnt = iUploadedCnt + 1;            
            }
        }
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }


    //Image Download Method
    static Hashtable ChkExportAuth = new Hashtable();
    [HttpGet]
    public HttpResponseMessage DownloadServiceReport([FromUri] string id)
    {
        HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_UPDATE_PLAN_DOC_DETAILS]");
            sp.Command.AddParameter("@ID", id, DbType.String);
            DataSet ds = sp.GetDataSet();

            string filePath = HttpContext.Current.Server.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"]+"/Maintenance/") + ds.Tables[0].Rows[0]["DFDC_NAME"].ToString();
            //string filePath = "C:\\ContractFileRepository\\" + ds.Tables[0].Rows[0]["DFDC_NAME"].ToString();
            byte[] bytes = System.IO.File.ReadAllBytes(filePath);
            result.Content = new StreamContent(new MemoryStream(bytes));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = ds.Tables[0].Rows[0]["DFDC_ACT_NAME"].ToString();
            ChkExportAuth.Remove(id);
            return result;
        }
        catch (Exception ex)
        {
            if (ex.InnerException != null)
                result = Request.CreateResponse(HttpStatusCode.ExpectationFailed, MessagesVM.ErrorMessage);
            else
                result = Request.CreateResponse(HttpStatusCode.ExpectationFailed, MessagesVM.ErrorMessage);
            return result;
        }
    }


}
