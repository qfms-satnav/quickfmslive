﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;


public class AMCWorkOrdersController : ApiController
{
    AMCWorkOrdersService amcwoService = new AMCWorkOrdersService();

    [HttpPost]
    public HttpResponseMessage BindDataToGrid(AMCWorkOrderVM VM)
    {
        var obj = amcwoService.BindDataToGrid(VM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> ExportReportdata([FromBody]AMCWorkOrderVM CustRpt)
    {
        ReportGenerator<AMCWorkOrderVMGridList> reportgen = new ReportGenerator<AMCWorkOrderVMGridList>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/AMCWorkOrders.rdlc"),
            DataSetName = "AMC_WO",
            ReportType = "Maintenance Contract Work Orders"
        };

        List<AMCWorkOrderVMGridList> reportdata = amcwoService.BindDataToGridList(CustRpt);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/AMCWorkOrders." + CustRpt.Type);
        await reportgen.GenerateReport(reportdata, filePath, CustRpt.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AMC_WO." + CustRpt.Type;
        return result;
    }
}
