﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;


public class ExpiredAssetsAMCController : ApiController
{
    ExpiredAssetsAMCService eaaservice = new ExpiredAssetsAMCService();

    [HttpPost]
    public HttpResponseMessage BindDataToGrid(ExpiredAssetsAMCModel VM)
    {
        var obj = eaaservice.BindDataToGrid(VM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> ExportReportdata([FromBody]ExpiredAssetsAMCModel CustRpt)
    {
        ReportGenerator<ExpiredAssetsAMCGridList> reportgen = new ReportGenerator<ExpiredAssetsAMCGridList>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/ExpiredAssetsAMC.rdlc"),
            DataSetName = "AssetMaint",
            ReportType = "Expired Assets AMC"
        };

        List<ExpiredAssetsAMCGridList> reportdata = eaaservice.BindDataToGridList(CustRpt);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ExpiredAssetsAMC." + CustRpt.Type);
        await reportgen.GenerateReport(reportdata, filePath, CustRpt.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ExpiredAssetsAMC." + CustRpt.Type;
        return result;
    }
}
