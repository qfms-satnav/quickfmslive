﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class CreatePlanMSController : ApiController
{
    CreatePlanMSService CPS = new CreatePlanMSService();

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getdata(CreatePlanSearch CRP)
    {
        var obj = CPS.getdata(CRP);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Update PPM Plans Method
    [GzipCompression]
    [HttpPost]
    public object CreatePPMPlane(CreatePModel CPM)
    {
        return CPS.CreatePPMPlane(CPM);
        //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        //return response;
    }

    //Update PPM Plans Method
    [GzipCompression]
    [HttpPost]
    public object UpdatePPMPlane(CreatePModel sad)
    {
        return CPS.UpdatePPMPlane(sad);
    }

    //Delete PPM Plans Method
    [GzipCompression]
    [HttpPost]
    public object DeletePPMPlane(CreatePModel sad)
    {
        return CPS.DeletePPMPlane(sad);
    }


}
