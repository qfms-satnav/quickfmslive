﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;



public class AMCPaymentAdviceController : ApiController
{
    AMCPaymentAdviceService MaintService = new AMCPaymentAdviceService();
    [HttpPost]
    public HttpResponseMessage GetGriddata(CompanyData MaitDetails)
    {
        var obj = MaintService.GetMaintDetails(MaitDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(CompanyData data)
    {
        ReportGenerator<AMCPaymentAdviceModel> reportgen = new ReportGenerator<AMCPaymentAdviceModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PaymentAdviceReport.rdlc"),
            DataSetName = "PaymentAdviceDS",
            ReportType = "Maintenance Contract Payment Advice"
        };

        MaintService = new AMCPaymentAdviceService();
        List<AMCPaymentAdviceModel> reportdata = MaintService.PaymentAdviceDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/PaymentAdviceReport.rdlc." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "MaintenanceContractPaymentAdvice." + data.Type;
        return result;
    }
}