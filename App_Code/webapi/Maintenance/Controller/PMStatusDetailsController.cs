﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;



public class PMStatusDetailsController : ApiController
{
    PMStatusDetailsService MaintService = new PMStatusDetailsService();
    [HttpPost]
    public HttpResponseMessage BindDataToGridList(PMStatus MaitDetails)
    {
        var obj = MaintService.GetMaintDetails(MaitDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> ExportReportdata([FromBody]PMStatus CustRpt)
    {
        ReportGenerator<PMStatusList> reportgen = new ReportGenerator<PMStatusList>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PMStatusDetails.rdlc"),
            DataSetName = "PMStatusDetails1",
            ReportType = "Preventive Maintenance Status Details"
        };

        List<PMStatusList> reportdata = MaintService.BindDataToGridList(CustRpt);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/PMStatusDetails." + CustRpt.Type);
        await reportgen.GenerateReport(reportdata, filePath, CustRpt.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "PMStatusDetails." + CustRpt.Type;
        return result;
    }
}