﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class PMCostController : ApiController
{
    PMCostService PService = new PMCostService();

    [HttpPost]
    public HttpResponseMessage GetCostDetails(PMCostVM VM)
    {
        var obj = PService.GetCostObject(VM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(PMCostVM data)
    {
        ReportGenerator<PMCostModel> reportgen = new ReportGenerator<PMCostModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PMCost.rdlc"),
            DataSetName = "PMCost",
            ReportType = "Preventive Maintenance Cost Details"
        };

        PService = new PMCostService();
        List<PMCostModel> reportdata = PService.GetCostDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/PMCost.rdlc." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "PrvntCostDetails." + data.Type;
        return result;
    }
}
