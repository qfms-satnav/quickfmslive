﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class AssetUnderMaintContController : ApiController
{
    AssetUnderMaintContService MaintService = new AssetUnderMaintContService();
    [HttpPost]
    public HttpResponseMessage GetGriddata(MaintenanceData MaitDetails)
    {
        var obj = MaintService.GetMaintDetails(MaitDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(MaintenanceData MaitDetails)
    {
        var obj = MaintService.GetDetailsOnSelection(MaitDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(MaintenanceData data)
    {
        ReportGenerator<AssetUnderMaint> reportgen = new ReportGenerator<AssetUnderMaint>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/AssetUnderMaintCont.rdlc"),
            DataSetName = "AssetMaint",
            ReportType = "Asset Under Maintenance Contract"
        };

        MaintService = new AssetUnderMaintContService();
        List<AssetUnderMaint> reportdata = MaintService.MaintenanceDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/AssetUnderMaintCont.rdlc." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AssetUnderMaintContract." + data.Type;
        return result;
    }
}
