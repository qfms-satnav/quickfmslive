using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//public class GetEmployeeList
//{
//    public List<GetEmployeeData> EMPHRMSlist { get; set; }

//}
    public class GetEmployeeData
{
    public string EmployeeID { set; get; }
    public string EmployeeName { set; get; }
    public string EmpEmail { set; get; }
    public string Gender { set; get; }
    public string Grade { set; get; }
    public string Level { set; get; }
    public string Designation { set; get; }
    public string MobileNo { set; get; }
    public string FunctionName { set; get; }
    public string SubDepartment { set; get; }
    public string Department { set; get; }
    public string DepartmentCostCenter { set; get; }
    public string FunctionCostCenter { set; get; }
    public string City { set; get; }
    public string State { set; get; }
    public string Location { set; get; }
    public string LocationCostCenter { set; get; }
    public string Category { set; get; }
    public DateTime JoiningDate { set; get; }
    public string EmployeeType { set; get; }
    public string Status { set; get; }
    public DateTime DateofResign { set; get; }
    public DateTime LastWorkingDate { set; get; }
    public DateTime EffectiveDate { set; get; }
    public string RAECode { set; get; }
    public string HODECode { set; get; }
    public string NHODECode { set; get; }
    public string WorkLocationType { set; get; }


}
public class HRMSDataM
{
    public string EMP_ID { set; get; }
    public string FIRST_NAME { set; get; }

    public string LAST_NAME { set; get; }
    public string GENDER { set; get; }
    public string EMAIL_ID { set; get; }
    public string MNGR_ID { set; get; }
    public string DESIGNATION { set; get; }
    public string PHONE_NO { set; get; }
    public string DEPARTMENT { set; get; }
    public string PARENT_ENTITY { set; get; }
    public string CHILD_ENTITY { set; get; }
    public string VERTICAL { set; get; }
    public string COSTCENTER { set; get; }
    public string LOCATION { set; get; }
    public string CITY { set; get; }
    public string COUNTRY { set; get; }
    public string JOINING_DATE { set; get; }
    public string EMPLOYEE_TYPE { set; get; }
    public string STATUS { set; get; }
    public string QFM_ACTIVE { set; get; }
    public string GRADE { set; get; }

    public string BAND { set; get; }

}
public class HRMSDataLTTS
{
    public string EmployeeID { set; get; }
    public string FIRST_NAME { get; set; }
    public string MIDDLE_NAME { get; set; }
    public string LAST_NAME { get; set; }
    public string GENDER { get; set; }
    public string EMAIL_ID { get; set; }
    public string SUPV_ID { get; set; }
    public string DESIGNATION { get; set; }
    public string PHONE_NO { get; set; }
    public string DEPARTMENT { get; set; }
    //public string PARENT_ENTITY { get; set; }
    //public string CHILD_ENTITY { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string LOCATION { get; set; }
    public string CITY { get; set; }
    public string COUNTRY { get; set; }
    public string JOINING_DATE { get; set; }
    public string EMPLOYEE_TYPE { get; set; }
    public string GRADE { get; set; }
    public string BAND { get; set; }
    public string STATUS { get; set; }
}

public class HRMSDataProduct
{
    public string EmployeeID { set; get; }
    public string FIRST_NAME { get; set; }
    public string MIDDLE_NAME { get; set; }
    public string LAST_NAME { get; set; }
    public string GENDER { get; set; }
    public string EMAIL_ID { get; set; }
    public string SUPV_ID { get; set; }
    public string DESIGNATION { get; set; }
    public string PHONE_NO { get; set; }
    public string DEPARTMENT { get; set; }
    public string PARENT_ENTITY { get; set; }
    public string CHILD_ENTITY { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string LOCATION { get; set; }
    public string CITY { get; set; }
    public string COUNTRY { get; set; }
    public string JOINING_DATE { get; set; }
    public string EMPLOYEE_TYPE { get; set; }
    public string GRADE { get; set; }
    public string BAND { get; set; }
    public string STATUS { get; set; }

    public string REMARKS { get; set; }

}
public class HRMSDataApx
{
    public string EmployeeID { set; get; }
    public string FIRST_NAME { get; set; }
    public string MIDDLE_NAME { get; set; }
    public string LAST_NAME { get; set; }
    public string GENDER { get; set; }
    public string EMAIL_ID { get; set; }
    public string SUPV_ID { get; set; }
    public string DESIGNATION { get; set; }
    public string PHONE_NO { get; set; }
    public string DEPARTMENT { get; set; }
    public string PARENT_ENTITY { get; set; }
    public string CHILD_ENTITY { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string LOCATION { get; set; }
    public string CITY { get; set; }
    public string COUNTRY { get; set; }
    public string JOINING_DATE { get; set; }
    public string EMPLOYEE_TYPE { get; set; }
    public string GRADE { get; set; }
    public string BAND { get; set; }
    public string STATUS { get; set; }
}
public class HRMSDataBAX
{
    public string EmployeeID { set; get; }
    public string FIRST_NAME { get; set; }
    public string MIDDLE_NAME { get; set; }
    public string LAST_NAME { get; set; }
    public string GENDER { get; set; }
    public string EMAIL_ID { get; set; }
    public string SUPV_ID { get; set; }
    public string DESIGNATION { get; set; }
    public string PHONE_NO { get; set; }
    public string DEPARTMENT { get; set; }
    public string PARENT_ENTITY { get; set; }
    public string CHILD_ENTITY { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string LOCATION { get; set; }
    public string CITY { get; set; }
    public string COUNTRY { get; set; }
    public string JOINING_DATE { get; set; }
    public string EMPLOYEE_TYPE { get; set; }
    public string STATUS { get; set; }
}
public class SPACEHISTORYREPORT
{
    public int STATUS { get; set; }
    public int COMPANYID { get; set; }
}

public class EmpRemarks
{
    public string STATUS { get; set; }
    public string Remarks { get; set; }
}
public class LstEmpRemarks
{
    public string Message { get; set; }
    public List<EmpRemarks> data { get; set; }
}




