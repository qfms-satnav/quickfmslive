
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace BasicAuthentication.Controllers
{
    public class GetEmployeController : ApiController
    {
        GetEmployeService Agnrex = new GetEmployeService();

        [BasicAuthentication]
        [HttpPost]

        //public async Task GetBulkEmployeeData(HttpRequestMessage request)
        //{
        //    var jObject = await request.Content.ReadAsAsync<JObject>();

        //    GetEmployeeList sample = JsonConvert.DeserializeObject<GetEmployeeList>(jObject.ToString());
        //    var obj = Agnrex.GetEmployees(sample);
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        //}

        public HttpResponseMessage GetBulkEmployeeData([FromBody] List<GetEmployeeData> getEmployeeData)
        {
            var obj = Agnrex.GetEmployees(getEmployeeData);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
            return response;
        }

        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage InsertHRMSEmployeeData([FromBody] List<HRMSDataM> GetEmpData)
        {
            HttpResponseMessage response;
            if (GetEmpData != null)
            {
                string result = Agnrex.InsertEmployees(GetEmpData);

                if (result == "Success")
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, GetEmpData);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Not Recieving any data");
            }

            return response;
        }

        //Johndeere 
        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage InsertHRMSEmpData([FromBody] List<HRMSDataM> GetEmpData)
        {
            HttpResponseMessage response;
            if (GetEmpData != null)
            {
                string result = Agnrex.InsertHRMSEmpData(GetEmpData);

                if (result == "Success")
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, GetEmpData);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Not Recieving any data");
            }

            return response;
        }
        //Apexon
        [BasicAuthentication]
        [HttpGet]
        public Object GetSpaceHistoryList()
        {
            return Agnrex.GetSpaceHistoryList();
        }

        //Apexon
        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage HRMSDATAList([FromBody] List<HRMSDataApx> hrm)
        {
            HttpResponseMessage response;
            if (hrm != null)
            {
                string result = Agnrex.HRMSDATAList(hrm);
                
                if (result == MessagesVM.UM_OK)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK,"Successfully data has been inserted");
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Not Recieving any data");
            }

            return response;
        }

        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage PushHRMSData([FromBody] List<HRMSDataLTTS> hrm)
        {
            HttpResponseMessage response;
            if (hrm != null)
            {
                string result = Agnrex.PushHRMSData(hrm);

                if (result == MessagesVM.UM_OK)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, "Successfully data has been inserted");
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Not Recieving any data");
            }

            return response;
        }

        //BharatiAXA
        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage HRMSDATAListBA(List<HRMSDataBAX> hrm)
        {
            HttpResponseMessage response;
            if (hrm != null)
            {
                string result = Agnrex.HRMSDATAListBA(hrm);

                if (result == MessagesVM.UM_OK)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, "Successfully data has been inserted");
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Not Recieving any data");
            }

            return response;
        }

        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage PushHRMSDataProduct([FromBody] List<HRMSDataProduct> hrms)
        {
            HttpHeaders headers = this.Request.Headers;
            HttpResponseMessage response;
            if (hrms != null)
            {
                if (headers.Contains("Companyid"))
                {
                    String Companyid = headers.GetValues("Companyid").First();
                    string result = Agnrex.PushHRMSDataProduct(hrms, Companyid);

                    if (result == MessagesVM.UM_OK)
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, "Successfully data has been inserted");
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.BadRequest, result);
                    }
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Something went Wrong");
                }


            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Not Recieving any data");
            }

            return response;
        }
        //Emids
        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage PushHRMSEmployeeData([FromBody] List<HRMSDataProduct> hrms)
        {
            HttpHeaders headers = this.Request.Headers;
            HttpResponseMessage response;
            if (hrms != null)
            {
                if (headers.Contains("Companyid"))
                {
                    String Companyid = headers.GetValues("Companyid").First();
                    LstEmpRemarks result = Agnrex.PushHRMSDataEmids(hrms, Companyid);

                    if (result.Message == "1")
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, result.data);
                    }
                    else if (result.Message == "0")
                    {
                        response = Request.CreateResponse(HttpStatusCode.BadRequest, "Something went Wrong");
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.BadRequest, result.Message);
                    }
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Something went Wrong");
                }


            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Not Recieving any data");
            }

            return response;
        }
    }
}
