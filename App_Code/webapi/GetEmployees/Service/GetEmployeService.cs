using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
using System.Web;

/// <summary>
/// Summary description for ContractMasterService
/// </summary>
public class GetEmployeService
{
    SubSonic.StoredProcedure sp;


    public object GetEmployees(List<GetEmployeeData> getEmployeeData)
    {
        System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
        System.Web.HttpContext.Current.Session["TENANT"] = "Bajaj.dbo";
        try
        {
            
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(getEmployeeData);

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_HRMS_DATA_BAJAJ", param);
            return new { Message = "Data Uploaded Successfully" };

            //ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_HRMS_DATA_BAJAJ", param);
            //return new { data = ds.Tables[0] };
            //param[0] = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            //param[0].Value = GetEmployees.EmployeeID;
            //param[1] = new SqlParameter("@EmployeeName", SqlDbType.VarChar);
            //param[1].Value = GetEmployees.EmployeeName;
            //param[2] = new SqlParameter("@EmpEmail", SqlDbType.VarChar);
            //param[2].Value = GetEmployees.EmpEmail;
            //param[3] = new SqlParameter("@Gender", SqlDbType.VarChar);
            //param[3].Value = GetEmployees.Gender;
            //param[4] = new SqlParameter("@Grade", SqlDbType.VarChar);
            //param[4].Value = GetEmployees.Grade;
            //param[5] = new SqlParameter("@Level", SqlDbType.VarChar);
            //param[5].Value = GetEmployees.Level;
            //param[6] = new SqlParameter("@Designation", SqlDbType.VarChar);
            //param[6].Value = GetEmployees.Designation;
            //param[7] = new SqlParameter("@MobileNo", SqlDbType.VarChar);
            //param[7].Value = GetEmployees.MobileNo;
            //param[8] = new SqlParameter("@Function", SqlDbType.VarChar);
            //param[8].Value = GetEmployees.FunctionName;
            //param[9] = new SqlParameter("@SubDepartment", SqlDbType.VarChar);
            //param[9].Value = GetEmployees.SubDepartment;
            //param[10] = new SqlParameter("@Department", SqlDbType.VarChar);
            //param[10].Value = GetEmployees.Department;
            //param[11] = new SqlParameter("@DepartmentCostCenter", SqlDbType.VarChar);
            //param[11].Value = GetEmployees.DepartmentCostCenter;
            //param[12] = new SqlParameter("@FunctionCostCenter", SqlDbType.VarChar);
            //param[12].Value = GetEmployees.FunctionCostCenter;
            //param[13] = new SqlParameter("@City", SqlDbType.VarChar);
            //param[13].Value = GetEmployees.City;
            //param[14] = new SqlParameter("@State", SqlDbType.VarChar);
            //param[14].Value = GetEmployees.State;
            //param[15] = new SqlParameter("@Location", SqlDbType.VarChar);
            //param[15].Value = GetEmployees.Location;
            //param[16] = new SqlParameter("@LocationCostCenter", SqlDbType.VarChar);
            //param[16].Value = GetEmployees.LocationCostCenter;
            //param[17] = new SqlParameter("@Category", SqlDbType.VarChar);
            //param[17].Value = GetEmployees.Category;
            //param[18] = new SqlParameter("@JoiningDate", SqlDbType.DateTime);
            //param[18].Value = GetEmployees.JoiningDate;
            //param[19] = new SqlParameter("@EmployeeType", SqlDbType.VarChar);
            //param[19].Value = GetEmployees.EmployeeType;
            //param[20] = new SqlParameter("@Status", SqlDbType.VarChar);
            //param[20].Value = GetEmployees.Status;
            //param[21] = new SqlParameter("@DateofResign", SqlDbType.DateTime);
            //param[21].Value = GetEmployees.DateofResign;
            //param[22] = new SqlParameter("@LastWorkingDate", SqlDbType.DateTime);
            //param[22].Value = GetEmployees.LastWorkingDate;
            //param[23] = new SqlParameter("@EffectiveDate", SqlDbType.DateTime);
            //param[23].Value = GetEmployees.EffectiveDate;
            //param[24] = new SqlParameter("@RAECode", SqlDbType.VarChar);
            //param[24].Value = GetEmployees.RAECode;
            //param[25] = new SqlParameter("@HODECode", SqlDbType.VarChar);
            //param[25].Value = GetEmployees.HODECode;
            //param[26] = new SqlParameter("@NHODECode", SqlDbType.VarChar);
            //param[26].Value = GetEmployees.NHODECode;



        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }

    internal string HRMSDATAList_S(List<HRMSDataM> hrm)
    {
        throw new NotImplementedException();
    }

    public string InsertEmployees(List<HRMSDataM> GetEmpData)
    {
        System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
        System.Web.HttpContext.Current.Session["TENANT"] = "HONDA.dbo";
        try
        {

            //DataSet ds = new DataSet();
            foreach (HRMSDataM inserteditems in GetEmpData)
            {

                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HRMS_DATA_BAPCO");
                sp.Command.AddParameter("@EMP_ID", inserteditems.EMP_ID, DbType.String);
                sp.Command.AddParameter("@FIRST_NAME", inserteditems.FIRST_NAME, DbType.String);
                sp.Command.AddParameter("@LAST_NAME", inserteditems.LAST_NAME, DbType.String);
                sp.Command.AddParameter("@GENDER", inserteditems.GENDER, DbType.String);
                sp.Command.AddParameter("@EMAIL_ID", inserteditems.@EMAIL_ID, DbType.String);
                sp.Command.AddParameter("@MNGR_ID", inserteditems.MNGR_ID, DbType.String);
                sp.Command.AddParameter("@DESIGNATION", inserteditems.DESIGNATION, DbType.String);
                sp.Command.AddParameter("@PHONE_NO", inserteditems.PHONE_NO, DbType.String);
                sp.Command.AddParameter("@DEPARTMENT", inserteditems.DEPARTMENT, DbType.String);
                sp.Command.AddParameter("@PARENT_ENTITY", inserteditems.PARENT_ENTITY, DbType.String);
                sp.Command.AddParameter("@CHILD_ENTITY", inserteditems.CHILD_ENTITY, DbType.String);
                sp.Command.AddParameter("@VERTICAL", inserteditems.VERTICAL, DbType.String);
                sp.Command.AddParameter("@COSTCENTER", inserteditems.COSTCENTER, DbType.String);
                sp.Command.AddParameter("@LOCATION", inserteditems.LOCATION, DbType.String);
                sp.Command.AddParameter("@CITY", inserteditems.CITY, DbType.String);
                sp.Command.AddParameter("@COUNTRY", inserteditems.COUNTRY, DbType.String);
                sp.Command.AddParameter("@JOINING_DATE", inserteditems.JOINING_DATE, DbType.DateTime);
                sp.Command.AddParameter("@EMPLOYEE_TYPE", inserteditems.EMPLOYEE_TYPE, DbType.String);
                sp.Command.AddParameter("@STATUS", inserteditems.STATUS, DbType.String);
                sp.Command.AddParameter("@QFM_ACTIVE", inserteditems.QFM_ACTIVE, DbType.String);
                //sp.ExecuteScalar();QFM_ACTIVE

                DataSet ds = new DataSet();
                ds = sp.GetDataSet();

            }
            return "Success";
        }
        catch (Exception Ex)
        {

            return Ex.Message;
        }
    }

    public string InsertHRMSEmpData(List<HRMSDataM> GetEmpData)
    {
        System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
        System.Web.HttpContext.Current.Session["TENANT"] = "johndeere.dbo";
        try
        {

            //DataSet ds = new DataSet();
            foreach (HRMSDataM inserteditems in GetEmpData)
            {

                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_HRMS_DATA_JOHNDEERE]");
                sp.Command.AddParameter("@EMP_ID", inserteditems.EMP_ID, DbType.String);
                sp.Command.AddParameter("@FIRST_NAME", inserteditems.FIRST_NAME, DbType.String);
                sp.Command.AddParameter("@LAST_NAME", inserteditems.LAST_NAME, DbType.String);
                sp.Command.AddParameter("@GENDER", inserteditems.GENDER, DbType.String);
                sp.Command.AddParameter("@EMAIL_ID", inserteditems.@EMAIL_ID, DbType.String);
                sp.Command.AddParameter("@MNGR_ID", inserteditems.MNGR_ID, DbType.String);
                sp.Command.AddParameter("@DESIGNATION", inserteditems.DESIGNATION, DbType.String);
                sp.Command.AddParameter("@PHONE_NO", inserteditems.PHONE_NO, DbType.String);
                sp.Command.AddParameter("@DEPARTMENT", inserteditems.DEPARTMENT, DbType.String);
                sp.Command.AddParameter("@PARENT_ENTITY", inserteditems.PARENT_ENTITY, DbType.String);
                sp.Command.AddParameter("@CHILD_ENTITY", inserteditems.CHILD_ENTITY, DbType.String);
                sp.Command.AddParameter("@VERTICAL", inserteditems.VERTICAL, DbType.String);
                sp.Command.AddParameter("@COSTCENTER", inserteditems.COSTCENTER, DbType.String);
                sp.Command.AddParameter("@LOCATION", inserteditems.LOCATION, DbType.String);
                sp.Command.AddParameter("@CITY", inserteditems.CITY, DbType.String);
                sp.Command.AddParameter("@COUNTRY", inserteditems.COUNTRY, DbType.String);
                sp.Command.AddParameter("@JOINING_DATE", inserteditems.JOINING_DATE, DbType.String);
                sp.Command.AddParameter("@EMPLOYEE_TYPE", inserteditems.EMPLOYEE_TYPE, DbType.String);
                sp.Command.AddParameter("@STATUS", inserteditems.STATUS, DbType.String);
                sp.Command.AddParameter("@QFM_ACTIVE", inserteditems.QFM_ACTIVE, DbType.String);
                //sp.ExecuteScalar();QFM_ACTIVE

                DataSet ds = new DataSet();
                ds = sp.GetDataSet();

            }
            return "Success";
        }
        catch (Exception Ex)
        {

            return Ex.Message;
        }
    }

    //For Apexon
    //public string GetInsertHRMSEmpData(List<HRMSDataM> GetEmpData)
    //{
    //    System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
    //    System.Web.HttpContext.Current.Session["TENANT"] = "apexon.dbo";
    //    try
    //    {

    //        //DataSet ds = new DataSet();
    //        foreach (HRMSDataM inserteditems in GetEmpData)
    //        {

    //            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_HRMS_DATA_APEXON]");
    //            sp.Command.AddParameter("@EMP_ID", inserteditems.EMP_ID, DbType.String);
    //            sp.Command.AddParameter("@FIRST_NAME", inserteditems.FIRST_NAME, DbType.String);
    //            sp.Command.AddParameter("@LAST_NAME", inserteditems.LAST_NAME, DbType.String);
    //            sp.Command.AddParameter("@GENDER", inserteditems.GENDER, DbType.String);
    //            sp.Command.AddParameter("@EMAIL_ID", inserteditems.@EMAIL_ID, DbType.String);
    //            sp.Command.AddParameter("@MNGR_ID", inserteditems.MNGR_ID, DbType.String);
    //            sp.Command.AddParameter("@DESIGNATION", inserteditems.DESIGNATION, DbType.String);
    //            sp.Command.AddParameter("@PHONE_NO", inserteditems.PHONE_NO, DbType.String);
    //            sp.Command.AddParameter("@DEPARTMENT", inserteditems.DEPARTMENT, DbType.String);
    //            sp.Command.AddParameter("@PARENT_ENTITY", inserteditems.PARENT_ENTITY, DbType.String);
    //            sp.Command.AddParameter("@CHILD_ENTITY", inserteditems.CHILD_ENTITY, DbType.String);
    //            sp.Command.AddParameter("@VERTICAL", inserteditems.VERTICAL, DbType.String);
    //            sp.Command.AddParameter("@COSTCENTER", inserteditems.COSTCENTER, DbType.String);
    //            sp.Command.AddParameter("@LOCATION", inserteditems.LOCATION, DbType.String);
    //            sp.Command.AddParameter("@CITY", inserteditems.CITY, DbType.String);
    //            sp.Command.AddParameter("@COUNTRY", inserteditems.COUNTRY, DbType.String);
    //            sp.Command.AddParameter("@JOINING_DATE", inserteditems.JOINING_DATE, DbType.String);
    //            sp.Command.AddParameter("@EMPLOYEE_TYPE", inserteditems.EMPLOYEE_TYPE, DbType.String);
    //            sp.Command.AddParameter("@STATUS", inserteditems.STATUS, DbType.String);
    //            sp.Command.AddParameter("@QFM_ACTIVE", inserteditems.QFM_ACTIVE, DbType.String);
    //            sp.Command.AddParameter("@GRADE", inserteditems.GRADE, DbType.String);
    //            sp.Command.AddParameter("@BAND", inserteditems.BAND, DbType.String);

    //            //sp.ExecuteScalar();QFM_ACTIVE

    //            DataSet ds = new DataSet();
    //            ds = sp.GetDataSet();

    //        }
    //        return "Success";
    //    }
    //    catch (Exception Ex)
    //    {

    //        return Ex.Message;
    //    }
    //}
    //public string HRMSDATAList(List<HRMSDataM> HRMSLST)
    //{
    //    System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
    //    System.Web.HttpContext.Current.Session["TENANT"] = "Apexon.dbo";
    //    SqlParameter[] param = new SqlParameter[1];
    //    param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
    //    param[0].Value = UtilityService.ConvertToDataTable(HRMSLST);
    //    int hrmsList = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure,"GET_HRMS_DATA_APEXON", param);

    //    if (hrmsList == 1)
    //        return MessagesVM.UM_OK ;
    //    else
    //        return MessagesVM.ErrorMessage;
    //}

    public string HRMSDATAList(List<HRMSDataApx> HRMSLST)
    {
        System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
        System.Web.HttpContext.Current.Session["TENANT"] = "Apexon.dbo";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(HRMSLST);
        int hrmsList = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "GET_HRMS_DATA_APEXON", param);

        if (hrmsList == 1)
            return MessagesVM.UM_OK;
        else
            return MessagesVM.ErrorMessage;
    }

    //Emids
    public LstEmpRemarks PushHRMSDataEmids(List<HRMSDataProduct> HRMSLST, string Company)
    {
        try
        {
            int hrmsList = 0;
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            List<EmpRemarks> Remarks = new List<EmpRemarks>();
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "CHECK_USER_TENANT");
            sp.Command.AddParameter("@TENANT_NAME", Company, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                System.Web.HttpContext.Current.Session["useroffset"] = ds.Tables[0].Rows[0]["OFFSET"].ToString();
                System.Web.HttpContext.Current.Session["TENANT"] = ds.Tables[0].Rows[0]["TENANT_ID"].ToString();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(HRMSLST);
                ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "POST_HRMS_DATA_PRODUCT", param);
                if (ds1.Tables[0].Rows[0]["Status"].ToString() == "1")
                {
                    foreach (DataRow row in ds1.Tables[1].Rows)
                    {
                        EmpRemarks empRemarks = new EmpRemarks();
                        empRemarks.STATUS = row.ItemArray[0].ToString();
                        empRemarks.Remarks = row.ItemArray[1].ToString();
                        Remarks.Add(empRemarks);
                    }
                    return new LstEmpRemarks { Message = "1", data = Remarks };
                }
                else
                {
                    return new LstEmpRemarks { Message = "0", data = new List<EmpRemarks> { null } };
                }
            }

            else
                return new LstEmpRemarks { Message = MessagesVM.ErrorMessage, data = new List<EmpRemarks> { null } };

        }
        catch (Exception EX)
        {
            return new LstEmpRemarks { Message = MessagesVM.ErrorMessage, data = new List<EmpRemarks> { null } };
        }

    }

    public string PushHRMSDataProduct(List<HRMSDataProduct> HRMSLST, string Company)
    {
        try
        {
            int hrmsList = 0;
            DataSet ds = new DataSet();
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "CHECK_USER_TENANT");
            sp.Command.AddParameter("@TENANT_NAME", Company, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                System.Web.HttpContext.Current.Session["useroffset"] = ds.Tables[0].Rows[0]["OFFSET"].ToString();
                System.Web.HttpContext.Current.Session["TENANT"] = ds.Tables[0].Rows[0]["TENANT_ID"].ToString();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(HRMSLST);
                hrmsList = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "POST_HRMS_DATA_PRODUCT", param);

            }
            if (hrmsList == 1)
                return MessagesVM.UM_OK;
            else
                return MessagesVM.ErrorMessage;

        }
        catch (Exception EX)
        {
            return MessagesVM.ErrorMessage;
        }

    }

    public string PushHRMSData(List<HRMSDataLTTS> HRMSLST)
    {
        try
        {
            System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
            System.Web.HttpContext.Current.Session["TENANT"] = "ltts.dbo";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(HRMSLST);
            int hrmsList = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "GET_HRMS_DATA_LTTS", param);

            if (hrmsList == 1)
                return MessagesVM.UM_OK;
            else
                return MessagesVM.ErrorMessage;
        }
        catch (Exception EX)
        {
            return MessagesVM.ErrorMessage;
        }

    }

    public string HRMSDATAListBA(List<HRMSDataBAX> HRMSLST)
    {
        System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
        System.Web.HttpContext.Current.Session["TENANT"] = "BhartiAXA.dbo";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(HRMSLST);
        int hrmsList = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "GET_HRMS_DATA_BHARTIAXA", param);

        if (hrmsList == 1)
            return MessagesVM.UM_OK;
        else
            return MessagesVM.ErrorMessage;
    }

    public object GetSpaceHistoryList()
    {

        System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
        System.Web.HttpContext.Current.Session["TENANT"] = "Apexon.dbo";
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_API_SPACE_BOOKING_HISTORY");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}


