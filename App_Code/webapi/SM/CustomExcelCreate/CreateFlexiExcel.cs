﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

public class CreateFlexiExcel
{
    private static void AddStyle(SpreadsheetDocument spreadsheetDocument)
    {
        var stylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
        stylesPart.Stylesheet = new Stylesheet();

        // blank font list
        stylesPart.Stylesheet.Fonts = new Fonts();
        stylesPart.Stylesheet.Fonts.Count = 1;

        stylesPart.Stylesheet.Fonts.AppendChild(new Font());
        stylesPart.Stylesheet.Fonts.AppendChild(new Font(new Bold()));

        // create fills
        stylesPart.Stylesheet.Fills = new Fills();

        // create a solid red fill
        //var yellow = new PatternFill() { PatternType = PatternValues.Solid };
        //yellow.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("fffa7c") }; // red fill
        //yellow.BackgroundColor = new BackgroundColor { Indexed = 64 };

        //// create a solid red fill
        var solidRed = new PatternFill() { PatternType = PatternValues.Solid };
        solidRed.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("FFFF0000") }; // red fill
        solidRed.BackgroundColor = new BackgroundColor { Indexed = 64 };

        stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } }); // required, reserved by Excel
        stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } }); // required, reserved by Excel
        //stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = yellow });
        stylesPart.Stylesheet.Fills.Count = 3;


        stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = solidRed });
        stylesPart.Stylesheet.Fills.Count = 4;

        // blank border list
        stylesPart.Stylesheet.Borders = new Borders();
        stylesPart.Stylesheet.Borders.Count = 1;
        stylesPart.Stylesheet.Borders.AppendChild(new Border());

        // blank cell format list
        stylesPart.Stylesheet.CellStyleFormats = new CellStyleFormats();
        stylesPart.Stylesheet.CellStyleFormats.Count = 1;
        stylesPart.Stylesheet.CellStyleFormats.AppendChild(new CellFormat());

        // cell format list
        stylesPart.Stylesheet.CellFormats = new CellFormats();
        // empty one for index 0, seems to be required
        stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat());
        // cell format references style format 0, font 0, border 0, fill 2 and applies the fill
        stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 0, FillId = 2, ApplyFill = true, }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, WrapText = true });

        stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 0, BorderId = 0, FillId = 3, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });

        stylesPart.Stylesheet.CellFormats.Count = 3;


        stylesPart.Stylesheet.Save();
    }

    private static void addStyle1(SpreadsheetDocument spreadsheetDocument)
    {
        WorkbookStylesPart stylesheet = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
        Stylesheet workbookstylesheet = new Stylesheet();

        // <Fonts>
        Fonts fonts = new Fonts();          // <APPENDING Fonts>
        Font font0 = new Font();            // Default font
        Font font1 = new Font(new Bold());
        fonts.Append(font0);
        fonts.Append(font1);

        // <Fills>
        Fills fills = new Fills();          // <APPENDING Fills>
        /*  Fill fill0 = new Fill();*/            // Default fill
        Fill fill0 = new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } }; // required, reserved by Excel
        Fill fill1 = new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } }; // required, reserved by Excel

        // create a solid yellow fill
        var patternYelloFill = new PatternFill() { PatternType = PatternValues.Solid, ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("fffa7c") }, BackgroundColor = new BackgroundColor { Indexed = 64 } };

        Fill fill2 = new Fill { PatternFill = patternYelloFill };

        fills.Append(fill0);
        fills.Append(fill1);
        fills.Append(fill2);

        // <Borders>
        Borders borders = new Borders();    // <APPENDING Borders>
        Border border0 = new Border();      // Defualt border

        Border border1 = new Border();



        LeftBorder leftBorder2 = new LeftBorder() { Style = BorderStyleValues.Thin };
        Color color1 = new Color() { Indexed = (UInt32Value)64U };

        leftBorder2.Append(color1);

        RightBorder rightBorder2 = new RightBorder() { Style = BorderStyleValues.Thin };
        Color color2 = new Color() { Indexed = (UInt32Value)64U };

        rightBorder2.Append(color2);

        TopBorder topBorder2 = new TopBorder() { Style = BorderStyleValues.Thin };
        Color color3 = new Color() { Indexed = (UInt32Value)64U };

        topBorder2.Append(color3);

        BottomBorder bottomBorder2 = new BottomBorder() { Style = BorderStyleValues.Thin };
        Color color4 = new Color() { Indexed = (UInt32Value)64U };

        bottomBorder2.Append(color4);
        DiagonalBorder diagonalBorder2 = new DiagonalBorder();

        border1.Append(leftBorder2);
        border1.Append(rightBorder2);
        border1.Append(topBorder2);
        border1.Append(bottomBorder2);
        border1.Append(diagonalBorder2);

        borders.Append(border0);
        borders.Append(border1);



        // <CellFormats>
        CellFormat cellformat0 = new CellFormat()   // Default style : Mandatory
        {
            FontId = 0,
            FillId = 0,
            BorderId = 0
        };
        CellFormat cellformat1 = new CellFormat { FormatId = 0, FontId = 1, BorderId = 1, FillId = 2, ApplyFill = true };
        cellformat1.Append(new Alignment { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, WrapText = true });

        CellFormat cellformat2 = new CellFormat { BorderId = 1 };

        CellFormat cellformat3 = new CellFormat { NumberFormatId = 14, ApplyNumberFormat = true, BorderId = 1 };

        // <APPENDING CellFormats>
        CellFormats cellformats = new CellFormats();
        cellformats.Append(cellformat0);
        cellformats.Append(cellformat1);
        cellformats.Append(cellformat2);
        cellformats.Append(cellformat3);

        // Append FONTS, FILLS , BORDERS & CellFormats to stylesheet <Preserve the ORDER>
        workbookstylesheet.Append(fonts);
        workbookstylesheet.Append(fills);
        workbookstylesheet.Append(borders);
        workbookstylesheet.Append(cellformats);

        // Finalize
        stylesheet.Stylesheet = workbookstylesheet;
        stylesheet.Stylesheet.Save();

        // Assign our defined style with text wrap.
        //Cell c1 = new Cell() { StyleIndex = Convert.ToUInt32(1) };

    }
    //Below function is used for creating a cell by passing cell data and cell style.
    //Below function is used for creating a cell by passing cell data and cell style.
    private static Cell CreateCell(string text, uint styleIndex)
    {
        Cell cell = new Cell();

        if (styleIndex == 3)
        {
            try
            {
                cell.StyleIndex = styleIndex;
                DateTime date = Convert.ToDateTime(text);
                cell.CellValue = new CellValue(date.ToOADate().ToString(CultureInfo.InvariantCulture));
            }
            catch (Exception ex)
            {
                cell.StyleIndex = styleIndex;
                //cell.StyleIndex = styleIndex;
                cell.DataType = ResolveCellDataTypeOnValue(text);
                cell.CellValue = new CellValue(text);
            }
        }
        else
        {
            cell.StyleIndex = styleIndex;
            //cell.StyleIndex = styleIndex;
            cell.DataType = ResolveCellDataTypeOnValue(text);
            cell.CellValue = new CellValue(text);
        }


        return cell;
    }

    private static Cell CreateCell(string text)
    {
        Cell cell = new Cell();
        cell.StyleIndex = 2;
        cell.DataType = ResolveCellDataTypeOnValue(text);
        cell.CellValue = new CellValue(text);
        return cell;
    }

    //Below function is created for resolving the data type of numeric value in a cell.
    private static EnumValue<CellValues> ResolveCellDataTypeOnValue(string text)
    {
        int intVal;
        double doubleVal;
        DateTime dateVal;
        if (int.TryParse(text, out intVal) || double.TryParse(text, out doubleVal))
        {
            //return CellValues.Number;
            return CellValues.String;
        }
        else if (DateTime.TryParse(text, out dateVal))
        {
            return CellValues.Date;
        }
        else
        {
            return CellValues.String;
        }
    }

    public static void GenerateExcel(List<UploadSeatTypeDataVM> reportData, string filePath)
    {
        try
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook))
            {
                // Add a WorkbookPart to the document.
                WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                SheetData sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);

                addStyle1(spreadsheetDocument);

                SetColumns(worksheetPart);

                // Get the sheetData cells
                //SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                // Add Sheets to the Workbook.
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.
                    AppendChild<Sheets>(new Sheets());

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new Sheet()
                {
                    Id = spreadsheetDocument.WorkbookPart.
                    GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "My Work Sheet"
                };

                var headerRow = CreateHeaderRowForExcel();
                //headerRow.Height = 20;
                //headerRow.CustomHeight = true;
                sheetData.Append(headerRow);

                var countRows = 1;
                foreach (var model in reportData)
                {

                    Row partsRows = GenerateRowForChildPartDetail(model);
                    sheetData.Append(partsRows);
                    countRows++;
                }


                sheets.Append(sheet);

                workbookpart.Workbook.Save();

                // Close the document.
                spreadsheetDocument.Close();
            }
        }
        catch (Exception ex)
        {

        }

    }

  


    private static void SetColumns(WorksheetPart worksheetPart)
    {
        // Create custom widths for columns
        Columns lstColumns = worksheetPart.Worksheet.GetFirstChild<Columns>();
        Boolean needToInsertColumns = false;
        if (lstColumns == null)
        {
            lstColumns = new Columns();
            needToInsertColumns = true;
        }
        // Min = 1, Max = 1 ==> Apply this to column 1 (A)
        // Min = 2, Max = 2 ==> Apply this to column 2 (B)
        // Width = 25 ==> Set the width to 25
        // CustomWidth = true ==> Tell Excel to use the custom width
        lstColumns.Append(new Column() { Min = 1, Max = 6, Width = 20, CustomWidth = true });

        // Only insert the columns if we had to create a new columns element
        if (needToInsertColumns)
            worksheetPart.Worksheet.InsertAt(lstColumns, 0);
    }

    private static Row CreateHeaderRowForExcel()
    {
        Row workRow = new Row();
        workRow.Append(CreateCell("City", 1));
        workRow.Append(CreateCell("Location", 1));
        workRow.Append(CreateCell("Tower", 1));
        workRow.Append(CreateCell("Floor", 1));
        workRow.Append(CreateCell("Space ID", 1));
        workRow.Append(CreateCell("Seat Type", 1));
        return workRow;
    }

    private static Row GenerateRowForChildPartDetail(UploadSeatTypeDataVM testmodel)
    {
        Row tRow = new Row();
        tRow.Append(CreateCell(testmodel.City));
        tRow.Append(CreateCell(testmodel.Location));
        tRow.Append(CreateCell(testmodel.Tower));
        tRow.Append(CreateCell(testmodel.Floor));
        tRow.Append(CreateCell(testmodel.SpaceID));
        tRow.Append(CreateCell(testmodel.SeatType));

        return tRow;
    }

    public static List<UploadSeatTypeDataVM> SeatTypeReadAsList(string Path)
    {
        List<UploadSeatTypeDataVM> uplst = new List<UploadSeatTypeDataVM>();
        SpreadsheetDocument spreadSheetDocument;
        string fileName = Path;
        spreadSheetDocument = SpreadsheetDocument.Open(fileName, false);
        int MainCount = 1;
        int Totalcount = 1;
        string ErrorMsg = "";
        try
        {
            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
            Worksheet workSheet = worksheetPart.Worksheet;
            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
            IEnumerable<Row> rows = sheetData.Descendants<Row>();


            List<Row> rows1 = sheetData.Elements<Row>().Where(row => row.Elements<Cell>().Any(cell => cell.DataType != null)).ToList();



            //SubSonic.StoredProcedure sp;
            //DataSet ds = new DataSet();
            //sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AMT_BSM_GETALL");
            //ds = sp.GetDataSet();

            if (sheetData.ElementAt(0).ChildElements.Count() != 6)
            {
                ErrorMsg = "No.of columns are not matching with downloaded excel";

            }
            else
            {
                Totalcount = rows.Count();
                foreach (Row row in rows1)
                {
                    var uadv = new UploadSeatTypeDataVM();

                    HttpContext.Current.Session["ErrorMsg"] = "";
                    if (ErrorMsg == "")
                    {
                        uadv.City = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(0));
                        uadv.Location = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(1));
                        uadv.Tower = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(2));
                        uadv.Floor = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(3));
                        uadv.SpaceID = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(4));
                        uadv.SeatType = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(5));

                        //uplst.Add(uadv);
                        if (MainCount == 1)
                        {

                            if (uadv.City != CheckSeatTypeExcelColumnNames.City)
                            {
                                // ErrorMsg = "City Column Was Replaced By" + CheckMonitorExcelColumnNames.City;
                                ErrorMsg = CheckSeatTypeExcelColumnNames.City + ",";
                            }
                            if (uadv.Location != CheckSeatTypeExcelColumnNames.Location)
                            {
                                ErrorMsg = ErrorMsg + CheckSeatTypeExcelColumnNames.Location + ",";
                            }
                            if (uadv.Tower != CheckSeatTypeExcelColumnNames.Tower)
                            {
                                ErrorMsg = ErrorMsg + CheckSeatTypeExcelColumnNames.Tower + ",";
                            }
                            if (uadv.Floor != CheckSeatTypeExcelColumnNames.Floor)
                            {
                                ErrorMsg = ErrorMsg + CheckSeatTypeExcelColumnNames.Floor + ",";
                            }
                            if (uadv.SpaceID != CheckSeatTypeExcelColumnNames.SpaceID)
                            {
                                ErrorMsg = ErrorMsg + CheckSeatTypeExcelColumnNames.SpaceID + ",";
                            }
                            if (uadv.SeatType != CheckSeatTypeExcelColumnNames.SeatType)
                            {
                                ErrorMsg = ErrorMsg + CheckSeatTypeExcelColumnNames.SeatType + ",";
                            }

                            if (ErrorMsg != "")
                            {
                                char lastchar = Convert.ToChar(ErrorMsg.Substring(ErrorMsg.Length - 1));
                                if (lastchar == ',')
                                    ErrorMsg = ErrorMsg.Remove(ErrorMsg.Length - 1, 1);
                                ErrorMsg = ErrorMsg + " Column names are not matching / misplaced";
                            }
                        }
                        else
                        {
                            //fix typo in seat type
                            var seatTypeValue = uadv.SeatType.Trim().ToLower();
                            if (seatTypeValue == "dedicated" || seatTypeValue == "dedicate" || seatTypeValue == "1")
                            {
                                uadv.SeatType = "Dedicated";
                            }
                            else if (seatTypeValue == "shared" || seatTypeValue == "share" || seatTypeValue == "2")
                            {
                                uadv.SeatType = "Shared";
                            }
                            else if (seatTypeValue == "flexi seats" || seatTypeValue == "flexi seat" || seatTypeValue == "flexi" || seatTypeValue == "flexiseats" || seatTypeValue == "flexiseat" || seatTypeValue == "4")
                            {
                                uadv.SeatType = "Flexi Seat";
                            }

                            //validate rows here
                        }
                        MainCount++;
                        uplst.Add(uadv);
                    }
                }
            }
            spreadSheetDocument.Close();
            HttpContext.Current.Session["ErrorMsg"] = ErrorMsg;
            return uplst;
        }
        catch (Exception ex)
        {
            if (ex.Message == "Input string was not in a correct format.")
            {
                ErrorMsg = "Input string was not in a correct format. " + MainCount;
                HttpContext.Current.Session["ErrorMsg"] = ErrorMsg;
            }

            else if (ex.Message == "Specified argument was out of the range of valid values.\r\nParameter name: index" && MainCount <= Totalcount)
            {
                ErrorMsg = "Please Apply Borders at " + MainCount + " Row";
                HttpContext.Current.Session["ErrorMsg"] = ErrorMsg;
            }

            spreadSheetDocument.Close();
            return uplst;
        }
    }

    private static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = "";
        if (cell.CellValue != null)
            value = Convert.ToString(cell.CellValue.InnerXml);
        else
            value = "";

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }


    public static string GetCellValueAsDateString(SpreadsheetDocument document, Cell thecurrentcell)
    {
        string result = string.Empty;

        if (thecurrentcell != null)
        {
            if (thecurrentcell.DataType != null)
            {
                switch (thecurrentcell.DataType.Value)
                {
                    case CellValues.SharedString:
                        {
                            int id;
                            if (Int32.TryParse(thecurrentcell.InnerText, out id))
                            {
                                SharedStringItem item = GetSharedStringItemById(document.WorkbookPart, id);
                                if (item.Text != null)
                                {
                                    result = item.Text.Text.Replace("'", "''");
                                }
                                else if (item.InnerText != null)
                                {
                                    result = item.InnerText;
                                }
                                else if (item.InnerXml != null)
                                {
                                    result = item.InnerXml;
                                }
                            } //end of if (Int32.TryParse ( thecurrentcell.InnerText, out id )  
                            break;
                        }
                    case CellValues.String:
                        {
                            result = thecurrentcell.InnerText;
                            break;
                        }

                    default:
                        result = thecurrentcell.InnerText;
                        break;

                } // end of switch(thecurrentcell.DataType.Value)  

            }

            else if (thecurrentcell.DataType == null)
            {
                double cellDouble;

                if (double.TryParse(thecurrentcell.InnerText, out cellDouble))
                {
                    var theDate = DateTime.FromOADate(cellDouble);
                    result = theDate.ToString("M/dd/yyyy");
                }
                else
                {
                    result = thecurrentcell.InnerText;
                }
            }

        }

        return result;


    }

    public static string GetCellValue1(SpreadsheetDocument document, Cell thecurrentcell)
    {
        string result = string.Empty;
        //start of if  
        if (thecurrentcell != null)
        {
            if (thecurrentcell.DataType != null)
            {
                switch (thecurrentcell.DataType.Value)
                {
                    case CellValues.SharedString:
                        {
                            int id;
                            if (Int32.TryParse(thecurrentcell.InnerText, out id))
                            {
                                SharedStringItem item = GetSharedStringItemById(document.WorkbookPart, id);
                                if (item.Text != null)
                                {
                                    result = item.Text.Text.Replace("'", "''");
                                }
                                else if (item.InnerText != null)
                                {
                                    result = item.InnerText;
                                }
                                else if (item.InnerXml != null)
                                {
                                    result = item.InnerXml;
                                }
                            } //end of if (Int32.TryParse ( thecurrentcell.InnerText, out id )  
                            break;
                        }

                    case CellValues.Boolean:
                        {
                            result = thecurrentcell.InnerText;
                            break;
                        }

                    case CellValues.Date:
                        {
                            result = thecurrentcell.InnerText;
                            break;
                        }

                    case CellValues.Error:
                        {
                            result = thecurrentcell.InnerText;
                            break;
                        }
                    case CellValues.InlineString:
                        {
                            result = thecurrentcell.InnerText;
                            break;
                        }

                    case CellValues.Number:
                        {
                            result = thecurrentcell.InnerText;
                            break;
                        }
                    case CellValues.String:
                        {
                            result = thecurrentcell.InnerText;
                            break;
                        }
                    default:
                        result = thecurrentcell.InnerText;
                        break;

                } // end of switch(thecurrentcell.DataType.Value)  

            } //end of (thecurrentcell.DataType != null)  
            else if (thecurrentcell.DataType == null)
            {
                if (thecurrentcell.CellFormula != null)
                {
                    result = thecurrentcell.CellValue.InnerText;
                }
                else
                {
                    result = thecurrentcell.InnerText;
                }
            }
        } //end of if(thecurrentcell!=null)  

        return result;

    } //end of public 


    public static SharedStringItem GetSharedStringItemById(WorkbookPart workbookPart, int id)
    {
        return workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
    }


    public class CheckSeatTypeExcelColumnNames
    {
        public static string City = "City";
        public static string Location = "Location";
        public static string Tower = "Tower";
        public static string Floor = "Floor";
        public static string SpaceID = "Space ID";
        public static string SeatType = "Seat Type";
    }

    public class SiftValidations
    {
        public int startRow { get; set; }
        public int endRow { get; set; }
        public string CellAddress { get; set; }
        public string CommaSepartedList { get; set; }

    }

}
