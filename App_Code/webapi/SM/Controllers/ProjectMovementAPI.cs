﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ProjectMovementAPI
/// </summary>
public class ProjectMovementAPIController : ApiController
{
    ProjectMovementService service = new ProjectMovementService();
    
    public DataTable GetCity()
    {
        return service.GetCity();
    }

    [HttpPost]
    public DataTable GetLocation([FromBody] ProjectMovementModel.GetcityID city)
    {
        return service.GetLocation(city);
    }

    [HttpGet]
    public DataTable GetVertical()
    {
        return service.GetVertical();    
    }

    [HttpPost]
    public DataTable GetCostCenter([FromBody] ProjectMovementModel.Getvertical getvertical)
    {
        return service.GetCostCenter(getvertical);    
    }

    public DataTable GetdesCity()
    {
        return service.GetdesCity();
    }

    [HttpPost]
    public DataTable GetdesLocation([FromBody] ProjectMovementModel.GetcityID descity)
    {
        return service.GetdesLocation(descity);
    }
    [HttpPost]
    public DataTable GetdesFloor([FromBody] ProjectMovementModel.GetlocID loc)
    {
        return service.GetdesFloor(loc);
    }
    //[HttpGet]
    //public DataTable Getfrmproj()
    //{
    //    var Aurid =  Convert.ToString(HttpContext.Current.Session["UID"]);
    //    return service.Getfrmproj(Aurid);
    //}

    [HttpPost]
    public DataTable Getsourceverticaldetails([FromBody] ProjectMovementModel.Getseatdetails obj)
    {
        return service.GetSourceverticalDetails(obj);
    }

    //[HttpPost]
    //public DataTable Getdesseatdetails([FromBody] ProjectMovementModel.Getseatdetails obj)
    //{
    //    return service.GetdesSeatTypeDetails(obj);
    //}

    [HttpPost]
    public DataTable Getdestseatdetails([FromBody] ProjectMovementModel.Getseatdetails obj)
    {
        return service.GetdestSeatTypeDetails(obj);
    }
}