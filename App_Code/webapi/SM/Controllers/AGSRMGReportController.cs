﻿using Microsoft.Reporting.WebForms;
using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

public class AGSRMGReportController : ApiController
{
    AGSRMGReportService AGSRMGReportService = new AGSRMGReportService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage BindGrid(AGSRMGReportModel DET)
    {
        var obj = AGSRMGReportService.GetSpaceDataDump(DET);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetReportDownload(AGSRMGReportModel DET)
    {
        var viewer = new ReportViewer();
        viewer.ProcessingMode = ProcessingMode.Local;
        viewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/ShiftUtilizationReport.rdlc");
        Microsoft.Reporting.WebForms.Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string filenameExtension;
        DataSet dataSet = AGSRMGReportService.GetSpaceDataDump_dataset(DET);
        viewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", dataSet.Tables[0]));
        //viewer.LocalReport.DataSources.Add(new ReportDataSource("Dataset2", dataSet.Tables[1]));
        viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
        viewer.LocalReport.Refresh();
        String apptype = GetApplicationName("xlsx");
        byte[] bytes = viewer.LocalReport.Render(
            apptype, null, out mimeType, out encoding, out filenameExtension,
            out streamids, out warnings);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceSubTypeReport.xlsx");
        using (FileStream fs = new FileStream(filePath, FileMode.Create))
        {
            fs.Write(bytes, 0, bytes.Length);
        }
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceSubTypeReport.xlsx";
        return result;

    }

    public String GetApplicationName(String ext)
    {
        switch (ext)
        {
            case "xls": return "Excel";
            case "xlsx": return "EXCELOPENXML";
            case "doc": return "Word";
            case "pdf": return "PDF";
        }
        return "PDF";
    }

}