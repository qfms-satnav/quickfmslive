﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class EntityWiseController : ApiController
{
    EntityWiseReportService ENT = new EntityWiseReportService();
    [HttpPost]
    public HttpResponseMessage GetSummary(SpaceConsolidatedParameters Params)
    {
        var obj = ENT.GetSummary(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SearchData(SpaceConsolidatedParameters Params)
    {
        var obj = ENT.SearchData(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
