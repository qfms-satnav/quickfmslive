﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SpaceDataReportController : ApiController
{
    SpaceDataReporttService Add = new SpaceDataReporttService();


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetData([FromBody] AddHerenceModel data)
    {
        var obj = Add.GetData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage NotBookButCametoOfc([FromBody] AddHerenceModel data)
    {
        var obj = Add.NotBookButCametoOfc(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
