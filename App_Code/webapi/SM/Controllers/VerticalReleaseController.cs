﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;

/// <summary>
/// Summary description for VerticalReleaseController
/// </summary>
public class VerticalReleaseController : ApiController
{
    VerticalReleaseService VRS = new VerticalReleaseService();

    [HttpPost]
    public HttpResponseMessage GetSpaceAllocationsForVerticalRelease([FromBody] VerticalReleaseDetails vrvm)
    {
        var obj = VRS.GetSpaceAllocationsForVerticalRelease(vrvm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UpdateSSAAndInsertVerticalRelease([FromBody] VerticalReleaseVM updtvrdtls)
    {
        var obj = VRS.UpdateSSAAndInsertVerticalRelease(updtvrdtls);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}