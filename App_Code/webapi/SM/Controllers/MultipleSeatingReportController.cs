﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class MultipleSeatingReportController : ApiController
{

    MultipleSeatingReportService MSRS = new MultipleSeatingReportService();
    [HttpGet]
    public HttpResponseMessage GetMultipleSeatingDetails()  
    {
        var obj = MSRS.GetMultipleSeatingDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
