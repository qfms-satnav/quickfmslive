﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class SpaceMancomReport1Controller : ApiController
{
    SpaceMancomReportService ECRS = new SpaceMancomReportService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetSpaceMancomReport()
    {
        var obj = ECRS.GetSpaceMancomReport();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetMancomReportdata(Company cnp)
    {

        ReportGenerator<SpaceMancomReportModel> reportgen = new ReportGenerator<SpaceMancomReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/MancomReport.rdlc"),
            DataSetName = "DataSet1",
            ReportType = "Mancom Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/MancomReport." + cnp.Type);
        List<SpaceMancomReportModel> reportdata = ECRS.GetMancomreport(cnp);
        await reportgen.GenerateReport(reportdata, filePath, cnp.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "Mancom." + cnp.Type;
        return result;

    }
}
