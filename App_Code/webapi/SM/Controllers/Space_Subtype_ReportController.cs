﻿using Microsoft.Reporting.WebForms;
using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class Space_Subtype_ReportController : ApiController
{
    Space_Subtype_ReportService Subtype_ReportService = new Space_Subtype_ReportService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage BindGrid(Space_Subtype_ReportModel DET)
    {
        var obj = Subtype_ReportService.GetSpaceDataDump(DET);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[GzipCompression]
    //[HttpPost]
    //public async Task<HttpResponseMessage> GetReportDownload(Space_Subtype_ReportModel DET)
    //{
    //    ReportGenerator< Space_Subtype_ReportModel> reportgen = new ReportGenerator<Space_Subtype_ReportModel>()
    //    {
    //        ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/Space_SubType_Report.rdlc"),
    //        DataSetName = "Space_Subtype_Report",
    //        //ReportType = "Customized Report",
    //        //Vertical = data.VERTICAL,
    //        //Costcenter = data.COSTCENTER
    //    };
    //    DataTable reportdata = Subtype_ReportService.GetSpaceDataDump_details(DET);
    //    string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceSubTypeReport.xlsx");
    //    await reportgen.GenerateReport(reportdata, filePath, "xlsx");
    //    HttpResponseMessage result = null;
    //    result = Request.CreateResponse(HttpStatusCode.OK);
    //    result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
    //    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
    //    result.Content.Headers.ContentDisposition.FileName = "SpaceSubTypeReport.xlsx";
    //    return result;

    //}
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetReportDownload(Space_Subtype_ReportModel DET)
    {
        var viewer = new ReportViewer();
        viewer.ProcessingMode = ProcessingMode.Local;
        viewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/Space_SubType_Report.rdlc");
        Microsoft.Reporting.WebForms.Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string filenameExtension;
        DataSet dataSet = Subtype_ReportService.GetSpaceDataDump_dataset(DET);
        viewer.LocalReport.DataSources.Add(new ReportDataSource("Space_Subtype_Report", dataSet.Tables[0]));
        viewer.LocalReport.DataSources.Add(new ReportDataSource("Space_Subtype_Report1", dataSet.Tables[1]));
        viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
        viewer.LocalReport.Refresh();
        String apptype = GetApplicationName("xlsx");
        byte[] bytes = viewer.LocalReport.Render(
            apptype, null, out mimeType, out encoding, out filenameExtension,
            out streamids, out warnings);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceSubTypeReport.xlsx");
        using (FileStream fs = new FileStream(filePath, FileMode.Create))
        {
            fs.Write(bytes, 0, bytes.Length);
        }
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceSubTypeReport.xlsx";
        return result;

    }

    public String GetApplicationName(String ext)
    {
        switch (ext)
        {
            case "xls": return "Excel";
            case "xlsx": return "EXCELOPENXML";
            case "doc": return "Word";
            case "pdf": return "PDF";
        }
        return "PDF";
    }

}