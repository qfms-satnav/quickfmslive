﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for SpaceHistoryReportCalController
/// </summary>
public class SpaceHistoryReportCalController : ApiController
{
    SpaceHistoryReportCalService shrs = new SpaceHistoryReportCalService();

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetDailyCount([FromBody] SpaceHistoryReportCalVM data)
    {
        var obj = shrs.GetDailyCount(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetDayData([FromBody] SpaceHistoryReportCalVM data)
    {
        var obj = shrs.GetDayData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}