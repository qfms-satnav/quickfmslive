﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Text;

public class UploadSpaceAllocationController : ApiController
{
    UploadSpaceAllocationService servc = new UploadSpaceAllocationService();

    [GzipCompression]
    [HttpPost]

    public async Task<HttpResponseMessage> DownloadTemplate([FromBody] UploadSpacesVM spcdet) 
    {
        string parent = "Vertical", child = "Costcenter";
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AMT_BSM_GETALL"))
        {
            if (sdr.Read())
            {
                parent = Convert.ToString(sdr["AMT_BSM_PARENT"]);
                child = Convert.ToString(sdr["AMT_BSM_CHILD"]);
            }
        }
        ReportGenerator<UploadAllocationDataDumpVM> reportgen = new ReportGenerator<UploadAllocationDataDumpVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/UploadSpaceAllocation.rdlc"),
            DataSetName = "DownloadAllocDT",
            Vertical = parent,
            Costcenter = child
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceUploadTemplate.xlsx");
        List<UploadAllocationDataDumpVM> reportdata = servc.DownloadTemplate(spcdet);
        await reportgen.GenerateReport(reportdata, filePath, "xlsx");
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceUploadTemplate.xlsx";
        return result;

    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = servc.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]

    public HttpResponseMessage DownloadTemplate1([FromBody] UploadSpacesVM spcdet)
    {
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceUploadTemplate.xlsx");
        string parent = "Vertical", child = "Costcenter";
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AMT_BSM_GETALL"))
        {
            if (sdr.Read())
            {
                parent = Convert.ToString(sdr["AMT_BSM_PARENT"]);
                child = Convert.ToString(sdr["AMT_BSM_CHILD"]);
            }
        }



        var reportdata = servc.DownloadTemplate(spcdet);

        HttpResponseMessage result = null;
        result = Request.CreateResponse();

        byte[] bytes;
        if (reportdata.Count() > 1)
        {
            CreateUploadSpaceAllocExcel.GenerateExcel(reportdata, filePath, parent, child);
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                bytes = new byte[stream.Length];
                stream.Read(bytes, 0, (int)stream.Length);
            }
        }
        else
        {
            bytes = Encoding.ASCII.GetBytes("No Data Found");
        }
        result.Content = new ByteArrayContent(bytes);
        //result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));

        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceUploadTemplateRostering.xlsx";
        result.StatusCode = HttpStatusCode.OK;
        return result;

    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage UploadTemplate1()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = servc.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }




    [GzipCompression]
    [HttpPost]

    public HttpResponseMessage DownloadTemplateRostering([FromBody] UploadSpacesVM spcdet)
    {
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceUploadTemplateRostering.xlsx");

        var reportdata = servc.DownloadTemplateForRostering(spcdet);

        var availableShift = servc.GetShiftInformationBasedOnLocation(spcdet);
        //await reportgen.GenerateReport(reportdata, filePath, "xlsx");

        CreateRosterExcel.GenerateExcel(reportdata, availableShift, filePath);

        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceUploadTemplateRostering.xlsx";
        return result;

    }
    public HttpResponseMessage UploadTemplateRostering()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = servc.UploadTemplateForRostering(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
