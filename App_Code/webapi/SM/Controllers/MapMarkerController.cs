﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class MapMarkerController : ApiController
{
    MapMarkerService servc = new MapMarkerService();
    [HttpGet]
    public Object GetCitywiseLoc()
    {
        return servc.GetCitywiseLoc();
    }

    [HttpGet]
    public Object GetMarkers()
    {
        return servc.GetMarkers();
    }
}
