﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

public class CustomizedReportController : ApiController
{
    CustomizedReportService Csvc = new CustomizedReportService();
    CustomizedReportView report = new CustomizedReportView();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(CustomizedDetails Custdata)
    {
        var obj = Csvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedDetailsPivotExcel(CustomizedDetails Custdata)
    {
        var obj = Csvc.GetCustomizedDetailsPivotExcel(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedDatadwn([FromBody]CustomizedDetails data)
    {
        ReportGenerator<CustomizedData> reportgen = new ReportGenerator<CustomizedData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/CustomizedReport.rdlc"),
            DataSetName = "CustomizedReport",
            ReportType = "Customized Report",
            Vertical = data.VERTICAL,
            Costcenter = data.COSTCENTER,
            BH1 = data.BH1,
            BH2 = data.BH2,
            PE = data.PE,
            CE = data.CE
        };

        Csvc = new CustomizedReportService();
        DataTable reportdata = Csvc.GetCustomizedDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CustomizedReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CustomizedReport." + data.Type;
        return result;
        //Csvc = new CustomizedReportService();
        //Guid gid = Guid.NewGuid();
        //DataTable reportdata = Csvc.GetCustomizedDetails_dump(data);
        //string filePath = HttpContext.Current.Server.MapPath("~/SMViews/Report_Output/CustomizedReport." + data.Type);
        //await reportgen.GenerateReport(reportdata, filePath, data.Type);

        //object obj = new { data = gid };
        //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        //return response;
    }
    //[HttpPost]
    //public HttpResponseMessage GetCustomizedDatadwn([FromBody]CustReport data)
    //{
    //    HttpResponseMessage result = null;
    //    string filePath = HttpContext.Current.Server.MapPath("~/SMViews/Report_Output/" + data.Request_Type + "." + data.Type);
    //    result = Request.CreateResponse(HttpStatusCode.OK);
    //    result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
    //    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
    //    result.Content.Headers.ContentDisposition.FileName = "CustomizedReport." + data.Type;
    //    return result;
    //}

    [HttpPost]
    public HttpResponseMessage SearchAllData(CustomizedDetails Custdata)
    {
        var obj = Csvc.SearchAllData(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
