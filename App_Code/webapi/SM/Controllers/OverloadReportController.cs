﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class OverloadReportController : ApiController
{
    OverloadService service = new OverloadService();
    [HttpPost]
    public HttpResponseMessage GetOverloadReportBindGrid(CompanySpace CNPDet)
    {
        var obj = service.GetOverloadObject(CNPDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Chart Data

    [HttpPost]
    public HttpResponseMessage GetOverloadChartData(String ID)
    {
        var obj = service.GetOverloadChart(ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> OverloadReportData(CompanySpace CNPDet)
    {
        OverloadParams over = new OverloadParams();

        ReportGenerator<OverloadReportModel> reportgen = new ReportGenerator<OverloadReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/OverloadReport.rdlc"),
            DataSetName = "OverloadReportSPC",
            ReportType = "Overload Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/OverloadReport." + CNPDet.Type);
        List<OverloadReportModel> reportdata = service.GetOverloadReportList(CNPDet);
        await reportgen.GenerateReport(reportdata, filePath, CNPDet.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "OverloadReport." + CNPDet.Type;
        return result;

    }
}
