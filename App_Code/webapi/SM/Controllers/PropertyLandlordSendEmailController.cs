﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;

public class PropertyLandlordSendEmailController : ApiController
{
    PropertyLandlordSendEmailService frmPropertyLandlordSendEmail = new PropertyLandlordSendEmailService();

    //Grid View
    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetPropertyType()
    {
        string SLAlist = frmPropertyLandlordSendEmail.GetPropertyType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SLAlist);
        return response;
    }

    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetLLOwnerNameList(int PM_PPT_SNO)
    {
        string SLAlist = frmPropertyLandlordSendEmail.GetLLOwnerName(PM_PPT_SNO);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SLAlist);
        return response;
    }

    [System.Web.Http.HttpGet]
    public HttpResponseMessage GetPropertList(int PN_TYPEID)
    {
        string Propertylist = frmPropertyLandlordSendEmail.GetProperty(PN_TYPEID);

        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Propertylist);
        return response;
    }

    [System.Web.Http.HttpPost]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
    public HttpResponseMessage SendEmail()
    {
        var req = new StreamReader(HttpContext.Current.Request.InputStream);
        req.BaseStream.Seek(0, SeekOrigin.Begin);
        dynamic body = req.ReadToEnd();

        //The Below Code was used when i change the request format
        var token = JToken.Parse(body);
        string sData = token["msg"].ToString();
        string[] mail = token.email.ToObject<string[]>();
        string[] llName = token.LLName.ToObject<string[]>();
        for (int i = 0; i < mail.Length; i++)
        {
            frmPropertyLandlordSendEmail.SendEmail(mail[i].ToString(), sData, llName[i].ToString());
        }

        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Email sent successfully");
        return response;
    }
}
