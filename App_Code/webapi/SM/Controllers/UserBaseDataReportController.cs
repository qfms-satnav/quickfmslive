﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class UserBaseDataReportController : ApiController
{
    UserBaseDataReportService Add = new UserBaseDataReportService();


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetData([FromBody] UserBaseDataReportModel data)
    {
        var obj = Add.GetData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}
