﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class L2ApprovalController : ApiController
{
    L2ApprovalService L2Approval = new L2ApprovalService();

    [HttpGet]
    public HttpResponseMessage GetPendingSpaceRequisitions()
    {
        var spclist = L2Approval.GetPendingSpaceRequisitions();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }

   
    [HttpPost]
    public HttpResponseMessage ApproveRequisitions([FromBody]SpaceReqDetails spcreqdet)
    {
        var obj = L2Approval.ApproveRequisitions(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ApproveAllReq([FromBody]SpaceReqDetails spcreqdet)
    {
        var obj = L2Approval.ApproveAllReq(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
