﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;


public class EntityAdminController: ApiController
{
    EntityAdminService service = new EntityAdminService();

    [HttpPost]
    public HttpResponseMessage Create(EntityAdminVM EntityAdmin)
    {
        if (service.Save(EntityAdmin) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, EntityAdmin);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
        }
    }
    [HttpPost]
    public HttpResponseMessage UpdateEntityAdminData(EntityAdminVM update)
    {
        if (service.UpdateEntityAdminData(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest);
    }
    [HttpGet]
    public HttpResponseMessage BindEntityGrid()
    {
        IEnumerable<EntityAdminVM> EntityAdminList = service.BindEntityGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, EntityAdminList);
        return response;
    }   

}