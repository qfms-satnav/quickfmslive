﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using QuickFMS.API.Filters;

public class SpaceMasterController : ApiController
{
    SpaceMasterService ss = new SpaceMasterService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetVacantSpaces([FromBody] SpaceMasterDetails spc)
    {
        var obj = ss.GetVacantSpaces(spc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetSeatType()
    {
        var obj = ss.GetSeatType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage RaiseRequest([FromBody] SpaceMasterDetails spc)
    {
        var obj = ss.RaiseRequest(spc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage SeatSubTypes()
    {
        var obj = ss.SeatSubTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage SpaceSubTypes()
    {
        var obj = ss.SpaceSubTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage UpdateRequest([FromBody] SpaceMasterDetails spc)
    {
        var obj = ss.UpdateRequest(spc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}
