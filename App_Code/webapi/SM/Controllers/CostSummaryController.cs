﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CostSummaryController
/// </summary>
public class CostSummaryController: ApiController
{
    CostSummaryService cs = new CostSummaryService();
    [HttpPost]
    public HttpResponseMessage GetCostReportBindGrid(Company cstvm)
    {
        var obj = cs.GetCostReportObject(cstvm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}