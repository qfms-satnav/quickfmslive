﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Net.Http;
/// <summary>
/// Summary description for TradeMomentOfEmployeeRptController
/// </summary>
public class TrailMomentOfEmployeeRptController : ApiController
{
    TrailMomentOfEmployeeRptService MSRS = new TrailMomentOfEmployeeRptService();
    [HttpGet]
    public HttpResponseMessage GetTrailMomentOfEmployeeRpt()
    {
        var obj = MSRS.GetTrailMomentOfEmployeeRpt();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}