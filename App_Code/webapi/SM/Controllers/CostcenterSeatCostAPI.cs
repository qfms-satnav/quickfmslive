﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

public class CostcenterSeatCostAPIController : ApiController
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    CostcenterSeatCostService service = new CostcenterSeatCostService();
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    public Object GetCostcenterDetails()
    {
        return service.GetCostcenterDetails();
    }

    [HttpPost]

    public object GetCostcenterChart([FromBody] string costcenterseatcost)
    {
        return service.GetCostcenterChart(costcenterseatcost);
    }
   
}
