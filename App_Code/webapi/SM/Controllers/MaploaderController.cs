﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class MaploaderAPIController : ApiController
{
    MaploaderService mspsrvc = new MaploaderService();

    [GzipCompression]
    [HttpGet]
   // [Authorize]
    //[ValidateAntiForgeryToken]
    public Object GetFloorLst()
    {
        return mspsrvc.GetFloorLst();
    }
    [HttpGet]
    public Object GetImageList([FromUri] string Image)
    {
        return mspsrvc.GetImageList(Image);
    }

    [HttpGet]
    // [Authorize]
    //[ValidateAntiForgeryToken]
    public Object validateuserspacedetails()
    {
        return mspsrvc.validateuserspacedetails();
    }


    [GzipCompression]
    [HttpPost]
    public Object GetMapItems([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetMapItems(svm);
    }

    [GzipCompression]
    [HttpGet]
    public Object GetEmpSeatDtlsHotDesk([FromUri] string svm)
    {
        var obj = mspsrvc.GetEmpSeatDtlsHotDesk(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    


    [GzipCompression]
    [HttpPost]
    public object approvalallocateSeats(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        return mspsrvc.approvalallocateSeats(allocDetLst);
    }

    [GzipCompression]
    [HttpPost]
    public Object GET_VERTICAL_WISE_ALLOCATIONS([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GET_VERTICAL_WISE_ALLOCATIONS(svm);
    }

    [GzipCompression]
    [HttpPost]
    // [ValidateAntiForgeryToken]
    //[SessionState(SessionStateBehavior.ReadOnly)]
    public Object GetMarkers([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetMarkers(svm);
    }

    [GzipCompression]
    [HttpPost]
    public Object GetCornerLables([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetCornerLables(svm);
    }

    [GzipCompression]
    [HttpPost]
    // [ValidateAntiForgeryToken]
    //[SessionState(SessionStateBehavior.ReadOnly)]
    public Object SeatRestriction([FromBody]Space_mapVM svm)
    {
        return mspsrvc.SeatRestriction(svm);
    }

    [GzipCompression]
    [HttpPost]
    public object SEAT_RESTRICTION_BOOKING(SPACE_ALLOC_DETAILS_TIME data)
    {
        return mspsrvc.SEAT_RESTRICTION_BOOKING(data);
    }

    [GzipCompression]
    [HttpGet]
    public Object GETEMPLOYEETYPE()
    {
        return mspsrvc.GETEMPLOYEETYPE();
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetSpaceRecords([FromBody]Space_mapVM svm)
    {
        var obj = mspsrvc.InactiveSpacesFromFloorMaps(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    // [ValidateAntiForgeryToken]
    //[SessionState(SessionStateBehavior.ReadOnly)]
    public Object GradeAllocStatus()
    {
        return mspsrvc.GradeAllocStatus();
    }

    [GzipCompression]
    [HttpGet]
    // [ValidateAntiForgeryToken]
    //[SessionState(SessionStateBehavior.ReadOnly)]
    public Object getSpaceDaysRestriction()
    {
        return mspsrvc.getSpaceDaysRestriction();
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getVer_And_CC([FromBody] AUR_DETAILS data)
    {
        var obj = mspsrvc.getVer_And_CC(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [GzipCompression]
    [HttpPost]
    public Object GetLegendsCount([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetLegendsCount(svm);
    }


    [GzipCompression]
    [HttpPost]
    public Object GetLegendsSummary([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetLegendsSummary(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetSpaceDetailsBySPCID([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsBySPCID(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetSpaceDetailsBySUBITEM([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsBySUBITEM(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetSpaceDetailsByREQID([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsByREQID(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetEmpDetails([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetEmpDetails(svm);
    }


    [GzipCompression]
    [HttpGet]
    public object GetallFilterbyItem()
    {
        return mspsrvc.GetallFilterbyItem();
    }


    [GzipCompression]
    [HttpPost]
    public object GetallFilterbySubItem([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetallFilterbySubItem(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetAllocEmpDetails([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetAllocEmpDetails(svm);
    }

    [GzipCompression]
    [HttpPost]
    public object InactiveSpaceSeats(CLS_INACTIVE_SPACE CIS)
    {
        return mspsrvc.InactiveSeats(CIS);
    }

    [GzipCompression]
    [HttpPost]
    public object ReleaseSelectedseat(SPACE_REL_DETAILS sad)
    {
        return mspsrvc.ReleaseSelectedseat(sad);
    }

    [GzipCompression]
    [HttpPost]
    public object ActivateSpaces(CLS_INACTIVE_SPACE CIS)
    {
        return mspsrvc.ActivateSpaces(CIS);
    }

    [GzipCompression]
    [HttpPost]
    public object AllocateSeats(REQ_DATA requestData)
    {
        return mspsrvc.AllocateSeats(requestData);
    }

    [GzipCompression]
    [HttpPost]
    public object AllocateSeatsPopUp(CP_SPACE_ALLOCATE_DETAILS_LST requestData)
    {
        return mspsrvc.AllocateSeatsPopUp(requestData);
    }
    [GzipCompression]
    [HttpGet]
    public object GetAminities()
    {
        return mspsrvc.GetAminities();
    }

    [GzipCompression]
    [HttpGet]
    public object GetAminitiesBySpace(string spaceId)
    {
        return mspsrvc.GetAminitiesBySpace(spaceId);
    }

    [GzipCompression]
    [HttpPost]
    public object allocateSeatsValidation(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        return mspsrvc.allocateSeatsValidation(allocDetLst);
    }

    [GzipCompression]
    [HttpPost]
    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS data)
    {
        return mspsrvc.SpcAvailabilityByShift(data);
    }

    [GzipCompression]
    [HttpPost]
    public object GetTotalAreaDetails(Space_mapVM data)
    {
        return mspsrvc.GetTotalAreaDetails(data);
    }

    [GzipCompression]
    [HttpPost]
    public object SpcAvailabilityByShiftEdit(SPACE_ALLOC_DETAILS_EDIT data)
    {
        return mspsrvc.SpcAvailabilityByShiftEdit(data);
    }

    [GzipCompression]
    [HttpPost]
    public object GetSeatingCapacity(Space_mapVM data)
    {
        return mspsrvc.GetSeatingCapacity(data);
    }

    [GzipCompression]
    [HttpPost]
    public object GetEmpAllocSeat(Space_mapVM data)
    {
        return mspsrvc.GetEmpAllocSeat(data);
    }

    //[HttpPost]
    //public DataTable Post(MaploaderVM mapVm)
    //{
    //    return Ok(mspsrvc.Post(mapVm));
    //}

    //[HttpPut]
    //public IHttpActionResult Put(MaploaderVM mapVm)
    //{
    //    return Ok(mspsrvc.Put(mapVm));
    //}

    //[HttpDelete]
    //public void Delete(MaploaderVM mapVm)
    //{
    //    mspsrvc.Delete(mapVm);
    //}

    //[HttpGet]
    //public IHttpActionResult GetLayerDet()
    //{
    //    return Ok(mspsrvc.GetLayerDet());
    //}

    //[HttpPost]
    //public IHttpActionResult GetMarkerDet(MaploaderVM mapvm)
    //{
    //    return Ok(mspsrvc.GetMarkerDet(mapvm));
    //}

    //[HttpPost]
    //public IHttpActionResult InsertMarkerDet(MaploaderVM mapvm)
    //{
    //    return Ok(mspsrvc.InsertMarkerDet(mapvm));
    //}
}
