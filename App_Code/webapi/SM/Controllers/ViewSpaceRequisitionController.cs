﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ViewSpaceRequisitionController : ApiController
{
    ViewSpaceRequisitionService ViewSpaceReq = new ViewSpaceRequisitionService();

    [HttpGet]
    public HttpResponseMessage GetPendingSpaceRequisitions()
    {
        var spclist = ViewSpaceReq.GetPendingSpaceRequisitions();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage UpdateSpaceRequisition([FromBody]SpaceReqDetails spcreqdet)
    {
        var obj = ViewSpaceReq.UpdateSpaceRequisition(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
   
}
