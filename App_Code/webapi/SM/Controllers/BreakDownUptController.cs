﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.IO;
using UtiltiyVM;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Globalization;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class BreakDownUptController : ApiController
{
    BreakDownUptService BDMsrvc = new BreakDownUptService();

    // GET Locations
    //[GzipCompression]
    //[HttpGet]
    //public Object getLocations()
    //{
    //    return BDMsrvc.getLocations();
    //}
    // GET Impact
    [GzipCompression]
    [HttpGet]
    public Object getImpact()
    {
        return BDMsrvc.getImpact();
    }
    // GET Startus
    [GzipCompression]
    [HttpGet]
    public Object getStartus()
    {
        return BDMsrvc.getStartus();
    }
    // GET Problem Category
    [GzipCompression]
    [HttpGet]
    public Object getProblemCategory()
    {
        return BDMsrvc.getProblemCategory();
    }
    // GET Ticket Type
    [GzipCompression]
    [HttpGet]
    public Object getTicketType()
    {
        return BDMsrvc.getTicketType();
    }
    // GET Ticket Type
    [GzipCompression]
    [HttpGet]
    public Object getRCAStatus()
    {
        return BDMsrvc.getRCAStatus();
    }

    // GET Equipment
    [GzipCompression]
    [HttpGet]
    public Object getEquipment([FromUri] string BLDG)
    {
        return BDMsrvc.getEquipment(BLDG);
    }

    // GET Error Desc
    [GzipCompression]
    [HttpGet]
    public Object getErrorDesc([FromUri] string equipment)
    {
        return BDMsrvc.getErrorDesc(equipment);
    }

    // GET Error Description
    [GzipCompression]
    [HttpPost]
    public Object getErrorDescription(ErrorDescription svm)
    {
        return BDMsrvc.getErrorDescription(svm);
    }

    // GET Assign to
    [GzipCompression]
    [HttpPost]
    public Object getAssignto(getAssignto svm)
    {
        return BDMsrvc.getAssignto(svm);
    }

    // GET Spare Parts
    [GzipCompression]
    [HttpPost]
    public Object getSpareParts(getAssignto svm)
    {
        return BDMsrvc.getSpareParts(svm);
    }

    // GET Request Ids
    [GzipCompression]
    [HttpPost]
    public Object getBreakDownRqsts(getAssignto svm)
    {
        return BDMsrvc.getBreakDownRqsts(svm);
    }

    [GzipCompression]
    [HttpPost]
    public Object GetDescrption(CustomizableRptVM svm)
    {
        return BDMsrvc.GetDescrption(svm);
    }
    // GET Urgency
    [GzipCompression]
    [HttpGet]
    public Object getUrgency()
    {
        return BDMsrvc.getUrgency();
    }
    // RCA SAVE
    [GzipCompression]
    [HttpPost]
    public object SaveRCA(breakdownRCA sad)
    {
        HttpContext.Current.Session["FileId"] = sad.NEWDATE;
        return BDMsrvc.SaveRCA(sad);
    }
    [GzipCompression]
    [HttpPost]
    public Object getRCAData(RCARpt CustRpt)
    {
        return BDMsrvc.getRCAData(CustRpt);
    }
    //public HttpResponseMessage GetRCAData(RCARpt CustRpt)
    //{
    //    var obj = BDMsrvc.GetRCAData(CustRpt);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}

    // DownTime SAVE
    [GzipCompression]
    [HttpPost]
    public object SaveDownTime(breakdowntime sad)
    {
        return BDMsrvc.SaveDownTime(sad);
    }
    [GzipCompression]
    [HttpPost]
    public Object getDownTimeData(DownTimeRpt CustRpt)
    {
        return BDMsrvc.getDownTimeData(CustRpt);
    }

    [GzipCompression]
    [HttpPost]
    public object DeleteDownTime(breaktime sad)
    {
        return BDMsrvc.DeleteDownTime(sad);
    }

    [GzipCompression]
    [HttpPost]
    public object DeleteRCA(breakRCA sad)
    {
        return BDMsrvc.DeleteRCA(sad);
    }

    [GzipCompression]
    [HttpPost]
    public Object getSpareData(SpareRpt CustRpt)
    {
        return BDMsrvc.getSpareData(CustRpt);
    }

    [GzipCompression]
    [HttpPost]
    public object DeleteSpare(breakSpare sad)
    {
        return BDMsrvc.DeleteSpare(sad);
    }

    // GET Submit
    [GzipCompression]
    [HttpPost]
    public object save(breakdownUpdate sad)
    {       
        return BDMsrvc.save(sad);
    }
    // GET Submit
    [GzipCompression]
    [HttpPost]
    public object submit(breakdownSubmit sad)
    {

        HttpContext.Current.Session["FileId"] = sad.NEWDATE;    
        //UploadFiles(sad.REQID);
        return BDMsrvc.submit(sad);
    }

    //Upload Files
    [GzipCompression]
    [HttpPost]
    public object UploadFiles()
    {
        int iUploadedCnt = 0;
        // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
        string fileId = HttpContext.Current.Session["FileId"].ToString();
        string sPath = HttpContext.Current.Session["TENANT"].ToString();     
        // var fileId = (Request.Properties["FileId"] as HttpContext).ToString();
        //string sPath = (Request.Properties["TENANT"] as HttpContext).ToString();
        //fileId = fileId.Replace("/", "_");
        //bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/" + fileId + "/"));
        //if (!exists)
        //    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/" + fileId + "/"));
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/");
        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
        //System.IO.DirectoryInfo di = new DirectoryInfo(sPath);        
        // CHECK THE FILE COUNT.
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                //foreach (FileInfo file in di.GetFiles())
                //{
                //    if(file.Name== hpf.FileName)
                //    {
                //        file.Delete();
                //    }
                //}
                string P = fileId + hpf.FileName;
                //(DateTime.Now.ToString("ddMMyyyy").ToString())
                hpf.SaveAs(sPath + Path.GetFileName(P));
                iUploadedCnt = iUploadedCnt + 1;
            }
        }
        // RETURN A MESSAGE.
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }

    // GET Locations
    [GzipCompression]
    [HttpGet]
    public Object getProblemOwner()
    {
        return BDMsrvc.getProblemOwner();
    }
    [GzipCompression]
    [HttpPost]
    public object GetBreakDownDetails(breakdownSubmit reldet)
    {
        return BDMsrvc.GetBreakDownDetails(reldet);
    }
}
