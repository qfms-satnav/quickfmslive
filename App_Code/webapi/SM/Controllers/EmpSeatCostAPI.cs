﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

public class EmpSeatCostAPIController : ApiController
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    EmpSeatCostService service = new EmpSeatCostService();
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }
    [HttpPost]
    public object GetEmpSeatCost([FromBody] EmpSeatCostVM ObjGridSummary)
    {
        return service.GetEmpSeatCost(ObjGridSummary);
    }
    [HttpPost]
    public object GetEmpSeatCount([FromBody] EmpSeatCostVM ObjGridSummary)
    {
        return service.GetEmpSeatCount(ObjGridSummary);
    }

    [HttpPost]
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }

}
