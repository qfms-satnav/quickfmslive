﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SpaceSeatBookingHistroyController : ApiController
{
    SpaceSeatBookingHistroy SSBH = new SpaceSeatBookingHistroy();
    [GzipCompression]
    [HttpPut]
    public object SpaceSeatBookingHistroy()
    {
        return SSBH.SpaceSeatBookingHistry();
    }

    [GzipCompression]
    [HttpPost]
    public object allocateSeatsValidation(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        return SSBH.allocateSeatsValidation(allocDetLst);
    }

}
