﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

public class SpaceAvailabilityReportController : ApiController
{
    SpaceAvailabilityReportService AvilSvc = new SpaceAvailabilityReportService();
    AvailReport Rpt = new AvailReport();

    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetAvailReport([FromBody] SpaceAvailDetails SpcAvl)
    {
        ReportGenerator<SpaceAvailData> GenAllocReport = new ReportGenerator<SpaceAvailData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceAvailabilityReport.rdlc"),
            DataSetName = "SpaceAvailabilityReportDS",
            ReportType = "Space Availability Report"
        };
        AvilSvc = new SpaceAvailabilityReportService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceAvailabilityReport." + SpcAvl.Type);
        List<SpaceAvailData> reportdata = AvilSvc.ExportExcel(SpcAvl);
        await GenAllocReport.GenerateReport(reportdata, filePath, SpcAvl.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceAvailability." + SpcAvl.Type;
        return result;
    }

    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetVacantSeatReport([FromBody] SpaceAvailData SpcAvl)
    {
        SpaceAvailData Avail = new SpaceAvailData();
        ReportGenerator<SpaceAvailData> GenAllocReport = new ReportGenerator<SpaceAvailData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/VacantSeatReport.rdlc"),
            DataSetName = "SpaceVacantSeatsDS",
            ReportType = "Vacant Seat Report"

        };
        AvilSvc = new SpaceAvailabilityReportService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/VacantSeatReport." + SpcAvl.Type);
        List<SpaceAvailData> reportdata = AvilSvc.GetVacantSeatList(SpcAvl);
        await GenAllocReport.GenerateReport(reportdata, filePath, SpcAvl.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "VacantSeatReport." + SpcAvl.Type;
        return result;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetAvlGrid(SpaceAvailDetails SpcAvl)
    {
        var obj = AvilSvc.GetAvailDetails(SpcAvl);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetSearchData(SpaceAvailDetails SpcAvl)
    {
        var obj = AvilSvc.GetSearchData(SpcAvl);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(SpaceAvailData Spcdata)
    {
        var obj = AvilSvc.GetSelectedAvlDetails(Spcdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetSpaceAvailabilityChartData(SpaceAvailDetails Spcdata)
    {
        var obj = AvilSvc.GetSpaceAvailabilityChart(Spcdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
