﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using QuickFMS.API.Filters;
/// <summary>
/// Summary description for InfraAdministrationReportController
/// </summary>
public class InfraAdministrationReportController :ApiController
{
    InfraAdministrationReportService IARS = new InfraAdministrationReportService();
    [HttpGet]
    public HttpResponseMessage GetInfraAdministrationReport()
    {
        var obj = IARS.GetInfraAdministrationReport();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetInfraAdministrationReportBySearch(getGridBySearch getGrid)
    {
        var obj = IARS.GetInfraAdministrationReportBySearch(getGrid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedPrjRpt(InfraAdminReportParameters rptCost)
    {

        ReportGenerator<InfraAdminReportParameters> reportgen = new ReportGenerator<InfraAdminReportParameters>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/InfraAdministrationReport.rdlc"),
            DataSetName = "DataSet1",
            ReportType = "Project wise occupied seat count",
            //Costcenter=rptCost.COSTCENTER
            //Vertical=rptCost.VERTICAL,
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/InfraAdministrationReport." + rptCost.DocType);

        List<InfraAdminReportParameters> reportdata = IARS.GetExportLst(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "InfraAdministrationReport." + rptCost.DocType;
        return result;

    }

}