﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;


public class EmployeeLocationReportController : ApiController
{

    EmployeeLocationReportService Csvc = new EmployeeLocationReportService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(EmployeeLocationDetails Empdata)
    {
        var obj = Csvc.GetCustomizedObject(Empdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}