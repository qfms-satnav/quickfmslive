﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
/// <summary>
/// Summary description for SpaceReleaseController
/// </summary>
public class SpaceReleaseController : ApiController
{
    SpaceReleaseService SRS = new SpaceReleaseService();

    [HttpPost]
    public HttpResponseMessage GetSpacesToRelease([FromBody] SPC_RELEASE_FLOOR_VERTICAL_SPACE_LIST srvm)
    {
        var obj = SRS.GetSpacesToRelease(srvm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ReleaseSelectedSpaces([FromBody] SpaceReleaseVM srvm)
    {
        var obj = SRS.ReleaseSelectedSpaces(srvm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}