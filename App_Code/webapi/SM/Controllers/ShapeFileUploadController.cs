﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for ShapeFileUploadController
/// </summary>
/// 
public class check_layout
{
    public string SPACE_ID { get; set; }
    public string EMP_NAME { get; set; }
}
public class ShapeFileUploadAPIController : ApiController
{
    SubSonic.StoredProcedure sp;
    ShapeFileUploadService shp = new ShapeFileUploadService();
    [HttpPost]
    public async Task<Object> UploadShapeFile()
    {
        List<check_layout> laylist = new List<check_layout>();
        check_layout chk;
        List<string> statuslst = new List<string>();
        object database = HttpContext.Current.Session["TENANT"];
        object db = HttpContext.Current.Session["TENANT"];
        database = Convert.ToString(database).Remove(Convert.ToString(database).Length -5);
        database= Convert.ToString(database).Remove(0,1);
        string constr = "Data Source=13.67.46.197;Initial Catalog=" + database + ";user id=amantra;Password=Super@123;Max Pool Size=5000; Connection Timeout=500";
             //string constr = ConfigurationManager.ConnectionStrings["CSAmantraFAM"].ConnectionString;
        String tablename = "";
        SqlDataReader reader;
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd;
        string uploadPath = HttpContext.Current.Server.MapPath("~/ShapeFilesUploads");
        if (Request.Content.IsMimeMultipartContent())
        {
             try
            {
            MultipartFormDataStreamProvider streamProvider = new MultipartFormDataStreamProvider(uploadPath);
                await Request.Content.ReadAsMultipartAsync(streamProvider);
                List<String> args = new List<string>();
                args.Add("-s 4326");
                args.Add("-g GEOM");
                args.Add(constr);
                foreach (var file in streamProvider.FileData)
                {
                    FileInfo fi = new FileInfo(file.LocalFileName);
                    String filename = file.Headers.ContentDisposition.FileName.Trim(new char[] { '"' });
                    fi.MoveTo(uploadPath + "\\" + filename);
                    if (filename.Contains(".shp"))
                    {
                        args.Add(uploadPath + "\\" + filename);
                        tablename = Path.GetFileNameWithoutExtension(filename);
                    }
                    //statuslst.Add("File uploaded as " + fi.FullName + " (" + fi.Length + " bytes)");
                }
           
                shp.UploadShapeFile(args.ToArray());
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(ex);
                statuslst.Add(String.Format("Map with this name {0} already exist", tablename, "Failed"));
            }
            try
            {
                con.Open();
                cmd = new SqlCommand(String.Format("select * from  [{0}] where  geom.STIsValid() = 0 or geom.ToString() like '%multi%'", tablename));
                cmd.Connection = con;
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {

                    statuslst.Add(String.Format("Geometric check {0} {1}", tablename, "Failed"));
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("[");
                    while (reader.Read())
                    {
                        sb.Append("{");
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            sb.Append("'" + reader.GetName(i) + "':" + reader[i].ToString() + ",");
                        }
                        sb.Length -= 1;
                        sb.Append("},");
                    }
                    sb.Length -= 1;
                    reader.Close();
                    sb.Append("]");
                    statuslst.Add(sb.ToString());
                }
                else
                {
                    //statuslst.Add(String.Format("Geometric check {0} {1}", tablename, "Success"));
                    reader.Close();
                    String floordatasql = "select " +
                        "FLR_CODE" +
                        " from FLOOR where FLR_CODE = '" + tablename + "'";
                    cmd = new SqlCommand(floordatasql);
                   cmd.Connection = con;
                    reader = cmd.ExecuteReader();
                    String FLR_CODE = "";
                    if (reader.Read())
                    {
                        FLR_CODE = reader.GetValue(0).ToString();
                        reader.Close();

                        cmd = new SqlCommand(String.Format("select * from  floormaps where  flr_id='{0}'", FLR_CODE));
                       cmd.Connection = con;
                        reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {

                            sp = new SubSonic.StoredProcedure(db + "." + "CHECK_LAYOUT_EXIST");
                            sp.Command.AddParameter("@TABLE", tablename, DbType.String);
                            sp.Command.AddParameter("@FLR_ID", FLR_CODE, DbType.String);

                            using (IDataReader sdr = sp.GetReader())
                            {
                                while (sdr.Read())
                                {
                                    chk = new check_layout();
                                    chk.SPACE_ID = sdr["SSA_SPC_ID"].ToString();
                                    chk.EMP_NAME = sdr["EMP_NAME"].ToString();
                                    laylist.Add(chk);
                                    reader.Close();
                                }
                            }
                            reader.Close();
                            cmd = new SqlCommand(String.Format("delete from floormaps where flr_id='{0}';delete from space where spc_flr_id='{0}'", FLR_CODE));
                            cmd.Connection = con;
                            int rowsdeleted = cmd.ExecuteNonQuery();
                            //statuslst.Add(String.Format("No Of Rows Deleted from floorMaps/Space table {0}", rowsdeleted));
                        }
                        reader.Close();
                        String insertfloormapsqry = String.Format(@"insert into floormaps(AREA_SQFT, ASSET_ID, DISPLAYNAM, FLR_ID, LAYER, MAP_ID, SPACE_ID, STATUS, SUB_TYPE, LONGITUDE, LATITUDE, geom,geomstring,CAPACITY, companyid)
                                    select AREA_SQFT, ASSET_ID, DISPLAYNAM,'{0}',LAYER,MAP_ID,SPACE_ID,STATUS,SUB_TYPE,geom.STCentroid().STX,geom.STCentroid().STY,geom,geom.ToString(),CAPACITY,1 from [{1}]", FLR_CODE, tablename);

                        cmd = new SqlCommand(insertfloormapsqry);
                        cmd.Connection = con;
                        int rowsadded = cmd.ExecuteNonQuery();
                        //statuslst.Add(String.Format("No Of Rows Added into floorMaps table {0}", rowsadded));
                        String sql = "";
                        
                            sql = String.Format(@"INSERT INTO SPACE (SPC_ID,SPC_CTY_ID,SPC_BDG_ID,SPC_TWR_ID,SPC_FLR_ID,SPC_WNG_ID,SPC_TYPE,SPC_LAYER,SPC_SUB_TYPE,SPC_NAME,SPC_VIEW_NAME,SPC_STA_ID,
SPC_X_VALUE,SPC_Y_VALUE,SPC_CREATED_DT,SPC_CREATED_BY,SPC_AREA,SPC_COST_SQFT,SPACE_TYPE,SPC_SEATING_CAPACITY,COMPANYID)
                                        SELECT SPACE_ID,F.FLR_CTY_ID ,FLR_BDG_ID,FLR_TWR_ID,FLR_CODE,'NA',(SELECT SPC_TYPE_NAME FROM SPACE_TYPE WHERE LAYER = SPC_TYPE_CODE) ,LAYER,SUB_TYPE,
(CASE WHEN EXISTS (SELECT SPC_TYPE_NAME FROM SPACE_TYPE WHERE LAYER = SPC_TYPE_CODE AND SPC_TYPE_STATUS = 4) THEN DISPLAYNAM ELSE SPACE_ID END ),
DISPLAYNAM ,1,geom.STCentroid().STY, geom.STCentroid().STX,GETDATE(),'karthik',AREA_SQFT, 25,1,CAPACITY,1
FROM FLOORMAPS FM
INNER JOIN FLOOR F ON F.FLR_CODE = FM.FLR_ID
                                        WHERE flr_code = '{0}' AND LAYER IN(select SPC_TYPE_CODE from space_type) and space_id<> ''",FLR_CODE);
                        
                        cmd = new SqlCommand(sql);
                        cmd.Connection = con;
                        try
                        {
                            rowsadded = cmd.ExecuteNonQuery();
                            if (rowsadded > 0)
                            {
                                //statuslst.Add(String.Format("Inserted into Space Table [ Map Name : {0} ] ", tablename));
                                reader.Close();
                            }
                            else
                            {
                                statuslst.Add(String.Format("Insertion failed into Space Table [ Map Name : {0} ] ", tablename));
                                reader.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            statuslst.Add(String.Format("Error while inserting into Space Table [ Map Name : {0},Error:{1} ]  ", tablename, ex.Message));
                            reader.Close();
                        }

                    }

                    else
                    {
                        statuslst.Add(String.Format("Floor Not found in Floor table"));
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)

            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(ex);
                statuslst.Add(String.Format("{0} {1}", ex.Message, tablename));
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    try
                    {
                        cmd = new SqlCommand("update space set SPACE_TYPE = null where SPC_TYPE in('cc','ss','ua')");
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand("drop table [" + tablename + "]");
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler erhndlr = new ErrorHandler();
                        erhndlr._WriteErrorLog(ex);
                        var err = ex.Message;
                    }
                    
                    con.Close();
                    System.IO.File.Delete(uploadPath + "\\" + tablename + ".shp");
                    System.IO.File.Delete(uploadPath + "\\" + tablename + ".dbf");
                    //System.IO.StreamWriter sw = new StreamWriter(System.IO.File.OpenWrite("log.txt"));
                    //foreach (String item in statuslst)
                    //{
                    //    sw.WriteLine(item);
                    //}
                    //sw.Close();
                }
            }

        }
        return new { statuslst = statuslst, laylist = laylist };

    }
}