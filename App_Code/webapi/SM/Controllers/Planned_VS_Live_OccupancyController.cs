﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;
/// <summary>
/// Summary description for Planned_VS_Live_Occupancy
/// </summary>
public class Planned_VS_Live_OccupancyController:ApiController
{

    Planned_VS_Live_OccupancyService ps = new Planned_VS_Live_OccupancyService();
    [HttpPost]
    public HttpResponseMessage GetData(List<LCMlst> lcm)
    {
        var obj = ps.GetData(lcm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> GetReport(List<LCMlst> lcm)
    {
        ReportGenerator<LCMlst> GenAllocReport = new ReportGenerator<LCMlst>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Space_Mgmt/Planned_VS_Live_Occupancy.rdlc"),
            DataSetName = "UTILIZATION_COUNT",
        
        };
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/Planned_VS_Live_Occupancy." + "xlsx");
        DataTable reportdata = ps.GetData(lcm);
        await GenAllocReport.GenerateReport(reportdata, filePath, "xlsx");
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "Planned_VS_Live_Occupancy." + "xlsx";
        return result;
    }
}