﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class L1ApprovalController : ApiController
{
    L1ApprovalService L1Approval = new L1ApprovalService();

    [HttpGet]
    public HttpResponseMessage GetPendingSpaceRequisitions()
    {
        var spclist = L1Approval.GetPendingSpaceRequisitions();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(SpaceRequistion selectedid)
    {
        var spclist = L1Approval.GetDetailsOnSelection(selectedid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ApproveRequisitions([FromBody]SpaceReqDetails spcreqdet)
    {
        var obj = L1Approval.ApproveRequisitions(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ApproveAllReq([FromBody]SpaceReqDetails spcreqdet)
    {
        var obj = L1Approval.ApproveAllReq(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
