﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

public class ValidateTicketController : ApiController
{
    ValidateTicketServices VTsrvc = new ValidateTicketServices();

    // GET Locations
    [GzipCompression]
    [HttpGet]
    public Object getLocations()
    {
        return VTsrvc.getLocations();
    }

    // GET Startus
    [GzipCompression]
    [HttpGet]
    public Object getStartus()
    {
        return VTsrvc.getStartus();
    }

    // GET Ticket Type
    [GzipCompression]
    [HttpGet]
    public Object getTicketType()
    {
        return VTsrvc.getTicketType();
    }

    // GET Equipment
    [GzipCompression]
    [HttpGet]
    public Object getEquipment([FromUri] string BLDG)
    {
        return VTsrvc.getEquipment(BLDG);
    }

    // GET Error Description
    [GzipCompression]
    [HttpPost]
    public Object getErrorDescription(ErrorDescription svm)
    {
        return VTsrvc.getErrorDescription(svm);
    }

    // GET Assign to
    [GzipCompression]
    [HttpPost]
    public Object getAssignto(getAssignto svm)
    {
        return VTsrvc.getAssignto(svm);
    }

    // GET Spare Parts
    [GzipCompression]
    [HttpPost]
    public Object getSpareParts(getAssignto svm)
    {
        return VTsrvc.getSpareParts(svm);
    }

    // GET Request Ids
    [GzipCompression]
    [HttpPost]
    public Object getBreakDownRqsts(getAssignto svm)
    {
        return VTsrvc.getBreakDownRqsts(svm);
    }

    // GET Urgency
    [GzipCompression]
    [HttpGet]
    public Object getUrgency()
    {
        return VTsrvc.getUrgency();
    }

    // GET Submit
    [GzipCompression]
    [HttpPost]
    public object submit(breakdownSubmit sad)
    {

        HttpContext.Current.Session["FileId"] = sad.NEWDATE;
        //UploadFiles(sad.REQID);
        return VTsrvc.submit(sad);
    }

    //Upload Files
    [GzipCompression]
    [HttpPost]
    public object UploadFiles()
    {
        int iUploadedCnt = 0;
        // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
        string fileId = HttpContext.Current.Session["FileId"].ToString();
        string sPath = HttpContext.Current.Session["TENANT"].ToString();
        //fileId = fileId.Replace("/", "_");
        //bool exists = System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/" + fileId + "/"));
        //if (!exists)
        //    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/" + fileId + "/"));
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/");
        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
        //System.IO.DirectoryInfo di = new DirectoryInfo(sPath);        
        // CHECK THE FILE COUNT.
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                //foreach (FileInfo file in di.GetFiles())
                //{
                //    if(file.Name== hpf.FileName)
                //    {
                //        file.Delete();
                //    }
                //}
                string P = fileId + hpf.FileName;
                //(DateTime.Now.ToString("ddMMyyyy").ToString())
                hpf.SaveAs(sPath + Path.GetFileName(P));
                iUploadedCnt = iUploadedCnt + 1;
            }
        }
        // RETURN A MESSAGE.
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }

    // GET Locations
    [GzipCompression]
    [HttpGet]
    public Object getProblemOwner()
    {
        return VTsrvc.getProblemOwner();
    }
}
