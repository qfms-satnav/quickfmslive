﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class EmployeeMappingController : ApiController
{
    EmployeeMappingService EmpMapSer = new EmployeeMappingService();

    [HttpGet]
    public HttpResponseMessage GetPendingEmpMapList()
    {
        var spclist = EmpMapSer.GetPendingEmpMapList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(SpaceRequistion selectedid)
    {
        var spclist = EmpMapSer.GetDetailsOnSelection(selectedid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveSelectedSpaces([FromBody]SpaceReqDetails spcreqdet)
    {
        var obj = EmpMapSer.SaveSelectedSpaces(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetRepeatColumns(EmployeeMappingVM spcreqdet)
    {
        var obj = EmpMapSer.GetRepeatColumns(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetRepeatColumns_FreeSeat(EmployeeMappingVM spcreqdet)
    {
        var obj = EmpMapSer.GetRepeatColumns_FreeSeat(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
