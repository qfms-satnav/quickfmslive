﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;

public class HotDeskRequisitionController : ApiController
{

     HotDeskRequisitionService  HDS = new  HotDeskRequisitionService();

    
    [HttpGet]
    public HttpResponseMessage GetEmployeeDetails()
    {
        var obj =  HDS.GetEmployeeDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetEmployeeDetails(string AUR_ID)
    {
        var obj = HDS.GetEmployeeDetails(AUR_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage EmpChange(string AUR_ID)
    {
        var obj = HDS.EmpChange(AUR_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public object GetVacantSeats(MoveDetails MD)
    {
        return HDS.GetVacantSeats(MD);
    }
    [HttpPost]
    public object GetSelectedfloors(MoveDetails MD)
    {
        return HDS.GetSelectedfloors(MD);
    }
    [HttpPost]
    public object RaiseRequst(HotDeskRequisitionVM HD)
    {
        return HDS.RaiseRequst(HD);
    }

    [HttpPost]
    public object RaiseRequstValidatin(HotDeskRequisitionVM HD)
    {
        return HDS.RaiseRequstValidatin(HD);
    }

    [HttpPost]
    public HttpResponseMessage GetVerCosByEmpName(string AUR_ID)
    {
        var obj = HDS.GetVerCosByEmpName(AUR_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

}
