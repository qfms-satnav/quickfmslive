﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

public class UtilizationCountController : ApiController
{
    UtilizationCountService Csvc = new UtilizationCountService();
    [HttpPost]
    public HttpResponseMessage GetUtilizedDetails(UtlzationCountDetails utilized)
    {
        var obj = Csvc.GetGriddata(utilized);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(UtlzationCountDetails utilized)
    {
        try
        {
            ReportGenerator<UtilizationGridDetails> reportgen = new ReportGenerator<UtilizationGridDetails>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Space_Mgmt/UtilizationCountReport.rdlc"),
                DataSetName = "UtilizationCountReport",
                ReportType = "ReportType",

            };
            Csvc = new UtilizationCountService();
            List<UtilizationGridDetails> reportdata = Csvc.GetGriddataexcel(utilized);
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/UtilizationCountReport." + utilized.Type);
            await reportgen.GenerateReport(reportdata, filePath, utilized.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "UtilizationCountReport." + utilized.Type;
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
