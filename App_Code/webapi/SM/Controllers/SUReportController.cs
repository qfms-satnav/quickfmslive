﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class SUReportController : ApiController
{
    SUReportService SU = new SUReportService();


    [HttpPost]
    public HttpResponseMessage BindGrid()
    {
        var obj = SU.BindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> SUDataReport([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SUReportModel> reportgen = new ReportGenerator<SUReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SUReport.rdlc"),
            DataSetName = "SpaceUtil",
            ReportType = "Space Utilization Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SUReport." + rptCost.DocType);
        List<SUReportModel> reportdata = SU.GetSUDataList();
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "Space Utilization." + rptCost.DocType;
        return result;

    }

    [HttpPost]
    public HttpResponseMessage GetSpaceTypes()
    {
        IEnumerable<SpaceTypesModel> SpaceTypeList = SU.GetSpaceTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpaceTypeList);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SpaceSUChart(SUparams par)
    {
        var obj = SU.SUChartDetails(par);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
