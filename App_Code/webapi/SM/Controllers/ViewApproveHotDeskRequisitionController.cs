﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// Summary description for ViewApproveHotDeskRequisitionController
/// </summary>
public class ViewApproveHotDeskRequisitionController : ApiController
{

    ViewApproveHotDeskRequisitionService VS = new ViewApproveHotDeskRequisitionService();
    [HttpGet]
    public HttpResponseMessage GetMyReqList(int id)
    {
        var Verlist = VS.GetMyReqList(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Verlist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(string Req_Id)
    {
        var Verlist = VS.GetDetailsOnSelection(Req_Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Verlist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ApproveRequests([FromBody] HotDeskRequisitionVM hddet)
    {
        var obj = VS.ApproveRequests(hddet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage UpdateHdmFloor([FromBody] HotDeskRequisitionVM hddet)
    {
        var obj = VS.UpdateHdmFloor(hddet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}