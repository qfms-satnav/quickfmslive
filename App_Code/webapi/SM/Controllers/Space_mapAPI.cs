﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Text;
using System.Reflection;

/// <summary>
/// Summary description for Space_map
/// </summary>
public class Space_mapAPIController : ApiController
{
   
    Space_MapService service = new Space_MapService();


    public string Gettenantid()
    {
        return HttpContext.Current.Session["TENANT"].ToString();
    }


    [HttpPost]
    public DataTable Getfloordetails([FromBody] Space_mapVM param)
    {
        return service.Getfloordetails(param);
    }

    public DataTable GetallFloorsbyuser()
    {
        return service.GetallFloorsbyuser();
    }

    public DataTable GetallFilterbyItem()
    {
        return service.GetallFilterbyItem();
    }
    [HttpPost]
    public DataTable GetallFilterbySubItem( [FromBody] Space_mapVM param)
    {
        return service.GetallFilterbySubItem(param);
    }

    [HttpPost]
    public DataTable GetallLocationbyURL([FromBody] Space_mapVM param)
    {
        return service.GetallLocationbyURL(param);
    }

    public DataTable GetQueryAnalycat()
    {
        return service.GetQueryAnalycat();
    }

    [HttpPost]
    public DataTable GetQueryAnalyValuebyCat([FromBody] Space_mapVM param)
    {
        return service.GetQueryAnalyValuebyCat(param);
    }

    [HttpPost]
    public DataTable GetspcidbyEmp([FromBody] Space_mapVM param)
    {
        return service.GetspcidbyEmp(param);
    }

    [HttpPost]
    public DataTable GetFloorIDBBox([FromUri] string lcm_code, [FromUri] string twr_code, [FromUri] string flr_code)
    {
        return service.GetFloorIDBBox(lcm_code,twr_code,flr_code);
    }

    [HttpPost]
    public DataTable GetFloorIDBBoxByID([FromUri] string lcm_code, [FromUri] string twr_code, [FromUri] string flr_code)
    {
        return service.GetFloorIDBBoxByID(lcm_code, twr_code, flr_code);
    }

    [HttpPost]
    public object GetMapdetailsbyFilters([FromBody] Space_mapVM param)
    {
        return service.GetMapdetailsbyFilters(param);
    }

    [HttpPost]
    public DataTable GetMapdetails([FromBody] Space_mapVM param)
    {
        return service.GetMapdetails(param);
    }

    [HttpPost]
    public DataTable GetSpaceTypedetails([FromBody] Space_mapVM param)
    {
        return service.GetSpaceTypedetails(param);
    }

    [HttpPost]
    public DataTable GetVerticalAllocation([FromBody] Space_mapVM param)
    {
        return service.GetVerticalAllocation(param);
    }

    [HttpPost]
    public DataTable GetConsolidatedreport([FromBody] Space_mapVM param)
    {
        return service.GetConsolidatedreport(param);
    }

    [HttpPost]
    public DataTable GetMapbyQueryAnalyser([FromBody] Space_mapVM param)
    {
        return service.GetMapbyQueryAnalyser(param);
    }


}