﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class userUtilizationController : ApiController
{
    userUtilizationService UUS = new userUtilizationService();
    [HttpGet]
    public HttpResponseMessage GetShifts()
    {
        var obj = UUS.GetShifts();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGriddata(userUtlzationDetails userUtlzationDet)
    {
        var obj = UUS.GetGriddata(userUtlzationDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage getUtilizationHoursGrid(userUtlzationDetails userUtlzationDet)
    {
        var obj = UUS.getUtilizationHoursGrid(userUtlzationDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetExcelReportrdlc([FromBody]userUtlzationDetails data)
    {
        try
        {
            ReportGenerator<userUtlzationGridDetails> reportgen = new ReportGenerator<userUtlzationGridDetails>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Space_Mgmt/userUtilizationServiceReport.rdlc"),
                DataSetName = "UserUtilizationReport",
                ReportType = "User Utilization Report"
            };

            List<userUtlzationGridDetails> reportdata = UUS.GetGriddata(data);
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/UserUtilizationReport." + data.Type);
            await reportgen.GenerateReport(reportdata, filePath, data.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "UserUtilizationReport." + data.Type;
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetUtilizationHoursExcelReportrdlc([FromBody]User_UitlizationCount_Report data)
    {
        try
        {
            ReportGenerator<User_UitlizationCount_Report> reportgen = new ReportGenerator<User_UitlizationCount_Report>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Space_Mgmt/USER_UTILIZATION_HOUR_COUNT.rdlc"),
                DataSetName = "Hour_Count",
                ReportType = "UserUtilizationCountReport"
            };

            List<User_UitlizationCount_Report> reportdata = UUS.GetUtilizationHoursExcelReportrdlc(data);
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/UserUtilizationCountReport." + data.Type);
            await reportgen.GenerateReport(reportdata, filePath, data.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "UserUtilizationCountReport." + data.Type;
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }

}