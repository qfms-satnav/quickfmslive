﻿using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UtiltiyVM;


/// <summary>
/// Summary description for MoveRequestController
/// </summary>
public class MoveRequestController:ApiController
{
    MoveRequestService ms = new MoveRequestService();

    [HttpPost]
    public object GetSpaces(MoveDetails MD)
    {
        return ms.GetSpaces(MD);
    }
    [HttpPost]
    public object GetEmployeeDetails(string SPC_ID)
    {
        return ms.GetEmployeeDetails(SPC_ID);
    }
    [HttpPost]
    public object RaiseRequst(List<RequestDetails> details)
    {
        return ms.RaiseRequst(details);
    }
    [HttpGet]
    public object GetRequests()
    {
        return ms.GetRequests();
    }

}