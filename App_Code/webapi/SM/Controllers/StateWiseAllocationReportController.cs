﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;


/// <summary>
/// Summary description for StateWiseAllocationReportController
/// </summary>
public class StateWiseAllocationReportController : ApiController
{
    StateWiseAllocationReportService Csvc = new StateWiseAllocationReportService();
    CustomizedReportViews report = new CustomizedReportViews();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(CustomizedDetailss Custdata)
    {
        var obj = Csvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedDatadwn([FromBody]CustomizedDetailss data)
    {
        ReportGenerator<CustomizedDatas> reportgen = new ReportGenerator<CustomizedDatas>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/CustomizedReport.rdlc"),
            DataSetName = "CustomizedReport",
            ReportType = "Customized Report",
            Vertical = data.VERTICAL,
            Costcenter = data.COSTCENTER
        };

        Csvc = new StateWiseAllocationReportService();
        DataTable reportdata = Csvc.GetCustomizedDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CustomizedReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CustomizedReport." + data.Type;
        return result;
    }

    [HttpPost]
    public HttpResponseMessage SearchAllData(CustomizedDetailss Custdata)
    {
        var obj = Csvc.SearchAllData(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetStatesBycountry(int id)
    {
        var obj = Csvc.GetStatesBycountry(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getCitiesByState([FromUri] int id, [FromBody] List<Statelst> state)
    {
        var obj = Csvc.getCitiesByState(state, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CityByState(int id)
    {
        var obj = Csvc.CityByState(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetVerticalByFloor(List<FLOORLSTS> floor, int id)
    {
        var obj = Csvc.GetVerticalByFloor(floor, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCostcenterByMancom(List<Verticallsts> verticallsts, int id)
    {
        var obj = Csvc.GetCostcenterByMancom(verticallsts, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCostCenterByVertical(int id)
    {
        var obj = Csvc.GetCostCenterByVertical(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    SpaceConsolidatedReportService SPC_CON_SER = new SpaceConsolidatedReportService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage BindGrid(SpaceConsolidatedParameters Params)
    {
        var obj = Csvc.BindGrid(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetProjectSummary(SpaceConsolidatedParameters Params)
    {
        var obj = Csvc.GetProjectSummary(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMancom(SpaceConsolidatedParameters Params)
    {
        var obj = Csvc.GetMancom(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage BindGridSummary(SpaceConsolidatedParameters Params)
    {
        var obj = Csvc.BindGridSummary(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage SpaceConsolidatedChart()
    {
        var obj = SPC_CON_SER.SpaceConsolidatedChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportVM> reportgen = new ReportGenerator<SpaceConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedReport.rdlc"),
            DataSetName = "SpaceConsolidatedReport",
            ReportType = "Space Consolidated Report",
            //Vertical=rptCost.VERTICAL,
            //Costcenter=rptCost.COSTCENTER
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedReport." + rptCost.DocType);

        List<SpaceConsolidatedReportVM> reportdata = SPC_CON_SER.GetReportList(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedReport." + rptCost.DocType;
        return result;

    }

    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedLocationRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportVM> reportgen = new ReportGenerator<SpaceConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedLocationReport.rdlc"),
            DataSetName = "SpaceConsolidatedLocationReport",
            ReportType = "Space Allocation report",
            //Vertical=rptCost.VERTICAL,
            //Costcenter=rptCost.COSTCENTER
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedLocationReport." + rptCost.DocType);

        List<SpaceConsolidatedReportVM> reportdata = SPC_CON_SER.GetReportList_location(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedLocationReport." + rptCost.DocType;
        return result;
    }

    [GzipCompression]
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedPrjRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportVM> reportgen = new ReportGenerator<SpaceConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedProjectReport.rdlc"),
            DataSetName = "DataSet1",
            ReportType = "Project wise occupied seat count",
            //Costcenter=rptCost.COSTCENTER
            //Vertical=rptCost.VERTICAL,
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedProjectReport." + rptCost.DocType);

        List<SpaceConsolidatedReportVM> reportdata = SPC_CON_SER.GetExportLst(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedProjectReport." + rptCost.DocType;
        return result;

    }

    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedMancomRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportMancom> reportgen = new ReportGenerator<SpaceConsolidatedReportMancom>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedMancomReport.rdlc"),
            DataSetName = "SpaceConsolidatedMancomReport",
            ReportType = "Space Allocation Mancom Wise Report",
            //Vertical=rptCost.VERTICAL,
            //Costcenter=rptCost.COSTCENTER
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedMancomReport." + rptCost.DocType);

        List<SpaceConsolidatedReportMancom> reportdata = SPC_CON_SER.GetMancom(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedMancomReport." + rptCost.DocType;
        return result;
    }

}