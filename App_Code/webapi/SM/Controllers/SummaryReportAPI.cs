﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;


public class SummaryReportAPIController : ApiController
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    SummaryReportService srvc = new SummaryReportService();
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    public Object GetlblVerticalCC()
    {
        return srvc.GetlblVerticalCC();
    }

    public Object GetCityDetails()
    {
        return srvc.GetCityDetails();
    }

    [HttpPost]
    public Object Getalllocations([FromBody] SummaryReportVM City)
    {
        return srvc.Getalllocations(City);
    }
    [HttpPost]
    public Object Getalltowers([FromBody] SummaryReportVM Citytower)
    {
        return srvc.Getalltowers(Citytower);
    }
    [HttpPost]
    public Object Getallfloors([FromBody] SummaryReportVM Cityfloor)
    {
        return srvc.Getallfloors(Cityfloor);
    }

    public Object Getverticals()
    {
        return srvc.Getverticals();
    }
    [HttpPost]
    public Object Getconsolidatedseatdetails([FromBody] SummaryReportVM Consolidateddetails)
    {
        return srvc.Getconsolidatedseatdetails(Consolidateddetails);
    }

    [HttpPost]
    public Object GetSummaryDetails([FromBody] SummaryReportVM ObjGridSummary)
    {
        return srvc.GetSummaryDetails(ObjGridSummary);
    }

    [HttpPost]
    public Object GetVerticalDetails([FromBody] SummaryReportVM ObjGridVertical)
    {
        return srvc.GetVerticalDetails(ObjGridVertical);
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }

}
