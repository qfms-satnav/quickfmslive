﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;

public class DuplicateRecordsReportController : ApiController
{
    DuplicateRecordsReportService drrs = new DuplicateRecordsReportService();

    [HttpPost]
    public HttpResponseMessage GetData(DuplicateRecordVM Dataobj)
    {
        var obj = drrs.GetData(Dataobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetDuplicateReportdata([FromBody]DuplicateRecordVM DUObj)
    {
        ReportGenerator<DuplicateGrid> reportgen = new ReportGenerator<DuplicateGrid>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Space_Mgmt/DuplicateRecordsReport.rdlc"),
            DataSetName = "DupSpaceDS",
            Vertical = DUObj.VERTICAL,
            Costcenter = DUObj.COSTCENTER
            //ReportType = "Personal VS Offical Booking Report"
        };

        List<DuplicateGrid> reportdata = drrs.GetData(DUObj);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Space_Mgmt/DuplicateRecordsReport.rdlc." + DUObj.Type);
        await reportgen.GenerateReport(reportdata, filePath, DUObj.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "DuplicateReport." + DUObj.Type;
        return result;
    }
}
