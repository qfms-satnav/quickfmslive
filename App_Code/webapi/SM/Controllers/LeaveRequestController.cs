﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class LeaveRequestController : ApiController
{
    LeaveRequestService service = new LeaveRequestService();

    [HttpPost]
    public HttpResponseMessage SaveLeaveDetails(LeaveRequestModel Model)
    {
        var obj = service.SMS_Leave_Details(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage DeleteData(LeaveRequestModel Model)
    {
        var obj = service.DeleteData(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGrid()
    {
        var obj = service.LeaveGridDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
