﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.IO;
using UtiltiyVM;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Globalization;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class ManagerApprovalController : ApiController
{
    ManagerApprovalServices MPsrvc = new ManagerApprovalServices();

    [GzipCompression]
    [HttpPut]
    public object Getapprovaldata()
    {
        return MPsrvc.Getapprovaldata();
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage updatedata([FromUri] int id)
    {
        var obj = MPsrvc.updatedata(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage RejectFn([FromUri] int id)
    {
        var obj = MPsrvc.RejectFn(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
