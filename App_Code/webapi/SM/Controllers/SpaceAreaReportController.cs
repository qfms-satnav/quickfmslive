﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class SpaceAreaReportController : ApiController
{

    SpaceAreaReportService areaService = new SpaceAreaReportService();

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetAreaReportBindGrid(CompanySpace CNPDet)
    {
        var obj = areaService.GetReportByUserObject(CNPDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Chart Data
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetAreaChartData(String ID)
    {
        var obj = areaService.GetAreaChart(ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Export Report Format
    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetAreaReportdata(CompanySpace CNPDet)
    {

        ReportGenerator<AreaReport> reportgen = new ReportGenerator<AreaReport>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/AreaReportSpace.rdlc"),
            DataSetName = "AreaReportSPC",
            ReportType = "Area Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/AreaReportSpace." + CNPDet.Type);
        List<AreaReport> reportdata = areaService.GetAreaReportByUserList(CNPDet);
        await reportgen.GenerateReport(reportdata, filePath, CNPDet.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AreaReportSpace." + CNPDet.Type;
        return result;

    }

}
