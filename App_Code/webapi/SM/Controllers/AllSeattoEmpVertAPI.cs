﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.VisualBasic;

using System.Text;
using Newtonsoft.Json;
using System.Reflection;
using System.Data.SqlClient;

public class AllSeattoEmpVertAPIController : ApiController
{
    // GET api/<controller>
    SubSonic.StoredProcedure sp;
    DataSet ds;
    string REQID;
    AllSeattoEmpVertService service = new AllSeattoEmpVertService();

    //label for vertical and costcenter
    public string[] GetlblVerticalCC()
    {
        return service.GetlblVerticalCC();       
    }

    //passing url and taking loc code from URL and using the same to fill shift ddl
    [HttpPost]
    public DataTable GetURL([FromBody] Param parameter)
    {
        return service.GetURL(parameter);
    }

    //passing url Querystring and to check spacetype 1 or not
    [HttpPost]
    public DataSet GetSpaceType([FromBody] string parameter)
    {
        return service.GetSpaceType(parameter);
    }
       
    //
    [HttpPost]
    public DataTable GetSeattoAllocateOccupied([FromBody] AllSeatToEmpVertModel.GetSeattoAllOcc gstao)
    {
       return service.GetSeattoAllocateOccupied(gstao);
    }

    //fill vertical ddl
    [HttpGet]
    public DataTable GetVerticals()
    {
        return service.GetVerticals();
    }

    [HttpPost]
    public DataTable GetEmpDetails([FromBody] Employee emp)
    {
        return service.GetEmpDetails(emp);
    }

    //fill gvassests table
    [HttpPost]
    public DataTable GetgvAssets([FromBody] string id)
    {
        return service.GetgvAssets(id);
    }
          

    [HttpPost]
    public string SubmitAllocationOccupied([FromBody]string Strfield)
    {
        return service.SubmitAllocationOccupied(Strfield);
    }


    //BLOCK_SEATS_REQUEST_PART1_updated


    [HttpPost]
    public int BlockSeatsRequestPart1_Updated([FromBody] AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
       return service.BlockSeatsRequestPart1_Updated(obj);
    }

    [HttpPost]
    public void VacantEmpToAllocateNewSeat([FromBody] AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        service.VacantEmpToAllocateNewSeat(obj);
    }

    [HttpPost]
    public void SharedEmployeeSpaceRelease([FromBody] AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        service.SharedEmployeeSpaceRelease(obj);
    }

    [HttpPost]
    public void AllocatedSeatsRelease([FromBody] AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        service.AllocatedSeatsRelease(obj);
    }

    //auto complete
    [HttpPost]
    public List<string> SearchCustomers([FromBody] AllSeatToEmpVertModel.BlockSeatsReqPOU obj)
    {
        return service.SearchCustomers(obj);
    }

    // bind Shift timings
    [HttpPost]
    public DataTable GetShiftTimings([FromBody] AllSeatToEmpVertModel.GetShifttimes seatalloc)
    {
        return service.GetShiftTimings(seatalloc);
    }
}



public class Param
{
    public string URL { get; set; }
}

public class Employee
{
    public string EmployeeID { get; set; }
}


