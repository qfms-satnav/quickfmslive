﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class AreaWiseCostController : ApiController
{

    AreaWiseCostService shs = new AreaWiseCostService();

    [HttpPost]
    public HttpResponseMessage GetAreaWiseCostDetails(SUbyFilters ASF)
    {
        var obj = shs.GetAreaWiseCostDetails(ASF);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(AreaWiseCostDetails Areadata)
    {
        var obj = shs.GetSpaceAreaDetails(Areadata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public async Task<HttpResponseMessage> GetAreawisecostReport(AreaWiseCostDetails Areadata)
    {
        AreaWiseCostDetails Avail = new AreaWiseCostDetails();
        ReportGenerator<AreaWiseCostDetails> GenAllocReport = new ReportGenerator<AreaWiseCostDetails>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/AreawisecostpopupReport.rdlc"),
            DataSetName = "AreawisecostpopupDS",
            ReportType = "Area Wise Seat Cost Report"

        };
        shs = new AreaWiseCostService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/AreawisecostpopupReport.xls");
        List<AreaWiseCostDetails> reportdata = shs.GetSpaceAreaDetails(Areadata);
        await GenAllocReport.GenerateReport(reportdata, filePath, "xls");
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AreawisecostpopupReport.xls";
        return result;
    }


    [HttpPost]
    public HttpResponseMessage GetAreaDetailsRPT(SUbyFilters ASF)
    {
        DataSet ds = new DataSet();
        ds = shs.GetAreaWiseCostdetailsLST(ASF);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/AreaWiseCostReport.xls");
        CreateExcelFile.CreateExcelDocument(ds, filePath);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AreaWiseCostReport.xls";
        return result;
    }

}
