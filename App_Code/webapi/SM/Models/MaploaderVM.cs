﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class MaploaderVM
    {
        public int ID { get; set; }
        public string SPACE_ID { get; set; }
        public string Wkt { get; set; }
        public string flr_id { get; set; }
        public string layer { get; set; }
        public string x { get; set; }
        public string y { get; set; }
    }
