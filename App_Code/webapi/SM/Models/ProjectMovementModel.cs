﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProjectMovementModel
/// </summary>
public class ProjectMovementModel
{
	public ProjectMovementModel()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public class GetcityID
    {
        public string CityId { get; set; }
        public string CitydesId { get; set; }
    }
    public class GetlocID
    {
        public string city { get; set; }
        public string location { get; set; }
        public string descity { get; set; }
        public string deslocation { get; set; }
    }

    public class Getvertical
    {
        public string getver { get; set; }
    }

    public class Getseatdetails
    {
        public string getempcity { get; set; }
        public string getemploc { get; set; }
        public string getempver { get; set; }
        public string getempcost { get; set; }
        public string getdescity { get; set; }
        public string getdesloc { get; set; }
        public string gettype { get; set; }
        public string getdestcity { get; set; }
        public string getdestloc { get; set; }
    }
}