﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BreakDownMaintenanceModel
/// </summary>
public class ErrorDescription
{
    public string TICKETTYPE { get; set; }
    public string CATEGORY { get; set; }
    public string LOC { get; set; }
}
public class getAssignto
{
    public string LOC { get; set; }
    public string TICKET_TYPE { get; set; }
    public string EQUIPMENT { get; set; }
    public string ERRORDESCRIPTION { get; set; }
}
public class RCARpt
{
    public string REQID { get; set; }
}
public class SpareRpt
{
    public string REQID { get; set; }
}
public class DownTimeRpt
{
    public string REQID { get; set; }
    public string CompanyID { get; set; }
}
public class breaktime
{
    public int SER_DOWN_ID { get; set; }
    public string CompanyId { get; set; }
}

public class breakRCA
{
    public int SR_NO { get; set; }
}

public class breakSpare
{
    public int BDMPD_SNO { get; set; }
}

public class breakdownSubmit
{
    public List<SPAREPARTDETAILS> spd { get; set; }
    public string REQID { get; set; }
    public string STATUS { get; set; }
    public string INCHARE_ID { get; set; }
    public string ROOT_CAUSE { get; set; }
    public string PREVENTIVE_ACTION { get; set; }
    public string CORRECTIVE_ACTION { get; set; }
    public string PRODUCTION_IMPCT { get; set; }
    public string PROBLEM_OWNER { get; set; }
    public string REMARKS { get; set; }
    public string ATTACHMENT { get; set; }
    public string USER { get; set; }
    public Nullable<DateTime> TODATE { get; set; }
    public Nullable<DateTime> FROMDATE { get; set; }
    public string BDMP_DWNTIME_FACTOR { get; set; }
    public string BDMP_DOWNTIME { get; set; }
    public string BDMP_BRACHED { get; set; }
    public string NEWDATE { get; set; }
    public string EQUIPMENT_CODE { get; set; }
    public string ERROR_DESCRIPTION_CODE { get; set; }
    public string BDMP_OTHERPROB_OWNER { get; set; }
    public string OTHER_ERROR_DESCRIPTION { get; set; }

    public int BDMP_SHIFT { get; set; }
    public string LOC_CODE { get; set; }
    public string SER_PREV_ACT { get; set; }
    public string REQREMARKS { get; set; }
    public float ADDI_BREACH { get; set; }
    public string PROBLEMCAT { get; set; }


}

public class breakdowntime
{
    public string SER_REQ_ID { get; set; }
    public Nullable<DateTime> TODATE { get; set; }
    public Nullable<DateTime> FROMDATE { get; set; }
    public string BDMP_DWNTIME_FACTOR { get; set; }
    public string BDMP_DOWNTIME { get; set; }
}
public class breakdownUpdate
{    
    public string LOC_CODE { get; set; }
    public string SUB_CAT { get; set; }
    public string ERROR_DESC { get; set; }
    public string AAS_VED { get; set; }
    public string SUB_EQUIP { get; set; }

    public string OTHER_DESC { get; set; }

    public string IMPACT { get; set; }


}


public class breakdownRCA
{
    public string RCA_DESCRIPTION { get; set; }
    public string RCA_STATUS { get; set; }
    public string RCA_BDMP_ID { get; set; }   

    public string RCAPATH { get; set; }
    public string NEWDATE { get; set; }

}

public class SPAREPARTDETAILS
{
    public string SPAREPARTCODE { get; set; }
    public string CATEGORY { get; set; }
    public string SUBCATCODE { get; set; }
    public string BRNDCODE { get; set; }
    public string MODELCODE { get; set; }
    public string UNITCOST { get; set; }
    public string QTY { get; set; }
}

public class IMAGEDETAILS
{
    public string NAME { get; set; }
}