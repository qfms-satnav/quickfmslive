﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceProjectionVM
/// </summary>
public class SpaceProjectionVM
{
    public SMS_SPACE_PROJECTION ssp { get; set; }
    public List<SP_Main> SP_Main { get; set; }

}

public class SP_Main
{
    public City Key { get; set; }
    public List<SP_Sub> Value { get; set; }
}

public class SP_Sub
{
    public MonthNames Key { get; set; }
    public SPACE_PROJECTION_DETAILS_VM Value { get; set; }
}


public class SPACE_PROJECTION_DETAILS_VM
{
    public string SSPD_SSP_REQ_ID { get; set; }
    public int SSPD_MONTH { get; set; }
    public string SSPD_PRJ_SPC_TYPE { get; set; }
    public string SSPD_PROJECTION_TYPE { get; set; }
    public string SSPD_STA_ID { get; set; }
    public int SSPD_PRJ_VALUE { get; set; }
    public string SSPD_MONTH_NAME { get; set; }
    public string SSPD_CTY_CODE { get; set; }
    public string SSPD_CTY_NAME { get; set; }
}

public class SMS_SPACE_PROJECTION
{
    public string SSP_CNY_CODE { get; set; }
    public string SSP_VER_CODE { get; set; }
    public string SSP_YEAR { get; set; }
    public string SSP_REQ_BY { get; set; }
    public string SSP_STA_ID { get; set; }
    public string SSP_PROJECTION_TYPE { get; set; }
    public string SSP_REQ_ID { get; set; }
    public string SSP_REQ_REM { get; set; }
    public int SSP_ID { get; set; }
}


public enum EntityState
{
    Detached = 1,
    Unchanged = 2,
    Added = 4,
    Deleted = 8,
    Modified = 16,
}

public class OfficeSpacePlanning
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Facility { get; set; }
    public string BU { get; set; }
    public string LOB { get; set; }
    public string Grade { get; set; }
    public string Q1 { get; set; }
    public string Q2 { get; set; }
    public string Q3 { get; set; }
    public string Q4 { get; set; }
    public string FY1 { get; set; }
    public string FY2 { get; set; }
    public string FY3 { get; set; }
    public string AVAILABLE_SEATS { get; set; }
    public string AVAILABLE_WS { get; set; }
    public string AVAILABLE_CB { get; set; }
    public string REQ_WS { get; set; }
    public string REQ_CB { get; set; }
    public string TOTAL_SEATS_REQUIRED { get; set; }
}

public class OSPSaveList
{
    public List<OSPSaveObj> savelst { get; set; }
    public List<OfficeSpacePlanning> CompData { get; set; }
    public string REQID { get; set; }
}

public class OSPSaveObj
{
    public int ID { get; set; }
    public string REQWS { get; set; }
    public string REQCAB { get; set; }
    public string REQTWD { get; set; }
}

public class OfficeReq
{
    public string OSP_REQ_ID { get; set; }
    public string OSP_CREATED_DATE { get; set; }
    public string OSP_CREATED_BY { get; set; }
    public string OSP_STA_ID { get; set; }

}

