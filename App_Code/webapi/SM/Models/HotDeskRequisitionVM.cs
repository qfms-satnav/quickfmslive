﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for HotDeskRequisitionVM
/// </summary>
public class HotDeskRequisitionVM
{
    public List<HD_Dtails> HDL { get; set; }

    public List<HD_Dtails_new> HDLN { get; set; }
    public List<LCMlst> LCMlst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public List<Floorlst> flrlst { get; set; }
    public string REM { get; set; }
    public string L1_REM { get; set; }
    public string L2_REM { get; set; }
    public string REQ_ID { get; set; }
    public int ALLOCSTA { get; set; }
    public List<HD_REQ_ID> hdreqlst { get; set; }


}
public class HD_Dtails
{
    public string SPC_ID { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public bool ticked { get; set; }
    public string AUR_ID { get; set; }

    public string VER_NAME { get; set; }

    public string Cost_Center_Name { get; set; }
    public string Seat_Type { get; set; }


}

public class HD_Dtails_new
{
    public string SPC_ID { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }


}
public class HD_REQ_ID
{
    public string REQ_ID { get; set; }
}


