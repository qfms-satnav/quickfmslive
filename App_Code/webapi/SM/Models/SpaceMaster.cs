﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for Anusha
/// </summary>
public class SpaceMasterDetails
{
    public List<Floorlst> flrlst { get; set; }
    public  List<VerticalRequistions> upddetlst { get; set; }
    public List<VerticalRequistions> verreqList { get; set; }
    public List<VerticalReq_detailss> verreqdeta { get; set; }
    public string spclst { get; set; }
    public List<SpaceList> verreqdet { get; set; }
    public string spcsub { get; set; }

    public int ALLOCSTA { get; set; }
    public string SpaceType { get; set; }
    public int CHANGEVALUE { get; set; }
}
public class VerticalRequistions
{
    public string SVR_REQ_ID { get; set; }
    public string SVR_REQ_BY { get; set; }
 
    public string SVD_SPACE_TYPE_NAME { get; set; }
    public string SVD_SPC_ID { get; set; }

    public string SPC_ID { get; set; }
  
    public int SVR_STA_ID { get; set; }
    public string SVR_REQ_REM { get; set; }
    public string SVR_APPR_BY { get; set; }
    public string SVR_APPR_REM { get; set; }
    //public int STACHECK { get; set; }
    public int  MODE { get; set; }
    public int CHANGEVALUE { get; set; }
}


public class VerticalReq_detailss
{
  
    public string SVD_SPC_ID { get; set; }
    public string SVD_SPC_NAME { get; set; }
    public string SVD_SPC_TYPE { get; set; }
    public string SVD_SPC_TYPE_NAME { get; set; }
    public string SVD_SPC_SUB_TYPE { get; set; }
    public string SVD_SPC_SUB_TYPE_NAME { get; set; }

    public string SVD_SPACE_TYPE_NAME { get; set; }

    public string FLR_ID { get; set; }
    public string FLR_NAME { get; set; }

}

public class SeatTyp
{

    public string SNO { get; set; }
    public string SVD_SPACE_TYPE_NAME { get; set; }
    public int CHANGEVALUE { get; set; }
    public bool ticked { get; set; }

}


public class SpaceList
{
    //public string SPC_ID { get; set; }
    public string SVD_SPC_ID { get; set; }
    //public string SPACE_TYPE { get; set; }
    //public string SVD_SPC_TYPE { get; set; }
    public string SVD_SPACE_TYPE_NAME { get; set; }
    public string SVD_SPC_SUB_TYPE_NAME { get; set; }
    //public string SVD_SPC_SUB_TYPE { get; set; }
    //public string SVD_SPACE_FLOOR { get; set; }
    //public bool ticked { get; set; }
    //public int CHANGEVALUE { get; set; }
    //public string SST_ID { get; set; }
    public string  FLR_ID { get; set; }

}

public class SeatSubType
{
    //public string SVD_SPC_ID { get; set; }
    //public string SVD_SPC_TYPE { get; set; }
    public string SST_CODE { get; set; }
    public string SST_ID { get; set; }
    //public string SVD_SPC_SUB_TYPE { get; set; }
    public string SVD_SPC_SUB_TYPE_NAME { get; set; }
    public int CHANGEVALUE { get; set; }
    public bool ticked { get; set; }

    //public string FLR_ID { get; set; }

}




