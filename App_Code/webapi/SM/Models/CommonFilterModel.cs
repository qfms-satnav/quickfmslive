﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CommonFilterModel
/// </summary>
public class CommonFilterModel
{
	public List<String> Country{get;set;}
    public List<String> City { get; set; }
    public List<String> Location { get; set; }
    public List<String> Tower { get; set; }
    public List<String> Floor { get; set; }
    public List<String> Vertical { get; set; }
    public List<String> Department { get; set; }
    public DateTime FromTime { get; set; }
    public DateTime ToTime { get; set; }
}