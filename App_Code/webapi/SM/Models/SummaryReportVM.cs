﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SummaryReportVM
/// </summary>
public class SummaryReportVM
{
    public String CityId { get; set; }
    public String CITY { get; set; }
    public String LOCATION { get; set; }
    public String TOWER { get; set; }
    public String FLOOR { get; set; }
    public String VERTICAL { get; set; }

}