﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InfraAdministrationReportVM
/// </summary>
public class getGridBySearch
{
    public List<LCMlst> LCMLst { get; set; }
}

public class InfraAdminReportParameters
{
    public string DocType { get; set; }
    public string LOCATION { get; set; }
    public string CITY { get; set; }
    public string TOTAL_SEATS { get; set; }
    public string On_Roll { get; set; }
    public string Off_Roll { get; set; }
    public string CRE { get; set; }
    public string CRE_TOTAL { get; set; }
    public string TOTAL { get; set; }
    public string SPC_BDG_ID { get; set; }
    public string VERTICAL_NAME { get; set; }
    public string VERTICALCOUNT { get; set; }
    public string Availability { get; set; }
}