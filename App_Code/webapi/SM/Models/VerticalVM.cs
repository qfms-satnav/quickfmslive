﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VerticalVM
/// </summary>
public class VerticalVM
{
    public int VER_ID { get; set; }
    public string VER_CODE { get; set; }
    public string VER_NAME { get; set; }
}