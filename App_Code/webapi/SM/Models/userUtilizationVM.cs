﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class userUtlzationDetails
{
    public List<Locationslst> Locations { get; set; }
    public List<Towerlst> Towers { get; set; }
    public List<Floorlst> Floors { get; set; }
    public List<Shiftlst> ShiftFilter { get; set; }
    public string SRN_FROM_DATE { get; set; }
    public string SRN_TO_DATE { get; set; }
    public string Type { get; set; }
}

public class Locationslst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class userUtlzationGridDetails
{
    public string aur_id { get; set; }
    public string aur_host_name { get; set; }
    public string FLR_NAME { get; set; }
    public string SPC_ID { get; set; }
    public string LOG_IN { get; set; }
    public string LOG_OFF { get; set; }
    public string SHFT_NAME { get; set; }
    public string Utilztionhours { get; set; }
    public string VER_NAME { get; set; }
    public string CC_NAME { get; set; }
}
public class User_UitlizationCount_Report
{
    public string uli_usr { get; set; }
    public string spc_id { get; set; }
    public string ULI_LOG_IN_DATE { get; set; }
    public string Utilztionhours { get; set; }
    public string Cost_Center_Name { get; set; }
    public string SRN_FROM_DATE { get; set; }
    public string SRN_TO_DATE { get; set; }
    public string Type { get; set; }
}