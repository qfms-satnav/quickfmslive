﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AddHerenceModel
/// </summary>
public class UserBaseDataReportModel
{
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string spc_id { get; set; }
    public string VER_NAME { get; set; }
    public string Cost_Center_Name { get; set; }
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string Locations { get; set; }
    public string Verticals { get; set; }
    public string Towers { get; set; }
    public string Floors { get; set; }


}