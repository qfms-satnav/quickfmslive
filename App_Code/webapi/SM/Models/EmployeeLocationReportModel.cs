﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for EmployeeLocationReportModel
/// </summary>
public class EmployeeLocationDetails
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public List<Floorlst> flrlst { get; set; }

}
