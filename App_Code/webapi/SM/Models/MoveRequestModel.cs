﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for MoveRequestModel
/// </summary>

public class MoveDetails
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }

    public List<LCMlst> LCMlst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public List<Floorlst> flrlst { get; set; }
    public List<Verticallst> verlst { get; set; }
    public List<Costcenterlst> cstlst { get; set; }
    public List<Countrylst> Destcnylst { get; set; }
    public List<Citylst> Destctylst { get; set; }
    public List<Locationlst> Destloclst { get; set; }
    public List<Towerlst> Desttwrlst { get; set; }
    public List<Floorlst> Destflrlst { get; set; }
    public List<Verticallst> Destverlst { get; set; }
    public List<Costcenterlst> Destcstlst { get; set; }
    public int Type { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }

}
public class RequestDetails
{
    public string AUR_KNOWN_AS { get; set; }
    public string DEST_EMP_ID { get; set; }
    public string DEST_FLOOR_ID { get; set; }
    public string DEST_SPC_ID { get; set; }
    public string SPC_FLR_ID { get; set; }
    public string SPC_ID { get; set; }
    public int TYPE_ID { get; set; }
    public string VACANT_FLOOR { get; set; }
    public string VACANT_SPC_ID { get; set; }
}

