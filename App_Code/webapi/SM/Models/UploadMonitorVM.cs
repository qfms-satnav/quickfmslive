﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UploadMonitorVM
/// </summary>
public class UploadMonitorVM
{
    public List<Floorlst> flrlst { get; set; }
    public string ALLOCSTA { get; set; }
}

public class UploadMonitorDataVM
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SpaceID { get; set; }
    public string MonitorType { get; set; }

}

public class UploadMasterDataDumpVM
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SpaceID { get; set; }
    public string MonitorType { get; set; }
    
}

public class MNTRTYPEVM
{
    public string MntrAllocType { get; set; }
    public string MntrOptions { get; set; }
}


#region SeatType upload

public class UploadSeatTypeDataVM
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SpaceID { get; set; }
    public string SeatType { get; set; }

}




#endregion