﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceTypeCostModel
/// </summary>
public class SpaceTypeCostDet
{
    public int SATD_SAT_ID { get; set; }
    public string SATD_OPT1 { get; set; }
    public string SATD_OPR1 { get; set; }
    public string SATD_OPT2 { get; set; }
    public string SATD_OPR2 { get; set; }
    public string SATD_OPT3 { get; set; }
}

public class AREA_TYPES
{
    public int SAT_ID { get; set; }
    public string SAT_CODE { get; set; }
    public int SAT_PARENT { get; set; }
}

public class SpaceTypeCostVM
{
    public List<SpaceTypeCostDet> stcd { get; set; }
    public List<Locationlst> lcmlst { get; set; }
}

public class seattype
{

    public string Code { get; set; }
    public string Name { get; set; }
    public string Value { get; set; }

}
public class SPCDetails
{
    public string FLR_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public Decimal AW_COST { get; set; }
    public Decimal VAR_COST { get; set; }
    public List<seattype> lstseattype { get; set; }
    public List<seattype> variableCost { get; set; }
}


public class SPCCostDetails
{
    public string SC_COST_TYPE { get; set; }
    public string SC_FLR_CODE { get; set; }
    public string SC_BASED_ON { get; set; }
    public Decimal SC_COST_VALUE { get; set; }
    public Decimal SC_VAR_COST_VALUE { get; set; }

    public bool ticked { get; set; }
}
