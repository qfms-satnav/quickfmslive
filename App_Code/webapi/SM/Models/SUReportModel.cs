﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class SUReportModel
{
    public string FLOORNAME { get; set; }
    public string WORKSTATION { get; set; }
    public string WSODC { get; set; }
    public string WSUNRES { get; set; }
    public string CABIN { get; set; }
    public string CBODC { get; set; }
    public string CBUNRES { get; set; }
    public string WS_OCCUPIED { get; set; }
    public string CB_OCCUPIED { get; set; }
    public string CABIN_VACANT { get; set; }
    public string WS_VACANT_COUNT { get; set; }

    public string URES_OCCUPIED { get; set; }
    public string ODC_OCCUPIED { get; set; }
    public string CB_URES_OCCUPIED { get; set; }
    public string CB_ODC_OCCUPIED { get; set; }
    public string WS_URES_VACANT { get; set; }
    public string WS_ODC_VACANT { get; set; }
    public string CB_URES_VACANT { get; set; }
    public string CB_ODC_VACANT { get; set; }






    public string SU { get; set; }
}

public class SpaceTypesModel
{
    public string SPC_TYPE_CODE { get; set; }
    public string SPC_TYPE_NAME { get; set; }
}

public class SUparams { public string SpaceType { get; set; } }