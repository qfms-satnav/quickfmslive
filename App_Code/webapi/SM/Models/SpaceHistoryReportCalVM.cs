﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceHistoryReportCalVM
/// </summary>
public class SpaceHistoryReportCalVM
{
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string Locations { get; set; }
    public string Towers { get; set; }
    public string Floors { get; set; }

}