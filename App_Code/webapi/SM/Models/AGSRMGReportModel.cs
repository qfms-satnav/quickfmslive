﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AGSRMGReportModel
/// </summary>
public class AGSRMGReportModel
{
    public List<AGSRMGData> SeatData { get; set; }
    public List<Floorlst> flrlst { get; set; }
}

public class AGSRMGData
{
    public string COUNTRY { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string TOWER { get; set; }
    public string FLOOR { get; set; }
    public string SPACE { get; set; }
    public string SPC_TYPE_NAME { get; set; }
    public string SH_NAME { get; set; }
    public string STATUS { get; set; }

}