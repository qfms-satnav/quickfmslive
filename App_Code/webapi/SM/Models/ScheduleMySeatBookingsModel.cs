﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ScheduleMySeatBookingsModel
/// </summary>
public class ScheduleMySeatBookingsModel
{
    public string flrlst { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }

    public string AUR_ID { get; set; }
}