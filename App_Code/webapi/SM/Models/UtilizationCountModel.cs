﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
public class UtilizationGridDetails
{
    public string Country { get; set; }
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string Vertical { get; set; }
    public string Costcenter { get; set; }
    public string SeatType { get; set; }
    public string HeadCOUNT { get; set; }
    public string Seats { get; set; }
    public string Max { get; set; }
    public string Seat_TURN { get; set; }
    public string Utilization { get; set; }

}
public class UtlzationCountDetails
{
    public List<Locationslst> Locations { get; set; }
    public List<Floorlst> Floors { get; set; }
    public List<Towerlst> Towers { get; set; }    
    public List<Verticallst> Vertical { get; set; }
    public List<Costcenterlst> costcenter { get; set; }
    public string Type { get; set; }
}