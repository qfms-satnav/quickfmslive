﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceRequisitionVM
/// </summary>
public class SpaceReqDetails
{
    public SpaceReq SpaceRe { get; set; }
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public List<Floorlst> flrlst { get; set; }
    public List<Verticallst> verlst { get; set; }
    public List<Costcenterlst> cstlst { get; set; }
    public SpaceRequistion spcreq { get; set; }
    public List<SpaceRequistion> spcreqList { get; set; }
    public List<SpaceRequistion_details> spcreqdet { get; set; }
    public List<SpaceRequistionCount> spcreqcount { get; set; }
    public List<SpaceRequistionCountList> spcreqcountList { get; set; }
    public List<SPACE_ALLOC_DETAILS> space_rel_details { get; set; }
    public int ALLOCSTA { get; set; }
    public string ShiftFilter { get; set; }
    public string SRN_L1_REM { get; set; }
    public string SRN_L2_REM { get; set; }
    public string SRN_IT_REM { get; set; }
    public int REQ_CNT_STA_FUT { get; set; }
    public string SRN_SYS_PRF_CODE { get; set; }
}

public class SpaceRequistion
{
    public string SRN_REQ_ID { get; set; }
    public string SRN_SYS_PRF_CODE { get; set; }
    public string SRN_VERTICAL { get; set; }
    public string SRN_COST_CENTER { get; set; }
    public int SRN_STA_ID { get; set; }
    public DateTime SRN_FROM_DATE { get; set; }
    public DateTime SRN_TO_DATE { get; set; }
    public string SRN_REQ_BY { get; set; }
    public string SRN_REQ_REM { get; set; }
    public string SRN_L1_APPR_BY { get; set; }
    public string SRN_L1_REM { get; set; }
    public string SRN_L2_APPR_BY { get; set; }
    public string SRN_L2_REM { get; set; }
    public string SRN_ALLC_REM { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
}

public class SpaceRequistion_details
{
    public string SRD_REQ_ID { get; set; }
    public string SRD_SRNREQ_ID { get; set; }
    public string SRD_SSA_SRNREQ_ID { get; set; }
    public string SRD_SPC_ID { get; set; }
    public string SRD_SPC_NAME { get; set; }
    public string SRD_SPC_TYPE { get; set; }
    public string SRD_SPC_TYPE_NAME { get; set; }
    public string SRD_SPC_SUB_TYPE { get; set; }
    public string SRD_SPC_SUB_TYPE_NAME { get; set; }
    public string SRD_SH_CODE { get; set; }
    public string SRD_AUR_ID { get; set; }
    public string SRD_REM { get; set; }
    public string SRD_LCM_CODE { get; set; }
    public string lat { get; set; }
    public string lon { get; set; }
    public string SSA_FLR_CODE { get; set; }
    public int SRD_STA_ID { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SRC_REQ_SEL_TYPE { get; set; }
    public string SRC_REQ_TYPE { get; set; }
    public int SRC_REQ_CNT { get; set; }
}

public class SpaceRequistionCount
{
    public string SRC_SRN_REQ_ID { get; set; }
    public string SRC_CNY_CODE { get; set; }
    public string SRC_CNY_NAME { get; set; }
    public string SRC_CTY_CODE { get; set; }
    public string SRC_CTY_NAME { get; set; }
    public string SRC_LCM_CODE { get; set; }
    public string SRC_LCM_NAME { get; set; }
    public string SRC_TWR_CODE { get; set; }
    public string SRC_TWR_NAME { get; set; }
    public string SRC_FLR_CODE { get; set; }
    public string SRC_FLR_NAME { get; set; }
    public string SRC_SEL_SYS_PRF_CODE { get; set; }
    public int SRC_REQ_CNT { get; set; }
    public int SRC_ALLC_CNT { get; set; }
    public string SRC_REQ_SEL_TYPE { get; set; }
    public string SRC_REQ_TYPE { get; set; }
    public int SRC_STA_ID { get; set; }

}
public class SpaceReq
{
    public string SRN_REQ_ID { get; set; }
    public string SRN_VERTICAL { get; set; }
    public string SRN_COST_CENTER { get; set; }
    public int SRN_STA_ID { get; set; }
    public DateTime SRN_FROM_DATE { get; set; }
    public DateTime SRN_TO_DATE { get; set; }
    public string SRN_REQ_BY { get; set; }
    public string SRN_REQ_REM { get; set; }
    public string SRN_L1_APPR_BY { get; set; }
    public string SRN_L1_REM { get; set; }
    public string SRN_L2_APPR_BY { get; set; }
    public string SRN_L2_REM { get; set; }
    public string SRN_IT_REM { get; set; }
    public int STACHECK { get; set; }
    public string VER_NAME { get; set; }
    public string COST_CENTER_NAME { get; set; }
    public string STA_DESC { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string SRN_SYS_PRF_CODE { get; set; }
    public Nullable<DateTime> SRN_REQ_DT { get; set; }
    public string APP_REM { get; set; }
    public string SRC_SRN_REQ_ID { get; set; }

}
public class SpaceRequistionCountList
{
    public string SRC_SRN_REQ_ID { get; set; }
    public string SRC_CNY_CODE { get; set; }
    public string SRC_CNY_NAME { get; set; }
    public string SRC_CTY_CODE { get; set; }
    public string SRC_CTY_NAME { get; set; }
    public string SRC_LCM_CODE { get; set; }
    public string SRC_LCM_NAME { get; set; }
    public string SRC_TWR_CODE { get; set; }
    public string SRC_TWR_NAME { get; set; }
    public string SRC_FLR_CODE { get; set; }
    public string SRC_FLR_NAME { get; set; }
    public string SRC_SEL_SYS_PRF_CODE { get; set; }
    public int SRC_REQ_CNT { get; set; }
    public string SRC_REQ_SEL_TYPE { get; set; }
    public string SRC_REQ_TYPE { get; set; }
    public int SRC_STA_ID { get; set; }
    public List<String> SelectedSpacesList { get; set; }
    public string SPC_TYPE_NAME { get; set; }
    public string SPC_SUB_TYPE_NAME { get; set; }
    public string SSA_SPC_ID { get; set; }
}

public class SpaceVacantDetails
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SRN_REQ_ID { get; set; }

}