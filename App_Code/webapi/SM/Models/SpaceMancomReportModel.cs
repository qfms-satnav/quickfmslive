﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceMancomReportModel
/// </summary>
public class SpaceMancomReportModel
{
   
        public string MANCOM { get; set; }
        public string SPC_TYPE { get; set; }
        public string TOTAL_SEATS { get; set; }
        public string OCCUPIED_SEATS { get; set; }
        public string ALLOCATED_VACANT_SEATS { get; set; }

        public string VACANT { get; set; }

    
}