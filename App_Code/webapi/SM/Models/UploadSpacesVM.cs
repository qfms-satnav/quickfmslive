﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UploadSpacesVM
/// </summary>
public class UploadSpacesVM
{
    public List<Floorlst> flrlst { get; set; }
    public string ALLOCSTA { get; set; }
}

public class UploadAllocationDataVM
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SpaceID { get; set; }
    public string SeatType { get; set; }
    public string Vertical { get; set; }
    public string Entity { get; set; }
    public string Costcenter { get; set; }
    public string EmployeeID { get; set; }
    public string EmployeeName { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
    public string PortNumber { get; set; }
    public string BayNumber { get; set; }
    public string FloorCode { get; set; }
}

public class UploadAllocationDataDumpVM
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SpaceID { get; set; }
    public string SeatType { get; set; }
    public string Vertical { get; set; }
    public string Entity { get; set; }
    public string Costcenter { get; set; }
    public string EmployeeID { get; set; }
    public string EmployeeName { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
}

public class UPLTYPEVM
{
    public string UplAllocType { get; set; }
    public string UplOptions { get; set; }
}
public class ShiftInformation
{
    public string Shift_Code { get; set; }
    public string Shift_Name { get; set; }
    public string Shift_Location { get; set; }
    public string Shift_SeatType { get; set; }
    public string Shift_FromTime { get; set; }
    public string Shift_ToTime { get; set; }
    public string Shift_Lcm_Name { get; set; }

}
public class SpaceRoster
{
    public string City { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Floor { get; set; }
    public string SpaceID { get; set; }
    public string SeatType { get; set; }
    public string Vertical { get; set; }
    public string Costcenter { get; set; }
    public string EmployeeID { get; set; }
    public string EmployeeName { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string Shift { get; set; }

    //hidden Fields
    public string FromTime { get; set; }
    public string ToTime { get; set; }
    public string ValidDatesOfBooking { get; set; }
    public string ServerRemarks { get; set; }
    public int? Excel_Row_No { get; set; }
}