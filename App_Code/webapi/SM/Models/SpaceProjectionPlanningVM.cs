﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SpaceProjectionPlanningVM : ApiController
{
    public VerticalReleaseMaster VRM { get; set; }
    public class VerticalReleaseMaster
    {
        public string EU_CITY { get; set; }
        public string EU_LOC { get; set; }
        public string EU_TOW{ get; set; }
       
    }
    
}
