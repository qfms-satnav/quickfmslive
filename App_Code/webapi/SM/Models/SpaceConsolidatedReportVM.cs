﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for SpaceConsolidatedReportVM
/// </summary>
public class SpaceConsolidatedReportVM
{
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string SPC_TYPE { get; set; }
    public string COST_CENTER { get; set; }
    public Int32 BLOCKED_SEATS { get; set; }
    public Int32 ALLOCATED_SEATS { get; set; }
    public Int32 OCCUPIED_SEATS { get; set; }
    public Int32 EMPLOYEE_OCCUPIED_SEAT_COUNT { get; set; }
    public Int32 ALLOCATED_VACANT { get; set; }
    public Int32 VACANT_SEATS { get; set; }
    public Int32 TOTAL_SEATS { get; set; }
    public Int32 REQUESTED_SEATS { get; set; }
    public string OVERALL_COUNT { get; set; }
}

public class SpaceConsolidatedReportVM_Chart
{
    public string ALLOCATED_SEATS { get; set; }
    public string OCCUPIED_SEATS { get; set; }
    public string EMPLOYEE_OCCUPIED_SEAT_COUNT { get; set; }
    public string ALLOCATED_VACANT { get; set; }
    public string VACANT_SEATS { get; set; }
    public string TOTAL_SEATS { get; set; }
}

public class SpaceConsolidatedParameters
{
    public string year { get; set; }
    public string month { get; set; }
    public string DocType { get; set; }
    public string CNP_NAME { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public int FLAG { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public string SearchValue { get; set; }
    public string VER_CODE { get; set; }
    public string Cost_Center_Code { get; set; }
    public string Towers { get; set; }
    public string FLOORS { get; set; }
    public string LOCATION { get; set; }
    public string TOWERS { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<LocationsList> loclst { get; set; }
    public List<ChildEntityLst> entitylst { get; set; }
 
}

public class LocationsList
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }

    public bool ticked { get; set; }
}

public class SpaceConsolGridCols
{
    public string headerName { get; set; }
    public string field { get; set; }
    public int width { get; set; }
    public string cellClass { get; set; }
    public bool suppressMenu { get; set; }
}
public class SpaceConsolClass
{
    public string title { get; set; }
    public string key { get; set; }
}
//Mancon
//public class SpaceConsolidatedReportMancom
//{
//    public string MANCOM { get; set; }
//    public string SPC_TYPE { get; set; }
//    public string TOTAL_SEATS { get; set; }
//    public string OCCUPIED_SEATS { get; set; }
//    public string ALLOCATED_VACANT_SEATS { get; set; }
//    public string VACANT { get; set; }

//}

//Summary
//public class SpaceSummaryReport
//{
//    public string CNY_NAME { get; set; }
//    public string CTY_NAME { get; set; }
//    public string LCM_NAME { get; set; }
//    public string TWR_NAME { get; set; }
//    public string FLR_NAME { get; set; }
//    public string SPC_TYPE { get; set; }
//    public string TOTAL_SEATS { get; set; }
//    public string ALLOCATED_SEATS { get; set; }
//    public string ALLOCATED_VACANT { get; set; }
//    public string VACANT_SEATS { get; set; }
//    public string OCCUPIED_SEATS { get; set; }
//    public string EMPLOYEE_OCCUPIED_SEAT_COUNT { get; set; }
//    public string OVERALL_COUNT { get; set; }
//    public string BLOCKED_SEATS { get; set; }
//    public string REQUESTED_SEATS { get; set; }


//}

public class SpaceSonsolidatedReportView
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public SpaceSonsolidatedReportView()
    {
        CostData = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> CostData { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case SpaceSonsolidatedReportView.ReportFormat.Word: return ".doc";
                case SpaceSonsolidatedReportView.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.CostData)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}