﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Space_Subtype_ReportModel
/// </summary>
public class Space_Subtype_ReportModel
{
   public List<SpaceSeatData> SeatData { get; set; }
   public List<Floorlst> flrlst { get; set; }
}

public class SpaceSeatData
{
    public string COUNTRY { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string TOWER { get; set; }
    public string FLOOR { get; set; }
    public string SPACE { get; set; }
    public string SPC_TYPE_NAME { get; set; }
    public string SST_NAME { get; set; }

    public string STATUS { get; set; }

}