﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class GeomStringObject
{
    public int Id { get; set; }
    public string FloorId { get; set; }
    public string SpaceId { get; set; }
    public string GeomString { get; set; }
    public string Layer { get; set; }
}

public class SpaceItem
{
    public string SPC_ID { get; set; }
    public string SPACE_TYPE { get; set; }
    public string SPC_SUB_TYPE { get; set; }
    public string SPC_FLR_ID { get; set; }
    public string SPC_LAYER { get; set; }
}

public class SpaceTypeItem
{
    public int SPC_TYPE_STATUS { get; set; }
    public string SPC_TYPE_CODE { get; set; }
}

public class GeomRelatedQuery
{
    public static List<SpaceItem> GetSpaceItemByFloorCode(string floorCode)
    {
        var connectionString = ConfigurationManager.ConnectionStrings["CSAmantraFAM"].ConnectionString;

        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand selectCommand = new SqlCommand(@"Select SPC_ID,SPC_LAYER, SPACE_TYPE, SPC_SUB_TYPE, SPC_FLR_ID From " + (HttpContext.Current.Session["TENANT"]) + "." + " [SPACE] Where SPC_FLR_ID = '" + floorCode + "'", connection)
        {
            CommandType = CommandType.Text
        };
        SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);


        List<SpaceItem> SpaceItemList = new List<SpaceItem>();

        if(dataTable != null && dataTable.Rows.Count > 0)
        {
            SpaceItemList = (from DataRow dr in dataTable.AsEnumerable()
                                 select new SpaceItem()
                                 {
                                     SPACE_TYPE = dr["SPACE_TYPE"] == DBNull.Value ? "" : dr["SPACE_TYPE"].ToString(),
                                     SPC_SUB_TYPE = dr["SPC_SUB_TYPE"] == DBNull.Value ? "" : dr["SPC_SUB_TYPE"].ToString(),
                                     SPC_FLR_ID = dr["SPC_FLR_ID"] == DBNull.Value ? "" : dr["SPC_FLR_ID"].ToString(),
                                     SPC_ID = dr["SPC_ID"] == DBNull.Value ? "" : dr["SPC_ID"].ToString(),
                                     SPC_LAYER = dr["SPC_LAYER"] == DBNull.Value ? "" : dr["SPC_LAYER"].ToString()
                                 }).ToList();
        }        

        return SpaceItemList;
    }

    public static List<SpaceTypeItem> GetSpaceTypes()
    {
        var connectionString = ConfigurationManager.ConnectionStrings["CSAmantraFAM"].ConnectionString;

        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand selectCommand = new SqlCommand(@"Select SPC_TYPE_STATUS, SPC_TYPE_CODE From " + (HttpContext.Current.Session["TENANT"]) + "." + " SPACE_TYPE", connection)
        {
            CommandType = CommandType.Text
        };
        SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);

        List<SpaceTypeItem> SpaceTypeItemList = new List<SpaceTypeItem>();

        if (dataTable != null && dataTable.Rows.Count > 0)
        {
            SpaceTypeItemList = (from DataRow dr in dataTable.AsEnumerable()
                                 select new SpaceTypeItem()
                                 {
                                     SPC_TYPE_STATUS = dr["SPC_TYPE_STATUS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SPC_TYPE_STATUS"]),
                                     SPC_TYPE_CODE = dr["SPC_TYPE_CODE"] == DBNull.Value ? "" : dr["SPC_TYPE_CODE"].ToString()
                                 }).ToList();
        }

        return SpaceTypeItemList;
    }
    //By Vinod For Mobile
    public static List<SpaceItem> GetSpaceItemByFloorCode(string floorCode,string DB)
    {
        var connectionString = ConfigurationManager.ConnectionStrings["CSAmantraFAM"].ConnectionString;

        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand selectCommand = new SqlCommand(@"Select SPC_ID,SPC_LAYER, SPACE_TYPE, SPC_SUB_TYPE, SPC_FLR_ID From " + DB + "." + " [SPACE] Where SPC_FLR_ID = '" + floorCode + "'", connection)
        {
            CommandType = CommandType.Text
        };
        SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);


        List<SpaceItem> SpaceItemList = new List<SpaceItem>();

        if (dataTable != null && dataTable.Rows.Count > 0)
        {
            SpaceItemList = (from DataRow dr in dataTable.AsEnumerable()
                             select new SpaceItem()
                             {
                                 SPACE_TYPE = dr["SPACE_TYPE"] == DBNull.Value ? "" : dr["SPACE_TYPE"].ToString(),
                                 SPC_SUB_TYPE = dr["SPC_SUB_TYPE"] == DBNull.Value ? "" : dr["SPC_SUB_TYPE"].ToString(),
                                 SPC_FLR_ID = dr["SPC_FLR_ID"] == DBNull.Value ? "" : dr["SPC_FLR_ID"].ToString(),
                                 SPC_ID = dr["SPC_ID"] == DBNull.Value ? "" : dr["SPC_ID"].ToString(),
                                 SPC_LAYER = dr["SPC_LAYER"] == DBNull.Value ? "" : dr["SPC_LAYER"].ToString()
                             }).ToList();
        }

        return SpaceItemList;
    }

    public static List<SpaceTypeItem> GetSpaceTypes(string DB)
    {
        var connectionString = ConfigurationManager.ConnectionStrings["CSAmantraFAM"].ConnectionString;

        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand selectCommand = new SqlCommand(@"Select SPC_TYPE_STATUS, SPC_TYPE_CODE From " + DB + "." + " SPACE_TYPE", connection)
        {
            CommandType = CommandType.Text
        };
        SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
        DataTable dataTable = new DataTable();
        adapter.Fill(dataTable);

        List<SpaceTypeItem> SpaceTypeItemList = new List<SpaceTypeItem>();

        if (dataTable != null && dataTable.Rows.Count > 0)
        {
            SpaceTypeItemList = (from DataRow dr in dataTable.AsEnumerable()
                                 select new SpaceTypeItem()
                                 {
                                     SPC_TYPE_STATUS = dr["SPC_TYPE_STATUS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SPC_TYPE_STATUS"]),
                                     SPC_TYPE_CODE = dr["SPC_TYPE_CODE"] == DBNull.Value ? "" : dr["SPC_TYPE_CODE"].ToString()
                                 }).ToList();
        }

        return SpaceTypeItemList;
    }
}