﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class CustomizedDetailss
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public List<Floorlst> flrlst { get; set; }
    public List<ChildEntityLst> enylist { get; set; }
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string FLOORS { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public string SearchValue { get; set; }
    public string BHO_NAME { get; set; }
    public string BHT_NAME { get; set; }

}


public class CustReports
{
    public List<CustomizedDatas> custdata { get; set; }
    public string Request_Type { get; set; }
    public string Type { get; set; }

}

public class Costcenterlsts
{
    public string Cost_Center_Code { get; set; }
    public string CostCenter_Name { get; set; }
    public bool ticked { get; set; }
}

public class SpaceConsolidatedReportMancom
{
    public string MANCOM { get; set; }
    public string SPC_TYPE { get; set; }
    public string TOTAL_SEATS { get; set; }
    public string OCCUPIED_SEATS { get; set; }
    public string ALLOCATED_VACANT_SEATS { get; set; }
    public string VACANT { get; set; }

}
public class CustomizedDatas
{
    public string COUNTRY { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string TOWER { get; set; }
    public string FLOOR { get; set; }
    public string SPACE { get; set; }
    public double AREA { get; set; }
    public double SEATCOST { get; set; }
    public double UNITCOST { get; set; }
    public string PE_CODE { get; set; }
    public string CHE_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string DEPARTMENT { get; set; }
    public string EMP_ID { get; set; }
    public string EMP_NAME { get; set; }
    public string MAIL { get; set; }
    public string REPORTING { get; set; }
    public string REPORTING_NAME { get; set; }
    public string STATUS { get; set; }
    public string AUR_TYPE { get; set; }
    public string AUR_GRADE { get; set; }
    public string SPACE_TYPE { get; set; }
    public string SPACE_SUB_TYPE { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string ShiftTime { get; set; }
    public string AllocationDate { get; set; }
    public string AUR_LONG_LEAVE_FROM_DT { get; set; }
    public string AUR_LONG_LEAVE_TO_DT { get; set; }
    public string AUR_LEAVE_STATUS { get; set; }
    public string AUR_LONG_LEAVE_REASON { get; set; }
    public string BHO_NAME { get; set; }
    public string BHT_NAME { get; set; }


}
public class SpaceSummaryReport
{
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string SPC_TYPE { get; set; }
    public string TOTAL_SEATS { get; set; }
    public string ALLOCATED_SEATS { get; set; }
    public string ALLOCATED_VACANT { get; set; }
    public string VACANT_SEATS { get; set; }
    public string OCCUPIED_SEATS { get; set; }
    public string EMPLOYEE_OCCUPIED_SEAT_COUNT { get; set; }
    public string OVERALL_COUNT { get; set; }
    public string BLOCKED_SEATS { get; set; }
    public string REQUESTED_SEATS { get; set; }


}
public class FLOORLSTS
{
    public string FLR_Code { get; set; }
    public string FLR_Name { get; set; }
    public bool ticked { get; set; }
}

public class Verticallsts
{
    public string VER_CODE { get; set; }
    public string VER_NAME { get; set; }
    public bool ticked { get; set; }
}

public class Statelst
{
    public string AM_ST_CODE { get; set; }
    public string AM_ST_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class CustomizedReportViews
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public CustomizedReportViews()
    {
        SpaceDatas = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> SpaceDatas { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case CustomizedReportViews.ReportFormat.Word: return ".doc";
                case CustomizedReportViews.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.SpaceDatas)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}
public class SpaceConsolidatedReportVMS
{
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string SPC_TYPE { get; set; }
    public string COST_CENTER { get; set; }
    public Int32 BLOCKED_SEATS { get; set; }
    public Int32 ALLOCATED_SEATS { get; set; }
    public Int32 OCCUPIED_SEATS { get; set; }
    public Int32 EMPLOYEE_OCCUPIED_SEAT_COUNT { get; set; }
    public Int32 ALLOCATED_VACANT { get; set; }
    public Int32 VACANT_SEATS { get; set; }
    public Int32 TOTAL_SEATS { get; set; }
    public Int32 REQUESTED_SEATS { get; set; }
    public string OVERALL_COUNT { get; set; }
}
public class SpaceConsolidatedReportVM_ChartS
{
    public string ALLOCATED_SEATS { get; set; }
    public string OCCUPIED_SEATS { get; set; }
    public string EMPLOYEE_OCCUPIED_SEAT_COUNT { get; set; }
    public string ALLOCATED_VACANT { get; set; }
    public string VACANT_SEATS { get; set; }
    public string TOTAL_SEATS { get; set; }
}

