﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceSeatHistoryVM
/// </summary>
public class SpaceSeatHistoryVM
{
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public int Status { get; set; }


}