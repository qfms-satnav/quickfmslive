﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpacePlanningReport
{
    public List<Towerlst> twrlst { get; set; }
    public List<Verticallst> verlst { get; set; }
    public List<Costcenterlst> cstlst { get; set; }
    public List<SpacePlanningReportVM> spcreqdet { get; set; }
    public string Type { get; set; }
    public int CNP_NAME { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}
public class SpacePlanningReportVM
{
    public string SP_REQ_ID { get; set; }
    public string SP_CTY { get; set; }
    public string SP_LOC { get; set; }
    public string SP_TOW { get; set; }
    public string VER_NAME { get; set; }
    public string Total_Seats { get; set; }
    public string COST_CENTER_NAME { get; set; }
    public string SP_GRADE { get; set; }
    public string Q1 { get; set; }
    public string Q2 { get; set; }
    public string Q3 { get; set; }
    public string Q4 { get; set; }
    public string FY1 { get; set; }
    public string FY2 { get; set; }
    public string FY3 { get; set; }
    public string TOTAL_SEATS_REQUIRED { get; set; }
    public string WORK_STATION { get; set; }
    public string Cabin { get; set; }
    public string TWD { get; set; }
    public string BUILD_CAPACITY { get; set; }
    public string UTILIZED_SEATS { get; set; }
    public string VACANT { get; set; }
    public string REQUIREMENT_PENDING { get; set; }
    public string STAUS { get; set; }
    public string OVERALL_COUNT { get; set; }

}
