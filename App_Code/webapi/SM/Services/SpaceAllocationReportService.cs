﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

public class SpaceAllocationReportService
{
    SubSonic.StoredProcedure sp;
    SpaceAllocationData SpcAlloc;
    DataSet ds;

    public List<SpaceAllocationData> GetAllocationDetails(SpaceAllocationDetials spd)
    {
        try
        {
            List<SpaceAllocationData> AllocList = new List<SpaceAllocationData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_ALLOCATION_REPORT_EXPORT");
            sp.Command.AddParameter("@FROM_DATE", spd.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", spd.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", spd.CNP_NAME, DbType.String);
            //sp.Command.AddParameter("@PageNumber", spd.PageNumber, DbType.String);
            //sp.Command.AddParameter("@PageSize", spd.PageSize, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            AllocList = JsonConvert.DeserializeObject<List<SpaceAllocationData>>(JSONString);

            //using (IDataReader reader = sp.GetReader())
            //{
            //    while (reader.Read())
            //    {
            //        SpcAlloc = new SpaceAllocationData();
            //        SpcAlloc.CNY_NAME = reader["CNY_NAME"].ToString();
            //        SpcAlloc.CTY_NAME = reader["CTY_NAME"].ToString();
            //        SpcAlloc.LCM_NAME = reader["LCM_NAME"].ToString();
            //        SpcAlloc.TWR_NAME = reader["TWR_NAME"].ToString();
            //        SpcAlloc.ENTITY = reader["ENTITY"].ToString();
            //        SpcAlloc.CHILD_ENTITY = reader["CHILD_ENTITY"].ToString();
            //        SpcAlloc.FLR_NAME = reader["FLR_NAME"].ToString();
            //        SpcAlloc.SPACE_TYPE = reader["SPC_TYPE"].ToString();
            //        SpcAlloc.VER_NAME = reader["VER_NAME"].ToString();
            //        SpcAlloc.DEP_NAME = reader["DEP_NAME"].ToString();
            //        SpcAlloc.SHIFT = reader["SHIFT"].ToString();
            //        SpcAlloc.ALLOC_COUNT = (int)reader["ALLOC_COUNT"];
            //        SpcAlloc.OCCUP_COUNT = (int)reader["OCCUP_COUNT"];
            //        SpcAlloc.VAC_COUNT = (int)reader["VAC_COUNT"];
            //        SpcAlloc.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
            //        AllocList.Add(SpcAlloc);
            //    }
            //    reader.Close();
            //}
            if (AllocList.Count != 0)
                return AllocList;
            else
                return AllocList;
        }
        catch(Exception ex)
        {
            throw;
        }
    }

    public List<SpaceAllocationData> GetAllocationDetailsExport(SpaceAllocationDetials spd)
    {
        try
        {
            List<SpaceAllocationData> AllocList = new List<SpaceAllocationData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_ALLOCATION_REPORT_EXPORT");
            sp.Command.AddParameter("@FROM_DATE", spd.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", spd.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", spd.CNP_NAME, DbType.String);

            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    SpcAlloc = new SpaceAllocationData();
                    SpcAlloc.CNY_NAME = reader["CNY_NAME"].ToString();
                    SpcAlloc.CTY_NAME = reader["CTY_NAME"].ToString();
                    SpcAlloc.LCM_NAME = reader["LCM_NAME"].ToString();
                    SpcAlloc.TWR_NAME = reader["TWR_NAME"].ToString();
                    SpcAlloc.ENTITY = reader["ENTITY"].ToString();
                    SpcAlloc.CHILD_ENTITY = reader["CHILD_ENTITY"].ToString();
                    SpcAlloc.FLR_NAME = reader["FLR_NAME"].ToString();
                    SpcAlloc.SPACE_TYPE = reader["SPC_TYPE"].ToString();
                    SpcAlloc.VER_NAME = reader["VER_NAME"].ToString();
                    SpcAlloc.DEP_NAME = reader["DEP_NAME"].ToString();
                    SpcAlloc.SHIFT = reader["SHIFT"].ToString();
                    SpcAlloc.ALLOC_COUNT = (int)reader["ALLOC_COUNT"];
                    SpcAlloc.OCCUP_COUNT = (int)reader["OCCUP_COUNT"];
                    SpcAlloc.VAC_COUNT = (int)reader["VAC_COUNT"];
                    SpcAlloc.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                    SpcAlloc.BHO_NAME = reader["BHO_NAME"].ToString();
                    SpcAlloc.BHT_NAME = reader["BHT_NAME"].ToString();
                    AllocList.Add(SpcAlloc);
                }
                reader.Close();
            }
            if (AllocList.Count != 0)
                return AllocList;
            else
                return AllocList;
        }
        catch
        {
            throw;
        }
    }

    //Vertical & Dept Wise Allocated Count
    public object GetChartCountData(SpaceReportDetails AllocDetails)
    {
        try
        {
            List<SpaceAllocationData> Spcdata = new List<SpaceAllocationData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_ALLOCATION_COUNT");
            sp.Command.AddParameter("@FROM_DATE", AllocDetails.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", AllocDetails.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", AllocDetails.CNP_NAME, DbType.String);
            ds = sp.GetDataSet();
            UtilityService userv = new UtilityService();
            DataTable dt = userv.GetInversedDataTable(ds.Tables[0], "VER_NAME");
            var columns = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
            return new { Message = "", data = new { Details = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), Columnnames = columns } };

        }
        catch
        {
            throw;
        }
    }

    public object SearchAllData(SpaceReportDetails Det)
    {

        List<SpaceAllocationData> AllocList = new List<SpaceAllocationData>();
        DataTable DT = new DataTable();


        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@FROM_DATE", SqlDbType.NVarChar);
            param[0].Value = Det.FromDate.ToString();
            param[1] = new SqlParameter("@TO_DATE", SqlDbType.NVarChar);
            param[1].Value = Det.ToDate.ToString();
            param[2] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = Det.CNP_NAME;
            param[4] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[4].Value = Det.PageNumber;
            param[5] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[5].Value = Det.PageSize;
            param[6] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
            param[6].Value = Det.SearchValue;

            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_SPACE_ALLOCATION_REPORT_SEARCH_VALUE", param);
            return DT;
        }
        catch (Exception ex)
        {
            return DT;
        }
    }
}