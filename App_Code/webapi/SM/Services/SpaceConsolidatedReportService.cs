﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;


/// <summary>
/// Summary description for SpaceConsolidatedReportService
/// </summary>
public class SpaceConsolidatedReportService
{
    SubSonic.StoredProcedure sp;
    //List<SpaceConsolidatedReportVM> rptByUserlst;
    //HDMReport_Bar_Graph rptBarGph;
    DataSet ds;

    public object BindGrid(SpaceConsolidatedParameters Params)
    {
        try
        {
            dynamic rptByUserlst;
            if (Params.FLAG == 1)
                rptByUserlst = GetReportList(Params);
            else
                rptByUserlst = GetReportList_location(Params);

            if (rptByUserlst != null)
            { return new { Message = MessagesVM.UM_OK, data = rptByUserlst }; }
            else
            { return new { Message = MessagesVM.UM_NO_REC, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<SpaceConsolidatedReportVM> GetReportList(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportVM> rptByUserlst = new List<SpaceConsolidatedReportVM>();
        SpaceConsolidatedReportVM rptByUser;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_SMSS");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        sp.Command.AddParameter("@LCM_NAME", Params.LCM_NAME, DbType.String);
        sp.Command.AddParameter("@Cost_Center_Code", Params.Cost_Center_Code, DbType.String);
        sp.Command.AddParameter("@VER_CODE", Params.VER_CODE, DbType.String);
        sp.Command.AddParameter("@month", Params.month, DbType.String);
        sp.Command.AddParameter("@year", Params.year, DbType.String);
        //sp.Command.AddParameter("@PageNumber", Params.PageNumber, DbType.String);
        //sp.Command.AddParameter("@PageSize", Params.PageSize, DbType.String);
        //sp.Command.AddParameter("@SearchValue", Params.SearchValue, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByUser = new SpaceConsolidatedReportVM();
                rptByUser.CNY_NAME = sdr["CNY_NAME"].ToString();
                rptByUser.CTY_NAME = sdr["CTY_NAME"].ToString();
                rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                rptByUser.FLR_NAME = sdr["FLR_NAME"].ToString();
                rptByUser.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                rptByUser.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptByUser.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptByUser.BLOCKED_SEATS = Convert.ToInt32(sdr["BLOCKED_SEATS"]);
                rptByUser.ALLOCATED_SEATS = Convert.ToInt32(sdr["ALLOCATED_SEATS"]);
                rptByUser.OCCUPIED_SEATS = Convert.ToInt32(sdr["OCCUPIED_SEATS"]);
                rptByUser.EMPLOYEE_OCCUPIED_SEAT_COUNT = Convert.ToInt32(sdr["EMPLOYEE_OCCUPIED_SEAT_COUNT"]);
                rptByUser.ALLOCATED_VACANT = Convert.ToInt32(sdr["ALLOCATED_VACANT"]);
                rptByUser.VACANT_SEATS = Convert.ToInt32(sdr["VACANT_SEATS"]);
                rptByUser.TOTAL_SEATS = Convert.ToInt32(sdr["TOTAL_SEATS"]);
                rptByUser.REQUESTED_SEATS = Convert.ToInt32(sdr["REQUESTED_SEATS"]);
                rptByUser.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();

                rptByUserlst.Add(rptByUser);
            }
            sdr.Close();
        }
        if (rptByUserlst.Count != 0)
            return rptByUserlst;
        else
            return null;
    }


    public List<SpaceConsolidatedReportVM> GetReportList_location(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportVM> rptByLocationlst = new List<SpaceConsolidatedReportVM>();
        SpaceConsolidatedReportVM rptByLocation;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_SM");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        //sp.Command.AddParameter("@PageNumber", Params.PageNumber, DbType.String);
        //sp.Command.AddParameter("@PageSize", Params.PageSize, DbType.String);
        //sp.Command.AddParameter("@SearchValue", Params.SearchValue, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByLocation = new SpaceConsolidatedReportVM();
                rptByLocation.CTY_NAME = sdr["CTY_NAME"].ToString();
                rptByLocation.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptByLocation.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByLocation.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptByLocation.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                rptByLocation.OCCUPIED_SEATS = Convert.ToInt32(sdr["OCCUPIED_SEATS"]);
                rptByLocation.EMPLOYEE_OCCUPIED_SEAT_COUNT = Convert.ToInt32(sdr["EMPLOYEE_OCCUPIED_SEAT_COUNT"]);
                rptByLocation.VACANT_SEATS = Convert.ToInt32(sdr["VACANT_SEATS"]);
                rptByLocation.TOTAL_SEATS = Convert.ToInt32(sdr["TOTAL_SEATS"]);
                rptByLocation.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                rptByLocationlst.Add(rptByLocation);
            }
            sdr.Close();
        }
        if (rptByLocationlst.Count != 0)
            return rptByLocationlst;
        else
            return null;
    }


    //mancon

    public List<SpaceConsolidatedReportMancom> GetMancom(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportMancom> rptByUserlst = new List<SpaceConsolidatedReportMancom>();
        SpaceConsolidatedReportMancom rptByMancom;
        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_MANCOM");
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_MANCOM_REPORT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByMancom = new SpaceConsolidatedReportMancom();
                rptByMancom.MANCOM = sdr["MANCOM"].ToString();
                rptByMancom.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                rptByMancom.TOTAL_SEATS = sdr["TOTAL_SEATS"].ToString();
                rptByMancom.OCCUPIED_SEATS = sdr["OCCUPIED_SEATS"].ToString();
                rptByMancom.ALLOCATED_VACANT_SEATS = sdr["ALLOCATED_VACANT_SEATS"].ToString();
                rptByMancom.VACANT = sdr["VACANT"].ToString();
                rptByUserlst.Add(rptByMancom);
            }
            sdr.Close();
        }

        return rptByUserlst;


    }


    public List<SpaceConsolidatedReportVM> GetExportLst(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportVM> rptByLocationlst = new List<SpaceConsolidatedReportVM>();
        SpaceConsolidatedReportVM rptByLocation;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROJECT_EXPORT_REPORT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByLocation = new SpaceConsolidatedReportVM();
                rptByLocation.COST_CENTER = sdr["COST_CENTER"].ToString();
                //rptByLocation.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByLocation.FLR_NAME = sdr["FLR_NAME"].ToString();
                rptByLocation.TOTAL_SEATS = Convert.ToInt32(sdr["TOTAL_SEATS"]);
                rptByLocationlst.Add(rptByLocation);
            }
            sdr.Close();
        }
        if (rptByLocationlst.Count != 0)
            return rptByLocationlst;
        else
            return null;
    }

    //public object GetSummary(SpaceConsolidatedParameters Params)
    //{
    //    try
    //    {

    //        List<SpaceConsolGridCols> gridcols = new List<SpaceConsolGridCols>();
    //        SpaceConsolGridCols gridcolscls;
    //        SpaceConsolClass exportcls;
    //        List<SpaceConsolClass> lstexportcls = new List<SpaceConsolClass>();
    //        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_LOCATION_SM");
    //        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
    //        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
    //        sp.Command.AddParameter("@LCM_NAME", Params.LCM_NAME, DbType.String);
    //        sp.Command.AddParameter("@Cost_Center_Code", Params.Cost_Center_Code, DbType.String);
    //        sp.Command.AddParameter("@VER_CODE", Params.VER_CODE, DbType.String);
    //        sp.Command.AddParameter("@month", Params.month, DbType.String);
    //        sp.Command.AddParameter("@year", Params.year, DbType.String);
    //        DataSet ds = sp.GetDataSet();

    //        List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
    //                               select dc.ColumnName).ToList();

    //        foreach (string col in Colstr)
    //        {

    //            gridcolscls = new SpaceConsolGridCols();
    //            gridcolscls.cellClass = "grid-align";
    //            gridcolscls.field = col;
    //            gridcolscls.headerName = col;
    //            //gridcolscls.suppressMenu = true;
    //            gridcolscls.width = 100;
    //            gridcols.Add(gridcolscls);
    //            exportcls = new SpaceConsolClass();
    //            exportcls.title = col;
    //            exportcls.key = col;
    //            lstexportcls.Add(exportcls);
    //        }

    //        return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    public object GetSummary(SpaceConsolidatedParameters Params)
    {
        try
        {

            List<SpaceConsolGridCols> gridcols = new List<SpaceConsolGridCols>();
            SpaceConsolGridCols gridcolscls;
            SpaceConsolClass exportcls;
            List<SpaceConsolClass> lstexportcls = new List<SpaceConsolClass>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[SMS_SPACE_CONSOLIDATED_REPORT_LOCATION_SM_NEW]");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@LCM_NAME", Params.LCM_NAME, DbType.String);
            sp.Command.AddParameter("@Cost_Center_Code", Params.Cost_Center_Code, DbType.String);
            sp.Command.AddParameter("@VER_CODE", Params.VER_CODE, DbType.String);
            sp.Command.AddParameter("@month", Params.month, DbType.String);
            sp.Command.AddParameter("@year", Params.year, DbType.String);
            DataSet ds = sp.GetDataSet();

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {

                gridcolscls = new SpaceConsolGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcolscls.width = 100;
                gridcols.Add(gridcolscls);
                exportcls = new SpaceConsolClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }

            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    public object SpaceConsolidatedChart(SpaceConsolidatedParameters Params)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_CHART_SM");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        ds = sp.GetDataSet();
        //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

        //if (arr.Length != 0)
        //    return new { Message = MessagesVM.UM_OK, data = arr };  
        //else
        //    return new { Message = MessagesVM.UM_NO_REC, data = arr };
        UtilityService userv = new UtilityService();
        DataTable dt = userv.GetInversedDataTable(ds.Tables[0], "ALLOCSTA");
        var columns = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
        return new { Message = "", data = new { Details = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), Columnnames = columns } };
    }
    public object SpaceConsolidatedChart()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "SMS_SPACE_CONSOLIDATED_REPORT_CHART");
            ds = sp.GetDataSet();

            List<object> Occupied = new List<object>();
            List<object> Vacant = new List<object>();
            List<object> Locations = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();



            foreach (var i in Locations)
            {
                Occupied.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("LCM_NAME") == i)).Sum(x => (x.Field<int>("OCCUPIED"))));
                Vacant.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("LCM_NAME") == i)).Sum(x => (x.Field<int>("VACANT"))));

            }
            Locations.Insert(0, "x");
            Occupied.Insert(0, "Occupied Count");
            Vacant.Insert(0, "Vacant Count");
            return new { Locations = Locations, Occupied = Occupied, Vacant = Vacant };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object GetVerticals(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_VERTICALS");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetCostcenter(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_COSTCENTERS");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
}