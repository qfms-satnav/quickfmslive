﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MapMarkerService
/// </summary>
public class MapMarkerService
{
    public object GetCitywiseLoc()
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CITY_WISE_LCM");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetMarkers()
    {
        DataSet ds = new DataSet();
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_GOOGLE_MAP_MARKERS");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
}