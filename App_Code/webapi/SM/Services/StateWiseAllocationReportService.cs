﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class StateWiseAllocationReportService
{
    SubSonic.StoredProcedure sp;
    List<CustomizedData> Cust;
    CustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(CustomizedDetailss Det)
    {
        try
        {
            DataTable dt = GetCustomizedDetails_dump(Det);
            if (dt.Rows.Count != 0) { return new { Message = MessagesVM.SER_OK, data = dt }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetCustomizedDetails(CustomizedDetailss Details)
    {
        try
        {
            DataTable DT = new DataTable();
            DT = (DataTable)HttpContext.Current.Session["SpaceCustomizedGrid"];
            return DT;

        }
        catch
        {
            throw;
        }
    }

    public DataTable GetCustomizedDetails_dump(CustomizedDetailss Det)
    {

        List<CustomizedData> CData = new List<CustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Det.Request_Type;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Det.flrlst);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[4].Value = Det.FromDate;
            param[5] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[5].Value = Det.ToDate;
            param[6] = new SqlParameter("@Entity", SqlDbType.Structured);
            param[6].Value = UtilityService.ConvertToDataTable(Det.enylist);
            param[7] = new SqlParameter("@VERTICL", SqlDbType.NVarChar);
            param[7].Value = Det.VERTICAL;
            param[8] = new SqlParameter("@COSTCENTER", SqlDbType.NVarChar);
            param[8].Value = Det.COSTCENTER;


            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_CUSTOMIZED_REPORT_EXPORT", param);
            HttpContext.Current.Session["SpaceCustomizedGrid"] = DT;
            return DT;
        }
        catch (Exception ex) { return DT; }
    }

    public object SearchAllData(CustomizedDetailss Det)
    {
        List<CustomizedData> CData = new List<CustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Det.Request_Type;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Det.flrlst);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[4].Value = Det.PageNumber;
            param[5] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[5].Value = Det.PageSize;
            param[6] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
            param[6].Value = Det.SearchValue;

            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_CUSTOMIZED_SEARCH_GRID", param);
            return DT;
        }
        catch (Exception ex) { return DT; }
    }

    public object GetStatesBycountry(int id)
    {
        List<Statelst> statelst = new List<Statelst>();
        Statelst state;

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@MODE", SqlDbType.Int);
        param[1].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_STATE_BY_CNYLST_ADM", param))
        {
            while (sdr.Read())
            {
                state = new Statelst();
                state.AM_ST_CODE = sdr["AM_ST_CODE"].ToString();
                state.AM_ST_NAME = sdr["State_NAME"].ToString();
                state.CNY_CODE = sdr["AM_ST_COUNTRY"].ToString();
                state.ticked = false;
                statelst.Add(state);
            }
        }
        if (statelst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = statelst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }



    public object getCitiesByState(List<Statelst> state, int id)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@STLST", SqlDbType.Structured);
        if (state == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(state);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CITY_BY_StateLST_ADM", param))
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object CityByState(int id)
    {
        List<Citylst> citylst = new List<Citylst>();
        Citylst city;

        SqlParameter[] param = new SqlParameter[2];
        //param[0] = new SqlParameter("@CNYLST", SqlDbType.Structured);
        //if (cny == null)
        //{
        //    param[0].Value = null;
        //}
        //else
        //{
        //    param[0].Value = ConvertToDataTable(cny);
        //}
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@MODE", SqlDbType.Int);
        param[1].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CITY_BY_STLST_ADM", param))
        {
            while (sdr.Read())
            {
                city = new Citylst();
                city.CTY_CODE = sdr["CTY_CODE"].ToString();
                city.CTY_NAME = sdr["CTY_NAME"].ToString();
                city.CNY_CODE = sdr["CNY_CODE"].ToString();
                city.ticked = false;
                citylst.Add(city);
            }
        }
        if (citylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = citylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //public List<SpaceConsolidatedReportMancom> GetMancom(SpaceConsolidatedParameters Params)
    //{

    //    List<SpaceConsolidatedReportMancom> rptByUserlst = new List<SpaceConsolidatedReportMancom>();
    //    SpaceConsolidatedReportMancom rptByMancom;
    //    //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_MANCOM");
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "CONSOLIDATED_SMS_SPACE_MANCOM_REPORT");
    //    sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
    //    sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
    //    sp.Command.AddParameter("@VERTICAL", Params.VERTICAL, DbType.String);
    //    sp.Command.AddParameter("@Towers", Params.Towers, DbType.String);
    //    sp.Command.AddParameter("@LOCATION", Params.LCM_NAME, DbType.String);
    //    sp.Command.AddParameter("@FLOORS", Params.FLOORS, DbType.String);
    //    using (IDataReader sdr = sp.GetReader())
    //    {
    //        while (sdr.Read())
    //        {
    //            rptByMancom = new SpaceConsolidatedReportMancom();
    //            rptByMancom.MANCOM = sdr["MANCOM"].ToString();
    //            rptByMancom.SPC_TYPE = sdr["SPC_TYPE"].ToString();
    //            rptByMancom.TOTAL_SEATS = sdr["TOTAL_SEATS"].ToString();
    //            rptByMancom.OCCUPIED_SEATS = sdr["OCCUPIED_SEATS"].ToString();
    //            rptByMancom.ALLOCATED_VACANT_SEATS = sdr["ALLOCATED_VACANT_SEATS"].ToString();
    //            rptByMancom.VACANT = sdr["VACANT"].ToString();
    //            rptByUserlst.Add(rptByMancom);
    //        }
    //        sdr.Close();
    //    }

    //    return rptByUserlst;


    //}

    public object GetVerticalByFloor(List<FLOORLSTS> floor, int id)
    {
        List<Verticallst> verticallst = new List<Verticallst>();
        Verticallst vertical;

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@STLST", SqlDbType.Structured);
        if (floor == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(floor);
        }
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@MODE", SqlDbType.Int);
        param[1].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_VERTICAL_BY_FLRLST_ADM", param))
        {
            while (sdr.Read())
            {
                vertical = new Verticallst();
                vertical.VER_CODE = sdr["VER_CODE"].ToString();
                vertical.VER_NAME = sdr["VER_NAME"].ToString();
                vertical.ticked = false;
                verticallst.Add(vertical);
            }
        }
        if (verticallst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verticallst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetCostCenterByVertical(int id)
    {
        List<Costcenterlsts> costcenterlst = new List<Costcenterlsts>();
        Costcenterlsts costcenter;

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@MODE", SqlDbType.Int);
        param[1].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CostCenter_BY_VERLST_ADM", param))
        {
            while (sdr.Read())
            {
                costcenter = new Costcenterlsts();
                costcenter.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                costcenter.CostCenter_Name = sdr["CostCenter_Name"].ToString();
                costcenter.ticked = false;
                costcenterlst.Add(costcenter);
            }
        }
        if (costcenterlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = costcenterlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }



    public object GetCostcenterByMancom(List<Verticallsts> verticallsts, int id)
    {
        List<Costcenterlsts> Costcenterlst = new List<Costcenterlsts>();
        Costcenterlsts costcenterlsts;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@VERLST", SqlDbType.Structured);
        if (verticallsts == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(verticallsts);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_COSTCENTER_BY_MANCOM", param))
        {
            while (sdr.Read())
            {
                costcenterlsts = new Costcenterlsts();
                costcenterlsts.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                costcenterlsts.CostCenter_Name = sdr["Cost_Center_Name"].ToString();
                costcenterlsts.ticked = false;
                Costcenterlst.Add(costcenterlsts);
            }
        }
        if (Costcenterlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Costcenterlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object BindGrid(SpaceConsolidatedParameters Params)
    {
        try
        {
            dynamic rptByUserlst;
            if (Params.FLAG == 1)
                rptByUserlst = GetReportList(Params);
            else
                rptByUserlst = GetReportList_location(Params);

            if (rptByUserlst != null)
            { return new { Message = MessagesVM.UM_OK, data = rptByUserlst }; }
            else
            { return new { Message = MessagesVM.UM_NO_REC, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<SpaceConsolidatedReportVMS> GetReportList(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportVMS> rptByUserlst = new List<SpaceConsolidatedReportVMS>();
        SpaceConsolidatedReportVMS rptByUser;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_SM");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        //sp.Command.AddParameter("@PageNumber", Params.PageNumber, DbType.String);
        //sp.Command.AddParameter("@PageSize", Params.PageSize, DbType.String);
        //sp.Command.AddParameter("@SearchValue", Params.SearchValue, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByUser = new SpaceConsolidatedReportVMS();
                rptByUser.CNY_NAME = sdr["CNY_NAME"].ToString();
                rptByUser.CTY_NAME = sdr["CTY_NAME"].ToString();
                rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                rptByUser.FLR_NAME = sdr["FLR_NAME"].ToString();
                rptByUser.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                rptByUser.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptByUser.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptByUser.BLOCKED_SEATS = Convert.ToInt32(sdr["BLOCKED_SEATS"]);
                rptByUser.ALLOCATED_SEATS = Convert.ToInt32(sdr["ALLOCATED_SEATS"]);
                rptByUser.OCCUPIED_SEATS = Convert.ToInt32(sdr["OCCUPIED_SEATS"]);
                rptByUser.EMPLOYEE_OCCUPIED_SEAT_COUNT = Convert.ToInt32(sdr["EMPLOYEE_OCCUPIED_SEAT_COUNT"]);
                rptByUser.ALLOCATED_VACANT = Convert.ToInt32(sdr["ALLOCATED_VACANT"]);
                rptByUser.VACANT_SEATS = Convert.ToInt32(sdr["VACANT_SEATS"]);
                rptByUser.TOTAL_SEATS = Convert.ToInt32(sdr["TOTAL_SEATS"]);
                rptByUser.REQUESTED_SEATS = Convert.ToInt32(sdr["REQUESTED_SEATS"]);
                rptByUser.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();

                rptByUserlst.Add(rptByUser);
            }
            sdr.Close();
        }
        if (rptByUserlst.Count != 0)
            return rptByUserlst;
        else
            return null;
    }


    public List<SpaceConsolidatedReportVMS> GetReportList_location(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportVMS> rptByLocationlst = new List<SpaceConsolidatedReportVMS>();
        SpaceConsolidatedReportVMS rptByLocation;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "CONSOLIDATED_SMS_SPACE_Location_REPORT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        sp.Command.AddParameter("@LOCATION", Params.LCM_NAME, DbType.String);
        sp.Command.AddParameter("@TOWERS", Params.Towers, DbType.String);
        sp.Command.AddParameter("@FLOORS", Params.FLOORS, DbType.String);
        sp.Command.AddParameter("@VERTICAL", Params.VERTICAL, DbType.String);
        sp.Command.AddParameter("@COSTCENTER", Params.COSTCENTER, DbType.String);

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByLocation = new SpaceConsolidatedReportVMS();
                rptByLocation.CTY_NAME = sdr["CTY_NAME"].ToString();
                rptByLocation.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptByLocation.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByLocation.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptByLocation.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                rptByLocation.OCCUPIED_SEATS = Convert.ToInt32(sdr["OCCUPIED_SEATS"]);
                rptByLocation.EMPLOYEE_OCCUPIED_SEAT_COUNT = Convert.ToInt32(sdr["EMPLOYEE_OCCUPIED_SEAT_COUNT"]);
                rptByLocation.VACANT_SEATS = Convert.ToInt32(sdr["VACANT_SEATS"]);
                rptByLocation.TOTAL_SEATS = Convert.ToInt32(sdr["TOTAL_SEATS"]);
                rptByLocation.ALLOCATED_SEATS = Convert.ToInt32(sdr["CNP_NAME"]);
                rptByLocation.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                rptByLocationlst.Add(rptByLocation);
            }
            sdr.Close();
        }
        if (rptByLocationlst.Count != 0)
            return rptByLocationlst;
        else
            return null;
    }


    //mancom

    public List<SpaceConsolidatedReportMancom> GetMancom(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportMancom> rptByUserlst = new List<SpaceConsolidatedReportMancom>();
        SpaceConsolidatedReportMancom rptByMancom;
        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_MANCOM");
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "CONSOLIDATED_SMS_SPACE_MANCOM_REPORT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        sp.Command.AddParameter("@VERTICAL", Params.VERTICAL, DbType.String);
        sp.Command.AddParameter("@Towers", Params.Towers, DbType.String);
        sp.Command.AddParameter("@LOCATION", Params.LCM_NAME, DbType.String);
        sp.Command.AddParameter("@FLOORS", Params.FLOORS, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByMancom = new SpaceConsolidatedReportMancom();
                rptByMancom.MANCOM = sdr["MANCOM"].ToString();
                rptByMancom.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                rptByMancom.TOTAL_SEATS = sdr["TOTAL_SEATS"].ToString();
                rptByMancom.OCCUPIED_SEATS = sdr["OCCUPIED_SEATS"].ToString();
                rptByMancom.ALLOCATED_VACANT_SEATS = sdr["ALLOCATED_VACANT_SEATS"].ToString();
                rptByMancom.VACANT = sdr["VACANT"].ToString();
                rptByUserlst.Add(rptByMancom);
            }
            sdr.Close();
        }

        return rptByUserlst;


    }
    public List<SpaceSummaryReport> BindGridSummary(SpaceConsolidatedParameters Params)
    {

        List<SpaceSummaryReport> rptByUserlst = new List<SpaceSummaryReport>();
        SpaceSummaryReport rptByMancom;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "VIEW_SPACE_ALLOCATIONS_SUMMARY_REPORT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        sp.Command.AddParameter("@LOCATION", Params.LOCATION, DbType.String);
        sp.Command.AddParameter("@TOWERS", Params.TOWERS, DbType.String);
        sp.Command.AddParameter("@FLOORS", Params.FLOORS, DbType.String);
        sp.Command.AddParameter("@VERTICAL", Params.VERTICAL, DbType.String);
        sp.Command.AddParameter("@COSTCENTER", Params.COSTCENTER, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByMancom = new SpaceSummaryReport();
                rptByMancom.CNY_NAME = sdr["CNY_NAME"].ToString();
                rptByMancom.CTY_NAME = sdr["CTY_NAME"].ToString();
                rptByMancom.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByMancom.TWR_NAME = sdr["TWR_NAME"].ToString();
                rptByMancom.FLR_NAME = sdr["FLR_NAME"].ToString();
                rptByMancom.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                rptByMancom.TOTAL_SEATS = sdr["TOTAL_SEATS"].ToString();
                rptByMancom.ALLOCATED_SEATS = sdr["ALLOCATED_SEATS"].ToString();
                rptByMancom.VACANT_SEATS = sdr["VACANT_SEATS"].ToString();
                rptByMancom.OCCUPIED_SEATS = sdr["OCCUPIED_SEATS"].ToString();
                rptByMancom.EMPLOYEE_OCCUPIED_SEAT_COUNT = sdr["EMPLOYEE_OCCUPIED_SEAT_COUNT"].ToString();
                rptByMancom.ALLOCATED_VACANT = sdr["ALLOCATED_VACANT"].ToString();
                rptByMancom.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                rptByMancom.BLOCKED_SEATS = sdr["BLOCKED_SEATS"].ToString();
                rptByMancom.REQUESTED_SEATS = sdr["REQUESTED_SEATS"].ToString();
                rptByUserlst.Add(rptByMancom);
            }
            sdr.Close();
        }

        return rptByUserlst;


    }
    public List<SpaceConsolidatedReportVMS> GetExportLst(SpaceConsolidatedParameters Params)
    {

        List<SpaceConsolidatedReportVMS> rptByLocationlst = new List<SpaceConsolidatedReportVMS>();
        SpaceConsolidatedReportVMS rptByLocation;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROJECT_EXPORT_REPORT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByLocation = new SpaceConsolidatedReportVMS();
                rptByLocation.COST_CENTER = sdr["COST_CENTER"].ToString();
                rptByLocation.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByLocation.TOTAL_SEATS = Convert.ToInt32(sdr["TOTAL_SEATS"]);
                rptByLocationlst.Add(rptByLocation);
            }
            sdr.Close();
        }
        if (rptByLocationlst.Count != 0)
            return rptByLocationlst;
        else
            return null;
    }

    public object GetProjectSummary(SpaceConsolidatedParameters Params)
    {
        try
        {

            List<SpaceConsolGridCols> gridcols = new List<SpaceConsolGridCols>();
            SpaceConsolGridCols gridcolscls;
            SpaceConsolClass exportcls;
            List<SpaceConsolClass> lstexportcls = new List<SpaceConsolClass>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "CONSOLIDATED_SMS_SPACE_COSTCENTER_REPORT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@Towers", Params.Towers, DbType.String);
            sp.Command.AddParameter("@FLOORS", Params.FLOORS, DbType.String);
            sp.Command.AddParameter("@LCM_NAME", Params.LCM_NAME, DbType.String);
            sp.Command.AddParameter("@VERTICAL", Params.VERTICAL, DbType.String);
            sp.Command.AddParameter("@COSTCENTER", Params.COSTCENTER, DbType.String);
            //sp.Command.AddParameter("@PageNumber", Params.PageNumber, DbType.String);
            //sp.Command.AddParameter("@PageSize", Params.PageSize, DbType.String);
            //sp.Command.AddParameter("@SearchValue", Params.SearchValue, DbType.String);
            DataSet ds = sp.GetDataSet();

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {

                gridcolscls = new SpaceConsolGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcolscls.width = 100;
                gridcols.Add(gridcolscls);
                exportcls = new SpaceConsolClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }

            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    public object SpaceConsolidatedChart(SpaceConsolidatedParameters Params)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_CONSOLIDATED_REPORT_CHART_SM");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Params.CNP_NAME, DbType.String);
        ds = sp.GetDataSet();
        //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

        //if (arr.Length != 0)
        //    return new { Message = MessagesVM.UM_OK, data = arr };  
        //else
        //    return new { Message = MessagesVM.UM_NO_REC, data = arr };
        UtilityService userv = new UtilityService();
        DataTable dt = userv.GetInversedDataTable(ds.Tables[0], "ALLOCSTA");
        var columns = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
        return new { Message = "", data = new { Details = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), Columnnames = columns } };
    }
    public object SpaceConsolidatedChart()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "SMS_SPACE_CONSOLIDATED_REPORT_CHART");
            ds = sp.GetDataSet();

            List<object> Occupied = new List<object>();
            List<object> Vacant = new List<object>();
            List<object> Locations = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();



            foreach (var i in Locations)
            {
                Occupied.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("LCM_NAME") == i)).Sum(x => (x.Field<int>("OCCUPIED"))));
                Vacant.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("LCM_NAME") == i)).Sum(x => (x.Field<int>("VACANT"))));

            }
            Locations.Insert(0, "x");
            Occupied.Insert(0, "Occupied Count");
            Vacant.Insert(0, "Vacant Count");
            return new { Locations = Locations, Occupied = Occupied, Vacant = Vacant };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }
}