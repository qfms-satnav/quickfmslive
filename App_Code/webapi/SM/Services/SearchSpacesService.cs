﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SearchSpacesService
/// </summary>
public class SearchSpacesService
{
    VerticalReq_details verreqdet;
    List<VerticalReq_details> verreqdetlst;
    SubSonic.StoredProcedure sp;
    public object GetVacantSpaces(SpaceDetails spcdet)
    {
        verreqdetlst = GetVacantSpacesList(spcdet);
        if (verreqdetlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verreqdetlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<VerticalReq_details> GetVacantSpacesList(SpaceDetails spcdet)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
        param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[1].Value = spcdet.verreq.SVR_FROM_DATE;
        param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[2].Value = spcdet.verreq.SVR_TO_DATE;
        param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];
        verreqdetlst = new List<VerticalReq_details>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_VACANT_SPACES", param))
        {
           
            while (sdr.Read())
            {
                verreqdet = new VerticalReq_details();
                verreqdet.SVD_SPC_ID = sdr["SPC_ID"].ToString();
                verreqdet.SVD_SPC_NAME = sdr["SPC_NAME"].ToString();
                verreqdet.SVD_SPC_TYPE = sdr["SPC_TYPE_CODE"].ToString();
                verreqdet.SVD_SPC_TYPE_NAME = sdr["SPC_TYPE_NAME"].ToString();
                verreqdet.SVD_SPC_SUB_TYPE = sdr["SST_CODE"].ToString();
                verreqdet.SVD_SPC_SUB_TYPE_NAME = sdr["SST_NAME"].ToString();
                verreqdet.lat = sdr["lat"].ToString();
                verreqdet.lon = sdr["lon"].ToString();
                verreqdet.FLR_ID = sdr["FLR_ID"].ToString();
                verreqdetlst.Add(verreqdet);
            }
        }
        return verreqdetlst;
    }

    public object RaiseRequest(SpaceDetails spcdet)
    {
        List<VerticalRequistion> verlst = new List<VerticalRequistion>();
        verlst.Add(spcdet.verreq);
        //if (spcdet.verreqdet.Count != 0)
        //{
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@VERT_ALLOC_DET", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(spcdet.verreqdet);
            param[1] = new SqlParameter("@VERT_ALLOC_REQ", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(verlst);
            param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
            param[3] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
            param[3].Value = spcdet.ALLOCSTA;
            param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            param[5] = new SqlParameter("@MODE", SqlDbType.NVarChar);
            param[5].Value = spcdet.MODE;
            param[6] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[6].Value = HttpContext.Current.Session["COMPANYID"];

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_VERTREQ_TRAN", param))
            {
                if (dr.Read())
                {
                    if ((int)dr["FLAG"] == 2)
                    {
                        string RetMessage = string.Empty;
                        RequestState sta = (RequestState)spcdet.ALLOCSTA;
                        switch (sta)
                        {
                            case RequestState.Added: RetMessage = MessagesVM.BSR_INSERTED;
                                break;
                            case RequestState.Modified: RetMessage = MessagesVM.BSR_UPDATED;
                                break;
                            case RequestState.Canceled: RetMessage = MessagesVM.BSR_CANCELED;
                                break;
                            case RequestState.Approved: RetMessage = MessagesVM.BSR_APPROVED;
                                break;
                            case RequestState.Rejected: RetMessage = MessagesVM.BSR_REJECTED;
                                break;
                            case RequestState.Blocked: RetMessage = MessagesVM.BSR_BLOCKED;
                                break;

                        }
                        SendMailVerticalRequisition(dr["REQID"].ToString(), spcdet.ALLOCSTA);
                        return new { Message = RetMessage + " For : " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                    }
                    else if ((int)dr["FLAG"] == 1)
                    {
                        SendMailVerticalAllocation(dr["REQID"].ToString(), spcdet.ALLOCSTA);
                        if (spcdet.ALLOCSTA == (int)RequestState.Blocked)
                            return new { Message = MessagesVM.BSR_BLOCKED, data = dr["REQID"].ToString() };
                        else
                        return new { Message = HttpContext.Current.Session["Parent"] + MessagesVM.BSR_ALLOCATED, data = dr["REQID"].ToString() };
                    }
                    else
                        return new { Message = dr["REQID"].ToString(), data = (object)null };
                }
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
       // }
        //else
        //    return new { Message = MessagesVM.BSR_NODET, data = (object)null };
    }

    public object ApproveRequests(SpaceDetails spcdet)
    {
        if (spcdet.verreqList.Count != 0)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@SVR_REQ_ID", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(spcdet.verreqList);
            param[1] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
            param[1].Value = spcdet.ALLOCSTA;
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_APPROVE_REQUISITIONS", param))
            {
                if (dr.Read())
                {
                    if ((int)dr["FLAG"] == 1)
                        if (spcdet.ALLOCSTA == (int)RequestState.Approved)
                        {
                            return new { Message = MessagesVM.BSR_APPROVED, data = dr["MSG"].ToString() };
                        }
                            else
                             return new { Message = MessagesVM.BSR_REJECTED, data = dr["MSG"].ToString() };                       

                        else
                            return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
            }

            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        else
            return new { Message = MessagesVM.BSR_NODET, data = (object)null };
    }

    public void SendMailVerticalAllocation(string REQUEST_ID, int FLAG)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_VERTICAL_ALLOCATED");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }

    public void SendMailVerticalRequisition(string REQUEST_ID, int FLAG)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_VERTICAL_REQUISITION_REQUEST");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }
}