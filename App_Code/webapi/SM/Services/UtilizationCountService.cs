﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class UtilizationCountService
{
    public object GetGriddata(UtlzationCountDetails utilized)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(utilized.Locations);
            param[1] = new SqlParameter("@TWRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(utilized.Towers);
            param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(utilized.Floors);
            param[3] = new SqlParameter("@VERTICALS", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(utilized.Vertical);
            param[4] = new SqlParameter("@COSTCENTER", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(utilized.costcenter);

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_USER_UTILIZATION_COUNT", param))
            {
                List<UtilizationGridDetails> GridLst = new List<UtilizationGridDetails>();
                while (reader.Read())
                {
                    GridLst.Add(new UtilizationGridDetails()
                    {
                        Country = reader["Country"].ToString(),
                        City = reader["City"].ToString(),
                        Location = reader["Location"].ToString(),
                        Tower = reader["Tower"].ToString(),
                        Floor = reader["Floor"].ToString(),
                        Vertical = reader["Vertical"].ToString(),
                        Costcenter = reader["Costcenter"].ToString(),
                        SeatType = reader["SeatType"].ToString(),
                        //HeadCOUNT = reader["HeadCOUNT"].ToString(),
                        Seats = reader["Seats"].ToString(),
                        Max = reader["Max"].ToString(),
                        //Seat_TURN = reader["Seat_TURN"].ToString(),
                        Utilization = reader["Utilization"].ToString(),

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = GridLst };
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
    public List<UtilizationGridDetails>  GetGriddataexcel(UtlzationCountDetails utilized)
    {
        SqlParameter[] param = new SqlParameter[5];

        param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(utilized.Locations);
        param[1] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(utilized.Towers);
        param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(utilized.Floors);
        param[3] = new SqlParameter("@VERTICALS", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(utilized.Vertical);
        param[4] = new SqlParameter("@COSTCENTER", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(utilized.costcenter);
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_USER_UTILIZATION_COUNT",param))
        {
            List<UtilizationGridDetails> GridLst1 = new List<UtilizationGridDetails>();
            while (reader.Read())
            {
                GridLst1.Add(new UtilizationGridDetails()
                {
                    Country = reader["Country"].ToString(),
                    City = reader["City"].ToString(),
                    Location = reader["Location"].ToString(),
                    Tower = reader["Tower"].ToString(),
                    Floor = reader["Floor"].ToString(),
                    Vertical = reader["Vertical"].ToString(),
                    Costcenter = reader["Costcenter"].ToString(),
                    SeatType = reader["SeatType"].ToString(),
                   // HeadCOUNT = reader["HeadCOUNT"].ToString(),
                    Seats = reader["Seats"].ToString(),
                    Max = reader["Max"].ToString(),
                   // Seat_TURN = reader["Seat_TURN"].ToString(),
                    Utilization = reader["Utilization"].ToString(),

                });
            }
            reader.Close();
            return GridLst1;
        }
    }
}