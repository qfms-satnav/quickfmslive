﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for EmployeeLocationReportService
/// </summary>
public class EmployeeLocationReportService
{
    SubSonic.StoredProcedure sp;
    List<CustomizedData> Cust;
    CustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(EmployeeLocationDetails Det)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["Uid"];
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Det.flrlst);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Branch_Location_Wise_Emp_Report", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (SqlException) { throw; }
    }
}