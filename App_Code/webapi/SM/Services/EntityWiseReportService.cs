﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for EntityWiseReportService
/// </summary>
public class EntityWiseReportService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public object GetSummary(SpaceConsolidatedParameters Params)
    {
        try
        {
            DataSet ds = new DataSet();
            List<string> str1 = new List<string>(); 
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            foreach (var item in Params.loclst)
            {
                str1.Add(item.LCM_CODE);
            }
            SqlParameter[] param = new SqlParameter[1];
            string str = String.Join(",", str1);
            param[0] = new SqlParameter("@loclst", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Params.loclst);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_SPACE_ENTITY_WISE", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (Exception EX)
        {
            throw EX;
        }
    }
    public object SearchData(SpaceConsolidatedParameters Params)
    {
        try
        {
            DataSet ds = new DataSet();
            List<string> str1 = new List<string>();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[0].Value = Params.LCM_CODE;
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_SPACE_ENTITY_WISE_SEARCH", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (Exception EX)
        {
            throw EX;
        }
    }
}

