﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;

/// <summary>
/// Summary description for KotakMaploaderService
/// </summary>
public class KotakMaploaderService
{
    SubSonic.StoredProcedure sp;

    public object GetFloorLst()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GETUSERFLOORLIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetImageList(string Image)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SELETOWER_IMAGE_LIST");
        sp.Command.Parameters.Add("@TWI_CODE", Image, DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetMapItems(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS_KOTAK");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        ds = sp.GetDataSet();
        Hashtable sendData = new Hashtable();

        foreach (DataRow drIn in ds.Tables[1].Rows)
        {
            sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
        }
        return new { mapDetails = ds.Tables[0], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
    }

    public object GetMarkers(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_MARKER_ALL_kotak");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetCornerLables(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOOR_CORNERS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetLegendsCount(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@FLAG", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@SH_CODE", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }

    public object GetLegendsSummary(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP_LEGEND");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        IDataReader dr = sp.GetReader();
        LEGEND_SUMMARY ls = new LEGEND_SUMMARY();
        if (dr.Read())
        {
            ls.ALLOCATEDVER = (int)dr["ALLOCATEDVER"];
            ls.ALLOCATEDCST = (int)dr["ALLOCATEDCST"];
            ls.TOTAL = (int)dr["TOTAL"];
            ls.VACANT = (int)dr["VACANT"];
            ls.OCCUPIED = (int)dr["OCCUPIED"];
            ls.OVERLOAD = (int)dr["OVERLOAD"];
            ls.BLOCKED = (int)dr["BLOCKED"];
            ls.ALLOCUNUSED = ds.Tables[1].Rows[0]["ALLOC_UNUSED"].ToString();
            ls.OCCUP_EMP = ds.Tables[2].Rows[0]["OCCUP_EMP"].ToString();
            ls.LEAVE = Convert.ToInt32(ds.Tables[3].Rows[0]["LEAVE"].ToString());
        }
        return ls;
    }

    public object GetSpaceDetailsBySPCID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYSPCID");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.CatValue, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbyItem()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_ITEM_SM");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbySubItem(Space_mapVM param)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_SUBITEM_SM");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEMID", param.Item, DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetSpaceDetailsBySUBITEM(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DATA_MAP_SM");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@SUB_ITEM", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();

        //if (svm.Item == "login")
        //{

        //    //ActiveDirectoryHelper adhelper = new ActiveDirectoryHelper();
        //    //List<ADUserDetail> useradlist = adhelper.GetUserslist();
        //    sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AD_DATA_LIVE");
        //    DataSet DS1 = sp.GetDataSet();

        //    List<AD_DATA_VM> useradlist = DS1.Tables[0].AsEnumerable()
        //          .Select(row => new AD_DATA_VM
        //          {
        //              USR_ID = row.Field<string>(0),
        //              LOG_IN_TIME = row.Field<string>(1),
        //              LOG_OUT_TIME = row.Field<string>(2)
        //          }).ToList();

        //    List<Space_MapEmployeeVM> empmaplist = ds.Tables[0].AsEnumerable()
        //      .Select(row => new Space_MapEmployeeVM
        //      {
        //          SPC_ID = row.Field<string>(0),
        //          x = row.Field<double>(1).ToString(),
        //          y = row.Field<double>(2).ToString(),
        //          SPC_FLR_ID = row.Field<string>(3),
        //          SPC_LAYER = row.Field<string>(4),
        //          SPC_DESC = row.Field<string>(5),
        //          STATUS = row.Field<int>(6).ToString(),
        //          AD_ID = row.Field<string>(7)
        //      }).ToList();

        //    var list3 = (from Item1 in empmaplist
        //                 join Item2 in useradlist
        //                 on Item1.AD_ID equals Item2.USR_ID // join on some property
        //                 into grouping
        //                 from Item2 in grouping.DefaultIfEmpty()
        //                 //where Item2 != null
        //                 select new { Item1, Item2 }).ToList();
        //    return list3;
        //}

        return ds;
    }

    public object GetSpaceDetailsByREQID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYREQID_kotak");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.category, DbType.String);
        sp.Command.Parameters.Add("@REQ_ID", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object ReleaseSelectedseat(SPACE_REL_DETAILS reldet)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@REL_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(reldet.sad);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@RELTYPE", SqlDbType.NVarChar);
        param[2].Value = reldet.reltype;
        param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];

        int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_SPACE_REL_MAP", param);

        if (RETVAL == 1)
            return new { Message = MessagesVM.UM_OK, data = reldet.reltype };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };


    }

    public object ActivateSpaces(CLS_INACTIVE_SPACE SpcAct)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@SPACE_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(SpcAct.SPC_LST);
            param[1] = new SqlParameter("@FLR_ID", SqlDbType.NVarChar);
            param[1].Value = SpcAct.FLR_CODE;
            param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            int RETVAL = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SMS_INSERT_SPACES", param);

            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };

        }
        catch (SqlException)
        {
            throw;
        }
    }

    public object InactiveSeats(CLS_INACTIVE_SPACE Inac)
    {
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@SPACE_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Inac.SPC_LST);
        param[1] = new SqlParameter("@FLR_ID", SqlDbType.NVarChar);
        param[1].Value = Inac.FLR_CODE;
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        int INAC = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_INACTIVE_SEATS", param);

        if (INAC == 1)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    public List<SPACEDETAILS> InactiveSpacesFromFloorMaps(Space_mapVM svm)
    {
        try
        {
            List<SPACEDETAILS> FlrMapslst = new List<SPACEDETAILS>();
            SPACEDETAILS ls;
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_INACTIVE_SEATS");
            sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            IDataReader dr = sp.GetReader();
            while (dr.Read())
            {
                ls = new SPACEDETAILS();
                ls.SPACE_ID = dr["SPACE_ID"].ToString();
                ls.SPC_TYPE = dr["SPC_TYPE_NAME"].ToString();
                ls.SPC_SUB_TYPE = dr["SST_NAME"].ToString();
                FlrMapslst.Add(ls);
            }
            dr.Close();
            return FlrMapslst;
        }
        catch
        {
            throw;
        }
    }

    public object GetEmpDetails(Space_mapVM svm)
    {
        if (svm.subitem != null && svm.subitem != "")
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_EMP_DETAILS_FLRID");
            sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
            sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
            sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
            sp.Command.Parameters.Add("@VER_CODE", svm.Item, DbType.String);
            sp.Command.Parameters.Add("@COST_CENTER_CODE", svm.subitem, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            return new { Message = MessagesVM.SelectCST, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.SelectCST, data = (object)null };
    }

    public object GetAllocEmpDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_ALLOC_EMP_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object AllocateSeats(List<SPACE_ALLOC_DETAILSKLI> allocDetLst)
    {
        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        List<SPACE_ALLOC_DETAILS_kotak> spcdet = new List<SPACE_ALLOC_DETAILS_kotak>();
        SPACE_ALLOC_DETAILS_kotak spc;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP_kotak", param))
        {
            while (sdr.Read())
            {
                string REQ_ID = sdr["SSA_SRNREQ_ID"].ToString();

                spc = new SPACE_ALLOC_DETAILS_kotak();
                spc.AUR_ID = sdr["AUR_ID"].ToString();
                spc.SPC_DESC = sdr["SPC_DESC"].ToString();
                spc.VERTICAL = sdr["VERTICAL"].ToString();
                spc.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                spc.FROM_DATE = Convert.ToDateTime(sdr["FROM_DATE"]);
                spc.TO_DATE = Convert.ToDateTime(sdr["TO_DATE"]);
                spc.SH_CODE = sdr["SH_CODE"].ToString();
                spc.SPC_ID = sdr["SPC_ID"].ToString();
                spc.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                spc.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                spc.STACHECK = (int)RequestState.Modified;
                spc.STATUS = Convert.ToInt16(sdr["STATUS"]);
                spc.ticked = true;
                spc.SHIFT_TYPE = sdr["SPACE_TYPE"].ToString();
                spcdet.Add(spc);

                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_EMPLOYEE_ALLOCATION_ACTION_SM");
                sp.Command.AddParameter("@REQID", REQ_ID, DbType.String);
                sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                sp.ExecuteScalar();
            }
        }


        foreach (SPACE_ALLOC_DETAILSKLI obj in allocDetLst.Where(x => x.STACHECK == (int)RequestState.Deleted).ToList())
        {
            spc = new SPACE_ALLOC_DETAILS_kotak();
            spc.SPC_ID = obj.SPC_ID;
            spc.STATUS = obj.STATUS;
            spc.ticked = true;
            spcdet.Add(spc);
        }
        if (spcdet.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcdet };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS sad)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_BY_SHIFT");
        sp.Command.Parameters.Add("@SPC_ID", sad.SPC_ID);
        sp.Command.Parameters.Add("@FDATE", sad.FROM_DATE);
        sp.Command.Parameters.Add("@TDATE", sad.TO_DATE);
        sp.Command.Parameters.Add("@SH_CODE", sad.SH_CODE);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);


        using (IDataReader dr = sp.GetReader())
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = dr["MSG"].ToString(), data = 1 };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetTotalAreaDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_GET_AREA_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetSeatingCapacity(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_GET_SEATING_CAPACITY_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetEmpAllocSeat(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_EMP_ALLOC_SEAT_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@MODE", svm.mode, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

}