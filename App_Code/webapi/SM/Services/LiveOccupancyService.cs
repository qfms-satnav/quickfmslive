﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for LiveOccupancyService
/// </summary>
public class LiveOccupancyService
{
    public DataSet DownloadOccpTemplate(UploadSpacesVM spcocc)
    {
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcocc.flrlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_DWNLD_OCCUPANCY_TEMPLATE", param);
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds;
    }

    public object UploadOccupancyTemplate(HttpRequest httpRequest)
    {
        try
        {
            List<UploadAllocationDataVM> uadmlst = GetDataTableFrmReq(httpRequest);

            String jsonstr = httpRequest.Params["CurrObj"];

            UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);


            string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_SPACEOCCUPATION_DATA (CITY,LOCATION,TOWER,FLOOR,SPACE_ID,SEATTYPE,VERTICAL,COSTCENTER,FROM_TIME,TO_TIME,PORT_NUMBER,BAY_ID,UPLOADEDBY,FLOOR_CODE) VALUES ";
            string str1 = "";
            int cnt = 1, retval;
            foreach (UploadAllocationDataVM uadm in uadmlst)
            {
                try
                {
                    if ((cnt % 1000) == 0)
                    {
                        str1 = str1.Remove(str1.Length - 1, 1);
                        retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                        if (retval == -1)
                        {
                            return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                        }
                        else
                        {
                            str1 = "";
                            cnt = 1;
                        }


                    }

                    str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}'),",
                              uadm.City,
                              uadm.Location,
                              uadm.Tower,
                              uadm.Floor,
                              uadm.SpaceID,
                              uadm.SeatType,
                              uadm.Vertical.Replace("'", "''"),
                              uadm.Costcenter.Replace("'", "''"),
                              uadm.FromTime,
                              uadm.ToTime,
                              uadm.PortNumber,
                              uadm.BayNumber,
                              HttpContext.Current.Session["Uid"],
                              uadm.FloorCode
                            );
                    cnt = cnt + 1;
                }
                catch (Exception)
                {                    
                    throw;
                }                
            }



            str1 = str1.Remove(str1.Length - 1, 1);
            retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[0].Value = HttpContext.Current.Session["UID"];
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];

                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_UPLOAD_OCCUPANCY_ALLOC", param);
                return new { Message = MessagesVM.UAD_UPLOK, data = dt };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public List<UploadAllocationDataVM> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
        var uplst = CreateExcelFile.ReadAsListOmega(filePath);
        if (uplst.Count != 0)
            uplst.RemoveAt(0);
        return uplst;

    }
}