﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.IO;

/// <summary>
/// Summary description for BreakDownMaintenanceServices
/// </summary>
public class BreakDownMaintenanceServices
{
    SubSonic.StoredProcedure sp;

    // GET Locations
    public object getLocations()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BDMP_GET_ALL_LOCATIONS");
        sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Startus
    public object getStartus()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_USER_STATUS_BYROLE");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Ticket Type
    public object getTicketType()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_BDMP_PRIORITY");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Equipment
    public object getEquipment(string BLDG)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BDMP_CREATE_GET_ASSET_GROUP_BY_LOCATION");
        sp.Command.Parameters.Add("@BLDG", BLDG, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    // GET Error Description
    public object getErrorDescription(ErrorDescription svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "HDM_GET_ERROR_DESCRIPTION");
        sp.Command.Parameters.Add("@TICKETTYPE", svm.TICKETTYPE, DbType.String);
        sp.Command.Parameters.Add("@CATEGORY", svm.CATEGORY, DbType.String);
        sp.Command.Parameters.Add("@LOC", svm.LOC, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Assign to
    public object getAssignto(getAssignto svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BREAKDOWN_BIND_INCHARGES");
        sp.Command.Parameters.Add("@LOC", svm.LOC, DbType.String);
        sp.Command.Parameters.Add("@TICKET_TYPE", svm.TICKET_TYPE, DbType.String);
        sp.Command.Parameters.Add("@EQUIPMENT", svm.EQUIPMENT, DbType.String);
       sp.Command.Parameters.Add("@ERRORDESCRIPTION", svm.ERRORDESCRIPTION, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Spare Parts
    public object getSpareParts(getAssignto svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPAREPARTS_DETAILS");
        sp.Command.Parameters.Add("@loc", svm.LOC, DbType.String);
        sp.Command.Parameters.Add("@Equipment", svm.EQUIPMENT, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Request Ids
    public object getBreakDownRqsts(getAssignto svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "VIEW_BDMP_STATUS");
        sp.Command.Parameters.Add("@loc", svm.LOC, DbType.String);
        sp.Command.Parameters.Add("@type", svm.EQUIPMENT, DbType.String);
        sp.Command.Parameters.Add("@aur_id", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Urgency
    public object getUrgency()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "HDM_BIND_URGENCY");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Submit 
    public object submit(breakdownSubmit reldet)
    {
        SqlParameter[] param = new SqlParameter[21];
        param[0] = new SqlParameter("@SPAREPARTDETAILS", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(reldet.spd);
        param[1] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        param[1].Value = reldet.REQID;
        param[2] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
        param[2].Value = reldet.STATUS;
        param[3] = new SqlParameter("@INCHARE_ID", SqlDbType.NVarChar);
        param[3].Value = reldet.INCHARE_ID;
        param[4] = new SqlParameter("@ROOT_CAUSE", SqlDbType.NVarChar);
        param[4].Value = reldet.ROOT_CAUSE;
        param[5] = new SqlParameter("@PREVENTIVE_ACTION", SqlDbType.NVarChar);
        param[5].Value = reldet.PREVENTIVE_ACTION;
        param[6] = new SqlParameter("@CORRECTIVE_ACTION", SqlDbType.NVarChar);
        param[6].Value = reldet.CORRECTIVE_ACTION;
        param[7] = new SqlParameter("@PRODUCTION_IMPCT", SqlDbType.NVarChar);
        param[7].Value = reldet.PRODUCTION_IMPCT;
        param[8] = new SqlParameter("@PROBLEM_OWNER", SqlDbType.NVarChar);
        param[8].Value = reldet.PROBLEM_OWNER;
        param[9] = new SqlParameter("@REMARKS", SqlDbType.NVarChar);
        param[9].Value = reldet.REMARKS;
        param[10] = new SqlParameter("@ATTACHMENT", SqlDbType.NVarChar);
        param[10].Value = reldet.ATTACHMENT;
        param[11] = new SqlParameter("@USER", SqlDbType.NVarChar);
        param[11].Value = HttpContext.Current.Session["UID"];
        param[12] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        if (reldet.TODATE == null)
            param[12].Value = ' ';
        else
            param[12].Value = reldet.TODATE;
        param[13] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        if (reldet.FROMDATE == null)
            param[13].Value = ' ';
        else
            param[13].Value = reldet.FROMDATE;
        param[14] = new SqlParameter("@BDMP_DWNTIME_FACTOR", SqlDbType.NVarChar);
        param[14].Value = reldet.BDMP_DWNTIME_FACTOR;
        param[15] = new SqlParameter("@BDMP_DOWNTIME", SqlDbType.NVarChar);
        param[15].Value = reldet.BDMP_DOWNTIME;
        param[16] = new SqlParameter("@BDMP_BRACHED", SqlDbType.NVarChar);
        param[16].Value = reldet.BDMP_BRACHED;
        param[17] = new SqlParameter("@EQUIPMENT_CODE", SqlDbType.NVarChar);
        param[17].Value = reldet.EQUIPMENT_CODE;
        param[18] = new SqlParameter("@ERROR_DESCRIPTION_CODE", SqlDbType.NVarChar);
        param[18].Value = reldet.ERROR_DESCRIPTION_CODE;
        param[19] = new SqlParameter("@BDMP_OTHERPROB_OWNER", SqlDbType.NVarChar);
        param[19].Value = reldet.BDMP_OTHERPROB_OWNER;
        param[20] = new SqlParameter("@OTHER_ERROR_DESCRIPTION", SqlDbType.NVarChar);
        param[20].Value = reldet.OTHER_ERROR_DESCRIPTION;

        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UPDATE_DETAILS_OF_BREAKDOWN", param);

        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }
    //Upload Files
    public dynamic UploadTemplate(HttpRequest httpRequest)
    {
        GetDocumentDetailsVM FDT;
        List<GetDocumentDetailsVM> FD = new List<GetDocumentDetailsVM>();
        if (httpRequest.Files.Count > 0)
        {
            String login = httpRequest.Params["login"];
            String typdoc = httpRequest.Params["CurrObj"];
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                var extension = Path.GetExtension(postedFile.FileName + "Brek");
                String filename = Guid.NewGuid() + extension;

                var filePath = HttpContext.Current.Server.MapPath("~/ContractFileRepository/" + filename);
                if (!FileInUse(filePath))
                {
                    postedFile.SaveAs(filePath);
                }

                FDT = new GetDocumentDetailsVM();
                FDT.DFDC_DDT_CODE = typdoc;
                FDT.DFDC_ACT_NAME = postedFile.FileName;
                FDT.DFDC_NAME = filename;
                FD.Add(FDT);
            }
            return new { Message = MessagesVM.UAD_UPLNOREC, data = FD };
        }
        else
            return new { Message = MessagesVM.UAD_UPLNOREC, data = (object)null };
    }
    // FileInUse Method
    public static bool FileInUse(string path)
    {
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            { return false; }
            //return false;
        }
        catch (IOException ex)
        { return true; }
    }
    // GET Locations--[dbo].[GET_DETAILS_OF_BREAKDOWN]
    public object getProblemOwner()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_HDM_OWNER");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }


    public object GetBreakDownDetails(breakdownSubmit reldet)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_DETAILS_OF_BREAKDOWN]");
        sp.Command.Parameters.Add("@REQID", reldet.REQID, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
}
