﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.IO;
using System.Collections.ObjectModel;

/// <summary>
/// Summary description for BreakDownMaintenanceServices
/// </summary>
public class BreakDownUptService
{
    SubSonic.StoredProcedure sp;

    // GET Locations
    //public object getLocations()
    //{
    //    DataSet ds = new DataSet();
    //    sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BDMP_GET_ALL_LOCATIONS");
    //    sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
    //    ds = sp.GetDataSet();
    //    return ds.Tables[0];
    //}
    //Get Impact
    public object getImpact()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "HDM_BIND_IMPACT");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Startus
    public object getStartus()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_USER_STATUS_BYROLE");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    // GET Problem Category
    public object getProblemCategory()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_HDM_FACILITY_DETAILS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    // GET Ticket Type
    public object getTicketType()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_BDMP_PRIORITY");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public object getRCAStatus()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetRCAStatus");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Equipment
    public object getEquipment(string BLDG)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BDMP_CREATE_GET_ASSET_GROUP_BY_LOCATION");
        sp.Command.Parameters.Add("@BLDG", BLDG, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getErrorDesc(string equipment)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY");
        sp.Command.Parameters.Add("@MNC_CODE", "MNTC", DbType.String);
        sp.Command.Parameters.Add("@SUB_CODE", equipment, DbType.String);
        sp.Command.Parameters.Add("@DSN_STA", 0, DbType.Int32);
        sp.Command.Parameters.Add("@DSN_CODE", "", DbType.String);
        sp.Command.Parameters.Add("@EMP_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    // GET Error Description
    public object getErrorDescription(ErrorDescription svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "HDM_GET_ERROR_DESCRIPTION");
        sp.Command.Parameters.Add("@TICKETTYPE", svm.TICKETTYPE, DbType.String);
        sp.Command.Parameters.Add("@CATEGORY", svm.CATEGORY, DbType.String);
        sp.Command.Parameters.Add("@LOC", svm.LOC, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Assign to
    public object getAssignto(getAssignto svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BREAKDOWN_BIND_INCHARGES");
        sp.Command.Parameters.Add("@LOC", svm.LOC, DbType.String);
        sp.Command.Parameters.Add("@TICKET_TYPE", svm.TICKET_TYPE, DbType.String);
        sp.Command.Parameters.Add("@EQUIPMENT", svm.EQUIPMENT, DbType.String);
       sp.Command.Parameters.Add("@ERRORDESCRIPTION", svm.ERRORDESCRIPTION, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Spare Parts
    public object getSpareParts(getAssignto svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPAREPARTS_DETAILS");
        sp.Command.Parameters.Add("@loc", svm.LOC, DbType.String);
        sp.Command.Parameters.Add("@Equipment", svm.EQUIPMENT, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Request Ids
    public object getBreakDownRqsts(getAssignto svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "VIEW_BDMP_STATUS");
        sp.Command.Parameters.Add("@loc", svm.LOC, DbType.String);
        sp.Command.Parameters.Add("@type", svm.EQUIPMENT, DbType.String);
        sp.Command.Parameters.Add("@aur_id", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // GET Urgency
    public object getUrgency()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "HDM_BIND_URGENCY");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public string GetTodaysReqCount()
    {
        string cnt;
        cnt = 0.ToString();
        var ds = new DataSet();
        var sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_TODAYS_REQUEST_ID");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ds = sp.GetDataSet();
        cnt = ds.Tables[0].Rows[0][0].ToString();
        return cnt;
    }
    public DateTime getoffsetdatetime(DateTime date)
    {
        object O = HttpContext.Current.Session["useroffset"];
        if (O is object)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
            {
                Temp = Temp.Insert(0, "+");
            }

            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            var startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            var _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    var tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }

            return _now;
        }
        else
        {
            return DateTime.Now;
        }
    }

    public string GetUserData(string aur_id)
    {
        string strusername;
        string spaceid;
        string phoneno;
        var ds = new DataSet();
        var sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "HRDGET_USER_DETAILS");
        sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String);
        ds = sp.GetDataSet();
        strusername = ds.Tables[0].Rows[0][0].ToString();
        spaceid = ds.Tables[0].Rows[0][2].ToString();
        phoneno = ds.Tables[0].Rows[0][3].ToString();
        return strusername+"~"+ spaceid+ "~"+ phoneno;
    }

    //Save RCA
    public object SaveRCA(breakdownRCA reldet)
    {        
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@RCA_DESCRIPTION", SqlDbType.NVarChar);
        param[0].Value = reldet.RCA_DESCRIPTION;
        param[1] = new SqlParameter("@RCA_STATUS", SqlDbType.NVarChar);
        param[1].Value = reldet.RCA_STATUS;
        param[2] = new SqlParameter("@RCA_BDMP_ID", SqlDbType.NVarChar);
        param[2].Value = reldet.RCA_BDMP_ID;
        param[3] = new SqlParameter("@RCAPATH", SqlDbType.NVarChar);
        param[3].Value = reldet.RCAPATH;

        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_InsertRCA", param);

        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    // GET Submit 
    public object save(breakdownUpdate reldet)
    {
        //string Main = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy");
        //string MaintenanceReq = Main + "-" + GetTodaysReqCount().ToString();
        string empDet= GetUserData(HttpContext.Current.Session["UID"].ToString());
        string impact = reldet.IMPACT == "FB" ? "FBD" : reldet.IMPACT == "PB" ? "PBD" : "Q";
        string MaintenanceReq = impact + "-" + GetTodaysReqCount().ToString();
        SqlParameter[] param = new SqlParameter[23];
        param[0] = new SqlParameter("@REQ_ID", SqlDbType.NVarChar);
        param[0].Value = MaintenanceReq;
        param[1] = new SqlParameter("@STATUS", SqlDbType.Int);
        param[1].Value = 1;
        param[2] = new SqlParameter("@LOC_CODE", SqlDbType.NVarChar);
        param[2].Value = reldet.LOC_CODE;
        param[3] = new SqlParameter("@SPC_ID", SqlDbType.NVarChar);
        param[3].Value = empDet.Split('~')[1];
        param[4] = new SqlParameter("@MNC_CODE", SqlDbType.NVarChar);
        param[4].Value = "MNTC";
        param[5] = new SqlParameter("@SUB_CAT", SqlDbType.NVarChar);
        param[5].Value = reldet.SUB_CAT;
        param[6] = new SqlParameter("@CHILD_CAT", SqlDbType.NVarChar);
        param[6].Value = reldet.ERROR_DESC;
        param[7] = new SqlParameter("@AST_LOC", SqlDbType.NVarChar);
        param[7].Value = "Others";
        param[8] = new SqlParameter("@REPEATCALL", SqlDbType.NVarChar);
        param[8].Value = "N";
        param[9] = new SqlParameter("@MOBILE", SqlDbType.NVarChar);
        param[9].Value = empDet.Split('~')[2];
        param[10] = new SqlParameter("@PROB_DESC", SqlDbType.NVarChar);
        param[10].Value = reldet.OTHER_DESC;//reldet.ERROR_DESC=="OT1"? "OTHER ISSUE":"";
        param[11] = new SqlParameter("@IMPACT", SqlDbType.NVarChar);
        param[11].Value = reldet.IMPACT;
        param[12] = new SqlParameter("@URGENCY", SqlDbType.NVarChar);
        param[12].Value = reldet.AAS_VED;
        param[13] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[13].Value = HttpContext.Current.Session["COMPANYID"];
        param[14] = new SqlParameter("@SER_CLAIM_AMT", SqlDbType.NVarChar);
        param[14].Value = "0";
        param[15] = new SqlParameter("@SER_EXP_AMT", SqlDbType.NVarChar);
        param[15].Value = "0";
        param[16] = new SqlParameter("@OtherDesc", SqlDbType.NVarChar);
        param[16].Value = reldet.OTHER_DESC;
        param[17] = new SqlParameter("@OtherProb", SqlDbType.NVarChar);
        param[17].Value = "";
        param[18] = new SqlParameter("@SER_REQ_TYPE", SqlDbType.Int);
        param[18].Value = 0;
        param[19] = new SqlParameter("@SER_CALL_LOG_BY", SqlDbType.NVarChar);
        param[19].Value = HttpContext.Current.Session["UID"]; //"Nothing";
        param[20] = new SqlParameter("@RAISED_BY", SqlDbType.NVarChar);
        param[20].Value = HttpContext.Current.Session["UID"]; 
        param[21] = new SqlParameter("@EMP_NAME", SqlDbType.NVarChar);
        param[21].Value = empDet.Split('~')[0];
        param[22] = new SqlParameter("@FACILITY", SqlDbType.NVarChar);
        param[22].Value = reldet.SUB_EQUIP;

        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_RAISE_HELP_DESK_REQUEST", param);

        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    // GET Submit 
    public object submit(breakdownSubmit reldet)
    {
        SqlParameter[] param = new SqlParameter[28];
        param[0] = new SqlParameter("@SPAREPARTDETAILS", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(reldet.spd);
        param[1] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        param[1].Value = reldet.REQID;
        param[2] = new SqlParameter("@STATUS", SqlDbType.NVarChar);
        param[2].Value = reldet.STATUS;
        param[3] = new SqlParameter("@INCHARE_ID", SqlDbType.NVarChar);
        param[3].Value = reldet.INCHARE_ID;
        param[4] = new SqlParameter("@ROOT_CAUSE", SqlDbType.NVarChar);
        param[4].Value = reldet.ROOT_CAUSE;
        param[5] = new SqlParameter("@PREVENTIVE_ACTION", SqlDbType.NVarChar);
        param[5].Value = reldet.PREVENTIVE_ACTION;
        param[6] = new SqlParameter("@CORRECTIVE_ACTION", SqlDbType.NVarChar);
        param[6].Value = reldet.CORRECTIVE_ACTION;
        param[7] = new SqlParameter("@PRODUCTION_IMPCT", SqlDbType.NVarChar);
        param[7].Value = reldet.PRODUCTION_IMPCT;
        param[8] = new SqlParameter("@PROBLEM_OWNER", SqlDbType.NVarChar);
        param[8].Value = reldet.PROBLEM_OWNER;
        param[9] = new SqlParameter("@REMARKS", SqlDbType.NVarChar);
        param[9].Value = reldet.REMARKS;
        param[10] = new SqlParameter("@ATTACHMENT", SqlDbType.NVarChar);
        param[10].Value = reldet.ATTACHMENT;
        param[11] = new SqlParameter("@USER", SqlDbType.NVarChar);
        param[11].Value = HttpContext.Current.Session["UID"];
        param[12] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        if (reldet.TODATE == null)
            param[12].Value = ' ';
        else
            param[12].Value = reldet.TODATE;
        param[13] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        if (reldet.FROMDATE == null)
            param[13].Value = ' ';
        else
            param[13].Value = reldet.FROMDATE;
        param[14] = new SqlParameter("@BDMP_DWNTIME_FACTOR", SqlDbType.NVarChar);
        param[14].Value = reldet.BDMP_DWNTIME_FACTOR;
        param[15] = new SqlParameter("@BDMP_DOWNTIME", SqlDbType.NVarChar);
        param[15].Value = reldet.BDMP_DOWNTIME;
        param[16] = new SqlParameter("@BDMP_BRACHED", SqlDbType.NVarChar);
        param[16].Value = reldet.BDMP_BRACHED;
        param[17] = new SqlParameter("@EQUIPMENT_CODE", SqlDbType.NVarChar);
        param[17].Value = reldet.EQUIPMENT_CODE;
        param[18] = new SqlParameter("@ERROR_DESCRIPTION_CODE", SqlDbType.NVarChar);
        param[18].Value = reldet.ERROR_DESCRIPTION_CODE;
        param[19] = new SqlParameter("@BDMP_OTHERPROB_OWNER", SqlDbType.NVarChar);
        param[19].Value = reldet.BDMP_OTHERPROB_OWNER;
        param[20] = new SqlParameter("@OTHER_ERROR_DESCRIPTION", SqlDbType.NVarChar);
        param[20].Value = reldet.OTHER_ERROR_DESCRIPTION;
        param[21] = new SqlParameter("@BDMP_SHIFT", SqlDbType.Int);
        param[21].Value = reldet.BDMP_SHIFT;
        param[22] = new SqlParameter("@LOC_CODE", SqlDbType.NVarChar);
        param[22].Value = reldet.LOC_CODE; 
        param[23] = new SqlParameter("@SER_PREV_ACT", SqlDbType.NVarChar);
        param[23].Value = reldet.SER_PREV_ACT;
        param[24] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
        param[24].Value = HttpContext.Current.Session["COMPANYID"];
        param[25] = new SqlParameter("@REQREMARKS", SqlDbType.NVarChar);
        param[25].Value = reldet.REQREMARKS;
        param[26] = new SqlParameter("@ADDI_BREACH", SqlDbType.Float);
        param[26].Value = reldet.ADDI_BREACH;
        param[27] = new SqlParameter("@PROBLEMCAT", SqlDbType.NVarChar);
        param[27].Value = reldet.PROBLEMCAT;

        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UPDATE_DETAILS_OF_BREAKDOWN", param);

        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }
    //Upload Files
    public dynamic UploadTemplate(HttpRequest httpRequest)
    {
        GetDocumentDetailsVM FDT;
        List<GetDocumentDetailsVM> FD = new List<GetDocumentDetailsVM>();
        if (httpRequest.Files.Count > 0)
        {
            String login = httpRequest.Params["login"];
            String typdoc = httpRequest.Params["CurrObj"];
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                var extension = Path.GetExtension(postedFile.FileName + "Brek");
                String filename = Guid.NewGuid() + extension;

                var filePath = HttpContext.Current.Server.MapPath("~/ContractFileRepository/" + filename);
                if (!FileInUse(filePath))
                {
                    postedFile.SaveAs(filePath);
                }

                FDT = new GetDocumentDetailsVM();
                FDT.DFDC_DDT_CODE = typdoc;
                FDT.DFDC_ACT_NAME = postedFile.FileName;
                FDT.DFDC_NAME = filename;
                FD.Add(FDT);
            }
            return new { Message = MessagesVM.UAD_UPLNOREC, data = FD };
        }
        else
            return new { Message = MessagesVM.UAD_UPLNOREC, data = (object)null };
    }
    // FileInUse Method
    public static bool FileInUse(string path)
    {
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            { return false; }
            //return false;
        }
        catch (IOException ex)
        { return true; }
    }
    // GET Locations--[dbo].[GET_DETAILS_OF_BREAKDOWN]
    public object getProblemOwner()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_HDM_OWNER");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }


    public object GetBreakDownDetails(breakdownSubmit reldet)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_DETAILS_OF_BREAKDOWN]");
        sp.Command.Parameters.Add("@REQID", reldet.REQID, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetDescrption(CustomizableRptVM CustRpt)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ITEMDESCRPTION");
        sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
        sp.Command.AddParameter("@AAS_AAT_CODE", CustRpt.AAS_AAT_CODE, DbType.String);
        sp.Command.AddParameter("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
        sp.Command.AddParameter("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //public object getRCAData(RCARpt CustRpt)
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[SP_GetRCADetail]");
    //        sp.Command.AddParameter("@RCA_BDMP_ID", CustRpt.REQID, DbType.String);            
    //        ds = sp.GetDataSet();
    //        if (ds.Tables.Count != 0)
    //            return new { Message = MessagesVM.UM_OK, data2 = ds.Tables[0] };
    //        else
    //            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }

    //}

    public object getRCAData(RCARpt CustRpt)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetRCADetail");
        sp.Command.Parameters.Add("@RCA_BDMP_ID", CustRpt.REQID, DbType.String);       
        ds = sp.GetDataSet();
        return ds.Tables[0];
        //if (ds.Tables.Count != 0)
        //    return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data2 = (HttpContext.Current.Session["TENANT"]) };
        //else
        //    return new { Message = MessagesVM.UM_NO_REC, data = (object)null, data2 = (object)null };
    }

    //Save RCA
    public object SaveDownTime(breakdowntime reldet)
    {
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@SER_REQ_ID", SqlDbType.NVarChar);
        param[0].Value = reldet.SER_REQ_ID;
        param[1] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        if (reldet.TODATE == null)
            param[1].Value = ' ';
        else
            param[1].Value = reldet.TODATE;
        param[2] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        if (reldet.FROMDATE == null)
            param[2].Value = ' ';
        else
            param[2].Value = reldet.FROMDATE;
        param[3] = new SqlParameter("@BDMP_DWNTIME_FACTOR", SqlDbType.NVarChar);
        param[3].Value = reldet.BDMP_DWNTIME_FACTOR;
        param[4] = new SqlParameter("@BDMP_DOWNTIME", SqlDbType.NVarChar);
        param[4].Value = reldet.BDMP_DOWNTIME;

        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_InsertDownTime", param);

        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    public object getDownTimeData(DownTimeRpt CustRpt)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetDownDetail");
        sp.Command.Parameters.Add("@SER_REQ_ID", CustRpt.REQID, DbType.String);
        ds = sp.GetDataSet();
        //return ds.Tables[0];
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data2 = ds.Tables[1] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null, data2 = (object)null };
    }

    public object DeleteDownTime(breaktime reldet)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@SER_DOWN_ID", SqlDbType.Int);
        param[0].Value = reldet.SER_DOWN_ID;    
        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_DeleteDownTime", param);
        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    public object DeleteRCA(breakRCA reldet)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@SR_NO", SqlDbType.Int);
        param[0].Value = reldet.SR_NO;
        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_DeleteRCA", param);
        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    public object getSpareData(SpareRpt CustRpt)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "spGetSparePartsTicketVise");
        sp.Command.Parameters.Add("@REQID", CustRpt.REQID, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object DeleteSpare(breakSpare reldet)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@BDMPD_SNO", SqlDbType.Int);
        param[0].Value = reldet.BDMPD_SNO;
        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "spDeleteSpareBDMP", param);
        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }
}
