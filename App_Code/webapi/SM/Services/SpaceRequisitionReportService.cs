﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpaceRequisitionReportService
{
    SubSonic.StoredProcedure sp;
    SpaceData Spcdt;
    DataSet ds;

    public List<SpaceData> GetSpaceReportDetails(SpaceReportDetails Spcdetails)
    {
        try
        {
            List<SpaceData> Spcdata = new List<SpaceData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_SPACE_REQUISITION_DETAILS");

            sp.Command.AddParameter("@FROM_DATE", Spcdetails.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", Spcdetails.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@REQTYP", Spcdetails.Request_Type, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", Spcdetails.CNP_NAME, DbType.Int32);
            sp.Command.AddParameter("@SEARCHVAL", Spcdetails.SearchValue, DbType.String);
            sp.Command.AddParameter("@PAGENUM", Spcdetails.PageNumber, DbType.String);
            sp.Command.AddParameter("@PAGESIZE", Spcdetails.PageSize, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    Spcdt = new SpaceData();
                    Spcdt.REQ_ID = reader["REQ_ID"].ToString();
                    Spcdt.REQ_DATE = (DateTime)reader["REQ_DATE"];
                    Spcdt.REQ_BY = reader["REQ_BY"].ToString();
                    Spcdt.FROM_DATE = Convert.ToDateTime(reader["FROM_DATE"].ToString());
                    Spcdt.TO_DATE = Convert.ToDateTime(reader["TO_DATE"].ToString());
                    Spcdt.CNY_NAME = reader["CNY_NAME"].ToString();
                    Spcdt.CTY_NAME = reader["CTY_NAME"].ToString();
                    Spcdt.LCM_NAME = reader["LCM_NAME"].ToString();
                    Spcdt.TWR_NAME = reader["TWR_NAME"].ToString();
                    Spcdt.FLR_NAME = reader["FLR_NAME"].ToString();
                    Spcdt.VER_NAME = reader["VER_NAME"].ToString();
                    Spcdt.STATUS = reader["STATUS"].ToString();
                    Spcdt.REQ_COUNT = reader["REQ_COUNT"].ToString();
                    Spcdt.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                    Spcdt.PREF_CODE = reader["PREF_CODE"].ToString();
                    Spcdt.REQ_REM = reader["REQ_REM"].ToString();
                    Spcdt.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    Spcdt.VER_CHE_CODE = reader["VER_CHE_CODE"].ToString();
                    Spcdata.Add(Spcdt);
                }
                reader.Close();
            }
            return Spcdata;
        }
        catch
        {
            throw;
        }

    }

    public object GetReqDetails(SpaceData data)
    {
        try
        {
            List<SpaceData> VMlist = new List<SpaceData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_REQDETAILS");
            sp.Command.AddParameter("@REQID", data.REQ_ID);
            sp.Command.AddParameter("@REQTYP", data.REQUEST_TYPE, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);

            using (IDataReader reader = sp.GetReader())
            {

                while (reader.Read())
                {
                    if (data.REQUEST_TYPE == "Vertical Requisition")
                    {
                        Spcdt = new SpaceData();
                        Spcdt.REQ_BY = reader["REQ_BY"].ToString();
                        Spcdt.SPACE_ID = reader["SPACE_ID"].ToString();
                        Spcdt.SPACE_TYPE = reader["SPACE_TYPE"].ToString();
                        Spcdt.SPACE_SUB_TYPE = reader["SPACE_SUB_TYPE"].ToString();
                        Spcdt.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                        VMlist.Add(Spcdt);
                    }
                    else if (data.REQUEST_TYPE == "Space Extension")
                    {
                        Spcdt = new SpaceData();
                        Spcdt.REQ_BY = reader["REQ_BY"].ToString();
                        Spcdt.SPACE_ID = reader["SPACE_ID"].ToString();
                        Spcdt.SPACE_TYPE = reader["SPACE_TYPE"].ToString();
                        Spcdt.SPACE_SUB_TYPE = reader["SPACE_SUB_TYPE"].ToString();
                        Spcdt.EXT_DT = reader["EXT_DT"].ToString();
                        Spcdt.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                        VMlist.Add(Spcdt);
                    }
                    else if (data.REQUEST_TYPE == "Space Requisition")
                    {
                        Spcdt = new SpaceData();
                        Spcdt.REQ_BY = reader["REQ_BY"].ToString();
                        Spcdt.SPACE_ID = reader["SPACE_ID"].ToString();
                        Spcdt.SPACE_TYPE = reader["SPACE_TYPE"].ToString();
                        Spcdt.SPACE_SUB_TYPE = reader["SPACE_SUB_TYPE"].ToString();
                        Spcdt.SHIFT = reader["SHIFT"].ToString();
                        Spcdt.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                        Spcdt.DESIGNATION = reader["DESIGNATION"].ToString();
                        Spcdt.PREF_CODE = reader["PREF_CODE"].ToString();
                        VMlist.Add(Spcdt);
                    }
                }
                reader.Close();
            }
            return new { Message = MessagesVM.UM_OK, data = VMlist };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public object GetSpaceChart(SpaceReportDetails Spcdetails)
    {
        try
        {
            List<SpaceData> Spcdata = new List<SpaceData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_REQCNT_BY_LOCATIONS");
            sp.Command.AddParameter("@FROM_DATE", Spcdetails.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", Spcdetails.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@REQTYP", Spcdetails.Request_Type, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", Spcdetails.CNP_NAME, DbType.Int32);
            ds = sp.GetDataSet();
            UtilityService userv = new UtilityService();
            DataTable dt = userv.GetInversedDataTable(ds.Tables[0], "LCM_NAME");
            var columns = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
            return new { Message = "", data = new { Details = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), Columnnames = columns } };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}
