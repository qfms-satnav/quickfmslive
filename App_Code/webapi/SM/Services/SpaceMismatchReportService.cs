﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpaceMismatchReportService
{
    SubSonic.StoredProcedure sp;
    List<MismatchData> Cust;
    MismatchData Custm;
    DataSet ds;

    public object GetMismatchObject(MismatchDetails Det)
    {
        try
        {
            DataTable dt = GetMismatchDetails_dump(Det);
            if (dt.Rows.Count != 0) { return new { Message = MessagesVM.SER_OK, data = dt }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetMismatchDetails_dump(MismatchDetails Det)
    {

        List<MismatchData> CData = new List<MismatchData>();
        DataTable DT = new DataTable();
        try
        {

            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_SPACE_MISMATCH_REPORT");
            return DT;
        }
        catch (Exception ex) { return DT; }
    }
}