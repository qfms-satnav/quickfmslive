﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class SUReportService
{
    SubSonic.StoredProcedure sp;
    List<SUReportModel> SUData;
    DataSet ds;

    public object BindGrid()
    {
        SUData = GetSUDataList();

        if (SUData.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUData };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<SUReportModel> GetSUDataList()
    {

        List<SUReportModel> rptByUserlst = new List<SUReportModel>();
        SUReportModel rptByUser;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SM_GET_SU_DETAILS");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByUser = new SUReportModel();
                rptByUser.FLOORNAME = sdr["FLOORNAME"].ToString();
                rptByUser.WORKSTATION = sdr["WORKSTATION"].ToString();
                rptByUser.WSODC = sdr["WSODC"].ToString();
                rptByUser.WSUNRES = sdr["WSUNRES"].ToString();
                rptByUser.WS_OCCUPIED = sdr["WS_OCCUPIED"].ToString();
                rptByUser.URES_OCCUPIED = sdr["URES_OCCUPIED"].ToString();
                rptByUser.ODC_OCCUPIED = sdr["ODC_OCCUPIED"].ToString();
                rptByUser.WS_VACANT_COUNT = sdr["WS_VACANT_COUNT"].ToString();
                rptByUser.WS_URES_VACANT = sdr["WS_URES_VACANT"].ToString();
                rptByUser.WS_ODC_VACANT = sdr["WS_ODC_VACANT"].ToString();
                rptByUser.CABIN = sdr["CABIN"].ToString();
                rptByUser.CBODC = sdr["CBODC"].ToString();
                rptByUser.CBUNRES = sdr["CBUNRES"].ToString();
                rptByUser.CB_OCCUPIED = sdr["CB_OCCUPIED"].ToString();
                rptByUser.CB_URES_OCCUPIED = sdr["CB_URES_OCCUPIED"].ToString();
                rptByUser.CB_ODC_OCCUPIED = sdr["CB_ODC_OCCUPIED"].ToString();
                rptByUser.CABIN_VACANT = sdr["CABIN_VACANT"].ToString();
                rptByUser.CB_URES_VACANT = sdr["CB_URES_VACANT"].ToString();
                rptByUser.CB_ODC_VACANT = sdr["CB_ODC_VACANT"].ToString();


                rptByUser.SU = sdr["SU"].ToString();
                rptByUserlst.Add(rptByUser);
            }
            sdr.Close();
        }
        if (rptByUserlst.Count != 0)
            return rptByUserlst;
        else
            return null;
    }


    public IEnumerable<SpaceTypesModel> GetSpaceTypes()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_SPACE_TYPES").GetReader();
        List<SpaceTypesModel> SpaceTypeList = new List<SpaceTypesModel>();
        while (reader.Read())
        {
            SpaceTypeList.Add(new SpaceTypesModel()
            {
                SPC_TYPE_CODE = reader.GetValue(0).ToString(),
                SPC_TYPE_NAME = reader.GetValue(1).ToString()
            });
        }
        reader.Close();
        return SpaceTypeList;
    }


    public object SUChartDetails(SUparams obj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SM_SUCHART");
        sp.Command.AddParameter("@SPC_TYPE", obj.SpaceType, DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

        UtilityService userv = new UtilityService();
        DataTable dt = userv.GetInversedDataTable(ds.Tables[0], "Floor");
        var columns = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
        if (arr.Length != 0)
            return new { Message = MessagesVM.UM_OK, data = new { rowData = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), colData = columns } };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

}