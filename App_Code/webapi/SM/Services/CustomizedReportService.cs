﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class CustomizedReportService
{
    SubSonic.StoredProcedure sp;
    List<CustomizedData> Cust;
    CustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(CustomizedDetails Det)
    {
        try
        {
            DataTable dt = GetCustomizedDetails_dump(Det);
            if (dt.Rows.Count != 0) { return new { Message = MessagesVM.SER_OK, data = dt }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetCustomizedDetails(CustomizedDetails Details)
    {
        try
        {
            //List<CustomizedData> CData = new List<CustomizedData>();
            //SqlParameter[] param = new SqlParameter[5];
            //param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            //param[0].Value = Details.Request_Type;
            //param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            //param[1].Value = UtilityService.ConvertToDataTable(Details.flrlst);
            //param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            //param[2].Value = HttpContext.Current.Session["UID"];
            //if (Details.CNP_NAME == null)
            //{
            //    param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            //    param[3].Value = HttpContext.Current.Session["COMPANYID"];

            //}
            //else
            //{
            //    param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            //    param[3].Value = Details.CNP_NAME;

            //}
            //param[4] = new SqlParameter("@Entity", SqlDbType.Structured);
            //param[4].Value = UtilityService.ConvertToDataTable(Details.enylist);

            //DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_CUSTOMIZED_REPORT_EXPORT", param);
            DataTable DT = new DataTable();
            DT = (DataTable)HttpContext.Current.Session["SpaceCustomizedGrid"];
            return DT;

        }
        catch
        {
            throw;
        }
    }

    public DataTable GetCustomizedDetails_dump(CustomizedDetails Det)
    {

        List<CustomizedData> CData = new List<CustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Det.Request_Type;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Det.flrlst);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[4].Value = Det.FromDate;
            param[5] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[5].Value = Det.ToDate;
            param[6] = new SqlParameter("@Entity", SqlDbType.Structured);
            param[6].Value = UtilityService.ConvertToDataTable(Det.enylist);
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_CUSTOMIZED_REPORT_EXPORT", param);
            HttpContext.Current.Session["SpaceCustomizedGrid"] = DT;
            return DT;
        }
        catch (Exception ex) { return DT; }
    }


    public DataTable GetCustomizedDetailsPivotExcel(CustomizedDetails Det)
    {
        List<CustomizedData> CData = new List<CustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Det.Request_Type;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Det.flrlst);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[4].Value = Det.FromDate;
            param[5] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[5].Value = Det.ToDate;
            param[6] = new SqlParameter("@Entity", SqlDbType.Structured);
            param[6].Value = UtilityService.ConvertToDataTable(Det.enylist);
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_CUSTOMIZED_REPORT_EXPORT_PIOVT", param);
            return DT;
        }
        catch
        {
            throw;
        }
    }

    public object SearchAllData(CustomizedDetails Det)
    {
        List<CustomizedData> CData = new List<CustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Det.Request_Type;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Det.flrlst);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[4].Value = Det.PageNumber;
            param[5] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[5].Value = Det.PageSize;
            param[6] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
            param[6].Value = Det.SearchValue;

            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_CUSTOMIZED_SEARCH_GRID", param);
            return DT;
        }
        catch (Exception ex) { return DT; }
    }
}