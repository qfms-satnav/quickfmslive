﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for Space_Subtype_ReportService
/// </summary>
public class Space_Subtype_ReportService
{
    public object GetSpaceDataDump(Space_Subtype_ReportModel Det)
    {
        try
        {
            DataTable dt = GetSpaceDataDump_details(Det);
            if (dt.Rows.Count != 0) 
            { 
                return new { Message = MessagesVM.SER_OK, data = dt };
            }
            else { 
                return new { Message = MessagesVM.SER_OK, data = (object)null }; 
            }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetSpaceDataDump_details(Space_Subtype_ReportModel Det)
    {

         DataTable DT = new DataTable();
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Det.flrlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Get_SeatSubType_Details", param);
            return DT;
      
    }

    public DataSet GetSpaceDataDump_dataset(Space_Subtype_ReportModel Det)
    {

        DataSet Ds = new DataSet();
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Det.flrlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        Ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Get_SeatSubType_Details_Report", param);
        return Ds;

    }
}