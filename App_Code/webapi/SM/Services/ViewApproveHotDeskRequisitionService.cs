﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for ViewApproveHotDeskRequisitionService
/// </summary>
public class ViewApproveHotDeskRequisitionService
{
    SubSonic.StoredProcedure sp;
    public object GetMyReqList(int id)
    {
        DataSet ds = new DataSet();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HOT_DESKING_REQUESTS");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.Command.AddParameter("@MODE", id, DbType.Int32);
            ds = sp.GetDataSet();
            return ds;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
    public object GetDetailsOnSelection(string Req_Id)
    {
        try
        {
            ViewVerticalRequisitionVM VerDetList = new ViewVerticalRequisitionVM();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "EDIT_VIEW_HOTDESK_REQUISITION");
            sp.Command.AddParameter("@REQ_ID", Req_Id, DbType.String);

            DataSet ds = sp.GetDataSet();
            MoveDetails MS = new MoveDetails();
            HotDeskRequisitionVM HVM = new HotDeskRequisitionVM();
            if (ds.Tables.Count > 0)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    MS.flrlst = new List<Floorlst>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        MS.flrlst.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["FLR_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["FLR_TWR_ID"]), LCM_CODE = Convert.ToString(dr["FLR_LOC_ID"]), CTY_CODE = Convert.ToString(dr["FLR_CTY_ID"]), CNY_CODE = Convert.ToString(dr["FLR_CNY_ID"]), ticked = true });
                    }
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        MS.FROM_DATE = dr["HD_FROM_DATE"].ToString();
                        MS.TO_DATE = dr["HD_TO_DATE"].ToString();
                    }
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    HVM.HDL = new List<HD_Dtails>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        HVM.HDL.Add(new HD_Dtails
                        {
                            SPC_ID = Convert.ToString(dr["HD_SPC_ID"]),
                            FROM_DATE = Convert.ToString(dr["HD_FROM_DATE"]),
                            TO_DATE = Convert.ToString(dr["HD_TO_DATE"]),
                            FROM_TIME = Convert.ToString(dr["HD_FROM_TIME"]),
                            TO_TIME = Convert.ToString(dr["HD_TO_TIME"]),
                            AUR_ID = Convert.ToString(dr["HD_AUR_ID"]),
                            VER_NAME = Convert.ToString(dr["VER_NAME"]),
                            Cost_Center_Name = Convert.ToString(dr["Cost_Center_Name"]),
                            //SPC_BDG_ID = Convert.ToString(dr["HD_LOC_ID"]),
                            //SPC_TWR_ID = Convert.ToString(dr["HD_TWR_ID"]),
                            //SPC_FLR_ID = Convert.ToString(dr["HD_FLR_ID"]),


                            ticked = true

                        });

                    }
                }
                else
                    HVM.HDL = new List<HD_Dtails>();


            }

            HotDeskRequisitionService HD = new HotDeskRequisitionService();
            List<HD_Dtails> spclst = HD.GetVacantSeats(MS);


            var common = from a1 in spclst
                         join a2 in HVM.HDL on a1.AUR_ID equals a2.AUR_ID into temp
                         from t1 in temp.DefaultIfEmpty()
                         select new { A1 = a1, A2 = t1 };

            foreach (var c in common)
            {
                if (c.A2 != null)
                {
                    c.A1.ticked = true;
                    c.A1.FROM_DATE = c.A2.FROM_DATE;
                    c.A1.TO_DATE = c.A2.TO_DATE;
                    c.A1.FROM_TIME = c.A2.FROM_TIME;
                    c.A1.TO_TIME = c.A2.TO_TIME;
                    c.A1.AUR_ID = c.A2.AUR_ID;
                    c.A1.VER_NAME = c.A2.VER_NAME;
                    c.A1.Cost_Center_Name = c.A2.Cost_Center_Name;
                    c.A1.Seat_Type = c.A2.Seat_Type;
                }

            }


            return new
            {
                Message = MessagesVM.UM_OK,
                data = new
                {
                    REM = ds.Tables[1].Rows[0]["HD_REM"],
                    L1_REM = ds.Tables[1].Rows[0]["HD_L1_REM"],
                    AUR_ID = ds.Tables[1].Rows[0]["HD_AUR_ID"],
                    L2_REM = ds.Tables[1].Rows[0]["HD_L2_REM"],
                    VER_NAME = ds.Tables[1].Rows[0]["VER_NAME"],
                    Cost_Center_Name = ds.Tables[1].Rows[0]["Cost_Center_Name"],
                    FROM_DATE = ds.Tables[1].Rows[0]["HD_FROM_DATE"],
                    TO_DATE = ds.Tables[1].Rows[0]["HD_TO_DATE"],
                    FROM_TIME = ds.Tables[1].Rows[0]["HD_FROM_TIME"],
                    TO_TIME = ds.Tables[1].Rows[0]["HD_TO_TIME"],
                    selectedfloors = MS.flrlst,
                    DETAILS = common.Select(cmn => cmn.A1).OrderByDescending(x => x.ticked).ToList()
                }
            };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }

    }

    public object ApproveRequests(HotDeskRequisitionVM hddet)
    {
        if (hddet.hdreqlst.Count != 0)
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@HD_REQ_ID", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(hddet.hdreqlst);
            param[1] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
            param[1].Value = hddet.ALLOCSTA;
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@L1_REM", SqlDbType.NVarChar);
            param[3].Value = hddet.L1_REM;
            param[4] = new SqlParameter("@L2_REM", SqlDbType.NVarChar);
            param[4].Value = hddet.L2_REM;

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HD_APPROVE_REQUISITIONS", param))
            {
                if (dr.Read())
                {
                    if ((int)dr["FLAG"] == 1)
                    {
                        if (hddet.ALLOCSTA == (int)RequestState.Approved)

                        {
                            SendMailHotDeskRequisition(hddet.hdreqlst[0].REQ_ID, hddet.ALLOCSTA);
                            return new { Message = MessagesVM.BSR_APPROVED, data = dr["MSG"].ToString() };

                        }
                        else
                            SendMailHotDeskRequisition(hddet.hdreqlst[0].REQ_ID, hddet.ALLOCSTA);
                        return new { Message = MessagesVM.BSR_REJECTED, data = dr["MSG"].ToString() };
                    }
                    else
                        return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
            }

            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        else
            return new { Message = MessagesVM.BSR_NODET, data = (object)null };
    }


    public void SendMailHotDeskRequisition(string REQUEST_ID, int FLAG)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_HOTDESK_REQUISITION_REQUEST");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.ExecuteScalar();
        }
        catch
        {

            throw;
        }
    }

    public object UpdateHdmFloor(HotDeskRequisitionVM hddet)
    {
        if (hddet.flrlst.Count != 0)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            param[0].Value = hddet.REQ_ID;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[1].Value = hddet.flrlst != null ? UtilityService.ConvertToDataTable(hddet.flrlst) : null;
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "[UPDATE_HOTDESKING_SEATS_FLOOR]", param);

            return new { Message = "Sucess", data = ds.Tables[0].Rows[0]["SUCESS"] };
        }
        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


}