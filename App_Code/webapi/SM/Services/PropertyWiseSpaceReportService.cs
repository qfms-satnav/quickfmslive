﻿using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using System;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for PropertyWiseSpaceReportService
/// </summary>
public class PropertyWiseSpaceReportService
{
    SubSonic.StoredProcedure sp;
    List<PropertySpacedata> psdata;
    PropertySpacedata Psd;
    DataSet ds;

    public object GetpeopertyspaceObject(PropertyWiseSpaceReport Details)
    {
        try
        {

            List<PropertySpacedata> pData = new List<PropertySpacedata>();
            SqlParameter[] param = new SqlParameter[3];
            //param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            //param[0].Value = UtilityService.ConvertToDataTable(Details.loclst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            //param[2] = new SqlParameter("@TOWER_LST", SqlDbType.DateTime);
            //param[2].Value = UtilityService.ConvertToDataTable(Details.twrlst);
            param[0] = new SqlParameter("@FLOOR_LST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Details.flrlst);
            if (Details.CNP_NAME == null)
            {
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[2].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                param[2].Value = Details.CNP_NAME;
            }
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PROPERTWISE_SPACE_REPORT", param);

            List<GridCols> gridcols = new List<GridCols>();
            GridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {
                gridcolscls = new GridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcols.Add(gridcolscls);

                exportcls = new ExportClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
            //psdata = GetPropertyspaceDetails(Det);
            //if (psdata.Count != 0) { return new { Message = MessagesVM.SER_OK, data = psdata }; }
            //else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    
}