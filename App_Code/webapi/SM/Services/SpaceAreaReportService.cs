﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class SpaceAreaReportService
{
    SubSonic.StoredProcedure sp;
    List<AreaReport> rptByUserlst;

    DataSet ds;
    public object GetReportByUserObject(CompanySpace CNPDet)
    {
        try
        {
            rptByUserlst = GetAreaReportByUserList(CNPDet);

            if (rptByUserlst.Count != 0)

                return new { Message = MessagesVM.UM_NO_REC, data = rptByUserlst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<AreaReport> GetAreaReportByUserList(CompanySpace CNPDet)
    {
        try
        {
            List<AreaReport> rptByUserlst = new List<AreaReport>();
            AreaReport rptByUser;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPC_AREA_REPORT");
            sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["UID"], DbType.String);

            if (CNPDet.CNP_NAME == null)
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            else
                sp.Command.AddParameter("@COMPANYID", CNPDet.CNP_NAME, DbType.Int32);
            sp.Command.AddParameter("@SEARCHVAL", CNPDet.SearchValue, DbType.String);
            sp.Command.AddParameter("@PAGENUM", CNPDet.PageNumber, DbType.String);
            sp.Command.AddParameter("@PAGESIZE", CNPDet.PageSize, DbType.String);

            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    rptByUser = new AreaReport();
                    rptByUser.CNY_NAME = sdr["CNY_NAME"].ToString();
                    rptByUser.CTY_NAME = sdr["CTY_NAME"].ToString();
                    rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                    rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                    rptByUser.FLR_NAME = sdr["FLR_NAME"].ToString();
                    rptByUser.FLR_AREA = (double)sdr["FLR_AREA"];
                    rptByUser.AISLE_AREA = (double)sdr["AISLE_AREA"];
                    rptByUser.COMMON_AREA = (double)sdr["COMMON_AREA"];
                    rptByUser.TOTAL_SPACES = sdr["TOTAL_SPACES"].ToString();
                    rptByUser.CHE_CODE = sdr["CHE_CODE"].ToString();
                    rptByUser.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                    rptByUserlst.Add(rptByUser);
                }
                sdr.Close();
            }
            if (rptByUserlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
                return rptByUserlst;
            else
                return new List<AreaReport>();
        }
        catch
        {
            throw;
        }

    }


    public object GetAreaChart(String ID)
    {
        try
        {
            List<SpaceData> area = new List<SpaceData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_AREA_BY_LOCATIONS");
            //sp.Command.AddParameter("@FROM_DATE", Spcdetails.FromDate, DbType.DateTime);
            //sp.Command.AddParameter("@TO_DATE", Spcdetails.ToDate, DbType.DateTime);
            //sp.Command.AddParameter("@REQTYP", Spcdetails.Request_Type, DbType.String);
            sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            if (ID == null)
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            else
                sp.Command.AddParameter("@COMPANYID", ID, DbType.Int32);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }

        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}