﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class CostReportService
{
    SubSonic.StoredProcedure sp;
    List<CostReportModel> rptByUserlst;
    //HDMReport_Bar_Graph rptBarGph;
    DataSet ds;
    public object GetCostReportObject(Company cstvm)
    {
        try
        {
            rptByUserlst = GetCostReportList(cstvm);

            if (rptByUserlst.Count != 0)
                return new { Message = MessagesVM.UM_NO_REC, data = rptByUserlst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<CostReportModel> GetCostReportList(Company cstvm)
    {
        try
        {
            List<CostReportModel> rptByUserlst = new List<CostReportModel>();
            CostReportModel rptByUser;

            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];

            if (cstvm.CNP_NAME == null)
            {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = cstvm.CNP_NAME;
            }
            param[2] = new SqlParameter("@CNYLST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(cstvm.cnylst);
            param[3] = new SqlParameter("@CTYLST", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(cstvm.ctylst);
            param[4] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(cstvm.loclst);
            param[5] = new SqlParameter("@TWRLST", SqlDbType.Structured);
            param[5].Value = UtilityService.ConvertToDataTable(cstvm.twrlst);
            param[6] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[6].Value = UtilityService.ConvertToDataTable(cstvm.flrlst);
            // sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_COST_REPORT", param);
            //sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SPACE_COST_REPORT", param))
            {
                while (reader.Read())
                {
                    rptByUser = new CostReportModel();
                    rptByUser.CNY_NAME = reader["CNY_NAME"].ToString();
                    rptByUser.CTY_NAME = reader["CTY_NAME"].ToString();
                    rptByUser.LCM_CODE = reader["LCM_CODE"].ToString();
                    rptByUser.LCM_NAME = reader["LCM_NAME"].ToString();
                    rptByUser.TWR_NAME = reader["TWR_NAME"].ToString();
                    rptByUser.FLR_NAME = reader["FLR_NAME"].ToString();
                    rptByUser.PE_NAME = reader["PE_NAME"].ToString();
                    rptByUser.CHE_NAME = reader["CHE_NAME"].ToString();
                    rptByUser.VER_NAME = reader["VER_NAME"].ToString();
                    rptByUser.DEP_NAME = reader["DEP_NAME"].ToString();
                    rptByUser.SHIFT = reader["SHIFT"].ToString();
                    rptByUser.SPC_TYPE = reader["SPC_TYPE"].ToString();
                    rptByUser.ALLOC_SEATS_COUNT = reader["ALLOC_SEATS_COUNT"].ToString();
                    rptByUser.UNITCOST = reader["UNITCOST"].ToString();
                    rptByUser.TOTAL_BUSS_UNITCOST = Convert.ToDouble(reader["TOTAL_BUSS_UNITCOST"]);
                    rptByUser.CNP_NAME = reader["CNP_NAME"].ToString();
                    rptByUserlst.Add(rptByUser);
                }
                reader.Close();
            }
            return rptByUserlst;
            // if (rptByUserlst.Count != 0)
            //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
            // return rptByUserlst;
            // else
            //   return null;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }


    public object GetCostChart()
    {
        try
        {
           
            List<CostReportModel> area = new List<CostReportModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_COST_CHART_REPORT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
           
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public object GetCostVerticalChart()
    {
        try
        {
            List<CostReportModel> area = new List<CostReportModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPACE_COST_VERTICAL_CHART_REPORT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}