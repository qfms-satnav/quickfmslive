﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for Planned_VS_Live_OccupancyService
/// </summary>
public class Planned_VS_Live_OccupancyService
{
    public DataTable GetData(List<LCMlst> lcm)
    {

        List<CustomizedData> CData = new List<CustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[1];
           
            param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(lcm);
            
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_DATA_PLANNED_VS_OCCUPIED", param);
            
            return DT;
        }
        catch (Exception ex) { return null; }
    }
}