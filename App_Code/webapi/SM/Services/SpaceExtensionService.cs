﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for SpaceExtensionService
/// </summary>
public class SpaceExtensionService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    List<SMS_SPACE_EXTENSION> SEDETLST;
    SMS_SPACE_EXTENSION SEDET;
    SpaceExtensionVM sevm;

    public object GetSpaceExtensionDetails(SpaceExtensionVM sevm)
    {
        try
        {
            SEDETLST = GetSpaceExtensionDetailsList(sevm);

            if (SEDETLST.Count != 0)
                return new { Message = MessagesVM.SER_OK, data = SEDETLST };
            else
                return new { Message = MessagesVM.SER_OK, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public List<SMS_SPACE_EXTENSION> GetSpaceExtensionDetailsList(SpaceExtensionVM sevm)
    {
        try
        {
            SEDETLST = new List<SMS_SPACE_EXTENSION>();
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(sevm.spceextn_flr_ver.selectedFloors);
            param[1] = new SqlParameter("@CCLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(sevm.spceextn_flr_ver.selectedCostcenters);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@From_Date", SqlDbType.DateTime);
            param[3].Value = sevm.spcextn.SSE_FROM_DATE;
            param[4] = new SqlParameter("@To_Date", SqlDbType.DateTime);
            param[4].Value = sevm.spcextn.SSE_TO_DATE;
            param[5] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[5].Value = HttpContext.Current.Session["COMPANYID"];
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SPACE_EXTENSION_DETAILS", param))
            {
                while (sdr.Read())
                {
                    SEDET = new SMS_SPACE_EXTENSION();
                    SEDET.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                    SEDET.SSED_SPC_ID = sdr["SSA_SPC_ID"].ToString();
                    SEDET.SPC_NAME = sdr["SPC_NAME"].ToString();
                    SEDET.SPC_FLR_ID = sdr["SPC_FLR_ID"].ToString();
                    SEDET.lat = sdr["lat"].ToString();
                    SEDET.lon = sdr["lon"].ToString();
                    SEDET.SSAD_FROM_DATE = Convert.ToDateTime(sdr["SSAD_FROM_DATE"]);
                    SEDET.SSAD_TO_DATE = Convert.ToDateTime(sdr["SSAD_TO_DATE"]);
                    SEDET.SSAD_FROM_TIME = Convert.ToDateTime(sdr["SSAD_FROM_TIME"]);
                    SEDET.SSAD_TO_TIME = Convert.ToDateTime(sdr["SSAD_TO_TIME"]);
                    SEDET.SSA_SPC_TYPE = sdr["SSA_SPC_TYPE"].ToString();
                    SEDET.SSA_SPC_SUB_TYPE = sdr["SSA_SPC_SUB_TYPE"].ToString();
                    SEDET.SH_NAME = sdr["SH_NAME"].ToString();
                    SEDET.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                    SEDET.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                    SEDET.SSED_AUR_ID = sdr["AUR_ID"].ToString();
                    SEDET.SSED_VER_NAME = sdr["VER_NAME"].ToString();
                    SEDET.EMP_DESIGNATION = sdr["DSN_AMT_TITLE"].ToString();
                    SEDET.STATUS = "Default";
                    SEDET.SSED_EXTN_DT = null;
                    SEDET.Selected = false;
                    SEDET.SSAD_ID = Convert.ToInt32(sdr["SSAD_ID"]);
                    SEDET.SSED_REQ_ID = sdr["SSED_REQ_ID"].ToString();
                    SEDETLST.Add(SEDET);
                }
                sdr.Close();
            }
            return SEDETLST;
        }
        catch
        {
            throw;
        }
    }

    public object RaiseRequest(SpaceExtensionVM SEVM)
    {
        try
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[7];
            List<SMS_SPACE_EXTENSION_DETAILS> copiedList = new List<SMS_SPACE_EXTENSION_DETAILS>();
            if (SEVM.FLAG == 16 || SEVM.FLAG == 32 || SEVM.FLAG == 64 || SEVM.FLAG == 1 || SEVM.FLAG == 4)
            {
                copiedList = SEVM.spceextn_flr_ver.selectedSpaces.Where(space => space.STACHECK == 4).ToList();
            }
            dt = CheckSpaceIDsBeforeRequest(UtilityService.ConvertToDataTable(copiedList));
            if (dt.Rows.Count == 0)
            {
                List<SMS_SPACE_EXTENSION_VM> spcextn_lst = new List<SMS_SPACE_EXTENSION_VM>();
                spcextn_lst.Add(SEVM.spcextn);

                param[0] = new SqlParameter("@SPC_EXTN_VM_LST", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(spcextn_lst);

                param[1] = new SqlParameter("@SPC_EXTN_DETLS_LST", SqlDbType.Structured);
                param[1].Value = UtilityService.ConvertToDataTable(SEVM.spceextn_flr_ver.selectedSpaces);

                param[2] = new SqlParameter("@SPC_EXTN_SUB_DETLS_LST", SqlDbType.Structured);
                param[2].Value = UtilityService.ConvertToDataTable(SEVM.spceextn_flr_ver.selectedFloors);

                param[3] = new SqlParameter("@SPC_EXTN_CHILD_DETLS_LST", SqlDbType.Structured);
                param[3].Value = UtilityService.ConvertToDataTable(SEVM.spceextn_flr_ver.selectedCostcenters);

                param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[4].Value = HttpContext.Current.Session["UID"];

                param[5] = new SqlParameter("@FLAG", SqlDbType.Int);
                param[5].Value = SEVM.FLAG;

                param[6] = new SqlParameter("@COMPANYID", SqlDbType.Int);
                param[6].Value = HttpContext.Current.Session["COMPANYID"];
                using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_UPDATE_SSA_AND_INSERT_SPACE_EXTENSION", param))
                {
                    if (dr.Read())
                    {
                        if ((int)dr["FLAG"] == 1)
                        {
                            string RetMessage = string.Empty;
                            RequestState sta = (RequestState)SEVM.FLAG;
                            switch (sta)
                            {
                                case RequestState.Added:
                                    RetMessage = MessagesVM.SPC_EXT_REQ_INSERTED;
                                    break;
                                case RequestState.Modified:
                                    RetMessage = MessagesVM.SPC_EXT_REQ_UPDATED;
                                    break;
                                case RequestState.Canceled:
                                    RetMessage = MessagesVM.SPC_EXT_REQ_CANCELED;
                                    break;
                                case RequestState.Approved:
                                    RetMessage = MessagesVM.SPC_EXT_REQ_APPROVED;
                                    break;
                                case RequestState.Rejected:
                                    RetMessage = MessagesVM.SPC_EXT_REQ_REJECTED;
                                    break;

                            }
                            SendMail(dr["REQID"].ToString(), SEVM.FLAG);
                            return new { Message = RetMessage + " For : " + dr["REQID"].ToString(), data = SEVM, STATUS = "SUCCESS" };
                        }
                        else
                        {
                            return new { Message = dr["REQID"].ToString(), data = (object)null };
                        }
                    }
                    dr.Close();
                }

            }

            return new { Message = MessagesVM.SPC_UNDER_REQUISITION, data = dt, STATUS = "FAIL" };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public DataTable CheckIfSpaceisAllocatedOrNot(SMS_SPACE_EXTENSION SPM)
    {
        try
        {
            DataTable dt;
            SqlParameter[] param = new SqlParameter[6];
            SMS_SPACE_EXTENSION SSED = new SMS_SPACE_EXTENSION();

            param[0] = new SqlParameter("@SSAD_SRN_REQ_ID", SqlDbType.NVarChar);
            param[0].Value = SPM.SSAD_SRN_REQ_ID;

            param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
            param[1].Value = SPM.SSAD_TO_DATE;

            param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
            param[2].Value = SPM.SSED_EXTN_DT;

            param[3] = new SqlParameter("@SPC_ID", SqlDbType.NVarChar);
            param[3].Value = SPM.SSED_SPC_ID;

            param[4] = new SqlParameter("@FTIME", SqlDbType.DateTime);
            param[4].Value = SPM.SSAD_FROM_TIME;

            param[5] = new SqlParameter("@TTIME", SqlDbType.DateTime);
            param[5].Value = SPM.SSAD_TO_TIME;

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_CHECK_SPACE_IDS_BEFORE_REQUEST", param);
            dt = ds.Tables[0];
            return dt;
        }
        catch
        {
            throw;
        }
    }

    public DataTable CheckSpaceIDsBeforeRequest(DataTable dtTable)
    {
        try
        {
            DataTable dt = dtTable;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SPC_EXTN_LST", SqlDbType.Structured);
            param[0].Value = dtTable;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_CHECK_SPACE_IDS_BEFORE_REQUEST_TVP_CURSOR", param);
            dt = ds.Tables[0];
            return dt;
        }
        catch
        {
            throw;
        }
    }

    public object ApproveAndRejectRequests(SpaceExtensionVM spcextndet)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@SPC_EXTN_VM_LST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(spcextndet.spcextn_lst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[2].Value = spcextndet.FLAG;
            param[3] = new SqlParameter("@REMARKS", SqlDbType.NVarChar);
            param[3].Value = spcextndet.Remarks;
            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_APPROVE_REJECT_SPACE_EXTENSION_REQUISITIONS", param))
            {
                if (dr.Read())
                {
                    if (spcextndet.FLAG == 32)
                    {
                        SendBulkApprRejMails(spcextndet);
                        return new { Message = MessagesVM.SPC_EXT_REQ_APPROVED };
                    }
                    else if (spcextndet.FLAG == 64)
                    {
                        SendBulkApprRejMails(spcextndet);
                        return new { Message = MessagesVM.SPC_EXT_REQ_REJECTED };
                    }
                    else
                        return new { Message = dr["MSG"].ToString(), data = (object)null };
                }

                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }

    }

    public void SendMail(string REQUEST_ID, int FLAG)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_EXTENSION_RAISE_REQUEST");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }

    public void SendBulkApprRejMails(SpaceExtensionVM spcextndet)
    {
        try
        {
            foreach (SMS_SPACE_EXTENSION_VM spc in spcextndet.spcextn_lst)
            {
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_EXTENSION_RAISE_REQUEST");
                sp.Command.AddParameter("@REQID", spc.SSE_REQ_ID, DbType.String);
                sp.Command.AddParameter("@STATUS", spcextndet.FLAG, DbType.Int32);
                sp.ExecuteScalar();
            }
        }
        catch
        {
            throw;
        }
    }

}