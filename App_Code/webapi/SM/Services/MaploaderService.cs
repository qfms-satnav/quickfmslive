﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Newtonsoft.Json;

public class MaploaderService
{
    SubSonic.StoredProcedure sp;

    //public string ReadFile(string floorCode, out bool isError)
    //{
    //    isError = true;
    //    try
    //    {
    //        var fileContent = "";
    //        var jsonFileServer = string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["JsonHostingURL"]) ? "" : System.Configuration.ConfigurationManager.AppSettings["JsonHostingURL"].ToString();

    //        //var webRequest = WebRequest.Create(jsonFileServer + "/" + floorCode + ".json");
    //        var webRequest = WebRequest.Create(jsonFileServer + "/" + (HttpContext.Current.Session["TENANT"]) + "/" + floorCode + ".json");
    //        using (var response = webRequest.GetResponse())
    //        using (var content = response.GetResponseStream())
    //        using (var reader = new StreamReader(content))
    //        {
    //            fileContent = reader.ReadToEnd();
    //            isError = false;
    //        }
    //        return fileContent;
    //    }
    //    catch (Exception ex)
    //    {
    //        return "";
    //    }
    //}by vinod
    public string ReadFile(string floorCode, out bool isError)
    {
        isError = true;

        try
        {
            var fileContent = string.Empty;
            var jsonFileServer = System.Configuration.ConfigurationManager.AppSettings["JsonHostingURL"];

            if (string.IsNullOrWhiteSpace(jsonFileServer))
            {

                return fileContent;
            }

            var tenant = HttpContext.Current.Session["TENANT"].ToString();
            if (string.IsNullOrWhiteSpace(tenant))
            {

                return fileContent;
            }

            var url = jsonFileServer + "/" + tenant + "/" + floorCode + ".json";
            var webRequest = WebRequest.Create(url);
            using (var response = webRequest.GetResponse())
            using (var content = response.GetResponseStream())
            using (var reader = new StreamReader(content))
            {
                fileContent = reader.ReadToEnd();
                isError = false;
            }

            return fileContent;
        }
        catch (WebException ex)
        {
            return string.Empty;
        }
        catch (IOException ex)
        {
            return string.Empty;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }

    }

    public object GetFloorLst()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GETUSERFLOORLIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetImageList(string Image)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SELETOWER_IMAGE_LIST");
        sp.Command.Parameters.Add("@TWI_CODE", Image, DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetEmpSeatDtlsHotDesk(string svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GETSPACEFROMEMPID");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@SELECT_SPC_ID", svm, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data1 = ds.Tables[1], data2 = ds.Tables[2] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GET_VERTICAL_WISE_ALLOCATIONS(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_VERTICAL_WISE_ALLOCATIONS");
            sp.Command.AddParameter("@FLR_CODE", svm.flr_code, DbType.String);
            ds = sp.GetDataSet();
            List<object> verticals = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();
            return new { data = verticals, data1 = ds.Tables[0], data2 = ds.Tables[1] };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object validateuserspacedetails()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_RECENT_BOOKING_LIST_BY_AUR_ID");
        sp.Command.Parameters.Add("@USER", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object approvalallocateSeats(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {

        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        List<SPACE_ALLOC_DETAILS_TIME> spcdet1 = new List<SPACE_ALLOC_DETAILS_TIME>();
        SPACE_ALLOC_DETAILS_TIME spc1;

        using (SqlDataReader sdr1 = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_MANAGER_APPROVAL_MAP", param))
        {
            while (sdr1.Read())
            {
                spc1 = new SPACE_ALLOC_DETAILS_TIME();
                spc1.SPC_ID = sdr1["MSG"].ToString();
                spcdet1.Add(spc1);
            }
        }
        if (spcdet1.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcdet1 };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }



    public object GetMapItems(Space_mapVM svm)
    {
        var fileContent = "";
        DataSet ds = new DataSet();
        if (svm.key_value == 1)
        {
            var isError = false;
            fileContent = ReadFile(svm.flr_code, out isError);

            if (isError)
            {
                sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS_View_In_Map");
                sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                ds = sp.GetDataSet();
                Hashtable sendData = new Hashtable();

                foreach (DataRow drIn in ds.Tables[1].Rows)
                {
                    sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                }

                return new { mapDetails = ds.Tables[0], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
            }
            else
            {
                //get geomstring using FileSystem method
                sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS_View_In_Map_GMR_V1");
                sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                ds = sp.GetDataSet();
                Hashtable sendData = new Hashtable();

                foreach (DataRow drIn in ds.Tables[1].Rows)
                {
                    sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                }


                var geomObject = JsonConvert.DeserializeObject<List<GeomStringObject>>(fileContent);
                var spaceItems = GeomRelatedQuery.GetSpaceItemByFloorCode(svm.flr_code);
                var spaceTypeItems = GeomRelatedQuery.GetSpaceTypes();

                var list1 = new List<GeomStringObject>();

                list1 = geomObject.Where(e => !e.Layer.Contains("_G")).ToList();

                var loadLayers = 0;

                var queryTableZero = (from lst1 in list1
                                      join spcLeft in spaceItems on lst1.SpaceId equals spcLeft.SPC_ID into spcDflt
                                      from spc in spcDflt.DefaultIfEmpty()
                                      join stLeft in spaceTypeItems on (spc == null ? null : spc.SPC_LAYER) equals stLeft.SPC_TYPE_CODE into stDflt
                                      from spcType in stDflt.DefaultIfEmpty()
                                      select (new
                                      {
                                          ID = lst1.Id,
                                          SPACE_ID = lst1.SpaceId ?? "",
                                          Wkt = lst1.GeomString ?? "",
                                          flr_id = lst1.FloorId ?? "",
                                          layer = lst1.Layer ?? "",
                                          SEATTYPE = spc == null ? "" : (spc.SPACE_TYPE ?? ""),
                                          SPC_SUB_TYPE = spc == null ? "" : spc.SPC_SUB_TYPE ?? "",
                                          STAID = 1,
                                          LOADLAYERS = loadLayers,
                                          SPC_TYPE_STATUS = spcType == null ? 0 : spcType.SPC_TYPE_STATUS
                                      })).ToList();

                var resultTableZero = queryTableZero.OrderBy(e => e.SPACE_ID)
                    .ThenByDescending(e => e.layer == "CHA" ? 0 : (e.SPC_TYPE_STATUS == 4 ? 0 : (e.SPC_TYPE_STATUS == 2 ? 1 : 0)))
                    .ToList();

                return new { mapDetails = resultTableZero, FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
            }
        }
        else
        {
            var isError = false;
            fileContent = ReadFile(svm.flr_code, out isError);
            if (isError)
            {
                sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS");
                sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                ds = sp.GetDataSet();
                Hashtable sendData = new Hashtable();

                foreach (DataRow drIn in ds.Tables[2].Rows)
                {
                    sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                }

                return new { mapDetails = ds.Tables[0], mapDetails2 = ds.Tables[1], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[3] };
            }
            else
            {
                //get geomstring using FileSystem method

                sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS_GMR_V1");
                sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                ds = sp.GetDataSet();
                Hashtable sendData = new Hashtable();

                foreach (DataRow drIn in ds.Tables[2].Rows)
                {
                    sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                }

                var geomObject = JsonConvert.DeserializeObject<List<GeomStringObject>>(fileContent);
                var spaceItems = GeomRelatedQuery.GetSpaceItemByFloorCode(svm.flr_code);
                var spaceTypeItems = GeomRelatedQuery.GetSpaceTypes();

                var areaAndChairCountDt = ds.Tables[4];
                Int64 area = 0;
                int chairCount = 0;
                int loadLayers = 0;

                if (areaAndChairCountDt != null && areaAndChairCountDt.Rows.Count > 0)
                {
                    Int64.TryParse((areaAndChairCountDt.Rows[0]["area"] == DBNull.Value ? "" : areaAndChairCountDt.Rows[0]["area"].ToString()), out area);
                    int.TryParse((areaAndChairCountDt.Rows[0]["chairCount"] == DBNull.Value ? "" : areaAndChairCountDt.Rows[0]["chairCount"].ToString()), out chairCount);
                }

                var list1 = new List<GeomStringObject>();
                var list2 = new List<GeomStringObject>();

                if (geomObject != null)
                {
                    if (area > 30000 && chairCount > 100)
                    {
                        list1 = geomObject.Where(e => e.Layer != "CHA" && e.Layer != "FUR" && !e.Layer.Contains("_G")).ToList();
                        list2 = geomObject.Where(e => e.Layer == "CHA" || e.Layer == "FUR").ToList();
                        loadLayers = 1;
                    }
                    else
                    {
                        list1 = geomObject.Where(e => !e.Layer.Contains("_G")).ToList();
                    }
                }

                var queryTableZero = (from lst1 in list1
                                      join spcLeft in spaceItems on lst1.SpaceId equals spcLeft.SPC_ID into spcDflt
                                      from spc in spcDflt.DefaultIfEmpty()
                                      join stLeft in spaceTypeItems on (spc == null ? null : spc.SPC_LAYER) equals stLeft.SPC_TYPE_CODE into stDflt
                                      from spcType in stDflt.DefaultIfEmpty()
                                      select (new
                                      {
                                          ID = lst1.Id,
                                          SPACE_ID = lst1.SpaceId ?? "",
                                          Wkt = lst1.GeomString ?? "",
                                          flr_id = lst1.FloorId ?? "",
                                          layer = lst1.Layer ?? "",
                                          SEATTYPE = spc == null ? "" : (spc.SPACE_TYPE ?? ""),
                                          SPC_SUB_TYPE = spc == null ? "" : spc.SPC_SUB_TYPE ?? "",
                                          STAID = 1,
                                          LOADLAYERS = loadLayers,
                                          SPC_TYPE_STATUS = spcType == null ? 0 : spcType.SPC_TYPE_STATUS
                                      })).ToList();

                var resultTableZero = queryTableZero.OrderBy(e => e.SPACE_ID)
                    .ThenByDescending(e => e.layer == "CHA" ? 0 : (e.SPC_TYPE_STATUS == 4 ? 0 : (e.SPC_TYPE_STATUS == 2 ? 1 : 0)))
                    .ToList();

                var queryTableOne = (from lst2 in list2
                                     join spcLeft in spaceItems on lst2.SpaceId equals spcLeft.SPC_ID into spcDflt
                                     from spc in spcDflt.DefaultIfEmpty()
                                     select (new
                                     {
                                         ID = lst2.Id,
                                         SPACE_ID = lst2.SpaceId ?? "",
                                         Wkt = lst2.GeomString ?? "",
                                         flr_id = lst2.FloorId ?? "",
                                         layer = lst2.Layer ?? "",
                                         SEATTYPE = spc == null ? "" : spc.SPACE_TYPE ?? "",
                                         SPC_SUB_TYPE = spc == null ? "" : spc.SPC_SUB_TYPE ?? "",
                                         STAID = 1,
                                         LOADLAYERS = loadLayers
                                     })).ToList();

                return new { mapDetails = resultTableZero, mapDetails2 = queryTableOne, FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[3] };
            }
        }

    }

    public object GETEMPLOYEETYPE()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "EMPLOY_TYPE_DATA");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetMarkers(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_MARKER_ALL");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@MODE", 1, DbType.Int32);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GradeAllocStatus()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GRADE_ALLOC_STATUS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object SeatRestriction(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "Seat_Restrictions_Checking");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@Seat_type", svm.category, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object SEAT_RESTRICTION_BOOKING(SPACE_ALLOC_DETAILS_TIME svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SEAT_RESTRICTION_BOOKING");
        sp.Command.Parameters.Add("@FROMDATE", svm.FROM_DATE, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.SPC_ID, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getSpaceDaysRestriction()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "RESTRICT_DAYS_COUNT");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object getVer_And_CC(AUR_DETAILS data)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_VERTICAL_COSTCENTER_BY_AUR_ID");
        sp.Command.Parameters.Add("@AUR_ID", data.AUR_ID, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetCornerLables(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOOR_CORNERS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetLegendsCount(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@FLAG", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@SH_CODE", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }

    public object GetLegendsSummary(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP_LEGEND");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        IDataReader dr = sp.GetReader();
        LEGEND_SUMMARY ls = new LEGEND_SUMMARY();
        if (dr.Read())
        {
            if (ds.Tables[0].Columns.Contains("FULLYOCCUPIED") && ds.Tables[0].Columns.Contains("PARTIALLYOCCUPIED"))
            {
                ls.ALLOCATEDVER = (int)dr["ALLOCATEDVER"];
                ls.ALLOCATEDCST = (int)dr["ALLOCATEDCST"];
                ls.TOTAL = (int)dr["TOTAL"];
                ls.VACANT = (int)dr["VACANT"];
                ls.OCCUPIED = (int)dr["OCCUPIED"];
                ls.OVERLOAD = (int)dr["OVERLOAD"];
                ls.BLOCKED = (int)dr["BLOCKED"];
                ls.FULLYOCCUPIED = Convert.ToInt32(ds.Tables[0].Rows[0]["FULLYOCCUPIED"].ToString());
                ls.PARTIALLYOCCUPIED = Convert.ToInt32(ds.Tables[0].Rows[0]["PARTIALLYOCCUPIED"].ToString());
                ls.ALLOCUNUSED = ds.Tables[1].Rows[0]["ALLOC_UNUSED"].ToString();
                ls.OCCUP_EMP = ds.Tables[2].Rows[0]["OCCUP_EMP"].ToString();
                ls.LEAVE = Convert.ToInt32(ds.Tables[3].Rows[0]["LEAVE"].ToString());
                ls.EMP_COUNT = Convert.ToInt32(ds.Tables[4].Rows[0]["EMP_COUNT"].ToString());
                ls.ALLOC_COUNT = Convert.ToInt32(ds.Tables[5].Rows[0]["ALLOC_COUNT"].ToString());
                ls.ispartial = true;
            }
            else
            {
                ls.ALLOCATEDVER = (int)dr["ALLOCATEDVER"];
                ls.ALLOCATEDCST = (int)dr["ALLOCATEDCST"];
                ls.TOTAL = (int)dr["TOTAL"];
                ls.VACANT = (int)dr["VACANT"];
                ls.OCCUPIED = (int)dr["OCCUPIED"];
                ls.OVERLOAD = (int)dr["OVERLOAD"];
                ls.BLOCKED = (int)dr["BLOCKED"];
                ls.ALLOCUNUSED = ds.Tables[1].Rows[0]["ALLOC_UNUSED"].ToString();
                ls.OCCUP_EMP = ds.Tables[2].Rows[0]["OCCUP_EMP"].ToString();
                ls.LEAVE = Convert.ToInt32(ds.Tables[3].Rows[0]["LEAVE"].ToString());
                ls.EMP_COUNT = Convert.ToInt32(ds.Tables[4].Rows[0]["EMP_COUNT"].ToString());
                ls.ALLOC_COUNT = Convert.ToInt32(ds.Tables[5].Rows[0]["ALLOC_COUNT"].ToString());
                ls.ispartial = false;
            }
        }
        return ls;
    }

    public object GetSpaceDetailsBySPCID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYSPCID");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.CatValue, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbyItem()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_ITEM_SM");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbySubItem(Space_mapVM param)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_SUBITEM_SM");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEMID", param.Item, DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetSpaceDetailsBySUBITEM(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DATA_MAP_SM");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@SUB_ITEM", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();

        if (svm.Item == "login")
        {

            //ActiveDirectoryHelper adhelper = new ActiveDirectoryHelper();
            //List<ADUserDetail> useradlist = adhelper.GetUserslist();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AD_DATA_LIVE");
            DataSet DS1 = sp.GetDataSet();

            List<AD_DATA_VM> useradlist = DS1.Tables[0].AsEnumerable()
                  .Select(row => new AD_DATA_VM
                  {
                      USR_ID = row.Field<string>(0),
                      LOG_IN_TIME = row.Field<string>(1),
                      LOG_OUT_TIME = row.Field<string>(2)
                  }).ToList();

            List<Space_MapEmployeeVM> empmaplist = ds.Tables[0].AsEnumerable()
              .Select(row => new Space_MapEmployeeVM
              {
                  SPC_ID = row.Field<string>(0),
                  x = row.Field<double>(1).ToString(),
                  y = row.Field<double>(2).ToString(),
                  SPC_FLR_ID = row.Field<string>(3),
                  SPC_LAYER = row.Field<string>(4),
                  SPC_DESC = row.Field<string>(5),
                  STATUS = row.Field<int>(6).ToString(),
                  AD_ID = row.Field<string>(7)
              }).ToList();

            var list3 = (from Item1 in empmaplist
                         join Item2 in useradlist
                         on Item1.AD_ID equals Item2.USR_ID // join on some property
                         into grouping
                         from Item2 in grouping.DefaultIfEmpty()
                             //where Item2 != null
                         select new { Item1, Item2 }).ToList();
            return list3;
        }

        return ds;
    }

    public object GetSpaceDetailsByREQID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYREQID");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.category, DbType.String);
        sp.Command.Parameters.Add("@REQ_ID", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object ReleaseSelectedseat(SPACE_REL_DETAILS reldet)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@REL_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(reldet.sad);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@RELTYPE", SqlDbType.NVarChar);
        param[2].Value = reldet.reltype;
        param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];

        int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_SPACE_REL_MAP", param);

        if (RETVAL == 1)
            return new { Message = MessagesVM.UM_OK, data = reldet.reltype };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };


    }

    public object ActivateSpaces(CLS_INACTIVE_SPACE SpcAct)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@SPACE_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(SpcAct.SPC_LST);
            param[1] = new SqlParameter("@FLR_ID", SqlDbType.NVarChar);
            param[1].Value = SpcAct.FLR_CODE;
            param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            int RETVAL = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SMS_INSERT_SPACES", param);

            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };

        }
        catch (SqlException)
        {
            throw;
        }
    }

    public object InactiveSeats(CLS_INACTIVE_SPACE Inac)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@SPACE_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Inac.SPC_LST);
        param[1] = new SqlParameter("@FLR_ID", SqlDbType.NVarChar);
        param[1].Value = Inac.FLR_CODE;
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        int INAC = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_INACTIVE_SEATS", param);

        if (INAC == 1)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    public List<SPACEDETAILS> InactiveSpacesFromFloorMaps(Space_mapVM svm)
    {
        try
        {
            List<SPACEDETAILS> FlrMapslst = new List<SPACEDETAILS>();
            SPACEDETAILS ls;
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_INACTIVE_SEATS");
            sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            IDataReader dr = sp.GetReader();
            while (dr.Read())
            {
                ls = new SPACEDETAILS();
                ls.SPACE_ID = dr["SPACE_ID"].ToString();
                ls.SPC_TYPE = dr["SPC_TYPE_NAME"].ToString();
                ls.SPC_SUB_TYPE = dr["SST_NAME"].ToString();
                FlrMapslst.Add(ls);
            }
            dr.Close();
            return FlrMapslst;
        }
        catch
        {
            throw;
        }
    }

    public object GetEmpDetails(Space_mapVM svm)
    {
        if (svm.subitem != null && svm.subitem != "")
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_EMP_DETAILS_FLRID");
            sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
            sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
            sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
            sp.Command.Parameters.Add("@VER_CODE", svm.Item, DbType.String);
            sp.Command.Parameters.Add("@COST_CENTER_CODE", svm.subitem, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            return new { Message = MessagesVM.SelectCST, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.SelectCST, data = (object)null };
    }

    public object GetAllocEmpDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_ALLOC_EMP_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //public object AllocateSeats(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    //{
    //    int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
    //    var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

    //    SqlParameter[] param = new SqlParameter[3];
    //    param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
    //    param[0].Value = UtilityService.ConvertToDataTable(details);
    //    param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
    //    param[1].Value = HttpContext.Current.Session["UID"];
    //    param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
    //    param[2].Value = HttpContext.Current.Session["COMPANYID"];

    //    List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();
    //    SPACE_ALLOC_DETAILS_TIME spc;
    //    DataSet ds = new DataSet();

    //    using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP", param))
    //    {

    //        while (sdr.Read())
    //        {
    //            string REQ_ID = sdr["SSA_SRNREQ_ID"].ToString();

    //            spc = new SPACE_ALLOC_DETAILS_TIME();
    //            spc.AUR_ID = sdr["AUR_ID"].ToString();
    //            spc.VERTICAL = sdr["VERTICAL"].ToString();
    //            spc.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
    //            spc.FROM_DATE = Convert.ToDateTime(sdr["FROM_DATE"]);
    //            spc.TO_DATE = Convert.ToDateTime(sdr["TO_DATE"]);
    //            spc.SH_CODE = sdr["SH_CODE"].ToString();
    //            spc.SPC_ID = sdr["SPC_ID"].ToString();
    //            spc.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
    //            spc.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
    //            spc.STACHECK = (int)RequestState.Modified;
    //            spc.STATUS = Convert.ToInt16(sdr["STATUS"]);
    //            spc.ticked = true;
    //            spc.SHIFT_TYPE = sdr["SPACE_TYPE"].ToString();
    //            spc.emp = sdr["emp"].ToString();
    //            spcdet.Add(spc);

    //            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_EMPLOYEE_ALLOCATION_ACTION_SM");
    //            sp.Command.AddParameter("@REQID", REQ_ID, DbType.String);
    //            sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
    //            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
    //            sp.ExecuteScalar();

    //        }
    //    }


    //    foreach (SPACE_ALLOC_DETAILS_TIME obj in allocDetLst.Where(x => x.STACHECK == (int)RequestState.Deleted).ToList())
    //    {
    //        spc = new SPACE_ALLOC_DETAILS_TIME();
    //        spc.SPC_ID = obj.SPC_ID;
    //        spc.STATUS = obj.STATUS;
    //        spc.ticked = true;
    //        spcdet.Add(spc);
    //    }
    //    if (spcdet.Count != 0)
    //        return new { Message = MessagesVM.UM_OK, data = spcdet };
    //    else
    //        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    //}
    public object AllocateSeatsPopUp(CP_SPACE_ALLOCATE_DETAILS_LST allocDetLst)
    {
        try
        {
            List<HD_SPACE_ALLOCATE_DETAILS> hdl = new List<HD_SPACE_ALLOCATE_DETAILS>();
            hdl.Add(new HD_SPACE_ALLOCATE_DETAILS
            {
                SPC_ID = allocDetLst.SPC_ID,
                SSA_SRNREQ_ID = allocDetLst.SSA_SRNREQ_ID,
                SSAD_SRN_REQ_ID = allocDetLst.SSAD_SRN_REQ_ID,
                SH_CODE = allocDetLst.SH_CODE,
                VERTICAL = allocDetLst.VERTICAL,
                Cost_Center_Code = allocDetLst.Cost_Center_Code,
                AUR_ID = allocDetLst.AUR_ID,
                STATUS = allocDetLst.STATUS,
                FROM_DATE = allocDetLst.FROM_DATE,
                TO_DATE = allocDetLst.TO_DATE,
                FROM_TIME = allocDetLst.FROM_TIME,
                TO_TIME = allocDetLst.TO_TIME,
                STACHECK = allocDetLst.STACHECK,
                ticked = allocDetLst.ticked,
                SHIFT_TYPE = allocDetLst.SHIFT_TYPE,
                emp = allocDetLst.emp
            }
                );

            int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
            var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

            String proc_name = (HttpContext.Current.Session["TENANT"]) + ".[COPILOT_HD_SMS_SPACE_ALLOC_MAP]";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(details);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = allocDetLst.AUR_ID;
            param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[2].Value = 1;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "[COPILOT_HD_SMS_SPACE_ALLOC_MAP]", param);
            string Result;
            return new { Status = 1, Message = MessagesVM.AF_OK, Data = ds.Tables[0].Rows[0][0].ToString() };

        }
        catch (Exception ex)
        {
            return new { Status = 0, Message = MessagesVM.ErrorMessage, Data = "" };
        };
    }
    public object GetAminities()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AMINITIES");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetAminitiesBySpace(string spaceId)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AMINITIES_BY_SPACE");
        sp.Command.Parameters.Add("@SpaceId", spaceId, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object AllocateSeats(REQ_DATA requestData)
    {
        var allocDetLst = new List<SPACE_ALLOC_DETAILS_TIME>();
        var aminities = new List<Aminity>();
        var AMENITY_REQ_ID = "";

        if (requestData != null)
        {
            allocDetLst = requestData.selSpaces;
            aminities = requestData.aminities ?? aminities;
        }

        foreach (var aminity in aminities)
        {
            DataSet ss = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "UPDATE_SPACE_AMINITY");
            sp.Command.Parameters.Add("@AMINITY_ID", aminity.AMINITY_ID, DbType.Int64);
            sp.Command.Parameters.Add("@SPACE_ID", aminity.SPACE_ID, DbType.String);
            sp.Command.Parameters.Add("@DESCRIPTION", aminity.DESCRIPTION, DbType.String);
            sp.Command.Parameters.Add("@isSelected", aminity.isSelected, DbType.Boolean);
            ss = sp.GetDataSet();
        }

        try
        {
            var checkingForDuplicateRquest = (from spce in allocDetLst
                                              group spce by new { spce.SPC_ID, spce.FROM_DATE, spce.TO_DATE, spce.SH_CODE, spce.FROM_TIME, spce.TO_TIME } into d
                                              where d.Count() > 1
                                              select new
                                              {
                                                  d.Key.SPC_ID,
                                                  Count = d.Count()
                                              }).ToList();

            if (checkingForDuplicateRquest.Count > 0)
            {
                return new { Message = "Seat can't be book for same timings", data = (object)null };
            }
        }
        catch (Exception ex)
        {

        }





        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();
        SPACE_ALLOC_DETAILS_TIME spc;
        DataSet ds = new DataSet();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP", param))
        {

            while (sdr.Read())
            {
                string REQ_ID = sdr["SSA_SRNREQ_ID"].ToString();

                spc = new SPACE_ALLOC_DETAILS_TIME();
                spc.AUR_ID = sdr["AUR_ID"].ToString();
                spc.VERTICAL = sdr["VERTICAL"].ToString();
                spc.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                spc.FROM_DATE = Convert.ToDateTime(sdr["FROM_DATE"]);
                spc.TO_DATE = Convert.ToDateTime(sdr["TO_DATE"]);
                spc.SH_CODE = sdr["SH_CODE"].ToString();
                spc.SPC_ID = sdr["SPC_ID"].ToString();
                spc.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                spc.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                spc.STACHECK = (int)RequestState.Modified;
                spc.STATUS = Convert.ToInt16(sdr["STATUS"]);
                spc.ticked = true;
                spc.SHIFT_TYPE = sdr["SPACE_TYPE"].ToString();
                spc.emp = sdr["emp"].ToString();
                AMENITY_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                spcdet.Add(spc);

            }
        }

        foreach (var aminity in aminities)
        {
            DataSet ss = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "UPDATE_SPACE_AMINITY");
            sp.Command.Parameters.Add("@AMINITY_ID", aminity.AMINITY_ID, DbType.Int64);
            sp.Command.Parameters.Add("@SPACE_ID", aminity.SPACE_ID, DbType.String);
            sp.Command.Parameters.Add("@DESCRIPTION", aminity.DESCRIPTION, DbType.String);
            sp.Command.Parameters.Add("@isSelected", aminity.isSelected, DbType.Boolean);
            sp.Command.Parameters.Add("@REQ_ID", AMENITY_REQ_ID, DbType.String);
            ss = sp.GetDataSet();
        }

        var listOfEmailDataUnique = new List<SPACE_ALLOC_DETAILS_TIME>();
        var isNotUniqueDataFound = false;
        //make unique list of email
        foreach (var spceDetail in spcdet)
        {
            var checkIfEmailExitsForReqId = listOfEmailDataUnique.Any(x => x.SSA_SRNREQ_ID == spceDetail.SSA_SRNREQ_ID);
            if (!checkIfEmailExitsForReqId)
            {
                //check if email is added for same space and same time
                var checkEmailExitsForSameSpcAndTime = listOfEmailDataUnique.Any(x => x.SPC_ID == spceDetail.SPC_ID &&
                x.FROM_DATE == spceDetail.FROM_DATE && x.TO_DATE == spceDetail.TO_DATE && x.FROM_TIME == spceDetail.FROM_TIME && x.TO_TIME == spceDetail.TO_TIME);

                if (!checkEmailExitsForSameSpcAndTime)
                {
                    listOfEmailDataUnique.Add(spceDetail);
                }
                else
                {
                    isNotUniqueDataFound = true;
                }
            }
            else
            {
                isNotUniqueDataFound = true;
            }

        }

        //send the unique emails
        foreach (var emaildata in listOfEmailDataUnique)
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_EMPLOYEE_ALLOCATION_ACTION_SM");
            sp.Command.AddParameter("@REQID", emaildata.SSA_SRNREQ_ID, DbType.String);
            sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.ExecuteScalar();
        }


        if (isNotUniqueDataFound)
        {
            var requestedData = Newtonsoft.Json.JsonConvert.SerializeObject(allocDetLst);
            var sendingEmailData = Newtonsoft.Json.JsonConvert.SerializeObject(spcdet);
            //mailto ragani
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "LogDuplicateEmailData");
            sp.Command.AddParameter("@RequestedData", requestedData, DbType.String);
            sp.Command.AddParameter("@SendingEmailBookingData", sendingEmailData, DbType.String);
            sp.ExecuteScalar();

        }


        foreach (SPACE_ALLOC_DETAILS_TIME obj in allocDetLst.Where(x => x.STACHECK == (int)RequestState.Deleted).ToList())
        {

            spc = new SPACE_ALLOC_DETAILS_TIME();
            spc.SPC_ID = obj.SPC_ID;
            spc.STATUS = obj.STATUS;
            spc.ticked = true;
            spcdet.Add(spc);
        }
        if (spcdet.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcdet };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }


    public object allocateSeatsValidation(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();
        SPACE_ALLOC_DETAILS_TIME spc;
        DataSet ds = new DataSet();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP_VALIDATION", param))
        {
            while (sdr.Read())
            {
                spc = new SPACE_ALLOC_DETAILS_TIME();
                spc.SPC_ID = sdr["SSAD_SPC_ID"].ToString();
                spc.FROM_DATE = Convert.ToDateTime(sdr["SSAD_FROM_DATE"]);
                spc.TO_DATE = Convert.ToDateTime(sdr["SSAD_TO_DATE"]);
                spc.SH_CODE = sdr["SH_NAME"].ToString();
                spc.AUR_ID = sdr["SSAD_AUR_ID"].ToString();
                spc.VERTICAL = sdr["AUR_KNOWN_AS"].ToString();
                spcdet.Add(spc);
            }
        }
        if (spcdet.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcdet };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object SpcAvailabilityByShiftEdit(SPACE_ALLOC_DETAILS_EDIT sad)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_BY_SHIFT_EDIT");
        sp.Command.Parameters.Add("@SPC_ID", sad.SPC_ID);
        sp.Command.Parameters.Add("@AUR_ID", sad.AUR_ID);
        sp.Command.Parameters.Add("@FDATE", sad.FROM_DATE);
        sp.Command.Parameters.Add("@TDATE", sad.TO_DATE);
        sp.Command.Parameters.Add("@SH_CODE", sad.SH_CODE);
        sp.Command.Parameters.Add("@FDATE_CURR", sad.FROM_DATE_CURR);
        sp.Command.Parameters.Add("@TDATE_CURR", sad.TO_DATE_CURR);
        sp.Command.Parameters.Add("@SH_CODE_CURR", sad.SH_CODE_CURR);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);


        using (IDataReader dr = sp.GetReader())
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = dr["MSG"].ToString(), data = 1 };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS sad)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_BY_SHIFT");
        sp.Command.Parameters.Add("@SPC_ID", sad.SPC_ID);
        sp.Command.Parameters.Add("@FDATE", sad.FROM_DATE);
        sp.Command.Parameters.Add("@TDATE", sad.TO_DATE);
        sp.Command.Parameters.Add("@SH_CODE", sad.SH_CODE);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);


        using (IDataReader dr = sp.GetReader())
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = dr["MSG"].ToString(), data = 1 };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetTotalAreaDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_GET_AREA_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetSeatingCapacity(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_GET_SEATING_CAPACITY_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetEmpAllocSeat(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_EMP_ALLOC_SEAT_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@MODE", svm.mode, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //public object Post(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = new FLOORMAP();
    //    var geom = MakeValidGeographyFromText(mapVm.Wkt);
    //    flr.geom = geom;
    //    flr.AREA_SQFT = geom.Area;
    //    flr.LATITUDE = geom.XCoordinate;
    //    flr.LONGITUDE = geom.YCoordinate;
    //    flr.FLR_ID = mapVm.flr_id;
    //    quickfmsentities.FLOORMAPS.Add(flr);
    //    quickfmsentities.SaveChanges();
    //    mapVm.ID = flr.ID;
    //    return mapVm;
    //}

    //public object Put(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = quickfmsentities.FLOORMAPS.Find(mapVm.ID);
    //    flr.geom = MakeValidGeographyFromText(mapVm.Wkt);
    //    flr.AREA_SQFT = MakeValidGeographyFromText(mapVm.Wkt).Area;
    //    quickfmsentities.Entry(flr).State = System.Data.EntityState.Modified;
    //    quickfmsentities.SaveChanges();
    //    mapVm.ID = flr.ID;
    //    return mapVm;
    //}

    //public void Delete(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = quickfmsentities.FLOORMAPS.Find(mapVm.ID);
    //    quickfmsentities.Entry(flr).State = System.Data.EntityState.Deleted;
    //    quickfmsentities.SaveChanges();
    //}

    //private DbGeometry MakeValidGeographyFromText(string inputWkt)
    //{
    //    SqlGeography sqlPolygon = SQLSpatialTools.Functions.MakeValidGeographyFromText(inputWkt, 4326);
    //    return DbGeometry.FromBinary(sqlPolygon.STAsBinary().Value);
    //}

    //public object GetMarkerDet(MaploaderVM mapVm)
    //{
    //    try
    //    {
    //        FLOORMAPS_MARKERS flrmarker = quickfmsentities.FLOORMAPS_MARKERS.Where(mrk => mrk.FM_SPC_ID == mapVm.SPACE_ID).SingleOrDefault();
    //        return new { Message = MessagesVM.UM_OK, data = flrmarker };
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Message = ex.Message, data = (object)null };
    //    }
    //}

    //public object InsertMarkerDet(MaploaderVM mapVm)
    //{
    //    return new { Message = MessagesVM.UM_OK, data = (object)null };
    //}
}
