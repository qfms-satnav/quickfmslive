﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SUTrendService
/// </summary>
public class AreaWiseCostService
{
    SubSonic.StoredProcedure sp;
    AreaWiseCostDetails AVData;
    DataSet ds;

    public object GetAreaWiseCostDetails(SUbyFilters ASF)
    {
        try
        {
            DataSet ds = GetAreaWiseCostdetailsLST(ASF);
            DataTable CData = ds.Tables[0];
            CustomizedGridCols grdObj;
            List<CustomizedGridCols> grdlst = new List<CustomizedGridCols>();

            if (ASF.Grouplst.Contains("Location"))
            {
                grdObj = new CustomizedGridCols();
                grdObj.headerName = "Location";
                grdObj.field = "Location";
                grdObj.width = 100;
                grdObj.cellClass = "grid-align";
                grdlst.Add(grdObj);
            }
            if (ASF.Grouplst.Contains("Floor"))
            {
                grdObj = new CustomizedGridCols();
                grdObj.headerName = "Floor";
                grdObj.field = "Floor";
                grdObj.width = 100;
                grdObj.cellClass = "grid-align";
                grdlst.Add(grdObj);
            }
            if (ASF.Grouplst.Contains("Vertical"))
            {
                grdObj = new CustomizedGridCols();
                grdObj.headerName = "Vertical";
                grdObj.field = "Vertical";
                grdObj.width = 100;
                grdObj.cellClass = "grid-align";
                grdlst.Add(grdObj);
            }
            if (ASF.Grouplst.Contains("Costcenter"))
            {
                grdObj = new CustomizedGridCols();
                grdObj.headerName = "Costcenter";
                grdObj.field = "Costcenter";
                grdObj.width = 100;
                grdObj.cellClass = "grid-align";
                grdlst.Add(grdObj);
            }
            if (ASF.Grouplst.Contains("Shift"))
            {
                grdObj = new CustomizedGridCols();
                grdObj.headerName = "Shift Name";
                grdObj.field = "Shift";
                grdObj.width = 100;
                grdObj.cellClass = "grid-align";
                grdlst.Add(grdObj);
            }
            if (ASF.Grouplst.Contains("ParentEntity"))
            {
                grdObj = new CustomizedGridCols();
                grdObj.headerName = "Parent Entity";
                grdObj.field = "ParentEntity";
                grdObj.width = 100;
                grdObj.cellClass = "grid-align";
                grdlst.Add(grdObj);
            }
            if (ASF.Grouplst.Contains("ChildEntity"))
            {
                grdObj = new CustomizedGridCols();
                grdObj.headerName = "Child Entity";
                grdObj.field = "ChildEntity";
                grdObj.width = 100;
                grdObj.cellClass = "grid-align";
                grdlst.Add(grdObj);
            }

            if (CData.Rows.Count != 0)
                return new
                {
                    Message = MessagesVM.UM_OK,
                    data = new
                    {
                        Griddata = CData,
                        Coldef = grdlst
                    }
                };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }
    }

    public DataSet GetAreaWiseCostdetailsLST(SUbyFilters ASF)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(ASF.Locationlst);
        param[1] = new SqlParameter("@CST_LST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(ASF.Costcenterlst);
        param[2] = new SqlParameter("@VER_LST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(ASF.Verticallst);
        param[3] = new SqlParameter("@GRPBY", SqlDbType.NVarChar);
        param[3].Value = ASF.Grouplst.Remove(ASF.Grouplst.Length - 1, 1);
        param[4] = new SqlParameter("@AUR_ID", HttpContext.Current.Session["UID"]);
        param[5] = new SqlParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "AREA_WISE_COST_REPORT", param);
        return ds;
    }

    public List<AreaWiseCostDetails> GetSpaceAreaDetails(AreaWiseCostDetails ADet)
    {
        try
        {
            List<AreaWiseCostDetails> AClist = new List<AreaWiseCostDetails>();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@VER_NAME", SqlDbType.NVarChar);
            param[0].Value = ADet.Vertical;
            param[1] = new SqlParameter("@COST_CENTER_NAME", SqlDbType.NVarChar);
            param[1].Value = ADet.Costcenter;
            param[2] = new SqlParameter("@SH_NAME", SqlDbType.NVarChar);
            param[2].Value = ADet.Shift;
            param[3] = new SqlParameter("@LCM_NAME", SqlDbType.NVarChar);
            param[3].Value = ADet.Location;
            param[4] = new SqlParameter("@FLR_NAME", SqlDbType.NVarChar);
            param[4].Value = ADet.Floor;
            param[5] = new SqlParameter("@UNIT_COST", SqlDbType.Int);
            param[5].Value = ADet.UNIT_COST;
            param[6] = new SqlParameter("@CHE_NAME", SqlDbType.NVarChar);
            param[6].Value = ADet.ChildEntity;
            param[7] = new SqlParameter("@USR_ID", HttpContext.Current.Session["UID"]);
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_AREA_WISE_COST_DETAILS", param))
            {
                while (reader.Read())
                {

                    AVData = new AreaWiseCostDetails();
                    AVData.CNY_NAME = reader["CNY_NAME"].ToString();
                    AVData.CTY_NAME = reader["CTY_NAME"].ToString();
                    AVData.LCM_NAME = reader["LCM_NAME"].ToString();
                    AVData.TWR_NAME = reader["TWR_NAME"].ToString();
                    AVData.FLR_NAME = reader["FLR_NAME"].ToString();
                    AVData.SPC_ID = reader["SPC_ID"].ToString();
                    AVData.COST_CENTER_NAME = reader["COST_CENTER_NAME"].ToString();
                    AVData.VER_NAME = reader["VER_NAME"].ToString();
                    AVData.SPC_TYPE_NAME = reader["SPC_TYPE_NAME"].ToString();
                    AVData.SH_NAME = reader["SH_NAME"].ToString();
                    AVData.PE_NAME = reader["PE_NAME"].ToString();
                    AVData.CHE_NAME = reader["CHE_NAME"].ToString();
                    AVData.UNIT_COST = (int)reader["Unit_Cost"];
                    AClist.Add(AVData);
                }
                reader.Close();
            }
            return AClist;
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}