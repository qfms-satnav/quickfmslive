﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
/// <summary>
/// Summary description for SpaceTypeCostService
/// </summary>
public class SpaceTypeCostService
{
    public IEnumerable<SpaceTypeCost> BindGridData(string flrcode)
    {
        try
        {
            SpaceTypeCost stc1;
            List<SpaceTypeCost> stclst = new List<SpaceTypeCost>();

            //SeatType stobj;

            //Dictionary<string, int> dictStatus = new Dictionary<string, int>();

            seattype seattype;
            List<seattype> seattypelst = new List<seattype>();
            using (SqlDataReader sdr1 = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_TYPE"))
            {
                while (sdr1.Read())
                {
                    seattype = new seattype();
                    seattype.Code = sdr1["SPC_TYPE_CODE"].ToString();
                    seattype.Name = sdr1["SPC_TYPE_NAME"].ToString();
                    seattypelst.Add(seattype);
                }
            }

            //KeyVal kv;
            //List<KeyVal> keyval = new List<KeyVal>();


            //foreach (KeyValuePair<string, int> pair in dictStatus)
            //{
            //    kv = new KeyVal();
            //    kv.Key = pair.Key;
            //    kv.Value = pair.Value;
            //    keyval.Add(kv);
            //}

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@FLOOR_CODE", SqlDbType.VarChar);
            param[0].Value = flrcode;

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_FLOOR_COST", param))
            {
                while (sdr.Read())
                {
                    stc1 = new SpaceTypeCost();
                    stc1.FLR_CODE = sdr["FLR_CODE"].ToString();
                    stc1.FLR_NAME = sdr["FLR_NAME"].ToString();
                    stc1.lstseattype = seattypelst;
                    stc1.variableCost = seattypelst;
                    stclst.Add(stc1);
                }
            }
            return stclst;
        }
        catch
        {
            throw;
        }
    }


    public IEnumerable<locationdata> getLocationdata(string lcmcode)
    {

        locationdata lcm;
        List<locationdata> lcmlst = new List<locationdata>();
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
            param[0].Value = lcmcode;

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_LOCATION", param))
            {
                while (sdr.Read())
                {
                    lcm = new locationdata();
                    lcm.LCM_CODE = sdr["LCM_CODE"].ToString();
                    lcm.LCM_NAME = sdr["LCM_NAME"].ToString();                    
                    lcmlst.Add(lcm);
                }
            }
            return lcmlst;

        }
        catch(Exception ex)
        {
            throw;
        }
    }

    public object SaveLocationCostData(List<locationdata> lcmcost)
    {
       

        try
        {

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
            param[0].Value = lcmcost[0].LCM_CODE;
            param[1] = new SqlParameter("@SEAT_COST", SqlDbType.VarChar);
            param[1].Value = lcmcost[0].SEAT_COST;

            int retval = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SMS_INSERT_LOCATION_WISE_COST", param);
            if (retval != -1)
                return new { Message = MessagesVM.AR_Success, data = "" };
            else
                return new { Message = MessagesVM.AR_UpdateError, data = (object)null };

        }
        catch (Exception ex)
        {
            throw;
        }      
    }

    public object SaveCostData(List<SPCDetails> spc)
    {
        SPCCostDetails cstdet;
        List<SPCCostDetails> cstlst = new List<SPCCostDetails>();

        if (spc != null)
        {
            foreach (SPCDetails spcdet in spc)
            {
                if (spcdet.lstseattype != null && spcdet.lstseattype.Count > 0)
                {
                    foreach (seattype stype in spcdet.lstseattype)
                    {
                        cstdet = new SPCCostDetails();
                        cstdet.SC_COST_TYPE = "spctype";
                        cstdet.SC_FLR_CODE = spcdet.FLR_CODE;
                        cstdet.SC_BASED_ON = stype.Code;
                        cstdet.SC_COST_VALUE = Convert.ToInt32(stype.Value);
                        var x = spcdet.variableCost.Where(s => s.Code == stype.Code).FirstOrDefault();
                        cstdet.SC_VAR_COST_VALUE = x == null ? 0 : Convert.ToInt32(x.Value);
                        cstlst.Add(cstdet);
                    }
                }
                else
                {
                    cstdet = new SPCCostDetails();
                    cstdet.SC_COST_TYPE = "area";
                    cstdet.SC_FLR_CODE = spcdet.FLR_CODE;
                    cstdet.SC_BASED_ON = "SQFT";
                    cstdet.SC_COST_VALUE =spcdet.AW_COST;
                    cstdet.SC_VAR_COST_VALUE = spcdet.VAR_COST;
                    cstlst.Add(cstdet);
                }
            }


            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@SEAT_TYPE_LST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(cstlst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];


            int retval = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SMS_INSERT_SPACE_TYPE_COST", param);
            if (retval != -1)
                return new { Message = MessagesVM.AR_Success, data = "" };
            else
                return new { Message = MessagesVM.AR_UpdateError, data = (object)null };

        }
        else
            return new { Message = MessagesVM.AR_UpdateError, data = (object)null };
    }



    public object GetAreaType()
    {
        SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_AREA_TYPE");
        SpaceTypeCostDet stc;
        List<SpaceTypeCostDet> stclst = new List<SpaceTypeCostDet>();
        AREA_TYPES at;
        List<AREA_TYPES> atlst = new List<AREA_TYPES>();
        while (dr.Read())
        {

            at = new AREA_TYPES();
            at.SAT_ID = Convert.ToInt32(dr["SAT_ID"]);
            at.SAT_CODE = Convert.ToString(dr["SAT_CODE"]);
            at.SAT_PARENT = Convert.ToInt32(dr["SAT_PARENT"]);
            if (at.SAT_PARENT == 0)
            {
                stc = new SpaceTypeCostDet();
                stc.SATD_SAT_ID = Convert.ToInt32(dr["SAT_ID"]);
                stclst.Add(stc);
            }
            atlst.Add(at);
        }
        if (atlst.Count > 0)
            return new { Message = MessagesVM.UM_OK, data = new { areatypes = atlst, typedetails = stclst } };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object SetAreaType(SpaceTypeCostVM stcvm)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@LOC_LST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(stcvm.lcmlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@AREATYPE", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(stcvm.stcd);
        param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];

        int retval = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SMS_SET_AREATYPES", param);
        if (retval != -1)
            return new { Message = MessagesVM.AR_Success, data = "" };
        else
            return new { Message = MessagesVM.AR_UpdateError, data = (object)null };

    }

    public object GetAreaTypeByLocation(List<Locationlst> lcmlst)
    {
        SpaceTypeCostDet stc;
        List<SpaceTypeCostDet> stclst = new List<SpaceTypeCostDet>();

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@LOC_LST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(lcmlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_AREA_TYPE_BYLOC", param);
        while (dr.Read())
        {
            stc = new SpaceTypeCostDet();
            stc.SATD_SAT_ID = Convert.ToInt32(dr["SATD_SAT_ID"]);
            stc.SATD_OPR1 = Convert.ToString(dr["SATD_OPR1"]);
            stc.SATD_OPR2 = Convert.ToString(dr["SATD_OPR2"]);
            stc.SATD_OPT1 = Convert.ToString(dr["SATD_OPT1"]);
            stc.SATD_OPT2 = Convert.ToString(dr["SATD_OPT2"]);
            stc.SATD_OPT3 = Convert.ToString(dr["SATD_OPT3"]);
            stclst.Add(stc);
        }
        if (stclst.Count > 0)
            return new { Message = MessagesVM.UM_OK, data = stclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSpcLst(String flrcode)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@FLOOR_CODE", SqlDbType.VarChar);
        param[0].Value = flrcode;

        DataTable spclst = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_SPC_LST", param);
        if (spclst.Rows.Count > 0)
            return new { Message = MessagesVM.UM_OK, data = spclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object ShowLocationGrid(String lcmcode)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
        param[0].Value = lcmcode;

        DataTable spclst = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "SMS_GET_LOCATION_GRID_DATA", param);
        if (spclst.Rows.Count > 0)
            return new { Message = MessagesVM.UM_OK, data = spclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    //public IEnumerable<SpaceTypeCost> Costupdate(SpaceTypeCost spcdet)
    //{
    //    try
    //    {
    //        List<SpaceTypeCost> stclst = new List<SpaceTypeCost>();
    //        stclst.Add(spcdet.spcreq);
    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}
}