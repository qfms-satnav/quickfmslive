﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.IO;

/// <summary>
/// Summary description for BreakDownMaintenanceServices
/// </summary>
public class ManagerApprovalServices
{
    SubSonic.StoredProcedure sp;
    public object Getapprovaldata()
    {

        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["Uid"];
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_SMS_SPACE_MANAGER_APPROVAL_MAP", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (SqlException) { throw; }

    }

    public object updatedata(int id)
    {
        DataSet DS = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPD_SMS_SPACE_MANAGER_APPROVAL_MAP");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@SSMA_ID", id, DbType.Int32);
        DS = sp.GetDataSet();
        if (DS.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.UM_NO_REC };

    }

    public object RejectFn(int id)
    {
        DataSet DS = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPD_SMS_SPACE_MANAGER_APPROVAL_REJECT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@SSMA_ID", id, DbType.Int32);
        DS = sp.GetDataSet();
        if (DS.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.UM_NO_REC };

    }

}
