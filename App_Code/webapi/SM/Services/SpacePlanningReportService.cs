﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
using System.Web.Script.Serialization;


public class SpacePlanningReportService
{
    SubSonic.StoredProcedure sp;
    SpacePlanningReportVM SpcPlnData;
    List<SpacePlanningReportVM> SpcPlnRptlist;


    public object BindGridData(SpacePlanningReport spcdet)
    {
        SpcPlnRptlist = GetData(spcdet);

        if (SpcPlnRptlist.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SpcPlnRptlist };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<SpacePlanningReportVM> GetData(SpacePlanningReport spcdet)
    {
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = spcdet.CNP_NAME;
        param[2] = new SqlParameter("@SEARCHVAL", DbType.String);
        param[2].Value = spcdet.SearchValue;
        param[3] = new SqlParameter("@PAGENUM", DbType.String);
        param[3].Value = spcdet.PageNumber;
        param[4] = new SqlParameter("@PAGESIZE", DbType.String);
        param[4].Value = spcdet.PageSize;

        SpcPlnRptlist = new List<SpacePlanningReportVM>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SP_PLANNING_GET_PROJECTION_REPORT_GRID", param))
        {
            while (sdr.Read())
            {
                SpcPlnData = new SpacePlanningReportVM();
                SpcPlnData.OVERALL_COUNT = sdr.GetValue(1).ToString();
                SpcPlnData.SP_LOC = sdr.GetValue(2).ToString();
                SpcPlnData.BUILD_CAPACITY = sdr.GetValue(3).ToString();
                SpcPlnData.UTILIZED_SEATS = sdr.GetValue(4).ToString();
                SpcPlnData.VACANT = sdr.GetValue(5).ToString();
                SpcPlnData.TOTAL_SEATS_REQUIRED = sdr.GetValue(6).ToString();
                SpcPlnData.REQUIREMENT_PENDING = sdr.GetValue(7).ToString();
                SpcPlnData.STAUS = sdr.GetValue(8).ToString();
                SpcPlnData.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                SpcPlnRptlist.Add(SpcPlnData);

            }
            sdr.Close();
        }
        return SpcPlnRptlist;
    }

    public object BindGridData1(SpacePlanningReport spcdet)
    {
        SpcPlnRptlist = GetDatavertical(spcdet);

        if (SpcPlnRptlist.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SpcPlnRptlist };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<SpacePlanningReportVM> GetDatavertical(SpacePlanningReport spcdet)
    {
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = spcdet.CNP_NAME;
        param[2] = new SqlParameter("@SEARCHVAL", DbType.String);
        param[2].Value = spcdet.SearchValue;
        param[3] = new SqlParameter("@PAGENUM", DbType.String);
        param[3].Value = spcdet.PageNumber;
        param[4] = new SqlParameter("@PAGESIZE", DbType.String);
        param[4].Value = spcdet.PageSize;

        SpcPlnRptlist = new List<SpacePlanningReportVM>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SP_PLANNING_GET_PROJECTION_REPORT_VERTICAL_GRID", param))
        {
            while (sdr.Read())
            {
                SpcPlnData = new SpacePlanningReportVM();
                SpcPlnData.VER_NAME = sdr.GetValue(0).ToString();
                SpcPlnData.Total_Seats = sdr.GetValue(1).ToString();
                SpcPlnData.UTILIZED_SEATS = sdr.GetValue(2).ToString();
                SpcPlnData.VACANT = sdr.GetValue(3).ToString();
                SpcPlnData.TOTAL_SEATS_REQUIRED = sdr.GetValue(4).ToString();
                SpcPlnData.REQUIREMENT_PENDING = sdr.GetValue(5).ToString();
                SpcPlnData.STAUS = sdr.GetValue(6).ToString();
                SpcPlnData.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                SpcPlnRptlist.Add(SpcPlnData);
            }
            sdr.Close();
        }
        return SpcPlnRptlist;
    }

    public static dynamic GetDynamicObject(Dictionary<string, object> properties)
    {
        var dynamicObject = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
        foreach (var property in properties)
        {
            dynamicObject.Add(property.Key, property.Value);
        }
        return dynamicObject;
    }

    public object GetSpaceChart(SpacePlanningReport Spcdetails)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_PLANNING_GET_PROJECTION_REPORT_CHART");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", Spcdetails.CNP_NAME, DbType.String);
            DataSet ds = sp.GetDataSet();
            UtilityService userv = new UtilityService();
            DataTable dt = userv.GetInversedDataTable(ds.Tables[0], "LOCATION");
            var columns = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
            return new { Message = "", data = new { Details = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), Columnnames = columns } };
        }
        catch
        {
            throw;
        }
    }

}