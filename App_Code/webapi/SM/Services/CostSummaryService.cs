﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for CostSummaryService
/// </summary>
public class CostSummaryService
{
    DataSet ds;
    public object GetCostReportObject(Company cstvm)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(cstvm.lcmlst);
            param[1] = new SqlParameter("@VERLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(cstvm.verlst);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_COST_SUMMARY_REPORT", param);
            return new { data = ds };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

}