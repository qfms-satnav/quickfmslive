﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;



/// <summary>
/// Summary description for SpaceMasterService
/// </summary>
public class SpaceMasterService
{
    VerticalReq_detailss verreqdet;
    List<VerticalReq_detailss> verreqdetlst;

    List<SeatTyp> verreqdeta;
    List<SpaceList> spclst;
    List<SeatSubType> spcsub;
    public List<VerticalRequistions> upddetlst;
    SubSonic.StoredProcedure sp;
    public object GetVacantSpaces(SpaceMasterDetails spc)
    {
        verreqdetlst = GetVacantSpacesList(spc);
        if (verreqdetlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verreqdetlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
  
    public List<VerticalReq_detailss> GetVacantSpacesList(SpaceMasterDetails spc)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spc.flrlst);
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = HttpContext.Current.Session["COMPANYID"];
        verreqdetlst = new List<VerticalReq_detailss>();
        using (SqlDataReader ds = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[SMS_GET_ALL_SPACES]", param))
        {
            while (ds.Read())
            {
                verreqdet = new VerticalReq_detailss();
                verreqdet.SVD_SPC_ID = ds["SPC_ID"].ToString();
                verreqdet.SVD_SPC_NAME = ds["SPC_NAME"].ToString();
                verreqdet.SVD_SPC_TYPE = ds["SPC_TYPE_CODE"].ToString();
                verreqdet.SVD_SPC_TYPE_NAME = ds["SPC_TYPE_NAME"].ToString();
                verreqdet.SVD_SPC_SUB_TYPE = ds["SST_CODE"].ToString();
                verreqdet.SVD_SPC_SUB_TYPE_NAME = ds["SST_NAME"].ToString();
                verreqdet.SVD_SPACE_TYPE_NAME = ds["SEAT_TYPE"].ToString();
                verreqdet.FLR_NAME = ds["FLR_NAME"].ToString();
                verreqdet.FLR_ID = ds["FLR_ID"].ToString();
                verreqdetlst.Add(verreqdet);
            }
        }
        return verreqdetlst;
    }


    public List<SeatTyp> GetSeatType()
    {

        verreqdeta = new List<SeatTyp>();

        using (SqlDataReader ds = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[SMS_GET_SEAT_TYPE]"))
        {

            while (ds.Read())
            {
                SeatTyp S = new SeatTyp();
                S.SVD_SPACE_TYPE_NAME = ds["SEAT_TYPE"].ToString();
                S.SNO = ds["SNO"].ToString();

                verreqdeta.Add(S);
            }
        }
        return verreqdeta;

    }
    public object RaiseRequest(SpaceMasterDetails spc)
    {
       
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(spc.flrlst);
            param[1] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
            param[1].Value = spc.ALLOCSTA;
            param[2] = new SqlParameter("@SPC_LIST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(spc.verreqdet);
            param[3] = new SqlParameter("@CHANGE_VALUE", SqlDbType.VarChar);
            param[3].Value = spc.spclst;
            param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            //int i = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[SMS_SEAT_TYPE_CHANGESS]", param);
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[SMS_SEAT_TYPE_CHANGESS]", param);
            if ((int)dt.Rows[0][0] == 1)
            {
                return new { data = GetVacantSpacesList(spc), Message = MessagesVM.SPC_REQ_UPDATED };
            }
            else if((int)dt.Rows[0][0] == 2)
            {
                return new { data = GetVacantSpacesList(spc), Message = dt.Rows[0][1].ToString()};
            }
            else
            {
                return new { data = GetVacantSpacesList(spc), Message = MessagesVM.SPC_REQ_CANCELED };
            }
        }
        catch (Exception ex)
        {
            return new { data = GetVacantSpacesList(spc), Message = MessagesVM.SPC_REQ_CANCELED + '/' + ex.Message.ToString() };
        }
    }

    public List<SeatSubType> SeatSubTypes()
    {

        spcsub = new List<SeatSubType>();

        using (SqlDataReader ds = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[SMS_GET_SEAT_SUBTYPE]"))
        {

            while (ds.Read())
            {
                SeatSubType Sb = new SeatSubType();
                Sb.SST_CODE = ds["SST_CODE"].ToString();
                Sb.SST_ID = ds["SST_ID"].ToString();
                Sb.SVD_SPC_SUB_TYPE_NAME = ds["SST_NAME"].ToString();

                spcsub.Add(Sb);
            }
        }
        return spcsub;
    }

    public object SpaceSubTypes()
    {
        return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[SMS_GET_SPACE_SUBTYPE]");
    }

    public object UpdateRequest(SpaceMasterDetails spc)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[6];

            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(spc.flrlst);
            param[1] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
            param[1].Value = spc.ALLOCSTA;
            param[2] = new SqlParameter("@SPC_LIST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(spc.verreqdet);
            param[3] = new SqlParameter("@CHANGE_VALUE", SqlDbType.VarChar);
            param[3].Value = spc.spcsub;
            param[4] = new SqlParameter("@SpaceType", SqlDbType.VarChar);
            param[4].Value = spc.SpaceType;
            param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["UID"];
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[SMS_SEAT_TYPE_CHANGESS]", param);
            if ((int)dt.Rows[0][0] == 1)
            {
                return new { data = GetVacantSpacesList(spc), Message = MessagesVM.SPC_REQ_UPDATED };
            }
            else if ((int)dt.Rows[0][0] == 2)
            {
                return new { data = GetVacantSpacesList(spc), Message = dt.Rows[0][1].ToString() };
            }
            else
            {
                return new { data = GetVacantSpacesList(spc), Message = MessagesVM.SPC_REQ_CANCELED };
            }
        }
        catch (Exception ex)
        {
            return new { data = GetVacantSpacesList(spc), Message = MessagesVM.SPC_REQ_CANCELED + '/' + ex.Message.ToString() };
        }
    }


}
