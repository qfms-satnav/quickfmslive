﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

public class SpaceOccupancyReportService
{
    SubSonic.StoredProcedure sp;
    SpaceOccupancyData SpcOccup;
    List<SpaceOccupancyData> OccupList;
    DataSet ds;

    public List<SpaceOccupancyData> GetOccupancyDetails(SpaceOccupancyDetials Occup)
    {
        try
        {
            OccupList = new List<SpaceOccupancyData>();
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[0].Value = Occup.FromDate;
            param[1] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[1].Value = Occup.ToDate;
            param[2] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@Costcenterlst", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(Occup.Costcenterlst);
            param[4] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[4].Value = HttpContext.Current.Session["COMPANYID"];
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_GET_OCCUPANCY_REPORT", param);
            SpaceOccupancyData SpaceOcc = new SpaceOccupancyData();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            OccupList = JsonConvert.DeserializeObject<List<SpaceOccupancyData>>(JSONString);

            //using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_OCCUPANCY_REPORT", param))
            //{
            //    while (reader.Read())
            //    {
            //        SpcOccup = new SpaceOccupancyData();
            //        SpcOccup.CNY_NAME = reader["CNY_NAME"].ToString();
            //        SpcOccup.CTY_NAME = reader["CTY_NAME"].ToString();
            //        SpcOccup.LCM_NAME = reader["LCM_NAME"].ToString();
            //        SpcOccup.TWR_NAME = reader["TWR_NAME"].ToString();
            //        SpcOccup.FLR_NAME = reader["FLR_NAME"].ToString();
            //        SpcOccup.EXTENSION = reader["EXTENSION"].ToString();
            //        SpcOccup.ENTITY = reader["ENTITY"].ToString();
            //        SpcOccup.CHILD_ENTITY = reader["CHILD_ENTITY"].ToString();
            //        SpcOccup.VER_NAME = reader["VER_NAME"].ToString();
            //        SpcOccup.DEP_NAME = reader["DEP_NAME"].ToString();
            //        SpcOccup.SHIFT = reader["SHIFT"].ToString();
            //        SpcOccup.SPACE_TYPE = reader["SPACE_TYPE"].ToString();
            //        SpcOccup.EMP_ID = reader["EMP_NAME"].ToString();
            //        SpcOccup.AUR_ID = reader["AUR_ID"].ToString();
            //        SpcOccup.SPC_ID = reader["SPC_ID"].ToString();
            //        SpcOccup.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
            //        SpcOccup.AUR_TYPE = reader["AUR_TYPE"].ToString();
            //        SpcOccup.AUR_REPORTING_TO = reader["AUR_REPORTING_TO"].ToString();
            //        SpcOccup.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
            //        OccupList.Add(SpcOccup);
            //    }
            //    reader.Close();
            //}
            if (OccupList.Count != 0)
                return OccupList;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }

    public List<SpaceOccupancyData> SearchAllData(SpaceOccupancyDetials Occup)
    {
        try
        {
            OccupList = new List<SpaceOccupancyData>();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[0].Value = Occup.FromDate;
            param[1] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[1].Value = Occup.ToDate;
            param[2] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@Costcenterlst", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(Occup.Costcenterlst);
            param[4] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[4].Value = HttpContext.Current.Session["COMPANYID"];
            param[5] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[5].Value = Occup.PageNumber;
            param[6] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[6].Value = Occup.PageSize;
            param[7] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
            param[7].Value = Occup.SearchValue;


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_OCCUPANCY_REPORT", param))
            {
                while (reader.Read())
                {
                    SpcOccup = new SpaceOccupancyData();
                    SpcOccup.CNY_NAME = reader["CNY_NAME"].ToString();
                    SpcOccup.CTY_NAME = reader["CTY_NAME"].ToString();
                    SpcOccup.LCM_NAME = reader["LCM_NAME"].ToString();
                    SpcOccup.TWR_NAME = reader["TWR_NAME"].ToString();
                    SpcOccup.FLR_NAME = reader["FLR_NAME"].ToString();
                    SpcOccup.EXTENSION = reader["EXTENSION"].ToString();
                    SpcOccup.ENTITY = reader["ENTITY"].ToString();
                    SpcOccup.CHILD_ENTITY = reader["CHILD_ENTITY"].ToString();
                    SpcOccup.VER_NAME = reader["VER_NAME"].ToString();
                    SpcOccup.DEP_NAME = reader["DEP_NAME"].ToString();
                    SpcOccup.SHIFT = reader["SHIFT"].ToString();
                    SpcOccup.SPACE_TYPE = reader["SPACE_TYPE"].ToString();
                    SpcOccup.EMP_ID = reader["EMP_NAME"].ToString();
                    SpcOccup.AUR_ID = reader["AUR_ID"].ToString();
                    SpcOccup.SPC_ID = reader["SPC_ID"].ToString();
                    SpcOccup.REQUEST_TYPE = reader["REQUEST_TYPE"].ToString();
                    SpcOccup.AUR_TYPE = reader["AUR_TYPE"].ToString();
                    SpcOccup.AUR_REPORTING_TO = reader["AUR_REPORTING_TO"].ToString();
                    SpcOccup.BHO_NAME = reader["BHO_NAME"].ToString();
                    SpcOccup.BHT_NAME = reader["BHT_NAME"].ToString();
                    OccupList.Add(SpcOccup);
                }
                reader.Close();
            }
            if (OccupList.Count != 0)
                return OccupList;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }
    public object GetChartCountData(SpaceOccupancyDetials OccupDetails)
    {
        try
        {
            List<SpaceOccupancyData> Spcdata = new List<SpaceOccupancyData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_OCCUPANCY_COUNT");
            sp.Command.AddParameter("@FROM_DATE", OccupDetails.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", OccupDetails.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", OccupDetails.CNP_NAME, DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch
        {
            throw;
        }
    }
}