﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpaceAvailabilityReportService
{
    SubSonic.StoredProcedure sp;
    SpaceAvailData AvData;
    DataSet ds;

    public List<SpaceAvailData> GetAvailDetails(SpaceAvailDetails Avail)
    {
        try
        {
            List<SpaceAvailData> AvailList = new List<SpaceAvailData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_REPORT");
            sp.Command.AddParameter("@COMPANYID", Avail.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@PageNumber", Avail.PageNumber, DbType.String);
            sp.Command.AddParameter("@PageSize", Avail.PageSize, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {

                    AvData = new SpaceAvailData();
                    AvData.CNY_NAME = reader["CNY_NAME"].ToString();
                    AvData.RGN_NAME = reader["RGN_NAME"].ToString();
                    AvData.LCM_ZONE = reader["LCM_ZONE"].ToString();
                    AvData.AM_ST_NAME = reader["AM_ST_NAME"].ToString();
                    AvData.CTY_NAME = reader["CTY_NAME"].ToString();
                    AvData.LCM_NAME = reader["LCM_NAME"].ToString();
                    AvData.TWR_NAME = reader["TWR_NAME"].ToString();
                    AvData.FLR_NAME = reader["FLR_NAME"].ToString();
                    AvData.SPC_TYPE = reader["SPC_TYPE"].ToString();
                    AvData.SPACE_SUB_TYPE = reader["SPACE_SUB_TYPE"].ToString();
                    AvData.FLR_CODE = reader["FLR_CODE"].ToString();
                    AvData.TOTAL_SEATS_COUNT = reader["TOTAL_SEATS_COUNT"].ToString();
                    AvData.VACANT_SEATS = (int)reader["VACANT_SEATS"];
                    // AvData.CNY_CODE = reader["CNY_CODE"].ToString();
                    // AvData.CTY_CODE = reader["CTY_CODE"].ToString();
                    AvData.LCM_CODE = reader["LCM_CODE"].ToString();
                    // AvData.FLR_CODE = reader["FLR_CODE"].ToString();
                    // AvData.SPC_TYPE_CODE = reader["SPC_TYPE_CODE"].ToString();
                    //AvData.CHE_CODE = reader["CHE_CODE"].ToString();
                    AvData.SST_CODE = reader["SST_CODE"].ToString();
                    AvData.SPC_TYPE_CODE = reader["SPC_TYPE_CODE"].ToString();
                    AvailList.Add(AvData);

                }
                reader.Close();
            }
            if (AvailList.Count != 0)
                return AvailList;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }

    public List<SpaceAvailData> GetSearchData(SpaceAvailDetails Avail)
    {
        try
        {
            List<SpaceAvailData> AvailList = new List<SpaceAvailData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_REPORT_SEARCHVALUE");
            sp.Command.AddParameter("@COMPANYID", Avail.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@PageNumber", Avail.PageNumber, DbType.String);
            sp.Command.AddParameter("@PageSize", Avail.PageSize, DbType.String);
            sp.Command.AddParameter("@SearchValue", Avail.SearchValue, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {

                    AvData = new SpaceAvailData();
                    AvData.CNY_NAME = reader["CNY_NAME"].ToString();
                    AvData.RGN_NAME = reader["RGN_NAME"].ToString();
                    AvData.LCM_ZONE = reader["LCM_ZONE"].ToString();
                    AvData.AM_ST_NAME = reader["AM_ST_NAME"].ToString();
                    AvData.CTY_NAME = reader["CTY_NAME"].ToString();
                    AvData.LCM_NAME = reader["LCM_NAME"].ToString();
                    AvData.TWR_NAME = reader["TWR_NAME"].ToString();
                    AvData.FLR_NAME = reader["FLR_NAME"].ToString();
                    AvData.SPC_TYPE = reader["SPC_TYPE"].ToString();
                    //AvData.SPACE_SUB_TYPE = reader["SPACE_SUB_TYPE"].ToString();
                    //AvData.SPACE_SUB_TYPE_CODE = reader["SPACE_SUB_TYPE_CODE"].ToString();
                    AvData.TOTAL_SEATS_COUNT = reader["TOTAL_SEATS_COUNT"].ToString();
                    AvData.VACANT_SEATS = (int)reader["VACANT_SEATS"];
                    AvData.CNY_CODE = reader["CNY_CODE"].ToString();
                    AvData.CTY_CODE = reader["CTY_CODE"].ToString();
                    AvData.LCM_CODE = reader["LCM_CODE"].ToString();
                    AvData.FLR_CODE = reader["FLR_CODE"].ToString();

                    //AvData.CNP_NAME = reader["CNP_NAME"].ToString();
                    //AvData.SST_CODE = reader["SST_CODE"].ToString();

                    AvailList.Add(AvData);

                }
                reader.Close();
            }
            if (AvailList.Count != 0)
                return AvailList;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }

    public List<SpaceAvailData> ExportExcel(SpaceAvailDetails Avail)
    {
        try
        {
            List<SpaceAvailData> AvailList = new List<SpaceAvailData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_REPORT_EXPORT");
            sp.Command.AddParameter("@COMPANYID", Avail.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);

            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {

                    AvData = new SpaceAvailData();
                    AvData.CNY_NAME = reader["CNY_NAME"].ToString();
                    AvData.RGN_NAME = reader["RGN_NAME"].ToString();
                    AvData.LCM_ZONE = reader["LCM_ZONE"].ToString();
                    AvData.AM_ST_NAME = reader["AM_ST_NAME"].ToString();
                    AvData.CTY_NAME = reader["CTY_NAME"].ToString();
                    AvData.LCM_NAME = reader["LCM_NAME"].ToString();
                    AvData.TWR_NAME = reader["TWR_NAME"].ToString();
                    AvData.FLR_NAME = reader["FLR_NAME"].ToString();
                    AvData.SPC_TYPE = reader["SPC_TYPE"].ToString();
                    //AvData.SPACE_SUB_TYPE = reader["SPACE_SUB_TYPE"].ToString();
                    //AvData.SPACE_SUB_TYPE_CODE = reader["SPACE_SUB_TYPE_CODE"].ToString();
                    AvData.TOTAL_SEATS_COUNT = reader["TOTAL_SEATS_COUNT"].ToString();
                    AvData.VACANT_SEATS = (int)reader["VACANT_SEATS"];
                    AvData.CNY_CODE = reader["CNY_CODE"].ToString();
                    AvData.CTY_CODE = reader["CTY_CODE"].ToString();
                    AvData.LCM_CODE = reader["LCM_CODE"].ToString();
                    AvData.FLR_CODE = reader["FLR_CODE"].ToString();
                    //AvData.CNP_NAME = reader["CNP_NAME"].ToString();
                    AvData.SPC_TYPE_CODE = reader["SPC_TYPE_CODE"].ToString();
                    //AvData.SST_CODE = reader["SST_CODE"].ToString();

                    AvailList.Add(AvData);

                }
                reader.Close();
            }
            if (AvailList.Count != 0)
                return AvailList;
            else
                return null;
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    public object GetSelectedAvlDetails(SpaceAvailData Avail)
    {
        try
        {
            List<SpaceAvailData> VMlist = GetVacantSeatList(Avail);
            return new { Message = MessagesVM.UM_OK, data = VMlist };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<SpaceAvailData> GetVacantSeatList(SpaceAvailData Avail)
    {
        try
        {
            List<SpaceAvailData> VMlist = new List<SpaceAvailData>();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CNY_CODE", SqlDbType.NVarChar);
            param[0].Value = Avail.CNY_CODE;
            param[1] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
            param[1].Value = Avail.CTY_CODE;
            param[2] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[2].Value = Avail.LCM_CODE;
            param[3] = new SqlParameter("@FLR_CODE", SqlDbType.NVarChar);
            param[3].Value = Avail.FLR_CODE;
            param[4] = new SqlParameter("@SPC_TYPE_CODE", SqlDbType.NVarChar);
            param[4].Value = Avail.SPC_TYPE_CODE;
            param[5] = new SqlParameter("@SST_CODE", SqlDbType.NVarChar);
            param[5].Value = Avail.SST_CODE;
            param[6] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[6].Value = HttpContext.Current.Session["Uid"].ToString();
            param[7] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[7].Value = Avail.CNP_NAME;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_VACANT_SEAT_DETAILS", param))
            {
                while (reader.Read())
                {

                    AvData = new SpaceAvailData();
                    AvData.CNY_NAME = reader["CNY_NAME"].ToString();
                    AvData.CTY_NAME = reader["CTY_NAME"].ToString();
                    AvData.LCM_CODE = reader["LCM_CODE"].ToString();
                    AvData.LCM_NAME = reader["LCM_NAME"].ToString();
                    AvData.TWR_NAME = reader["TWR_NAME"].ToString();
                    AvData.FLR_NAME = reader["FLR_NAME"].ToString();
                    AvData.SPC_ID = reader["SPC_ID"].ToString();
                    AvData.CNP_NAME = reader["CNP_NAME"].ToString();
                    VMlist.Add(AvData);
                }
                reader.Close();
            }
            return VMlist;
        }
        catch
        {
            throw;
        }

    }
    public object GetSpaceAvailabilityChart(SpaceAvailDetails Avail)
    {
        try
        {
            List<SpaceAvailData> Spcdata = new List<SpaceAvailData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_GRAPH");
            sp.Command.AddParameter("@COMPANYID", Avail.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}