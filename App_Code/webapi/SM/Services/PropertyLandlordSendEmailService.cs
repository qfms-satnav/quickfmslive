﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for LeaseEscalationService
/// </summary>
public class PropertyLandlordSendEmailService
{
    SubSonic.StoredProcedure sp;
    public string GetPropertyType()
    {

        List<DataRow> propertyRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_PROPTYPE").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
        // List<DataRow> rolesRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_GET_LEASE_ESC_ROLES").GetDataSet().Tables[0].Select().AsEnumerable().ToList();

        List<PropertyType> propertylist = new List<PropertyType>();
        List<ROLE> rolelist = new List<ROLE>();
        PropertyType pt;
        foreach (DataRow dr in propertyRows)
        {
            pt = new PropertyType();
            pt.PN_TYPEID = (int)dr["PN_TYPEID"];
            pt.PN_PROPERTYTYPE = dr["PN_PROPERTYTYPE"].ToString();
            propertylist.Add(pt);
        }

        return JsonConvert.SerializeObject(propertylist);
    }


    public string GetProperty(int PN_TYPEID)
    {

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_PROPERTYNAMES_AS_PROTYPE");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["uid"]);
        sp.Command.Parameters.Add("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"]);
        sp.Command.Parameters.Add("@PROPERTYTYPE", PN_TYPEID);
        List<PropertyOwnerSendEmail> propertyOwner = new List<PropertyOwnerSendEmail>();
        using (IDataReader property = sp.GetReader())
        {
            // reader.op

            while (property.Read())
            {
                propertyOwner.Add(new PropertyOwnerSendEmail()
                {
                    PM_PPT_NAME = property["PM_PPT_NAME"].ToString(),
                    PM_PPT_SNO = Convert.ToInt32(property["PM_PPT_SNO"].ToString())
                });
            }
            property.Close();
        }

        return JsonConvert.SerializeObject(propertyOwner);
    }

    public string GetLLOwnerName(int PM_PPT_SNO)
    {

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_LANDLORD_AS_PROPERTY");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["uid"]);
        sp.Command.Parameters.Add("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"]);
        sp.Command.Parameters.Add("@PROPERTY", PM_PPT_SNO);
        List<LandLordOwner> LLOwner = new List<LandLordOwner>();
        using (IDataReader property = sp.GetReader())
        {
            // reader.op

            while (property.Read())
            {
                LLOwner.Add(new LandLordOwner()
                {
                    LL_Name = property["PM_LL_NAME"].ToString(),
                    LL_Email = property["PM_LL_EMAIL"].ToString()
                });
            }
            property.Close();
        }

        return JsonConvert.SerializeObject(LLOwner);
    }
    public void SendEmail(string email, string msg, string LLName )
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_SEND_MAIL_LL");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["uid"]);
            sp.Command.Parameters.Add("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"]);
            sp.Command.Parameters.Add("@PROP_NAME", LLName);
            sp.Command.Parameters.Add("@To_EMAILID", email);
            sp.Command.Parameters.Add("@msg", msg);
            Object o = sp.ExecuteScalar();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message.ToString());
        }
    }
}