﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for DuplicateRecordsReportService
/// </summary>
public class DuplicateRecordsReportService
{

    DuplicateGrid DGrid;

    public List<DuplicateGrid> GetData(DuplicateRecordVM obj)
    {

        SqlParameter[] param1 = new SqlParameter[1];
        param1[0] = new SqlParameter("@FLR_LST", SqlDbType.Structured);
        param1[0].Value = UtilityService.ConvertToDataTable(obj.flrlst);

        List<DuplicateGrid> DGridLst = new List<DuplicateGrid>();
        using (SqlDataReader Drlst = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_DUPLICATE_RECORDS", param1))
        {
            while (Drlst.Read())
            {
                DGrid = new DuplicateGrid();
                DGrid.LCM_NAME = Drlst["LCM_NAME"].ToString();
                DGrid.SSA_SPC_ID = Drlst["SSA_SPC_ID"].ToString();
                DGrid.VER_NAME = Drlst["VER_NAME"].ToString();
                DGrid.COST_CENTER_NAME = Drlst["COST_CENTER_NAME"].ToString();
                DGrid.VER_CHE_CODE = Drlst["VER_CHE_CODE"].ToString();
                DGrid.SSAD_AUR_ID = Drlst["SSAD_AUR_ID"].ToString();
                DGrid.AUR_KNOWN_AS = Drlst["AUR_KNOWN_AS"].ToString();
                DGridLst.Add(DGrid);
            }
        }
        return DGridLst;
    }
}