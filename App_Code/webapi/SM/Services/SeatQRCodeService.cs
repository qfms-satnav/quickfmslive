﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using System.Drawing;
using System.IO;
using System.Web.Http;

public class SeatQRCodeService : ApiController
{

    public object GetQRCodeDetails(SeatQRCode data)
    {

        //QRCodeLst = new List<SeatQRCode>();
        try
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(data.flrlst);
            param[1] = new SqlParameter("@Type", SqlDbType.Int);
            param[1].Value = data.Type;
            param[2] = new SqlParameter("@Companyid", SqlDbType.Int);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "QR_CODE_READER", param);
            if (dt.Rows.Count > 0)
            {
                return new { data = dt, Message = "Data loaded successfully" };
            }
            else
            {
                return new { data = (object)null, Message = MessagesVM.UM_NO_REC };
            }

        }
        catch (Exception ex)
        {
            return new { data = (object)null, Message = MessagesVM.UM_NO_REC };
        }
    }


   



}
