﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for HotDeskRequisitionService
/// </summary>
public class HotDeskRequisitionService
{
    SubSonic.StoredProcedure sp;

    public object GetEmployeeDetails()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_EMPLOYEE_DETAILS_FOR_HOTDESKING");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] }; ;
    }
    public object GetEmployeeDetails(string AUR_ID)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_EMPLOYEE_DETAILS_FOR_HOTDESKING");
        sp.Command.Parameters.Add("@AUR_ID", AUR_ID, DbType.String);
        ds = sp.GetDataSet();
        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] }; ;
    }
    public object EmpChange(string AUR_ID)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "CHECK_EMPLOYEE_STATUS");
        sp.Command.Parameters.Add("@AUR_ID", AUR_ID, DbType.String);
        ds = sp.GetDataSet();
        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] }; ;
    }

    public object GetVerCosByEmpName(string AUR_ID)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_VERTICAL_COSTCENTER_BY_AUR_ID");
        sp.Command.Parameters.Add("@AUR_ID", AUR_ID, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", 1, DbType.String);

        ds = sp.GetDataSet();
        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] }; ;
    }

    public List<HD_Dtails> GetVacantSeats(MoveDetails MD)
    {

        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[0].Value = MD.LCMlst != null ? UtilityService.ConvertToDataTable(MD.LCMlst) : null;
        param[1] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[1].Value = MD.twrlst != null ? UtilityService.ConvertToDataTable(MD.twrlst) : null;
        param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[2].Value = MD.flrlst != null ? UtilityService.ConvertToDataTable(MD.flrlst) : null;

        List<HD_Dtails> HDS = new List<HD_Dtails>();
        HD_Dtails hd;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_VACANT_HOT_DESKING_SEATS", param))
        {

            while (sdr.Read())
            {
                hd = new HD_Dtails();
                hd.SPC_ID = Convert.ToString(sdr["SPC_ID"]);
                hd.Seat_Type = Convert.ToString(sdr["SPACE_TYPE"]);
                hd.ticked = false;
                HDS.Add(hd);
            }
        }
        return HDS;
    }
    public object GetSelectedfloors(MoveDetails MD)
    {

        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[0].Value = MD.LCMlst != null ? UtilityService.ConvertToDataTable(MD.LCMlst) : null;
        param[1] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[1].Value = MD.twrlst != null ? UtilityService.ConvertToDataTable(MD.twrlst) : null;
        param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[2].Value = MD.flrlst != null ? UtilityService.ConvertToDataTable(MD.flrlst) : null;
        MoveDetails MS = new MoveDetails();
        MS.flrlst = new List<Floorlst>();
        HD_Dtails hd;
        using (DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_SELECTED_FLOORS", param))
        {

            if (ds.Tables[0].Rows.Count > 0)
            {
                MS.flrlst = new List<Floorlst>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    MS.flrlst.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["FLR_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["FLR_TWR_ID"]), LCM_CODE = Convert.ToString(dr["FLR_LOC_ID"]), CTY_CODE = Convert.ToString(dr["FLR_CTY_ID"]), CNY_CODE = Convert.ToString(dr["FLR_CNY_ID"]), ticked = true });
                }
            }
        }
        return new { Message = MessagesVM.UM_OK, selectedfloors = MS.flrlst };
    }
    public object RaiseRequst(HotDeskRequisitionVM HD)
    {

        DataSet ds = new DataSet();


        SqlParameter[] param = new SqlParameter[10];

        param[0] = new SqlParameter("@HDLST", SqlDbType.Structured);
        param[0].Value = HD.HDLN != null ? UtilityService.ConvertToDataTable(HD.HDLN) : null;
        param[1] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[1].Value = HD.LCMlst != null ? UtilityService.ConvertToDataTable(HD.LCMlst) : null;
        param[2] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[2].Value = HD.twrlst != null ? UtilityService.ConvertToDataTable(HD.twrlst) : null;
        param[3] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[3].Value = HD.flrlst != null ? UtilityService.ConvertToDataTable(HD.flrlst) : null;
        param[4] = new SqlParameter("@REM", SqlDbType.VarChar);
        param[4].Value = HD.REM;
        param[5] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[5].Value = HttpContext.Current.Session["UID"];
        param[6] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
        param[6].Value = HD.ALLOCSTA;
        param[7] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
        param[7].Value = HD.REQ_ID;
        param[8] = new SqlParameter("@L1_REM", SqlDbType.VarChar);
        param[8].Value = HD.L1_REM;
        param[9] = new SqlParameter("@L2_REM", SqlDbType.VarChar);
        param[9].Value = HD.L2_REM;

        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "REQUEST_HOT_DESKING_SEATS_NEWEMP", param);





        if (ds.Tables.Count != 0)
        {
            string RetMessage = string.Empty;
            RequestState sta = (RequestState)HD.ALLOCSTA;
            switch (sta)
            {
                case RequestState.Added:
                    RetMessage = MessagesVM.BSR_INSERTED;
                    break;
                case RequestState.Modified:
                    RetMessage = MessagesVM.BSR_UPDATED;
                    break;
                case RequestState.Canceled:
                    RetMessage = MessagesVM.BSR_CANCELED;
                    break;
                case RequestState.Approved:
                    RetMessage = MessagesVM.BSR_APPROVED;
                    break;
                case RequestState.Rejected:
                    RetMessage = MessagesVM.BSR_REJECTED;
                    break;

            }
            SendMailHotDeskRequisition(ds.Tables[0].Rows[0]["REQ_ID"].ToString(), HD.ALLOCSTA);
            return new { Message = RetMessage + " For : " + ds.Tables[0].Rows[0]["REQ_ID"] };

        }

        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object RaiseRequstValidatin(HotDeskRequisitionVM HD)
    {

        DataSet ds = new DataSet();


        SqlParameter[] param = new SqlParameter[10];

        param[0] = new SqlParameter("@HDLST", SqlDbType.Structured);
        param[0].Value = HD.HDLN != null ? UtilityService.ConvertToDataTable(HD.HDLN) : null;
        param[1] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[1].Value = HD.LCMlst != null ? UtilityService.ConvertToDataTable(HD.LCMlst) : null;
        param[2] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[2].Value = HD.twrlst != null ? UtilityService.ConvertToDataTable(HD.twrlst) : null;
        param[3] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[3].Value = HD.flrlst != null ? UtilityService.ConvertToDataTable(HD.flrlst) : null;
        param[4] = new SqlParameter("@REM", SqlDbType.VarChar);
        param[4].Value = HD.REM;
        param[5] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[5].Value = HttpContext.Current.Session["UID"];
        param[6] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
        param[6].Value = HD.ALLOCSTA;
        param[7] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
        param[7].Value = HD.REQ_ID;
        param[8] = new SqlParameter("@L1_REM", SqlDbType.VarChar);
        param[8].Value = HD.L1_REM;
        param[9] = new SqlParameter("@L2_REM", SqlDbType.VarChar);
        param[9].Value = HD.L2_REM;

        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "REQUEST_HOT_DESKING_SEATS_NEWEMP_VALIDATIN", param);

        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };

    }
    public void SendMailHotDeskRequisition(string REQUEST_ID, int FLAG)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_HOTDESK_REQUISITION_REQUEST");
            sp.Command.AddParameter("@REQID", REQUEST_ID, DbType.String);
            sp.Command.AddParameter("@STATUS", FLAG, DbType.Int32);
            sp.ExecuteScalar();
        }
        catch
        {

            throw;
        }
    }


}

