﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ScheduleMySeatBookingsService
/// </summary>
public class ScheduleMySeatBookingsService
{
    SubSonic.StoredProcedure sp;


    DataSet ds;

    public object SearchSpaces(ScheduleMySeatBookingsModel Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DETAILS_BY_DATE");
            sp.Command.Parameters.Add("@From_Date", Data.FROM_DATE, DbType.Date);
            sp.Command.Parameters.Add("@To_Date", Data.TO_DATE, DbType.Date);
            sp.Command.Parameters.Add("@FLR_ID", Data.flrlst, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;

        }
        catch (Exception e)
        {
            throw;
        }
    }
    public object GetShifts(List<Locationlst> Loc)
    {
        List<Shiftlst> Shflst = new List<Shiftlst>();
        Shiftlst shf;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Loc);

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SHIFTS_BY_LOC", param))
        {
            while (sdr.Read())
            {
                shf = new Shiftlst();
                shf.SH_CODE = sdr["SH_CODE"].ToString();
                shf.SH_NAME = sdr["SH_NAME"].ToString();
                shf.SH_LOC_ID = sdr["SH_LOC_ID"].ToString();
                shf.SH_SEAT_TYPE = sdr["SH_SEAT_TYPE"].ToString();
                shf.REP_COL = sdr["REP_COL"].ToString();
                shf.SH_FRM_HRS = sdr["SH_FRM_HRS"].ToString();
                shf.SH_TO_HRS = sdr["SH_TO_HRS"].ToString();
                shf.ticked = false;
                Shflst.Add(shf);
            }
            sdr.Close();
            if (Shflst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Shflst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public int AllocateSeats(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        DataSet ds = new DataSet();

        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.ToList();


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();
        SPACE_ALLOC_DETAILS_TIME spc;

        Object result = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP11", param);

        return Convert.ToInt32(result);

        //using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP11", param))
        //{
        //    while (sdr.Read())
        //    {

        //    }
        //}

        //return 1;

    }



    public object allocateSeatsValidation(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();
        SPACE_ALLOC_DETAILS_TIME spc;
        DataSet ds = new DataSet();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP_VALIDATIONS", param))
        {
            while (sdr.Read())
            {
                spc = new SPACE_ALLOC_DETAILS_TIME();
                spc.SPC_ID = sdr["SSAD_SPC_ID"].ToString();
                spc.FROM_DATE = Convert.ToDateTime(sdr["SSAD_FROM_DATE"]);
                spc.TO_DATE = Convert.ToDateTime(sdr["SSAD_TO_DATE"]);
                spc.SH_CODE = sdr["SH_NAME"].ToString();
                spc.AUR_ID = sdr["SSAD_AUR_ID"].ToString();
                spc.VERTICAL = sdr["AUR_KNOWN_AS"].ToString();
                spcdet.Add(spc);
            }
        }
        if (spcdet.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcdet };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS sad)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_BY_SHIFT");
        sp.Command.Parameters.Add("@SPC_ID", sad.SPC_ID);
        sp.Command.Parameters.Add("@FDATE", sad.FROM_DATE);
        sp.Command.Parameters.Add("@TDATE", sad.TO_DATE);
        sp.Command.Parameters.Add("@SH_CODE", sad.SH_CODE);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);


        using (IDataReader dr = sp.GetReader())
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = dr["MSG"].ToString(), data = 1 };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }



    public object getEmployeeSpaceDetails(ScheduleMySeatBookingsModel Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "getEmployeeSpaceDetails");
            sp.Command.Parameters.Add("@AUR_ID",Data.AUR_ID, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;

        }
        catch (Exception e)
        {
            throw;
        }
    }

    public object GetSpaceDetailsByREQID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACE_SCHEDULE");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.category, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }


}