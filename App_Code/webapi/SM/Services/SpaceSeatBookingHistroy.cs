﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Newtonsoft.Json;
/// <summary>
/// Summary description for SpaceSeatBookingHistroy
/// </summary>
public class SpaceSeatBookingHistroy
{
    public object SpaceSeatBookingHistry()
    {

        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["Uid"];
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SPACE_SEAT_BOOKING_HISTORY", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (SqlException) { throw; }

    }

    public object allocateSeatsValidation(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];

        List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();
        SPACE_ALLOC_DETAILS_TIME spc;
        DataSet ds = new DataSet();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[SMS_SPACE_ALLOC_MAP_VALIDATION_FOR_AUR_ID]", param))
        {
            while (sdr.Read())
            {
                spc = new SPACE_ALLOC_DETAILS_TIME();
                spc.SPC_ID = sdr["SSAD_SPC_ID"].ToString();
                spc.FROM_DATE = Convert.ToDateTime(sdr["SSAD_FROM_DATE"]);
                spc.TO_DATE = Convert.ToDateTime(sdr["SSAD_TO_DATE"]);
                spc.SH_CODE = sdr["SSAD_SH_CODE"].ToString();
                spc.AUR_ID = sdr["SSAD_AUR_ID"].ToString();
                //spc.VERTICAL = sdr["AUR_KNOWN_AS"].ToString();
                spcdet.Add(spc);
            }
        }
        if (spcdet.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcdet };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
}