﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UploadSpaceAllocationService
/// </summary>
public class UploadSpaceAllocationService
{
    public List<UploadAllocationDataDumpVM> DownloadTemplate(UploadSpacesVM spcdet)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@DWNTYPE", SqlDbType.VarChar);
        param[2].Value = spcdet.ALLOCSTA;
        param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];

        UploadAllocationDataDumpVM dwndata;
        List<UploadAllocationDataDumpVM> dwndatalst = new List<UploadAllocationDataDumpVM>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_DWNLD_ALLOC_TEMPLATE", param))
        {
            while (sdr.Read())
            {
                dwndata = new UploadAllocationDataDumpVM();
                dwndata.City = sdr["City"].ToString();
                dwndata.Location = sdr["Location"].ToString();
                dwndata.Tower = sdr["Tower"].ToString();
                dwndata.Floor = sdr["Floor"].ToString();
                dwndata.SpaceID = sdr["SpaceID"].ToString();
                dwndata.SeatType = sdr["SeatType"].ToString();
                dwndata.Vertical = sdr["Vertical"].ToString();
                dwndata.Entity = sdr["Entity"].ToString();
                dwndata.Costcenter = sdr["Costcenter"].ToString();
                dwndata.EmployeeID = sdr["EmployeeID"].ToString();
                dwndata.EmployeeName = sdr["EmployeeName"].ToString();
                dwndata.FromDate = sdr["FromDate"].ToString();
                dwndata.ToDate = sdr["ToDate"].ToString();
                dwndata.FromTime = sdr["FromTime"].ToString();
                dwndata.ToTime = sdr["ToTime"].ToString();
                dwndatalst.Add(dwndata);
            }
            sdr.Close();
        }

        return dwndatalst;

    }

    public object UploadTemplate(HttpRequest httpRequest)
    {
        List<UploadAllocationDataVM> uadmlst;
        try
        {
            uadmlst = GetDataTableFrmReq(httpRequest);

            if (HttpContext.Current.Session["ErrorMsg"].ToString() == "")
            {
                String jsonstr = httpRequest.Params["CurrObj"];

                UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);

                string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_SPACEALLOCATION_DATA (CITY,LOCATION,TOWER,FLOOR,SPACE_ID,SEATTYPE,VERTICAL,ENTITY,COSTCENTER,EMP_ID,EMP_NAME,FROM_DATE,TO_DATE,FROM_TIME,TO_TIME,UPLOADEDBY) VALUES ";
                string str1 = "";
                int retval, cnt = 1;
                foreach (UploadAllocationDataVM uadm in uadmlst)
                {


                    if ((cnt % 1000) == 0)
                    {
                        str1 = str1.Remove(str1.Length - 1, 1);
                        retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                        if (retval == -1)
                        {
                            return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                        }
                        else
                        {
                            str1 = "";
                            cnt = 1;
                        }

                    }

                    str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}'),",
                              uadm.City,
                              uadm.Location.Replace("'", "''"),
                              uadm.Tower.Replace("'", "''"),
                              uadm.Floor,
                              uadm.SpaceID,
                              uadm.SeatType,
                              uadm.Vertical.Replace("'", "''"),
                              uadm.Entity,
                              uadm.Costcenter.Replace("'", "''"),
                              uadm.EmployeeID,
                              uadm.EmployeeName.Replace("'", "''"),
                              uadm.FromDate,
                              uadm.ToDate,
                              uadm.FromTime,
                              uadm.ToTime,
                              HttpContext.Current.Session["Uid"]

                            );
                    cnt = cnt + 1;
                }
                str1 = str1.Remove(str1.Length - 1, 1);
                retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                if (retval != -1)
                {
                    SqlParameter[] param = new SqlParameter[4];
                    param[0] = new SqlParameter("@UPLTYPE", SqlDbType.VarChar);
                    param[0].Value = UVM.UplAllocType;
                    param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                    param[1].Value = HttpContext.Current.Session["UID"];
                    param[2] = new SqlParameter("@UPLOPTIONS", SqlDbType.VarChar);
                    param[2].Value = UVM.UplOptions;
                    param[3] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                    param[3].Value = HttpContext.Current.Session["COMPANYID"];
                    DataSet dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_UPLOAD_SPACE_ALLOC", param);
                    if (dt.Tables != null)
                        return new { Message = MessagesVM.UAD_UPLOK, data = dt };
                    else
                        return new { Message = MessagesVM.UAD_UPLVALDAT, data = dt };
                }
                else
                    return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
            }
            else
            {
                return new { Message = HttpContext.Current.Session["ErrorMsg"].ToString(),  data = (object)null };
            }
        }
        catch (Exception ex)
        {
            SqlHelper.ExecuteScalar(CommandType.Text, "TRUNCATE TABLE " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_SPACEALLOCATION_DATA");
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
           
        }
    }

    string ErrorMsg = "";
    public List<UploadAllocationDataVM> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        //DataTable dt = new DataTable();
        //DataSet dt = new DataSet();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
        var uplst = CreateUploadSpaceAllocExcel.UploadReadAsList(filePath);
        if (uplst.Count != 0)
        {

            uplst.RemoveAt(0);
        }
        return uplst;

    }

    public class CheckExcelColumnNames
    {
        public static string City = "City";
        public static string Location = "Location";
        public static string Tower = "Tower";
        public static string Floor = "Floor";
        public static string SpaceID = "Space ID";
        public static string SeatType = "Seat Type";
        public static string Vertical = "Business Unit";
        public static string Entity = "Entity";
        public static string Costcenter = "Costcenter";
        public static string EmployeeID = "Employee ID";
        public static string EmployeeName = "Employee Name";
        public static string FromDate = "From Date (in MM/dd/yyyy)";
        public static string ToDate = "To Date  (in MM/dd/yyyy)";
        public static string FromTime = "From Time  (in  HH:MM)";
        public static string ToTime = "To Time in (HH:MM)";
    }
    public List<SpaceRoster> DownloadTemplateForRostering(UploadSpacesVM spcdet)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@DWNTYPE", SqlDbType.VarChar);
            param[2].Value = spcdet.ALLOCSTA;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            List<SpaceRoster> dwndatalst = new List<SpaceRoster>();

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_DWNLD_ALLOC_TEMPLATE_ROSTER1", param))
            {
                while (sdr.Read())
                {
                    var dwndata = new SpaceRoster();
                    dwndata.City = sdr["City"].ToString();
                    dwndata.Location = sdr["Location"].ToString();
                    dwndata.Tower = sdr["Tower"].ToString();
                    dwndata.Floor = sdr["Floor"].ToString();
                    dwndata.SpaceID = sdr["SpaceID"].ToString();
                    dwndata.SeatType = sdr["SeatType"].ToString();
                    dwndata.Vertical = sdr["Vertical"].ToString();
                    dwndata.Costcenter = sdr["Costcenter"].ToString();
                    dwndata.EmployeeID = sdr["EmployeeID"].ToString();
                    dwndata.EmployeeName = sdr["EmployeeName"].ToString();
                    dwndata.FromDate = sdr["FromDate"].ToString();
                    dwndata.ToDate = sdr["ToDate"].ToString();
                    dwndata.Shift = "";
                    dwndatalst.Add(dwndata);
                }
                sdr.Close();
            }

            return dwndatalst;
        }
        catch (Exception ex)
        {
            return null;
        }

    }
    public List<ShiftInformation> GetShiftInformationBasedOnLocation(UploadSpacesVM spcdet)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];

        var shiftList = new List<ShiftInformation>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_SHIFT_ROSTERING", param))
        {
            while (sdr.Read())
            {
                var shiftData = new ShiftInformation();
                shiftData.Shift_Code = sdr["SH_CODE"].ToString();
                shiftData.Shift_Name = sdr["SH_NAME"].ToString();
                shiftData.Shift_Location = sdr["SH_LOC_ID"].ToString();
                shiftData.Shift_Lcm_Name = sdr["LCM_NAME"].ToString();
                shiftData.Shift_SeatType = sdr["SH_SEAT_TYPE"].ToString();
                shiftData.Shift_FromTime = sdr["SH_FRM_HRS"].ToString();
                shiftData.Shift_ToTime = sdr["SH_TO_HRS"].ToString();

                shiftList.Add(shiftData);
            }

            sdr.Close();
        }

        return shiftList;

    }
    public object UploadTemplateForRostering(HttpRequest httpRequest)
    {

        try
        {
            var uadmlst = GetUploadedDataFrmReq(httpRequest);

            ExcelDataValidation.ServerSideValidation(uadmlst);

            if (HttpContext.Current.Session["ErrorMsg"].ToString() == "")
            {
                String jsonstr = httpRequest.Params["CurrObj"];

                UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);

                string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_SCHEDULESEATBOOKINGS_DATA (CITY,LOCATION,TOWER,FLOOR,SPACE_ID,SEATTYPE,VERTICAL,ENTITY,COSTCENTER,EMP_ID,EMP_NAME,FROM_DATE,TO_DATE,FROM_TIME,TO_TIME,Shift_Name,ROSTER_TYPE,SERVER_REMARKS,BOOKING_DATES,EXCEL_ROW_NO,UPLOADEDBY) VALUES ";
                string str1 = "";
                int retval, cnt = 1;
                foreach (var uadm in uadmlst)
                {

                    if ((cnt % 1000) == 0)
                    {
                        str1 = str1.Remove(str1.Length - 1, 1);
                        retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                        if (retval == -1)
                        {
                            return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                        }
                        else
                        {
                            str1 = "";
                            cnt = 1;
                        }

                    }

                    str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','20'),",
                              uadm.City,
                              uadm.Location.Replace("'", "''"),
                              uadm.Tower.Replace("'", "''"),
                              uadm.Floor,
                              uadm.SpaceID,
                              uadm.SeatType,
                              uadm.Vertical.Replace("'", "''"),
                              "",//entity
                              uadm.Costcenter.Replace("'", "''"),
                              uadm.EmployeeID,
                              uadm.EmployeeName.Replace("'", "''"),
                              uadm.FromDate,
                              uadm.ToDate,
                              uadm.FromTime,
                              uadm.ToTime,
                              uadm.Shift,
                              "",//roster type
                              uadm.ServerRemarks,
                              uadm.ValidDatesOfBooking,
                              uadm.Excel_Row_No,
                              HttpContext.Current.Session["Uid"]

                            );
                    cnt = cnt + 1;
                }
                str1 = str1.Remove(str1.Length - 1, 1);
                retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                if (retval != -1)
                {
                    SqlParameter[] param = new SqlParameter[4];
                    param[0] = new SqlParameter("@UPLTYPE", SqlDbType.VarChar);
                    param[0].Value = UVM.UplAllocType;
                    param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                    param[1].Value = HttpContext.Current.Session["UID"];
                    param[2] = new SqlParameter("@UPLOPTIONS", SqlDbType.VarChar);
                    param[2].Value = UVM.UplOptions;
                    param[3] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                    param[3].Value = HttpContext.Current.Session["COMPANYID"];
                    DataSet dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_UPLOAD_SPACE_Roster", param);
                    if (dt.Tables != null)
                        return new { Message = MessagesVM.UAD_UPLOK, data = dt };
                    else
                        return new { Message = MessagesVM.UAD_UPLVALDAT, data = dt };
                }
                else
                    return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
            }
            else
            {
                return new { Message = HttpContext.Current.Session["ErrorMsg"].ToString(), data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };

        }
    }
    public List<SpaceRoster> GetUploadedDataFrmReq(HttpRequest httpRequest)
    {
        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        var uplst = CreateRosterExcel.RosterReadAsList(filePath);

        if (uplst.Count != 0)
        {
            uplst.RemoveAt(0);
        }

        return uplst;

    }


}

public class CheckExcelColumnNames
{
    public static string City = "City";
    public static string Location = "Location";
    public static string Tower = "Tower";
    public static string Floor = "Floor";
    public static string SpaceID = "Space ID";
    public static string SeatType = "Seat Type";
    public static string Vertical = "Business Unit";
    public static string Entity = "Entity";
    public static string Costcenter = "Costcenter";
    public static string EmployeeID = "Employee ID";
    public static string EmployeeName = "Employee Name";
    public static string FromDate = "From Date (in MM/dd/yyyy)";
    public static string ToDate = "To Date  (in MM/dd/yyyy)";
    public static string FromTime = "From Time  (in  HH:MM)";
    public static string ToTime = "To Time in (HH:MM)";
}
public static class ExcelDataValidation
{
    public static Dictionary<string, string[]> DaysDictionary = new Dictionary<string, string[]>()
    {
        {"monday", new string[] { "monday", "mon","1" } },
        {"tuesday", new string[] { "tuesday", "tue","2" } },
        {"wednesday", new string[] { "wednesday", "wed","3" } },
        {"thursday", new string[] { "thursday", "thu","4" } },
        {"friday", new string[] { "friday", "fri","5" } },
        {"saturday", new string[] { "saturday", "sat","6" } },
        {"sunday", new string[] { "sunday", "sun","7" } },
    };

    public static void ServerSideValidation(List<SpaceRoster> ExcelUploadedItem)
    {
        int counter = 0;
        foreach (var exccelUpload in ExcelUploadedItem)
        {
            exccelUpload.Excel_Row_No = counter++;
            var validationMessage = new StringBuilder();

            if (string.IsNullOrEmpty(exccelUpload.FromDate) || string.IsNullOrEmpty(exccelUpload.ToDate) || string.IsNullOrEmpty(exccelUpload.Shift))
            {
                validationMessage.Append("FromDate,ToDate,Shift is mandatory;");
            }
            if (string.IsNullOrEmpty(exccelUpload.SpaceID))
            {
                validationMessage.Append("SpaceId null;");
            }
            if (string.IsNullOrEmpty(exccelUpload.EmployeeID))
            {
                validationMessage.Append("EmployeeId null;");
            }

            if (validationMessage.Length != 0)
            {
                exccelUpload.ServerRemarks = validationMessage.ToString();
                continue;
            }

            DateTime FDate;
            DateTime TDate;
            TimeSpan F_Time;
            TimeSpan T_time;

            if (!DateTime.TryParse(exccelUpload.FromDate, out FDate))
            {
                validationMessage.Append("Invalid FromDate");
            }

            if (!DateTime.TryParse(exccelUpload.ToDate, out TDate))
            {
                validationMessage.Append("Invalid ToDate");
            }


            if (FDate != null && TDate != null && TDate < FDate)
            {
                validationMessage.Append("Todate lessthen Fromdate");
            }

            if (validationMessage.Length != 0)
            {
                exccelUpload.ServerRemarks = validationMessage.ToString();
                continue;
            }

            var bookingDatesByRoster = ExcelDataValidation.getBookingDatesByRoster(FDate, TDate);

            exccelUpload.ValidDatesOfBooking = string.Join(",", bookingDatesByRoster);

        }

    }
    public static List<string> IsValidRoster(string DaysString, out bool ValidRoster)
    {
        ValidRoster = true;
        var validDaysList = new List<string>();
        var excelDays = DaysString.Split(',');

        foreach (var excelday in excelDays)
        {
            var getDay = DaysDictionary.FirstOrDefault(x => x.Value.Contains(excelday.ToLower().Trim()));
            if (getDay.Key != null)
            {
                validDaysList.Add(getDay.Key);
            }
            else
            {
                ValidRoster = false;
                break;
            }
        }

        return validDaysList;
    }

    public static List<string> getBookingDatesByRoster(DateTime fromDate, DateTime todate)
    {

        DateTime startDate = fromDate;
        DateTime endDate = todate;

        List<string> list = new List<string>();
        for (DateTime runDate = startDate; runDate <= endDate; runDate = runDate.AddDays(1))
        {
            //if (dayNames.Contains(runDate.DayOfWeek.ToString().ToLower()))
            //{
            //    var stringDate = runDate.ToShortDateString();
            //    list.Add(stringDate);
            //}
            var stringDate = runDate.ToShortDateString();
            list.Add(stringDate);
        }

        return list;

    }

}

