﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Linq;


/// <summary>
/// Summary description for Space_MapService
/// </summary>
public class Space_MapService
{

    SubSonic.StoredProcedure sp;
    DataSet ds;
    ActiveDirectoryHelper adhelper = new ActiveDirectoryHelper();

    public DataTable GetallFloorsbyuser()
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_FLOORS_FOR_MAP");
        sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable Getfloordetails(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_MAP_HEADING");
        sp.Command.Parameters.Add("@LOC_CODE", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@MODE", param.mode, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetallFilterbyItem()
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_ITEM_SM");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetallFilterbySubItem(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_SUBITEM_SM");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEMID", param.Item, DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", param.flr_code, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetallLocationbyURL(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_URL_LOCATION");
        sp.Command.Parameters.Add("@LOC_CODE", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", HttpUtility.HtmlEncode(param.twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", param.flr_code, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetQueryAnalycat()
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_MAP_QUERY_DDLCATEGORY");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetQueryAnalyValuebyCat(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_MAP_QUERY_DDLCATVALUE");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWRID", HttpUtility.HtmlEncode(param.twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLRID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@CATID", param.category, DbType.String);
        sp.Command.Parameters.Add("@MODE", param.mode, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetspcidbyEmp(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GETSPACEDETAILS_AURID");
        sp.Command.Parameters.Add("@AUR_ID", param.CatValue, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetFloorIDBBox(string lcm_code, string twr_code, string flr_code)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_FLR_FILTER_BBOX");
        sp.Command.Parameters.Add("@LOCID", lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWRID", HttpUtility.HtmlEncode(twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLRID", flr_code, DbType.String);
        sp.Command.Parameters.Add("@MODE", 1, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetFloorIDBBoxByID(string lcm_code, string twr_code, string flr_code)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_FLR_FILTER_BBOX");
        sp.Command.Parameters.Add("@LOCID", lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWRID", HttpUtility.HtmlEncode(twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLRID", flr_code, DbType.String);
        sp.Command.Parameters.Add("@MODE", 0, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetMapdetailsbyFilters(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_ALL_DATA_FLITER_SM");
        sp.Command.Parameters.Add("@LCM_CODE", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", HttpUtility.HtmlEncode(param.twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", param.Item, DbType.String);
        sp.Command.Parameters.Add("@SUBITEM", param.subitem, DbType.String);
        ds = sp.GetDataSet();

        if (param.Item == "3")
        {
            //List<ADUserDetail> useradlist = adhelper.GetUserslist();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AD_DATA_LIVE");
            DataSet DS1 = sp.GetDataSet();

            List<AD_DATA_VM> useradlist = DS1.Tables[0].AsEnumerable()
                  .Select(row => new AD_DATA_VM
                  {
                      USR_ID = row.Field<string>(0),
                      LOG_IN_TIME = row.Field<string>(1),
                      LOG_OUT_TIME = row.Field<string>(2)
                  }).ToList();

            List<Space_MapEmployeeVM> empmaplist = ds.Tables[0].AsEnumerable()
              .Select(row => new Space_MapEmployeeVM
              {
                  SPC_ID = row.Field<string>(0),
                  x = row.Field<double>(1).ToString(),
                  y = row.Field<double>(2).ToString(),
                  SPC_FLR_ID = row.Field<string>(3),
                  SPC_LAYER = row.Field<string>(4),
                  SPC_DESC = row.Field<string>(5),
                  STATUS = row.Field<int>(6).ToString(),
                  AD_ID = row.Field<string>(7)
              }).ToList();

            var list3 = (from Item1 in  empmaplist
                         join Item2 in useradlist
                         on Item1.AD_ID equals Item2.USR_ID // join on some property
                         into grouping
                         from Item2 in grouping.DefaultIfEmpty()
                         //where Item2 != null
                         select new { Item1, Item2 }).ToList();
            return list3;

        }

        return ds.Tables[0];
    }

    public DataTable GetMapdetails(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DATA_MAP_SM");
        sp.Command.Parameters.Add("@LCM_CODE", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", HttpUtility.HtmlEncode(param.twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", param.flr_code, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetSpaceTypedetails(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_TYPE_COUNT_SM");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWRID", HttpUtility.HtmlEncode(param.twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLRID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@MODE", param.mode, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public DataTable GetVerticalAllocation(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_VERTICAL_ALLOC_COUNT_SM");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWRID", HttpUtility.HtmlEncode(param.twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLRID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@MODE", param.mode, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetConsolidatedreport(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_ALLOCATION_SUMMARY_TEMP");
        sp.Command.Parameters.Add("@BDG_ID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_ID", HttpUtility.HtmlEncode(param.twr_code), DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@MODE", param.mode, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetMapbyQueryAnalyser(Space_mapVM param)
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "getSpaceDetails_space_queryanaly_aurid");
        sp.Command.Parameters.Add("@aur_id", param.CatValue, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

}