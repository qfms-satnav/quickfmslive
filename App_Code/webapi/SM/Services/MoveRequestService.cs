﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for MoveRequestService
/// </summary>
public class MoveRequestService
{
    SubSonic.StoredProcedure sp;
    public object GetSpaces(MoveDetails MD)
    {
        DataSet ds = new DataSet();
     
        SqlParameter[] param = new SqlParameter[15];
        param[0] = new SqlParameter("@CNYLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(MD.cnylst);
        param[1] = new SqlParameter("@CTYLST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(MD.ctylst);
        param[2] = new SqlParameter("@LCMLST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(MD.loclst);
        param[3] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(MD.twrlst);
        param[4] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(MD.flrlst);
        param[5] = new SqlParameter("@VERLST", SqlDbType.Structured);
        param[5].Value = UtilityService.ConvertToDataTable(MD.verlst);
        param[6] = new SqlParameter("@COSTLST", SqlDbType.Structured);
        param[6].Value = UtilityService.ConvertToDataTable(MD.cstlst);
        param[7] = new SqlParameter("@DESTCNYLST", SqlDbType.Structured);
        param[7].Value = UtilityService.ConvertToDataTable(MD.Destcnylst);
        param[8] = new SqlParameter("@DESTCTYLST", SqlDbType.Structured);
        param[8].Value = UtilityService.ConvertToDataTable(MD.Destctylst);
        param[9] = new SqlParameter("@DESTLCMLST", SqlDbType.Structured);
        param[9].Value = UtilityService.ConvertToDataTable(MD.Destloclst);
        param[10] = new SqlParameter("@DESTTWRLST", SqlDbType.Structured);
        param[10].Value = UtilityService.ConvertToDataTable(MD.Desttwrlst);
        param[11] = new SqlParameter("@DESTFLRLST", SqlDbType.Structured);
        param[11].Value = UtilityService.ConvertToDataTable(MD.Destflrlst);
        param[12] = new SqlParameter("@DESTVERLST", SqlDbType.Structured);
        param[12].Value = UtilityService.ConvertToDataTable(MD.Destverlst);
        param[13] = new SqlParameter("@DESTCOSTLST", SqlDbType.Structured);
        param[13].Value = UtilityService.ConvertToDataTable(MD.Destcstlst);
        param[14] = new SqlParameter("@Type", SqlDbType.Int);
        param[14].Value = MD.Type;

        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_DETAILS_FOR_MOVEMENT", param);


        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], DestSpaceids = ds.Tables[1],VacantSpaceids=ds.Tables[2] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object GetEmployeeDetails(string SPC_ID)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_EMPLOYEE_DETAILS_BY_SPCID");
        sp.Command.Parameters.Add("@SPC_ID", SPC_ID, DbType.String);
        ds = sp.GetDataSet();
        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0]}; ;
    }
    public object RaiseRequst(List<RequestDetails> details)
    {

        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@MOVELST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];


        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "INSERT_DETAILS_FOR_MOVEMENT", param);
        if(ds.Tables.Count !=0) 
        return new { data = "Request "+ds.Tables[0].Rows[0]["REQID"] +" raised sussessfully" };
        else
        return new { data ="Error occurs" };


    }
    public object GetRequests()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_PENDING_MOVE_DETAILS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] }; ;
    }

}