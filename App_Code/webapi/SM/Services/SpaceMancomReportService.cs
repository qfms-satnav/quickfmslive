﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceMancomReportService
/// </summary>
public class SpaceMancomReportService
{
    SubSonic.StoredProcedure sp;
    SpaceMancomReportModel spaceMancom;
    public object GetSpaceMancomReport()
    {
        try
        {
            DataTable dt = GetSpaceManDetails();
            if (dt.Rows.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = dt };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    private DataTable GetSpaceManDetails()
    {
        DataTable DT = new DataTable();
        try
        {
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[SMS_SPACE_MANCOM_REPORT2]");
            //HttpContext.Current.Session["AssetHistory"] = DT;
            return DT;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public List<SpaceMancomReportModel> GetMancomreport(Company cnp)
    {
        try
        {
            List<SpaceMancomReportModel> rptByUserlst = new List<SpaceMancomReportModel>();
            SpaceMancomReportModel rptByUser;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_MANCOM_REPORT2");
            

            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    rptByUser = new SpaceMancomReportModel();
                    rptByUser.MANCOM = sdr["MANCOM"].ToString();
                    rptByUser.SPC_TYPE = sdr["SPC_TYPE"].ToString();
                    rptByUser.TOTAL_SEATS = sdr["TOTAL_SEATS"].ToString();
                    rptByUser.OCCUPIED_SEATS = sdr["OCCUPIED_SEATS"].ToString();
                    rptByUser.ALLOCATED_VACANT_SEATS = sdr["ALLOCATED_VACANT_SEATS"].ToString();
                    rptByUser.TOTAL_SEATS = sdr["VACANT"].ToString();
                    rptByUserlst.Add(rptByUser);
                }
                sdr.Close();
            }
            if (rptByUserlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
                return rptByUserlst;
            else
                return new List<SpaceMancomReportModel>();
        }
        catch
        {
            throw;
        }

    }
}