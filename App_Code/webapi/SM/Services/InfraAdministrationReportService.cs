﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
/// <summary>
/// Summary description for InfraAdministrationReportService
/// </summary>
public class InfraAdministrationReportService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;


    public object GetInfraAdministrationReport()
    {
        try
        {

            List<GridCols> gridcols = new List<GridCols>();
            GridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPC_INFRA_ADMIN_REPORT");

            DataSet ds = sp.GetDataSet();

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            CreateExcelFile.CreateExcelDocument(ds, System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/[TATA_CAP].dbo/InfraAdministrationReport.xlsx"));
            foreach (string col in Colstr)
            {
                gridcolscls = new GridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcols.Add(gridcolscls);

                exportcls = new ExportClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
        }
        catch
        {
            throw;
        }
    }

    public object GetInfraAdministrationReportBySearch(getGridBySearch getGrid)
    {
        try
        {

            List<GridCols> gridcols = new List<GridCols>();
            GridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@LCMlst", SqlDbType.Structured);
            udParams[0].Value = UtilityService.ConvertToDataTable(getGrid.LCMLst);

            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPC_INFRA_ADMIN_REPORT");

            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SPC_INFRA_ADMIN_REPORT_BY_SEARCH", udParams);

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {
                gridcolscls = new GridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcols.Add(gridcolscls);

                exportcls = new ExportClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
        }
        catch
        {
            throw;
        }
    }


    public List<InfraAdminReportParameters> GetExportLst(InfraAdminReportParameters Params)
    {

        List<InfraAdminReportParameters> rptByLocationlst = new List<InfraAdminReportParameters>();
        InfraAdminReportParameters rptByLocation;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPC_INFRA_ADMIN_RDLC_REPORT");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByLocation = new InfraAdminReportParameters();
                rptByLocation.CITY = sdr["CITY"].ToString();
                rptByLocation.LOCATION = sdr["LCM_NAME"].ToString();
                rptByLocation.TOTAL_SEATS = sdr["CITY"].ToString();
                rptByLocation.On_Roll = sdr["LCM_NAME"].ToString();
                rptByLocation.Off_Roll = sdr["CITY"].ToString();
                rptByLocation.CRE = sdr["LCM_NAME"].ToString();
                rptByLocation.CRE_TOTAL = sdr["CITY"].ToString();
                rptByLocation.TOTAL = sdr["LCM_NAME"].ToString();
                rptByLocation.SPC_BDG_ID = sdr["CITY"].ToString();
                rptByLocation.VERTICAL_NAME = sdr["LCM_NAME"].ToString();
                rptByLocation.VERTICALCOUNT = sdr["CITY"].ToString();
                rptByLocation.Availability = sdr["LCM_NAME"].ToString();
                rptByLocationlst.Add(rptByLocation);
            }
            sdr.Close();
        }
        if (rptByLocationlst.Count != 0)
            return rptByLocationlst;
        else
            return null;
    }

}