﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for AddHerenceService
/// </summary>
public class UserBaseDataReportService
{
    SubSonic.StoredProcedure sp;

    public object GetData(UserBaseDataReportModel Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ADM_GET_USER_BASEDATA_REPORT_DETAILS");
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.Parameters.Add("@Location", Data.Locations, DbType.String);
            sp.Command.Parameters.Add("@Vertical", Data.Verticals, DbType.String);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables[0];
        }
        catch (Exception e)
        {
            throw;
        }
    }


}