﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for userUtilizationService
/// </summary>
public class userUtilizationService
{
    public object GetShifts()
    {
        List<Shiftlst> Shflst = new List<Shiftlst>();
        Shiftlst shf;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SHIFTS"))
        {
            while (sdr.Read())
            {
                shf = new Shiftlst();
                shf.SH_CODE = sdr["SH_CODE"].ToString();
                shf.SH_NAME = sdr["SH_NAME"].ToString();
                shf.SH_LOC_ID = sdr["SH_LOC_ID"].ToString();
                shf.SH_SEAT_TYPE = sdr["SH_SEAT_TYPE"].ToString();
                shf.REP_COL = sdr["REP_COL"].ToString();
                shf.ticked = false;
                Shflst.Add(shf);
            }
            sdr.Close();
            if (Shflst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Shflst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public List<userUtlzationGridDetails> GetGriddata(userUtlzationDetails userUtlzation)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(userUtlzation.Floors);
        param[1] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(userUtlzation.Locations);
        param[2] = new SqlParameter("@FDATE", SqlDbType.NVarChar);
        param[2].Value = userUtlzation.SRN_FROM_DATE == null ? "" : userUtlzation.SRN_FROM_DATE;
        param[3] = new SqlParameter("@TDATE", SqlDbType.NVarChar);
        param[3].Value = userUtlzation.SRN_TO_DATE == null ? "" : userUtlzation.SRN_TO_DATE;
        param[4] = new SqlParameter("@TWRLST", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(userUtlzation.Towers);
        param[5] = new SqlParameter("@SHIFTLST", SqlDbType.Structured);
        param[5].Value = UtilityService.ConvertToDataTable(userUtlzation.ShiftFilter);
        List<userUtlzationGridDetails> userUtlzationdetlst = new List<userUtlzationGridDetails>();
        userUtlzationGridDetails userUtlzationdet = new userUtlzationGridDetails();

        //using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SPACES_FOR_REQUISITION", param))
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_USERUTILIZTIONREPORT_BY_SEARCH", param))
        {
            while (sdr.Read())
            {
                userUtlzationdet = new userUtlzationGridDetails();
                userUtlzationdet.aur_id = sdr["aur_id"].ToString();
                userUtlzationdet.aur_host_name = sdr["aur_host_name"].ToString();
                userUtlzationdet.FLR_NAME = sdr["FLR_NAME"].ToString();
                userUtlzationdet.SPC_ID = sdr["SPC_ID"].ToString();
                userUtlzationdet.LOG_IN = sdr["LOG_IN"].ToString();
                userUtlzationdet.LOG_OFF = sdr["LOG_OFF"].ToString();
                userUtlzationdet.Utilztionhours = sdr["Utilztionhours"].ToString();
                userUtlzationdet.SHFT_NAME = sdr["SHFT_NAME"].ToString();
                userUtlzationdet.VER_NAME = sdr["VER_NAME"].ToString();
                userUtlzationdet.CC_NAME = sdr["CC_NAME"].ToString();
                userUtlzationdetlst.Add(userUtlzationdet);
            }
            sdr.Close();
        }
        return userUtlzationdetlst;
    }

    public object getUtilizationHoursGrid(userUtlzationDetails userUtlzation)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@FDATE", SqlDbType.NVarChar);
            param[0].Value = userUtlzation.SRN_FROM_DATE == null ? "" : userUtlzation.SRN_FROM_DATE;
            param[1] = new SqlParameter("@TDATE", SqlDbType.NVarChar);
            param[1].Value = userUtlzation.SRN_TO_DATE == null ? "" : userUtlzation.SRN_TO_DATE;
            List<GridCols> gridcols = new List<GridCols>();
            GridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_USER_UTIL_HRS_BY_DATE",param);

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {
                gridcolscls = new GridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcols.Add(gridcolscls);

                exportcls = new ExportClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
        }
        catch
        {
            throw;
        }
    }

    public List<User_UitlizationCount_Report> GetUtilizationHoursExcelReportrdlc(User_UitlizationCount_Report userUtlzation)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@FDATE", SqlDbType.NVarChar);
        param[0].Value = userUtlzation.SRN_FROM_DATE == null ? "" : userUtlzation.SRN_FROM_DATE;
        param[1] = new SqlParameter("@TDATE", SqlDbType.NVarChar);
        param[1].Value = userUtlzation.SRN_TO_DATE == null ? "" : userUtlzation.SRN_TO_DATE;
        List<User_UitlizationCount_Report> userUtlzationdetlst = new List<User_UitlizationCount_Report>();
        User_UitlizationCount_Report userUtlzationdet = new User_UitlizationCount_Report();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_USR_UTIL_HRS_BY_DATE_RDLC", param))
        {
            while (sdr.Read())
            {
                userUtlzationdet = new User_UitlizationCount_Report();
                userUtlzationdet.uli_usr = sdr["uli_usr"].ToString();
                userUtlzationdet.spc_id= sdr["spc_id"].ToString();
                userUtlzationdet.ULI_LOG_IN_DATE = sdr["ULI_LOG_IN_DATE"].ToString();
                userUtlzationdet.Utilztionhours = sdr["Utilztionhours"].ToString();
                userUtlzationdet.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                userUtlzationdetlst.Add(userUtlzationdet);
            }
            sdr.Close();
        }
        return userUtlzationdetlst;
    }

}