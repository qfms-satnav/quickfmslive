﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ViewVerticalRequisitionService
/// </summary>
public class ViewVerticalRequisitionService
{
    SubSonic.StoredProcedure sp;
    //Grid View
    public object GetMyReqList(int id)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "View_Vertical_Requisition_Details");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.Command.AddParameter("@MODE", id, DbType.Int32);
            using (IDataReader reader = sp.GetReader())
            {
                List<SMS_VERTICAL_REQUISITION> Verlist = new List<SMS_VERTICAL_REQUISITION>();
                while (reader.Read())
                {
                    Verlist.Add(new SMS_VERTICAL_REQUISITION()
                    {
                        SVR_REQ_ID = reader["SVR_REQ_ID"].ToString(),
                        AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                        SVR_REQ_DATE = (DateTime)reader["SVR_REQ_DATE"],
                        VER_NAME = reader["VER_NAME"].ToString(),
                        SVR_FROM_DATE = (DateTime)reader["SVR_FROM_DATE"],
                        SVR_TO_DATE = (DateTime)reader["SVR_TO_DATE"],
                        STA_DESC = reader["STA_DESC"].ToString(),
                        SVR_ID = (int)reader["SVR_ID"],
                        SVR_STA_ID = (int)reader["SVR_STA_ID"],
                        SVR_REQ_BY = reader["SVR_REQ_BY"].ToString(),
                        SVR_REQ_REM = reader["SVR_REQ_REM"].ToString(),
                        SVR_APPR_REM = reader["SVR_APPR_REM"].ToString(),
                        SVR_VER_CODE = reader["SVR_VER_CODE"].ToString(),
                    });
                }
                reader.Close();
                if (Verlist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = Verlist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object GetDetailsOnSelection(SMS_VERTICAL_REQUISITION selectedid)
    {
        try
        {
            ViewVerticalRequisitionVM VerDetList = new ViewVerticalRequisitionVM();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Edit_View_Vertical_Requisition");
            sp.Command.AddParameter("@SVRSD_ID", selectedid.SVR_REQ_ID, DbType.String);
            VerticalDetails VerticalDetails = new VerticalDetails();
            SMS_VERTICAL_REQUISITION VerticalRequsition = new SMS_VERTICAL_REQUISITION();
            VerticalRequsition.SVR_FROM_DATE = selectedid.SVR_FROM_DATE;
            VerticalRequsition.SVR_TO_DATE = selectedid.SVR_TO_DATE;
            VerticalRequsition.SVR_REQ_ID = selectedid.SVR_REQ_ID;
            VerticalRequsition.AUR_KNOWN_AS = selectedid.AUR_KNOWN_AS;
            VerticalRequsition.STA_DESC = selectedid.STA_DESC;
            VerticalRequsition.SVR_APPR_REM = selectedid.SVR_APPR_REM;
            VerticalRequsition.SVR_REQ_REM = selectedid.SVR_REQ_REM;
            VerticalRequsition.SVR_VER_CODE = selectedid.SVR_VER_CODE;
            VerticalRequsition.SVR_REQ_DATE = selectedid.SVR_REQ_DATE;
            VerticalRequsition.VER_NAME = selectedid.VER_NAME;
            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    VerticalDetails.selectedCountries = new List<Countrylst>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        VerticalDetails.selectedCountries.Add(new Countrylst { CNY_CODE = Convert.ToString(dr["SVRSD_CNY_CODE"]), CNY_NAME = Convert.ToString(dr["CNY_NAME"]), ticked = true });
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    VerticalDetails.selectedCities = new List<Citylst>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        VerticalDetails.selectedCities.Add(new Citylst { CTY_CODE = Convert.ToString(dr["SVRSD_CTY_CODE"]), CTY_NAME = Convert.ToString(dr["CTY_NAME"]), CNY_CODE = Convert.ToString(dr["SVRSD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    VerticalDetails.selectedLocations = new List<Locationlst>();
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        VerticalDetails.selectedLocations.Add(new Locationlst { LCM_CODE = Convert.ToString(dr["SVRSD_LCM_CODE"]), LCM_NAME = Convert.ToString(dr["LCM_NAME"]), CTY_CODE = Convert.ToString(dr["SVRSD_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SVRSD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    VerticalDetails.selectedTowers = new List<Towerlst>();
                    foreach (DataRow dr in ds.Tables[3].Rows)
                    {
                        VerticalDetails.selectedTowers.Add(new Towerlst { TWR_CODE = Convert.ToString(dr["SVRSD_TWR_CODE"]), TWR_NAME = Convert.ToString(dr["TWR_NAME"]), LCM_CODE = Convert.ToString(dr["SVRSD_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["SVRSD_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SVRSD_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[4].Rows.Count > 0)
                {
                    VerticalDetails.selectedFloors = new List<Floorlst>();
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        VerticalDetails.selectedFloors.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["SVRSD_FLR_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["SVRSD_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["SVRSD_LCM_CODE"]), CTY_CODE = Convert.ToString(dr["SVRSD_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SVRSD_CNY_CODE"]), ticked = true });
                    }
                }

                if (ds.Tables[5].Rows.Count > 0)
                {
                    VerticalDetails.selectedSeats = new List<VerticalReq_details>();
                    foreach (DataRow dr in ds.Tables[5].Rows)
                    {
                        VerticalDetails.selectedSeats.Add(new VerticalReq_details 
                        { SVD_SPC_NAME = Convert.ToString(dr["SVD_SPC_NAME"]), 
                          SVD_SPC_TYPE_NAME = Convert.ToString(dr["SVD_SPC_TYPE_NAME"]), 
                          SVD_SPC_SUB_TYPE_NAME = Convert.ToString(dr["SVD_SPC_SUB_TYPE_NAME"]), 
                          SVD_SPC_ID = Convert.ToString(dr["SVD_SPC_ID"]), 
                          SVD_REQ_ID = dr["SVD_REQ_ID"].ToString(),
                          FLR_ID = dr["FLR_ID"].ToString(), 
                          STACHECK = (int)RequestState.Unchanged
                        });
                    }
                }
                else
                    VerticalDetails.selectedSeats = new List<VerticalReq_details>();
            }

            SearchSpacesService serachSpc = new SearchSpacesService();
            SpaceDetails spcdet = new SpaceDetails();
            spcdet.flrlst = VerticalDetails.selectedFloors;
            spcdet.verreq = new VerticalRequistion() { SVR_FROM_DATE = (DateTime)selectedid.SVR_FROM_DATE, SVR_TO_DATE = (DateTime)selectedid.SVR_TO_DATE };
            List<VerticalReq_details> verdet = serachSpc.GetVacantSpacesList(spcdet);
            verdet.AddRange(VerticalDetails.selectedSeats);
            var common = from a1 in verdet
                         join a2 in VerticalDetails.selectedSeats on a1.SVD_SPC_ID equals a2.SVD_SPC_ID into temp
                         from t1 in temp.DefaultIfEmpty()
                         select new { A1 = a1, A2 = t1 };

            foreach (var c in common)
            {
                if (c.A2 != null)
                {
                    c.A1.SVD_REQ_ID = c.A2.SVD_REQ_ID;
                    c.A1.FLR_ID = c.A2.FLR_ID;
                    c.A1.ticked = true;
                    c.A1.STACHECK = (int)RequestState.Unchanged;
                }
                else
                    c.A1.SVD_REQ_ID = "";
            }

            VerDetList.VerticalDetails = VerticalDetails;
            VerDetList.VerticalRequsition = VerticalRequsition;
            return new { Message = MessagesVM.UM_OK, data = new { SELSPACES = VerDetList, DETAILS = common.Select(cmn => cmn.A1).OrderByDescending(x => x.ticked).ToList() } };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }

    }

}

 