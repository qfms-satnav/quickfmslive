﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for ProjectMovementService
/// </summary>
public class ProjectMovementService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    
    public DataTable GetCity()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ALLCITY");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetLocation(ProjectMovementModel.GetcityID city)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATIONSbyCTYID");
        sp.Command.Parameters.Add("@CTYID", city.CityId, DbType.String);
        ds = sp.GetDataSet();   
        return ds.Tables[0];
    }

    public DataTable GetVertical()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ALL_VERTICALS");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetCostCenter(ProjectMovementModel.Getvertical obj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "usp_getvscostcenter");
        sp.Command.Parameters.Add("@vc_Vertical", obj.getver, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetdesCity()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ALLCITY");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetdesLocation(ProjectMovementModel.GetcityID city)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATIONSbyCTYID");
        sp.Command.Parameters.Add("@CTYID", city.CitydesId, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetdesFloor(ProjectMovementModel.GetlocID loc)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_FLOOR_BYLOCATIONCITY");
        sp.Command.Parameters.Add("@FLR_CTY_ID", loc.descity, DbType.String);
        sp.Command.Parameters.Add("@FLR_LOC_ID", loc.deslocation, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //public DataTable Getfrmproj(string aurid)
    //{
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_COSTCENTER_AURID");
    //    sp.Command.Parameters.Add("@AUR_ID", aurid, DbType.String);
    //    ds = sp.GetDataSet();
    //    return ds.Tables[0];
    //}   

    public DataTable GetSourceverticalDetails(ProjectMovementModel.Getseatdetails obj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_DEDICATED_SHARED_ VERTICAL_SEAT_DETAILS_BY_TYPE");
        sp.Command.Parameters.Add("@CITY", obj.getempcity, DbType.String);
        sp.Command.Parameters.Add("@LOC", obj.getemploc, DbType.String);
        sp.Command.Parameters.Add("@VERTICAL", obj.getempver, DbType.String);
        sp.Command.Parameters.Add("@COSTCENTER", obj.getempcost, DbType.String);
        sp.Command.Parameters.Add("@TYPE", obj.gettype, DbType.String);        
        ds = sp.GetDataSet();
        //string desfc = ds.Tables[0].Rows[0]["DedicatedFC "].ToString();
        //string decfc = ds.Tables[0].Rows[0]["DEDICATEDHC"].ToString();
        return ds.Tables[0];    
    }

    //public DataTable GetdesSeatTypeDetails(ProjectMovementModel.Getseatdetails obj)
    //{
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_AVAILABLE_SEATS_FROM_DESTINATION");
    //    sp.Command.Parameters.Add("@CITY", obj.getdescity, DbType.String);
    //    sp.Command.Parameters.Add("@LOCATION", obj.getdesloc, DbType.String);     
    //    ds = sp.GetDataSet();
    //    return ds.Tables[0];  
    //}


    public DataTable GetdestSeatTypeDetails(ProjectMovementModel.Getseatdetails obj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_DES_SEAT_TYPE_DETAILS");
        sp.Command.Parameters.Add("@city", obj.getdestcity, DbType.String);
        sp.Command.Parameters.Add("@location", obj.getdestloc, DbType.String);
        ds = sp.GetDataSet();
        //string dhc;    
        //dhc = ds.Tables[0].Rows[0]["DedicatedFC"].ToString();     
        return ds.Tables[0];
    }
}