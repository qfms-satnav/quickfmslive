﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMViewSubRequistionsVM
/// </summary>
//public class HDMViewSubRequistionsVM
//{
public class BindGridDetails
{
    public string REQUEST_ID { get; set; }
    public string REQUESTED_DATE { get; set; }
    public string REQUESTED_BY { get; set; }
    public string LOCATION { get; set; }
    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string CHILD_CATEGORY { get; set; }
    public string ASSIGNED_TO { get; set; }
    public string CONTACT_NO { get; set; }
    public string REQ_STATUS { get; set; }
    public string ESC_COUNT { get; set; }
    public string TOTAL_TIME { get; set; }
    public string UPDATED_COM { get; set; }
    public string ROLID { get; set; }
}

public class BindGridDetailsBySearch
{
    public string RequestID { get; set; }
    public List<CHILDCATlst> CHILDlst { get; set; }
    public List<LCMlst> LCMlst { get; set; }
    public List<STATUSlst> STATUSlst { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string HSTSTATUS { get; set; }
}
//}