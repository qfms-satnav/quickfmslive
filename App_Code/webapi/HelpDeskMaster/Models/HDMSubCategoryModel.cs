﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class HDMSubCategoryModel
{
    public int Id { get; set; }
    public string SUBC_CODE { get; set; }
    public string SUBC_NAME { get; set; }
    public string MNC_CODE { get; set; }
    public string MNC_NAME { get; set; }
    public string SUBC_STA_ID { get; set; }
    public string SUBC_REM { get; set; }
    public string Company { get; set; }
    public string CompanyId { get; set; }
}