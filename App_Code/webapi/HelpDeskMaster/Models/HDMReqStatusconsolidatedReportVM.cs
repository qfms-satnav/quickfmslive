﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMconsolidatedReportVM
/// </summary>
public class HDMReqStatusconsolidatedReportVM
{
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string LCM_ZONE { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    //public Int32 Total_Requests { get; set; }
    //public Int32 Pending_Requests { get; set; }
    //public Int32 In_Progress_Requests { get; set; }
    //public Int32 Closed_Requests { get; set; }
    //public Int32 Canceled_Requests { get; set; }
    public string CTY_CODE { get; set; }
    public string IN_PROGRESS { get; set; }
    public string CLOSED { get; set; }
    public string ON_HOLD { get; set; }
    public string Assigned { get; set; }
    public string REQUEST_SUBMITTED { get; set; }
    public string REQUEST_CANCELLED { get; set; }
    public string WAITING_FOR_CLARIFICATION { get; set; }
    public string Total_Tickets { get; set; }
    public int FLAG { get; set; }
    public string DocType { get; set; }
    public string HSTSTATUS { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string RequisitionId { get; set; }
   public string ChildCategory { get; set; }
    public string CallLogDate { get; set; }
    public string STA_DESC { get; set; }
    public string CREATED_BY { get; set; }
    public string SERH_ASSIGN_TO { get; set; }
}
public class HDMReqconsolidatedReportVM
{
    public string AUR_ID { get; set; }
    public string CNP_NAME { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string HSTSTATUS { get; set; }
    public int FLAG { get; set; }
    public string DocType { get; set; }
    public string CTY_CODE { get; set; }
    public List<LocList> LCM_CODE { get; set; }
}
public class HDMReqconsolidatedReportVMChart
{
    public string AUR_ID { get; set; }
    public string CNP_NAME { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string HSTSTATUS { get; set; }
    public int FLAG { get; set; }
    public string DocType { get; set; }
    public string CTY_CODE { get; set; }
    public List<LocList> LCM_CODE { get; set; }
    public string Status { get; set; }
  
}
public class LocList
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public bool ticked { get; set; }

}
public class HDMconsolidatedReportGraphVM
{
    public string Status_Type { get; set; }
    public int RequestsCount { get; set; }
}

public class HDMconsolidatedParameters
{

    public int CompanyId { get; set; }
}
