﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SkabWorkOrder
/// </summary>
public class SkabWorkOrder
{
    public class WorkOrder
    {

        public string SER_REQ_ID { get; set; }
        public string MNC_NAME { get; set; }
        public string SUBC_NAME { get; set; }
        public string CHC_TYPE_NAME { get; set; }
        public string AUR_KNOWN_AS { get; set; }
        public string SER_CAL_LOG_DT { get; set; }
        public string IMP_NAME { get; set; }
        public string UGC_NAME { get; set; }
        public string SER_PROB_DESC { get; set; }
        public string SER_STATUS { get; set; }


    }
    public class AssetDetails
    {
        public string AAT_CODE { get; set; }
        //public string AAS_AAT_CODE { get; set; }
        //public string AAS_SPAREPART_ID { get; set; }
        //public string AAS_SPAREPART_NAME { get; set; }
        //public string AAS_SPAREPART_COST { get; set; }
        //public bool ticked { get; set; }
    }
    public class Assetdata
    {
        public List<Workorderdeatils> workorder { get; set; }
        public string Remarks { get; set; }
        public string termsandcdtns { get; set; }
        public string workorderid { get; set; }
    }
    public class Workorderdeatils
    {
        public string Reqid { get; set; }
        public string Asset_code { get; set; }
        public string Asset_Name { get; set; }
        public string SpareName { get; set; }
        public decimal Sparecost { get; set; }
        public int sparecount { get; set; }
        public decimal indcost { get; set; }
        public string req_id { get; set; }
        public string CHC_TYPE_NAME { get; set; }
        public string CHC_TYPE_CODE { get; set; }
        public string LCM_NAME { get; set; }
        public decimal Total { get; set; }
        public string SUBC_CODE { get; set; }
        public string SUBC_NAME { get; set; }
        public string MNC_NAME { get; set; }
        public string MNC_CODE { get; set; }
        public string LCM_CODE { get; set; }
        public string Resource { get; set; }

    }
    public class MAintdata
    {
        public List<MaintWorkorderdeatils> maintworkorder { get; set; }
        public string Remarks { get; set; }
        public string termsandcdtns { get; set; }
        public string maintworkorderid { get; set; }
    }
    public class MaintWorkorderdeatils
    {
        public string PVD_ID { get; set; }
        public string PVD_PLAN_ID { get; set; }
        public string PVD_ASSET_CODE { get; set; }
        public string PVD_PLANSCHD_DT { get; set; }
        public decimal MaintTotal { get; set; }
        public decimal cost { get; set; }
        public string PVM_GRP_ID { get; set; }
        public string PVM_GRP_TYPE_ID { get; set; }
        public string PVM_BRND_ID { get; set; }
        public string AAT_DESC { get; set; }
        public string Sparename { get; set; }
        public string Spareid { get; set; }
        public string Resourceid { get; set; }
    }
}