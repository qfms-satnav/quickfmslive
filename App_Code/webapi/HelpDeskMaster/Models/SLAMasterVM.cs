﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SLAMasterVM
/// </summary>
public class SLAMasterVM
{
    public int SLA_ID { get; set; }
    public List<SLALocationlst> Lcmlst { get; set; }
    public List<SLA> SLA { get; set; }
    public JArray SLADET { get; set; }
    public int UPLSTA { get; set; }

}

public class SLALocationlst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}
public class SLA
{
    public string CHC_TYPE_CODE { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string CHC_TYPE_SUBC_CODE { get; set; }
    public string CHC_TYPE_MNC_CODE { get; set; }
    public bool ticked { get; set; }
}

public class JarrMain
{
    public object Key { get; set; }
    public List<JarrSub> Value { get; set; }
    public Boolean selected { get; set; }
}

public class JarrSub
{
    public object Key { get; set; }
    public object Value { get; set; }
}

public class SLAModel
{
    public int SLA_ID { get; set; }
    public string SLA_CNY_CODE { get; set; }
    public string SLA_CTY_CODE { get; set; }
    public string SLA_LOC_CODE { get; set; }
    public string SLA_MNC_CODE { get; set; }
    public string SLA_SUBC_CODE { get; set; }
    public string SLA_CHC_CODE { get; set; }
    public string SLA_STA_ID { get; set; }
    public string SLA_UPLOAD_TYPE { get; set; }
    public string HDM_MAIN_MOD_ID { get; set; }
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string PCN_NAME { get; set; }
}

public class SLADetailsModel
{
    public int SLAD_SLA_ID { get; set; }
    public int SLAD_HDM_STATUS { get; set; }
    public int SLAD_HDM_ROL_ID { get; set; }
    public int SLAD_ESC_TIME { get; set; }
    public int SLAD_ESC_TIME_TYPE { get; set; }
    public Boolean SLAD_EMAIL_ESC { get; set; }
    //public int HDM_MAIN_MOD_ID { get; set; }
}

public class EditDetails
{
    public int SLA_ID { get; set; }
    public int MOD_ID { get; set; }
}