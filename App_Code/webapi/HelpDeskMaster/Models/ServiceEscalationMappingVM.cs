﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ServiceEscalationMappingVM
/// </summary>
public class ServiceEscalationMappingVM
{
    public string SEM_ID { get; set; }
    public List<TowerlstHirachy> Twrlst { get; set; }
    public List<SE> SEM { get; set; }
    public List<ServiceEscalationMappingDetails> SEMD { get; set; }


    public string City { get; set; }
    public string Country { get; set; }
    public string Location { get; set; }
    public string Tower { get; set; }
    public string Main_Category { get; set; }
    public string Sub_Category { get; set; }
    public string Child_Category { get; set; }
    public string Company { get; set; }
    public string HelpDesk_InCharge { get; set; }
    public string L1_Manager { get; set; }
    public string L2_Manager { get; set; }
    public string L3_Manager { get; set; }
    public string L4_Manager { get; set; }
    public string L5_Manager { get; set; }
    public string L6_Manager { get; set; }
}

public class TowerlstHirachy
{
    public string TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class SE
{
    public string CHC_TYPE_CODE { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string CHC_TYPE_SUBC_CODE { get; set; }
    public string CHC_TYPE_MNC_CODE { get; set; }
    public bool ticked { get; set; }
}

public class ServiceEscalationMappingDetails
{
    public string AD_ID { get; set; }
    public string AUR_EMAIL { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public int CNP_NAME { get; set; }
    public string ROL_ACRONYM { get; set; }
    public int ROL_ID { get; set; }
    public int ROL_ORDER { get; set; }

}
public class ServiceEscalationMappingModel
{
    public int SEM_ID { get; set; }
    public string SEM_CNY_CODE { get; set; }
    public string SEM_CTY_CODE { get; set; }
    public string SEM_LOC_CODE { get; set; }
    public string SEM_TWR_CODE { get; set; }
    public string SEM_MNC_CODE { get; set; }
    public string SEM_SUBC_CODE { get; set; }
    public string SEM_CHC_CODE { get; set; }
    public int SEM_STA_ID { get; set; }
    public string SEM_MAP_TYPE { get; set; }

    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }

    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string CNP_NAME { get; set; }
    public string HDM_MAIN_MOD_ID { get; set; }
}