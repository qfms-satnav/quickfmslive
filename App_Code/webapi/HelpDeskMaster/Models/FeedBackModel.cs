﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class FeedBackModel
{
    public string FBD_CODE { get; set; }
    public string FBD_NAME { get; set; }
    public string FBD_STA_ID { get; set; }
    public string FBD_REM { get; set; }
    public string FBD_IMAGE { get; set; }
}

public class EmojiModel
{
    public string EMOJI_NAME { get; set; }
    public string EMOJI_PATH { get; set; }
}