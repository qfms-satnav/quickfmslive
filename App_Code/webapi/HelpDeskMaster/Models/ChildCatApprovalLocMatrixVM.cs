﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChildCatApprovalLocMatrixVM
/// </summary>
public class ChildCatApprovalLocMatrixVM
{
    public class ChildHDMlist
    {
        public string ROL_ID { get; set; }
        public string ROL_DESCRIPTION { get; set; }
    }
}