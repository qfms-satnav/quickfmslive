﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using HDMUtilityVM;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for ChildCatApprovalMatVM
/// </summary>


public class CatwiseApprlst
{
    public List<ChildCatlst> categories { get; set; }
    public string HDM_NUM { get; set; }
}


public class ChildHDMlist
{
    public string ROL_ID { get; set; }
    public string ROL_DESCRIPTION { get; set; }
}

public class RoleBasedEmp
{
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
}

public class ChildCatHDMList
{
    public List<ChildCatlst> ChildCatlst { get; set; }
    public List<Approverlst> Apprlst { get; set; }
    public List<Locationlst> lcmlst { get; set; }
    public List<Designationlst> dsglst { get; set; }
    public string HDM_NUM { get; set; }
    public string HDM_MAIN_MOD_ID { get; set; }
    public string HDM_MAIN_MOD_NAME { get; set; }
}

public class SelectedRole
{
    public int Role { get; set; }
}

public class Employeelst
{
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public Boolean ticked { get; set; }
}
public class Approverlst
{
    public string ROL_ID { get; set; }
    public string ROL_LEVEL { get; set; }
    public List<Employeelst> OpEmplst { get; set; }
    public List<Employeelst> Emplst { get; set; }
}

public class Chlidgriddata
{
    public string CHC_TYPE_NAME { get; set; }
    public string CAM_CHC_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string DSG_CODE { get; set; }
    public string DSN_AMT_TITLE { get; set; }
    public string DSG_NAME { get; set; }
    public int COUNT { get; set; }
    public int CAM_ID { get; set; }
}

public class GetData
{
    public string CHILD_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string DSG_CODE { get; set; }
}
public class DeleteData
{
    public string CAM_ID { get; set; }
}