﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

/// <summary>
/// Summary description for HDMRaiseSubRequestVM
/// </summary>
namespace HDMRaiseSubRequestVM
{
    public class AssertLocations
    {
        public string HAL_LOC_NAME { get; set; }
        public string HAL_LOC_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Approvals
    {
        public string SER_APP_STA_NAME { get; set; }
        public string SER_APP_STA_ID { get; set; }
        public bool ticked { get; set; }
    }

    public class GetSubCategories {
        public string MNC_CODE { get; set; }
        public string SUBC_NAME { get; set; }
        public string SUBC_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class GetChildCategories
    {
        public string MNC_CODE { get; set; }
        public string SUBC_CODE { get; set; }
        public string CHC_TYPE_NAME { get; set; }
        public string CHC_TYPE_CODE { get; set; }
        public bool ticked { get; set; }
    }
    public class child_Category
    {
        public string CHC_TYPE_MNC_CODE { get; set; }
        public string CHC_TYPE_SUBC_CODE { get; set; }
        public string CHC_TYPE_NAME { get; set; }
        public string CHC_TYPE_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class FileProperties {
        public string name { get; set; }
        public string size { get; set; }
        public string type { get; set; }
        public string SaveAs { get; set; }
        public string Length { get; set; }
    }




    public class SaveReqDetails
    {
        public string AUR_ID { get; set; }
        public string REQ_ID { get; set; }
        public int STATUS { get; set; }
        public string Amount { get; set; }
        public string CustID { get; set; }
        public string BillDate { get; set; }
        public string BillNo { get; set; }
        public string InvoiceDate { get; set; }
        public string Approvals { get; set; }
        public string CustName { get; set; }
        public string InvoiceNo { get; set; }
        public string Mobile { get; set; }
        public string BilledBy { get; set; }
        public string ProbDesc { get; set; }
        public string CALL_LOG_BY { get; set; }
        public List<CHILDCATlst> CHILDlst { get; set; }
        public List<LCMlst> LCMlst { get; set; }
        public List<AssertLocations> ASSERTlst { get; set; }
        public string FileName { get; set; }
        public List<FileProperties> PostedFiles { get; set; }
        public List<HttpPostedFile> file { get; set; }
        public int length { get; set; }
    }

        //public class HDMRaiseSubRequestVM
        //{
        //    public HDMRaiseSubRequestVM()
        //    {
        //        //
        //        // TODO: Add constructor logic here
        //        //
        //    }
        //}
    }

