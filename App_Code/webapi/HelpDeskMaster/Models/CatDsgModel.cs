﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using HDMUtilityVM;
/// <summary>
/// Summary description for CatDsgModel
/// </summary>

public class CatDsgList
{
    //public List<MainCatlst> MainCatlst { get; set; }
    //public List<SubCatlst> SubCatlst { get; set; }
    public List<ChildCatlst> ChildCatlst { get; set; }
    public List<Dsglst> Dsglst { get; set; }
    public string HDM_REM { get; set; }
}

public class Dsglst
{
    public string DSN_CODE { get; set; }
    public string DSN_AMT_TITLE { get; set; }
    public bool ticked { get; set; }
}
public class SaveData
{
    public string HDM_DSN_CODE { get; set; }
    public string HDM_CHC_CODE { get; set; }
    public string HDM_SUB_CODE { get; set; }
    public string HDM_MN_CODE { get; set; }
    public string HDM_REM { get; set; }
}
public class GridData
{
    public string DSN_AMT_TITLE { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string HDM_DSN_CODE { get; set; }
}
    