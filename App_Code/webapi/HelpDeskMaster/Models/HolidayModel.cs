﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class HolidayModel
{
    public string HOL_REASON { get; set; }
    public Nullable<System.DateTime> HOL_DATE { get; set; }
    public string HOL_CITY_CODE { get; set; }
    public string HOL_CITY_NAME { get; set; }
    public string HOL_STA_ID { get; set; }
    public string HOL_REM { get; set; }
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public List<Locationlst> Location { get; set; }
    public Nullable<System.DateTime> HOL_PREVDATE { get; set; }
}

public class HolidayModelUpload
{
    public string HOL_REASON { get; set; }
    public string HOL_DATE { get; set; }
    public string HOL_CITY_CODE { get; set; }
    public string HOL_CITY_NAME { get; set; }
    public string HOL_STA_ID { get; set; }
    public string HOL_REM { get; set; }
    public string HOL_UPL_BY { get; set; }
    public string HOL_UPL_DT { get; set; }
    public string HOL_COMPANY_ID { get; set; }
    public string HOL_LOC_CODE { get; set; }
}