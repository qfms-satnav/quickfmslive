﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class HDMMainCategoryModel
{
    public string MNC_Code { get; set; }
    public string MNC_Name { get; set; }
    public string MNC_Status_Id { get; set; }
    public string MNC_REM { get; set; }
    public string MNC_CREATED_DT { get; set; }
    public string MNC_UPDATED_DT { get; set; }
    public string Company { get; set; }
}

public class GetCompanyModules
{
    public string HDM_MAIN_MOD_NAME { get; set; }
    public string HDM_MAIN_MOD_ID { get; set; }
    public bool ticked { get; set; }
}

