﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMcustomizedReportMSVM
/// </summary>
public class HDMcustomizedReportMSVM
{
    public string COL { get; set; }
    public string value { get; set; }
    public bool ticked { get; set; }
}

public class HDMcustomizedReportMS
{
    public string SER_REQ_ID { get; set; }
    public string hdIncharge { get; set; }
    public string L1Manager { get; set; }
    public string L2Manager { get; set; }
    public string L3Manager { get; set; }
    public string MAIN { get; set; }
    public string SUB { get; set; }
    public string CHILD { get; set; }
    public DateTime? SER_CAL_LOG_DT { get; set; }
    public string REQUESTEDBY { get; set; }
    public string SER_PROB_DESC { get; set; }
    public string LCM_CODE { get; set; }
    public string LCM_ZONE { get; set; }
    public string LCM_NAME { get; set; }
    public string SERH_COMMENTS { get; set; }
    public DateTime? CLOSEDDATE { get; set; }
    public string STA_TITLE { get; set; }

    public string ser_claim_amt { get; set; }
    public string Raised { get; set; }

    public string AUR_TYPE { get; set; }
    public string Approved_BY { get; set; }
    public DateTime? Approved_Date { get; set; }
    public string SER_UPDATED_BY { get; set; }
    public string StoreCode { get; set; }
    public string Month { get; set; }
}

public class HDMcustomizedDetailsMS
{
    public List<CHILDCATlst> CHILDlst { get; set; }
    public List<LCMlst> LCMlst { get; set; }
    public List<ZNlst> ZNlst { get; set; }
    public List<STATUSlst> STATUSlst { get; set; }
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string CompanyId { get; set; }
    public string HSTSTATUS { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public string CNP_NAME { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string SearchValue { get; set; }
    public string DEFINED_TAT { get; set; }
    public string RESPONSE_TAT { get; set; }
    public string TOTAL_TIME { get; set; }


}

public class CustomizedGridColsMS
{
    public string headerName { get; set; }
    public string field { get; set; }
    public int width { get; set; }
    public string cellClass { get; set; }
    public bool suppressMenu { get; set; }
}

public class ExportClassMS
{
    public string title { get; set; }
    public string key { get; set; }
}


public class GetCustomizedGridMS
{
    public string RequisitionId { get; set; }
    public string MainCategory { get; set; }
    public string SubCategory { get; set; }
    public string ChildCategory { get; set; }
    public string CallLogDate { get; set; }
    public string REQUESTEDBY_ID { get; set; }
    public string REQUESTEDBY_NAME { get; set; }
    public string Problem_Description { get; set; }
    public string Location { get; set; }
    public string LatestRemarks { get; set; }
    public string STA_TITLE { get; set; }
    public string TAT_STATUS { get; set; }
    public string ClosedTime { get; set; }
    public string TAT { get; set; }
    public string DEFINED_TAT { get; set; }
    public string HELPDESK { get; set; }
    public string L1Manager { get; set; }
    public string L2Manager { get; set; }
    public string L3Manager { get; set; }
    public string L4Manager { get; set; }
    public string L5Manager { get; set; }
    public string L6Manager { get; set; }
    public DateTime? Helpdesk_Escalation_Date { get; set; }
    public DateTime? L1Manager_Escalation_Date { get; set; }
    public DateTime? L2Manager_Escalation_Date { get; set; }
    public DateTime? L3Manager_Escalation_Date { get; set; }
    public DateTime? L4Manager_Escalation_Date { get; set; }
    public string ZONE { get; set; }
    public string rgn_name { get; set; }
    public string SER_REQUSET_FROM { get; set; }
    public string IMP_NAME { get; set; }
    public string UGC_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public string TOTAL_TIME { get; set; }
    public string RESPONSE_TAT { get; set; }
    public string DELAYED_TAT { get; set; }
    public string ser_claim_amt { get; set; }
    public string Raised { get; set; }
    public string Approved_BY { get; set; }
    public DateTime? Approved_Date { get; set; }
    public string SER_UPDATED_BY { get; set; }
    public string StoreCode { get; set; }
    public string Month { get; set; }

}