﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMFeedBackVM
/// </summary>
public class users {
    public string LCM_CODE { get; set; }
    public string CLINIC_MGR_NAME { get; set; }
    public string CLINIC_MGR_ID { get; set; }
    public string AOM_NAME { get; set; }
    public string AOM_ID { get; set; }
    public string OTH_ID { get; set; }
    public string OTH_NAME { get; set; }
    public bool ticked { get; set; }
}
public class Modes
{
    public string MODE_ID { get; set; }
    public string MODE_NAME { get; set; }
    public bool ticked { get; set; }
}

public class saveFDBCKDetails {
    public string LCM_CODE { get; set; }
    public string COMPLAINT_DATE { get; set; }
    public string CLIENTNAME { get; set; }
    public string CONTACT { get; set; }
    public string MAIN_CAT { get; set; }
    public string SUB_CAT { get; set; }
    public string CHILD_CAT { get; set; }
    public string AOM_ID { get; set; }
    public string OTH_ID { get; set; }
    public string STATUS { get; set; }
    public string CLINIC_MGR_ID { get; set; }
    public string MODECOMMENTS { get; set; }
    public string MODE { get; set; }
    public string STATUSCOMMENTS { get; set; }
    public string STATUSCOM { get; set; }
    public List<FileProperties> PostedFiles { get; set; }
    public string RequestId { get; set; }
}


public class saveFDBCKDetailsIns
{
    public List<FDBK_LIST> FDBK_LIST { get; set; }
    public List<FileProperties> PostedFiles { get; set; }
    public string REQ_ID { get; set; }
}


public class FDBK_LIST
{
    public string REQ_ID { get; set; }
    public string AUR_ID { get; set; }
    public string COMPLAINT_DATE { get; set; }
    public string LCM_CODE { get; set; }
    public string CLIENTNAME { get; set; }
    public string CONTACT { get; set; }
    public string MAIN_CAT { get; set; }
    public string SUB_CAT { get; set; }
    public string CHILD_CAT { get; set; }
    public string AOM_ID { get; set; }
    public string STATUS { get; set; }
    public string CLINIC_MGR_ID { get; set; }
    public string MODECOMMENTS { get; set; }
    public string MODE { get; set; }
    public string STATUSCOMMENTS { get; set; }
    public string STATUSCOM { get; set; }
    public string OTHERS { get; set; }
}