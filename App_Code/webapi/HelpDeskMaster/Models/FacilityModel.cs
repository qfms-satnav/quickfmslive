﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class FacilityModel
{
    public string FD_CODE { get; set; }
    public string FD_Name { get; set; }
    public string FD_LOCATION { get; set; }
    public string FD_STA_ID { get; set; }
    public string FD_REM { get; set; }
    public int FD_ID { get; set; }
    public float COST { get; set; }
}