﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChildCategoryModel
/// </summary>
public class ChildCategoryModel
{
    public string CHC_TYPE_CODE { get; set; }
    public string CHC_TYPE_NAME { get; set; }
}