﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMViewSubRequistionsVM
/// </summary>
//public class HDMViewSubRequistionsVM
//{
public class BindFdbkGridDetails

{
    public string FDBK_SUB_CAT_CODE { get; set; }
    public int FDBK_ID { get; set; }
    public string REQUEST_ID { get; set; }
    public string LOCATION { get; set; }
    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string CHILD_CATEGORY { get; set; }
    public string CLNC_MGR { get; set; }
    public string AOM { get; set; }
    public string CLNT_NAME { get; set; }
    public string CMPLT_REV_DATE { get; set; }
    public string MAIN_STATUS { get; set; }
    public string CLOSED_TIME { get; set; }
    public string MODE_NAME { get; set; }
    public string MODE_CMTS { get; set; }
    public string SUB_STA_NAME { get; set; }
    public string SUB_STA_CMTS { get; set; }
    public string REQUESTED_BY { get; set; }
    public string CONTACT_NO { get; set; }
    public string OTHERS { get; set; }
}

public class BindFdbkGridDetailsBySearch
{
    public string RequestID { get; set; }
    //public string LCM_CODE { get; set; }
    //public string MNC_CODE { get; set; }
    //public string SUB_CAT_CODE { get; set; }
    //public string STA_CODE { get; set; }
    public List<SUBCATlst> SUBlst { get; set; }
    public List<MAINCATlst> MAINlst { get; set; }
    public List<CHILDCATlst> CHILDlst { get; set; }
    public List<LCMlst> LCMlst { get; set; }
    public List<ApprovalStat> STATUSlst { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string HSTSTATUS { get; set; }
}


//}