﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BindFdbkRDLCDetails
{
    public int FDBK_ID { get; set; }
    public string REQUEST_ID { get; set; }
    public string LOCATION { get; set; }
    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string CLNC_MGR { get; set; }
    public string AOM { get; set; }
    public string CLNT_NAME { get; set; }
    public string CMPLT_REV_DATE { get; set; }
    public string MAIN_STATUS { get; set; }
    public string MODE_NAME { get; set; }
    public string MODE_CMTS { get; set; }
    public string SUB_STA_NAME { get; set; }
    public string SUB_STA_CMTS { get; set; }
    public string REQUESTED_BY { get; set; }
    public string CONTACT_NO { get; set; }
    public string Type { get; set; }
    public string OTHERS { get; set; }
    public string FDBK_SUB_CAT_CODE { get; set; }
    public string CLOSED_TIME { get; set; }
    public string CHILD_CATEGORY { get; set; }
}