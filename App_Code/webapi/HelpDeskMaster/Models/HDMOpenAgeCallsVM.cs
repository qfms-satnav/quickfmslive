﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMOpenAgeCalls
/// </summary>
public class HDMOpenAgeCallsVM
{
    public int REQCOUNT { get; set; }
    public string COL1 { get; set; }
    public string COL2 { get; set; }
    public string COL3 { get; set; }
}

public class HDMOpenAgeCallsFiltersVM
{
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string Request_Type { get; set; }
    public string CompanyId { get; set; }
    public string Type { get; set; }
}