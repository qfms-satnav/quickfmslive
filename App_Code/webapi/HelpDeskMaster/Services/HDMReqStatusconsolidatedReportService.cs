﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMconsolidatedReportService
/// </summary>
public class HDMReqStatusconsolidatedReportService
{
    SubSonic.StoredProcedure sp;
    public object BindGrid(HDMReqconsolidatedReportVM Params)
    {

        dynamic hdmconslst;
        //if (Params.FLAG == 1)
        //    hdmconslst = GetReportList(Params);
        //else        
        //    hdmconslst = GetReportList_zone(Params);     

        hdmconslst = GetReportListNew(Params);
        if (hdmconslst != null)
            return new { Message = MessagesVM.UM_OK, data = hdmconslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //public object GetReportList()
    public List<HDMReqStatusconsolidatedReportVM> GetReportList(HDMReqconsolidatedReportVM Params)
    {
        List<HDMReqStatusconsolidatedReportVM> hdmconslst = new List<HDMReqStatusconsolidatedReportVM>();
        HDMReqStatusconsolidatedReportVM hdmcons;


        //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_CONSOLIDATED_RPT");
        //    sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        //    sp.Command.AddParameter("@COMPANY", Params.CNP_NAME, DbType.Int32);
        //    sp.Command.AddParameter("@HIS_STATUS", Params.HSTSTATUS, DbType.Int32);
        //    using (IDataReader sdr = sp.GetReader())
        //    {
        //        while (sdr.Read())
        //        {
        //            hdmcons = new HDMReqStatusconsolidatedReportVM();
        //            hdmcons.CNY_NAME = sdr["CNY_NAME"].ToString();
        //            hdmcons.CTY_NAME = sdr["CTY_NAME"].ToString();
        //            hdmcons.LCM_NAME = sdr["LCM_NAME"].ToString();
        //            hdmcons.LCM_CODE = sdr["LCM_CODE"].ToString();
        //            hdmcons.CNP_NAME = sdr["CNP_NAME"].ToString();
        //            hdmcons.Total_Requests = Convert.ToInt32(sdr["Total_Requests"]);
        //            hdmcons.Pending_Requests = Convert.ToInt32(sdr["Pending_Requests"]);
        //            hdmcons.In_Progress_Requests = Convert.ToInt32(sdr["In_Progress_Requests"]);
        //            hdmcons.Closed_Requests = Convert.ToInt32(sdr["Closed_Requests"]);
        //            hdmcons.Canceled_Requests = Convert.ToInt32(sdr["Canceled_Requests"]);
        //            hdmconslst.Add(hdmcons);
        //        }
        //        sdr.Close();
        //    }       
        //        return hdmconslst;

        //}
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@user_id", SqlDbType.NVarChar);
        param[0].Value = Params.AUR_ID;
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = 1;
        param[2] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
        param[2].Value = Params.CTY_CODE;
        param[3] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(Params.LCM_CODE);
        param[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        param[4].Value = Params.FromDate;
        param[5] = new SqlParameter("@ToDate", SqlDbType.DateTime);
        param[5].Value = Params.ToDate;
        using (IDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_HELPDESK_CONSOLIDATED_RPT", param))
        {
            while (sdr.Read())
            {
                hdmcons = new HDMReqStatusconsolidatedReportVM();
                hdmcons.LCM_ZONE = sdr["ZONE"].ToString();
                hdmcons.LCM_NAME = sdr["LOCATION"].ToString();
                hdmcons.SUB_CATEGORY = sdr["SUB CATEGORY"].ToString();
                hdmcons.CLOSED = sdr["CLOSED"].ToString();
                hdmcons.ON_HOLD = sdr["ON HOLD"].ToString();
                hdmcons.IN_PROGRESS = sdr["IN PROGRESS"].ToString();
                hdmcons.Assigned = sdr["Assigned"].ToString();
                hdmcons.REQUEST_SUBMITTED = sdr["REQUEST SUBMITTED"].ToString();
                hdmcons.REQUEST_CANCELLED = sdr["REQUEST CANCELLED"].ToString();
                hdmcons.WAITING_FOR_CLARIFICATION = sdr["WAITING FOR CLARIFICATION"].ToString();
                hdmcons.Total_Tickets = sdr["Total Tickets"].ToString();
                hdmconslst.Add(hdmcons);
            }
            sdr.Close();
        }
        return hdmconslst;

    }

    //internal List<HDMReqStatusconsolidatedReportVM> GetReportList1(HDMReqconsolidatedReportVM exType)
    //{
    //    throw new NotImplementedException();
    //}


    //public List<HDMReqStatusconsolidatedReportVM> GetReportList_zone(HDMReqconsolidatedReportVM Params)
    //{
    //    List<HDMReqStatusconsolidatedReportVM> hdmconslst = new List<HDMReqStatusconsolidatedReportVM>();
    //    HDMReqStatusconsolidatedReportVM hdmcons;



    //        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_ZONE_WISE_CONSOLIDATED_RPT");
    //        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
    //        sp.Command.AddParameter("@COMPANY", Params.CNP_NAME, DbType.Int32);
    //        sp.Command.AddParameter("@HIS_STATUS", Params.HSTSTATUS, DbType.Int32);
    //        using (IDataReader sdr = sp.GetReader())
    //        {
    //            while (sdr.Read())
    //            {
    //                hdmcons = new HDMReqStatusconsolidatedReportVM();
    //                hdmcons.LCM_ZONE = sdr["LCM_ZONE"].ToString();
    //                hdmcons.Total_Requests = Convert.ToInt32(sdr["Total_Requests"]);
    //                hdmcons.Pending_Requests = Convert.ToInt32(sdr["Pending_Requests"]);
    //                hdmcons.In_Progress_Requests = Convert.ToInt32(sdr["In_Progress_Requests"]);
    //                hdmcons.Closed_Requests = Convert.ToInt32(sdr["Closed_Requests"]);
    //                hdmcons.Canceled_Requests = Convert.ToInt32(sdr["Canceled_Requests"]);
    //                hdmconslst.Add(hdmcons);
    //            }
    //            sdr.Close();

    //        }
    //        return hdmconslst;


    //}
    public List<HDMReqStatusconsolidatedReportVM> GetReportList1(HDMReqconsolidatedReportVMChart Params)
    {
        List<HDMReqStatusconsolidatedReportVM> hdmconslst = new List<HDMReqStatusconsolidatedReportVM>();
        HDMReqStatusconsolidatedReportVM hdmcons;
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@user_id", SqlDbType.NVarChar);
        param[0].Value = Params.AUR_ID;
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = 1;
        param[2] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
        param[2].Value = Params.CTY_CODE;
        param[3] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(Params.LCM_CODE);
        param[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        param[4].Value = Params.FromDate;
        param[5] = new SqlParameter("@ToDate", SqlDbType.DateTime);
        param[5].Value = Params.ToDate;
        param[6] = new SqlParameter("@Status", SqlDbType.NVarChar);
        param[6].Value = Params.Status;
        using (IDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "Excel_Down_Chart", param))
        {
            while (sdr.Read())
            {
                hdmcons = new HDMReqStatusconsolidatedReportVM();
                hdmcons.RequisitionId = sdr["Request_ID"].ToString();
                hdmcons.LCM_NAME = sdr["LOCATION"].ToString();
                hdmcons.ChildCategory = sdr["CHILD_CATEGORY"].ToString();
                hdmcons.CallLogDate = sdr["Requested_Date"].ToString();
                hdmcons.STA_DESC = sdr["status"].ToString();
                hdmcons.CREATED_BY = sdr["Raised_By"].ToString();
                hdmcons.SERH_ASSIGN_TO = sdr["Assigned_To"].ToString();
                hdmconslst.Add(hdmcons);
            }
            sdr.Close();
        }
        return hdmconslst;

    }

    public object GetReportListNew(HDMReqconsolidatedReportVM Params)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@user_id", SqlDbType.NVarChar);
        param[0].Value = Params.AUR_ID;
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = 1;
        param[2] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
        param[2].Value = Params.CTY_CODE;
        param[3] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(Params.LCM_CODE);
        param[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        param[4].Value = Params.FromDate;
        param[5] = new SqlParameter("@ToDate", SqlDbType.DateTime);
        param[5].Value = Params.ToDate;


        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HelpDesk_Request_Details", param);
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }
    //public object HDMconsolidatedChart(int CompanyID)
    //{
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_CONSOLIDATED_CHART_RPT");
    //    sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
    //    sp.Command.AddParameter("@COMPANY", CompanyID, DbType.Int32);
    //    DataSet ds = sp.GetDataSet();
    //    object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

    //    if (arr.Length != 0)
    //        return new { Message = MessagesVM.UM_OK, data = arr };
    //    else
    //        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    //}

    public object HDMconsolidatedChart(HDMReqconsolidatedReportVM Params)
    {
       DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@user_id", SqlDbType.NVarChar);
        param[0].Value = Params.AUR_ID;
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = 1;
        param[2] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
        param[2].Value = Params.CTY_CODE;
        param[3] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(Params.LCM_CODE);
        param[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        param[4].Value = Params.FromDate;
        param[5] = new SqlParameter("@ToDate", SqlDbType.DateTime);
        param[5].Value = Params.ToDate;


        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_HELPDESK_CONSOLIDATED_CHART_RPT", param);

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

        if (arr.Length != 0)
            return new { Message = MessagesVM.UM_OK, data = arr };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };


        //if (ds.Tables.Count > 0)
        //    return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        //else
        //    return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }
    public object HDMReq_Details(HDMReqconsolidatedReportVMChart Params)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@user_id", SqlDbType.NVarChar);
        param[0].Value = Params.AUR_ID;
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = 1;
        param[2] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
        param[2].Value = Params.CTY_CODE;
        param[3] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(Params.LCM_CODE);
        param[4] = new SqlParameter("@FromDate", SqlDbType.DateTime);
        param[4].Value = Params.FromDate;
        param[5] = new SqlParameter("@ToDate", SqlDbType.DateTime);
        param[5].Value = Params.ToDate;
        param[6] = new SqlParameter("@Status", SqlDbType.NVarChar);
        param[6].Value = Params.Status;

        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HD_Request_Details", param);
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }
}