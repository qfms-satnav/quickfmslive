﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMViewFeedbackService
/// </summary>
public class HDMViewFeedbackService
{
    DataSet ds;
    public object GetGriddata()
    {
        //List<AssertLocations> asserts_Lst = new List<AssertLocations>();
        //AssertLocations assert;
        ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_VIEW_FDBK_REQUISITIONS", param))
        {
            List<BindFdbkGridDetails> GridLst = new List<BindFdbkGridDetails>();
            //List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
            while (reader.Read())
            {
                GridLst.Add(new BindFdbkGridDetails()
                {
                    FDBK_SUB_CAT_CODE = reader["FDBK_SUB_CAT_CODE"].ToString(),
                    FDBK_ID = Convert.ToInt32(reader["FDBK_ID"]),
                    REQUEST_ID = reader["FDBK_REQ_ID"].ToString(),
                    LOCATION = reader["LCM_NAME"].ToString(),
                    MAIN_CATEGORY = reader["MNC_NAME"].ToString(),
                    SUB_CATEGORY = reader["SUBC_NAME"].ToString(),
                    CHILD_CATEGORY = reader["FDBK_CHILD_CAT_CODE"].ToString(),
                    CMPLT_REV_DATE = reader["FDBK_CMP_REV_DT"].ToString(),
                    CLNC_MGR = reader["CLINIC_MGR"].ToString(),
                    AOM = reader["AOM"].ToString(),
                    OTHERS = reader["OTHERS"].ToString(),
                    CLNT_NAME = reader["FDBK_CLNT_NAME"].ToString(),
                    MAIN_STATUS = reader["MAIN_STATUS"].ToString(),
                    MODE_NAME = reader["MODE_NAME"].ToString(),
                    MODE_CMTS = reader["FDBK_MODE_CMTS"].ToString(),
                    SUB_STA_NAME = reader["SER_APP_STA_REMARKS_NAME_SUB1"].ToString(),
                    SUB_STA_CMTS = reader["FDBK_STATUS_CMTS"].ToString(),
                    REQUESTED_BY = reader["FDBK_CREATED_BY"].ToString(),
                    CONTACT_NO = reader["FDBK_CNT_NO"].ToString(),
                });
            }
            reader.Close();
            return new { Message = MessagesVM.AF_OK, data = GridLst };
        }
    }

    public object GetGriddataBySearch(BindFdbkGridDetailsBySearch bindGridDetailsBySearch)
    {
        //List<AssertLocations> asserts_Lst = new List<AssertLocations>();
        //AssertLocations assert;
        //ds = new DataSet();
        SqlParameter[] udParams = new SqlParameter[11];
        udParams[0] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        udParams[0].Value = HttpContext.Current.Session["UID"];
        udParams[1] = new SqlParameter("@LCMLst", SqlDbType.Structured);
        udParams[1].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.LCMlst);
        udParams[2] = new SqlParameter("@MAINlst", SqlDbType.Structured);
        udParams[2].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.MAINlst);
        udParams[3] = new SqlParameter("@STATUSlst", SqlDbType.Structured);
        udParams[3].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.STATUSlst);
        udParams[4] = new SqlParameter("@HSTSTATUS", SqlDbType.VarChar);
        udParams[4].Value = bindGridDetailsBySearch.HSTSTATUS;
        udParams[5] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        udParams[5].Value = bindGridDetailsBySearch.FromDate.ToString();
        udParams[6] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        udParams[6].Value = bindGridDetailsBySearch.ToDate.ToString();
        udParams[7] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        udParams[7].Value = bindGridDetailsBySearch.RequestID;
        udParams[8] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        udParams[8].Value = HttpContext.Current.Session["COMPANYID"];
        udParams[9] = new SqlParameter("@SUBlst", SqlDbType.Structured);
        udParams[9].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.SUBlst);
        udParams[10] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
        udParams[10].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.CHILDlst);
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_SEARCH_FDBK_REQUISITIONS", udParams))
        {
            List<BindFdbkGridDetails> GridLst = new List<BindFdbkGridDetails>();
            //List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
            while (reader.Read())
            {
                GridLst.Add(new BindFdbkGridDetails()
                {
                    REQUEST_ID = reader["FDBK_REQ_ID"].ToString(),
                    FDBK_ID = Convert.ToInt32(reader["FDBK_ID"]),
                    LOCATION = reader["LCM_NAME"].ToString(),
                    MAIN_CATEGORY = reader["MNC_NAME"].ToString(),
                    SUB_CATEGORY = reader["SUBC_NAME"].ToString(),
                    CHILD_CATEGORY = reader["FDBK_CHILD_CAT_CODE"].ToString(),
                    CMPLT_REV_DATE = reader["FDBK_CMP_REV_DT"].ToString(),
                    CLNC_MGR = reader["CLINIC_MGR"].ToString(),
                    AOM = reader["AOM"].ToString(),
                    CLNT_NAME = reader["FDBK_CLNT_NAME"].ToString(),
                    MAIN_STATUS = reader["MAIN_STATUS"].ToString(),
                    MODE_NAME = reader["MODE_NAME"].ToString(),
                    MODE_CMTS = reader["FDBK_MODE_CMTS"].ToString(),
                    SUB_STA_NAME = reader["SER_APP_STA_REMARKS_NAME_SUB1"].ToString(),
                    SUB_STA_CMTS = reader["FDBK_STATUS_CMTS"].ToString(),
                    REQUESTED_BY = reader["FDBK_CREATED_BY"].ToString(),
                    CONTACT_NO = reader["FDBK_CNT_NO"].ToString(),
                });
            }
            reader.Close();
            return new { Message = MessagesVM.AF_OK, data = GridLst };
        }
    }

}