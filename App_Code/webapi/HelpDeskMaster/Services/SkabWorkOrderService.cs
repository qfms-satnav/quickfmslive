﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;


/// <summary>
/// Summary description for SkabWorkOrderService
/// </summary>
public class SkabWorkOrderService
{
    SubSonic.StoredProcedure sp;
    SpaceOccupancyData SpcOccup;
    List<SpaceOccupancyData> OccupList;
    DataSet ds;

    public object HelpdeskRequists()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HDM_RAISEREQUIST_FOR_WO");
            ds = sp.GetDataSet();
            //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return new { data = ds.Tables[0] };
        }
        catch
        {
            throw;
        }
    }

    public object maintenanceRequests()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_PVM_RAISEREQUIST_FOR_WO");
            ds = sp.GetDataSet();
            //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return new { data = ds.Tables[0] };
        }
        catch
        {
            throw;
        }
    }

    public object GetResource(string Reqid)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_HDM_BIND_RESOURCE]");
            sp.Command.AddParameter("@SER_REQ_ID", Reqid);
            ds = sp.GetDataSet();
            return new { data = ds.Tables[0] };
        }
        catch
        {
            throw;
        }
    }
    public object GetMaintResource(string Reqid)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_HDM_BIND_MNTRESOURCE]");
            sp.Command.AddParameter("@SER_REQ_ID", Reqid);
            ds = sp.GetDataSet();
            return new { data = ds.Tables[0] };
        }
        catch
        {
            throw;
        }
    }

    public object getSparePartsData()
    {
        try
        {
            DataSet ds = new DataSet();
            //SqlParameter[] param = new SqlParameter[1];

            //param[0] = new SqlParameter("@AST_DETAILS", SqlDbType.Structured);
            //param[0].Value = UtilityService.ConvertToDataTable(wo);

            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_SPAREPART_BY_ASSET");
            return ds.Tables[0];

        }
        catch (Exception ex)
        {
            throw;
        }


    }

    public object GetAssets()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CAPASSETS");
            ds = sp.GetDataSet();
            //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return new { data = ds.Tables[0] };
        }
        catch
        {
            throw;
        }
    }

    public object GetSubmitDetails(Assetdata Det)
    {
        string msg = "";
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@WRKORDERLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Det.workorder);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@REMARKS", SqlDbType.VarChar);
            param[2].Value = Det.Remarks;
            param[3] = new SqlParameter("@Termscndts", SqlDbType.VarChar);
            param[3].Value = Det.termsandcdtns;
            Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "INSERT_WORKORDER_DETAILS", param);
            msg = o.ToString();

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return new { Message = MessagesVM.SER_OK, data = msg };
    }
    public object GetMaintSubmitDetails(MAintdata Det)
    {
        string msg = "";
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@MNTWRKORDERLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Det.maintworkorder);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@REMARKS", SqlDbType.VarChar);
            param[2].Value = Det.Remarks;
            param[3] = new SqlParameter("@Termscndts", SqlDbType.VarChar);
            param[3].Value = Det.termsandcdtns;
            Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "INSERT_MAINT_WORKORDER", param);
            msg = o.ToString();

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        return new { Message = MessagesVM.SER_OK, data = msg };
    }
}