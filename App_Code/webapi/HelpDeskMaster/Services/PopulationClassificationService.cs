﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public class PopulationClassificationService
{
    SubSonic.StoredProcedure sp;

    public int Save(HDMPopulationClassificationModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_POPULATION_CLASSIFICATION");
            sp.Command.AddParameter("@PCN_CODE", model.PCN_Code, DbType.String);
            sp.Command.AddParameter("@PCN_NAME", model.PCN_Name, DbType.String);
            sp.Command.AddParameter("@PCN_STA_ID", model.PCN_Status_Id, DbType.String);
            sp.Command.AddParameter("@PCN_REM", model.PCN_REM, DbType.String);
            sp.Command.Parameters.Add("@PCN_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch {
            throw;
        }
    }


    public Boolean Update(HDMPopulationClassificationModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_POPULATION_CLASSIFICATION");
            sp.Command.AddParameter("@PCN_CODE", upd.PCN_Code, DbType.String);
            sp.Command.AddParameter("@PCN_NAME", upd.PCN_Name, DbType.String);
            sp.Command.AddParameter("@PCN_STA_ID", upd.PCN_Status_Id, DbType.Int32);
            sp.Command.AddParameter("@PCN_REM", upd.PCN_REM, DbType.String);
            sp.Command.Parameters.Add("@PCN_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Execute();
            return true;
        }
        catch {
            throw;
        }
    }



    public IEnumerable<HDMPopulationClassificationModel> MainCategoryBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[HDM_POPULATION_CLASSIFICATION_BINDGRID]").GetReader())
            {
                List<HDMPopulationClassificationModel> pcncatlist = new List<HDMPopulationClassificationModel>();
                while (reader.Read())
                {
                    pcncatlist.Add(new HDMPopulationClassificationModel()
                    {
                        PCN_Code = reader.GetValue(0).ToString(),
                        PCN_Name = reader.GetValue(1).ToString(),
                        PCN_Status_Id = reader.GetValue(2).ToString(),
                        PCN_REM = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return pcncatlist;
            }
        }
        catch {
            throw;
        }
    }
	
}