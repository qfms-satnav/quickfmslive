﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using UtiltiyVM;
using HDMRaiseSubRequestVM;

/// <summary>
/// Summary description for HDMRaiseSubRequestService
/// </summary>
public class HDMRaiseSubRequestService
{
    SubSonic.StoredProcedure sp;

    public object GetMainCat()
    {
        List<MAINCATlst> MAIN_lst = new List<MAINCATlst>();
        MAINCATlst MAIN;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MAIN_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                MAIN = new MAINCATlst();
                MAIN.MNC_CODE = sdr["MNC_CODE"].ToString();
                MAIN.MNC_NAME = sdr["MNC_NAME"].ToString();
                MAIN.ticked = false;
                MAIN_lst.Add(MAIN);
            }
        }
        if (MAIN_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = MAIN_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getsubcat()
    {
        List<SUBCATlst> SUB_lst = new List<SUBCATlst>();
        SUBCATlst SUB;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SUB_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SUB = new SUBCATlst();
                SUB.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                SUB.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                SUB.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                SUB.ticked = false;
                SUB_lst.Add(SUB);
            }
        }
        if (SUB_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUB_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getchildcat()
    {
        List<CHILDCATlst> CHILD_lst = new List<CHILDCATlst>();
        CHILDCATlst CHILD;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILD_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CHILD = new CHILDCATlst();
                CHILD.CHC_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                CHILD.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                CHILD.CHC_TYPE_SUBC_CODE = sdr["CHC_TYPE_SUBC_CODE"].ToString();
                CHILD.CHC_TYPE_MNC_CODE = sdr["CHC_TYPE_MNC_CODE"].ToString();
                CHILD.ticked = false;
                CHILD_lst.Add(CHILD);
            }
        }
        if (CHILD_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = CHILD_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetAssertLocations()
    {
        List<AssertLocations> asserts_Lst = new List<AssertLocations>();
        AssertLocations assert;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                assert = new AssertLocations();
                assert.HAL_LOC_NAME = sdr["HAL_LOC_NAME"].ToString();
                assert.HAL_LOC_CODE = sdr["HAL_LOC_CODE"].ToString();
                assert.ticked = false;
                asserts_Lst.Add(assert);
                //SUB.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                //SUB.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                //SUB.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                //SUB.ticked = false;
                //SUB_lst.Add(SUB);
            }
        }
        if (asserts_Lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = asserts_Lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object GetSubCategories(GetSubCategories getSubCategories)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@MNC_CODE", SqlDbType.NVarChar);
            param[0].Value = getSubCategories.MNC_CODE;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_SUB_CATEGORYS_BY_MAIN", param))
            {
                List<GetSubCategories> subCategoriesLst = new List<GetSubCategories>();
                while (reader.Read())
                {
                    subCategoriesLst.Add(new GetSubCategories()
                    {
                        SUBC_CODE = reader["SUBC_CODE"].ToString(),
                        SUBC_NAME = reader["SUBC_NAME"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = subCategoriesLst };
            }
        }
        catch (Exception exe)
        {
            var err = exe.Message;
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetChildCategories(GetChildCategories getChildCategories)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@MNC_CODE", SqlDbType.NVarChar);
            param[0].Value = getChildCategories.MNC_CODE;
            param[1] = new SqlParameter("@SUB_CODE", SqlDbType.NVarChar);
            param[1].Value = getChildCategories.SUBC_CODE;
            param[2] = new SqlParameter("@DSN_STA", SqlDbType.NVarChar);
            param[2].Value = "";
            param[3] = new SqlParameter("@DSN_CODE", SqlDbType.NVarChar);
            param[3].Value = "";
            param[4] = new SqlParameter("@EMP_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY", param))
            {
                List<GetChildCategories> ChildCategoriesLst = new List<GetChildCategories>();
                while (reader.Read())
                {
                    ChildCategoriesLst.Add(new GetChildCategories()
                    {
                        CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                        CHC_TYPE_CODE = reader["CHC_TYPE_CODE"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = ChildCategoriesLst };
            }
        }
        catch (Exception exe)
        {
            var err = exe.Message;
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }


    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }

    public object GetApprovals()
    {
        List<Approvals> approvals = new List<Approvals>();
        Approvals app;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_APPROVALS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                app = new Approvals();
                app.SER_APP_STA_ID = sdr["SER_APP_STA_ID_SUB1"].ToString();
                app.SER_APP_STA_NAME = sdr["SER_APP_STA_NAME_SUB1"].ToString();
                app.ticked = false;
                approvals.Add(app);
                //SUB.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                //SUB.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                //SUB.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                //SUB.ticked = false;
                //SUB_lst.Add(SUB);
            }
        }
        if (approvals.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = approvals };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object getClientPrefixId(string LcmCode)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_LOC_PREFIX_CODE");
        sp.Command.Parameters.Add("@LOCCODE", LcmCode, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object SaveEmpDetails(SaveReqDetails user)
    {
        try
        {
            int len = user.length;
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_TODAYS_REQUEST_ID_SUB1");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            string cnt = ds.Tables[0].Rows[0]["cnt"].ToString();
            string main = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy");
            string MaintenanceReq = main + "/" + HttpContext.Current.Session["UID"] + "/" + cnt;
            string MaintenanceReq_Upl = MaintenanceReq.Replace("/", "_");
            string status;

            //HDM_CHECK_APPROVAL_LEVELS_BY_CATEGORIES
            SqlParameter[] Params = new SqlParameter[1];
            Params[0] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
            Params[0].Value = UtilityService.ConvertToDataTable(user.CHILDlst);
            ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_CHECK_APPROVAL_LEVELS_BY_SUB_CATEGORIES", Params);
            if (ds.Tables.Count > 0)
            {
                status = ds.Tables[0].Rows[0]["APPROVALCOUNT"].ToString();
            }
            else
            {
                status = "1";
            }
            //string UID = System.Web.HttpUtility.UrlDecode(Session("UID")); // to replace spaces and comma etc from %5C %22 %2F %3E %2C %3A with \ " / > , :
            //UID = UID.Replace("_", "/");
            SqlParameter[] udParams = new SqlParameter[17];
            udParams[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            udParams[0].Value = HttpContext.Current.Session["UID"];
            udParams[1] = new SqlParameter("@LCMLst", SqlDbType.Structured);
            udParams[1].Value = UtilityService.ConvertToDataTable(user.LCMlst);
            udParams[2] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
            udParams[2].Value = UtilityService.ConvertToDataTable(user.CHILDlst);
            udParams[3] = new SqlParameter("@CustID", SqlDbType.VarChar);
            udParams[3].Value = user.CustID;
            udParams[4] = new SqlParameter("@CustName", SqlDbType.VarChar);
            udParams[4].Value = user.CustName;
            //if (len != 0)
            //{
            ////udParams[5] = new SqlParameter("@ASSERTLst", SqlDbType.Structured);
            ////udParams[5].Value = UtilityService.ConvertToDataTable(user.ASSERTlst);
            //}
            //else
            //{
            // udParams[5] = new SqlParameter("@Assert", SqlDbType.VarChar);
            //udParams[5].Value = 0;
            //}
            udParams[5] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            udParams[5].Value = user.InvoiceDate;
            udParams[6] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar);
            udParams[6].Value = user.InvoiceNo;
            udParams[7] = new SqlParameter("@Mobile", SqlDbType.VarChar);
            udParams[7].Value = user.Mobile== null ? "" : user.Mobile;
            udParams[8] = new SqlParameter("@PROB_DESC", SqlDbType.NVarChar);
            udParams[8].Value = user.ProbDesc;
            udParams[9] = new SqlParameter("@STATUS", SqlDbType.Int);
            udParams[9].Value = status;
            udParams[10] = new SqlParameter("@REQ_ID", SqlDbType.NVarChar);
            udParams[10].Value = MaintenanceReq;
            udParams[11] = new SqlParameter("@SER_CALL_LOG_BY", SqlDbType.NVarChar);
            udParams[11].Value = HttpContext.Current.Session["UID"];
            udParams[12] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            udParams[12].Value = HttpContext.Current.Session["COMPANYID"];
            udParams[13] = new SqlParameter("@BilledBy", SqlDbType.NVarChar);
            udParams[13].Value = user.BilledBy;
            udParams[14] = new SqlParameter("@SER_REQ_TYPE", SqlDbType.VarChar);
            udParams[14].Value = 0;
            udParams[15] = new SqlParameter("@RAISED_BY", SqlDbType.NVarChar);
            udParams[15].Value = HttpContext.Current.Session["UID"];
            udParams[16] = new SqlParameter("@AMOUNT", SqlDbType.VarChar);
            udParams[16].Value = user.Amount;
            //udParams[17] = new SqlParameter("@BillNo", SqlDbType.VarChar);
            //udParams[17].Value = user.BillNo;
            DataSet dset = new DataSet();
            dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_RAISE_HELP_DESK_REQUEST_SUB", udParams);

            DataSet dset2 = new DataSet();
            foreach (var File in user.PostedFiles)
            {
                //if (user.PostedFiles[fucount].Equals(File.name))
                //{
                long intsize = System.Convert.ToInt64(File.size);
                string strFileName;
                string strFileExt;
                if (intsize <= 30971520)
                {
                    string Upload_Time = getoffsetdatetime(DateTime.Now).ToString("hhmmss");
                    strFileName = System.IO.Path.GetFileName(File.name);
                    strFileExt = System.IO.Path.GetExtension(File.name);
                    string filePath = HttpContext.Current.Server.MapPath("~/UploadFiles") + @"\" + HttpContext.Current.Session["TENANT"] + @"\" + MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName; // & "." & strFileExt
                    File.SaveAs = filePath;
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_UPLOADED_FILES_SUB1");
                    sp2.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String);
                    sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
                    sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String);
                    sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String);
                    sp2.Command.AddParameter("@REMARKS", user.ProbDesc, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["Uid"], DbType.String);
                    sp2.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
                    //sp2.ExecuteScalar();

                    dset2 = sp2.GetDataSet();
                }

            }
            if (dset2.Tables.Count > 0)
            {
                return new { Message = "Service Requisition(" + MaintenanceReq + ") raised successfully" };
            }
            else if (dset.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                return new { Message = "Service Requisition(" + MaintenanceReq + ") raised successfully" };
            }
            else
            {
                return new { Message = MessagesVM.ErrorMessage };
            }

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

}