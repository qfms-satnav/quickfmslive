﻿using HDMUtilityVM;
using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class ChildCatApprovalLocMatrixService
{
    SubSonic.StoredProcedure sp;
    public object InsertDetails(ChildCatHDMList ChildLst)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@CHILDCATLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(ChildLst.ChildCatlst);

            param[1] = new SqlParameter("@HDM_NUM", SqlDbType.Int);
            param[1].Value = ChildLst.HDM_NUM;
            param[2] = new SqlParameter("@HDM_CREATED_BY", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(ChildLst.lcmlst);
            //param[5] = new SqlParameter("@DSGLST", SqlDbType.Structured);
            //param[5].Value = UtilityService.ConvertToDataTable(ChildLst.dsglst);
            //param[6] = new SqlParameter("@AURID", SqlDbType.Structured);
            //param[6].Value = UtilityService.ConvertToDataTable(ChildLst.Apprlst);

            string result = "";
            object value = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_INSERT_CHILD_LOCATION_MAPPING_MATRIX", param);

            if (value.ToString() == "Already Exists")
            {
                result = value.ToString();
            }
            else
            {
                foreach (var lcmcode in ChildLst.lcmlst)
                {
                    foreach (var childcode in ChildLst.ChildCatlst)
                    {
                        foreach (var opt in ChildLst.Apprlst)
                        {
                            foreach (var opt1 in opt.OpEmplst)
                            {
                                SqlParameter[] param1 = new SqlParameter[5];
                                param1[0] = new SqlParameter("@ROL_ID", SqlDbType.NVarChar);
                                param1[0].Value = opt.ROL_ID;
                                param1[1] = new SqlParameter("@ROL_LVL", SqlDbType.NVarChar);
                                param1[1].Value = opt.ROL_LEVEL;
                                param1[2] = new SqlParameter("@EMP", SqlDbType.NVarChar);
                                param1[2].Value = opt1.AUR_ID;
                                param1[3] = new SqlParameter("@CAM_CHC_CODE", SqlDbType.NVarChar);
                                param1[3].Value = childcode.CHC_TYPE_CODE;
                                param1[4] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
                                param1[4].Value = lcmcode.LCM_CODE;
                                object value2 = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_INSERT_CHILD_LOCATION_MAPPING_MATRIX_DETAILS", param1);
                            }
                        }
                    }
                }
                result = "Success";
            }

            return result;

        }
        catch
        {
            throw;
        }
    }
    public object GetGridDetails()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure,"HDM_GET_CHILD_CATEGORY_MAPPING_COUNT");
            int c = ds.Tables[0].Rows.Count;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_CHILD_CATEGORY_MAPPING_COUNT");
            using (IDataReader reader = sp.GetReader())
            {
                List<Chlidgriddata> Grid = new List<Chlidgriddata>();
                while (reader.Read())
                {
                    Grid.Add(new Chlidgriddata()
                    {
                        CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                        CAM_CHC_CODE = reader["CAM_CHC_CODE"].ToString(),
                        LCM_NAME = reader["LCM_NAME"].ToString(),
                        LCM_CODE = reader["LCM_CODE"].ToString(),
                        COUNT = Convert.ToInt32(reader["COUNT"]),
                        CAM_ID = Convert.ToInt32(reader["CAM_ID"]),
                    });
                }
                reader.Close();
                return new { data = Grid };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object editData(GetData obj)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILDMATRIX_DETAILS_BASED_ON_LOC");
            sp.Command.AddParameter("@CHILD_CODE", obj.CHILD_CODE, DbType.String);
            sp.Command.AddParameter("@LCM_CODE", obj.LCM_CODE, DbType.String);

            ChildCatHDMList Userlist = new ChildCatHDMList();
            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Userlist.ChildCatlst = new List<ChildCatlst>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Userlist.ChildCatlst.Add(new ChildCatlst { CHC_TYPE_CODE = Convert.ToString(dr["CAM_CHC_CODE"]), CHC_TYPE_NAME = Convert.ToString(dr["CHC_TYPE_NAME"]), CHC_TYPE_SUBC_CODE = Convert.ToString(dr["CAM_SUB_CODE"]), CHC_TYPE_MNC_CODE = Convert.ToString(dr["CAM_MN_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    Userlist.HDM_NUM = ds.Tables[1].Rows[0]["CAM_NO_OF_APPR"].ToString();
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    Userlist.Apprlst = new List<Approverlst>();
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        Userlist.Apprlst.Add(new Approverlst { ROL_ID = Convert.ToString(dr["CAMD_APPR_ROL_ID"]), ROL_LEVEL = Convert.ToString(dr["CAMD_APPR_LEVELS"]) });
                    }
                }


                foreach (var rolid in Userlist.Apprlst)
                {
                    rolid.Emplst = new List<Employeelst>();
                    Employeelst ver;
                    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ROLE_BASED_EMP");
                    sp.Command.AddParameter("@ROL", rolid.ROL_ID, DbType.Int32);
                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            ver = new Employeelst();
                            ver.AUR_ID = sdr["AUR_ID"].ToString();
                            ver.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                            rolid.Emplst.Add(ver);
                        }
                    }
                }


                foreach (var rol in Userlist.Apprlst)
                {
                    foreach (var rollst in rol.Emplst)
                    {


                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[3].Rows)
                            {
                                if (rollst.AUR_ID == Convert.ToString(dr["CAMD_CAM_AUR_ID"]) && rol.ROL_ID == Convert.ToString(dr["CAMD_APPR_ROL_ID"]))
                                {
                                    rollst.ticked = true;
                                }
                            }
                        }

                    }
                }

                if (ds.Tables[4].Rows.Count > 0)
                {
                    Userlist.lcmlst = new List<Locationlst>();
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        Userlist.lcmlst.Add(new Locationlst
                        {
                            LCM_CODE = Convert.ToString(dr["CAM_LCM_CODE"]),
                            LCM_NAME = Convert.ToString(dr["LCM_NAME"]),
                            CTY_CODE = Convert.ToString(dr["CTY_CODE"]),
                            CNY_CODE = Convert.ToString(dr["CNY_CODE"]),
                            ticked = true
                        });
                    }
                }
                if (ds.Tables[5].Rows.Count > 0)
                {
                    Userlist.HDM_MAIN_MOD_ID = ds.Tables[5].Rows[0]["HDM_MAIN_MOD_ID"].ToString();
                    Userlist.HDM_MAIN_MOD_NAME = ds.Tables[5].Rows[0]["HDM_MAIN_MOD_NAME"].ToString();
                }
                //if (ds.Tables[5].Rows.Count > 0)
                //{
                //    Userlist.dsglst = new List<Designationlst>();
                //    foreach (DataRow dr in ds.Tables[5].Rows)
                //    {
                //        Userlist.dsglst.Add(new Designationlst { DSG_CODE = Convert.ToString(dr["DSG_CODE"]), DSG_NAME = Convert.ToString(dr["DSN_AMT_TITLE"]), ticked = true });
                //    }
                //}


            }
            return new { Message = MessagesVM.ErrorMessage, data = Userlist };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object UpdateDetails(ChildCatHDMList ChildLst)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@CHILDCATLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(ChildLst.ChildCatlst);

            param[1] = new SqlParameter("@HDM_NUM", SqlDbType.Int);
            param[1].Value = ChildLst.HDM_NUM;
            param[2] = new SqlParameter("@HDM_UPDATED_BY", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(ChildLst.lcmlst);
            string result = "";
            object value = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_UPDATE_CHILD_CATEGORY_LOC_BASED_MATRIX", param);

            if (value.ToString() == "CHILD CATEGORY IS ALREADY MAPPED")
            {
                result = value.ToString();
            }
            else
            {
                foreach (var lcmcode in ChildLst.lcmlst)
                {
                    foreach (var childcode in ChildLst.ChildCatlst)
                    {
                        foreach (var opt in ChildLst.Apprlst)
                        {
                            foreach (var opt1 in opt.OpEmplst)
                            {
                                SqlParameter[] param1 = new SqlParameter[5];
                                param1[0] = new SqlParameter("@ROL_ID", SqlDbType.NVarChar);
                                param1[0].Value = opt.ROL_ID;
                                param1[1] = new SqlParameter("@ROL_LVL", SqlDbType.NVarChar);
                                param1[1].Value = opt.ROL_LEVEL;
                                param1[2] = new SqlParameter("@EMP", SqlDbType.NVarChar);
                                param1[2].Value = opt1.AUR_ID;
                                param1[3] = new SqlParameter("@CAM_CHC_CODE", SqlDbType.NVarChar);
                                param1[3].Value = childcode.CHC_TYPE_CODE;
                                param1[4] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
                                param1[4].Value = lcmcode.LCM_CODE;
                                object value2 = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_INSERT_CHILD_LOCATION_MAPPING_MATRIX_DETAILS", param1);
                            }
                        }
                    }
                }

                result = "Success";
            }

            return result;

        }
        catch
        {
            throw;
        }
    }
    public IEnumerable<ChildCatlst> GetChildCategoryByModule(HDMSubCategoryModel model)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@COMPANY", SqlDbType.NVarChar);
        param[0].Value = model.CompanyId;
        using (IDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_ACTIVE_CHILD_CATEGORIES_BY_MODULE", param))
        {
            //using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORIES_BY_MODULE").GetReader())
            {
                try
                {
                    List<ChildCatlst> Childcategorylist = new List<ChildCatlst>();
                    while (reader.Read())
                    {
                        Childcategorylist.Add(new ChildCatlst()
                        {
                            CHC_TYPE_CODE = reader.GetValue(0).ToString(),
                            CHC_TYPE_NAME = reader.GetValue(1).ToString(),
                            CHC_TYPE_SUBC_CODE = reader.GetValue(2).ToString(),
                            CHC_TYPE_MNC_CODE = reader.GetValue(3).ToString(),
                            ticked = false
                        });
                    }
                    reader.Close();
                    return Childcategorylist;
                }
                catch
                {
                    throw;
                }
            }
        }
    }

    public object DeleteSeat(string CAM_ID)
    {
        try
        {
            //string UID = id.Replace("__", "/");
            //UID = System.Web.HttpUtility.UrlDecode(UID); // to replace spaces and comma etc from %5C %22 %2F %3E %2C %3A with \ " / > , 
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@CAM_ID", SqlDbType.VarChar);
            param[0].Value = CAM_ID;
            SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_DEL_CAT_APP_MAP", param);
                return new { Message = "Record Has Been Succesfully Deleted" };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
}