﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for SLAService
/// </summary>
public class SLAService
{
    SubSonic.StoredProcedure sp;

    //country
    public IEnumerable<Country> GetActiveCountries()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GETCOUNTRY").GetReader())
        {
            List<Country> countrylist = new List<Country>();
            while (reader.Read())
            {
                countrylist.Add(new Country()
                {
                    CNY_CODE = reader["CNY_CODE"].ToString(),
                    CNY_NAME = reader["CNY_NAME"].ToString(),
                });
            }
            reader.Close();
            return countrylist;
        }
    }

    //get city by country
    public IEnumerable<City> GetActiveCityByCountry(string id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_City_By_Country");
        sp.Command.Parameters.Add("@CTY_CNY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<City> citylist = new List<City>();
            while (reader.Read())
            {
                citylist.Add(new City()
                {
                    CTY_CODE = reader["CTY_CODE"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString()
                });
            }
            reader.Close();
            return citylist;
        }
    }

    //GET location by city
    public IEnumerable<Location> GetActiveLocationByCity(string id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_location_By_City");
        sp.Command.Parameters.Add("@LCM_CTY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {

            List<Location> locationlist = new List<Location>();
            while (reader.Read())
            {
                locationlist.Add(new Location()
                {
                    LCM_CODE = reader["LCM_CODE"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                });
            }
            reader.Close();
            return locationlist;
        }
    }

    //Main Category
    public IEnumerable<MainCategoryModel> GetActiveMainCategories()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORIES").GetReader())
        {
            List<MainCategoryModel> maincategorylist = new List<MainCategoryModel>();
            while (reader.Read())
            {
                maincategorylist.Add(new MainCategoryModel()
                {
                    MNC_CODE = reader["MNC_CODE"].ToString(),
                    MNC_NAME = reader["MNC_NAME"].ToString(),
                });
            }
            reader.Close();
            return maincategorylist;
        }
    }

    //sub category
    public IEnumerable<SubCategoryModel> GetActiveSubCategoriesByMain(string id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_SUBCAT_BY_MAINCAT");
        sp.Command.Parameters.Add("@SUBC_MNC_CODE", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<SubCategoryModel> subcategorylist = new List<SubCategoryModel>();
            while (reader.Read())
            {
                subcategorylist.Add(new SubCategoryModel()
                {
                    SUBC_CODE = reader["SUBC_CODE"].ToString(),
                    SUBC_NAME = reader["SUBC_NAME"].ToString(),
                });
            }
            reader.Close();
            return subcategorylist;
        }
    }
    //child category
    public IEnumerable<ChildCategoryModel> GetActiveChildCategoriesBySub(string id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_CHILDCAT_BY_SUBCAT");
        sp.Command.Parameters.Add("@CHC_TYPE_SUBC_CODE", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ChildCategoryModel> childcategorylist = new List<ChildCategoryModel>();
            while (reader.Read())
            {
                childcategorylist.Add(new ChildCategoryModel()
                {
                    CHC_TYPE_CODE = reader["CHC_TYPE_CODE"].ToString(),
                    CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                });
            }
            reader.Close();
            return childcategorylist;
        }
    }



    public object GetSLATime(int id)
    {
        try
        {
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@ModuleId", SqlDbType.Int);
            udParams[0].Value = id;
            DataSet dset = new DataSet();
            dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_Get_Status", udParams);
            List<DataRow> statusRows = dset.Tables[0].Select().AsEnumerable().ToList();
            dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_GET_ROLES", udParams);
            List<DataRow> rolesRows = dset.Tables[0].Select().AsEnumerable().ToList();

            //List<DataRow> statusRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Status").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
            //List<DataRow> rolesRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ROLES").GetDataSet().Tables[0].Select().AsEnumerable().ToList();

            List<STATUS> statuslist = new List<STATUS>();
            List<ROLE> rolelist = new List<ROLE>();
            STATUS stat;
            foreach (DataRow dr in statusRows)
            {
                stat = new STATUS();
                stat.STA_ID = (int)dr["STA_ID"];
                stat.STA_DESC = dr["STA_DESC"].ToString();
                statuslist.Add(stat);
            }

            ROLE role;
            foreach (DataRow dr in rolesRows)
            {
                role = new ROLE();
                role.ROL_ID = (int)dr["ROL_ID"];
                role.ROL_DESCRIPTION = dr["ROL_DESCRIPTION"].ToString();
                rolelist.Add(role);
            }

            Dictionary<object, object> dictStatus = new Dictionary<object, object>();
            Dictionary<object, object> dictRole;


            foreach (STATUS status in statuslist)
            {
                var selected = false;
                dictRole = new Dictionary<object, object>();
                foreach (ROLE rol in rolelist)
                {
                    dictRole.Add(new { SLAD_HDM_ROL_ID = rol.ROL_ID }, new { SLAD_ESC_TIME = 0, SLAD_ESC_TIME_TYPE = "1" });
                }
                dictStatus.Add(new { SLAD_HDM_STATUS = status.STA_ID, SLAD_DESC = status.STA_DESC, selected = selected }, dictRole.ToList());
            }
            return new { SLADET = dictStatus.ToList(), ROLELST = rolelist, STATUSLST = statuslist };
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }



    //To insert  records into SLA

    public object SaveDetails(SLAMasterVM dataobject)
    {
        List<SLAMasterVM> SLAlist = new List<SLAMasterVM>();
        JArray jarr = dataobject.SLADET;
        var SLA = new SLAModel();

        List<JarrMain> Slamain = jarr.ToObject<List<JarrMain>>();
        List<SLADetailsModel> slalst = new List<SLADetailsModel>();
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobject.Lcmlst);
        param[1] = new SqlParameter("@CHC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(dataobject.SLA);
        param[2] = new SqlParameter("@SLA_USR_ID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["UID"];
        param[3] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];
        param[4] = new SqlParameter("@SLA_UPLOAD_TYPE", SqlDbType.NVarChar);
        param[4].Value = "SCREEN";
        foreach (JarrMain slaouter in Slamain)
        {
            dynamic status = slaouter.Key;
            int sta = Convert.ToInt32(status.SLAD_HDM_STATUS.Value);
            bool email = status.selected.Value;
            foreach (JarrSub slainner in slaouter.Value)
            {
                SLADetailsModel staref;
                dynamic role = slainner;
                if (Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value) == 0)
                    continue;
                staref = new SLADetailsModel();
                staref.SLAD_HDM_STATUS = sta;
                staref.SLAD_EMAIL_ESC = email;
                staref.SLAD_HDM_ROL_ID = Convert.ToInt32(role.Key.SLAD_HDM_ROL_ID.Value);
                staref.SLAD_ESC_TIME = Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value);
                staref.SLAD_ESC_TIME_TYPE = Convert.ToInt32(role.Value.SLAD_ESC_TIME_TYPE.Value);
                slalst.Add(staref);
            }
        }
        param[5] = new SqlParameter("@SLADET", SqlDbType.Structured);
        param[5].Value = UtilityService.ConvertToDataTable(slalst);
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_INSERT_SLA", param);
        int slad_sla_id = (int)o;
        string msg = "";
        if (slad_sla_id == 0)
            msg = "SLA is Already Defined for the Above Selection. Please Edit and Modify.";
        else
            msg = "Data Inserted Successfully";
        return new { data = SLA, Message = msg };
    }

    public object AddSla(SLAMasterVM dataobject)
    {
        //List<SLAMasterVM> SLAlist = new List<SLAMasterVM>();
        //JArray jarr = dataobject.SLADET;
        //var SLA = new SLAModel();
        string msg = "";
        //List<JarrMain> Slamain = jarr.ToObject<List<JarrMain>>();
        //List<SLADetailsModel> slalst = new List<SLADetailsModel>();
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobject.Lcmlst);



        param[1] = new SqlParameter("@SLA_USR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        param[3] = new SqlParameter("@SLA_UPLOAD_TYPE", SqlDbType.NVarChar);
        param[3].Value = "SCREEN";
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_ADD_SLA", param);
        int sla_value = (int)o;

        if (sla_value == 0)
            msg = "SLA is Already Defined for the Above Selection. Please Edit and Modify.";
        else
            msg = "Data Inserted Successfully";

        return new { Message = msg };

    }

    //update SLA
    public SLAModel UpdateDetails(SLAMasterVM dataobject)
    {
        try
        {
            List<SLAMasterVM> SLAlist = new List<SLAMasterVM>();
            JArray jarr = dataobject.SLADET;
            var SLA = new SLAModel();

            List<JarrMain> Slamain = jarr.ToObject<List<JarrMain>>();
            List<SLADetailsModel> slalst = new List<SLADetailsModel>();

            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(dataobject.Lcmlst);
            param[1] = new SqlParameter("@CHC_LIST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(dataobject.SLA);
            param[2] = new SqlParameter("@SLA_USR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@SLA_UPLOAD_TYPE", SqlDbType.NVarChar);
            param[4].Value = "SCREEN";
            foreach (JarrMain slaouter in Slamain)
            {
                dynamic status = slaouter.Key;
                int sta = Convert.ToInt32(status.SLAD_HDM_STATUS.Value);
                bool email = status.selected.Value;
                foreach (JarrSub slainner in slaouter.Value)
                {
                    SLADetailsModel staref;
                    dynamic role = slainner;
                    if (Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value) == 0)
                        continue;
                    staref = new SLADetailsModel();
                    staref.SLAD_HDM_STATUS = sta;
                    staref.SLAD_EMAIL_ESC = email;
                    staref.SLAD_HDM_ROL_ID = Convert.ToInt32(role.Key.SLAD_HDM_ROL_ID.Value);
                    staref.SLAD_ESC_TIME = Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value);
                    staref.SLAD_ESC_TIME_TYPE = Convert.ToInt32(role.Value.SLAD_ESC_TIME_TYPE.Value);
                    slalst.Add(staref);
                }
            }
            param[5] = new SqlParameter("@SLADET", SqlDbType.Structured);
            param[5].Value = UtilityService.ConvertToDataTable(slalst);
            param[6] = new SqlParameter("@SLA_ID", SqlDbType.Int);
            param[6].Value = dataobject.SLA_ID;
            Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_Update_SLA", param);
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Update_SLA");
            //sp.Command.AddParameter("@SLA_ID", dataobject.SLA.SLA_ID, DbType.String);
            //sp.Command.AddParameter("@SLA_CNY_CODE", dataobject.SLA.SLA_CNY_CODE, DbType.String);
            //sp.Command.AddParameter("@SLA_CTY_CODE", dataobject.SLA.SLA_CTY_CODE, DbType.String);
            //sp.Command.AddParameter("@SLA_LOC_CODE", dataobject.SLA.SLA_LOC_CODE, DbType.String);
            //sp.Command.AddParameter("@SLA_MNC_CODE", dataobject.SLA.SLA_MNC_CODE, DbType.String);
            //sp.Command.AddParameter("@SLA_SUBC_CODE", dataobject.SLA.SLA_SUBC_CODE, DbType.String);
            //sp.Command.AddParameter("@SLA_CHC_CODE", dataobject.SLA.SLA_CHC_CODE, DbType.String);
            //sp.Command.AddParameter("@SLA_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            //sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //Object o = sp.ExecuteScalar();
            int slad_sla_id = (int)o;

            //if (slad_sla_id == 0)
            //{
            //    SLA.SLA_ID = slad_sla_id;
            //}
            //else
            //{
            //    var i = dataobject.SLA.SLA_ID;
            //    List<SLADetailsModel> slalst = Update_SLA_Details(Slamain, i);
            //    SLA.SLA_ID = slad_sla_id;
            //}
            return SLA;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
    // Update SLA Details
    //public List<SLADetailsModel> Update_SLA_Details(List<JarrMain> slamain, int i)
    //{
    //    SLADetailsModel staref;
    //    List<SLADetailsModel> slalst = new List<SLADetailsModel>();
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Delete_SLA_Details");
    //    sp.Command.AddParameter("@SLAD_SLA_ID", i, DbType.Int32);
    //    sp.Execute();
    //    foreach (JarrMain slaouter in slamain)
    //    {
    //        dynamic status = slaouter.Key;

    //        int sta = Convert.ToInt32(status.SLAD_HDM_STATUS.Value);
    //        bool email = status.selected.Value;
    //        foreach (JarrSub slainner in slaouter.Value)
    //        {
    //            dynamic role = slainner;
    //            if (Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value) == 0)
    //                continue;
    //            staref = new SLADetailsModel();
    //            staref.SLAD_HDM_STATUS = sta;
    //            staref.SLAD_EMAIL_ESC = email;
    //            staref.SLAD_HDM_ROL_ID = Convert.ToInt32(role.Key.SLAD_HDM_ROL_ID.Value);
    //            staref.SLAD_ESC_TIME = Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value);
    //            staref.SLAD_ESC_TIME_TYPE = Convert.ToInt32(role.Value.SLAD_ESC_TIME_TYPE.Value);
    //            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Insert_SLA_Details");
    //            sp.Command.AddParameter("@SLAD_SLA_ID", i, DbType.Int32);
    //            sp.Command.AddParameter("@SLAD_HDM_STATUS", staref.SLAD_HDM_STATUS, DbType.Int32);
    //            sp.Command.AddParameter("@SLAD_HDM_ROL_ID", staref.SLAD_HDM_ROL_ID, DbType.Int32);
    //            sp.Command.AddParameter("@SLAD_ESC_TIME", staref.SLAD_ESC_TIME, DbType.Int32);
    //            sp.Command.AddParameter("@SLAD_ESC_TIME_TYPE", staref.SLAD_ESC_TIME_TYPE, DbType.Int32);
    //            sp.Command.AddParameter("@SLAD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
    //            sp.Command.AddParameter("@SLAD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
    //            sp.Command.AddParameter("@SLAD_EMAIL_ESC", staref.SLAD_EMAIL_ESC, DbType.Boolean);
    //            sp.Execute();
    //        }
    //    }
    //    return slalst;
    //}

    //Grid View
    public IEnumerable<SLAModel> GetSLAList()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_SLA_DETAILS").GetReader())
        {

            List<SLAModel> SLAlist = new List<SLAModel>();
            while (reader.Read())
            {
                SLAlist.Add(new SLAModel()
                {
                    SLA_CNY_CODE = reader["SLA_CNY_CODE"].ToString(),
                    SLA_CTY_CODE = reader["SLA_CTY_CODE"].ToString(),
                    SLA_LOC_CODE = reader["SLA_LOC_CODE"].ToString(),
                    SLA_MNC_CODE = reader["SLA_MNC_CODE"].ToString(),
                    SLA_SUBC_CODE = reader["SLA_SUBC_CODE"].ToString(),
                    SLA_CHC_CODE = reader["SLA_CHC_CODE"].ToString(),
                    CNY_NAME = reader["CNY_NAME"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    MNC_NAME = reader["MNC_NAME"].ToString(),
                    SUBC_NAME = reader["SUBC_NAME"].ToString(),
                    CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                    SLA_ID = (int)reader["SLA_ID"],
                    HDM_MAIN_MOD_ID = reader["HDM_MAIN_MOD_ID"].ToString()
                });
            }
            reader.Close();
            return SLAlist;
        }
    }


    int Mod_id;
    // To edit the record
    public object EditSLADetails(EditDetails ed)
    {
        
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Edit_SLA_Details");
        sp.Command.Parameters.Add("@SLA_ID", ed.SLA_ID);
        using (IDataReader reader = sp.GetReader())
        {
            List<SLADetailsModel> sladetlist = new List<SLADetailsModel>();

            while (reader.Read())
            {
                sladetlist.Add(new SLADetailsModel()
                {
                    SLAD_HDM_STATUS = (int)reader["SLAD_HDM_STATUS"],
                    SLAD_HDM_ROL_ID = (int)reader["SLAD_HDM_ROL_ID"],
                    SLAD_ESC_TIME = (int)reader["SLAD_ESC_TIME"],
                    SLAD_ESC_TIME_TYPE = (int)reader["SLAD_ESC_TIME_TYPE"],
                    SLAD_SLA_ID = (int)reader["SLAD_SLA_ID"],
                    SLAD_EMAIL_ESC = (bool)reader["SLAD_EMAIL_ESC"],
                });
            }
            
            reader.Close();

            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@ModuleId", SqlDbType.Int);
            udParams[0].Value = ed.MOD_ID;//Mod_id;
            DataSet dset = new DataSet();
            //dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_Get_Status", udParams);
            List<DataRow> statusRows = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_Get_Status", udParams).Tables[0].Select().AsEnumerable().ToList();
            //dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_GET_ROLES", udParams);
            List<DataRow> rolesRows = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_GET_ROLES", udParams).Tables[0].Select().AsEnumerable().ToList();

            //List<DataRow> statusRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Status").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
            //List<DataRow> rolesRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ROLES").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
            List<STATUS> statuslist = new List<STATUS>();
            List<ROLE> rolelist = new List<ROLE>();
            STATUS stat;
            foreach (DataRow dr in statusRows)
            {
                stat = new STATUS();
                stat.STA_ID = (int)dr["STA_ID"];
                stat.STA_DESC = dr["STA_DESC"].ToString();
                statuslist.Add(stat);
            }

            ROLE role;
            foreach (DataRow dr in rolesRows)
            {
                role = new ROLE();
                role.ROL_ID = (int)dr["ROL_ID"];
                role.ROL_DESCRIPTION = dr["ROL_DESCRIPTION"].ToString();
                role.ROL_SUB_MOD_ID = dr["ROL_SUB_MOD_ID"].ToString();
                rolelist.Add(role);
            }

            Dictionary<object, object> dictStatus = new Dictionary<object, object>();

            Dictionary<object, object> dictRole;
            foreach (STATUS status in statuslist)
            {
                dictRole = new Dictionary<object, object>();
                var selected = false;
                foreach (ROLE rol in rolelist)
                {
                    var sladet = sladetlist.Where(det => det.SLAD_HDM_STATUS == status.STA_ID && det.SLAD_HDM_ROL_ID == rol.ROL_ID)
                                .Select(slad => new
                                {
                                    SLAD_ESC_TIME = slad.SLAD_ESC_TIME,
                                    SLAD_ESC_TIME_TYPE = slad.SLAD_ESC_TIME_TYPE.ToString(),
                                    SLAD_EMAIL_ESC = slad.SLAD_EMAIL_ESC

                                }).FirstOrDefault();
                    if (sladet != null)
                    {
                        dictRole.Add(new { SLAD_HDM_ROL_ID = rol.ROL_ID }, sladet);
                        selected = selected ? selected : sladet.SLAD_EMAIL_ESC;
                    }

                    else
                        dictRole.Add(new { SLAD_HDM_ROL_ID = rol.ROL_ID }, new
                        {
                            SLAD_ESC_TIME = 0,
                            SLAD_ESC_TIME_TYPE = "1"
                        });
                }
                dictStatus.Add(new { SLAD_HDM_STATUS = status.STA_ID, SLAD_DESC = status.STA_DESC, selected = selected }, dictRole.ToList());

            }
            return new { SLADET = dictStatus.ToList(), ROLELST = rolelist, STATUSLST = statuslist };

        }
    }
}



