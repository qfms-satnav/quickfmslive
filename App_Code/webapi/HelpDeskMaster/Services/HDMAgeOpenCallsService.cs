﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for AgeOpenCallsService
/// </summary>
public class HDMAgeOpenCallsService
{
    SubSonic.StoredProcedure sp;
    List<HDMOpenAgeCallsVM> VMlist;
    DataSet ds;
    public object GetHDMopenclsReport(HDMOpenAgeCallsFiltersVM hdmUsr)
    {
        object objVMlist = GetHDMopenclsRpt(hdmUsr);

        if (objVMlist != null)
            return new { Message = MessagesVM.UM_OK, data = new { VMList = objVMlist, Graphdate = "" } };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //public List<HDMOpenAgeCallsVM> GetHDMopenclsRpt(HDMOpenAgeCallsFiltersVM HDM)
    public object GetHDMopenclsRpt(HDMOpenAgeCallsFiltersVM HDM)
    {
        //List<HDMReportByUserVM> rptByUserlst = new List<HDMReportByUserVM>();
        HDMOpenAgeCallsVM rptVMobj;

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_AGE_OPENCALLS_RPT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", HDM.FromDate.ToString(), DbType.String);
        sp.Command.AddParameter("@TODATE", HDM.ToDate.ToString(), DbType.String);
        sp.Command.AddParameter("@CompanyId", HDM.CompanyId, DbType.Int32);
        DataSet ds = sp.GetDataSet();
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<HDMOpenAgeCallsVM>();
            while (sdr.Read())
            {
                rptVMobj = new HDMOpenAgeCallsVM();
                rptVMobj.REQCOUNT = Convert.ToInt32(sdr["REQCOUNT"]);
                rptVMobj.COL1 = sdr["COL1"].ToString();
                rptVMobj.COL2 = sdr["COL2"].ToString();
                VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }

        //graph
        DataTable dt = ds.Tables[0].DefaultView.ToTable(false, "COL3", "REQCOUNT");
        object[] arr = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        if (VMlist.Count != 0)
            return new { VMlist = VMlist, graphData = arr };
        else
            return null;
    }
}