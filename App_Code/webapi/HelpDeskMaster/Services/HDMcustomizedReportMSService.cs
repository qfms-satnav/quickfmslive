﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System.Diagnostics;

public class HDMcustomizedReportMSService
{
    SubSonic.StoredProcedure sp;
    List<HDMcustomizedReportMS> Cust;
    HDMcustomizedReportMS Custm;

    public object GetCustomizedObjectMS(HDMcustomizedDetailsMS Det)
    {
        try
        {
            //col names in ddl
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ESCALTION_ROLES");

            var lst = GetCustomizedDetailsMS(Det);
            if (lst != null)
            {
                return new { data = new { lst }, Message = MessagesVM.SER_OK };
            }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }

        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    //internal object GetCustomizedObjectMS(HDMcustomizedDetailsMS custdata)
    //{
    //    throw new NotImplementedException();
    //}

    public object GetCustomizedDetailsMS(HDMcustomizedDetailsMS Details)
    {

        try
        {

            List<HDMcustomizedReportMS> CData = new List<HDMcustomizedReportMS>();
            System.Data.DataTable DT = new DataTable();
            List<CustomizedGridColsMS> gridcols = new List<CustomizedGridColsMS>();
            CustomizedGridColsMS gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();

            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];
            param[1] = new SqlParameter("@LCMlst", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.LCMlst);
            param[2] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(Details.CHILDlst);
            param[3] = new SqlParameter("@STATUSlst", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(Details.STATUSlst);
            param[4] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
            param[4].Value = Details.FromDate.ToString();
            param[5] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
            param[5].Value = Details.ToDate.ToString();
            param[6] = new SqlParameter("@COMPANY", SqlDbType.NVarChar);
            param[6].Value = Details.CompanyId;
            param[7] = new SqlParameter("@HIS_STATUS", SqlDbType.NVarChar);
            param[7].Value = Details.HSTSTATUS;
            param[8] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[8].Value = Details.SearchValue;
            param[9] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[9].Value = Details.PageNumber;
            param[10] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[10].Value = Details.PageSize;
            param[11] = new SqlParameter("@ZNlst", SqlDbType.Structured);
            param[11].Value = UtilityService.ConvertToDataTable(Details.ZNlst);

            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_CUSTOMIZED_RPT_MAIN", param);
            HttpContext.Current.Session["CustomizedGrid"] = ds;

            //List<GetCustomizedGridMS> customizedGridList = new List<GetCustomizedGridMS>();
            //GetCustomizedGridMS customizedGrid = new GetCustomizedGridMS();
            //string JSONString = string.Empty;
            //JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            //customizedGridList = JsonConvert.DeserializeObject<List<GetCustomizedGridMS>>(JSONString);
            return new { griddata = ds.Tables[0] };

        }
        catch (Exception Ex)
        {
            var err = Ex.Message;
            throw;
        }

    }
    public List<GetCustomizedGridMS> GetCustomizedDetailsExportMS(HDMcustomizedDetailsMS Details)
    {

        try
        {
            DataSet ds = new DataSet();
            ds = (DataSet)HttpContext.Current.Session["CustomizedGrid"];
            List<GetCustomizedGridMS> customizedGridList = new List<GetCustomizedGridMS>();
            GetCustomizedGridMS customizedGrid = new GetCustomizedGridMS();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            customizedGridList = JsonConvert.DeserializeObject<List<GetCustomizedGridMS>>(JSONString);
            return customizedGridList;  //, Coldef = gridcols, exportCols = lstexportcls
        }
        catch (Exception Ex)
        {
            var err = Ex.Message;
            throw;
        }
    }
}