﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public class AssetLocationService
{
    SubSonic.StoredProcedure sp;

    public int Save(AssetLocationModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_ASSET_LOCATION");
            sp.Command.AddParameter("@ASSETLOC_CODE", model.ASSETLOC_Code, DbType.String);
            sp.Command.AddParameter("@ASSETLOC_NAME", model.ASSETLOC_Name, DbType.String);
            sp.Command.AddParameter("@ASSETLOC_STA_ID", model.ASSETLOC_Status_Id, DbType.String);
            sp.Command.AddParameter("@ASSETLOC_REM", model.ASSETLOC_REM, DbType.String);
            sp.Command.Parameters.Add("@ASSETLOC_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch {
            throw;
        }
    }


    public Boolean Update(AssetLocationModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_ASSET_LOCATION");
            sp.Command.AddParameter("@ASSETLOC_CODE", upd.ASSETLOC_Code, DbType.String);
            sp.Command.AddParameter("@ASSETLOC_NAME", upd.ASSETLOC_Name, DbType.String);
            sp.Command.AddParameter("@ASSETLOC_STA_ID", upd.ASSETLOC_Status_Id, DbType.Int32);
            sp.Command.AddParameter("@ASSETLOC_REM", upd.ASSETLOC_REM, DbType.String);
            sp.Command.Parameters.Add("@ASSETLOC_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.Execute();
            return true;
        }
        catch {
            throw;
        }
    }



    public IEnumerable<AssetLocationModel> AssetLocationBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[HDM_ASSET_LOCATION_BINDGRID]").GetReader())
            {
                List<AssetLocationModel> ASSETLOCcatlist = new List<AssetLocationModel>();
                while (reader.Read())
                {
                    ASSETLOCcatlist.Add(new AssetLocationModel()
                    {
                        ASSETLOC_Code = reader.GetValue(0).ToString(),
                        ASSETLOC_Name = reader.GetValue(1).ToString(),
                        ASSETLOC_Status_Id = reader.GetValue(2).ToString(),
                        ASSETLOC_REM = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return ASSETLOCcatlist;
            }
        }
        catch {
            throw;
        }
    }
}