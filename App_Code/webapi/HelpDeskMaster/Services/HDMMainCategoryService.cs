﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class HDMMainCategoryService
{
    SubSonic.StoredProcedure sp;

    public int Save(HDMMainCategoryModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_MAIN_CATEGORY");
            sp.Command.AddParameter("@MNC_CODE", model.MNC_Code, DbType.String);
            sp.Command.AddParameter("@MNC_NAME", model.MNC_Name, DbType.String);
            sp.Command.AddParameter("@MNC_STA_ID", model.MNC_Status_Id, DbType.String);
            sp.Command.AddParameter("@MNC_REM", model.MNC_REM, DbType.String);
            sp.Command.Parameters.Add("@MNC_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            if(model.Company != null)
            {
                sp.Command.Parameters.Add("@HELPDESKMODULE", model.Company, DbType.String);
            }
            else
            {
                sp.Command.Parameters.Add("@HELPDESKMODULE", 1, DbType.String);
            }
            
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }


    public Boolean Update(HDMMainCategoryModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_MAIN_CATEGORY");
            sp.Command.AddParameter("@MNC_CODE", upd.MNC_Code, DbType.String);
            sp.Command.AddParameter("@MNC_NAME", upd.MNC_Name, DbType.String);
            sp.Command.AddParameter("@MNC_STA_ID", upd.MNC_Status_Id, DbType.Int32);
            sp.Command.AddParameter("@MNC_REM", upd.MNC_REM, DbType.String);
            sp.Command.Parameters.Add("@MNC_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            if (upd.Company != null)
            {
                sp.Command.Parameters.Add("@HELPDESKMODULE", upd.Company, DbType.String);
            }
            else
            {
                sp.Command.Parameters.Add("@HELPDESKMODULE", 1, DbType.String);
            }
            sp.Execute();
            return true;
        }
        catch
        {
            throw;
        }
    }



    public IEnumerable<HDMMainCategoryModel> MainCategoryBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[HDM_MAIN_CATEGORY_BINDGRID]").GetReader())
            {
                List<HDMMainCategoryModel> MNCcatlist = new List<HDMMainCategoryModel>();
                while (reader.Read())
                {
                    MNCcatlist.Add(new HDMMainCategoryModel()
                    {
                        MNC_Code = reader["MNC_CODE"].ToString(),
                        MNC_Name = reader["MNC_NAME"].ToString(),
                        MNC_Status_Id = reader["MNC_STA_ID"].ToString(),
                        MNC_REM = reader["MNC_REM"].ToString(),
                        MNC_CREATED_DT = reader["MNC_CREATED_DT"].ToString(),
                        MNC_UPDATED_DT = reader["MNC_UPDATED_DT"].ToString(),
                        Company = reader["HDM_MAIN_MOD_NAME"].ToString(),
                    });
                }
                reader.Close();
                return MNCcatlist;
            }
        }
        catch
        {
            throw;
        }
    }

    public object GetCompanyModules()
    {
        List<GetCompanyModules> getCompanyModules = new List<GetCompanyModules>();
        GetCompanyModules app;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_MODULES");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                app = new GetCompanyModules();
                app.HDM_MAIN_MOD_ID = sdr["HDM_MAIN_MOD_ID"].ToString();
                app.HDM_MAIN_MOD_NAME = sdr["HDM_MAIN_MOD_NAME"].ToString();
                app.ticked = false;
                getCompanyModules.Add(app);
            }
        }
        if (getCompanyModules.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = getCompanyModules };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public String HelpDeskModuleHide()
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_SYSTEM_PREFERENCES_VAL");
        string sys_val = ds.Tables[0].Rows[0]["SYSP_VAL1"].ToString();
            return sys_val;
    }

}