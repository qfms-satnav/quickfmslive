﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class HDMReportByCategoryService
{
    SubSonic.StoredProcedure sp;
    List<HDMReportByCategory> rptCategoryList;
    HDMReport_Bar_Graph rptBarGph;
    DataSet ds;
    public object GetReportByCategory(HDMReportByCatParams hdmUsr)
    {
        try
        {
            rptCategoryList = GetHDMReportByCategoryData(hdmUsr);

            if (rptCategoryList.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = rptCategoryList };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<HDMReportByCategory> GetHDMReportByCategoryData(HDMReportByCatParams HDM)
    {
        try
        {
            List<HDMReportByCategory> rptByCategorylst = new List<HDMReportByCategory>();
            HDMReportByCategory rptByCategory;
            TimeSpan ts = new TimeSpan(23, 59, 0);
            HDM.ToDate = HDM.ToDate + ts;

            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_REPORT_BY_CATEGORY");
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[HDM_REPORT_BY_CATEGORY]");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@FromDate", HDM.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@ToDate", HDM.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@CompanyId", HDM.CompanyId, DbType.Int32);
            sp.Command.AddParameter("@PageNumber", HDM.PageNumber, DbType.Int32);
            sp.Command.AddParameter("@PageSize", HDM.PageSize, DbType.Int32);
            sp.Command.AddParameter("@SEARCHVAL", HDM.SEARCHVAL, DbType.String);
       

            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    rptByCategory = new HDMReportByCategory();
                    rptByCategory.MAIN_CATEGORY = sdr["MAIN_CATEGORY"].ToString();
                    rptByCategory.SUB_CATEGORY = sdr["SUB_CATEGORY"].ToString();
                    rptByCategory.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                    //rptByCategory.TWR_NAME = sdr["TWR_NAME"].ToString();
                    rptByCategory.LOCATION = sdr["LOCATION"].ToString();
                    rptByCategory.REQUESTED_BY_ID = sdr["REQUESTED_BY_ID"].ToString();
                    rptByCategory.REQUESTED_BY_NAME = sdr["REQUESTED_BY_NAME"].ToString();
                    rptByCategory.REQUESTED_DATE = sdr["REQUESTED_DATE"].ToString();
                    rptByCategory.SER_PROB_DESC = sdr["SER_PROB_DESC"].ToString();
                    rptByCategory.ASSIGNED_TO = sdr["ASSIGNED_TO"].ToString();
                    rptByCategory.ASSIGNED_TO_NAME = sdr["ASSIGNED_TO_NAME"].ToString();
                    rptByCategory.CONTACT_NO = sdr["CONTACT_NO"].ToString();
                    rptByCategory.REQ_STATUS = sdr["REQ_STATUS"].ToString();
                    rptByCategory.TOTAL_TIME = sdr["TOTAL_TIME"].ToString();
                    rptByCategory.CLOSED_TIME = sdr["CLOSED_TIME"].ToString();
                    rptByCategory.DEFINED_TAT = sdr["DEFINED_TAT"].ToString();
                    //rptByCategory.TAT = sdr["TAT"].ToString();
                    rptByCategory.DELAYED_TIME = sdr["DELAYED_TIME"].ToString();
                    rptByCategory.SER_REQ_ID = sdr["SER_REQ_ID"].ToString();
                    rptByCategory.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                    rptByCategory.STA_COLOR = sdr["STA_COLOR"].ToString();
                    rptByCategorylst.Add(rptByCategory);
                }
                sdr.Close();
            }
            if (rptByCategorylst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = rptByCategorylst };
                return rptByCategorylst;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }

    public object GetCategoryWiseChart(HDMReportByCatParams data)
    {
        try
        {
            //List<HDMReport_Bar_Graph> rptBarGphLst = new List<HDMReport_Bar_Graph>();
            TimeSpan ts = new TimeSpan(23, 59, 0);
            data.ToDate = data.ToDate + ts;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_CATEGORY_WISE_CHART_RPT");
            sp.Command.AddParameter("@FROM_DATE", data.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", data.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@CompanyId", data.CompanyId, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);          
            ds = sp.GetDataSet();
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public object GetCategoryWisePieChart(HDMReportByCatParams data)
    {
        try
        {
            //List<HDMReport_Bar_Graph> rptBarGphLst = new List<HDMReport_Bar_Graph>();
            TimeSpan ts = new TimeSpan(23, 59, 0);
            data.ToDate = data.ToDate + ts;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_CATEGORY_WISE_PIE_CHART_RPT");
            sp.Command.AddParameter("@FROM_DATE", data.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", data.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@CompanyId", data.CompanyId, DbType.Int32);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            //object[] arr =new object[2];
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            //arr[0] = ds.Tables[0].Rows[0]["CHC_TYPE_NAME"].ToString();
            //arr[1] = (int)ds.Tables[0].Rows[0]["TOTAL"];
            return arr;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    //public DataTable GetReportByCatDetails(HDMReportByCatParams Details)
    //{
    //    try
    //    {
    //        List<HDMReportByCategory> CData = new List<HDMReportByCategory>();
    //        SqlParameter[] param = new SqlParameter[2];
    //        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
    //        param[0].Value = HttpContext.Current.Session["UID"];
    //        if (Details.CNP_NAME == null)
    //        {
    //            param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
    //            param[1].Value = HttpContext.Current.Session["COMPANYID"];

    //        }
    //        else
    //        {
    //            param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
    //            param[1].Value = Details.CNP_NAME;

    //        }


    //        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "HDM_GET_REPORT_BY_USER_EXPORTTOEXCEL", param);
    //        return dt;

    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}
    //public object SearchAllData(HDMReportByCatParams Det)
    //{
    //    List<HDMReportByCategory> CData = new List<HDMReportByCategory>();
    //    DataTable DT = new DataTable();
    //    try
    //    {
    //        SqlParameter[] param = new SqlParameter[5];
    //        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
    //        param[0].Value = HttpContext.Current.Session["UID"];
    //        param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
    //        param[1].Value = HttpContext.Current.Session["COMPANYID"];
    //        param[2] = new SqlParameter("@PageNumber", SqlDbType.Int);
    //        param[2].Value = Det.PageNumber;
    //        param[3] = new SqlParameter("@PageSize", SqlDbType.Int);
    //        param[3].Value = Det.PageSize;
    //        param[4] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
    //        param[4].Value = Det.SearchValue;

    //        DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "HDM_REPORT_BY_CATEGORY_SEARCH", param);
    //        return DT;
    //    }
    //    catch (Exception ex) { return DT; }
    //}

}