﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

/// <summary>
/// Summary description for HDMConsolidatedReportService
/// </summary>
public class HDMConsolidatedReportService
{
    SubSonic.StoredProcedure sp;
    List<HDMConsolidatedReportVM> consVMList;// Cust;
    HDMConsolidatedReportVM consVM; //Custm;

    public object GetHDMConsolidatedData(HDMcustomizedDetails hdmUsr)
    {
        try
        {
            consVMList = GethdmConsReportData(hdmUsr);

            if (consVMList != null)
                return new { Message = MessagesVM.UM_OK, data = consVMList };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<HDMConsolidatedReportVM> GethdmConsReportData(HDMcustomizedDetails HDM)
    {

        try
        {

            List<HDMConsolidatedReportVM> VMlst = new List<HDMConsolidatedReportVM>();
            HDMConsolidatedReportVM cvm;
            TimeSpan ts = new TimeSpan(23, 59, 0);

            HDM.ToDate = HDM.ToDate + ts;

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_CONSOLIDATED_REPORT_MAIN");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@FromDate", HDM.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@ToDate", HDM.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@CompanyId", HDM.CompanyId, DbType.Int32);
            sp.Command.AddParameter("@PageNumber", HDM.PageNumber, DbType.Int32);
            sp.Command.AddParameter("@PageSize", HDM.PageSize, DbType.Int32);
            DataSet ds = sp.GetDataSet();
            HttpContext.Current.Session["ConsolidateGrid"] = ds;
            cvm = new HDMConsolidatedReportVM();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            VMlst = JsonConvert.DeserializeObject<List<HDMConsolidatedReportVM>>(JSONString);
            if (VMlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = VMlst };
                return VMlst;
            else
                return null;
        }
        catch
        {
            throw;
        }
    }
    public List<HDMConsolidatedReportVM> HdmConsolidateDetails(HDMcustomizedDetails HDM)
    {

        try
        {

            List<HDMConsolidatedReportVM> VMlst = new List<HDMConsolidatedReportVM>();
            HDMConsolidatedReportVM cvm;
            TimeSpan ts = new TimeSpan(23, 59, 0);
            HDM.ToDate = HDM.ToDate + ts;

            DataSet ds = new DataSet();
            ds = (DataSet)HttpContext.Current.Session["ConsolidateGrid"];
            cvm = new HDMConsolidatedReportVM();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            VMlst = JsonConvert.DeserializeObject<List<HDMConsolidatedReportVM>>(JSONString);

            if (VMlst.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = VMlst };
                return VMlst;
            else
                return null;
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    //public DataTable HdmConsolidateDetails(HDMcustomizedDetails Details)
    //{
    //    try
    //    {
    //        List<HDMConsolidatedReportVM> VMlst = new List<HDMConsolidatedReportVM>();
    //        HDMConsolidatedReportVM cvm;
    //        SqlParameter[] param = new SqlParameter[2];
    //        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
    //        param[0].Value = HttpContext.Current.Session["UID"];
    //        if (Details.CNP_NAME == null)
    //        {
    //            param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
    //            param[1].Value = HttpContext.Current.Session["COMPANYID"];

    //        }
    //        else
    //        {
    //            param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
    //            param[1].Value = Details.CNP_NAME;

    //        }

    //        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "HDM_CONSOLIDATED_REPORT_Exoprttoexcel", param);
    //        return dt;

    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}
    public object SearchAllData(HDMcustomizedDetails Det)
    {
        List<HDMConsolidatedReportVM> CData = new List<HDMConsolidatedReportVM>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];
            param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["COMPANYID"];
            param[2] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[2].Value = Det.PageNumber;
            param[3] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[3].Value = Det.PageSize;
            param[4] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
            param[4].Value = Det.SearchValue;

            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[HDM_GET_REPORT_BY_USER_SEARCHGRID]", param);
            return DT;
        }
        catch (Exception ex) { return DT; }
    }
}