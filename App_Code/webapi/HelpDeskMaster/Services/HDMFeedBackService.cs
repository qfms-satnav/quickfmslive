﻿using System.Collections.Generic;
using System.Data;
using System.Web;
using UtiltiyVM;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System;
/// <summary>
/// Summary description for HDMFeedBackService
/// </summary>
public class HDMFeedBackService
{
    SubSonic.StoredProcedure sp;

    public object GetMainCat()
    {
        List<MAINCATlst> MAIN_lst = new List<MAINCATlst>();
        MAINCATlst MAIN;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MAIN_CAT_FDBACK");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                MAIN = new MAINCATlst();
                MAIN.MNC_CODE = sdr["MNC_CODE"].ToString();
                MAIN.MNC_NAME = sdr["MNC_NAME"].ToString();
                MAIN.ticked = false;
                MAIN_lst.Add(MAIN);
            }
        }
        if (MAIN_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = MAIN_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getsubcat()
    {
        List<SUBCATlst> SUB_lst = new List<SUBCATlst>();
        SUBCATlst SUB;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SUB_CAT_FDBACK");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SUB = new SUBCATlst();
                SUB.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                SUB.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                SUB.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                SUB.ticked = false;
                SUB_lst.Add(SUB);
            }
        }
        if (SUB_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUB_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object Getchildcat()
    {
        List<CHILDCATlst> CHILD_lst = new List<CHILDCATlst>();
        CHILDCATlst CHILD;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILD_CAT1");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CHILD = new CHILDCATlst();
                CHILD.CHC_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                CHILD.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                CHILD.CHC_TYPE_SUBC_CODE = sdr["CHC_TYPE_SUBC_CODE"].ToString();
                CHILD.CHC_TYPE_MNC_CODE = sdr["CHC_TYPE_MNC_CODE"].ToString();
                CHILD.ticked = false;
                CHILD_lst.Add(CHILD);
            }
        }
        if (CHILD_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = CHILD_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetStatusList()
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@AURID", SqlDbType.VarChar);
            udParams[0].Value = HttpContext.Current.Session["UID"];
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_APPROVALS_STATUS_FDBACK", udParams))
            {
                List<ApprovalStat> AppStatuslist = new List<ApprovalStat>();
                while (reader.Read())
                {
                    AppStatuslist.Add(new ApprovalStat()
                    {
                        SER_APP_STA_REMARKS_ID = reader["SER_APP_STA_REMARKS_ID_SUB1"].ToString(),
                        SER_APP_STA_REMARKS_NAME = reader["SER_APP_STA_REMARKS_NAME_SUB1"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = AppStatuslist };
            }



        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetSubStatusList()
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@AURID", SqlDbType.VarChar);
            udParams[0].Value = HttpContext.Current.Session["UID"];
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_APPROVALS_SUB_STATUS_FDBACK", udParams))
            {
                List<ApprovalStat> AppStatuslist = new List<ApprovalStat>();
                while (reader.Read())
                {
                    AppStatuslist.Add(new ApprovalStat()
                    {
                        SER_APP_STA_REMARKS_ID = reader["SER_APP_STA_REMARKS_ID_SUB1"].ToString(),
                        SER_APP_STA_REMARKS_NAME = reader["SER_APP_STA_REMARKS_NAME_SUB1"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = AppStatuslist };
            }



        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetClinicMangerByLoc(users usr)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
            udParams[0].Value = usr.LCM_CODE;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CINCHARGE_BY_LOC", udParams))
            {
                List<users> usrLst = new List<users>();
                while (reader.Read())
                {
                    usrLst.Add(new users()
                    {
                        CLINIC_MGR_ID = reader["AUR_ID"].ToString(),
                        CLINIC_MGR_NAME = reader["AUR_KNOWN_AS"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = usrLst };
            }



        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetAOMByLoc(users usr)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
            udParams[0].Value = usr.LCM_CODE;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_AOM_BY_LOC", udParams))
            {
                List<users> usrLst = new List<users>();
                while (reader.Read())
                {
                    usrLst.Add(new users()
                    {
                        AOM_ID = reader["AUR_ID"].ToString(),
                        AOM_NAME = reader["AUR_KNOWN_AS"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = usrLst };
            }



        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object GetOTHERSByLoc(users usr)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
            udParams[0].Value = usr.LCM_CODE;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_OTHERS_BY_LOC", udParams))
            {
                List<users> usrLst = new List<users>();
                while (reader.Read())
                {
                    usrLst.Add(new users()
                    {
                        OTH_ID = reader["AUR_ID"].ToString(),
                        OTH_NAME = reader["AUR_KNOWN_AS"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = usrLst };
            }



        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object GetFdBackMode()
    {
        try
        {
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_FDBACK_MODE"))
            {
                List<Modes> ModesLst = new List<Modes>();
                while (reader.Read())
                {
                    ModesLst.Add(new Modes()
                    {
                        MODE_ID = reader["MODE_ID"].ToString(),
                        MODE_NAME = reader["MODE_NAME"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = ModesLst };
            }



        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }

    public object SaveFDBckDetails(saveFDBCKDetailsIns user)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_TODAYS_FDBCK_REQUEST_ID_SUB1");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            string cnt = ds.Tables[0].Rows[0]["cnt"].ToString();
            string main = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy");
            string MaintenanceReq = main + "/" + cnt;
            string MaintenanceReq_Upl = MaintenanceReq.Replace("/", "_");

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@USER_LST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(user.FDBK_LIST);
            param[1] = new SqlParameter("@AURID", SqlDbType.VarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            param[2].Value = MaintenanceReq;

            //SqlParameter[] udParams = new SqlParameter[16];
            //udParams[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            //udParams[0].Value = HttpContext.Current.Session["UID"];
            //udParams[1] = new SqlParameter("@COMPLAINT_DATE", SqlDbType.VarChar);
            //udParams[1].Value = user.COMPLAINT_DATE;
            //udParams[2] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
            //udParams[2].Value = user.LCM_CODE;
            //udParams[3] = new SqlParameter("@CLIENTNAME", SqlDbType.VarChar);
            //udParams[3].Value = user.CLIENTNAME;
            //udParams[4] = new SqlParameter("@CONTACT", SqlDbType.VarChar);
            //udParams[4].Value = user.CONTACT;
            //udParams[5] = new SqlParameter("@MAIN_CAT", SqlDbType.VarChar);
            //udParams[5].Value = user.MAIN_CAT;
            //udParams[6] = new SqlParameter("@SUB_CAT", SqlDbType.VarChar);
            //udParams[6].Value = user.SUB_CAT;
            //udParams[7] = new SqlParameter("@CLINIC_MGR_ID", SqlDbType.NVarChar);
            //udParams[7].Value = user.CLINIC_MGR_ID;
            //udParams[8] = new SqlParameter("@AOM_ID", SqlDbType.VarChar);
            //udParams[8].Value = user.AOM_ID;
            //udParams[9] = new SqlParameter("@STATUS", SqlDbType.Int);
            //udParams[9].Value = user.STATUS;
            //udParams[10] = new SqlParameter("@MODECOMMENTS", SqlDbType.NVarChar);
            //udParams[10].Value = user.MODECOMMENTS;
            //udParams[11] = new SqlParameter("@MODE", SqlDbType.Int);
            //udParams[11].Value = user.MODE;
            //udParams[12] = new SqlParameter("@STATUSCOMMENTS", SqlDbType.NVarChar);
            //udParams[12].Value = user.STATUSCOMMENTS;   //user.STATUSCOMMENTS == null ?"": user.STATUSCOMMENTS;
            //udParams[13] = new SqlParameter("@STATUSCOM", SqlDbType.NVarChar);
            //udParams[13].Value = user.STATUSCOM;
            //udParams[14] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            //udParams[14].Value = MaintenanceReq;
            //udParams[15] = new SqlParameter("@OTHERS", SqlDbType.VarChar);
            //udParams[15].Value = user.OTH_ID; ;
            DataSet dset = new DataSet();
            dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_RAISE_FDBK_REQ", param);

            DataSet dset2 = new DataSet();
            foreach (var File in user.PostedFiles)
            {
                //if (user.PostedFiles[fucount].Equals(File.name))
                //{
                long intsize = System.Convert.ToInt64(File.size);
                string strFileName;
                string strFileExt;
                if (intsize <= 30971520)
                {
                    string Upload_Time = getoffsetdatetime(DateTime.Now).ToString("hhmmss");
                    strFileName = System.IO.Path.GetFileName(File.name);
                    strFileExt = System.IO.Path.GetExtension(File.name);
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_FDBK_UPLOADED_FILES");
                    sp2.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String);
                    sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String);
                    sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String);
                    //sp2.Command.AddParameter("@REMARKS", user.ProbDesc, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["Uid"], DbType.String);
                    sp2.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
                    //sp2.ExecuteScalar();

                    dset2 = sp2.GetDataSet();
                }

            }
            if (dset2.Tables.Count > 0)
            {
                return new { Message = "Service Requisition(" + MaintenanceReq + ") raised successfully" };
            }
            else if (dset.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                return new { Message = "Service Requisition(" + MaintenanceReq + ") raised successfully" };
            }
            else
            {
                return new { Message = MessagesVM.ErrorMessage };
            }

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object getmainbysub(string code)
    {
        List<SUBCATlst> SUBlst = new List<SUBCATlst>();
        SUBCATlst sub;

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CODE", SqlDbType.NVarChar);
        param[0].Value = code;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_MAIN_BY_SUB", param))
        {
            while (sdr.Read())
            {
                sub = new SUBCATlst();
                sub.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                sub.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                sub.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                sub.ticked = false;
                SUBlst.Add(sub);
            }
        }
        if (SUBlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUBlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

}