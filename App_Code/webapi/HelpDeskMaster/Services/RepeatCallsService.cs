﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class RepeatCallsService
{

    SubSonic.StoredProcedure sp;

    public int Save(RepeatCallsModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_REPEAT_CALLS");
            sp.Command.AddParameter("@RPT_CODE", model.RPT_Code, DbType.String);
            sp.Command.AddParameter("@RPT_NAME", model.RPT_Name, DbType.String);
            sp.Command.AddParameter("@RPT_STA_ID", model.RPT_Status_Id, DbType.String);
            sp.Command.AddParameter("@RPT_REM", model.RPT_REM, DbType.String);
            sp.Command.Parameters.Add("@RPT_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }

    public Boolean Update(RepeatCallsModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_REPEAT_CALLS");
            sp.Command.AddParameter("@RPT_CODE", upd.RPT_Code, DbType.String);
            sp.Command.AddParameter("@RPT_NAME", upd.RPT_Name, DbType.String);
            sp.Command.AddParameter("@RPT_STA_ID", upd.RPT_Status_Id, DbType.Int32);
            sp.Command.AddParameter("@RPT_REM", upd.RPT_REM, DbType.String);
            sp.Command.Parameters.Add("@RPT_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.Execute();
            return true;
        }
        catch
        {
            throw;
        }
    }

    public IEnumerable<RepeatCallsModel> GetRepeatCallBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ALL_REPEAT_CALLS_GRID").GetReader())
            {
                List<RepeatCallsModel> rptcatlist = new List<RepeatCallsModel>();
                while (reader.Read())
                {
                    rptcatlist.Add(new RepeatCallsModel()
                    {
                        RPT_Code = reader.GetValue(0).ToString(),
                        RPT_Name = reader.GetValue(1).ToString(),
                        RPT_Status_Id = reader.GetValue(2).ToString(),
                        RPT_REM = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return rptcatlist;
            }
        }
        catch
        {
            throw;
        }
    }
}


