﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for FeedBackReportController
/// </summary>
public class HDMFeedBackReportController:ApiController
{
    HDMFeedBackReportService viewFDBKReport = new HDMFeedBackReportService();

    [HttpGet]
    public HttpResponseMessage GetGriddata()
    {
        var obj = viewFDBKReport.GetGriddata();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGriddataBySearch(BindFdbkGridDetailsBySearch bindGridDetailsBySearch)
    {
        var obj = viewFDBKReport.GetGriddataBySearch(bindGridDetailsBySearch);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid([FromBody]BindFdbkRDLCDetails data)
    {
        try
        {
            ReportGenerator<BindFdbkRDLCDetails> reportgen = new ReportGenerator<BindFdbkRDLCDetails>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/FeedbackReport.rdlc"),
                DataSetName = "FeedBackReport",
                ReportType = "FeedBack Report"
            };
            data.Type = "xls";
            viewFDBKReport = new HDMFeedBackReportService();
            List<BindFdbkRDLCDetails> reportdata = viewFDBKReport.GetReportGridrdlc();
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/FeedBackReport." + data.Type);
            await reportgen.GenerateReport(reportdata, filePath, data.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "FeedBackReport." + data.Type;
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }
}