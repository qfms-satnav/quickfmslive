﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for HDMconsolidatedReportController
/// </summary>
public class HDMReqStatusconsolidatedReportController : ApiController
{
    HDMReqStatusconsolidatedReportService hdmCR = new HDMReqStatusconsolidatedReportService();

    [HttpPost]
    public HttpResponseMessage BindGrid(HDMReqconsolidatedReportVM Params)
    {
        var obj = hdmCR.BindGrid(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage HDMconsolidatedChart(HDMReqconsolidatedReportVM Params)
    {
        var obj = hdmCR.HDMconsolidatedChart(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage HDMReq_Details(HDMReqconsolidatedReportVMChart Params)
    {
        var obj = hdmCR.HDMReq_Details(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
   
    [HttpPost]
    public async Task<HttpResponseMessage> Export_HDMconsolidatedRpt([FromBody] HDMReqconsolidatedReportVM exType)
    {

        ReportGenerator<HDMReqStatusconsolidatedReportVM> reportgen = new ReportGenerator<HDMReqStatusconsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/HDMRequestStatusReport.rdlc"),
            DataSetName = "HDMconsolidatedReport",
            ReportType = "Summary Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/HDMRequestStatusReport." + exType.DocType);
        List<HDMReqStatusconsolidatedReportVM> reportdata = hdmCR.GetReportList(exType);
        await reportgen.GenerateReport(reportdata, filePath, exType.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "HDMConsolidatedReport." + exType.DocType;
        return result;

    }
    //[HttpPost]
    //public async Task<HttpResponseMessage> ExportZoneconsolidatedRpt([FromBody]HDMReqStatusconsolidatedReportVM exType)
    //{

    //    ReportGenerator<HDMReqStatusconsolidatedReportVM> reportgen = new ReportGenerator<HDMReqStatusconsolidatedReportVM>()
    //    {
    //        ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/ZoneconsolidatedReport.rdlc"),
    //        DataSetName = "ZoneconsolidatedReport",
    //        ReportType = "Help Desk Zone Wise Requests"
    //    };

    //    string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/HDMRequestStatusReport." + exType.DocType);
    //    List<HDMReqStatusconsolidatedReportVM> reportdata = hdmCR.GetReportList_zone(exType);
    //    await reportgen.GenerateReport(reportdata, filePath, exType.DocType);
    //    HttpResponseMessage result = null;
    //    result = Request.CreateResponse(HttpStatusCode.OK);
    //    result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
    //    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
    //    result.Content.Headers.ContentDisposition.FileName = "ZoneConsolidatedReport." + exType.DocType;
    //    return result;

    //}
    public async Task<HttpResponseMessage> Export_HDMconsolidatedRpt1([FromBody] HDMReqconsolidatedReportVMChart exType)
    {

        ReportGenerator<HDMReqStatusconsolidatedReportVM> reportgen = new ReportGenerator<HDMReqStatusconsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/HDMReqReport.rdlc"),
            DataSetName = "HDMRequestReport",
            ReportType = "Summary Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/HDMReqReport." + exType.DocType);
        List<HDMReqStatusconsolidatedReportVM> reportdata = hdmCR.GetReportList1(exType);
        await reportgen.GenerateReport(reportdata, filePath, exType.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "HDMReqReport." + exType.DocType;
        return result;

    }
}