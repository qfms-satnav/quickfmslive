﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;
using QuickFMS.API.Filters;

public class HDMcustomizedReportMSController : ApiController
{
    HDMcustomizedReportMSService Csvc = new HDMcustomizedReportMSService();
    CustomizedReportView report = new CustomizedReportView();

    [HttpPost]
    public HttpResponseMessage GetCustomizedDetailsMS(HDMcustomizedDetailsMS Custmdata)
    {
        var obj = Csvc.GetCustomizedObjectMS(Custmdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedDetailsExportMS([FromBody] HDMcustomizedDetailsMS Custmdata)
    {
        ReportGenerator<GetCustomizedGridMS> reportgen = new ReportGenerator<GetCustomizedGridMS>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/CustomizableReportMS.rdlc"),
            DataSetName = "CustomizableReportMS"
        };
        Csvc = new HDMcustomizedReportMSService();
        List<GetCustomizedGridMS> reportdata = Csvc.GetCustomizedDetailsExportMS(Custmdata);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CustomizableReport." + Custmdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, Custmdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CustomizableReport." + Custmdata.Type;
        return result;
    }


}