﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;
public class SkabWorkOrderController : ApiController
{
    SkabWorkOrderService swoservice = new SkabWorkOrderService();

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage HelpdeskRequists()
    {
        var obj = swoservice.HelpdeskRequists();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage maintenanceRequests()
    {
        var obj = swoservice.maintenanceRequests();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetResource([FromUri] string Reqid)
    {
        var obj = swoservice.GetResource(Reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetMaintResource([FromUri] string Reqid)
    {
        var obj = swoservice.GetMaintResource(Reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getSparePartsData()
    {
        var obj = swoservice.getSparePartsData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetAssets()
    {
        var obj = swoservice.GetAssets();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetSubmitDetails(Assetdata data)
    {
        var obj = swoservice.GetSubmitDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMaintSubmitDetails(MAintdata data)
    {
        var obj = swoservice.GetMaintSubmitDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}