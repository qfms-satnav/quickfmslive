﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class HDMTATReportController : ApiController
{
    HDMTATReportService HTR = new HDMTATReportService();
    ReportView view = new ReportView();

    [HttpGet]
    public HttpResponseMessage GetRequestTypes()
    {
        var obj = HTR.GetRequestTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    //Getting Grid Data
    [HttpPost]
    public HttpResponseMessage GetGrid(HDMTAT_Params Spcdata)
    {
        var obj = HTR.GetSpaceReportDetails(Spcdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Getting Requisition Data On Popup
    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(HDMTAT_Details Spcdata)
    {
        var obj = HTR.GetReqDetails(Spcdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetSpaceRequisitionReportdata([FromBody]HDMTAT_Params Spcdata)
    {
        ReportGenerator<HDMTAT_Details> reportgen = new ReportGenerator<HDMTAT_Details>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/TATReport.rdlc"),
            DataSetName = "TATReportData",
            ReportType = "TAT Report"
        };

        HTR = new HDMTATReportService();
        List<HDMTAT_Details> reportdata = HTR.GetSpaceReportDetails(Spcdata);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/TATReport." + Spcdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, Spcdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "TATReport." + Spcdata.Type;
        return result;
    }
}
