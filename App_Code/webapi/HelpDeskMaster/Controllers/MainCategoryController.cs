﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuickFMS.API.Filters;

public class MainCategoryController : ApiController
{
    HDMMainCategoryService service = new HDMMainCategoryService();

    [HttpPost]
    public HttpResponseMessage Create(HDMMainCategoryModel mainCat)
    {
        if (service.Save(mainCat) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, mainCat);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already Exists");
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCompanyModules()
    {
        var obj = service.GetCompanyModules();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage HelpDeskModuleHide()
    {
        var obj = service.HelpDeskModuleHide();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UpdateMainCategoryData(HDMMainCategoryModel update)
    {

        if (service.Update(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage GetMainCategoryBindGrid()
    {
        IEnumerable<HDMMainCategoryModel> mncCatlist = service.MainCategoryBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, mncCatlist);
        return response;
    }

}
