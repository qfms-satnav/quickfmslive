﻿using System.Web.Http;
using QuickFMS.API.Filters;
using System.Net;
using System.Net.Http;
using System.IO;

/// <summary>
/// Summary description for HDMFeedBackController
/// </summary>
public class HDMFeedBackController : ApiController
{
    HDMFeedBackService feedBackService = new HDMFeedBackService();
    [HttpGet]
    public HttpResponseMessage GetMainCat()
    {
        var obj = feedBackService.GetMainCat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage Getsubcat()
    {
        var obj = feedBackService.Getsubcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage Getchildcat()
    {
        var obj = feedBackService.Getchildcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetStatusList()
    {
        var user = feedBackService.GetStatusList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetSubStatusList()
    {
        var user = feedBackService.GetSubStatusList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetClinicMangerByLoc(users usr)
    {
        var user = feedBackService.GetClinicMangerByLoc(usr);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetAOMByLoc(users usr)
    {
        var user = feedBackService.GetAOMByLoc(usr);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetOTHERSByLoc(users usr)
    {
        var user = feedBackService.GetOTHERSByLoc(usr);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetFdBackMode()
    {
        var user = feedBackService.GetFdBackMode();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveFDBckDetails(saveFDBCKDetailsIns dataobject)
    {
        var user = feedBackService.SaveFDBckDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;

        // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
        string sPath = "";
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/OLivaUploadFiles/FeedbackUploads/");

        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

        // CHECK THE FILE COUNT.
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];

            if (hpf.ContentLength > 0)
            {
                // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                {
                    // SAVE THE FILES IN THE FOLDER.
                    hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                    iUploadedCnt = iUploadedCnt + 1;
                }
            }
        }

        // RETURN A MESSAGE.
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }

    [HttpGet]
    public HttpResponseMessage getmainbysub(string code)
    {
        var obj = feedBackService.getmainbysub(code);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}