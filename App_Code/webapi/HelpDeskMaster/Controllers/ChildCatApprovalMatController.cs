﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ChildCatApprovalMatController : ApiController
{
    ChildCatApprovalMatService CAMS = new ChildCatApprovalMatService();

    [HttpGet]
    public HttpResponseMessage GetHMDList()
    {
        var obj = CAMS.GetHMDList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetRoleBasedEmployees(SelectedRole Role)
    {
        var obj = CAMS.GetRoleBasedEmployees(Role);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage InsertDetails(ChildCatHDMList ChildLst)
    {
        if (CAMS.InsertDetails(ChildLst).ToString() != "Already Exists")
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildLst);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Child Catgory Approval Matrix already Exists.");
    }

    [HttpPost]
    public HttpResponseMessage CheckMappingDetails(ChildCatHDMList ChildLst)
    {
        if (CAMS.CheckMappingDetails(ChildLst).ToString() != "Already Exists")
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Ok");
            return response;
        }
        else
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Child catgory is already mapped.");
            return response;
        }
    }

    [HttpPost]
    public HttpResponseMessage UpdateDetails(ChildCatHDMList ChildLst)
    {
        if (CAMS.UpdateDetails(ChildLst).ToString() != "CHILD CATEGORY IS ALREADY MAPPED")
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildLst);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Child catgory is already mapped.");
    }

    [HttpGet]
    public HttpResponseMessage GetGridDetails()
    {
        var Grid = CAMS.GetGridDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Grid);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage editData(GetData obj)
    {
        var Userlist = CAMS.editData(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Userlist);
        return response;
    }

}
