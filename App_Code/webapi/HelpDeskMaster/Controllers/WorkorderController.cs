﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;


/// <summary>
/// Summary description for WorkorderController
/// </summary>
public class WorkorderController : ApiController
{
    WorkorderService woservice = new WorkorderService();

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage HelpdeskRequists()
    {
        var obj = woservice.HelpdeskRequists();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage maintenanceRequests()
    {
        var obj = woservice.maintenanceRequests();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetResource([FromUri] string Reqid)
    {
        var obj = woservice.GetResource(Reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetMaintResource([FromUri] string Reqid)
    {
        var obj = woservice.GetMaintResource(Reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getSparePartsData()
    {
        var obj = woservice.getSparePartsData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetAssets()
    {
        var obj = woservice.GetAssets();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetSubmitDetails(Assetdata data)
    {
        var obj = woservice.GetSubmitDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMaintSubmitDetails(MAintdata data)
    {
        var obj = woservice.GetMaintSubmitDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}