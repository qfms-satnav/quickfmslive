﻿using QuickFMS.API.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;

/// <summary>
/// Summary description for HDMViewModifySubRequesitionsController
/// </summary>
public class HDMViewModifySubRequesitionsController : ApiController
{
    //GetDetailsByRequestId 
    HDMViewModifySubRequesitionsService ViewModifyreq = new HDMViewModifySubRequesitionsService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetDetailsByRequestId(GetDetailsByRequestId getDetailsByRequestId)
    {
        var obj = ViewModifyreq.GetDetailsByRequestId(getDetailsByRequestId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage ModifyEmpDetails(GetDetailsByRequestId dataobject)
    {
        var user = ViewModifyreq.ModifyEmpDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage ApproveEmployeeDetails(GetDetailsByRequestId dataobject)
    {
        var user = ViewModifyreq.ApproveEmployeeDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage CancelRequest(GetDetailsByRequestId dataobject)
    {
        var user = ViewModifyreq.CancelRequest(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetMainCat()
    {
        var obj = ViewModifyreq.GetMainCat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getsubcat()
    {
        var obj = ViewModifyreq.Getsubcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getchildcat()
    {
        var obj = ViewModifyreq.Getchildcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage UpdateRequest()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = ViewModifyreq.SaveFileUploads(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetApprovalStatus()
    {
        var user = ViewModifyreq.GetApprovalStatus();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    //[ValidateModelState]
    //[CheckModelForNull]
    //[HttpPost]
    //public HttpResponseMessage UploadTemplate()
    //{

    //    var httpRequest = HttpContext.Current.Request;
    //    dynamic retobj = FactService.UploadTemplate(httpRequest);
    //    string retContent = "";
    //    if (retobj.data != null)
    //        retContent = JsonConvert.SerializeObject(retobj);
    //    else
    //        retContent = JsonConvert.SerializeObject(retobj);

    //    var response = Request.CreateResponse(HttpStatusCode.OK);
    //    response.Content = new StringContent(retContent, Encoding.UTF8, "text/plain");
    //    response.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
    //    return response;
    //}

}