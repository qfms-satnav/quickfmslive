﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class CatDsgController : ApiController
{
    CatDsgService cdServ = new CatDsgService();

    [HttpGet]
    public HttpResponseMessage getDesignations()
    {
        var obj = cdServ.getDesignations();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage Create(CatDsgList saveCat)
    {
        if (cdServ.Save(saveCat) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, saveCat);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Mapping already exists");
    }

    [HttpPost]
    public HttpResponseMessage Update(CatDsgList updateCat)
    {

        if (cdServ.Update(updateCat))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, updateCat);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }


    //Grid View
    [HttpGet]
    public HttpResponseMessage GetMappingDetails()
    {
        var Grid = cdServ.GetMappingDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Grid);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage editData(String Id)
    {
        var Userlist = cdServ.editData(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Userlist);
        return response;
    }

}
