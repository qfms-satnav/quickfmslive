﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for HDMViewFeedbackController
/// </summary>
public class HDMViewFeedbackController:ApiController
{
    HDMViewFeedbackService viewFbckSer = new HDMViewFeedbackService();
    [HttpGet]
    public HttpResponseMessage GetGriddata()
    {
        var obj = viewFbckSer.GetGriddata();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGriddataBySearch(BindFdbkGridDetailsBySearch bindGridDetailsBySearch)
    {
        var obj = viewFbckSer.GetGriddataBySearch(bindGridDetailsBySearch);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}