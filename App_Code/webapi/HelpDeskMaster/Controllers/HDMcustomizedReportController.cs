﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json;
using QuickFMS.API.Filters;

public class HDMcustomizedReportController : ApiController
{
    HDMcustomizedReportService Csvc = new HDMcustomizedReportService();
    CustomizedReportView report = new CustomizedReportView();

    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(HDMcustomizedDetails Custdata)
    {
         var obj = Csvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //[HttpPost]
    //public HttpResponseMessage GetCustomizedDetailsExport(HDMcustomizedDetails Custdata)
    //{
    //    var obj = Csvc.GetCustomizedDetailsExport(Custdata);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}

    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedDetailsExport([FromBody]HDMcustomizedDetails Custdata)
    {
        ReportGenerator<GetCustomizedGrid> reportgen = new ReportGenerator<GetCustomizedGrid>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/CustomizableReport.rdlc"),
            DataSetName = "CustomizableReport"
        };
        Csvc = new HDMcustomizedReportService();
        List<GetCustomizedGrid> reportdata = Csvc.GetCustomizedDetailsExport(Custdata);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CustomizableReport." + Custdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, Custdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CustomizableReport." + Custdata.Type;
        return result;
    }


}