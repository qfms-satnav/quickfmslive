﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

/// <summary>
/// Summary description for HDMISFCReportByUserController
/// </summary>
public class HDMISFCReportByUserController : ApiController
{
    HDMISFCReportByUserService rptbyUserSer = new HDMISFCReportByUserService();
    [HttpPost]
    public HttpResponseMessage GetHDMReportByUser([FromBody] HDMReportByUser hdm)
    {
        var obj = rptbyUserSer.GetHDMReportByUser(hdm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetHistory([FromBody] HDMReportByUserVM hdm)
    {
        var obj = rptbyUserSer.GetHistory(hdm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetStatusTypes()
    {
        var obj = rptbyUserSer.GetStatusTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetReportByUser([FromBody]HDMReportByUser rptByUser)
    {

        ReportGenerator<HDMISFCReportByUserVM> reportgen = new ReportGenerator<HDMISFCReportByUserVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/ISFCReportByUser.rdlc"),
            DataSetName = "RptByUser",
            ReportType = "Report By User"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ReportByUser." + rptByUser.DocType);
        List<HDMISFCReportByUserVM> reportdata = rptbyUserSer.GetHDMReportByUser(rptByUser);
        await reportgen.GenerateReport(reportdata, filePath, rptByUser.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ReportByUser." + rptByUser.DocType;
        return result;

    }

    [HttpPost]
    public HttpResponseMessage GetLocationWiseCount(HDMReportByUser data)
    {
        var obj = rptbyUserSer.GetLocationWiseCount(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetSubCatWiseCount(HDMReportByUser data)
    {
        var obj = rptbyUserSer.GetSubCatWiseCount(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]

    public HttpResponseMessage DloadFiles(HDMReportByUser data)
    {
        var obj = rptbyUserSer.DloadFiles(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


   
}