﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for HDMViewSubRequistionsController
/// </summary>
public class HDMViewSubRequistionsController : ApiController
{
    HDMViewSubRequistionsService viewSubReq = new HDMViewSubRequistionsService();
    //[GzipCompression]

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetMainCat()
    {
        var obj = viewSubReq.GetMainCat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getsubcat()
    {
        var obj = viewSubReq.Getsubcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getchildcat()
    {
        var obj = viewSubReq.Getchildcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetGriddata()
    {
        var obj = viewSubReq.GetGriddata();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetGriddataBySearch(BindGridDetailsBySearch bindGridDetailsBySearch)
    {
        var obj = viewSubReq.GetGriddataBySearch(bindGridDetailsBySearch);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetStatusList()
    {
        var user = viewSubReq.GetStatusList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
}