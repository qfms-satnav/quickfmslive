﻿using System;
using QuickFMS.API.Filters;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

public class ServiceEscalationMappingController : ApiController
{
    ServiceEscalationMappingService SEMService = new ServiceEscalationMappingService();
    //main cat dropdowm
    [HttpGet]
    public HttpResponseMessage GetMaincategory()
    {
        IEnumerable<MainCategoryModel> maincategorylist = SEMService.GetActiveMainCategories();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }
    //sub dropdown by main
    [HttpGet]
    public HttpResponseMessage GetSubCategoryByMain(String id)
    {
        IEnumerable<SubCategoryModel> subcategorylist = SEMService.GetActiveSubCategoriesByMain(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcategorylist);
        return response;
    }
    //Child dropdown by sub
    [HttpGet]
    public HttpResponseMessage GetChildCategoryBySubCat(String id)
    {
        IEnumerable<ChildCategoryModel> childcategorylist = SEMService.GetActiveChildCategoriesBySub(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, childcategorylist);
        return response;
    }


    //country
    [HttpGet]
    public HttpResponseMessage GetCountries()
    {
        IEnumerable<Country> countrylist = SEMService.GetActiveCountries();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, countrylist);
        return response;
    }
    //city by country
    [HttpGet]
    public HttpResponseMessage GetCityByCountry(String id)
    {
        IEnumerable<City> citylist = SEMService.GetActiveCityByCountry(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, citylist);
        return response;
    }
    //location by city
    [HttpGet]
    public HttpResponseMessage GetLocationByCity(String id)
    {
        IEnumerable<Location> locationlist = SEMService.GetActiveLocationByCity(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, locationlist);
        return response;
    }

    //tower by location
    [HttpGet]
    public HttpResponseMessage GetTowerByLocation(String id)
    {
        IEnumerable<Tower> locationlist = SEMService.GetActiveTowerByLocation(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, locationlist);
        return response;
    }


    //Grid View
    [HttpGet]
    public HttpResponseMessage GetSEMDetails(int id)
    {
        IEnumerable<ServiceEscalationMappingModel> SEMlist = SEMService.GetSEMDetails(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SEMlist);
        return response;
    }
    //to get role user data
    [HttpGet]
    public HttpResponseMessage GetRoleUserDetails(int id)
    {
        IEnumerable<ServiceEscalationMappingDetails> SEMlist = SEMService.GetRoleUserDetails(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SEMlist);
        return response;
    }
    // edit
    [HttpGet]
    public HttpResponseMessage EditSEMDetails(int id)
    {
        IEnumerable<ServiceEscalationMappingDetails> SEMlist = SEMService.EditSEMDetails(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SEMlist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetRoleUserListByModule(int id)
    {
        IEnumerable<ServiceEscalationMappingDetails> SEMlist = SEMService.GetRoleUserListByModule(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SEMlist);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveDetails(ServiceEscalationMappingVM dataobject)
    {
        var obj = SEMService.Save(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UpdateDetails(ServiceEscalationMappingVM dataobject)
    {

        var obj = SEMService.UpdateSEMDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

        //if (SEMService.UpdateSEMDetails(dataobject).SEM_ID == 0)
        //{
        //    return Request.CreateResponse(HttpStatusCode.BadRequest, "Data Already Exists With Same Details");
        //}
        //else
        //{
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, dataobject);
        //    return response;
        //}

    }
    [HttpPost]
    public async Task<HttpResponseMessage> DownloadTemplate([FromBody] ServiceEscalationMappingVM sermap)
    {

        ReportGenerator<ServiceEscalationMappingVM> reportgen = new ReportGenerator<ServiceEscalationMappingVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/UploadHDMServiceEscalationMapping.rdlc"),
            DataSetName = "HdmDwnldServiceMapping",

        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/HelpDeskUploadTemplate.xlsx");
        List<ServiceEscalationMappingVM> reportdata = SEMService.DownloadTemplate(sermap);
        await reportgen.GenerateReport(reportdata, filePath, "xlsx");
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "HelpDeskUploadTemplate.xlsx";
        return result;

    }

    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = SEMService.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}


