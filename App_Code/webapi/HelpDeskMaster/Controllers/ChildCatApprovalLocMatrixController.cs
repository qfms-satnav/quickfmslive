﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using HDMUtilityVM;

/// <summary>
/// Summary description for ChildCatApprovalLocMatrixController
/// </summary>
public class ChildCatApprovalLocMatrixController:ApiController
{
    ChildCatApprovalLocMatrixService CCAM = new ChildCatApprovalLocMatrixService();
    [HttpGet]
    public HttpResponseMessage GetGridDetails()
    {
        var Grid = CCAM.GetGridDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Grid);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage editData(GetData obj)
    {
        var Userlist = CCAM.editData(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Userlist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage InsertDetails(ChildCatHDMList ChildLst)
    {
        if (CCAM.InsertDetails(ChildLst).ToString() != "Already Exists")
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildLst);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Child Category Approval Matrix already exists.");
    }
    [HttpPost]
    public HttpResponseMessage UpdateDetails(ChildCatHDMList ChildLst)
    {
        if (CCAM.UpdateDetails(ChildLst).ToString() != "CHILD CATEGORY IS ALREADY MAPPED")
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildLst);
            return response;
        }

        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Child category is already mapped.");
    }
    [HttpPost]
    public HttpResponseMessage GetChildCategoryByModule(HDMSubCategoryModel com)
    {

        IEnumerable<ChildCatlst> maincategorylist = CCAM.GetChildCategoryByModule(com);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage DeleteSeat(DeleteData del)
    {
        var user = CCAM.DeleteSeat(del.CAM_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

}