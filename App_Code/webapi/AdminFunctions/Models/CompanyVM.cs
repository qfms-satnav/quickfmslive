﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for CompanyVM
/// </summary>
public class CompanyVM
{
    public int CNP_ID { get; set; }
    public string CNP_NAME { get; set; }
    public int CNP_PARENT_ID { get; set; }
    public string CNP_CNY_CODE { get; set; }
    public string CNP_CTY_CODE { get; set; }
    public string CNP_LOC_CODE { get; set; }
    public string CNP_ADDRESS { get; set; }
    public string CNP_REMARKS { get; set; }
    public string CNP_STA_ID { get; set; }
    public string CNP_MODULE { get; set; }
    public List<ModuleVM> MODlst { get; set; }
}

public class ModuleVM
{
    public string MOD_CODE { get; set; }
    public string MOD_NAME { get; set; }
    public bool ticked { get; set; }
}