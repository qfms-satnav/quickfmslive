﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UserRoleMappingVM
/// </summary>
public class UserRoleMappingVM
{
    public UserRoleMapping UserRoleMapping { get; set; }
    public List<UserRoleVM> UserRoleVM { get; set; }
    public USERMAPPINGDETAILS USERMAPPINGDETAILS { get; set; }
    public List<Module> modulelist { get; set; }
    public string MapLevelAccess { get; set; }
    public string Type { get; set; }
}
public class UserRoleVM
{
    
    public int ROL_ID { get; set; }  
    public string ROL_ACRONYM { get; set; }
    public string ROL_DESCRIPTION { get; set; }
    public Boolean isChecked { get; set; }
}

public class UserRoleMapping
{
    public string AUR_ID { get; set; }
    public string NEW_AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string AUR_EMAIL { get; set; }
    public string ROL_DESCRIPTION { get; set; }
    public string AUR_DESGN_ID { get; set; }
    public string AUR_DEP_ID { get; set; }
    public string AUR_LOCATION { get; set; }
    public string AUR_RES_NUMBER { get; set; }
    public Nullable<System.DateTime> AUR_DOB { get; set; }
    public string AUR_DOJ { get; set; }
    public string AUR_COUNTRY { get; set; }
    public string AUR_TIME_ZONE { get; set; }
    public string AUR_REPORTING_TO { get; set; }
    public string AUR_EXTENSION { get; set; }
    public int AUR_STA_ID { get; set; }
    public string AUR_PASSWORD { get; set; }
    public string AUR_OLD_PASSWORD { get; set; }
    public string AUR_REPORTING_TO_NAME { get; set; }
    public string VERTICAL { get; set; }
    public string LOCATION { get; set; }
    public string COSTCENTER { get; set; }
    public string AUR_VERTICAL_NAME { get; set; }
    public string AUR_COSTCENTER_NAME { get; set; }
    public string MAP_ACCESS { get; set; }
    public string EMP_COUNT { get; set; }
    public string DSG_CODE { get; set; }
    public string AUR_GENDER { get; set; }
    public string AUR_GRADE { get; set; }
    public string CHE_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string AUR_TYPE { get; set; }
    public Nullable<System.DateTime> AUR_LWD { get; set; }
    public Nullable<System.DateTime> AUR_TRANSFER_DT { get; set; }
    public string DEP_NAME { get; set; }
    public string AUR_EMP_REMARKS { get; set; }
    public string Status { get; set; }
}
public class Module
{
    public string MOD_CODE { get; set; }
    public string MOD_NAME { get; set; }
}
public class USERMAPPINGDETAILS
{
    public List<Countrylst> selectedCountries { get; set; }
    public List<Citylst> selectedCities { get; set; }
    public List<Locationlst> selectedLocations { get; set; }
    public List<Towerlst> selectedTowers { get; set; }
    public List<Floorlst> selectedFloors { get; set; }
    public List<Verticallst> selectedVerticals { get; set; }
    public List<Costcenterlst> selectedCostcenter { get; set; }
    public List<ParentEntityLst> selectedParentEntity { get; set; }
    public List<ChildEntityLst> selectedChildEntity { get; set; }
}

public class UserCompanyModules
{
    public string AurId { get; set; }
    public string ModuleId { get; set; }
}
public class InactiveSpace
{
    public string AurId { get; set; }
    public string Status { get; set; }
}