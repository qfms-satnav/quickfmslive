﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.OleDb;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;

/// <summary>
/// Summary description for UserRoleMappingService
/// </summary>
public class UserRoleMappingNewService
{
    SubSonic.StoredProcedure sp;

    /**************************************************************************************************************/

   
    public dynamic UploadTemplate(HttpRequest httpRequest)
    {
        try
        {
            if (httpRequest.Files.Count > 0)
            {
                SqlParameter[] udParams = new SqlParameter[1];
                String guid = System.Guid.NewGuid().ToString();
                DataTable UsrDtl = GetDataTableFrmReq(httpRequest);
                string strSQL;
                if (UsrDtl == null)
                    return new { Message = MessagesVM.UAD_UPLVALDAT, data = (object)null };
                if (UsrDtl.Rows.Count > 0)
                {
                    foreach (DataRow x in UsrDtl.Select())
                    {
                        strSQL = "INSERT INTO " + HttpContext.Current.Session["TENANT"].ToString() + "." + "UDM_MAPPING_DETAILS_TEMP (HST_REQ_ID, HST_CNY_NAME,HST_CTY_NAME,HST_LCM_NAME, HST_TWR_NAME, HST_FLR_NAME, HST_VERT_NAME, HST_CST_NAME, HST_PAR_ENTY_NAME, HST_CHL_ENTY_NAME, HST_USR_ID,HST_USR_ROLE_NAME, HST_CREATED_DT, HST_CREATED_BY) VALUES('" + guid + "','" + x["COUNTRY_NAME"].ToString().Replace("'", "''") + "','" + x["CITY_NAME"].ToString().Replace("'", "''") + "','" + x["LOCATION_NAME"].ToString().Replace("'", "''") + "','" + x["TOWER_NAME"].ToString().Replace("'", "''") + "','" + x["FLOOR_NAME"].ToString().Replace("'", "''") + "','" + x["VERTICAL_NAME"].ToString().Replace("'", "''") + "','" + x["COSTCENTER_NAME"].ToString().Replace("'", "''") + "','" + x["PARENT_ENTITY"].ToString().Replace("'", "''") + "','" + x["CHILD_ENTITY"].ToString().Replace("'", "''") + "','" + x["USER_ID"] + "','" + x["USER_ROLE"] + "','" + System.DateTime.Now + "','" + HttpContext.Current.Session["UID"].ToString() + "')";
                        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL);
                    }
                    udParams[0] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
                    udParams[0].Value = guid;
                    DataSet DS = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "UDM_EXCEL_UPLOAD_CHECKER", udParams);
                    return new { Message = MessagesVM.UAD_UPLNOREC, data = DS };
                }
                else
                    return new { Message = MessagesVM.UAD_UPLNOREC, data = (object)null };
            }
            else
                return new { Message = MessagesVM.UAD_UPLNO, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = "Invalid Input! Please try again", data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public DataTable GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable uplst = new DataTable();
        foreach (string file in httpRequest.Files)
        {
            var postedFile = httpRequest.Files[file];
            var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
            postedFile.SaveAs(filePath);
            uplst = ReadAsList(filePath);
        }
        if (uplst != null)
            if (uplst.Rows.Count != 0)
                uplst.Rows.RemoveAt(0);
        return uplst;
    }

    /**************************************************************************************************************/
    public DataTable ReadAsList(string Path)
    {
        string fileName = Path;
        SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false);
        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        string relationshipId = sheets.First().Id.Value;
        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        Worksheet workSheet = worksheetPart.Worksheet;
        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        IEnumerable<Row> rows = sheetData.Descendants<Row>();
        DataTable dt = new DataTable();
        foreach (Cell cell in rows.ElementAt(0))
        {
            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
        }
        foreach (Row row in rows) //this will also include your header row...
        {
            DataRow tempRow = dt.NewRow();
            int columnIndex = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                // Gets the column index of the cell with data
                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                cellColumnIndex--; //zero based index
                if (columnIndex < cellColumnIndex)
                {
                    do
                    {
                        tempRow[columnIndex] = ""; //Insert blank data here;
                        columnIndex++;
                    }
                    while (columnIndex < cellColumnIndex);
                }
                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                columnIndex++;
            }
            dt.Rows.Add(tempRow);
        }
        spreadSheetDocument.Close();
        return dt;
    }

    /**************************************************************************************************************/
    public static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        if (cell.CellValue == null)
        {
            return "";
        }
        string value = cell.CellValue.InnerXml;
        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }

    /**************************************************************************************************************/
    public static int? GetColumnIndexFromName(string columnName)
    {
        //return columnIndex;
        string name = columnName;
        int number = 0;
        int pow = 1;
        for (int i = name.Length - 1; i >= 0; i--)
        {
            number += (name[i] - 'A' + 1) * pow;
            pow *= 26;
        }
        return number;
    }

    /**************************************************************************************************************/
    public static string GetColumnName(string cellReference)
    {
        Regex regex = new Regex("[A-Za-z]+");
        Match match = regex.Match(cellReference);
        return match.Value;
    }

    /**************************************************************************************************************/
    public object GetUserRoleDetails()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ADM_GET_USER_ROLE_MAPPING_DETAILS_NEW");
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public object GetUserEmpDetails(string Id)
    {
        try
        {
            string UID = Id.Replace("__", "/");
            UserRoleMapping URMlist = new UserRoleMapping();
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ADM_GET_AMANTRA_USER_DETAILS_NEW");
            sp.Command.Parameters.Add("@AUR_ID", UID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public object SaveEmpDetails(UserRoleMapping user)
    {
        try
        {
            string UID = System.Web.HttpUtility.UrlDecode(user.AUR_ID);
            UID = UID.Replace("_", "/");
            SqlParameter[] udParams = new SqlParameter[18];
            udParams[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            udParams[0].Value = UID;
            udParams[1] = new SqlParameter("@AUR_EMAIL", SqlDbType.VarChar);
            udParams[1].Value = user.AUR_EMAIL;
            udParams[2] = new SqlParameter("@AUR_PHONE", SqlDbType.VarChar);
            udParams[2].Value = user.AUR_RES_NUMBER;
            udParams[3] = new SqlParameter("@AUR_DOB", SqlDbType.Date);
            udParams[3].Value = user.AUR_DOB;
            udParams[4] = new SqlParameter("@AUR_DOJ", SqlDbType.VarChar);
            udParams[4].Value = user.AUR_DOJ;
            udParams[5] = new SqlParameter("@AUR_CNY", SqlDbType.VarChar);
            udParams[5].Value = user.AUR_COUNTRY;
            udParams[6] = new SqlParameter("@AUR_REPTO", SqlDbType.VarChar);
            udParams[6].Value = user.AUR_REPORTING_TO;
            udParams[7] = new SqlParameter("@AUR_EXTN", SqlDbType.VarChar);
            udParams[7].Value = user.AUR_EXTENSION;
            udParams[8] = new SqlParameter("@AUR_STATUS", SqlDbType.VarChar);
            udParams[8].Value = user.AUR_STA_ID;
            udParams[9] = new SqlParameter("@AUR_PASSWORD", SqlDbType.NVarChar);
            udParams[9].Value = user.AUR_PASSWORD;
            udParams[10] = new SqlParameter("@AUR_KNOWN_AS", SqlDbType.NVarChar);
            udParams[10].Value = user.AUR_KNOWN_AS;
            udParams[11] = new SqlParameter("@VERTICAL", SqlDbType.NVarChar);
            udParams[11].Value = user.VERTICAL;
            udParams[12] = new SqlParameter("@COSTCENTER", SqlDbType.NVarChar);
            udParams[12].Value = user.COSTCENTER;
            udParams[13] = new SqlParameter("@NEW_AUR_ID", SqlDbType.NVarChar);
            udParams[13].Value = user.NEW_AUR_ID;
            udParams[14] = new SqlParameter("@DSG", SqlDbType.NVarChar);
            udParams[14].Value = user.DSG_CODE;
            udParams[15] = new SqlParameter("@LCM", SqlDbType.NVarChar);
            udParams[15].Value = user.LOCATION;
            udParams[16] = new SqlParameter("@AUR_GRADE", SqlDbType.NVarChar);
            udParams[16].Value = user.AUR_GRADE;
            udParams[17] = new SqlParameter("@AUR_EMP_REMARKS", SqlDbType.NVarChar);
            udParams[17].Value = user.AUR_EMP_REMARKS;

            int returnvalue = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "UPDATE_USERDETAILS", udParams);
            if (returnvalue == 1)
            {
                return new { Message = "EmployeeId already Exists" };
            }
            else
            {
                if (user.AUR_OLD_PASSWORD != null)
                {
                    if (user.AUR_PASSWORD.ToLower() != user.AUR_OLD_PASSWORD.ToLower())
                    {
                        SqlParameter[] ParamsEmail = new SqlParameter[3];
                        ParamsEmail[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                        ParamsEmail[0].Value = user.AUR_ID;
                        ParamsEmail[1] = new SqlParameter("@ADMIN_ID", SqlDbType.VarChar);
                        ParamsEmail[1].Value = HttpContext.Current.Session["UID"].ToString();
                        ParamsEmail[2] = new SqlParameter("@NEW_PASSWORD", SqlDbType.VarChar);
                        ParamsEmail[2].Value = user.AUR_PASSWORD;
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PASSWORD_CHANGE_EMAIL_NOTIFICATION", ParamsEmail);
                    }
                }
                //Object MRdetails = GetUserModRolesDetails(user.AUR_ID);
                return new { Message = MessagesVM.UM_OK, data = new { usr = "", MRD = "" } };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public object Getmodules()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ADM_GET_COMPANY_MODULES_BY_USER_COMPANY");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public object GetAccessByModule(UserCompanyModules ucm)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ADM_Get_Roles_By_Module");
            sp.Command.Parameters.Add("@MOD_NAME", ucm.ModuleId, DbType.String);
            sp.Command.Parameters.Add("@AUR_NAME", ucm.AurId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public DataSet EmployeeData()
    {
        List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
        DataSet dataSet = new DataSet();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_GET_USER_ROLE_MAPPING_DETAILS_NEW");
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            return sp.GetDataSet();
        }
        catch (Exception ex)
        {
            return dataSet;
        }
    }

    

    /**************************************************************************************************************/
    public object SaveRoleModDetls(UserRoleMappingVM user)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@ROLLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(user.UserRoleVM);
            param[1] = new SqlParameter("@UDM_CREATED_BY", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@UDM_AUR_ID", SqlDbType.NVarChar);
            param[2].Value = user.UserRoleMapping.AUR_ID;
            param[3] = new SqlParameter("@MAP_ACCESS", SqlDbType.NVarChar);
            param[3].Value = user.MapLevelAccess;
            param[4] = new SqlParameter("@COMPANY", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["COMPANYID"];
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_ROLE_DETAILS", param);

            // mapping details
            //Object mappingDtls = GetUserMappingDetails(user.UserRoleMapping.AUR_ID);
            return new { Message = MessagesVM.UM_OK, data = new { RES = "" } };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public object GetAllMapData(string Id)
    {
        try
        {
            DataSet ds = new DataSet();
            //sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "MAPPING_PROCEDURE");
            //sp.Command.Parameters.Add("@AUR_ID", Id, DbType.String);
            //ds = sp.GetDataSet();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = Id;
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MAPPING_PROCEDURE", param);
            if (ds.Tables.Count != 0)
                return new { Message = "OK", data = ds.Tables[0], data1 = ds.Tables[1], data2 = ds.Tables[2]
                ,data3 = ds.Tables[3],data4 = ds.Tables[4],data5 = ds.Tables[5]};
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public object SaveMappingDetails(UserRoleMappingVM user)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@TWRLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedTowers);
            param[1] = new SqlParameter("@PARLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedParentEntity);
            param[2] = new SqlParameter("@CHDLST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedChildEntity);
            param[3] = new SqlParameter("@VERLST", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedVerticals);
            param[4] = new SqlParameter("@COSTLIST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedCostcenter);
            param[5] = new SqlParameter("@UDM_CREATED_BY", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["UID"];
            param[6] = new SqlParameter("@UDM_AUR_ID", SqlDbType.NVarChar);
            param[6].Value = user.UserRoleMapping.AUR_ID;
            param[7] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[7].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedLocations);
            param[8] = new SqlParameter("@MAP_ACCESS", SqlDbType.NVarChar);
            param[8].Value = user.MapLevelAccess;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_MAPPING_DETAILS", param);


            SqlParameter[] flrparam = new SqlParameter[3];
            flrparam[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            flrparam[0].Value = UtilityService.ConvertToDataTable(user.USERMAPPINGDETAILS.selectedFloors);
            flrparam[1] = new SqlParameter("@UFM_CREATED_BY", SqlDbType.NVarChar);
            flrparam[1].Value = HttpContext.Current.Session["UID"];
            flrparam[2] = new SqlParameter("@UFM_AUR_ID", SqlDbType.NVarChar);
            flrparam[2].Value = user.UserRoleMapping.AUR_ID;
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSERT_USER_FLOOR_MAPPING_ADM", flrparam);
            return new { Message = MessagesVM.UM_OK, data = new { usr = "" } };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/
    public object DeleteEmployee(string id)
    {
        try
        {
            string UID = id.Replace("__", "/");
            UID = System.Web.HttpUtility.UrlDecode(UID); // to replace spaces and comma etc from %5C %22 %2F %3E %2C %3A with \ " / > , :
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GN_EMPLOYEE_DELETE");
            sp.Command.AddParameter("@AUR_ID", UID, DbType.String);
            sp.Command.AddParameter("@SESSION_ID", HttpContext.Current.Session["UID"], DbType.String);
            int dlt = (int)sp.ExecuteScalar();
            if (dlt == 1)
            {
                return new { Message = MessagesVM.UM_OK, data = dlt };
            }
            else
            {
                return new { Message = MessagesVM.Err };
            }
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    /**************************************************************************************************************/

    public object InactiveSpaces(InactiveSpace user)
    {
        string UID = user.AurId.Replace("_", "/");

        SqlParameter[] InacParams = new SqlParameter[2];
        InacParams[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        InacParams[0].Value = UID;
        InacParams[1] = new SqlParameter("@AUR_STATUS", SqlDbType.VarChar);
        InacParams[1].Value = 0;
        int returnvalue = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "INACTIVE_EMPLOYEE", InacParams);
        if (returnvalue == 1)
        {
            return new { Status = 1, Message = MessagesVM.UM_OK };
        }
        else { return new { Message = MessagesVM.Err }; }
    }
}

