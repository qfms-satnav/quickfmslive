﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Data;
using System.Globalization;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

public class UserRoleMappingNewController : ApiController
{
    UserRoleMappingNewService UserRoleService = new UserRoleMappingNewService();

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        HttpPostedFile file = HttpContext.Current.Request.Files[0];
        string Extension;
        string[] ExtensionList = file.FileName.Split('.');
        Extension = ExtensionList.Last();
        dynamic retobj = "";
        if (Extension.Equals("xlsx") || Extension.Equals("xls"))
        {
            retobj = UserRoleService.UploadTemplate(httpRequest);
        }
        else
        {
            var response1 = Request.CreateResponse(HttpStatusCode.OK);
            response1.Content = new StringContent("Invalid File Format", Encoding.UTF8, "text/plain");
            response1.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
            return response1;
        }
        string retContent = "";
        if (retobj.data != null)
            retContent = JsonConvert.SerializeObject(retobj);
        else
            retContent = JsonConvert.SerializeObject(retobj);
        var response = Request.CreateResponse(HttpStatusCode.OK);
        response.Content = new StringContent(retContent, Encoding.UTF8, "text/plain");
        response.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
        return response;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetUserRoleDetails()
    {
        var URMlist = UserRoleService.GetUserRoleDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetUserEmpDetails(String Id)
    {
        var URMlist = UserRoleService.GetUserEmpDetails(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage SaveEmpDetails(UserRoleMapping dataobject)
    {
        var user = UserRoleService.SaveEmpDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getmodules()
    {
        var modulelist = UserRoleService.Getmodules();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, modulelist);
        return response;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetAccessByModule(UserCompanyModules ucm)
    {
        var Rolelist = UserRoleService.GetAccessByModule(ucm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Rolelist);
        return response;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetEmployeeData([FromBody] UserRoleMappingVM EmpData)
    {
        ReportGenerator<UserRoleMapping> GenEmployeeData = new ReportGenerator<UserRoleMapping>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/EmployeeData.rdlc"),
            DataSetName = "EmployeeData",
            ReportType = "Employee Data"
        };
        UserRoleService = new UserRoleMappingNewService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/EmployeeData." + EmpData.Type);
        DataSet dataSet = UserRoleService.EmployeeData();
        await GenEmployeeData.GenerateReport(dataSet.Tables[0], filePath, EmpData.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "EmployeeData." + EmpData.Type;
        return result;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage SaveRoleModDetls(UserRoleMappingVM dataobject)
    {
        var user = UserRoleService.SaveRoleModDetls(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    /**************************************************************************************************************/
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetAllMapData(String Id)
    {
        var modulelist = UserRoleService.GetAllMapData(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, modulelist);
        return response;
    }

    /**************************************************************************************************************/
    [HttpPost]
    public HttpResponseMessage SaveMappingDetails(UserRoleMappingVM dataobject)
    {
        var URMlist = UserRoleService.SaveMappingDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    /**************************************************************************************************************/
    [HttpPost]
    public HttpResponseMessage DeleteSeat(UserCompanyModules dataobject)
    {
        var user = UserRoleService.DeleteEmployee(dataobject.AurId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    /**************************************************************************************************************/
    [HttpPost]
    public HttpResponseMessage InactiveSpaces(InactiveSpace user)
    {
        var modulelist = UserRoleService.InactiveSpaces(user);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, modulelist);
        return response;
    }
}
