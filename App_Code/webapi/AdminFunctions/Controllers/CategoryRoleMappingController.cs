﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;
public class CategoryRoleMappingController : ApiController
{
	CategoryRoleMappingService CRMS = new CategoryRoleMappingService();

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedRole(EquipmentRL Equipment)
    {
        var obj = CRMS.GetCustomizedObject(Equipment);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetGridRoleData()
    {
        var obj = CRMS.GetGridRoleData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}