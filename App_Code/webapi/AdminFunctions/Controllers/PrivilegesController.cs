﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// Summary description for PrivilegesController
/// </summary>
public class PrivilegesController : ApiController
{
    PrivilegesService prvlgService = new PrivilegesService();
    
   
    [HttpGet]
    public HttpResponseMessage GetRoledataObj(string id)
    {
        object prvlglist = prvlgService.Get(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, prvlglist);
        return response;
    }

    //To save Privileges
    [HttpPost]
    public object SavePrivileges(RoleVM dataobject)
    {
        return prvlgService.SavePrivileges(dataobject);
    }



    [HttpGet]
    public HttpResponseMessage GetPrivilegesDataByRoleID(string id)
    {
        object prvlglist = prvlgService.getPrivilegesByRoleID(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, prvlglist);
        return response;
    }

    //To save Privileges
    [HttpPost]
    public object save(saveGrantedPermissions grntdPermissions)
    {
        return prvlgService.SaveGrantedPermissions(grntdPermissions);
    }


}