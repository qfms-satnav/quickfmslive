﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;


public class CompanyController : ApiController
{
    CompanyService CompanyService = new CompanyService();

    [HttpGet]
    public object GetModules()
    {
        return CompanyService.GetModules();
    }

    //Grid View
    [HttpGet]
    public object GetCompanyList()
    {
        return CompanyService.GetCompanyList();
    }

    //To save records
    [HttpPost]
    public HttpResponseMessage Save([FromBody] CompanyVM CNPdet)
    {
        var obj = CompanyService.Save(CNPdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GRPSave([FromBody] CompanyVM GRPCNPdet)
    {
        var obj = CompanyService.GRPSave(GRPCNPdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Modify([FromBody] CompanyVM CNPdet)
    {
        var obj = CompanyService.Modify(CNPdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GRPModify([FromBody] CompanyVM GRPCNPdet)
    {
        var obj = CompanyService.GRPModify(GRPCNPdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GRPEdit([FromBody] CompanyVM GRPCNPdet)
    {
        var obj = CompanyService.GRPEdit(GRPCNPdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
