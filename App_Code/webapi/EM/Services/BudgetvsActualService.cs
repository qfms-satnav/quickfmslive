﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class BudgetvsActualService
{
    SubSonic.StoredProcedure sp;
    List<BudgetvsActualReportData> Cust;
    BudgetvsActualReportData Custm;
    DataSet ds;

    public object GetBudgetvsActualObject(BudgetvsActualParams Det)
    {
        try
        {
            Cust = GetBudgetvsActualDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<BudgetvsActualReportData> GetBudgetvsActualDetails(BudgetvsActualParams Details)
    {
        try
        {
            List<BudgetvsActualReportData> CData = new List<BudgetvsActualReportData>();
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@TWRLIST", SqlDbType.Structured);

            if (Details.twrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Details.twrlst);
            }

            param[1] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
            if (Details.DateParam == null)
            {
                param[1].Value = DateTime.Now.Year.ToString();
            }
            else
            {
                param[1].Value = Details.DateParam; ;
            }
            //param[2] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
            //param[2].Value = Details.ToDate.ToString();
            param[2] = new SqlParameter("@STAT", SqlDbType.Int);
            param[2].Value = Details.STAT;
            param[3] = new SqlParameter("@DATE", SqlDbType.NVarChar);
            param[3].Value = Details.Request_Type;
            param[4] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[4].Value = Details.companyid;


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BUDGETVSACTUAL_DETAILS", param))
            {
                while (reader.Read())
                    
                {
                    Custm = new BudgetvsActualReportData();
                    Custm.COUNTRY = Convert.ToString(reader["COUNTRY"]);
                    Custm.CITY = Convert.ToString(reader["CITY"]);
                    Custm.LOCATION = Convert.ToString(reader["LOCATION"]);
                    Custm.TOWER = Convert.ToString(reader["TOWER"]);
                    Custm.EM_THRD_TYPE = Convert.ToString(reader["EM_THRD_TYPE"]);
                    //Custm.QUANTITY = Convert.ToString(reader["QUANTITY"]);
                    Custm.EM_THRD_QUNTY = Convert.ToString(reader["EM_THRD_QUNTY"]) + " " + Convert.ToString(reader["EM_UM_MEASUR"]);
                    Custm.EM_DUD_QUNT = Convert.ToString(reader["EM_DUD_QUNT"]) + " " + Convert.ToString(reader["EM_UM_MEASUR"]);
                    Custm.outcome = Convert.ToString(reader["outcome"]) + " " + Convert.ToString(reader["EM_UM_MEASUR"]);
                    Custm.CAL_LOG_DATE = Convert.ToString(reader["CAL_LOG_DATE"]);
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}