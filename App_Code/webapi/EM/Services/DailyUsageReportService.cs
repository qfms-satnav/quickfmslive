﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class DailyUsageReportService
{
    SubSonic.StoredProcedure sp;
    List<DailyUsageReportData> Cust;
    DailyUsageReportData Custm;
    DataSet ds;

    public object GetDailyUsageObject(DailyUsageReportParams Det)
    {
        try
        {
            Cust = GetDailyUsageReportDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<DailyUsageReportData> GetDailyUsageReportDetails(DailyUsageReportParams Details)
    {
        try
        {
            List<DailyUsageReportData> CData = new List<DailyUsageReportData>();
            SqlParameter[] param = new SqlParameter[7];

            param[0] = new SqlParameter("@TWRLIST", SqlDbType.Structured);

            if (Details.twrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Details.twrlst);
            }

            param[1] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
            param[1].Value = Details.FromDate.ToString();
            param[2] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
            param[2].Value = Details.ToDate.ToString();
            param[3] = new SqlParameter("@STAT", SqlDbType.Int);
            param[3].Value = Details.STAT;

            param[4] = new SqlParameter("@DATEPARAM", SqlDbType.NVarChar);
            if (Details.DateParam == null) {
                param[4].Value = DateTime.Now.Year.ToString();
            }
            else {
                param[4].Value = Details.DateParam; ;
            }
            param[5] = new SqlParameter("@QuickSelectedParam", SqlDbType.NVarChar);
            param[5].Value = Details.QuickSelectedParam;
            param[6] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[6].Value = Details.companyid;

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "EM_GET_DAILY_USAGE_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new DailyUsageReportData();
                    Custm.COUNTRY = Convert.ToString(reader["COUNTRY"]);
                    Custm.CITY = Convert.ToString(reader["CITY"]);
                    Custm.LOCATION = Convert.ToString(reader["LOCATION"]);
                    Custm.TOWER = Convert.ToString(reader["TOWER"]);
                    Custm.RESOURCE = Convert.ToString(reader["RESOURCE"]);
                    Custm.QUANTITY = Convert.ToString(reader["QUANTITY"]);
                    Custm.SERIAL_NO = Convert.ToString(reader["SERIAL_NO"]);
                    Custm.CAL_LOG_DATE = Convert.ToString(reader["CAL_LOG_DATE"]);
                    Custm.TOTAL = Convert.ToString(reader["total"]) + " " + Convert.ToString(reader["EM_UM_MEASUR"]); ;
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}