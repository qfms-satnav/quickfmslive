﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for DailyUsageModel
/// </summary>
public class DailyUsageModel
{

    public List<Countrylst> cnylst { get; set; }
    public List<Towerlst> twrlst  { get; set; }
    public DailyUsage DailyUsge { get; set; }
    public List<DailyUsage> DailyUsgelist { get; set; }
    public List<DailyUsageDetails> DailyUsgeDetList { get; set; }
    public DailyUsageDetails DailyUsageDetails { get; set; }
    public DateTime date { get; set; }
    public int companyid { get; set; }
}

public class DailyUsage
{

    public string EM_DUD_TYPE { get; set; }
    public string EM_UM_MEASUR { get; set; }
    public string EM_UM_CNY_ID { get; set; }
    public string EM_DUD_QUNTY { get; set; }
    public string EM_DUD_SERNO { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }

}
public class DailyUsageDetails
{

    public string EM_DUD_TYPE { get; set; }
    public string EM_DUD_QUNTY { get; set; }
    public string EM_DUD_SERNO { get; set; }

}

