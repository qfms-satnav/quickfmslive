﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Reporting.WebForms;



public class BusinessCardUserWiseController : ApiController
{
    BusinessCardUserWiseService BCService = new BusinessCardUserWiseService();

    [HttpPost]
    public object GetUserWiseDetails([FromBody] UserWiseparams obj)
    {
        var response = BCService.GetUserWiseDetails(obj);
        //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetUserWiseReport([FromBody] UserWiseparams obj)
    {
        ReportGenerator<BusinessCardUserWiseModel> reportgen = new ReportGenerator<BusinessCardUserWiseModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/BusinessCard/BusinessCardUserWise.rdlc"),
            DataSetName = "UserWiseReq",
            ReportType = "User Wise Request Report"
        };
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/UserWiseRequest." + obj.Type);
        SubSonic.StoredProcedure sp;
        List<BusinessCardUserWiseModel> VMlist;
        BusinessCardUserWiseModel rptVMobj;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_USER_WISE_REQUEST_REPORT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", obj.FromDate.ToString(), DbType.String);
        sp.Command.AddParameter("@TODATE", obj.ToDate.ToString(), DbType.String);
        sp.Command.AddParameter("@CompanyId", obj.CompanyId, DbType.Int32);
        sp.Command.AddParameter("@SEARCHVAL", obj.SearchValue, DbType.String);
        sp.Command.AddParameter("@PAGENUM", obj.PageNumber, DbType.String);
        sp.Command.AddParameter("@PAGESIZE", obj.PageSize, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<BusinessCardUserWiseModel>();
            while (sdr.Read())
            {
                rptVMobj = new BusinessCardUserWiseModel();
                rptVMobj.DESIGNATION = sdr["DESIGNATION"].ToString();
                rptVMobj.EMPID = sdr["EMPID"].ToString();
                rptVMobj.ADDRESS = sdr["ADDRESS"].ToString();               
                rptVMobj.CARDS = Convert.ToInt32(sdr["CARDS"]);
                rptVMobj.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                rptVMobj.LOCATION = sdr["LOCATION"].ToString();
                rptVMobj.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptVMobj.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptVMobj.REMARKS = sdr["REMARKS"].ToString();
                rptVMobj.STATUS = sdr["STATUS"].ToString();
                rptVMobj.BC_FAX = sdr["BC_FAX"].ToString();
                rptVMobj.BC_PHONE_NO = sdr["BC_PHONE_NO"].ToString();
                rptVMobj.BC_LAND_NO = sdr["BC_LAND_NO"].ToString();
                rptVMobj.BC_EMAIL = sdr["BC_EMAIL"].ToString();
                rptVMobj.BC_CREATED_BY = sdr["BC_CREATED_BY"].ToString();
                rptVMobj.BC_CREATED_DT = sdr["BC_CREATED_DT"].ToString();
                rptVMobj.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                rptVMobj.CLOSURE_DATE = Convert.ToString(sdr["CLOSURE_DATE"]);

                if (sdr["CLOSURE_DATE"].ToString() == "01/01/1900 00:00:00")
                {

                    rptVMobj.CLOSURE_DATE = "Not Closed";

                }
                else
                {
                    rptVMobj.CLOSURE_DATE = Convert.ToString(sdr["CLOSURE_DATE"]);
                }

                //Custm.Lcm_zone = Convert.ToString(reader["Lcm_zone"]);
                rptVMobj.TAT = Convert.ToString(sdr["TAT"]);

                VMlist.Add(rptVMobj);

                //VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }
        await reportgen.GenerateReport(VMlist, filePath, obj.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "UserWiseRequest." + obj.Type;
        return result;
    }
}
