﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class BusinessCardDeptCmprController : ApiController
{
    BusinessCardDeptCmprService BCService = new BusinessCardDeptCmprService();

    [HttpPost]
    public object GetUserWiseDetails([FromBody] BusinessCardDeptparams obj)
    {
        var response = BCService.GetDepWiseCount(obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetDeptCountReport([FromBody] BusinessCardDeptparams obj)
    {
        ReportGenerator<BusinessCardData> reportgen = new ReportGenerator<BusinessCardData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/BusinessCard/BusinessCardDeptWise.rdlc"),
            DataSetName = "BusinessCardDeptWise",
            ReportType = "Department Wise Request Comparision"
        };
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/DepReqCount." + obj.Type);
        SubSonic.StoredProcedure sp;
        List<BusinessCardData> VMlist;
        BusinessCardData rptVMobj;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_DEPARTMENT_WISE_REQ_COUNT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@YEAR", obj.Year.ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", obj.Company, DbType.Int32);
        sp.Command.AddParameter("@SEARCHVAL", obj.SearchValue, DbType.String);
        sp.Command.AddParameter("@PAGENUM", obj.PageNumber, DbType.String);
        sp.Command.AddParameter("@PAGESIZE", obj.PageSize, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<BusinessCardData>();
            while (sdr.Read())
            {
                rptVMobj = new BusinessCardData();
                rptVMobj.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                rptVMobj.LOCATION = sdr["LOCATION"].ToString();
                rptVMobj.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptVMobj.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptVMobj.January = sdr["January"].ToString();
                rptVMobj.Febrary = sdr["Febrary"].ToString();
                rptVMobj.March = sdr["March"].ToString();
                rptVMobj.April = sdr["April"].ToString();
                rptVMobj.May = sdr["May"].ToString();
                rptVMobj.June = sdr["June"].ToString();
                rptVMobj.July = sdr["July"].ToString();
                rptVMobj.August = sdr["August"].ToString();
                rptVMobj.September = sdr["September"].ToString();
                rptVMobj.October = sdr["October"].ToString();
                rptVMobj.November = sdr["November"].ToString();
                rptVMobj.December = sdr["December"].ToString();
                rptVMobj.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }
        await reportgen.GenerateReport(VMlist, filePath, obj.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "DepartmentWise." + obj.Type;
        return result;
    }
}
