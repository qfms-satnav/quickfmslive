﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using UtiltiyVM;

public class BusinessCardDeptCmprService
{
    SubSonic.StoredProcedure sp;
    List<BusinessCardData> VMlist;
    System.Data.DataSet ds;
    public object GetDepWiseCount(BusinessCardDeptparams param)
    {
        object objVMlist = GetDeptCmprReport(param);

        if (objVMlist != null)
            return new { Message = MessagesVM.UM_OK, data = objVMlist };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetDeptCmprReport(BusinessCardDeptparams param)
    {
        BusinessCardData rptVMobj;

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_DEPARTMENT_WISE_REQ_COUNT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@YEAR", param.Year.ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", param.Company, DbType.Int32);
        sp.Command.AddParameter("@SEARCHVAL", param.SearchValue, DbType.String);
        sp.Command.AddParameter("@PAGENUM", param.PageNumber, DbType.String);
        sp.Command.AddParameter("@PAGESIZE", param.PageSize, DbType.String);        
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<BusinessCardData>();
            while (sdr.Read())
            {
                rptVMobj = new BusinessCardData();
                rptVMobj.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                rptVMobj.LOCATION = sdr["LOCATION"].ToString();
                rptVMobj.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptVMobj.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptVMobj.January = sdr["January"].ToString();
                rptVMobj.Febrary = sdr["Febrary"].ToString();
                rptVMobj.March = sdr["March"].ToString();
                rptVMobj.April = sdr["April"].ToString();
                rptVMobj.May = sdr["May"].ToString();
                rptVMobj.June = sdr["June"].ToString();
                rptVMobj.July = sdr["July"].ToString();
                rptVMobj.August = sdr["August"].ToString();
                rptVMobj.September = sdr["September"].ToString();
                rptVMobj.October = sdr["October"].ToString();
                rptVMobj.November = sdr["November"].ToString();
                rptVMobj.December = sdr["December"].ToString();
                rptVMobj.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }
        if (VMlist.Count != 0)
            return new { VMlist = VMlist };
        else
            return null;
    }
}