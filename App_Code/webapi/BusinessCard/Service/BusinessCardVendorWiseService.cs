﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class BusinessCardVendorWiseService
{
    SubSonic.StoredProcedure sp;
    List<BusinessCardVendorWiseModel> VMlist;
    DataSet ds;
    public object GetVendorWiseDetails(BusinessCardsParams param)
    {
        object objVMlist = GetUserWiseReport(param);

        if (objVMlist != null)
            return new { Message = MessagesVM.UM_OK, data = objVMlist };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetUserWiseReport(BusinessCardsParams param)
    {
        BusinessCardVendorWiseModel rptVMobj;

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_VENDOR_WISE_REQUEST_REPORT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", param.FromDate.ToString(), DbType.String);
        sp.Command.AddParameter("@TODATE", param.ToDate.ToString(), DbType.String);
        sp.Command.AddParameter("@CompanyId", param.CompanyId, DbType.Int32);
        sp.Command.AddParameter("@SEARCHVAL", param.SearchValue, DbType.String);
        sp.Command.AddParameter("@PAGENUM", param.PageNumber, DbType.String);
        sp.Command.AddParameter("@PAGESIZE", param.PageSize, DbType.String);
        DataSet ds = sp.GetDataSet();
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<BusinessCardVendorWiseModel>();
            while (sdr.Read())
            {
                rptVMobj = new BusinessCardVendorWiseModel();
                rptVMobj.ADDRESS = sdr["ADDRESS"].ToString();
                rptVMobj.REQ = Convert.ToInt32(sdr["REQ"]);
                rptVMobj.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                rptVMobj.LOCATION = sdr["LOCATION"].ToString();
                rptVMobj.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptVMobj.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptVMobj.VENDOR = sdr["VENDOR"].ToString();
                rptVMobj.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }
        if (VMlist.Count != 0)
            return new { VMlist = VMlist };
        else
            return null;
    }
}