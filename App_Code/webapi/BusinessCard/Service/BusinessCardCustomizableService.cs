﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class BusinessCardCustomizableService
{
    SubSonic.StoredProcedure sp;
    List<BusinessCardCustomizableModel> CustData;
    BusinessCardCustomizableModel Custm;
    DataSet ds;
    public object GetBusinessCardobject(CustomParams param)
    {
        try
        {
            CustData = GetCustomData(param);

            if (CustData.Count != 0)

                return new { Message = MessagesVM.UM_NO_REC, data = CustData };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public List<BusinessCardCustomizableModel> GetCustomData(CustomParams Details)
    {
        try
        {
            List<BusinessCardCustomizableModel> CData = new List<BusinessCardCustomizableModel>();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Details.Request_Type;
            param[1] = new SqlParameter("@LOCLST", SqlDbType.Structured);


            if (Details.loclst == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(Details.loclst);
        }


            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            param[3] = new SqlParameter("@STAT", SqlDbType.Int);
            param[3].Value = Details.STAT;

            param[4] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[4].Value = Details.FromDate;

            param[5] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[5].Value = Details.ToDate;

            param[6] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[6].Value = Details.CNP_NAME;
            param[7] = new SqlParameter("@DEPLST", SqlDbType.Structured);




            if (Details.Deplst == null)
            {
                param[7].Value = null;
            }
            else
            {
                param[7].Value = UtilityService.ConvertToDataTable(Details.Deplst);
            }
           
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BC_CUSTOMIZABLE_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new BusinessCardCustomizableModel();
                    Custm.REQ_ID = Convert.ToString(reader["REQ_ID"]);
                    Custm.LOCATION = Convert.ToString(reader["LOCATION"]);
                    Custm.LCM_CODE = Convert.ToString(reader["LCM_CODE"]);
                    Custm.CNP_NAME = Convert.ToString(reader["CNP_NAME"]);
                    Custm.DEP_NAME = Convert.ToString(reader["DEP_NAME"]);
                    Custm.EMP_NAME = Convert.ToString(reader["EMP_NAME"]);
                    Custm.CARDS = Convert.ToInt32(reader["CARDS"]);
                    Custm.STATUS = Convert.ToString(reader["STATUS"]);
                    Custm.DSN_AMT_TITLE = Convert.ToString(reader["DSN_AMT_TITLE"]);
                    Custm.BC_EMAIL = Convert.ToString(reader["BC_EMAIL"]);
                    Custm.BC_PHONE_NO = Convert.ToString(reader["BC_PHONE_NO"]);
                    Custm.BC_FAX = Convert.ToString(reader["BC_FAX"]);
                    Custm.BC_LAND_NO = Convert.ToString(reader["BC_LAND_NO"]);
                    Custm.BC_ADDR = Convert.ToString(reader["BC_ADDR"]);
                    Custm.BC_REM = Convert.ToString(reader["BC_REM"]);
                    Custm.CREATED_DATE = Convert.ToString(reader["CREATED_DATE"]);                   
                    Custm.CLOSURE_DATE= Convert.ToString(reader["CLOSURE_DATE"]);

                    if (reader["CLOSURE_DATE"].ToString() == "01/01/1900 00:00:00")
                    {

                        Custm.CLOSURE_DATE = "Not Closed";

                    }
                    else
                    {
                        Custm.CLOSURE_DATE = Convert.ToString(reader["CLOSURE_DATE"]);
                    }

                    //Custm.Lcm_zone = Convert.ToString(reader["Lcm_zone"]);
                    Custm.TAT = Convert.ToString(reader["TAT"]);
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch(Exception ex)
        {
            throw;
        }
    }


    public List<BusinessCardCustomizableModel> LoadCustomizableDetails(CustomParams Details)
    {
        try
        {
            List<BusinessCardCustomizableModel> CData = new List<BusinessCardCustomizableModel>();
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Details.Request_Type;
          
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];

            param[2] = new SqlParameter("@STAT", SqlDbType.Int);
            param[2].Value = Details.STAT;

            param[3] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[3].Value = Details.FromDate;

            param[4] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[4].Value = Details.ToDate;

            param[5] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[5].Value = Details.CNP_NAME;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BC_LOAD_CUSTOMIZABLE_DATA", param))
            {
                while (reader.Read())
                {
                    Custm = new BusinessCardCustomizableModel();
                    Custm.REQ_ID = Convert.ToString(reader["REQ_ID"]);
                    Custm.LOCATION = Convert.ToString(reader["LOCATION"]);
                    Custm.LCM_CODE = Convert.ToString(reader["LCM_CODE"]);
                    Custm.CNP_NAME = Convert.ToString(reader["CNP_NAME"]);
                    Custm.DEP_NAME = Convert.ToString(reader["DEP_NAME"]);
                    Custm.EMP_NAME = Convert.ToString(reader["EMP_NAME"]);
                    Custm.CARDS = Convert.ToInt32(reader["CARDS"]);
                    Custm.STATUS = Convert.ToString(reader["STATUS"]);
                    Custm.DSN_AMT_TITLE = Convert.ToString(reader["DSN_AMT_TITLE"]);
                    Custm.BC_EMAIL = Convert.ToString(reader["BC_EMAIL"]);
                    Custm.BC_PHONE_NO = Convert.ToString(reader["BC_PHONE_NO"]);
                    Custm.BC_FAX = Convert.ToString(reader["BC_FAX"]);
                    Custm.BC_LAND_NO = Convert.ToString(reader["BC_LAND_NO"]);
                    Custm.BC_ADDR = Convert.ToString(reader["BC_ADDR"]);
                    Custm.BC_REM = Convert.ToString(reader["BC_REM"]);
                    Custm.CREATED_DATE = Convert.ToString(reader["CREATED_DATE"]);
                    Custm.CLOSURE_DATE = Convert.ToString(reader["CLOSURE_DATE"]);
                    //if ((reader["CLOSURE_DATE"]).ToString() == "")
                    //{
                    //    Custm.CLOSURE_DATE = "Not Closed";
                    //}
                    //else
                    //{
                    //    Custm.CLOSURE_DATE = Convert.ToString(reader["CLOSURE_DATE"]);
                    //}
                    //Custm.BC_REM = Convert.ToString(reader["BC_REM"]);
                    Custm.Lcm_zone = Convert.ToString(reader["Lcm_zone"]);

                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}