﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class BusinessCardUserWiseService
{
    SubSonic.StoredProcedure sp;
    List<BusinessCardUserWiseModel> VMlist;
    DataSet ds;
    public object GetUserWiseDetails(UserWiseparams param)
    {
        object objVMlist = GetUserWiseReport(param);

        if (objVMlist != null)
            return new { Message = MessagesVM.UM_OK, data = objVMlist };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetUserWiseReport(UserWiseparams param)
    {
        BusinessCardUserWiseModel rptVMobj;

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_USER_WISE_REQUEST_REPORT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", param.FromDate.ToString(), DbType.String);
        sp.Command.AddParameter("@TODATE", param.ToDate.ToString(), DbType.String);
        sp.Command.AddParameter("@CompanyId", param.CompanyId, DbType.Int32);
        sp.Command.AddParameter("@SEARCHVAL", param.SearchValue, DbType.String);
        sp.Command.AddParameter("@PAGENUM", param.PageNumber, DbType.String);
        sp.Command.AddParameter("@PAGESIZE", param.PageSize, DbType.String);      
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<BusinessCardUserWiseModel>();
            while (sdr.Read())
            {
                rptVMobj = new BusinessCardUserWiseModel();
                rptVMobj.DESIGNATION = sdr["DESIGNATION"].ToString();
                rptVMobj.EMPID = sdr["EMPID"].ToString();
                rptVMobj.ADDRESS = sdr["ADDRESS"].ToString();              
                rptVMobj.CARDS = Convert.ToInt32(sdr["CARDS"]);
                rptVMobj.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                rptVMobj.LOCATION = sdr["LOCATION"].ToString();
                rptVMobj.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptVMobj.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptVMobj.REMARKS = sdr["REMARKS"].ToString();
                rptVMobj.STATUS = sdr["STATUS"].ToString();
                rptVMobj.BC_FAX = sdr["BC_FAX"].ToString();
                rptVMobj.BC_PHONE_NO = sdr["BC_PHONE_NO"].ToString();
                rptVMobj.BC_LAND_NO = sdr["BC_LAND_NO"].ToString();
                rptVMobj.BC_EMAIL = sdr["BC_EMAIL"].ToString();
                rptVMobj.BC_CREATED_BY = sdr["BC_CREATED_BY"].ToString();
                rptVMobj.BC_CREATED_DT = sdr["BC_CREATED_DT"].ToString();
                rptVMobj.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                rptVMobj.CLOSURE_DATE = Convert.ToString(sdr["CLOSURE_DATE"]);

                if (sdr["CLOSURE_DATE"].ToString() == "01/01/1900 00:00:00")
                {

                    rptVMobj.CLOSURE_DATE = "Not Closed";

                }
                else
                {
                    rptVMobj.CLOSURE_DATE = Convert.ToString(sdr["CLOSURE_DATE"]);
                }

                //Custm.Lcm_zone = Convert.ToString(reader["Lcm_zone"]);
                rptVMobj.TAT = Convert.ToString(sdr["TAT"]);

                VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }
        if (VMlist.Count != 0)
            return new { VMlist = VMlist };
        else
            return null;
    }
}