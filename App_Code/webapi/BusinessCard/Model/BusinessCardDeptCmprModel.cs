﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BusinessCardDeptparams
{
    public string Year { get; set; }
    public string Company { get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}
public class BusinessCardData
{
    public string LOCATION { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public string DEPARTMENT { get; set; }
    public string January { get; set; }
    public string Febrary { get; set; }
    public string March { get; set; }
    public string April { get; set; }
    public string May { get; set; }
    public string June { get; set; }
    public string July { get; set; }
    public string August { get; set; }
    public string September { get; set; }
    public string October { get; set; }
    public string November { get; set; }
    public string December { get; set; }
    public string OVERALL_COUNT { get; set; }
}