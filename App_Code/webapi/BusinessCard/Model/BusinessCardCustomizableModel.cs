﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for BusinessCardCustomizableModel
/// </summary>
public class BusinessCardCustomizableModel
{
    public string REQ_ID { get; set; }
    public string LOCATION { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public string EMP_NAME { get; set; }
    public string DEP_NAME { get; set; }
    public int CARDS { get; set; }
    public string STATUS { get; set; }
    public string DSN_AMT_TITLE { get; set; }
    public string BC_EMAIL { get; set; }
    public string BC_PHONE_NO { get; set; }
    public string BC_FAX { get; set; }
    public string BC_LAND_NO { get; set; }
    public string BC_ADDR { get; set; }
    public string CREATED_DATE { get; set; }
    public string CLOSURE_DATE { get; set; }
    public string BC_REM { get; set; }
    public string Lcm_zone { get; set; }
    public string TAT { get; set; }
}
public class CustomParams
{
    public List<Locationlst> loclst { get; set; }
    public List<Department> Deplst { get; set; }
    public string Request_Type { get; set; }
    public string STAT { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public int CNP_NAME { get; set; }
    public string Type { get; set; }
}