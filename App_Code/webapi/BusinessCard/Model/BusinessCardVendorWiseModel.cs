﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BusinessCardVendorWiseModel
{
    public string VENDOR { get; set; }
    public int REQ { get; set; }
    public string DEPARTMENT { get; set; }
    public string LOCATION { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public string ADDRESS { get; set; }
    public string OVERALL_COUNT { get; set; }
}
public class BusinessCardsParams
{
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public int CompanyId { get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}