﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for BranchScoreCardReportService
/// </summary>
public class BranchScoreCardReportService
{
    SubSonic.StoredProcedure sp;
    public object GetRegions()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_REGIONS]");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public object GetSpocs()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_SPOCS]");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetLocationsScoreReport()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_LOCATION_SCORE_REPORTS]");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }


    public object GetCitiesByZone(List<ZNlsts> cost, int id)
    {
        List<Citylsts> loclst = new List<Citylsts>();
        Citylsts loc;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@ZONELST", SqlDbType.Structured);
        if (cost == null)
        {
            param[1].Value = null;
        }
        else
        {
            param[1].Value = ConvertToDataTable(cost);
        }
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CITY_BY_ZONE", param))
        {
            while (sdr.Read())
            {
                loc = new Citylsts();
                loc.CTY_CODE = sdr["LCM_ZONE"].ToString();
                loc.CTY_NAME = sdr["CTY_NAME"].ToString();
                loc.LCM_ZONE = sdr["CTY_CODE"].ToString();
                loc.LCM_CTY_ID = sdr["LCM_CTY_ID"].ToString();
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetScoreCardtDetails(BranchScoreCardReportModel det)
    {
        try
        {
            DataSet ds = new DataSet();
            List<GridColumns> gridcols = new List<GridColumns>();
            GridColumns gridcolscls;
            SqlParameter[] param = new SqlParameter[6];
            String[] str = det.LCM_ZONE.Split(Convert.ToChar(","));
            param[0] = new SqlParameter("@LCM_ZONE", SqlDbType.NVarChar);
            if (Convert.ToInt32(str.Length) <= 0)
                param[0].Value = "";
            else
                param[0].Value = det.LCM_ZONE;

            String[] str1 = det.LCM_CODE.Split(Convert.ToChar(","));
            param[1] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
            if (Convert.ToInt32(str1.Length) <= 0)
                param[1].Value = "";
            else
                param[1].Value = det.CTY_CODE;

            String[] str2 = det.LCM_CODE.Split(Convert.ToChar(","));
            param[2] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            if (Convert.ToInt32(str2.Length) <= 0)
                param[2].Value = "";
            else
                param[2].Value = det.LCM_CODE;

            param[3] = new SqlParameter("@AUR_KNOWN_AS", SqlDbType.NVarChar);
            param[3].Value = det.AUR_KNOWN_AS;
            param[4] = new SqlParameter("@FromDate", SqlDbType.NVarChar);
            param[4].Value = det.FromDate;
            param[5] = new SqlParameter("@ToDate", SqlDbType.NVarChar);
            param[5].Value = det.ToDate;
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_SCORE_CARD_DATA", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new GridColumns();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, Message = MessagesVM.AF_OK };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.CNP_ERROR };
        };
    }
    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }
}
