﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
/// <summary>
/// Summary description for BranchIncentiveLocationMasterService
/// </summary>
public class BranchIncentiveLocationMasterService

{
    SubSonic.StoredProcedure sp;
    public object GetCategory()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_INCENTIVE_CATEGORY]");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public object SaveData(BranchIncentiveLocationMasterModel Save)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[SAVE_BRANCH_INCENTIVE_LOCATION_CATEGORY_MASTER]");
            sp.Command.AddParameter("@LCM_CODE", Save.LCM_CODE, DbType.String);
            sp.Command.AddParameter("@BICM_CODE", Save.BICM_CODE, DbType.String);
            sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Execute();
            return new { Message = "Data Inserted Successfully", data = Save };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public IEnumerable<BranchIncentiveLocationMasterModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_BRANCH_INCENTIVE_LOCATION_CATEGORY_MASTER]").GetReader())
        {
            try
            {
                List<BranchIncentiveLocationMasterModel> BicModel = new List<BranchIncentiveLocationMasterModel>();
                while (reader.Read())
                {
                    BicModel.Add(new BranchIncentiveLocationMasterModel()
                    {
                        
                         BILCM_LOC_NAME = reader.GetValue(0).ToString(),
                        BILCM_LOC_CODE = reader.GetValue(1).ToString(),
                        BILCM_CAT_CODE = reader.GetValue(2).ToString(),
                        BILCM_STA_ID = reader.GetValue(3).ToString(),
                        BILCM_ID = reader.GetValue(4).ToString()

                    });
                }
                reader.Close();
                return BicModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }


    public object ModifyCategoryDetails(BranchIncentiveLocationMasterModel BicModel)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@BILCM_ID", SqlDbType.NVarChar);
            param[0].Value = BicModel.BILCM_ID;
            param[1] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[1].Value = BicModel.LCM_CODE;
            param[2] = new SqlParameter("@BILCM_LOC_CODE", SqlDbType.NVarChar);
            param[2].Value = BicModel.BILCM_LOC_CODE;
            param[3] = new SqlParameter("@BILCM_LOC_NAME", SqlDbType.NVarChar);
            param[3].Value = BicModel.BILCM_LOC_NAME;
            param[4] = new SqlParameter("@BICM_CODE", SqlDbType.NVarChar);
            param[4].Value = BicModel.BICM_CODE;
            param[5] = new SqlParameter("@BILCM_STA_ID", SqlDbType.NVarChar);
            param[5].Value = BicModel.BILCM_STA_ID;
            param[6] = new SqlParameter("@BILCM_UPDATED_BY", SqlDbType.NVarChar);
            param[6].Value = HttpContext.Current.Session["UID"].ToString();
            param[7] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[MODIFY_BRANCH_INCENTIVE_LOCATION_CATEGORY_MASTER]", param);
            return new { Message = MessagesVM.Facility_Modify, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

}