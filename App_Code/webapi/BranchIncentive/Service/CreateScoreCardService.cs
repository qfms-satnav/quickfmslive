﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
using System.ComponentModel;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Net.Mail;
using System.Net;
/// <summary>
/// Summary description for CreateScoreCardService
/// </summary>
public class CreateScoreCardService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object getInspectors()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_INSPECTORS");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getInspectorsZonalCentral(int TPM_TP_ID, string TPM_LCM_CODE, int TPMD_APPR_LEVELS)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BIM_GET_CREATEVALIDATERAPPROVE");
        sp.Command.Parameters.Add("@TPM_TP_ID", TPM_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@TPM_LCM_CODE", TPM_LCM_CODE, DbType.String);
        sp.Command.Parameters.Add("@TPMD_APPR_LEVELS", TPMD_APPR_LEVELS, DbType.Int32);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getCompany()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_COMPANIES");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object getLocations()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_LOCATIONS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetLocationsIn(int id)
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_LOCATION_BY_CITY_IN]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object GetInspectorsIn(int id)
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BCL_GET_INSPECTORSZONALCENTRAL_IN]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetScorePer()
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BCL_GET_SCORECARD_PERCENTAGE]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");
            sp.Command.AddParameter("@user_id", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetGriddata(int BCL_ID)
    {
        //Vinod
        DataSet dataSet = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[BCL_GET_SCORECARD_CATEGORIES1]");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@BCLID", BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetail1> details = UtilityService.ConvertDataTableToList<ChecklistDetail1>(ds.Tables[1]);
        List<ChecklistData1> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistData1>(ds.Tables[0]);
        List<ChecklistScore> ChecklistScore = UtilityService.ConvertDataTableToList<ChecklistScore>(ds.Tables[2]);

        foreach (var chkitem in ChecklistScore)
        {
            foreach (var item in checklistDatas)
            {
                if (chkitem.BCL_TP_ID == item.BCL_TP_ID)
                {
                    foreach (var items in details)
                    {
                        if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                        {
                            item.checklistDetails.Add(items);
                        }
                    }
                    chkitem.ChecklistScore1.Add(item);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ChecklistScore };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object getSavedList(int BCLStatus, int Status)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_SCORE_SAVED_LIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@BCLStatus", BCLStatus, DbType.Int32);
        sp.Command.Parameters.Add("@Status", Status, DbType.Int32);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object InsertCheckListV2(selecteddata1 dataobj)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[13];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
            param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
            param[1].Value = dataobj.InspectdBy;
            param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            param[2].Value = dataobj.date;
            param[3] = new SqlParameter("@SelCompany", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
            param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["UID"];
            param[6] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[6].Value = HttpContext.Current.Session["COMPANYID"];
            param[7] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[7].Value = dataobj.Flag;
            param[8] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
            param[8].Value = dataobj.OVERALL_CMTS;
            param[9] = new SqlParameter("@BCL_TYPE_ID", SqlDbType.Int);
            param[9].Value = dataobj.BCL_TYPE_ID;
            param[10] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[10].Value = dataobj.BCL_ID;
            param[11] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
            param[11].Value = "WEB";
            param[12] = new SqlParameter("@FPERCENT", SqlDbType.Float);
            param[12].Value = dataobj.F_Percentage;
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[DYNAMIC_SCORE_CHECKLIST]", param);
            int sem_id = (int)dt.Rows[0][0];
            string msg;
            if (sem_id == 0)
            {
                msg = "Saved as Draft";
            }
            else
            {
                //GenerateCheck_Report((int)dt.Rows[0][1]);
                msg = "Scorecard Saved Successfully";
            }

            return new { data = msg, Message = msg };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    ///////////**********Validaion Methods************////////

    public object SaveValidationData(selecteddata1 dataobj)
    {
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
        param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
        param[1].Value = dataobj.InspectdBy;
        param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
        param[2].Value = dataobj.date;
        param[3] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[3].Value = dataobj.Flag;
        param[4] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[4].Value = dataobj.BCL_ID;
        param[5] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
        param[5].Value = "WEB";
        param[6] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
        param[6].Value = dataobj.OVERALL_CMTS;
        param[7] = new SqlParameter("@FPERCENT", SqlDbType.Float);
        param[7].Value = dataobj.F_Percentage;
        int sem_id = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[ScoreCard_Save_ValidationData1]", param);
        if (sem_id == 1 && dataobj.Flag == 5)
        {
            return new { Message = MessagesVM.UM_OK, data = "Scorecard Rejected Successfully" };
        }
        else if (sem_id == 1)
        {
            return new { Message = MessagesVM.UM_OK, data = "Scorecard Validated Successfully" };
        }
        else
        {
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

    }

    ///////////**********Approval Submit************////////
    public object SaveApprovalData(selecteddata1 dataobj)
    {
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
        param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
        param[1].Value = dataobj.InspectdBy;
        param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
        param[2].Value = dataobj.date;
        param[3] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[3].Value = dataobj.Flag;
        param[4] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[4].Value = dataobj.BCL_ID;
        param[5] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
        param[5].Value = "WEB";
        param[6] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
        param[6].Value = dataobj.OVERALL_CMTS;
        param[7] = new SqlParameter("@FPERCENT", SqlDbType.Float);
        param[7].Value = dataobj.F_Percentage;
        int sem_id = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[ScoreCard_Save_ApprovalData]", param);
        if (sem_id == 1 && dataobj.Flag == 6)
        {
            return new { Message = MessagesVM.UM_OK, data = "Scorecard Rejected Successfully" };
        }
        else if (sem_id == 1)
        {
            return new { Message = MessagesVM.UM_OK, data = "Scorecard Approved Successfully" };
        }
        else
        {
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GetLocationsScore(int id)
    {

        List<LocationLstDisabled> Loc_lst = new List<LocationLstDisabled>();
        LocationLstDisabled Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_SCORE");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new LocationLstDisabled();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                Loc.ticked = false;
                Loc.isDisabled = Convert.ToBoolean(sdr["isDisabled"]);
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }





    ///////////**********Approval Submit************////////
    public object BulkApprovalData(int[] selectedData)
    {
        List<object> results = new List<object>();

        foreach (int bclId in selectedData)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[0].Value = bclId;
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            int sem_id = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[ScoreCard_Save_BuklApprovalData]", param);

            if (sem_id == 1)
            {
                results.Add(new { Message = MessagesVM.UM_OK, data = "Scorecard Approved Successfully" });
            }

            else
            {
                results.Add(new { Message = MessagesVM.UM_NO_REC, data = (object)null });
            }
        }

        return results;
    }

}