﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
/// <summary>
/// Summary description for BranchIncentiveCategoryService
/// </summary>
public class BranchIncentiveCategoryService
{
    SubSonic.StoredProcedure sp;
    public int SaveCategoryData(BranchIncentiveCategoryModel BicModel)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[SAVE_BRANCH_INCENTIVE_CATEGORY_MASTER]");
            sp.Command.AddParameter("@BICM_CODE", BicModel.BICM_CODE, DbType.String);
            sp.Command.AddParameter("@BICM_NAME", BicModel.BICM_NAME, DbType.String);
            sp.Command.AddParameter("@BICM_RANGE", BicModel.BICM_RANGE, DbType.String);
            sp.Command.AddParameter("@BICM_STA_ID", BicModel.BICM_STA_ID, DbType.String);
            sp.Command.AddParameter("@BICM_CREATED_BY", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }

    public object ModifyCategoryDetails(BranchIncentiveCategoryModel BicModel)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@BICM_CODE", SqlDbType.NVarChar);
        param[0].Value = BicModel.BICM_CODE;
        param[1] = new SqlParameter("@BICM_NAME", SqlDbType.NVarChar);
        param[1].Value = BicModel.BICM_NAME;
        param[2] = new SqlParameter("@BICM_STA_ID", SqlDbType.NVarChar);
        param[2].Value = BicModel.BICM_STA_ID;
        param[3] = new SqlParameter("@BICM_RANGE", SqlDbType.NVarChar);
        param[3].Value = BicModel.BICM_RANGE;
        param[4] = new SqlParameter("@BICM_UPDATED_BY", SqlDbType.NVarChar);
        param[4].Value = HttpContext.Current.Session["UID"].ToString();
        param[5] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[5].Value = HttpContext.Current.Session["COMPANYID"];
        SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[MODIFY_BRANCH_INCENTIVE_CATEGORY_MASTER]", param);
        return new { Message = MessagesVM.Facility_Modify, data = (object)null };
    }

    public IEnumerable<BranchIncentiveCategoryModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_BRANCH_INCENTIVE_CATEGORY_MASTER]").GetReader())
        {
            try
            {
                List<BranchIncentiveCategoryModel> BicModel = new List<BranchIncentiveCategoryModel>();
                while (reader.Read())
                {
                    BicModel.Add(new BranchIncentiveCategoryModel()
                    {
                        BICM_CODE = reader.GetValue(0).ToString(),
                        BICM_NAME = reader.GetValue(1).ToString(),
                        BICM_STA_ID = reader.GetValue(2).ToString(),
                        BICM_RANGE = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return BicModel;
            }
            catch
            {
                throw;
            }
        }

    }
}