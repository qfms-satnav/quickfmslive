﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for BranchIncentiveTypeMasterService
/// </summary>
public class BranchIncentiveTypeMasterService
{
    SubSonic.StoredProcedure sp;
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[SP_GetBIM_CATEGORY_CODE]");
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetGridData(BranchIncentiveTypeMasterModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[SP_GetBIM_INCENTIVE_MASTER]");
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(BranchIncentiveTypeMasterModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_ADD_UPDATE_INCENTIVE_MASTER");
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.Int32);
            sp.Command.AddParameter("@BIM_CODE", CustRpt.BIM_CODE, DbType.String);
            sp.Command.AddParameter("@BIM_NAME", CustRpt.BIM_NAME, DbType.String);
            sp.Command.AddParameter("@BIM_FROM_SCORE", CustRpt.BIM_FROM_SCORE, DbType.String);
            sp.Command.AddParameter("@BIM_UPTO_SCORE", CustRpt.BIM_UPTO_SCORE, DbType.String);
            sp.Command.AddParameter("@BIM_INCENTIVE_AMOUNT", CustRpt.BIM_INCENTIVE_AMOUNT, DbType.String);
            sp.Command.AddParameter("@BIM_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BIM_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    //public object updateData(BranchIncentiveTypeMasterModel CustRpt)
    //{
    //    try
    //    {
    //        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[SP_ADD_UPDATE_INCENTIVE_MASTER_gg");
    //        sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.Int32);
    //        sp.Command.AddParameter("@BIM_CODE", CustRpt.BIM_CODE, DbType.String);
    //        sp.Command.AddParameter("@BIM_NAME", CustRpt.BIM_NAME, DbType.String);
    //        sp.Command.AddParameter("@BIM_FROM_SCORE", CustRpt.BIM_FROM_SCORE, DbType.String);
    //        sp.Command.AddParameter("@BIM_UPTO_SCORE", CustRpt.BIM_UPTO_SCORE, DbType.String);
    //        sp.Command.AddParameter("@BIM_INCENTIVE_AMOUNT", CustRpt.BIM_INCENTIVE_AMOUNT, DbType.String);
    //        sp.Command.AddParameter("@BIM_MC_STA_ID", CustRpt.BIM_MC_STA_ID, DbType.String);
    //        sp.Command.AddParameter("@BIM_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
    //        sp.Command.AddParameter("@BIM_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
    //        int flag = (int)sp.ExecuteScalar();
    //        return flag;
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Message = ex.Message, data = (object)null };
    //    }

    //}

  public object updateData(BranchIncentiveTypeMasterModel CustRpt)
    {
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@BCL_TP_MC_ID", SqlDbType.Int);
        param[0].Value = CustRpt.BCL_TP_MC_ID;
        param[1] = new SqlParameter("@BIM_CODE", SqlDbType.NVarChar);
        param[1].Value = CustRpt.BIM_CODE;
        param[2] = new SqlParameter("@BIM_NAME", SqlDbType.NVarChar);
        param[2].Value = CustRpt.BIM_NAME;
        param[3] = new SqlParameter("@BIM_FROM_SCORE", SqlDbType.NVarChar);
        param[3].Value = CustRpt.BIM_FROM_SCORE;
        param[4] = new SqlParameter("@BIM_UPTO_SCORE", SqlDbType.NVarChar);
        param[4].Value = CustRpt.BIM_UPTO_SCORE;
        param[5] = new SqlParameter("@BIM_INCENTIVE_AMOUNT", SqlDbType.NVarChar);
        param[5].Value = CustRpt.BIM_INCENTIVE_AMOUNT;
        param[6] = new SqlParameter("@BIM_MC_STA_ID", SqlDbType.NVarChar);
        param[6].Value = CustRpt.BIM_MC_STA_ID;
        param[7] = new SqlParameter("@BIM_CREATED_BY", SqlDbType.NVarChar);
        param[7].Value = HttpContext.Current.Session["UID"].ToString();
        param[8] = new SqlParameter("@BIM_COMPANYID", SqlDbType.Int);
        param[8].Value = HttpContext.Current.Session["COMPANYID"];

        SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SP_ADD_UPDATE_INCENTIVE_MASTER_gg", param);
        return new { Message = MessagesVM.Facility_Modify, data = (object)null };
    }


}