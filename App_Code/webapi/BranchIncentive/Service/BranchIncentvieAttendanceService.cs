﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for BranchIncentvieAttendanceService
/// </summary>
public class BranchIncentvieAttendanceService
{
    SubSonic.StoredProcedure sp;
    internal object ReadExcel_Data(HttpRequest httpFile)
    {
        try
        {
            if (httpFile.Files.Count > 0)
            {
                HttpPostedFile Inputfile = null;
                Inputfile = httpFile.Files[0];
                List<BranchIncentvieAttendanceModel> excelFields = ReadExcelData<BranchIncentvieAttendanceModel>(Inputfile);
                if (excelFields.Count > 0)
                {
                    return SaveAttendanceData(excelFields);
                }
                else
                {
                    return new { Message = MessagesVM.PM_NO_REC, data = (object)null };
                }

            }
            else
            {
                return new { Message = MessagesVM.UAD_UPLNO, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }


    public List<T> ReadExcelData<T>(HttpPostedFile httpPostedFile)
    {
        List<T> list = new List<T>();
        Type typeOfObject = typeof(T);

        using (IXLWorkbook xlWorkbook = new XLWorkbook(httpPostedFile.InputStream))
        {
            IXLWorksheet worksheet = xlWorkbook.Worksheet(1);

            var properties = typeOfObject.GetProperties();

            var columns = worksheet.FirstRow().Cells()
                .Select((v, i) => new { Value = v.Value.ToString(), Index = i + 1 });

            // Compare custom attribute names with column header values
            foreach (var prop in properties)
            {
                var jsonPropAttr = prop.GetCustomAttribute(typeof(JsonPropertyAttribute)) as JsonPropertyAttribute;
                var columnName = jsonPropAttr != null ? jsonPropAttr.PropertyName : prop.Name;
                if (!columns.Any(c => c.Value == columnName))
                {
                    throw new Exception(string.Format("Column '{0}' not found.", columnName));
                }
            }

            // Check if column headers match properties
            if (!properties.Select(p =>
            {
                var jsonPropAttr = p.GetCustomAttribute(typeof(JsonPropertyAttribute)) as JsonPropertyAttribute;
                return jsonPropAttr != null ? jsonPropAttr.PropertyName : p.Name;
            }).SequenceEqual(columns.Select(c => c.Value)))
            {
                throw new Exception("Header mismatch.");
            }

            foreach (IXLRow row in worksheet.RowsUsed().Skip(1)) // Skip header row
            {
                T obj = (T)Activator.CreateInstance(typeOfObject);
                foreach (var prop in properties)
                {
                    var jsonPropAttr = prop.GetCustomAttribute(typeof(JsonPropertyAttribute)) as JsonPropertyAttribute;
                    var columnName = jsonPropAttr != null ? jsonPropAttr.PropertyName : prop.Name;
                    int colIndex = columns.Single(c => c.Value == columnName).Index; // Get the column index for the property
                    var val = row.Cell(colIndex).Value; // Get cell value

                    if (val == null || string.IsNullOrWhiteSpace(val.ToString()))
                    {
                        
                        throw new Exception(String.Format("Empty value found for property '{0}' in row {1}.", prop.Name, row.RowNumber()));

                    }

                    var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType; // Handle nullable types
                    prop.SetValue(obj, Convert.ChangeType(val, type)); // Convert cell value and set property value
                }
                list.Add(obj);
            }

        }
        return list;
    }
    public List<T> ImportExcel<T>(HttpPostedFile httpPostedFile)
    {
        List<T> list = new List<T>();
        Type typeOfObject = typeof(T);
        using (IXLWorkbook workbook = new XLWorkbook(httpPostedFile.InputStream))
        {
            IXLWorksheet worksheet = workbook.Worksheet(1);
            var properties = typeOfObject.GetProperties();
            var columns = worksheet.FirstRow().Cells().Select((v, i) => new { Value = v.Value, Index = i + 1 }); //indexing foreach(IXLRow row in worksheet. RowsUsed (). Skip (1))//Skip first row which is used for column header texts
            if (!properties.Select(p => p.Name).SequenceEqual(columns.Select(c => c.Value.ToString())))
            {
                // Handle the mismatch (throw exception, log, or take appropriate action)
                throw new Exception("Header mismatch:");
            }
            foreach (IXLRow row in worksheet.RowsUsed().Skip(1))
            {
                T obj = (T)Activator.CreateInstance(typeOfObject);
                foreach (var prop in properties)
                {
                    int colIndex = columns.SingleOrDefault(c => c.Value.ToString() == prop.Name.ToString()).Index;
                    var val = row.Cell(colIndex).Value;
                    var type = prop.PropertyType;
                    prop.SetValue(obj, Convert.ChangeType(val, type));
                }
                list.Add(obj);
            }
        }
        return list;
    }


    private object SaveAttendanceData(List<BranchIncentvieAttendanceModel> excelFields)
    {
        try
        {
            // string str1 = "insert into " + HttpContext.Current.Session["TENANT"] + ".BIM_BIC_ATTENDANCE_TEMP (BIM_BIC_LOC_CODE, BIM_BIC_LOC_NAME, BIM_BIC_REGION,BIM_BIC_LOC_STATUS,BIM_BIC_USR_ID,BIM_BIC_USR_NAME,BIM_BIC_EMAIL,BIM_BIC_QUATER_NAME,BIM_BIC_QTR_START_DATE,BIM_BIC_QTR_END_DATE,BIM_BIC_TENURE, BIM_BIC_START_DATE, BIM_BIC_APP_START_DATE, BIM_BIC_END_DATE,BIM_BIC_APPLICABLE_DAYS,BIM_BIC_CREATED_BY,BIM_BIC_CREATED_DT) values ";
            //string str1 = "insert into " + HttpContext.Current.Session["TENANT"] + ".BIM_BIC_ATTENDANCE_TEMP (BIM_BIC_LOC_NAME, BIM_BIC_REGION,BIM_BIC_LOC_STATUS,BIM_BIC_USR_ID,BIM_BIC_USR_NAME,BIM_BIC_EMAIL,BIM_BIC_QUATER_NAME,BIM_BIC_QTR_START_DATE,BIM_BIC_QTR_END_DATE,BIM_BIC_TENURE, BIM_BIC_START_DATE, BIM_BIC_APP_START_DATE, BIM_BIC_END_DATE,BIM_BIC_CREATED_BY,BIM_BIC_CREATED_DT) values ";
            string str1 = "insert into " + HttpContext.Current.Session["TENANT"] + ".BIM_BIC_ATTENDANCE_TEMP (BIM_BIC_LOC_CODE, BIM_BIC_REGION,BIM_BIC_LOC_STATUS,BIM_BIC_USR_ID,BIM_BIC_USR_NAME,BIM_BIC_EMAIL,BIM_BIC_QUATER_NAME,BIM_BIC_QTR_START_DATE,BIM_BIC_QTR_END_DATE,BIM_BIC_TENURE, BIM_BIC_START_DATE, BIM_BIC_APP_START_DATE, BIM_BIC_END_DATE,BIM_BIC_CREATED_BY,BIM_BIC_CREATED_DT) values ";

            string str2 = "";
            int retval, cnt = 1;
            foreach (BranchIncentvieAttendanceModel excel in excelFields)
            {
                if ((cnt % 1000) == 0)
                {
                    str2 = str2.Remove(str2.Length - 1, 1);
                    retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str1 + str2);
                    if (retval == -1)
                    {
                        return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                    }
                    else
                    {
                        str2 = "";
                        cnt = 1;
                    }

                }
                // str2 += string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}'),", excel.Branch_Code, excel.Branch_Name, excel.Region, excel.Br_Status, excel.Emp_Code, excel.BIC_Name, excel.Email_Id, excel.Quarter_Name, excel.Qtr_Start_Date, excel.Qtr_End_Date, excel.Tenure, excel.BIC_Start_Date, excel.App_Start_Date,excel.BIC_End_date, excel.Applicable_Days, HttpContext.Current.Session["UID"], DateTime.Now);
                str2 += string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}'),",  excel.Branch_Name, excel.Region, excel.Br_Status, excel.Emp_Code, excel.BIC_Name, excel.Email_Id, excel.Quarter_Name, excel.Qtr_Start_Date, excel.Qtr_End_Date, excel.Tenure, excel.BIC_Start_Date, excel.App_Start_Date,excel.BIC_End_date, HttpContext.Current.Session["UID"], DateTime.Now);

                cnt++;
            }
            str2 = str2.Remove(str2.Length - 1, 1);
            retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str1 + str2);
            //if (retval == -1)
            //return new { Message = MessagesVM.UAD_UPLOK, data = (object)null };
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[0].Value = HttpContext.Current.Session["UID"];
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[SAVE_BIM_BIC_ATTENDANCE]", param);
                if (dt.Rows.Count > 0)
                    return new { Message = MessagesVM.UAD_UPLOK, data = dt };
                else
                    return new { Message = MessagesVM.UAD_UPLVALDAT, data = dt };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        

        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public object GetGridData()
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_BIC_ATTENDANCE_DATA]");
            sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public IEnumerable<ExpLocationVM> BindLocation()
    {
        List<ExpLocationVM> LocUtilityList = new List<ExpLocationVM>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UTILITY_LOCATION");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                LocUtilityList.Add(new ExpLocationVM()
                {
                    LCM_CODE = sdr.GetValue(0).ToString(),
                    LCM_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return LocUtilityList;
        }
    }
    private XLWorkbook CreateUtilityExcelWithValidation(List<ExpLocationVM> locations)
    {
        try
        {
            var wb = new XLWorkbook();
            var ws = wb.Worksheets.Add("Location Wise"); //Make sure changing work sheet name in corresponding references also
            var dataWs = wb.Worksheets.Add("Data Sheet");
            CreateHeaders(ref ws);
            AdjustColumnWidth(ref ws);
            var locationList = locations.Select(x => x.LCM_CODE).ToList();
            int lastRowNumber = AddDataToSheet(ref dataWs, "A", locationList);
            var locationCell = ws.Range("A2", ws.Column("A").LastCell().Address.ToString()).SetDataValidation();
            locationCell.List(dataWs.Range("A1:A" + (lastRowNumber - 1)), true);
            ApplyValidationMessage("Invalid Location", "Please select any one of the option provided", ref locationCell);

            var QtrStartDate = ws.Range("H2", ws.Column("H").LastCell().Address.ToString()).SetDataValidation();
            QtrStartDate.Date.EqualOrGreaterThan(new DateTime(2000, 1, 1));
            ApplyValidationMessage("Invalid Date", "Provided date should be in valid format", ref QtrStartDate);
            ws.Column("H").Style.NumberFormat.Format = "dd-MM-yyyy";

            var QtrEndDate  = ws.Range("I2", ws.Column("I").LastCell().Address.ToString()).SetDataValidation();
            QtrEndDate.Date.EqualOrGreaterThan(new DateTime(2000, 1, 1));
            ApplyValidationMessage("Invalid Date", "Provided date should be in valid format", ref QtrEndDate);
            ws.Column("H").Style.NumberFormat.Format = "dd-MM-yyyy";

            var BICStartDate = ws.Range("K2", ws.Column("K").LastCell().Address.ToString()).SetDataValidation();
            BICStartDate.Date.EqualOrGreaterThan(new DateTime(2000, 1, 1));
            ApplyValidationMessage("Invalid Date", "Provided date should be in valid format", ref BICStartDate);
            ws.Column("H").Style.NumberFormat.Format = "dd-MM-yyyy";

            var AppStartDate = ws.Range("L2", ws.Column("L").LastCell().Address.ToString()).SetDataValidation();
            AppStartDate.Date.EqualOrGreaterThan(new DateTime(2000, 1, 1));
            ApplyValidationMessage("Invalid Date", "Provided date should be in valid format", ref AppStartDate);
            ws.Column("H").Style.NumberFormat.Format = "dd-MM-yyyy";


            var BICEnddate = ws.Range("M2", ws.Column("M").LastCell().Address.ToString()).SetDataValidation();
            BICEnddate.Date.EqualOrGreaterThan(new DateTime(2000, 1, 1));
            ApplyValidationMessage("Invalid Date", "Provided date should be in valid format", ref BICEnddate);
            ws.Column("H").Style.NumberFormat.Format = "dd-MM-yyyy";

            wb.Worksheet("Data Sheet").Visibility = XLWorksheetVisibility.VeryHidden;
          

            return wb;

        }
        catch (Exception ex)
        {
            throw;
        }
    }
    public string DownloadLocationExcel()
    {
        var locations = BindLocation();
        var wb = CreateUtilityExcelWithValidation(locations.ToList());
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName("BIC_ATTENDANCE.xlsx"));
        wb.SaveAs(filePath);
        return filePath;
    }

    private void CreateHeaders(ref IXLWorksheet ws)
    {
        ws.Cell(1, "A").Value = "Branch Code";
        ws.Cell(1, "B").Value = "Region";
        ws.Cell(1, "C").Value = "Br Status";
        ws.Cell(1, "D").Value = "Emp Code";
        ws.Cell(1, "E").Value = "BIC Name";
        ws.Cell(1, "F").Value = "Email Id";
        ws.Cell(1, "G").Value = "Quarter Name";
        ws.Cell(1, "H").Value = "Qtr Start Date";
        ws.Cell(1, "I").Value = "Qtr End Date";
        ws.Cell(1, "J").Value = "Tenure";
        ws.Cell(1, "K").Value = "BIC Start Date";
        ws.Cell(1, "L").Value = "App Start Date";
        ws.Cell(1, "M").Value = "BIC End date";
    }

    private void AdjustColumnWidth(ref IXLWorksheet ws)
    {
        ws.Column(1).Width = 30;
        ws.Column(2).Width = 25;
        ws.Column(3).Width = 25;
        ws.Column(4).Width = 20;
        ws.Column(5).Width = 20;
        ws.Column(6).Width = 20;
        ws.Column(7).Width = 20;
        ws.Column(8).Width = 35;
        ws.Column(9).Width = 20;
        ws.Column(10).Width = 20;
        ws.Column(11).Width = 20;
        ws.Column(12).Width = 30;
        ws.Column(13).Width = 30;
    }

    private void ApplyValidationMessage(string title, string message, ref IXLDataValidation dataValidation)
    {
        dataValidation.ErrorStyle = XLErrorStyle.Stop;
        dataValidation.ErrorTitle = title;
        dataValidation.ErrorMessage = message;
        dataValidation.ShowErrorMessage = true;
    }


    private int AddDataToSheet(ref IXLWorksheet ws, string columnName, List<string> dataList)
    {
        int rowNumber = 1;
        foreach (var data in dataList)
        {
            ws.Cell(columnName + rowNumber).Value = data;
            rowNumber++;
        }
        return rowNumber;
    }
}