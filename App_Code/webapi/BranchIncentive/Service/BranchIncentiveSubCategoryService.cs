﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for BranchIncentiveSubCategoryService
/// </summary>
public class BranchIncentiveSubCategoryService
{
    SubSonic.StoredProcedure sp;
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBIMMaintype");
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetMainCategory(BranchIncentiveSubCategoryModel CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBIMMainCat");
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetGridData(BranchIncentiveSubCategoryModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBIMMainSubCat");
            sp.Command.AddParameter("@BCL_SUB_MC_CODE", CustRpt.BCL_MC_CODE, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public object GetScoreData(BranchIncentiveSubCategoryModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBIMCatSubCatScoreMapping");
            sp.Command.AddParameter("@BCL_SUB_ID", CustRpt.BCL_SUB_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_MC_CODE", CustRpt.BCL_MC_CODE, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(BranchIncentiveSubCategoryModel CustRpt)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[13];
            param[0] = new SqlParameter("@BCL_SUB_ID", SqlDbType.Int);
            param[0].Value = 0;
            param[1] = new SqlParameter("@BCL_SUB_NAME", SqlDbType.NVarChar);
            param[1].Value = CustRpt.BCL_SUB_NAME;
            param[2] = new SqlParameter("@BCL_SUB_MC_CODE", SqlDbType.NVarChar);
            param[2].Value = CustRpt.BCL_MC_CODE;
            param[3] = new SqlParameter("BCL_SUB_CREATED_BY", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            param[4] = new SqlParameter("@BCL_SUB_REM", SqlDbType.NVarChar);
            param[4].Value = CustRpt.BCL_SUB_REM;
            param[5] = new SqlParameter("@BCL_SUB_COMPANYID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["COMPANYID"];
            param[6] = new SqlParameter("@BCL_SUB_TYPE", SqlDbType.NVarChar);
            param[6].Value = CustRpt.BCL_SUB_TYPE;
            param[7] = new SqlParameter("@BCL_SUB_FLAG_TYPE1", SqlDbType.NVarChar);
            param[7].Value = CustRpt.BCL_SUB_FLAG_TYPE1;
            param[8] = new SqlParameter("@BCL_SUB_FLAG_TYPE2", SqlDbType.NVarChar);
            param[8].Value = CustRpt.BCL_SUB_FLAG_TYPE2;
            param[9] = new SqlParameter("@BCL_SUB_FLAG_PLANTYPE", SqlDbType.NVarChar);
            param[9].Value = CustRpt.BCL_SUB_FLAG_PLANTYPE;
            param[10] = new SqlParameter("@BCL_SUB_WEIGHTAGE", SqlDbType.NVarChar);
            param[10].Value = CustRpt.BCL_SUB_WEIGHTAGE;
            param[11] = new SqlParameter("@BCL_SUB_TYPE_NO", SqlDbType.NVarChar);
            param[11].Value = CustRpt.BCL_SUB_TYPE_NO;
            param[12] = new SqlParameter("@SCOREDETAILS", SqlDbType.Structured);
            param[12].Value = UtilityService.ConvertToDataTable(CustRpt.scoreA);
            int flag = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_AddUpdateBIMMainSubCat", param);
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object updateData(BranchIncentiveSubCategoryModel CustRpt)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter("@BCL_SUB_ID", SqlDbType.Int);
            param[0].Value = CustRpt.BCL_SUB_ID;
            param[1] = new SqlParameter("@BCL_SUB_NAME", SqlDbType.NVarChar);
            param[1].Value = CustRpt.BCL_SUB_NAME;
            param[2] = new SqlParameter("@BCL_SUB_MC_CODE", SqlDbType.NVarChar);
            param[2].Value = CustRpt.BCL_MC_CODE;
            param[3] = new SqlParameter("BCL_SUB_CREATED_BY", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            param[4] = new SqlParameter("@BCL_SUB_REM", SqlDbType.NVarChar);
            param[4].Value = CustRpt.BCL_SUB_REM;
            param[5] = new SqlParameter("@BCL_SUB_COMPANYID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["COMPANYID"];
            param[6] = new SqlParameter("@BCL_SUB_TYPE", SqlDbType.NVarChar);
            param[6].Value = CustRpt.BCL_SUB_TYPE;
            param[7] = new SqlParameter("@BCL_SUB_FLAG_TYPE1", SqlDbType.NVarChar);
            param[7].Value = CustRpt.BCL_SUB_FLAG_TYPE1;
            param[8] = new SqlParameter("@BCL_SUB_FLAG_TYPE2", SqlDbType.NVarChar);
            param[8].Value = CustRpt.BCL_SUB_FLAG_TYPE2;
            param[9] = new SqlParameter("@BCL_SUB_FLAG_PLANTYPE", SqlDbType.NVarChar);
            param[9].Value = CustRpt.BCL_SUB_FLAG_PLANTYPE;
            param[10] = new SqlParameter("@BCL_SUB_TYPE_NO", SqlDbType.NVarChar);
            param[10].Value = CustRpt.BCL_SUB_TYPE_NO;
            param[11] = new SqlParameter("@SCOREDETAILS", SqlDbType.Structured);
            param[11].Value = UtilityService.ConvertToDataTable(CustRpt.scoreA);
            int flag = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_AddUpdateBIMMainSubCat", param);
            //int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object DeleteScore(BIMSCOREDETAILSDELETE reldet)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BCL_CH_ID", SqlDbType.Int);
        param[0].Value = reldet.BCL_CH_ID;
        param[1] = new SqlParameter("@BCL_CH_STA_ID", SqlDbType.Int);
        param[1].Value = reldet.BCL_CH_STA_ID;
        int flag = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "BIM_spActiveInActiveScore", param);
        return flag;     
    }
}