﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CreateScoreCardModel
/// </summary>
public class CreateScoreCardModel
{
   
}
public class SelRadio1
{
    //public string row { get; set; }
    public string TypeCode { get; set; }
    public string CatCode { get; set; }
    public string SubcatCode { get; set; }
    public string ScoreCode { get; set; }
    public string ScoreName { get; set; }
    //public List<Score_Name> ScoreName { get; set; }
    public string txtdata { get; set; }
    //public string txtdatas { get; set; }
    public string Date { get; set; }
    public string SubScore { get; set; }
    public string FilePath { get; set; }
    //public string Raise { get; set; }

    public string BCLD_ZF_ACTIONS { get; set; }
    public string BCLD_ZF_COMENTS { get; set; }
    public string BCLD_CENTRAL_ACTIONS { get; set; }
    public string BCLD_CENTRAL_TEAM_CMTS { get; set; }
    public float ScorePercent { get; set; }



}

public class selecteddata1
{
    public int BCL_ID { get; set; }
    public List<LoclstHirachy> LCMLST { get; set; }
    public string LcmList { get; set; }
    public string InspectdBy { get; set; }
    public string date { get; set; }
    public string ReviewedBy { get; set; }
    public string Revieweddate { get; set; }
    public string ApprovedBy { get; set; }
    public string Approveddate { get; set; }
    public string SelCompany { get; set; }
    public List<SelRadio1> Seldata { get; set; }
    public List<FileProperties> PostedFiles { get; set; }
    public int Flag { get; set; }
    public ObservableCollection<ImagesList> imagesList { get; set; }
    public string OVERALL_CMTS { get; set; }
    public string BCL_TYPE_ID { get; set; }
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public float F_Percentage { get; set; }
}

public class ChecklistScore
{
    public int BCL_TP_ID { get; set; }

    public List<ChecklistData1> ChecklistScore1 = new List<ChecklistData1>();
}

public class ChecklistData1
{
    public int BCL_TP_ID { get; set; }
    public string BCL_TP_NAME { get; set; }
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public string BCL_SUB_TYPE { get; set; }
    public string ISFILE { get; set; }
    public string ISCOMMENTS { get; set; }
    public string ISTEXTBOX { get; set; }
    public string ISDATE { get; set; }

    public string ISBUTTON { get; set; }
    public DateTime DATERESPONSE { get; set; }
    public string FILEPATH { get; set; }
    public string TEXT { get; set; }
    public string TEXTBOX { get; set; }
    public string RESPONSE { get; set; }
    public string BCL_TP_CODE { get; set; }
    public string BCL_SUB_WEIGHTAGE { get; set; }
    public int BCL_SUB_FLAG_TYPE_RATING { get; set; }
    public string ValidateInchargeComment { get; set; }

    public int BCL_ID { get; set; }
    public int Rating { get; set; }

    public List<ChecklistDetail1> checklistDetails = new List<ChecklistDetail1>();
}


public class ChecklistDetail1
{
    public int BCL_CH_ID { get; set; }
    public string BCL_CH_CODE { get; set; }
    public string BCL_CH_NAME { get; set; }
    public string BCL_CH_SUB_CODE { get; set; }
    public bool RESPONSE { get; set; }
    public string BCL_SUB_WEIGHTAGE { get; set; }
    public string ValidateInchargeComment { get; set; }

    public Boolean Editable { get; set; }
}


