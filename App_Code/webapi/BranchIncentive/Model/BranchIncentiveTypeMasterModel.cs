﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BranchIncentiveTypeMasterModel
/// </summary>
public class BranchIncentiveTypeMasterModel
{

    public string BICM_CODE { get; set; }
    public string BICM_NAME { get; set; }
    public string BICM_ID { get; set; }
    public string BIM_ID { get; set; }  
    public string BIM_CODE { get; set; }
    public string BIM_NAME { get; set; }
    public string BCL_TP_NAME { get; set; }
    public string BIM_FROM_SCORE { get; set; }
    public string BIM_UPTO_SCORE { get; set; }
    public string BIM_INCENTIVE_AMOUNT { get; set; }
    public string BIM_MC_STA_ID { get; set; }
    public int BCL_TP_MC_ID { get; set; }


}