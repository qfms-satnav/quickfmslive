﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BranchIncentiveCategoryModel
/// </summary>
public class BranchIncentiveCategoryModel
{
     public string BICM_CODE { get; set; }
    public string BICM_NAME { get; set; }
    public string BICM_RANGE { get; set; }
    public string BICM_STA_ID { get; set; }
}

