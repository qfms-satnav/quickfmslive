﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BranchIncentiveLocationMasterModel
/// </summary>
public class BranchIncentiveLocationMasterModel
{
    public string LCM_CODE { get; set; }
    public string BICM_CODE { get; set; }
    public string BILCM_LOC_CODE { get; set; }
    public string BILCM_CAT_CODE { get; set; }
    public string BILCM_STA_ID { get; set; }
    public string BILCM_LOC_NAME { get; set; }
    public string BILCM_ID { get; set; }
}