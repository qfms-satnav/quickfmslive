﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

/// <summary>
/// Summary description for BranchIncentvieAttendanceModel
/// </summary>
public class BranchIncentvieAttendanceModel
{
	//[JsonProperty("Branch Code")]
	//public string Branch_Code { get; set; }


	[JsonProperty("Branch Name")]
	public string Branch_Name { get; set; }

	public string Region { get; set; }


	[JsonProperty("Br Status")]
	public string Br_Status { get; set; }



	[JsonProperty("Emp Code")]
	public string Emp_Code { get; set; }


	[JsonProperty("BIC Name")]
	public string BIC_Name { get; set; }

	[JsonProperty("Email Id")]
	public string Email_Id { get; set; }
	

	[JsonProperty("Quarter Name")]
	public string Quarter_Name { get; set; }


	[JsonProperty("Qtr Start Date")]
	public DateTime Qtr_Start_Date { get; set; }


	[JsonProperty("Qtr End Date")]
	public DateTime Qtr_End_Date { get; set; }

	public int Tenure { get; set; }


	[JsonProperty("BIC Start Date")]
	public DateTime BIC_Start_Date { get; set; }



	[JsonProperty("App Start Date")]
	public DateTime App_Start_Date { get; set; }


	[JsonProperty("BIC End date")]
	public DateTime BIC_End_date { get; set; }


	//[JsonProperty("Applicable Days")]
	//public int Applicable_Days { get; set; }


	public class ExpAttendanceVM
	{
		public string LCM_CODE { get; set; }
		public string LCM_NAME { get; set; }

		public bool ticked { get; set; }
	}
}
public class ExpLocationVM
{
	public string LCM_CODE { get; set; }
	public string LCM_NAME { get; set; }

	public bool ticked { get; set; }
}