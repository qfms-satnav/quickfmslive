﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BranchIncentiveTypeModel
/// </summary>
public class BranchIncentiveTypeModel
{
    public int BCL_TP_ID { get; set; }
    public string BCL_TP_NAME { get; set; }
    public string BCL_TP_REMARKS { get; set; }
    public int BCL_TP_LEVEL { get; set; }

}