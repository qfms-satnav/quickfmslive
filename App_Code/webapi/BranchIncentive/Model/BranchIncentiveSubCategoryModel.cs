﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BranchIncentiveSubCategoryModel
/// </summary>
public class BranchIncentiveSubCategoryModel
{
    public int BCL_SUB_ID { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public int BCL_TP_MC_ID { get; set; }
    public string BCL_SUB_REM { get; set; }
    public string BCL_SUB_TYPE { get; set; }
    public string BCL_MC_CODE { get; set; }
    public string BCL_SUB_FLAG_TYPE1 { get; set; }
    public string BCL_SUB_FLAG_TYPE2 { get; set; }
    public string BCL_SUB_FLAG_TYPE3 { get; set; }
    public string BCL_TP_NAME { get; set; }
    public string BCL_SUB_FLAG_PLANTYPE { get; set; }
    public decimal BCL_SUB_FLAG_TYPE_RATING { get; set; }
    public decimal BCL_SUB_FLAG_RATING_VALUE { get; set; }
    public string BCL_SUB_FLAG_RAISEREQUEST { get; set; }
    public int BCL_SUB_TYPE_NO { get; set; }
    public string BCL_SUB_WEIGHTAGE { get; set; }
    public List<BIMSCOREDETAILS> scoreA { get; set; }
}
public class BIMSCOREDETAILS
{
    public int BCL_CH_ID { get; set; }
    public string BCL_CH_CODE { get; set; }
}

public class BIMSCOREDETAILSDELETE
{
    public int BCL_CH_ID { get; set; }
    public string BCL_CH_STA_ID { get; set; }
}