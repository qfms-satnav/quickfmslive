﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BranchScoreCardReportModel
/// </summary>
public class BranchScoreCardReportModel
{
    public string LCM_ZONE { get; set; }
    public string CTY_CODE { get; set; }
    public string LCM_CODE { get; set; }

    public string AUR_KNOWN_AS { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }


}
public class GridColumns
{
    public string headerName { get; set; }
    public string field { get; set; }
    public int width { get; set; }
    public string cellClass { get; set; }
    public bool suppressMenu { get; set; }
}

public class ZNlsts
{

    public string LCM_ZONE { get; set; }
    public bool ticked { get; set; }

}

public class Citylsts
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_ZONE{ get; set; }
    public string LCM_CTY_ID { get; set; }
    public bool ticked { get; set; }
}