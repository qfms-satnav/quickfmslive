﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BranchIncentiveManCategoryController : ApiController
{
    BranchIncentiveManCategoryService BIMservice = new BranchIncentiveManCategoryService();
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = BIMservice.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGridData(BranchIncentiveManCategoryModel CustRpt)
    {
        var obj = BIMservice.GetGridData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(BranchIncentiveManCategoryModel CustRpt)
    {
        var obj = BIMservice.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage updateData(BranchIncentiveManCategoryModel CustRpt)
    {
        var obj = BIMservice.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}

