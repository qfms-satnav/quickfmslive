﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BranchIncentiveSubCategoryController : ApiController
{
    BranchIncentiveSubCategoryService BISservice = new BranchIncentiveSubCategoryService();
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = BISservice.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMainCategory(BranchIncentiveSubCategoryModel CustRpt)
    {
        var obj = BISservice.GetMainCategory(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGridData(BranchIncentiveSubCategoryModel CustRpt)
    {
        var obj = BISservice.GetGridData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetScoreData(BranchIncentiveSubCategoryModel CustRpt)
    {
        var obj = BISservice.GetScoreData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(BranchIncentiveSubCategoryModel CustRpt)
    {
        var obj = BISservice.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage updateData(BranchIncentiveSubCategoryModel CustRpt)
    {
        var obj = BISservice.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage DeleteScore(BIMSCOREDETAILSDELETE reldet)
    {
        var obj = BISservice.DeleteScore(reldet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
