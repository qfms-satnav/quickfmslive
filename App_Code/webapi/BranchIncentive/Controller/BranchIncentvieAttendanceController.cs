﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;
using LocWiseUtilityVModel;
using System.Threading.Tasks;
using System.IO;
using System;
using ClosedXML.Excel;
using System.Web.UI.WebControls;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Web.Http.Filters;

public class BranchIncentvieAttendanceController : ApiController
{
    BranchIncentvieAttendanceService BIAS = new BranchIncentvieAttendanceService();

    [HttpPost]
    public HttpResponseMessage UploadFile()
    {
        var httpFile = HttpContext.Current.Request;
        var obj = BIAS.ReadExcel_Data(httpFile);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGridData()
    {
        var obj = BIAS.GetGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    private MemoryStream GetStream(XLWorkbook excelWorkbook)
    {
        MemoryStream fs = new MemoryStream();
        excelWorkbook.SaveAs(fs);
        fs.Position = 0;
        return fs;
    }

    [HttpPost]

    public HttpResponseMessage LocationExcelDownload()
    {
        var filePath = BIAS.DownloadLocationExcel();
        var response = Request.CreateResponse(HttpStatusCode.OK, filePath);
        return response;
    }
}
