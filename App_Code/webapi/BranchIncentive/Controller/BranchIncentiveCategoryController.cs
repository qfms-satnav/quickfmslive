﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

public class BranchIncentiveCategoryController : ApiController
{
    BranchIncentiveCategoryService BICS = new BranchIncentiveCategoryService();

    [HttpPost]
    public HttpResponseMessage SaveCategoryDetails(BranchIncentiveCategoryModel BicModel)
    {
        if (BICS.SaveCategoryData(BicModel) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already Exists");
    }


    [HttpPost]
    public HttpResponseMessage ModifyCategoryDetails(BranchIncentiveCategoryModel BicModel)
    {
        var obj = BICS.ModifyCategoryDetails(BicModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<BranchIncentiveCategoryModel> BicModel = BICS.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
        return response;
    }

}
