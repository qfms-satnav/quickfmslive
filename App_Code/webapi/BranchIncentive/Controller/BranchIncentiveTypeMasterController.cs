﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BranchIncentiveTypeMasterController : ApiController
{
    BranchIncentiveTypeMasterService BITMS = new BranchIncentiveTypeMasterService();
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = BITMS.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGridData(BranchIncentiveTypeMasterModel CustRpt)
    {
        var obj = BITMS.GetGridData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(BranchIncentiveTypeMasterModel CustRpt)
    {
        var obj = BITMS.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage updateData(BranchIncentiveTypeMasterModel CustRpt)
    {
        var obj = BITMS.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
