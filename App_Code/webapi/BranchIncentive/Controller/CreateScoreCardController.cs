﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


public class CreateScoreCardController : ApiController
{
    CreateScoreCardService CSCS = new CreateScoreCardService();
    [HttpGet]
    public Object getInspectors()
    {

        return CSCS.getInspectors();
    }
    [HttpPost]
    public HttpResponseMessage getInspectorsZonalCentral(int TPM_TP_ID, string TPM_LCM_CODE, int TPMD_APPR_LEVELS)
    {
        var obj = CSCS.getInspectorsZonalCentral(TPM_TP_ID, TPM_LCM_CODE, TPMD_APPR_LEVELS);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public Object getCompany()
    {

        return CSCS.getCompany();
    }
    [HttpGet]
    public Object getLocations()
    {

        return CSCS.getLocations();
    }


    [HttpGet]
    public HttpResponseMessage GetLocationsIn([FromUri] int id)
    {
        var obj = CSCS.GetLocationsIn(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetScorePer()
    {
        var obj = CSCS.GetScorePer();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetInspectorsIn([FromUri] int id)
    {
        var obj = CSCS.GetInspectorsIn(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = CSCS.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpPost]
    //public HttpResponseMessage GetGriddata(int BCL_TP_ID, int BCL_ID)
    //{
    //    //var response = ccs.GetGriddata(lst);
    //    //return response;
    //    var obj = CSCS.GetGriddata(BCL_TP_ID, BCL_ID);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    //[HttpPost]
    //public HttpResponseMessage getSavedList([FromUri] int BCLStatus)
    //{
    //    var obj = CSCS.getSavedList(BCLStatus);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    [HttpPost]
    public HttpResponseMessage getSavedList([FromUri] int BCLStatus, int Status)
    {
        var obj = CSCS.getSavedList(BCLStatus, Status);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGriddata(int BCL_ID)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = CSCS.GetGriddata(BCL_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage InsertCheckListV2(selecteddata1 seldt)
    {
        var obj = CSCS.InsertCheckListV2(seldt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    ///////////**********Validaion Submit************////////
    [HttpPost]
    public HttpResponseMessage SaveValidationData(selecteddata1 dataobj)
    {

        var obj = CSCS.SaveValidationData(dataobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    ///////////**********Approval Submit************////////
    [HttpPost]
    public HttpResponseMessage SaveApprovalData(selecteddata1 dataobj)
    {

        var obj = CSCS.SaveApprovalData(dataobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }



    [HttpGet]
    public HttpResponseMessage GetLocationsScore(int id)
    {
        var obj = CSCS.GetLocationsScore(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }




    ///////////**********Bulk Approval Submit************////////
    [HttpPost]
    public HttpResponseMessage BulkApprovalData(int[] selectedData)
    {

        var obj = CSCS.BulkApprovalData(selectedData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
