﻿using System;
using QuickFMS.API.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BranchIncentiveTypeController : ApiController
{
    BranchIncentiveTypeService BITS = new BranchIncentiveTypeService();
    [HttpPost]
    public HttpResponseMessage GetGridData()
    {
        var obj = BITS.GetGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(BranchIncentiveTypeModel CustRpt)
    {
        var obj = BITS.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage updateData(BranchIncentiveTypeModel CustRpt)
    {
        var obj = BITS.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
