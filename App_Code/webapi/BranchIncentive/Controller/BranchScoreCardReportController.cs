﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

public class BranchScoreCardReportController : ApiController
{
    BranchScoreCardReportService BSCRS = new BranchScoreCardReportService();

    [HttpPost]
    public HttpResponseMessage GetRegions()
    {
        var obj = BSCRS.GetRegions();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetSpocs()
    {
        var obj = BSCRS.GetSpocs();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpPost]
    //public HttpResponseMessage GetCitiesByZone([FromUri] int id, [FromBody] List<ZNlsts> cny)
    //{
    //    var obj = BSCRS.GetCitiesByZone(cny, id );
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}

    [HttpPost]
    public HttpResponseMessage GetCitiesByZone([FromUri] int id, [FromBody] List<ZNlsts> cost)
    {
        var obj = BSCRS.GetCitiesByZone(cost, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetScoreCardtDetails(BranchScoreCardReportModel det)
    {
        var obj = BSCRS.GetScoreCardtDetails(det);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetLocationsScoreReport()
    {
        var obj = BSCRS.GetLocationsScoreReport();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}
