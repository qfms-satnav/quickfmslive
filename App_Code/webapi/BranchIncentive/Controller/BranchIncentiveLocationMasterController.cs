﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

public class BranchIncentiveLocationMasterController : ApiController
{
    BranchIncentiveLocationMasterService BICLM = new BranchIncentiveLocationMasterService();
    [HttpPost]
    public HttpResponseMessage GetCategory()
    {
        var obj = BICLM.GetCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveData(BranchIncentiveLocationMasterModel Save)
    {
        var obj = BICLM.SaveData(Save);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage BindGridData()
    {
        IEnumerable<BranchIncentiveLocationMasterModel> BicModel = BICLM.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ModifyCategoryDetails(BranchIncentiveLocationMasterModel BicModel)
    {
        var obj = BICLM.ModifyCategoryDetails(BicModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
        return response;
    }

}
