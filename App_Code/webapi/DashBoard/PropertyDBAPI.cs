﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class PropertyDBAPIController : ApiController
{
    PropertyDBModuleService service = new PropertyDBModuleService();

    public class company
    {
        public int companyid { get; set; }
        public string Type { get; set; }
    }
    //Bind Properties - Graph
    [HttpPost]
    public object[] BindProperties([FromBody] company c)
    {
        return service.BindProperties(c.companyid);
    }

    //Received and Pending Amounts Bar Chart
    [HttpPost]
    public object BindAmounts([FromBody] company c)
    {
        return service.BindAmounts(c.companyid);
    }

    //Work Req Total/Completed/Pending 
    [HttpGet]
    public object GetWorkRequests()
    {
        return service.GetWorkRequests();
    }

    [HttpPost]
    public DataTable GetExpLeasesDetails([FromBody] company c)
    {
        return service.GetExpLeasesDetails(c.companyid);
    }
    [HttpPost]
    public object GetLeaseDueDetails([FromBody] company c)
    {
        return service.GetLeaseDueDetails(c.companyid);
    }

    [HttpPost]
    public HttpResponseMessage GetPropDetailsByPropName([FromBody] company c)
    {
        var obj = service.GetPropDetailsByPropName(c.Type, c.companyid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetPendingAndReceivedAmounts([FromBody] company c)
    {
        var obj = service.GetPendingAndReceivedAmounts(c.Type,c.companyid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public DataTable GetDueLeaseReceivedAmounts([FromUri] int category)
    {
        return service.GetDueLeaseReceivedAmounts(category);

    }

    //[HttpGet]
    //public DataSet GetProperty_Details()
    //{
    //    return service.GetProperty_Details();
    //}

    //Bind city wise roperties - Graph
    [HttpPost]
    public object[] BindCityWise_Properties([FromBody] company c)
    {
        return service.BindCityWise_Properties(c.companyid);
    }

    [HttpGet]
    public HttpResponseMessage GetProperties_City([FromUri] string cityName)
    {
        var obj = service.GetProperties_City(cityName);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [HttpPost]
    public DataSet GetTenant_Vacancies([FromBody] company c)
    {
        return service.GetTenant_Vacancies(c.companyid);
    }

    [HttpPost]
    public DataSet Get_Vacant_Properties([FromBody] company c)
    {
        return service.Get_Vacant_Properties(c.companyid);
    }

    [HttpPost]
    public object ExpenseUtilityChartByLoc([FromBody] company c)
    {
        return service.ExpenseUtilityChartByLoc(c.companyid);
    }

    [HttpPost]
    public HttpResponseMessage GetExpenseUtilityDetailsByLoc([FromBody] parameters p)
    {
        var obj = service.GetExpenseUtilityDetailsByLoc(p.location, p.expense);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public class parameters
    {
        public string location { get; set; }
        public string expense { get; set; }
    }
}
