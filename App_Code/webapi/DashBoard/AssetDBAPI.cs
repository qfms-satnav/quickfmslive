﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class AssetDBAPIController : ApiController
{
    AssetDBModuleService service = new AssetDBModuleService();
    public class company
    {
        public int companyid { get; set; }
        public DateTime From_Date { get; set; }
        public DateTime To_Date { get; set; }
    }

   [HttpPost]
    public object GetAssetTypes([FromBody] company c)
    {
        return service.GetAssetTypes(c.companyid);
    }

   [HttpPost]
   public object GetMapunmapassets([FromBody] company c)
    {
        return service.GetMapunmapassets(c.companyid);
    }

    [HttpGet]
   public DataSet GetMap_umMapAssets_DB([FromUri] int Companyid)
   {
       return service.GetMap_umMapAssets_DB(Companyid);
   }

   [HttpGet]
   public HttpResponseMessage GetAssetType_Categories([FromUri] string category, [FromUri] int Companyid)
   {
       var obj = service.GetAssetType_Categories(Companyid, category);
       HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
       return response;
   }
    [HttpPost]
    public object GetTotalAssets_by_Sub_category(company C)
    {
        var obj = service.GetTotalAssets_by_Sub_category(C.From_Date,C.To_Date);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public object GetTotalAssets_by_Quantity()
    {
        var obj = service.GetTotalAssets_by_Quantity();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public object Asset_Po_Status()
    {
        var obj = service.Asset_Po_Status();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object Asset_Dispose_details()
    {
        var obj = service.Asset_Dispose_details();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public object Asset_Movement_Details()
    {
        var obj = service.Asset_Movement_Details();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
