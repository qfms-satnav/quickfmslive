﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ConferenceDBAPIController : ApiController
{
    ConferenceDBModuleService service = new ConferenceDBModuleService();

    public class company
    {
        public int companyid { get; set; }
    }

    [HttpPost]
    public object GetcitywiseconfCount([FromBody] company c)
    {
        return service.GetcitywiseconfCount(c.companyid);
    }

    [HttpPost]
    public object GetcitywiseUtil([FromBody] company c)
    {
        return service.GetcitywiseUtil(c.companyid);
    }

    [HttpPost]
    public object GetConfUtil([FromBody] company c)
    {
        return service.GetConfUtil(c.companyid);
    }
}
