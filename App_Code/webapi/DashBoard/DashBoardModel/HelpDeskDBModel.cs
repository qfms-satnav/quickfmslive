﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HelpDeskDBModel
/// </summary>
public class HelpDeskDBModel
{
	public class SLADetails
    {
        public int SLACount { get; set; }
        public decimal SLAratio { get; set; }
    }
    public class StatuswiseData
    {
        public string SER_MNC_CODE { get; set; }
        public string SER_REQ_ID { get; set; }
        public string SER_PROB_DESC { get; set; }
        public string SER_CAL_LOG_BY { get; set; }
    }
}