﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class MaintenanceDBAPIController : ApiController
{
    MaintenanceDBModuleService service=new MaintenanceDBModuleService();
    // GET api/<controller>
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }

    public class company
    {
        public int companyid { get; set; }
    }


    //AMC Types PIE
    [HttpPost]
    public object[] BindAMCTypes([FromBody] company c)
    {
        return service.BindAMCTypes(c.companyid);
    }

    //Assets Loc Wise Bar Chart
    [HttpPost]
    public object BindAMCNonAMCBarCh([FromBody] company c)
    {
        return service.BindAMCNonAMCBarCh(c.companyid);
    }

    // AMC AMOUNT RECEIVED CURR MONTH
    [HttpPost]
    public DataTable GetAMCAmounts([FromBody] company c)
    {
        return service.GetAMCAmounts(c.companyid);
    }

    [HttpGet]
    public DataTable GetAMCAssetsExp([FromUri] company c)
    {
        return service.GetAMCAssetsExp(c.companyid);
    }

    [HttpGet]
    public HttpResponseMessage GetAssetsByCategory([FromUri] string category, int company)
    {
        var obj = service.GetAssetsByCategory(category, company);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object GetWorkRequests([FromBody] company c)
    {
        return service.GetWorkRequests(c.companyid);
    }

    [HttpPost]
    public object GetWorkOrdersDetails([FromBody] company c)
    {
        return service.GetWorkOrdersDetails(c.companyid);
    }

    [HttpPost]
    public object GetAssetsUnderAMC_and_PrvntMain([FromBody] company c)
    {
        return service.GetAssetsUnderAMC_and_PrvntMain(c.companyid);
    }

    //AMC ASSETS
    [HttpGet]
    public DataSet GetAMCAssets()
    {
        return service.GetAMCAssets();
    }
    [HttpPost]
    public HttpResponseMessage GetAssetsByCategoryLocationAndAmcorNonAMC([FromBody] parameters p)
    {
        var obj = service.GetAssetsByCategoryLocationAndAmcorNonAMC(p.location, p.category, p.company);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    public class parameters
    {
        public string location { get; set; }
        public string category { get; set; }
        public string company { get; set; }
    }
}
