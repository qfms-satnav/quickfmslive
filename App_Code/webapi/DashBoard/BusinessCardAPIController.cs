﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BusinessCardAPIController : ApiController
{
    BusinessCardService service = new BusinessCardService();

    public class company
    {
        public int companyid { get; set; }
    }

    [HttpPost]
    public object GetVendorwiseCount([FromBody] company c)
    {
        return service.GetVendorReqCount(c.companyid);
    }

    [HttpPost]
    public object GeStaCount([FromBody] company c)
    {
        return service.GetStaCount(c.companyid);
    }
}
