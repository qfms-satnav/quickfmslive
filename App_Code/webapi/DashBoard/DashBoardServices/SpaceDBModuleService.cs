﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for SpaceDBModuleService
/// </summary>
public class SpaceDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object bindWorkstation(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_WS_REPORT");
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int32);
        ds = sp.GetDataSet();

        //Total WS Count
        int totalcount = ds.Tables[0].AsEnumerable().Sum(x => x.Field<int>("cnt"));

        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();

        //Distinct Countries
        List<object> countries = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();

        foreach (string country in countries)
        {
            Loclist.Add(country);

            List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[2])).ToList();
            listLocCount.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CNY_NAME") == country)).Sum(x => x.Field<int>("cnt")));

            //Adding Locations 
            foreach (string location in locs)
            {
                Loclist.Add(location);
            }

            //Adding WS
            foreach (int loccount in ReqCount)
            {
                listLocCount.Add(loccount);
            }
        }
        Loclist.Insert(0, "x");
        listLocCount.Insert(0, "Total Seats");

        //Adding Locations/WS-Count to parent List
        listParent.Add(Loclist);
        listParent.Add(listLocCount);
        return new { ws = listParent, ttlcount = totalcount };
    }

    public object bindCabin(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_VACANT_DASHBOARD_COUNT");
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int32);
        ds = sp.GetDataSet();
        int totalcount = ds.Tables[0].AsEnumerable().Sum(x => x.Field<int>("FTE"));

        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();

        //Distinct Countries
        List<object> countries = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();

        foreach (string country in countries)
        {
            Loclist.Add(country);
            //Loclist.Add(".");

            List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[2])).ToList();
            listLocCount.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CNY_NAME") == country)).Sum(x => x.Field<int>("FTE")));

            //Adding Locations 
            foreach (string location in locs)
            {
                Loclist.Add(location);
                //Loclist.Add(".");
            }

            //Adding Cabins
            foreach (int loccount in ReqCount)
            {
                listLocCount.Add(loccount);
            }
        }
        Loclist.Insert(0, "x");
        listLocCount.Insert(0, "Total Vacant Seats");

        //Adding Locations/Cabins-Count to parent List
        listParent.Add(Loclist);
        listParent.Add(listLocCount);
        return new { cabins = listParent, ttlcabinscount = totalcount };
    }

    public object bindFTE(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_FTE_REPORT");
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();

        //Total WS Count
        int totalcount = ds.Tables[0].AsEnumerable().Sum(x => int.Parse(x.Field<string>("FTE")));

        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();

        //Distinct Countries
        List<object> countries = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[2]).Distinct().ToList();

        foreach (string country in countries)
        {
            Loclist.Add(country);
            //Loclist.Add(".");

            List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[0])).ToList();
            //List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("CNY_NAME") == country)).Select(r => (r.ItemArray[1])).ToList();
            listLocCount.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CNY_NAME") == country)).Sum(x => int.Parse(x.Field<string>("FTE"))));

            //Adding Locations 
            foreach (string location in locs)
            {
                Loclist.Add(location);
                //Loclist.Add(".");
            }

            //Adding FTE
            foreach (string loccount in ReqCount)
            {
                listLocCount.Add(loccount);
            }
        }
        Loclist.Insert(0, "x");
        listLocCount.Insert(0, "Total Occupied Employee(s)");

        //Adding Locations/FTE-Count to parent List
        listParent.Add(Loclist);
        listParent.Add(listLocCount);
        return new { FTE = listParent, ttlFTE = totalcount };
    }

    public object bindShift(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "NP_GET_DASHBOARD_SHIFT_REPORT");
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();

        //Total WS Count
        string totalcount = Convert.ToString(ds.Tables[0].Rows[0]["TOT"]);

        List<object> Loclist = new List<object>();
        List<object> listLocCount = new List<object>();
        List<object> listParent = new List<object>();


        Loclist = ds.Tables[1].AsEnumerable().Select(r => r.ItemArray[0]).ToList();
        var cnylst = ds.Tables[1].AsEnumerable().Select(r => r.ItemArray[2]).Distinct().FirstOrDefault().ToString();

        var resultList = Loclist.Select(x => string.Format("{0}", x)).ToList();

        listLocCount = ds.Tables[1].AsEnumerable().Select(r => r.ItemArray[1]).ToList();

        resultList.Insert(0, "x");
        resultList.Insert(1, cnylst);
        listLocCount.Insert(0, "Seat Utilization(Occupied/Total Seats) ");
        listLocCount.Insert(1, totalcount);

        listParent.Add(resultList);
        listLocCount.Add(totalcount);
        listParent.Add(listLocCount);

        return new { shift = listParent, ttlshift = totalcount };
    }

    public object bindDepartmentCount(int company, string vertical)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "SMS_DEPARTMENT_COUNT");
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        sp.Command.AddParameter("@VERTICAL", vertical, DbType.String);
        ds = sp.GetDataSet();
        return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] } } };
        //List<Object> list = new List<object>();
        //list.Add(ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray());
        //list.Add(ds.Tables[1].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray());
        //return list;
    }

    public object GetVerticals()
    {
        List<Verticallst> verlst = new List<Verticallst>();
        Verticallst ver;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Vertical_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", 3, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                ver = new Verticallst();
                ver.VER_CODE = sdr["VER_CODE"].ToString();
                ver.VER_NAME = sdr["VER_NAME"].ToString();
                ver.ticked = false;
                verlst.Add(ver);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getLocations()
    {
        List<Locationlst> Loclst = new List<Locationlst>();
        Locationlst Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM_all");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", 1, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new Locationlst();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.ticked = false;
                Loclst.Add(Loc);
            }
        }
        if (Loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getCities()
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CITY_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", 1, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object getCostCenters()
    {
        List<Costcenterlst> Cst_lst = new List<Costcenterlst>();
        Costcenterlst CST;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_COSTCENTERS_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", 1, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CST = new Costcenterlst();
                CST.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                CST.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                CST.Vertical_Code = sdr["Vertical_Code"].ToString();
                CST.ticked = false;
                Cst_lst.Add(CST);
            }
        }
        if (Cst_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Cst_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Get_Space_Utilization(string company, int valu, string type, int flg,string citylist, string LocationLst, string VerticalLst, string CostCenterLst)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_UTILIZATION");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", company, DbType.String);
            sp.Command.AddParameter("@TYPE", type, DbType.String);
            sp.Command.AddParameter("@VALUE", valu, DbType.Int32);
            sp.Command.AddParameter("@FLAG", flg, DbType.Int32);
            sp.Command.AddParameter("@CITY", citylist, DbType.String);
            sp.Command.AddParameter("@LOCATION", LocationLst, DbType.String);
            sp.Command.AddParameter("@VERTICAL", VerticalLst, DbType.String);
            sp.Command.AddParameter("@COSTCENTER", CostCenterLst, DbType.String);
            ds = sp.GetDataSet();

            List<object> Util = new List<object>();
            List<object> cities = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[1]).Distinct().ToList();

            List<object> Dynamiccols = new List<object>();
            var random = new Random();

            foreach (var i in cities)
            {
                Util.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CTY_NAME") == i)).Sum(x => decimal.Parse(x.Field<string>("UTILIZATION"))));
                Dynamiccols.Add(String.Format("#{0:X6}", random.Next(0x1000000)));
            }
            cities.Insert(0, "y");
            Util.Insert(0, "Seat Utilization");
            return new { Citydata = cities, Utilization = Util, Dynamiccols = Dynamiccols };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object Get_City_Wise_UnusedArea(string company, int valu, string type, int flg)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_CTY_UNUSED_AREA_COUNT");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", company, DbType.String);
            sp.Command.AddParameter("@TYPE", type, DbType.String);
            sp.Command.AddParameter("@VALUE", valu, DbType.Int32);
            sp.Command.AddParameter("@FLAG", flg, DbType.Int32);
            ds = sp.GetDataSet();

            List<object> UNUSED = new List<object>();
            List<object> UNUSERAREA = new List<object>();
            List<object> cities = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();
            object obj = ds.Tables[0].Rows[0]["CTYDT"].ToString();
            // List<object> Dynamiccols = new List<object>();
            // var random = new Random();

            foreach (var i in cities)
            {
                UNUSED.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CTY_NAME") == i)).Sum(x => int.Parse(x.Field<string>("UNUSED"))));
                UNUSERAREA.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CTY_NAME") == i)).Sum(x => decimal.Parse(x.Field<string>("UNUSERAREA"))));
                // Dynamiccols.Add(String.Format("#{0:X6}", random.Next(0x1000000)));
            }
            cities.Insert(0, "x");
            UNUSED.Insert(0, "Unused Space Count");
            UNUSERAREA.Insert(0, "Unused Space Area In Sq.ft");
            return new { Citydata = cities, UNUSED = UNUSED, UNUSERAREA = UNUSERAREA, obj = obj };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object Get_Vertical_Wise_Allocated_But_Not_Occupied(string company, int valu, string type, int flg)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_ALLOCATED_BUT_NOT_OCCUPIED");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", company, DbType.String);
            sp.Command.AddParameter("@TYPE", type, DbType.String);
            sp.Command.AddParameter("@VALUE", valu, DbType.Int32);
            sp.Command.AddParameter("@FLAG", flg, DbType.Int32);
            ds = sp.GetDataSet();

            List<object> Allocated = new List<object>();
            List<object> UNUSEDAREA = new List<object>();
            List<object> cities = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();
            object obj = ds.Tables[0].Rows[0]["CTYDT"].ToString();
            // List<object> Dynamiccols = new List<object>();
            // var random = new Random();

            foreach (var i in cities)
            {
                Allocated.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CTY_NAME") == i)).Sum(x => int.Parse(x.Field<string>("ALLOCATED"))));
                UNUSEDAREA.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("CTY_NAME") == i)).Sum(x => decimal.Parse(x.Field<string>("SPC_AREA"))));
                //  Dynamiccols.Add(String.Format("#{0:X6}", random.Next(0x1000000)));
            }
            cities.Insert(0, "x");
            Allocated.Insert(0, "Unoccupied Space Count");
            UNUSEDAREA.Insert(0, "Unoccupied Space Cost");
            return new { Citydata = cities, UNUSED = Allocated, UNUSERAREA = UNUSEDAREA, obj = obj };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object Get_Space_Release_Details(int companyid)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SPACE_RELEASE_DETAILS");
        sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", companyid, DbType.Int32);
        ds = sp.GetDataSet();
        return ds;
    }

    public object Get_Space_Release_Details_Onclick(string companyid, int value)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SPACE_RELEASE_DETAILS_ONCLICK");
        sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", companyid, DbType.Int32);
        sp.Command.AddParameter("@VALUE", value, DbType.Int32);
        ds = sp.GetDataSet();
        return ds;
    }

    public object Get_Space_Unused_Details_Onclick(string selectedval, string TYPE, int pagenum, int pagesize, string _Excel)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_UNUSED_DETAILS_ONCLICK");
        sp.Command.AddParameter("@selectedval", selectedval, DbType.String);
        sp.Command.AddParameter("@flag", TYPE, DbType.String);
        sp.Command.AddParameter("@PAGENUMBER", pagenum, DbType.Int32);
        sp.Command.AddParameter("@PAGESIZE", pagesize, DbType.Int32);
        sp.Command.AddParameter("@EXCELDATA", _Excel, DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }

    public object Get_City_Wise_Ver_Onclick(string selectedval, string TYPE, int pagenum, int pagesize, string _Excel)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CITY_WISE_VER_ONCLICK");
        sp.Command.AddParameter("@SELECTEDVAL", selectedval, DbType.String);
        sp.Command.AddParameter("@flag", TYPE, DbType.String);
        sp.Command.AddParameter("@PAGENUMBER", pagenum, DbType.Int32);
        sp.Command.AddParameter("@PAGESIZE", pagesize, DbType.Int32);
        sp.Command.AddParameter("@EXCELDATA", _Excel, DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }

    public List<GetExceletails> GetExcelData(string selectedval, string TYPE, int pagenum, int pagesize, string _Excel, int flag)
    {
        GetExceletails obj;
        List<GetExceletails> objlst = new List<GetExceletails>();

        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@SELECTEDVAL", SqlDbType.NVarChar);
            param[0].Value = selectedval;
            param[1] = new SqlParameter("@FLAG", SqlDbType.NVarChar);
            param[1].Value = TYPE;
            param[2] = new SqlParameter("@PAGENUMBER", SqlDbType.Int);
            param[2].Value = pagenum;
            param[3] = new SqlParameter("@PAGESIZE", SqlDbType.Int);
            param[3].Value = pagesize;
            param[4] = new SqlParameter("@EXCELDATA", SqlDbType.NVarChar);
            param[4].Value = _Excel;

            string proced;

            if (flag == 1)
                proced = "GET_UNUSED_DETAILS_ONCLICK";
            else
                proced = "GET_CITY_WISE_VER_ONCLICK";

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, proced, param))
            {
                while (reader.Read())
                {
                    obj = new GetExceletails();
                    obj.SPC_ID = reader["SPC_ID"].ToString();
                    obj.FLR_NAME = reader["FLR_NAME"].ToString();
                    obj.TWR_NAME = reader["TWR_NAME"].ToString();
                    obj.LCM_NAME = reader["LCM_NAME"].ToString();
                    obj.CTY_NAME = reader["CTY_NAME"].ToString();
                    obj.SPC_TYPE = reader["SPC_TYPE"].ToString();
                    obj.VER_NAME = reader["VER_NAME"].ToString();
                    obj.COST_NAME = reader["COST_NAME"].ToString();
                    objlst.Add(obj);
                }
                reader.Close();
            }

            if (objlst.Count != 0)
                return objlst;
            else
                return null;
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}
