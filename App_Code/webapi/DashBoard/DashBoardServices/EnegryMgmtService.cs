﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for EnegryMgmtService
/// </summary>
public class EnegryMgmtService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    SqlDataReader sdr;

    public object GetWaterCount( int id)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "EM_GET_WATER_COUNT_GRAPH");
        sp.Command.AddParameter("@COMPANYID", id, DbType.Int32);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetElectricalCount(int id)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "EM_GET_ELECTRICAL_COUNT_GRAPH");
        sp.Command.AddParameter("@COMPANYID", id, DbType.Int32);
        ds = sp.GetDataSet();
        //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return ds.Tables[0];
    }

    public object GetSolarCount(int id)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "EM_GET_SOLAR_COUNT_GRAPH");
        sp.Command.AddParameter("@COMPANYID", id, DbType.Int32);
        ds = sp.GetDataSet();
        //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return ds.Tables[0];
    }

    public object GetFuelCount(int id)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "EM_GET_FUEL_COUNT_GRAPH");
        sp.Command.AddParameter("@COMPANYID", id, DbType.Int32);
        ds = sp.GetDataSet();
        //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return ds.Tables[0];
    }

    public object GetGasCount(int id)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "EM_GET_GAS_COUNT_GRAPH");
        sp.Command.AddParameter("@COMPANYID", id, DbType.Int32);
        ds = sp.GetDataSet();
        //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return ds.Tables[0];
    }
}