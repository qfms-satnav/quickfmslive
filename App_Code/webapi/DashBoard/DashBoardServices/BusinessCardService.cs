﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


public class BusinessCardService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public object[] GetVendorReqCount(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "BC_VENDORREQ_COUNT");
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }


    public object[] GetStaCount(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "BC_STA_COUNT");
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }
}