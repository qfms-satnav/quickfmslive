﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MaintenanceDBModuleService
/// </summary>
public class MaintenanceDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    //Category-Wise Assets Count
    public object[] BindAMCTypes(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETAMCTYPES_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
       //object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => new { Name = r.ItemArray[1], total = r.ItemArray[0] }).ToArray();
        return arr;       
    }
    //pop up --Category-Wise Assets Count
    public DataTable GetAssetsByCategory(string category, int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETAMCTYPES_DASHBOARD_ON_PIE_CLICK");
        sp.Command.AddParameter("@CATEGORY", category, DbType.String);
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    // amount recieved this month
    public DataTable GetAMCAmounts(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETPAYMENT_ADVICE");
        sp.Command.AddParameter("@TYPE", "1", DbType.String);
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    //AMCs Expiring this month
    public DataTable GetAMCAssetsExp(int comapany)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_AMC_EXPIRYASSETS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", comapany, DbType.Int16);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    //Amc and Non AMC pie
    public object BindAMCNonAMCBarCh(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_AMC_NONAMC_BYLOC_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();               
        
        ////Locations
        ////object Locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[0].ToString()) ? r.ItemArray[1].ToString().Insert(0, "'").Insert(r.ItemArray[1].ToString().Length + 1, "'") : r.ItemArray[0].ToString().Insert(0, "'").Insert(r.ItemArray[0].ToString().Length + 1, "'"))).ToArray();        
        //List<object> Locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[0].ToString()) ? r.ItemArray[1] : r.ItemArray[0])).ToList();
        //Locs.Insert(0, "x");
        ////AMCS
        //List<object> AMCs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[2].ToString()) ? 0 : r.ItemArray[2])).ToList();
        //AMCs.Insert(0, "AMCS");
        ////NONAMCS
        //List<object> NonAMCs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[3].ToString()) ? 0 : r.ItemArray[3])).ToList();
        //NonAMCs.Insert(0, "NONAMCS");

        //List<Object> list = new List<object>();
        //list.Add(Locs);
        //list.Add(AMCs);
        //list.Add(NonAMCs);

        List<Object> list = new List<object>();
        list.Add(ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray());
        list.Add(ds.Tables[1].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray());
        return list;
    }
    // popup -- Amc and Non AMC pie
    public DataTable GetAssetsByCategoryLocationAndAmcorNonAMC(string location, string category, string company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_ASSETS_BY_CATEGORY_AND_LOCATION");
        sp.Command.AddParameter("@CATEGORY", category, DbType.String);
        sp.Command.AddParameter("@LOC", location, DbType.String);
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    //Total Work Orders 
    public object GetWorkRequests(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_WORKORDERS_COUNT_DASHBOARD");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();
        return ds;
        //return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] }, new { WRExpenses = ds.Tables[0].Rows[2]["Total"] } } };
    }
    //popup- Total Work Orders 
    public object GetWorkOrdersDetails(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_WORKORDERS_COUNT_DETAILS");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();
        return ds;
        //return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] }, new { WRExpenses = ds.Tables[0].Rows[2]["Total"] } } };
    }

    public object GetAssetsUnderAMC_and_PrvntMain(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_ASSETS_UNDER_AMC_PRVNT_MAINT");
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@COMPANYID", company, DbType.Int16);
        ds = sp.GetDataSet();
        return ds;
        //return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] }, new { WRExpenses = ds.Tables[0].Rows[2]["Total"] } } };
    }

    public DataSet GetAMCAssets()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GETAMCTYPES_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }
  
}