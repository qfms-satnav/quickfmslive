﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class HelpDeskDBAPIController : ApiController
{
    HelpDeskDBModuleService service = new HelpDeskDBModuleService();
    // GET api/<controller>
    public IEnumerable<string> Get()
    {
        return new string[] { "value1", "value2" };
    }

    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }

    //Req raised for last 7 days
    [HttpGet]
    public object GetHDreqWeekly(int ID)
    {
       return service.GetHDreqWeekly(ID);
    }
    
    [HttpGet]
    public HelpDeskDBModel.SLADetails GetSLACount(int id)
    {
        return service.GetSLACount(id);
    }

    [HttpGet]
    public object GetStatuswiseData()
    {
        return service.GetStatuswiseData();
    }
    //Location-Wise Pending Services
    [HttpGet]
    public object BindHDCategories(int id)
    {
        return service.BindHDCategories(id);
    }

    [HttpGet]
    public object GetRequestDetails(int id)
    {
        return service.GetRequestDetails(id);
    }

    [HttpGet]
    public HttpResponseMessage GetUserReqs_StatusWise([FromUri] string Statusid)
    {
        var obj = service.GetUserReqs_StatusWise(Statusid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetRequestDetailsbySelectedServiceAndLocation([FromBody] parameters p)
    {
        var obj = service.GetRequestDetailsbySelectedServiceAndLocation(p.location, p.category, p.company);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    public DataTable BindSupport()
    {
        return service.BindSupport();

    }

    public class parameters
    {
        public string location { get; set; }
        public string category { get; set; }
        public int company { get; set; }
    }
}
