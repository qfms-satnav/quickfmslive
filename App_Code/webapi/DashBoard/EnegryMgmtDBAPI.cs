﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;


public class EnegryMgmtDBAPIController : ApiController
{
    EnegryMgmtService service = new EnegryMgmtService();

    [HttpGet]
    public object GetWaterCount(int id)
    {
        return service.GetWaterCount(id);
    }

    [HttpGet]
    public object GetElectricalCount(int id)
    {
        return service.GetElectricalCount(id);
    }

    [HttpGet]
    public object GetFuelCount(int id)
    {
        return service.GetFuelCount(id);
    }

    [HttpGet]
    public object GetGasCount(int id)
    {
        return service.GetGasCount(id);
    }

    [HttpGet]
    public object GetSolarCount(int id)
    {
        return service.GetSolarCount(id);
    }
}
