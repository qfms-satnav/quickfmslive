﻿using System;
using System.Data;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for Validatesparesrvices
/// </summary>
public class Validatesparesrvices
{
    SubSonic.StoredProcedure sp;
    public object GetData(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_AMG_VAL_SPR]");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@lcm_code", CustRpt.LCM_NAME, DbType.String);
            sp.Command.Parameters.Add("@SEARCHVAL", CustRpt.SearchValue, DbType.String);
            sp.Command.Parameters.Add("@PAGENUM", CustRpt.PageNumber, DbType.Int32);
            sp.Command.Parameters.Add("@PAGESIZE", CustRpt.PageSize, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    //public object GetAssetDetails(CustomizableRptVM CustRpt)
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS");
    //        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
    //        ds = sp.GetDataSet();
    //        if (ds.Tables.Count != 0)
    //            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
    //        else
    //            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    //    }
    //    catch (Exception e)
    //    {
    //        throw e;
    //    }

    //}

    public object GetAssetDetails(CustomizableRptVM CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "AM_USP_GET_ASSETSPAREPARTS_LCM");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object SearchData(CustomizableRpt CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[Search_SpareParts]");
            sp.Command.AddParameter("@LCM_NAME", CustRpt.LCM_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public object GetValidateDetails(CustomizableRptVM CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_VALD_SPAREPARTSS");
            sp.Command.Parameters.Add("@ASP_REQ_ID_DTLS", CustRpt.ASP_REQ_ID_DTLS, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public object GetUpdateStatus(ValidateSSpare CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[UPDATE_SPAREPARTS_STATUS]");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@REMARKS", CustRpt.REMARKS, DbType.String);
            sp.Command.Parameters.Add("@TYPE", CustRpt.TYPE, DbType.String);
            sp.Command.Parameters.Add("@REQ_ID", CustRpt.REQ_ID, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
}