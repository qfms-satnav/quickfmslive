﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ValidatespareController : ApiController


{
    Validatesparesrvices service = new Validatesparesrvices();
    [HttpPost]
    public HttpResponseMessage GetData(CustomizableRptVM CustRpt)
    {
        var obj = service.GetData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]

    public HttpResponseMessage GetAssetDetails(CustomizableRptVM CustRpt)
    {
        var obj = service.GetAssetDetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage SearchData(CustomizableRpt CustRpt)
    {
        var obj = service.SearchData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetValidateDetails(CustomizableRptVM CustRpt)
    {
        object obj = service.GetValidateDetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetUpdateStatus(ValidateSSpare CustRpt)
    {
        object obj = service.GetUpdateStatus(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}