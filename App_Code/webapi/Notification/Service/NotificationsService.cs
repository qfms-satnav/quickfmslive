﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using UtiltiyVM;


public class NotificationsService
{
    SubSonic.StoredProcedure sp;
    string ReqId, FPath,fileName;
    //string Pth = "D:/Bitbucket/T-HUB/t-hub_uat/UploadFiles/fileUpload/";
    //string Pth = "E:/Bitbucket/T-HUB/t-hub_uat/UploadFiles/fileUpload/";
    static string Tenant = HttpContext.Current.Session["TENANT"].ToString();
    //string Pth = "~/UploadFiles/" + Tenant + "/NotificationUploads/";
    string Pth = HttpContext.Current.Server.MapPath("~/UploadFiles/" + Tenant + "/NotificationUploads/");

    public object Submit(Notification notification)
    {
        try
        {
            ReqId = Convert.ToString(DateTime.Now.Year) + DateTime.Now.ToString("ddMMyyyyHHmmss");
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@RequestId", SqlDbType.VarChar)
            {
                Value = ReqId
            };
            param[1] = new SqlParameter("@Role", SqlDbType.VarChar)
            {
                Value = notification.Role
            };
            param[2] = new SqlParameter("@Subject", SqlDbType.VarChar)
            {
                Value = notification.Subject
            };
            param[3] = new SqlParameter("@Documents", SqlDbType.VarChar)
            {
                Value = notification.Documents
            };
            param[4] = new SqlParameter("@Discription", SqlDbType.VarChar)
            {
                Value = notification.Discription
            };
            param[5] = new SqlParameter("@Status", SqlDbType.VarChar)
            {
                Value = notification.Status
            };
            param[6] = new SqlParameter("@Createdby", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };
            param[7] = new SqlParameter("@Path", SqlDbType.VarChar)
            {
                Value = Pth
            };

            object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SET_NOTIFICATION_DATA", param);
            int sem_id = (int)o;
            string msg;
            if (sem_id == 1)
            {
                if (SendMsg(ReqId))
                {
                    UpdateStatus(ReqId);
                    msg = "Details are Submited Successfully";
                }
                else msg = "Error";
            }
            else if(sem_id == 0)
            {
                msg = "Employee is not assigned with the selected role";
            }
            else
                msg = "Something Went Wrong";
            DeleteFile(FPath);
            return new { data = msg, Message = msg };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }

    }
    protected void DeleteFile(string filePath)
    {
        try
        {
            if (File.Exists(filePath))
            {
                // Delete the file
                File.Delete(filePath);               
            }
            
        }
        catch (Exception ex)
        {
            
        }
    }
    public void UpdateStatus(string id)
    {
        var aurId = HttpContext.Current.Session["UID"].ToString();       
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "Update_Mail_Status");     
        sp.Command.Parameters.Add("@AUR_ID", aurId, DbType.String);
        sp.Command.Parameters.Add("@REQID", id, DbType.String);       
        sp.ExecuteScalar();
    }

    public object GetRole()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GetRoleDetails");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = "", data = ds.Tables[0], };
            else
                return new { Message = "No Records", data = (object)null };
        }
        catch (Exception ex) { return new { Message = ex.Message }; }
    }


    public object GetNotificationData(Notification data)
    {
        try
        {
            DataSet ds = new DataSet();


            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "Get_Notification_Data");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);

            ds = sp.GetDataSet();

            if (ds.Tables.Count != 0)
                return new { Message = "", data = ds.Tables, };
            else
                return new { Message = "No Records", data = (object)null };
        }
        catch (Exception ex) { return new { Message = ex.Message }; }
    }

    public bool SendMsg(string id)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "Get_SMS_Mail_Data");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@REQID", id, DbType.String);
            ds = sp.GetDataSet();
            DataTable dataTable = ds.Tables[0];
            
            string sub = string.Empty;
            string discrip = string.Empty;
            string eml = string.Empty;
            FPath = string.Empty;

            if (dataTable.Rows.Count > 0)
            {
                DataRow row = dataTable.Rows[0];

                sub = row["SUBJECT"].ToString();
                discrip = row["DISCRIPTION"].ToString();
                eml = row["EMAIL"].ToString();
                string P = row["PATH"].ToString();
                FPath = P.Replace("\\", "/");
                int lastIndex = FPath.LastIndexOf("/");
                if (lastIndex != -1 && lastIndex < FPath.Length - 1)
                {
                    fileName = FPath.Substring(lastIndex + 1);
                }
            }

            using (SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["Host"], Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["Port"])))
            {
                smtp.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailid"].ToString(), System.Configuration.ConfigurationManager.AppSettings["password"].ToString());
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                using (MailMessage msg = new MailMessage())
                {
                    msg.Subject = sub;
                    msg.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["from"].ToString().Trim());
                    string[] Tomail = eml.Split(';'); //dataSet.Tables[5].Rows[0][0].ToString().Split(';');
                    foreach (string to in Tomail)
                    {
                        if (!string.IsNullOrEmpty(to))
                            msg.To.Add(new MailAddress(to.Trim()));
                    }
                    string[] Ccmail = "razvi@quickfms.com".Split(';'); //dataSet.Tables[5].Rows[0][1].ToString().Split(';');
                    foreach (string cc in Ccmail)
                    {
                        if (!string.IsNullOrEmpty(cc))
                            msg.CC.Add(new MailAddress(cc.Trim()));
                    }
                    msg.IsBodyHtml = true;
                    msg.Body = discrip;// +"<br><br><P>Regards,<br>T-Hub Team.</p>";//dataSet.Tables[4].Rows[0][0].ToString();
                    if (FPath != Pth.Replace("\\","/"))
                    {
                        Attachment attach = new Attachment(FPath);
                        attach.Name = fileName;
                        msg.Attachments.Add(attach);
                    }
                    smtp.Send(msg);
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            return false;
            //throw;

        }
    }

}