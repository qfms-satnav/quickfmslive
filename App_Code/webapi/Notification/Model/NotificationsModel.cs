﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;




public class Models
{
    public List<Notification> Notification { get; set; }
}
    public class Notification
{
    public string RequestId { get; set; }
    public string Role { get; set; }
    public string Subject { get; set; }
    public string Documents { get; set; }
    public string Discription { get; set; }
    public string Status { get; set; }
    public string Createdby { get; set; }

}
public class GetPDF
{
    public string pdfContent { get; set; }
    public string Mailcc { get; set; }
    public string MailTo { get; set; }
    public string MailSub { get; set; }
    public string Req_ID { get; set; }
    public string MailBody { get; set; }
}