﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

public class NotificationsController : ApiController
{
    NotificationsService ccs = new NotificationsService();
    AESEncrytDecryService service = new AESEncrytDecryService();
    //PDF_Service_Utility PDFSer = new PDF_Service_Utility();
    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;
        string sPath = "";
        string Tenant = HttpContext.Current.Session["TENANT"].ToString();
        var httpRequest = HttpContext.Current.Request;
        //var validateFile = FileValidator.ValidateUploadedFile(httpRequest.Files[0], ".xlsx,.xls,.pdf", 11000);
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/"+ Tenant + "/NotificationUploads/");
        if (!System.IO.Directory.Exists(sPath))
        {
            System.IO.Directory.CreateDirectory(sPath);
        }
        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                //hpf.SaveAs(sPath + Path.GetFileName( hpf.FileName.Replace(" ","")));
                hpf.SaveAs(sPath + System.IO.Path.GetFileName(hfc.Keys[iCnt].ToString()));
                iUploadedCnt = iUploadedCnt + 1;
            }
        }
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }
    [HttpGet]
    public HttpResponseMessage GetRole()
    {
        var obj = ccs.GetRole();
        HttpResponseMessage res = Request.CreateResponse(HttpStatusCode.OK, obj);
        return res;

    }
    [HttpPost]
    public HttpResponseMessage SubmitNotification(Notification data)
    {
        try
        {
            var obj = ccs.Submit(data);
            if (obj != null)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
                return response;
            }
            else
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid data");
                return response;
            }
        }
        catch (Exception ex)
        {
            // An exception occurred during processing
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.InternalServerError, "An error occurred: " + ex.Message);
            return response;
        }
    }

    [HttpPost]
    public HttpResponseMessage GetNotificationData(Notification data)
    {
        var obj = ccs.GetNotificationData(data);
        HttpResponseMessage res = Request.CreateResponse(HttpStatusCode.OK, obj);
        return res;

    }
}