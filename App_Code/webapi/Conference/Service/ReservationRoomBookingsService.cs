﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using UtiltiyVM;
using System.Data.SqlClient;


/// <summary>
/// Summary description for ReservationRoomBookingsService
/// </summary>
public class ReservationRoomBookingsService
{

    SubSonic.StoredProcedure sp;
    DataSet ds;
    List<ReservationRoomBookingsModel> ConfCustomList;
    ReservationRoomBookingsModel Custm;




    public object GetReservationRoomBookings(ReservationRoomBookingsModel RRB)
    {
        try
        {
            var lst = GetReservationRoomBookingsList(RRB);
            if (lst != null)
            {
                return new { Message = MessagesVM.UM_NO_REC, data = lst };
            }
            else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public object GetReservationRoomBookingsList(ReservationRoomBookingsModel det)
    {
        try
        {

            List<ReservationRoomBookingsModel> CData = new List<ReservationRoomBookingsModel>();
            DataTable DT = new DataTable();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();
            SqlParameter[] param = new SqlParameter[2];
            
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);

            if (det.flrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(det.flrlst);
            }
           
              
            param[1] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
            param[1].Value = det.FromDate.ToString();
           

            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_RESERVATION_ROOM_BOOKINGS", param);

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcols.Add(gridcolscls);

                exportcls = new ExportClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
        }
        catch (Exception Ex)
        {
            var err = Ex.Message;
            throw;
        }
    }
}