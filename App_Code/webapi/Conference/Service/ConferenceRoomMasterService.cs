﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class ConferenceRoomMasterService
{

    public object GetConferenceDetails()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CONFERENCE_DATA").GetReader())
        {
            try
            {
                List<ConferenceType> Conflist = new List<ConferenceType>();
                while (reader.Read())
                {
                    Conflist.Add(new ConferenceType()
                    {
                        CONF_CODE = reader.GetValue(0).ToString(),
                        CONF_NAME = reader.GetValue(1).ToString(),
                    });
                }
                reader.Close();
                return Conflist;
            }
            catch
            {
                throw;
            }
        }
    }

    public object SaveConferenceRoomDetails(ConferenceLocationStructure ConfModel)
    {
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@CONF_TYPE", SqlDbType.NVarChar);
        param[0].Value = ConfModel.Model.CONFERENCE_TYPE;
        param[1] = new SqlParameter("@CONF_ROOM_CODE", SqlDbType.NVarChar);
        param[1].Value = ConfModel.Model.CONFERENCE_ROOM_CODE;
        param[2] = new SqlParameter("@CONF_ROOM_NAME", SqlDbType.NVarChar);
        param[2].Value = ConfModel.Model.CONFERENCE_ROOM_NAME;
        param[3] = new SqlParameter("@CONF_ROOM_CAPACITY", SqlDbType.NVarChar);
        param[3].Value = ConfModel.Model.CONFERENCE_CAPACITY;
        param[4] = new SqlParameter("@CONF_LOCATION_HIERARCHY", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(ConfModel.Structure);
        param[5] = new SqlParameter("@CONF_CREATED_BY", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["UID"];
        param[6] = new SqlParameter("@CONF_STAUTS", SqlDbType.Int);
        param[6].Value = ConfModel.Model.CONFERENCE_STATUS;
        param[7] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[7].Value = HttpContext.Current.Session["COMPANYID"];
        param[8] = new SqlParameter("@CONF_ROOM_COST", SqlDbType.NVarChar);
        param[8].Value = ConfModel.Model.CONFERENCE_COST;

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SAVE_CONFERENCE_ROOM_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                    return new { Message = MessagesVM.Facility_Success };
                else
                    return new { Message = MessagesVM.Facility_InsertError };
            }
        }
        return new { Message = MessagesVM.Facility_InsertError, data = (object)null };
    }

    public object ModifyConferenceRoomDetails(ConferenceLocationStructure ConfModel) 
    {
        try
        {
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@CONF_TYPE", SqlDbType.NVarChar);
            param[0].Value = ConfModel.Model.CONFERENCE_TYPE;
            param[1] = new SqlParameter("@CONF_ROOM_CODE", SqlDbType.NVarChar);
            param[1].Value = ConfModel.Model.CONFERENCE_ROOM_CODE;
            param[2] = new SqlParameter("@CONF_ROOM_NAME", SqlDbType.NVarChar);
            param[2].Value = ConfModel.Model.CONFERENCE_ROOM_NAME;
            param[3] = new SqlParameter("@CONF_ROOM_CAPACITY", SqlDbType.NVarChar);
            param[3].Value = ConfModel.Model.CONFERENCE_CAPACITY;
            param[4] = new SqlParameter("@CONF_LOCATION_HIERARCHY", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(ConfModel.Structure);
            param[5] = new SqlParameter("@CONF_UPDATED_BY", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["UID"];
            param[6] = new SqlParameter("@CONF_STAUTS", SqlDbType.Int);
            param[6].Value = ConfModel.Model.CONFERENCE_STATUS;
            param[7] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            param[8] = new SqlParameter("@CONF_ROOM_COST", SqlDbType.NVarChar);
            param[8].Value = ConfModel.Model.CONFERENCE_COST;
            SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MODIFY_CONFERENCE_ROOM_DETAILS", param);
            if(dr==null)
                return new { Message = MessagesVM.Facility_UpdateError };
            else
            return new { Message = MessagesVM.Facility_Modify };
        }
        catch 
        {
            throw;
        }
    }

    public object BindGridData() 
    {
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BIND_CONFERENCE_DETAILS"))
        {
            List<ConferenceRoomMasterModel> ConfRoomLst = new List<ConferenceRoomMasterModel>();
            while (reader.Read())
            {
                ConfRoomLst.Add(new ConferenceRoomMasterModel()
                {
                    CONFERENCE_TYPE = reader["CONFERENCE_TYPE"].ToString(),
                    CONFERENCE_TYPE_NAME = reader["CONF_NAME"].ToString(),
                    CONFERENCE_ROOM_CODE = reader["CONFERENCE_CODE"].ToString(),
                    CONFERENCE_ROOM_NAME = reader["CONFERENCE_NAME"].ToString(),
                    CONFERENCE_CITY_CODE = reader["CTY_CODE"].ToString(),
                    CONFERENCE_CITY = reader["CTY_NAME"].ToString(),
                    CONFERENCE_LOCATION_CODE = reader["LCM_CODE"].ToString(),
                    CONFERENCE_LOCATION = reader["LCM_NAME"].ToString(),
                    CONFERENCE_TOWER_CODE = reader["TWR_CODE"].ToString(),
                    CONFERENCE_TOWER = reader["TWR_NAME"].ToString(),
                    CONFERENCE_FLOOR_CODE = reader["FLR_CODE"].ToString(),
                    CONFERENCE_FLOOR = reader["FLR_NAME"].ToString(),
                    CONFERENCE_CAPACITY = Convert.ToInt32(reader["CONFERENCE_CAPACITY"].ToString()),
                    CONFERENCE_COST = Convert.ToInt32(reader["CONFERENCE_COST"].ToString()),
                    CONFERENCE_STATUS = reader["CONFERENCE_STATUS"].ToString()
                });
            }
            reader.Close();
            return ConfRoomLst;
        }
    }
}