﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ConferenceRoomBookingService
/// </summary>
public class ConferenceRoomBookingService
{
    public List<ConferenceRoomBookingModel> GetConferenceRooms(ConferenceBookingSearch confModel)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@CITYCODE", SqlDbType.VarChar)
            {
                Value = confModel.CityCode
            };
            param[1] = new SqlParameter("@LCMCODE", SqlDbType.VarChar)
            {
                Value = confModel.LocationCode
            };
            param[2] = new SqlParameter("@TWRCODE", SqlDbType.VarChar)
            {
                Value = confModel.TowerCode
            };
            param[3] = new SqlParameter("@FLRCODE", SqlDbType.VarChar)
            {
                Value = confModel.FloorCode
            };
            param[4] = new SqlParameter("@CAPACITY", SqlDbType.Int)
            {
                Value = confModel.Capacity
            };
            var dataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_GET_CONFERENCE_ROOM_DETAILS", param);
            if (dataTable != null && dataTable.Rows != null && dataTable.Rows.Count > 0)
            {
                var jsonString = JsonConvert.SerializeObject(dataTable);
                var result = JsonConvert.DeserializeObject<List<ConferenceRoomBookingModel>>(jsonString);
                if (result.Any())
                {
                    List<ConferenceIdsType> conferenceIdsTypes = new List<ConferenceIdsType>();
                    result.ForEach(x =>
                    {
                        ConferenceIdsType conferenceIdsType = new ConferenceIdsType
                        {
                            ConferenceId = x.CONFERENCE_CODE
                        };
                        conferenceIdsTypes.Add(conferenceIdsType);
                    });
                    var conferenceBookedTimings = GetConferenceRoomBookedTimings(new ConferenceTimingFilter { ConferenceDate = confModel.FromDate, ConferenceIdsType = conferenceIdsTypes });
                    if (conferenceBookedTimings.Any())
                    {
                        result.ForEach(y =>
                        {
                            if (conferenceBookedTimings.Any(x => x.ConferenceId == y.CONFERENCE_CODE))
                            {
                                y.BookedTimings = conferenceBookedTimings.Where(x => x.ConferenceId == y.CONFERENCE_CODE).ToList();
                            }
                        });
                    }
                }
                return result;
            }
            return new List<ConferenceRoomBookingModel>();
        }
        catch (Exception ex)
        {
            return new List<ConferenceRoomBookingModel>();
        }
    }

    public List<ConferenceTimings> GetConferenceRoomBookedTimings(ConferenceTimingFilter conferenceTimingFilter)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ConferenceDate", SqlDbType.DateTime)
            {
                Value = conferenceTimingFilter.ConferenceDate
            };
            param[1] = new SqlParameter("@ConferenceIdsType", SqlDbType.Structured)
            {
                Value = UtilityService.ConvertToDataTable(conferenceTimingFilter.ConferenceIdsType)
            };
            var dataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_GET_CONFERENCE_BOOKED_TIMINGS", param);
            if (dataTable != null && dataTable.Rows != null && dataTable.Rows.Count > 0)
            {
                var jsonString = JsonConvert.SerializeObject(dataTable);
                return JsonConvert.DeserializeObject<List<ConferenceTimings>>(jsonString);
            }
            return ConferenceBookingHours.BookedTimings;
        }
        catch (Exception ex)
        {
            return new List<ConferenceTimings>();
        }
    }

    public object SaveBookingRequest(ConferenceBookingVM conferenceBookingVM)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[23];
            param[0] = new SqlParameter("@RequestId", SqlDbType.VarChar)
            {
                Value = DateTime.Now.Year + "/" + HttpContext.Current.Session["UID"] + "/" + DateTime.Now.ToString("dd/MM/yyyy/HH/mm/ss")
            };
            param[1] = new SqlParameter("@CcId", SqlDbType.VarChar)
            {
                Value = DateTime.Now.Year + "/Conference/" + DateTime.Now.ToString("dd/MM/yyyy/HH/mm/ss")
            };
            param[2] = new SqlParameter("@VerticalId", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.VerticalId
            };
            param[3] = new SqlParameter("@VerticalCode", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.VerticalCode
            };
            param[4] = new SqlParameter("@LocationCode", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.LocationCode
            };
            param[5] = new SqlParameter("@TowerCode", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.TowerCode
            };
            param[6] = new SqlParameter("@FloorCode", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.FloorCode
            };
            param[7] = new SqlParameter("@SpaceType", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.SpaceType
            };
            param[8] = new SqlParameter("@SpaceSubType", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.SpaceSubType
            };
            param[9] = new SqlParameter("@ConferenceCode", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.ConferenceCode
            };
            param[10] = new SqlParameter("@StatusId", SqlDbType.Int)
            {
                Value = conferenceBookingVM.StatusId
            };
            param[11] = new SqlParameter("@AllocationId", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.AllocationId
            };
            param[12] = new SqlParameter("@FromDate", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.FromDate
            };
            param[13] = new SqlParameter("@ToDate", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.ToDate
            };
            param[14] = new SqlParameter("@FromTime", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.FromTime
            };
            param[15] = new SqlParameter("@EndTime", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.EndTime
            };
            param[16] = new SqlParameter("@Description", SqlDbType.NVarChar)
            {
                Value = conferenceBookingVM.Description
            };
            param[17] = new SqlParameter("@InternalAttendes", SqlDbType.NVarChar)
            {
                Value = !string.IsNullOrEmpty(conferenceBookingVM.InternalAttendes) ? conferenceBookingVM.InternalAttendes.Replace(";", ",") : ""
            };
            param[18] = new SqlParameter("@ExternalAttendes", SqlDbType.NVarChar)
            {
                Value = !string.IsNullOrEmpty(conferenceBookingVM.ExternalAttendes) ? conferenceBookingVM.ExternalAttendes.Replace(";", ",") : ""
            };
            param[19] = new SqlParameter("@UserId", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };
            param[20] = new SqlParameter("@CompanyId", SqlDbType.Int)
            {
                Value = 1
            };
            param[21] = new SqlParameter("@BehalfUser", SqlDbType.VarChar)
            {
                Value = !string.IsNullOrEmpty(conferenceBookingVM.BehalfUser) ? conferenceBookingVM.BehalfUser : HttpContext.Current.Session["UID"]
            };
            param[22] = new SqlParameter("@Cost", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.Cost
            };
            DataTable result = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_RAISE_BOOKING_REQUEST", param);
            if ((int)result.Rows[0][0] == 0)
            {
                return new { Status = 0, Message = result.Rows[0][1] };
            }
            else
            {
                return GetConferenceRoomBookedTimings(new ConferenceTimingFilter { ConferenceDate = conferenceBookingVM.FromDate, ConferenceIdsType = new List<ConferenceIdsType> { new ConferenceIdsType { ConferenceId = conferenceBookingVM.ConferenceCode } } });

            }
        }
        catch (Exception ex)
        {
            return new List<ConferenceTimings>();
        }
    }

    public List<ConferenceTimings> DeleteBookingRequest(ConferenceBookingCancelModel conferenceBookingCancelModel)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BookingId", SqlDbType.NVarChar)
            {
                Value = conferenceBookingCancelModel.BookingId
            };
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "CONF_DELETE_BOOKING_REQUEST", param);
            var result = GetConferenceRoomBookedTimings(new ConferenceTimingFilter { ConferenceDate = conferenceBookingCancelModel.BookingDate, ConferenceIdsType = new List<ConferenceIdsType> { new ConferenceIdsType { ConferenceId = conferenceBookingCancelModel.ConferenceId } } });
            result.ForEach(x => x.ConferenceId = conferenceBookingCancelModel.ConferenceId);
            return result;
        }
        catch (Exception ex)
        {
            return new List<ConferenceTimings>();
        }
    }

    public DataTable GetInternalAttendess(string searchText)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SEARCH_CRITERIA", SqlDbType.NVarChar)
            {
                Value = searchText
            };
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_GET_INT_ATTENDESS_EMAILS", param);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetAttendessByConferenceId(string conferenceReqId)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ConferenceReqId", SqlDbType.NVarChar)
            {
                Value = conferenceReqId
            };
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_GET_ATTENDESS_BY_CONF_ID", param);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public object ModifyBookingRequest(ConferenceBookingVM conferenceBookingVM)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@RequestId", SqlDbType.VarChar)
            {
                Value = conferenceBookingVM.RequestId
            };
            param[1] = new SqlParameter("@ToDate", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.ToDate
            };
            param[2] = new SqlParameter("@FromTime", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.FromTime
            };
            param[3] = new SqlParameter("@EndTime", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.EndTime
            };
            param[4] = new SqlParameter("@Description", SqlDbType.NVarChar)
            {
                Value = conferenceBookingVM.Description
            };
            param[5] = new SqlParameter("@InternalAttendes", SqlDbType.NVarChar)
            {
                Value = !string.IsNullOrEmpty(conferenceBookingVM.InternalAttendes) ? conferenceBookingVM.InternalAttendes.Replace(";", ",").Replace(" ", "") : ""
            };
            param[6] = new SqlParameter("@ExternalAttendes", SqlDbType.NVarChar)
            {
                Value = !string.IsNullOrEmpty(conferenceBookingVM.ExternalAttendes) ? conferenceBookingVM.ExternalAttendes.Replace(";", ",").Replace(" ", "") : ""
            };
            param[7] = new SqlParameter("@UserId", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };
            param[8] = new SqlParameter("@BehalfUser", SqlDbType.VarChar)
            {
                Value = !string.IsNullOrEmpty(conferenceBookingVM.BehalfUser) ? conferenceBookingVM.BehalfUser : HttpContext.Current.Session["UID"]
            };
            param[9] = new SqlParameter("@FromDate", SqlDbType.DateTime)
            {
                Value = conferenceBookingVM.FromDate
            };
            DataTable result = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_UPDATE_BOOKING_REQUEST", param);
            if ((int)result.Rows[0][0] == 0)
            {
                return new { Status = 0, Message = result.Rows[0][1] };
            }
            else
            {
                var result1 = GetConferenceRoomBookedTimings(new ConferenceTimingFilter { ConferenceDate = conferenceBookingVM.SelectedDate, ConferenceIdsType = new List<ConferenceIdsType> { new ConferenceIdsType { ConferenceId = conferenceBookingVM.ConferenceCode } } });
                result1.ForEach(x => x.ConferenceId = conferenceBookingVM.ConferenceCode);
                return result1;
            }
        }
        catch (Exception ex)
        {
            return new List<ConferenceTimings>();
        }
    }
    public DataTable GetConferenceBehalfUsers(string searchText)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SEARCH_CRITERIA", SqlDbType.NVarChar)
            {
                Value = searchText
            };
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_GET_BEHALF_USERS", param);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public bool IsEmployee()
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };
            var result = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CONF_IS_EMPLOYEE", param);
            return result != null && Convert.ToInt32(result) == 1;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool IsSlotBooked(ConferenceSlotVM conferenceSlotVM)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@StartHour", SqlDbType.Int)
            {
                Value = conferenceSlotVM.StartHour
            };
            param[1] = new SqlParameter("@StartMinutes", SqlDbType.Int)
            {
                Value = conferenceSlotVM.StartMinutes
            };
            param[2] = new SqlParameter("@EndHour", SqlDbType.Int)
            {
                Value = conferenceSlotVM.EndHour
            };
            param[3] = new SqlParameter("@EndMinutes", SqlDbType.Int)
            {
                Value = conferenceSlotVM.EndMinutes
            };
            param[4] = new SqlParameter("@SSA_FROM_DATE", SqlDbType.DateTime)
            {
                Value = conferenceSlotVM.FromDate
            };
            param[5] = new SqlParameter("@SSA_TO_DATE", SqlDbType.DateTime)
            {
                Value = conferenceSlotVM.ToDate
            };
            param[6] = new SqlParameter("@ConferenceId", SqlDbType.VarChar)
            {
                Value = conferenceSlotVM.ConferenceId
            };
            param[7] = new SqlParameter("@RequestId", SqlDbType.VarChar)
            {
                Value = conferenceSlotVM.RequestId
            };
            var dataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_GET_IS_SLOT_BOOKED", param);
            if (dataTable != null && dataTable.Rows != null && dataTable.Rows.Count > 0)
            {
                var jsonString = JsonConvert.SerializeObject(dataTable);
                var conferenceTimings = JsonConvert.DeserializeObject<List<ConferenceTimings>>(jsonString);
                return conferenceTimings.Any(x => x.IsBooked);
            }
            return false;
        }
        catch (Exception ex)
        {
            return true;
        }
    }
    public bool CheckSlotAvalability(ConferenceSlotVM conferenceSlotVM)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@FROMTIME", SqlDbType.VarChar)
            {
                Value = conferenceSlotVM.StartHour + ":" + conferenceSlotVM.StartMinutes + ":" + "00"
            };
            param[1] = new SqlParameter("@TOTIME", SqlDbType.VarChar)
            {
                Value = conferenceSlotVM.EndHour + ":" + conferenceSlotVM.EndMinutes + ":" + "00"
            };
            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime)
            {
                Value = conferenceSlotVM.FromDate
            };
            param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime)
            {
                Value = conferenceSlotVM.ToDate
            };
            param[4] = new SqlParameter("@CONFERENCEID", SqlDbType.VarChar)
            {
                Value = conferenceSlotVM.ConferenceId
            };
            param[5] = new SqlParameter("@REQUESTID", SqlDbType.VarChar)
            {
                Value = !string.IsNullOrEmpty(conferenceSlotVM.RequestId) ? conferenceSlotVM.RequestId : ""
            };
            var result = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CONF_CHECK_SLOT_AVALIABLITY", param);
            return result != null && Convert.ToInt32(result) == 1;
        }
        catch (Exception ex)
        {
            return true;
        }
    }

    public DataTable GetCreatedUserInfo(string conferenceReqId)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ConferenceReqId", SqlDbType.NVarChar)
            {
                Value = conferenceReqId
            };
            return SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "CONF_GET_CREATED_USER_INFO", param);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

}