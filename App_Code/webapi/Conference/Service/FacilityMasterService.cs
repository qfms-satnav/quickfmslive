﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class FacilityMasterService
{
    SubSonic.StoredProcedure sp;

    public object SaveFacilityMaster(FacilityDetails Fac)
    {
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@FAC_CODE", SqlDbType.NVarChar);
        param[0].Value = Fac.FMastr.FACILITY_CODE;
        param[1] = new SqlParameter("@FAC_NAME", SqlDbType.NVarChar);
        param[1].Value = Fac.FMastr.FACILITY_NAME;
        param[2] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(Fac.LocatinoList);
        param[3] = new SqlParameter("@UNIT_COST", SqlDbType.Int);
        param[3].Value = Fac.FMastr.FACILITY_UNIT_COST;
        param[4] = new SqlParameter("@REMARKS", SqlDbType.NVarChar);
        param[4].Value = Fac.FMastr.FACILITY_REMARKS;
        param[5] = new SqlParameter("@USER", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["UID"];
        param[6] = new SqlParameter("@FAC_STATUS", SqlDbType.Int);
        param[6].Value = Fac.FMastr.FACILITY_STATUS;
        param[7] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[7].Value = HttpContext.Current.Session["COMPANYID"];
        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SAVE_FACILITY_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                    return new { Message = MessagesVM.Facility_Success };
                else
                    return new { Message = MessagesVM.Facility_InsertError };
            }
        }
        return new { Message = MessagesVM.Facility_InsertError, data = (object)null };
    }

    public object BindGridData()
    {
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BIND_FACILITIES"))
        {
            List<FacilityMasterModel> FacilityList = new List<FacilityMasterModel>();
            while (reader.Read())
            {
                FacilityList.Add(new FacilityMasterModel()
                {
                    FACILITY_CODE = reader["FACILITY_CODE"].ToString(),
                    FACILITY_NAME = reader["FACILITY_NAME"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    LCM_CODE = reader["LCM_CODE"].ToString(),
                    FACILITY_UNIT_COST = Convert.ToInt32(reader["FAC_UNIT_COST"].ToString()),
                    FACILITY_STATUS = reader["FACILITY_STATUS"].ToString(),
                    FACILITY_REMARKS = reader["FACILITY_REMARKS"].ToString(),
                });
            }
            reader.Close();
            return FacilityList;
        }
    }

    public Object ModifyFacilityDetails(FacilityDetails Fac)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@FACILITY_CODE", SqlDbType.NVarChar);
            param[0].Value = Fac.FMastr.FACILITY_CODE;
            param[1] = new SqlParameter("@FACILITY_NAME", SqlDbType.NVarChar);
            param[1].Value = Fac.FMastr.FACILITY_NAME;
            param[2] = new SqlParameter("@FACILITY_LCM", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(Fac.LocatinoList);
            param[3] = new SqlParameter("@FACILITY_COST", SqlDbType.Int);
            param[3].Value = Fac.FMastr.FACILITY_UNIT_COST;
            param[4] = new SqlParameter("@FACILITY_RMK", SqlDbType.NVarChar);
            param[4].Value = Fac.FMastr.FACILITY_REMARKS;
            param[5] = new SqlParameter("@FACILITY_UPT", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["UID"];
            param[6] = new SqlParameter("@FACILITY_STATUS", SqlDbType.Int);
            param[6].Value = Fac.FMastr.FACILITY_STATUS;
            param[7] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "UPDATE_CONF_DETAILS", param);
            return new { Message = MessagesVM.Facility_Modify };
        }

        catch
        {

            throw;
        }
    }
}