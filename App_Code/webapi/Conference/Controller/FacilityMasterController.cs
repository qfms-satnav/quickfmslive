﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;

public class FacilityMasterController : ApiController
{
    FacilityMasterService FacService = new FacilityMasterService();

    [HttpPost]
    public HttpResponseMessage SaveFacilityDetails([FromBody] FacilityDetails Facdt)
    {
        var obj = FacService.SaveFacilityMaster(Facdt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGridData()
    {
        var obj = FacService.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ModifyFacilityDetails([FromBody] FacilityDetails Facdt)
    {
        var obj = FacService.ModifyFacilityDetails(Facdt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
