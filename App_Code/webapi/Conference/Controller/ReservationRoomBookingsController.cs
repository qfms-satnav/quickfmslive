﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net.Http;
using System.Net;


/// <summary>
/// Summary description for ReservationRoomBookingsController
/// </summary>
public class ReservationRoomBookingsController : ApiController
{
    ReservationRoomBookingsService RRBS = new ReservationRoomBookingsService();
    [HttpPost]
    public HttpResponseMessage GetReservationRoomBookings(ReservationRoomBookingsModel rrm)
    {
        var obj = RRBS.GetReservationRoomBookings(rrm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}