﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;


public class ConferenceRoomMasterController : ApiController
{
    ConferenceRoomMasterService RoomService = new ConferenceRoomMasterService();

    [HttpPost]
    public HttpResponseMessage BindConferenceDetails()
    {
        var obj = RoomService.GetConferenceDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    [HttpPost]
    public HttpResponseMessage SaveConferenceRoomDetails(ConferenceLocationStructure ConfModel)
    {
        var obj = RoomService.SaveConferenceRoomDetails(ConfModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    [HttpPost]
    public HttpResponseMessage ModifyConferenceRoomDetails(ConferenceLocationStructure ConfModel)
    {
        var obj = RoomService.ModifyConferenceRoomDetails(ConfModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    [HttpPost]
    public HttpResponseMessage BindGridData()
    {
        var obj = RoomService.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
}
