﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ConferenceAssetMasterController : ApiController
{
    ConferenceAssetMasterService astsvc = new ConferenceAssetMasterService();
    [HttpPost]
    public HttpResponseMessage SaveAssetDetails(ConferenceAssetMasterModel Ast)
    {
        var obj = astsvc.SaveAssetDetails(Ast);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ModifyAssetDetails(ConferenceAssetMasterModel Ast)
    {
        var obj = astsvc.ModifyAssetDetails(Ast);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<ConferenceAssetMasterModel> astlst = astsvc.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, astlst);
        return response;
    }
}
