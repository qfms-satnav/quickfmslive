﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

public class ConferenceTypeMasterController : ApiController
{
    ConferenceTypeMasterService Confsvc = new ConferenceTypeMasterService();

    [HttpPost]
    public HttpResponseMessage SaveConferenceDetails(ConferenceTypeMasterModel Conf)
    {
        if (Confsvc.SaveConferenceData(Conf) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Conf);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
    }

    [HttpPost]
    public HttpResponseMessage ModifyConferenceDetails(ConferenceTypeMasterModel ConfUpdt)
    {
        var obj = Confsvc.ModifyConferenceData(ConfUpdt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ConfUpdt);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<ConferenceTypeMasterModel> Conflsit = Confsvc.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Conflsit);
        return response;
    }
}
