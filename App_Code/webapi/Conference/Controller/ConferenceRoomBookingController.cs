﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for ConferenceRoomBookingController
/// </summary>
public class ConferenceRoomBookingController : ApiController
{
    readonly ConferenceRoomBookingService _conferenceRoomBookingService;
    public ConferenceRoomBookingController()
    {
        _conferenceRoomBookingService = new ConferenceRoomBookingService();
    }

    [HttpPost]
    public HttpResponseMessage GetConferenceRooms(ConferenceBookingSearch conferenceBookingSearch)
    {
        var result = _conferenceRoomBookingService.GetConferenceRooms(conferenceBookingSearch);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetConferenceRoomBookedTimings(ConferenceTimingFilter conferenceTimingFilter)
    {
        var result = _conferenceRoomBookingService.GetConferenceRoomBookedTimings(conferenceTimingFilter);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveBookingRequest(ConferenceBookingVM conferenceBookingVM)
    {
        var result = _conferenceRoomBookingService.SaveBookingRequest(conferenceBookingVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage DeleteBookingRequest(ConferenceBookingCancelModel conferenceBookingCancelModel)
    {
        var result = _conferenceRoomBookingService.DeleteBookingRequest(conferenceBookingCancelModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetInternalAttendess([FromUri] string searchText)
    {
        var result = _conferenceRoomBookingService.GetInternalAttendess(searchText);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetAttendessByConferenceId([FromUri] string conferenceReqId)
    {
        var result = _conferenceRoomBookingService.GetAttendessByConferenceId(conferenceReqId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ModifyBookingRequest(ConferenceBookingVM conferenceBookingVM)
    {
        var result = _conferenceRoomBookingService.ModifyBookingRequest(conferenceBookingVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetConferenceBehalfUsers([FromUri] string searchText)
    {
        var result = _conferenceRoomBookingService.GetConferenceBehalfUsers(searchText);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage IsEmployee()
    {
        var result = _conferenceRoomBookingService.IsEmployee();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage IsSlotBooked(ConferenceSlotVM conferenceSlotVM)
    {
        var result = _conferenceRoomBookingService.IsSlotBooked(conferenceSlotVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CheckSlotAvalability(ConferenceSlotVM conferenceSlotVM)
    {
        var result = _conferenceRoomBookingService.CheckSlotAvalability(conferenceSlotVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetCreatedUserInfo([FromUri] string conferenceReqId)
    {
        var result = _conferenceRoomBookingService.GetCreatedUserInfo(conferenceReqId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, result);
        return response;
    }
}