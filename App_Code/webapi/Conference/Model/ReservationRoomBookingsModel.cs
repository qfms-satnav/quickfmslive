﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ReservationRoomBookingsModel
/// </summary>
public class ReservationRoomBookingsModel
{
    public List<Floorlst> flrlst { get; set; }
    public DateTime FromDate { get; set; }
}