﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ConferenceTypeMasterModel
{
    public string CONF_CODE { get; set; }
    public string CONF_NAME { get; set; }
    public string CONF_STA_ID { get; set; }
    public string CONF_REMARKS { get; set; }
}