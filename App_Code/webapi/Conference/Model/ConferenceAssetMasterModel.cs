﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class ConferenceAssetMasterModel
{
    public string AST_CODE { get; set; }
    public string AST_NAME { get; set; }
    public string AST_STA_ID { get; set; }
    public string AST_REMARKS { get; set; }
}