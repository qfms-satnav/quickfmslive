﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for AssetSparePartsModel
/// </summary>
public class AssetSparePartsModel
{
    public string Type { get; set; }
    public string Vendor { get; set; }

}


public class SpareDumpData
{
    public string AAS_ID { get; set; }
    public string AAS_SPAREPART_NAME { get; set; }
    public string AAS_SPAREPART_DES { get; set; }
    public string AAS_SUB_NAME { get; set; }
    public string AAS_SPAREPART_AVBQUANTITY { get; set; }
    public string AAS_SPAREPART_QUANTITY { get; set; }
    public string AAS_SPAREPART_MINQUANTITY { get; set; }
    public string AAS_REMARKS { get; set; }
    public string AAS_VED { get; set; }
    public string AAS_FSN { get; set; }
    public string AAS_RACK_NO { get; set; }
    public string AAS_IMAGE_PATH { get; set; }
    public string AAS_LOC_ID { get; set; }
    public string AAS_CTY_CODE { get; set; }
    public string AAS_AAB_NAME { get; set; }
    public string AAS_AVR_CODE { get; set; }
    public string AAS_SPAREPART_COST { get; set; }
    public string AAS_SUB_CODE { get; set; }
    public string AAS_AAT_CODE { get; set; }
    public string AVR_NAME { get; set; }
    public string AAS_SPAREPART_NUMBER { get; set; }
    public string ASS_LEAD_TIME { get; set; }


}
