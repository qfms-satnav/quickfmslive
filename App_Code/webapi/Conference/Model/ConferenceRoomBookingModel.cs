﻿using System;
using System.Collections.Generic;
/// <summary>
/// Summary description for ConferenceRoomBookingModel
/// </summary>
public class ConferenceRoomBookingModel
{
    public ConferenceRoomBookingModel()
    {
        BookedTimings = ConferenceBookingHours.BookedTimings;
    }

    public string CONFERENCE_CODE { get; set; }
    public string CONFERENCE_NAME { get; set; }
    public int CONFERENCE_CAPACITY { get; set; }
    public int CONFERENCE_COST { get; set; }
    public string CONFERENCE_TYPE { get; set; }
    public string CONF_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string FLR_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public List<ConferenceTimings> BookedTimings { get; set; }
}

public class ConferenceBookingSearch
{
    public string CityCode { get; set; }
    public string LocationCode { get; set; }
    public string TowerCode { get; set; }
    public string FloorCode { get; set; }
    public int Capacity { get; set; }
    public DateTime FromDate { get; set; }
}

public class ConferenceTimings
{
    public string ConferenceId { get; set; }
    public string StartHour { get; set; }
    public string StartMinutes { get; set; }
    public string StartType { get; set; }
    public string EndHour { get; set; }
    public string EndMinutes { get; set; }
    public string EndType { get; set; }
    public bool IsBooked { get; set; }
    public string BookingStatus { get; set; }
    public string CreatedBy { get; set; }
    public string BookingId { get; set; }
}

public class ConferenceIdsType
{
    public string ConferenceId { get; set; }
}

public class ConferenceTimingFilter
{
    public List<ConferenceIdsType> ConferenceIdsType { get; set; }
    public DateTime ConferenceDate { get; set; }
}

public class ConferenceBookingVM
{
    public string RequestId { get; set; }
    public string CcId { get; set; }
    public string VerticalId { get; set; }
    public string VerticalCode { get; set; }
    public string LocationCode { get; set; }
    public string TowerCode { get; set; }
    public string FloorCode { get; set; }
    public string SpaceType { get; set; }
    public string SpaceSubType { get; set; }
    public string ConferenceCode { get; set; }
    public int StatusId { get; set; }
    public string Cost { get; set; }
    public string AllocationId { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public DateTime FromTime { get; set; }
    public DateTime EndTime { get; set; }
    public string Description { get; set; }
    public string InternalAttendes { get; set; }
    public string ExternalAttendes { get; set; }
    public string BehalfUser { get; set; }
    public DateTime SelectedDate { get; set; }
}

public class ConferenceBookingCancelModel
{
    public string BookingId { get; set; }
    public DateTime BookingDate { get; set; }
    public string ConferenceId { get; set; }
}

public class ConferenceSlotVM
{
    public string StartHour { get; set; }
    public string StartMinutes { get; set; }
    public string EndHour { get; set; }
    public string EndMinutes { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string ConferenceId { get; set; }
    public string RequestId { get; set; }
}

public static class ConferenceBookingHours
{
    private static List<string> Hours = new List<string> { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "00" };
    private static List<string> Minutes = new List<string> { "00", "30", "00" };
    private static string StartType = "AM";
    private static string EndType = "AM";
    public static List<ConferenceTimings> BookedTimings = new List<ConferenceTimings>();
    static ConferenceBookingHours()
    {
        BookedTimings = new List<ConferenceTimings>();
        for (var i = 0; i < (Hours.Count - 1); i++)
        {
            for (var j = 0; j < (Minutes.Count - 1); j++)
            {
                ConferenceTimings conferenceTimings = new ConferenceTimings
                {
                    StartHour = Hours[i],
                    StartMinutes = Minutes[j],
                    StartType = StartType,
                    EndHour = Hours[i],
                    EndMinutes = Minutes[j + 1],
                    EndType = EndType,
                    IsBooked = false,
                    BookingStatus = "Free"
                };
                if (j == (Minutes.Count - 2))
                {
                    conferenceTimings.EndHour = Hours[i + 1];
                    if (conferenceTimings.EndHour == "12")
                    {
                        conferenceTimings.EndType = "PM";
                        EndType = "PM";
                        StartType = "PM";
                    }
                }
                BookedTimings.Add(conferenceTimings);
            }
        }
    }
}