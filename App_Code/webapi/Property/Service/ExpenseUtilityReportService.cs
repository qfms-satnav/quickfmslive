﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using UtiltiyVM;
using LocWiseUtilityVModel;

public class ExpenseUtilityReportService
{
    SubSonic.StoredProcedure sp;
    List<ExpenseCustomizedData> Cust;
    ExpenseCustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(ExpenseUtilityReportVM Det)
    {
        try
        {
            Cust = GetCustomizedDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<ExpenseCustomizedData> GetCustomizedDetails(ExpenseUtilityReportVM Details)
    {
        try
        {
            List<ExpenseCustomizedData> CData = new List<ExpenseCustomizedData>();
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@STAT", SqlDbType.NVarChar);
            param[0].Value = Details.Request_Type;
            param[1] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.loclst);
            param[2] = new SqlParameter("@EXPLST", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(Details.explst);
            param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            param[4] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[4].Value = Details.FromDate;
            param[5] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[5].Value = Details.ToDate;

            if (Details.CNP_NAME == null)
            {
                param[6] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[6].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[6] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[6].Value = Details.CNP_NAME;
            }
            param[7] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[7].Value = Details.SearchValue;
            param[8] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[8].Value = Details.PageNumber;
            param[9] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[9].Value = Details.PageSize;

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_EXPENSE_UTILITY_CUSTOMIZED_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new ExpenseCustomizedData();
                    Custm.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                    Custm.EXP_NAME = Convert.ToString(reader["EXP_NAME"]);
                    Custm.BILL_NO = Convert.ToString(reader["BILL_NO"]);
                    Custm.BILL_INVOICE = Convert.ToString(reader["BILL_INVOICE"]);
                    Custm.AMT = Convert.ToDouble(reader["AMT"]);
                    Custm.VENDOR = Convert.ToString(reader["VENDOR"]);
                    Custm.VEN_MAIL = Convert.ToString(reader["VEN_MAIL"]);
                    Custm.VEN_PHNO = Convert.ToString(reader["VEN_PHNO"]);
                    Custm.FromDate = Convert.ToString(reader["FromDate"]);
                    Custm.DEP_NAME = Convert.ToString(reader["DEP_NAME"]);
                    Custm.REM = Convert.ToString(reader["REMARKS"]);
                    Custm.Expensemonths = Convert.ToString(reader["Expensemonths"]);
                    Custm.OVERALL_COUNT = Convert.ToString(reader["OVERALL_COUNT"]);
                    Custm.USER_ID = Convert.ToString(reader["USER_ID"]);
                    Custm.USER_NAME = Convert.ToString(reader["USER_NAME"]);
                    Custm.LOGIN_DATE = Convert.ToString(reader["LOGIN_DATE"]);
                    Custm.Expense_Year = Convert.ToString(reader["Expenseyear"]);
                    Custm.CLIENTREMARKS = Convert.ToString(reader["CLIENTREMARKS"]);

                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }

}