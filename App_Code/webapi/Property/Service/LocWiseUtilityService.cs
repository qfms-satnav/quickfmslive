﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using UtiltiyVM;
using LocWiseUtilityVModel;
using System.Globalization;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.Reporting.Map.WebForms.VirtualEarth;
using DocumentFormat.OpenXml.EMMA;
using System.Web.Http.Routing;
using System.Web.Services.Description;

public class LocWiseUtilityService


{
    SubSonic.StoredProcedure sp;
    bool datavalidation = false;
    //string worngdate = "";
    DateTime worngdate = DateTime.MinValue;

    //Location
    public IEnumerable<ExpLocationVM> BindLocation()
    {
        List<ExpLocationVM> LocUtilityList = new List<ExpLocationVM>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UTILITY_LOCATION");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                LocUtilityList.Add(new ExpLocationVM()
                {
                    LCM_CODE = sdr.GetValue(0).ToString(),
                    LCM_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return LocUtilityList;
        }
    }

    //ExpenseHead
    public IEnumerable<ExpHeadUtilityModel> BindExpenseHead()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_BIND_EXPENSE_HEAD").GetReader();
        List<ExpHeadUtilityModel> ExpenseUtility = new List<ExpHeadUtilityModel>();
        while (reader.Read())
        {
            ExpenseUtility.Add(new ExpHeadUtilityModel()
            {
                EXP_CODE = reader.GetValue(0).ToString(),
                EXP_NAME = reader.GetValue(1).ToString(),
                ticked = false

            });
        }
        reader.Close();
        return ExpenseUtility;
    }

    //BindUser
    //public IEnumerable<UserVM> BindUser()
    //{
    //    IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_FINANCE_HEAD").GetReader();
    //    List<UserVM> UserList = new List<UserVM>();
    //    while (reader.Read())
    //    {
    //        UserList.Add(new UserVM()
    //        {
    //            AUR_ID = reader.GetValue(0).ToString(),
    //            AUR_NAME = reader.GetValue(1).ToString()

    //        });
    //    }
    //    reader.Close();
    //    return UserList;
    //}

    //BindDepartment
    public IEnumerable<DepVM> BindDepartment()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_BIND_DEPARTMENT").GetReader();
        List<DepVM> DepList = new List<DepVM>();
        while (reader.Read())
        {
            DepList.Add(new DepVM()
            {
                DEP_CODE = reader.GetValue(0).ToString(),
                DEP_NAME = reader.GetValue(1).ToString()

            });
        }
        reader.Close();
        return DepList;
    }

    private void CreateHeaders(ref IXLWorksheet ws)
    {
        ws.Cell(1, "A").Value = "Location";
        ws.Cell(1, "B").Value = "Expenses Head";
        ws.Cell(1, "C").Value = "Bill Date (DD-MM-YYYY)";
        ws.Cell(1, "D").Value = "Bill No";
        ws.Cell(1, "E").Value = "Invoice / PO No";
        ws.Cell(1, "F").Value = "Units / Deployment";
        ws.Cell(1, "G").Value = "Amount";
        ws.Cell(1, "H").Value = "Expense Month (Month Name YYYY)";
        ws.Cell(1, "I").Value = "Vendor Name";
        ws.Cell(1, "J").Value = "Vendor Phone No";
        ws.Cell(1, "K").Value = "Department";
        ws.Cell(1, "L").Value = "Remarks";
    }

    private void AdjustColumnWidth(ref IXLWorksheet ws)
    {
        ws.Column(1).Width = 30;
        ws.Column(2).Width = 25;
        ws.Column(3).Width = 25;
        ws.Column(4).Width = 20;
        ws.Column(5).Width = 20;
        ws.Column(6).Width = 20;
        ws.Column(7).Width = 20;
        ws.Column(8).Width = 35;
        ws.Column(9).Width = 20;
        ws.Column(10).Width = 20;
        ws.Column(11).Width = 20;
        ws.Column(12).Width = 30;
    }

    private void ApplyValidationMessage(string title, string message, ref IXLDataValidation dataValidation)
    {
        dataValidation.ErrorStyle = XLErrorStyle.Stop;
        dataValidation.ErrorTitle = title;
        dataValidation.ErrorMessage = message;
        dataValidation.ShowErrorMessage = true;
    }


    private int AddDataToSheet(ref IXLWorksheet ws, string columnName, List<string> dataList)
    {
        int rowNumber = 1;
        foreach (var data in dataList)
        {
            ws.Cell(columnName + rowNumber).Value = data;
            rowNumber++;
        }
        return rowNumber;
    }

    private XLWorkbook CreateUtilityExcelWithValidation(List<ExpLocationVM> locations, List<ExpHeadUtilityModel> expensesHeads, List<DepVM> departments)
    {
        try
        {
            var wb = new XLWorkbook();
            var ws = wb.Worksheets.Add("Location Wise"); //Make sure changing work sheet name in corresponding references also
            var dataWs = wb.Worksheets.Add("Data Sheet");
            CreateHeaders(ref ws);
            AdjustColumnWidth(ref ws);
            var locationList = locations.Select(x => x.LCM_NAME).ToList();
            int lastRowNumber = AddDataToSheet(ref dataWs, "A", locationList);
            var locationCell = ws.Range("A2", ws.Column("A").LastCell().Address.ToString()).SetDataValidation();
            locationCell.List(dataWs.Range("A1:A" + (lastRowNumber - 1)), true);
            ApplyValidationMessage("Invalid Location", "Please select any one of the option provided", ref locationCell);

            var expenseHeadList = expensesHeads.Select(x => x.EXP_NAME).ToList();
            lastRowNumber = AddDataToSheet(ref dataWs, "B", expenseHeadList);
            var expenseHeadCell = ws.Range("B2", ws.Column("B").LastCell().Address.ToString()).SetDataValidation();
            expenseHeadCell.List(dataWs.Range("B1:B" + (lastRowNumber - 1)), true);
            ApplyValidationMessage("Invalid Expense Head", "Please select any one of the option provided", ref expenseHeadCell);

            var billDate = ws.Range("C2", ws.Column("C").LastCell().Address.ToString()).SetDataValidation();
            billDate.Date.EqualOrGreaterThan(new DateTime(2000, 1, 1));
            ApplyValidationMessage("Invalid Date", "Provided date should be in valid format", ref billDate);
            ws.Column("C").Style.NumberFormat.Format = "dd-MM-yyyy";

            var amount = ws.Range("G2", ws.Column("G").LastCell().Address.ToString()).SetDataValidation();
            amount.Decimal.EqualOrGreaterThan(0.00);
            ApplyValidationMessage("Invalid Amount", "Amount should be number and more or less than 0", ref amount);

            var vendorPhoneNumber = ws.Range("J2", ws.Column("J").LastCell().Address.ToString()).SetDataValidation();
            vendorPhoneNumber.WholeNumber.Between("0000000000", "9999999999");
            ApplyValidationMessage("Invalid Phone Number", "Phone Number should have valid 10 digits", ref vendorPhoneNumber);

            var expenseMonthCell = ws.Range("H2", ws.Column("H").LastCell().Address.ToString()).SetDataValidation();
            expenseMonthCell.Date.EqualOrGreaterThan("01-2000");
            ApplyValidationMessage("Invalid Expense", "Please enter a valid month name and year", ref expenseMonthCell);
            ws.Column("H").Style.NumberFormat.Format = "mmmm yyyy";


            var departmentsList = departments.Select(x => x.DEP_NAME).ToList();
            lastRowNumber = AddDataToSheet(ref dataWs, "C", departmentsList);
            var departmentsCell = ws.Range("K2", ws.Column("K").LastCell().Address.ToString()).SetDataValidation();
            departmentsCell.List(dataWs.Range("C1:C" + (lastRowNumber - 1)), true);
            ApplyValidationMessage("Invalid Department", "Please select any one of the option provided", ref departmentsCell);

            //ws.Range("L2", ws.Column("L").LastCell().Address.ToString()).Style.Alignment.WrapText = true;
            wb.Worksheet("Data Sheet").Visibility = XLWorksheetVisibility.VeryHidden;
            //var dataRange = ws.Range("D2", ws.Column("F").LastCell().Address.ToString()).SetDataValidation();
            //dataRange.Custom("=IF(AND(A2<>\"\", D2=\"\"), FALSE, TRUE)");
            //ApplyValidationMessage("Invalid", "Invalid", ref dataRange);

            return wb;

        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string DownloadLocationExcel()
    {
        var locations = BindLocation();
        var expensesHeads = BindExpenseHead();
        var departments = BindDepartment();

        var wb = CreateUtilityExcelWithValidation(locations.ToList(), expensesHeads.ToList(), departments.ToList());
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName("LocationWiseUtilityReport.xlsx"));
        wb.SaveAs(filePath);
        return filePath;
    }

    public List<ExcelDownload> ExcelDownload(ExcelDownload spcdet)
    {
        List<ExcelDownload> dwndatalst = new List<ExcelDownload>();
        try
        {

            SqlParameter[] param = new SqlParameter[1];

            ExcelDownload dwndata;

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "get_expense_down_dat"))
            {
                while (sdr.Read())
                {
                    dwndata = new ExcelDownload();
                    dwndata.LCM_NAME = sdr["LCM_NAME"].ToString();
                    dwndata.EXP_NAME = sdr["EXP_NAME"].ToString();
                    dwndata.BILL_DATE = sdr["BILL_DATE"].ToString();
                    dwndata.BILL_NO = sdr["BILL_NO"].ToString();
                    dwndata.BILL_INVOICE = sdr["BILL_INVOICE"].ToString();
                    dwndata.AMOUNT = sdr["AMOUNT"].ToString();
                    dwndata.VENDOR = sdr["VENDOR"].ToString();
                    dwndata.VENDOR_EMAIL = sdr["VENDOR_EMAIL"].ToString();
                    dwndata.VENDOR_PHNO = sdr["VENDOR_PHNO"].ToString();
                    dwndata.DEP_NAME = sdr["DEP_NAME"].ToString();
                    dwndata.CLIENTREMARKS = sdr["CLIENTREMARKS"].ToString();
                    dwndatalst.Add(dwndata);
                }
                sdr.Close();
            }

            return dwndatalst;

        }
        catch (Exception ex)
        {
            return dwndatalst;
        }

    }
    //SaveData
    public object SaveData(LocWiseUtilityModel Save)
    {
        try
        {
            // string[] temp = Save.ExpenseMonth.;
            //string ExpenseMonth = getFullName(Int32.Parse(temp[1])+1,Int32.Parse(temp[0]));

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_INSERT_UTILITY_EXPENSES");
            sp.Command.AddParameter("@LOC_CODE", Save.LCM_CODE, DbType.String);
            sp.Command.AddParameter("@EXP_HEAD", Save.EXP_CODE, DbType.String);
            sp.Command.AddParameter("@BILL_NO", Save.BILL_NO, DbType.String);
            sp.Command.AddParameter("@INVOICE", Save.BILL_INVOICE, DbType.String);
            sp.Command.AddParameter("@AMT", Save.AMT, DbType.String);
            sp.Command.AddParameter("@BILL_DATE", Save.FromDate, DbType.String);
            sp.Command.AddParameter("@VEN_NAME", (Save.VENDOR == null) ? "--" : Save.VENDOR, DbType.String);
            sp.Command.AddParameter("@VEN_EMAIL", (Save.VEN_MAIL == null) ? "--" : Save.VEN_MAIL, DbType.String);
            sp.Command.AddParameter("@VEN_PH", (Save.VEN_PHNO == null) ? "--" : Save.VEN_PHNO, DbType.String);
            sp.Command.AddParameter("@DEPID", Save.DEP_CODE, DbType.String);
            sp.Command.AddParameter("@REM", (Save.REM == null) ? "--" : Save.REM, DbType.String);
            sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.AddParameter("@ExpenseMonth", Save.ExpenseMonth, DbType.Date);
            sp.Execute();
            return new { Message = "Data Inserted Successfully", data = Save };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    static string getFullName(int month, int year)
    {
        DateTime date = new DateTime(2020, month, 1);

        return date.ToString("MMMM") + ',' + year;
    }


    //update
    public object UpdateExpUtilityDetails(LocWiseUtilityModel update)
    {
        try
        {

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UPDATE_UTILITY_EXPENSE_DETAILS");
            sp.Command.AddParameter("@LOC_CODE", update.LCM_CODE, DbType.String);
            sp.Command.AddParameter("@EXP_HEAD", update.EXP_CODE, DbType.String);
            sp.Command.AddParameter("@BILL_NO", update.BILL_NO, DbType.String);
            sp.Command.AddParameter("@INVOICE", update.BILL_INVOICE, DbType.String);
            sp.Command.AddParameter("@AMT", update.AMT, DbType.String);
            sp.Command.AddParameter("@BILL_DATE", update.FromDate, DbType.String);
            sp.Command.AddParameter("@VEN_NAME", (update.VENDOR == null) ? "--" : update.VENDOR, DbType.String);
            sp.Command.AddParameter("@VEN_EMAIL", (update.VEN_MAIL == null) ? "--" : update.VEN_MAIL, DbType.String);
            sp.Command.AddParameter("@VEN_PH", (update.VEN_PHNO == null) ? "--" : update.VEN_PHNO, DbType.String);
            sp.Command.AddParameter("@DEPID", update.DEP_CODE, DbType.String);
            sp.Command.AddParameter("@REM", (update.REM == null) ? "--" : update.REM, DbType.String);
            sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.AddParameter("@ExpenseMonth", update.ExpenseMonth, DbType.Date);
            sp.Command.AddParameter("@UTIL_ID", update.UTIL_ID, DbType.Int32);
            sp.Execute();
            return new { Message = "Data Updated Successfully", data = update };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }
    }

    //BindGrid
    public IEnumerable<LocWiseUtilityModel> BindUtilityExpenseGrid()
    {
        List<LocWiseUtilityModel> UtilityExpGridList = new List<LocWiseUtilityModel>();

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_BIND_UTILITY_EXPENSE_GRID");

        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                UtilityExpGridList.Add(new LocWiseUtilityModel()
                {
                    UTIL_ID = Convert.ToInt32(sdr["UTIL_ID"]),
                    EXP_CODE = sdr["EXP_CODE"].ToString(),
                    LCM_CODE = sdr["LCM_CODE"].ToString(),
                    FromDate = sdr["BILL_DATE"].ToString(),
                    BILL_NO = sdr["BILL_NO"].ToString(),
                    BILL_INVOICE = sdr["BILL_INVOICE"].ToString(),
                    AMT = Convert.ToDecimal(sdr["AMOUNT"]),
                    DEP_CODE = sdr["BILL_GENERATED_BY"].ToString(),
                    VENDOR = sdr["VENDOR"].ToString(),
                    VEN_MAIL = sdr["VENDOR_EMAIL"].ToString(),
                    LCM_NAME = sdr["LCM_NAME"].ToString(),
                    EXP_NAME = sdr["EXP_NAME"].ToString(),
                    DEP_NAME = sdr["DEP_NAME"].ToString(),
                    REM = sdr["CLIENTREMARKS"].ToString(),
                    //REMARKS = sdr["REMARKS"].ToString(),
                    //ExpenseMonth=Convert.ToDateTime(sdr["ExpenseMonth"])
                    ExpenseMonth = sdr["ExpenseMonth"] != null ? Convert.ToString(sdr["ExpenseMonth"]) : Convert.ToString(string.Empty)
                });
            }
            sdr.Close();
            return UtilityExpGridList;
        }
    }

    public object GetUtilityDetails(LocWiseUtilityModel BindDetails)
    {
        try
        {
            LocWiseUtilityVM UDVMList = new LocWiseUtilityVM();

            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_UTILDETAILS_ON_SELECTION");
            sp.Command.AddParameter("@LOC_CODE", BindDetails.LCM_CODE, DbType.String);
            sp.Command.AddParameter("@EXP_CODE", BindDetails.EXP_CODE, DbType.String);
            sp.Command.AddParameter("@DATE", BindDetails.FromDate, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                LocWiseUtilityModel UDList = new LocWiseUtilityModel();
                if (reader.Read())
                {

                    UDList.LCM_CODE = reader["LCM_CODE"].ToString();
                    UDList.EXP_CODE = reader["EXP_CODE"].ToString();
                    UDList.FromDate = reader["BILL_DATE"].ToString();
                    UDList.BILL_NO = reader["BILL_NO"].ToString();
                    UDList.BILL_INVOICE = reader["BILL_INVOICE"].ToString();
                    UDList.AMT = Convert.ToDecimal(reader["AMOUNT"]);
                    UDList.VENDOR = reader["VENDOR"].ToString();
                    UDList.VEN_MAIL = reader["VENDOR_EMAIL"].ToString();
                    UDList.VEN_PHNO = reader["VENDOR_PHNO"].ToString();
                    UDList.DEP_CODE = reader["BILL_GENERATED_BY"].ToString();
                    UDList.REM = reader["REMARKS"].ToString();
                    UDList.STATUS = "Modify";

                }
                else
                {
                    UDList.FromDate = BindDetails.FromDate;
                    UDList.EXP_CODE = BindDetails.EXP_CODE;
                    UDList.LCM_CODE = BindDetails.LCM_CODE;
                    UDList.STATUS = "New";
                }
                reader.Close();
                UDVMList.LocWiseUtilityModel = UDList;
                return UDVMList;
            }

        }
        catch (Exception ex)
        {
            return new { Message = ex.ToString(), data = (object)null };
        }
    }
    public object UploadExcel(HttpRequest httpRequest)
    {
        try
        {
            var locationList = BindLocation();
            var expenseHeadList = BindExpenseHead();
            var departmentList = BindDepartment();
            List<LocWiseUtilityModel> uadmlst = GetDataTableFrmReq(httpRequest, locationList.ToList(), expenseHeadList.ToList(), departmentList.ToList());
            if (uadmlst.Any(x => string.IsNullOrEmpty(x.REM)))
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@UTILITYLIST", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(uadmlst.Where(x => string.IsNullOrEmpty(x.REM)).ToList());
                param[1] = new SqlParameter("@AURID", SqlDbType.VarChar);
                param[1].Value = HttpContext.Current.Session["UID"].ToString();
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "AST_BULK_INSERT_UTILITY_BY_LOC", param);
            }
            uadmlst.ForEach(x =>
            {
                x.REM = !string.IsNullOrEmpty(x.REM) ? x.REM : @"Success\New";
                x.FromDate = (string.IsNullOrEmpty(x.REM) || (!x.REM.Contains("Invalid Bill Date Format") && !x.REM.Contains("Please Fill Bill Date"))) ? Convert.ToDateTime(x.FromDate).ToString("dd-MM-yyyy") : x.FromDate;
                x.ExpenseMonth = (string.IsNullOrEmpty(x.REM) || (!x.REM.Contains("Invalid Expense Month") && !x.REM.Contains("Please Fill ExpenseMonth"))) ? Convert.ToDateTime(x.ExpenseMonth).ToString("MMMM yyyy") : x.ExpenseMonth;
            });
            //else
            //{
            //    return new { Message = MessagesVM.UAD_UPLVALDAT, data = (object)null };
            //}
            var wb = CreateUtilityExcelWithValidation(locationList.ToList(), expenseHeadList.ToList(), departmentList.ToList());
            var ws = wb.Worksheet("Location Wise");
            //ws.Cell("M1").Value = "Error Comments";
            int rowNumber = 2;
            foreach (var row in uadmlst)
            {
                ws.Cell("A" + rowNumber).Value = row.LCM_NAME;
                ws.Cell("B" + rowNumber).Value = row.EXP_NAME;
                ws.Cell("C" + rowNumber).Value = row.FromDate;
                ws.Cell("D" + rowNumber).Value = row.BILL_NO;
                ws.Cell("E" + rowNumber).Value = row.BILL_INVOICE;
                ws.Cell("G" + rowNumber).Value = row.AMT;
                ws.Cell("H" + rowNumber).Value = row.ExpenseMonth;
                ws.Cell("I" + rowNumber).Value = row.VENDOR;
                ws.Cell("J" + rowNumber).Value = row.VEN_PHNO;
                ws.Cell("F" + rowNumber).Value = row.VEN_MAIL;
                ws.Cell("K" + rowNumber).Value = row.DEP_NAME;
                ws.Cell("L" + rowNumber).Value = row.REM;
                //ws.Cell("M" + rowNumber).Value = errorRow.ERRORCOMMENTS;
                rowNumber++;
            }
            var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName("LocationWiseUtilityReport.xlsx"));
            wb.SaveAs(filePath);
            return new { Message = MessagesVM.UAD_UPLOK, data = uadmlst };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
    public List<LocWiseUtilityModel> GetDataTableFrmReq(HttpRequest httpRequest, List<ExpLocationVM> locationList, List<ExpHeadUtilityModel> expenseHeadList, List<DepVM> departmentList)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);
        return CreateExcelFile.ReadAsListExp2(filePath, locationList, expenseHeadList, departmentList);
    }
    public class CheckExcelColumnNamess
    {
        public static string LCM_NAME = "location";
        public static string EXP_NAME = "Expenses Head";
        public static string BILL_DATE = "Bill Date(DD-MM-YYYY)";
        public static string BILL_NO = "Bill No";
        public static string BILL_INVOICE = "Invoice / PO No";
        public static string VENDOR_EMAIL = "Units / Quantity";
        public static string AMOUNT = "Amount";
        public static string ExpenseMonth = "Expense Month(Month Name, YYYY)";
        public static string VENDOR = "Vendor";
        public static string VENDOR_PHNO = "Vendor Phone No";
        public static string DEP_NAME = "Department";
        public static string CLIENTREMARKS = "Remarks";
    }

}

