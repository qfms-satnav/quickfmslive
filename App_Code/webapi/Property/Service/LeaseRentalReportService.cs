﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class LeaseRentalReportService
{
    SubSonic.StoredProcedure sp;
    List<LeaseRentalReportModel> Leaselist;
    LeaseRentalReportModel Lease;
    DataSet ds;

    public object GetLeaseData(Company Propdetails)
    {
        try
        {
            Leaselist = LeaseReportDetails(Propdetails);
            if (Leaselist.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Leaselist };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<LeaseRentalReportModel> LeaseReportDetails(Company Propdetails)
    {
        try
        {
            List<LeaseRentalReportModel> LeaseData = new List<LeaseRentalReportModel>();

            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@FRMDT", SqlDbType.DateTime);
            param[0].Value = Propdetails.FromDate;
            param[1] = new SqlParameter("@TODT", SqlDbType.DateTime);
            param[1].Value = Propdetails.ToDate;
            param[2] = new SqlParameter("@AURID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            if (Propdetails.CNP_NAME == null)
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = Propdetails.CNP_NAME;
            }
            param[4] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(Propdetails.loclst);
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[5].Value = Propdetails.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[6].Value = Propdetails.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[7].Value = Propdetails.PageSize;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_RENTAL_REPORT", param))
            {
                while (reader.Read())
                {
                    Lease = new LeaseRentalReportModel();
                    Lease.LCM_NAME = reader["LCM_NAME"].ToString();
                    Lease.PM_PPT_NAME = reader["PM_PPT_NAME"].ToString();
                    Lease.PM_PPT_ADDRESS = reader["PM_PPT_ADDRESS"].ToString();
                    Lease.PM_LPD_REQ_ID = reader["PM_LPD_REQ_ID"].ToString();
                    Lease.PM_LPD_LL_NAME = reader["PM_LPD_LL_NAME"].ToString();
                    Lease.PM_LPD_LL_BASIC_RENT = reader["PM_LPD_LL_BASIC_RENT"].ToString();
                    Lease.PM_LPD_LL_PAY_TERM = reader["PM_LPD_LL_PAY_TERM"].ToString();
                    Lease.PM_LPD_LL_PAY_MONTH = reader["PM_LPD_LL_PAY_MONTH"].ToString();
                    Lease.PM_PPT_LOC_CODE = reader["PM_PPT_LOC_CODE"].ToString();
                    Lease.PM_LPD_LL_PAY_YEAR = reader["PM_LPD_LL_PAY_YEAR"].ToString();
                    Lease.PM_LPD_CREATED_DT = reader["PM_LPD_CREATED_DT"].ToString();
                    Lease.PM_LAD_EFFE_DT_AGREEMENT = (DateTime)reader["PM_LAD_EFFE_DT_AGREEMENT"];
                    Lease.PM_LAD_EXP_DT_AGREEMENT = (DateTime)reader["PM_LAD_EXP_DT_AGREEMENT"];
                    Lease.INVOICE_DT = reader["PM_LP_INVOICE_DT"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["PM_LP_INVOICE_DT"]); 
                    Lease.PM_LPD_LL_STATUS = reader["PM_LPD_LL_STATUS"].ToString();
                    Lease.INVOICE_NO = reader["PM_LP_INVOICE_NO"].ToString();
                    Lease.PAY_MODE = reader["PAY_MODE"].ToString();
                    Lease.PM_LP_ACC_NO = reader["PM_LP_ACC_NO"].ToString();
                    Lease.PM_LP_NEFT_BRNCH = reader["PM_LP_NEFT_BRNCH"].ToString();
                    Lease.PM_LP_BANK_NAME = reader["PM_LP_BANK_NAME"].ToString();
                    Lease.PM_LP_IFSC = reader["PM_LP_IFSC"].ToString();
                    Lease.company = reader["company"].ToString();
                    Lease.dueamnt = reader["dueamnt"].ToString();
                    Lease.RentPaid = reader["rent_paid"].ToString();
                    Lease.withhold = reader["withhold"].ToString();
                    Lease.CHE_CODE = reader["CHE_CODE"].ToString();
                    Lease.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    LeaseData.Add(Lease);
                }
                reader.Close();
            }
            return LeaseData;
        }
        catch
        {
            throw;
        }
    }


}