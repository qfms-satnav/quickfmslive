﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class OpexFormatReportService
{
    SubSonic.StoredProcedure sp;
    List<OpexFormatData> Cust;
    OpexFormatData Custm;
    DataSet ds;

    //public object GetOpexFormatObject(OpexFormatDetails Empdata)
    //{
    //    try
    //    {
    //        Cust = GetOpexFormatDetails(Empdata);
    //        if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
    //        else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
    //    }
    //    catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    //}
    public object getProjects()
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PCL_GET_PROJECTTYPES_REPORT");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetOpexFormatObject(OpexFormatDetails det)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[4];
            String[] str = det.loclist.Split(Convert.ToChar(","));
            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            if (Convert.ToInt32(str.Length) <= 0)
                param[0].Value = "";
            else
                param[0].Value = det.loclist;

            param[1] = new SqlParameter("@CHE_CODE", SqlDbType.NVarChar);
            param[1].Value = det.entitylist;
            param[2] = new SqlParameter("@FromDate", SqlDbType.NVarChar);
            param[2].Value = det.FromDate;
            param[3] = new SqlParameter("@ToDate", SqlDbType.NVarChar);
            param[3].Value = det.ToDate;
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "RPT_OPEX_FORMAT", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, Message = MessagesVM.AF_OK };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.CNP_ERROR };
        };
    }

}