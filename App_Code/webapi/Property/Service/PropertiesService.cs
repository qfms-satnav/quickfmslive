﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class PropertiesService
{
    SubSonic.StoredProcedure sp;
    PropertiesModel Prop;
    List<PropertiesModel> PropList;
    DataSet ds;

    public object ViewProperties(ReportType Type)
    {
        try
        {
            PropList = GetProperties(Type);
            if (PropList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = PropList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public object GetStatus()
    {
        List<StatusD> stlst = new List<StatusD>();
        StatusD S;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_STATUS_VAL");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                S = new StatusD();
                S.STA_CODE = sdr["sta_id"].ToString();
                S.STA_DESC = sdr["STA_DESC"].ToString();
                S.ticked = false;
                stlst.Add(S);
            }
            sdr.Close();
        }
        if (stlst.Count != 0)
            return new { Message = MessagesVM.HDM_UM_OK, data = stlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetLocations(ReportType r)
    {
        List<LocationList> llst = new List<LocationList>();
        LocationList lst;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_COMPANYBYLOCATION");
        sp.Command.AddParameter("@Company", r.CNP_NAME, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                lst = new LocationList();
                lst.LCM_CODE = sdr["LCM_CODE"].ToString();
                lst.LCM_NAME = sdr["LCM_NAME"].ToString();
                lst.ticked = false;
                llst.Add(lst);
            }
            sdr.Close();
        }
        if (llst.Count != 0)
            return new { Message = MessagesVM.HDM_UM_OK, data = llst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetMarkers(ReportType Type)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ACTIVE_PROPERTIES");
            sp.Command.AddParameter("@COMPANYID", Type.CNP_NAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return new { Message = MessagesVM.SER_OK, data = ds };
                }
                else
                {
                    return new { Message = MessagesVM.SER_OK, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.SER_OK, data = (object)null };

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<PropertiesModel> GetProperties(ReportType Type)
    {
        try
        {
            DateTime? dt = null;
            List<PropertiesModel> VMlist = new List<PropertiesModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_VIEW_ALL_PROPERTIES");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANY", Type.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@SEARCHVAL", Type.SearchValue, DbType.String);
            sp.Command.AddParameter("@PAGENUM", Type.PageNumber, DbType.String);
            sp.Command.AddParameter("@PAGESIZE", Type.PageSize, DbType.String);
            sp.Command.AddParameter("@Status", Type.STA_CODE, DbType.String);
            sp.Command.AddParameter("@Location", Type.LCM_CODE==null?"": Type.LCM_CODE, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    Prop = new PropertiesModel();
                    Prop.PN_PROPERTYTYPE = reader["PN_PROPERTYTYPE"].ToString();
                    Prop.PM_PPT_NAME = reader["PM_PPT_NAME"].ToString();
                    Prop.LCM_NAME = reader["LCM_NAME"].ToString();
                    Prop.PM_PPT_ADDRESS = reader["PM_PPT_ADDRESS"].ToString();
                    Prop.PM_AR_CARPET_AREA = reader["PM_AR_CARPET_AREA"].ToString();
                    Prop.PM_AR_BUA_AREA = reader["PM_AR_BUA_AREA"].ToString();
                    Prop.PM_OWN_NAME = reader["PM_OWN_NAME"].ToString();
                    Prop.AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString();
                   // Prop.PM_INS_START_DT = reader["PM_INS_START_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PM_INS_START_DT"]);
                   // Prop.PM_INS_END_DT = reader["PM_INS_END_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PM_INS_END_DT"]);
                    Prop.AUR_RES_NUMBER = reader["AUR_RES_NUMBER"].ToString();
                    Prop.AUR_EMAIL = reader["AUR_EMAIL"].ToString();
                    Prop.PM_IT_NAME = reader["PM_IT_NAME"].ToString();
                   // Prop.PM_INS_VENDOR = reader["PM_INS_VENDOR"].ToString();
                   // Prop.PM_INS_AMOUNT = reader["PM_INS_AMOUNT"].ToString();
                   // Prop.PM_INS_PNO = reader["PM_INS_PNO"].ToString();
                    //Prop.CNP_NAME = reader["CNP_NAME"].ToString();
                    Prop.CHE_NAME = reader["CHE_NAME"].ToString();
                    Prop.STA_DESC = reader["STA_DESC"].ToString();
                    Prop.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    Prop.PM_PROPERTY_REMARK = reader["PM_PROPERTY_REMARK"].ToString();

                    Prop.TWR_NAME = reader["TWR_NAME"].ToString();
                    Prop.FLR_NAME = reader["FLR_NAME"].ToString();
                    Prop.PM_AR_RENT_AREA = reader["PM_AR_RENT_AREA"].ToString();
                    Prop.PM_AR_USABEL_AREA = reader["PM_AR_USABEL_AREA"].ToString();
                    Prop.PM_AR_PLOT_AREA = reader["PM_AR_PLOT_AREA"].ToString();
                    Prop.PM_OWN_ADDRESS = reader["PM_OWN_ADDRESS"].ToString();
                   // Prop.PM_PUR_PRICE = reader["PM_PUR_PRICE"].ToString();
                  //  Prop.PM_PUR_DATE = reader["PM_PUR_DATE"].ToString() == "" ? dt : Convert.ToDateTime(reader["PM_PUR_DATE"]);
                   // Prop.PM_PUR_MARKET_VALUE = reader["PM_PUR_MARKET_VALUE"].ToString();
                   // Prop.PM_GOV_IRDA = reader["PM_GOV_IRDA"].ToString();
                    Prop.PM_GOV_PC_CODE = reader["PM_GOV_PC_CODE"].ToString();
                   // Prop.PM_GOV_PROP_CODE = reader["PM_GOV_PROP_CODE"].ToString();
                   // Prop.PM_GOV_UOM_CODE = reader["PM_GOV_UOM_CODE"].ToString();
                  //  Prop.PM_INS_TYPE = reader["PM_INS_TYPE"].ToString();
                    //Prop.PM_INS_VENDOR = reader["PM_INS_VENDOR"].ToString();
                    //Prop.PM_INS_PNO = reader["PM_INS_PNO"].ToString();
                    Prop.PM_COST_RENT = reader["PM_COST_RENT"].ToString();
                    Prop.PM_COST_RENT_SFT = reader["PM_COST_RENT_SFT"].ToString();
                    Prop.PM_COST_RATIO = reader["PM_COST_RATIO"].ToString();
                    Prop.PM_COST_OWN_SHARE = reader["PM_COST_OWN_SHARE"].ToString();
                    Prop.PM_COST_SECDEPOSIT = reader["PM_COST_SECDEPOSIT"].ToString();
                    Prop.PM_COST_GST = reader["PM_COST_GST"].ToString();
                    Prop.PM_COST_MAINTENANCE = reader["PM_COST_MAINTENANCE"].ToString();
                    Prop.PM_COST_ESC_RENTALS = reader["PM_COST_ESC_RENTALS"].ToString();
                    Prop.PM_COST_RENT_FREE = reader["PM_COST_RENT_FREE"].ToString();
                    Prop.PM_COST_STAMP = reader["PM_COST_STAMP"].ToString();
                    Prop.PM_COST_AGREEMENT_PERIOD = reader["PM_COST_AGREEMENT_PERIOD"].ToString();
                    Prop.PM_OTHR_FLOORING = reader["PM_OTHR_FLOORING"].ToString();

                    Prop.PM_OTHR_WASHRM_REQUIRED = reader["PM_OTHR_WASHRM_REQUIRED"].ToString();
                    Prop.PM_OTHR_POTABLE_WTR = reader["PM_OTHR_POTABLE_WTR"].ToString();
                    Prop.PM_DOC_TITLE = reader["PM_DOC_TITLE"].ToString();
                    Prop.PM_DOC_TITLLE_RMKS = reader["PM_DOC_TITLLE_RMKS"].ToString();
                    Prop.PM_DOC_OCCUPANCY = reader["PM_DOC_OCCUPANCY"].ToString();
                    Prop.PM_DOC_OCC_RMKS = reader["PM_DOC_OCC_RMKS"].ToString();
                    Prop.PM_DOC_BUILD = reader["PM_DOC_BUILD"].ToString();
                    Prop.PM_DOC_BUILD_RMKS = reader["PM_DOC_BUILD_RMKS"].ToString();
                    Prop.PM_DOC_PAN = reader["PM_DOC_PAN"].ToString();
                    Prop.PM_DOC_PAN_RMKS = reader["PM_DOC_PAN_RMKS"].ToString();

                    Prop.PM_DOC_TAX = reader["PM_DOC_TAX"].ToString();
                    Prop.PM_DOC_TAX_RMKS = reader["PM_DOC_TAX_RMKS"].ToString();
                    Prop.PM_DOC_OTHER_INFO = reader["PM_DOC_OTHER_INFO"].ToString();

                    VMlist.Add(Prop);
                }
                reader.Close();
            }
            return VMlist;
        }
        catch
        {
            throw;
        }
    }
}
public class MasterData
{
    public List<ACQISITION_THROUGH> ACQISITION_LIST { get; set; }
    public List<REQUEST_TYPES> REQUEST_TYPES_LIST { get; set; }

    public List<PROPERTY_NATURE> REQUEST_NATURE_LIST { get; set; }

    public List<string> LOCATION_ZONE_LIST { get; set; }
    public List<ACTPROP_TYPE> ACTPROP_TYPE_LIST { get; set; }
    public List<ACTCTY> ACTCTY_LIST { get; set; }
    public List<LOCATION_DATA> LOCATION_DATA_LIST { get; set; }
    public List<TOWER_DATA> TOWER_DATA_LIST { get; set; }
    public List<FLOOR_DATA> FLOOR_DATA_LIST { get; set; }

}

public class PROPERTY_NATURE
{
    public string Property_Nature_ID { get; set; }
    public string Property_Nature_Name { get; set; }

}
public class ACQISITION_THROUGH
{
    public string ACQISITION_THROUGH_NAME { get; set; }
    public string ACQISITION_THROUGH_ID { get; set; }
}

public class REQUEST_TYPES
{
    public string PM_RT_SNO { get; set; }
    public string PM_RT_TYPE { get; set; }
}

public class ACTPROP_TYPE
{
    public string PN_PROPERTYTYPE { get; set; }
    public string PN_TYPEID { get; set; }
}

public class ACTCTY
{
    public string CTY_NAME { get; set; }
    public string CTY_CODE { get; set; }
}

public class LOCATION_DATA
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CTY_ID { get; set; }
    //public string LCM_ZONE   { get; set; }

}

public class TOWER_DATA
{
    public string TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public string TWR_BDG_ID { get; set; }

}

public class FLOOR_DATA
{
    public string FLR_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public string UDM_CTY_CODE { get; set; }
    public string FLR_BDG_ID { get; set; }
    public string UDM_TWR_CODE { get; set; }

}
///////////////////////END Code ///////////////////////////////////
public class PropertiesDetails
{
    public string ENTITY { get; set; }
    public string Remarks { get; set; }
    public string ValidationField { get; set; }
    public string REQUEST_TYPE { get; set; }
    public string LCM_ZONE { get; set; }
    public string State { get; set; }
    public string TOWER { get; set; }
    public string FLOOR { get; set; }
    public string LOCATION { get; set; }
    public string CITY { get; set; }
    public string PROP_TYPE { get; set; }
    public string LOCATION_CODE { get; set; }

    public string GL_code { get; set; }
    //public string PC { get; set; }
    //public string CC { get; set; }
    public string LandlordName { get; set; }
    public string C_B_SB_Area { get; set; }
    public string Area { get; set; }
    public string Chargeable_Area { get; set; }
    public string Rentper_sqft { get; set; }
    public string BasicRent { get; set; }
    public string LandlordMaintenance { get; set; }
    public string LandlordAmenities { get; set; }
    //public string DG_Charges { get; set; }
    //public string AMC_HIVAC { get; set; }
    public string CarParkingCharges { get; set; }
    //public string Another_charges { get; set; }
    //public string PT_Charges { get; set; }
    //public string Signage_PM { get; set; }
    public string TotalRent { get; set; }
    //public string BankScopeRecovery { get; set; }
    //public string LLScopeRecoveryDetails { get; set; }
    //public string Remarks { get; set; }
    public string Remark { get; set; }
    //public string RE_PM { get; set; }
    public string SecurityDeposit { get; set; }
    public string Electricity_Deposit { get; set; }
    public string PROP_ADDR { get; set; }
    public string LeaseSignDate { get; set; }
    public string LeaseStartDate { get; set; }
    public string LeaseEndDate { get; set; }
    public string RentStartDate { get; set; }
    public string Escalation { get; set; }
    public string LockinPeriod { get; set; }
    public string No_Car_Parking { get; set; }
    public string PropertyTaxApplicable { get; set; }
    public string PropertyTax { get; set; }
    public string NoticePeriodinMonths { get; set; }
    public string Address { get; set; }
    public string ContactDetails { get; set; }
    public string PANNo { get; set; }
    public string Email { get; set; }
    public string GST { get; set; }
    public string GSTNumber { get; set; }
    public string PROP_NATURE { get; set; }
    public string ACQ_TRH { get; set; }
    public string Pin_Code { get; set; }
    public string PROP_NAME { get; set; }
    public string LL_State { get; set; }
    public string LL_City { get; set; }
    public string Registration_Charges { get; set; }
    public string Owner_Name { get; set; }
    public string Owner_Mobile_Number { get; set; }
    //public string Super_Built_up_area { get; set; }
    //public string build_up_area { get; set; }
    //public string Carpet_area { get; set; }
    //public string Rentable_area { get; set; }

}

public class DatabaseSavedInfo
{
    public string ExcelUniqueId { get; set; }
    public int TotalNoOfSavedRecords { get; set; }
    public int TotalNoOfFailureRecords { get; set; }
    public string Message { get; set; }
    public List<PropertiesDetails> FailureRecordDesc { get; set; }

}

public class PropertyReqInfo
{
    public string PropertyReqId { get; set; }
}