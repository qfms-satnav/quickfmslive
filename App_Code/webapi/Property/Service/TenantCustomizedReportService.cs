﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class TenantCustomizedReportService
{

    SubSonic.StoredProcedure sp;
    List<TenantCustomizedData> Cust;
    TenantCustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(TenantCustomizedDetails Det)
    {
        try
        {
            Cust = GetCustomizedDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    public List<TenantCustomizedData> GetCustomizedDetails(TenantCustomizedDetails Details)
    {
        try
        {
            List<TenantCustomizedData> CData = new List<TenantCustomizedData>();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Details.loclst);           
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[2].Value = Details.FromDate;
            param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[3].Value = Details.ToDate;

            if (Details.CNP_NAME == null)
            {
                param[4] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[4].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[4] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[4].Value = Details.CNP_NAME;
            }
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[5].Value = Details.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[6].Value = Details.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[7].Value = Details.PageSize;

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_TENANT_CUSTOMIZED_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new TenantCustomizedData();
                    Custm.PN_PROPERTYTYPE = Convert.ToString(reader["PN_PROPERTYTYPE"]);
                    Custm.PM_PPT_CODE = Convert.ToString(reader["PM_PPT_CODE"]);
                    Custm.PM_PPT_NAME = Convert.ToString(reader["PM_PPT_NAME"]);
                    Custm.PM_PPT_ADDRESS = Convert.ToString(reader["PM_PPT_ADDRESS"]);
                    Custm.PM_OWN_NAME = Convert.ToString(reader["PM_OWN_NAME"]);
                    Custm.CNY_NAME = Convert.ToString(reader["CNY_NAME"]);
                    Custm.CTY_NAME = Convert.ToString(reader["CTY_NAME"]);
                    Custm.PM_TD_JOIN_DT = (DateTime)reader["PM_TD_JOIN_DT"];
                    Custm.PM_TEN_FRM_DT = (DateTime)(reader["PM_TEN_FRM_DT"]);
                    Custm.PM_TEN_TO_DT = (DateTime)(reader["PM_TEN_TO_DT"]);
                    Custm.PM_TD_SECURITY_DEPOSIT = Convert.ToDouble(reader["PM_TD_SECURITY_DEPOSIT"]);
                    Custm.PM_TD_RENT = Convert.ToDouble(reader["PM_TD_RENT"]);
                    Custm.PM_TD_TOT_RENT = Convert.ToDouble(reader["PM_TD_TOT_RENT"]);
                    Custm.PM_TD_MAINT_FEES = Convert.ToDouble(reader["PM_TD_MAINT_FEES"]);
                    Custm.PARK_FEE = Convert.ToDouble(reader["PARK_FEE"]);
                    Custm.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                    Custm.TEN_EMAIL = Convert.ToString(reader["TEN_EMAIL"]);
                    Custm.TEN_STATUS = Convert.ToString(reader["TEN_STATUS"]);
                    Custm.PM_PT_NAME = Convert.ToString(reader["PM_PT_NAME"]);
                    Custm.TEN_NAME = Convert.ToString(reader["TEN_NAME"]);
                    Custm.OVERALL_COUNT = Convert.ToString(reader["OVERALL_COUNT"]);
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}