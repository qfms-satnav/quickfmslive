﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class SubLeaseReportService
{

    SubSonic.StoredProcedure sp;

    public object GetGridObject(SubLeaseReportModel Det)
    {
        try
        {
            DataTable dt = GetGriddata(Det);
            if (dt.Rows.Count != 0) { return new { Message = MessagesVM.SER_OK, data = dt }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetGriddata(SubLeaseReportModel Det)
    {
        List<SubLeaseData> CData = new List<SubLeaseData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Det.loclst);
            param[1] = new SqlParameter("@CPYLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Det.cpylst);
            
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "PM_SUB_LEASE_REPORT", param);
            return DT;
        }
        catch (Exception ex) { return DT; }
    }
}