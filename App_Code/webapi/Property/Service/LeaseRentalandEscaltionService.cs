﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

/// <summary>
/// Summary description for LeaseRentalandEscaltionService
/// </summary>
public class LeaseRentalandEscaltionService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    List<LeaseCustomizedData> Cust;
    LeaseCustomizedData Custm;

    public object GetPoprtyData()
    {
        try
        {
            List<LeaseProperty> proplst = new List<LeaseProperty>();
            LeaseProperty prop;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_PROPERTYNAME");
            //sp.Command.AddParameter("@PM_PROPERTY", HttpContext.Current.Session["UID"], DbType.String);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    prop = new LeaseProperty();
                    prop.PM_PPT_NAME = sdr["PM_PPT_NAME"].ToString();
                    prop.PM_PPT_CODE = sdr["PM_PPT_CODE"].ToString();
                    proplst.Add(prop);
                }
            }
            if (proplst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = proplst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    

    public object GetGriddata(LeaseProperty data)
    {
       
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "LEASE_CUSTOMIZED_REPORT_BY_PROERTY");
        sp.Command.AddParameter("@PROPERTY_CODE", data.PM_PPT_CODE);
        sp.Command.AddParameter("@Year", data.year);
        sp.Command.AddParameter("@ReqType", data.PM_PPT_TYPE);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
           
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data1 = ds.Tables[1], data2 = ds.Tables[2],data3=ds.Tables[3] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetReqType()
    {
        try
        {
            List<LeaseProperty> proplst = new List<LeaseProperty>();
            LeaseProperty prop;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_PROREQTYPE");
            //sp.Command.AddParameter("@PM_PROPERTY", HttpContext.Current.Session["UID"], DbType.String);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    prop = new LeaseProperty();
                    prop.PN_PROPERTYTYPE = sdr["PN_PROPERTYTYPE"].ToString();
                    prop.PM_PPT_TYPE = sdr["PM_PPT_TYPE"].ToString();
                    proplst.Add(prop);
                }
            }
            if (proplst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = proplst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    //public object getProjects()
    //{

    //    DataSet ds = new DataSet();
    //    sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PCL_GET_PROJECTTYPES_REPORT");
    //    ds = sp.GetDataSet();
    //    if (ds.Tables.Count != 0)
    //        return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
    //    else
    //        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    //}



}
