﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using UtiltiyVM;
using System.Web.UI;
/// <summary>
/// Summary description for LeaseRentalConfirmationService
/// </summary>
public class LeaseRentalConfirmationService
{
    //public LeaseRentalConfirmationService()
    //{
    public object btnbrowse(HttpRequest httpRequest)
    {
        try
        {
            List<leaserentalpayment> uadmlst = GetDataTableFrmReq(httpRequest);

            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@RENTAL_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(uadmlst); ;

            param[1] = new SqlParameter("@CMP_ID", SqlDbType.VarChar);
            param[1].Value = HttpContext.Current.Session["COMPANYID"];
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PM_INSERT_LL_PAYMENT_DETAILS_EXCEL", param);

            return new { data = ds.Tables[0] };

            //return new { Message = MessagesVM.UAD_UPLVALDAT, data = ds};
            ////}
            //else
            //return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ExcelValidations, Info = ex.InnerException, data = (object)null };

        }


    }
    public List<leaserentalpayment> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
        var uplst = CreateExcelFile.ReadAsLists1(filePath);
        if (uplst.Count != 0)
            uplst.RemoveAt(0);
        return uplst;

    }



}