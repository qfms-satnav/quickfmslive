﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class WorkRequestService
{
    SubSonic.StoredProcedure sp;
    WorkRequestModel WR;
    List<WorkRequestModel> WRList;
    DataSet ds;

    public object ViewTotalRequests(WorkRequestDetails WRS)
    {
        try
        {
            WRList = GetWRDetails(WRS);
            if (WRList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = WRList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public object PropertyDetails(WorkRequestDetails PModel)
    {
        try
        {
            WRList = PropertyRequestDetails(PModel);
            if (WRList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = WRList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<WorkRequestModel> GetWRDetails(WorkRequestDetails WRS)
    {
        try
        {
            List<WorkRequestModel> VMlist = new List<WorkRequestModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_TOTAL_REQUESTS");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANY", WRS.CNP_NAME, DbType.String);
            sp.Command.AddParameter("@SEARCHVAL", WRS.SearchValue, DbType.String);
            sp.Command.AddParameter("@PAGENUM", WRS.PageNumber, DbType.String);
            sp.Command.AddParameter("@PAGESIZE", WRS.PageSize, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                ds = sp.GetDataSet();

                while (reader.Read())
                {
                    WR = new WorkRequestModel();
                    WR.PM_CODE = reader["PM_CODE"].ToString();
                    WR.PM_NAME = reader["PM_NAME"].ToString();
                    WR.TOTAL_WR = Convert.ToInt32(reader["TOTAL_WR"]);
                    WR.OVERALL_COUNT = Convert.ToString(reader["OVERALL_COUNT"]);
                    VMlist.Add(WR);
                }
                reader.Close();
            }
            return VMlist;
        }
        catch
        {
            throw;
        }
    }

    public List<WorkRequestModel> PropertyRequestDetails(WorkRequestDetails PModel)
    {
        try
        {
            DateTime? dt = null;
            List<WorkRequestModel> Prlist = new List<WorkRequestModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_PROPERTY_DETAILS");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
            sp.Command.AddParameter("@PROPERTY", PModel.Property, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                ds = sp.GetDataSet();

                while (reader.Read())
                {
                    WR = new WorkRequestModel();
                    WR.PM_WR_REQ_ID = reader["PM_WR_REQ_ID"].ToString();
                    WR.PM_WR_CREATED_BY = reader["PM_WR_CREATED_BY"].ToString();
                    WR.STA_TITLE = reader["STA_TITLE"].ToString();
                    WR.PM_WR_CREATED_DT = reader["PM_WR_CREATED_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PM_WR_CREATED_DT"]);
                    WR.PM_UWR_CREATED_DT = reader["PM_UWR_CREATED_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PM_UWR_CREATED_DT"]);
                    Prlist.Add(WR);
                }
                reader.Close();
            }
            return Prlist;
        }
        catch
        {
            throw;
        }
    }

    public List<WorkRequestModel> GetCompleteWR(WorkRequestDetails PModel)
    {
        try
        {
            List<WorkRequestModel> WRlist = new List<WorkRequestModel>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_WR_DETAILS");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
            sp.Command.AddParameter("@REQ", PModel.RequestId, DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                ds = sp.GetDataSet();

                while (reader.Read())
                {
                    WR = new WorkRequestModel();
                    WR.CTY_NAME = reader["CTY_NAME"].ToString();
                    WR.LCM_NAME = reader["LCM_NAME"].ToString();
                    WR.PM_PPT_NAME = reader["PM_PPT_NAME"].ToString();
                    WR.PN_PROPERTYTYPE = reader["PN_PROPERTYTYPE"].ToString();
                    WR.AVR_NAME = reader["AVR_NAME"].ToString();
                    WR.PM_WR_VEN_ADDRESS = reader["PM_WR_VEN_ADDRESS"].ToString();
                    WR.PM_WR_VEN_PH_NO = reader["PM_WR_VEN_PH_NO"].ToString();
                    WRlist.Add(WR);
                }
                reader.Close();
            }
            return WRlist;
        }
        catch
        {
            throw;
        }
    }
}