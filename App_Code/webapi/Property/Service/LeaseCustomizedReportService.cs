﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class LeaseCustomizedReportService
{
    SubSonic.StoredProcedure sp;
    List<LeaseCustomizedData> Cust;
    LeaseCustomizedData Custm;
    DataSet ds;

    public object GetCustomizedObject(LeaseCustomizedDetails Det)
    {
        try
        {
            Cust = GetCustomizedDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    public object getProjects()
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PCL_GET_PROJECTTYPES_REPORT");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public List<LeaseCustomizedData> GetCustomizedDetails(LeaseCustomizedDetails Details)
    {
        try
        {
            List<LeaseCustomizedData> CData = new List<LeaseCustomizedData>();
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Details.loclst);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[2].Value = Details.FromDate;
            param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[3].Value = Details.ToDate;
            if (Details.CNP_NAME == null)
            {
                param[4] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[4].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[4] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                param[4].Value = Details.CNP_NAME;
            }
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[5].Value = Details.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[6].Value = Details.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[7].Value = Details.PageSize;
            param[8] = new SqlParameter("@Entity", SqlDbType.Structured);
            param[8].Value = UtilityService.ConvertToDataTable(Details.entitylst);
            param[9] = new SqlParameter("@Propstatus", SqlDbType.Structured);
            param[9].Value = UtilityService.ConvertToDataTable(Details.prptystats);
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_CUSTOMIZED_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new LeaseCustomizedData();
                    Custm.PM_PPT_NAME = Convert.ToString(reader["PM_PPT_NAME"]);
                    Custm.LEASE_NAME = Convert.ToString(reader["LEASE_NAME"]);
                    Custm.AGREEMENT_TYPE = reader["AGREEMENT_TYPE"].ToString();
                    Custm.SUB_GROUP = reader["SUB_GROUP"].ToString();
                    Custm.AGREE_START_DATE = reader["AGREE_START_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["AGREE_START_DATE"]);
                    Custm.LEASE_START_DATE = reader["LEASE_START_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["LEASE_START_DATE"]);
                    Custm.LEASE_END_DATE = (reader["LEASE_END_DATE"]) == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["LEASE_END_DATE"]);
                    //Custm.EXTESNION_TODATE = (DateTime)(reader["EXTENSION_TODATE"]);
                    Custm.EXTESNION_TODATE = reader["EXTENSION_TODATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["EXTENSION_TODATE"]);
                    Custm.COUNTRY = Convert.ToString(reader["CNY_NAME"]);
                    Custm.CITY = Convert.ToString(reader["CTY_NAME"]);
                    Custm.LOCATION = Convert.ToString(reader["LCM_NAME"]);
                    Custm.PPT_ESTD = reader["PM_PPT_ESTD"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["PM_PPT_ESTD"]);
                    Custm.branch_code = reader["branch_code"].ToString();
                    Custm.STATE = reader["STATE"].ToString();
                    Custm.Request_type = reader["pm_ppt_req_type"].ToString();
                    //Custm.agreement_type = reader["pm_ppt_nature"].ToString();
                    Custm.COMPANYID = reader["COMPANYID"].ToString();
                    Custm.CARPET_AREA = Convert.ToString(reader["PM_AR_CARPET_AREA"]);
                    Custm.RENT_AREA = Convert.ToString(reader["PM_AR_RENT_AREA"]);
                    Custm.BUILT_UP = Convert.ToString(reader["PM_AR_BUA_AREA"]);
                    Custm.LANDLORD_NAME = Convert.ToString(reader["LANDLORD_NAME"]);
                    Custm.LANDLORD_ADDRESS = Convert.ToString(reader["LANDLORD_ADDRESS"]);
                    Custm.Rent_type = Convert.ToString(reader["Rent_type"]);
                    Custm.MONTHLY_RENT = Convert.ToString(reader["MONTHLY_RENT"]);
                    Custm.PN_PROPERTYTYPE = Convert.ToString(reader["PN_PROPERTYTYPE"]);
                    Custm.PM_PPT_ADDRESS = Convert.ToString(reader["PM_PPT_ADDRESS"]);
                    Custm.SECURITY_DEPOSIT = Convert.ToString(reader["SECURITY_DEPOSIT"]);
                    Custm.MAINTENANCE_CHARGES = Convert.ToString(reader["MAINTENANCE_CHARGES"]);
                    Custm.Total_Rent = Convert.ToString(reader["pm_les_tot_rent"]);
                    Custm.Amenities = Convert.ToString(reader["PM_LC_FF_CHARGES"]);
                    Custm.LEASE_ESCALATION = Convert.ToString(reader["LEASE_ESCALATION"]);
                    Custm.lease_payment_terms = reader["LEASE_PAYMENT_TERMS"].ToString();
                    Custm.LEASE_STATUS = reader["LEASE_STATUS"].ToString();
                    Custm.STATUS = Convert.ToString(reader["STATUS"]);
                    Custm.ESCLATIONDATE = reader["ESCLATIONDATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["ESCLATIONDATE"]);
                    Custm.Zone = reader["lcm_zone"].ToString();
                    Custm.PM_PPT_INSPECTED_DT = reader["PM_PPT_INSPECTED_DT"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["PM_PPT_INSPECTED_DT"]);
                    Custm.PM_PPT_INSPECTED_BY = Convert.ToString(reader["PM_PPT_INSPECTED_BY"]);
                    Custm.PM_PPT_POA = Convert.ToString(reader["PM_PPT_POA"]);
                    Custm.PM_PPT_SIG_LENGTH = reader["PM_PPT_SIG_LENGTH"].ToString();
                    Custm.PM_PPT_SIG_WIDTH = Convert.ToString(reader["PM_PPT_SIG_WIDTH"]);
                    Custm.PM_PPT_AC_OUTDOOR = Convert.ToString(reader["PM_PPT_AC_OUTDOOR"]);
                    Custm.PM_PPT_GSB = Convert.ToString(reader["PM_PPT_GSB"]);
                    Custm.ESCPERCENT = Convert.ToDouble(reader["PM_LLD_PERCENT"]);
                    Custm.REQ_STATUS = Convert.ToString(reader["REQ_STATUS"]);
                    Custm.SURRENDER_DATE = reader["PM_SL_SURRENDER_DT"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["PM_SL_SURRENDER_DT"]);
                    Custm.SURRENDER_REFUNDDATE = reader["PM_SL_REFUND_DT"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["PM_SL_REFUND_DT"]);
                    Custm.S_OUT_STANDINGAMOUNT = Convert.ToString(reader["PM_SL_OUTSTAND_AMT"]);
                    Custm.OVERALL_COUNT = Convert.ToString(reader["OVERALL_COUNT"]);
                    Custm.PM_LES_REMARKS = Convert.ToString(reader["PM_LES_REMARKS"]);
                    Custm.PM_LES_REN_L1_REMARKS = Convert.ToString(reader["PM_LES_REN_L1_REMARKS"]);
                    Custm.PM_SL_REMARKS = Convert.ToString(reader["PM_SL_REMARKS"]);
                    Custm.PM_LE_REMARKS = Convert.ToString(reader["PM_LE_REMARKS"]);
                    Custm.GST = Convert.ToString(reader["GST"]);
                    Custm.GST_NO = Convert.ToString(reader["GSTNO"]);
                    Custm.NOT_PIRED = reader["NOT_PIRED"].ToString();
                    Custm.LOC_IN = reader["LOC_IN"].ToString();
                    Custm.RENT_FREE = reader["RENT_FREE"].ToString();
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}