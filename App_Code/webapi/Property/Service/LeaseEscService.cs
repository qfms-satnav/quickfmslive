﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

/// <summary>
/// Summary description for LeaseEscService
/// </summary>
public class LeaseEscService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object GetPoprtyData()
    {
        try
        {
            List<LeaseEscVM> proplst = new List<LeaseEscVM>();
            LeaseEscVM prop;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_PROPERTYNAME");
            //sp.Command.AddParameter("@PM_PROPERTY", HttpContext.Current.Session["UID"], DbType.String);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    prop = new LeaseEscVM();
                    prop.PM_PPT_NAME = sdr["PM_PPT_NAME"].ToString();
                    prop.PM_PPT_CODE = sdr["PM_PPT_CODE"].ToString();
                    proplst.Add(prop);
                }
            }
            if (proplst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = proplst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }


    public object GetLeaseinfobyReqid(LeaseEscVMs obj)
    {


        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PM_BIND_LEASE_BASIC_INFO");
        sp.Command.Parameters.Add("@PM_PROPERTY", obj.PM_PPT_CODE, DbType.String);
        sp.Command.Parameters.Add("@EscalationOn", obj.ESCALATION, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetLandlordinfobyReqid(LeaseEscVMs obj)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PM_BIND_LANDLORD_BASIC_INFO");
        sp.Command.Parameters.Add("@LLNAME", obj.PM_LL_NAME, DbType.String);
        sp.Command.Parameters.Add("@PM_LL_PM_LR_REQ_ID", obj.PM_LL_PM_LR_REQ_ID, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object SaveLeaseData(LeaseSave leaseDetails)
    {
        int RETVAL = 0;
        if (leaseDetails.EscalationAmountList != null && leaseDetails.EscalationAmountList.Any())
        {
            List<EscalationAmount> escalationAmounts = new List<EscalationAmount>();
            foreach (var escalation in leaseDetails.EscalationAmountList)
            {
                var Dateformats = new[] { "dd-MM-yyyy", "d-MM-yyyy", "MM-dd-yyyy", "M-dd-yyyy", "yyyy-MM-dd", "yyyy-dd-MM", "yyyy-M-dd", "yyyy-M-d", "yyyy-MM-d" };
                DateTime startDate;
                DateTime.TryParseExact(escalation.StartDate, Dateformats, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                DateTime endDate;
                DateTime.TryParseExact(escalation.EndDate, Dateformats, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);

                var differenceList = Enumerable.Range(0, 1000).Select(a => startDate.AddMonths(a))
           .TakeWhile(a => a <= endDate.AddDays(DateTime.DaysInMonth(endDate.Year, endDate.Month) - endDate.Day))
           .Select(a => String.Concat(a.Year + "-" + a.Month + "-" + a.Day)).ToList();

                if (differenceList.Count > 2)
                {
                    DateTime fromDate = Convert.ToDateTime(differenceList[differenceList.Count - 1]);
                    var monthsDiff = endDate.Subtract(fromDate).Days / (365.25 / 12);
                    if (monthsDiff > 0)
                    {
                        // differenceList.Add(fromDate.AddDays(-1).AddMonths(1).ToString("yyyy-MM-dd"));
                        differenceList.Add(fromDate.AddMonths(1).ToString("yyyy-MM-dd"));
                    }
                }
                for (int i = 1; i < differenceList.Count(); i++)
                {
                    if (differenceList.Count == (i+1))
                    {
                       
                        differenceList[i] = endDate.ToString("yyyy-MM-dd");
                    }
                    EscalationAmount escalationAmount = new EscalationAmount()
                    {
                        StartDate = Convert.ToDateTime(differenceList[i - 1]).ToString("yyyy-MM-dd"),
                        EndDate = Convert.ToDateTime(differenceList[i]).ToString("yyyy-MM-dd"),
                        Amount = escalation.Amount == 0 ? 0 : escalation.MonthWiseAmount,
                        IndexNo = escalation.IndexNo,
                        Type = escalation.Type,
                        VALUE = escalation.VALUE,
                        Amountin = leaseDetails.EscalationTypeDetails.Amountin,
                        EscalationType = leaseDetails.EscalationTypeDetails.ESCType,
                        IntervalDuration = leaseDetails.EscalationTypeDetails.Interval_Duration,
                        IntervalType = leaseDetails.EscalationTypeDetails.Intrveltype,
                        Lease = leaseDetails.EscalationTypeDetails.Lease
                        //PM_LAD_START_DT_AGREEMENT= leaseDetails.PropertyBasicDetails.PM_LAD_START_DT_AGREEMENT,
                        //PM_LAD_EXP_DT_AGREEMENT = leaseDetails.PropertyBasicDetails.PM_LAD_EXP_DT_AGREEMENT,
                        //LeaseID=leaseDetails.PropertyBasicDetails.PM_LR_REQ_ID


                    };
                    escalationAmounts.Add(escalationAmount);

                }


            }
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@ESC_DET", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(escalationAmounts);
            param[1] = new SqlParameter("@LEASE_REQID", SqlDbType.NVarChar);
            param[1].Value = leaseDetails.PropertyBasicDetails.PM_LR_REQ_ID;
            param[2] = new SqlParameter("@LEASE_SNO", SqlDbType.NVarChar);
            param[2].Value = leaseDetails.PropertyBasicDetails.PM_LES_SNO;
            param[3] = new SqlParameter("@LEASE_PROP_CODE", SqlDbType.NVarChar);
            param[3].Value = leaseDetails.PropertyBasicDetails.PM_PPT_CODE;
            param[4] = new SqlParameter("@LEASE_PROP_NAME", SqlDbType.NVarChar);
            param[4].Value = leaseDetails.PropertyBasicDetails.PM_PPT_NAME;
            param[5] = new SqlParameter("@LEASE_LL_NAME", SqlDbType.NVarChar);
            param[5].Value = leaseDetails.PropertyBasicDetails.PM_LL_NAME;
            param[6] = new SqlParameter("@LEASE_LL_SNO", SqlDbType.NVarChar);
            param[6].Value = leaseDetails.PropertyBasicDetails.PM_LL_SNO;
            param[7] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[7].Value = HttpContext.Current.Session["COMPANYID"];
            param[8] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[8].Value = HttpContext.Current.Session["UID"];

            RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_LEASE_ESC_INSERT_DATA", param);

        }
        if (RETVAL == 1)
            return new { Message = MessagesVM.UM_OK, data = "SUCCESS" };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    IEnumerable<DateTime> GetDates(DateTime date1, DateTime date2)
    {
        while (date1 <= date2)
        {
            yield return date1;
            date1 = date1.AddMonths(1);
        }

        if (date1 > date2)
        {
            // Include the last month
            yield return date1;
        }
    }

}


