﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for LeaseReportService
/// </summary>
public class LeaseReportService
{
    SubSonic.StoredProcedure sp;
    List<LeaseReportModel> Leaselist;
    LeaseReportModel Lease;
    DataSet ds;
    public object GetLeaseData(Company Propdetails)
    {
        try
        {
            Leaselist = LeaseReportDetails(Propdetails);
            if (Leaselist.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Leaselist };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<LeaseReportModel> LeaseReportDetails(Company Propdetails)
    {
        try
        {
            List<LeaseReportModel> LeaseData = new List<LeaseReportModel>();

            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@FRMDT", SqlDbType.DateTime);
            param[0].Value = Propdetails.FromDate;
            param[1] = new SqlParameter("@TODT", SqlDbType.DateTime);
            param[1].Value = Propdetails.ToDate;
            param[2] = new SqlParameter("@AURID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            if (Propdetails.CNP_NAME == null)
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = Propdetails.CNP_NAME;
            }
            param[4] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(Propdetails.loclst);
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[5].Value = Propdetails.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.Int);
            param[6].Value = Propdetails.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.Int);
            param[7].Value = Propdetails.PageSize;




            //        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_REPORT", param))
            //        {

            //            while (reader.Read())
            //            {
            //                Lease = new LeaseReportModel();
            //                Lease.PROP_TYPE = reader["PROP_TYPE"].ToString();
            //                Lease.PRP_NAME = reader["PRP_NAME"].ToString();
            //                Lease.LEASE_SDATE = reader["LEASE_SDATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["LEASE_SDATE"]);
            //                Lease.LEASE_EDATE = reader["LEASE_EDATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["LEASE_EDATE"]);
            //                Lease.MON_RENT = reader["MONTHLY_RENT"].ToString();
            //                Lease.SEC_DEPOSIT = reader["SECURITY_DEPOSIT"].ToString();
            //                Lease.TOTAL_RENT = reader["TOTAL_RENT"].ToString();
            //                Lease.branch_code = reader["branch_code"].ToString();
            //                Lease.STATE = reader["STATE"].ToString();
            //                Lease.MAINT_CHRG = reader["PM_LL_MAINT_CHARGES"].ToString();
            //                Lease.LL_NUM = reader["PM_LL_NUMBER"].ToString();
            //                Lease.BRO_NAME = reader["BROKER_NAME"].ToString();
            //                Lease.BRO_FEE = reader["BROKER_FEE"].ToString();
            //                Lease.LAND_NAME = reader["LANDLORD_NAME"].ToString();
            //                Lease.LAND_ADDR = reader["LANDLORD_ADDRESS"].ToString();
            //                Lease.LAND_RENT = reader["LANDLORD_RENT"].ToString();
            //                Lease.LAND_SEC_DEP = reader["LANDLORD_DEPOSIT"].ToString();
            //                Lease.LAND_GST = reader["LAND_GST"].ToString();
            //                Lease.landlord_phone = reader["LANDLORD_NUMBER"].ToString();
            //                Lease.lease_payment_terms = reader["LEASE_PAYMENT_TERMS"].ToString();
            //                Lease.CHE_NAME = reader["CHE_NAME"].ToString();
            //                Lease.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();

            //                LeaseData.Add(Lease);
            //            }
            //            reader.Close();
            //        }

            //        return LeaseData;
            //    }
            //    catch(Exception ex)
            //    {
            //        throw;
            //    }
            //}

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_REPORT", param))
            {

                while (reader.Read())
                {
                    Lease = new LeaseReportModel();
                    Lease.PROP_TYPE = reader["PROP_TYPE"].ToString();
                    Lease.PRP_NAME = reader["PRP_NAME"].ToString();
                    Lease.LEASE_SDATE = reader["LEASE_SDATE"].ToString();
                    Lease.LEASE_EDATE = reader["LEASE_EDATE"].ToString();
                    Lease.MON_RENT = reader["MONTHLY_RENT"].ToString();
                    Lease.SEC_DEPOSIT = reader["SECURITY_DEPOSIT"].ToString();
                    Lease.TOTAL_RENT = reader["TOTAL_RENT"].ToString();
                    Lease.branch_code = reader["branch_code"].ToString();
                    Lease.STATE = reader["STATE"].ToString();
                    Lease.MAINT_CHRG = reader["PM_LL_MAINT_CHARGES"].ToString();
                    Lease.PM_LES_NO_OF_LL = reader["PM_LES_NO_OF_LL"].ToString();
                    Lease.BRO_NAME = reader["BROKER_NAME"].ToString();
                    Lease.BRO_FEE = reader["BROKER_FEE"].ToString();
                    Lease.PM_LES_NO_OF_LL = reader["PM_LES_NO_OF_LL"].ToString();
                    Lease.LAND_NAME = reader["LANDLORD_NAME"].ToString();
                    Lease.LAND_ADDR = reader["LANDLORD_ADDRESS"].ToString();
                    Lease.LAND_RENT = reader["LANDLORD_RENT"].ToString();
                    Lease.LAND_SEC_DEP = reader["LANDLORD_DEPOSIT"].ToString();
                    Lease.LAND_GST = reader["LAND_GST"].ToString();
                    Lease.LANDLORD_NUMBER = reader["LANDLORD_NUMBER"].ToString();
                    Lease.LANDLORD_EMAIL = reader["LANDLORD_EMAIL"].ToString();
                    Lease.lease_payment_terms = reader["LEASE_PAYMENT_TERMS"].ToString();
                    Lease.CHE_NAME = reader["CHE_NAME"].ToString();
                    Lease.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    Lease.LANDLORD_BANK = reader["LANDLORD_BANK"].ToString();
                    Lease.LANDLORD_ACCOUNT = reader["LANDLORD_ACCOUNT"].ToString();
                    Lease.LANDLORD_IFSC = reader["LANDLORD_IFSC"].ToString();
                    Lease.PM_LL_ADDRESS2 = reader["PM_LL_ADDRESS2"].ToString();
                    Lease.LANDLORD_GST_NO = reader["LANDLORD_GST_NO"].ToString();
                    Lease.LANDLORD_PAN = reader["LANDLORD_PAN"].ToString();
                    Lease.VENDER_CODE = reader["VENDER_CODE"].ToString();
                    Lease.STATE_NAME = reader["STATE_NAME"].ToString();
                    Lease.LOC_NAME = reader["LOC_NAME"].ToString();
                    Lease.ZONE_NAME = reader["ZONE_NAME"].ToString();
                    Lease.FLOOR_NAME = reader["FLOOR_NAME"].ToString();
                    Lease.PPT_ADDRESS = reader["PPT_ADDRESS"].ToString();
                    Lease.CRAPET_AREA = reader["CRAPET_AREA"].ToString();
                    Lease.BUILT_AREA = reader["BUILT_AREA"].ToString();
                    Lease.Property_Code = reader["Property_Code"].ToString();
                    Lease.SEC_DEPOSITMONTH = reader["SEC_DEPOSITMONTH"].ToString();
                    Lease.ESC_1 = reader["ESC_1"].ToString();
                    Lease.LOCK_IN = reader["LOCK_IN"].ToString();
                    Lease.NOTICE_PERIOD = reader["NOTICE_PERIOD"].ToString();
                    Lease.RENTFREE_PERIOD = reader["RENTFREE_PERIOD"].ToString();
                    Lease.LANDLORD_NO = reader["LANDLORD_NO"].ToString();
                    Lease.LL_STATE = reader["LL_STATE"].ToString();
                    Lease.LL_CITY = reader["LL_CITY"].ToString();
                    Lease.LL_PIN = reader["LL_PIN"].ToString();
                    Lease.MAINT_CHARG = reader["MAINT_CHARG"].ToString();
                    Lease.AMNI_CHARG = reader["AMNI_CHARG"].ToString();
                    Lease.STATUS1 = reader["STATUS1"].ToString();
                    Lease.Branch_Status = reader["Branch_Status"].ToString();

                    LeaseData.Add(Lease);
                }
                reader.Close();
            }

            return LeaseData;
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string GetTotlaSum()
    {
        //object Tol_Amount = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_LEASE_LAST_3MONTHS_RENT");
        
       DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PM_LEASE_LAST_3MONTHS_RENT");
       string val = ds.Tables[0].Rows[0]["LAST_3MONTHS_TOTAL_RENT"].ToString(); 
        return val;
    }


}