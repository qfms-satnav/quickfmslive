﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


/// <summary>
/// Summary description for LeaseBudgetService
/// </summary>
public class LeaseBudgetService
{
    SubSonic.StoredProcedure sp;
    List<CustomizedData> Cust;
    CustomizedData Custm;
    DataSet ds;

    public object GetPoprtyData(LeaseBudgetModel Details)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["Uid"];
        param[1] = new SqlParameter("@PM_ENTITY", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(Details.propertyLists);
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "[BIND_PROPERTYNAME_ByEntity]", param);
        return new { data = ds.Tables[0] };

    }


    public object BindPropType(LeaseBudgetModel Details)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["Uid"];
            param[1] = new SqlParameter("@PM_ENTITY", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.propertyLists);
            //param[2] = new SqlParameter("PN_PROPERTYTYPE", SqlDbType.NVarChar);
            //param[2].Value = UtilityService.ConvertToDataTable(Details.ddlPropertyType);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "[BIND_PROPERTYTYPE_ByEntity]", param);
            return new { data = ds.Tables[0] };

        }
        catch (Exception e)
        {
            throw e;
        }

    }




    public object GetCustomizedObject(LeaseBudgetModel Details)
    //public List<LeaseCustomizedData> GetCustomizedDetails(LeaseCustomizedDetails Details)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["Uid"];
            //param[1] = new SqlParameter("@Entity", SqlDbType.Structured);
            //param[1].Value = UtilityService.ConvertToDataTable(Details.entitylst);
            param[1] = new SqlParameter("@Entity", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.propertyLists);
            param[2] = new SqlParameter("@year", SqlDbType.NVarChar);
            param[2].Value = Details.year;

            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PM_LEASE_BUDGET_RPT", param);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                       select dc.ColumnName).ToList();
                foreach (string col in Colstr)
                {
                    gridcolscls = new CustomizedGridCols();
                    gridcolscls.cellClass = "grid-align";
                    gridcolscls.field = col;
                    gridcolscls.headerName = col;
                    gridcols.Add(gridcolscls);
                }
                return new { griddata = ds.Tables[0], Coldef = gridcols };
            }
            return new { griddata = (object)null, Coldef = (object)null };
        }
        catch (SqlException) { throw; }
        //catch (Exception)
        //{
        //    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        //}

    }

    //public List<SpaceConsolidatedReportMancom> GetMancom(SpaceConsolidatedParameters Params)
    public object GetMonthwise(LeaseBudgetModel Details)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["Uid"];
            param[1] = new SqlParameter("@Entity", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.propertyLists);
            param[2] = new SqlParameter("@year", SqlDbType.NVarChar);
            param[2].Value = Details.year;

            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PM_LEASE_MONTH_WISE_BUDGET_RPT", param);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
            }
            return new { griddata = (object)null, Coldef = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
}