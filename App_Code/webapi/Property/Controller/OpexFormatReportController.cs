﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;
public class OpexFormatReportController : ApiController
{

    OpexFormatReportService Csvc = new OpexFormatReportService();
    ////LeaseCustomizedReportView report = new LeaseCustomizedReportView();

   [HttpPost]
   // [HttpGet]
    public HttpResponseMessage GetOpexFormatDetails(OpexFormatDetails Empdata)
    {
        var obj = Csvc.GetOpexFormatObject(Empdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage getProjects()
    {
        //return Csvc.getProjects();
        var obj = Csvc.getProjects();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpPost]
    //public async Task<HttpResponseMessage>GetOpexFormatData([FromBody] OpexFormatDetails data)
    //{
    //    try
    //    {
    //        ReportGenerator<OpexFormatData> reportgen = new ReportGenerator<OpexFormatData>()
    //        {
    //            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/OpexFormatReport.rdlc"),
    //            DataSetName = "OpexFormatDataReport",
    //            ReportType = "Opex Format Report"
    //        };

    //        Csvc = new OpexFormatReportService();
    //        List<OpexFormatData> reportdata = Csvc.GetOpexFormatDetails(data);
    //        //string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/OpexFormatReport." + data.Type);
    //        //await reportgen.GenerateReport(reportdata, filePath, data.Type);
    //        HttpResponseMessage result = null;
    //        //result = Request.CreateResponse(HttpStatusCode.OK);
    //        //result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
    //        //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
    //        //result.Content.Headers.ContentDisposition.FileName = "OpexFormatReport." + data.Type;
    //        //return result;
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}
}

