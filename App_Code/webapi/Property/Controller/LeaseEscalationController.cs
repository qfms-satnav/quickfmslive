﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class LeaseEscalationController : ApiController
{
    LeaseEscalationService LeaseEscalationService = new LeaseEscalationService();
    [HttpGet]
    public object GetSLATime()
    {
        return LeaseEscalationService.GetSLATime();
    }
    //Grid View
    [HttpGet]
    public HttpResponseMessage GetSLAGrid()
    {
        IEnumerable<LeaseEsc> SLAlist = LeaseEscalationService.GetSLAList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SLAlist);
        return response;
    }

    //To save records

    [HttpPost]
    public HttpResponseMessage SaveDetails(LeaseEscalationModel dataobject)
    {

        if (LeaseEscalationService.SaveDetails(dataobject).SLA_ID == 0)
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { msg = "SLA already exists with same details" });

        }
        else
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, new { msg = "Data Inserted Successfully" });
            return response;
        }
    }


    //edit
    [HttpGet]
    public object EditSLA(int id)
    {
        return LeaseEscalationService.EditSLADetails(id);

    }

    //To Update records
    [HttpPost]
    public HttpResponseMessage UpdateDetails(LeaseEscalationModel dataobject)
    {


        if (LeaseEscalationService.UpdateDetails(dataobject).SLA_ID == 0)
        {
            return Request.CreateResponse(HttpStatusCode.OK, new { msg = "SLA Already Exists With Same Details" });
        }
        else
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, new { msg = "Data Updated Successfully" });
            return response;
        }
    }



}
