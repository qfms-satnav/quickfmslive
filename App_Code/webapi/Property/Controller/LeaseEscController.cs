﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UtiltiyVM;


public class LeaseEscController : ApiController
{
    // GET api/<controller>
    LeaseEscService LE = new LeaseEscService();
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetPoprtyData()
    {
        var obj = LE.GetPoprtyData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // GET Equipment
    [GzipCompression]
    [HttpPost]
    public Object GetLeaseinfobyReqid([FromBody] LeaseEscVMs obj)
    {
        return LE.GetLeaseinfobyReqid(obj);
    }
    [GzipCompression]
    [HttpPost]
    public Object GetLandlordinfobyReqid([FromBody] LeaseEscVMs obj)
    {
        return LE.GetLandlordinfobyReqid(obj);
    }
    //[HttpPost]
    //public HttpResponseMessage SaveLeaseData(LeaseSave leaseDetails)
    //{
    //    var test = 1;
    //    LE.SaveLeaseData(leaseDetails);
    //    return new HttpResponseMessage();
    //}
    [GzipCompression]
    [HttpPost]
    public Object SaveLeaseData(LeaseSave leaseDetails)
    {
        var test = 1;
        var obj = LE.SaveLeaseData(leaseDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //[GzipCompression]
    //[HttpGet]
    //public HttpResponseMessage GetLeaseinfobyReqid(LeaseEscVM data)
    //{
    //    var list = LE.GetLeaseinfobyReqid(data);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, list);
    //    return response;
    //}

}
