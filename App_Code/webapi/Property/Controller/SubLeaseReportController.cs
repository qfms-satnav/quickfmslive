﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class SubLeaseReportController : ApiController
{
    SubLeaseReportService slr = new SubLeaseReportService();


    [HttpPost]
    public HttpResponseMessage GetGriddata(SubLeaseReportModel SubLease)
    {
        var obj = slr.GetGridObject(SubLease);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetSubLeaseData([FromBody]SubLeaseReportModel data)
    {
        ReportGenerator<SubLeaseData> reportgen = new ReportGenerator<SubLeaseData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/SubLeaseReport.rdlc"),
            DataSetName = "SubLeaseReport",
            ReportType = "Sub Lease Report",
            
        };

        slr = new SubLeaseReportService();
        DataTable reportdata = slr.GetGriddata(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SubLeaseReport.rdlc" + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SubLeaseReport." + data.Type;
        return result;
    }
}
