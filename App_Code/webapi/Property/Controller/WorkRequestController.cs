﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class WorkRequestController : ApiController
{
    WorkRequestService WR = new WorkRequestService();

    [HttpPost]
    public HttpResponseMessage GetWR(WorkRequestDetails WRS)
    {
        var obj = WR.ViewTotalRequests(WRS);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetPropertyDetails(WorkRequestDetails Property)
    {
        var obj = WR.PropertyRequestDetails(Property);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCompleteWR(WorkRequestDetails Property)
    {
        var obj = WR.GetCompleteWR(Property);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetAllProperties([FromBody] WorkRequestDetails obj)
    {
        ReportGenerator<WorkRequestModel> reportgen = new ReportGenerator<WorkRequestModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/WorkRequestReport.rdlc"),
            DataSetName = "WorkRequestDetails",
            ReportType = "Work Request Details"
        };

        WR = new WorkRequestService ();
        List<WorkRequestModel> reportdata = WR.GetCompleteWR(obj);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/WorkRequest." + obj.Type);
        await reportgen.GenerateReport(reportdata, filePath, obj.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "WorkRequest." + obj.Type;
        return result;
    }
}
