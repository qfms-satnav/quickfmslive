﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class LeaseRentalReportController : ApiController
{

    LeaseRentalReportService LRR = new LeaseRentalReportService();

    [HttpPost]
    public HttpResponseMessage GetGriddata(Company Propdetails)
    {
        var obj = LRR.GetLeaseData(Propdetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(Company data)
    {
        try
        {
            ReportGenerator<LeaseRentalReportModel> reportgen = new ReportGenerator<LeaseRentalReportModel>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/LeaseRentalReport.rdlc"),
                DataSetName = "LeaseRentalReport",
                ReportType = "Lease Rental Report",

            };
            LRR = new LeaseRentalReportService();
            List<LeaseRentalReportModel> reportdata = LRR.LeaseReportDetails(data);
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/LeaseRentalReport." + data.Type);
            await reportgen.GenerateReport(reportdata, filePath, data.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "LeaseRentalReport." + data.Type;
            return result;
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    LeaseRentalConfirmationService SubReqs = new LeaseRentalConfirmationService();

    [HttpPost]
    public HttpResponseMessage UploadproprtyleaseRent()
    {

        var httpRequest = HttpContext.Current.Request;
        var obj = SubReqs.btnbrowse(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);

        return response;


    }
}