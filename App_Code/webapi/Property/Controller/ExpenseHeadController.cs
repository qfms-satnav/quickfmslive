﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ExpenseHeadController : ApiController
{
    // GET api/<controller>
    ExpenseHeadService ExpenseHeadService = new ExpenseHeadService();

    [HttpPost]
    public HttpResponseMessage BindExpenseHeadGrid()
    {
        var obj = ExpenseHeadService.BindExpenseHeadGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveData(ExpHeadModel Sdata)
    {
        if (ExpenseHeadService.SaveData(Sdata) == 0)
        {

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Sdata);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
        }

    }
    [HttpPost]
    public HttpResponseMessage UpdateExpenseHeadData(ExpHeadModel Udata)
    {
        if (ExpenseHeadService.UpdateExpenseHeadData(Udata))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Udata);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }
}
