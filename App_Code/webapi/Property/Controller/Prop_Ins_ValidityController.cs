﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class Prop_Ins_ValidityController : ApiController
{
    Prop_Ins_ValidityService valdService = new Prop_Ins_ValidityService();
    [HttpPost]
    public HttpResponseMessage GetGriddata(Prop_Ins_Validity_Details InsDetails)
    {
        var obj = valdService.GetGriddata(InsDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(Prop_Ins_Validity_Details InsDetails)
    {
        ReportGenerator<Prop_Ins_ValidityModel> reportgen = new ReportGenerator<Prop_Ins_ValidityModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/PropertyInsuranceReport.rdlc"),
            DataSetName = "PropertyInsuranceDS",
            ReportType = "Property Insurance Validity Report"
        };
        valdService = new Prop_Ins_ValidityService();
        List<Prop_Ins_ValidityModel> reportdata = valdService.GetInsList(InsDetails);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/PropertyInsuranceReport.rdlc." + InsDetails.Type);
        await reportgen.GenerateReport(reportdata, filePath, InsDetails.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "PropertyInsuranceReport." + InsDetails.Type;
        return result;
    }
}
