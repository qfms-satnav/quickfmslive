﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;



public class LeaseBudgetController : ApiController
{
    LeaseBudgetService Csvc = new LeaseBudgetService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(LeaseBudgetModel Empdata) 
    {
        var obj = Csvc.GetCustomizedObject(Empdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetPoprtyData(LeaseBudgetModel Details)
    {
        var obj = Csvc.GetPoprtyData(Details);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage BindPropType(LeaseBudgetModel Details)
    {
        var obj = Csvc.BindPropType(Details);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetMonthwise(LeaseBudgetModel Empdata)
    {
        var obj = Csvc.GetMonthwise(Empdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
