﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for TenantReportController
/// </summary>
public class TenantReportController:ApiController
{
    TenantReportService TRService = new TenantReportService();
    [HttpPost]
    public HttpResponseMessage GetTenantDetails(TenantReportModel TRM)
    {
        var obj = TRService.GetTenantObject(TRM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(TenantReportModel data)
    {
        //CultureInfo ci = new CultureInfo(HttpContext.Current.Session["userculture"].ToString());
        //NumberFormatInfo nfi = ci.NumberFormat;
        //ReportParameter p1 = new ReportParameter("Currencyparam", nfi.CurrencySymbol);

        ReportGenerator<TenantModels> reportgen = new ReportGenerator<TenantModels>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/TenantPropertiesReport.rdlc"),
            DataSetName = "TenantPropertiesReportDS",
            ReportType = "Tenant Report"
        };
        TRService = new TenantReportService();
        List<TenantModels> reportdata = TRService.GetTenantDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/TenantReport.rdlc" + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "TenantReport." + data.Type;
        return result;
    }
}