﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;


namespace LocWiseUtilityVModel
{

    public class LocWiseUtilityVM
    {
        public LocWiseUtilityModel LocWiseUtilityModel { get; set; }
    }

    public class LocWiseUtilityModel
    {

        public int UTIL_ID { get; set; }
        public string LCM_CODE { get; set; }
        public string EXP_CODE { get; set; }
        public string BILL_NO { get; set; }
        public string BILL_INVOICE { get; set; }
        public decimal? AMT { get; set; }
        public string FromDate { get; set; }
        public string VENDOR { get; set; }
        public string VEN_MAIL { get; set; }
        public string VEN_PHNO { get; set; }
        public string AUR_ID { get; set; }
        public string REM { get; set; }
        public string EXP_NAME { get; set; }
        public string LCM_NAME { get; set; }
        public string AUR_NAME { get; set; }
        public string STATUS { get; set; }
        public string DEP_CODE { get; set; }
        public string DEP_NAME { get; set; }
        public string ExpenseMonth { get; set; }
        public string CLIENTREMARKS { get; set; }

    }

    public class Month
    {
        public int id { get; set; }
        public string name { get; set; }

    }
    public class ExcelDownload
    {
        public string LCM_NAME { get; set; }
        public string EXP_NAME { get; set; }
        public string BILL_DATE { get; set; }
        public string BILL_NO { get; set; }
        public string BILL_INVOICE { get; set; }
        public string AMOUNT { get; set; }
        public string VENDOR { get; set; }
        public string VENDOR_EMAIL { get; set; }
        public string VENDOR_PHNO { get; set; }
        public string DEP_NAME { get; set; }
        public string REMARKS { get; set; }
        public string CLIENTREMARKS { get; set; }
    }


    public class UtilityExpenses
    {
        public string LCM_NAME { get; set; }
        public string EXP_NAME { get; set; }
        public string FromDate { get; set; }
        public string BILL_NO { get; set; }
        public string BILL_DATE { get; set; }
        public string BILL_INVOICE { get; set; }
        public string AMOUNT { get; set; }
        public string VENDOR { get; set; }
        public string VENDOR_EMAIL { get; set; }
        public string VENDOR_PHNO { get; set; }
        public string DEP_NAME { get; set; }
        public string ExpenseMonth { get; set; }
        public string CLIENTREMARKS { get; set; }
        public string LCMCODE { get; set; }
        public string EXPCODE { get; set; }
        public string DEPCODE { get; set; }
        public string ERRORCOMMENTS { get; set; }
    }
    public class ExpHeadUtilityModel
    {
        public string EXP_CODE { get; set; }
        public string EXP_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class ExpLocationVM
    {
        public string LCM_CODE { get; set; }
        public string LCM_NAME { get; set; }
        public string CTY_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class UserVM
    {
        public string AUR_ID { get; set; }
        public string AUR_NAME { get; set; }
    }
    public class DepVM
    {
        public string DEP_CODE { get; set; }
        public string DEP_NAME { get; set; }
    }
}