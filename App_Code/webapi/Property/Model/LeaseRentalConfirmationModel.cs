﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaseRentalConfirmationModel
/// </summary>

public class leaserentalpayment
{
    public string sno { get; set; }
    public string LEASE_ID { get; set; }
    public string STATE { get; set; }
    public string Branch_code { get; set; }
    public string Location { get; set; }
    public string Landlord_Name { get; set; }
    public string Landlord_Number { get; set; }
    public string Payment_terms { get; set; }
    public string LANDLORDAMOUNT { get; set; }
    public string Status { get; set; }
    public string MONTH { get; set; }
    public string YEAR { get; set; }
    public string Agreement_Start_Date { get; set; }
    public string Agreement_End_Date { get; set; }
    public string Company { get; set; }
    public string Remarks { get; set; }
    //public string DUE { get; set; }
    //public string YEAR { get; set; }
    //public string REMARKS { get; set; }
    //public string WITHHOLDAMOUNT { get; set; }

}

//public class SpaceConsolGridCols
//{
//    public string headerName { get; set; }
//    public string field { get; set; }
//    public int width { get; set; }
//    public string cellClass { get; set; }
//    public bool suppressMenu { get; set; }
//}

//public class SpaceConsolClass
//{
//    public string title { get; set; }
//    public string key { get; set; }
//}

