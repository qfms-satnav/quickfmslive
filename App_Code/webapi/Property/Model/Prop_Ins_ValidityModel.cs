﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Prop_Ins_ValidityModel
/// </summary>
public class Prop_Ins_ValidityModel
{
    public string PPT_TYPE { get; set; }
    public string PPT_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string PPT_ADDR { get; set; }
    public string OWNER { get; set; }
    public string INS_TYPE { get; set; }
    public string INS_VENDOR { get; set; }
    public double INS_AMOUNT { get; set; }
    public string INS_PNO { get; set; }
    public DateTime? INS_START_DT { get; set; }
    public DateTime? INS_END_DT { get; set; }
    public string OVERALL_COUNT { get; set; }
}

public class Prop_Ins_Validity_Details
{
    public string CNP_NAME { get; set; }
    public DateTime? FROM_DATE { get; set; }
    public DateTime? TO_DATE { get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}