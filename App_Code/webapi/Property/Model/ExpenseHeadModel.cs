﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ExpenseHeadModel
/// </summary>
public class ExpHeadModel
{
    public string EXP_CODE { get; set; }
    public string EXP_NAME { get; set; }
    public string EXP_STATUS { get; set; }
    public string EXP_REM { get; set; }
}