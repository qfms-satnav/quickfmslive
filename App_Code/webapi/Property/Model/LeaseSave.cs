﻿using System.Collections.Generic;
/// <summary>
/// Summary description for LeaseSave
/// </summary>
public class LeaseSave
{
    public LeaseEscVM PropertyBasicDetails { get; set; }
    public List<EscalationAmount> EscalationAmountList { get; set; }
    public EscalationTypeModel EscalationTypeDetails { get; set; }
}
