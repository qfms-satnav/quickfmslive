﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class SubLeaseReportModel
{

    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<Companylst> cpylst { get; set; }
    public string Type { get; set; }
	
}

public class Companylst
{
    public string CNP_ID { get; set; }
    public string CNP_NAME { get; set; }
    public bool ticked { get; set; }
}
public class SubLeaseData
{
    public string CNP_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string PM_SLA_PM_LR_REQ_ID { get; set; }
    public string PM_SLA_SUB_LES_REQ_ID { get; set; }
    public Nullable<System.DateTime> PM_SLA_AGREE_ST_DT { get; set; }
    public Nullable<System.DateTime> PM_SLA_AGREE_END_DT { get; set; }
    public string PM_AR_CARPET_AREA { get; set; }
    public string PM_SLA_AREA_OR_SEATING { get; set; }
    public string PM_SLA_COST { get; set; }
    public string PM_SLA_MAINT_CHARGES { get; set; }
    public string Total { get; set; }
    public string PM_SLA_PAY_TERMS { get; set; }
    public string STA_TITLE { get; set; }
    public string PM_SLA_APPR_REMARKS { get; set; }
    public string PM_SLA_REMARKS { get; set; }
    public string PM_LES_AGR_TYPE { get; set; }
    


}