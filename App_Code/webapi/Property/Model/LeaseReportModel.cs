﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaseReportModel
/// </summary>
//public class Company
//{
//    public string CNP_NAME { get; set; }
//    public string PLAN_ID { get; set; }
//    public string Type { get; set; }
//}
public class LeaseReportModel
{
    public string PROP_TYPE { get; set; }
    public string PRP_NAME { get; set; }
    //public string LEASE_SDATE { get; set; }
    //public string LEASE_EDATE { get; set; }
    public string LEASE_SDATE { get; set; }
    public string LEASE_EDATE { get; set; }
    public string MON_RENT { get; set; }
    public string SEC_DEPOSIT { get; set; }
    public string BRO_NAME { get; set; }
    public string BRO_FEE { get; set; }
    public string LAND_NAME { get; set; }
    public string LAND_ADDR { get; set; }
    public string LAND_RENT { get; set; }
    public string LAND_SEC_DEP { get; set; }
    public string branch_code { get; set; }
    public string STATE { get; set; }
    public string MAINT_CHRG { get; set; }
    public string LL_NUM { get; set; }
    public string LAND_GST { get; set; }
    public string TOTAL_RENT { get; set; }
    //public string STATE { get; set; }
    public string LANDLORD_NUMBER { get; set; }
    public string LANDLORD_EMAIL { get; set; }
    public string lease_payment_terms { get; set; }
    public string CHE_NAME { get; set; }
    public string OVERALL_COUNT { get; set; }
    public string LANDLORD_BANK { get; set; }
    public string LANDLORD_ACCOUNT { get; set; }
    public string LANDLORD_IFSC { get; set; }
    public string PM_LES_NO_OF_LL { get; set; }
    public string PM_LL_ADDRESS2 { get; set; }
    public string LANDLORD_GST_NO { get; set; }
    public string LANDLORD_PAN { get; set; }
    public string VENDER_CODE { get; set; }

    public string STATE_NAME { get; set; }
    public string LOC_NAME { get; set; }
    public string ZONE_NAME { get; set; }
    public string FLOOR_NAME { get; set; }
    public string PPT_ADDRESS { get; set; }
    public string CRAPET_AREA { get; set; }
    public string BUILT_AREA { get; set; }
    public string Property_Code { get; set; }
    public string SEC_DEPOSITMONTH { get; set; }
    public string ESC_1 { get; set; }
    public string LOCK_IN { get; set; }
    public string NOTICE_PERIOD { get; set; }
    public string RENTFREE_PERIOD { get; set; }
    public string LANDLORD_NO { get; set; }
    public string LL_STATE { get; set; }
    public string LL_CITY { get; set; }
    public string LL_PIN { get; set; }
    public string MAINT_CHARG { get; set; }
    public string AMNI_CHARG { get; set; }
    public string STATUS1 { get; set; }
    public string Branch_Status { get; set; }



}