﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for OpexFormatReportModel
/// </summary>
public class OpexFormatDetails
{
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<ChildEntityLst> entitylst { get; set; }
    public string ctylist { get; set; }
    public string loclist { get; set; }
    public string entitylist { get; set; }
    public string year { get; set; }
    public string month { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
}
public class OpexFormatData
{
    public string utlityexpenses { get; set; }
    public string location { get; set; }
    public string region { get; set; }
    public string legalentity{ get; set; }
    public string Rent { get; set; }

}
//public class ProjctstasLstc
//{
//    public string PROJECTCD { get; set; }
//    public string PROJECTNAM { get; set; }
//    public bool ticked { get; set; }

//}



