﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class TenantCustomizedDetails
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string CNP_NAME { get; set; }
    public string CNP_ID { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}

public class TenantCustomizedData
{
    public string PN_PROPERTYTYPE { get; set; }
    public string PM_PPT_CODE { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string PM_PPT_ADDRESS { get; set; }
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public double PM_AR_CARPET_AREA { get; set; }
    public string PM_TEN_CODE { get; set; }
    public string PM_OWN_NAME { get; set; }
    public string TEN_NAME { get; set; }
    public double PM_TD_RENT { get; set; }
    public double PM_TD_SECURITY_DEPOSIT { get; set; }
    public string PM_PT_NAME { get; set; }
    public double PM_TD_MAINT_FEES { get; set; }
    public double PARK_FEE { get; set; }
    public double PM_TD_TOT_RENT { get; set; }
    public string TEN_EMAIL { get; set; }
    public string TEN_STATUS { get; set; }
    public Nullable<System.DateTime> PM_TD_JOIN_DT { get; set; }
    public Nullable<System.DateTime> PM_TEN_FRM_DT { get; set; }
    public Nullable<System.DateTime> PM_TEN_TO_DT { get; set; }
    public string OVERALL_COUNT { get; set; }

}

public class TenantCustomizedReportView
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public TenantCustomizedReportView()
    {
        SpaceDatas = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> SpaceDatas { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case TenantCustomizedReportView.ReportFormat.Word: return ".doc";
                case TenantCustomizedReportView.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.SpaceDatas)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}