﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for LeaseBudgetModel
/// </summary>
/// 
public class LeaseBudgetModel
{
    //public List<ChildEntityLst> entitylst { get; set; }
    public List<PropertyList> propertyLists { get; set; }

    public string year { get; set; }


    //public bool ticked { get; set; }


    //public string PM_PPT_NAME { get; set; }
    //public string PM_PPT_CODE { get; set; }

    //public string PM_PPT_TYPE { get; set; }
    //public string PN_PROPERTYTYPE { get; set; }

    //public Nullable<System.DateTime> PM_LEASE_ESC_S_DATE { get; set; }
    //public Nullable<System.DateTime> PM_LEASE_ESC_E_DATE { get; set; }
    //public string CHE_CODE { get; set; }
    //public string CHE_NAME { get; set; }
    //public double PM_LEASE_ESC_AMOUNT { get; set; }
   

}

public class PropertyList
{
    public string CHE_CODE { get; set; }
    public string CHE_NAME { get; set; }

    public string PE_CODE { get; set; }
    public string PM_PPT_NAME { get; set; }
   
    public string PM_PPT_ENTITY { get; set; }

    public bool ticked { get; set; }

}