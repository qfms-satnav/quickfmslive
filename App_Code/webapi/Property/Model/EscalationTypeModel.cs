﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EscalationTypeModel
/// </summary>
public class EscalationTypeModel
{
    public string Amountin { get; set; }
    public string ESCType { get; set; }
    public string Interval_Duration { get; set; }
    public string Intrveltype { get; set; }
    public string Lease { get; set; }
}