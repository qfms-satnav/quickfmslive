﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class LeaseCustomizedDetails
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<ChildEntityLst> entitylst { get; set; }
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public List<ProjctstasLst> prptystats { get; set; }
}

public class ProjctstasLst
{
    public string PROJECTCD { get; set; }
    public string PROJECTNAM { get; set; }
    public bool ticked { get; set; }
    
}
public class LeaseCustomizedData
{
    public string COUNTRY { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string PM_PPT_ADDRESS { get; set; }
    public string STATUS { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string EMPLOYEE_ID { get; set; }
    public string EMPLOYEE_NAME { get; set; }
    public string SECURITY_DEPOSIT { get; set; }
    public string LEASE_NAME { get; set; }
    public string LANDLORD_NAME { get; set; }
    public string MONTHLY_RENT { get; set; }
    public double RECOVERY_AMOUNT { get; set; }
    public double BUILTUP_AREA { get; set; }
    public string MAINTENANCE_CHARGES { get; set; }
    public string CARPET_AREA { get; set; }
    public string RENT_AREA { get; set; }
    public string LANDLORD_ADDRESS { get; set; }
    public string branch_code { get; set; }
    public string STATE { get; set; }
    public string Amenities { get; set; }
    public DateTime? PPT_ESTD { get; set; }
    public string RENT_REVISION { get; set; }
    public string PN_LEASE_TYPE { get; set; }
    public string PN_PROPERTYTYPE { get; set; }
    public string lease_payment_terms { get; set; }
    public string COMPLETE_ADDRESS { get; set; }
    public double PAID_AMOUNT { get; set; }
    public string Total_Rent { get; set; }
    public string RENEWAL_STATUS { get; set; }
    public string Request_type { get; set; }
    public string agreement_type { get; set; }
    public string Rent_type { get; set; }
    public string GST { get; set; }
    public string GST_NO { get; set; }
    public string BUILT_UP { get; set; }
    public string LEASE_STATUS { get; set; }
    public string Zone { get; set; }
    public string COMPANYID { get; set; }

    public string PM_LO_ADDI_PARK_CHR { get; set; }
    public DateTime? PM_LAD_LOCK_INSPEDATE { get; set; }
    public DateTime? PM_LAD_RENTT_DATE { get; set; }
    public string PM_COST_MAINTENANCES { get; set; }

    public string PM_LL_MON_RENT_PAYABLE { get; set; }
    public string PM_LL_SECURITY_DEPOSIT { get; set; }
    public string PM_LL_MAINT_CHARGES { get; set; }
    public DateTime? PM_LL_FROM_DATE { get; set; }
    public DateTime? PM_LL_TO_DATE { get; set; }

    public string LEASE_ESCALATION { get; set; }
    public double ESCPERCENT { get; set; }
    public DateTime? LEASE_START_DATE { get; set; }
    public DateTime? AGREE_START_DATE { get; set; }
    public DateTime? EXTESNION_TODATE { get; set; }
    public DateTime? LEASE_END_DATE { get; set; }
    public DateTime? ESCLATIONDATE { get; set; }
    public DateTime? EXPIRY_AGREEMENT_DATE { get; set; }

    public string REQ_STATUS { get; set; }
    public string AGREEMENT_TYPE { get; set; }
    public string SUB_GROUP { get; set; }
    public DateTime? SURRENDER_DATE { get; set; }
    public DateTime? SURRENDER_REFUNDDATE { get; set; }
    public string S_OUT_STANDINGAMOUNT { get; set; }
    public DateTime? PM_PPT_INSPECTED_DT { get; set; }
    public string PM_PPT_INSPECTED_BY { get; set; }
    public string PM_PPT_POA { get; set; }
    public string PM_PPT_SIG_LENGTH { get; set; }
    public string PM_PPT_SIG_WIDTH { get; set; }
    public string PM_PPT_AC_OUTDOOR { get; set; }
    public string PM_PPT_GSB { get; set; }
    public string PM_SL_POSS_LETR_NAME { get; set; }
    public string OVERALL_COUNT { get; set; }
    public string PM_LES_REMARKS { get; set; }
    public string PM_LES_REN_L1_REMARKS { get; set; }
    public string PM_SL_REMARKS { get; set; }
    public string PM_LE_REMARKS { get; set; }
    public string NOT_PIRED { get; set; }
    public string LOC_IN { get; set; }
    public string RENT_FREE { get; set; }
}

public class LeaseCustomizedReportView
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public LeaseCustomizedReportView()
    {
        SpaceDatas = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> SpaceDatas { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case LeaseCustomizedReportView.ReportFormat.Word: return ".doc";
                case LeaseCustomizedReportView.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.SpaceDatas)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}