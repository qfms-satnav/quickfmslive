﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EscalationAmount
/// </summary>
public class EscalationAmount
{
    public string StartDate { get; set; }
    public string EndDate { get; set; }
    public double MonthWiseAmount { get; set; }
    public double Amount { get; set; }
    public string Type { get; set; }
    public int IndexNo { get; set; }
    public string VALUE { get; set; }
    public string Amountin { get; set; }
    public string IntervalType { get; set; }
    public string IntervalDuration { get; set; }
    public string EscalationType { get; set; }
    public string Lease { get; set; }
    
   


}