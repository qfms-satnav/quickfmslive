﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaseEscVM
/// </summary>
public class LeaseEscVM
{
    public string PM_PPT_NAME { get; set; }
    public string PM_PPT_CODE { get; set; }

    public string PM_LR_REQ_ID { get; set; }
    public int PM_LES_SNO { get; set; }
    public bool ticked { get; set; }
    public Nullable<DateTime> PM_LAD_START_DT_AGREEMENT { get; set; }
    public Nullable<DateTime> PM_LAD_EXP_DT_AGREEMENT { get; set; }
    public double PM_LES_BASIC_RENT { get; set; }
    public double PM_LES_SEC_DEPOSIT { get; set; }
    public double PM_LO_ADDI_PARK_CHRG { get; set; }
    public double PM_LC_FF_CHARGES { get; set; }
    public double PM_LC_MAINT_CHARGES { get; set; }
    public string PM_PPT_Entity { get; set; }

    public string PM_LL_NUMBER { get; set; }
    public string PM_LL_SNO { get; set; }
    public string PM_LL_NAME { get; set; }
   

}
public class LeaseEscVMs
{
    public string PM_PPT_CODE { get; set; }
    public string ESCALATION { get; set; }
    public string PM_LL_NAME { get; set; }
    public string PM_LL_PM_LR_REQ_ID { get; set; }
}