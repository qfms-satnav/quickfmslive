﻿using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for LeaseRentalandEscaltionModel
/// </summary>

//public class LeasePropert
//{
//    public DateTime FROM_DATE { get; set; }
//    public DateTime TO_DATE { get; set; }  
//    public string LCM_NAME { get; set; }
//    public string CNP_NAME { get; set; }
//}

public class LeaseProperty
{
    public string PM_PPT_NAME { get; set; }
    public string PM_PPT_CODE { get; set; }
    public string year { get; set; }
    //public Nullable<System.DateTime> FromDate { get; set; }
    //public Nullable<System.DateTime> ToDate { get; set; }
    public bool ticked { get; set; }
    public List<ProjctstasLst> prptystats { get; set; }
    public string PM_PPT_TYPE { get; set; }
    public string PN_PROPERTYTYPE { get; set; }
}
