﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class PropertiesModel
{
    public string PN_PROPERTYTYPE { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string PM_PPT_ADDRESS { get; set; }
    public string PM_AR_CARPET_AREA { get; set; }
    public string PM_AR_BUA_AREA { get; set; }
    public string PM_OWN_NAME { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string STA_DESC { get; set; }
    public Nullable<System.DateTime> PM_INS_START_DT { get; set; }
    public Nullable<System.DateTime> PM_INS_END_DT { get; set; }
    public string AUR_RES_NUMBER { get; set; }
    public string AUR_EMAIL { get; set; }
    public string PM_IT_NAME { get; set; }
    public string PM_INS_VENDOR { get; set; }
    public string PM_INS_AMOUNT { get; set; }
    public string PM_INS_PNO { get; set; }
    public string CNP_NAME { get; set; }
    public string OVERALL_COUNT { get; set; }
    public string CHE_NAME { get; set; }
    public string PM_PROPERTY_REMARK { get; set; }



    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string PM_AR_RENT_AREA { get; set; }
    public string PM_AR_USABEL_AREA { get; set; }
    public string PM_AR_PLOT_AREA { get; set; }
    public string PM_OWN_ADDRESS { get; set; }
    public string PM_PUR_PRICE { get; set; }
    public string PM_GOV_IRDA { get; set; }
    public string PM_GOV_PC_CODE { get; set; }

    public string PM_GOV_PROP_CODE { get; set; }
    public string PM_GOV_UOM_CODE { get; set; }

    public Nullable<System.DateTime> PM_PUR_DATE { get; set; }
    public string PM_PUR_MARKET_VALUE { get; set; }
    public string PM_INS_TYPE { get; set; }
    public string PM_COST_RENT { get; set; }
    public string PM_COST_RENT_SFT { get; set; }
    public string PM_COST_RATIO { get; set; }
    public string PM_COST_OWN_SHARE { get; set; }
    public string PM_COST_SECDEPOSIT { get; set; }
    public string PM_COST_GST { get; set; }
    public string PM_COST_MAINTENANCE { get; set; }
    public string PM_COST_ESC_RENTALS { get; set; }
    public string PM_COST_RENT_FREE { get; set; }
    public string PM_COST_STAMP { get; set; }


    public string PM_COST_AGREEMENT_PERIOD { get; set; }
    public string PM_OTHR_FLOORING { get; set; }
    public string PM_OTHR_WASHRM_REQUIRED { get; set; }
    public string PM_OTHR_POTABLE_WTR { get; set; }
    public string PM_DOC_TITLE { get; set; }
    public string PM_DOC_TITLLE_RMKS { get; set; }
    public string PM_DOC_OCCUPANCY { get; set; }
    public string PM_DOC_OCC_RMKS { get; set; }
    public string PM_DOC_BUILD { get; set; }
    public string PM_DOC_BUILD_RMKS { get; set; }
    public string PM_DOC_PAN { get; set; }
    public string PM_DOC_PAN_RMKS { get; set; }
    public string PM_DOC_TAX { get; set; }
    public string PM_DOC_TAX_RMKS { get; set; }
    public string PM_DOC_OTHER_INFO { get; set; }



}


public class ReportType
{
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string STA_CODE { get; set; }
    public string LCM_CODE { get; set; }
    
}

public class StatusD
{
    public string STA_DESC { get; set; }
    public string STA_CODE { get; set; }
    public bool ticked { get; set; }
}
public class LocationList
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public bool ticked { get; set; }
}