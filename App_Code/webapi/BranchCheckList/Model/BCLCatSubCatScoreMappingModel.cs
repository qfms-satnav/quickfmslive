﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BCLCatSubCatScoreMappingModel
{    

    public int BCL_CH_ID { get; set; }    
    public string BCL_CH_NAME { get; set; }
    public int BCL_TP_MC_ID { get; set; }    
    public string BCL_CH_SUB_CODE { get; set; }
    public string BCL_CH_MNC_CODE { get; set; }
    public string BCL_CH_TYPE { get; set; }
    public string BCL_CH_REMARKS { get; set; }
}
