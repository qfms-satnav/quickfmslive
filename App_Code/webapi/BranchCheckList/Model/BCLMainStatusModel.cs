﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BCLMainStatusModel
{
    public int BCL_MC_STATUS_ID { get; set; }    
    public string BCL_MC_STATUS { get; set; }
    public int BCL_TP_MC_ID { get; set; }
    public string BCL_MC_TYPENAME { get; set; }
   
}
public class Branch_Images_List
{
    public string BCL_MC_NAME { get; set; }

    public string BCL_SUB_NAME { get; set; }

    public string BCLD_FILE_UPLD { get; set; }

    public string File_Folder { get; set; }

}
public class Branch_filedetails
{
    public string BCLID { get; set; }

    public string CLIENTNAME { get; set; }
}