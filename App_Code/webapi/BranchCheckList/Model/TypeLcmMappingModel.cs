﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using HDMUtilityVM;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for ChildCatApprovalMatVM
/// </summary>


public class TypeLcmMappingModel
{
    public int TPM_TP_ID { get; set; }
    public int COUNT { get; set; }
    public List<Approverlst> Apprlst { get; set; }
    public List<Locationlst> lcmlst { get; set; }  
}


public class TypeLcmgriddata
{
    public string BCL_TP_NAME { get; set; }
    public string BCL_TP_ID { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }   
    public int COUNT { get; set; }
    public int TPM_ID { get; set; }
}

public class GetTypeLcmData
{
    public int TPM_TP_ID { get; set; }
    public string LCM_CODE { get; set; }
    public int LEVEL { get; set; }
}
