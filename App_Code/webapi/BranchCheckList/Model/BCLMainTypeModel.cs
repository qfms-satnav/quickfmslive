﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BCLMainTypeModel
{
    public int BCL_TP_ID { get; set; }  
    public string BCL_TP_NAME { get; set; }
    public string BCL_TP_REMARKS { get; set; }
    public int BCL_TP_LEVEL { get; set; }
    public int BCL_TP_GPS { get; set; }

}
