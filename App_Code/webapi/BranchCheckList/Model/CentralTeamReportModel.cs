﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CentralTeamReport
/// </summary>
public class CentralTeamReport
{
    public string SecurityName { get; set; }
    public string LocationName { get; set; }
    public string SG_Status { get; set; }
    public string SubCategoery { get; set; }
    public string Comments { get; set; }
    public string CreatedDate { get; set; }
    public string ZFMAction { get; set; }
    public string ZfmComments { get; set; }
    public string CentralTeamAction { get; set; }
    public string CentralTeamComments { get; set; }
    public string ZFMUpdateDate { get; set; }
    public string CentralTeamUpdateDate { get; set; }
}

public class CentraTeamRequestModel
{
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string UserID { get; set; }
    public string Type { get; set; }
}
