﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


public class selecteddata
{
    public int BCL_ID { get; set; }
    public List<LoclstHirachy> LCMLST { get; set; }
    public string LcmList { get; set; }
    public string InspectdBy { get; set; }
    public string date { get; set; }
    public string ReviewedBy { get; set; }
    public string Revieweddate { get; set; }
    public string ApprovedBy { get; set; }
    public string Approveddate { get; set; }
    public string SelCompany { get; set; }
    public List<SelRadio> Seldata { get; set; }
    public List<FileProperties> PostedFiles { get; set; }
    public int Flag { get; set; }
    public ObservableCollection<ImagesList> imagesList { get; set; }
    public string OVERALL_CMTS { get; set; }
    public string BCL_TYPE_ID { get; set; }
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string ON_BEHALF_SUBMITTED_BY { get; set; }
    public string LAST_SUBCAT { get; set; }

}

public class CityList
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class LoclstHirachy
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class LocationByCity
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public int LCM_CNP { get; set; }
    public bool ticked { get; set; }
}
public class SelRadio
{
    //public string row { get; set; }
    public string CatCode { get; set; }
    public string SubcatCode { get; set; }
    public string ScoreCode { get; set; }
    public string ScoreName { get; set; }
    //public List<Score_Name> ScoreName { get; set; }
    public string txtdata { get; set; }
    public string Date { get; set; }
    public string SubScore { get; set; }
    public string FilePath { get; set; }
    public string BCLD_ZF_ACTIONS { get; set; }
    public string BCLD_ZF_COMENTS { get; set; }
    public string BCLD_CENTRAL_ACTIONS { get; set; }
    public string BCLD_CENTRAL_TEAM_CMTS { get; set; }


}

//public class Score_Name
//{
//    public string ScoreName { get; set; }
//}
public class list
{
    public string LcmCode { get; set; }
    public DateTime? InspectdDate { get; set; }
}

//public class FileProperties
//{
//    public string name { get; set; }
//    public string size { get; set; }
//    public string type { get; set; }
//    public string SaveAs { get; set; }
//    public string Length { get; set; }
//}
public class InspectorsValidatorsApprover
{
    public string CompanyId { get; set; }
    public int TPM_TP_ID { get; set; }
    public string TPM_LCM_CODE { get; set; }
    public int TPMD_APPR_LEVELS { get; set; }
}

public class GridDataCheck
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public int BCL_TP_ID { get; set; }
}

public class SavedList
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public int BCLStatus { get; set; }
    public int BCL_TP_ID { get; set; }
}

public class SavedListZonalCentral
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public int BCLStatus { get; set; }
}

public class CheckedListDetail
{
    public string CompanyId { get; set; }
    public int BCLID { get; set; }
    public string UserID { get; set; }
}

public class ZonalCentralStatus
{
    public string CompanyId { get; set; }
    public int BCL_TP_ID { get; set; }
    public string typeName { get; set; }
}

public class DeleteCheck
{
    public string CompanyId { get; set; }
    public string userId { get; set; }
    public int BCLID { get; set; }
}


public class ChecklistData
{
    public int BCL_TP_ID { get; set; }
    public string BCL_TP_NAME { get; set; }
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public string BCL_SUB_TYPE { get; set; }
    public string ISFILE { get; set; }
    public string ISCOMMENTS { get; set; }
    public string ISDATE { get; set; }

    public DateTime DATERESPONSE { get; set; }
    public string FILEPATH { get; set; }
    public string TEXT { get; set; }
    public string RESPONSE { get; set; }
    public List<ChecklistDetail> checklistDetails = new List<ChecklistDetail>();
}


public class ChecklistDetail
{
    public int BCL_CH_ID { get; set; }
    public string BCL_CH_CODE { get; set; }
    public string BCL_CH_NAME { get; set; }
    public string BCL_CH_SUB_CODE { get; set; }
    public bool RESPONSE { get; set; }

}
public class ChecklistDetails
{
    public List<ChecklistData> CHKLST { get; set; }
    public string Description { get; set; }
}
public class ChecklistDataV2
{
    public int BCL_TP_ID { get; set; }
    public string BCL_TP_NAME { get; set; }
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public string BCL_SUB_TYPE { get; set; }
    public string ISFILE { get; set; }
    public string ISCOMMENTS { get; set; }
    public string ISDATE { get; set; }

    public DateTime DATERESPONSE { get; set; }
    public string FILEPATH { get; set; }
    public string TEXT { get; set; }
    public string RESPONSE { get; set; }
    public string ValidateInchareAction { get; set; }

    public string ValidateInchareComment { get; set; }

    public string ApprovalAction { get; set; }
    public string ApprovalComment { get; set; }


    public List<ChecklistDetail> checklistDetails = new List<ChecklistDetail>();
}

public class Designation_EMP
{
    public string EMP_DESG { get; set; }
    public string EMP_ID { get; set; }

    public bool ticked { get; set; }
}
public class Items_list1
{
    public string Inspection { get; set; }
    public string Designation { get; set; }
    public bool ticked { get; set; }
}
