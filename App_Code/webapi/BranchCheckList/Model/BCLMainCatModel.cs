﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BCLMainCatModel
{
    public int BCL_MC_ID { get; set; }    
    public string BCL_MC_NAME { get; set; }
    public int BCL_TP_MC_ID { get; set; }    
    public string BCL_MC_REM { get; set; }
   
}
