﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ViewAndModifyScheduleModel
/// </summary>
public class ViewAndModifyScheduleModel
{
    public string BCLDS_ID { get; set; }
    public string BCLDS_CTY_CODE { get; set; }
    public string BCLDS_LCM_CODE { get; set; }
    public string BCLDS_FROM_DATE { get; set; }
    public string BCLDS_TO_DATE { get; set; }
    public string BCLDS_CREATED_DT { get; set; }
    public string BCLDS_CREATED_BY { get; set; }
    public string REMARKS { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string UserId { get; set; }
    public string Companyid { get; set; }
    public List<ModifyScheduleVisitsList> ModifyScheduleVisitsList { get; set; }
}
public class ModifyScheduleVisitsList
{
    public string DATEVALUE { get; set; }
    public string CTY_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string REMARKS { get; set; }
    public bool ticked { get; set; }
}