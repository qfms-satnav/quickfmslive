﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class BCLLoc
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public bool ticked { get; set; }
}
public class BCLUser
{
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string URL_ROL_ID { get; set; }
    public bool ticked { get; set; }

}
public class BCLType
{
    public string BCL_TP_ID { get; set; }
    public string BCL_TP_NAME { get; set; }
    public bool ticked { get; set; }
}
public class BCLData
{
    public List<Rolelst> Rolelst { get; set; }
    public List<BCLUser> BCLUserlst { get; set; }
    public List<BCLType> BCLTypelst { get; set; }
    public List<BCLLoc> BCLLocList { get; set; }
}
