﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ProjectCheckListReportService
/// </summary>
public class prjCustParams
{
    public List<LoclstHirachy> LCMLST { get; set; }
    public List<Companylst> CNPLST { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string Type { get; set; }
}
public class PrjImgParams
{
    public List<LoclstHirachy> LCMLST { get; set; }
    public string INSPCTED_BY { get; set; }
    public DateTime INSPCTED_DT { get; set; }
}
public class ProjectCheckListReportModel
{
        public string CNY_NAME { get; set; }
        public string CTY_NAME { get; set; }
        public string LCM_CODE { get; set; }
        public string LCM_NAME { get; set; }
        public string CNP_NAME { get; set; }
        public string PM_CL_MAIN_PRJ_NAME { get; set; }
        public string PM_CL_MAIN_INSP_BY { get; set; }
        public string PM_CL_MAIN_PRJ_TYPE { get; set; }
       
        public string PM_CL_CAT_NAME { get; set; }
        //public DateTime? BCLD_DATE { get; set; }
        public string PM_CL_SUBCAT_NAME { get; set; }
        public DateTime? PM_CL_MAIN_VISIT_DT { get; set; }
        public string PM_CL_CHILDCAT_NAME { get; set; }
        public string PM_CL_SCC_NAME { get; set; }
        public string PM_CLD_CHILDCAT_VALUE { get; set; }

}
