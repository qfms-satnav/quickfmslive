﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CreateScheduleModel
/// </summary>
public class CreateScheduleModel
{

    public DateTime FROMDATE { get; set; }
    public DateTime TODATE { get; set; }

    public List<ScheduleVisitsList> ScheduleVisitsList { get; set; }

}
public class ScheduleVisitsList
{
    public DateTime DATEVALUE { get; set; }
    public string CTY_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string REMARKS { get; set; }
    public bool ticked { get; set; }

}

public class LCMlstforlocations
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public bool ticked { get; set; }
}