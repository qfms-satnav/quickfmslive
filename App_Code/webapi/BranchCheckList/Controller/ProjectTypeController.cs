﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

/// <summary>
/// Summary description for ProjectTypeController
/// </summary>
public class ProjectTypeController: ApiController
{
    ProjectTypeService PT = new ProjectTypeService();

    [HttpPost]
    public HttpResponseMessage Create(ProjectTypeModel PM)
    {
        if (PT.Save(PM) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, PM);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
    }
    [HttpPost]
    public HttpResponseMessage Update(ProjectTypeModel PM)
    {
        if (PT.Update(PM) ==1)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, PM);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Something went wrong");
    }
    [HttpGet]
    public object BindGrid()
    {
        return PT.BindGrid();
       
    }
}