﻿using System;
using QuickFMS.API.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BCLCatSubCatScoreMappingController : ApiController
{
    BCLCatSubCatScoreMappingService service = new BCLCatSubCatScoreMappingService();
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = service.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMainCategory(BCLCatSubCatScoreMappingModel CustRpt)
    {
        var obj = service.GetMainCategory(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetSubCategory(BCLCatSubCatScoreMappingModel CustRpt)
    {
        var obj = service.GetSubCategory(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGridData(BCLCatSubCatScoreMappingModel CustRpt)
    {
        var obj = service.GetGridData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(BCLCatSubCatScoreMappingModel CustRpt)
    {
        var obj = service.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage updateData(BCLCatSubCatScoreMappingModel CustRpt)
    {
        var obj = service.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
