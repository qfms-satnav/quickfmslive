﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class CheckListReportController : ApiController
{
    CheckListReportService BclReport = new CheckListReportService();

    [HttpGet]
    public Object getInspectors()
    {
        return BclReport.getInspectors();
    }
    [HttpPost]
    public HttpResponseMessage GetImage(ImgParams paramImg)
    {
        var obj = BclReport.GetImage(paramImg);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage LoadGrid(CustParams param)
    {
        var obj = BclReport.LoadGrid(param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> ExportGrid([FromBody]CustParams data)
    {
        ReportGenerator<CheckListReportModel> reportgen = new ReportGenerator<CheckListReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/BranchCheckList_Mgmt/CheckListReport.rdlc"),
            DataSetName = "CheckListReport",
            ReportType = "CheckList Report"
        };

        BclReport = new CheckListReportService();
        List<CheckListReportModel> reportdata = BclReport.LoadGrid(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CheckListReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CheckListReport." + data.Type;
        return result;
    }
}