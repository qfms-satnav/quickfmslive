﻿using System;
using QuickFMS.API.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BCLMainSubCategoryController : ApiController
{
    BCLMainSubCategoryService service = new BCLMainSubCategoryService();
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = service.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMainCategory(BCLMainSubCatModel CustRpt)
    {
        var obj = service.GetMainCategory(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGridData(BCLMainSubCatModel CustRpt)
    {
        var obj = service.GetGridData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetScoreData(BCLMainSubCatModel CustRpt)
    {
        var obj = service.GetScoreData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(BCLMainSubCatModel CustRpt)
    {
        var obj = service.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage updateData(BCLMainSubCatModel CustRpt)
    {
        var obj = service.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage DeleteScore(SCOREDETAILSDELETE reldet)
    {
        var obj = service.DeleteScore(reldet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
