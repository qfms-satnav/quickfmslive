﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuickFMS.API.Filters;

public class ViewAndModifyScheduleController : ApiController
{
    ViewAndModifyScheduleService VAM = new ViewAndModifyScheduleService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetScheduleData()
    {
        var obj = VAM.GetScheduleData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage EditScheduleData(ViewAndModifyScheduleModel VAMmodel)
    {
        var obj = VAM.EditScheduleData(VAMmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage DeleteMySchedule(ViewAndModifyScheduleModel VAMmodel)
    {
        var obj = VAM.DeleteMySchedule(VAMmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }



    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage ModifyScheduleVisits(ViewAndModifyScheduleModel VAMmodel)
    {
        var obj = VAM.ModifyScheduleVisits(VAMmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
