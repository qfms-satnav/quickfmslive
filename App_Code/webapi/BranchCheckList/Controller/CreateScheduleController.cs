﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using QuickFMS.API.Filters;

public class CreateScheduleController : ApiController
{
    CreateScheduleService CS = new CreateScheduleService();


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetDates(CreateScheduleModel CSM)
    {
        var obj = CS.GetDates(CSM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetLocationsall([FromUri] int id)
    {
        var obj = CS.GetLocationsall(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage ScheduleVisits(CreateScheduleModel CSM)
    {
        var obj = CS.ScheduleVisits(CSM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
