﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
/// <summary>
/// Summary description for BCLRoleMappingController
/// </summary>
public class BCLRoleMappingController : ApiController
{
    BCLRoleMappingService BCLMap = new BCLRoleMappingService();
    [HttpGet]
    public HttpResponseMessage GetRoles([FromUri] int id)
    {
        var obj = BCLMap.GetRoles(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetBCLUsers([FromUri] int id)
    {
        var obj = BCLMap.GetBCLUsers(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetBCLType([FromUri] int id)
    {
        var obj = BCLMap.GetBCLType(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetLocations([FromUri] int id)
    {
        var obj = BCLMap.GetLocations(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SubmitData(BCLData data)
    {
        var obj = BCLMap.SubmitData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetData()
    {
        var obj = BCLMap.GetData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage DeleteBCL([FromUri] int id)
    {
        var obj = BCLMap.DeleteBCL(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}