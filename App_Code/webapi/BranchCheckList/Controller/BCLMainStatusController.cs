﻿using System;
using QuickFMS.API.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BCLMainStatusController : ApiController
{
    BCLMainStatusService service = new BCLMainStatusService();
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = service.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGridData(BCLMainStatusModel CustRpt)
    {
        var obj = service.GetGridData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(BCLMainStatusModel CustRpt)
    {
        var obj = service.SaveData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage updateData(BCLMainStatusModel CustRpt)
    {
        var obj = service.updateData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    //public HttpResponseMessage Get_Branch_Images(string id,string id1)
    //{
    //    var obj = service.Get_Branch_Images(id);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    public HttpResponseMessage Get_Branch_Images(Branch_filedetails filedetails)
    {
        var obj = service.Get_Branch_Images(filedetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


}
