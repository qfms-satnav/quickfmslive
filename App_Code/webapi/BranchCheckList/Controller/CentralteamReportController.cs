﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for CentralteamReportController
/// </summary>
public class CentralteamReportController: ApiController
{
	
    CentralTeamReportService service = new CentralTeamReportService();
  
   
    ////get grid data based on Dat
    [HttpPost]
    public HttpResponseMessage BindGrid([FromBody] CentraTeamRequestModel data)
    {
        var obj = service.CentralTeamReport(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = service.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}