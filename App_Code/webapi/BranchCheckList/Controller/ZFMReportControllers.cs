﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using UtiltiyVM;

public class ZFMReportController : ApiController
{
    SubSonic.StoredProcedure sp;
    ZFMReportService service = new ZFMReportService();


    ////get grid data based on Dat
    [HttpPost]
    public HttpResponseMessage BindGrid([FromBody] ZfmRequestModel data)
    {
        var obj = service.ZfmReport(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage BindGridpivot([FromBody] ZfmRequestModelpivot data)
    {
        var obj = service.ZfmReportpivot(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }



    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = service.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    static Hashtable ChkExportAuth = new Hashtable();
    [HttpGet]
    public HttpResponseMessage DownloadDocuments([FromUri] string id)
    {
        HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[PICTURE_DOWNLOAD]");
            sp.Command.AddParameter("@ID", id, DbType.String);
            DataSet ds = sp.GetDataSet();

            string filePath = HttpContext.Current.Server.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"] + "/") + ds.Tables[0].Rows[0]["BCLD_FILE_UPLD"].ToString();
            //string filePath = "C:\\ContractFileRepository\\" + ds.Tables[0].Rows[0]["DOCUMENT"].ToString();                                     
            byte[] bytes = System.IO.File.ReadAllBytes(filePath);
            result.Content = new StreamContent(new MemoryStream(bytes));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = ds.Tables[0].Rows[0]["BCLD_FILE_UPLD"].ToString();
            ChkExportAuth.Remove(id);
            return result;
        }
        catch (Exception ex)
        {
            if (ex.InnerException != null)
                result = Request.CreateResponse(HttpStatusCode.ExpectationFailed, MessagesVM.ErrorMessage);
            else
                result = Request.CreateResponse(HttpStatusCode.ExpectationFailed, MessagesVM.ErrorMessage);
            return result;
        }
    }
    [HttpPost]
    public HttpResponseMessage GetAttachmentwise(ZfmRequestModel data)
    {
        var obj = service.GetAttachmentwise(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
