﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.IO;

public class CheckListCreationController : ApiController
{
    CheckListCreationService ccs = new CheckListCreationService();

    [HttpGet]
    public Object getInspectors()
    {

        return ccs.getInspectors();
    }

    [HttpPost]
    public HttpResponseMessage getInspectorsZonalCentral(int TPM_TP_ID, string TPM_LCM_CODE, int TPMD_APPR_LEVELS)
    {
        var obj = ccs.getInspectorsZonalCentral(TPM_TP_ID, TPM_LCM_CODE, TPMD_APPR_LEVELS);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public Object getCompany()
    {

        return ccs.getCompany();
    }
    [HttpGet]
    public Object getLocations()
    {

        return ccs.getLocations();
    }


    [HttpGet]
    public HttpResponseMessage GetLocationsIn([FromUri] int id)
    {
        var obj = ccs.GetLocationsIn(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetInspectorsIn([FromUri] int id)
    {
        var obj = ccs.GetInspectorsIn(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage GetItems(string q)
    {
        var obj = ccs.GetItems(q);

        var response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }


    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = ccs.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGriddata(int BCL_TP_ID, list lst)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = ccs.GetGriddata(BCL_TP_ID, lst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetGriddata(int BCL_TP_ID, int BCL_ID)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = ccs.GetGriddata(BCL_TP_ID, BCL_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage GetValidation_Grid(int BCL_TP_ID, int BCL_ID)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = ccs.GetValidation_Grid(BCL_TP_ID, BCL_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetValidation_Grid(int BCL_TP_ID, int BCL_ID, int BCL_SUBMIT)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = ccs.GetValidation_Grid(BCL_TP_ID, BCL_ID, BCL_SUBMIT);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveValidationData(selecteddata dataobj)
    {

        var obj = ccs.SaveValidationData(dataobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CTMGetGriddata(int BCL_TP_ID, list lst, int BCL_ID)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = ccs.CTMGetGriddata(BCL_TP_ID, lst, BCL_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public object GetScore()
    {
        var response = ccs.GetScore();
        return response;
    }

    [HttpPost]
    public HttpResponseMessage InsertCheckList(selecteddata seldt)
    {
        var obj = ccs.InsertCheckList(seldt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ZFMInsertCheckList(selecteddata seldt)
    {
        var obj = ccs.ZFMInsertCheckList(seldt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CTMInsertCheckList(selecteddata seldt)
    {
        var obj = ccs.CTMInsertCheckList(seldt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage DeleteCheckList([FromUri] int BCLID)
    {
        var obj = ccs.DeleteCheckList(BCLID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage LocationByCity(List<CityList> citylst)
    {
        var obj = ccs.LocationByCity(citylst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage getSavedList([FromUri] int BCLStatus, int BCL_TP_ID)
    {
        var obj = ccs.getSavedList(BCLStatus, BCL_TP_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage getSavedListZonalCentral([FromUri] int BCLStatus)
    {
        var obj = ccs.getSavedListZonalCentral(BCLStatus);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getCheckedListDetail([FromUri] int BCLID)
    {
        var obj = ccs.getCheckedListDetail(BCLID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getSavedListItems([FromUri] string LcmCode)
    {
        var obj = ccs.getSavedListItems(LcmCode);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //var response = getSavedListItems(LcmCode);
        //return response;
    }

    [HttpPost]
    public HttpResponseMessage getZonalCentralStatus(int BCL_TP_ID, string typeName)
    {
        var obj = ccs.getZonalCentralStatus(BCL_TP_ID, typeName);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage InsertCheckListV2(selecteddata seldt)
    {
        var obj = ccs.InsertCheckListV2(seldt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;

        // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
        string sPath = HttpContext.Current.Session["TENANT"].ToString();

        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + sPath + "/");

        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
        //bool folder = Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"].ToString()));
        if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"].ToString())))
        {
            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"].ToString()));
        }
        // CHECK THE FILE COUNT.
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                //if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                //{
                // SAVE THE FILES IN THE FOLDER.
                hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                iUploadedCnt = iUploadedCnt + 1;
                //}
            }
        }

        // RETURN A MESSAGE.
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }
    [HttpPost]
    public HttpResponseMessage SaveApprovalData(selecteddata dataobj)
    {

        var obj = ccs.SaveApprovalData(dataobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage GetSysPreferences(string CHECKLIST_PDF_MAIL)
    {
        var obj = ccs.GetSysPreferences(CHECKLIST_PDF_MAIL);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindDesignation()
    {
        IEnumerable<Designation_EMP> DesignationList = ccs.BindDesignation();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, DesignationList);
        return response;
    }
}