﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using HDMUtilityVM;

/// <summary>
/// Summary description for ChildCatApprovalLocMatrixController
/// </summary>
public class BCLTypeLocMappingController : ApiController
{
    BCLTypeLocMappingService CCAM = new BCLTypeLocMappingService();
    [HttpGet]
    public HttpResponseMessage GetHMDList(int intTypeLevel)
    {
        var obj = CCAM.GetHMDList(intTypeLevel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetRoleBasedEmployees(SelectedRole Role)
    {
        var obj = CCAM.GetRoleBasedEmployees(Role);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMainType()
    {
        var obj = CCAM.GetMainType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGridDetails(GetTypeLcmData obj)
    {
        var Grid = CCAM.GetGridDetails(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Grid);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage editData(GetTypeLcmData obj)
    {
        var Userlist = CCAM.editData(obj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Userlist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage InsertDetails(TypeLcmMappingModel ChildLst)
    {
        if (CCAM.InsertDetails(ChildLst).ToString() != "Already Exists")
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildLst);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Type location approval matrix already exists.");
    }
    [HttpPost]
    public HttpResponseMessage UpdateDetails(TypeLcmMappingModel ChildLst)
    {
        if (CCAM.UpdateDetails(ChildLst).ToString() != "TYPE IS ALREADY MAPPED")
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildLst);
            return response;
        }

        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Type is already mapped.");
    }
  

    [HttpPost]
    public HttpResponseMessage DeleteSeat(TypeLcmgriddata tpm)
    {
        var user = CCAM.DeleteSeat(tpm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

}