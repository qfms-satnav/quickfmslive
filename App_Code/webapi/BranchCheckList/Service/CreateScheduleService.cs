﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using UtiltiyVM;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CreateScheduleService
/// </summary>
public class CreateScheduleService
{
    SubSonic.StoredProcedure sp;
    public object GetDates(CreateScheduleModel CSM)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ALLDATES_BY_RANGE");
            sp.Command.AddParameter("@FROMDATE", CSM.FROMDATE, DbType.Date);
            sp.Command.AddParameter("@TODATE", CSM.TODATE, DbType.Date);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            return ds;
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }


    public object GetLocationsall(int id)
    {
        List<LCMlstforlocations> Loc_lst = new List<LCMlstforlocations>();
        LCMlstforlocations Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM_all");

        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ALL");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new LCMlstforlocations();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object ScheduleVisits(CreateScheduleModel CSM)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@ScheduleVisitsList", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(CSM.ScheduleVisitsList);

            param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[1].Value = HttpContext.Current.Session["UID"];

            //SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SUBMIT_SCHEDULE_VISITS", param);

            using (DataSet dataset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SUBMIT_SCHEDULE_VISITS", param))
            {
                if (dataset.Tables.Count > 0)
                {
                    return new { Message = MessagesVM.AF_OK, data = dataset.Tables[0] };
                }
                else
                {
                    return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
                }
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }

}