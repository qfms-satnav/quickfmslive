﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ZonalFacilityManagerServices
/// </summary>
public class ZFMReportService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object ZfmReport(ZfmRequestModel SelectedDate)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "ZFMREPORT");
            sp.Command.Parameters.Add("@FromDate", SelectedDate.FromDate, DbType.DateTime);
            sp.Command.Parameters.Add("@UserID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@ToDate", SelectedDate.ToDate, DbType.DateTime);
            sp.Command.Parameters.Add("@Type", SelectedDate.Type, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public object ZfmReportpivot(ZfmRequestModelpivot SelectedDate)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[ZFMREPORT_PIVOT]");
            sp.Command.Parameters.Add("@FromDate", SelectedDate.FromDate, DbType.DateTime);
            sp.Command.Parameters.Add("@UserID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@ToDate", SelectedDate.ToDate, DbType.DateTime);
            sp.Command.Parameters.Add("@Type", SelectedDate.Type, DbType.Int32);
            ds = sp.GetDataSet();
            //return ds.Tables;

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, Message = MessagesVM.AF_OK };
        }

        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }



    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");
            sp.Command.AddParameter("@user_id", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetAttachmentwise(ZfmRequestModel SelectedDate)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@FromDate", SqlDbType.NVarChar);
            param[0].Value = SelectedDate.FromDate;
            param[1] = new SqlParameter("@UserID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["Uid"];
            param[2] = new SqlParameter("@ToDate", SqlDbType.NVarChar);
            param[2].Value = SelectedDate.ToDate;
            param[3] = new SqlParameter("@Type", SqlDbType.NVarChar);
            param[3].Value = SelectedDate.Type;


            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "[ZFMREPORT2]", param);
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                       select dc.ColumnName).ToList();
                foreach (string col in Colstr)
                {
                    gridcolscls = new CustomizedGridCols();
                    gridcolscls.cellClass = "grid-align";
                    gridcolscls.field = col;
                    gridcolscls.headerName = col;
                    gridcols.Add(gridcolscls);
                }
                return new { griddata = ds.Tables[0], Coldef = gridcols };
            }
            return new { griddata = (object)null, Coldef = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

}