﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
using System.ComponentModel;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;


public class CheckListCreationService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object getInspectors()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_INSPECTORS");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getInspectorsZonalCentral(int TPM_TP_ID, string TPM_LCM_CODE, int TPMD_APPR_LEVELS)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_INSPECTORSZONALCENTRAL");
        sp.Command.Parameters.Add("@TPM_TP_ID", TPM_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@TPM_LCM_CODE", TPM_LCM_CODE, DbType.String);
        sp.Command.Parameters.Add("@TPMD_APPR_LEVELS", TPMD_APPR_LEVELS, DbType.Int32);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getCompany()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_COMPANIES");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object getLocations()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_LOCATIONS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetLocationsIn(int id)
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_LOCATION_BY_CITY_IN]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };


        //List<Locationlst> Loc_lst = new List<Locationlst>();
        //Locationlst Loc;
        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_LOCATION_BY_CITY_IN]");
        //sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.AddParameter("@MODE", id, DbType.Int32);
        //using (IDataReader sdr = sp.GetReader())
        //{
        //    while (sdr.Read())
        //    {
        //        Loc = new Locationlst();
        //        Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
        //        Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
        //        Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
        //        Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
        //        Loc.ticked = false;
        //        Loc_lst.Add(Loc);
        //    }
        //}
        //if (Loc_lst.Count != 0)
        //    return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        //else
        //    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetInspectorsIn(int id)
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BCL_GET_INSPECTORSZONALCENTRAL_IN]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };


        //List<VER_LST> Loc_lst = new List<VER_LST>();
        //VER_LST Loc;
        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BCL_GET_INSPECTORSZONALCENTRAL_IN]");
        //sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.AddParameter("@MODE", id, DbType.Int32);
        //using (IDataReader sdr = sp.GetReader())
        //{
        //    while (sdr.Read())
        //    {
        //        Loc = new VER_LST();
        //        Loc.VER_CODE = sdr["VER_CODE"].ToString();
        //        Loc.VER_NAME = sdr["VER_NAME"].ToString();             
        //        Loc.ticked = false;
        //        Loc_lst.Add(Loc);
        //    }
        //}
        //if (Loc_lst.Count != 0)
        //    return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        //else
        //    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetItems(string name)
    {
        List<Items_list1> Itemslst = new List<Items_list1>();
        Items_list1 lst;
        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@EMPLOYEID", SqlDbType.NVarChar);
        param[0].Value = name;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[GET_CHECKLIST_EMPLOYEES]", param))
        {
            while (sdr.Read())
            {
                lst = new Items_list1();
                lst.Inspection = sdr["INSPECTOR"].ToString();
                lst.Designation = sdr["Designation"].ToString();
                Itemslst.Add(lst);
            }
        }
        return new { items = Itemslst, total_count = Itemslst.Count, incomplete_results = false };
    }

    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");
            sp.Command.AddParameter("@user_id", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetValidation_Grid(int BCL_TP_ID, int BCL_ID)
    {
        DataSet dataSet = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES_New2");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCLID", BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetail> details = UtilityService.ConvertDataTableToList<ChecklistDetail>(ds.Tables[1]);
        List<ChecklistDataV2> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistDataV2>(ds.Tables[0]);
        foreach (var item in checklistDatas)
        {
            foreach (var items in details)
            {
                if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                {
                    item.checklistDetails.Add(items);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = checklistDatas };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetValidation_Grid(int BCL_TP_ID, int BCL_ID, int BCL_SUBMIT)
    {
        DataSet dataSet = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES_New3");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCLID", BCL_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCL_SUBMIT", BCL_SUBMIT, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetail> details = UtilityService.ConvertDataTableToList<ChecklistDetail>(ds.Tables[1]);
        List<ChecklistDataV2> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistDataV2>(ds.Tables[0]);
        foreach (var item in checklistDatas)
        {
            foreach (var items in details)
            {
                if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                {
                    item.checklistDetails.Add(items);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = checklistDatas };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object SaveValidationData(selecteddata dataobj)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
        param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
        param[1].Value = dataobj.InspectdBy;
        param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
        param[2].Value = dataobj.date;
        param[3] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[3].Value = dataobj.Flag;
        param[4] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[4].Value = dataobj.BCL_ID;
        param[5] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
        param[5].Value = "WEB";
        int sem_id = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[Checklist_Save_ValidationData]", param);
        if (sem_id == 1)
            return new { Message = MessagesVM.UM_OK, data = "Submitted" };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetGriddata(int BCL_TP_ID, list lst)
    {
        DataSet ds = new DataSet();
        if (lst != null)
        {

            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES");
            sp.Command.Parameters.Add("@LOC_ID", lst.LcmCode, DbType.String);
            sp.Command.Parameters.Add("@InspectedDate", lst.InspectdDate, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
            ds = sp.GetDataSet();
        }
        else
        {
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES");
            sp.Command.Parameters.Add("@LOC_ID", "", DbType.String);
            sp.Command.Parameters.Add("@InspectedDate", "", DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
            ds = sp.GetDataSet();
        }

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object GetGriddata(int BCL_TP_ID, int BCL_ID)
    {
        //DataSet ds = new DataSet();
        //if (lst != null)
        //{

        //    sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES");
        //    sp.Command.Parameters.Add("@LOC_ID", lst.LcmCode, DbType.String);
        //    sp.Command.Parameters.Add("@InspectedDate", lst.InspectdDate, DbType.String);
        //    sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //    sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
        //    ds = sp.GetDataSet();
        //}
        //else
        //{
        //    sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES");
        //    sp.Command.Parameters.Add("@LOC_ID", "", DbType.String);
        //    sp.Command.Parameters.Add("@InspectedDate", "", DbType.String);
        //    sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //    sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
        //    ds = sp.GetDataSet();
        //}

        //if (ds.Tables.Count != 0)
        //    return new { Message = MessagesVM.UM_OK, data = ds.Tables };
        //else
        //    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        //Vinod

        DataSet dataSet = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES_New");
        //sp.Command.Parameters.Add("@LOC_ID", lst.LcmCode, DbType.String);
        //sp.Command.Parameters.Add("@InspectedDate", lst.InspectdDate, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCLID", BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetail> details = UtilityService.ConvertDataTableToList<ChecklistDetail>(ds.Tables[1]);
        List<ChecklistData> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistData>(ds.Tables[0]);
        foreach (var item in checklistDatas)
        {
            foreach (var items in details)
            {
                if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                {
                    item.checklistDetails.Add(items);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = new ChecklistDetails { CHKLST = checklistDatas, Description = ds.Tables[2].Rows[0][0].ToString() } };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object CTMGetGriddata(int BCL_TP_ID, list lst, int BCL_ID)
    {
        DataSet ds = new DataSet();
        if (lst != null)
        {

            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES_CTM_WEB");
            sp.Command.Parameters.Add("@LOC_ID", lst.LcmCode, DbType.String);
            sp.Command.Parameters.Add("@InspectedDate", lst.InspectdDate, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
            //sp.Command.Parameters.Add("@BCL_ID", BCL_ID, DbType.Int32);
            ds = sp.GetDataSet();
        }
        else
        {

            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES_CTM_WEB");
            sp.Command.Parameters.Add("@LOC_ID", "", DbType.String);
            sp.Command.Parameters.Add("@InspectedDate", "", DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
            sp.Command.Parameters.Add("@BCL_ID", BCL_ID, DbType.Int32);
            ds = sp.GetDataSet();
        }

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }


    public object GetScore()
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_SCORE");
        DataSet ds = sp.GetDataSet();
        return new { data = ds.Tables[0] };
    }


    public object getSavedList(int BCLStatus, int BCL_TP_ID)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_SAVED_LIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@BCLStatus", BCLStatus, DbType.Int32);
        sp.Command.Parameters.Add("@BCL_TP_ID", BCL_TP_ID, DbType.Int32);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getSavedListZonalCentral(int BCLStatus)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[BCL_GET_SAVED_LIST_ZONALCENTRAL_New]");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@BCLStatus", BCLStatus, DbType.Int32);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object getCheckedListDetail(int BCLID)
    {
        DataSet ds = new DataSet();
        //sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CHECKLIST_DETAIL");
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CHECKLIST_DETAIL_UPDATED");
        sp.Command.Parameters.Add("@BCL_ID", BCLID, DbType.Int32);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getSavedListItems(string LcmCode)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_SAVED_LIST_ITEMS");
        sp.Command.Parameters.Add("@LCM_CODE", LcmCode, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getZonalCentralStatus(int BCL_TP_ID, string typeName)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "spGetZonalCentralStatus");
        sp.Command.Parameters.Add("@BCL_TP_MC_ID", BCL_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCL_MC_TYPENAME", typeName, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object LocationByCity(List<CityList> citylst)
    {
        List<LocationByCity> loclst = new List<LocationByCity>();
        LocationByCity loc;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CITYLST", SqlDbType.Structured);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        if (citylst == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(citylst);
        }


        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_LOCATION_BY_CITY", param))
        {
            while (sdr.Read())
            {
                loc = new LocationByCity();
                loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                loc.LCM_CNP = Convert.ToInt32(sdr["LCM_CNP"]);
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }
    private string BindLogo()
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@type", SqlDbType.VarChar);
            param[0].Value = "2";
            DataTable dataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Update_Get_LogoImage", param);
            if (dataTable.Rows.Count > 0)
            {
                return new Uri(HttpContext.Current.Server.MapPath(dataTable.Rows[0][1].ToString())).AbsoluteUri;
            }
            else
            {
                return new Uri(HttpContext.Current.Server.MapPath("~/BootStrapCSS/images/yourlogo.png")).AbsoluteUri;
            }
        }
        catch (Exception ex)
        {
            return new Uri(HttpContext.Current.Server.MapPath("~/BootStrapCSS/images/yourlogo.png")).AbsoluteUri;
        }

    }
    public DataSet GetCheckList_Report(SqlParameter[] param)
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "CheckList_Report", param);
    }
    public void GenerateCheck_Report(int ChecklistID)
    {
        try
        {
            string DB = "";
            //string DB = _ReturnDB(hD);
            using (var viewer = new ReportViewer())
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@BCLID", SqlDbType.Int);
                param[0].Value = ChecklistID;
                DataSet dataSet = GetCheckList_Report(param);
                viewer.ProcessingMode = ProcessingMode.Local;
                viewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/CheckList_Mgmt/CheckList_Creator.rdlc");
                Microsoft.Reporting.WebForms.Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;
                viewer.LocalReport.DataSources.Clear();
                viewer.LocalReport.DataSources.Add(new ReportDataSource("User_Info", dataSet.Tables[0]));
                viewer.LocalReport.DataSources.Add(new ReportDataSource("CheckList_Details", dataSet.Tables[1]));
                //viewer.LocalReport.DataSources.Add(new ReportDataSource("RatingColor", dataSet.Tables[5]));
                viewer.LocalReport.DataSources.Add(new ReportDataSource("CheckList_User", dataSet.Tables[2]));
                //viewer.LocalReport.DataSources.Add(new ReportDataSource("CheckList_File", dataSet.Tables[7]));
                viewer.LocalReport.EnableHyperlinks = true;
                viewer.LocalReport.EnableExternalImages = true;
                viewer.LocalReport.SetParameters(new ReportParameter("ImagePath", BindLogo()));
                viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                viewer.LocalReport.Refresh();
                String apptype = GetApplicationName("pdf");
                byte[] bytes = viewer.LocalReport.Render(
                    apptype, null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);
                //string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/" + "CheckList_Report_" + DateTime.Now.ToString("MMddyyyy") + ".pdf");
                //using (FileStream fs = new FileStream(filePath, FileMode.Create))
                //{
                //    fs.Write(bytes, 0, bytes.Length);
                //}
                using (SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["Host"], Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["Port"])))
                {
                    smtp.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailid"].ToString(), System.Configuration.ConfigurationManager.AppSettings["password"].ToString());
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    using (MailMessage msg = new MailMessage())
                    {
                        msg.Subject = dataSet.Tables[3].Rows[0][3].ToString();
                        msg.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["from"].ToString());
                        foreach (var tomail in dataSet.Tables[3].Rows[0][1].ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(address => new MailAddress(address.Trim())).Where(mailTo => !msg.To.Contains(mailTo)))
                        {
                            msg.To.Add(tomail);
                        }
                        foreach (var ccmail in dataSet.Tables[3].Rows[0][2].ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(address => new MailAddress(address.Trim())).Where(mailCc => !msg.CC.Contains(mailCc) && !msg.To.Contains(mailCc)))
                        {
                            msg.CC.Add(ccmail);
                        }
                        msg.IsBodyHtml = true;
                        msg.Body = dataSet.Tables[3].Rows[0][0].ToString();
                        msg.Attachments.Add(new Attachment(new MemoryStream(bytes), "Branch_Visit_Report_" + DateTime.Now.ToString("MMddyyyy") + ".pdf"));
                        smtp.Send(msg);
                    }
                }
            };
            //return true;
        }
        catch (Exception ex)
        {

            throw ex;
            //return false;
        }

    }
    public String GetApplicationName(String ext)
    {
        switch (ext)
        {
            case "xls": return "Excel";
            case "xlsx": return "EXCELOPENXML";
            case "doc": return "Word";
            case "pdf": return "PDF";
        }
        return "PDF";
    }



    public object InsertCheckListV2(selecteddata dataobj)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[13];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
            param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
            param[1].Value = dataobj.InspectdBy;
            param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            param[2].Value = dataobj.date;
            param[3] = new SqlParameter("@SelCompany", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            param[4] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
            param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["UID"];
            param[6] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[6].Value = HttpContext.Current.Session["COMPANYID"];
            param[7] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[7].Value = dataobj.Flag;
            param[8] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
            param[8].Value = dataobj.OVERALL_CMTS;
            param[9] = new SqlParameter("@BCL_TYPE_ID", SqlDbType.Int);
            param[9].Value = dataobj.BCL_TYPE_ID;
            param[10] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[10].Value = dataobj.BCL_ID;
            param[11] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
            param[11].Value = "WEB";
            param[12] = new SqlParameter("@ON_BEHALF_SUBMITTED_BY", SqlDbType.NVarChar);
            param[12].Value = HttpContext.Current.Session["UID"];

            //Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[DYNAMIC_CHECKLIST]", param);
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[DYNAMIC_CHECKLIST]", param);
            //int sem_id = (int)o;
            int sem_id = (int)dt.Rows[0][0];
            string msg;
            if (sem_id == 0)
                msg = "Request Id : " + dt.Rows[0][1].ToString() + " Saved as Draft";
            else if (sem_id == 5)
            {
                return new { Message = "5" };
            }
            else
            {
                UtilityService utilityService = new UtilityService();
                dynamic dynamicResult = utilityService.GetSysPreferences();
                if (dynamicResult.Message == MessagesVM.UM_OK)
                {
                    List<SysPreference> SysPrflst = dynamicResult.data;
                    if (SysPrflst.Any(d => d.SYSP_CODE == "CHECKLIST_PDF_MAIL" && d.SYSP_VAL1 == "1"))
                    {
                        //GenerateCheck_Report((int)dt.Rows[0][0]);
                        GenerateCheck_Report(Convert.ToInt32(dt.Rows[0][1]));
                    }
                }
                msg = "Request Id : " + dt.Rows[0][1].ToString() + " Submitted Succesfully";
            }
            return new { data = msg, Message = msg };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    public object InsertCheckList(selecteddata dataobj)
    {
        if (dataobj != null)
        {
            DataSet ds = new DataSet();
            string msg;
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "CHECK_USER_INSPECTION");
            sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables[0].Rows[0].ItemArray[0].ToString() == "0")
            {

                for (int i = 0; i < dataobj.imagesList.Count; i++)
                {
                    SqlParameter[] param1 = new SqlParameter[6];
                    param1[0] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
                    param1[0].Value = dataobj.LcmList;
                    // param1[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
                    param1[1] = new SqlParameter("@INSP_BY", SqlDbType.NVarChar);
                    param1[1].Value = dataobj.InspectdBy;
                    param1[2] = new SqlParameter("@INSP_DT", SqlDbType.NVarChar);
                    param1[2].Value = dataobj.date;
                    param1[3] = new SqlParameter("@UPLOAD_PATH", SqlDbType.NVarChar);
                    param1[3].Value = dataobj.imagesList[i].Imagepath;
                    param1[4] = new SqlParameter("@FILENAME", SqlDbType.NVarChar);
                    param1[4].Value = dataobj.imagesList[i].Imagepath;
                    param1[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                    param1[5].Value = HttpContext.Current.Session["UID"];
                    //param1[6] = new SqlParameter("@REM", SqlDbType.NVarChar);
                    //param1[6].Value = "Uploaded From Web";
                    SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "API_HD_INSERT_CHECKLIST_FILES", param1);
                }
                List<selecteddata> HDSEM = new List<selecteddata>();
                SqlParameter[] param = new SqlParameter[15];
                param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
                param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
                param[1].Value = dataobj.InspectdBy;
                param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
                param[2].Value = dataobj.date;

                param[3] = new SqlParameter("@BCLD_REVIEWED_BY", SqlDbType.NVarChar);
                param[3].Value = dataobj.ReviewedBy;
                param[4] = new SqlParameter("@BCLD_REVIEWED_DT", SqlDbType.NVarChar);
                param[4].Value = dataobj.Revieweddate;
                param[5] = new SqlParameter("@BCLD_APPROVED_BY", SqlDbType.NVarChar);
                param[5].Value = dataobj.ApprovedBy;
                param[6] = new SqlParameter("@BCLD_APPROVED_DT", SqlDbType.NVarChar);
                param[6].Value = dataobj.Approveddate;

                param[7] = new SqlParameter("@SelCompany", SqlDbType.NVarChar);
                param[7].Value = dataobj.SelCompany;
                param[8] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
                param[8].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
                param[9] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[9].Value = HttpContext.Current.Session["UID"];
                param[10] = new SqlParameter("@COMPANY", SqlDbType.Int);
                param[10].Value = HttpContext.Current.Session["COMPANYID"];
                param[11] = new SqlParameter("@FLAG", SqlDbType.Int);
                param[11].Value = dataobj.Flag;
                param[12] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
                param[12].Value = dataobj.OVERALL_CMTS;
                param[13] = new SqlParameter("@BCL_TYPE_ID", SqlDbType.Int);
                param[13].Value = dataobj.BCL_TYPE_ID;
                param[14] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                param[14].Value = dataobj.BCL_ID;
                //param[3] = new SqlParameter("@SelCompany", SqlDbType.NVarChar); 
                //param[3].Value = dataobj.SelCompany;
                //param[4] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
                //param[4].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
                //param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                //param[5].Value = HttpContext.Current.Session["UID"];
                //param[6] = new SqlParameter("@COMPANY", SqlDbType.Int);
                //param[6].Value = HttpContext.Current.Session["COMPANYID"];
                //param[7] = new SqlParameter("@FLAG", SqlDbType.Int);
                //param[7].Value = dataobj.Flag;
                //param[8] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
                //param[8].Value = dataobj.OVERALL_CMTS;
                //Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "BCL_INSERT_CHECKLIST", param);
                Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DYNAMIC_CHECKLIST", param);
                int sem_id = (int)o;

                if (sem_id == 0)
                    msg = "Saved as Draft";
                else
                    msg = "Submitted Succesfully";
                return new { data = msg, Message = msg };
            }
            else
            {
                msg = "Already checklist submitted";
                return new { data = msg, Message = msg };
            }
        }
        else
        {
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object ZFMInsertCheckList(selecteddata dataobj)
    {
        if (dataobj != null)
        {
            //string DB = _ReturnDB(mn);
            //string DB = ((string)HttpContext.Current.Session["TENANT"]);

            //String proc_name = DB + "." + "[ZONAL_INSERT_CHECKLIST]";

            //String proc_name1 = DB + "." + "[ZONAL_INSERT_CHECK_DETAILS]";
            //String proc_name2 = DB + "." + "[SEND_ZONAL_INCEPTION]";

            int sem_id = 0;
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[0].Value = dataobj.BCL_ID;
            param[1] = new SqlParameter("@SaveFlag", SqlDbType.VarChar);
            param[1].Value = dataobj.Flag;
            param[2] = new SqlParameter("@Datetime", SqlDbType.DateTime);
            param[2].Value = dataobj.date;
            param[3] = new SqlParameter("@user", SqlDbType.NVarChar);
            param[3].Value = dataobj.InspectdBy;

            object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "ZONAL_INSERT_CHECKLIST", param);
            sem_id = (int)o;
            if (sem_id > 0)
            {
                for (int i = 0; i < dataobj.Seldata.Count; i++)
                {
                    SqlParameter[] param1 = new SqlParameter[6];
                    param1[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                    param1[0].Value = dataobj.BCL_ID;

                    param1[1] = new SqlParameter("@CatCode", SqlDbType.VarChar);
                    param1[1].Value = dataobj.Seldata[i].CatCode;
                    param1[2] = new SqlParameter("@SubcatCode", SqlDbType.VarChar);
                    param1[2].Value = dataobj.Seldata[i].SubcatCode;
                    param1[3] = new SqlParameter("@WorkCondition", SqlDbType.VarChar);
                    param1[3].Value = dataobj.Seldata[i].ScoreName;
                    param1[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                    param1[4].Value = dataobj.Seldata[i].BCLD_ZF_COMENTS;
                    param1[5] = new SqlParameter("@USER", SqlDbType.VarChar);
                    param1[5].Value = dataobj.Seldata[i].BCLD_ZF_ACTIONS;

                    object ChkDetails = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "ZONAL_INSERT_CHECK_DETAILS", param1);
                }
            }

            SqlParameter[] param2 = new SqlParameter[1];
            param2[0] = new SqlParameter("@ID", SqlDbType.Int);
            param2[0].Value = dataobj.BCL_ID;
            object ChkDetailss = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SEND_ZONAL_INCEPTION", param2);

            string msg;
            if (sem_id == 1)
                msg = "Record Submitted Succesful.";
            else
                msg = "Something Went Wrong";
            return new { Message = msg };
        }
        else
        {
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object CTMInsertCheckList(selecteddata dataobj)
    {
        if (dataobj != null)
        {
            //string DB = _ReturnDB(mn);
            //string DB = ((string)HttpContext.Current.Session["TENANT"]);

            //String proc_name = DB + "." + "[CENTRAL_INSERT_CHECKLIST]";

            //String proc_name1 = DB + "." + "[CENTRAL_INSERT_CHECK_DETAILS]";
            //String proc_name2 = DB + "." + "[SEND_CentralTeam_INCEPTION]";

            int sem_id = 0;
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[0].Value = dataobj.BCL_ID;
            param[1] = new SqlParameter("@SaveFlag", SqlDbType.VarChar);
            param[1].Value = dataobj.Flag;
            param[2] = new SqlParameter("@Datetime", SqlDbType.DateTime);
            param[2].Value = dataobj.date;
            param[3] = new SqlParameter("@user", SqlDbType.NVarChar);
            param[3].Value = dataobj.InspectdBy;

            object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CENTRAL_INSERT_CHECKLIST", param);
            sem_id = (int)o;
            if (sem_id > 0)
            {
                for (int i = 0; i < dataobj.Seldata.Count; i++)
                {
                    SqlParameter[] param1 = new SqlParameter[6];
                    param1[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                    param1[0].Value = dataobj.BCL_ID;

                    param1[1] = new SqlParameter("@CatCode", SqlDbType.VarChar);
                    param1[1].Value = dataobj.Seldata[i].CatCode;
                    param1[2] = new SqlParameter("@SubcatCode", SqlDbType.VarChar);
                    param1[2].Value = dataobj.Seldata[i].SubcatCode;
                    param1[3] = new SqlParameter("@WorkCondition", SqlDbType.VarChar);
                    param1[3].Value = dataobj.Seldata[i].ScoreName;
                    param1[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                    param1[4].Value = dataobj.Seldata[i].BCLD_CENTRAL_TEAM_CMTS;
                    param1[5] = new SqlParameter("@USER", SqlDbType.VarChar);
                    param1[5].Value = dataobj.Seldata[i].BCLD_CENTRAL_ACTIONS;

                    object ChkDetails = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CENTRAL_INSERT_CHECK_DETAILS", param1);
                }
            }

            SqlParameter[] param2 = new SqlParameter[1];
            param2[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param2[0].Value = dataobj.BCL_ID;
            object ChkDetailss = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SEND_CentralTeam_INCEPTION", param2);

            string msg;
            if (sem_id == 1)
                msg = "Record Submitted Succesful.";
            else
                msg = "Something Went Wrong";
            return new { Message = msg };
        }
        else
        {
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object DeleteCheckList(int BCLID)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[0].Value = BCLID;
        Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "BCL_DELETECHECKLIST_DETAIL", param);
        if (obj != null)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }
    public object SaveApprovalData(selecteddata dataobj)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
        param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
        param[1].Value = dataobj.InspectdBy;
        param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
        param[2].Value = dataobj.date;
        param[3] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[3].Value = dataobj.Flag;
        param[4] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[4].Value = dataobj.BCL_ID;
        param[5] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
        param[5].Value = "WEB";
        int sem_id = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[Checklist_Save_ApprovalData]", param);
        if (sem_id == 1)
            return new { Message = MessagesVM.UM_OK, data = "Submitted" };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetSysPreferences(string CHECKLIST_PDF_MAIL)
    {
        List<SysPreference> SysPrflst = new List<SysPreference>();
        SysPreference SysPrf;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_SYSTEM_PREFERENCE]");
        sp.Command.AddParameter("@SYSTEMPREFERNCEVALUE", CHECKLIST_PDF_MAIL, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SysPrf = new SysPreference();
                SysPrf.SYSP_CODE = sdr["SYSP_CODE"].ToString();
                SysPrf.SYSP_VAL1 = sdr["SYSP_VAL1"].ToString();
                SysPrf.SYSP_VAL2 = sdr["SYSP_VAL2"].ToString();
                SysPrf.SYSP_OPR = sdr["SYSP_OPR"].ToString();
                SysPrf.SYSP_STA_ID = Convert.ToInt32(sdr["SYSP_STA_ID"]);
                SysPrflst.Add(SysPrf);
            }
        }
        if (SysPrflst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SysPrflst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public IEnumerable<Designation_EMP> BindDesignation()
    {
        List<Designation_EMP> DesignationList = new List<Designation_EMP>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_EMP_DESIGNATION");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                DesignationList.Add(new Designation_EMP()
                {
                    EMP_DESG = sdr.GetValue(0).ToString(),
                    EMP_ID = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return DesignationList;
        }
    }
}