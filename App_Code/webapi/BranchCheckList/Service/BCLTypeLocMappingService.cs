﻿using HDMUtilityVM;
using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class BCLTypeLocMappingService
{
    SubSonic.StoredProcedure sp;

    public IEnumerable<ChildHDMlist> GetHMDList(int intTypeLevel)
    {
        try
        {
            //using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHECKLIST_ROLES").GetReader())//GET_HELPDESK_ROLES
            //{
            //    List<ChildHDMlist> HDMList = new List<ChildHDMlist>();
            //    while (reader.Read())
            //    {
            //        HDMList.Add(new ChildHDMlist()
            //        {
            //            ROL_ID = reader.GetValue(0).ToString(),
            //            ROL_DESCRIPTION = reader.GetValue(1).ToString()
            //        });
            //    }
            //    reader.Close();
            //    return HDMList;
            //}
            List<ChildHDMlist> HDMList = new List<ChildHDMlist>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHECKLIST_ROLES");//GET_ROLE_BASED_EMP
            sp.Command.AddParameter("intTypeLevel", intTypeLevel, DbType.Int32);
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    HDMList.Add(new ChildHDMlist()
                    {
                        ROL_ID = reader.GetValue(0).ToString(),
                        ROL_DESCRIPTION = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
            }
            return HDMList;
        }
        catch
        {
            throw;
        }
    }

    public object GetRoleBasedEmployees(SelectedRole Role)
    {
        try
        {
            List<RoleBasedEmp> verlst = new List<RoleBasedEmp>();
            RoleBasedEmp ver;
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ROLE_BASED_EMP_CHECK");//GET_ROLE_BASED_EMP
            sp.Command.AddParameter("@ROL", Role.Role, DbType.Int32);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    ver = new RoleBasedEmp();
                    ver.AUR_ID = sdr["AUR_ID"].ToString();
                    ver.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                    verlst.Add(ver);
                }
            }
            return new { Message = MessagesVM.UM_OK, data = verlst };
        }
        catch
        {
            throw;
        }
    }
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object InsertDetails(TypeLcmMappingModel ChildLst)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@BCLTPID", SqlDbType.Int);
            param[0].Value = ChildLst.TPM_TP_ID;
            param[1] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(ChildLst.lcmlst);
            param[2] = new SqlParameter("@CL_CREATED_BY", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["COMPANYID"];
            string result = "";
            object value = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CL_INSERT_TYPE_LOCATION_MAPPING", param);

            //if (value.ToString() == "Already Exists")
            //{
            //    result = value.ToString();
            //}
            //else
            //{
                foreach (var lcmcode in ChildLst.lcmlst)
                {
                    foreach (var opt in ChildLst.Apprlst)
                    {
                        foreach (var opt1 in opt.OpEmplst)
                        {
                            SqlParameter[] param1 = new SqlParameter[5];
                            param1[0] = new SqlParameter("@ROL_ID", SqlDbType.NVarChar);
                            param1[0].Value = opt.ROL_ID;
                            param1[1] = new SqlParameter("@ROL_LVL", SqlDbType.NVarChar);
                            param1[1].Value = opt.ROL_LEVEL;
                            param1[2] = new SqlParameter("@EMP", SqlDbType.NVarChar);
                            param1[2].Value = opt1.AUR_ID;
                            param1[3] = new SqlParameter("@TPM_TP_ID", SqlDbType.Int);
                            param1[3].Value = ChildLst.TPM_TP_ID;
                            param1[4] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
                            param1[4].Value = lcmcode.LCM_CODE;
                            object value2 = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CL_INSERT_TYPE_LOCATION_MAPPING_DETAILS", param1);
                        }
                    }

                }
                result = "Success";
            //}

            return result;

        }
        catch
        {
            throw;
        }
    }
    public object GetGridDetails(GetTypeLcmData obj)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "CL_GET_TYPE_MAPPING_COUNT");
            sp.Command.AddParameter("@TPM_TP_ID", obj.TPM_TP_ID, DbType.Int32);
            using (IDataReader reader = sp.GetReader())
            {
                List<TypeLcmgriddata> Grid = new List<TypeLcmgriddata>();
                while (reader.Read())
                {
                    Grid.Add(new TypeLcmgriddata()
                    {
                        BCL_TP_NAME = reader["BCL_TP_NAME"].ToString(),
                        BCL_TP_ID = reader["BCL_TP_ID"].ToString(),
                        LCM_NAME = reader["LCM_NAME"].ToString(),
                        LCM_CODE = reader["LCM_CODE"].ToString(),
                        COUNT = Convert.ToInt32(reader["COUNT"]),
                        TPM_ID = Convert.ToInt32(reader["TPM_ID"]),
                    });
                }
                reader.Close();
                return new { data = Grid };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object editData(GetTypeLcmData obj)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_TYPEMAP_DETAILS_BASED_ON_LOC");
            sp.Command.AddParameter("@TPM_TP_ID", obj.TPM_TP_ID, DbType.Int32);
            sp.Command.AddParameter("@LCM_CODE", obj.LCM_CODE, DbType.String);

            sp.Command.AddParameter("@LEVEL", obj.LEVEL, DbType.Int32);

            TypeLcmMappingModel Userlist = new TypeLcmMappingModel();
            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Userlist.TPM_TP_ID = Convert.ToInt32(dr["BCL_TP_ID"]);
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    Userlist.COUNT = Convert.ToInt32(ds.Tables[1].Rows[0]["BCL_TP_LEVEL"].ToString());
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    Userlist.Apprlst = new List<Approverlst>();
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        Userlist.Apprlst.Add(new Approverlst { ROL_ID = Convert.ToString(dr["TPMD_APPR_ROL_ID"]), ROL_LEVEL = Convert.ToString(dr["TPMD_APPR_LEVELS"]) });
                    }
                }


                foreach (var rolid in Userlist.Apprlst)
                {
                    rolid.Emplst = new List<Employeelst>();
                    Employeelst ver;
                    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ROLE_BASED_EMP_CHECK");//GET_ROLE_BASED_EMP
                    sp.Command.AddParameter("@ROL", rolid.ROL_ID, DbType.Int32);
                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            ver = new Employeelst();
                            ver.AUR_ID = sdr["AUR_ID"].ToString();
                            ver.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                            rolid.Emplst.Add(ver);
                        }
                    }
                }


                foreach (var rol in Userlist.Apprlst)
                {
                    foreach (var rollst in rol.Emplst)
                    {


                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[3].Rows)
                            {
                                if (rollst.AUR_ID == Convert.ToString(dr["TPMD_APPR_AUR_ID"]) && rol.ROL_ID == Convert.ToString(dr["TPMD_APPR_ROL_ID"]))
                                {
                                    rollst.ticked = true;
                                }
                            }
                        }

                    }
                }

                if (ds.Tables[4].Rows.Count > 0)
                {
                    Userlist.lcmlst = new List<Locationlst>();
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        Userlist.lcmlst.Add(new Locationlst
                        {
                            LCM_CODE = Convert.ToString(dr["TPM_LCM_CODE"]),
                            LCM_NAME = Convert.ToString(dr["LCM_NAME"]),
                            CTY_CODE = Convert.ToString(dr["CTY_CODE"]),
                            CNY_CODE = Convert.ToString(dr["CNY_CODE"]),
                            ticked = true
                        });
                    }
                }
                //if (ds.Tables[5].Rows.Count > 0)
                //{
                //    Userlist.HDM_MAIN_MOD_ID = ds.Tables[5].Rows[0]["HDM_MAIN_MOD_ID"].ToString();
                //    Userlist.HDM_MAIN_MOD_NAME = ds.Tables[5].Rows[0]["HDM_MAIN_MOD_NAME"].ToString();
                //}
                //if (ds.Tables[5].Rows.Count > 0)
                //{
                //    Userlist.dsglst = new List<Designationlst>();
                //    foreach (DataRow dr in ds.Tables[5].Rows)
                //    {
                //        Userlist.dsglst.Add(new Designationlst { DSG_CODE = Convert.ToString(dr["DSG_CODE"]), DSG_NAME = Convert.ToString(dr["DSN_AMT_TITLE"]), ticked = true });
                //    }
                //}


            }
            return new { Message = MessagesVM.ErrorMessage, data = Userlist };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object UpdateDetails(TypeLcmMappingModel ChildLst)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@BCLTPID", SqlDbType.Int);
            param[0].Value = ChildLst.TPM_TP_ID;
            param[1] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(ChildLst.lcmlst);
            param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            param[3] = new SqlParameter("@CL_UPDATED_BY", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            string result = "";
            object value = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CL_UPDATE_TYPE_LOCATION_MAPPING", param);

            if (value.ToString() == "TYPE IS ALREADY MAPPED")
            {
                result = value.ToString();
            }
            else
            {
                foreach (var lcmcode in ChildLst.lcmlst)
                {

                    foreach (var opt in ChildLst.Apprlst)
                    {
                        foreach (var opt1 in opt.OpEmplst)
                        {
                            SqlParameter[] param1 = new SqlParameter[5];
                            param1[0] = new SqlParameter("@ROL_ID", SqlDbType.NVarChar);
                            param1[0].Value = opt.ROL_ID;
                            param1[1] = new SqlParameter("@ROL_LVL", SqlDbType.NVarChar);
                            param1[1].Value = opt.ROL_LEVEL;
                            param1[2] = new SqlParameter("@EMP", SqlDbType.NVarChar);
                            param1[2].Value = opt1.AUR_ID;
                            param1[3] = new SqlParameter("@TPM_TP_ID", SqlDbType.Int);
                            param1[3].Value = ChildLst.TPM_TP_ID;
                            param1[4] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
                            param1[4].Value = lcmcode.LCM_CODE;
                            object value2 = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CL_INSERT_TYPE_LOCATION_MAPPING_DETAILS", param1);
                        }
                    }

                }

                result = "Success";
            }

            return result;

        }
        catch
        {
            throw;
        }
    }


    public object DeleteSeat(TypeLcmgriddata tpm)
    {
        try
        {
            //string UID = id.Replace("__", "/");
            //UID = System.Web.HttpUtility.UrlDecode(UID); // to replace spaces and comma etc from %5C %22 %2F %3E %2C %3A with \ " / > , 
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@TPM_ID", SqlDbType.Int);
            param[0].Value = tpm.TPM_ID;
            SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CL_TYPE_LOC_DEL_MAP", param);
            return new { Message = "Record Has Been Succesfully Deleted" };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
}