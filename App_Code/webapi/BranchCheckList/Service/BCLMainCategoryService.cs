﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class BCLMainCategoryService
{
    SubSonic.StoredProcedure sp;
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");            
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetGridData(BCLMainCatModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMainCat");              
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.String);            
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(BCLMainCatModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMainCat");
            sp.Command.AddParameter("@BCL_MC_ID", 0, DbType.Int32);           
            sp.Command.AddParameter("@BCL_MC_NAME", CustRpt.BCL_MC_NAME, DbType.String);
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_MC_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BCL_MC_REM", CustRpt.BCL_MC_REM, DbType.String);
            sp.Command.AddParameter("@BCL_MC_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object updateData(BCLMainCatModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMainCat");
            sp.Command.AddParameter("@BCL_MC_ID", CustRpt.BCL_MC_ID, DbType.Int32);            
            sp.Command.AddParameter("@BCL_MC_NAME", CustRpt.BCL_MC_NAME, DbType.String);
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_MC_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BCL_MC_REM", CustRpt.BCL_MC_REM, DbType.String);
            sp.Command.AddParameter("@BCL_MC_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }
}