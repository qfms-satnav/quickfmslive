﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class BCLMainSubCategoryService
{
    SubSonic.StoredProcedure sp;
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");            
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetMainCategory(BCLMainSubCatModel CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMainCat");
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetGridData(BCLMainSubCatModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMainSubCat");              
            sp.Command.AddParameter("@BCL_SUB_MC_CODE", CustRpt.BCL_MC_CODE, DbType.String);            
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public object GetScoreData(BCLMainSubCatModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClCatSubCatScoreMapping");
            sp.Command.AddParameter("@BCL_SUB_ID", CustRpt.BCL_SUB_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_MC_CODE", CustRpt.BCL_MC_CODE, DbType.String); 
             ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(BCLMainSubCatModel CustRpt)
    {
        try
        {
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMainSubCat");
            //sp.Command.AddParameter("@BCL_SUB_ID", 0, DbType.Int32);           
            //sp.Command.AddParameter("@BCL_SUB_NAME", CustRpt.BCL_SUB_NAME, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_MC_CODE", CustRpt.BCL_MC_CODE, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_REM", CustRpt.BCL_SUB_REM, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //sp.Command.AddParameter("@BCL_SUB_TYPE", CustRpt.BCL_SUB_TYPE, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_FLAG_TYPE1", CustRpt.BCL_SUB_FLAG_TYPE1, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_FLAG_TYPE2", CustRpt.BCL_SUB_FLAG_TYPE2, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_FLAG_PLANTYPE", CustRpt.BCL_SUB_FLAG_PLANTYPE, DbType.String);
            ////sp.Execute();
            ////return new { data = CustRpt };
            //int flag = (int)sp.ExecuteScalar();
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter("@BCL_SUB_ID", SqlDbType.Int);
            param[0].Value = 0;
            param[1] = new SqlParameter("@BCL_SUB_NAME", SqlDbType.NVarChar);
            param[1].Value = CustRpt.BCL_SUB_NAME;
            param[2] = new SqlParameter("@BCL_SUB_MC_CODE", SqlDbType.NVarChar);
            param[2].Value = CustRpt.BCL_MC_CODE;
            param[3] = new SqlParameter("BCL_SUB_CREATED_BY", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            param[4] = new SqlParameter("@BCL_SUB_REM", SqlDbType.NVarChar);
            param[4].Value = CustRpt.BCL_SUB_REM;
            param[5] = new SqlParameter("@BCL_SUB_COMPANYID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["COMPANYID"];
            param[6] = new SqlParameter("@BCL_SUB_TYPE", SqlDbType.NVarChar);
            param[6].Value = CustRpt.BCL_SUB_TYPE;
            param[7] = new SqlParameter("@BCL_SUB_FLAG_TYPE1", SqlDbType.NVarChar);
            param[7].Value = CustRpt.BCL_SUB_FLAG_TYPE1;
            param[8] = new SqlParameter("@BCL_SUB_FLAG_TYPE2", SqlDbType.NVarChar);
            param[8].Value = CustRpt.BCL_SUB_FLAG_TYPE2;
            param[9] = new SqlParameter("@BCL_SUB_FLAG_PLANTYPE", SqlDbType.NVarChar);
            param[9].Value = CustRpt.BCL_SUB_FLAG_PLANTYPE;
            param[10] = new SqlParameter("@BCL_SUB_TYPE_NO", SqlDbType.NVarChar);
            param[10].Value = CustRpt.BCL_SUB_TYPE_NO;
            param[11] = new SqlParameter("@SCOREDETAILS", SqlDbType.Structured);
            param[11].Value = UtilityService.ConvertToDataTable(CustRpt.scoreA);
            int flag = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_AddUpdateBClMainSubCat", param);
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object updateData(BCLMainSubCatModel CustRpt)
    {
        try
        {
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMainSubCat");
            //sp.Command.AddParameter("@BCL_SUB_ID", CustRpt.BCL_SUB_ID, DbType.Int32);
            //sp.Command.AddParameter("@BCL_SUB_NAME", CustRpt.BCL_SUB_NAME, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_MC_CODE", CustRpt.BCL_MC_CODE, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_REM", CustRpt.BCL_SUB_REM, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //sp.Command.AddParameter("@BCL_SUB_TYPE", CustRpt.BCL_SUB_TYPE, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_FLAG_TYPE1", CustRpt.BCL_SUB_FLAG_TYPE1, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_FLAG_TYPE2", CustRpt.BCL_SUB_FLAG_TYPE2, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_FLAG_PLANTYPE", CustRpt.BCL_SUB_FLAG_PLANTYPE, DbType.String);
            //sp.Command.AddParameter("@BCL_SUB_TYPE_NO", CustRpt.BCL_SUB_TYPE_NO, DbType.Int32);
            //sp.Command.AddParameter("@SCOREDETAILS", CustRpt.scoreA, DbType.String);
            ////sp.Execute();
            ////return new { data = CustRpt };
            ///
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter("@BCL_SUB_ID", SqlDbType.Int);
            param[0].Value = CustRpt.BCL_SUB_ID;
            param[1] = new SqlParameter("@BCL_SUB_NAME", SqlDbType.NVarChar);
            param[1].Value = CustRpt.BCL_SUB_NAME;
            param[2] = new SqlParameter("@BCL_SUB_MC_CODE", SqlDbType.NVarChar);
            param[2].Value = CustRpt.BCL_MC_CODE;
            param[3] = new SqlParameter("BCL_SUB_CREATED_BY", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            param[4] = new SqlParameter("@BCL_SUB_REM", SqlDbType.NVarChar);
            param[4].Value = CustRpt.BCL_SUB_REM;
            param[5] = new SqlParameter("@BCL_SUB_COMPANYID", SqlDbType.NVarChar);
            param[5].Value = HttpContext.Current.Session["COMPANYID"];
            param[6] = new SqlParameter("@BCL_SUB_TYPE", SqlDbType.NVarChar);
            param[6].Value = CustRpt.BCL_SUB_TYPE;
            param[7] = new SqlParameter("@BCL_SUB_FLAG_TYPE1", SqlDbType.NVarChar);
            param[7].Value = CustRpt.BCL_SUB_FLAG_TYPE1;
            param[8] = new SqlParameter("@BCL_SUB_FLAG_TYPE2", SqlDbType.NVarChar);
            param[8].Value = CustRpt.BCL_SUB_FLAG_TYPE2;
            param[9] = new SqlParameter("@BCL_SUB_FLAG_PLANTYPE", SqlDbType.NVarChar);
            param[9].Value = CustRpt.BCL_SUB_FLAG_PLANTYPE;
            param[10] = new SqlParameter("@BCL_SUB_TYPE_NO", SqlDbType.NVarChar);
            param[10].Value = CustRpt.BCL_SUB_TYPE_NO;
            param[11] = new SqlParameter("@SCOREDETAILS", SqlDbType.Structured);
            param[11].Value = UtilityService.ConvertToDataTable(CustRpt.scoreA);
            int flag = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_AddUpdateBClMainSubCat", param);
            //int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object DeleteScore(SCOREDETAILSDELETE reldet)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BCL_CH_ID", SqlDbType.Int);
        param[0].Value = reldet.BCL_CH_ID;
        param[1] = new SqlParameter("@BCL_CH_STA_ID", SqlDbType.Int);
        param[1].Value = reldet.BCL_CH_STA_ID;
        int flag = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "spActiveInActiveScore", param);
        return flag;
        //if (obj != null)
        //    return new { Message = MessagesVM.UM_OK };
        //else
        //    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }
}