﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class BCLMainTypeService
{
    SubSonic.StoredProcedure sp;    
    public object GetGridData()
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");              
            //sp.Command.AddParameter("@AAS_AVR_CODE", CustRpt.AAS_AVR_CODE, DbType.String);            
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(BCLMainTypeModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMaintype");
            sp.Command.AddParameter("@BCL_TP_ID", 0, DbType.Int32);           
            sp.Command.AddParameter("@BCL_TP_NAME", CustRpt.BCL_TP_NAME, DbType.String);
            sp.Command.AddParameter("@BCL_TP_REMARKS", CustRpt.BCL_TP_REMARKS, DbType.String);
            sp.Command.AddParameter("@BCL_TP_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BCL_TP_LEVEL", CustRpt.BCL_TP_LEVEL, DbType.Int32);
            sp.Command.AddParameter("@BCL_TP_GPS", CustRpt.BCL_TP_GPS, DbType.Int32);
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object updateData(BCLMainTypeModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMaintype");
            sp.Command.AddParameter("@BCL_TP_ID", CustRpt.BCL_TP_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_TP_NAME", CustRpt.BCL_TP_NAME, DbType.String);
            sp.Command.AddParameter("@BCL_TP_REMARKS", CustRpt.BCL_TP_REMARKS, DbType.String);
            sp.Command.AddParameter("@BCL_TP_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BCL_TP_LEVEL", CustRpt.BCL_TP_LEVEL, DbType.Int32);
            sp.Command.AddParameter("@BCL_TP_GPS", CustRpt.BCL_TP_GPS, DbType.Int32);
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object GetGPS()
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_BCL_GPS");
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
}