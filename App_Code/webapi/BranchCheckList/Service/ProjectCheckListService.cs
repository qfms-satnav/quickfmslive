﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
using System.ComponentModel;

public class ProjectCheckListService
{
    SubSonic.StoredProcedure sp;

    public object getInspectors()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_INSPECTORS");
        sp.Command.Parameters.Add("@PRJCKL",2,DbType.Int16);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object getProjects()
    {

        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PCL_GET_PROJECTTYPES");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object getSavedList()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "PCL_GET_SAVED_LIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetGriddata(int pType)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_PROJECT_CHECKLIST_DETAILS");
            sp.Command.Parameters.Add("@PROJECTID", pType, DbType.Int32);
            ds = sp.GetDataSet();
                if (ds.Tables.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = ds.Tables };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
              
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object GetSavedDraftsdata(GetSavedDraftsList savedDraftsList)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_PROJECT_SAVED_CHECKLIST_DETAILS");
            sp.Command.Parameters.Add("@LCM_CODE", savedDraftsList.LCM_CODE, DbType.String);
            sp.Command.Parameters.Add("@INSP_BY", savedDraftsList.PM_CL_MAIN_INSP_BY, DbType.String);
            sp.Command.Parameters.Add("@INSP_DT", savedDraftsList.PM_CL_MAIN_VISIT_DT, DbType.DateTime);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object InsertCheckList(SaveProjectChecklist dataobj)
    {
        SqlParameter[] param = new SqlParameter[13];
        param[0] = new SqlParameter("@ProjectChecklistDetails", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobj.ChecklistDetails);
        param[1] = new SqlParameter("@Country", SqlDbType.NVarChar);
        param[1].Value = dataobj.Country;
        param[2] = new SqlParameter("@City", SqlDbType.VarChar);
        param[2].Value = dataobj.City;
        param[3] = new SqlParameter("@Location", SqlDbType.VarChar);
        param[3].Value = dataobj.Location;
        param[4] = new SqlParameter("@BusinessUnit", SqlDbType.VarChar);
        param[4].Value =dataobj.BusinessUnit;
        param[5] = new SqlParameter("@Inspectionby", SqlDbType.VarChar);
        param[5].Value = dataobj.Inspectionby;
        param[6] = new SqlParameter("@ProjectType", SqlDbType.VarChar);
        param[6].Value = dataobj.ProjectType;
        param[7] = new SqlParameter("@FromDate", SqlDbType.VarChar);
        param[7].Value = dataobj.FromDate;
        param[8] = new SqlParameter("@ProjectName", SqlDbType.VarChar);
        param[8].Value = dataobj.ProjectName;
        param[9] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[9].Value = HttpContext.Current.Session["UID"];
        param[10] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[10].Value = HttpContext.Current.Session["COMPANYID"];
        param[11] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[11].Value = dataobj.Flag;
        param[12] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
        param[12].Value = dataobj.OVERALL_CMTS;
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SAVE_PROJECTCHECKLIST_DETAILS", param);
        for (int i = 0; i < dataobj.imagesList.Count; i++)
        {
            SqlParameter[] param1 = new SqlParameter[7];
            param1[0] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param1[0].Value = dataobj.Location;
            param1[1] = new SqlParameter("@INSP_BY", SqlDbType.NVarChar);
            param1[1].Value = dataobj.Inspectionby;
            param1[2] = new SqlParameter("@INSP_DT", SqlDbType.NVarChar);
            param1[2].Value = dataobj.FromDate;
            param1[3] = new SqlParameter("@UPLOAD_PATH", SqlDbType.NVarChar);
            param1[3].Value = dataobj.imagesList[i].Imagepath;
            param1[4] = new SqlParameter("@FILENAME", SqlDbType.NVarChar);
            param1[4].Value = dataobj.imagesList[i].Imagepath;
            param1[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param1[5].Value = HttpContext.Current.Session["UID"];
            param1[6] = new SqlParameter("@REM", SqlDbType.NVarChar);
            param1[6].Value = "Uploaded From Web";
            SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "API_HD_INSERT_PM_CHECKLIST_FILES", param1);
        }
        int sem_id = (int)o;
        string msg;
        if (sem_id == 0)
            msg = "Saved as Draft";
        else
            msg = "CheckList Submitted Succesfully";
        return new { data = msg, Message = msg };
    }
}