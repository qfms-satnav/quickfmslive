﻿using DocumentFormat.OpenXml.Drawing.Charts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
using System.Data;
/// <summary>
/// Summary description for BCLRoleMappingService
/// </summary>
public class BCLRoleMappingService
{
    SubSonic.StoredProcedure sp;
    public object GetRoles(int id)
    {
        List<Rolelst> Rollst = new List<Rolelst>();
        Rolelst role;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + ".GETBCLROLES");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                role = new Rolelst();
                role.ROL_ID = sdr["ROL_ID"].ToString();
                role.ROL_ACRONYM = sdr["ROL_ACRONYM"].ToString();
                role.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                role.isChecked = false;
                Rollst.Add(role);
            }
        }
        if (Rollst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Rollst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetBCLUsers(int id)
    {
        List<BCLUser> Userlst = new List<BCLUser>();
        BCLUser Usr;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + ".GET_BCL_USERS");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Usr = new BCLUser();
                Usr.AUR_ID = sdr["AUR_ID"].ToString();
                Usr.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                Usr.URL_ROL_ID = sdr["URL_ROL_ID"].ToString();
                Usr.ticked = false;
                Userlst.Add(Usr);
            }
        }
        if (Userlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Userlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetBCLType(int id)
    {
        List<BCLType> BCLTypelst = new List<BCLType>();
        BCLType BCLType;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + ".BCL_TYPE_MAP");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                BCLType = new BCLType();
                BCLType.BCL_TP_ID = sdr["BCL_TP_ID"].ToString();
                BCLType.BCL_TP_NAME = sdr["BCL_TP_NAME"].ToString();
                BCLType.ticked = false;
                BCLTypelst.Add(BCLType);
            }
        }
        if (BCLTypelst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = BCLTypelst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetLocations(int id)
    {
        List<BCLLoc> Loc_lst = new List<BCLLoc>();
        BCLLoc Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + ".[GET_LOCTION_new]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new BCLLoc();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object SubmitData(BCLData data)
    {
        System.Data.DataTable DT = new System.Data.DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@Rolelst", SqlDbType.Structured);
            if (data.Rolelst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(data.Rolelst);
            }

            param[1] = new SqlParameter("@BCLUserlst", SqlDbType.Structured);
            if (data.BCLUserlst == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(data.BCLUserlst);
            }

            param[2] = new SqlParameter("@BCLTypelst", SqlDbType.Structured);
            if (data.BCLTypelst == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(data.BCLTypelst);
            }
            param[3] = new SqlParameter("@BCLLocList", SqlDbType.Structured);
            if (data.BCLLocList == null)
            {
                param[3].Value = null;
            }
            else
            {
                param[3].Value = UtilityService.ConvertToDataTable(data.BCLLocList);
            }
            param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"];
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure,"INSERT_BCL_MAPPING", param);
            if (DT.Rows.Count != 0)
            {
                return new { Message = MessagesVM.UM_OK, data = DT };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }

    }
    public object GetData()
    {
        System.Data.DataTable DT = new System.Data.DataTable();
        try
        {
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_BCL_MAPPING");
            if (DT.Rows.Count != 0)
            {
                return new { Message = MessagesVM.UM_OK, data = DT };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }

    }
    public object DeleteBCL(int ID)
    {
        System.Data.DataTable DT = new System.Data.DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@BCL_MAP_ID", SqlDbType.Int);
            param[0].Value = ID;
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "DELETE_BCL_BY_ID", param);
            if (DT.Rows.Count != 0)
            {
                return new { Message = MessagesVM.UM_OK, data = DT };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }

    }
}