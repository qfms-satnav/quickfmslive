﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using UtiltiyVM;
using System.Data.SqlClient;

public class ViewAndModifyScheduleService
{
    SubSonic.StoredProcedure sp;
    public object GetScheduleData()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SCHEDULE_DETAILS");
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            return ds;
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }

    public object EditScheduleData(ViewAndModifyScheduleModel VAMmodel)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_EDIT_SCHEDULES");
            sp.Command.AddParameter("@BCLDS_ID", VAMmodel.BCLDS_ID, DbType.Int32);
            sp.Command.AddParameter("@BCLDS_FROM_DATE", VAMmodel.BCLDS_FROM_DATE, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            return ds;
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }

    public object DeleteMySchedule(ViewAndModifyScheduleModel VAMmodel)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "DELETE_MY_SCHEDULE");
            sp.Command.AddParameter("@BCLDS_ID", VAMmodel.BCLDS_ID, DbType.Int32);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            return ds;
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }
    public object ModifyScheduleVisits(ViewAndModifyScheduleModel VAMmodel)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@ScheduleVisitsList", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(VAMmodel.ModifyScheduleVisitsList);

            param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[1].Value = HttpContext.Current.Session["UID"];

            param[2] = new SqlParameter("@BCLDS_ID", SqlDbType.Int);
            param[2].Value = VAMmodel.BCLDS_ID;

            using (DataSet dataset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MODIFY_SCHEDULE_VISITS", param))
            {
                if (dataset.Tables.Count > 0)
                {
                    return new { Message = MessagesVM.AF_OK, data = dataset.Tables[0] };
                }
                else
                {
                    return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
                }
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.UM_Not_OK, data = (object)null };
        }
    }

}