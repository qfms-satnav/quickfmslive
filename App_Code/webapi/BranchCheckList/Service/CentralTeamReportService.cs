﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for CentralTeamReportService
/// </summary>
public class CentralTeamReportService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object CentralTeamReport(CentraTeamRequestModel SelectedDate)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[CENTRAL_TEAM_REPORT]");
            sp.Command.Parameters.Add("@FromDate", SelectedDate.FromDate, DbType.DateTime);
            sp.Command.Parameters.Add("@UserID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@ToDate", SelectedDate.ToDate, DbType.DateTime);
            sp.Command.Parameters.Add("@Type", SelectedDate.Type, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;
        }
        catch (Exception ex)
        {
            return new { Message = UtiltiyVM.MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_MainType");
            sp.Command.AddParameter("@user_id", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
}