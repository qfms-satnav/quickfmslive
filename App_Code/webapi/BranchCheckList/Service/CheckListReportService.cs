﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class CheckListReportService
{
    SubSonic.StoredProcedure sp;
    List<CheckListReportModel> CustData;
    CheckListReportModel Custm;
    DataSet ds;
    public object getInspectors()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_INSPECTORS");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public DataTable GetImage(ImgParams model)
    {
        try
        {
            List<CheckListReportModel> CData = new List<CheckListReportModel>();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
            if (model.LCMLST == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(model.LCMLST);
            }
            param[1] = new SqlParameter("@Aur_id", SqlDbType.VarChar);
            param[1].Value = model.INSPCTED_BY;
            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[2].Value = model.INSPCTED_DT;
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "BCL_CHECKLIST_IMG_DETAILS", param);
            if (ds.Tables[0].Rows.Count != 0)
            {
                return ds.Tables[0];
            }
            else {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public object GetBusinessCardobject(CustParams param)
    {
        try
        {
            CustData = LoadGrid(param);

            if (CustData.Count != 0)

                return new { Message = MessagesVM.UM_NO_REC, data = CustData };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public List<CheckListReportModel> LoadGrid(CustParams Details)
    {
        try
        {
            List<CheckListReportModel> CData = new List<CheckListReportModel>();
            SqlParameter[] param = new SqlParameter[4];           
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);


            if (Details.LCMLST == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Details.LCMLST);
            }

            param[1] = new SqlParameter("@CNP_LST", SqlDbType.Structured);
            
            if (Details.CNPLST == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(Details.CNPLST);
            }           

            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[2].Value = Details.FromDate;

            param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[3].Value = Details.ToDate;
            //DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "BCL_CHECKLIST_REPORT", param);
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BCL_CHECKLIST_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new CheckListReportModel();
                    Custm.CNY_NAME = Convert.ToString(reader["CNY_NAME"]);
                    Custm.CTY_NAME = Convert.ToString(reader["CTY_NAME"]);
                    Custm.LCM_CODE = Convert.ToString(reader["LCM_CODE"]);
                    Custm.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                    Custm.BCL_MC_NAME = Convert.ToString(reader["BCL_MC_NAME"]);
                    Custm.BCL_SUB_NAME = Convert.ToString(reader["BCL_SUB_NAME"]);
                    Custm.BCLD_SCORE_NAME = Convert.ToString(reader["BCLD_SCORE_NAME"]);
                    Custm.BCL_INSPECTED_BY = Convert.ToString(reader["BCL_INSPECTED_BY"]);
                    Custm.BCL_SELECTED_DT = (DateTime)reader["BCL_SELECTED_DT"];
                    Custm.SELECTED_CNP = Convert.ToString(reader["SELECTED_CNP"]);
                    Custm.BCL_CREATED_DT = (DateTime)reader["BCL_CREATED_DT"];
                    Custm.BCL_CREATED_BY = Convert.ToString(reader["BCL_CREATED_BY"]);                  
                    Custm.BCLD_SCORE_CODE = Convert.ToString(reader["BCLD_SCORE_CODE"]);
                    Custm.BCLD_TEXTDATA = Convert.ToString(reader["BCLD_TEXTDATA"]);
                    Custm.BCLD_DATE = (reader["BCLD_DATE"]) == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["BCLD_DATE"]); 
                    Custm.BCL_OVERALL_CMTS = Convert.ToString(reader["BCL_OVERALL_CMTS"]);
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch(Exception ex)
        {
            throw;
        }
    }

    //public List<CheckListReportModel> ExportGrid(CustParams Details)
    //{
    //    try
    //    {
    //        List<CheckListReportModel> CData = new List<CheckListReportModel>();
    //        SqlParameter[] param = new SqlParameter[4];
    //        param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);


    //        if (Details.LCMLST == null)
    //        {
    //            param[0].Value = null;
    //        }
    //        else
    //        {
    //            param[0].Value = UtilityService.ConvertToDataTable(Details.LCMLST);
    //        }

    //        param[1] = new SqlParameter("@CNP_LST", SqlDbType.Structured);

    //        if (Details.CNPLST == null)
    //        {
    //            param[1].Value = null;
    //        }
    //        else
    //        {
    //            param[1].Value = UtilityService.ConvertToDataTable(Details.CNPLST);
    //        }

    //        param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
    //        param[2].Value = Details.FromDate;

    //        param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
    //        param[3].Value = Details.ToDate;
    //        DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "BCL_CHECKLIST_REPORT", param);
    //        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BCL_CHECKLIST_REPORT", param))
    //        {
    //            while (reader.Read())
    //            {
    //                Custm = new CheckListReportModel();
    //                Custm.CNY_NAME = Convert.ToString(reader["CNY_NAME"]);
    //                Custm.CTY_NAME = Convert.ToString(reader["CTY_NAME"]);
    //                Custm.LCM_CODE = Convert.ToString(reader["LCM_CODE"]);
    //                Custm.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
    //                Custm.BCL_MC_NAME = Convert.ToString(reader["BCL_MC_NAME"]);
    //                Custm.BCL_SUB_NAME = Convert.ToString(reader["BCL_SUB_NAME"]);
    //                Custm.BCL_SCORE_NAME = Convert.ToString(reader["BCL_SCORE_NAME"]);
    //                Custm.BCL_INSPECTED_BY = Convert.ToString(reader["BCL_INSPECTED_BY"]);
    //                Custm.BCL_SELECTED_DT = (DateTime)reader["BCL_SELECTED_DT"];
    //                Custm.SELECTED_CNP = Convert.ToString(reader["SELECTED_CNP"]);
    //                Custm.BCL_CREATED_DT = (DateTime)reader["BCL_CREATED_DT"];
    //                Custm.BCL_CREATED_BY = Convert.ToString(reader["BCL_CREATED_BY"]);
    //                CData.Add(Custm);
    //            }
    //            reader.Close();
    //        }
    //        return CData;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw;
    //    }
    //}
}
