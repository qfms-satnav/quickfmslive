﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class BCLCatSubCatScoreMappingService
{
    SubSonic.StoredProcedure sp;
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");            
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetMainCategory(BCLCatSubCatScoreMappingModel CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMainCat");
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetSubCategory(BCLCatSubCatScoreMappingModel CustRpt)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMainSubCat");
            sp.Command.AddParameter("@BCL_SUB_MC_CODE", CustRpt.BCL_CH_MNC_CODE, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }

    public object GetGridData(BCLCatSubCatScoreMappingModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClCatSubCatScoreMapping");              
            sp.Command.AddParameter("@BCL_CH_SUB_CODE", CustRpt.BCL_CH_SUB_CODE, DbType.String);            
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(BCLCatSubCatScoreMappingModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClCatSubCatScoreMapping");            
            sp.Command.AddParameter("@BCL_CH_ID", 0, DbType.Int32);           
            sp.Command.AddParameter("@BCL_CH_NAME", CustRpt.BCL_CH_NAME, DbType.String);
            sp.Command.AddParameter("@BCL_CH_SUB_CODE", CustRpt.BCL_CH_SUB_CODE, DbType.String);
            sp.Command.AddParameter("@BCL_CH_MNC_CODE", CustRpt.BCL_CH_MNC_CODE, DbType.String);
            sp.Command.AddParameter("@BCL_CH_TYPE", CustRpt.BCL_CH_TYPE, DbType.String);
            sp.Command.AddParameter("@BCL_CH_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BCL_CH_REMARKS", CustRpt.BCL_CH_REMARKS, DbType.String);
            sp.Command.AddParameter("@BCL_CH_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);           
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object updateData(BCLCatSubCatScoreMappingModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClCatSubCatScoreMapping");
            sp.Command.AddParameter("@BCL_CH_ID", CustRpt.BCL_CH_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_CH_NAME", CustRpt.BCL_CH_NAME, DbType.String);
            sp.Command.AddParameter("@BCL_CH_SUB_CODE", CustRpt.BCL_CH_SUB_CODE, DbType.String);
            sp.Command.AddParameter("@BCL_CH_MNC_CODE", CustRpt.BCL_CH_MNC_CODE, DbType.String);
            sp.Command.AddParameter("@BCL_CH_TYPE", CustRpt.BCL_CH_TYPE, DbType.String);
            sp.Command.AddParameter("@BCL_CH_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BCL_CH_REMARKS", CustRpt.BCL_CH_REMARKS, DbType.String);
            sp.Command.AddParameter("@BCL_CH_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }
}