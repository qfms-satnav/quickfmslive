﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ProjectCheckListReportService
/// </summary>
public class ProjectCheckListReportService
{
    SubSonic.StoredProcedure sp;
    List<ProjectCheckListReportModel> CustData;
    ProjectCheckListReportModel Custm;
    DataSet ds;
    public object getInspectors()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_INSPECTORS");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetBusinessCardobject(CustParams param)
    {
        try
        {
            CustData = LoadGrid(param);

            if (CustData.Count != 0)

                return new { Message = MessagesVM.UM_NO_REC, data = CustData };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
    public DataTable GetImage(PrjImgParams model)
    {
        try
        {
            List<CheckListReportModel> CData = new List<CheckListReportModel>();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
            if (model.LCMLST == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(model.LCMLST);
            }
            param[1] = new SqlParameter("@INSP_BY", SqlDbType.VarChar);
            param[1].Value = model.INSPCTED_BY;
            param[2] = new SqlParameter("@INSP_DT", SqlDbType.DateTime);
            param[2].Value = model.INSPCTED_DT;
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PCL_CHECKLIST_IMAGE_REPORT", param);
            if (ds.Tables[0].Rows.Count != 0)
            {
                return ds.Tables[0];
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public List<ProjectCheckListReportModel> LoadGrid(CustParams Details)
    {
        try
        {
            List<ProjectCheckListReportModel> CData = new List<ProjectCheckListReportModel>();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);


            if (Details.LCMLST == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Details.LCMLST);
            }

            param[1] = new SqlParameter("@CNP_LST", SqlDbType.Structured);

            if (Details.CNPLST == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(Details.CNPLST);
            }

            param[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param[2].Value = Details.FromDate;

            param[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param[3].Value = Details.ToDate;
            //DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PCL_CHECKLIST_REPORT", param);
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PCL_CHECKLIST_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new ProjectCheckListReportModel();
                    Custm.CNY_NAME = Convert.ToString(reader["CNY_NAME"]);
                    Custm.CTY_NAME = Convert.ToString(reader["CTY_NAME"]);
                    Custm.LCM_CODE = Convert.ToString(reader["LCM_CODE"]);
                    Custm.LCM_NAME = Convert.ToString(reader["LCM_NAME"]);
                    Custm.CNP_NAME = Convert.ToString(reader["CNP_NAME"]);
                    Custm.PM_CL_MAIN_PRJ_NAME = Convert.ToString(reader["PM_CL_MAIN_PRJ_NAME"]);
                    Custm.PM_CL_MAIN_INSP_BY = Convert.ToString(reader["PM_CL_MAIN_INSP_BY"]);
                    Custm.PM_CL_MAIN_PRJ_TYPE = Convert.ToString(reader["PM_CL_MAIN_PRJ_TYPE"]);
                    Custm.PM_CL_CAT_NAME = Convert.ToString(reader["PM_CL_CAT_NAME"]);
                    Custm.PM_CL_SUBCAT_NAME = Convert.ToString(reader["PM_CL_SUBCAT_NAME"]);
                    Custm.PM_CL_CHILDCAT_NAME = Convert.ToString(reader["PM_CL_CHILDCAT_NAME"]);
                    //Custm.BCL_CREATED_DT = (DateTime)reader["BCL_CREATED_DT"];
                    Custm.PM_CLD_CHILDCAT_VALUE = Convert.ToString(reader["PM_CLD_CHILDCAT_VALUE"]);
                    Custm.PM_CL_SCC_NAME = Convert.ToString(reader["PM_CL_SCC_NAME"]);
                    //Custm.BCLD_TEXTDATA = Convert.ToString(reader["BCLD_TEXTDATA"]);
                    Custm.PM_CL_MAIN_VISIT_DT = (reader["PM_CL_MAIN_VISIT_DT"]) == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["PM_CL_MAIN_VISIT_DT"]);
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch (Exception ex)
        {
            throw;
        }
    }

}