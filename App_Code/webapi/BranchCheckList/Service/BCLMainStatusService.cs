﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class BCLMainStatusService
{
    SubSonic.StoredProcedure sp;
    public object GetMainType()
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMaintype");
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }

    }
    public object GetGridData(BCLMainStatusModel CustRpt)
    {

        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SP_GetBClMainStatus");
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.String);
            sp.Command.AddParameter("@BCL_MC_TYPENAME", CustRpt.BCL_MC_TYPENAME, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception e)
        {
            throw e;
        }
    }
    public object SaveData(BCLMainStatusModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMainStatus");
            sp.Command.AddParameter("@BCL_MC_STATUS_ID", 0, DbType.Int32);           
            sp.Command.AddParameter("@BCL_MC_STATUS", CustRpt.BCL_MC_STATUS, DbType.String);
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_MC_TYPENAME", CustRpt.BCL_MC_TYPENAME, DbType.String);
            sp.Command.AddParameter("@BCL_MC_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);         
            sp.Command.AddParameter("@BCL_MC_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }

    public object updateData(BCLMainStatusModel CustRpt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SP_AddUpdateBClMainStatus");
            sp.Command.AddParameter("@BCL_MC_STATUS_ID", CustRpt.BCL_MC_STATUS_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_MC_STATUS", CustRpt.BCL_MC_STATUS, DbType.String);
            sp.Command.AddParameter("@BCL_TP_MC_ID", CustRpt.BCL_TP_MC_ID, DbType.Int32);
            sp.Command.AddParameter("@BCL_MC_TYPENAME", CustRpt.BCL_MC_TYPENAME, DbType.String);
            sp.Command.AddParameter("@BCL_MC_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@BCL_MC_COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //sp.Execute();
            //return new { data = CustRpt };
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message, data = (object)null };
        }

    }
    public object Get_Branch_Images(Branch_filedetails filedetails)
    {
        List<Branch_Images_List> SysPrflst = new List<Branch_Images_List>();
        sp = new SubSonic.StoredProcedure(string.Concat(string.Concat(filedetails.CLIENTNAME.ToString() , ".dbo.") , "[GET_BRANCH_IMAGES_LIST]"));
        sp.Command.AddParameter("@BCLID", filedetails.BCLID, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Branch_Images_List SysPrf = new Branch_Images_List();
                SysPrf.BCL_MC_NAME = sdr["BCL_MC_NAME"].ToString();
                SysPrf.BCL_SUB_NAME = sdr["BCL_SUB_NAME"].ToString();
                SysPrf.BCLD_FILE_UPLD = sdr["BCLD_FILE_UPLD"].ToString();
                SysPrf.File_Folder = string.Concat(filedetails.CLIENTNAME.ToString(), ".dbo");
                SysPrflst.Add(SysPrf);
            }
        }
        if (SysPrflst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SysPrflst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
}