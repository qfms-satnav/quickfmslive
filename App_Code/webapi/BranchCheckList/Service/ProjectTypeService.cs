﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for ProjectTypeService
/// </summary>
public class ProjectTypeService
{
    SubSonic.StoredProcedure sp;
    //Insert
    public int Save(ProjectTypeModel PM)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ProjectTYPE_INSERT");
            sp.Command.AddParameter("@Prj_CODE", PM.Prj_Code, DbType.String);
            sp.Command.AddParameter("@Prj_NAME", PM.Prj_Name, DbType.String);
            sp.Command.Parameters.Add("@User", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }
    public object BindGrid()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_PROJECT_TYPES");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public int Update(ProjectTypeModel PM)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ProjectTYPE_Update");
            sp.Command.AddParameter("@Prj_CODE", PM.Prj_Code, DbType.String);
            sp.Command.AddParameter("@Prj_NAME", PM.Prj_Name, DbType.String);
            sp.Command.AddParameter("@Status", PM.Prj_Sta_Id, DbType.String);
            sp.Command.Parameters.Add("@User", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }
}