﻿using Newtonsoft.Json;
using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

[Authorize]
public class CopilotController : ApiController
{
    CopilotService CBAIS = new CopilotService();


    //Menu 
    [HttpPost]
    public object GetMenu(CPModel Model)
    {
        object res = CBAIS.GetMenu(Model);
        return res;

    }
    [HttpPost]
    public object GetSubMenu(CPModel Model)
    {
        object res = CBAIS.GetSubMenu(Model);
        return res;
    }
    [HttpPost]
    public object GetMenuLinks(CPModel Model)
    {
        object res = CBAIS.GetMenuLinks(Model);
        return res;
    }


    //Space Module
    //Get single seat
    [HttpPost]
    public Object UserSapceHistoryDetails(CPModel Model)
    {
        object res = CBAIS.UserSapceHistoryDetails(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    //Get last 3 bookings seat
    [HttpPost]
    public Object UserSelectSapceDetails(CPModel Model)
    {
        object res = CBAIS.UserSelectSapceDetails(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    ////Get Shifts based on sapce
    //[HttpPost]
    //public Object ShiftDetailsBySpaceID(CPModel Model)
    //{
    //    object res = CBAIS.ShiftDetailsBySpaceID(Model);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
    //    return response;
    //}


    //Get FLOORS based on user Location
    [HttpPost]
    public Object GETFLOORSBYUSERID(CPModel Model)
    {
        object res = CBAIS.GETFLOORSBYUSERID(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    //Get single vacant seat based on Floor
    [HttpPost]
    public Object GETVACANTSPACESBYFLOOR(CPModel Model)
    {
        object res = CBAIS.GETVACANTSPACESBYFLOOR(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    //Get Current Booking
    [HttpPost]
    public Object GETCURRENTBOOKING(CPModel Model)
    {
        object res = CBAIS.GETCURRENTBOOKING(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    // Book a seat
    [GzipCompression]
    [HttpPost]
    public object AllocateSeatsCopilot(CP_SPACE_ALLOCATE_DETAILS_LST allocDetLst)
    {
        return CBAIS.AllocateSeatsCopilot(allocDetLst);
    }

    // Mapping the location
    [HttpPost]
    public HttpResponseMessage LocationMapping(CPModel CPM)
    {

        var obj = CBAIS.LocationMapping(CPM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    // Release the Seat
    [GzipCompression]
    [HttpPost]
    public object ReleaseSeatCopilot(CP_SPACE_REL_DETAILS RLSeat)
    {
        return CBAIS.ReleaseSeatCopilot(RLSeat);
    }


    // SpaceCustomizable Report
    [HttpPost]
    public Object DownloadSpaceReport([FromBody] Helpdesk hd)
    {
        object res = CBAIS.DownloadSpaceReport(hd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }



    //HD Module

    // Child cat's Based on Sub cat's
    [HttpPost]
    public HttpResponseMessage ChidCatInSubCat(HDRaiseRequest child)
    {
        ErrorHandler err = new ErrorHandler();
        var obj = CBAIS.ChidCatInSubCat(child);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }



    // Raise request 
    [HttpPost]
    public HttpResponseMessage SubmitRaiseRequestAI(HDRaiseRequest child)
    {
        ErrorHandler err = new ErrorHandler();
        var obj = CBAIS._RaiseHDRequestAI(child);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public Object GetInchargeByReqID(Helpdesk hd)
    {
        object res = CBAIS.GetInchargeByReqID(hd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [HttpPost]
    public Object GetStatusAndSlaByReqID(Helpdesk hd)
    {
        object res = CBAIS.GetStatusAndSlaByReqID(hd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [HttpPost]
    public Object GetClosedTickets(Helpdesk hd)
    {
        object res = CBAIS.GetClosedTickets(hd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [HttpPost]
    public Object DownloadReport([FromBody] Helpdesk hd)
    {
        object res = CBAIS.DownloadReport(hd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [HttpPost]
    public Object CancelTodayReservationBooking(Helpdesk hd)
    {
        object res = CBAIS.CancelTodayReservationBooking(hd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [HttpPost]
    public Object MeetingSchedulesToday(Helpdesk hd)
    {
        object res = CBAIS.MeetingSchedulesToday(hd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [HttpPost]
    public object PendingReqApproval(AST_COP_REQ COPAST)
    {
        object res = CBAIS.PendingReqApproval(COPAST);
        return res;
    }

    [HttpPost]
    public object Cop_Approve_Req(AST_COP_REQ COPAST)
    {
        object res = CBAIS.Cop_Approve_Req(COPAST);
        return res;
    }

    [HttpPost]
    public object Bk_Meet_Room(COP_RESERVE Resrv)
    {
        object res = CBAIS.Bk_Meet_Room(Resrv);
        return res;
    }
    [HttpPost]
    public Object GetPendingValidationChk(CPModel Model)
    {
        var res = CBAIS.GetPendingValidationChk(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [HttpPost]
    public Object GetPendingApprovalChk(CPModel Model)
    {
        var res = CBAIS.GetPendingApprovalChk(Model);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [HttpPost]
    public object GetAstLowStockList(AST_COP_REQ COPAST)
    {
        object res = CBAIS.GetAstLowStockList(COPAST);
        return res;
    }
    [HttpPost]
    public object TodaysPos(AST_COP_REQ COPAST)
    {
        object res = CBAIS.TodaysPos(COPAST);
        return res;
    }
    [HttpPost]
    public object Ast_Tot_Val(AST_COP_REQ COPAST)
    {
        object res = CBAIS.Ast_Tot_Val(COPAST);
        return res;
    }
    [HttpPost]
    public object PVM_Today_Active(MM_COP_REQ COMM)
    {
        object res = CBAIS.PVM_Today_Active(COMM);
        return res;
    }
    [HttpPost]
    public object PVM_CurrnentMonthCost(MM_COP_REQ COMM)
    {
        object res = CBAIS.PVM_CurrnentMonthCost(COMM);
        return res;
    }
    [HttpPost]
    public object ExpiringAMC(MM_COP_REQ COMM)
    {
        object res = CBAIS.ExpiringAMC(COMM);
        return res;
    }
    [HttpPost]
    public object UpcommingMMActive(MM_COP_REQ COMM)
    {
        object res = CBAIS.UpcommingMMActive(COMM);
        return res;
    }
    [HttpPost]
    public object Avlb_GH(GH_COP_REQ GH)
    {
        object res = CBAIS.Avlb_GH(GH);
        return res;
    }
    [HttpPost]
    public object Total_Prty_Lease_Cost(PROPERTY_MODULE TC)
    {
        object res = CBAIS.Total_Prty_Lease_Cost(TC);
        return res;
    }
    [HttpPost]
    public object Ren_Nxt_Quarter(PROPERTY_MODULE TC)
    {
        object res = CBAIS.Ren_Nxt_Quarter(TC);
        return res;
    }
    [HttpPost]
    public object Cur_Avail_Book(COP_RESERVE Resrv)
    {
        object res = CBAIS.Cur_Avail_Book(Resrv);
        return res;
    }
    [HttpPost]
    public object Avail_Time_Book(COP_RESERVE Resrv)
    {
        object res = CBAIS.Avail_Time_Book(Resrv);
        return res;
    }
    [HttpPost]
    public object Edit_My_Active_Bookings(COP_RESERVE Resrv)
    {
        object res = CBAIS.Edit_My_Active_Bookings(Resrv);
        return res;
    }
    // Branch Checklist
    [HttpPost]
    public object InsertChecklist(ChecklistIns data)
    {
        object res = CBAIS.InsertChecklist(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [HttpPost]
    public object getLocation(ChecklistIns data)
    {
        object res = CBAIS.getLocation(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [HttpPost]
    public object getType(ChecklistIns data)
    {
        object res = CBAIS.getType(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [HttpPost]
    public object ChkGetSubLst(ChecklistIns data)
    {
        object res = CBAIS.ChkGetSubLst(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [HttpPost]
    public object ChkGetLstMnth(ChecklistIns data)
    {
        object res = CBAIS.ChkGetLstMnth(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
}

