﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// Summary description for HDController1
/// </summary>
public partial class HDController : ApiController
{ 

    #region+++++++++++++++++++++Check List+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET")]
    [HttpGet]
    public HttpResponseMessage GetMainType(string CompanyId, string UserID)
    {
        var obj = HDSvc.GetMainType(CompanyId, UserID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    //public HttpResponseMessage getInspectorsValidatorsApprover(string CompanyId, int TPM_TP_ID, string TPM_LCM_CODE, int TPMD_APPR_LEVELS)
    public HttpResponseMessage getInspectorsValidatorsApprover(InspectorsValidatorsApprover iva)
    {
        var obj = HDSvc.getInspectorsValidatorsApprover(iva);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET")]
    [HttpGet]
    public HttpResponseMessage getInspectors(string CompanyId)
    {
        var obj = HDSvc.getInspectors(CompanyId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetGriddata(GridDataCheck gd)
    {
        var obj = HDSvc.GetGriddata(gd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage getSavedListInspector(SavedList sv)
    {
        var obj = HDSvc.getSavedListInspector(sv);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage getSavedListZonalCentral(SavedListZonalCentral svz)
    {
        var obj = HDSvc.getSavedListZonalCentral(svz);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage getCheckedListDetail(CheckedListDetail chk)
    {
        var obj = HDSvc.getCheckedListDetail(chk);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage getZonalCentralStatus(ZonalCentralStatus zcs)
    {
        var obj = HDSvc.getZonalCentralStatus(zcs);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage DeleteCheckList(DeleteCheck Dc)
    {
        var obj = HDSvc.DeleteCheckList(Dc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage DynamicInsertCheckList(selecteddata dataobj)
    {
        var obj = HDSvc.DynamicInsertCheckList(dataobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

   
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetUser(HDModel hDModel)
    {
        var obj = HDSvc.GetUserDetails(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //public partial class HDController
    //{
    //// code
    //HDService HDSvc = new HDService();
    ////Spare Dashboard for Ekart
    //[AllowAnonymous]
    //[System.Web.Http.AcceptVerbs("POST")]
    //[HttpPost]
    //public HttpResponseMessage GetSpareDashboard(SpareDashboardModel spareDashboardModel)
    //{
    //    object res = HDSvc.GetSpareDashboard(spareDashboardModel);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
    //    return response;

    //}
    //}
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetChecklist_DataV2(NewCheckListV2 newCheckListV2)
    {
        var obj = HDSvc.NewCheckListV2(newCheckListV2);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetChecklist_Data_Validation(NewCheckListV2 newCheckListV2)
    {
        var obj = HDSvc.GetChecklist_Data_Validation(newCheckListV2);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetChecklist_Data_Approval(NewCheckListV2 newCheckListV2)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = HDSvc.GetChecklist_Data_Approval(newCheckListV2);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #endregion
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage BindDesignation(HDModel hDModel)
    {
        var obj = HDSvc.BindDesignation(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetScheduleData(HDModel hDModel)
    {
        //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
        //// Filter a specific claim by type
        //var userNameClaim = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase));
        //// Access the value if the claim is found
        //var userName = (userNameClaim != null) ? userNameClaim.Value : null;
        //if (userName != hDModel.UserId)
        //{
        //    HttpResponseMessage responser = Request.CreateResponse(HttpStatusCode.BadRequest, "Not a valid User for this Request");
        //    return responser;
        //}
        var obj = HDSvc.GetScheduleData(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CreateScheduleVisits(CreateScheduleVisits CSV)
    {
        //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
        //// Filter a specific claim by type
        //var userNameClaim = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase));
        //// Access the value if the claim is found
        //var userName = (userNameClaim != null) ? userNameClaim.Value : null;
        //if (userName != CSV.UserId)
        //{
        //    HttpResponseMessage responser = Request.CreateResponse(HttpStatusCode.BadRequest, "Not a valid User for this Request");
        //    return responser;
        //}
        //if (ModelState.IsValid)
        //{
        var obj = HDSvc.CreateScheduleVisits(CSV);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //}
        //else
        //{
        //    var message = string.Join(" | ", ModelState.Values
        //.SelectMany(v => v.Errors)
        //.Select(e => e.ErrorMessage));
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, message);
        //    return response;
        //}

    }

    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage ModifyScheduleVisits(ViewAndModifyScheduleModel VAMmodel)
    {
        //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
        //// Filter a specific claim by type
        //var userNameClaim = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase));
        //// Access the value if the claim is found
        //var userName = (userNameClaim != null) ? userNameClaim.Value : null;
        //if (userName != VAMmodel.UserId)
        //{
        //    HttpResponseMessage responser = Request.CreateResponse(HttpStatusCode.BadRequest, "Not a valid User for this Request");
        //    return responser;
        //}
        var obj = HDSvc.ModifyScheduleVisits(VAMmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage DeleteMySchedule(ViewAndModifyScheduleModel VAMmodel)
    {
        //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
        //// Filter a specific claim by type
        //var userNameClaim = claims.FirstOrDefault(x => string.Equals(x.Type, ClaimTypes.NameIdentifier, StringComparison.OrdinalIgnoreCase));
        //// Access the value if the claim is found
        //var userName = (userNameClaim != null) ? userNameClaim.Value : null;
        //if (userName != VAMmodel.UserId)
        //{
        //    HttpResponseMessage responser = Request.CreateResponse(HttpStatusCode.BadRequest, "Not a valid User for this Request");
        //    return responser;
        //}
        var obj = HDSvc.DeleteMySchedule(VAMmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetLocationByCity_CheckList(NewCheckListV2_For_Location newCheckListV2)
    {

        var obj = HDSvc.GetLocationByCity_CheckList(newCheckListV2);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage LocationCords(LocationCords LCM_CORD)
    {
        var obj = HDSvc.LocationCords(LCM_CORD);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage DeleteDraftBCL(DeleteCheck DDrfModel)
    {
        var obj = HDSvc.DeleteDraftBCL(DDrfModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}