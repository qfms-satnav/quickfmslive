﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using QuickFMS.API.Filters;


public partial class HDController : ApiController
{
    HDService HDSvc = new HDService();
    APIIntegrationService APISvc = new APIIntegrationService();
    NotificationService notificationService = new NotificationService();
    //Release Space Details

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage ReleaseSelectedseat(HD_SPACE_REL_DETAILS sad)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("SpaceReleaseDetails:" + JsonConvert.SerializeObject(sad));
        var obj = HDSvc.ReleaseSelectedseat(sad);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object GetLegendsSummary([FromBody] Maplistfloors svm)
    {
        // return mspsrvc.GetLegendsSummary(svm);
        var obj = HDSvc.GetLegendsSummary(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public object GetAllocEmpDetails([FromBody] Space_mapVM svm)
    {
        var obj = HDSvc.GetAllocEmpDetails(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GeyFloorbyentity(RaiseRequest RRST)
    {
        var obj = HDSvc.GeyFloorbyentity(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    //QRCODE STATUS UPDATE for OPTUM

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage QRCodeStatus(QR_CODE_STUS HDSAC)
    {
        var obj = HDSvc._QRCodeStatus(HDSAC);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage DIRECTFINDPATH(QR_CODE_STUS HDSAC)
    {
        var obj = HDSvc.DIRECTFINDPATH(HDSAC);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //Get space from EmpID for OPTUM
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetSpaceFromEMPID(QR_CODE_STUS HDSAC)
    {
        var obj = HDSvc._GetSpaceFromEMPID(HDSAC);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object GetLegendsCount([FromBody] Maplistfloors svm)
    {
        var obj = HDSvc.GetLegendsCount(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public HttpResponseMessage GetShifts([FromBody] Locationlstt Location)
    {
        var obj = HDSvc.GetShifts(Location);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // Insert Contact Details
    //[AllowAnonymous]
    //[System.Web.Http.AcceptVerbs("GET", "POST")]
    //[HttpPost]
    ////public HttpResponseMessage InsertContactDetails([FromBody]HD_INSERT_CONTACT_DETAILS main)
    ////{

    ////    ErrorHandler err = new ErrorHandler();
    ////    err._WriteErrorLog_string("InsertContactDetails:" + JsonConvert.SerializeObject(main));
    ////    var obj = HDSvc.InsertContactDetails(main);
    ////    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    ////    return response;
    ////}

    // Get SpaceMap Details
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetSpaceMapDetails([FromBody] SpaceMapDet main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("SpaceMapDetails:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetBindMapDetails(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object GetBookedSeatsByUser([FromBody] HD_SPACE_ALLOCATE_DETAILS_LST svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("GetBookedSeatsByUSer:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.GetBookedSeatsByUser(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage SEAT_STATUS_CHECK([FromBody] SEAT_CHECK SC)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("Seat Check:" + JsonConvert.SerializeObject(SC));
        var obj = HDSvc.SEAT_STATUS_CHECK(SC);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetFloorList([FromBody] Maplistfloors main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("Space Details:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetFloorDetails(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object GetMarkers([FromBody] GetSpaceDetails svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("SpaceMapDetails:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.GetMarkers(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object GetSpaceDetailsByREQID([FromBody] GetSpaceDetails svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("GetSpaceRequesitionByREQID:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.GetSpaceDetailsByREQID(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object GetEmpAllocSeat([FromBody] GetSpaceDetails svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("GetEmpAllocSeat:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.GetEmpAllocSeat(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object SeatRestriction([FromBody] Space_Restrict_Seat svm)
    {
        return HDSvc.SeatRestriction(svm);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object SpaceRestriction([FromBody] Space_Restrict_Seat svm)
    {
        return HDSvc.SpaceRestriction(svm);
    }


    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public object SEAT_RESTRICTION_BOOKING(Space_Restrict_Seat_details data)
    {
        return HDSvc.SEAT_RESTRICTION_BOOKING(data);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetRoleAndReportingManger(Space_Restrict_Seat data)
    {
        object obj = HDSvc.GetRoleAndReportingManger(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetEmployees(Space_Restrict_Seat data)
    {
        object obj = HDSvc.GetEmployees(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

   // [GzipCompression]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public object GetSysPreferences(Space_Restrict_Seat data)
    {
        return HDSvc.GetSysPreferences(data);
    }

    //[GzipCompression]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public object GETEMPLOYEETYPE(Space_Restrict_Seat_details data)
    {
        return HDSvc.GETEMPLOYEETYPE(data);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object AllocateSeats([FromBody] HD_SPACE_ALLOCATE_DETAILS_LST svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("AllocateSeats:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.AllocateSeats(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object GET_VERTICAL_WISE_ALLOCATIONS([FromBody] GetSpaceDetails vertical)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("VerticalWiseAllocations:" + JsonConvert.SerializeObject(vertical));
        var obj = HDSvc.GET_VERTICAL_WISE_ALLOCATIONS(vertical);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public Object getZonalFmLocations(ZonalFmLocations zonalFmLocations)
    {
        return HDSvc.GetZonalFmLocations(zonalFmLocations);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public HttpResponseMessage InsertHRMSData([FromBody] List<APIIntegrationModel> hrms)
    {
        var re = Request;
        var headers = re.Headers;
        HDModel login = new HDModel();
        HttpResponseMessage response;
        if (headers.GetValues("APIKey").First() != "61872a55-21dc-4fdc-8219-39b9c0560bc8" || headers.GetValues("APIKey").First() == "")
        {
            response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid API Key");
            return response;
        }
        if (headers.GetValues("UserID").First() != "qfmsadmin" || headers.GetValues("UserID").First() == "")
        {
            response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid User Id");
            return response;
        }
        if (headers.GetValues("CompanyId").First() == "")
        {
            response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid User Id");
            return response;
        }
        if (headers.Contains("CompanyId") && headers.Contains("UserID") && headers.Contains("APIKey"))
        {
            login.CompanyId = headers.GetValues("CompanyId").First();
            login.UserId = headers.GetValues("UserID").First();
            login.APIKey = headers.GetValues("APIKey").First();
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("InsertHRMSData:" + JsonConvert.SerializeObject(hrms));
            var obj = APISvc.InsertHRMSDataService(hrms, login);
            response = Request.CreateResponse(HttpStatusCode.OK, obj);
            return response;
        }

        else
        {
            response = Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide all the headers");
            return response;

        }

    }
    SecureAPIKey Secure = new SecureAPIKey();
    //QRCodeScanner
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetAssetCodeDetails([FromBody] QrCodeVariable QrCode1)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("QRCodeData:" + JsonConvert.SerializeObject(QrCode1));
        var obj = HDSvc.GetAssetCodeDetails(QrCode1);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Space Details
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetSpaceDetails([FromBody] GetSpaceDetails main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("Space Details:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetEmployeeSpaceDetails(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage SubmitSpaceDetails([FromBody] SubmitSpaceDetails main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("Space Details:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.SubmitSpaceDetails(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #region Project Check List
    // Get Project Types
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetProjectType([FromBody] HDModel main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("ProjectCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetProjectType(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //BranchCheck List
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetProjectCheckList([FromBody] GetProjectList main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("ProjectCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetProjectCheckList(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // InsertProjectChecklist
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage InsertCheckList([FromBody] SaveProjectChecklistInsert Spci)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("ProjectCheckList:" + JsonConvert.SerializeObject(Spci));
        var obj = HDSvc.InsertCheckList(Spci);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //GetSavedList
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage getSavedList([FromBody] HDModel main)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("getSavedList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.getSavedList(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //GetDashboardList
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage getDashboardList([FromBody] HDModel main)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("getDashboardList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.getDashboardList(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // GetSavedDraftsData
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("Get", "Post")]
    [HttpPost]
    public HttpResponseMessage GetSavedDraftsdata(GetSavedDraftsList savedDraftsList)
    {
        var obj = HDSvc.GetSavedDraftsdata(savedDraftsList);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //PropertyProposal get Saved Draft
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("Get", "Post")]
    [HttpPost]
    public HttpResponseMessage GetPropertyProSavedDraft(ProSavedDraft savedDraftsList)
    {
        var obj = HDSvc.GetPropertyProposalSavedDraft(savedDraftsList);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //PropertyProposal get Saved Draft Details
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("Get", "Post")]
    [HttpPost]
    public HttpResponseMessage GetPPSavedDetails(ProposalDraftDetails savedDraftsList)
    {
        var obj = HDSvc.GetPPSavedDetails(savedDraftsList);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //PropertyProposal post Save as draft  data
    [HttpPost]
    public HttpResponseMessage InsertPPSaveAsDraftData([FromBody] ProposalDraftDetails Spci)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("ProjectCheckList:" + JsonConvert.SerializeObject(Spci));
        var obj = HDSvc.InsertPPSaveAsDraftData(Spci);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    // Add Property Proposal Submit
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage SubmitPropertyDetails(ProposalDraftDetails main)
    {
        var obj = HDSvc.SubmitPropertyDetails(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    // Get Bussiness Unit
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("Get", "Post")]
    [HttpPost]
    public HttpResponseMessage GetBussinessUnit(HDModel main)
    {
        var obj = HDSvc.GetBussinessUnit(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Multiple Image Save 
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage UploadPCLMultipleImages()
    {
        var httpRequest = HttpContext.Current.Request;
        ErrorHandler err = new ErrorHandler();
        var obj = HDSvc.UploadPCLMultipleImages(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    #endregion

    #region MobileApp Menu
    //MobileApp Menu
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetMainMenu([FromBody] HDModel main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("MainMenuItems:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetMainMenu(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Get Menu based on Role
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetMenuByRole([FromBody] MenuByRole main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("MenuByRole:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetMenuByRole(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Get Main Menu based on Role
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetMainMenuByRole([FromBody] MenuByRole main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("MenuByRole:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetMainMenuByRole(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #endregion

    #region BranchCheckList
    //Multiple Image Save 
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage UploadMultipleImages()
    {
        var httpRequest = HttpContext.Current.Request;
        ErrorHandler err = new ErrorHandler();
        var obj = HDSvc.UploadMultipleImages(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    //BranchCheck List
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetBranchCheckList([FromBody] HDModel main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("BranchCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetBranchCheckList(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //BranchCheck List Inspectors
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetInspectors([FromBody] Inspectors main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("BranchCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetInspectors(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //BranchCheck List Locations
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetLocations([FromBody] HDModel main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("BranchCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetLocations(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //BranchCheck List Insert 
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage BCLInsert([FromBody] BranchCheckListInsert main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("BranchCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.BCLInsert(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Upload Files Dinamically Updated By --Raghav 20-02-2021
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage InsertBCLUploadImages()
    {

        var httpRequest = HttpContext.Current.Request;
        List<string> res = HDSvc.UploadFiles(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    //Saved Drafts List  ---BranchCheckList Creation
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage BCLGetSavedDrafts([FromBody] HDModel main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("BranchCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.BCLGetSavedDrafts(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Saved Drafts List Location-wise  ---BranchCheckList Saved Drafts
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage BCLGetSavedDraftsBasedOnLoc([FromBody] BranchCheckListInsert main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("BranchCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.BCLGetSavedDraftsBasedOnLoc(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #endregion


    #region MileStone #02
    //Sign In
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]

    public HttpResponseMessage SignIn(HDModel Login)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("SignIn:" + JsonConvert.SerializeObject(Login));
        var obj = HDSvc._LoginValidate(Login);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [AllowAnonymous]
    [HttpPost]
    public HttpResponseMessage SignOut(HDModel ADFSLogin)
    {

        ErrorHandler err = new ErrorHandler();
        var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();
        err._WriteErrorLog_string("SignInWithADFS:" + JsonConvert.SerializeObject(allUrlKeyValues));
        var obj = HDSvc._SignOut(ADFSLogin);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SignInWithADFS(HDModel ADFSLogin)
    {

        ErrorHandler err = new ErrorHandler();
        var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();
        err._WriteErrorLog_string("SignInWithADFS:" + JsonConvert.SerializeObject(allUrlKeyValues));
        var obj = HDSvc._LoginValidateWithADFS(ADFSLogin);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //[HttpPost]
    //public HttpResponseMessage SignInWithGmail()
    //{
    //    var obj = HDSvc.SignInWithGmail();
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
    //Forgot Password
    [HttpPost]
    public HttpResponseMessage ForgotPassword(HDModel Login)
    {
        var obj = HDSvc._PasswordForget(Login);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //User Profile
    [HttpPost]
    public HttpResponseMessage GetProfile(HDModel Login)
    {
        var obj = HDSvc._GetDashboardProfile(Login);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetCompanyLogo(HDModel Login)
    {
        var obj = HDSvc._GetCompanyLogo(Login);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Update Profile
    [HttpPost]
    public HttpResponseMessage UpdateProfile(Profile Prof)
    {
        var obj = HDSvc._UpdateProfile(Prof);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Change Password
    [HttpPost]
    public HttpResponseMessage ChangePassword(ChangePwd ChangePwd)
    {
        var obj = HDSvc._ChangePasswod(ChangePwd);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Dashboard Count
    [HttpPost]
    public HttpResponseMessage HDDashboard(HDModel HD)
    {
        var obj = HDSvc._HDCount(HD);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Dropdown Binding Methods

    //Department List
    [HttpPost]
    public HttpResponseMessage DepartmentList(Key APIKey)
    {
        var obj = HDSvc._GetDepartments(APIKey);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Country List
    [HttpPost]
    public HttpResponseMessage CountryList(Key APIKey)
    {
        var obj = HDSvc._GetCountries(APIKey);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Reporting Manager List
    [HttpPost]
    public HttpResponseMessage ReportingManagerList(HDModel name)
    {
        var obj = HDSvc._ReportingManagers(name);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    #endregion

    #region MileStone #03

    //Service Type
    [HttpPost]
    public HttpResponseMessage GetServiceType(RaiseRequest RRST)
    {
        var obj = HDSvc._ServiceType(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Location Type
    [HttpPost]
    public HttpResponseMessage GetLocationType(RaiseRequest RRST)
    {
        var obj = HDSvc._LocationType(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    // Request Type Property Proposal
    [HttpPost]
    public HttpResponseMessage GetRequestType(RaiseRequest RRST)
    {
        var obj = HDSvc._RequestType(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetLocationType_cor(RaiseRequest RRST)
    {
        var obj = HDSvc._LocationType_cor(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // Aquisition Through Property Proposal
    [HttpPost]
    public HttpResponseMessage GetAquisitionThrs(RaiseRequest RRST)
    {
        var obj = HDSvc._AquisitionThr(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // Entity Type Property Proposal
    [HttpPost]
    public HttpResponseMessage GetEntityType(RaiseRequest RRST)
    {
        var obj = HDSvc._EntityType(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // Floor Property Proposal
    [HttpPost]
    public HttpResponseMessage GeyFloor(HDModel hDModel)
    {
        var obj = HDSvc._FloorDetails(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // Property Type Property Proposal
    [HttpPost]
    public HttpResponseMessage GetPropertyType(RaiseRequest RRST)
    {
        var obj = HDSvc._PropertyType(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    // Flooring Type
    [HttpPost]
    public HttpResponseMessage GetFooringType(RaiseRequest RRST)
    {
        var obj = HDSvc._FlooringType(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // Insurence Type
    [HttpPost]
    public HttpResponseMessage GetInsuranceType(RaiseRequest RRST)
    {
        var obj = HDSvc._InsuranceType(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //View SLA Details
    [HttpPost]
    public HttpResponseMessage ViewSLA(RaiseRequest RRST)
    {
        var obj = HDSvc._ViewSLADetails(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Get SLA Details
    [HttpPost]
    public HttpResponseMessage GetSLA(GETSLA RRST)
    {
        var obj = HDSvc.GetSLA(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public Object Getserviceinchege(GETINCH RRST)
    {
        var obj = HDSvc.Getserviceinchege(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage SlaNotDefine(SLAND RRST)
    {
        var obj = HDSvc.SlaNotDefine(RRST);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Submit Raise Request
    [HttpPost]
    //public async Task<HttpResponseMessage> SubmitRaiseRequest()
    public HttpResponseMessage SubmitRaiseRequest()
    {
        var httpRequest = HttpContext.Current.Request;
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("SubmitRaiseRequest:" + JsonConvert.SerializeObject(httpRequest.Params));
        var obj = HDSvc._RaiseHDRequest(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        string Key = httpRequest.Params["APIKey"];
        int cntobj = Convert.ToInt32(HDSvc.GetTotalHDReqCount(Key));
        cntobj--;
        string HelDeskReq = "";
        HelDeskReq = (DateTime.Now.ToString("ddMMyyyy") + '/' + cntobj).ToString();
        SendPushNotification(HelDeskReq, 1, Key);
        return response;
    }
    //View Raise Requests
    [HttpPost]
    public HttpResponseMessage GetViewRequestList(Pagination K)
    {
        var obj = HDSvc._ViewRequests(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetViewUpdateRequestList(Pagination K)
    {
        var obj = HDSvc._ViewUpdateRequestList(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Filter Grid
    [HttpPost]
    public HttpResponseMessage SearchRequest(FilterParam FilterParam)
    {
        var obj = HDSvc._SearchRequests(FilterParam.APIKey, FilterParam.Filter);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Dropdown List Impact, Urgency, Repeat , Feedback,Status
    [HttpPost]
    public HttpResponseMessage ConsolidatedDropdown(Key K)
    {
        var obj = HDSvc._ConsolidatedDDL(K.APIKey);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Status Grid
    [HttpPost]
    public HttpResponseMessage StatusList(Key K)
    {
        var obj = HDSvc._Status(K.APIKey, 1);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Cancel Request
    [HttpPost]
    public HttpResponseMessage CancelViewRequest(HDParams Param)
    {
        var obj = HDSvc._CancelRequest(Param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Modify Request
    [HttpPost]
    public HttpResponseMessage ModifyViewRequest(ModifyViewReq MReq)
    {
        var obj = HDSvc._ModifyRequest(MReq);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        SendPushNotification(MReq.RequestId, 2, MReq.APIKey);
        return response;
    }

    //Feedback Request
    [HttpPost]
    public HttpResponseMessage SubmitFeedback(HDParams Params)
    {
        var obj = HDSvc._SubmitFeedback(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Assign Employee 
    [HttpPost]
    public HttpResponseMessage GetNameForAssign(RaiseRequest Params)
    {
        var obj = HDSvc._ReAssign(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Update Picture
    [HttpPost]
    public HttpResponseMessage UpdatePicture()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = HDSvc._UpdatePicture(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Update Asset Image 
    [HttpPost]
    public HttpResponseMessage UpdateAssetImage()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = HDSvc._UpdateAssetImage(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Get Locations for  Incentive
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetLocationsScore(HDModel Data)
    {
        var obj = HDSvc.GetLocationsScore(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    //Get employee details based on login for Incentive
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public Object GetDetails(HDModel hDModel)
    {
        return HDSvc.GetDetails(hDModel);
    }
    //getgrid data for Incentive
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetGriddataIncentive(NewCheckListV2 newCheckListV2)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = HDSvc.GetGriddataIncentive(newCheckListV2);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Save for Incentive
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage SaveIncentive(selecteddata1 dataobj)
    {
        var obj = HDSvc.SaveIncentive(dataobj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Remove Picture
    [HttpPost]
    public HttpResponseMessage RemovePicture(Image Img)
    {
        var obj = HDSvc._RemovePicture(Img);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #endregion

    #region MileStone #04
    //Get Update Request Details
    [HttpPost]
    public HttpResponseMessage GetUpdateRequestList(Pagination K)
    {
        var obj = HDSvc._UpdateViewRequests(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Search Update Details
    [HttpPost]
    public HttpResponseMessage SearchUpdateRequest(FilterParam FilterParam)
    {
        var obj = HDSvc._SearchUpdateRequests(FilterParam.APIKey, FilterParam.Filter);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Get Status List
    [HttpPost]
    public HttpResponseMessage UpdateStatusList(Key K)
    {
        var obj = HDSvc._Status(K.APIKey, 2);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Ticket Complete Details
    [HttpPost]
    public HttpResponseMessage ViewHelpDeskDetails(HDParams Params)
    {
        var obj = HDSvc._ViewDetails(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Multiple Request Approval
    [HttpPost]
    public HttpResponseMessage UpdateMultipleRequest(ListHDParams Param)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("UpdateMultipleRequest:" + JsonConvert.SerializeObject(Param.HdParmsList));
        var obj = HDSvc._UpdateMultipleRequest(Param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        foreach (var values in Param.HdParmsList)
        {
            SendPushNotification(values.RequestID, 3, Param.APIKey);
        }
        return response;
    }
    //Single Request Approval
    [HttpPost]
    public HttpResponseMessage UpdateRequest()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = HDSvc._UpadteSingleRequest(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        string Key = httpRequest.Params["APIKey"];
        String RID = httpRequest.Params["RequestId"];
        SendPushNotification(RID, 3, Key);
        return response;
    }
    //Get ReAssing Details
    [HttpPost]
    public HttpResponseMessage GetReAssignList(Pagination K)
    {
        var obj = HDSvc._ReAssignViewRequests(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Search ReAssign
    [HttpPost]
    public HttpResponseMessage SearchReAssign(FilterParam FilterParam)
    {
        var obj = HDSvc._SearchReAssignRequests(FilterParam.APIKey, FilterParam.Filter);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Multiple Approval
    [HttpPost]
    public HttpResponseMessage SubmitMultipleReAssign(ListHDParams Param)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("SubmitMultipleReAssign:" + JsonConvert.SerializeObject(Param.HdParmsList));

        var obj = HDSvc._ReAssignMultipleRequest(Param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        foreach (var values in Param.HdParmsList)
        {
            SendPushNotification(values.RequestID, 4, Param.APIKey);
        }
        return response;
    }
    //Single Approval
    [HttpPost]
    public HttpResponseMessage SubmitReAssign(ListHDParams Param)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("SubmitReAssign:" + JsonConvert.SerializeObject(Param.HdParmsList));

        var obj = HDSvc._ReAssignMultipleRequest(Param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        foreach (var values in Param.HdParmsList)
        {
            SendPushNotification(values.RequestID, 4, Param.APIKey);
        }
        return response;
    }

    //View Reopen Tickets
    [HttpPost]
    public HttpResponseMessage GetReOpenList(Pagination K)
    {
        var obj = HDSvc._ReOpenViewRequests(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Search Reopen
    [HttpPost]
    public HttpResponseMessage SearchReOpen(FilterParam FilterParam)
    {
        var obj = HDSvc._SearchReOpenRequests(FilterParam.APIKey, FilterParam.Filter);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Reopen Ticket
    [HttpPost]
    public HttpResponseMessage SubmitReOpenRequest(HDParams Param)
    {
        var obj = HDSvc._ReOpenRequest(Param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        SendPushNotification(Param.RequestID, 5, Param.APIKey);
        return response;
    }
    #endregion

    #region MileStone #05
    //Get Report
    [HttpPost]
    public HttpResponseMessage GetMyReportList(ReportParams Params)
    {
        var obj = HDSvc._GetReport(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Get Dashbaord Count Details
    [HttpPost]
    public HttpResponseMessage GetConsolidatedTickets(DashboardDetails K)
    {
        var obj = HDSvc._GetTotalDashboardDetails(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Search Report
    [HttpPost]
    public HttpResponseMessage SearchReport(ReportFilterParams FilterParam)
    {
        var obj = HDSvc._SearchReport(FilterParam);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    // SLA Violation
    [HttpPost]
    public HttpResponseMessage SLAViolation(Key K)
    {
        var obj = HDSvc._SLAViolation(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #endregion

    #region Miscelleneous

    #region Device Token Update
    [HttpPost]
    public HttpResponseMessage UpdateDeviceToken(DeviceToken Tkn)
    {
        var obj = HDSvc._UpdateDeviceToken(Tkn);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #endregion

    #region Emergency Request
    [HttpPost]
    public HttpResponseMessage EmergencyRequestList(Key K)
    {
        var obj = HDSvc._Emergency(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    #endregion

    #region Notifications
    [HttpPost]
    public HttpResponseMessage NotificationHistory(Key K)
    {
        var obj = HDSvc._NotificationList(K.APIKey);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage NotificationUpdate(StatusUpdate K)
    {
        var obj = HDSvc._UpdateNotificationStatus(K);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    public static void SendPushNotification(string RequestId, int Count, string APIKey)
    {
        SecureAPIKey Secure = new SecureAPIKey();
        ValidateKey VALDB = Secure._ValidateAPIKey(APIKey);

        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_NOTIFICATIONS");
                sp2.Command.AddParameter("@RID", RequestId, DbType.String);
                sp2.Command.AddParameter("@CNT", Count, DbType.Int32);
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@CMP", VALDB.COMPANYID, DbType.String);
                DataSet ds1 = sp2.GetDataSet();
                foreach (DataTable table in ds1.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        var TITLE = dr["TITLE"].ToString();
                        var TYPE = dr["TYPE"].ToString();
                        var BODY = dr["BODY"].ToString();
                        var SER_REQ_ID = dr["SER_REQ_ID"].ToString();
                        var SER_PROB_DESC = dr["SER_PROB_DESC"].ToString();
                        var STA_DESC = dr["STA_DESC"].ToString();
                        var TOKEN = dr["DEVICE_TOKEN"].ToString();
                        string applicationID = "AAAAXme4gGQ:APA91bHwTrlJVanL1em_LSQOcxzC5lCnMHtH8KZetfaUA8XLWHzSz6-lHBmfSQX83Fn7wEsVLWRc35HWChvG36hucIoRbYwvH5XDgD4yd4i1nMPdVQuPp_ZnZGOMKcF3WTwkf2ZUEiAG";
                        string deviceId = TOKEN;

                        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                        tRequest.Method = "post";
                        tRequest.ContentType = "application/json";
                        //var data = new
                        //{
                        //    to = deviceId,
                        //    notification = new
                        //    {
                        //        body = BODY,
                        //        title = TITLE,
                        //        type = TYPE,
                        //        REQUEST_ID = SER_REQ_ID,
                        //        REQ_STATUS_NAME = STA_DESC,
                        //        PROBLEM_DESC = SER_PROB_DESC

                        //    }
                        //};
                        var jsondata = new
                        {
                            data = new
                            {
                                aps = new
                                {
                                    alert = new
                                    {
                                        title = TITLE,
                                        type = TYPE,
                                        body = BODY,
                                        REQUEST_ID = SER_REQ_ID,
                                        REQ_STATUS_NAME = STA_DESC,
                                        PROBLEM_DESC = SER_PROB_DESC
                                    }
                                }
                            },
                            to = deviceId
                        };
                        var serializer = new JavaScriptSerializer();
                        var json = serializer.Serialize(jsondata);
                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                        tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                        tRequest.ContentLength = byteArray.Length;
                        using (Stream dataStream = tRequest.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            using (WebResponse tResponse = tRequest.GetResponse())
                            {
                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                {
                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                    {
                                        String sResponseFromServer = tReader.ReadToEnd();
                                        string str = sResponseFromServer;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex);
            };
        }
    }

    #endregion
    #endregion

    #region Add Property

    //Bind General Details
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetGeneralDetails(HDModel main)
    {
        var obj = HDSvc.GetGeneralDetails(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    #endregion


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage InsertDailyCheckList(SelectedDataModel seledata)
    {
        var obj = HDSvc.InsertDailyCheckList(seledata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetNewSecurityCheckListID(HDModel hDModel)
    {
        var obj = HDSvc._GetNewSecurityCheckListID(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public Object getCheckListDetails(HDModel hDModel)
    {
        return HDSvc.GetCheckListDetails(hDModel);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object GetRequestId(HDModel hDModel)
    {
        return HDSvc.getRequestID(hDModel);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GET_ZONAL_ACTIVE_IDS(HDModel hDModel)
    {
        var obj = HDSvc.GET_ZONAL_ACTIVE_IDS(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Get Zonal FM View Page Data
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public Object getZonalFMDetails(HDModel hDModel)
    {
        return HDSvc.GetZonaLFmDetails(hDModel);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Insert_ZF_CheckList(SelectedDataModel sdm)
    {
        var obj = HDSvc.Insert_ZF_CheckList(sdm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }



    #region Central Team    

    // Get Central Team IDs to filter 
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage getCentralTeamActiveIds(HDModel hDModel)
    {
        var obj = HDSvc.GetCentralTeamIds(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Get Central Team View Page Data
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public Object getCentralTeamDetails(HDModel hDModel)
    {
        return HDSvc.GetCentralTeamDetails(hDModel);
    }

    //Get Central Team Locations 
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public Object getCentralTeamLocations(ZonalFmLocations zonalFmLocations)
    {
        return HDSvc.GetCentralTeamLocations(zonalFmLocations);
    }

    #endregion


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object uploadFilesImages()
    {
        var httpRequest = HttpContext.Current.Request;
        return HDSvc.UploaImages(httpRequest);
    }

    //GENERATE_REQUESTID for Property Proposel
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object GenerateRequestId(HDModel hDModel)
    {
        return HDSvc.GenerateRequestId(hDModel);
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object GetLocationsOnCity(HDModel hDModel)
    {
        var response = HDSvc.GetLocationBasedOnCityCode(hDModel);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object GetCityID(HDModel hDModel)
    {
        var response = HDSvc.GetCityID(hDModel);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object GetTowerID(HDModel hDModel)
    {
        var response = HDSvc.GetTowerID(hDModel);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object GetEntities(HDModel hDModel)
    {
        var response = HDSvc.GetEntities(hDModel);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public object GetDB(HDModel hDModel)
    {
        var response = HDSvc._ReturnDB(hDModel);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage InsertCentralTeam(SelectedDataModel sdm)
    {
        var obj = HDSvc.InsertCentralTeam(sdm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetInspectionForm(HDModel hDModel)
    {
        var obj = HDSvc.GetSecurityGuardForm(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetDashboard(HDModel hDModel)
    {
        var obj = HDSvc.GetDashBoard(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //Zonal FM and Central Team View Details.
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetViewDetails(HDModel hDModel)
    {
        var obj = HDSvc.GetViewDetails(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetViewIds(HDModel hDModel)
    {
        var obj = HDSvc.GetViewIds(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetInspectionLocations(HDModel hDModel)
    {
        var obj = HDSvc.GetInspectionLocations(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //SearchSpaces

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage SearchSpaces(ScheduleMySeatBookingsModelMobile Data)
    {
        var obj = HDSvc.SearchSpaces(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //AllocateSeats

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Allocate_Space_Seats(SPACE_ALLOC_DETAILS_TIME_Mobile Data)
    {
        var obj = HDSvc.Allocate_Space_Seats(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }


    //getAurNames
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage getAurNames(HDModel Data)
    {
        var obj = HDSvc.getAurNames(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //GetCities

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetCities(HDModel Data)
    {
        var obj = HDSvc.GetCities(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //GetLocationsData
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetLocationsData(HDModel Data)
    {
        var obj = HDSvc.GetLocationsData(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //GetTowersData
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetTowersData(HDModel Data)
    {
        var obj = HDSvc.GetTowersData(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    //GetTowerByLocation
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetTowerByLocation(LocationData Data)
    {
        var obj = HDSvc.GetTowerByLocation(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    //GetFloorByTower Towerlsts
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetFloorByTower(TowerData Data)
    {
        var obj = HDSvc.GetFloorByTower(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //GetCountries
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetCountries(HDModel Data)
    {
        var obj = HDSvc.GetCountries(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //GetInspectionView

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetInspectionView(HDModel Data)
    {
        var obj = HDSvc.GetInspectionView(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetHelpDesk(HDModel Data)
    {
        var obj = HDSvc.GetHelpDesk(Data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }



    //Upload Profile Written By --Raghav 15-05-2021
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage UploadProfilePic()
    {

        var httpRequest = HttpContext.Current.Request;
        List<string> res = HDSvc.UploadProfile(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET")]
    [HttpGet]
    public HttpResponseMessage DeleteFileFromPath()
    {

        var httpRequest = HttpContext.Current.Request;
        string res = HDSvc.DeleteFileFromPath(httpRequest.Params[0]);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage UpdateProfileInfo(HDModel hDModel)
    {
        object res = HDSvc.UpdateProfile(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetProfileInfo(HDModel hDModel)
    {

        var httpRequest = HttpContext.Current.Request;
        object res = HDSvc.GetProfile(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    //GetMaintainanceMenu
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetMaintainanceMenu(HDModel hDModel)
    {
        object res = HDSvc.GetMaintainanceMenu(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetMaintainanceMenuTemp(HDModel hDModel)
    {
        object res = HDSvc.GetMaintainanceMenuTemp(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetAssertVendor(HDModel hDModel)
    {
        object res = HDSvc.GetAssertVendor(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }
    //Quick select Icons Reddy 
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage QUICKSELECTICONSMOBILE(HDModel hDModel)
    {
        object res = HDSvc.QUICKSELECTICONSMOBILE(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage QUICKICONSSUBMODMOBILE(MaintenanceMenu MM)
    {
        object res = HDSvc.QUICKICONSSUBMODMOBILE(MM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetAssetBrand(HDModel hDModel)
    {
        object res = HDSvc.GetAssetBrand(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetAssertSubcategoryByAssertCategory(HDModel hDModel)
    {
        object res = HDSvc.GetAssertSubcategoryByAssertCategory(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetAssetOnLocation(HDModel hDModel)
    {
        object res = HDSvc.GetAssetOnLocation(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetUserLocation(HDModel hDModel)
    {
        object res = HDSvc.GetUserLocation(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetTicketIds(AssertTicketRequestModel assertTicketRequest)
    {
        object res = HDSvc.GetTicketIds(assertTicketRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET")]
    [HttpGet]
    public HttpResponseMessage GetTicketType()
    {

        var httpRequest = HttpContext.Current.Request;
        //string res = HDSvc.DeleteFileFromPath(httpRequest.Params[0]);
        object res = HDSvc.GetTicketType(httpRequest.Params[0]);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage InsertBreakDownTicket(GenerateBreakDownTicket GenerateBreakDownTicket)
    {
        object res = HDSvc.InsertBreakDownTicket(GenerateBreakDownTicket);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetHDIncharge(GenerateBreakDownTicket GenerateBreakDownTicket)
    {
        object res = HDSvc.GetHDIncharge(GenerateBreakDownTicket);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage BreakDownStatus(BreakDownStatusModel breakdownStatusModel)
    {

        object res = HDSvc.BreakDownStatus(breakdownStatusModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetBreakDownErrorDescription(HDModel hDModel)
    {

        object res = HDSvc.GetBreakDownErrorDescription(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }
    //GetSpareParts
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetSpareParts(HDModel hDModel)
    {
        object res = HDSvc.GetSpareParts(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    //GetBreakDownData
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetBreakDownData(HDModel hDModel)
    {
        object res = HDSvc.GetBreakDownData(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetAsset(HDModel hDModel)
    {
        object res = HDSvc.GetAsset(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    //UpdatebreakDown

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage UpdatebreakDown(MaintanceTicketModel maintanceTicketModel)
    {
        object res = HDSvc.UpdatebreakDown(maintanceTicketModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }



    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Get_Maintainance_Id_Status(GetMaintainanceidStatusModel getmaintainanceidstatusModel)
    {
        object res = HDSvc.Get_Maintainance_Id_Status(getmaintainanceidstatusModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Get_Checklists_Data_On_Asset_Id(GetChecklistsDataOnAssetIdModel getChecklistsDataOnAssetIdModel)
    {
        object res = HDSvc.Get_Checklists_Data_On_Asset_Id(getChecklistsDataOnAssetIdModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET")]
    [HttpGet]
    public HttpResponseMessage GetUrgency()
    {

        var httpRequest = HttpContext.Current.Request;
        //string res = HDSvc.DeleteFileFromPath(httpRequest.Params[0]);
        object res = HDSvc.GetUrgency(httpRequest.Params[0]);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Get_User_Status_By_Role(HDModel hDModel)
    {
        object res = HDSvc.Get_User_Status_By_Role(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    //Get_User_Status_By_ID

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Get_User_Status_By_ID(HDModel hDModel)
    {
        object res = HDSvc.Get_User_Status_By_ID(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Get_Break_Down_Tickets_To_ValiDate(HDModel hDModel)
    {
        object res = HDSvc.Get_Break_Down_Tickets_To_ValiDate(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET")]
    [HttpGet]
    public HttpResponseMessage GetOwnerList()
    {

        var httpRequest = HttpContext.Current.Request;
        //string res = HDSvc.DeleteFileFromPath(httpRequest.Params[0]);
        object res = HDSvc.GetOwnerList(httpRequest.Params[0]);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Get_BreakDownTickets_By_Search(GetSearchTicketModel getsearchticketmodel)
    {
        object res = HDSvc.Get_BreakDownTickets_By_Search(getsearchticketmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Attach_Tickets_List(HDModel hDModel)
    {
        object res = HDSvc.Attach_Tickets_List(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetCurrentInventory(HDModel hDModel)
    {
        object res = HDSvc.GetCurrentInventory(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage InsetSpareParts(InsertAddSpareModel insertAddSpareModel)
    {
        object res = HDSvc.InsetSpareParts(insertAddSpareModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetValidatespare(HDModel hDModel)
    {
        object res = HDSvc.GetValidatespare(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage ValidateSpare(ValidateSpareRequestModel validateSpareRequestModel)
    {
        object res = HDSvc.ValidateSpare(validateSpareRequestModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetSparePart_Details(HDModel hDModel)
    {
        object res = HDSvc.GetSparePart_Details(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }



    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage BusinessCardInsertDetails(BusinessCardInsertModel businessCardInsertModel)
    {
        object res = HDSvc.BusinessCardInsertDetails(businessCardInsertModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetBusinessCardDetails(HDModel hDModel)
    {
        object res = HDSvc.GetBusinessCardDetails(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage ModifyBusinessCard(BusinessCardInsertModel businessCardInsertModel)
    {
        object res = HDSvc.ModifyBusinessCard(businessCardInsertModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetBusinessCardStatus(HDModel hDModel)
    {
        object res = HDSvc.GetBusinessCardStatus(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage RequisitionApproval(BusinessCardInsertModel businessCardInsertModel)
    {
        object res = HDSvc.RequisitionApproval(businessCardInsertModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage InsertPPMChecklistData(InsertPPMChecklistDataModel insertPPMChecklistDataModel)
    {
        object res = HDSvc.InsertPPMChecklistData(insertPPMChecklistDataModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetAllVendors(HDModel hDModel)
    {
        object res = HDSvc.GetAllVendors(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetEmployeesData(HDModel hDModel)
    {
        object res = HDSvc.GetEmployeesData(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetAllData(HDModel hDModel)
    {
        object res = HDSvc.GetAllData(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetMainSubChidCategory(ChildCategory childCategory)
    {
        object res = HDSvc.GetMainSubChidCategory(childCategory);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }
    //GetValidatePPM
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetValidatePPM(HDModel hDModel)
    {
        object res = HDSvc.GetValidatePPM(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }



    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage BusinessCardUserAcknowledge(Acknowledge acknowledge)
    {
        object res = HDSvc.BusinessCardUserAcknowledge(acknowledge);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage BusinessCardRejection(Acknowledge acknowledge)
    {
        object res = HDSvc.BusinessCardRejection(acknowledge);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage BusinessCardDetails(Acknowledge acknowledge)
    {
        object res = HDSvc.BusinessCardDetails(acknowledge);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetBusinessCardApprove(HDModel hDModel)
    {
        object res = HDSvc.GetBusinessCardApprove(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    //Spare Dashboard for Ekart
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetSpareDashboard(SpareDashboardModel spareDashboardModel)
    {
        object res = HDSvc.GetSpareDashboard(spareDashboardModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage StatusWiseZone(SpareDashboardModel spareDashboardModel)
    {
        object res = HDSvc.StatusWiseZone(spareDashboardModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage Getcovidterm(HDModel hDModel)
    {
        object res = HDSvc.Getcovidterm(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetRCAdata(HDModel hDModel)
    {
        object res = HDSvc.GetRCAdata(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetCovidNorms(CovidModel covidModel)
    {
        object res = HDSvc.GetCovidNorms(covidModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage InsertRCAquestionary(RCAquestionary rcaquestionary)
    {
        object res = HDSvc.InsertRCAquestionary(rcaquestionary);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetRCAQuestionaryData(RCAquestionary rcaquestionary)
    {
        object res = HDSvc.GetRCAQuestionaryData(rcaquestionary);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage UpdateRCAquestionary(RCAquestionary rcaquestionary)
    {
        object res = HDSvc.UpdateRCAquestionary(rcaquestionary);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }



    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetWFHterm(HDModel hDModel)
    {
        object res = HDSvc.GetWFHterm(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetWFHNorms(CovidModel covidModel)
    {
        object res = HDSvc.GetWFHNorms(covidModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetRCATicketTiming(TicketModel ticketModel)
    {
        object res = HDSvc.GetRCATicketTiming(ticketModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage TempVisitorDetails(VisitorDetails visitorDetails)
    {
        object res = HDSvc.TempVisitorDetails(visitorDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetTempVisitorDetailsMobile(VisitorDetails visitorDetails)
    {
        object res = HDSvc.GetTempVisitorDetailsMobile(visitorDetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetNotification(PushNotification pushNotification)
    {
        object res = HDSvc.GetNotification(pushNotification);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage UpdateNotification(PushNotification pushNotification)
    {
        object res = HDSvc.UpdateNotification(pushNotification);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage NotificationDetails(PushNotification pushNotification)
    {
        object res = HDSvc.NotificationDetails(pushNotification);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    //notificationService

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetNotificationDeviceIDs(HDModel hDModel)
    {
        object res = notificationService.getDeviceIds(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    //EkartImage Upload

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage InsertSpareIcon()
    {

        var httpRequest = HttpContext.Current.Request;
        List<string> res = HDSvc.InsertSpareIcon(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }



    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage UpdateSpareImage(SpareImagePath spareImagePath)
    {
        object res = HDSvc.UpdateSpareImage(spareImagePath);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetLowStock()
    {

        var httpRequest = HttpContext.Current.Request;
        object res = HDSvc.GetLowStock(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object AllocateSeatsOpt([FromBody] SPACE_ALLOCATE_DETAILS_LST_HDO svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("AllocateSeats:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.AllocateSeatsOpt(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }



    //HONDA


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object ConferenceRoomTimeSlots(GetSpaceDetails getspacedetails)
    {
        object res = HDSvc.ConferenceRoomTimeSlots(getspacedetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetLocationData(HDModel hdmodel)
    {
        object res = HDSvc.GetFloorList(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object Getfacility(AssertTicketRequestModel assertTicketRequestModel)
    {
        object res = HDSvc.Getfacility(assertTicketRequestModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object Getuserdetails(HDModel hdmodel)
    {
        object res = HDSvc.Getuserdetails(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetUpdateModifyWithhold(BlockSeatRequest blockSeatRequest)
    {
        object res = HDSvc.GetUpdateModifyWithhold(blockSeatRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    //18Oct2021
    public Object GetReleaseRequest(BlockSeatRequest blockSeatRequest)
    {
        object res = HDSvc.GetReleaseRequest(blockSeatRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetEditModify(BlockSeatRequest blockSeatRequest)
    {
        object res = HDSvc.GetEditModify(blockSeatRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public object GetReservationRoomlist(GetSpaceDetails getspacedetails)
    {
        object res = HDSvc.GetReservationRoomlist(getspacedetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public object GetCapacitylist(GetSpaceDetails getspacedetails)
    {
        object res = HDSvc.GetCapacitylist(getspacedetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    //Update con room booking
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage UpdateSeatRequest(BlockSeatRequest blockSeatRequest)
    {
        object res = HDSvc.UpdateSeatRequest(blockSeatRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetModifyBookedDetails(ConferencedBooked conferencedBooked)
    {
        object res = HDSvc.GetModifyBookedDetails(conferencedBooked);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    //[AllowAnonymous]
    //[System.Web.Http.AcceptVerbs("POST")]
    //[HttpPost]
    //public HttpResponseMessage UpdateSeatRequest(BlockSeatRequest blockSeatRequest)
    //{
    //    object res = HDSvc.UpdateSeatRequest(blockSeatRequest);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
    //    return response;

    //}



    //CheckUserInspection

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    [HttpHead]
    public Object CheckUserInspection(HDModel hdmodel)
    {
        object res = HDSvc.CheckUserInspection(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetConfBookingDetails(GetSpaceDetails getspacedetails)
    {
        object res = HDSvc.GetConfBookingDetails(getspacedetails);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object Getuseremaildetails(HDModel hdmodel)
    {
        object res = HDSvc.Getuseremaildetails(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetConfSubmit(ConferencedBooked conferencedBooked)
    {
        object res = HDSvc.ConfSubmit(conferencedBooked);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetSeatRequest(BlockSeatRequest blockSeatRequest)
    {
        object res = HDSvc.GetSeatRequest(blockSeatRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetEmpDetails(HDModel hdmodel)
    {
        object res = HDSvc.GetEmpDetails(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetBookedDetails(ConferencedBooked conferencedBooked)
    {
        object res = HDSvc.GetBookedDetails(conferencedBooked);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public object GetupdateNotfi(HDModel hdmodel)
    {
         HDSvc.GetupdateNotfi(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetCancelRequest(BlockSeatRequest blockSeatRequest)
    {
        object res = HDSvc.GetCancelRequest(blockSeatRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET")]
    [HttpGet]
    [HttpHead]

    public object Support()
    {

        var httpRequest = HttpContext.Current.Request;
        HDModel hDModel = new HDModel
        {
            CompanyId = httpRequest.Params[0]
        };
        object res = HDSvc.Support(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }



    //LTTS

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object ApprovalList(HDModel hdmodel)
    {
        object res = HDSvc.ApprovalList(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object ApprovalBooking(HDModel hdmodel)
    {
        object res = HDSvc.ApprovalBooking(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }




    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object AllocateSpaceValidation([FromBody] HD_SPACE_ALLOCATE_DETAILS_LST svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("AllocateSeats:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.AllocateSpaceValidation(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public Object AllocateSpaceManagerValidation([FromBody] HD_SPACE_ALLOCATE_DETAILS_LST svm)
    {
        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("AllocateSeats:" + JsonConvert.SerializeObject(svm));
        var obj = HDSvc.AllocateSpaceManagerValidation(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //return mspsrvc.GetMarkers(svm);
    }



    //LTTSHotDeskingATKINS
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetEmployeeData(HDModel hdmodel)
    {
        object res = HDSvc.GetEmployeeData(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetEmployeeStatus(HDModel hdmodel)
    {
        object res = HDSvc.GetEmployeeStatus(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetEmployeeCostCenterDetails(HDModel hdmodel)
    {
        object res = HDSvc.GetEmployeeCostCenterDetails(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetRequestDetails(GetSpaceDetails HDCNT)
    {
        object res = HDSvc.GetRequestDetails(HDCNT);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetRequestDetailsSatusWise(GetSpaceDetails HDCNT)
    {
        object res = HDSvc.GetRequestDetailsSatusWise(HDCNT);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;


    }
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object GetEmployeeHotDesking(HDModel hdmodel)
    {
        object res = HDSvc.GetEmployeeHotDesking(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public object RaiseRequst(HotDeskRequisitionMobile hotDeskRequisitionMobile)
    {
        object res = HDSvc.RaiseRequst(hotDeskRequisitionMobile);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public object RequstApprove(HotDeskApproveMobile hotDeskApproveMobile)
    {
        object res = HDSvc.RequstApprove(hotDeskApproveMobile);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }



    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]
    public object FinalApprovel(HotDeskRequisitionMobile hotDeskRequisitionMobile)
    {
        object res = HDSvc.FinalApprovel(hotDeskRequisitionMobile);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }



    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpGet]
    [HttpHead]

    public Object GetAppVersion()
    {
        var httpRequest = HttpContext.Current.Request;
        ErrorHandler err = new ErrorHandler();
        var obj = HDSvc.GetAppVersion(httpRequest.Params[0]);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object InsertDownTime(DownTimeModel downTimeModel)
    {
        var obj = HDSvc.InsertDownTime(downTimeModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //getDownTimeData

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object getDownTimeData(DownTimeRpt1 CustRpt)
    {
        var obj = HDSvc.getDownTimeData(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //DeleteDownTime

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    [HttpHead]

    public Object DeleteDownTime(breaktime1 reldet)
    {
        var obj = HDSvc.DeleteDownTime(reldet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetFloor(HDModel hDModel)
    {
        object res = HDSvc.GetFloor(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;

    }


    //Helpdesk approval flow API's
    //Get approval list
    [HttpPost]
    public HttpResponseMessage GetApprovalViewRequestList(HDModel hDModel)
    {
        var obj = HDSvc.HelpdeskApprovalList(hDModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    //Reject Approval Request
    [HttpPost]
    public HttpResponseMessage RejectApprovalRequest(HDApproveModel hDApproveModel)
    {
        var obj = HDSvc.RejectApprovalRequest(hDApproveModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Skip Approval Request
    [HttpPost]
    public HttpResponseMessage SkipApprovalRequest(HDApproveModel hDApproveModel)
    {
        var obj = HDSvc.SkipApprovalRequest(hDApproveModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Approve Approval Request
    [HttpPost]
    public HttpResponseMessage ApproveApprovalRequest(HDApproveModel hDApproveModel)
    {
        var obj = HDSvc.ApproveApprovalRequest(hDApproveModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage GetLocationsByCity([FromBody] HDModel main)
    {

        ErrorHandler err = new ErrorHandler();
        err._WriteErrorLog_string("BranchCheckList:" + JsonConvert.SerializeObject(main));
        var obj = HDSvc.GetLocationsByCity(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    //PPM update for M&S
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage GetPlanViewMobile(PvmGetPlans PGP)
    {
        var obj = HDSvc.GetPlanViewMobile(PGP);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage PvmUpdatePlansMobile(PvmUpdatePlansMobile PvmUpdatePlansMobiles)
    {
        var obj = HDSvc.PvmUpdatePlansMobile(PvmUpdatePlansMobiles);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Multiple Image Save 
    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("GET", "POST")]
    [HttpPost]
    public HttpResponseMessage UploadPPMMultipleImages()
    {
        var httpRequest = HttpContext.Current.Request;
        ErrorHandler err = new ErrorHandler();
        var obj = HDSvc.UploadPPMMultipleImages(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage InsertCheckListV3([FromBody] HDModel hD, [FromUri] int BCLID)
    {
        var obj = HDSvc.GenerateCheck_Report(BCLID, hD);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetAssetBarScan([FromBody] HDModel hD)
    {
        var obj = HDSvc.GetAssetBarScan(hD);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Asset_Type([FromBody] HDModel hD)
    {
        var obj = HDSvc.Asset_Type(hD);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage AssetData_To_Reconsolidate([FromBody] PostAssetBarScan hD)
    {
        var obj = HDSvc.AssetData_To_Reconsolidate(hD);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage AssetScanReport(AssetScanModel ASTModel)
    {
        var obj = HDSvc.AssetScanReport(ASTModel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Validation AllocateSeats

    [AllowAnonymous]
    [System.Web.Http.AcceptVerbs("POST")]
    [HttpPost]
    public HttpResponseMessage AllocateSeatsValidation(List<HD_SPACE_ALLOCATE_DETAILS_Val> allocDetLst)
    {
        var obj = HDSvc.allocateSeatsValidation(allocDetLst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
    [HttpPost]
    public Object validateuserspacedetails(HDModel hdmodel)
    {
        object res = HDSvc.validateuserspacedetails(hdmodel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, res);
        return response;
    }
}
