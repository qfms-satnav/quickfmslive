﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>
/// 
public class CPModel
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string Password { get; set; }
    public string APIKey { get; set; }
    public string code { get; set; }
    public string Role_id { get; set; }
    public string Search { get; set; }
    public string DeviceType { get; set; }
    public string DeviceId { get; set; }
    public string Name { get; set; }
    public string City { get; set; }
    public string LoginSno { get; set; }
    public string searchValue { get; set; }
}

public class CopilotLogin
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string AUR_KNOWN_AS { get; set; }
}

public class CP_SPACE_ALLOCATE_DETAILS_LST
{
    public string COMPANYID { get; set; }
    public string Mode { get; set; }
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
}

public class CP_SPACE_REL_DETAILS
{
    public int reltype { get; set; }
    public string COMPANYID { get; set; }
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
}

public class CP_SPACE_ALLOCATE_DETAILS
{
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
}

public class SPACE_ALLOC_DETAILS_TIMEAI
{
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
}

//Help Desk
public class HDRaiseRequest
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string ChildCat { get; set; }
    public string probdesc { get; set; }
}
public class ChecklistIns
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string Type { get; set; }
    public string Loc_id { get; set; }
    public int State { get; set; }
}
public class Helpdesk
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string reqid { get; set; }
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public string UserEmail { get; set; }
}

public class AST_COP_REQ
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string AIR_REQ_ID { get; set; }
}

public class COP_RESERVE
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string RMId { get; set; }
    public DateTime? FROM_DT { get; set; }
    public DateTime? TO_DT { get; set; }
    public DateTime FROM_TIME { get; set; }
    public DateTime TO_TIME { get; set; }
    public string REQID { get; set; }
}

public class MM_COP_REQ
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
}
public class GH_COP_REQ
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
}
//PROPERTY
public class PROPERTY_MODULE
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
}
