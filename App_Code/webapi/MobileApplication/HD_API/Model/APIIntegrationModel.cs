﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for APIIntegrationModel
/// </summary>
public class APIIntegrationModel
{
    public string EmployeeID { get; set; }  
    public string Name { get; set; }  
    public string Gender { get; set; }  
    public string EmailId { get; set; }  
    public string ReportingManager { get; set; }  
    public string Designation { get; set; }  
    public string MobileNumber { get; set; }  
    public string Department { get; set; }  
    public string ParentEntity { get; set; }  
    public string ChildEntity { get; set; }  
    public string Vertical { get; set; }  
    public string Costcenter { get; set; }  
    public string Location { get; set; }  
    public string City { get; set; }  
    public string Country { get; set; }  
    public string JoiningDate { get; set; }  
    public string EmployeeType { get; set; }  
    public string Grade { get; set; }  
    public string Status { get; set; }  
    public string Timezone { get; set; }  
    public string Remarks { get; set; }  
    
}