﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;


public class HD_SPACE_REL_DETAILS
{

    public string CompanyId { get; set; }
    public string User_Id { get; set; }
    public List<SPACE_ALLOC_DETAILS> sad { get; set; }
    public int reltype { get; set; }
}
public class Locationlstt
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class LocationData
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public List<Locationlstt> Locationlstts { get; set; }
}

public class SpaceMapDet
{
    public String CompanyId { get; set; }
    public String User_Id { get; set; }
    public String Item { get; set; }
    public String lcm_code { get; set; }
    public String flr_code { get; set; }
    public String twr_code { get; set; }
    public String subitem { get; set; }
    public String category { get; set; }
    public String CatValue { get; set; }
    public Int32 mode { get; set; }
    public String spcid { get; set; }
    public int key_value { get; set; }
}
public class QrCodeVariable
{
    public string AssetCode { get; set; }
    public string AssetID { get; set; }
    public string AssetName { get; set; }
    public string Location { get; set; }
    public string Brand { get; set; }
    public string Model { get; set; }
    public string CompanyId { get; set; }
    public string AssignedTo { get; set; }
    public string SerialNumber { get; set; }
    public string State { get; set; }
    public string ActucalCost { get; set; }
    public string AdditionalCost { get; set; }
    public string LabourCost { get; set; }
    public string SparePartsCost { get; set; }
    public string BreakdownStatus { get; set; }
    public string BreakdownNumbers { get; set; }
    public string ImagePath { get; set; }
    public string LastPPMDate { get; set; }

}
//Space Module
public class Maplistfloors
{
    public string FLR_ID { get; set; }
    public string CTY_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string APIKey { get; set; }
    public string AURID { get; set; }
    public string FLR_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string TWR_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string COMPANYID { get; set; }
    public string FLR_NAME { get; set; }
    public string FLR_USR_MAP { get; set; }
}

public class HD_SPACE_ALLOCATE_DETAILS_LST
{
    public string COMPANYID { get; set; }
    public string AUR_ID { get; set; }
    public string Mode { get; set; }
    public HD_SPACE_ALLOCATE_DETAILS LST { get; set; }
}



public class HD_SPACE_ALLOCATE_DETAILS
{

    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
  
}
public class ZonalFmLocations
{
    public string companyId { get; set; }
    public string userId { get; set; }
    public int submittedCode { get; set; }
}



public class GetSpaceDetails
{
    public string Space_Id { get; set; }
    public string Space_Type { get; set; }
    public string Floor_Id { get; set; }
    public string APIKey { get; set; }
    public string AURID { get; set; }
    public string COMPANYID { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string lcm_code { get; set; }
    public string twr_code { get; set; }
    public string flr_code { get; set; }
    public string capacity { get; set; }
    public string city_code { get; set; }
    public string Item { get; set; }
    public string mode { get; set; }
    public int modetype { get; set; }
    public string category { get; set; }
    public string subitem { get; set; }
    public string RequestId { get; set; }
}


public class ConferencedBooked
{
    public string ConferenceId { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
    public string COMPANYID { get; set; }
    public string UserID { get; set; }
    public string Type { get; set; }
}
public class SubmitSpaceDetails
{
    public string COMPANYID { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string SPC_ID { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
    public string AURID { get; set; }
    public string STATUS { get; set; }
}

public class Space_Restrict_Seat
{
    public string AUR_ID { get; set; }

    public string SPC_ID { get; set; }

    public string COMPANYID { get; set; }
}

public class Space_Restrict_Seat_details
{
    public string SPC_ID { get; set; }

    public string AUR_ID { get; set; }

    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string COMPANYID { get; set; }
}



//Property Proposal
public class ProSavedDraft
{
    public string PM_SNO { get; set; }
    public string PM_REQ_ID { get; set; }
    public string PM_CREATED_DT { get; set; }
    public string STA_TITLE { get; set; }
    public string PM_CREATED_BY { get; set; }
    public string CompanyId { get; set; }
    public string AUR_ID { get; set; }

}
public class ImageClas
{
    private string _fn;
    public string Filename
    {
        get
        {
            return _fn;
        }
        set
        {
            _fn = value;
        }
    }

    private string _Fpath;
    public string FilePath
    {
        get
        {
            return _Fpath;
        }
        set
        {
            _Fpath = value;
        }
    }
}

public class ProposalDraftDetails
{
    public string CompanyId { get; set; }
    public string AUR_ID { get; set; }
    public string PM_PPT_PM_REQ_ID { get; set; }
    public string PM_PPT_REQ_TYPE { get; set; }
    public string PM_PPT_NATURE { get; set; }
    public string PM_PPT_ACQ_THR { get; set; }
    public string PM_PPT_CTY_CODE { get; set; }
    public string PM_PPT_LOC_CODE { get; set; }
    public string PM_PPT_TOW_CODE { get; set; }
    public string PM_PPT_FLR_CODE { get; set; }
    public string PM_PPT_TOT_TOI { get; set; }
    public string PM_PPT_TYPE { get; set; }
    public string PM_PPT_NAME { get; set; }
    public DateTime PM_PPT_ESTD { get; set; }
    public string PM_PPT_AGE { get; set; }
    public string PM_PPT_ADDRESS { get; set; }
    public string PM_PPT_SIGN_LOC { get; set; }
    public string PM_PPT_SOC_NAME { get; set; }
    public string PM_PPT_LAT { get; set; }
    public string PM_PPT_LONG { get; set; }
    public string PM_PPT_OWN_SCOPE { get; set; }
    public string PM_PPT_RECOMMENDED { get; set; }
    public string PM_PPT_RECO_REM { get; set; }
    public string PM_OWN_NAME { get; set; }
    public string PM_OWN_PH_NO { get; set; }
    public string PM_OWN_EMAIL { get; set; }
    public string PM_PREV_OWN_NAME { get; set; }
    public string PM_PREV_OWN_PH_NO { get; set; }
    public string PM_OWN_ADDRESS { get; set; }
    public float PM_AR_CARPET_AREA { get; set; }
    public float PM_AR_BUA_AREA { get; set; }
    public float PM_AR_COM_AREA { get; set; }
    public float PM_AR_RENT_AREA { get; set; }
    public float PM_AR_USABEL_AREA { get; set; }
    public float PM_AR_SBU_AREA { get; set; }
    public float PM_AR_PLOT_AREA { get; set; }
    public float PM_AR_FTC_HIGHT { get; set; }
    public float PM_AR_FTBB_HIGHT { get; set; }
    public string PM_AR_MAX_CAP { get; set; }
    public string PM_AR_OPT_CAP { get; set; }
    public string PM_AR_SEATING_CAP { get; set; }
    public string PM_AR_FLOOR_TYPE { get; set; }
    public string PM_AR_FSI { get; set; }
    public float PM_AR_FSI_RATIO { get; set; }
    public string PM_AR_PREF_EFF { get; set; }
    public float PM_PUR_PRICE { get; set; }
    public DateTime PM_PUR_DATE { get; set; }
    public float PM_PUR_MARKET_VALUE { get; set; }
    public string PM_GOV_IRDA { get; set; }
    public string PM_GOV_PC_CODE { get; set; }
    public string PM_GOV_PROP_CODE { get; set; }
    public string PM_GOV_UOM_CODE { get; set; }
    public string PM_INS_TYPE { get; set; }
    public string PM_INS_VENDOR { get; set; }
    public float PM_INS_AMOUNT { get; set; }
    public string PM_INS_PNO { get; set; }
    public DateTime PM_INS_START_DT { get; set; }
    public DateTime PM_INS_END_DT { get; set; }
    public string CHE_NAME { get; set; }
    public string PM_PPT_ENTITY { get; set; }
    public string PM_PPT_INSPECTED_BY { get; set; }
    public DateTime PM_PPT_INSPECTED_DT { get; set; }
    public string PM_OWN_LANDLINE { get; set; }
    public string PM_PPT_POA { get; set; }
    public string PM_PHYCDTN_WALLS { get; set; }
    public string PM_PHYCDTN_ROOF { get; set; }
    public string PM_PHYCDTN_CEILING { get; set; }
    public string PM_PHYCDTN_WINDOWS { get; set; }
    public string PM_PHYCDTN_DAMAGE { get; set; }
    public string PM_PHYCDTN_SEEPAGE { get; set; }
    public string PM_PHYCDTN_OTHR_TNT { get; set; }
    public string PM_BLDG_NAME_ADDRESS { get; set; }
    public string PM_LLS_VITRIFIED { get; set; }
    public string PM_LLS_VITRIFIED_RMKS { get; set; }
    public string PM_LLS_WASHROOMS { get; set; }
    public string PM_LLS_WASH_RMKS { get; set; }
    public string PM_LLS_PANTRY { get; set; }
    public string PM_LLS_PANTRY_RMKS { get; set; }
    public string PM_LLS_SHUTTER { get; set; }
    public string PM_LLS_SHUTTER_RMKS { get; set; }
    public string PM_LLS_OTHERS { get; set; }
    public string PM_LLS_OTHERS_RMKS { get; set; }
    public string PM_LLS_WORK_DAYS { get; set; }
    public string PM_LLS_ELEC_EXISTING { get; set; }
    public string PM_LLS_ELEC_REQUIRED { get; set; }
    public string PM_COST_RENT { get; set; }
    public string PM_COST_RENT_SFT { get; set; }
    public string PM_COST_RATIO { get; set; }
    public string PM_COST_OWN_SHARE { get; set; }
    public string PM_COST_SECDEPOSIT { get; set; }
    public string PM_COST_GST { get; set; }
    public string PM_COST_MAINTENANCE { get; set; }
    public string PM_COST_ESC_RENTALS { get; set; }
    public string PM_COST_RENT_FREE { get; set; }
    public string PM_COST_STAMP { get; set; }
    public string PM_COST_AGREEMENT_PERIOD { get; set; }
    public string PM_OTHR_FLOORING { get; set; }
    public string PM_OTHR_FLRG_RMKS { get; set; }
    public string PM_OTHR_WASHRM_EXISTING { get; set; }
    public string PM_OTHR_WASHRM_REQUIRED { get; set; }
    public string PM_OTHR_POTABLE_WTR { get; set; }
    public string PM_OTHR_POTABLE_WTR_REMARKS { get; set; }
    public string PM_OTHR_IT_INSTALL { get; set; }
    public string PM_OTHR_IT_INSTALL_RMKS { get; set; }
    public string PM_OTHR_DG_INSTALL { get; set; }
    public string PM_OTHR_DG_INSTALL_RMKS { get; set; }
    public string PM_PPT_SIG_LENGTH { get; set; }
    public string PM_PPT_SIG_WIDTH { get; set; }
    public string PM_PPT_AC_OUTDOOR { get; set; }
    public string PM_PPT_GSB { get; set; }
    public string PM_DOC_TITLE { get; set; }
    public string PM_DOC_TITLLE_RMKS { get; set; }
    public string PM_DOC_OCCUPANCY { get; set; }
    public string PM_DOC_OCC_RMKS { get; set; }
    public string PM_DOC_BUILD { get; set; }
    public string PM_DOC_BUILD_RMKS { get; set; }
    public string PM_DOC_PAN { get; set; }
    public string PM_DOC_PAN_RMKS { get; set; }
    public string PM_DOC_TAX { get; set; }
    public string PM_DOC_TAX_RMKS { get; set; }
    public string PM_DOC_OTHER_INFO { get; set; }
    public string PM_PPT_TOT_FLRS { get; set; }
    public string PM_PPT_RELOC_NAME { get; set; }
    public string PM_OTHR_DG_INSTALL_REMARKS { get; set; }
    public string PM_OTHR_IT_INSTALL_REMARKS { get; set; }
    public string PM_PHYCDTN_OTHRBU { get; set; }
    public string PM_PPT_RELOC_ADDRESS { get; set; }
    public string PM_POA_ADDRESS { get; set; }
    public string PM_POA_MOBILE { get; set; }
    public string PM_POA_LL_TYPE { get; set; }
    public string PM_POA_EMAIL { get; set; }
    public string PM_POA_NAME { get; set; }
    public string PM_POA_SIGN { get; set; }
    public ImageClas SIGNAGEIMAGES { get; set; }
    public ImageClas AC_OUTDOOR_IMAGE { get; set; }
    public ImageClas GSB_IMAGE { get; set; }
    public List<ImageClas> IMAGES { get; set; }

}



#region Project CheckList
public class SaveProjectChecklistInsert
{
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public string PT_CODE { get; set; }
    public string VER_CODE { get; set; }
    public string INSPECTOR { get; set; }
    public string VISITDATE { get; set; }
    public string CompanyId { get; set; }
    public string AUR_ID { get; set; }
    public string FLAG { get; set; }
    public string PROJECTNAME { get; set; }
    public List<SelectedPCLDetails> SelectedLists { get; set; }
    public string MainCompanyId { get; set; }
    public string OVERALL_CMTS { get; set; }
}
public class SelectedPCLDetails
{
    public string CatCode { get; set; }
    public string SubCatCode { get; set; }
    public string ChildCode { get; set; }
    public string ChildSubCode { get; set; }
    public string ChildCatValue { get; set; }
    public string ChildSubValue { get; set; }
    public string ChildSubCatValue { get; set; }
    public string Type { get; set; }
}

public class GetProjectList
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string PTCode { get; set; }
}

#endregion
public class SubMenuItems
{
    public string SUB_MOD_NAME { get; set; }
    public string SUB_MOD_ID { get; set; }
    public string MAIN_MOD_ID { get; set; }
    public string SUB_MOD_IMG { get; set; }
    public string SUB_MOD_PAGE_PATH { get; set; }
    public Type TargetPage { get; set; }
}

public class MainMenuItems
{
    public string MOD_NAME { get; set; }
    public string MOD_ID { get; set; }
    public string Text { get; set; }
    public string ImagePath { get; set; }
    public Type TargetPage { get; set; }
    public List<SubMenuItems> SubMenu { get; set; }

}

public class MenuByRole
{
    public string CompanyId { get; set; }
    public string APIKey { get; set; }
    public string RoleId { get; set; }
    public string MOD_ID { get; set; }
}

public class HDModel
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string Password { get; set; }
    public string APIKey { get; set; }
    public string code { get; set; }
    public string Role_id { get; set; }
    public string Search { get; set; }
    public string DeviceType { get; set; }
    public string DeviceId { get; set; }
    public string Name { get; set; }
    public string City { get; set; }
    public string LoginSno { get; set; }
    public string searchValue { get; set; }


}
public class MaintenanceMenu
{
    public string Message { get; set; }
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string code { get; set; }
    public Dictionary<string, List<MenuItem>> MenuItems { get; set; }
}

public class MenuItem
{
    public string Main_Menu { get; set; }
    public string Sub_Menu { get; set; }
    public string IMG_PATH { get; set; }
    public int CLS_SORT_ORDER { get; set; }
    public string PAGE_NAME { get; set; }
}

public class Root
{
    //public string Message { get; set; }
    public List<MaintenanceMenu> MaintanaceMenu { get; set; }
}
public class DownTimeModel
{
    public string RequestID { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string DownTime { get; set; }
    public string DownTimeFactor { get; set; }
    public string CompanyID { get; set; }
}

public class CovidModel
{   public string UserId { get; set; }
    public string companyId { get; set; }
    public List<CovidTerms> covidTerms { get; set; }
}

public class CovidTerms
{
    public string COVID_CODE { get; set; }
    public string COVID_NAME { get; set; }
    public int COVID_SUBCODE { get; set; }
}
public class SpareDashboardModel
{
    public DateTime Fromedate { get; set; }
    public DateTime ToDate { get; set; }
    public string CompanyID { get; set; }
}
public class Modules
{
    public string T_MODULE { get; set; }
    public string TNAME { get; set; }


}

public class GETSLA
{
    public string CompanyId { get; set; }
    public string Location { get; set; }
    public string ChildCategory { get; set; }
    public string APIKey { get; set; }

}

public class GETINCH
{
    internal string TENANT_ID;

    public string CompanyId { get; set; }
    public string Location { get; set; }
    public string MAINCAT { get; set; }
    public string SUBCAT { get; set; }
    public string CHILDCAT { get; set; }
    public string COMPANYID { get; set; }
    public string AURID { get; set; }
    public string ChildCategory { get; internal set; }
    public string APIKey { get; internal set; }
}

public class SLAND
{
    public string CompanyId { get; set; }
    public string Location { get; set; }
    public string MAINCAT { get; set; }
    public string SUBCAT { get; set; }
    public string CHILDCAT { get; set; }
    public string COMPANYID { get; set; }
    public string AURID { get; set; }
    public string ChildCategory { get; internal set; }
    public string APIKey { get; internal set; }

}

#region Branch CheckList
public class BranchCheckListSavedDrafts
{
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public string LCM_CNP { get; set; }
    public string VISITDATE { get; set; }
    public string LCM_NAME { get; set; }
    public string SavedCount { get; set; }
}


public class SavedDraftsList
{
    public List<BranchCheckListInsert> BCLSavedDraftsByLoc { get; set; }
}

public class BranchCheckListInsert
{
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public string LCM_CNP { get; set; }
    public string INSPECTOR { get; set; }
    public string VISITDATE { get; set; }
    public string AUR_ID { get; set; }
    public string FLAG { get; set; }
    public string CompanyId { get; set; }
    public List<SelectedList> SelectedLists { get; set; }
    public List<ImagesList> imagesList { get; set; }
    public string OVERALL_CMTS { get; set; }

}

public class SelectedList
{
    public string CatCode { get; set; }
    public string CatName { get; set; }
    public string SubCatName { get; set; }
    public string SubcatCode { get; set; }
    public string ScoreCode { get; set; }
    public string ScoreName { get; set; }
    public string txtdata { get; set; }
    public string Date { get; set; }
    public string SubScore { get; set; }
    public string FilePath { get; set; }
}

public class ImagesList
{
    public string Imagepath { get; set; }
}

public class Score
{
    public string ScoreValues { get; set; }
}



public class TextBoxValues
{
    public string Names { get; set; }
}

public class BranchChecklist
{
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_CH_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public string BCL_SUB_TYPE { get; set; }
    public string BCLD_FLAG { get; set; }
    public string BCL_SELECTED_DT { get; set; }
    public List<Score> Score { get; set; }
    public List<TextBoxValues> TextBoxValues { get; set; }
}

public class Inspectors
{
    public string INSPECTOR { get; set; }
    public string PRJCKL { get; set; }
    public string CompanyId { get; set; }
}

public class BCLLocations
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public string LCM_CNP { get; set; }
    public string CTY_NAME { get; set; }
}

public class BCLLocationsByCity
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
}


#endregion

public class GoogleProfile
{
    public string Id { get; set; }
    public string DisplayName { get; set; }
    public Image Image { get; set; }
    public List<Email1> Emails { get; set; }
    public string Gender { get; set; }
    public string ObjectType { get; set; }
}
public class Email1
{
    public string Value { get; set; }
    public string Type { get; set; }
}

public class PropertyOwnerSendEmail
{
    public int PN_TYPEID { get; set; }
    public string PN_PROPERTYTYPE { get; set; }
    public string PM_PPT_NAME { get; set; }
    public int PM_PPT_SNO { get; set; }
    public string Message { get; set; }
    public string Email { get; set; }

}
public class LandLordOwner
{
    public string LL_Name { get; set; }
    public string LL_Email { get; set; }

}

public class Profile
{
    public string EmployeeId { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string Extension { get; set; }
    public Nullable<System.DateTime> DOB { get; set; }
    public Nullable<System.DateTime> DOJ { get; set; }
    public string Country { get; set; }
    public string TimeZone { get; set; }
    public string ReportingManager { get; set; }
    public string Department { get; set; }
    public string Designation { get; set; }
    public string Status { get; set; }
    public string APIKey { get; set; }
}

public class Key
{
    public string APIKey { get; set; }
}

public class ValidateKey
{
    public string TENANT_ID { get; set; }
    public int COMPANYID { get; set; }
    public string AURID { get; set; }
    public string AUR_LOCATION { get; set; }
}
public class DashboardProfile
{
    public string EMP_ID { get; set; }
    public string EMP_NAME { get; set; }
    public string EMP_EMAIL { get; set; }
    public string EMP_DESIG { get; set; }
    public string EMP_REPNAME { get; set; }
    public string EMP_IMG { get; set; }
    public string DEPARTMENT { get; set; }
    public string REPORTING { get; set; }
    public string DOJ { get; set; }
    public string DOB { get; set; }
    public string EXTENSION { get; set; }
    public string AUR_RES_NUMBER { get; set; }
    public string STATUS { get; set; }
}
public class ChangePwd
{
    public string APIKey { get; set; }
    public string UserId { get; set; }
    public string OldPwd { get; set; }
    public string NewPwd { get; set; }
}

public class HDDashboardCount
{
    public string TOTAL { get; set; }
    public string PENDING { get; set; }
    public string INPROGRESS { get; set; }
    public string CLOSED { get; set; }
    public string ONHOLD { get; set; }
    public string REJECTED { get; set; }
    public string SLA_VIOLATION { get; set; }
}
public class QR_CODE_STUS
{
    public string AUR_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string companyId { get; set; }
    public string QR_STATUS { get; set; }
    public string QR_CODE { get; set; }
}

public class RaiseRequest
{
    public string ServiceType { get; set; }
    public string FlooringType { get; set; }
    public string PropertyType { get; set; }
    public string RequestType { get; set; }
    public string InsurenceType { get; set; }
    public string LocationType { get; set; }
    public string EntityType { get; set; }
    public string AcqThr { get; set; }
    public string Floor { get; set; }
    public string Problem { get; set; }
    public string RepeatCall { get; set; }
    public string ImpactType { get; set; }
    public string UrgencyType { get; set; }
    public string FileName { get; set; }
    public string APIKey { get; set; }
    public string PageNo { get; set; }
    public string PageSize { get; set; }

}
public class ServiceType
{
    public string SERVICE_TYPE_CODE { get; set; }
    public string SERVICE_TYPE_NAME { get; set; }
}
public class InsuranceType
{
    public string INSURANCE_TYPE { get; set; }
    public string ID { get; set; }
}
public class LocationType
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
}
public class RequestType
{
    public string PM_RT_TYPE { get; set; }
    public string PM_RT_SNO { get; set; }
}
public class PropType
{
    public string PN_PROPERTYTYPE { get; set; }
    public string PN_TYPEID { get; set; }
}
public class EntityType
{
    public string CHE_NAME { get; set; }
    public string CHE_CODE { get; set; }
}
public class FloorDetails
{
    public string FLR_NAME { get; set; }
    public string FLR_CODE { get; set; }
}
public class FlooringType
{
    public string PM_FT_TYPE { get; set; }
    public string PM_FT_SNO { get; set; }
}
public class AquisitionThr
{
    public string PN_ACQISITION_THROUGH { get; set; }
    public string PN_ACQISITION_ID { get; set; }
}
public class Categories
{
    public string MAINCAT { get; set; }
    public string SUBCAT { get; set; }
    public string CHILDCAT { get; set; }
}

public class ViewReq
{
    public string MAIN_CAT_CODE { get; set; }
    public string SUB_CAT_CODE { get; set; }
    public string CHILD_CAT_CODE { get; set; }
    public string CHILD_CATEGORY { get; set; }
    public string REQUEST_ID { get; set; }
    public string ASSIGN { get; set; }
    public string REQ_STATUS { get; set; }
    public string REQ_STATUS_NAME { get; set; }
    public string REQ_DATE { get; set; }
    public string REQ_BY { get; set; }
    public string SERE_ROL_ID { get; set; }
    public string ROL_DESCRIPTION { get; set; }
    public string LOCATION { get; set; }
    public string MOBILENO { get; set; }
    public string ASSIGNED_TO_NAME { get; set; }
}
public class FilterParam
{
    public string Filter { get; set; }
    public string APIKey { get; set; }
}
public class HDStatus
{
    public int STA_ID { get; set; }
    public string STA_TITLE { get; set; }
}
public class LocationType_cor
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string LAT { get; set; }
    public string LONG { get; set; }
}
public class HDParams
{
    public string RequestID { get; set; }
    public string ProblemDesc { get; set; }
    public string Feedback { get; set; }
    public string Remarks { get; set; }
    public string APIKey { get; set; }
    public string Status { get; set; }
    public string AssignTo { get; set; }
}
public class HDParamListData
{
    public string RequestID { get; set; }
    public string Status { get; set; }
    public string Remarks { get; set; }
    public string AssignTo { get; set; }
    public string Cost { get; set; }
}
public class ListHDParams
{
    public List<HDParamListData> HdParmsList { get; set; }
    public string APIKey { get; set; }
}


public class ReAssignName
{
    public string REASSING_EMP_ID { get; set; }
    public string REASSING_EMP_NAME { get; set; }

}
public class Image
{
    public string IMG_ID { get; set; }
    public string APIKey { get; set; }
    public string Url { get; set; }
}
public class HDRequestDetails
{
    public string SERVICE_TYPE { get; set; }
    public string PROBLEM_DESC { get; set; }
    public string STATUS { get; set; }
    public string REPEAT { get; set; }
    public string IMPACT { get; set; }
    public string URGENCY { get; set; }
    public string FEEDBACK { get; set; }
    public string REMARKS { get; set; }
    public string REQ_DATE { get; set; }
}
public class ReportParams
{
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string APIKey { get; set; }
}

public class ReportFilterParams
{
    public string Filter { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string APIKey { get; set; }
}


public class SelectedDataModel
{
    //public string LCMLST { get; set; }
    //public string InspectdBy { get; set; }
    //public string date { get; set; }

    //public int Flag { get; set; }

    public int BCL_ID { get; set; }
    public string Location { get; set; }
    public string InspectdBy { get; set; }
    public DateTime InspectdDate { get; set; }
    public string CreatedBy { get; set; }
    public string Company { get; set; }
    public string SaveFlag { get; set; }

    public int Flag { get; set; }
    public List<SelectedRadioModel> Seldata { get; set; }
    public List<FileProperties> filesupload { get; set; }

}

public class SelectedRadioModel
{
    public string CatCode { get; set; }
    public string SubcatCode { get; set; }
    public string ScoreName { get; set; }
    public string FilePath { get; set; }
    public string row { get; set; }
    public string txtdata { get; set; }
    public string Date { get; set; }
    public string SubScore { get; set; }
}


public class ReportDataSet
{
    public string REQUEST_ID { get; set; }
    public string SERVICE_TYPE { get; set; }
    public string REQ_BY { get; set; }
    public string STATUS { get; set; }
    public string LOCATION { get; set; }
    public string TAT { get; set; }
    public string SLA { get; set; }
    public string SERE_ROL_ID { get; set; }
    public string ROL_DESCRIPTION { get; set; }
    public string MOBILE_NO { get; set; }
    public string SER_ADDN_COST { get; set; }
    public string REQ_DATE { get; set; }
    public string ASSIGNED_TO { get; set; }

}
public class DashboardDetails
{
    public string Status { get; set; }
    public string APIKey { get; set; }
}
public class DeviceToken
{
    public string APIKey { get; set; }
    public string Token { get; set; }
}
public class Pagination
{
    public string APIKey { get; set; }
    public int PageNo { get; set; }
    public int PageSize { get; set; }
}
public class ModifyViewReq
{
    public string APIKey { get; set; }
    public string RequestId { get; set; }
    public string Remarks { get; set; }
    public string Impact { get; set; }
    public string Urgency { get; set; }
    public string Repeat { get; set; }
    public string Problem { get; set; }

    public string FeedBack { get; set; }

}
public class SLAViolation
{
    public string REQUEST_ID { get; set; }
    public string PROBLEM_DESC { get; set; }
    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string SERVICE_TYPE { get; set; }
    public string STATUS { get; set; }
}
public class Emergency
{
    public string SERVICE_TYPE_CODE { get; set; }
    public string SERVICE_TYPE_NAME { get; set; }
    public string SERVICE_TYPE_IMAGE { get; set; }
}
public class Notifications
{
    public string title { get; set; }
    public string type { get; set; }
    public string body { get; set; }
    public string Request_Id { get; set; }
    public string Request_Status { get; set; }
    public string Problem_Desc { get; set; }
}
public class NotificationParams
{
    public string RequestId { get; set; }
    public int Cnt { get; set; }
    public string APIKey { get; set; }
}
public class NotificationHistory
{
    public string SNO { get; set; }
    public string NOTIF_BODY { get; set; }
    public string NOTIF_RESULT { get; set; }
    public string NOTIF_REQUEST_ID_STATUS { get; set; }
    public string NOTIF_REQUEST_ID { get; set; }
}
public class StatusUpdate
{
    public string Sno { get; set; }
    public string RequestId { get; set; }
    public string APIKey { get; set; }
}

public class ProjectCheckList
{
    public string Category { get; set; }
    public string SubCategory { get; set; }
    public string CatCode { get; set; }
    public string SubCatCode { get; set; }
    public List<childList> childlist { get; set; }
    public List<TextBoxValues> TextBoxValues { get; set; }
    public string Mandatory { get; set; }
    public string SubChildCategory { get; set; }
    public string Subchildfiled { get; set; }
    public string SubChildCategory1 { get; set; }
    public string PM_SCC1_R_C_T { get; set; }
    public string ChildCatCode { get; set; }

}
public class childList
{
    public string ChildCode { get; set; }
    public string ChildCategory { get; set; }
    public string childfiled { get; set; }
    public List<Subchildcategory> Subchildcategory { get; set; }
}
public class Subchildcategory
{
    public string ScoreCode { get; set; }
    public string ScoreName { get; set; }
    public string ScoreText { get; set; }
    public List<subchildcat1> subchildcat1 { get; set; }
}
public class subchildcat1
{
    public string SubchildcatCode { get; set; }
    public string Subchildcatname { get; set; }
    public string Subchildcatfield { get; set; }

}
public class SaveProjectCheck
{
    public string Country { get; set; }
    public string City { get; set; }
    public string Location { get; set; }
    public string BusinessUnit { get; set; }
    public string Inspectionby { get; set; }
    public string ProjectType { get; set; }
    public string FromDate { get; set; }
    public string ProjectName { get; set; }
    public int Flag { get; set; }
    public List<Details> ChecklistDetails { get; set; }
}
public class BindDetails
{
    public string CatCode { get; set; }
    public string SubCatCode { get; set; }
    public string ChildCode { get; set; }
    public string ChildSubCode { get; set; }
    public string ChildCatValue { get; set; }
    public string ChildSubValue { get; set; }
    public bool ticked { get; set; }
}


public class Zonal_ID
{
    public string id { get; set; }
    public string BCL_ID { get; set; }
    public bool ticked { get; set; }
}
public class Towerlsts
{

    public string TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class TowerData
{
    public string CompanyID { get; set; }
    public string UserId { get; set; }
    public List<Towerlsts> Towerlsts { get; set; }
}

public class EMPRoles
{
    public string ROLE { get; set; }
}



public class Floorlsts
{

    public string FLR_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public string TWR_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}
public class SPACE_ALLOC_DETAILS_TIME_Mobile
{
    public string CompanyID { get; set; }
    public string UserId { get; set; }
    public List<Space_alloc_details> Data { get; set; }

}

public class Space_alloc_details
{
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
}

public class ScheduleMySeatBookingsModelMobile
{
    public string companyId { get; set; }
    public string userid { get; set; }
    public string flrlst { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }
}

public class AssertTicketRequestModel
{
    public string CompanyId { get; set; }
    public string LocationCode { get; set; }
    public string CategoryId { get; set; }
    public string SubcategoryId { get; set; }
    public string BrandId { get; set; }
    public string VendorId { get; set; }
}


public class MaintanceTicketModel
{
    public string RequesteId { get; set; }
    public string Status { get; set; }
    public string InchargeId { get; set; }
    public string RootCause { get; set; }
    public string PreventiveAction { get; set; }
    public string CorrectiveAction { get; set; }
    public string ProductionImpact { get; set; }
    public string ProoblemOwner { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string Remarks { get; set; }
    public string AttachmentPath { get; set; }
    public string User { get; set; }
    public string CompanyId { get; set; }
    public List<SpareParts> spareParts { get; set; }
    public string DownTime { get; set; }
    public string DownTieFactor { get; set; }
    public string Breached { get; set; }
    public string SubAsset { get; set; }
    public string ErrorCode { get; set; }
    public string AssetCode { get; set; }
    public string OtherProblemOwner { get; set; }
    public string OtherErrorDesc { get; set; }
    public string Lcm_Code { get; set; }

}

public class SpareParts
{
    public string SPAREPARTCODE { get; set; }
    public string CATEGORY { get; set; }
    public string SUBCATCODE { get; set; }
    public string BRNDCODE { get; set; }
    public string MODELCODE { get; set; }
    public string QTY { get; set; }
}

public class BreakDownStatusModel
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string Location { get; set; }
    public string SubCategory { get; set; }
}


public class GetMaintainanceidStatusModel
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string LocationCode { get; set; }
    public string Category { get; set; }
    public string PlanType { get; set; }
}

public class GetChecklistsDataOnAssetIdModel
{
    public string CompanyId { get; set; }
    public string AssetsId { get; set; }
    public string UserId { get; set; }
    public string PvdID { get; set; }
}

public class GetSearchTicketModel
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string LocationCode { get; set; }
    public string Screen { get; set; }
}

public class InsertAddSpareModel
{
    public string RequestLocation { get; set; }
    public string Remarks { get; set; }
    public string UserId { get; set; }
    public string CompanyId { get; set; }
    public string FileName { get; set; }
    public string MAINT_ID { get; set; }
    public string CostOfInventory { get; set; }
    public string Type { get; set; }
    public List<SpareParts> spareParts { get; set; }
}


public class InsertPPMChecklistDataModel
{
    public string LocationCode { get; set; }
    public string Inspectd { get; set; }
    public string Date { get; set; }
    public string UserId { get; set; }
    public string CompanyId { get; set; }
    public string Request { get; set; }
    public string Key { get; set; }
    public string AssetId { get; set; }
    public string OverCmts { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }

    public List<SelRadios> spareParts { get; set; }
}


public class SelRadios
{
    //public string row { get; set; }
    public string CatCode { get; set; }
    public string SubcatCode { get; set; }
    public string ScoreCode { get; set; }
    public string ScoreName { get; set; }
    public string txtdata { get; set; }
    public string Date { get; set; }
    public string SubScore { get; set; }
    public string FilePath { get; set; }
}




#region Add Property Models

public class SavePropertDetails
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }

    //General Details
    public string REQUEST_ID { get; set; }
    public string REQUEST_TYPE { get; set; }
    public string PROP_NATURE { get; set; }
    public string ACQ_TRH { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string TOWER { get; set; }
    public string FLOOR { get; set; }
    public string TOI_BLKS { get; set; }
    public string PROP_TYPE { get; set; }
    public string PROP_NAME { get; set; }
    public string ESTD_YR { get; set; }
    public int AGE { get; set; }
    public string PROP_ADDR { get; set; }
    public string SING_LOC { get; set; }
    public string SOC_NAME { get; set; }
    public string LATITUDE { get; set; }
    public string LONGITUDE { get; set; }
    public string SCOPE_WK { get; set; }
    public string RECM_PROP { get; set; }
    public string RECM_PROP_REM { get; set; }

    // OWNER DETAILS 
    public string OWN_NAME { get; set; }
    public string OWN_PH { get; set; }
    public string PREV_OWN_NAME { get; set; }
    public string PREV_OWN_PH { get; set; }
    public string OWN_EMAIL { get; set; }
    public string OWN_LANDLINE { get; set; }

    //Area Details 
    public float CARPET_AREA { get; set; }
    public float BUILTUP_AREA { get; set; }
    public float COMMON_AREA { get; set; }
    public float RENTABLE_AREA { get; set; }
    public float USABLE_AREA { get; set; }
    public float SUPER_BLT_AREA { get; set; }
    public float PLOT_AREA { get; set; }
    public float FTC_HIGHT { get; set; }
    public float FTBB_HIGHT { get; set; }
    public float MAX_CAPACITY { get; set; }
    public float OPTIMUM_CAPACITY { get; set; }
    public float SEATING_CAPACITY { get; set; }
    public string FLR_TYPE { get; set; }
    public string FSI { get; set; }
    public string FSI_RATIO { get; set; }
    public string PFR_EFF { get; set; }

    //PURCHASE DETAILS
    public float PUR_PRICE { get; set; }
    public string PUR_DATE { get; set; }
    public float MARK_VALUE { get; set; }
    public string IRDA { get; set; }
    public string PC_CODE { get; set; }
    public string PROP_CODE { get; set; }
    public string UOM_CODE { get; set; }

    //INSURANCE DETAILS
    public string IN_TYPE { get; set; }
    public string IN_VENDOR { get; set; }
    public string IN_AMOUNT { get; set; }
    public string IN_POLICY_NUMBER { get; set; }
    public DateTime IN_SDATE { get; set; }
    public DateTime IN_EDATE { get; set; }

    public string AUR_ID { get; set; }
    public string CMP_ID { get; set; }
    public string ENTITY { get; set; }
    public List<IMAGES> IMAGES { get; set; }
    public List<IMAGES> SIGNAGEIMAGES { get; set; }
    public List<IMAGES> AC_OUTDOOR_IMAGE { get; set; }
    public List<IMAGES> GSB_IMAGE { get; set; }

    //POA 
    public string POA_SIGNED { get; set; }
    public string POA_NAME { get; set; }
    public string POA_ADDRESS { get; set; }
    public string POA_MOBILE { get; set; }
    public string POA_EMAIL { get; set; }
    public string POA_LLTYPE { get; set; }
    //physical condition
    public string PHYCDTN_WALLS { get; set; }
    public string PHYCDTN_ROOF { get; set; }
    public string PHYCDTN_CEILING { get; set; }
    public string PHYCDTN_WINDOWS { get; set; }
    public string PHYCDTN_DAMAGE { get; set; }
    public string PHYCDTN_SEEPAGE { get; set; }
    public string PHYCDTN_OTHRTNT { get; set; }
    //Landlord's Scope of work 
    public string LL_VITRIFIED { get; set; }
    public string LL_VITRIFIED_RMKS { get; set; }
    public string LL_WASHRMS { get; set; }
    public string LL_WASHRM_RMKS { get; set; }
    public string LL_PANTRY { get; set; }
    public string LL_PANTRY_RMKS { get; set; }
    public string LL_SHUTTER { get; set; }
    public string LL_SHUTTER_RMKS { get; set; }
    public string LL_OTHERS { get; set; }
    public string LL_OTHERS_RMKS { get; set; }
    public string LL_WORK_DAYS { get; set; }
    public string LL_ELEC_EXISTING { get; set; }
    public string LL_ELEC_REQUIRED { get; set; }
    //OTHER DETAILS
    public string OTHR_FLOORING { get; set; }
    public string OTHR_FLRG_RMKS { get; set; }
    public string OTHR_WASH_EXISTING { get; set; }
    public string OTHR_WASH_REQUIRED { get; set; }
    public string OTHR_POTABLE_WTR { get; set; }
    //COST DETAILS
    public string COST_RENT { get; set; }
    public string COST_RENT_SFT { get; set; }
    public string COST_RATIO { get; set; }
    public string COST_OWN_SHARE { get; set; }
    public string COST_SECDEP { get; set; }
    public string COST_GST { get; set; }
    public string COST_MAINTENANCE { get; set; }
    public string COST_ESC { get; set; }
    public string COST_RENT_FREE { get; set; }
    public string COST_STAMP { get; set; }
    public string COST_AGREE { get; set; }

    //DOCUMENT DETAILS
    public string DOC_TITLE { get; set; }
    public string DOC_TITLE_RMKS { get; set; }
    public string DOC_OCCUP { get; set; }
    public string DOC_OCCUP_RMKS { get; set; }
    public string DOC_BUILD { get; set; }
    public string DOC_BUILD_RMKS { get; set; }
    public string DOC_PAN { get; set; }
    public string DOC_PAN_RMKS { get; set; }
    public string DOC_TAX { get; set; }
    public string DOC_TAX_RMKS { get; set; }
    public string DOC_OTHR_INFO { get; set; }
    public string SIG_LENGTH { get; set; }
    public string SIG_WIDTH { get; set; }
    public string AC_OUTDOOR { get; set; }
    public string GSB { get; set; }
    public DateTime INSPECTION_DATE { get; set; }
    public string INSPECTION_BY { get; set; }
    public string RELOCATION { get; set; }
    public string RELOCADDRS { get; set; }
    public string PM_PPT_TOT_FLRS { get; set; }

}

public class IMAGES
{
    public string FILENAME { get; set; }
    public string FILEPATH { get; set; }
}

public class GetGeneralDetails
{
    public string REQ_ID { get; set; }
    public List<RequestTypes> RequestTypes { get; set; }
    public List<PropertyNature> PropertyNature { get; set; }
    public List<AcquistionThrgh> AcquistionThrgh { get; set; }
    public List<Entity> Entity { get; set; }
    public List<Locations> Locations { get; set; }
    public List<PropertyType> PropertyType { get; set; }
    public List<Recommened> Recommened { get; set; }
    public List<FlooringTypes> FlooringTypes { get; set; }
    public List<InsuranceTypes> InsuranceTypes { get; set; }
    public List<StampDuty> StampDuty { get; set; }
    public string PM_RestRoom { get; set; }
    public string PM_PP_NAME { get; set; }
    public string ESTD { get; set; }
    public string PM_PP_ADDRESS { get; set; }
    public string PM_SIGNAGE_LOC { get; set; }
    public string PM_SOC_NAME { get; set; }
    public string PM_LAT { get; set; }
    public string PM_LONG { get; set; }
    public string PM_OWNER_SW { get; set; }
    public string PM_INSPC_BY { get; set; }
    public string PM_INSPC_DT { get; set; }
}

public class RequestTypes
{
    public int PM_RT_SNO { get; set; }
    public string PM_RT_TYPE { get; set; }
}

public class PropertyNature
{
    public int PM_PN_ID { get; set; }
    public string PM_PN_NAME { get; set; }
}

public class AcquistionThrgh
{
    public int PN_ACQISITION_ID { get; set; }
    public string PN_ACQISITION_THROUGH { get; set; }
}

public class Entity
{
    public string CHE_CODE { get; set; }
    public string CHE_NAME { get; set; }
}

public class Locations
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CTY_ID { get; set; }
    public string LCM_CNY_ID { get; set; }
}

public class PropertyType
{
    public int PN_TYPEID { get; set; }
    public string PN_PROPERTYTYPE { get; set; }
}

public class Recommened
{
    public int PM_RCMD_ID { get; set; }
    public string PM_RCMD_NAME { get; set; }
}

public class FlooringTypes
{
    public int PM_FT_SNO { get; set; }
    public string PM_FT_TYPE { get; set; }
}

public class InsuranceTypes
{
    public int ID { get; set; }
    public string INSURANCE_TYPE { get; set; }
}

public class StampDuty
{
    public string PM_STP_NAME { get; set; }
}
public class SEAT_CHECK
{
    public string SPC_ID { get; set; }
    public string AUR_ID { get; set; }
    public DateTime? FROM_DATE { get; set; }
    public DateTime? TO_DATE { get; set; }
    public string COMPANY_ID { get; set; }
}

public class HD_INSERT_CONTACT_DETAILS

{
    public string CompanyId { get; set; }
    public string Full_Name { get; set; }
    public string Company_Name { get; set; }

    public string Email { get; set; }

    public string Country { get; set; }

    public string PhoneNo { get; set; }

}





#endregion

public class GenerateBreakDownTicket
{
    public string CompanyId { get; set; }
    public string Location { get; set; }
    public string TicketType { get; set; }
    public string SubCategoryCode { get; set; }
    public string ErrorDescription { get; set; }
    public string OtherErrorDescription { get; set; }
    public string Comments { get; set; }
    public string UserId { get; set; }
    public string Impact { get; set; }
    public string Urgency { get; set; }
    public string StartDate { get; set; }
    public string ErrorDescriptionText { get; set; }
    public string SubEquipment { get; set; }
}

public class ValidateSpareRequestModel
{
    public string CompanyID { get; set; }
    public string UserID { get; set; }
    public string Remarks { get; set; }
    public string Type { get; set; }
    public string RequestID { get; set; }
}

public class BusinessCardInsertModel
{
    public string Department { get; set; }
    public string UserID { get; set; }
    public string Location { get; set; }
    public string Designation { get; set; }
    public string Mails { get; set; }
    public string Fax { get; set; }
    public string TelePhone { get; set; }
    public string Mobile { get; set; }
    public string Address { get; set; }
    public int TotalCards { get; set; }
    public string CreatedBy { get; set; }
    public string CompanyID { get; set; }
    public string Remarks { get; set; }
    public string Id { get; set; }
   
    public string Vendor { get; set; }
    public string Status { get; set; }
    public string Approved { get; set; }



    
   
}


public class ChildCategory
{
    public string TypeName { get; set; }
    public int PageNo { get; set; }
    public int PageSize { get; set; }
    public string CompanyId { get; set; }
}


public class Acknowledge
{
 
    public string CompanyId { get; set; }
    public string RequestId { get; set; }
    public string EmployeName { get; set; }


}


public class RCAquestionary
{
 
    public string RCACNYCode { get; set; }
    public string RCACityCode { get; set; }
    public string RCALocationCode { get; set; }
    public string RCADate { get; set; }
    public int RCACNP { get; set; }
    public string UserId { get; set; }
    public string companyId { get; set; }
    public string RCAoverallComments { get; set; }
    public int RCASubmit { get; set; }
    public string RCAbreakdownid { get; set; }
    public List<RCASubCatList>  rCASubCats { get; set; }
    public int RCAId { get; set; }
    public string RcaId { get; set; }
    public string BearkDownId { get; set; }
    
    



}

public class RCASubCatList
{
    public int RCASubId { get; set; }
    public string RCASubCode { get; set; }
    public string RCASubMNCCode { get; set; }
    public string RCAANS { get; set; }
}


public class TicketModel
{
    public string UserId { get; set; }
    public string companyId { get; set; }
    public string BreakdownId { get; set; }
    public List<TicketTiming> ticketTimings { get; set; }
}

public class TicketTiming
{
    public string description { get; set; }
    public string time { get; set; }
 
}


public class VisitorDetails
{
    public string companyId { get; set; }
    public string RequestId { get; set; }
    public string VisitorName { get; set; }
    public DateTime VisitingDate { get; set; }
    public int VisitingTime { get; set; }
    public string Relation { get; set; }
    public string Remarks { get; set; }
    public string Status { get; set; }
    public string VehicleRegNumber { get; set; }
    public string VehicleMake { get; set; }
    public string VehicleType { get; set; }
    public string OwnerAttachment { get; set; }
    public string CPRAttachment { get; set; }
    public DateTime validation { get; set; }
}


public class PushNotification
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
  
    public string DeviceType { get; set; }
    public string DeviceId { get; set; }
    public string TokenId { get; set; }
    public string Description { get; set; }
    public string Remarks { get; set; }
}

public class AndroidNotificationId
{
    public string ANDROID_ID { get; set; }

}

public class IosNotificationID
{
    public string IOS_ID { get; set; }
}



public class SPACE_ALLOCATE_DETAILS_LST_HDO
{
    public string COMPANYID { get; set; }
    public string AUR_ID { get; set; }
    public SPACE_ALLOCATE_DETAILS_HDO LST { get; set; }
}



public class SPACE_ALLOCATE_DETAILS_HDO
{

    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
    public string COUNTS { get; set; }
    public string Cost_Center_Name { get; set; }
}


public class SpareImagePath
{
    public  string companyId { get; set; } 
    public  string SpareId { get; set; } 
    public  string SpareImage { get; set; } 
}


public class BlockSeatRequest
{
    public string companyId { get; set; }
    public string RequestId { get; set; }
    public string VerticalRequestId { get; set; }
    public string VerticalName { get; set; }
    public string SpaceId { get; set; }
    public string EmployeeId { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string SpaceType { get; set; }
    public DateTime FromTime { get; set; }
    public DateTime ToTime { get; set; }
    public int Status { get; set; }
    public string InstanceAttendance { get; set; }
    public string ExtendAttendance { get; set; }
    public string Description { get; set; }
    public string UserId { get; set; }
    public List<string> emails { get; set; }

}

//public class HotDeskRequisitionMobile
//{
//    public string companyId { get; set; }
//    public List<HD_Details> HDL { get; set; }

//    public List<HD_Details_new> HDLN { get; set; }
//    public List<LCMlst> LCMlst { get; set; }
//    public List<Towerlst> twrlst { get; set; }
//    public List<Floorlst> flrlst { get; set; }
//    public string REM { get; set; }
//    public string L1_REM { get; set; }
//    public string L2_REM { get; set; }
//    public string REQ_ID { get; set; }
//    public int ALLOCSTA { get; set; }
//    public List<HD_REQI_ID> hdreqlst { get; set; }


//}
public class HD_Details
{
    public string SPC_ID { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public bool ticked { get; set; }
    public string AUR_ID { get; set; }

    public string VER_NAME { get; set; }

    public string Cost_Center_Name { get; set; }

    public string Seat_Type { get; set; }


}

public class HD_Details_new
{
    public string SPC_ID { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }


}
public class HD_REQI_ID
{
    public string REQ_ID { get; set; }
}

public class Towerlst
{
    public string TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class Floorlst
{
    public string FLR_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public string TWR_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}
public class HotDeskRequisitionMobile
{
    public string companyId { get; set; }
    public List<HDDetails> hDDetails { get; set; }
    public string EmployeeRemarks { get; set; }
    public string User_ID { get; set; }
    public int Allocation_ID { get; set; }
    public List<Locationlist> locationlstts { get; set; }
    public List<Towerlsts> towerlsts { get; set; }
    public List<Floorlsts> floorlsts { get; set; }
    public string RequestID { get; set; }
    public string L1Remarks { get; set; }
    public string L2Remarks { get; set; }

  
}
public class NewCheckListV2_For_Location
{
    public string CTY_CODE { get; set; }

    public string LCM_CODE { get; set; }

    public string LCM_NAME { get; set; }

    public string LAT { get; set; }
    public string LONG { get; set; }
    public string CompanyId { get; set; }

    public string User_ID { get; set; }
}

public class Locationlist
{

    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
 
    public bool ticked { get; set; }
}
public class HotDeskApproveMobile
{
    public string companyId { get; set; }

    public string User_ID { get; set; }
    public int Allocation_ID { get; set; }

    public string L1Remarks { get; set; }
    public string L2Remarks { get; set; }

    public List<HDRequistID> hDRequistIDs { get; set; }
}

public class HDDetails
{
    public string SPC_ID { get; set; }
    public string FROM_DATE { get; set; }
    public string TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
}


public class HDRequistID
{
    public string REQ_ID { get; set; }
}

public class DownTimeRpt1
{
    public string REQID { get; set; }
    public string CompanyID { get; set; }
}
public class breaktime1
{
    public int SER_DOWN_ID { get; set; }
    public string CompanyId { get; set; }
}

public class HDApproveModel
{
    public string UserID { get; set; }
    public string RequestID { get; set; }
    public string StatusID { get; set; }
    public string LocationCode { get; set; }
    public string Remarks { get; set; }
    public string ApprovalAmount { get; set; }
    public string ClaimAmount { get; set; }
    public string CompanyID { get; set; }
}

public class PvmGetPlans
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string PageNo { get; set; }
    public string PageSize { get; set; }

}

public class PvmUpdatePlansMobile
{

    public string PVD_PLANEND_TIME { get; set; }
    public DateTime PVD_PLANEXEC_DT { get; set; }
    public string PVD_PLANSPARES_COST { get; set; }
    public string PVD_PLANLABOUR_COST { get; set; }
    public string PVD_PLAN_REMARKS { get; set; }
    public int PVD_PLANSTA_ID { get; set; }
    public string PVD_PLAN_ID { get; set; }
    public int PVD_ID { get; set; }
    public string PVD_UPLOADED_DOC { get; set; }
    public string COMPANYID { get; set; }

}

public class NewCheckListV2
{
    public int BCL_ID { get; set; }

    public int BCL_TP_ID { get; set; }
    public string CompanyId { get; set; }

    public string User_ID { get; set; }
    public string BCL_SUBMIT { get; set; }

}



public class ChecklistDetailMob
{
    public int BCL_CH_ID { get; set; }
    public string BCL_CH_CODE { get; set; }
    public string BCL_CH_NAME { get; set; }
    public string BCL_CH_SUB_CODE { get; set; }
    public bool RESPONSE { get; set; }

}

public class ChecklistDataV2Mob
{
    public int BCL_TP_ID { get; set; }
    public string BCL_TP_NAME { get; set; }
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public string BCL_SUB_TYPE { get; set; }
    public string ISFILE { get; set; }
    public string ISCOMMENTS { get; set; }
    public string ISDATE { get; set; }

    public DateTime DATERESPONSE { get; set; }
    public string FILEPATH { get; set; }
    public string TEXT { get; set; }
    public string RESPONSE { get; set; }
    public string ValidateInchareAction { get; set; }

    public string ValidateInchareComment { get; set; }

    public string ApprovalAction { get; set; }
    public string ApprovalComment { get; set; }


    public List<ChecklistDetailMob> checklistDetails = new List<ChecklistDetailMob>();
}
public class AssetBarScan
{
    public string AssetID { get; set; }
    public string AssetName { get; set; }
    public string AssetDescription { get; set; }
    public string LcmCode { get; set; }
    public string Scan_Flag { get; set; }
}


public class PostAssetBarScan
{
    public string AssetID { get; set; }
    public string AssetName { get; set; }
    public string LcmCode { get; set; }
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public string Ast_Status_Code { get; set; }
}


public class Asset_Type
{
    public string TAG_CODE { get; set; }
    public string TAG_NAME { get; set; }

}

public class CreateScheduleVisits
{
    public string Companyid { get; set; }
    public string FROMDATE { get; set; }
    public string TODATE { get; set; }
    public string UserId { get; set; }
    public List<ScheduleVisits> ScheduleVisits { get; set; }

}
public class ScheduleVisits
{
    public string DATEVALUE { get; set; }
    public string CTY_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string REMARKS { get; set; }
    public bool ticked { get; set; }

}
public class AssetScanModel
{
    public string CompanyId { get; set; }
    public string UserId { get; set; }
    public List<string> Locations { get; set; }
    public List<string> AstDetails { get; set; }
    public int AstType { get; set; }
    public DateTime Month { get; set; }
    public int PageNo { get; set; }
    public int PageSize { get; set; }

}
public class LocationCords
{
    public string CompanyId { get; set; }
    public string User_ID { get; set; }
    public string BCL_TYPE { get; set; }
    
    public string LCM_CODE { get; set; }

}
public class HD_SPACE_ALLOCATE_DETAILS_Val
{

    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }
    public string COMPANYID { get; set; }

}
